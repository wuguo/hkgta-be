package testCase;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.RoomResDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml" })
public class ResversionInit {

	@Autowired
	private PMSApiService	PMSService;

	@Test
	public void testHotelReservationInit() throws Exception {
		RoomResDto dto = new RoomResDto();
		dto.setAddress1("Rm 12344");
		dto.setAddress2("HHHH House");
		dto.setAddress3("Kowloon");
		dto.setAddress4("Hong Kong");
		dto.setAdultCount(2L);
		dto.setChildCount(0L);
		dto.setEmail("peter123@yahoo.com");
		dto.setStartDate("2015-08-28");
		dto.setEndDate("2015-08-29");
		dto.setSurname("Chan");
		dto.setGivenName("Peter");
		dto.setGender("Male");
		dto.setMembershipID("998");
		dto.setNamePrefix("Mr");
		dto.setPhoneNumber("93336333");
		dto.setRoomRateCode("BPIC");
		dto.setRoomTypeCode("STND");
		MessageResult result = PMSService.hotelReservation(dto);

		Map<String, String> propertiesMap = result.getProperties();
		String amountAfterTaxStr = propertiesMap.get("AmountAfterTax");
		// String resIdTypeStr = propertiesMap.get("ResID_Type");
		String resIdStr = propertiesMap.get("ResID_Value");

		System.out.println(String.format("%s\t%s", resIdStr, amountAfterTaxStr));

	}

}
