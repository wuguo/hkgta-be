package testCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml" })
public class UpdateRoomStatus {

	@Autowired
	private PMSApiService	pMSService;

	@Test
	public void testUpdateRoomStatus() throws Exception {
		UpdateRoomStatusDto dto = new UpdateRoomStatusDto();
		dto.setReasonCode("OOTH");
		dto.setRoleName("ROOM_ATTENDANT");
		dto.setRoomNo("123");
		dto.setStatus("NEEDS_INSPECTION");
		dto.setUserId("0000001");
		MessageResult messageResult = pMSService.updateRoomStatus(dto);

		System.out.println(messageResult);

	}

}
