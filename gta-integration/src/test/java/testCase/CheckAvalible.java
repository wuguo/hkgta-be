package testCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.CheckHotelAvailableDto;
import com.sinodynamic.hkgta.dto.pms.RatePlanDto;
import com.sinodynamic.hkgta.dto.pms.RoomRateDto;
import com.sinodynamic.hkgta.dto.pms.RoomStayDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml" })
public class CheckAvalible {

	@Autowired
	private PMSApiService	pMSService;

	@Test
	public void testQueryAvailableHotelsReturnObjct() throws Exception {
		CheckHotelAvailableDto checkHotelAvailableDto = new CheckHotelAvailableDto();
		// DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		checkHotelAvailableDto.setStayBeginDate("2016-02-27");
		checkHotelAvailableDto.setStayEndDate("2016-02-28");
		checkHotelAvailableDto.setAdultCount(2);
		checkHotelAvailableDto.setChildCount(0);
		MessageResult messageResult = pMSService.getkHotelAvailable(checkHotelAvailableDto);
		System.out.println("Message Result : " + messageResult);
		System.out.println("Room Type : ");
		RoomStayDto dto = (RoomStayDto) messageResult.getObject();
		for (RoomTypeDto roomtype : dto.getRoomTypes()) {
			System.out.println(String.format("%s\t%s", roomtype.getRoomTypeCode(), roomtype.getRoomTypeDescription()));
		}

		System.out.println("Room Plan : ");
		for (RatePlanDto ratePlanDto : dto.getRatePlans()) {
			System.out.println(String.format("%s\t%s", ratePlanDto.getRatePlanCode(), ratePlanDto.getRatePlanName()));
		}

		System.out.println("Room Rate : ");
		for (RoomRateDto roomRate : dto.getRoomRates()) {
			System.out.println(String.format("%s\t%s\t%s", roomRate.getRoomTypeCode(), roomRate.getRatePlanCode(), roomRate.getAmountAfterTax()));
		}

		System.out.println(messageResult);

	}

}
