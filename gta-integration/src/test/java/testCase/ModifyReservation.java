package testCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.RoomResDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml" })
public class ModifyReservation {

	@Autowired
	private PMSApiService pMSService;

	@Test
	public void testQueryAvailableHotelsReturnObjct() throws Exception {
		RoomResDto dto = new RoomResDto();
		dto.setReservationID("7997");
		dto.setMembershipID("0000043");
		dto.setAddress1("Address1M");
		dto.setAddress2("Addresstest2N");
		dto.setAddress3("Address3");
		dto.setAddress4("address4");
		dto.setEmail("peter888@yahoo.com");
		dto.setSurname("Cha3");
		dto.setGivenName("Petr112");
		dto.setGender("Male");
		dto.setNamePrefix("Mrs");
		dto.setPhoneNumber("93338888");


		MessageResult messageResult = pMSService.modifyReservation(dto);
		System.out.println(messageResult);

	}

}
