package testCase;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.FoodItemDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml" })
public class QuertOrderByOutletId {

	@Autowired
	private PMSApiService pMSService;

	@Test
	public void testQueryAvailableHotelsReturnObjct() throws Exception {
		String outedId = "0001";
		List<FoodItemDto> foodItemDtoList = pMSService.queryOrdersByOutletId(outedId);
		System.out.println("print foodItemDtoList! ");
		if (foodItemDtoList != null) {
			for (FoodItemDto foodItemDto : foodItemDtoList) {
				System.out.println(String.format("%s\t%s\t%s", foodItemDto.getId(), foodItemDto.getPrice(), foodItemDto.getDesc()));
			}
		}
	}

}
