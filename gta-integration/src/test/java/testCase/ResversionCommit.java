package testCase;

import java.math.BigDecimal;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.HotelPaymentDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml" })
public class ResversionCommit {

	@Autowired
	private PMSApiService	PMSService;

	@Test
	public void testPayment() throws Exception {
		HotelPaymentDto dto = new HotelPaymentDto();
		dto.setAmount(new BigDecimal("6930"));
		dto.setAmountAfterTax(new BigDecimal("6930"));
		dto.setResID("255");
		// byCard(dto);
		byCashValue(dto);
		MessageResult messageResult = PMSService.payment(dto);
		Map<String, String> propertyMap = messageResult.getProperties();
		String aa = propertyMap.get("");
		System.out.println(messageResult);
	}

	private void byCard(HotelPaymentDto dto) throws Exception {
		dto.setPaymentType("Card");// Card;CashValue;
		// Card item :
		dto.setCardCode("MC");
		dto.setCardHolderName("Peter Chan");
		dto.setExpireDate("1614");
		dto.setMaskedCardNumber("*************99");
	}

	private void byCashValue(HotelPaymentDto dto) throws Exception {
		dto.setPaymentType("CashValue");// Card;CashValue;
		// CashValue Item:
		dto.setCertificateNumber("1231231");
		dto.setMemberNumber("999");
	}

}
