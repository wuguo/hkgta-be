package testCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.CancelReservationDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml" })
public class ResversionCancel {

	@Autowired
	private PMSApiService	PMSService;

	@Test
	public void testReservationCancel() throws Exception {
		CancelReservationDto dto = new CancelReservationDto();
		dto.setResId("258");
		dto.setMemberName("hhh4");
		dto.setMembershipID("998");
		dto.setReservationTimeSpanStart("2015-08-28");
		dto.setReservationTimeSpanEnd("2015-08-29");
		MessageResult result = PMSService.cancelReservation(dto);
		System.out.println(result);
	}

}
