package testCase;

import java.math.BigDecimal;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.ChargePostingDto;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.util.constant.ChargeType;
import com.sinodynamic.hkgta.util.response.MessageResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-integration.xml" })
public class ChargePosting {

	@Autowired
	private PMSApiService PMSService;

	@Test
	public void chargePostingReserved() throws Exception {

		ChargePostingDto dto = new ChargePostingDto();
		dto.setAmount(new BigDecimal("10"));
		dto.setDescription("TEST Payment2");
		dto.setPmsRevenueCode("9000");
		dto.setResvID("7883");
		dto.setTicketID("123456798");
		dto.setType(ChargeType.CHARGE.getCode());
		MessageResult result = PMSService.chargePostingReserved(dto);
		Map<String, String> propertiesMap = result.getProperties();
		// String resultCodeStr = propertiesMap.get("resultCode");
		// String messageStr = propertiesMap.get("message");
		System.out.println("result:" + result);
	}

}
