package com.sinodynamic.hkgta.integration.util;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;

public class CloseableHttpClientFactoryBean implements FactoryBean<HttpClient> {

	@Value(value ="#{spaProperties['maxTotal']}")
	private  int maxTotal;
	@Value(value ="#{spaProperties['defaultMaxPerRoute']}")
	private  int defaultMaxPerRoute;
	@Value(value ="#{spaProperties['connectTimeout']}")
	private int connectTimeout;
	@Value(value ="#{spaProperties['socketTimeout']}")
	private int socketTimeout;
	
	public int getMaxTotal() {
		return maxTotal;
	}

	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}

	public int getDefaultMaxPerRoute() {
		return defaultMaxPerRoute;
	}

	public void setDefaultMaxPerRoute(int defaultMaxPerRoute) {
		this.defaultMaxPerRoute = defaultMaxPerRoute;
	}
	


	@Override
	public HttpClient getObject() throws Exception {
		 HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();

	        // setup a Trust Strategy that allows all certificates.
	        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
	            public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
	                return true;
	            }
	        }).build();
	        httpClientBuilder.setSslcontext(sslContext);

	       
	        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	        //SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
	                .register("http", PlainConnectionSocketFactory.getSocketFactory())
	                .register("https", sslSocketFactory)
	                .build();

	        // now, we create connection-manager using our Registry.
	        //      -- allows multi-threaded use
	        PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager( socketFactoryRegistry);
	        connMgr.setMaxTotal(maxTotal);
	        connMgr.setDefaultMaxPerRoute(defaultMaxPerRoute);
	        
	        
            
	        org.apache.http.client.config.RequestConfig.Builder configBuilder = RequestConfig.custom();
            configBuilder.setConnectTimeout(connectTimeout);
            configBuilder.setSocketTimeout(socketTimeout);
            httpClientBuilder.setDefaultRequestConfig(configBuilder.build());
            
            httpClientBuilder.setConnectionManager(connMgr);
	        //httpClientBuilder.setDefaultRequestConfig(requestConfig);
	        httpClientBuilder.setRetryHandler(new DefaultHttpRequestRetryHandler(1, false));
	        
	        // 连接保持时间
	        httpClientBuilder.setKeepAliveStrategy(new ConnectionKeepAliveStrategy() {
	            @Override
	            public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
	                // Honor 'keep-alive' header
	                HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
	                while (it.hasNext()) {
	                    HeaderElement he = it.nextElement();
	                    String param = he.getName();
	                    String value = he.getValue();
	                    if (value != null && param.equalsIgnoreCase("timeout")) {
	                        try {
	                            return Long.parseLong(value) * 1000;
	                        } catch (NumberFormatException ignore) {
	                        }
	                    }
	                }
	                return connectTimeout * 2;
	            }
	        });

	        // finally, build the HttpClient;
	        //      -- done!
	        //CloseableHttpClient client = httpClientBuilder.build();
	        
	        //httpClientBuilder.setDefaultRequestConfig(configBuilder.build());
	        CloseableHttpClient client = httpClientBuilder.build();
	        
	        
	        return client;
	}



	@Override
	public Class<?> getObjectType() {
		return HttpClient.class;
	}



	@Override
	public boolean isSingleton() {
		return true;
	}

}
