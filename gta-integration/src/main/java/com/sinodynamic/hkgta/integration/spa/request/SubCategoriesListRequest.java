package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class SubCategoriesListRequest extends BaseRequest {

	private static final long serialVersionUID = 3517897826892251982L;

	@UrlParameter(name="categoryCode",pos=1)
	private String categoryCode;

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	
	
}
