package com.sinodynamic.hkgta.integration.spa.request;

import java.io.Serializable;
import java.util.Date;

import com.sinodynamic.hkgta.integration.util.FieldRules;

public class ServiceInfoItem implements Serializable {
	private static final long serialVersionUID = 7127633935237529930L;

	@FieldRules(nullable=false)
	private String serviceCode;
	
	@FieldRules(nullable=false)
	private String therapistCode;
	
	private Date startTime;
	
	private Date endTime;
	
	@FieldRules(nullable=false)
	private String roomCode;
	
	@FieldRules(nullable=false)
	private TherapistRequestType therapistRequestType;

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getTherapistCode() {
		return therapistCode;
	}

	public void setTherapistCode(String therapistCode) {
		this.therapistCode = therapistCode;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public TherapistRequestType getTherapistRequestType() {
		return therapistRequestType;
	}

	public void setTherapistRequestType(TherapistRequestType therapistRequestType) {
		this.therapistRequestType = therapistRequestType;
	}
	
	
	
}
