package com.sinodynamic.hkgta.integration.spa.request;

import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class CCPaymentRequest extends BaseRequest{

	private static final long serialVersionUID = 4269913615451533903L;

	@UrlParameter(name="InvoiceNo")
	@FieldRules(nullable=false)
	private String invoiceNo;
	
	@UrlParameter(name="GuestId")
	private String guestId;
	
	@UrlParameter(name="GuestCode")
	private String guestCode;
	
	@UrlParameter(name="AmountPaid")
	private BigDecimal amountPaid;
	
	@UrlParameter(name="TipAmount")
	private BigDecimal tipAmount;
	
	@UrlParameter(name="CCNumber")
	private String CCNumber;
	
	@UrlParameter(name="ExpiryDate",dateFormat="yyyy-MM-dd")
	//@FieldRules(format="MM/dd")
	private Date expiryDate;
	
	@UrlParameter(name="CardType")
	private String cardType;
	
	@UrlParameter(name="BankName")
	private String bankName;
	
	@UrlParameter(name="SecurityCode")
	private String securityCode;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getGuestId() {
		return guestId;
	}

	public void setGuestId(String guestId) {
		this.guestId = guestId;
	}

	public String getGuestCode() {
		return guestCode;
	}

	public void setGuestCode(String guestCode) {
		this.guestCode = guestCode;
	}

	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public BigDecimal getTipAmount() {
		return tipAmount;
	}

	public void setTipAmount(BigDecimal tipAmount) {
		this.tipAmount = tipAmount;
	}

	public String getCCNumber() {
		return CCNumber;
	}

	public void setCCNumber(String cCNumber) {
		CCNumber = cCNumber;
	}

	

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	
	
	
}
