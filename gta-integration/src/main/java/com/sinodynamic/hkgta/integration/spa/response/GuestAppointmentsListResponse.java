package com.sinodynamic.hkgta.integration.spa.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"success",
"total",
"Appointments"
})
public class GuestAppointmentsListResponse extends BaseResponse {
	private static final long serialVersionUID = -8926405968956747323L;
	@JsonProperty("success")
	private Boolean success;
	@JsonProperty("message")
	private String message;
	@JsonProperty("total")
	private Integer total;
	@JsonProperty("Appointments")
	private List<Appointment> appointments = new ArrayList<Appointment>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	* 
	* @return
	* The success
	*/
	@JsonProperty("success")
	public Boolean getSuccess() {
	return success;
	}

	/**
	* 
	* @param success
	* The success
	*/
	@JsonProperty("success")
	public void setSuccess(Boolean success) {
	this.success = success;
	}
	
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	* 
	* @return
	* The total
	*/
	@JsonProperty("total")
	public Integer getTotal() {
	return total;
	}

	/**
	* 
	* @param total
	* The total
	*/
	@JsonProperty("total")
	public void setTotal(Integer total) {
	this.total = total;
	}

	/**
	* 
	* @return
	* The Appointments
	*/
	@JsonProperty("Appointments")
	public List<Appointment> getAppointments() {
	return appointments;
	}

	/**
	* 
	* @param Appointments
	* The Appointments
	*/
	@JsonProperty("Appointments")
	public void setAppointments(List<Appointment> Appointments) {
	this.appointments = Appointments;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
