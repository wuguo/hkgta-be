package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "GuestCode", "GuestName", "paymentType", "DeletionDate", "PaymentDate", "Amount", "InvoiceId",
		"PaymentID", "CreditCard#", "CardExpiry", "Remarks" })
public class Refund extends BaseResponse{

	private static final long serialVersionUID = 3075356519144070439L;
	@JsonProperty("GuestCode")
	private String patronId;
	@JsonProperty("GuestName")
	private String patronName;
	@JsonProperty("PaymentType")
	private String paymentType;
	@JsonProperty("DeletionDate")
	private String dateTime;
	@JsonProperty("PaymentDate")
	private String paymentDate;
	@JsonProperty("Amount")
	private String amount;
//	@JsonProperty("status")
//	private String status;
//	@JsonProperty("invoice")
//	private String invoice;
	@JsonProperty("InvoiceId")
	private String refInvoiceId;
	@JsonProperty("PaymentID")
	private String refPaymentId;
	@JsonProperty("CreditCard#")
	private String creditCard;
	@JsonProperty("CardExpiry")
	private String creditCardDate;
	private String refundRequest;
	@JsonProperty("Remarks")
	private String remark;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getPatronId() {
		return patronId;
	}

	public void setPatronId(String patronId) {
		this.patronId = patronId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//
//	public String getInvoice() {
//		return invoice;
//	}
//
//	public void setInvoice(String invoice) {
//		this.invoice = invoice;
//	}

	public String getRefInvoiceId() {
		return refInvoiceId;
	}

	public void setRefInvoiceId(String refInvoiceId) {
		this.refInvoiceId = refInvoiceId;
	}

	public String getRefPaymentId() {
		return refPaymentId;
	}

	public void setRefPaymentId(String refPaymentId) {
		this.refPaymentId = refPaymentId;
	}

	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	public String getCreditCardDate() {
		return creditCardDate;
	}

	public void setCreditCardDate(String creditCardDate) {
		this.creditCardDate = creditCardDate;
	}

	public String getRefundRequest() {
		return refundRequest;
	}

	public void setRefundRequest(String refundRequest) {
		this.refundRequest = refundRequest;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}