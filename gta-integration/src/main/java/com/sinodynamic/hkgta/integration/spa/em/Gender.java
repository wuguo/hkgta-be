package com.sinodynamic.hkgta.integration.spa.em;


public enum Gender {
    
	MALE("M"),FEMALE("F");
	
	private Gender(String code){
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}



	private String code;
}
