package com.sinodynamic.hkgta.integration.spa.request;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class BaseRequest implements Serializable {
	private static final long serialVersionUID = 3237462641541415808L;

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
