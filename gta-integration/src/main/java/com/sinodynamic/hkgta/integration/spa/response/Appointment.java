package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"GroupId",
"GuestID",
"service_id",
"StartTime",
"CheckInPossible",
"CheckInTime",
"ServiceCount",
"Status",
"GuestFName",
"GuestLName",
"GuestPhone",
"EmployeeFName",
"EmployeeLName",
"ServiceName",
"Room",
"FeedbackEnteredFlag",
"FeedbackCommentPresentFlag",
"hasScd"
})
public class Appointment extends BaseResponse{

	private static final long serialVersionUID = -6674512710525593326L;
	@JsonProperty("GroupId")
	private String groupId;
	@JsonProperty("GuestID")
	private String guestID;
	@JsonProperty("service_id")
	private String serviceId;
	@JsonProperty("StartTime")
	private String startTime;
	@JsonProperty("CheckInPossible")
	private Integer checkInPossible;
	@JsonProperty("CheckInTime")
	private String checkInTime;
	@JsonProperty("ServiceCount")
	private Integer serviceCount;
	@JsonProperty("Status")
	private Integer status;
	@JsonProperty("GuestFName")
	private String guestFName;
	@JsonProperty("GuestLName")
	private String guestLName;
	@JsonProperty("GuestPhone")
	private String guestPhone;
	@JsonProperty("EmployeeFName")
	private String employeeFName;
	@JsonProperty("EmployeeLName")
	private String employeeLName;
	@JsonProperty("ServiceName")
	private String serviceName;
	@JsonProperty("Room")
	private Object room;
	@JsonProperty("FeedbackEnteredFlag")
	private Integer feedbackEnteredFlag;
	@JsonProperty("FeedbackCommentPresentFlag")
	private Integer feedbackCommentPresentFlag;
	@JsonProperty("hasScd")
	private Integer hasScd;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGuestID() {
		return guestID;
	}

	public void setGuestID(String guestID) {
		this.guestID = guestID;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Integer getCheckInPossible() {
		return checkInPossible;
	}

	public void setCheckInPossible(Integer checkInPossible) {
		this.checkInPossible = checkInPossible;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public Integer getServiceCount() {
		return serviceCount;
	}

	public void setServiceCount(Integer serviceCount) {
		this.serviceCount = serviceCount;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getGuestFName() {
		return guestFName;
	}

	public void setGuestFName(String guestFName) {
		this.guestFName = guestFName;
	}

	public String getGuestLName() {
		return guestLName;
	}

	public void setGuestLName(String guestLName) {
		this.guestLName = guestLName;
	}

	public String getGuestPhone() {
		return guestPhone;
	}

	public void setGuestPhone(String guestPhone) {
		this.guestPhone = guestPhone;
	}

	public String getEmployeeFName() {
		return employeeFName;
	}

	public void setEmployeeFName(String employeeFName) {
		this.employeeFName = employeeFName;
	}

	public String getEmployeeLName() {
		return employeeLName;
	}

	public void setEmployeeLName(String employeeLName) {
		this.employeeLName = employeeLName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Object getRoom() {
		return room;
	}

	public void setRoom(Object room) {
		this.room = room;
	}

	public Integer getFeedbackEnteredFlag() {
		return feedbackEnteredFlag;
	}

	public void setFeedbackEnteredFlag(Integer feedbackEnteredFlag) {
		this.feedbackEnteredFlag = feedbackEnteredFlag;
	}

	public Integer getFeedbackCommentPresentFlag() {
		return feedbackCommentPresentFlag;
	}

	public void setFeedbackCommentPresentFlag(Integer feedbackCommentPresentFlag) {
		this.feedbackCommentPresentFlag = feedbackCommentPresentFlag;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public Integer getHasScd() {
	return hasScd;
	}
	
	public void setHasScd(Integer hasScd) {
	this.hasScd = hasScd;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
}