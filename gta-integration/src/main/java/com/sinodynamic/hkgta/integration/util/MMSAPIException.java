package com.sinodynamic.hkgta.integration.util;

public class MMSAPIException extends RuntimeException {

	public MMSAPIException() {
		super();
	}

	public MMSAPIException(String message, Throwable cause) {
		super(message, cause);
	}

	public MMSAPIException(String message) {
		super(message);
	}

	public MMSAPIException(Throwable cause) {
		super(cause);
	}

}
