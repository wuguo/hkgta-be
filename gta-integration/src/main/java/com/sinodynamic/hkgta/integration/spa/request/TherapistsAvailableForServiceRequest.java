package com.sinodynamic.hkgta.integration.spa.request;

import java.util.Date;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class TherapistsAvailableForServiceRequest extends BaseRequest {
	
	private static final long serialVersionUID = -2233444208782336244L;
	@UrlParameter(name="requestDate",dateFormat="yyyy-MM-dd")
	@FieldRules(nullable=false)
	private Date requestDate;
	@UrlParameter(name = "serviceCode")
	@FieldRules(nullable=false)
	private String serviceCode;
	@UrlParameter(name = "type",formatterMethod="formatter")
	@FieldRules(nullable=false)
	private TherapistRequestType type;
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public TherapistRequestType getType() {
		return type;
	}
	public void setType(TherapistRequestType type) {
		this.type = type;
	}
	
	public String formatter(TherapistRequestType type) {
		return type.getCode();
	}
	
}