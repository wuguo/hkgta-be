package com.sinodynamic.hkgta.integration.spa.request;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.sinodynamic.hkgta.integration.util.BeanValidate;
import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class BookAppointmentRequest extends BaseRequest {
	private static final long serialVersionUID = 5476783179309376399L;

	@UrlParameter(name = "GuestCode")
	@FieldRules(nullable = false)
	private String guestCode;

	@UrlParameter(name = "ServiceInfo",formatterMethod="formatter")
	private List<ServiceInfoItem> serviceInfo;
	
	@UrlParameter(name = "startTime",dateFormat="yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	
	@UrlParameter(name = "invoiceNo")
	private String invoiceNo; 

	public String getGuestCode() {
		return guestCode;
	}

	public void setGuestCode(String guestCode) {
		this.guestCode = guestCode;
	}

	public List<ServiceInfoItem> getServiceInfo() {
		return serviceInfo;
	}

	public void setServiceInfo(List<ServiceInfoItem> serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String formatter(List<ServiceInfoItem> items) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		String result = "";
		StringBuffer sbf = new StringBuffer();
		//boolean isFirst = true;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(ServiceInfoItem item:items) {
			StringBuffer itemStr = new StringBuffer();
			BeanValidate.validateFields(item, ServiceInfoItem.class);
			itemStr.append(item.getServiceCode() + "|" + item.getTherapistCode() + "|");
			if(null != item.getStartTime()){
				String startTimeStr = df.format(item.getStartTime());
				itemStr.append(startTimeStr + "|");
			} else {
				itemStr.append( "" + "|");
			}
			if(null != item.getEndTime()){
				String endTimeStr = df.format(item.getEndTime());
				itemStr.append( endTimeStr + "|");
			} else {
				itemStr.append( "" + "|");
			}
			itemStr.append( item.getRoomCode() + "|");
			itemStr.append( item.getTherapistRequestType().getCode());
			
			sbf.append(itemStr).append(",");
			/*if(isFirst) {
				sbf.append(itemStr).append(",");
				isFirst = false;
			} else {
				sbf.append(",").append(itemStr).append(",");
			}*/
		}
		result = sbf.toString();
		if(!StringUtils.isBlank(result)) {
			result = StringUtils.removeEnd(result,",");
		}
		return result;
	}
	

}
