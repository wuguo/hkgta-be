package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"CenterCode",
"CenterName",
"InvoiceNo",
"InvoiceDate",
"InvoiceCloseDate",
"InvoicePrice",
"InvoicePayments",
"GuestCode",
"Guest",
"GuestGender",
"RevCCCC",
"RevMembershipRedemption",
"RevMembershipInitialRec",
"RevMembershipMonthlyRec",
"RevGiftCard",
"RevPrePaidCard",
"RevPackageRedemption",
"RevPackageInitialRec",
"RevPackageExpired",
"RevTotal"
})
public class InvoiceRevenue extends BaseResponse{
	private static final long serialVersionUID = -7663318137607718224L;
	@JsonProperty("CenterCode")
	private String centerCode;
	@JsonProperty("CenterName")
	private String centerName;
	@JsonProperty("InvoiceNo")
	private String invoiceNo;
	@JsonProperty("InvoiceDate")
	private String invoiceDate;
	@JsonProperty("InvoiceCloseDate")
	private String invoiceCloseDate;
	@JsonProperty("InvoicePrice")
	private String invoicePrice;
	@JsonProperty("InvoicePayments")
	private String invoicePayments;
	@JsonProperty("GuestCode")
	private String guestCode;
	@JsonProperty("Guest")
	private String guest;
	@JsonProperty("GuestGender")
	private String guestGender;
	@JsonProperty("RevCCCC")
	private String revCCCC;
	@JsonProperty("RevMembershipRedemption")
	private String revMembershipRedemption;
	@JsonProperty("RevMembershipInitialRec")
	private String revMembershipInitialRec;
	@JsonProperty("RevMembershipMonthlyRec")
	private String revMembershipMonthlyRec;
	@JsonProperty("RevGiftCard")
	private String revGiftCard;
	@JsonProperty("RevPrePaidCard")
	private String revPrePaidCard;
	@JsonProperty("RevPackageRedemption")
	private String revPackageRedemption;
	@JsonProperty("RevPackageInitialRec")
	private String revPackageInitialRec;
	@JsonProperty("RevPackageExpired")
	private String revPackageExpired;
	@JsonProperty("RevTotal")
	private String revTotal;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	

	public String getCenterCode() {
		return centerCode;
	}

	public void setCenterCode(String centerCode) {
		this.centerCode = centerCode;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceCloseDate() {
		return invoiceCloseDate;
	}

	public void setInvoiceCloseDate(String invoiceCloseDate) {
		this.invoiceCloseDate = invoiceCloseDate;
	}

	public String getInvoicePrice() {
		return invoicePrice;
	}

	public void setInvoicePrice(String invoicePrice) {
		this.invoicePrice = invoicePrice;
	}

	public String getInvoicePayments() {
		return invoicePayments;
	}

	public void setInvoicePayments(String invoicePayments) {
		this.invoicePayments = invoicePayments;
	}

	public String getGuestCode() {
		return guestCode;
	}

	public void setGuestCode(String guestCode) {
		this.guestCode = guestCode;
	}

	public String getGuest() {
		return guest;
	}

	public void setGuest(String guest) {
		this.guest = guest;
	}

	public String getGuestGender() {
		return guestGender;
	}

	public void setGuestGender(String guestGender) {
		this.guestGender = guestGender;
	}

	public String getRevCCCC() {
		return revCCCC;
	}

	public void setRevCCCC(String revCCCC) {
		this.revCCCC = revCCCC;
	}

	public String getRevMembershipRedemption() {
		return revMembershipRedemption;
	}

	public void setRevMembershipRedemption(String revMembershipRedemption) {
		this.revMembershipRedemption = revMembershipRedemption;
	}

	public String getRevMembershipInitialRec() {
		return revMembershipInitialRec;
	}

	public void setRevMembershipInitialRec(String revMembershipInitialRec) {
		this.revMembershipInitialRec = revMembershipInitialRec;
	}

	public String getRevMembershipMonthlyRec() {
		return revMembershipMonthlyRec;
	}

	public void setRevMembershipMonthlyRec(String revMembershipMonthlyRec) {
		this.revMembershipMonthlyRec = revMembershipMonthlyRec;
	}

	public String getRevGiftCard() {
		return revGiftCard;
	}

	public void setRevGiftCard(String revGiftCard) {
		this.revGiftCard = revGiftCard;
	}

	public String getRevPrePaidCard() {
		return revPrePaidCard;
	}

	public void setRevPrePaidCard(String revPrePaidCard) {
		this.revPrePaidCard = revPrePaidCard;
	}

	public String getRevPackageRedemption() {
		return revPackageRedemption;
	}

	public void setRevPackageRedemption(String revPackageRedemption) {
		this.revPackageRedemption = revPackageRedemption;
	}

	public String getRevPackageInitialRec() {
		return revPackageInitialRec;
	}

	public void setRevPackageInitialRec(String revPackageInitialRec) {
		this.revPackageInitialRec = revPackageInitialRec;
	}

	public String getRevPackageExpired() {
		return revPackageExpired;
	}

	public void setRevPackageExpired(String revPackageExpired) {
		this.revPackageExpired = revPackageExpired;
	}

	public String getRevTotal() {
		return revTotal;
	}

	public void setRevTotal(String revTotal) {
		this.revTotal = revTotal;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}