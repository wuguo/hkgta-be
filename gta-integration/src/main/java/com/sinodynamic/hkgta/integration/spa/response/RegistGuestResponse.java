package com.sinodynamic.hkgta.integration.spa.response;

import com.fasterxml.jackson.annotation.JsonProperty;
public class RegistGuestResponse extends BaseResponse {
	private static final long serialVersionUID = -1263845653741164788L;

	private boolean success;

	private String message;
	@JsonProperty("UserId")
	private String userId;
	@JsonProperty("UserCode")
	private String userCode;
	@JsonProperty("UserPassword")
	private String userPassword;

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
}
