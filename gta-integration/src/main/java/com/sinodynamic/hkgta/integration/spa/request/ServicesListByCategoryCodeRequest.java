package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class ServicesListByCategoryCodeRequest extends BaseRequest {
	private static final long serialVersionUID = 8783233325125819826L;
	@UrlParameter(name="categoryCode",pos=1)
	@FieldRules(nullable = false)
	private String categoryCode;

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	
	
}
