package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"CanBook",
"CatalogNew",
"CatalogRecommended",
"CategoryId",
"CategoryName",
"Description",
"Duration",
"FinalPrice",
"HasVariant",
"id",
"IncludesTax",
"IsVariant",
"Name",
"Price",
"PriceText",
"ShowPrice",
"SubCategoryId",
"SubCategoryName",
"Type",
"VideoText",
"VideoText2",
"VideoText3",
"VideoText4",
"VideoURL",
"VideoURL2",
"VideoURL3",
"VideoURL4"
})
public class ServiceDetail extends BaseResponse{
	private static final long serialVersionUID = 8686592617322106004L;
	@JsonProperty("CanBook")
	private String canBook;
	@JsonProperty("CatalogNew")
	private String catalogNew;
	@JsonProperty("CatalogRecommended")
	private String catalogRecommended;
	@JsonProperty("CategoryId")
	private String categoryId;
	@JsonProperty("CategoryName")
	private String categoryName;
	@JsonProperty("Description")
	private String description;
	@JsonProperty("Duration")
	private String duration;
	@JsonProperty("FinalPrice")
	private String finalPrice;
	@JsonProperty("HasVariant")
	private String hasVariant;
	@JsonProperty("id")
	private String id;
	@JsonProperty("IncludesTax")
	private String includesTax;
	@JsonProperty("IsVariant")
	private String isVariant;
	@JsonProperty("Name")
	private String name;
	@JsonProperty("Price")
	private String price;
	@JsonProperty("PriceText")
	private String priceText;
	@JsonProperty("ShowPrice")
	private String showPrice;
	@JsonProperty("SubCategoryId")
	private String subCategoryId;
	@JsonProperty("SubCategoryName")
	private String subCategoryName;
	@JsonProperty("Type")
	private String type;
	@JsonProperty("VideoText")
	private String videoText;
	@JsonProperty("VideoText2")
	private String videoText2;
	@JsonProperty("VideoText3")
	private String videoText3;
	@JsonProperty("VideoText4")
	private String videoText4;
	@JsonProperty("VideoURL")
	private String videoURL;
	@JsonProperty("VideoURL2")
	private String videoURL2;
	@JsonProperty("VideoURL3")
	private String videoURL3;
	@JsonProperty("VideoURL4")
	private String videoURL4;
	@JsonProperty("resourceFile")
	private String resourceFile;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	
	public String getCanBook() {
		return canBook;
	}

	public void setCanBook(String canBook) {
		this.canBook = canBook;
	}

	public String getCatalogNew() {
		return catalogNew;
	}

	public void setCatalogNew(String catalogNew) {
		this.catalogNew = catalogNew;
	}

	public String getCatalogRecommended() {
		return catalogRecommended;
	}

	public void setCatalogRecommended(String catalogRecommended) {
		this.catalogRecommended = catalogRecommended;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(String finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getHasVariant() {
		return hasVariant;
	}

	public void setHasVariant(String hasVariant) {
		this.hasVariant = hasVariant;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIncludesTax() {
		return includesTax;
	}

	public void setIncludesTax(String includesTax) {
		this.includesTax = includesTax;
	}

	public String getIsVariant() {
		return isVariant;
	}

	public void setIsVariant(String isVariant) {
		this.isVariant = isVariant;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPriceText() {
		return priceText;
	}

	public void setPriceText(String priceText) {
		this.priceText = priceText;
	}

	public String getShowPrice() {
		return showPrice;
	}

	public void setShowPrice(String showPrice) {
		this.showPrice = showPrice;
	}

	public String getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVideoText() {
		return videoText;
	}

	public void setVideoText(String videoText) {
		this.videoText = videoText;
	}

	public String getVideoText2() {
		return videoText2;
	}

	public void setVideoText2(String videoText2) {
		this.videoText2 = videoText2;
	}

	public String getVideoText3() {
		return videoText3;
	}

	public void setVideoText3(String videoText3) {
		this.videoText3 = videoText3;
	}

	public String getVideoText4() {
		return videoText4;
	}

	public void setVideoText4(String videoText4) {
		this.videoText4 = videoText4;
	}

	public String getVideoURL() {
		return videoURL;
	}

	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}

	public String getVideoURL2() {
		return videoURL2;
	}

	public void setVideoURL2(String videoURL2) {
		this.videoURL2 = videoURL2;
	}

	public String getVideoURL3() {
		return videoURL3;
	}

	public void setVideoURL3(String videoURL3) {
		this.videoURL3 = videoURL3;
	}

	public String getVideoURL4() {
		return videoURL4;
	}

	public void setVideoURL4(String videoURL4) {
		this.videoURL4 = videoURL4;
	}

	public String getResourceFile() {
		return resourceFile;
	}
	public void setResourceFile(String resourceFile) {
		this.resourceFile = resourceFile;
	}
	
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}