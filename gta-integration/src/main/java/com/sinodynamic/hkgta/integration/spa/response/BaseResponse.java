package com.sinodynamic.hkgta.integration.spa.response;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class BaseResponse implements Serializable {
	private static final long serialVersionUID = -2492463400121986423L;

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
