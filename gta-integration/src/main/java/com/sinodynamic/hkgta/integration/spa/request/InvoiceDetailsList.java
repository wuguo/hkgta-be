package com.sinodynamic.hkgta.integration.spa.request;

import java.io.Serializable;
import java.util.List;

public class InvoiceDetailsList implements Serializable {
	private static final long serialVersionUID = 4185885381442134371L;
	private List<InvoiceDetails> invoiceDetailsList;

	public List<InvoiceDetails> getInvoiceDetailsList() {
		return invoiceDetailsList;
	}

	public void setInvoiceDetailsList(List<InvoiceDetails> invoiceDetailsList) {
		this.invoiceDetailsList = invoiceDetailsList;
	}
	
	

}
