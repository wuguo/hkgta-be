package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "CategoryId", "ParentCategoryId", "resourceFile",
		"CategoryName", "ParentCategoryName", "Description", "Name",
		"SortOrder", "ParentSortOrder" })
public class Category extends BaseResponse {
	private static final long serialVersionUID = -5191977214747793280L;
	@JsonProperty("id")
	private String id;
	@JsonProperty("CategoryId")
	private String categoryId;
	/*@JsonProperty("CategoryCode")
	private String categoryCode;*/
	@JsonProperty("ParentCategoryId")
	private String parentCategoryId;
	@JsonProperty("resourceFile")
	private Object resourceFile;
	@JsonProperty("CategoryName")
	private String categoryName;
	@JsonProperty("ParentCategoryName")
	private String parentCategoryName;
	@JsonProperty("Description")
	private String description;
	@JsonProperty("Name")
	private String name;
	@JsonProperty("SortOrder")
	private String sortOrder;
	@JsonProperty("ParentSortOrder")
	private String parentSortOrder;
	@JsonProperty("ImageURL")
	private String imageURL;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	/*public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}*/

	public String getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(String parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public Object getResourceFile() {
		return resourceFile;
	}

	public void setResourceFile(Object resourceFile) {
		this.resourceFile = resourceFile;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getParentCategoryName() {
		return parentCategoryName;
	}

	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getParentSortOrder() {
		return parentSortOrder;
	}

	public void setParentSortOrder(String parentSortOrder) {
		this.parentSortOrder = parentSortOrder;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
}