package com.sinodynamic.hkgta.integration.spa.request;

import java.math.BigDecimal;

import com.sinodynamic.hkgta.integration.util.UrlParameter;
import com.sinodynamic.hkgta.integration.util.UrlParameterAssemble;
/**
 *   Cash$AmountPaid
 *   Amex$AmountPaid$AmexNumber$AmexExpiry$AmexRcptNumber
 *   DinersCard$AmountPaid$DinersCardNumber$DinersCardExpiry$DinersCardReceiptNumber
 *   DiscoverCard$AmountPaid$Number$Expiry$ReceiptNumber
 *   MasterCard$AmountPaid$Number$Expiry$ReceiptNumber
 *   Visa$AmountPaid$Number$Expiry$ReceiptNumber
 * @author Nick_Xiong
 *
 */
public class PaymentsList {
	@UrlParameter(pos = 1, name = "paymentInstrument", map = "CashDetails=cash,AmexDetails=Amex,DinersCardDetails=DinersCard,DiscoverCardDetails=DiscoverCard,MasterCardDetails=MasterCard,VisaCardDetails=Visa")
	private String paymentInstrument;
	@UrlParameter(pos = 2, name = "amount")
	private BigDecimal amount;

	// payment is by Amex
	@UrlParameter(pos = 3, name = "amexNumber",condition="paymentInstrument=AmexDetails")
	private String amexNumber;
	@UrlParameter(pos = 4, name = "amexExpiry",condition="paymentInstrument=AmexDetails")
	private String amexExpiry;
	@UrlParameter(pos = 5, name = "amexExpiry",condition="paymentInstrument=AmexDetails")
	private String amexRcptNumber;

	// payment is by Diners Card
	@UrlParameter(pos = 3, name = "dinersCardNumber",condition="paymentInstrument=DinersCardDetails")
	private String dinersCardNumber;
	@UrlParameter(pos = 4, name = "dinersCardExpiry",condition="paymentInstrument=DinersCardDetails")
	private String dinersCardExpiry;
	@UrlParameter(pos = 5, name = "dinersCardReceiptNumber",condition="paymentInstrument=DinersCardDetails")
	private String dinersCardReceiptNumber;

	// payment is by Discover Card,master card and Visa
	@UrlParameter(pos = 3, name = "number",condition="paymentInstrument=DiscoverCardDetails,MasterCardDetails,VisaCardDetails")
	private String number;
	@UrlParameter(pos = 4, name = "expiry",condition="paymentInstrument=DiscoverCardDetails,MasterCardDetails,VisaCardDetails")
	private String expiry;
	@UrlParameter(pos = 5, name = "receiptNumber",condition="paymentInstrument=DiscoverCardDetails,MasterCardDetails,VisaCardDetails")
	private String receiptNumber;

	

	public String getPaymentInstrument() {
		return paymentInstrument;
	}

	public void setPaymentInstrument(String paymentInstrument) {
		this.paymentInstrument = paymentInstrument;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getAmexNumber() {
		return amexNumber;
	}

	public void setAmexNumber(String amexNumber) {
		this.amexNumber = amexNumber;
	}

	public String getAmexExpiry() {
		return amexExpiry;
	}

	public void setAmexExpiry(String amexExpiry) {
		this.amexExpiry = amexExpiry;
	}

	public String getAmexRcptNumber() {
		return amexRcptNumber;
	}

	public void setAmexRcptNumber(String amexRcptNumber) {
		this.amexRcptNumber = amexRcptNumber;
	}

	public String getDinersCardNumber() {
		return dinersCardNumber;
	}

	public void setDinersCardNumber(String dinersCardNumber) {
		this.dinersCardNumber = dinersCardNumber;
	}

	public String getDinersCardExpiry() {
		return dinersCardExpiry;
	}

	public void setDinersCardExpiry(String dinersCardExpiry) {
		this.dinersCardExpiry = dinersCardExpiry;
	}

	public String getDinersCardReceiptNumber() {
		return dinersCardReceiptNumber;
	}

	public void setDinersCardReceiptNumber(String dinersCardReceiptNumber) {
		this.dinersCardReceiptNumber = dinersCardReceiptNumber;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	
	public static void main(String[] args) throws Exception{
		//System.out.println(PaymentInstrument.AmexDetails);
		PaymentsList pay = new PaymentsList();
		pay.setPaymentInstrument("AmexDetails");
		pay.setAmexNumber("88888888");
		pay.setAmexExpiry("12/25");
		pay.setAmexRcptNumber("123456");
		pay.setAmount(new BigDecimal(1000));
		String str = UrlParameterAssemble.assembleString(pay,pay.getClass(),"$");
    	System.out.println(str);
	}

}
