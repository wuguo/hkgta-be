package com.sinodynamic.hkgta.integration.zoomtech.service;


import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.golfmachine.BatInfo;
import com.sinodynamic.hkgta.util.constant.AccessBatCmdType;

/**
 * Remote call Golf Feeding Machine. To replace gta-util/.../AccessDatabaseUtil.java
 * @author Sam Hui
 *
 */
public interface GolfMachineService {
	/*
	 * 0,Init, 
	 * 3.Ready(Show teeline number)
	 * 4.Checkin
	 * 5.Play
	 * 6.Ready(Show END)
	 * 7.Manual
	 */
	//public final static String IS_ON="4,5"; // use app.properties file instead (SAMHUI 20160322)
	
	/**
	 * set host/url of the golf feeding machine. Default get from app.properties file  
	 * @param host
	 */
	public void setRemoteHost(String host);

	/**
	 * try to reach the host
	 * @return true if alive
	 * @throws Exception 
	 */
	public boolean testHost() throws Exception;	
	
	/**
	 * format command in sql value part and post to remote service to insert into batCmd table of GolfScreen2005.mdb  
	 * @param batNo
	 * @param startTime
	 * @param timeCount
	 * @param ballCount
	 * @param CmdType
	 * @throws Exception
	 */
	public  void sendBatchCmd(long batNo, String startTime, long timeCount, long ballCount, long CmdType) throws Exception;
	
	
	/**
	 * 
	 * @param batNo Golf bay no. 
	 * @param ballFeedingStatus {"ON" | "OFF"}
	 * @return True - success; False - not success 
	 * @throws Exception
	 */
	public Boolean changeTeeupMachineStatsbyBatNo(Long batNo, String ballFeedingStatus)throws Exception;
		
	/**
	 * scheduling to turn on a golf feeding machine in a specific date time 
	 * @param batNo Golf bay no.
	 * @param startTime
	 * @param endTime
	 * @throws Exception
	 */
	public void setTeeupMachineStatsbyCheckinTime(Long batNo, Date startTime, Date endTime) throws Exception;
	
	/**
	 * scheduling to turn off a golf feeding machine in a specific date time 
	 * @param batNo Golf bay no.
	 * @param startTime
	 * @param endTime
	 * @throws Exception
	 */
	public void setTeeupMachineStatsbyCancelTime(Long batNo, Date startTime, Date endTime) throws Exception;
	
	/**
	 * Get remote table BatInfo in json format and covert to the list of BatInof DTO object 
	 * @param batNo bay Number or null to retrieve all bay info
	 * @return list of BatInof DTO object
	 * @throws Exception 
	 */
	public List<BatInfo> getBatInfo(Long batNo) throws Exception;
	
	/**
	 * get all records remote table BatInfo from Golf feeding machine
	 * @return Map of all BatInfo. The key map is batNo
	 * @throws Exception
	 */
	public Map<Long, BatInfo> getAllBatInfosMap() throws Exception;	
}
