/**
 * 
 */
package com.sinodynamic.hkgta.integration.util;

import java.lang.reflect.Field;
import java.util.Comparator;

import com.sinodynamic.hkgta.integration.util.UrlParameter;

/**   
 *  
 * 
 * @Description:   
 * @Author:       XiongLiangSheng  
 * @CreateDate:   2014年10月9日 下午4:29:41   
 * @Version:      v1.0
 *    
 * Date    	CR/DEFECT   Modified By    Description of change
 */
public class FieldComparator implements Comparator<Object>
{
  public int compare(Object arg0, Object arg1)
  {
    Field fieldOne = (Field)arg0;
    Field fieldTwo = (Field)arg1;
    if (fieldOne.isAnnotationPresent(UrlParameter.class) && fieldTwo.isAnnotationPresent(UrlParameter.class)) {
    	UrlParameter annoOne = (UrlParameter)fieldOne.getAnnotation(UrlParameter.class);
    	UrlParameter annoTwo = (UrlParameter)fieldTwo.getAnnotation(UrlParameter.class);
        return annoOne.pos() - annoTwo.pos();
    } else {
        return 0;
    }
  }
}