package com.sinodynamic.hkgta.integration.pms.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;

import com.sinodynamic.hkgta.dto.pms.CancelReservationDto;
import com.sinodynamic.hkgta.dto.pms.ChargePostingDto;
import com.sinodynamic.hkgta.dto.pms.CheckHotelAvailableDto;
import com.sinodynamic.hkgta.dto.pms.FoodItemDto;
import com.sinodynamic.hkgta.dto.pms.HotelPaymentDto;
import com.sinodynamic.hkgta.dto.pms.RatePlanDto;
import com.sinodynamic.hkgta.dto.pms.RoomRateDto;
import com.sinodynamic.hkgta.dto.pms.RoomResDto;
import com.sinodynamic.hkgta.dto.pms.RoomStayDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.dto.pms.UniqueId;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.integration.util.HttpClientUtil;
import com.sinodynamic.hkgta.util.Dom4jUtils;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.ChargeType;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.response.MessageResult;

import net.sf.json.JSONObject;

@Service
public class PMSApiServiceImpl implements PMSApiService {
	protected final Logger	logger	= Logger.getLogger(PMSApiServiceImpl.class);
	private Logger						pmsLog	= Logger.getLogger(LoggerType.PMS.getName());

	@Autowired
	@Resource(name = "jaxbMarshaller")
	private Jaxb2Marshaller	jaxbMarshaller;

	@Value(value = "#{oasisProperties['pmsapi.url']}")
	private String			apiUrl;

	@Value(value = "#{oasisProperties['posapi.url']}")
	private String			posApiUrl;

	@Value(value = "#{oasisProperties['paramName']}")
	private String			paramName;

	@Value(value = "#{oasisProperties['socketTimeout']}")
	private int				socketTimeout;

	@Value(value = "#{oasisProperties['connectTimeout']}")
	private int				connectTimeout;

	@Value(value = "#{oasisProperties['oasis.host']}")
	private String			oasisHost;

	@Value(value = "#{oasisProperties['oasis.login.url']}")
	private String			loginUrl;

	@Value(value = "#{oasisProperties['oasis.login.username']}")
	private String			userName;

	@Value(value = "#{oasisProperties['oasis.login.password']}")
	private String			password;

	@Value(value = "#{oasisProperties['oasis.room.statu.page']}")
	private String			roomStatusPage;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public MessageResult updateRoomStatus(UpdateRoomStatusDto dto) throws Exception {
		MessageResult r = new MessageResult();
		if ("OFF_MARKET".equals(dto.getStatus()) || "OUT_OF_ORDER".equals(dto.getStatus())) {
			if (StringUtils.isBlank(dto.getReasonCode())) {
				r.setSuccess(false);
				r.setMessage("reasonCode can not empty");
				return r;
			}
		}
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format("<HTNG_HotelRoomStatusUpdateNotifRQ EchoToken=\"75fe59ae-7c59-413e-85f7-7be31f0018c7\" Version=\"1.000\" TimeStamp=\"%s\"",currentTimstamp())); 
		sb.append(" xsi:schemaLocation=\"http://htng.org/2014B HTNG_HotelRoomStatusUpdateNotifRQ.xsd\" xmlns=\"http://htng.org/2014B\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<POS>");
		sb.append("<Source>");
		sb.append("<RequestorID ID=\"CRM\" Type=\"18\"/>");
		sb.append("</Source>");
		sb.append("</POS>");
		sb.append("<UniqueID Type=\"18\" ID=\"" + dto.getUserId() + "\" ID_Content=\"" + dto.getRoleName() + "\"/>");
		sb.append("<PropertyInfo HotelCode=\"HKGTA\"/>");
		sb.append("<Room RoomID=\"" + dto.getRoomNo() + "\">");
		sb.append("<HKStatus>" + dto.getStatus() + "</HKStatus>");
		sb.append("<TPA_Extensions>");
		sb.append("<Reason Code=\"" + dto.getReasonCode() + "\">");
		sb.append("<Text>" + dto.getReasonCode() + "</Text>");
		sb.append("</Reason>");
		sb.append("</TPA_Extensions>");
		sb.append("</Room>");
		sb.append("</HTNG_HotelRoomStatusUpdateNotifRQ>");
		logger.info(sb.toString());
		Map params = new HashMap();
		params.put("payload", sb.toString());
		logger.info(sb);
		String result = HttpClientUtil.sslPost(apiUrl, params, socketTimeout, connectTimeout);
		logger.info(result);
		Document doc = Dom4jUtils.parseText(result);
		Map<String, Object> map = Dom4jUtils.Dom2Map(doc);
		Map m = (Map) map.get("Errors");
		if (m != null) {
			r.setSuccess(false);
			r.setMessage(m.get("Error").toString());
		} else {
			r.setSuccess(true);
		}
		logger.info(Log4jFormatUtil.logInfoMessage(apiUrl, params.toString(), HttpPost.METHOD_NAME, JSONObject.fromObject(r).toString()));
		return r;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public MessageResult cancelReservation(CancelReservationDto dto) throws Exception {
		MessageResult r = new MessageResult();
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format("<OTA_CancelRQ Version=\"3.000\" TimeStamp=\"%s\" ", currentTimstamp()));
		sb.append("xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_CancelRQ.xsd\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"); 
		sb.append("<POS>");
		sb.append("<Source>");
		sb.append("<RequestorID ID=\"CRM\" Type=\"18\"/>");
		sb.append("</Source>");
		sb.append("</POS>");
		sb.append("<UniqueID ID=\"" + dto.getResId() + "\" Type=\"" + dto.getMembershipID() + "\"/>");
		sb.append("<Verification>");
		sb.append("<PersonName>");
		sb.append("<Surname>" + dto.getMemberName() + "</Surname>");
		sb.append("</PersonName>");
		sb.append("<CustLoyalty ProgramID=\"HKGTA\" MembershipID=\"" + dto.getMembershipID() + "\" />");
		sb.append("<ReservationTimeSpan Start=\"" + dto.getReservationTimeSpanStart() + "\" End=\"" + dto.getReservationTimeSpanEnd() + "\"/>");
		sb.append("</Verification>");
		sb.append("</OTA_CancelRQ>");
		logger.info(sb.toString());
		Map params = new HashMap();
		params.put("payload", sb.toString());
		logger.info(sb);
		String result = HttpClientUtil.sslPost(apiUrl, params, socketTimeout, connectTimeout);
		logger.info(result);
		Document doc = Dom4jUtils.parseText(result);
		Element root = doc.getRootElement();
		Element errorsElement = Dom4jUtils.getChild(root, "Errors");
		if (errorsElement != null) {
			r.setSuccess(false);
			for (Iterator git = errorsElement.elementIterator(); git.hasNext();) {
				Element errorElement = (Element) git.next();
				r.addProperty("errorCode" + Dom4jUtils.getAttrVal(errorElement, "Code"), errorElement.getText());
			}
		} else {
			r.setSuccess(true);
			List<UniqueId> uniqueIdList = new ArrayList<UniqueId>();
			for (Iterator git = root.elementIterator(); git.hasNext();) {
				Element confirmElement = (Element) git.next();
				if (StringUtils.isNotBlank(Dom4jUtils.getAttrVal(confirmElement, "Type"))) {
					UniqueId uniqueId = new UniqueId();
					uniqueId.setId(Dom4jUtils.getAttrVal(confirmElement, "ID"));
					uniqueId.setType(Dom4jUtils.getAttrVal(confirmElement, "Type"));
					uniqueIdList.add(uniqueId);
					r.setObj(uniqueIdList);
				}
			}

		}
		logger.info(Log4jFormatUtil.logInfoMessage(apiUrl, params.toString(), HttpPost.METHOD_NAME, JSONObject.fromObject(r).toString()));
		return r;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public MessageResult getkHotelAvailable(CheckHotelAvailableDto dto) throws Exception {
		MessageResult r = new MessageResult();
		if (dto.getAdultCount() == null) {
			r.setSuccess(false);
			r.setMessage("AdultCount can not empty");
			return r;
		}
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format(" <OTA_HotelAvailRQ EchoToken=\"a\" Version=\"5.0\" TimeStamp=\"%s\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelAvailRQ.xsd\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">",currentTimstamp()));
		sb.append("<POS>");
		sb.append("<Source>");
		sb.append("<RequestorID ID=\"CRM\" Type=\"18\"/>");
		sb.append("</Source>");
		sb.append("</POS>");
		sb.append("<AvailRequestSegments>");
		sb.append("<AvailRequestSegment>");
		sb.append("<HotelSearchCriteria>");
		sb.append("<Criterion>");
		sb.append("<StayDateRange Start=\"" + dto.getStayBeginDate() + "\" End=\"" + dto.getStayEndDate() + "\">");
		sb.append("<DateWindowRange CrossDateAllowedIndicator=\"false\"/>");
		sb.append("</StayDateRange>");
		sb.append("<RoomStayCandidates>");
		sb.append("<RoomStayCandidate>");
		sb.append("<GuestCounts>");
		sb.append("<GuestCount AgeQualifyingCode=\"10\" Count=\"" + dto.getAdultCount() + "\" />");
		sb.append("<GuestCount AgeQualifyingCode=\"8\"  Count=\"" + dto.getChildCount() + "\" />");
		sb.append("</GuestCounts>");
		sb.append("</RoomStayCandidate>");
		sb.append("</RoomStayCandidates>");
		sb.append("</Criterion>");
		sb.append("</HotelSearchCriteria>");
		sb.append("</AvailRequestSegment>");
		sb.append("</AvailRequestSegments>");
		sb.append("</OTA_HotelAvailRQ>");
		Map params = new HashMap();
		params.put("payload", sb.toString());
		logger.info(sb);
		String result = HttpClientUtil.sslPost(apiUrl, params, socketTimeout, connectTimeout);
		logger.info("result:" + result);
		RoomStayDto roomStayDto = new RoomStayDto();
		Document doc = Dom4jUtils.parseText(result);
		Element root = doc.getRootElement();

		Element errorsElement = Dom4jUtils.getChild(root, "Errors");
		if (errorsElement != null) {
			r.setSuccess(false);
			for (Iterator git = errorsElement.elementIterator(); git.hasNext();) {
				Element errorElement = (Element) git.next();
				r.addProperty("errorCode" + Dom4jUtils.getAttrVal(errorElement, "Code"), errorElement.getText());
			}
		} else {
			Element roomStayElement = Dom4jUtils.getChild(Dom4jUtils.getChild(root, "RoomStays"), "RoomStay");
			Element roomTypesElement = Dom4jUtils.getChild(roomStayElement, "RoomTypes");
			List<RoomTypeDto> roomTypes = new ArrayList<RoomTypeDto>();
			for (Iterator git = roomTypesElement.elementIterator(); git.hasNext();) {
				Element roomTypeElement = (Element) git.next();
				RoomTypeDto roomType = new RoomTypeDto();
				roomType.setRoomTypeCode(Dom4jUtils.getAttrVal(roomTypeElement, "RoomTypeCode"));
				roomType.setRoomTypeDescription(Dom4jUtils.getText(Dom4jUtils.getChild(roomTypeElement, "RoomTypeDescription"), "Text"));
				roomTypes.add(roomType);
			}
			roomStayDto.setRoomTypes(roomTypes);
			Element ratePlansElement = Dom4jUtils.getChild(roomStayElement, "RatePlans");
			List<RatePlanDto> ratePlans = new ArrayList<RatePlanDto>();
			for (Iterator git = ratePlansElement.elementIterator(); git.hasNext();) {
				Element ratePlanElement = (Element) git.next();
				RatePlanDto ratePlan = new RatePlanDto();
				ratePlan.setRatePlanCode(Dom4jUtils.getAttrVal(ratePlanElement, "RatePlanCode"));
				ratePlan.setRatePlanName(Dom4jUtils.getAttrVal(ratePlanElement, "RatePlanName"));
				ratePlans.add(ratePlan);
			}
			roomStayDto.setRatePlans(ratePlans);
			Element roomRatesElement = Dom4jUtils.getChild(roomStayElement, "RoomRates");
			List<RoomRateDto> roomRates = new ArrayList<RoomRateDto>();
			for (Iterator git = roomRatesElement.elementIterator(); git.hasNext();) {
				Element roomRateElement = (Element) git.next();
				RoomRateDto roomRate = new RoomRateDto();
				roomRate.setRatePlanCode(Dom4jUtils.getAttrVal(roomRateElement, "RatePlanCode"));
				roomRate.setRoomTypeCode(Dom4jUtils.getAttrVal(roomRateElement, "RoomTypeCode"));
				roomRate.setAmountAfterTax(
						Dom4jUtils.getAttrVal(
								Dom4jUtils.getChild(Dom4jUtils.getChild(Dom4jUtils.getChild(roomRateElement, "Rates"), "Rate"), "Total"),
								"AmountAfterTax"));
				roomRate.setRoomRateDescription(Dom4jUtils.getText(Dom4jUtils.getChild(roomRateElement, "RoomRateDescription"), "Text"));
				roomRates.add(roomRate);
			}
			roomStayDto.setRoomRates(roomRates);
			
			r.setSuccess(true);
			r.setObject(roomStayDto);
		}
		logger.info(Log4jFormatUtil.logInfoMessage(apiUrl, params.toString(), HttpPost.METHOD_NAME, JSONObject.fromObject(r).toString()));
		return r;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public MessageResult hotelReservation(RoomResDto dto) throws Exception {
		MessageResult r = new MessageResult();
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format("<OTA_HotelResRQ ResStatus=\"Initiate\" EchoToken=\"a\" Version=\"6.000\" TimeStamp=\"%s\"", currentTimstamp()));
		sb.append(" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelResRQ.xsd\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<POS>");
		sb.append("<Source>");
		sb.append("<RequestorID ID=\"CRM\" Type=\"18\"/>");
		sb.append("</Source>");
		sb.append("</POS>");
		sb.append("<HotelReservations>");
		sb.append("<HotelReservation>");
		sb.append("<RoomStays>");
		sb.append("<RoomStay>");
		sb.append("<RoomRates>");
		sb.append("<RoomRate RoomTypeCode=\"" + dto.getRoomTypeCode() + "\" RoomRateCode=\"" + dto.getRoomRateCode() + "\"/>");
		sb.append("</RoomRates>");
		sb.append("<GuestCounts>");
		sb.append("<GuestCount AgeQualifyingCode=\"10\" Count=\"" + dto.getAdultCount() + "\" />");
		sb.append("<GuestCount AgeQualifyingCode=\"8\"  Count=\"" + dto.getChildCount() + "\" />");
		sb.append("</GuestCounts>");
		sb.append("<TimeSpan Start=\"" + dto.getStartDate() + "\" End=\"" + dto.getEndDate() + "\"/>");
		sb.append("</RoomStay>");
		sb.append("</RoomStays>");
		sb.append("<ResGuests>");
		sb.append("<ResGuest>");
		sb.append("<Profiles>");
		sb.append("<ProfileInfo>");
		sb.append("<Profile ProfileType=\"1\">");
		sb.append("<Customer Gender=\"" + dto.getGender() + "\">");
		sb.append("<PersonName>");
		sb.append("<NamePrefix>" + dto.getNamePrefix() + "</NamePrefix>");
		sb.append("<GivenName>" + dto.getGivenName() + "</GivenName>");
		sb.append("<Surname>" + dto.getSurname() + "</Surname>");
		sb.append("</PersonName>");
		sb.append("<Telephone PhoneNumber=\"" + dto.getPhoneNumber() + "\"/>");
		sb.append("<Email>" + dto.getEmail() + "</Email>");
		sb.append("<Address>");
		sb.append("<AddressLine>" + dto.getAddress1() + "</AddressLine>");
		sb.append("<AddressLine>" + dto.getAddress2() + "</AddressLine>");
		sb.append("<AddressLine>" + dto.getAddress3() + "</AddressLine>");
		sb.append("<AddressLine>" + dto.getAddress4() + "</AddressLine>");
		sb.append("</Address>");
		sb.append("</Customer>");
		sb.append("</Profile>");
		sb.append("</ProfileInfo>");
		sb.append("</Profiles>");
		sb.append("</ResGuest>");
		sb.append("</ResGuests>");
		sb.append("<ResGlobalInfo>");
		sb.append("<Memberships>");
		sb.append("<Membership ProgramCode=\"HKGTA\" AccountID=\"" + dto.getMembershipID() + "\"/>");
		sb.append("</Memberships>");
		sb.append("</ResGlobalInfo>");
		sb.append("</HotelReservation>");
		sb.append("</HotelReservations>");
		sb.append("</OTA_HotelResRQ>");
		Map params = new HashMap();
		params.put("payload", sb.toString());
		logger.info(sb);
		String result = HttpClientUtil.sslPost(apiUrl, params, socketTimeout, connectTimeout);
		logger.info(result);
		Document doc = Dom4jUtils.parseText(result);
		Element root = doc.getRootElement();
		Element errorsElement = Dom4jUtils.getChild(root, "Errors");
		if (errorsElement != null) {
			r.setSuccess(false);
			for (Iterator git = errorsElement.elementIterator(); git.hasNext();) {
				Element errorElement = (Element) git.next();
				r.addProperty("errorCode" + Dom4jUtils.getAttrVal(errorElement, "Code"), errorElement.getText());
			}
		} else {
			r.setSuccess(true);
			Element resGlobalInfoElement = Dom4jUtils
					.getChild(Dom4jUtils.getChild(Dom4jUtils.getChild(root, "HotelReservations"), "HotelReservation"), "ResGlobalInfo");
			r.addProperty("AmountAfterTax", Dom4jUtils.getAttrVal(Dom4jUtils.getChild(resGlobalInfoElement, "Total"), "AmountAfterTax"));
			Element hotelReservationIDElement = Dom4jUtils
					.getChild(Dom4jUtils.getChild(resGlobalInfoElement, "HotelReservationIDs"), "HotelReservationID");
			r.addProperty("ResID_Type", Dom4jUtils.getAttrVal(hotelReservationIDElement, "ResID_Type"));
			r.addProperty("ResID_Value", Dom4jUtils.getAttrVal(hotelReservationIDElement, "ResID_Value"));
		}
		logger.info(Log4jFormatUtil.logInfoMessage(apiUrl, params.toString(), HttpPost.METHOD_NAME, JSONObject.fromObject(r).toString()));
		return r;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public MessageResult ignoreReservation(String resId) throws Exception {
		MessageResult r = new MessageResult();
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format("<OTA_HotelResRQ ResStatus=\"Ignore\" EchoToken=\"a\" Version=\"6.000\" TimeStamp=\"%s\"", currentTimstamp()));
		sb.append(" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelResRQ.xsd\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<POS>");
		sb.append("<Source>");
		sb.append("<RequestorID ID=\"CRM\" Type=\"18\"/>");
		sb.append("</Source>");
		sb.append("</POS>");
		sb.append("<HotelReservations>");
		sb.append("<HotelReservation>");
		sb.append("<ResGlobalInfo>");
		sb.append("<HotelReservationIDs>");
		sb.append("<HotelReservationID ResID_Type=\"40\" ResID_Value=\"" + resId + "\"/>");
		sb.append("</HotelReservationIDs>");
		sb.append("</ResGlobalInfo>");
		sb.append("</HotelReservation>");
		sb.append("</HotelReservations>");
		sb.append("</OTA_HotelResRQ>");
		Map params = new HashMap();
		params.put("payload", sb.toString());
		logger.info(sb);
		String result = HttpClientUtil.sslPost(apiUrl, params, socketTimeout, connectTimeout);
		logger.info(result);
		Document doc = Dom4jUtils.parseText(result);
		Element root = doc.getRootElement();
		Element errorsElement = Dom4jUtils.getChild(root, "Errors");
		if (errorsElement != null) {
			r.setSuccess(false);
			for (Iterator git = errorsElement.elementIterator(); git.hasNext();) {
				Element errorElement = (Element) git.next();
				r.addProperty("errorCode" + Dom4jUtils.getAttrVal(errorElement, "Code"), errorElement.getText());
			}
		} else {
			r.setSuccess(true);
			Element resGlobalInfoElement = Dom4jUtils
					.getChild(Dom4jUtils.getChild(Dom4jUtils.getChild(root, "HotelReservations"), "HotelReservation"), "ResGlobalInfo");
			Element hotelReservationIDElement = Dom4jUtils
					.getChild(Dom4jUtils.getChild(resGlobalInfoElement, "HotelReservationIDs"), "HotelReservationID");
			r.addProperty("ResID_Type", Dom4jUtils.getAttrVal(hotelReservationIDElement, "ResID_Type"));
			r.addProperty("ResID_Value", Dom4jUtils.getAttrVal(hotelReservationIDElement, "ResID_Value"));
		}
		logger.info(Log4jFormatUtil.logInfoMessage(apiUrl, params.toString(), HttpPost.METHOD_NAME, JSONObject.fromObject(r).toString()));
		return r;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public MessageResult payment(HotelPaymentDto dto) throws Exception {
		MessageResult r = new MessageResult();
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format("<OTA_HotelResRQ ResStatus=\"Commit\" EchoToken=\"a\" Version=\"6.000\" TimeStamp=\"%s\"", currentTimstamp()));
		sb.append(" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelResRQ.xsd\" xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<POS>");
		sb.append("<Source>");
		sb.append("<RequestorID ID=\"CRM\" Type=\"18\"/>");
		sb.append("</Source>");
		sb.append("</POS>");
		sb.append("<HotelReservations>");
		sb.append("<HotelReservation>");
		sb.append("<ResGlobalInfo>");
		sb.append("<DepositPayments>");
		sb.append("<GuaranteePayment GuaranteeType=\"PrePay\">");
		sb.append("<AcceptedPayments>");
		sb.append("<AcceptedPayment>");
		if (PaymentMethod.CASHVALUE.getCardCode().equalsIgnoreCase(dto.getCardCode())) {
			sb.append(
					"<LoyaltyRedemption MemberNumber=\"" + dto.getMemberNumber() + "\" ProgramName=\"Cash Value\" CertificateNumber=\""
							+ dto.getCertificateNumber() + "\"/>");
		}else if(PaymentMethod.CASH.getCardCode().equalsIgnoreCase(dto.getCardCode())){
			sb.append("<Cash/>");
		}else {
			sb.append(
					"<PaymentCard CardCode=\"" + ObjectUtils.toString(dto.getCardCode()) + "\" MaskedCardNumber=\"" + dto.getMaskedCardNumber()
							+ "\" ExpireDate=\"" + dto.getExpireDate() + "\">");
			sb.append("<CardHolderName>" + dto.getCardHolderName() + "</CardHolderName>");
			sb.append("</PaymentCard>");
		}
		sb.append("</AcceptedPayment>");
		sb.append("</AcceptedPayments>");
		sb.append("<AmountPercent Amount=\"" + dto.getAmount() + "\" CurrencyCode=\"HKD\"/>");
		sb.append("</GuaranteePayment>");
		sb.append("</DepositPayments>");
		sb.append("<Total AmountAfterTax=\"" + dto.getAmountAfterTax() + "\" CurrencyCode=\"HKD\"/>");
		sb.append("<HotelReservationIDs>");
		sb.append("<HotelReservationID ResID_Type=\"40\" ResID_Value=\"" + dto.getResID() + "\"/>");
		sb.append("</HotelReservationIDs>");
		sb.append("</ResGlobalInfo>");
		sb.append("</HotelReservation>");
		sb.append("</HotelReservations>");
		sb.append("</OTA_HotelResRQ>");
		Map params = new HashMap();
		params.put("payload", sb.toString());
		pmsLog.info(" commit oasis request:"+sb);
		
		String result = HttpClientUtil.sslPost(apiUrl, params, socketTimeout, connectTimeout);
		pmsLog.info("response: "+result);
		
		Document doc = Dom4jUtils.parseText(result);
		Element root = doc.getRootElement();
		Element errorsElement = Dom4jUtils.getChild(root, "Errors");
		if (errorsElement != null) {
			r.setSuccess(false);
			for (Iterator git = errorsElement.elementIterator(); git.hasNext();) {
				Element errorElement = (Element) git.next();
				r.addProperty("errorCode" + Dom4jUtils.getAttrVal(errorElement, "Code"), errorElement.getText());
			}
		} else {
			r.setSuccess(true);
			Element resGlobalInfoElement = Dom4jUtils
					.getChild(Dom4jUtils.getChild(Dom4jUtils.getChild(root, "HotelReservations"), "HotelReservation"), "ResGlobalInfo");
			Element hotelReservationIDElement = Dom4jUtils
					.getChild(Dom4jUtils.getChild(resGlobalInfoElement, "HotelReservationIDs"), "HotelReservationID");
			r.addProperty("ResID_Type", Dom4jUtils.getAttrVal(hotelReservationIDElement, "ResID_Type"));
			r.addProperty("ResID_Value", Dom4jUtils.getAttrVal(hotelReservationIDElement, "ResID_Value"));
		}
		pmsLog.info(Log4jFormatUtil.logInfoMessage(apiUrl, params.toString(), HttpPost.METHOD_NAME, JSONObject.fromObject(r).toString()));
		return r;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public MessageResult chargePostingReserved(ChargePostingDto dto) throws Exception {
		MessageResult r = new MessageResult();
		StringBuffer sb = new StringBuffer();
		if (ChargeType.REFUND.getCode().equals(dto.getType())) {
			if (dto.getAmount().compareTo(BigDecimal.ZERO) == 1) {
				dto.setAmount(dto.getAmount().negate());
			}
		}
		if (ChargeType.CHARGE.getCode().equals(dto.getType())) {
			if (dto.getAmount().compareTo(BigDecimal.ZERO) == -1) {
				dto.setAmount(dto.getAmount().negate());
			}
		}
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format("<HTNG_ChargePostingRQ TransactionStatusCode=\"Subsequent\" TransactionIdentifier=\"String\" EchoToken=\"a\" Version=\"0.0\" TimeStamp=\"%s\"",currentTimstamp())); 
		sb.append(" xsi:schemaLocation=\"http://htng.org/2014B HTNG_ChargePostingRQ.xsd\" xmlns=\"http://htng.org/2014B\" xmlns:ota=\"http://www.opentravel.org/OTA/2003/05\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<POS>");
		sb.append("<Source>");
		sb.append("<RequestorID ID=\"CRM\" Type=\"18\"/>");
		sb.append("</Source>");
		sb.append("</POS>");
		sb.append("<PropertyInfo HotelCode=\"HKGTA\"/>");
		sb.append("<Posting>");
		sb.append("<RevenueCenter>");
		sb.append("<Terminal ID=\"\"/>");
		sb.append("</RevenueCenter>");
		sb.append("<Transaction TicketID=\"" + dto.getTicketID() + "\">");
		sb.append("<RevenueDetails>");
		sb.append("<RevenueDetail Description=\""+dto.getDescription()+"\" Amount=\""+dto.getAmount()+"\" PMSRevenueCode=\""+dto.getPmsRevenueCode()+"\">");
		sb.append("<FolioIDs>");
		sb.append("<FolioID>0</FolioID>");
		sb.append("</FolioIDs>");
		sb.append("</RevenueDetail>");
		sb.append("</RevenueDetails>");
		sb.append("<TaxItems Amount=\"0.00\"/>");
		sb.append("<Tenders>");
		sb.append("<RevenueDetail Amount=\"" + dto.getAmount() + "\">");
		sb.append("<FolioIDs>");
		sb.append("<FolioID>0</FolioID>");
		sb.append("</FolioIDs>");
		sb.append("<Account Type=\"40\" ID=\"" + dto.getResvID() + "\"/>");
		// sb.append("<Account Type=\"ROOM\" ID=\"131\"/>");

		sb.append("</RevenueDetail>");
		sb.append("</Tenders>");
		sb.append("</Transaction>");
		sb.append("</Posting>");
		sb.append("</HTNG_ChargePostingRQ>");
		Map params = new HashMap();
		params.put("payload", sb.toString());
		logger.info(sb);
		String result = HttpClientUtil.sslPost(apiUrl, params, socketTimeout, connectTimeout);
		logger.info(result);
		Document doc = Dom4jUtils.parseText(result);
		Element root = doc.getRootElement();
		Element errorsElement = Dom4jUtils.getChild(root, "Errors");
		if (errorsElement != null) {
			r.setSuccess(false);
			for (Iterator git = errorsElement.elementIterator(); git.hasNext();) {
				Element errorElement = (Element) git.next();
				r.addProperty("errorCode" + Dom4jUtils.getAttrVal(errorElement, "Code"), errorElement.getText());
			}
		} else {
			r.setSuccess(true);
			r.addProperty("PostingGUID", Dom4jUtils.getText(root, "PostingGUID"));
		}
		logger.info(Log4jFormatUtil.logInfoMessage(apiUrl, params.toString(), HttpPost.METHOD_NAME, JSONObject.fromObject(r).toString()));
		return r;
	}

	public String queryHouseKeepingStatusByRoomId(String roomId) throws ClientProtocolException, IOException {
		String status = "";
		Map<String, String> statusMap = queryHouseKeepingStatusList();
		status = statusMap.get(roomId);
		return status;
	}

	private String currentTimstamp() {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		return sdf.format(new Date());

	}

	@Override
	public List<FoodItemDto> queryOrdersByOutletId(String outedId) throws Exception {
		List<FoodItemDto> itemsList = new ArrayList<FoodItemDto>();
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format("<HTNG_StatisticsRQ   EchoToken=\"a\" Version=\"5.0\" TimeStamp=\"%s\"", currentTimstamp()));
		sb.append(" xsi:schemaLocation=\"http://htng.org/2014B HTNG_StatisticsRQ.xsd\" xmlns=\"http://htng.org/2014B\"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<RequestorID ID=\"ORDERING\" Type=\"18\" ID_Context=\"OASIS_POS\"/>");
		sb.append("<Queries>");
		sb.append("<Query StoredQueryName=\"Outlet_Price_Schedule\" QueryTrackingID=\"cb2bca98-4e28-4a19-978b-a4e1a9e50011\">");
		sb.append("<RequestParameters>");
		sb.append("<KeyValueItem Key=\"Outlet\" Value=\"" + outedId + "\"/>");
		sb.append("</RequestParameters>");
		sb.append("<ResponseParameters/>");
		sb.append("</Query>");
		sb.append("</Queries>");
		sb.append("</HTNG_StatisticsRQ>");
		Map params = new HashMap();
		params.put("payload", sb.toString());
		logger.info(sb);
		String result = HttpClientUtil.sslPost(posApiUrl, params, socketTimeout, connectTimeout);
		logger.info(result);
		Document doc = Dom4jUtils.parseText(result);
		Element root = doc.getRootElement();
		Element errorsElement = Dom4jUtils.getChild(root, "Errors");
		if (errorsElement != null) {
			return null;
		} else {
			// r.setSuccess(false);
			Element queriesElement = Dom4jUtils.getChild(root, "Queries");
			Element queryElement = Dom4jUtils.getChild(queriesElement, "Query");
			Element queryResultElement = Dom4jUtils.getChild(queryElement, "QueryResult");
			String itemsStr = queryResultElement.getTextTrim();
			String itemsStrReg = "\\s*\"(.*?)\"\\s*";
			Pattern pattern = Pattern.compile(itemsStrReg);
			Matcher matcher = pattern.matcher(itemsStr);
			if (matcher.matches()) {
				itemsStr = matcher.group(1);
			}
			Document listItemDoc = Dom4jUtils.parseText(itemsStr);
			Element listItemElement = listItemDoc.getRootElement();
			Iterator it = listItemElement.elementIterator("Item");
			while (it.hasNext()) {
				Element itemElement = (Element) it.next();
				String id = itemElement.attribute("ID").getValue();
				String desc = itemElement.attribute("Description").getValue();
				String price = itemElement.attribute("Price").getValue();
				FoodItemDto item = new FoodItemDto(id, desc, price);
				itemsList.add(item);
			}
		}
		logger.info(Log4jFormatUtil.logInfoMessage(posApiUrl, params.toString(), HttpPost.METHOD_NAME, itemsList.toString()));
		return itemsList;
	}

	@Override
	public MessageResult modifyReservation(RoomResDto dto) throws Exception {
		MessageResult r = new MessageResult();
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(String.format("<OTA_HotelResModifyRQ  ResStatus=\"Modify\" EchoToken=\"a\" Version=\"7.000\" TimeStamp=\"%s\"", currentTimstamp()));
		sb.append(" xsi:schemaLocation=\"http://htng.org/2014B OTA_HotelResModifyRQ.xsd\" xmlns=\"http://www.opentravel.org/OTA/2003/05\"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<POS>");
		sb.append("<Source>");
		sb.append("<RequestorID ID=\"CRM\" Type=\"18\"/>");
		sb.append("</Source>");
		sb.append("</POS>");
		sb.append("<HotelResModifies>");
		sb.append("<UniqueID Type=\"40\" ID=\"" + dto.getReservationID() + "\"/>");
		sb.append("<HotelResModify>");
		sb.append("<Verification>");
		sb.append("<CustLoyalty ProgramID=\"HKGTA\" MembershipID=\"" + dto.getMembershipID() + "\"/>");
		sb.append("</Verification>");
		sb.append("<ResGuests>");
		sb.append("<ResGuest>");
		sb.append("<Profiles>");
		sb.append("<ProfileInfo>");
		sb.append("<Profile ProfileType=\"1\">");
		sb.append("<Customer>");
		sb.append("<PersonName>");
		sb.append("<NamePrefix>" + dto.getNamePrefix() + "</NamePrefix>");
		sb.append("<GivenName>" + dto.getGivenName() + "</GivenName>");
		sb.append("<Surname>" + dto.getSurname() + "</Surname>");
		sb.append("</PersonName>");
		sb.append("<Telephone PhoneNumber=\"" + dto.getPhoneNumber() + "\"/>");
		sb.append("<Email>" + dto.getEmail() + "</Email>");
		sb.append("<Address>");
		sb.append("<AddressLine>" + dto.getAddress1() + "</AddressLine>");
		sb.append("<AddressLine>" + dto.getAddress2() + "</AddressLine>");
		sb.append("<AddressLine>" + dto.getAddress3() + "</AddressLine>");
		sb.append("<AddressLine>" + dto.getAddress4() + "</AddressLine>");
		sb.append("</Address>");
		sb.append("</Customer>");
		sb.append("</Profile>");
		sb.append("</ProfileInfo>");
		sb.append("</Profiles>");
		sb.append("</ResGuest>");
		sb.append("</ResGuests>");
		sb.append("</HotelResModify>");
		sb.append("</HotelResModifies>");
		sb.append("</OTA_HotelResModifyRQ>");
		Map params = new HashMap();
		params.put("payload", sb.toString());
		logger.info(sb);
		String result = HttpClientUtil.sslPost(apiUrl, params, socketTimeout, connectTimeout);
		System.out.println(result);
		logger.info("result:" + result);
		Document doc = Dom4jUtils.parseText(result);
		Element root = doc.getRootElement();
		Element errorsElement = Dom4jUtils.getChild(root, "Errors");

		if (errorsElement != null) {
			r.setSuccess(false);
			for (Iterator git = errorsElement.elementIterator(); git.hasNext();) {
				Element errorElement = (Element) git.next();
				r.addProperty("errorCode" + Dom4jUtils.getAttrVal(errorElement, "Code"), errorElement.getText());
			}
		} else {
			r.setSuccess(true);
			Element resGlobalInfoElement = Dom4jUtils
					.getChild(Dom4jUtils.getChild(Dom4jUtils.getChild(root, "HotelResModifies"), "HotelResModify"), "ResGlobalInfo");
			Element hotelReservationIDElement = Dom4jUtils
					.getChild(Dom4jUtils.getChild(resGlobalInfoElement, "HotelReservationIDs"), "HotelReservationID");
			r.addProperty("ResID_Type", Dom4jUtils.getAttrVal(hotelReservationIDElement, "ResID_Type"));
			r.addProperty("ResID_Value", Dom4jUtils.getAttrVal(hotelReservationIDElement, "ResID_Value"));
		}
		logger.info(Log4jFormatUtil.logInfoMessage(apiUrl, params.toString(), HttpPost.METHOD_NAME, JSONObject.fromObject(r).toString()));
		
		return r;
	}

	@Override
	public Map<String, String> queryHouseKeepingStatus() throws ClientProtocolException, IOException {
		Map<String, String> statusMap = queryHouseKeepingStatusList();
		return statusMap;
	}

	private Map<String, String> queryHouseKeepingStatusList() throws ClientProtocolException, IOException {
		Map<String, String> statusMap = new HashMap<String, String>();
		CloseableHttpClient httpClient = null;
		try {

			httpClient = HttpClientUtil.getHttpClientWithLogin(oasisHost, loginUrl, userName, password, socketTimeout, connectTimeout);

			HttpPost post = new HttpPost(roomStatusPage);

			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).build();// 设置请求和传输超时时间
			post.setConfig(requestConfig);

			HttpResponse httpResponse = httpClient.execute(post);

			HttpEntity entity = httpResponse.getEntity();

			String entityMsg = EntityUtils.toString(entity, "GBK");
			org.jsoup.nodes.Document doc = Jsoup.parse(entityMsg);

			org.jsoup.nodes.Element body = doc.body();

			org.jsoup.select.Elements span = body.getElementsByClass("_ROOM_");
			Iterator<org.jsoup.nodes.Element> iter = span.iterator();
			while (iter.hasNext()) {
				org.jsoup.nodes.Element div = iter.next();
				org.jsoup.nodes.Element roomNo = div.child(0);
				org.jsoup.nodes.Element roomStatusElement = div.child(2);
				logger.debug("room No:" + roomNo.html() + ",room status:" + roomStatusElement.html());
				statusMap.put(roomNo.html(), roomStatusElement.html());

			}
			logger.info(Log4jFormatUtil.logInfoMessage(roomStatusPage, null, HttpPost.METHOD_NAME,statusMap.toString()));

		} catch (Exception e) {
			logger.error(Log4jFormatUtil.logErrorMessage(roomStatusPage, null, HttpPost.METHOD_NAME, null, e.getMessage()));
			throw e;
		} finally {
			if(null!=httpClient)
			{
				httpClient.close();
			}
			
		}
		return statusMap;
	}

}
