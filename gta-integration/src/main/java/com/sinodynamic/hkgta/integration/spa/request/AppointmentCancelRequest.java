package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class AppointmentCancelRequest extends BaseRequest {
	
	private static final long serialVersionUID = 7990846679486520150L;
	@UrlParameter(name = "appointmentId")
	@FieldRules(nullable = false)
	private String appointmentId;
	@UrlParameter(name = "comments")
	private String comments;

	public String getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
