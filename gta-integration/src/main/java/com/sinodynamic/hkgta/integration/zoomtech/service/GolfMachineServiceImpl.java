package com.sinodynamic.hkgta.integration.zoomtech.service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sinodynamic.hkgta.dto.golfmachine.BatInfo;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
//import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinodynamic.hkgta.util.constant.AccessBatCmdType;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.http.HttpClientUtil;

@Service
public class GolfMachineServiceImpl implements GolfMachineService {
	private Logger logger = Logger.getLogger(GolfMachineServiceImpl.class);
	
	private Logger zoomtechLog = Logger.getLogger(LoggerType.ZOOMTECH.getName()); 
	
	String remoteHost = null; // = appProps.getProperty("golfmachine.host");
	final String HTTP_SUCCESS = "success";
	final String GOLF_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private Integer MAX_BALLCOUNT = null;
	private Integer MAX_TIMECOUNT = null;
	final String ENDPOINT_BAYINFO = "/bayInfo";
	final String ENDPOINT_BATCMD = "/bayCmd";

	@Resource(name = "appProperties")
	Properties appProps;

	public void setRemoteHost(String host) {
		remoteHost = host;
	}

	/**
	 * to get remote host address from property file if remoteHost is not
	 * initialized
	 * 
	 * @return
	 */
	public String getPropertyRemoteHost() {
		if (remoteHost == null) // default from app.properties file
			return appProps.getProperty("golfmachine.host");

		return remoteHost;
	}

	public Integer getMAX_BALLCOUNT() {
		if (MAX_BALLCOUNT == null)
			return Integer.parseInt(appProps.getProperty("golfmachine.max_ballcount"));
		return MAX_BALLCOUNT;
	}

	public Integer getMAX_TIMECOUNT() {
		if (MAX_TIMECOUNT == null)
			return Integer.parseInt(appProps.getProperty("golfmachine.max_timecount"));
		return MAX_TIMECOUNT;
	}

	public void setMAX_BALLCOUNT(Integer mAX_BALLCOUNT) {
		MAX_BALLCOUNT = mAX_BALLCOUNT;
	}

	public void setMAX_TIMECOUNT(Integer mAX_TIMECOUNT) {
		MAX_TIMECOUNT = mAX_TIMECOUNT;
	}

	public boolean testHost() throws Exception {
		remoteHost = getPropertyRemoteHost();

		String response = HttpClientUtil.get(remoteHost + ENDPOINT_BAYINFO, "batNo=1");
		System.out.println("result=" + response);
		if (response == null || response.length() < 20)
			return false;
		// BatInfo batInfo=(new ObjectMapper()).readValue(response,
		// BatInfo.class) ;
		return true;
	}

	public void sendBatchCmd(long batNo, String startTime, long timeCount, long ballCount, long CmdType)
			throws Exception {

		String result = "";
		String params = "cmd=" + batNo + ",'" + startTime + "'," + timeCount + "," + ballCount + ",0," // use
																										// Type
																										// is
																										// always
				+ CmdType;
		remoteHost = getPropertyRemoteHost();

		// logger.info("send to golfmachine batCmd:" + params);
		String reqeust = "send to golfmachine batCmd:" + params;
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(remoteHost + ENDPOINT_BATCMD, reqeust, HttpPost.METHOD_NAME,
				null));
		
		result = HttpClientUtil.postForm(remoteHost + ENDPOINT_BATCMD, params);

		if (!HTTP_SUCCESS.equals(result)) {
			zoomtechLog.error(Log4jFormatUtil.logErrorMessage(null, null, null,
					result, new Exception(result).getMessage()));
			throw new Exception(result);
		} else {
			zoomtechLog.info(Log4jFormatUtil.logInfoMessage(null, null,null,
					result));
		}

	}

	public Boolean changeTeeupMachineStatsbyBatNo(Long batNo, String ballFeedingStatus) throws Exception {
		try {

			String currentTimeStr = (new SimpleDateFormat(GOLF_DATETIME_FORMAT)).format(new Date());
			if (ballFeedingStatus.equals("OFF"))
				sendBatchCmd(batNo, currentTimeStr, 0, 0, AccessBatCmdType.Finish.getCmd());
			else
				sendBatchCmd(batNo, currentTimeStr, getMAX_TIMECOUNT(), getMAX_BALLCOUNT(),
						AccessBatCmdType.Checkin.getCmd());
			return true;

		} catch (Exception ex) {
			// logger.error(ex.getMessage());
			throw ex;
			// return false;
		}
	}

	public void setTeeupMachineStatsbyCheckinTime(Long batNo, Date startTime, Date endTime) throws Exception {
		Date currentTime = new Date();
		String currentTimeStr = (new SimpleDateFormat(GOLF_DATETIME_FORMAT)).format(new Date());
		Long timeDiff;

		if (currentTime.after(startTime)) { // if start time is earlier than
											// now, current time will be the
											// start time
			timeDiff = endTime.getTime() - currentTime.getTime();
			Long min = timeDiff / (1000 * 60);

			sendBatchCmd(batNo, currentTimeStr, min, getMAX_BALLCOUNT(), AccessBatCmdType.Checkin.getCmd());

		} else { // on schedule start time
			timeDiff = endTime.getTime() - startTime.getTime();
			Long min = timeDiff / (1000 * 60);
			String startTimeStr = (new SimpleDateFormat(GOLF_DATETIME_FORMAT)).format(startTime);
			sendBatchCmd(batNo, startTimeStr, min, getMAX_BALLCOUNT(), AccessBatCmdType.Checkin.getCmd());
		}

	}

	public void setTeeupMachineStatsbyCancelTime(Long batNo, Date startTime, Date endTime) throws Exception {
		Date currentTime = new Date();
		String currentTimeStr = (new SimpleDateFormat(GOLF_DATETIME_FORMAT)).format(new Date());
		if (currentTime.after(startTime)) { // if start time is earlier than
											// now, current time will be the
											// start time
			sendBatchCmd(batNo, currentTimeStr, 0, 0, AccessBatCmdType.Finish.getCmd());
		} else { // on schedule start time
			String startTimeStr = (new SimpleDateFormat(GOLF_DATETIME_FORMAT)).format(startTime);
			sendBatchCmd(batNo, startTimeStr, 0, 0, AccessBatCmdType.Finish.getCmd());
		}
	}

	public List<BatInfo> getBatInfo(Long batNo) throws Exception {
		remoteHost = getPropertyRemoteHost();
		String param = "";
		if (batNo != null && batNo > 0)
			param = "batNo=" + batNo;

		String response = HttpClientUtil.get(remoteHost + ENDPOINT_BAYINFO, param);
		// System.out.println("result=" + response);
		if (response == null || response.length() < 20) {
			zoomtechLog.error(Log4jFormatUtil.logErrorMessage(remoteHost + ENDPOINT_BATCMD, param, HttpGet.METHOD_NAME,
					response, new Exception(response).getMessage()));
			return null;
		}
		List<BatInfo> batInfos = Arrays.asList((new ObjectMapper()).readValue(response, BatInfo[].class));
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(remoteHost + ENDPOINT_BATCMD, param, HttpPost.METHOD_NAME, response));
		return batInfos;

	}

	public Map<Long, BatInfo> getAllBatInfosMap() throws Exception {
		remoteHost = getPropertyRemoteHost();
		String response = HttpClientUtil.get(remoteHost + ENDPOINT_BAYINFO, "");
		// System.out.println("result=" + response);
		if (response == null || response.length() < 20) {
			zoomtechLog.error(Log4jFormatUtil.logErrorMessage(remoteHost + ENDPOINT_BATCMD, null, HttpGet.METHOD_NAME,
					response, new Exception(response).getMessage()));
			return null;
		}
		BatInfo[] batInfos = (new ObjectMapper()).readValue(response, BatInfo[].class);
		Map<Long, BatInfo> batInfMap = new HashMap<Long, BatInfo>();
		for (BatInfo bi : batInfos)
			batInfMap.put(bi.getBatNo(), bi);
		
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(remoteHost + ENDPOINT_BATCMD, null, HttpGet.METHOD_NAME, response));

		return batInfMap;
	}

}
