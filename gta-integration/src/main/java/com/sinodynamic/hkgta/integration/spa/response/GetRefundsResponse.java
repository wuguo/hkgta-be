package com.sinodynamic.hkgta.integration.spa.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "success", "message", "Deleted Payments" })
public class GetRefundsResponse extends BaseResponse{

	private static final long serialVersionUID = 4780437146006522951L;
	@JsonProperty("success")
	private Boolean success;
//	@JsonProperty("allSize")
//	private Integer allSize;
//	@JsonProperty("allPage")
//	private Integer allPage;
//	@JsonProperty("isLast")
//	private Boolean isLast;
	@JsonProperty("message")
	private String message;
	@JsonProperty("total")
	private Integer total;
	@JsonProperty("Deleted Payments")
	private List<Refund> refunds = new ArrayList<Refund>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("success")
	public Boolean getSuccess() {
		return success;
	}

	@JsonProperty("success")
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
//	@JsonProperty("allSize")
//	public Integer getAllSize() {
//		return allSize;
//	}
//	
//	@JsonProperty("allSize")
//	public void setAllSize(Integer allSize) {
//		this.allSize = allSize;
//	}
//
//	@JsonProperty("allPage")
//	public Integer getAllPage() {
//		return allPage;
//	}
//
//	@JsonProperty("allPage")
//	public void setAllPage(Integer allPage) {
//		this.allPage = allPage;
//	}
//
//	@JsonProperty("isLast")
//	public Boolean getIsLast() {
//		return isLast;
//	}
//
//	@JsonProperty("isLast")
//	public void setIsLast(Boolean isLast) {
//		this.isLast = isLast;
//	}

	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	@JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@JsonProperty("Deleted Payments")
	public List<Refund> getRefunds() {
		return refunds;
	}

	@JsonProperty("Deleted Payments")
	public void setRefunds(List<Refund> refunds) {
		this.refunds = refunds;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}