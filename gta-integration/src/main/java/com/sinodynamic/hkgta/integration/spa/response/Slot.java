package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ServiceName", "TherapistName", "timeslot", "serviceid",
		"therapistid", "starttimestring", "endtimestring" })
public class Slot extends BaseResponse{
	private static final long serialVersionUID = 2268480725573654589L;
	@JsonProperty("ServiceName")
	private String serviceName;
	@JsonProperty("TherapistName")
	private String therapistName;
	@JsonProperty("timeslot")
	private String timeslot;
	@JsonProperty("serviceid")
	private String serviceid;
	@JsonProperty("therapistid")
	private String therapistid;
	@JsonProperty("starttimestring")
	private String starttimestring;
	@JsonProperty("endtimestring")
	private String endtimestring;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getTherapistName() {
		return therapistName;
	}

	public void setTherapistName(String therapistName) {
		this.therapistName = therapistName;
	}

	public String getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getTherapistid() {
		return therapistid;
	}

	public void setTherapistid(String therapistid) {
		this.therapistid = therapistid;
	}

	public String getStarttimestring() {
		return starttimestring;
	}

	public void setStarttimestring(String starttimestring) {
		this.starttimestring = starttimestring;
	}

	public String getEndtimestring() {
		return endtimestring;
	}

	/**
	 * 
	 * @param endtimestring
	 *            The endtimestring
	 */
	@JsonProperty("endtimestring")
	public void setEndtimestring(String endtimestring) {
		this.endtimestring = endtimestring;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}