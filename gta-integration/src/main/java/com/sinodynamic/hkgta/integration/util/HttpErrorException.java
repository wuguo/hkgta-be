package com.sinodynamic.hkgta.integration.util;
/**
 * 
 * @author Nick_Xiong
 *
 */

public class HttpErrorException extends Exception {
	private String url;

	private String errorMsg;

	public HttpErrorException() {
		super();
	}

	public HttpErrorException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public HttpErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public HttpErrorException(String message) {
		super(message);

	}

	public HttpErrorException(Throwable cause) {
		super(cause);
	}

	public HttpErrorException(String url, String errorMsg) {
		super();
		this.url = url;
		this.errorMsg = errorMsg;
	}

	/**
	 * 
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
