package com.sinodynamic.hkgta.integration.spa.response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * "AccountCategory": "Discounts", "AccountName": "GiftCardDiscount",
 * "Description": "Gift Card discounts", "Name": "GiftCardDiscount",
 * "AccountCode": "GiftCardDiscount", "Credit": "0", "Debit": "0"
 * 
 * @author Dell
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "AccountCategory", "AccountName", "Description", "Name", "AccountCode", "Credit", "Debit" })
public class SpaAccountFlex extends BaseResponse {
	private static final long serialVersionUID = -5191977214747793280L;
	@JsonProperty("AccountCategory")
	private String accountCategory;

	@JsonProperty("AccountName")
	private String accountName;

	@JsonProperty("Description")
	private String description;

	@JsonProperty("Name")
	private String name;

	@JsonProperty("AccountCode")
	private String accountCode;

	@JsonProperty("Credit")
	private BigDecimal credit;

	@JsonProperty("Debit")
	private BigDecimal debit;

	public String getAccountCategory() {
		return accountCategory;
	}

	public void setAccountCategory(String accountCategory) {
		this.accountCategory = accountCategory;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public BigDecimal getCredit() {
		return credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	public BigDecimal getDebit() {
		return debit;
	}

	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}
	
}