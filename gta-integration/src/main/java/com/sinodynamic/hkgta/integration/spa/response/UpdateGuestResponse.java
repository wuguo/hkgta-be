package com.sinodynamic.hkgta.integration.spa.response;


public class UpdateGuestResponse extends BaseResponse {
	private static final long serialVersionUID = 6445996037521381342L;

	private boolean success;

	private String message;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

}
