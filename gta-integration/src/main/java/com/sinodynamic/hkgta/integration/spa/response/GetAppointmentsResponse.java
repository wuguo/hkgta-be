package com.sinodynamic.hkgta.integration.spa.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"success",
"appointmentData"
})
public class GetAppointmentsResponse extends BaseResponse{

	private static final long serialVersionUID = -8062642813686447213L;
	@JsonProperty("success")
	private Boolean success;
	@JsonProperty("message")
	private String message;
	@JsonProperty("appointmentData")
	private List<AppointmentDatum> appointmentData = new ArrayList<AppointmentDatum>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	/**
	* 
	* @return
	* The success
	*/
	@JsonProperty("success")
	public Boolean getSuccess() {
	return success;
	}
	
	/**
	* 
	* @param success
	* The success
	*/
	@JsonProperty("success")
	public void setSuccess(Boolean success) {
	this.success = success;
	}
	
	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	* 
	* @return
	* The appointmentData
	*/
	@JsonProperty("appointmentData")
	public List<AppointmentDatum> getAppointmentData() {
	return appointmentData;
	}
	
	/**
	* 
	* @param appointmentData
	* The appointmentData
	*/
	@JsonProperty("appointmentData")
	public void setAppointmentData(List<AppointmentDatum> appointmentData) {
	this.appointmentData = appointmentData;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
