package com.sinodynamic.hkgta.integration.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.sinodynamic.hkgta.dto.pms.xml.input.membership.POS;

@Deprecated // seem nothing reference this and many hard code http url. To be deleted
public class HttpClientUtilWithLogin { 

//	private String loginPage = "https://hkgtauat.sinodynamic.com:8088/hotel/oasis.php";
//	private String loginUrl = "https://hkgtauat.sinodynamic.com:8088/hotel/code/login.php";
//	private String host = "hkgtauat.sinodynamic.com:8088";
//	private String referUrl ="https://hkgtauat.sinodynamic.com:8088/hotel/template/bohmain.php";
//	//private String responseHeaderCookie
//	private String sessionCookieName = "PHPSESSID";
//	private CloseableHttpClient client;
//	private String cookie;
//
//	private void init() {
//		/*HttpClientBuilder builder = HttpClientBuilder.create();
//		builder.setRedirectStrategy(new LaxRedirectStrategy());
//		client = builder.build();*/
//		
//		client = createSSLInsecureClient();
//		
//		
//		
//	}
//
//	private String getCookie() throws ClientProtocolException, IOException {
//		HttpGet indexGet = new HttpGet(loginPage);
//		HttpResponse indexRes = client.execute(indexGet);
//		this.cookie = indexRes.getHeaders("Set-Cookie")[0].getValue();
//		indexGet.releaseConnection();
//		return cookie;
//	}
//
//	public void login(String userName, String password) throws IOException {
//		init();
//		getCookie();
//
//		System.out.println("[Info] Login...");
//		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//		nvps.add(new BasicNameValuePair("user", userName));
//		nvps.add(new BasicNameValuePair("passwd", password));
//		//CloseableHttpClient client = createSSLInsecureClient();
//		HttpPost httpPost = new HttpPost(loginUrl);
//		RequestConfig requestConfig = RequestConfig.custom()
//				.setSocketTimeout(5000)
//				.setConnectTimeout(5000).build();// 设置请求和传输超时时间
//		httpPost.setConfig(requestConfig);
//		
//		httpPost.setEntity(new UrlEncodedFormEntity(nvps,"UTF-8"));
//		httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safari/537.36");
//		httpPost.setHeader("Host", host);
//		httpPost.setHeader("Cookie", cookie);
//		//httpPost.setHeader("Referer",referUrl);
//		HttpResponse response = client.execute(httpPost);
//		if (response.getStatusLine().getStatusCode() == 200) {
//			System.out.println("[Info] Login success.");
//		}
//		HttpEntity entity2 = response.getEntity();
//
//		InputStream in = entity2.getContent();
//		StringBuilder content = new StringBuilder();
//		int length = -1;
//		byte[] data = new byte[10240];
//		while ((length = in.read(data)) != -1) {
//			content.append(new String(data, 0, length));
//		}
//		System.out.println(content);
//		httpPost.releaseConnection();
//		
//		
//		
//		
//		HttpPost post2 = new HttpPost("https://hkgtauat.sinodynamic.com:8088/hotel/code/availability/pgsql/rooms_block.php?request_type=rooms_block&Complex=&blockStr=&roomtype=");
//		
//		post2.setConfig(requestConfig);
//		
//
//		
//		HttpResponse httpResponse2 = client.execute(post2);
//	
//		HttpEntity entity = httpResponse2.getEntity();
//		
//		System.out.println("=============================================");
//		
//		//System.out.println(EntityUtils.toString(entity));
//		
//		String entityMsg = EntityUtils.toString(entity, "GBK") ;
//		Document doc2 = Jsoup.parse(entityMsg) ;  
//		
//		Element  body = doc2.body();
//		
//		Elements span = body.getElementsByClass("_ROOM_");
//		
//		
//		
//		Iterator<Element> iter = span.iterator();
//		int count = 1;
//		 while(iter.hasNext()){
//			 Element div = iter.next();
//			 Element roomNo = div.child(0);
//			 Element roomStatusElement = div.child(2);
//			 System.out.println("room No:" + roomNo.html() + ",room status:" + roomStatusElement.html()) ;
//			 count++;
//			 
//		 }
//		
//		System.out.println();
//		
//		
//		client.close();
//	}
//
//	public static String post(String url, Map<String, String> params,
//			int socketTimeout, int connectTimeout) throws Exception {
//		CloseableHttpClient client = HttpClients.createDefault();
//		try {
//			HttpPost post = new HttpPost(url);
//			RequestConfig requestConfig = RequestConfig.custom()
//					.setSocketTimeout(socketTimeout)
//					.setConnectTimeout(connectTimeout).build();// 设置请求和传输超时时间
//			post.setConfig(requestConfig);
//			List<NameValuePair> qparams = new ArrayList<NameValuePair>();
//
//			if (params != null && !params.isEmpty()) {
//				for (String key : params.keySet()) {
//					qparams.add(new BasicNameValuePair(key, params.get(key)));
//				}
//			}
//
//			post.setEntity(new UrlEncodedFormEntity(qparams, "UTF-8"));
//			HttpResponse httpResponse = client.execute(post);
//
//			HttpEntity entity = httpResponse.getEntity();
//
//			if (entity != null) {
//				return EntityUtils.toString(entity);
//			} else {
//				return null;
//			}
//
//		} catch (Exception e) {
//			throw e;
//		} finally {
//			client.close();
//		}
//	}
//
//	public static String post(String url, String content, int socketTimeout,
//			int connectTimeout, String contentType) throws Exception {
//		CloseableHttpClient client = HttpClients.createDefault();
//		try {
//			HttpPost post = new HttpPost(url);
//			post.setHeader("content-type", contentType);
//			RequestConfig requestConfig = RequestConfig.custom()
//					.setSocketTimeout(socketTimeout)
//					.setConnectTimeout(connectTimeout).build();// 设置请求和传输超时时间
//			post.setConfig(requestConfig);
//
//			StringEntity requestEntity = new StringEntity(content, "UTF-8");
//			post.setEntity(requestEntity);
//			HttpResponse httpResponse = client.execute(post);
//			HttpEntity entity = httpResponse.getEntity();
//			Header[] responseHeaders = httpResponse.getAllHeaders();
//			for (Header header : responseHeaders) {
//				System.out.println(header.getName() + ":" + header.getValue());
//			}
//			if (entity != null) {
//				return EntityUtils.toString(entity);
//			} else {
//				return null;
//			}
//
//		} catch (Exception e) {
//			throw e;
//		} finally {
//			client.close();
//		}
//	}
//
//	public static String get(String url, Map<String, String> params,
//			int socketTimeout, int connectTimeout) throws Exception {
//		CloseableHttpClient client = HttpClients.createDefault();
//		try {
//
//			List<NameValuePair> qparams = new ArrayList<NameValuePair>();
//			if (params != null && !params.isEmpty()) {
//				for (String key : params.keySet()) {
//					qparams.add(new BasicNameValuePair(key, params.get(key)));
//				}
//			}
//
//			// 要传递的参数.　
//			url = url + "?" + URLEncodedUtils.format(qparams, HTTP.UTF_8);
//			HttpGet get = new HttpGet(url);
//			RequestConfig requestConfig = RequestConfig.custom()
//					.setSocketTimeout(socketTimeout)
//					.setConnectTimeout(connectTimeout).build();// 设置请求和传输超时时间
//			get.setConfig(requestConfig);
//
//			HttpResponse httpResponse = client.execute(get);
//			HttpEntity entity = httpResponse.getEntity();
//
//			if (entity != null) {
//				return EntityUtils.toString(entity);
//			} else {
//				return null;
//			}
//
//		} catch (Exception e) {
//			throw e;
//		} finally {
//			client.close();
//		}
//	}
//
//	public static CloseableHttpClient createSSLInsecureClient() {
//		try {
//			SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(
//					null, new TrustStrategy() {
//						// 信任所有
//						public boolean isTrusted(X509Certificate[] chain,
//								String authType) throws CertificateException {
//							return true;
//						}
//					}).build();
//			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
//					sslContext);
//			return HttpClients.custom().setSSLSocketFactory(sslsf).build();
//		} catch (KeyManagementException e) {
//			e.printStackTrace();
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		} catch (KeyStoreException e) {
//			e.printStackTrace();
//		}
//		return HttpClients.createDefault();
//	}
//
//	public static String sslPost(String url, Map<String, String> params,
//			int socketTimeout, int connectTimeout) throws Exception {
//		CloseableHttpClient client = createSSLInsecureClient();
//		try {
//			HttpPost post = new HttpPost(url);
//			RequestConfig requestConfig = RequestConfig.custom()
//					.setSocketTimeout(socketTimeout)
//					.setConnectTimeout(connectTimeout).build();// 设置请求和传输超时时间
//			post.setConfig(requestConfig);
//			List<NameValuePair> qparams = new ArrayList<NameValuePair>();
//
//			if (params != null && !params.isEmpty()) {
//				for (String key : params.keySet()) {
//					qparams.add(new BasicNameValuePair(key, params.get(key)));
//				}
//			}
//
//			post.setEntity(new UrlEncodedFormEntity(qparams, "UTF-8"));
//			HttpResponse httpResponse = client.execute(post);
//			HttpEntity entity = httpResponse.getEntity();
//
//			if (entity != null) {
//				return EntityUtils.toString(entity);
//			} else {
//				return null;
//			}
//
//		} catch (Exception e) {
//			throw e;
//		} finally {
//			client.close();
//		}
//	}
//
//	
//	 public static void main(String[] args) throws Exception{
//         new HttpClientUtilWithLogin().login("oasis", "oasis");
//     }
}
