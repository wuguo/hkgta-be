package com.sinodynamic.hkgta.integration.spa.response;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "success", "FinancialData" })
public class SpaAccountFlexReponse extends BaseResponse{
	private static final long serialVersionUID = 5784340005269259195L;
	@JsonProperty("success")
	private Boolean success;
	
	@JsonProperty("FinancialData")
	private List<SpaAccountFlex> accountFlexs = new ArrayList<SpaAccountFlex>();

	/**
	 * 
	 * @return The success
	 */
	@JsonProperty("success")
	public Boolean getSuccess() {
		return success;
	}

	/**
	 * 
	 * @param success
	 *            The success
	 */
	@JsonProperty("success")
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public List<SpaAccountFlex> getAccountFlexs() {
		return accountFlexs;
	}
	@JsonProperty("FinancialData")
	public void setAccountFlexs(List<SpaAccountFlex> accountFlexs) {
		this.accountFlexs = accountFlexs;
	}


}