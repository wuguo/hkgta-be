package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"GuestFirstName",
"GuestLastName",
"GuestMobilePhone",
"process_status",
"gender",
"usercode",
"roomno",
"comments",
"Rebooked",
"entityId",
"Email",
"AppointmentCategoryId"
})
public class GuestDetails extends BaseResponse {
	
	private static final long serialVersionUID = 5349207287856832639L;
	@JsonProperty("GuestFirstName")
	private String guestFirstName;
	@JsonProperty("GuestLastName")
	private String guestLastName;
	@JsonProperty("GuestMobilePhone")
	private String guestMobilePhone;
	@JsonProperty("process_status")
	private Integer processStatus;
	@JsonProperty("gender")
	private Integer gender;
	@JsonProperty("usercode")
	private String usercode;
	@JsonProperty("roomno")
	private Object roomno;
	@JsonProperty("comments")
	private Object comments;
	@JsonProperty("Rebooked")
	private Boolean rebooked;
	@JsonProperty("entityId")
	private Integer entityId;
	@JsonProperty("Email")
	private Object email;
	@JsonProperty("AppointmentCategoryId")
	private Object appointmentCategoryId;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	

	public String getGuestFirstName() {
		return guestFirstName;
	}

	public void setGuestFirstName(String guestFirstName) {
		this.guestFirstName = guestFirstName;
	}

	public String getGuestLastName() {
		return guestLastName;
	}

	public void setGuestLastName(String guestLastName) {
		this.guestLastName = guestLastName;
	}

	public String getGuestMobilePhone() {
		return guestMobilePhone;
	}

	public void setGuestMobilePhone(String guestMobilePhone) {
		this.guestMobilePhone = guestMobilePhone;
	}

	public Integer getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(Integer processStatus) {
		this.processStatus = processStatus;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public Object getRoomno() {
		return roomno;
	}

	public void setRoomno(Object roomno) {
		this.roomno = roomno;
	}

	public Object getComments() {
		return comments;
	}

	public void setComments(Object comments) {
		this.comments = comments;
	}

	public Boolean getRebooked() {
		return rebooked;
	}

	public void setRebooked(Boolean rebooked) {
		this.rebooked = rebooked;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public Object getEmail() {
		return email;
	}

	public void setEmail(Object email) {
		this.email = email;
	}

	public Object getAppointmentCategoryId() {
		return appointmentCategoryId;
	}

	public void setAppointmentCategoryId(Object appointmentCategoryId) {
		this.appointmentCategoryId = appointmentCategoryId;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
