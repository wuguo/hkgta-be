package com.sinodynamic.hkgta.integration.spa.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppointmentServiceCancelResponse extends BaseResponse {
	
	private static final long serialVersionUID = -3471533940767055680L;
	@JsonProperty("success")
	private Boolean success;
	@JsonProperty("message")
	private String message;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
