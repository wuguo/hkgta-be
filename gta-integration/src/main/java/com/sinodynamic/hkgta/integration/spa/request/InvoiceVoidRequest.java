package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class InvoiceVoidRequest extends BaseRequest {

	private static final long serialVersionUID = 8227893363562780626L;
	
	@UrlParameter(name = "invoiceNo")
	private String invoiceNo;
	
	@UrlParameter(name = "userCode")
	private String userCode;
	
	@UrlParameter(name = "comments")
	private String comments;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	

}
