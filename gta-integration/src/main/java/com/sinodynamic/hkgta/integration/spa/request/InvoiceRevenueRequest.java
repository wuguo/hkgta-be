package com.sinodynamic.hkgta.integration.spa.request;

import java.util.Date;

import com.sinodynamic.hkgta.integration.util.ClassValidator;
import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;


@ClassValidator(validatMethod="valiadte")
public class InvoiceRevenueRequest extends BaseRequest {

	private static final long serialVersionUID = 2549141769224004889L;
	
	@UrlParameter(name = "fromDate",dateFormat="yyyy-MM-dd")
	@FieldRules(nullable = false)
	private Date fromDate;
	@UrlParameter(name = "toDate",dateFormat="yyyy-MM-dd")
	@FieldRules(nullable = false)
	private Date toDate;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public void valiadte(){
		if(null !=fromDate && fromDate.after(toDate)) {
			throw new IllegalArgumentException("The parameter fromDate must before toDate");
		}
	}
}
