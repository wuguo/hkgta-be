package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

public class InvoiceCreateResponse extends BaseResponse{
	private static final long serialVersionUID = 7504907765106916891L;
	@JsonProperty("success")
	private boolean success;

//	@JsonProperty("message")
//	private String message;
//	@JsonProperty("Invoice-No")
//	@SerializedName("Invoice-No") 
//	private String InvoiceNo;
//	@JsonProperty("status")
//	private String status;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

//	public String getMessage() {
//		return message;
//	}
//
//	public void setMessage(String message) {
//		this.message = message;
//	}

//	public String getInvoiceNo() {
//		return InvoiceNo;
//	}
//
//	public void setInvoiceNo(String invoiceNo) {
//		InvoiceNo = invoiceNo;
//	}

//	@JsonProperty("status")
//	public String getStatus() {
//	return status;
//	}
//
//
//	@JsonProperty("status")
//	public void setStatus(String status) {
//	this.status = status;
//	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
}
