package com.sinodynamic.hkgta.integration.spa.request;

import java.util.Date;

import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class GetCenterTherapistsRequest extends BaseRequest {

	private static final long serialVersionUID = -8095081466031084910L;

	@UrlParameter(name = "requestDate",dateFormat="yyyy-MM-dd")
	private Date requestDate;

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	
}
