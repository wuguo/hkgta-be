/**
 * 
 */
package com.sinodynamic.hkgta.integration.util;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author Nick_Xiong
 *
 */
@Retention(value=RetentionPolicy.RUNTIME)
@Target(value=ElementType.FIELD)
public @interface UrlParameter {
       
    String name();//参数名
    
    int pos() default 0;//指定列位置
    
    String dateFormat() default "";//格式化字符串，如日期
    
    String map() default "";//字段值转换映射
    
    String condition() default "";//显示的前提条件
    
    String formatterMethod() default "";//格式化方法
    
}
