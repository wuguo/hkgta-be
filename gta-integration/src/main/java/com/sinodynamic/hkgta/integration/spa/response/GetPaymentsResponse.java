package com.sinodynamic.hkgta.integration.spa.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "success", "Payments" })
public class GetPaymentsResponse extends BaseResponse{

	private static final long serialVersionUID = 4780437146006522951L;
	@JsonProperty("success")
	private Boolean success;
	@JsonProperty("Payments")
	private List<Payment> payments = new ArrayList<Payment>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The success
	 */
	@JsonProperty("success")
	public Boolean getSuccess() {
		return success;
	}

	/**
	 * 
	 * @param success
	 *            The success
	 */
	@JsonProperty("success")
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	/**
	 * 
	 * @return The Payments
	 */
	@JsonProperty("Payments")
	public List<Payment> getPayments() {
		return payments;
	}

	/**
	 * 
	 * @param Payments
	 *            The Payments
	 */
	@JsonProperty("Payments")
	public void setPayments(List<Payment> Payments) {
		this.payments = Payments;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}