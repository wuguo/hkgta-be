package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class InvoiceCancelRequest extends BaseRequest {

	private static final long serialVersionUID = 8712739142730464870L;

	@UrlParameter(name = "invoiceNo")
	@FieldRules(nullable = false)
	private String invoiceNo;
    
	@UrlParameter(name = "comments")
	@FieldRules(nullable = false)
	private String comments;
	
	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	

}
