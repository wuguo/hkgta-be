package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "RoomId", "RoomName", "Factor", "capacity", "used" })
public class Room extends BaseResponse{
	private static final long serialVersionUID = 3793187526111683870L;
	@JsonProperty("RoomId")
	private String roomId;
	@JsonProperty("RoomName")
	private String roomName;
	@JsonProperty("Factor")
	private Integer factor;
	@JsonProperty("capacity")
	private Integer capacity;
	@JsonProperty("used")
	private Integer used;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();


	public String getRoomId() {
		return roomId;
	}


	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}


	public String getRoomName() {
		return roomName;
	}


	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}


	public Integer getFactor() {
		return factor;
	}


	public void setFactor(Integer factor) {
		this.factor = factor;
	}


	public Integer getCapacity() {
		return capacity;
	}


	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}


	public Integer getUsed() {
		return used;
	}


	public void setUsed(Integer used) {
		this.used = used;
	}


	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}


	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}


	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}