package com.sinodynamic.hkgta.integration.spa.request;

import java.math.BigDecimal;

import com.sinodynamic.hkgta.integration.spa.em.CustomPaymentMethod;
import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class PaymentByCustomRequest extends BaseRequest {
	private static final long serialVersionUID = -2180111930018003978L;
	@UrlParameter(name = "invoiceNo")
	@FieldRules(nullable = false)
	private String invoiceNo;
	@UrlParameter(name = "amountPaid")
	@FieldRules(nullable = false)
	private BigDecimal amountPaid;
	@UrlParameter(name = "tipAmount")
	@FieldRules(nullable = false)
	private BigDecimal tipAmount;
	@UrlParameter(name = "customPaymentName",formatterMethod="formatter")
	@FieldRules(nullable = false)
	private CustomPaymentMethod paymentMethod;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public BigDecimal getTipAmount() {
		return tipAmount;
	}

	public void setTipAmount(BigDecimal tipAmount) {
		this.tipAmount = tipAmount;
	}

	public CustomPaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(CustomPaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String formatter(CustomPaymentMethod paymentMethod) {
		String str = null;
		if (null == paymentMethod) {
			throw new IllegalArgumentException("The parameter paymentMethod can't be null");
		} else {
			str = paymentMethod.getDesc();
		}
		return str;
	}
	
}
