package com.sinodynamic.hkgta.integration.spa.request;

import java.math.BigDecimal;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class CashPaymentRequest extends BaseRequest{

	private static final long serialVersionUID = 5273072150730195326L;

	@UrlParameter(name="InvoiceNo")
	@FieldRules(nullable=false)
	private String invoiceNo;
	
	@UrlParameter(name="GuestId")
	private String guestId;
	
	@UrlParameter(name="GuestCode")
	private String guestCode;
	
	@UrlParameter(name="AmountPaid")
	private BigDecimal amountPaid;
	
	@UrlParameter(name="TipAmount")
	private BigDecimal tipAmount;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getGuestId() {
		return guestId;
	}

	public void setGuestId(String guestId) {
		this.guestId = guestId;
	}

	public String getGuestCode() {
		return guestCode;
	}

	public void setGuestCode(String guestCode) {
		this.guestCode = guestCode;
	}

	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public BigDecimal getTipAmount() {
		return tipAmount;
	}

	public void setTipAmount(BigDecimal tipAmount) {
		this.tipAmount = tipAmount;
	}
	
	
}
