package com.sinodynamic.hkgta.integration.push.service;

import com.sinodynamic.hkgta.dto.push.PushDto;
import com.sinodynamic.hkgta.dto.push.PushResultDto;
import com.sinodynamic.hkgta.dto.push.PushTopicDto;
import com.sinodynamic.hkgta.dto.push.RegisterDto;
import com.sinodynamic.hkgta.dto.push.RegisterResultDto;
import com.sinodynamic.hkgta.dto.push.SubscriptDto;
import com.sinodynamic.hkgta.dto.push.SubscriptResultDto;

public interface PushService {
	public RegisterResultDto register(RegisterDto dto) throws Exception;
	public PushResultDto push(PushDto dto) throws Exception;
	public SubscriptResultDto subscript(SubscriptDto dto) throws Exception;
	public PushResultDto pushTopic(PushTopicDto dto) throws Exception;

}
