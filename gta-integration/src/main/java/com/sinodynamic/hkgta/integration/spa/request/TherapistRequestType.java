package com.sinodynamic.hkgta.integration.spa.request;

public enum TherapistRequestType {

	ANY("-1"),FEMALE("0"),MALE("1");
	
	
	private TherapistRequestType(String code) {
		this.code = code;
	}

	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	private String code;
}
