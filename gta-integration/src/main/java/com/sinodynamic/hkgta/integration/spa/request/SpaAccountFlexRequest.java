package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class SpaAccountFlexRequest extends BaseRequest {

	private static final long serialVersionUID = 8227893363562780626L;
	
	@UrlParameter(name = "userName")
	private String userName;
	
	@UrlParameter(name = "userPassword")
	private String userPassword;
	
	@UrlParameter(name = "accountName")
	private String accountName;
	
	@UrlParameter(name = "fromDate")
	private String appVersion;
	
	@UrlParameter(name = "fromDate")
	private String centerId;
	
	@UrlParameter(name = "fromDate")
	private String fromDate;
	
	@UrlParameter(name = "toDate")
	private String toDate;
	
	@UrlParameter(name = "Status")
	private String status;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
