package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class AppointmentDetailsRequest extends BaseRequest {

	private static final long serialVersionUID = 3977131485505955796L;

	@UrlParameter(name = "invoiceNo")
	@FieldRules(nullable = false)
	private String invoiceNo;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
	
}
