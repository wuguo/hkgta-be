package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class GetServiceDetailsRequest extends BaseRequest {
	private static final long serialVersionUID = 1L;
	
	@UrlParameter(name="serviceCode",pos=1)
	@FieldRules(nullable = false)
	private String serviceCode;

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	
}
