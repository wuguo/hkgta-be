package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class InvoiceCreate extends BaseRequest {

	private static final long serialVersionUID = 581651558597201094L;

	@UrlParameter(pos = 1, name = "InvoiceNo")
	private String invoiceNo;
	
	@UrlParameter(pos = 2, name = "CenterId")
	private String centerId;
	
	@UrlParameter(pos = 3, name = "InvoiceDetails")
	private String InvoiceDetails;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getInvoiceDetails() {
		return InvoiceDetails;
	}

	public void setInvoiceDetails(String invoiceDetails) {
		InvoiceDetails = invoiceDetails;
	}

	
}
