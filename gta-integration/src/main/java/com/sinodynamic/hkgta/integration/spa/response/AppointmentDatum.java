package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"RowNum",
"GroupId",
"AppointmentId",
"AppointmentNum",
"ServiceName",
"ServiceCode",
"Room",
"CreationDate",
"StartTime",
"EndTime",
"Status",
"CheckInTime",
"Rebooked",
"ServiceInternalCost",
"AppointmentSource",
"IsLock",
"GuestID",
"GuestCode",
"GuestFName",
"GuestLName",
"GuestPhone",
"EmployeeCode",
"EmployeeFName",
"EmployeeLName",
"Note",
"createdby",
"InvoiceNo",
"ServiceTime",
"RecoveryTime",
"ActualTime",
"Category",
"SubCategory",
"Package",
"BusinessUnit",
"RequestType",
"CenterName",
"CenterCode",
"EmployeeJobCode",
"PackageInvoice"
})
public class AppointmentDatum extends BaseResponse{

	private static final long serialVersionUID = 5907324179461121262L;
	@JsonProperty("RowNum")
	private String rowNum;
	@JsonProperty("GroupId")
	private String groupId;
	@JsonProperty("AppointmentId")
	private String appointmentId;
	@JsonProperty("AppointmentNum")
	private String appointmentNum;
	@JsonProperty("ServiceName")
	private String serviceName;
	@JsonProperty("ServiceCode")
	private String serviceCode;
	@JsonProperty("Room")
	private String room;
	@JsonProperty("CreationDate")
	private String creationDate;
	@JsonProperty("StartTime")
	private String startTime;
	@JsonProperty("EndTime")
	private String endTime;
	@JsonProperty("Status")
	private String status;
	@JsonProperty("CheckInTime")
	private String checkInTime;
	@JsonProperty("Rebooked")
	private String rebooked;
	@JsonProperty("ServiceInternalCost")
	private String serviceInternalCost;
	@JsonProperty("AppointmentSource")
	private String appointmentSource;
	@JsonProperty("IsLock")
	private String isLock;
	@JsonProperty("GuestID")
	private String guestID;
	@JsonProperty("GuestCode")
	private String guestCode;
	@JsonProperty("GuestFName")
	private String guestFName;
	@JsonProperty("GuestLName")
	private String guestLName;
	@JsonProperty("GuestPhone")
	private String guestPhone;
	@JsonProperty("EmployeeCode")
	private String employeeCode;
	@JsonProperty("EmployeeFName")
	private String employeeFName;
	@JsonProperty("EmployeeLName")
	private String employeeLName;
	@JsonProperty("Note")
	private String note;
	@JsonProperty("createdby")
	private String createdby;
	@JsonProperty("InvoiceNo")
	private String invoiceNo;
	@JsonProperty("ServiceTime")
	private String serviceTime;
	@JsonProperty("RecoveryTime")
	private String recoveryTime;
	@JsonProperty("ActualTime")
	private String actualTime;
	@JsonProperty("Category")
	private String category;
	@JsonProperty("SubCategory")
	private String subCategory;
	@JsonProperty("Package")
	private String packageName;
	@JsonProperty("BusinessUnit")
	private String businessUnit;
	@JsonProperty("RequestType")
	private String requestType;
	@JsonProperty("CenterName")
	private String centerName;
	@JsonProperty("CenterCode")
	private String centerCode;
	@JsonProperty("EmployeeJobCode")
	private String employeeJobCode;
	@JsonProperty("PackageInvoice")
	private String packageInvoice;
	@JsonProperty("FinalSalePrice")
	private String finalSalePrice;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	
	
	public String getRowNum() {
		return rowNum;
	}

	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getAppointmentNum() {
		return appointmentNum;
	}

	public void setAppointmentNum(String appointmentNum) {
		this.appointmentNum = appointmentNum;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public String getRebooked() {
		return rebooked;
	}

	public void setRebooked(String rebooked) {
		this.rebooked = rebooked;
	}

	public String getServiceInternalCost() {
		return serviceInternalCost;
	}

	public void setServiceInternalCost(String serviceInternalCost) {
		this.serviceInternalCost = serviceInternalCost;
	}

	public String getAppointmentSource() {
		return appointmentSource;
	}

	public void setAppointmentSource(String appointmentSource) {
		this.appointmentSource = appointmentSource;
	}

	public String getIsLock() {
		return isLock;
	}

	public void setIsLock(String isLock) {
		this.isLock = isLock;
	}

	public String getGuestID() {
		return guestID;
	}

	public void setGuestID(String guestID) {
		this.guestID = guestID;
	}

	public String getGuestCode() {
		return guestCode;
	}

	public void setGuestCode(String guestCode) {
		this.guestCode = guestCode;
	}

	public String getGuestFName() {
		return guestFName;
	}

	public void setGuestFName(String guestFName) {
		this.guestFName = guestFName;
	}

	public String getGuestLName() {
		return guestLName;
	}

	public void setGuestLName(String guestLName) {
		this.guestLName = guestLName;
	}

	public String getGuestPhone() {
		return guestPhone;
	}

	public void setGuestPhone(String guestPhone) {
		this.guestPhone = guestPhone;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeFName() {
		return employeeFName;
	}

	public void setEmployeeFName(String employeeFName) {
		this.employeeFName = employeeFName;
	}

	public String getEmployeeLName() {
		return employeeLName;
	}

	public void setEmployeeLName(String employeeLName) {
		this.employeeLName = employeeLName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}

	public String getRecoveryTime() {
		return recoveryTime;
	}

	public void setRecoveryTime(String recoveryTime) {
		this.recoveryTime = recoveryTime;
	}

	public String getActualTime() {
		return actualTime;
	}

	public void setActualTime(String actualTime) {
		this.actualTime = actualTime;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getCenterCode() {
		return centerCode;
	}

	public void setCenterCode(String centerCode) {
		this.centerCode = centerCode;
	}

	public String getEmployeeJobCode() {
		return employeeJobCode;
	}

	public void setEmployeeJobCode(String employeeJobCode) {
		this.employeeJobCode = employeeJobCode;
	}

	public String getPackageInvoice() {
		return packageInvoice;
	}

	public void setPackageInvoice(String packageInvoice) {
		this.packageInvoice = packageInvoice;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
	
	public String getFinalSalePrice() {
		return finalSalePrice;
	}

	public void setFinalSalePrice(String finalSalePrice) {
		this.finalSalePrice = finalSalePrice;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}