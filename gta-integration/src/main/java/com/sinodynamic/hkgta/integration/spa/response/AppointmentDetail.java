package com.sinodynamic.hkgta.integration.spa.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"AppointmentId",
"IsLock",
"ItemName",
"ItemTypeName",
"validdays",
"SrvcTime",
"ItmType",
"TherapistRequestType",
"serviceprice",
"TherapistName",
"FinalPrice",
"Tax",
"saleprice",
"Room",
"starttime",
"endtime",
"note",
"pkgsvcopt",
"pkggroupno",
"TherapistRequestType1",
"SegmentModified",
"hasJobServiceSF",
"canModifyTherapist",
"entityId",
"entity",
"hasAddOns",
"AddOnIds",
"IsAddOn"
})
public class AppointmentDetail extends BaseResponse {
	
	private static final long serialVersionUID = 6119534429379611774L;
	@JsonProperty("AppointmentId")
	private String AppointmentId;
	@JsonProperty("IsLock")
	private Boolean isLock;
	@JsonProperty("ItemName")
	private String itemName;
	@JsonProperty("ItemTypeName")
	private String itemTypeName;
	@JsonProperty("validdays")
	private Object validdays;
	@JsonProperty("SrvcTime")
	private Integer srvcTime;
	@JsonProperty("ItmType")
	private Integer itmType;
	@JsonProperty("TherapistRequestType")
	private Object therapistRequestType;
	@JsonProperty("serviceprice")
	private Double serviceprice;
	@JsonProperty("TherapistName")
	private String therapistName;
	@JsonProperty("FinalPrice")
	private Double finalPrice;
	@JsonProperty("Tax")
	private Double tax;
	@JsonProperty("saleprice")
	private Double saleprice;
	@JsonProperty("Room")
	private Object room;
	@JsonProperty("starttime")
	private String starttime;
	@JsonProperty("endtime")
	private String endtime;
	@JsonProperty("note")
	private Object note;
	@JsonProperty("pkgsvcopt")
	private String pkgsvcopt;
	@JsonProperty("pkggroupno")
	private Integer pkggroupno;
	@JsonProperty("TherapistRequestType1")
	private Integer therapistRequestType1;
	@JsonProperty("SegmentModified")
	private Integer segmentModified;
	@JsonProperty("hasJobServiceSF")
	private Integer hasJobServiceSF;
	@JsonProperty("canModifyTherapist")
	private Integer canModifyTherapist;
	@JsonProperty("entityId")
	private Integer entityId;
	@JsonProperty("entity")
	private String entity;
	@JsonProperty("hasAddOns")
	private Integer hasAddOns;
	@JsonProperty("AddOnIds")
	private Object addOnIds;
	@JsonProperty("IsAddOn")
	private Boolean isAddOn;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	
	public String getAppointmentId() {
		return AppointmentId;
	}

	public void setAppointmentId(String appointmentId) {
		AppointmentId = appointmentId;
	}

	public Boolean getIsLock() {
		return isLock;
	}

	public void setIsLock(Boolean isLock) {
		this.isLock = isLock;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemTypeName() {
		return itemTypeName;
	}

	public void setItemTypeName(String itemTypeName) {
		this.itemTypeName = itemTypeName;
	}

	public Object getValiddays() {
		return validdays;
	}

	public void setValiddays(Object validdays) {
		this.validdays = validdays;
	}

	public Integer getSrvcTime() {
		return srvcTime;
	}

	public void setSrvcTime(Integer srvcTime) {
		this.srvcTime = srvcTime;
	}

	public Integer getItmType() {
		return itmType;
	}

	public void setItmType(Integer itmType) {
		this.itmType = itmType;
	}

	public Object getTherapistRequestType() {
		return therapistRequestType;
	}

	public void setTherapistRequestType(Object therapistRequestType) {
		this.therapistRequestType = therapistRequestType;
	}

	public Double getServiceprice() {
		return serviceprice;
	}

	public void setServiceprice(Double serviceprice) {
		this.serviceprice = serviceprice;
	}

	public String getTherapistName() {
		return therapistName;
	}

	public void setTherapistName(String therapistName) {
		this.therapistName = therapistName;
	}

	public Double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public Double getSaleprice() {
		return saleprice;
	}

	public void setSaleprice(Double saleprice) {
		this.saleprice = saleprice;
	}

	public Object getRoom() {
		return room;
	}

	public void setRoom(Object room) {
		this.room = room;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public Object getNote() {
		return note;
	}

	public void setNote(Object note) {
		this.note = note;
	}

	public String getPkgsvcopt() {
		return pkgsvcopt;
	}

	public void setPkgsvcopt(String pkgsvcopt) {
		this.pkgsvcopt = pkgsvcopt;
	}

	public Integer getPkggroupno() {
		return pkggroupno;
	}

	public void setPkggroupno(Integer pkggroupno) {
		this.pkggroupno = pkggroupno;
	}

	public Integer getTherapistRequestType1() {
		return therapistRequestType1;
	}

	public void setTherapistRequestType1(Integer therapistRequestType1) {
		this.therapistRequestType1 = therapistRequestType1;
	}

	public Integer getSegmentModified() {
		return segmentModified;
	}

	public void setSegmentModified(Integer segmentModified) {
		this.segmentModified = segmentModified;
	}

	public Integer getHasJobServiceSF() {
		return hasJobServiceSF;
	}

	public void setHasJobServiceSF(Integer hasJobServiceSF) {
		this.hasJobServiceSF = hasJobServiceSF;
	}

	public Integer getCanModifyTherapist() {
		return canModifyTherapist;
	}

	public void setCanModifyTherapist(Integer canModifyTherapist) {
		this.canModifyTherapist = canModifyTherapist;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Integer getHasAddOns() {
		return hasAddOns;
	}

	public void setHasAddOns(Integer hasAddOns) {
		this.hasAddOns = hasAddOns;
	}

	public Object getAddOnIds() {
		return addOnIds;
	}

	public void setAddOnIds(Object addOnIds) {
		this.addOnIds = addOnIds;
	}

	public Boolean getIsAddOn() {
		return isAddOn;
	}

	public void setIsAddOn(Boolean isAddOn) {
		this.isAddOn = isAddOn;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
}
