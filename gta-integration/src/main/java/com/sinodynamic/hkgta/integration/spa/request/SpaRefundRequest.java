package com.sinodynamic.hkgta.integration.spa.request;

import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class SpaRefundRequest extends BaseRequest {

	private static final long serialVersionUID = 8227893363562780626L;
	
	@UrlParameter(name = "fromDate")
	private String fromDate;
	
	@UrlParameter(name = "toDate")
	private String toDate;
	
	@UrlParameter(name = "offset")
	private Integer offset;
	
	@UrlParameter(name = "page")
	private Integer page;

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	
}
