package com.sinodynamic.hkgta.integration.spa.em;

public enum CustomPaymentMethod {

	CASHVALUE("CashValue");

	private CustomPaymentMethod(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private String desc;
}
