package com.sinodynamic.hkgta.integration.spa.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"success",
"Guest Details",
"Appointment Details"
})
public class AppointmentDetailsResponse extends BaseResponse{
	
	private static final long serialVersionUID = 4490791272167303922L;
	@JsonProperty("success")
	private Boolean success;
	@JsonProperty("message")
	private String message;
	@JsonProperty("Guest Details")
	private GuestDetails guestDetails;
	@JsonProperty("Appointment Details")
	private List<AppointmentDetail> appointmentDetails = new ArrayList<AppointmentDetail>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	* 
	* @return
	* The success
	*/
	@JsonProperty("success")
	public Boolean getSuccess() {
	return success;
	}

	/**
	* 
	* @param success
	* The success
	*/
	@JsonProperty("success")
	public void setSuccess(Boolean success) {
	this.success = success;
	}

	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

	public GuestDetails getGuestDetails() {
		return guestDetails;
	}

	public void setGuestDetails(GuestDetails guestDetails) {
		this.guestDetails = guestDetails;
	}

	public List<AppointmentDetail> getAppointmentDetails() {
		return appointmentDetails;
	}

	public void setAppointmentDetails(List<AppointmentDetail> appointmentDetails) {
		this.appointmentDetails = appointmentDetails;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
}
