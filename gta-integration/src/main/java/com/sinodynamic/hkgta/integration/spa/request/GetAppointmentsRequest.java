package com.sinodynamic.hkgta.integration.spa.request;

import java.util.Date;

import com.sinodynamic.hkgta.integration.spa.em.AppointmentStatus;
import com.sinodynamic.hkgta.integration.util.FieldRules;
import com.sinodynamic.hkgta.integration.util.UrlParameter;

public class GetAppointmentsRequest extends BaseRequest {

	private static final long serialVersionUID = -7659937931156944555L;
	@UrlParameter(name = "fromDate",dateFormat="yyyy-MM-dd")
	@FieldRules(nullable = false)
	private Date fromDate;
	@UrlParameter(name = "toDate",dateFormat="yyyy-MM-dd")
	@FieldRules(nullable = false)
	private Date toDate;
	@UrlParameter(name="status",formatterMethod="formatter")
	@FieldRules(nullable = false)
	private AppointmentStatus status;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public AppointmentStatus getStatus() {
		return status;
	}

	public void setStatus(AppointmentStatus status) {
		this.status = status;
	}

	public String formatter(AppointmentStatus status) {
		return status.getCode();
	}
}
