package com.sinodynamic.hkgta.security;

import java.util.Collection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.sinodynamic.hkgta.util.constant.Constant;

public class GtaAccessDecisionManager implements AccessDecisionManager {

	private static final Logger logger = LogManager.getLogger(GtaAccessDecisionManager.class);

	@Override
	public void decide(Authentication auth, Object url, Collection<ConfigAttribute> configAttrs)
			throws AccessDeniedException, InsufficientAuthenticationException {

		// @see org.springframework.security.web.access.DefaultWebInvocationPrivilegeEvaluator#isAllowed, param
		// "configAttrs" probably will not be null, otherwise it will not go here.
		if (configAttrs == null) {
			logger.error("Spring-security config-attributes not found.");
			// throw new AccessDeniedException("no right");
		}

		boolean authorised = false;
		for (GrantedAuthority granted : auth.getAuthorities()) {
			for (ConfigAttribute config : configAttrs) {
				if (granted.getAuthority().equalsIgnoreCase(config.getAttribute())
						&& config instanceof GtaSecurityConfig) {
					String programAccessRight = ((GtaSecurityConfig) config).getAccessRight();

					if (Constant.SECURITY_ACCESS_RIGHT_ALL.equals(programAccessRight)) {
						authorised = true;

					} else {
						String httpMethod = ((org.springframework.security.web.FilterInvocation) url).getHttpRequest()
								.getMethod();

						if (Constant.SECURITY_ACCESS_RIGHT_READ.equals(programAccessRight)
								&& HttpMethod.GET.toString().equals(httpMethod)) {
							authorised = true;

						} else if (Constant.SECURITY_ACCESS_RIGHT_CREATE_UPDATE.equals(programAccessRight)
								&& (HttpMethod.GET.toString().equals(httpMethod)
										|| HttpMethod.POST.toString().equals(httpMethod) || HttpMethod.PUT.toString()
										.equals(httpMethod))) {
							authorised = true;
						}
					}
				}
			}
		}
		if (!authorised) {
			logger.error("Spring-security blocked unauthorized access.");
			throw new AccessDeniedException("access not authorized");
		}

		logger.debug("Spring-security config-attributes found, let pass.");

	}

	@Override
	public boolean supports(ConfigAttribute configAttr) {
		return true;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

}
