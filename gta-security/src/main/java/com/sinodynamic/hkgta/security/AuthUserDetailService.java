package com.sinodynamic.hkgta.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.sinodynamic.hkgta.security.service.UserMasterService;
import com.sinodynamic.hkgta.util.exception.GTACommonException;



public class AuthUserDetailService implements UserDetailsService
{

    @Autowired
    private UserMasterService userMasterService;
    
    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException, DataAccessException, GTACommonException {
        return userMasterService.loadUserByUsername(userId);
    }
	

}
