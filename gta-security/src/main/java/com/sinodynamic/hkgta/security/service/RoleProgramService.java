package com.sinodynamic.hkgta.security.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.sys.AssignProgramDto;
import com.sinodynamic.hkgta.dto.sys.MenuDto;
import com.sinodynamic.hkgta.dto.sys.ProgramMasterDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramURLDto;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;
import com.sinodynamic.hkgta.entity.crm.RoleProgram;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RoleProgramService extends IServiceBase<RoleProgram>{
	public List<RoleProgramURLDto> getRoleProgramsWithUrlPath();

	public ResponseResult roleAddProg(RoleProgramDto dto);

	public ResponseResult roleDelProg(RoleProgramDto dto);

	public ResponseResult getRoleProg(BigInteger roleId, String programId);

	public ResponseResult roleEditProg(RoleProgramDto dto);

	public List<RoleMaster> getRoleListByUserType(String userType);

	public ResponseResult assignProgram(AssignProgramDto dto);

	void refreshRole();

	public ResponseResult getRoleProgramsByRoleId(BigInteger roleId, String userType);

	Map<String, List<MenuDto>> initRoleMenu(String userId);

	List<ProgramMasterDto> getSalesReportCategories(List<Long> userRoleIds);
	
	public  List<MenuDto>  initRoleMenu(RoleMaster role) ;

}
