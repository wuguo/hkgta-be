package com.sinodynamic.hkgta.security.service;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.sys.ProgramMasterDao;
import com.sinodynamic.hkgta.dao.sys.RoleMasterDao;
import com.sinodynamic.hkgta.dao.sys.RoleProgramDao;
import com.sinodynamic.hkgta.dto.sys.AssignProgramDto;
import com.sinodynamic.hkgta.dto.sys.MenuDto;
import com.sinodynamic.hkgta.dto.sys.ProgramDto;
import com.sinodynamic.hkgta.dto.sys.ProgramMasterDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramURLDto;
import com.sinodynamic.hkgta.entity.crm.ProgramMaster;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;
import com.sinodynamic.hkgta.entity.crm.RoleProgram;
import com.sinodynamic.hkgta.entity.crm.RoleProgramPK;
import com.sinodynamic.hkgta.security.GtaInvocationSecurityMetadataSource;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RoleProgramServiceImpl extends ServiceBase<RoleProgram>implements RoleProgramService {
	@Autowired
	private RoleProgramDao roleProgramDao;
	@Autowired
	private RoleMasterDao roleMasterDao;
	@Autowired
	private ProgramMasterDao programMasterDao;

	@Autowired
	private GtaInvocationSecurityMetadataSource securityMetadataSource;

	@Transactional(readOnly = true)
	public List<RoleProgramURLDto> getRoleProgramsWithUrlPath() {
		return roleProgramDao.getRoleProgramsWithUrlPath();
	}

	@Override
	@Transactional
	public ResponseResult getRoleProgramsByRoleId(BigInteger roleId, String userType) {
		List<ProgramMasterDto> roleProgramList = new ArrayList<ProgramMasterDto>();
		try {
			String hql = "";

			if (userType.toUpperCase().equals("ADMIN")) {
				hql = "from ProgramMaster where status='ACT' and programId='m_User_Role') order by sortSeq asc";
			} else if (userType.toUpperCase().equals("STAFF")) {
				hql = "from ProgramMaster where status='ACT' and programId<>'m_User_Role' and ((parentId is null or parentId='')) order by sortSeq asc";
			} else if (userType.toUpperCase().equals("SA")) {
				hql = "from ProgramMaster where status='ACT' and ((parentId is null or parentId='')) order by sortSeq asc";
			}
			List<ProgramMaster> pmList1 = programMasterDao.getByHql(hql);

			for (ProgramMaster pm : pmList1) {
				ProgramMasterDto menu = new ProgramMasterDto();
				menu.setProgramId(pm.getProgramId() == null ? "" : pm.getProgramId());
				menu.setMenuName(pm.getMenuName() == null ? "" : pm.getMenuName());
				menu.setProgramType(pm.getProgramType() == null ? "" : pm.getProgramType());
				if (roleId != null || roleId != BigInteger.ZERO) {
					RoleProgramPK id = new RoleProgramPK();
					id.setProgramId(pm.getProgramId());
					id.setRoleId(roleId.toString());
					RoleProgram rp = roleProgramDao.get(RoleProgram.class, id);
					if (rp != null) {
						menu.setAccessRight(rp.getAccessRight());
						menu.setChecked(true);
					} else {
						menu.setChecked(false);
						menu.setAccessRight("*");
					}
				}
				menu.setChildren(getChildren(menu, roleId));
				roleProgramList.add(menu);
			}
			responseResult.initResult(GTAError.Success.SUCCESS, roleProgramList);
		} catch (HibernateException e) {
			logger.error(RoleProgramServiceImpl.class.getName() + " getRoleProgramsByRoleId Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_ADD_ROLE_PROGRAM);
		}
		return responseResult;
	}

	@Transactional
    public List<ProgramMasterDto> getChildren(ProgramMasterDto menu, BigInteger roleId) {
		List<ProgramMasterDto> subMenuList = new ArrayList<ProgramMasterDto>();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(menu.getProgramId());
		List<ProgramMaster> subLs = programMasterDao
				.getByHql("from ProgramMaster where status='ACT' and parentId=? order by sortSeq asc", param);
		if (subLs != null && subLs.size() > 0) {
			for (ProgramMaster pm : subLs) {
				ProgramMasterDto m = new ProgramMasterDto();
				m.setProgramId(pm.getProgramId() == null ? "" : pm.getProgramId());
				m.setMenuName(pm.getMenuName() == null ? "" : pm.getMenuName());
				m.setProgramType(pm.getProgramType() == null ? "" : pm.getProgramType());
				if (roleId != null || roleId != BigInteger.ZERO) {
					RoleProgramPK id = new RoleProgramPK();
					id.setProgramId(pm.getProgramId());
					id.setRoleId(roleId.toString());
					RoleProgram rp = roleProgramDao.get(RoleProgram.class, id);
					if (rp != null) {
						m.setAccessRight(rp.getAccessRight());
						m.setChecked(true);
					} else {
						m.setChecked(false);
						m.setAccessRight("*");
					}
				}
				m.setChildren(getChildren(m, roleId));
				subMenuList.add(m);
			}
		}
		return subMenuList;
	}

	@Override
	@Transactional
	public ResponseResult roleAddProg(RoleProgramDto dto) {
		if (dto.getRoleId() == null) {
			responseResult.initResult(GTAError.SysError.ROLE_ID_EMPTY);
			return responseResult;
		}
		if (StringUtils.isEmpty(dto.getProgramIds())) {
			responseResult.initResult(GTAError.SysError.PROGRAM_IDS_EMPTY);
			return responseResult;
		}
		String[] pIds = dto.getProgramIds().split(",");
		boolean flg = true;
		for (int i = 0; i < pIds.length; i++) {
			RoleProgram roleProgram = new RoleProgram();
			RoleProgramPK id = new RoleProgramPK();
			id.setRoleId(dto.getRoleId().toString());
			id.setProgramId(pIds[i]);
			roleProgram.setId(id);
			roleProgram.setRoleMaster(roleMasterDao.get(RoleMaster.class, dto.getRoleId()));
			;
			roleProgram.setProgramMaster(programMasterDao.get(ProgramMaster.class, pIds[i]));
			roleProgram.setAccessRight("*");
			try {
				roleProgramDao.saveOrUpdate(roleProgram);
			} catch (HibernateException e) {
				logger.error(RoleProgramServiceImpl.class.getName() + " ADD Failed!", e);
				flg = false;
				break;
			}
		}
		if (flg) {
			responseResult.initResult(GTAError.Success.SUCCESS);
		} else {
			responseResult.initResult(GTAError.SysError.FAIL_ADD_ROLE_PROGRAM);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult roleDelProg(RoleProgramDto dto) {
		if (dto.getRoleId() == null) {
			responseResult.initResult(GTAError.SysError.ROLE_ID_EMPTY);
			return responseResult;
		}
		if (StringUtils.isEmpty(dto.getProgramIds())) {
			responseResult.initResult(GTAError.SysError.PROGRAM_IDS_EMPTY);
			return responseResult;
		}
		String[] pIds = dto.getProgramIds().split(",");
		boolean flg = true;
		for (int i = 0; i < pIds.length; i++) {
			RoleProgram roleProgram = new RoleProgram();
			RoleProgramPK id = new RoleProgramPK();
			id.setRoleId(dto.getRoleId().toString());
			id.setProgramId(pIds[i]);
			roleProgram.setId(id);
			roleProgram.setRoleMaster(roleMasterDao.get(RoleMaster.class, dto.getRoleId()));
			;
			roleProgram.setProgramMaster(programMasterDao.get(ProgramMaster.class, pIds[i]));
			roleProgram.setAccessRight("*");
			try {
				roleProgramDao.delete(roleProgram);
			} catch (HibernateException e) {
				logger.error(RoleProgramServiceImpl.class.getName() + " del Failed!", e);
				flg = false;
				break;
			}
		}
		if (flg) {
			responseResult.initResult(GTAError.Success.SUCCESS);
		} else {
			responseResult.initResult(GTAError.SysError.FAIL_DEL_ROLE_PROGRAM);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getRoleProg(BigInteger roleId, String programId) {
		if (roleId == null) {
			responseResult.initResult(GTAError.SysError.ROLE_ID_EMPTY);
			return responseResult;
		}
		if (StringUtils.isEmpty(programId)) {
			responseResult.initResult(GTAError.SysError.PROGRAM_IDS_EMPTY);
			return responseResult;
		}
		RoleProgramPK id = new RoleProgramPK();
		id.setRoleId(roleId.toString());
		id.setProgramId(programId);
		try {
			RoleProgram rp = roleProgramDao.get(RoleProgram.class, id);
			responseResult.initResult(GTAError.Success.SUCCESS, rp);
		} catch (HibernateException e) {
			logger.error(RoleProgramServiceImpl.class.getName() + " getRoleProg Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_GET_ROLE_PROGRAM);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult roleEditProg(RoleProgramDto dto) {
		if (dto.getRoleId() == null) {
			responseResult.initResult(GTAError.SysError.ROLE_ID_EMPTY);
			return responseResult;
		}
		if (StringUtils.isEmpty(dto.getProgramIds())) {
			responseResult.initResult(GTAError.SysError.PROGRAM_IDS_EMPTY);
			return responseResult;
		}
		if (StringUtils.isEmpty(dto.getAccessRight())) {
			responseResult.initResult(GTAError.SysError.ACCESS_RIGHT_EMPTY);
			return responseResult;
		}
		RoleProgram roleProgram = new RoleProgram();
		RoleProgramPK id = new RoleProgramPK();
		id.setRoleId(dto.getRoleId().toString());
		id.setProgramId(dto.getProgramIds());
		roleProgram.setId(id);
		roleProgram.setAccessRight(dto.getAccessRight());
		try {
			roleProgramDao.update(roleProgram);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (HibernateException e) {
			logger.error(RoleProgramServiceImpl.class.getName() + " roleEditProg Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_EDIT_ROLE_PROGRAM);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public List<RoleMaster> getRoleListByUserType(String userType) {
		List<RoleMaster> qr = null;
		if (StringUtils.isEmpty(userType)) {
			qr = (List<RoleMaster>) roleMasterDao.getByHql("from RoleMaster where status='ACT' order by roleId");
		} else {
			List<Serializable> param = new ArrayList<Serializable>();
			param.add(userType);
			qr = (List<RoleMaster>) roleMasterDao
					.getByHql("from RoleMaster where status='ACT' and forUserType=? order by roleId", param);
		}
		return qr;
	}

	@Override
	@Transactional
	public ResponseResult assignProgram(AssignProgramDto dto) {
		Long roleId = null;
		RoleMaster r = null;
		if (dto.getRoleId() == null || dto.getRoleId().intValue() == BigInteger.ZERO.intValue()) {
			r = new RoleMaster();
			r.setRoleName(dto.getRoleName());
			r.setForUserType(dto.getUserType());
			r.setStatus("ACT");
			roleId = (Long) roleMasterDao.save(r);
		} else {
			roleId = dto.getRoleId();
			r = roleMasterDao.get(RoleMaster.class, roleId);
			if (r == null) {
				responseResult.initResult(GTAError.SysError.ROLE_EMPTY);
				return responseResult;
			}
			r.setRoleName(dto.getRoleName());
			r.setForUserType(dto.getUserType());
			roleMasterDao.update(r);
			List<Serializable> param = new ArrayList<Serializable>();
			param.add(roleId.toString());
			roleProgramDao.hqlUpdate("delete from RoleProgram where id.roleId=?", param);
		}

		try {
			List<ProgramDto> programs = dto.getPrograms();
			boolean flg = true;
			if (programs != null && programs.size() > 0) {
				for (ProgramDto program : programs) {
					if (StringUtils.isNotBlank(program.getProgramId())
							&& StringUtils.isNotBlank(program.getAccessRight())) {
						RoleProgram roleProgram = new RoleProgram();
						RoleProgramPK id = new RoleProgramPK();
						id.setRoleId(roleId.toString());
						id.setProgramId(program.getProgramId());
						roleProgram.setId(id);
						roleProgram.setRoleMaster(roleMasterDao.get(RoleMaster.class, dto.getRoleId()));
						;
						roleProgram.setProgramMaster(programMasterDao.get(ProgramMaster.class, program.getProgramId()));
						roleProgram.setAccessRight(program.getAccessRight());
						try {
							roleProgramDao.saveOrUpdate(roleProgram);
						} catch (HibernateException e) {
							logger.error(RoleProgramServiceImpl.class.getName() + " ADD Failed!", e);
							flg = false;
							break;
						}
					} else {
						flg = false;
						responseResult.initResult(GTAError.SysError.FAIL_ASSIGN_PROGRAM_PROGRAM);
						break;
					}
				}
			}
			if (flg) {
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		} catch (HibernateException e) {
			logger.error(RoleProgramServiceImpl.class.getName() + " assignProgram Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_ASSIGN_PROGRAM_PROGRAM);
		}

		// refresh();

		return responseResult;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	@CacheEvict(value = "menuCache", allEntries = true) // Added by SAM 20160222
	@Deprecated
	public void refreshRole() {
		securityMetadataSource.loadProgramRoles();
	}

	@Transactional
	@Override
	//@Cacheable(value = "menuCache")
	@Cacheable(value = "menuCache", key="#userId")
	public Map<String, List<MenuDto>> initRoleMenu(String userId) { // added parameter userId for ehcache only. SAM 20170314 
		Map<String, List<MenuDto>> roleMenu = new HashMap<String, List<MenuDto>>();
		List<RoleMaster> roles=null; 

		logger.info("** user=" + userId);
		
		if (userId!=null && !"".equals(userId)) {
			  //logger.info("**** hql ***:" +"from RoleMaster as rm where rm.status='ACT' and rm.roleId in (select ur.id.roleId from UserRole as ur where ur.id.userId='" + userId + "') order by rm.roleId");			
		   roles = (List<RoleMaster>) roleMasterDao
				.getByHql("from RoleMaster as rm where rm.status='ACT' and exists (from UserRole as ur where ur.id.roleId=rm.roleId and ur.id.userId='" + userId + "') order by rm.roleId");
				//.getByHql("from RoleMaster as rm where rm.status='ACT' and rm.roleId in (select ur.id.roleId from UserRole as ur where ur.id.userId='" + userId + "') order by rm.roleId");		

		}
		else  { 
		   return new HashMap<String, List<MenuDto>>();
//		   roles = (List<RoleMaster>) roleMasterDao
//				.getByHql("from RoleMaster where status='ACT' order by roleId");		
		   
		}   
		
		// SAM moved this code here on 20170315
		List<ProgramMaster> progmasterList = programMasterDao.getByHql(
				"from ProgramMaster where status='ACT' and (parentId is null or parentId='') order by sortSeq asc");		
		
		for (RoleMaster role : roles) {
			logger.info("Role=" + role.getRoleId());
			
			List<ProgramDto> tmpRPList = roleProgramDao.getRoleProgramsById(role.getRoleId());
			//roleMenu.put(role.getRoleId().toString(), initMenu(tmpRPList));
			roleMenu.put(role.getRoleId().toString(), initMenu(tmpRPList, progmasterList));
		}
		logger.info("Init Role Menu Done!");
		return roleMenu;
	}
	@Transactional
	@Override
	public  List<MenuDto>  initRoleMenu(RoleMaster role) {
		   // SAM moved this code here on 20170315
		   List<ProgramMaster> progmasterList = programMasterDao.getByHql(
				"from ProgramMaster where status='ACT' and (parentId is null or parentId='') order by sortSeq asc");
				   
			List<ProgramDto> tmpRPList = roleProgramDao.getRoleProgramsById(role.getRoleId());
			return initMenu(tmpRPList, progmasterList);
	}
	@SuppressWarnings("unchecked")
	private List<MenuDto> initMenu(List<ProgramDto> rpList, List<ProgramMaster> progmasterList) {

		List<MenuDto> menuList = new ArrayList<MenuDto>();

		// SAM commented this on 20170315: put it outside this function to improve performance. No reason to get from table every time.  
//		List<ProgramMaster> pmList1 = programMasterDao.getByHql(
//				"from ProgramMaster where status='ACT' and (parentId is null or parentId='') order by sortSeq asc");
//		for (ProgramMaster pm : pmList1) {
		for (ProgramMaster pm : progmasterList) {
			if (pm.getProgramType() == "H")
				continue;

			ProgramDto tmpProgram = new ProgramDto();
			tmpProgram.setProgramId(pm.getProgramId());
			
			if (!rpList.contains(tmpProgram))
				continue;

			MenuDto menu = new MenuDto();
			menu.setSeq(pm.getSortSeq());
			menu.setAccessRight(rpList.get(rpList.indexOf(tmpProgram)).getAccessRight());
			menu.setIcon(pm.getIcon() == null ? "" : pm.getIcon());
			menu.setIndex(pm.getProgramId() == null ? "" : pm.getProgramId());
			menu.setLink(pm.getUrlPath() == null ? "" : pm.getUrlPath());
			menu.setName(pm.getMenuName() == null ? "" : pm.getMenuName());
			menu.setType(pm.getProgramType() == null ? "" : pm.getProgramType());
			menu.setSubmenu(getSubmenu(menu, rpList));
			menuList.add(menu);
		}
		//Collections.sort(menuList); SAM comment this line on 20170315 to improve performance. This sort function seem useless for sorting unknown column   
		return menuList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<MenuDto> getSubmenu(MenuDto menu, List<ProgramDto> rpList) {
		List<MenuDto> subMenuList = new ArrayList<MenuDto>();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(menu.getIndex());
		List<ProgramMaster> subLs = programMasterDao
				.getByHql("from ProgramMaster where status='ACT' and parentId=? order by sortSeq asc", param);
		if (subLs != null && subLs.size() > 0) {
			for (ProgramMaster pm : subLs) {
				ProgramDto tmpProgram = new ProgramDto();
				tmpProgram.setProgramId(pm.getProgramId());
				if (!rpList.contains(tmpProgram))
					continue;

				MenuDto m = new MenuDto();
				m.setSeq(pm.getSortSeq());
				m.setAccessRight(rpList.get(rpList.indexOf(tmpProgram)).getAccessRight());
				m.setIcon(pm.getIcon() == null ? "" : pm.getIcon());
				m.setIndex(pm.getProgramId() == null ? "" : pm.getProgramId());
				m.setLink(pm.getUrlPath() == null ? "" : pm.getUrlPath());
				m.setName(pm.getMenuName() == null ? "" : pm.getMenuName());
				m.setType(pm.getProgramType() == null ? "" : pm.getProgramType());
				m.setSubmenu(getSubmenu(m, rpList));
				subMenuList.add(m);
			}
		}
		Collections.sort(subMenuList);
		return subMenuList;
	}

	@Transactional
	@Override
	public List<ProgramMasterDto> getSalesReportCategories(List<Long> userRoleIds) {

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(Constant.PROGRAM_SALES_REPORT_OVERVIEW);
		param.add(Constant.PROGRAM_TYPE_MENU);
		List<ProgramMaster> categories = programMasterDao.getByHql(
				"from ProgramMaster where status='ACT' and parentId=? and programType=? order by sortSeq asc", param);

		List<ProgramMaster> catItems = null;
		List<ProgramMasterDto> retList = new ArrayList<ProgramMasterDto>();
		ProgramMasterDto catDto = null;
		// more role show repetition deal with delete repetition  use distinct 
		StringBuffer hql = new StringBuffer("select DISTINCT p from ProgramMaster p, RoleProgram rp where ");
		hql.append("p.programId = rp.programMaster.programId ");
		hql.append("and rp.roleMaster.roleId in (");
		for (int i = 0; i < userRoleIds.size(); i++) {
			hql.append("?");
			if (i < userRoleIds.size() - 1) {
				hql.append(",");
			}
		}
		hql.append(") ");
		hql.append("and p.status='ACT' ");
		hql.append("and p.parentId=? ");
		hql.append("and p.programType=? ");
		hql.append("order by p.sortSeq asc");
		String queryCategoryItemHql = hql.toString();

		for (ProgramMaster cat : categories) {
			catDto = buildProgramDto(cat);
			catDto.setChildren(new ArrayList<ProgramMasterDto>());

			param = new ArrayList<Serializable>();
			param.addAll(userRoleIds);
			param.add(cat.getProgramId());
			param.add(Constant.PROGRAM_TYPE_REPORT);

			catItems = programMasterDao.getByHql(queryCategoryItemHql, param);
			for (ProgramMaster item : catItems) {
				catDto.getChildren().add(buildProgramDto(item));
			}
			retList.add(catDto);
		}
		return retList;
	}

	private ProgramMasterDto buildProgramDto(ProgramMaster entity) {
		ProgramMasterDto dto = new ProgramMasterDto();
		dto.setProgramId(entity.getProgramId());
		dto.setProgramType(entity.getProgramType());
		dto.setMenuName(entity.getMenuName());
		dto.setUrlPath(entity.getUrlPath());
		return dto;
	}

}
