package com.sinodynamic.hkgta.security;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.util.exception.GTACommonException;

public class GtaAuthenticationProvider implements AuthenticationProvider
{

	@Autowired
	@Resource(name="authUserDetailService")
	private AuthUserDetailService userDetailsService;
	
	

	@Override
	@Transactional
	public Authentication authenticate(Authentication authentication) throws AuthenticationException
	{
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();

		if (authentication instanceof GtaAuthenticationToken)
		{
			UserDetails user = null;

			try
			{
				user = userDetailsService.loadUserByUsername(username);
			}
			catch (GTACommonException e)
			{
				throw e;
			}
			if (user == null)
			{
				throw new BadCredentialsException("Username not found.");
			}

			if (!password.equals(user.getPassword()))
			{
				throw new BadCredentialsException("Wrong password.");
			}

			Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
			Authentication auth = new GtaAuthenticationToken(user, password, authorities);
			return auth;
		}

		return authentication;
	}

	@Override
	public boolean supports(Class<? extends Object> authentication)
	{
		boolean yes = (GtaAuthenticationToken.class.isAssignableFrom(authentication));
		return yes;
	}

}
