package com.sinodynamic.hkgta.security;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class GtaAuthenticationToken extends
		UsernamePasswordAuthenticationToken {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5030789506144885956L;
	
	private String roleType;
	private String gtaToken;

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public GtaAuthenticationToken(Object principal,
			Object credentials) {
		super(principal, credentials);
		this.setRoleType(roleType);
		// TODO Auto-generated constructor stub
	}
	
//	public GtaAuthenticationToken(Object principal,
//			Object credentials, String roleType) {
//		super(principal, credentials);
//		this.setRoleType(roleType);
//	}
	
	public GtaAuthenticationToken(Object principal,
			Object credentials,
			Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
//		this.setRoleType(roleType);
	}


}
