/**
 * Project Name:gta-security
 * File Name:UserMasterService.java
 * Package Name:com.sinodynamic.hkgta.security.service
 * Create Date:Jul 10, 2015 6:45:21 PM
 *
*/

package com.sinodynamic.hkgta.security.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CorporateMemberDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.LoginHistoryDao;
import com.sinodynamic.hkgta.dao.crm.LoginSessionDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.StaffDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.sys.RoleMasterDao;
import com.sinodynamic.hkgta.dto.crm.CorporateServiceAccDto;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.UserType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

/**
 * ClassName:UserMasterService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Create Date:     Jul 10, 2015 6:45:21 PM <br/>
 * @author   Nick_Xiong
 * @version  	 
 */
@Service
@Transactional
public class UserMasterService{
    @Autowired
    private UserMasterDao userMasterDao;

    @Autowired
    private LoginSessionDao loginSessionDao;

    @Autowired
    private LoginHistoryDao loginHistoryDao;
    
    @Autowired
    private StaffProfileDao staffProfileDao;

    @Autowired
    private StaffDao staffDao;
    
    @Autowired
    private MemberDao memberDao;
    
    @Autowired
    private RoleMasterDao roleMasterDao;
    
    @Autowired
    private CustomerProfileDao customerProfileDao;
    
    @Autowired
    private CustomerServiceDao customerServiceDao;
    
    @Autowired
    private CorporateServiceAccDao corporateServiceAccDao;
    
    @Autowired
    private CorporateMemberDao corporateMemberDao;
    
    
	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException, DataAccessException, GTACommonException
	{
		UserMaster userMaster = null;
		LoginUser userDetails = null;
		// try
		// {
		userMaster = (UserMaster) userMasterDao.getUserByUserId(userId);
		if (userMaster == null)
			throw new UsernameNotFoundException("User: '" + userId + "' not found...");

		if (!"ACT".equals(userMaster.getStatus()))
			throw new GTACommonException(GTAError.LoginError.USER_ACCOUNT_INACTIVE);

		List<GrantedAuthority> grantedAuthoritiesList = new ArrayList<GrantedAuthority>();

		if (StringUtils.isEmpty(userMaster.getUserId()))
			throw new UsernameNotFoundException("Member: '" + userId + "' not found...");

		List<RoleMaster> roleMasterList = roleMasterDao.getRoleListByUserId(userMaster.getUserId());

		for (RoleMaster r : roleMasterList)
		{
			grantedAuthoritiesList.add(new SimpleGrantedAuthority(r.getRoleName()));
			// grantedAuthoritiesList.add(new
			// SimpleGrantedAuthority(r.getForUserType()));
		}

		userDetails = new LoginUser(userMaster.getUserId(), userMaster.getPassword(), grantedAuthoritiesList);
		userDetails.setRole(userMaster.getUserType().toString());
		userDetails.setUserId(userMaster.getUserId());
		userDetails.setDefaultModule(userMaster.getDefaultModule());
		userDetails.setUserType(userMaster.getUserType().toString());
		userDetails.setLoginId(userMaster.getLoginId());

		UserType type = UserType.valueOf(userMaster.getUserType().toString());

		switch (type)
		{
		case STAFF:
			userDetails.setUserName(userMaster.getNickname());
			StaffMaster staff = staffDao.getStaffMasterByUserId(userMaster.getUserId());
			
			if (null == staff)
			{
				userDetails.setUserNo(userMaster.getLoginId());
				userDetails.setStaffType("Temp Staff");
			}
			else
			{
				if (!"ACT".equals(staff.getStatus()))
					throw new GTACommonException(GTAError.LoginError.USER_ACCOUNT_INACTIVE);

				userDetails.setUserNo(staff.getStaffNo());
				userDetails.setStaffType(staff.getPositionTitle());
				userDetails.setPositionTitle(staff.getStaffType());
			}
			
			StaffProfile staffprofile = staffProfileDao.getByUserId(userMaster.getUserId());
			if (staffprofile != null)
			{
				userDetails.setFirstName(staffprofile.getGivenName());
				userDetails.setLastName(staffprofile.getSurname());
				userDetails.setPortraitPhoto(staffprofile.getPortraitPhoto());
				userDetails.setFullname(staffprofile.getGivenName() + " " + staffprofile.getSurname());
				userDetails.setUserName(staffprofile.getGivenName() + " " + staffprofile.getSurname());
			}
			break;
		case CUSTOMER:
			Member member = memberDao.getMemberByUserId(userMaster.getUserId());
			checkEffective(member);
			
			CustomerProfile customerProfile = customerProfileDao.getById(member.getCustomerId());
			if (customerProfile != null)
			{
				userDetails.setUserName(customerProfile.getGivenName() + " " + customerProfile.getSurname());
				userDetails.setPortraitPhoto(customerProfile.getPortraitPhoto());
				userDetails.setDateOfBirth(customerProfile.getDateOfBirth());
			}
			if (member != null)
			{
				userDetails.setUserNo(member.getAcademyNo());
				userDetails.setMemberType(member.getMemberType());
			}
			break;
		case ADMIN:
			userDetails.setUserName(userMaster.getNickname());
			userDetails.setUserNo(userMaster.getLoginId());
		default:
			break;

		}
		// }
		// catch (Exception e)
		// {
		// e.printStackTrace();
		// }
		return userDetails;
	}
	
	private void checkEffective(Member member)
	{
		if (!"ACT".equals(member.getStatus()))
			throw new GTACommonException(GTAError.LoginError.USER_ACCOUNT_INACTIVE);
		
		CustomerServiceAcc customerServiceAcc = null;
		CorporateServiceAccDto corporateServiceAcc = null;
		CorporateMember cm = null;
		Constant.memberType type = null;
		
		try
		{
			type = Constant.memberType.valueOf(member.getMemberType());
		}catch (IllegalArgumentException e)	{}
		
		if (null == type)
			throw new GTACommonException(GTAError.LoginError.USER_ACCOUNT_INACTIVE);
		
		switch (type)
		{
		case IPM:
		case IDM:
			customerServiceAcc = customerServiceDao.getCurrentActiveAcc(type.equals(Constant.memberType.IPM) ? member.getCustomerId() : member.getSuperiorMemberId());
			if (null == customerServiceAcc)
			{
				throw new GTACommonException(GTAError.LoginError.USER_ACCOUNT_INACTIVE);
			}
			break;
		case CPM:
		case CDM:
			
			customerServiceAcc = customerServiceDao.getCurrentActiveAcc(type.equals(Constant.memberType.CPM) ? member.getCustomerId() : member.getSuperiorMemberId());
			if (null == customerServiceAcc)
			{
				throw new GTACommonException(GTAError.LoginError.USER_ACCOUNT_INACTIVE);
			}
			break;
			
//			cm = corporateMemberDao.getCorporateMemberById(type.equals(Constant.memberType.CPM) ? member.getCustomerId() : member.getSuperiorMemberId());
//			corporateServiceAcc = corporateServiceAccDao.getAactiveByCustomerId(cm.getCorporateProfile().getCorporateId());
//			Date begin = corporateServiceAcc.getEffectiveDate();
//			Date end = corporateServiceAcc.getExpiryDate();
//			if(begin.after(new Date()) || end.before(new Date()))
//			{
//				throw new GTACommonException(GTAError.LoginError.USER_ACCOUNT_INACTIVE);
//			}
//			break;
		default:
			break;
		}
		
	}
	public StaffDto getStaffDto(String staffId){
		return  userMasterDao.getStaffDto(staffId);
	}
}

