package com.sinodynamic.hkgta.service.pms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.crm.UserDeviceDao;
import com.sinodynamic.hkgta.dao.crm.setting.UserPreferenceSettingDao;
import com.sinodynamic.hkgta.dao.pms.RoomCrewDao;
import com.sinodynamic.hkgta.dao.pms.RoomDao;
import com.sinodynamic.hkgta.dao.pms.RoomHousekeepTaskDao;
import com.sinodynamic.hkgta.dao.pms.RoomPreassignSyncDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.dao.sys.UserRoleDao;
import com.sinodynamic.hkgta.dto.crm.IsCanCheckInDto;
import com.sinodynamic.hkgta.dto.pms.HousekeepTaskFileDto;
import com.sinodynamic.hkgta.dto.pms.RoomCrewDto;
import com.sinodynamic.hkgta.dto.pms.RoomDetailDto;
import com.sinodynamic.hkgta.dto.pms.RoomDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskQueryDto;
import com.sinodynamic.hkgta.dto.pms.RoomMemberDto;
import com.sinodynamic.hkgta.dto.pms.RoomTaskDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.dto.push.RegisterDto;
import com.sinodynamic.hkgta.dto.push.RegisterResultDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSettingPK;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomCrew;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.entity.pms.RoomPreassignSync;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.integration.push.service.PushService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RoomFrontdeskStatus;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.RoomCrewRoleType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskStatus;
import com.sinodynamic.hkgta.util.constant.RoomServiceStatus;
import com.sinodynamic.hkgta.util.constant.RoomStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.MessageResult;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RoomServiceImpl extends ServiceBase<Room>implements RoomService {
	protected final Logger allotmentLog = Logger.getLogger(LoggerType.ALLOTMENTLOG.getName());
	
	@Autowired
	private RoomDao roomDao;

	@Autowired
	private RoomHousekeepTaskDao roomHousekeepTaskDao;

	@Autowired
	private RoomCrewDao roomCrewDao;

	@Autowired
	private SysCodeDao sysCodeDao;

	@Autowired
	private StaffMasterInfoDtoDao staffMasterDao;

	@Autowired
	private GlobalParameterService globalParameterService;

	@Autowired
	private PMSApiService pMSService;

	@Autowired
	private UserRoleDao userRoleDao;

	@Autowired
	private PushService pushService;

	@Autowired
	private UserDeviceDao userDeviceDao;

	@Autowired
	private RoomHousekeepTaskService roomHousekeepTaskService;

	// added by Kaster 20160317
	@Autowired
	private RoomReservationRecDao roomReservationRecDao;
	
	@Autowired
	private RoomPreassignSyncService roomPreassignSyncService;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	private CustomerProfileDao					customerProfileDao;

	@Autowired
	private MemberService				memberService;
	@Autowired
	private RoomPreassignSyncDao roomPreassignSyncDao;
	

	@Autowired
	private UserPreferenceSettingDao userPreferenceSettingDao;

	
	@Autowired
	private MemberDao memberDao;
	
	@Deprecated
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<RoomDto> getRoomList(String updateBy) throws Exception {
		List<RoomDto> rooms = new ArrayList<RoomDto>();
		List<Room> roomList = roomDao.getRoomList();
		if (null != roomList && roomList.size() > 0) {
			RoomDto roomDto = null;
			for (Room room : roomList) {
				roomDto = new RoomDto();
				roomDto.setRoomId(room.getRoomId());
				roomDto.setRoomNo(room.getRoomNo());
				roomDto.setStatus(room.getStatus());
				roomDto.setFrontdeskStatus(room.getFrontdeskStatus());
				List<RoomHousekeepTask> roomHousekeepTasks = roomHousekeepTaskDao
						.getRoomHousekeepTaskList(room.getRoomId());
				setRoomHousekeepTaskDto(roomDto, roomHousekeepTasks);
				if (roomDto.getRoomId() > 0)
					rooms.add(roomDto);
				if (roomDto.getNoResponse() == null)
					roomDto.setNoResponse(false);
			}
		}
		return rooms;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<RoomDto> getRoomListWithTaskCounts(String updateBy) throws Exception {

		List<RoomDto> dtos = roomDao.getRoomListWithTaskCounts();

		GlobalParameter assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
		int raRespTime = assistant != null ? Integer.parseInt(assistant.getParamValue()) : 0;

		List<Long> noRespRoomIds = roomDao.getNoResponseRoomIdx(raRespTime, new Date());

		for (RoomDto dto : dtos) {
			if (noRespRoomIds.indexOf(dto.getRoomId()) > -1) {
				dto.setNoResponse(Boolean.TRUE);
			} else {
				dto.setNoResponse(Boolean.FALSE);
			}

			// 检查该房间当天是否被预订而且客人是否当天到达
			// modified by Kaster 20160411 客户的不合理要求，导致一间客房在同一天可以被多个不同的客户预订。
			List<RoomReservationRec> recList = roomReservationRecDao.getByCols(RoomReservationRec.class,
					new String[] { "roomId", "arrivalDate" }, new Serializable[] { dto.getRoomId(), new Date() }, null);
			boolean notCheckOut = false;
			if (recList != null && recList.size() > 0) {
				for (int i = 0; i < recList.size(); i++) {
					if (recList.get(i).getCheckoutTimestamp() == null) {
						notCheckOut = true;
						break;
					}
				}
			}
			// RoomReservationRec rec =
			// roomReservationRecDao.getUniqueByCols(RoomReservationRec.class,
			// new String[]{"roomId","arrivalDate"}, new
			// Serializable[]{dto.getRoomId(),new Date()});
			// modified by Kaster 20160405 加多一个检查条件：还没check
			// out，判断方法是room_reservation_rec表的checkout_timestamp字段为null。
			if (notCheckOut) {
				if (dto.getStatus().equalsIgnoreCase(RoomStatus.VC.name())) {
					dto.setStatus("VCA");
				} else if (dto.getStatus().equalsIgnoreCase(RoomStatus.D.name())) {
					dto.setStatus("VDA");
				} else if (dto.getStatus().equalsIgnoreCase(RoomStatus.R.name())) {
					dto.setStatus("VRA");
				}
			}

			// 修改前端显示的 FD status
			if (dto.getServiceStatus().equalsIgnoreCase("OOS")) {
				dto.setServiceStatus("OOS");
			}
			// added by Kaster 20160318 根据HK Status、FD Status以及房间预订的入住时间修改前端显示的
			// HK status。
			// 1.Support to show housekeeping status
			// (VC,OC,D,R,VCA,VDA,VRA,Skip,Sleep)
			// 2.Support to display front deck status (V,O,OOO,OOS)
			// modified by Kaster 20160331 将Skip和Sleep状态显示在FD
			// Status中，若OOS同时满足，则优先显示Skip和Sleep.
			if (dto.getFrontdeskStatus().equalsIgnoreCase("O")
					&& dto.getStatus().equalsIgnoreCase(RoomStatus.VC.name())) {
				dto.setCalculatedStatus("Skip");
			}
			if (dto.getFrontdeskStatus().equalsIgnoreCase("V")
					&& dto.getStatus().equalsIgnoreCase(RoomStatus.OC.name())) {
				dto.setCalculatedStatus("Sleep");
			}
		}
		return dtos;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public RoomDetailDto getRoomDetail(Long roomId) {
		RoomDetailDto roomDetail = new RoomDetailDto();
		Room room = roomDao.get(Room.class, roomId);
		if (null != room) {
			roomDetail.setRoomId(room.getRoomId());
			roomDetail.setRoomNo(room.getRoomNo());
			roomDetail.setStatus(room.getStatus());
			roomDetail.setFrontdeskStatus(room.getFrontdeskStatus());
			roomDetail.setServiceStatus(room.getServiceStatus());
			roomDetail.setCalculatedStatus("");

			// 检查该房间当天是否被预订而且客人是否当天到达
			// modified by Kaster 20160411 客户的不合理要求，导致一间客房在同一天可以被多个不同的客户预订。
			List<RoomReservationRec> recList = roomReservationRecDao.getByCols(RoomReservationRec.class,
					new String[] { "roomId", "arrivalDate" }, new Serializable[] { roomId, new Date() }, null);
			boolean notCheckOut = false;
			if (recList != null && recList.size() > 0) {
				for (int i = 0; i < recList.size(); i++) {
					if (recList.get(i).getCheckoutTimestamp() == null) {
						notCheckOut = true;
						break;
					}
				}
			}
			// RoomReservationRec rec =
			// roomReservationRecDao.getUniqueByCols(RoomReservationRec.class,
			// new String[]{"roomId","arrivalDate"}, new
			// Serializable[]{roomId,new Date()});
			// modified by Kaster 20160407 加多一个检查条件：还没check
			// out，判断方法是room_reservation_rec表的checkout_timestamp字段为null。
			if (notCheckOut) {
				if (room.getStatus().equalsIgnoreCase(RoomStatus.VC.name())) {
					roomDetail.setStatus("VCA");
				} else if (room.getStatus().equalsIgnoreCase(RoomStatus.D.name())) {
					roomDetail.setStatus("VDA");
				} else if (room.getStatus().equalsIgnoreCase(RoomStatus.R.name())) {
					roomDetail.setStatus("VRA");
				}
			}

			// 修改前端显示的 FD status
			if (room.getServiceStatus() != null && room.getServiceStatus().equalsIgnoreCase("OOS")) {
				roomDetail.setServiceStatus("OOS");
			}
			// added by Kaster 20160318 修改前端展示的状态
			// modified by Kaster 20160407 将Skip和Sleep状态显示在FD
			// Status中，若OOS同时满足，则优先显示Skip和Sleep.
			if (room.getFrontdeskStatus() != null && room.getFrontdeskStatus().equalsIgnoreCase("O")
					&& room.getStatus().equalsIgnoreCase(RoomStatus.VC.name())) {
				roomDetail.setCalculatedStatus("Skip");
			}
			if (room.getFrontdeskStatus() != null && room.getFrontdeskStatus().equalsIgnoreCase("V")
					&& room.getStatus().equalsIgnoreCase(RoomStatus.OC.name())) {
				roomDetail.setCalculatedStatus("Sleep");
			}

			List<RoomCrew> roomCrewList = roomCrewDao.getRoomCrewList(roomId);
			List<RoomCrewDto> roomCrewDtoList = null;
			List<RoomHousekeepTaskDto> roomHousekeepTaskDtoList = null;
			if (null != roomCrewList && roomCrewList.size() > 0) {
				roomCrewDtoList = new ArrayList<RoomCrewDto>();
				for (RoomCrew roomCrew : roomCrewList) {
					RoomCrewDto roomCrewDto = new RoomCrewDto();
					roomCrewDto.setUserId(roomCrew.getStaffProfile().getUserId());
					SysCode sysCode = sysCodeDao.getByCategoryAndCodeValue("HK-CREW-ROLE", roomCrew.getCrewRoleId());
					roomCrewDto.setTitle(null == sysCode ? "" : sysCode.getCodeDisplay());
					roomCrewDto.setUserName(
							roomCrew.getStaffProfile().getGivenName() + " " + roomCrew.getStaffProfile().getSurname());
					roomCrewDtoList.add(roomCrewDto);
				}
			}
			List<RoomHousekeepTask> roomHousekeepTasks = roomHousekeepTaskDao.getRoomHousekeepTaskList(roomId);
			if (null != roomHousekeepTasks && roomHousekeepTasks.size() > 0) {
				roomHousekeepTaskDtoList = new ArrayList<RoomHousekeepTaskDto>();
				GlobalParameter assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
				Integer assistantResTime = assistant != null ? Integer.parseInt(assistant.getParamValue()) : 0;
				for (RoomHousekeepTask task : roomHousekeepTasks) {
					RoomHousekeepTaskDto taskDto = setRoomHousekeepTaskDetailDto(assistantResTime, task);
					roomHousekeepTaskDtoList.add(taskDto);
				}
			}
			roomDetail.setRoomCrewList(roomCrewDtoList);
			roomDetail.setTaskList(roomHousekeepTaskDtoList);

			// added by Kaster 20160407
			RoomDto dto = roomDao.getRoomWithTaskCountByRoomId(roomId);
			if (dto != null) {
				roomDetail.setAdHocCount(dto.getAdHocCount());
				roomDetail.setRoutineCount(dto.getRoutineCount());
				roomDetail.setMaintenanceCount(dto.getMaintenanceCount());
				roomDetail.setScheduleCount(dto.getScheduleCount());
			}
			/***
			 * deal with RoomPreassignSync  display status is Due In
			 * ROOM status is VC OR D OR R ,DISPLAY DUE IN
		
			if(room.getStatus().equals(RoomStatus.VC.name())||room.getStatus().equals(RoomStatus.D.name())||room.getStatus().equals(RoomStatus.R.name())){
				/***
				 *  roomId not null 
				 *  status no check in
				 *  arrivalDate =currentDate
				 */
		/*		RoomReservationRec rec=roomReservationRecDao.getUniqueByCols(RoomReservationRec.class, new String[] { "roomId", "arrivalDate" }, new Serializable[]{room.getRoomId(),new Date()});
				if(null!=rec){
					if(!rec.getStatus().equalsIgnoreCase(RoomReservationStatus.CKI.name()))
					{
						roomDetail.setStatus("Due In");
					}
				}
				
				if(this.showDueInByRoomNo(room.getRoomNo())){
					roomDetail.setStatus("Due In");
				}
			}	
		*/
		}
		return roomDetail;
	}

	private RoomHousekeepTaskDto setRoomHousekeepTaskDetailDto(Integer assistantResTime, RoomHousekeepTask task) {
		RoomHousekeepTaskDto taskDto = new RoomHousekeepTaskDto();
		taskDto.setTaskId(task.getTaskId());
		taskDto.setJobType(task.getJobType());
		taskDto.setStatus(task.getStatus());
		taskDto.setBeginDate(task.getScheduleDatetime());
		taskDto.setEndDate(task.getTargetCompleteDatetime());
		taskDto.setStartTimestamp(task.getStartTimestamp());
		taskDto.setFinishTimestamp(task.getFinishTimestamp());
		taskDto.setTaskDescription(task.getTaskDescription());
		taskDto.setRemark(task.getRemark());
		if (task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString())
				&& task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString())
				&& null == task.getStartTimestamp()) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(task.getScheduleDatetime());
			cal.add(Calendar.MINUTE, assistantResTime);
			taskDto.setExpireTime(cal.getTime());
			if (taskDto.getExpireTime().before(new Date()))
				taskDto.setNoResponse(true);
		} else if (task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString())
				&& task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && null == task.getStartTimestamp()
				&& null != task.getTargetCompleteDatetime()) {
			if (task.getTargetCompleteDatetime().before(new Date()))
				taskDto.setNoResponse(true);
		} else if (task.getJobType().equalsIgnoreCase(RoomHousekeepTaskJobType.RUT.toString())
				&& taskDto.getStatus().equalsIgnoreCase(RoomHousekeepTaskStatus.DND.toString())) {
			taskDto.setDndTimestamp(task.getStatusDate());
		} else
			taskDto.setNoResponse(false);
		setHousekeepTaskFileList(task, taskDto);
		return taskDto;
	}

	private void setHousekeepTaskFileList(RoomHousekeepTask task, RoomHousekeepTaskDto taskDto) {
		List<HousekeepTaskFile> taskFiles = task.getHousekeepTaskFiles();
		List<HousekeepTaskFileDto> taskFileList = null;
		if (null != taskFiles && taskFiles.size() > 0) {
			taskFileList = new ArrayList<HousekeepTaskFileDto>();
			for (HousekeepTaskFile taskFile : taskFiles) {
				if (!StringUtils.isEmpty(taskFile.getServerFile())) {
					HousekeepTaskFileDto taskFileDto = new HousekeepTaskFileDto();
					taskFileDto.setFileId(taskFile.getFileId());
					if ("AAC".equalsIgnoreCase(getFileType(taskFile.getServerFile()))) {
						taskFileDto.setServerFile("/room/task/get_aacfile?fileId=" + taskFile.getFileId());
					} else if ("AMR".equalsIgnoreCase(getFileType(taskFile.getServerFile()))) {
						taskFileDto.setServerFile("/room/task/get_amrfile?fileId=" + taskFile.getFileId());
					} else {
						taskFileDto.setServerFile("/room/task/get_thumbnail?fileId=" + taskFile.getFileId());
					}
					taskFileDto.setFileType(getFileType(taskFile.getServerFile()));
					taskFileList.add(taskFileDto);
				}
			}
			taskDto.setTaskFileList(taskFileList);
		}
	}

	public String getFileType(String fileUri) {
		String fileType = "";
		if (fileUri.lastIndexOf(".") > 0) {
			fileType = fileUri.substring(fileUri.lastIndexOf(".") + 1, fileUri.length()).toUpperCase();
		}
		return fileType;
	}

	/***
	 * Housekeeping status change from HKGTA including the both OOO and OOS which is special status NOT belong to HouseKeeping or FrontDesk
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ResponseResult changeRoomStatus(Long roomId, String status, String userId) throws Exception {
		Room room = roomDao.get(Room.class, roomId);
		if (null != room) {
			//Update FrontDesk status, need to update the HouseKeeping status
			if (RoomFrontdeskStatus.V.name().equals(room.getFrontdeskStatus())) {
				// after change OOO AND the HK is (VC/D/R) then change HK status is D
				if (status.equalsIgnoreCase(RoomFrontdeskStatus.OOO.name()) && (RoomStatus.VC.name().equals(room.getStatus())
						|| RoomStatus.D.name().equals(room.getStatus())
						|| RoomStatus.R.name().equals(room.getStatus()))) {
					// TODO SGG-2447 [BE] Modify the change room API to release the OOO/OOS if staff change the room status from OOO/OOS to others
					MessageResult messageResult = changeHKStatus(room, roomId, RoomStatus.OOO.name(), userId);
					if (null != messageResult && messageResult.isSuccess()) {
						changeFDStatus(room, roomId, status, userId);
					} 

				} // after change OOS not need change housekeeping status FD  V to OOS
				else if (status.equalsIgnoreCase(RoomServiceStatus.OOS.name())) {
					// TODO SGG-2447 [BE] Modify the change room API to release the OOO/OOS if staff change the room status from OOO/OOS to others
					MessageResult messageResult = changeHKStatus(room, roomId, RoomStatus.OOS.name(), userId);
					if (null != messageResult && messageResult.isSuccess()) {
						changeFDStatus(room, roomId, status, userId);
					} 
				}	
//				 when FD status is V and serviceStatus is OOS , change V ,only change FD status is V and serviceStatus is null  (V+OOS to V)
				else if (RoomServiceStatus.OOS.name().equals(room.getServiceStatus()) && status.equals(RoomFrontdeskStatus.V.name())) {
					changeFDStatus(room, roomId, status, userId);
				} 
//				 TODO SGG-2447 [BE] Modify the change room API to release the OOO/OOS if staff change the room status from OOO/OOS to others
				else {
					changeHKStatus(room, roomId, status, userId);
				}
			}else if (RoomFrontdeskStatus.O.name().equals(room.getFrontdeskStatus())) {
				// when FD status is O change after is OOS ,only change FD status is O+OOS
				if(status.equalsIgnoreCase(RoomServiceStatus.OOS.name()))
				{
					// TODO SGG-2447 [BE] Modify the change room API to release the OOO/OOS if staff change the room status from OOO/OOS to others
					MessageResult messageResult = changeHKStatus(room, roomId, RoomStatus.OOS.name(), userId);
					if (null != messageResult && messageResult.isSuccess()) {
						changeFDStatus(room, roomId, status, userId);
					} 
				}//O+OOS to O
				else if (RoomServiceStatus.OOS.name().equals(room.getServiceStatus()) && status.equals(RoomFrontdeskStatus.O.name())) {
					changeFDStatus(room, roomId, status, userId);
				}
				// TODO SGG-2447 [BE] Modify the change room API to release the OOO/OOS if staff change the room status from OOO/OOS to others
				else{
					 changeHKStatus(room, roomId, RoomStatus.D.name(), userId);
				}
			}else if (RoomFrontdeskStatus.OOO.name().equals(room.getFrontdeskStatus())) {
				// when FD status is OOO change after is V AND the HK is (VC/D/R) then change HK status is D
				if (status.equals(RoomFrontdeskStatus.V.name()) && (RoomStatus.VC.name().equals(room.getStatus()) || RoomStatus.D.name().equals(room.getStatus())
						|| RoomStatus.R.name().equals(room.getStatus()))) {
					// TODO SGG-2447 [BE] Modify the change room API to release the OOO/OOS if staff change the room status from OOO/OOS to others
					MessageResult messageResult = changeHKStatus(room, roomId, RoomStatus.D.name(), userId);
					if (null != messageResult && messageResult.isSuccess()) {
						changeFDStatus(room, roomId, status, userId);
					} 
				}
			}else {
				//Housekeeping status update should not update Front Desk Status
//				 TODO SGG-2447 [BE] Modify the change room API to release the OOO/OOS if staff change the room status from OOO/OOS to others
				changeHKStatus(room, roomId, status, userId);
			}
		}
		return responseResult;
	}

	/**
	 * Update HouseKeeping Status
	 */
	public MessageResult changeHKStatus(Room room, Long roomId, String status, String userId) {
		// update oasis room status
		UpdateRoomStatusDto statusDto = new UpdateRoomStatusDto();
		statusDto.setRoomNo(room.getRoomNo());
		statusDto.setUserId(userId);
		for (RoomStatus roomStatus : RoomStatus.values()) {
			if (roomStatus.name().equals(status)) {
				statusDto.setStatus(roomStatus.getDesc());
				break;
			}
		}
		// OOO and OOS need also update to OASIS
		if(RoomStatus.OOO.name().equals(status) || RoomStatus.OOS.name().equals(status)){
			statusDto.setReasonCode("OOTH");
		}
		List<UserRoleDto> userRoles = userRoleDao.getUserRoleListByUserId(userId);
		if (null != userRoles && userRoles.size() > 0)
			statusDto.setRoleName(userRoles.get(0).getRoleName());
		MessageResult messageResult = null;
		try {
			//  TODO SGG-2447 [BE] Modify the change room API to release the OOO/OOS if staff change the room status from OOO/OOS to others
			if (!RoomStatus.OOO.name().equals(status) && !RoomStatus.OOS.name().equals(status)) {
				messageResult = pMSService.updateRoomStatus(statusDto);
			} else { // 如果为OOO或者OOS状态时不同步第三方系统
				messageResult = new MessageResult();
				messageResult.setSuccess(true);
			}
			
		} catch (Exception e) {
			logger.error(GTAError.HouseKeepError.FAIL_GET_ROOM_LIST, e);
		}
		if (null != messageResult && messageResult.isSuccess()) {
			if(RoomStatus.OOO.name().equals(status)){
				room.setStatus(RoomStatus.D.name());
			}else if(!RoomStatus.OOS.name().equals(status)){
				room.setStatus(status);
			}
			room.setUpdateBy(userId);
			room.setUpdateDate(new Date());
			roomDao.update(room);

			// SGG-1645
			// 1 House keep status change to D . Front Desk status will display
			// V
			// 2 House keep status change to ' OC' Front Desk status will change
			// to O
			// 3 House keep status change to R Front Desk status will display V
			// comment it because Housekeeping Status update shouldn't update
			// Front desk status(SGG-1945)
			/*
			 * if(status.equalsIgnoreCase(RoomStatus.D.name())){
			 * changeFDStatus(room, roomId, "V", userId); }else
			 * if(status.equalsIgnoreCase(RoomStatus.OC.name())){
			 * changeFDStatus(room, roomId, "O", userId); }else
			 * if(status.equalsIgnoreCase(RoomStatus.R.name())){
			 * changeFDStatus(room, roomId, "V", userId); }
			 */

			RoomDetailDto roomDetailDto = getRoomDetail(roomId);

			roomHousekeepTaskService.autoRemindRoomAssistant(Constant.TEMPLATE_ID_STATUS_CHANGED,
					RoomCrewRoleType.RA.name(), room, null);
			responseResult.initResult(GTAError.Success.SUCCESS, roomDetailDto);

		} else {
			responseResult.initResult(GTAError.HouseKeepError.CHANGE_STATUS_FAIL);
		}

		return messageResult;
	}

	/**
	 * added by Kaster 20160429 修改 Front Desk Status
	 */
	public void changeFDStatus(Room room, Long roomId, String status, String userId) {
		if (status.equalsIgnoreCase("OOS") ) {
			room.setServiceStatus(status);
		} else {
			room.setFrontdeskStatus(status);
			room.setServiceStatus(null);
		}
		room.setUpdateBy(userId);
		room.setUpdateDate(new Date());
		roomDao.update(room);

		RoomDetailDto roomDetailDto = getRoomDetail(roomId);
		responseResult.initResult(GTAError.Success.SUCCESS, roomDetailDto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Room> getRoomListByStatus(String[] status) throws Exception {
		return roomDao.getRoomListByStatus(status);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Room> getRoomListOutOfStatus(String[] status) throws Exception {
		return roomDao.getRoomListOutOfStatus(status);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateRoom(Room room) {
		roomDao.update(room);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<RoomDto> getRoomCrewList(String crewRoleId) {
		return roomDao.getRoomCrewList(crewRoleId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ResponseResult getRoomTaskList(String userId, Boolean isMyRoom, String roomNo) {
		try {
			StaffMaster staffMaster = staffMasterDao.getByUserId(userId);
			if (staffMaster == null) {
				responseResult.initResult(GTAError.HouseKeepError.NOT_FOUND_STAFF_TYPE);
				return responseResult;
			}
			List<RoomMemberDto> roomMemberDtos = isMyRoom
					? roomHousekeepTaskDao.getMyRoomListForApp(staffMaster.getUserId(), staffMaster.getStaffType(),
							roomNo)
					: roomHousekeepTaskDao.getRoomListForApp(staffMaster.getUserId(), staffMaster.getStaffType(),
							roomNo);
			if (null != roomMemberDtos && roomMemberDtos.size() > 0) {
				List<RoomHousekeepTaskQueryDto> queryTaskDtos = isMyRoom
						? roomHousekeepTaskDao.getMyRoomTaskListForApp(staffMaster.getUserId(),
								staffMaster.getStaffType(), roomNo)
						: roomHousekeepTaskDao.getRoomTaskListForApp(staffMaster.getUserId(),
								staffMaster.getStaffType(), roomNo);
				setRoomMemberDtoList(staffMaster, roomMemberDtos, queryTaskDtos);
			}
			
			/***
			 * setRoom Status VCA, VRA and VDA
			 */ 
			if(null!=roomMemberDtos){
				this.setRoomStatusVCA_VDA_VRA(roomMemberDtos);
			}
			
			responseResult.initResult(GTAError.Success.SUCCESS, roomMemberDtos);

		} catch (Exception e) {
			logger.error(GTAError.HouseKeepError.FAIL_GET_ROOM_LIST, e);
			System.out.println(e.getMessage());
			responseResult.initResult(GTAError.HouseKeepError.FAIL_GET_ROOM_LIST);
		}
		return responseResult;
	}

	private void setRoomStatusVCA_VDA_VRA(List<RoomMemberDto> roomMemberDtos){
		
		for (RoomMemberDto dto : roomMemberDtos) {
			if(RoomStatus.VC.name().equalsIgnoreCase(dto.getStatus())||
					RoomStatus.D.name().equalsIgnoreCase(dto.getStatus())
					||RoomStatus.R.name().equalsIgnoreCase(dto.getStatus())){
				RoomReservationRec rec=roomReservationRecDao.getUniqueByCols(RoomReservationRec.class, new String[] { "roomId", "arrivalDate" }, new Serializable[]{dto.getRoomId(),new Date()});
				if(null!=rec){
					if(!rec.getStatus().equalsIgnoreCase(RoomReservationStatus.CKI.name()))
					{
						if (dto.getStatus().equalsIgnoreCase(RoomStatus.VC.name())) {
							dto.setStatus("VCA");
						} else if (dto.getStatus().equalsIgnoreCase(RoomStatus.D.name())) {
							dto.setStatus("VDA");
						} else if (dto.getStatus().equalsIgnoreCase(RoomStatus.R.name())) {
							dto.setStatus("VRA");
						}
					}
				}
			}
		}
	}
	
	private void setRoomMemberDtoList(StaffMaster staffMaster, List<RoomMemberDto> roomMemberDtos,
			List<RoomHousekeepTaskQueryDto> queryTaskDtos) {
		Map<String, List<RoomTaskDto>> roomNoMap = new HashMap<String, List<RoomTaskDto>>();
		GlobalParameter assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
		GlobalParameter inspector = globalParameterService.getGlobalParameter("HK-MAXRESP-INSPECTOR");
		GlobalParameter supervisor = globalParameterService.getGlobalParameter("HK-MAXRESP-SUPERVISOR");
		Integer assistantResTime = assistant != null ? Integer.parseInt(assistant.getParamValue()) : 0;
		Integer inspectorResTime = inspector != null ? Integer.parseInt(inspector.getParamValue()) : 0;
		Integer supervisorResTime = supervisor != null ? Integer.parseInt(supervisor.getParamValue()) : 0;

		if (null != queryTaskDtos && queryTaskDtos.size() > 0) {
			for (RoomHousekeepTaskQueryDto taskDto : queryTaskDtos) {
				List<RoomTaskDto> roomTasks = roomNoMap.get(taskDto.getRoomNo());
				if (null == roomTasks) {
					roomTasks = new ArrayList<RoomTaskDto>();
					setRoomTaskDtoData(staffMaster, assistantResTime, inspectorResTime, supervisorResTime, taskDto,
							roomTasks);
					if (null != taskDto.getTaskId() && taskDto.getTaskId().longValue() > 0) {
						roomNoMap.put(taskDto.getRoomNo(), roomTasks);
					}
				} else {
					setRoomTaskDtoData(staffMaster, assistantResTime, inspectorResTime, supervisorResTime, taskDto,
							roomTasks);
				}
			}
			for (RoomMemberDto memberDto : roomMemberDtos) {
				memberDto.setTasks(roomNoMap.get(memberDto.getRoomNo()));
			}
		}

	}

	private void setRoomTaskDtoData(StaffMaster staffMaster, Integer assistantResTime, Integer inspectorResTime,
			Integer supervisorResTime, RoomHousekeepTaskQueryDto taskDto, List<RoomTaskDto> roomTasks) {
		if (null != taskDto.getTaskId() && taskDto.getTaskId().longValue() > 0) {
			RoomTaskDto toomTaskDto = buildRoomTaskDto(staffMaster, assistantResTime, inspectorResTime,
					supervisorResTime, taskDto);
			roomTasks.add(toomTaskDto);
		}
	}

	private RoomTaskDto buildRoomTaskDto(StaffMaster staffMaster, Integer assistantResTime, Integer inspectorResTime,
			Integer supervisorResTime, RoomHousekeepTaskQueryDto taskDto) {
		RoomTaskDto toomTaskDto = new RoomTaskDto();
		toomTaskDto.setJobType(taskDto.getJobType());
		toomTaskDto.setStatus(taskDto.getTaskStatus());
		toomTaskDto.setTaskId(taskDto.getTaskId());
		toomTaskDto.setStartTimestamp(taskDto.getStartTimestamp());
		toomTaskDto.setFinishTimestamp(taskDto.getFinishTimestamp());
		toomTaskDto.setNoResponse(getTaskNoResponse(taskDto, assistantResTime, inspectorResTime, supervisorResTime,
				staffMaster.getStaffType()));
		return toomTaskDto;
	}

	private void setRoomHousekeepTaskDto(RoomDto roomDto, List<RoomHousekeepTask> roomHousekeepTasks) {
		if (null != roomHousekeepTasks && roomHousekeepTasks.size() > 0) {
			GlobalParameter assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
			Integer assistantResTime = null != assistant ? Integer.parseInt(assistant.getParamValue()) : 0;
			for (RoomHousekeepTask task : roomHousekeepTasks) {
				if (task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString()))
					roomDto.setAdHocCount(null == roomDto.getAdHocCount() ? 0l : roomDto.getAdHocCount());
				if (task.getJobType().equals(RoomHousekeepTaskJobType.RUT.toString()))
					roomDto.setRoutineCount(null == roomDto.getRoutineCount() ? 0l : roomDto.getRoutineCount());
				if (task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString()))
					roomDto.setScheduleCount(null == roomDto.getScheduleCount() ? 0l : roomDto.getScheduleCount());
				if (task.getJobType().equals(RoomHousekeepTaskJobType.MTN.toString()))
					roomDto.setMaintenanceCount(
							null == roomDto.getMaintenanceCount() ? 0l : roomDto.getMaintenanceCount());

				if (task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString())
						&& task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString())
						&& null == task.getStartTimestamp()) {
					Calendar cal = Calendar.getInstance();
					cal.setTime(task.getScheduleDatetime());
					cal.add(Calendar.MINUTE, assistantResTime);
					if (cal.getTime().before(new Date()))
						roomDto.setNoResponse(true);
				} else if (task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString())
						&& task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString())
						&& null == task.getStartTimestamp() && null != task.getTargetCompleteDatetime()) {
					if (task.getTargetCompleteDatetime().before(new Date()))
						roomDto.setNoResponse(true);
				}
			}
		}
	}

	public boolean getTaskNoResponse(RoomHousekeepTaskQueryDto task, Integer assistantResTime, Integer inspectorResTime,
			Integer supervisorResTime, String roleType) {
		if (task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString())) {
			if (task.getTaskStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && task.getStartTimestamp() == null
					&& null != task.getEndDate() && task.getEndDate().before(new Date()))
				return true;
		} else if (task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString())) {
			if (task.getTaskStatus().equals(RoomHousekeepTaskStatus.OPN.toString())
					&& task.getStartTimestamp() == null) {
				if (roleType.equals(RoomCrewRoleType.SPV.name())) {
					Calendar calInspector = Calendar.getInstance();
					calInspector.setTime(task.getBeginDate());
					calInspector.add(Calendar.MINUTE, (assistantResTime + inspectorResTime));
					if (calInspector.getTime().before(new Date()))
						return true;
				} else if (roleType.equals(RoomCrewRoleType.MGR.name())) {
					Calendar calSpv = Calendar.getInstance();
					calSpv.setTime(task.getBeginDate());
					calSpv.add(Calendar.MINUTE, (assistantResTime + inspectorResTime + supervisorResTime));
					if (calSpv.getTime().before(new Date()))
						return true;
				} else {
					Calendar calAssistant = Calendar.getInstance();
					calAssistant.setTime(task.getBeginDate());
					calAssistant.add(Calendar.MINUTE, assistantResTime);
					if (calAssistant.getTime().before(new Date()))
						return true;

				}
			}
		}
		return false;
	}

	public boolean getTaskNoResponse(RoomHousekeepTask task, Integer assistantResTime, Integer inspectorResTime,
			Integer supervisorResTime, String roleType) {
		if (task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString())) {
			if (task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && task.getStartTimestamp() == null
					&& null != task.getTargetCompleteDatetime() && task.getTargetCompleteDatetime().before(new Date()))
				return true;
		} else if (task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString())) {
			if (task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && task.getStartTimestamp() == null) {
				if (roleType.equals(RoomCrewRoleType.ISP.name())) {
					Calendar calAssistant = Calendar.getInstance();
					calAssistant.setTime(task.getScheduleDatetime());
					calAssistant.add(Calendar.MINUTE, assistantResTime);
					if (calAssistant.getTime().before(new Date()))
						return true;
				} else if (roleType.equals(RoomCrewRoleType.SPV.name())) {
					Calendar calInspector = Calendar.getInstance();
					calInspector.setTime(task.getScheduleDatetime());
					calInspector.add(Calendar.MINUTE, (assistantResTime + inspectorResTime));
					if (calInspector.getTime().before(new Date()))
						return true;
				} else if (roleType.equals(RoomCrewRoleType.MGR.name())) {
					Calendar calSpv = Calendar.getInstance();
					calSpv.setTime(task.getScheduleDatetime());
					calSpv.add(Calendar.MINUTE, (assistantResTime + inspectorResTime + supervisorResTime));
					if (calSpv.getTime().before(new Date()))
						return true;
				}
			}
		}
		return false;
	}

	public static long getDiffMinutes(Date startDate, Date endDate) {
		long time1 = startDate.getTime();
		long time2 = endDate.getTime();
		long diff;
		if (time1 < time2)
			diff = time2 - time1;
		else
			diff = time1 - time2;
		long day = diff / (24 * 60 * 60 * 1000);
		long hour = (diff / (60 * 60 * 1000) - day * 24);
		long min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
		long sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		return day * 24 * 60 + hour * 60 + min + sec / 60;
	}

	@Override
	public MessageResult updateRoomStatus(UpdateRoomStatusDto dto) throws Exception {
		if (null != dto && !StringUtils.isEmpty(dto.getRoomNo()) && !StringUtils.isEmpty(dto.getStatus())) {
			return pMSService.updateRoomStatus(dto);
		} else
			throw new GTACommonException(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
	}

	@Override
	@Transactional
	public ResponseResult pushRegister(RegisterDto dto, String userId) {
		try {
			RegisterResultDto resultDto = pushService.register(dto);
			if (resultDto != null) {
				if ("success".equals(resultDto.getStatus().toLowerCase())) {
					String endpointArn = resultDto.getEndpointArn();
					UserDevice ud = new UserDevice();
					ud.setArnApplication("housekeeper");
					ud.setArnEndpoint(endpointArn);
					ud.setCreateBy(userId);
					Date date = new Date();
					ud.setCreateDate(date);
					ud.setDeviceLanguage(dto.getLang());
					ud.setDeviceToken(dto.getToken());
					ud.setPlatform(dto.getPlatform());
					ud.setStatus("ACT");
					ud.setSystemVersion(dto.getSystemVersion());
					ud.setUpdateBy(userId);
					ud.setUserId(userId);
					ud.setUpdateDate(date);
					userDeviceDao.saveOrUpdate(ud);
					responseResult.initResult(GTAError.Success.SUCCESS);
				} else {
					responseResult.initResult(GTAError.HouseKeepError.FAIL_PUSH_REG, resultDto.getErrorMessageEn());
				}
			} else {
				responseResult.initResult(GTAError.HouseKeepError.FAIL_PUSH_REG);
			}
		} catch (Exception e) {
			logger.error(GTAError.HouseKeepError.FAIL_PUSH_REG, e);
			responseResult.initResult(GTAError.HouseKeepError.FAIL_PUSH_REG);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public List<Room> getRoomList() {
		return roomDao.getRoomList();
	}

	@Transactional
	public List<Room> getLimitedInfoRoomList() {
		return roomDao.getLimitedInfoRoomList();
	}

	@Override
	@Transactional
	public void staffPushRegister(String staffId, String deviceArn, String token, String platform, String version,
			String updatedBy) {
		List<UserDevice> userDeviceList = userDeviceDao.getByCol(UserDevice.class, "userId", staffId, null);
		UserDevice ud;
		if (userDeviceList != null && userDeviceList.size() > 0) {
			ud = userDeviceList.get(0);
			setUserDevice(ud, staffId, deviceArn, token, platform, version, updatedBy, userDeviceList);
			userDeviceDao.update(ud);
		} else {
			ud = new UserDevice();
			setUserDevice(ud, staffId, deviceArn, token, platform, version, updatedBy, userDeviceList);
			userDeviceDao.saveOrUpdate(ud);
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
	}

	private void setUserDevice(UserDevice ud, String staffId, String deviceArn, String token, String platform,
			String version, String updatedBy, List<UserDevice> userDeviceList) {
		ud.setArnApplication("housekeeper");
		ud.setArnEndpoint(deviceArn);
		ud.setCreateBy(updatedBy);
		Date date = new Date();
		ud.setCreateDate(date);
		ud.setDeviceToken(token);
		ud.setPlatform(platform);
		ud.setStatus("ACT");
		ud.setSystemVersion(version);
		ud.setUpdateBy(updatedBy);
		ud.setUserId(staffId);
		ud.setUpdateDate(date);
	}

	@Override
	@Transactional
	public List<Room> getCheckInRoomList() {
		return roomDao.getCheckInRoomList();
	}

	@Override
	@Transactional
	public List<Room> getRoomListServiceStatus(String serviceStatus) {
		String hql = "from Room r where r.serviceStatus = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(serviceStatus);
		return roomDao.getByHql(hql, param);//(hql, param, Room.class);
	} 
	/**
	 * 是否可以checking
	 * @param customerId
	 * @param resvId
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult isCanCheckIn(Long customerId, Long resvId) {
		IsCanCheckInDto dto = new IsCanCheckInDto();
		dto.setIsCanCheckIn(false);
		if (globalParameterService.initGuestRoomSysConfig() && Constant.GUEST_ROOM_CHECK_IN_PUSH_MESSAGE.equals("Y") && roomReservationRecDao.todayToSeeIfThereBookingRoom(customerId, resvId) != null  && DateConvertUtil.compareTime(new Date(), Constant.GUEST_ROOM_PUSH_MESSAGE_TIME)) {
			Member member = memberDao.getMemberById(customerId);
			if(member == null && null != member.getUserId()) {
				responseResult.initResult(GTAError.Success.SUCCESS,dto);
				return responseResult;
			}
			UserPreferenceSettingPK pk = new UserPreferenceSettingPK();
			pk.setParamId(Constant.SILENTCHKIN_PUSHTIME_SEITCH);
			pk.setUserId(member.getUserId());
			UserPreferenceSetting setting =  userPreferenceSettingDao.get(UserPreferenceSetting.class, pk);
			if (setting != null) {
				dto.setIsCanCheckIn(setting.getParamValue().equals("Y") ? true : false);
			}
		} 
		responseResult.initResult(GTAError.Success.SUCCESS,dto);
		return responseResult;
	} 
	/**
	 * member checkInProgress
	 * @param customerId
	 * @param resvId
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult checkInProgress(Long customerId, Long resvId) {
		if (globalParameterService.initGuestRoomSysConfig() && Constant.GUEST_ROOM_CHECK_IN_PUSH_MESSAGE.equals("Y") && DateConvertUtil.compareTime(new Date(), Constant.GUEST_ROOM_PUSH_MESSAGE_TIME)) {
			RoomReservationRec re = roomReservationRecDao.todayToSeeIfThereBookingRoom(customerId, resvId);
			if (re == null) {
				responseResult.initResult(GTAError.FacilityError.RESVID_NOT_FOUND);
			} else {
				re.setStatus(Constant.RoomReservationStatus.CKP.name());
				re.setUpdateDate(new Date());
				roomReservationRecDao.update(re);
    			CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(customerId);
    			String[] staffLocation = new String[]{profile.getGivenName() + " " + profile.getSurname()};
    			devicePushService.pushMessage(Constant.TEMPLATE_ID_APP_GUEST_ROOM_SILENT_CHECK_IN_STAFF_NOTIFICATION, staffLocation, Constant.STAFFAPP_PUSH_APPLICATION);
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
			
		} else {
			responseResult.initResult(GTAError.FacilityError.CHECK_IN_OVERDUE);
		}
		return responseResult;
	} 
	
	/**
	 * 获取当前今天CheckInProgress状态的总数
	 * @return
	 */
	@Override
	@Transactional
	public int getSilentCheckInProgressTotalNum(){
		return roomReservationRecDao.getSilentCheckInProgressTotalNum();
	}
	  
	@Override
	@Transactional
	public void autoPushMsg4SilentCheckin() {
		try {
			List<RoomReservationRec> list =roomReservationRecDao.getSilentCheckInProgress();
			for(RoomReservationRec rec : list){
				String subject = "Your guestroom is ready for check-in!";
				String content = "Your guestroom is ready for check-in!";
				Member member = memberService.getMemberById(rec.getCustomerId());
				logger.info("RoomServiceImpl  autoPushMsg4SilentCheckin run start ...resvId:"+rec.getResvId());
				devicePushService.pushMessage(new String[]{member.getUserId()+""}, subject, content, "member");
				logger.info("RoomServiceImpl  autoPushMsg4SilentCheckin run end ...resvId:"+rec.getResvId());
				
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	@Transactional
	public Room getRoomByRoomNo(Long roomNo) {
		// TODO Auto-generated method stub
		return this.roomDao.getByRoomNo(String.valueOf(roomNo));
	}	
//	Check whether the csv file or not.
//	Empty check for the confirm #, ignore it if the confirm # is not existing in HKGTA.
//	Empty check for the room #, if the room is not exist in HKGTA. Alert the message.
//	Get the "room_id" value from table "room" according to room #
//	Update the "room_id" value of table "room_reservation_rec" according to the Confirm #.
//	Log the success or fail message one by one
	@Transactional
	public  ResponseResult importAllotment(String userId) throws Exception{
		File csvRoot = new File(FileUtil.getFilePath(Constant.IMPORT_FILE_PATH));
		File[] csvFiles = csvRoot.listFiles();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		Date theDay = calendar.getTime();
		String formatCurrentDate = DateConvertUtil.formatCurrentDate(theDay);
		List<Room> roomList = roomDao.getRoomList();
		BufferedReader br = null;
		ArrayList<String[]> csvList = new ArrayList<String[]>();
		ArrayList<String[]> csvRemoveList = new ArrayList<String[]>();
		String name=formatCurrentDate + "_" + Constant.ALLOTMENT_REPORT_NO;
		StringBuffer logImport=new StringBuffer();
		logger.info("start import:"+name);
		try {
			boolean isFoundCsv=false;
			for (File csvFile : csvFiles) {
				if (csvFile.getName().indexOf(name) >= 0 && !isFoundCsv) {
					isFoundCsv=true;
					logger.info("csvFile path" + csvFile.getAbsolutePath() + " file name:" + csvFile.getName());
					String line = "";
					String cvsSplitBy = "\\t";
					FileInputStream fis = new FileInputStream(csvFile);
					br = new BufferedReader(new InputStreamReader(fis, "UTF-16"));
					boolean isUpdate=true;
					boolean isValidDay=false; 
					int count=0;
					 br.readLine();
					while ((line = br.readLine()) != null) {			
							line=line.replaceAll("\"", "");
							logger.info(line);
							count=count+1;
							String[] ooArray = line.split(cvsSplitBy);
							if (StringUtils.isBlank(ooArray[0]) || StringUtils.isBlank(ooArray[11])) {
								logger.error("row :"+ count+ "  format or content error of csv file");
								isUpdate=false;
								break;
							}else{
								if(!isValidDay){
									String theDate=ooArray[11].substring(0, 10).replaceAll("-", "");
									if(formatCurrentDate.equals(theDate)){
										isValidDay=true;
									}else{
										logger.error("The day is not match!");
										isUpdate=false;
										break;
									}									
								}
								RoomReservationRec roomReservationRec =roomReservationRecDao.getReservationByConfirmId(ooArray[0]);
								if( roomReservationRec!=null){
									if (!StringUtils.isBlank(ooArray[1])){
										boolean isAdded=false;
										for (Room room : roomList) {
											String roomNo = room.getRoomNo();
											if (roomNo.equals(ooArray[1])) {
												csvList.add(ooArray);
												isAdded=true;
												break;
											} 
										}
										if(isAdded){
											logImport.append("row "+ count+ "  is fine to update; ConfirmId:"+ooArray[0]+"\n");
											
										}else{
											logImport.append("row "+ count+ ":  Room no  is not match; It will be ignored! ConfirmId:"+ooArray[0]+"\n");
										}
									}else{
										if(roomReservationRec.getRoomId()!=null){
											logImport.append("row "+ count+ "  is fine to update; The Room no will be removed! ConfirmId:"+ooArray[0]+"\n");
											csvRemoveList.add(ooArray);
										}else{
											logImport.append("row "+ count+ "  is same record! Not need to updated!"+"\n");
										}
									}
								}else{
									logImport.append("row "+ count+ "  is not fine to update; It will be ignored! ConfirmId:"+ooArray[0]+"\n");
								}
							}
						
					}
					logger.info("logImport:"+logImport.toString());
					if(isUpdate){
						if(csvList.size()>0 || csvRemoveList.size()>0){
							boolean isSucc=updateReservationRec(roomList,csvList,csvRemoveList,userId);
							if(isSucc){
								responseResult.initResult(GTAError.Success.SUCCESS,logImport.toString());
							}else{
								responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION,"Can not save to database");
							}
						}else{
							responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION,"Csv file is empty");
						}
					}else{
						responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION,"Format or content error of csv file");
					}
					break;
				}
			}
			
			if(!isFoundCsv){
				responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION,"Can not found csv file");
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw new GTACommonException(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION,e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.error(e.getMessage(),e);
				}
			}
		}
		return responseResult;
	}
	/***
	 * 
	 * Check whether the csv file or not.
	  Empty check for the confirm #, ignore it if the confirm # is not existing in HKGTA.
	  Empty check for the room #, if the room is not exist in HKGTA. Alert the message.	
	  Get the "room_id" value from table "room" according to room #
	  Update the "room_id" value of table "room_reservation_rec" according to the Confirm #.
	 * @param userId
	 * @param csvFile
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional
	public ResponseResult importAllotment(String userId, File csvFile) throws Exception {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		Date theDay = calendar.getTime();
		String formatCurrentDate = DateConvertUtil.formatCurrentDate(theDay);
		
		BufferedReader br = null;
		ArrayList<String[]> csvList = new ArrayList<String[]>();
		ArrayList<String[]> csvUpdateList = new ArrayList<String[]>();
		
		List<String[]> roomPreassignSyncList = new ArrayList<String[]>();
		
		try {
			allotmentLog.info("csvFile path" + csvFile.getAbsolutePath() + " file name:" + csvFile.getName());
			String line = "";
			String cvsSplitBy = "\\t";
			FileInputStream fis = new FileInputStream(csvFile);
			br = new BufferedReader(new InputStreamReader(fis, "UTF-16"));
			int count = 0;
			br.readLine();
			while ((line = br.readLine()) != null) {
				if(StringUtils.isEmpty(line))continue;
				allotmentLog.info(line.replace("	", ","));
				line = line.replaceAll("\"", "");
				count = count + 1;
				String[] ooArray = line.split(cvsSplitBy);

				if (StringUtils.isBlank(ooArray[0]) || StringUtils.isBlank(ooArray[11])) {
					allotmentLog.info(Log4jFormatUtil.logErrorMessage(null, null, null, null,
							"row :" + count + " handler result  content is null , csv[0]=" + ooArray[0] + " or csv[11]" + ooArray[11]));
				} else {
					String theDate = ooArray[11].substring(0, 10).replaceAll("-", "");
					if (formatCurrentDate.equals(theDate.trim())) {
						// ooArray[0] confirmId,ooArray[1] is roomNo
						RoomReservationRec roomReservationRec = roomReservationRecDao
								.getReservationByConfirmId(ooArray[0]);
						if (roomReservationRec != null) {
							if (!StringUtils.isBlank(ooArray[1])) {
								Room roomO=roomDao.getByRoomNo(ooArray[1]);
								/***
								 * check ooArray[1] exist HKGTA ,if exist ,update RoomReservationRec roomId
								 */
								if(null!=roomO){
									/***
									 * check roomId arrival_date is currentDate  exist HKGTA 
									 */
									RoomReservationRec rec=roomReservationRecDao.getUniqueByCols(RoomReservationRec.class, new String[] { "roomId", "arrivalDate" }, new Serializable[]{roomO.getRoomId(),new Date()});
									// is exist in HKGTA. no set roomNo
									if(null!=rec){
										allotmentLog.info("row " + count
												+ " hander result : the roomId :"+ooArray[1]+" is  exist the table room_reservation_rec's  don't  update to "+ooArray[1]+", ! the ConfirmId: is "
												+ ooArray[0] + "\n");
									}else{
										// is not exist in HKGTA. set roomNo room_reservation_rec's 
										Date arrivalDate=  DateConvertUtil.parseCurrentDate(ooArray[11].substring(0, 10));
										Date depDate=  DateConvertUtil.parseCurrentDate(ooArray[13].substring(0, 10));
										if(arrivalDate.compareTo(depDate)>=0){
											allotmentLog.info("row " + count
													+ " hander result :import arrival Date "+ooArray[11]+" is greater than departure date "+ooArray[13]+"");
										}else{
											csvList.add(ooArray);
											allotmentLog.info("row " + count
													+ " hander result  the table room_reservation_rec's room_no  will be update to "+ooArray[1]+", ! the ConfirmId: is "
													+ ooArray[0] + "\n");
										}
										
											
									}
								}else{
									allotmentLog.info("row " + count+
											"handler result the roomNo " + ooArray[1] + "  is not exist in HKGTA  room table .......");
								}
							} else {
								if (roomReservationRec.getRoomId() != null) {
									csvUpdateList.add(ooArray);
									allotmentLog.info("row " + count
											+ " hander result  the table room_reservation_rec's  RoomNo will be update to null ! ConfirmId: is "
											+ ooArray[0] + "\n");
								} 
							}
						} else {
							allotmentLog.info("row " + count + "handler result :ConfirmId "+ooArray[0] +" ,save RoomPreassignSync table in HKGTA ");
							/***
							 * add RoomPreassignSync table 
							 */
						    roomPreassignSyncList.add(ooArray);
						}

					} else {
						allotmentLog.info(Log4jFormatUtil.logErrorMessage(null, null, null, null, "row " + count + " handler result :line data ,The day" + theDate + " is not match to " + formatCurrentDate));
					}
				}
			}
			/***
			 * save roomPreassignSynList
			 */
			this.saveRoomPreassignSynList(roomPreassignSyncList);
			
			if (csvList.size() > 0 || csvUpdateList.size() > 0) {
				boolean isSucc = updateReservationRec(null, csvList, csvUpdateList, userId);
				if (isSucc) {
					responseResult.initResult(GTAError.Success.SUCCESS);
				} else {
					responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION, "Can not save to database");
					allotmentLog.info(Log4jFormatUtil.logErrorMessage(null, null, null, null, "Can not save to database"));
				}
			} else {
				responseResult.initResult(GTAError.Success.SUCCESS, "No valid data import");
				allotmentLog.info(Log4jFormatUtil.logErrorMessage(null, null, null, null, "No valid data import"));
			}
		} catch (Exception e) {
			allotmentLog.info(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
			throw new GTACommonException(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION, e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					allotmentLog.info(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
				}
			}
		}
		return responseResult;
	}

	private void saveRoomPreassignSynList(List<String[]> roomPreassignSyncList) {
		if (null != roomPreassignSyncList&&roomPreassignSyncList.size()>0) {
			/**
			 * clear history record
			 */
			roomPreassignSyncDao.removeAll();
			for (String[] rooms : roomPreassignSyncList) {
				if (null!=rooms[0]&&null != rooms[11] && null != rooms[13]) {
					Date arrivalDate = DateConvertUtil.parseString2Date(rooms[11].substring(0, 10),
							"yyyy-MM-dd");
					Date departureDate = DateConvertUtil
							.parseString2Date(rooms[13].substring(0, 10), "yyyy-MM-dd");
					RoomPreassignSync sync = new RoomPreassignSync();
					sync.setConfirmId(rooms[0]);
					sync.setRoomNo(rooms[1]);
					sync.setArrivalDatetime(arrivalDate);
					sync.setDepartureDatetime(departureDate);
					sync.setSyncTimestamp(new Date());
					roomPreassignSyncDao.saveOrUpdate(sync);
				}
			}

		}

	}
	private boolean updateReservationRec(List<Room> roomList,ArrayList<String[]>csvList,ArrayList<String[]>csvUpdateList,String userId) {	
			boolean isSucc=true;
			if(csvList.size()>0){
				for (String[] ooArray : csvList) {				
						RoomReservationRec roomReservationRec =roomReservationRecDao.getReservationByConfirmId(ooArray[0]);
						Room roomO=roomDao.getByRoomNo(ooArray[1]);
						if (null!=roomO) {
							roomReservationRec.setRoomId(roomO.getRoomId());	
							roomReservationRec.setUpdateDate(new Date());
							roomReservationRec.setUpdateBy(userId);
							roomReservationRecDao.update(roomReservationRec);
						}
				}
			}
			if(csvUpdateList.size()>0){
				for (String[] ooArray : csvUpdateList) {		
					RoomReservationRec roomReservationRec =roomReservationRecDao.getReservationByConfirmId(ooArray[0]);
					roomReservationRec.setRoomId(null);	
					roomReservationRec.setUpdateDate(new Date());
					roomReservationRec.setUpdateBy(userId);
					roomReservationRecDao.update(roomReservationRec);
				}
			}
			return isSucc;
		}

	@Override
	@Transactional
	public boolean  showDueInByRoomNo(String roomNo) {
		boolean blean=Boolean.FALSE;
		RoomPreassignSync sync = roomPreassignSyncService.getRoomPreassignSyncByRoomNo(roomNo);
		if (null != sync) {
			if(StringUtils.isNotEmpty(sync.getConfirmId())&&null!=sync.getArrivalDatetime())
			{
				Date currentDate=DateConvertUtil.parseString2Date(DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd"), "yyyy-MM-dd");
				if(currentDate.compareTo(sync.getArrivalDatetime())==0){
					blean=Boolean.TRUE;
				}
			}
		}
		return blean;
	}
	
	/**
	 * 对CheckInProgress状态的数据进行决策 ，accept批准/reject拒绝
	 * @param decision
	 * @param customerId
	 * @param resvId
	 * @return
	 * @throws Exception 
	 */
	@Override
	@Transactional
	public ResponseResult acceptOrRejectSilentCheckInProgress(String decision, Long customerId, Long resvId) throws Exception {
		RoomReservationRec re = roomReservationRecDao.todayToSeeIfThereBookingRoomByCheckinInProgress(customerId, resvId);
		//处理结果必须为 SA/SR 否则抛出错误
		if (re == null || (!Constant.RoomReservationStatus.SA.name().equals(decision) && !Constant.RoomReservationStatus.SR.name().equals(decision))) {
			responseResult.initResult(GTAError.FacilityError.RESVID_NOT_FOUND);
		} else {
			re.setCheckinMethod(decision);
			re.setUpdateDate(new Date());
			roomReservationRecDao.update(re);
			Member member = memberService.getMemberById(customerId);
			if (null == member) {
				responseResult.initResult(GTAError.FacilityError.RESVID_NOT_FOUND);
			}
			String[] userIds = new String[]{member.getUserId()};
			String functionId = "";
			String[] replaceParams = null;
			if (Constant.RoomReservationStatus.SA.name().equals(decision)) {
				functionId = Constant.TEMPLATE_ID_APP_GUEST_ROOM_NOTICE_MEMBER_SILENT_CHECKIN_DONE;
				replaceParams = new String[]{re.getRoomId() == null ? "null" : re.getRoomId().toString(), " GRM-" + resvId};
			} else {
				functionId = Constant.TEMPLATE_ID_APP_GUEST_ROOM_NOTICE_MEMBER_SILENT_CHECKIN_REJECTED;
				replaceParams = new String[]{" GRM-" + resvId};
			}
			
			devicePushService.pushMessage(userIds, functionId, replaceParams, Constant.MEMBERAPP_PUSH_APPLICATION);
			responseResult.initResult(GTAError.Success.SUCCESS);
		}
		return responseResult;
	} 
}
