package com.sinodynamic.hkgta.service.rpos;

import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface PosServiceItemPriceService extends IServiceBase<PosServiceItemPrice> {

	public PosServiceItemPrice getByItemNo(String itemNo);
}
