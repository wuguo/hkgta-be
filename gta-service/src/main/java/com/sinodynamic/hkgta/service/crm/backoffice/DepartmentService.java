package com.sinodynamic.hkgta.service.crm.backoffice;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.DepartmentBranch;
import com.sinodynamic.hkgta.entity.crm.PositionTitle;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface DepartmentService extends IServiceBase {
	
	List<DepartmentBranch> getAllDepartments() throws Exception;
	
	List<PositionTitle> getAllPositionTitle(Long departId) throws Exception;
}
