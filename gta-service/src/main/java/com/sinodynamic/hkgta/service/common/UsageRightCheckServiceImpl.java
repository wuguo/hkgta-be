package com.sinodynamic.hkgta.service.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.CorporateMemberDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CorporateServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class UsageRightCheckServiceImpl extends ServiceBase implements UsageRightCheckService {
	enum Permission{BA,AO,A};
	
	private Logger logger = Logger.getLogger(UsageRightCheckServiceImpl.class);
	@Autowired
	CustomerServiceDao customerServiceDao;
	
	@Autowired
	CustomerServiceSubscribeDao customerServiceSubscribeDao;
	
	@Autowired
	ServicePlanDao servicePlanDao;
	
	@Autowired
	MemberDao memberDao;
	
	@Autowired
	CorporateMemberDao corporateMemberDao;
	
	@Autowired
	CorporateServiceAccDao corporateServiceAccDao;
	
	@Autowired
	CorporateServiceSubscribeDao corporateServiceSubscribeDao;
	
	@Autowired
	ServicePlanFacilityDao servicePlanFacilityDao;
	
	@Autowired
	MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	@Autowired
	private CustomerServiceAccDao customerServiceAccDao;
	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;
	
	
	@Autowired
	MemberLimitRuleDao limitRuleDao;
	
	ServicePlan getServicePlan(Long customerId){
		try{
			Member member = memberDao.get(Member.class, customerId);
			String memberType = member.getMemberType();
			
			if(MemberType.CPM.name().equals(memberType) || MemberType.CDM.name().equals(memberType)){
				if(MemberType.CDM.name().equals(memberType)){
					customerId = member.getSuperiorMemberId();
				}
				CorporateMember corporateMember = corporateMemberDao.getCorporateMemberById(customerId);
				Long accNo = corporateServiceAccDao.getLatestAccNoForCorporate(corporateMember.getCorporateProfile().getCorporateId());
				CorporateServiceAcc corporateServiceAcc = corporateServiceAccDao.getByAccNo(accNo);
				CorporateServiceSubscribe corporateServiceSubscribe = corporateServiceSubscribeDao.getByAccountNo(corporateServiceAcc.getAccNo());
				ServicePlan servicePlan = servicePlanDao.get(ServicePlan.class, corporateServiceSubscribe.getId().getServicePlanNo());
				
				return servicePlan;
			}else{
				if(MemberType.IDM.name().equals(memberType)){
					customerId = member.getSuperiorMemberId();
				}
				
				CustomerServiceAcc customerServiceAcc = customerServiceDao.getCurrentActiveAcc(customerId);
				List<CustomerServiceSubscribe> customerServiceSubscribes = customerServiceSubscribeDao.getCustomerServiceSubscribeByAccNo(customerServiceAcc.getAccNo());
				ServicePlan servicePlan = servicePlanDao.get(ServicePlan.class, customerServiceSubscribes.get(0).getId().getServicePlanNo());
				return servicePlan;
			}
		}catch(Exception e){
			logger.error("getServicePlan() error", e);
			return null;
		}
	}
	
	@Override
	@Transactional
	public FacilityRight checkFacilityRight(Long customerId, FacilityCode facilityCode,Date effectiveDate) {
		FacilityRight result = new FacilityRight();
		try{
			MemberPlanFacilityRight right = memberPlanFacilityRightDao.getEffectiveRightByCustomerIdAndFacilityType(customerId, facilityCode.name(),effectiveDate);
			
			if (null == right)
			{
				result.setCanAccess(false);
				result.setCanBook(false);
				return result;
			}
			
			Permission permission = Permission.valueOf(right.getPermission());
			if(Permission.BA == permission){
				result.setCanAccess(true);
				result.setCanBook(true);
				return result;
			}
			if(Permission.A == permission || Permission.AO == permission){
				result.setCanAccess(true);
				return result;
			}
			return result;
		}catch(Exception e){
			logger.error("checkFacilityRight() error", e);
			return result;
		}
	}
	@Override
	@Transactional
	public FacilityRight checkFacilityRight(Long customerId, FacilityCode facilityCode,String startTime,String endTime) {
		FacilityRight result = new FacilityRight();
		try{
			MemberPlanFacilityRight right = memberPlanFacilityRightDao.getEffectiveRightByCustomerIdAndFacilityType(customerId, facilityCode.name(),startTime,endTime);
			
			if (null == right)
			{
				result.setCanAccess(false);
				result.setCanBook(false);
				return result;
			}
			
			Permission permission = Permission.valueOf(right.getPermission());
			if(Permission.BA == permission){
				result.setCanAccess(true);
				result.setCanBook(true);
				return result;
			}
			if(Permission.A == permission || Permission.AO == permission){
				result.setCanAccess(true);
				return result;
			}
			return result;
		}catch(Exception e){
			logger.error("checkFacilityRight() error", e);
			return result;
		}
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional
	public TrainingRight checkTrainingRight(Long customerId, TrainingCode trainingCode,Date effectiveDate,String startTime,String endTime) {
		// TODO Auto-generated method stub
		TrainingRight right=new TrainingRight();
		right.setCanBook(false);
		MemberLimitRule memberLimitRule=null;
		if(null==effectiveDate&&StringUtils.isEmpty(startTime)&&StringUtils.isEmpty(endTime))
		{
			//default currentTime
			memberLimitRule=limitRuleDao.getEffectiveMemberLimitRule(customerId, trainingCode.name());
		}else{
			memberLimitRule=limitRuleDao.getEffectiveRightByCustomerIdAndFacilityType(customerId, trainingCode.name(), effectiveDate,startTime,endTime);
		}
	  if(null!=memberLimitRule){
		  if(memberLimitRule.getTextValue().equals("true")){
			  right.setCanBook(true);
		  }
	  }
	  return right;
	}
	@Override
	@Transactional
	public boolean  checkRenewedTrainingRight(Long customerId, TrainingCode trainingCode) {
		// TODO Auto-generated method stub
		Boolean renewedBlean= Boolean.FALSE;
		MemberLimitRule memberLimitRule=null;
		StringBuilder sbf=new StringBuilder();
		sbf.append(" SELECT  right_code AS limitType,input_value AS textValue  FROM service_plan_addition_rule WHERE plan_no=");
		sbf.append(" ( SELECT service_plan_no FROM customer_service_subscribe ");
		sbf.append("   WHERE acc_no =");
		sbf.append("  ( SELECT acc_no FROM customer_service_acc  AS acc ");
		sbf.append("   LEFT JOIN customer_order_hd AS coh ON acc.order_no=coh.order_no ");
		sbf.append("   WHERE  acc.customer_id = ?");
		sbf.append("  AND DATE_ADD(acc.expiry_date, INTERVAL 1 DAY) >= DATE_FORMAT(?,'%Y-%m-%d')");
		sbf.append("  AND coh.order_status='CMP' ");
		sbf.append("  AND acc. remark='RENEW'  LIMIT 1 ");
		sbf.append("  ) ");
		sbf.append(" ) and right_code=?");
		
		List<Serializable> param=new ArrayList<>();
		param.add(customerId);
		param.add(new Date());
		param.add(trainingCode.name());
		
		List<MemberLimitRule> rules=limitRuleDao.getDtoBySql(sbf.toString(), param, MemberLimitRule.class);
		if(null!=rules&&rules.size()>0){
			memberLimitRule=rules.get(0);
			 if(null!=memberLimitRule){
				  if(memberLimitRule.getTextValue().equals("true")){
					  renewedBlean=Boolean.TRUE;
				  }
			  }
		}
		
	  return renewedBlean;
	}
	@Override
	@Transactional
	public boolean checkRenewedFacilityRight(Long customerId, FacilityCode facilityCode){
		
		Boolean renewedBlean= Boolean.FALSE;
		/***
		 * check rnewed service  ,customer_order_hd status is CMP
		 */
		StringBuilder sbf=new StringBuilder();
		sbf.append(" SELECT facility_type_code AS facilityTypeCode ,permission AS permission  FROM service_plan_facility WHERE service_plan_id=");
		sbf.append(" ( SELECT service_plan_no FROM customer_service_subscribe ");
		sbf.append("   WHERE acc_no =");
		sbf.append("  ( SELECT acc_no FROM customer_service_acc  AS acc ");
		sbf.append("   LEFT JOIN customer_order_hd AS coh ON acc.order_no=coh.order_no ");
		sbf.append("   WHERE  acc.customer_id = ?");
		sbf.append("  AND DATE_ADD(acc.expiry_date, INTERVAL 1 DAY) >= DATE_FORMAT(?,'%Y-%m-%d')");
		sbf.append("  AND coh.order_status='CMP' ");
		sbf.append("  AND acc. remark='RENEW'  LIMIT 1 ");
		sbf.append("  ) ");
		sbf.append(" ) and facility_type_code=?");
		List<Serializable> param=new ArrayList<>();
		param.add(customerId);
		param.add(new Date());
		param.add(facilityCode.name());
		
		List<MemberPlanFacilityRight>rights=limitRuleDao.getDtoBySql(sbf.toString(), param, MemberPlanFacilityRight.class);
		if(null!=rights&&rights.size()>0){
			Permission permission = Permission.valueOf(rights.get(0).getPermission());
			if(Permission.BA == permission){
				renewedBlean=true;
			}
		}
		
		return renewedBlean;
	}
	@Override
	@Transactional
	public OtherRight checkOtherRight(Long customerId, OtherRightCode trainingCode) {
		// TODO Auto-generated method stub
		//check the quotas, if day pass quotas per day in service plan <=0, means no permission, can't go into next page
		CustomerServiceAcc csa=customerServiceAccDao.getAactiveByCustomerId(customerId);
		OtherRight otherRight=new OtherRight();
		otherRight.setCanBook(Boolean.FALSE);
		if(null!=csa){
			List<CustomerServiceSubscribe> sbus=customerServiceSubscribeDao.getCustomerServiceSubscribeByAccNo(csa.getAccNo());
			if(null!=sbus&&sbus.size()>0){
				CustomerServiceSubscribe customerServiceSubscribe=sbus.get(0);
				Long planNo=customerServiceSubscribe.getId().getServicePlanNo();
				ServicePlanAdditionRule rule=servicePlanAdditionRuleDao.getByPlanNoAndRightCode(planNo, OtherRightCode.D1.name());
				if(Long.valueOf(rule.getInputValue())>0){
					otherRight.setCanBook(Boolean.TRUE);
				}
			}
		}
		return otherRight;
	}

	@Override
	@Transactional
	public ResponseResult getEffectiveFacilityRightByCustomerId(Long customerId) {
		// TODO Auto-generated method stub
		List<MemberPlanFacilityRight>rights=memberPlanFacilityRightDao.getEffectiveFacilityRightByCustomerId(customerId);
		/***
		 * check rnewed service  ,customer_order_hd status is CMP
		 */
		responseResult.initResult(GTAError.Success.SUCCESS, rights);
		return responseResult;
	}
	@Transactional
	public ResponseResult getRenewedFacilityRightByCustomerId(Long customerId) {
		// TODO Auto-generated method stub
		/***
		 * check rnewed service  ,customer_order_hd status is CMP
		 */
		StringBuilder sbf=new StringBuilder();
		sbf.append(" SELECT facility_type_code AS facilityTypeCode ,permission AS permission  FROM service_plan_facility WHERE service_plan_id=");
		sbf.append(" ( SELECT service_plan_no FROM customer_service_subscribe ");
		sbf.append("   WHERE acc_no =");
		sbf.append("  ( SELECT acc_no FROM customer_service_acc  AS acc ");
		sbf.append("   LEFT JOIN customer_order_hd AS coh ON acc.order_no=coh.order_no ");
		sbf.append("   WHERE  acc.customer_id = ?");
		sbf.append("  AND DATE_ADD(acc.expiry_date, INTERVAL 1 DAY) >= DATE_FORMAT(?,'%Y-%m-%d')");
		sbf.append("  AND coh.order_status='CMP' ");
		sbf.append("  AND acc. remark='RENEW'  LIMIT 1 ");
		sbf.append("  ) ");
		sbf.append(" ) ");
		List<Serializable> param=new ArrayList<>();
		param.add(customerId);
		param.add(new Date());
		List<MemberPlanFacilityRight>rights=limitRuleDao.getDtoBySql(sbf.toString(), param, MemberPlanFacilityRight.class);
		responseResult.initResult(GTAError.Success.SUCCESS, rights);
		return responseResult;
	}
	

	@Override
	@Transactional
	public ResponseResult getTrainingRight(Long customerId) {
		// TODO Auto-generated method stub
		List<MemberLimitRule> rules=limitRuleDao.getEffectiveListByCustomerId(customerId);
		/***
		 * check rnewed service  ,customer_order_hd status is CMP
		 */
		responseResult.initResult(GTAError.Success.SUCCESS, rules);
		return responseResult;
	}
	@Transactional
	public ResponseResult getRenewedTrainingRight(Long customerId) {
		// TODO Auto-generated method stub
		/***
		 * check rnewed service  ,customer_order_hd status is CMP
		 */
		StringBuilder sbf=new StringBuilder();
		sbf.append(" SELECT  right_code AS limitType,input_value AS textValue  FROM service_plan_addition_rule WHERE plan_no=");
		sbf.append(" ( SELECT service_plan_no FROM customer_service_subscribe ");
		sbf.append("   WHERE acc_no =");
		sbf.append("  ( SELECT acc_no FROM customer_service_acc  AS acc ");
		sbf.append("   LEFT JOIN customer_order_hd AS coh ON acc.order_no=coh.order_no ");
		sbf.append("   WHERE  acc.customer_id = ?");
		sbf.append("  AND DATE_ADD(acc.expiry_date, INTERVAL 1 DAY) >= DATE_FORMAT(?,'%Y-%m-%d')");
		sbf.append("  AND coh.order_status='CMP' ");
		sbf.append("  AND acc. remark='RENEW'  LIMIT 1 ");
		sbf.append("  ) ");
		sbf.append(" ) ");
		
		List<Serializable> param=new ArrayList<>();
		param.add(customerId);
		param.add(new Date());
		List<MemberLimitRule> rules=limitRuleDao.getDtoBySql(sbf.toString(), param, MemberLimitRule.class);
		
		responseResult.initResult(GTAError.Success.SUCCESS, rules);
		return responseResult;
	}

}
