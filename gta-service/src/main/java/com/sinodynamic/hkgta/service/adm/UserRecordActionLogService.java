package com.sinodynamic.hkgta.service.adm;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.entity.adm.UserRecordActionLog;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.ResponseMsg;

/**
 * @author christ
 * @since oct 10 2016
 */
public interface UserRecordActionLogService extends IServiceBase<UserRecordActionLog>{

	public List<UserRecordActionLog> getUserRecordActionLogByDate(Date actionDate);
	/***
	 * excute genenate pdf report by actionDate
	 * @param actionDate
	 * @return
	 */
	public  String excute(Date actionDate);
	/***
	 * save user_record_action_log
	 * @param customerProfile
	 * @param loginUserId
	 * @param action
	 * @return
	 */
	public  ResponseMsg save(CustomerProfile customerProfile,String loginUserId,String action);
	/***
	 * send dailPatronActivityReport email to staff
	 */
	public void sendEmailActivityReport(String reportPath);
	
	
}
