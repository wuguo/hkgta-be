package com.sinodynamic.hkgta.service.sys;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.sys.AssignRoleDto;
import com.sinodynamic.hkgta.dto.sys.MenuDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.crm.UserRole;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface UserRoleService extends IServiceBase<UserRole>{

	public List<UserRoleDto> getUserRoleListByUserId(String userId);

	public ResponseResult assignRole(AssignRoleDto dto,String createBy);

	public ResponseResult removeRole(AssignRoleDto dto);

	public ResponseResult getUserPermissions(String userId);

	ResponseResult getUserPermissionsV2(String userId);

	ResponseResult loadUserPermissionsV2(String userId, Map<String,  List<MenuDto>> roleMenu);
	
	ResponseResult checkPermissionsByUserId(String userId,String programId);

	public void clearMenuCache();
	public void clearMenuCache(String userId);	
	
}
