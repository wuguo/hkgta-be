package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.NotificationOnOffSettingDao;
import com.sinodynamic.hkgta.dto.crm.UserPreferenceSettingDto;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSettingPK;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class NotificationOnOffServiceImpl extends ServiceBase<UserPreferenceSetting> implements NotificationOnOffService{
	
	 @Autowired
	 private NotificationOnOffSettingDao notificationOnOffSettingDao;
	
	 @Transactional
	 public List<UserPreferenceSettingDto> getNotificationSettings(Long customerId) throws Exception{
		 return notificationOnOffSettingDao.getNotificationOnOffSetting(customerId);
	 }
	 
	 @Transactional
	 public List<UserPreferenceSettingDto> getRestaurantNotificationSettings(String userId) throws Exception{
		 return notificationOnOffSettingDao.getRestaurantNotificationOnOffSetting(userId);
	 }
	 
	 @Transactional
	 public List<String> getPushNotificationStaffByRestaurantId(String restaurantId) throws Exception{
		 return notificationOnOffSettingDao.getPushNotificationStaffByRestaurantId(restaurantId);
	 }
	 
	 @Transactional
	 public void updateNotificationSetting(String userId, String paramId, String paramValue) throws Exception{
		 UserPreferenceSettingPK pk = new UserPreferenceSettingPK();
		 pk.setParamId(paramId);
		 pk.setUserId(userId);
		 
		 UserPreferenceSetting setting = notificationOnOffSettingDao.get(UserPreferenceSetting.class, pk);
		 if (setting == null){
			 setting = new UserPreferenceSetting();
			 setting.setId(pk);
			 setting.setParamValue(paramValue);
			 setting.setUpdateDate(new Date());
			 notificationOnOffSettingDao.save(setting); 
		 } else {
			 setting.setParamValue(paramValue);
			 setting.setUpdateDate(new Date());
			 notificationOnOffSettingDao.update(setting); 
		 }
	 }
}
