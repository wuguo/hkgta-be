package com.sinodynamic.hkgta.service.mms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.mms.SpaAppointmentRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentSynDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.spa.em.AppointmentStatus;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAppointmentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetPaymentsRequest;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDatum;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAppointmentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetPaymentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.Payment;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.OrderStatus;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.SceneType;

@Service
public class SynchronizeSpaDataImpl extends ServiceBase implements SynchronizeSpaData {
    
    private Logger logger = Logger.getLogger(SynchronizeSpaDataImpl.class);
	private Logger mmsLog = Logger.getLogger(LoggerType.MMS.getName());
    
//    private static final int APPOINTMENT_DAYS_FORWORD = 0;
//    private static final int PAYMENT_DAYS_FORWORD = 0;
    private static final int APPOINTMENT_DAYS_AFTERWORD = 14;
    private static final String CUSTOMIZE_TYPE = "Custom";
    private static final String SCV = "Standalone Cash Value";
    private static final String SACV = "SACV";//staffPortal Standalone Cash Value  indentifying CustomerOrder orderRemark  
    
    @Autowired
    private SpaServcieManager spaServcieManager;
    
    @Autowired
    private SpaAppointmentRecDao spaAppointmentRecDao;
    
    @Autowired
    private CustomerOrderDetDao customerOrderDetDao;
    
    @Autowired
    private CustomerOrderHdDao customerOrderHdDao;
    
    @Autowired
    private CustomerOrderTransDao customerOrderTransDao;
    
    @Autowired
    private MemberDao memberDao;
    
    @Autowired 
    private SpaAppointmentRecService spaAppointmentRecService;
    
    @Autowired
    private MMSService mMSService;
    
    @SuppressWarnings("serial")
    private static final HashMap<String, String> APPOINTMENT_STATUS = new HashMap<String, String>() {
	    {
	        put("Open", "RSV");  
	        put("Confirmed", "RSV");
	        put("Checkin", "ATN");
	        put("No Show", "NAT");
	        put("Cancelled", "CAN");
	        put("Closed", "CLD");
	        put("Deleted", "DEL");
	    }
	};
	
	    @SuppressWarnings("serial")
	    private static final HashMap<String, PaymentMethod> PAYMENT_METHOD = new HashMap<String, PaymentMethod>() {
		    {
		        put("CASH", PaymentMethod.CASH);
		        put("CASHVALUE", PaymentMethod.CASHVALUE);
		        put("VISA", PaymentMethod.VISA);
		        put("MASTERCARD", PaymentMethod.MASTER);
//		        put("CHECK", PaymentMethod.CHEQUE);
		        put("GUESTFOLIO", PaymentMethod.CHRGPOST);
		        put("AMEX", PaymentMethod.AMEX);
		    }
	    };
    
	    
    @Override
    @Transactional
    public void synchronizePayments() {
	try {
	    Date toTime = new Date();
	    Date fromTime = getPaymentsLatestSynSuccessTime(toTime);
	    paymentSynchronize(fromTime, toTime);
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}
	
    }


    @Override
    @Transactional
    public void synchronizeAppointments() {
	try {
	    
	    Date now = new Date();
	    Date fromTime = getAppointmentsLatestSynSuccessTime(now);
	    Date toTime = DateCalcUtil.getNearDay(now, APPOINTMENT_DAYS_AFTERWORD);
	    appointmentSynchronize(fromTime, toTime);
	    
	} catch (Exception e) {
		mmsLog.error(e.getMessage());
	    throw new RuntimeException(e);
	}
	
    }

    private Map<String, List<SpaAppointmentSynDto>> groupingAppointments(List<SpaAppointmentSynDto> result) {
	
	if (result == null || result.size() == 0) return null;
	
	Map<String, List<SpaAppointmentSynDto>> groupResult =  new HashMap<String, List<SpaAppointmentSynDto>>();
	for (SpaAppointmentSynDto dto : result) {
	    
	    String key = dto.getExtInvoiceNo();
	    if (StringUtils.isEmpty(key)) continue;
	    List<SpaAppointmentSynDto> value = groupResult.get(key);
	    if (value == null) {
		value = new ArrayList<SpaAppointmentSynDto>();
		value.add(dto);
		groupResult.put(key, value);
	    } else {
		value.add(dto);
	    }
	}
	
	return groupResult;
    }
    
    private Map<String, List<Payment>> groupingPayments(List<Payment> result) {
	
	if (result == null || result.size() == 0) return null;
	Map<String, List<Payment>> groupResult =  new HashMap<String, List<Payment>>();
	for (Payment p : result) {
	    String key = p.getInvoiceNo();
	    if (StringUtils.isEmpty(key)) continue;
	    List<Payment> value = groupResult.get(key);
	    if (value == null) {
		value = new ArrayList<Payment>();
		value.add(p);
		groupResult.put(key, value);
	    } else {
		value.add(p);
	    }
	}
	
	return groupResult;
    }
    
    
    private List<SpaAppointmentSynDto> formatAppointmentData(GetAppointmentsResponse appointmentsResponse) {
	
	if (appointmentsResponse == null || appointmentsResponse.getAppointmentData() == null || 
		appointmentsResponse.getAppointmentData().size() == 0) return null;
	
	List<AppointmentDatum> appointmentData = appointmentsResponse.getAppointmentData();
	List<SpaAppointmentSynDto> appointmentResult = new ArrayList<SpaAppointmentSynDto>();
	for (AppointmentDatum appointment : appointmentData) {
	    
	    SpaAppointmentSynDto dto = new SpaAppointmentSynDto();
	    dto.setExtAppointmentId(appointment.getAppointmentId());
	    dto.setExtServiceCode(appointment.getServiceCode());
	    dto.setExtInvoiceNo(appointment.getInvoiceNo());
	    dto.setGuestCode(appointment.getGuestCode());
	    dto.setServicePrice(appointment.getFinalSalePrice());
	    dto.setServiceInternalCost(appointment.getServiceInternalCost());
	    dto.setTherapistNo(appointment.getEmployeeCode());
	    dto.setServiceTime(appointment.getServiceTime());
	    dto.setServiceDescription(null);
	    dto.setStatus(appointment.getStatus());
	    dto.setStartTime(appointment.getStartTime());
	    dto.setEndTime(appointment.getEndTime());
	    dto.setServiceName(appointment.getServiceName());
	    dto.setActualTime(appointment.getActualTime());
	    dto.setRebooked(appointment.getRebooked());
	    dto.setCheckInTime(appointment.getCheckInTime());
	    dto.setCreatedBy(appointment.getCreatedby());
	    dto.setCreationDate(appointment.getCreationDate());
	    dto.setExtNote(appointment.getNote());
	    dto.setEmployeeFName(appointment.getEmployeeFName());
	    dto.setEmployeeLName(appointment.getEmployeeLName());
	    appointmentResult.add(dto);
	}
	
	return appointmentResult;
    }
    
    
    private void appointmentSynchronize(Date fromDate, Date toDate) throws Exception {

	// fetch appointment item data from remote
    GetAppointmentsRequest request = new GetAppointmentsRequest();
    request.setFromDate(fromDate);
    request.setToDate(toDate);
    request.setStatus(AppointmentStatus.ALL);
    mmsLog.info("---appointmentSynchronize----request--->>>" + request);
	GetAppointmentsResponse appointmentsResponse = spaServcieManager.getAppointments(request);
	
	mmsLog.info(fromDate+"--fromDate--toDate-->>"+toDate+"--appointmentSynchronize--response---->>>"+appointmentsResponse);
	List<SpaAppointmentSynDto> appointmentResult = formatAppointmentData(appointmentsResponse);
	if (appointmentResult == null || appointmentResult.size() == 0) return;

	// grouping result
	Map<String, List<SpaAppointmentSynDto>> appointmentGroup = groupingAppointments(appointmentResult);
	if (appointmentGroup == null || appointmentGroup.isEmpty()) return;

	//iterator for synchronize
	for (Map.Entry<String, List<SpaAppointmentSynDto>> entry : appointmentGroup.entrySet()) {
	    
			// update spa appointments start
			String extInvoiceNo = entry.getKey();
			try {
			//	    System.out.println("appointmentSynchronize extInvoiceNo: "+extInvoiceNo);
			List<SpaAppointmentSynDto> newItems = entry.getValue();
			List<SpaAppointmentRec> oldItems = spaAppointmentRecDao.getSpaAppointmentRecByExtinvoiceNo(extInvoiceNo);
			//when current appointment list closed, then stop current and continue
			boolean isClosed = isAppointmentClosed(oldItems);
			long recoveryTime = 0l;
			if (isClosed) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】-newItems--->>" + newItems
						+ "----oldItems---->>>" + oldItems);
				continue;
			}
			// ignore items when guestCode is empty
			String academyNo = getAcademyNo(newItems);
			if (StringUtils.isEmpty(academyNo)) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】-newItems--->>" + newItems
						+ "----oldItems---->>>" + oldItems);
				continue;
			}
			// ignore items when guest not exists in gta db
			Long customerId = memberDao.getMemberCustomerId(academyNo);
			if (null == customerId) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----academyNo---->>>" + academyNo
						+ "--【Fail】-newItems--->>" + newItems + "----oldItems---->>>" + oldItems);
				continue;
			}
			/***
			 * when member have no appointMent ,don't excute next flows
			 */
			if (null == newItems || newItems.size() == 0) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----new spaAppointment size is zero return -->>>"
						+ academyNo);
				continue;
			}
			// logic for if a reservation is canceled by mms
			boolean doCanceled = doesCancelHapppened(oldItems, newItems);
			// delete old items info
			if (oldItems != null && oldItems.size() > 0) {
				for (SpaAppointmentRec entity : oldItems) {
					recoveryTime = entity.getRecoveryTime();
					spaAppointmentRecDao.delete(entity);
				}
			}
			CustomerOrderHd order = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
			Map<String, String> map = new HashMap<>();
			if (order != null) {
				order.setCustomerId(customerId);
				customerOrderHdDao.update(order);
				List<CustomerOrderDet> oldOrderDets = customerOrderDetDao
						.getCustomerOrderDetsByOrderNo(order.getOrderNo());
				if (oldOrderDets != null && oldOrderDets.size() > 0) {
					for (CustomerOrderDet entity : oldOrderDets) {
						entity.setCustomerOrderHd(null);
						/***
						 * staffPortal Standalone Cash Value  indentifying CustomerOrder orderRemark  
						 * if is SACV don't remove CustomerOrderDet
						 * else remove
						 */
						if (!SACV.equals(order.getOrderRemark())) {
							String key = order.getOrderNo().toString() + entity.getItemTotalAmout();
							map.put(key, entity.getItemNo());
							customerOrderDetDao.delete(entity);
						}
					}
				}
			}
			// insert new items info
			Date curDate = new Date();
			Timestamp curTimestamp = new Timestamp(curDate.getTime());
			if (newItems != null && newItems.size() > 0) {
				if (order == null) {
					order = new CustomerOrderHd();
					order.setOrderDate(curDate);
					order.setOrderStatus(OrderStatus.OPN.getDesc());
					order.setCustomerId(customerId);
					order.setOrderRemark("");
					order.setCreateDate(curTimestamp);
					order.setVendorRefCode(extInvoiceNo);
					Long orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(order);
					order.setOrderNo(orderNo);
				}
				BigDecimal orderAmount = BigDecimal.ZERO;
				boolean isOrderCanceled = true;
				for (SpaAppointmentSynDto item : newItems) {
					String status = APPOINTMENT_STATUS.get(item.getStatus());
					if (StringUtils.isEmpty(status))
						continue;
					if (!"CAN".equals(status))
						isOrderCanceled = false;
					//regenerate customer_order_det
					//eudo standalone  
					CustomerOrderDet customerOrderDet = new CustomerOrderDet();
					order.setCustomerOrderDets(null);
					customerOrderDet.setCustomerOrderHd(order);
					customerOrderDet.setItemRemark("");
					customerOrderDet.setOrderQty(1);
					BigDecimal amount = new BigDecimal(
							StringUtils.isEmpty(item.getServicePrice()) ? "0.00" : item.getServicePrice());

					if (!"CAN".equals(status))
						orderAmount = orderAmount.add(amount);
					customerOrderDet.setItemTotalAmout(amount);
					//		    customerOrderDet.setExtRefNo(item.getExtServiceCode());
					customerOrderDet.setExtRefNo(item.getExtInvoiceNo());
					customerOrderDet.setUpdateDate(curDate);
					customerOrderDet.setCreateDate(curTimestamp);
					customerOrderDet.setUpdateBy("spa_sync");
					customerOrderDet.setCreateBy("spa_sync");

					String key = order.getOrderNo().toString() + amount;
					if (map.containsKey(key)) {
						customerOrderDet.setItemNo(map.get(key));
					} else {
						//customerOrderDet.setItemNo("MMS2BRS");
						customerOrderDet.setItemNo(Constant.MMSTHIRD_ITEM_NO);
					}
					Long orderDetId = (Long) customerOrderDetDao.saveOrderDet(customerOrderDet);

					//regenerate SpaAppointmentRec
					SpaAppointmentRec spaAppointmentRec = new SpaAppointmentRec();
					spaAppointmentRec.setCustomerId(order.getCustomerId());
					spaAppointmentRec.setServiceName(item.getServiceName());
					spaAppointmentRec.setRecoveryTime(StringUtils.isEmpty(item.getRecoveryTime()) ? recoveryTime
							: Long.parseLong(item.getRecoveryTime()));
					spaAppointmentRec.setActualTime(
							StringUtils.isEmpty(item.getActualTime()) ? 0L : Long.parseLong(item.getActualTime()));
					spaAppointmentRec.setExtNote(item.getExtNote());
					spaAppointmentRec.setRebooked(
							StringUtils.isEmpty(item.getRebooked()) ? 0L : Long.parseLong(item.getRebooked()));
					//spaAppointmentRec.setCreateBy(item.getCreatedBy());
					//spaAppointmentRec.setCreateDate(StringUtils.isEmpty(item.getCreationDate()) ? null : Timestamp.valueOf(item.getCreationDate()));
					spaAppointmentRec.setCreateBy("spa_sync");
					spaAppointmentRec.setCreateDate(curTimestamp);
					spaAppointmentRec.setCheckinDatetime(StringUtils.isEmpty(item.getCheckInTime()) ? null
							: Timestamp.valueOf(item.getCheckInTime()));
					spaAppointmentRec.setTherapistFirstname(item.getEmployeeFName());
					spaAppointmentRec.setTherapistLastname(item.getEmployeeLName());
					spaAppointmentRec.setServiceTime(Long.parseLong(item.getServiceTime()) - recoveryTime);
					BigDecimal serviceInternalCost = new BigDecimal(StringUtils.isEmpty(item.getServiceInternalCost())
							? "0.00" : item.getServiceInternalCost());
					spaAppointmentRec.setServiceInternalCost(serviceInternalCost);
					spaAppointmentRec.setExtAppointmentId(item.getExtAppointmentId());
					spaAppointmentRec.setExtInvoiceNo(item.getExtInvoiceNo());
					spaAppointmentRec.setExtServiceCode(item.getExtServiceCode());
					spaAppointmentRec.setServiceDescription(item.getServiceDescription());
					spaAppointmentRec.setOrderDetId(orderDetId);
					String startTime = item.getStartTime();
					String endTime = item.getEndTime();
					if (recoveryTime > 0) {
						endTime = DateCalcUtil.formatDatetime(DateCalcUtil
								.getDateLessMins(DateCalcUtil.parseDateTime(item.getEndTime()), (int) recoveryTime));
					}
					spaAppointmentRec.setStatus(status);
					spaAppointmentRec.setStartDatetime(Timestamp.valueOf(startTime));
					spaAppointmentRec.setEndDatetime(Timestamp.valueOf(endTime));
					spaAppointmentRec.setTherapistCode(item.getTherapistNo());
					spaAppointmentRec.setUpdateBy("spa_sync");
					spaAppointmentRec.setUpdateDate(curTimestamp);
					spaAppointmentRec.setExtSyncTimestamp(curTimestamp);
					spaAppointmentRecDao.saveSpaAppointmenRec(spaAppointmentRec);
				}

				//update order amount info
				if (isOrderCanceled)
					order.setOrderStatus(OrderStatus.CAN.getDesc());
				/***
				* staffPortal Standalone Cash Value  indentifying CustomerOrder orderRemark  
				* if is SACV don't update  OrderTotalAmount
				* else update OrderTotalAmount
				*/
				if (!SACV.equals(order.getOrderRemark())) {
					order.setOrderTotalAmount(orderAmount);
					order.setUpdateBy("spa_sync");
					order.setUpdateDate(curDate);
					customerOrderHdDao.update(order);
				}
			}
			//send reservation cancel notification
			if (doCanceled) {
				spaAppointmentRecService.sendSmsWhenCancelSpaOrder(extInvoiceNo);
			}
			mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----academyNo---->>>" + academyNo
					+ "--【Success】-newItems--->>" + newItems + "----oldItems---->>>" + oldItems);
		} catch (Exception e) {
			mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo
					+ "--【Fail】------>>> order_no is null, Synchronization order error!--->>" + e.getMessage());
			continue;

		}
	}

    }
    
    
    private void paymentSynchronize(Date fromDate, Date toDate) throws Exception {
		GetPaymentsRequest request = new GetPaymentsRequest();
		request.setFromDate(fromDate);
		request.setToDate(toDate);
		mmsLog.info("---paymentSynchronize----request--->>>" + request);
		GetPaymentsResponse response = spaServcieManager.getPayments(request);
		mmsLog.info(fromDate + "--fromDate--toDate-->>" + toDate + "--paymentSynchronize--response---->>>" + response);

		if (response == null || response.getPayments() == null || response.getPayments().size() == 0)
			return;
		List<Payment> payments = response.getPayments();
		Map<String, List<Payment>> groupingResult = groupingPayments(payments);
		if (groupingResult == null || groupingResult.size() == 0)
			return;

		for (Map.Entry<String, List<Payment>> entry : groupingResult.entrySet()) {
			String extInvoiceNo = entry.getKey();
			List<Payment> transactionSet = entry.getValue();
			// when order is null, means appointmet in such order synchronize
			// failed
			// when status is cmp, means order process finished, and no need to
			// handle again
			// CustomerOrderHd :invoice no/PO no from 3rd party
			CustomerOrderHd order = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
			//SGG-3453   check the order number is null by invoice number in hkgta,
			if(null==order)
			{
					mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--No appointment check --and save transaction ");
					this.checkPaymentNoAppointMentSaveTransaction(extInvoiceNo,transactionSet);	
					continue;
			}else{
				if(OrderStatus.CMP.getDesc().equals(order.getOrderStatus())){
					mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】--transactionSet---->>> the customerOrderHd status is CMP " + transactionSet);
					continue;
				}
			}
//			if (order == null || OrderStatus.CMP.getDesc().equals(order.getOrderStatus())) {
//				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】--transactionSet---->>>" + transactionSet);
//				continue;
//			}
			/***
			 * SGG-2924 delete old customer order transaction info cancel the
			 * delete operation for cash value and online credit card payment
			 * List<CustomerOrderTrans> orderTranses =
			 * customerOrderTransDao.getPaymentDetailsByOrderNo(order.getOrderNo
			 * ()); for (CustomerOrderTrans cot : orderTranses) {
			 * customerOrderTransDao.delete(cot); }
			 */
			Date curDate = new Date();
			Timestamp curTimestamp = new Timestamp(curDate.getTime());
			/***
			 * all payment times paidAmount
			 */
			String paymentMethod = null;
			BigDecimal total = BigDecimal.ZERO;
			for (Payment payment : transactionSet) {
				/**
				 * SGG-2924 check MMS synchronized payment with receiptno is not
				 * null, and change order status to CMP when payment completed
				 */
				if (StringUtils.isEmpty(payment.getReceiptNo()))continue;
				
				BigDecimal paidAmount = new BigDecimal(
						StringUtils.isEmpty(payment.getPaymentAmount()) ? "0.00" : payment.getPaymentAmount());
				String paymentType = payment.getPaymentType();
				if (filterVisaOlineAndCashValue(payment))
				{
					total = total.add(paidAmount);
					continue;
				}
				PaymentMethod method = PAYMENT_METHOD.get(paymentType.toUpperCase());
				if (method != null) {
					paymentMethod = method.name();
				} else {
					paymentMethod = PaymentMethod.OTH.name();
				}
				Date paymentTime=null;
				if(StringUtils.isEmpty(payment.getPaymentDate())){
					paymentTime=curDate;
				}else{
					paymentTime=DateConvertUtil.parseString2Date(payment.getPaymentDate(), "yyyy-MM-dd HH:mm:ss");	
				}
				if(this.checkTransactionExist(payment.getInvoiceNo(), paymentMethod, payment.getTransactionNo(), paidAmount, paymentTime))
				{
					total = total.add(paidAmount);
					continue;
				}
				CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
				initCustomerOrderTransForCronjob(customerOrderTrans);
				// ExtRefNo means: invoice number from mms
				customerOrderTrans.setExtRefNo(payment.getInvoiceNo());
				// from mms transactionNo
				customerOrderTrans.setAgentTransactionNo(payment.getTransactionNo());
				customerOrderTrans.setCustomerOrderHd(order);
				customerOrderTrans.setTransactionTimestamp(paymentTime);	
				customerOrderTrans.setPaidAmount(paidAmount);
				customerOrderTrans.setStatus(Constant.Status.SUC.name());
				customerOrderTrans.setPaymentMethodCode(paymentMethod);
				customerOrderTrans.setPaymentMedia("");
				customerOrderTrans.setCreateBy("spa_sync");
				customerOrderTrans.setCreateDate(curDate);
				customerOrderTrans.setUpdateBy("spa_sync");
				customerOrderTrans.setUpdateDate(curTimestamp);
				total = total.add(paidAmount);
				customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
				mmsLog.info("paymentSynchronize  save customerOrderTransaction .....extRefNo:"+payment.getInvoiceNo()+"--CustomerId:"+order.getCustomerId()+"-- order:"+order);
			}
			/***
			 *  Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect):
			 *  solve method  
			 */
			CustomerOrderHd orderOrig = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
			if (total.compareTo(orderOrig.getOrderTotalAmount()) == 0) {
				orderOrig.setOrderStatus(OrderStatus.CMP.getDesc());
				mmsLog.info("update order sucess the status is CMP........");
			} else {
				orderOrig.setOrderStatus(OrderStatus.OPN.getDesc());
				mmsLog.info("update order sucess the status is OPN....when order totalPaidAmount no equest ALL payment amount ....");
			}
			orderOrig.setUpdateBy("spa_sync");
			orderOrig.setUpdateDate(curDate);
			customerOrderHdDao.update(orderOrig);
			 // cancel send mail 
    		 //sendPaymentReceipt(order);
			 
		}
	}
    /**
     * As discussed allow these transactions without appointment (i.e purchase production,enroll class) to sync to HKGTA
    modify the sync payment logic:
   2. if null, then call MMS API to get appointment by invoice number, 
   3. if null, then create order head, order detail with "MMS00000001" type, transaction record with internal remark "MMS Invoice #xxxx"
     */
    private void checkPaymentNoAppointMentSaveTransaction(String invoiceNo,List<Payment>transactionSet)
    {
    	mmsLog.info("check Payment no appointment  :invoiceNo:"+invoiceNo +" start........");
    	AppointmentDetailsRequest request = new AppointmentDetailsRequest();
		request.setInvoiceNo(invoiceNo);
		AppointmentDetailsResponse response = spaServcieManager.appointmentDetails(request);
		/**
		 * {"success": false,"message": "This Appointment does not exist" }
		 */
		mmsLog.info("customerOrder is null ,call MMS invoiceNo:"+invoiceNo +" ,response:"+response);
		
		if(response!=null && !response.getSuccess())
		{
			BigDecimal totalAmount = BigDecimal.ZERO;
			String academyNo=null;
			for (Payment payment : transactionSet)
			{
				BigDecimal paidAmount = new BigDecimal(StringUtils.isEmpty(payment.getPaymentAmount()) ? "0.00" : payment.getPaymentAmount());
				totalAmount=totalAmount.add(paidAmount);
				if(StringUtils.isNotEmpty(payment.getGuestCode())&&StringUtils.isEmpty(academyNo))
				{
					academyNo=payment.getGuestCode();
				}
			}
			if(StringUtils.isEmpty(academyNo))
			{
				mmsLog.error("payment guestCode is null ,don't maping member ");
				return ;
			}
			Member member=memberDao.getMemberByAcademyNo(academyNo);
			if(null==member){
				mmsLog.error("payment guestCode is null ,don't find member by acadeMyNo,the acadeNo: "+academyNo);
				return ;
			}
			CustomerOrderHd customerOrderHd = new CustomerOrderHd();
			customerOrderHd.setOrderDate(new Date());
			customerOrderHd.setOrderStatus(Constant.Status.OPN.name());			
			customerOrderHd.setCustomerId(member.getCustomerId());
			customerOrderHd.setOrderTotalAmount(totalAmount);
			customerOrderHd.setOrderRemark("");
			customerOrderHd.setCreateBy("spa_sync");		
			customerOrderHd.setCreateDate(new Timestamp(new Date().getTime()));	
			customerOrderHd.setUpdateBy("spa_sync");	
			customerOrderHd.setUpdateDate(new Date());
			customerOrderHd.setVendorRefCode(invoiceNo);
			customerOrderHdDao.save(customerOrderHd);
			
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(customerOrderHd);
			customerOrderDet.setItemNo(Constant.MMSTHIRD_ITEM_NO); 
			customerOrderDet.setOrderQty(1L);
			customerOrderDet.setItemTotalAmout(totalAmount);
			customerOrderDet.setExtRefNo(invoiceNo); //set ext ref no as the service code
			customerOrderDet.setCreateDate(new Timestamp(new Date().getTime()));			
			customerOrderDet.setUpdateDate(new Date());	               
		    customerOrderDetDao.saveOrderDet(customerOrderDet);
			
			
			String paymentMethod = null;
			BigDecimal total=BigDecimal.ZERO;
			for (Payment payment : transactionSet) {
				if (StringUtils.isEmpty(payment.getReceiptNo()))continue;
				BigDecimal paidAmount = new BigDecimal(StringUtils.isEmpty(payment.getPaymentAmount()) ? "0.00" : payment.getPaymentAmount());
				String paymentType = payment.getPaymentType();
				if (filterVisaOlineAndCashValue(payment))
				{
					total = total.add(paidAmount);
					continue;
				}
				PaymentMethod method = PAYMENT_METHOD.get(paymentType.toUpperCase());
				if (method != null) {
					paymentMethod = method.name();
				} else {
					paymentMethod = PaymentMethod.OTH.name();
				}
				Date paymentTime=null;
				if(StringUtils.isEmpty(payment.getPaymentDate())){
					paymentTime=new Date();
				}else{
					paymentTime=DateConvertUtil.parseString2Date(payment.getPaymentDate(), "yyyy-MM-dd HH:mm:ss");	
				}
				if(this.checkTransactionExist(payment.getInvoiceNo(), paymentMethod, payment.getTransactionNo(), paidAmount, paymentTime))
				{
					total = total.add(paidAmount);
					continue;
				}
				
				CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
				initCustomerOrderTransForCronjob(customerOrderTrans);
				// ExtRefNo means: invoice number from mms
				customerOrderTrans.setExtRefNo(payment.getInvoiceNo());
				// from mms transactionNo
				customerOrderTrans.setAgentTransactionNo(payment.getTransactionNo());
				customerOrderTrans.setCustomerOrderHd(customerOrderHd);
				customerOrderTrans.setTransactionTimestamp(paymentTime);	
				customerOrderTrans.setPaidAmount(paidAmount);
				customerOrderTrans.setStatus(Constant.Status.SUC.name());
				customerOrderTrans.setPaymentMethodCode(paymentMethod);
				customerOrderTrans.setPaymentMedia("");
				customerOrderTrans.setCreateBy("spa_sync");
				customerOrderTrans.setCreateDate(new Date());
				customerOrderTrans.setUpdateBy("spa_sync");
				customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
				customerOrderTrans.setInternalRemark("MMS Invoice #"+invoiceNo);
				
				total = total.add(paidAmount);
				customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
				
				mmsLog.info("paymentSynchronize no appointment , save customerOrderTransaction .....extRefNo:"+payment.getInvoiceNo());
			}
			/***
			 *  Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect):
			 *  solve method  
			 */
			CustomerOrderHd orderOrig = customerOrderHdDao.getOrderByExtInvoiceNo(invoiceNo);
			if (total.compareTo(orderOrig.getOrderTotalAmount()) == 0) {
				orderOrig.setOrderStatus(OrderStatus.CMP.getDesc());
				mmsLog.info("update order sucess the status is CMP........");
			} else {
				orderOrig.setOrderStatus(OrderStatus.OPN.getDesc());
				mmsLog.info("update order sucess the status is OPN....when order totalPaidAmount no equest ALL payment amount ....");
			}
			customerOrderHdDao.update(orderOrig);
		}
		mmsLog.info("check Payment no appointment  :invoiceNo:"+invoiceNo +" end........");
    }
    /***
	 * filter cashvalue and online credit card payment
	 * Standalone Cash Value
	 * @param payment
	 */
    private boolean filterVisaOlineAndCashValue(Payment payment){
    	if ((CUSTOMIZE_TYPE.equals(payment.getPaymentType())
				&& payment.getCustomName().toUpperCase().contains(PaymentMethod.CASHVALUE.name()))
				|| payment.getCardBankName().contains("online")
						&& (PaymentMethod.VISA.name().equalsIgnoreCase(payment.getPaymentType())
								|| PaymentMethod.CUP.name().equalsIgnoreCase(payment.getPaymentType())
								|| PaymentMethod.MASTER.name().equalsIgnoreCase(payment.getPaymentType())
								|| PaymentMethod.AMEX.name().equalsIgnoreCase(payment.getPaymentType()))
						||(SCV.toUpperCase().equals(payment.getCustomName().toUpperCase())&&CUSTOMIZE_TYPE.equals(payment.getPaymentType()))
						)
    	{
			return true;
		}
    	return false;
    }
    private boolean checkTransactionExist(String extRefNo,String paymentMethodCode,String agentTransactionNo,BigDecimal paidAmount,Date paymentTime){
    	List<CustomerOrderTrans>trans=customerOrderTransDao.getByCols(CustomerOrderTrans.class,
    			new String[]{"extRefNo","paymentMethodCode","agentTransactionNo","paidAmount","transactionTimestamp"},
    			new Serializable[]{extRefNo,paymentMethodCode,agentTransactionNo,paidAmount,paymentTime,},null);
    	if(null!=trans&&trans.size()>0){
    		return true;
    	}
    	return false;
    }
    
    private boolean isPaymentsComplete(List<Payment> transactions) {
	
	if (transactions == null || transactions.size() == 0) return false;
	for (Payment p : transactions) {
	    if (!StringUtils.isEmpty(p.getReceiptNo())) return true;
	}
	return false;
    }
    
    private String getAcademyNo(List<SpaAppointmentSynDto> items) {
	
	if (items == null || items.size() == 0) return null;
	for (SpaAppointmentSynDto dto : items) {
	    String guestCode = dto.getGuestCode();
	    if (!StringUtils.isEmpty(guestCode)) return guestCode;
	}
	return null;
    }
    
    @Transactional
    public Date getAppointmentsLatestSynSuccessTime(Date current) {
	
	Date date = spaAppointmentRecDao.getLastestSpaSyncTime();
	if (date == null) return current;
	return date;
    }
    
    @Transactional
    public Date getPaymentsLatestSynSuccessTime(Date current) {
	
	Date date = customerOrderTransDao.getLastestSpaSyncTime();
	if (date == null) return current;
	return date;
    }
    
    private void sendPaymentReceipt(CustomerOrderHd order) throws Exception {
	
	if (order == null) return;
	
	//send email recepit
	MMSPaymentDto dto = new MMSPaymentDto();
	dto.setOrderNo(String.valueOf(order.getOrderNo()));
	dto.setInvoiceNo(order.getVendorRefCode());
	mMSService.sentPaymentReceipt(dto, true);
	
	//send sms confirm
	List<SpaAppointmentRec> spaAppointmentRecs = spaAppointmentRecDao.getAppointmentRecsByInvoiceNo(order.getVendorRefCode());
	List<SpaAppointmentRec> appointmentList = new ArrayList<SpaAppointmentRec>();
	for (SpaAppointmentRec entity : spaAppointmentRecs) {
	    SpaAppointmentRec spaAppointmentRec = new SpaAppointmentRec();
	    spaAppointmentRec.setExtAppointmentId(entity.getExtAppointmentId());
	    spaAppointmentRec.setCustomerId(entity.getCustomerId());
	    spaAppointmentRec.setExtInvoiceNo(entity.getExtInvoiceNo());
	    appointmentList.add(spaAppointmentRec);
	}
	mMSService.sentMessage(appointmentList, SceneType.WELLNESS_PAYMENT_SETTLE);
	logger.info("send paymentReceipt  param:"+appointmentList.toString());
    }
    
    private boolean doesCancelHapppened(List<SpaAppointmentRec> oldItems, List<SpaAppointmentSynDto> newItems) {
	
	if (newItems == null || newItems.size() == 0) return false;
	
	if (oldItems == null || oldItems.size() == 0) {
	    
	    int i = 0;
	    for (SpaAppointmentSynDto dto : newItems) if ("Cancelled".equals(dto.getStatus())) i++;
	    if (i == newItems.size()) return true;
	    
	} else {
	    
	    int i = 0;
	    int j = 0;
	    for (SpaAppointmentSynDto dto : newItems) if ("Cancelled".equals(dto.getStatus())) i++;
	    for (SpaAppointmentRec entity : oldItems) if ("CAN".equals(entity.getStatus())) j++;
	    if (i == newItems.size() && i > j) return true;
	}
	
	return false;
    }
    
    private boolean isAppointmentClosed(List<SpaAppointmentRec> oldItems) {
	
	if (oldItems == null || oldItems.size() == 0) return false;
	
	int i = 0;
	for (SpaAppointmentRec appointment : oldItems) {
	    if ("CLD".equals(appointment.getStatus())) i++;
	}
	
	return  oldItems.size() == i ? true : false;
    }
    
	private void clearHistoryAndCreateNew(Entry<String, List<SpaAppointmentSynDto>> entry) {
		// update spa appointments start
		String extInvoiceNo = entry.getKey();
		try {
			// System.out.println("appointmentSynchronize extInvoiceNo:
			// "+extInvoiceNo);
			List<SpaAppointmentSynDto> newItems = entry.getValue();
			List<SpaAppointmentRec> oldItems = spaAppointmentRecDao.getSpaAppointmentRecByExtinvoiceNo(extInvoiceNo);
			// when current appointment list closed, then stop current and
			// continue
			boolean isClosed = isAppointmentClosed(oldItems);
			long recoveryTime = 0l;
			if (isClosed) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】-newItems--->>" + newItems
						+ "----oldItems---->>>" + oldItems);
				return ;
			}
			// ignore items when guestCode is empty
			String academyNo = getAcademyNo(newItems);
			if (StringUtils.isEmpty(academyNo)) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】-newItems--->>" + newItems
						+ "----oldItems---->>>" + oldItems);
				return;
			}
			// ignore items when guest not exists in gta db
			Long customerId = memberDao.getMemberCustomerId(academyNo);
			if (null == customerId) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----academyNo---->>>" + academyNo
						+ "--【Fail】-newItems--->>" + newItems + "----oldItems---->>>" + oldItems);
				return;
			}
			/***
			 * when member have no appointMent ,don't excute next flows
			 */
			if (null == newItems || newItems.size() == 0) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----new spaAppointment size is zero return -->>>"
						+ academyNo);
				return;
			}
			// logic for if a reservation is canceled by mms
			boolean doCanceled = doesCancelHapppened(oldItems, newItems);
			// delete old items info
			if (oldItems != null && oldItems.size() > 0) {
				for (SpaAppointmentRec entity : oldItems) {
					recoveryTime = entity.getRecoveryTime();
					spaAppointmentRecDao.delete(entity);
				}
			}
			CustomerOrderHd order = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
			Map<String, String> map = new HashMap<>();
			if (order != null) {
				order.setCustomerId(customerId);
				customerOrderHdDao.update(order);
				List<CustomerOrderDet> oldOrderDets = customerOrderDetDao
						.getCustomerOrderDetsByOrderNo(order.getOrderNo());
				if (oldOrderDets != null && oldOrderDets.size() > 0) {
					for (CustomerOrderDet entity : oldOrderDets) {
						entity.setCustomerOrderHd(null);
						/***
						 * staffPortal Standalone Cash Value indentifying
						 * CustomerOrder orderRemark if is SACV don't remove
						 * CustomerOrderDet else remove
						 */
						if (!SACV.equals(order.getOrderRemark())) {
							String key = order.getOrderNo().toString() + entity.getItemTotalAmout();
							map.put(key, entity.getItemNo());
							customerOrderDetDao.delete(entity);
						}
					}
				}
			}
			// insert new items info
			Date curDate = new Date();
			Timestamp curTimestamp = new Timestamp(curDate.getTime());
			if (newItems != null && newItems.size() > 0) {
				if (order == null) {
					order = new CustomerOrderHd();
					order.setOrderDate(curDate);
					order.setOrderStatus(OrderStatus.OPN.getDesc());
					order.setCustomerId(customerId);
					order.setOrderRemark("");
					order.setCreateDate(curTimestamp);
					order.setVendorRefCode(extInvoiceNo);
					Long orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(order);
					order.setOrderNo(orderNo);
				}
				BigDecimal orderAmount = BigDecimal.ZERO;
				boolean isOrderCanceled = true;
				for (SpaAppointmentSynDto item : newItems) {
					String status = APPOINTMENT_STATUS.get(item.getStatus());
					if (StringUtils.isEmpty(status))
						continue;
					if (!"CAN".equals(status))
						isOrderCanceled = false;
					// regenerate customer_order_det
					// eudo standalone
					CustomerOrderDet customerOrderDet = new CustomerOrderDet();
					order.setCustomerOrderDets(null);
					customerOrderDet.setCustomerOrderHd(order);
					customerOrderDet.setItemRemark("");
					customerOrderDet.setOrderQty(1);
					BigDecimal amount = new BigDecimal(
							StringUtils.isEmpty(item.getServicePrice()) ? "0.00" : item.getServicePrice());

					if (!"CAN".equals(status))
						orderAmount = orderAmount.add(amount);
					customerOrderDet.setItemTotalAmout(amount);
					// customerOrderDet.setExtRefNo(item.getExtServiceCode());
					customerOrderDet.setExtRefNo(item.getExtInvoiceNo());
					customerOrderDet.setUpdateDate(curDate);
					customerOrderDet.setCreateDate(curTimestamp);
					customerOrderDet.setUpdateBy("spa_sync");
					customerOrderDet.setCreateBy("spa_sync");

					String key = order.getOrderNo().toString() + amount;
					if (map.containsKey(key)) {
						customerOrderDet.setItemNo(map.get(key));
					} else {
						// customerOrderDet.setItemNo("MMS2BRS");
						customerOrderDet.setItemNo(Constant.MMSTHIRD_ITEM_NO);
					}
					Long orderDetId = (Long) customerOrderDetDao.saveOrderDet(customerOrderDet);

					// regenerate SpaAppointmentRec
					SpaAppointmentRec spaAppointmentRec = new SpaAppointmentRec();
					spaAppointmentRec.setCustomerId(order.getCustomerId());
					spaAppointmentRec.setServiceName(item.getServiceName());
					spaAppointmentRec.setRecoveryTime(StringUtils.isEmpty(item.getRecoveryTime()) ? recoveryTime
							: Long.parseLong(item.getRecoveryTime()));
					spaAppointmentRec.setActualTime(
							StringUtils.isEmpty(item.getActualTime()) ? 0L : Long.parseLong(item.getActualTime()));
					spaAppointmentRec.setExtNote(item.getExtNote());
					spaAppointmentRec.setRebooked(
							StringUtils.isEmpty(item.getRebooked()) ? 0L : Long.parseLong(item.getRebooked()));
					// spaAppointmentRec.setCreateBy(item.getCreatedBy());
					// spaAppointmentRec.setCreateDate(StringUtils.isEmpty(item.getCreationDate())
					// ? null : Timestamp.valueOf(item.getCreationDate()));
					spaAppointmentRec.setCreateBy("spa_sync");
					spaAppointmentRec.setCreateDate(curTimestamp);
					spaAppointmentRec.setCheckinDatetime(StringUtils.isEmpty(item.getCheckInTime()) ? null
							: Timestamp.valueOf(item.getCheckInTime()));
					spaAppointmentRec.setTherapistFirstname(item.getEmployeeFName());
					spaAppointmentRec.setTherapistLastname(item.getEmployeeLName());
					spaAppointmentRec.setServiceTime(Long.parseLong(item.getServiceTime()) - recoveryTime);
					BigDecimal serviceInternalCost = new BigDecimal(StringUtils.isEmpty(item.getServiceInternalCost())
							? "0.00" : item.getServiceInternalCost());
					spaAppointmentRec.setServiceInternalCost(serviceInternalCost);
					spaAppointmentRec.setExtAppointmentId(item.getExtAppointmentId());
					spaAppointmentRec.setExtInvoiceNo(item.getExtInvoiceNo());
					spaAppointmentRec.setExtServiceCode(item.getExtServiceCode());
					spaAppointmentRec.setServiceDescription(item.getServiceDescription());
					spaAppointmentRec.setOrderDetId(orderDetId);
					String startTime = item.getStartTime();
					String endTime = item.getEndTime();
					if (recoveryTime > 0) {
						endTime = DateCalcUtil.formatDatetime(DateCalcUtil
								.getDateLessMins(DateCalcUtil.parseDateTime(item.getEndTime()), (int) recoveryTime));
					}
					
					/***
					 * SGG-3827
					 * when mms cancel member payment ,syn appointment the status is 'Deleted'  then hkgta status is CAN
					 */
					if("DEL".equals(status)){
						status=Status.CAN.name();
						isOrderCanceled=Boolean.TRUE;
					}
					spaAppointmentRec.setStatus(status);
					spaAppointmentRec.setStartDatetime(Timestamp.valueOf(startTime));
					spaAppointmentRec.setEndDatetime(Timestamp.valueOf(endTime));
					spaAppointmentRec.setTherapistCode(item.getTherapistNo());
					spaAppointmentRec.setUpdateBy("spa_sync");
					spaAppointmentRec.setUpdateDate(curTimestamp);
					spaAppointmentRec.setExtSyncTimestamp(curTimestamp);
					spaAppointmentRecDao.saveSpaAppointmenRec(spaAppointmentRec);
				}

				// update order amount info
				if (isOrderCanceled)
					order.setOrderStatus(OrderStatus.CAN.getDesc());
				
				/***
				 * staffPortal Standalone Cash Value indentifying CustomerOrder
				 * orderRemark if is SACV don't update OrderTotalAmount else
				 * update OrderTotalAmount
				 */
				if (!SACV.equals(order.getOrderRemark())) {
					order.setOrderTotalAmount(orderAmount);
					order.setUpdateBy("spa_sync");
					order.setUpdateDate(curDate);
					customerOrderHdDao.update(order);
				}
			}
			// send reservation cancel notification
			if (doCanceled) {
				spaAppointmentRecService.sendSmsWhenCancelSpaOrder(extInvoiceNo);
			}
			mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----academyNo---->>>" + academyNo
					+ "--【Success】-newItems--->>" + newItems + "----oldItems---->>>" + oldItems);
		} catch (Exception e) {
			mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo
					+ "--【Fail】------>>> order_no is null, Synchronization order error!--->>" + e.getMessage());
		}
	}
	@Override
	@Transactional
	public void synchronizeAppointmentsNew(Entry<String, List<SpaAppointmentSynDto>> entry) {
		String extInvoiceNo = entry.getKey();
		try {
			List<SpaAppointmentSynDto> newItems = entry.getValue();
			List<SpaAppointmentRec> oldItems = spaAppointmentRecDao.getSpaAppointmentRecByExtinvoiceNo(extInvoiceNo);
			boolean isClosed = isAppointmentClosed(oldItems);
			long recoveryTime = 0l;
			if (isClosed) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】-newItems--->>" + newItems
						+ "----oldItems---->>>" + oldItems);
				return ;
			}
			// ignore items when guestCode is empty
			String academyNo = getAcademyNo(newItems);
			if (StringUtils.isEmpty(academyNo)) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】-newItems--->>" + newItems
						+ "----oldItems---->>>" + oldItems);
				return;
			}
			// ignore items when guest not exists in gta db
			Long customerId = memberDao.getMemberCustomerId(academyNo);
			if (null == customerId) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----academyNo---->>>" + academyNo
						+ "--【Fail】-newItems--->>" + newItems + "----oldItems---->>>" + oldItems);
				return;
			}
			/***
			 * when member have no appointMent ,don't excute next flows
			 */
			if (null == newItems || newItems.size() == 0) {
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----new spaAppointment size is zero return -->>>"
						+ academyNo);
				return;
			}
			// logic for if a reservation is canceled by mms
			boolean doCanceled = doesCancelHapppened(oldItems, newItems);
			
			if (oldItems != null && oldItems.size() > 0) {
				for (SpaAppointmentRec entity : oldItems) {
					recoveryTime = entity.getRecoveryTime();
				}
			}
			CustomerOrderHd order = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
			
			Map<String, String> map = new HashMap<>();
			if (order != null) {
				order.setCustomerId(customerId);
				customerOrderHdDao.update(order);
				List<CustomerOrderDet> oldOrderDets = customerOrderDetDao
						.getCustomerOrderDetsByOrderNo(order.getOrderNo());
				if (oldOrderDets != null && oldOrderDets.size() > 0) {
					for (CustomerOrderDet entity : oldOrderDets) {
						if (!SACV.equals(order.getOrderRemark())) {
							String key = order.getOrderNo().toString() + entity.getItemTotalAmout();
							map.put(key, entity.getItemNo());
						}
					}
					//if less new items size then clear old data 
					if(oldOrderDets.size()>newItems.size())
					{
						mmsLog.info("excute clear history spaAppointmentRec and customerOrderDet reason : old items size gt new items size .");
						clearHistoryAndCreateNew(entry);
						return ;
					}
				}
			}
			// insert new items info
			Date curDate = new Date();
			Timestamp curTimestamp = new Timestamp(curDate.getTime());
			if (newItems != null && newItems.size() > 0) {
				if (order == null) {
					order = new CustomerOrderHd();
					order.setOrderDate(curDate);
					order.setOrderStatus(OrderStatus.OPN.getDesc());
					order.setCustomerId(customerId);
					order.setOrderRemark("");
					order.setCreateDate(curTimestamp);
					order.setVendorRefCode(extInvoiceNo);
					Long orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(order);
					order.setOrderNo(orderNo);
				}
				BigDecimal orderAmount = BigDecimal.ZERO;
				boolean isOrderCanceled = true;
				for (SpaAppointmentSynDto item : newItems) {
					String status = APPOINTMENT_STATUS.get(item.getStatus());
					if (StringUtils.isEmpty(status))
						continue;
					if (!"CAN".equals(status)){
						isOrderCanceled = false;
					}
					String invoiceNo=item.getExtInvoiceNo();
					String extAppointmentId=item.getExtAppointmentId();
					SpaAppointmentRec spaAppointmentRec=spaAppointmentRecDao.getSpaAppointmentRecByInvoiceNoAndExAppointmentId(invoiceNo, extAppointmentId);
					//if exist and update 
					CustomerOrderDet customerOrderDet=null;
					if(null!=spaAppointmentRec)
					{
						//if exist rec status is CAN and item status is cancel no update 
						if(Status.CAN.name().equals(spaAppointmentRec.getStatus())
								&&(Status.CAN.name().equals(status)
										||"DEL".equals(status)))
						{
							mmsLog.info("no update SpaAppointmentRec reason: the Exist SpaAppointmentRec status is CAN and MMS item status is "+status+""
									 +"and MMS item invoiceNo:"+invoiceNo+" and extAppointmentId is :"+extAppointmentId);
							continue;
						}
						customerOrderDet=customerOrderDetDao.get(CustomerOrderDet.class, spaAppointmentRec.getOrderDetId());
					}else{
						spaAppointmentRec=new SpaAppointmentRec();
						//create
						customerOrderDet = new CustomerOrderDet();
						order.setCustomerOrderDets(null);
					}
					BigDecimal amount = new BigDecimal(
							StringUtils.isEmpty(item.getServicePrice()) ? "0.00" : item.getServicePrice());
					if (!"CAN".equals(status))
						orderAmount = orderAmount.add(amount);
					customerOrderDet.setItemTotalAmout(amount);
					// customerOrderDet.setExtRefNo(item.getExtServiceCode());
					customerOrderDet.setCustomerOrderHd(order);
					customerOrderDet.setItemRemark("");
					customerOrderDet.setOrderQty(1);
					customerOrderDet.setExtRefNo(item.getExtInvoiceNo());
					customerOrderDet.setUpdateDate(curDate);
					customerOrderDet.setCreateDate(curTimestamp);
					customerOrderDet.setUpdateBy("spa_sync");
					customerOrderDet.setCreateBy("spa_sync");
					String key = order.getOrderNo().toString() + amount;
					if (map.containsKey(key)) {
						customerOrderDet.setItemNo(map.get(key));
					} else {
						customerOrderDet.setItemNo(Constant.MMSTHIRD_ITEM_NO);
					}
					
					if(null!=customerOrderDet.getOrderDetId()){
						customerOrderDetDao.saveOrUpdate(customerOrderDet);
					}else{
						Long orderDetId=(Long)customerOrderDetDao.save(customerOrderDet);
						spaAppointmentRec.setOrderDetId(orderDetId);
					}
					// regenerate SpaAppointmentRec
					spaAppointmentRec.setCustomerId(order.getCustomerId());
					spaAppointmentRec.setServiceName(item.getServiceName());
					spaAppointmentRec.setRecoveryTime(StringUtils.isEmpty(item.getRecoveryTime()) ? recoveryTime
							: Long.parseLong(item.getRecoveryTime()));
					spaAppointmentRec.setActualTime(
							StringUtils.isEmpty(item.getActualTime()) ? 0L : Long.parseLong(item.getActualTime()));
					spaAppointmentRec.setExtNote(item.getExtNote());
					spaAppointmentRec.setRebooked(
							StringUtils.isEmpty(item.getRebooked()) ? 0L : Long.parseLong(item.getRebooked()));
					spaAppointmentRec.setCreateBy("spa_sync");
					spaAppointmentRec.setCreateDate(curTimestamp);
					spaAppointmentRec.setCheckinDatetime(StringUtils.isEmpty(item.getCheckInTime()) ? null
							: Timestamp.valueOf(item.getCheckInTime()));
					spaAppointmentRec.setTherapistFirstname(item.getEmployeeFName());
					spaAppointmentRec.setTherapistLastname(item.getEmployeeLName());
					spaAppointmentRec.setServiceTime(Long.parseLong(item.getServiceTime()) - recoveryTime);
					BigDecimal serviceInternalCost = new BigDecimal(StringUtils.isEmpty(item.getServiceInternalCost())
							? "0.00" : item.getServiceInternalCost());
					spaAppointmentRec.setServiceInternalCost(serviceInternalCost);
					spaAppointmentRec.setExtAppointmentId(item.getExtAppointmentId());
					spaAppointmentRec.setExtInvoiceNo(item.getExtInvoiceNo());
					spaAppointmentRec.setExtServiceCode(item.getExtServiceCode());
					spaAppointmentRec.setServiceDescription(item.getServiceDescription());
					
					String startTime = item.getStartTime();
					String endTime = item.getEndTime();
					if (recoveryTime > 0) {
						endTime = DateCalcUtil.formatDatetime(DateCalcUtil
								.getDateLessMins(DateCalcUtil.parseDateTime(item.getEndTime()), (int) recoveryTime));
					}
					/***
					 * SGG-3827
					 * when mms cancel member payment ,syn appointment the status is 'Deleted'  then hkgta status is CAN
					 */
					if("DEL".equals(status)){
						status=Status.CAN.name();
						isOrderCanceled=Boolean.TRUE;
					}
					spaAppointmentRec.setStatus(status);
					spaAppointmentRec.setStartDatetime(Timestamp.valueOf(startTime));
					spaAppointmentRec.setEndDatetime(Timestamp.valueOf(endTime));
					spaAppointmentRec.setTherapistCode(item.getTherapistNo());
					spaAppointmentRec.setUpdateBy("spa_sync");
					spaAppointmentRec.setUpdateDate(curTimestamp);
					spaAppointmentRec.setExtSyncTimestamp(curTimestamp);
					
					spaAppointmentRecDao.saveOrUpdate(spaAppointmentRec);
				}
				// update order amount info
				if (isOrderCanceled)
					order.setOrderStatus(OrderStatus.CAN.getDesc());
				/***
				 * staffPortal Standalone Cash Value indentifying CustomerOrder
				 * orderRemark if is SACV don't update OrderTotalAmount else
				 * update OrderTotalAmount
				 */
				if (!SACV.equals(order.getOrderRemark())) {
					order.setOrderTotalAmount(orderAmount);
					order.setUpdateBy("spa_sync");
					order.setUpdateDate(curDate);
					customerOrderHdDao.update(order);
				}
			}
			// send reservation cancel notification
			if (doCanceled) {
				spaAppointmentRecService.sendSmsWhenCancelSpaOrder(extInvoiceNo);
			}
			mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "----academyNo---->>>" + academyNo
					+ "--【Success】-newItems--->>" + newItems + "----oldItems---->>>" + oldItems);
		} catch (Exception e) {
			mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo
					+ "--【Fail】------>>> order_no is null, Synchronization order error!--->>" + e.getMessage());
		}
	}


	@Override
	@Transactional
	public void synchronizePaymentsNew(Entry<String, List<Payment>> entry) {
		// TODO Auto-generated method stub
		String extInvoiceNo = entry.getKey();
		List<Payment> transactionSet = entry.getValue();
		// when order is null, means appointmet in such order synchronize
		// failed
		// when status is cmp, means order process finished, and no need to
		// handle again
		// CustomerOrderHd :invoice no/PO no from 3rd party
		CustomerOrderHd order = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
		//SGG-3453   check the order number is null by invoice number in hkgta,
		if(null==order)
		{
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--No appointment check --and save transaction ");
				this.checkPaymentNoAppointMentSaveTransaction(extInvoiceNo,transactionSet);	
				return;
		}else{
			if(OrderStatus.CMP.getDesc().equals(order.getOrderStatus())){
				mmsLog.info("--extInvoiceNo---->>>" + extInvoiceNo + "--【Fail】--transactionSet---->>> the customerOrderHd status is CMP " + transactionSet);
				return;
			}
		}
		/***
		 * SGG-2924 delete old customer order transaction info cancel the
		 * delete operation for cash value and online credit card payment
		 * List<CustomerOrderTrans> orderTranses =
		 * customerOrderTransDao.getPaymentDetailsByOrderNo(order.getOrderNo
		 * ()); for (CustomerOrderTrans cot : orderTranses) {
		 * customerOrderTransDao.delete(cot); }
		 */
		Date curDate = new Date();
		Timestamp curTimestamp = new Timestamp(curDate.getTime());
		/***
		 * all payment times paidAmount
		 */
		String paymentMethod = null;
		BigDecimal total = BigDecimal.ZERO;
		for (Payment payment : transactionSet) {
			/**
			 * SGG-2924 check MMS synchronized payment with receiptno is not
			 * null, and change order status to CMP when payment completed
			 */
			if (StringUtils.isEmpty(payment.getReceiptNo()))continue;
			
			BigDecimal paidAmount = new BigDecimal(
					StringUtils.isEmpty(payment.getPaymentAmount()) ? "0.00" : payment.getPaymentAmount());
			String paymentType = payment.getPaymentType();
			if (filterVisaOlineAndCashValue(payment))
			{
				total = total.add(paidAmount);
				continue;
			}
			PaymentMethod method = PAYMENT_METHOD.get(paymentType.toUpperCase());
			if (method != null) {
				paymentMethod = method.name();
			} else {
				paymentMethod = PaymentMethod.OTH.name();
			}
			Date paymentTime=null;
			if(StringUtils.isEmpty(payment.getPaymentDate())){
				paymentTime=curDate;
			}else{
				paymentTime=DateConvertUtil.parseString2Date(payment.getPaymentDate(), "yyyy-MM-dd HH:mm:ss");	
			}
			if(this.checkTransactionExist(payment.getInvoiceNo(), paymentMethod, payment.getTransactionNo(), paidAmount, paymentTime))
			{
				total = total.add(paidAmount);
				continue;
			}
			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			initCustomerOrderTransForCronjob(customerOrderTrans);
			// ExtRefNo means: invoice number from mms
			customerOrderTrans.setExtRefNo(payment.getInvoiceNo());
			// from mms transactionNo
			customerOrderTrans.setAgentTransactionNo(payment.getTransactionNo());
			customerOrderTrans.setCustomerOrderHd(order);
			customerOrderTrans.setTransactionTimestamp(paymentTime);	
			customerOrderTrans.setPaidAmount(paidAmount);
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			customerOrderTrans.setPaymentMethodCode(paymentMethod);
			customerOrderTrans.setPaymentMedia("");
			customerOrderTrans.setCreateBy("spa_sync");
			customerOrderTrans.setCreateDate(curDate);
			customerOrderTrans.setUpdateBy("spa_sync");
			customerOrderTrans.setUpdateDate(curTimestamp);
			total = total.add(paidAmount);
			customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
			mmsLog.info("paymentSynchronize  save customerOrderTransaction .....extRefNo:"+payment.getInvoiceNo()+"--CustomerId:"+order.getCustomerId()+"-- order:"+order);
		}
		/***
		 *  Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect):
		 *  solve method  
		 */
		CustomerOrderHd orderOrig = customerOrderHdDao.getOrderByExtInvoiceNo(extInvoiceNo);
		if (total.compareTo(orderOrig.getOrderTotalAmount()) == 0) {
			orderOrig.setOrderStatus(OrderStatus.CMP.getDesc());
			mmsLog.info("update order sucess the status is CMP........");
		} else {
			orderOrig.setOrderStatus(OrderStatus.OPN.getDesc());
			mmsLog.info("update order sucess the status is OPN....when order totalPaidAmount no equest ALL payment amount ....");
		}
		orderOrig.setUpdateBy("spa_sync");
		orderOrig.setUpdateDate(curDate);
		customerOrderHdDao.update(orderOrig);
	}
		
}
