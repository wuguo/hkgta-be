package com.sinodynamic.hkgta.service.pms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.RoomDao;
import com.sinodynamic.hkgta.dao.pms.RoomStatusScheduleDao;
import com.sinodynamic.hkgta.dto.pms.RoomDeskDto;
import com.sinodynamic.hkgta.dto.pms.RoomStatusScheduleDto;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomStatusSchedule;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CSVFileUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RoomFrontdeskStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.RoomServiceStatus;
import com.sinodynamic.hkgta.util.constant.RoomStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import au.com.bytecode.opencsv.CSVReader;

@Service
public class RoomStatusScheduleServiceImpl extends ServiceBase<RoomStatusSchedule>implements RoomStatusScheduleService {

	protected final Logger oooLog = Logger.getLogger(LoggerType.OOOLOG.getName());
	protected final Logger oosLog = Logger.getLogger(LoggerType.OOSLOG.getName());
	protected final Logger deskLog = Logger.getLogger(LoggerType.DESKLOG.getName());

	@Autowired
	private RoomStatusScheduleDao roomStatusScheduleDao;
	@Autowired
	private RoomDao roomDao;

	@Override
	@Transactional
	public Serializable save(RoomStatusSchedule roomStatusSchedule) {
		return roomStatusScheduleDao.save(roomStatusSchedule);
	}

	@Override
	@Transactional
	public void autoUpdateRoomStatus(Date date) {
		// TODO Auto-generated method stub
		if (null == date) {
			date = new Date();
		}
		List<RoomStatusSchedule> list = roomStatusScheduleDao.getExpiredRoomStatusSchedule(date);
		if (null != list) {
			for (RoomStatusSchedule schedule : list) {
				handleOOS_OOO(schedule);
			}
		}
	}

	@Override
	@Transactional
	public void autoSettingRoomStatus(Date date) {
		if (null == date) {
			date = new Date();
		}
		List<RoomStatusSchedule> list = roomStatusScheduleDao.getRoomStatusScheduleByDate(date);
		if (null != list) {
			for (RoomStatusSchedule schedule : list) {
				handleRoomStatus(schedule);
			}
		}
	}

	/***
	 * if room Room service_status =OOS then update room service_status is null
	 * ,other status no change if room frontdesk_status=OOO then update room
	 * frontdesk_status is null ,status is D
	 * 
	 * @param schedule
	 */
	private void handleOOS_OOO(RoomStatusSchedule schedule) {
		Room room = roomDao.getByRoomId(schedule.getRoomId());
		if (null != room) {
			if (RoomServiceStatus.OOS.name().equals(schedule.getStascheduleOoStatus())) {
				room.setServiceStatus(null);
			} else if (RoomFrontdeskStatus.OOO.name().equals(schedule.getStascheduleOoStatus())) {
				room.setStatus(RoomStatus.D.name());
				room.setFrontdeskStatus(null);
			}
			room.setUpdateDate(new Date());
			room.setUpdateBy("system");
			roomDao.saveOrUpdate(room);
		}

	}

	/***
	 * 
	 * update ROOM ServiceStatus is OOS or FrontdeskStatus is OOO
	 * 
	 * @param schedule
	 */
	private void handleRoomStatus(RoomStatusSchedule schedule) {
		Room room = roomDao.getByRoomId(schedule.getRoomId());
		if (null != room) {
			if (RoomServiceStatus.OOS.name().equals(schedule.getStascheduleOoStatus())) {
				room.setServiceStatus(schedule.getStascheduleOoStatus());
			} else if (RoomFrontdeskStatus.OOO.name().equals(schedule.getStascheduleOoStatus())) {
				room.setFrontdeskStatus(schedule.getStascheduleOoStatus());
			}
			room.setUpdateDate(new Date());
			room.setUpdateBy("system");
			roomDao.saveOrUpdate(room);
		}
	}

	/**
	 * 获取schedule list
	 */
	@Override
	@Transactional
	public List<RoomStatusSchedule> getRoomStatusScheduleList(long roomId) {
		return roomStatusScheduleDao.getRoomStatusSchedule(roomId);
	}

	/**
	 * 获取schedule list
	 */
	@Override
	@Transactional
	public List<RoomStatusScheduleDto> getRoomStatusScheduleDtoList(long roomId) {
		return roomStatusScheduleDao.getRoomStatusScheduleDtoList(roomId);
	}

	/**
	 * 根据id删除记录
	 * 
	 * @param scheduleId
	 */
	@Override
	@Transactional
	public void deleteRoomStatusScheduleByScheduleId(long scheduleId) throws Exception {
		RoomStatusSchedule roomStatusSchedule = roomStatusScheduleDao.get(RoomStatusSchedule.class, scheduleId);
		if (roomStatusSchedule == null) {
			throw new GTACommonException(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		Room room = roomDao.getByRoomId(roomStatusSchedule.getRoomId());
		if (room == null) {
			throw new GTACommonException(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		if (room.getFrontdeskStatus() != null && RoomStatus.OOO.name().equals(room.getFrontdeskStatus())) {
			room.setFrontdeskStatus("V");
			room.setStatus(RoomStatus.D.name());
		} else if (room.getServiceStatus() != null && RoomStatus.OOS.name().equals(room.getServiceStatus())) {
			room.setServiceStatus(null);
		}
		roomDao.update(room);
		roomStatusScheduleDao.deleteById(RoomStatusSchedule.class, scheduleId);
	}

	/**
	 * 创建schedule
	 * 
	 * @param dto
	 * @param userId
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult createRoomStatusSchedule(RoomStatusScheduleDto dto, String userId) throws Exception {
		try {
			if (dto.getRoomId().isEmpty() || dto.getReasonCode().isEmpty() || dto.getStascheduleOoStatus().isEmpty()
					|| dto.getBeginDate() == null || dto.getEndDate() == null) {
				throw new GTACommonException(GTAError.CommonError.REQUIRED_PARAMETERS_IS_NULL);
			}
			String[] roomIds = dto.getRoomId().split(",");
			boolean checkOtoOOS = false;

			for (String roomIdStr : roomIds) {
				Long roomId = Long.parseLong(roomIdStr);
				Room room = roomDao.getByRoomId(roomId);
				if (room == null) {
					throw new GTACommonException(GTAError.GuestRoomError.ROOM_NOT_EXIST);
				}

				List<RoomStatusSchedule> scheduleList = getRoomStatusScheduleList(roomId);
				for (RoomStatusSchedule roomStatusSchedule : scheduleList) {
					// 新建schedule的时间必须满足开始和结束时间都大于现有schedule的开始和结束时间或者同时小于开始和结束时间
					if (!(dto.getBeginDate().compareTo(roomStatusSchedule.getBeginDate()) == 1
							&& dto.getBeginDate().compareTo(roomStatusSchedule.getEndDate()) == 1
							&& dto.getEndDate().compareTo(roomStatusSchedule.getBeginDate()) == 1
							&& dto.getEndDate().compareTo(roomStatusSchedule.getEndDate()) == 1)) {
						if (!(dto.getBeginDate().compareTo(roomStatusSchedule.getBeginDate()) == -1
								&& dto.getBeginDate().compareTo(roomStatusSchedule.getEndDate()) == -1
								&& dto.getEndDate().compareTo(roomStatusSchedule.getBeginDate()) == -1
								&& dto.getEndDate().compareTo(roomStatusSchedule.getEndDate()) == -1)) {
							// if
							// (!(roomStatusSchedule.getStascheduleOoStatus().equals(RoomStatus.OOS.name())
							// &&
							// dto.getStascheduleOoStatus().equals(RoomStatus.OOS.name())))
							// {
							throw new GTACommonException(GTAError.HouseKeepError.SCHEDULE_TIME_CONFLICT);
							// }
						}
					}
				}
				if (scheduleList.size() == 0) {
					/***
					 * check FE selected room [frontdesk_status、service_status]
					 * if equals ,no check OOO update to OOS / OOS TO OOO
					 * 
					 * stascheduleOoStatus
					 */
					if (dto.getStascheduleOoStatus().equalsIgnoreCase(RoomStatus.OOO.name())) {
						if (RoomStatus.OOS.name().equalsIgnoreCase(room.getServiceStatus())) {
							checkOtoOOS = true;
							break;
						}
					} else if (dto.getStascheduleOoStatus().equalsIgnoreCase(RoomStatus.OOS.name())) {
						if (RoomStatus.OOO.name().equalsIgnoreCase(room.getFrontdeskStatus())) {
							checkOtoOOS = true;
							break;
						}

					}
				}

				if (DateConvertUtil.isSameDay(new Date(), dto.getBeginDate())) {
					if (dto.getStascheduleOoStatus().equals(RoomStatus.OOO.name())) {
						room.setFrontdeskStatus(RoomStatus.OOO.name());
					} else {
						room.setServiceStatus(RoomStatus.OOS.name());
					}
					roomDao.update(room);
				}

				RoomStatusSchedule roomStatusSchedule = new RoomStatusSchedule();
				roomStatusSchedule.setBeginDate(dto.getBeginDate());
				roomStatusSchedule.setCreateBy(userId);
				roomStatusSchedule.setCreateDate(new Timestamp(new Date().getTime()));
				roomStatusSchedule.setEndDate(dto.getEndDate());
				roomStatusSchedule.setReasonCode(dto.getReasonCode());
				roomStatusSchedule.setRemark(dto.getRemark());
				roomStatusSchedule.setRoomId(room.getRoomId());
				roomStatusSchedule.setStascheduleOoStatus(dto.getStascheduleOoStatus());
				roomStatusScheduleDao.save(roomStatusSchedule);
			}
			if (checkOtoOOS) {
				responseResult.initResult(GTAError.CommonError.REQUIRED_PARAMETERS_IS_NULL,
						" the assign room' status is not release ,please selected others room !");
			} else {
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
			return responseResult;
		} catch (Exception e) {
			if (e.getClass() != GTACommonException.class) {
				logger.error(e.getMessage(), e);
				throw new GTACommonException(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			} else {
				throw e;
			}
		}
	}

	/**
	 * Import OOS and OOO
	 * 
	 * @param type
	 * @param userId
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult importOoCsv(String userId, String type, File csvFile) throws Exception {
		List<Room> roomList = roomDao.getRoomList();
		BufferedReader br = null;
		ArrayList<String[]> csvList = new ArrayList<String[]>();
		ArrayList<String> allCsvRoomList = new ArrayList<String>();
		try {
			if (RoomStatus.OOS.name().equals(type)) {
				oosLog.info("start import:" + csvFile.getName());
				oosLog.info("csvFile path" + csvFile.getAbsolutePath() + " file name:" + csvFile.getName());
			} else {
				oooLog.info("start import:" + csvFile.getName());
				oooLog.info("csvFile path" + csvFile.getAbsolutePath() + " file name:" + csvFile.getName());
			}
			String line = "";
			String cvsSplitBy = "\\t";
			FileInputStream fis = new FileInputStream(csvFile);
			br = new BufferedReader(new InputStreamReader(fis, "UTF-16"));
			// boolean isUpdate=true;
			br.readLine();
			int count = 0;
			while ((line = br.readLine()) != null) {
				if (StringUtils.isEmpty(line))
					continue;
				/***
				 * log oos/oos record
				 */
				if (RoomStatus.OOS.name().equals(type)) {
					oosLog.info(line.replace("	", ","));
				} else {
					oooLog.info(line.replace("	", ","));
				}
				line = line.replaceAll("\"", "");
				String[] ooArray = line.split(cvsSplitBy);
				count = count + 1;

				if (RoomStatus.OOS.name().equals(type)) {
					if (StringUtils.isBlank(ooArray[0]) || StringUtils.isBlank(ooArray[3])
							|| StringUtils.isBlank(ooArray[4]) || StringUtils.isBlank(ooArray[5])
							|| StringUtils.isBlank(ooArray[10])) {
						oosLog.info("row :" + count + " hander result:  content is null in csv file ");
						continue;
					} else {
						allCsvRoomList.add(ooArray[0]);
						boolean isAdded = false;
						for (Room room : roomList) {
							String roomNo = room.getRoomNo();
							if (ooArray[0].equals(roomNo) && DateConvertUtil.parseCurrentDate(ooArray[3]) != null
									&& DateConvertUtil.parseCurrentDate(ooArray[4]) != null) {
								if (checkRoomScheduleBeginDate(DateConvertUtil.parseCurrentDate(ooArray[3]),
										DateConvertUtil.parseCurrentDate(ooArray[4]))) {

									oosLog.info("row " + count + " hander result: the startDate " + ooArray[3]
											+ " before currentDate" + new Date() + " or startDate " + ooArray[6]
											+ " is greater than " + ooArray[7] + " this record is Invalid....");
								} else {
									csvList.add(ooArray);
									isAdded = true;
									break;
								}
							}
						}
						if (!isAdded) {
							oosLog.info("row " + count
									+ " hander result: table room's service_status will to be update to OOS, the roomNo is "
									+ ooArray[0] + "\n");
						} else {
							oosLog.info("row " + count
									+ " hander result:  roomNo is not exist in room table of HKGTA ........; RoomNo:"
									+ ooArray[0]);
						}
					}
				} else {
					if (StringUtils.isBlank(ooArray[0]) || StringUtils.isBlank(ooArray[2])
							|| StringUtils.isBlank(ooArray[6]) || StringUtils.isBlank(ooArray[7])
							|| StringUtils.isBlank(ooArray[8])) {
						oooLog.info("row :" + count + " hander result:  content is null in csv file ");
						continue;
					} else {
						allCsvRoomList.add(ooArray[2]);
						boolean isAdded = false;
						for (Room room : roomList) {
							String roomNo = room.getRoomNo();
							if (ooArray[2].equals(roomNo) && DateConvertUtil.parseCurrentDate(ooArray[6]) != null
									&& DateConvertUtil.parseCurrentDate(ooArray[7]) != null) {
								if (checkRoomScheduleBeginDate(DateConvertUtil.parseCurrentDate(ooArray[6]),
										DateConvertUtil.parseCurrentDate(ooArray[7]))) {
									oooLog.info("row " + count + " hander result: the startDate " + ooArray[6]
											+ " before currentDate" + new Date() + " or startDate " + ooArray[6]
											+ " is greater than " + ooArray[7] + " this record is Invalid....");
								} else {
									csvList.add(ooArray);
									isAdded = true;
									break;
								}
							}
						}
						if (isAdded) {
							oooLog.info("row " + count
									+ " hander result: table room's frontdesk_status will be update to OOO, the RoomNo is "
									+ ooArray[2] + "\n");
						} else {
							oooLog.info("row " + count
									+ " hander result:  roomNo is not exist in room table of HKGTA ........; RoomNo:"
									+ ooArray[2] + "\n");
						}
					}
				}
			}
			// success Record
			if (csvList.size() > 0) {
				boolean isSucc = saveRoomItem(roomList, csvList, userId, type, allCsvRoomList);
				if (isSucc) {
					if (RoomStatus.OOS.name().equals(type)) {
						oosLog.info("import OOS data is success.......");
					} else {
						oooLog.info("import OOO data is success.......");
					}
					responseResult.initResult(GTAError.Success.SUCCESS);
				} else {
					responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION, "Can not save to database");
					if (RoomStatus.OOS.name().equals(type)) {
						oosLog.info("save  OOS data  to HKGTA table room fail.......");
					} else {
						oooLog.info("save  OOO data  to HKGTA table room fail.......");
					}
				}
			} else {
				responseResult.initResult(GTAError.Success.SUCCESS, " No valid  data import");
				if (RoomStatus.OOS.name().equals(type)) {
					oosLog.info("OOS' data no valid data import to room table in HKGTA.......");
				} else {
					oooLog.info("OOO' data no valid data import to room table in HKGTA.......");
				}
			}
		} catch (Exception e) {
			if (RoomStatus.OOS.name().equals(type)) {
				oosLog.info(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
			} else {
				oooLog.info(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
			}
			e.printStackTrace();
			throw new GTACommonException(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION, e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return responseResult;
	}

	private boolean saveRoomItem(List<Room> roomList, ArrayList<String[]> csvList, String userId, String type,
			ArrayList<String> allCsvRoomList) {
		boolean isSucc = true;
		// 2. Clean table "room_status_schedule" data.
		int isOK = roomStatusScheduleDao.deleteAll(type);

		logger.info("isOK:" + isOK);
		String needUpdateRoomNo = "";
		Date today = DateConvertUtil.parseCurrentDate(DateConvertUtil.getYMDFormatDate(new Date()));
		// 4. Insert report data into this table ""room_status_schedule"" one by
		// one
		if (isOK >= 0) {
			for (String[] ooArray : csvList) {
				for (Room room : roomList) {
					String roomNo = room.getRoomNo();
					if (RoomStatus.OOS.name().equals(type)) {
						needUpdateRoomNo = ooArray[0];
					} else if (RoomStatus.OOO.name().equals(type)) {
						needUpdateRoomNo = ooArray[2];
					}

					if (needUpdateRoomNo.equals(roomNo)) {
						logger.info("roomNo:" + roomNo);
						/***
						 * check roomNo exist OOO/OOS, and delete history
						 */
						roomStatusScheduleDao.deleteByHql("delete from room_status_schedule  where room_id = ?",
								room.getRoomId());

						RoomStatusSchedule roomStatusSchedule = new RoomStatusSchedule();
						roomStatusSchedule.setCreateBy(userId);
						roomStatusSchedule.setCreateDate(new Timestamp(new Date().getTime()));
						roomStatusSchedule.setRoomId(room.getRoomId());
						roomStatusSchedule.setStascheduleOoStatus(type);
						if (RoomStatus.OOS.name().equals(type)) {
							Date startDate = DateConvertUtil.parseCurrentDate(ooArray[3]);
							Date endDate = DateConvertUtil.parseCurrentDate(ooArray[4]);

							roomStatusSchedule.setReasonCode(ooArray[10]);
							roomStatusSchedule.setRemark(ooArray[5]);
							roomStatusSchedule.setEndDate(endDate);
							roomStatusSchedule.setBeginDate(startDate);
							if (DateConvertUtil.isWithinRange(today, startDate, endDate)
									&& !RoomStatus.OOS.name().equals(room.getServiceStatus())) {
								room.setServiceStatus(RoomStatus.OOS.name());
								if (RoomStatus.OOO.name().equals(room.getFrontdeskStatus())) {
									room.setFrontdeskStatus(null);
								}
								room.setUpdateDate(new Date());
								room.setUpdateBy(userId);
								roomDao.saveOrUpdate(room);
							}
						} else if (RoomStatus.OOO.name().equals(type)) {
							Date startDate = DateConvertUtil.parseCurrentDate(ooArray[6]);
							Date endDate = DateConvertUtil.parseCurrentDate(ooArray[7]);

							roomStatusSchedule.setReasonCode(ooArray[0]);
							roomStatusSchedule.setRemark(ooArray[8]);
							roomStatusSchedule.setEndDate(endDate);
							roomStatusSchedule.setBeginDate(startDate);
							if (DateConvertUtil.isWithinRange(today, startDate, endDate)
									&& !RoomStatus.OOO.name().equals(room.getFrontdeskStatus())) {
								room.setFrontdeskStatus(RoomStatus.OOO.name());
								if (RoomStatus.OOS.name().equals(room.getServiceStatus())) {
									room.setServiceStatus(null);
								}
								room.setUpdateDate(new Date());
								room.setUpdateBy(userId);
								roomDao.saveOrUpdate(room);
							}
						}
						roomStatusScheduleDao.save(roomStatusSchedule);
						break;
					}
				}
			}

			// For the room exist in hkgta but not in report, if OOO report,
			// change the "status" field of the room table to "D", the
			// "frontdesk_status" to "V". if OOS report, change the
			// "service_status" to null

			if (RoomStatus.OOO.name().equals(type)) {
				List<Room> roomOooList = new ArrayList<Room>();

				for (Room room : roomList) {
					if (RoomStatus.OOO.name().equals(room.getFrontdeskStatus())) {
						roomOooList.add(room);
					}
				}
				boolean isAdded = false;

				for (Room room : roomOooList) {
					for (String roomNo : allCsvRoomList) {
						if (roomNo.equals(room.getRoomNo())) {
							isAdded = true;
							break;
						}
					}
					if (!isAdded) {
						logger.info("roomNo:" + room.getRoomNo());
						room.setStatus(RoomStatus.D.name());
						room.setFrontdeskStatus(Constant.RoomFrontdeskStatus.V.name());
						room.setUpdateDate(new Date());
						room.setUpdateBy(userId);
						roomDao.saveOrUpdate(room);
					}
				}
			} else if (RoomStatus.OOS.name().equals(type)) {
				List<Room> roomOosList = new ArrayList<Room>();
				for (Room room : roomList) {
					if (RoomStatus.OOS.name().equals(room.getServiceStatus())) {
						roomOosList.add(room);
					}
				}
				boolean isAdded = false;
				for (Room room : roomOosList) {
					for (String roomNo : allCsvRoomList) {
						if (roomNo.equals(room.getRoomNo())) {
							isAdded = true;
							break;
						}
					}
					if (!isAdded) {
						logger.info("roomNo:" + room.getRoomNo());
						room.setServiceStatus(null);
						room.setUpdateDate(new Date());
						room.setUpdateBy(userId);
						roomDao.saveOrUpdate(room);
					}
				}
			}
		} else {
			isSucc = false;
		}
		return isSucc;
	}

	private boolean checkRoomScheduleBeginDate(Date beginDate, Date endDate) {
		if (beginDate.compareTo(endDate) > 0) {
			return true;
		}
		/***
		 * check bschedule beginDate less 0 ,Invalid bschedule
		 */
		if (beginDate.compareTo(DateConvertUtil.getDate(new Date(), "yyyy-MM-dd")) < 0) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public ResponseResult importDeskStatusCsv(String userId, File file) throws Exception {
		deskLog.info("start importDeskStatusCsv:" + file.getName() + "csvFile path" + file.getAbsolutePath()
				+ " file name:" + file.getName());
//		List<RoomDeskDto> roomDesks = CSVFileUtil.parseCSVFile(RoomDeskDto.class, file,
//				new String[] { "room", "roomtype", "fdstatus" }, ' ');
		List<RoomDeskDto> roomDesks=this.readCSVParseList(file);
		if (null != roomDesks && roomDesks.size() > 0) {
			Map<String, Room> map = new HashMap<>();
			List<Room> roomList = roomDao.getRoomList();
			for (Room r : roomList) {
				map.put(r.getRoomNo(), r);
			}
			for (RoomDeskDto roomDesk : roomDesks) {
				if (map.containsKey(roomDesk.getRoom())) {
					Room room = map.get(roomDesk.getRoom());
					// only update O/V
					if (!roomDesk.getFdstatus().equals(room.getFrontdeskStatus())) {
						if (roomDesk.getRoomtype().equalsIgnoreCase(room.getRoomType().getTypeCode())) {
							// update room frontDesk
							if("O".equalsIgnoreCase(roomDesk.getFdstatus())
									||"V".equalsIgnoreCase(roomDesk.getFdstatus())){
								
								room.setFrontdeskStatus(roomDesk.getFdstatus());
								room.setUpdateBy(userId);
								room.setUpdateDate(new Date());
								roomDao.saveOrUpdate(room);
								deskLog.info(" update HKGTA room frontdeskStatus :" + room.getFrontdeskStatus() + " to "
										+ roomDesk.getFdstatus() + " ,the roomNo:" + roomDesk.getRoom() + " ");
							}else{
								deskLog.info("The csv roomDesk not in (O/V) the no update room, the line content is : csvFile line roomNo:" + roomDesk.getRoom()
										+ " and  roomtype :" + roomDesk.getRoomtype() + " and fdstatus :"
										+ roomDesk.getFdstatus());
							}
							
						} else {
							deskLog.info("no update  the room type is matching  the hkgta room type is "
									+ room.getRoomType().getTypeCode() + " , csvFile line roomNo:" + roomDesk.getRoom()
									+ " and  roomtype :" + roomDesk.getRoomtype() + " and fdstatus :"
									+ roomDesk.getFdstatus());
						}
					} else {
						deskLog.info("the exist roomNo:" + roomDesk.getRoom() + " and  is same frontdeskStatus :"
								+ roomDesk.getFdstatus() + " ,don't update room ");
					}

				} else {
					deskLog.info(" HKGTA no exist roomNo:" + roomDesk.getRoom());
				}
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		} else {
			deskLog.info("importDeskStatusCsv  have no  valid data import to room table in HKGTA.......");
			responseResult.initResult(GTAError.Success.SUCCESS, " No valid  data import");
		}
		return responseResult;
	}

	private List<RoomDeskDto> readCSVParseList(File csvFile) {
		List<RoomDeskDto> dtos=new ArrayList<>();
		FileInputStream fis=null;
		BufferedReader br =null;
		try {
			fis = new FileInputStream(csvFile);
			br = new BufferedReader(new InputStreamReader(fis, "UTF-16"));
			//remove title
			br.readLine();
			String line;
			while ((line = br.readLine()) != null) {
				if (StringUtils.isEmpty(line))
					continue;
				line = line.replaceAll("\"", "");
				String[] ooArray = line.split("\\t");
				RoomDeskDto dto=new RoomDeskDto();
				dto.setRoom(ooArray[0]);
				dto.setRoomtype(ooArray[1]);
				dto.setFdstatus(ooArray[2]);
				dtos.add(dto);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}finally{
			if(null!=br){
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(null!=fis){
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return dtos;
	}
}
