package com.sinodynamic.hkgta.service.onlinepayment;

import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.net.ssl.SSLSocketFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.Dialect.VPC3PartyConnection;
import com.Dialect.VPCPaymentCodesHelper;
import com.Dialect.VPCPaymentConnection;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayCmdDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayDao;
import com.sinodynamic.hkgta.dto.paymentGateway.AmexResultDto;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmd;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmdPK;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.NoResultCallBack;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.PaymentCmd;
import com.sinodynamic.hkgta.util.constant.PaymentGatewayProvider;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.X509TrustManager;

@Component
public class AmexPaymentGatewayManager {
	private Logger logger = Logger.getLogger(AmexPaymentGatewayManager.class);
	
	@Resource(name = "appProperties")
	protected Properties appProps;
	
	@Autowired
	private PaymentGatewayDao paymentGatewayDao;
	
	@Autowired
	private PaymentGatewayCmdDao paymentGatewayCmdDao;
	
	// VPC3PartyConnection vpc3conn = new VPC3PartyConnection();
	// VPCPaymentConnection payconn = new VPCPaymentConnection();
	// VPCPaymentCodesHelper helper = new VPCPaymentCodesHelper();

	// Define Constants
	// ****************
	// This is secret for encoding the MD5 hash
	// This secret will vary from merchant to merchant

	public static X509TrustManager s_x509TrustManager = null;
	public static SSLSocketFactory s_sslSocketFactory = null;

	static {
		s_x509TrustManager = new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] {};
			}

			public boolean isClientTrusted(X509Certificate[] chain) {
				return true;
			}

			public boolean isServerTrusted(X509Certificate[] chain) {
				return true;
			}
		};

		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		try {
			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, new X509TrustManager[] { s_x509TrustManager }, null);
			s_sslSocketFactory = context.getSocketFactory();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------
	private String translateUrlParams(final Map<String, Object> urlParams) {
		final StringBuilder string = new StringBuilder();

		CollectionUtil.loop(urlParams.keySet(), new NoResultCallBack<String>() {
			@Override
			public void execute(String key, int index) {
				if (index == 0) {
					string.append(key + "=" + urlParams.get(key));
				} else {
					string.append("&" + key + "=" + urlParams.get(key));
				}
			}
		});

		return "?" + string.toString();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AmexResultDto paymentCallBack(Map<String, String> fields) {
		logger.info("===call back from AMEX payment gateway START===");

		PaymentGateway paymentGateway = paymentGatewayDao.getUniqueByCol(PaymentGateway.class, "provider",
				PaymentGatewayProvider.AMEX.name());

		VPC3PartyConnection vpc3conn = new VPC3PartyConnection();
		VPCPaymentConnection payconn = new VPCPaymentConnection();
		String secureSecret = paymentGateway.getSecureSecret();
		vpc3conn.setSecureSecret(secureSecret);

		AmexResultDto amexResultDto = new AmexResultDto();
		////////////////////// BEING: Copy from AMEX sample code
		////////////////////// //////////////////////////////
		// retrieve all the incoming parameters into a hash map
		// Map<String, String> fields = new HashMap<String, String>();
		// for (Enumeration en = request.getParameterNames();
		// en.hasMoreElements();) {
		// String fieldName = (String) en.nextElement();
		// String fieldValue = request.getParameter(fieldName);
		// if ((fieldValue != null) && (fieldValue.length() > 0)) {
		// fields.put(fieldName, fieldValue);
		// }
		// }

		/*
		 * If there has been a merchant secret set then sort and loop through
		 * all the data in the Virtual Payment Client response. while we have
		 * the data, we can append all the fields that contain values (except
		 * the secure hash) so that we can create a hash and validate it against
		 * the secure hash in the Virtual Payment Client response.
		 * 
		 * NOTE: If the vpc_TxnResponseCode in not a single character then there
		 * was a Virtual Payment Client error and we cannot accurately validate
		 * the incoming data from the secure hash.
		 */

		// remove the vpc_TxnResponseCode code from the response fields as we do
		// not
		// want to include this field in the hash calculation
		String vpc_Txn_Secure_Hash = payconn.null2unknownDR((String) fields.remove("vpc_SecureHash"));
		String hashValidated = null;

		// defines if error message should be output
		// boolean errorExists = false;

		if (secureSecret != null && secureSecret.length() > 0 && (fields.get("vpc_TxnResponseCode") != null
				|| fields.get("vpc_TxnResponseCode") != "No Value Returned")) {

			// create secure hash and append it to the hash map if it was
			// created
			// remember if secureSecret = "" it wil not be created
			String secureHash = vpc3conn.hashAllFields(fields);

			// Validate the Secure Hash (remember MD5 hashes are not case
			// sensitive)
			if (vpc_Txn_Secure_Hash.equalsIgnoreCase(secureHash)) {
				// Secure Hash validation succeeded, add a data field to be
				// displayed later.
				// hashValidated = "<font
				// color='#00AA00'><strong>CORRECT</strong></font>";
				logger.info("AMEX purchase: Correct");

			} else {
				// Secure Hash validation failed, add a data field to be
				// displayed later.
				// errorExists = true;
				// hashValidated = "<font color='#FF0066'><strong>INVALID
				// HASH</strong></font>";
				logger.error("AMEX purchase: INVALID HASH");
				return null;
			}
		} else {
			// Secure Hash was not validated,
			// hashValidated = "<font color='orange'><strong>Not Calculated - No
			// 'SECURE_SECRET' present.</strong></font>";
			logger.error("AMEX purchase: Not Calculated - No 'SECURE_SECRET' present.");
			return null;
		}

		// Extract the available receipt fields from the VPC Response
		// If not present then let the value be equal to 'Unknown'
		// Standard Receipt Data
		amexResultDto.setTitle(payconn.null2unknownDR((String) fields.get("Title")));
		amexResultDto.setAgainLink(payconn.null2unknownDR((String) fields.get("AgainLink")));
		amexResultDto.setAmount(payconn.null2unknownDR((String) fields.get("vpc_Amount")));
		amexResultDto.setLocale(payconn.null2unknownDR((String) fields.get("vpc_Locale")));
		amexResultDto.setBatchNo(payconn.null2unknownDR((String) fields.get("vpc_BatchNo")));
		amexResultDto.setCommand(payconn.null2unknownDR((String) fields.get("vpc_Command")));
		amexResultDto.setMessage(payconn.null2unknownDR((String) fields.get("vpc_Message")));
		amexResultDto.setVersion(payconn.null2unknownDR((String) fields.get("vpc_Version")));
		amexResultDto.setCardType(payconn.null2unknownDR((String) fields.get("vpc_Card")));
		amexResultDto.setOrderInfo(payconn.null2unknownDR((String) fields.get("vpc_OrderInfo")));
		amexResultDto.setReceiptNo(payconn.null2unknownDR((String) fields.get("vpc_ReceiptNo")));
		amexResultDto.setMerchantID(payconn.null2unknownDR((String) fields.get("vpc_Merchant")));
		amexResultDto.setMerchTxnRef(payconn.null2unknownDR((String) fields.get("vpc_MerchTxnRef")));
		amexResultDto.setAuthorizeID(payconn.null2unknownDR((String) fields.get("vpc_AuthorizeId")));
		amexResultDto.setTransactionNo(payconn.null2unknownDR((String) fields.get("vpc_TransactionNo")));
		amexResultDto.setAcqResponseCode(payconn.null2unknownDR((String) fields.get("vpc_AcqResponseCode")));
		amexResultDto.setTxnResponseCode(payconn.null2unknownDR((String) fields.get("vpc_TxnResponseCode")));

		// CSC Receipt Data
		amexResultDto.setCscResultCode(payconn.null2unknownDR((String) fields.get("vpc_CSCResultCode")));
		amexResultDto.setACQCSCRespCode(payconn.null2unknownDR((String) fields.get("vpc_AcqCSCRespCode")));

		// AVS Receipt Data
		amexResultDto.setAvsResultCode(payconn.null2unknownDR((String) fields.get("vpc_AVSResultCode")));
		amexResultDto.setACQAVSRespCode(payconn.null2unknownDR((String) fields.get("vpc_AcqAVSRespCode")));
		////////////////////// END Copy from AMEX sample code
		////////////////////// //////////////////////////////

		logger.info("===call back from AMEX payment gateway END===");
		return amexResultDto;

		// return "rest/getProfile"; // temporary only. Will change to the real
		// url page later
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AmexResultDto capture(Map<String, String> requestFields) {
		logger.info("== capture authorized  transaction start ==");
		// Define Variables
		// If using a proxy server you must set the following variables
		// If NOT using a proxy server then set the 'useProxy' to false
		boolean useProxy = false;
		String proxyHost = appProps.getProperty("hkgta.paymentgateway.proxyHost");
		int proxyPort = Integer.parseInt(appProps.getProperty("hkgta.paymentgateway.proxyPort"));

		AmexResultDto amexResultDto = new AmexResultDto();
		//////////////// BEGIN copy from sample code JSP_VPC_Refund.jsp
		//////////////// ////////////////////////////////
		// create new element that uses the VPC Payment Connection class
		VPCPaymentConnection payconn = new VPCPaymentConnection();
		VPCPaymentCodesHelper helper = new VPCPaymentCodesHelper();

		// no need to send the vpcURL, Title and Submit Button to the vpc
		// String vpcURL = (String)
		// requestFields.remove("virtualPaymentClientURL");
		String title = (String) requestFields.remove("Title");
		requestFields.remove("SubButL");

		PaymentGateway paymentGateway = paymentGatewayDao.getUniqueByCol(PaymentGateway.class, "provider",
				PaymentGatewayProvider.AMEX.name());
		Long gatewayId = paymentGateway.getGatewayId();

		String cmdId = PaymentCmd.CAPTURE.getDesc();
		PaymentGatewayCmdPK pk = new PaymentGatewayCmdPK(gatewayId, cmdId);
		PaymentGatewayCmd paymentGatewayCmd = paymentGatewayCmdDao.get(PaymentGatewayCmd.class, pk);
		String vpcURL = paymentGateway.getGatewayUrl() + "/" + paymentGatewayCmd.getUrlCmd();

		requestFields.put("vpc_Command", paymentGatewayCmd.getCommandType());
		requestFields.put("vpc_AccessCode", paymentGateway.getAccessCode());
		requestFields.put("vpc_User", paymentGateway.getOperatorId());
		requestFields.put("vpc_Password", paymentGateway.getOperatorPassword());
		// Shopping Transaction Number:
		String vpc_TransactionNo = requestFields.remove("vpc_TransactionNo");
		requestFields.put("vpc_TransNo", vpc_TransactionNo);

		VPC3PartyConnection vpc3conn = new VPC3PartyConnection();
		String secureSecret = paymentGateway.getSecureSecret();
		vpc3conn.setSecureSecret(secureSecret);

		// Create MD5 secure hash and insert it into the hash map if it was
		// created. Remember if secureSecret = "" it will not be created
		if (secureSecret != null && secureSecret.length() > 0) {
			String secureHash = vpc3conn.hashAllFields(requestFields);
			requestFields.put("vpc_SecureHash", secureHash);
			requestFields.put("vpc_SecureHashType", "SHA256"); // added on
																// 20161031 (new
																// HASH
																// algorithm
																// from AE)
		}
		// create the post data string to send
		String postData = payconn.createPostDataFromMap(requestFields);

		String resQS = "";
		String message = "";
		logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST", null));
		try {
			// create a URL connection to the Virtual Payment Client
			payconn.s_sslSocketFactory=s_sslSocketFactory;
			
			resQS = payconn.doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);
		} catch (Exception ex) {
			logger.error(Log4jFormatUtil.logErrorMessage(vpcURL, postData, "POST", message, ex.getMessage()));
			/***
			 * when doPost timeout, the job hander timeout to queryDR if queryDR
			 * is fail and update transaction status PND to fail
			 */
			// The response is an error message so generate an Error Page
			message = ex.toString();
			logger.error("capture error" + message, ex);

		} // try-catch

		// create a hash map for the response data
		Map<String, String> responseFields = payconn.createMapFromResponse(resQS);

		// Extract the available receipt fields from the VPC Response
		// If not present then let the value be equal to 'No Value returned'
		// Not all data fields will return values for all transactions.

		// don't overwrite message if any error messages detected
		if (message.length() == 0) {
			amexResultDto.setMessage(payconn.null2unknown("vpc_Message", responseFields));
		}

		// Standard Receipt Data
		amexResultDto.setAmount(payconn.null2unknown("vpc_Amount", responseFields));
		amexResultDto.setLocale(payconn.null2unknown("vpc_Locale", responseFields));
		amexResultDto.setBatchNo(payconn.null2unknown("vpc_BatchNo", responseFields));
		amexResultDto.setCommand(payconn.null2unknown("vpc_Command", responseFields));
		amexResultDto.setVersion(payconn.null2unknown("vpc_Version", responseFields));
		amexResultDto.setCardType(payconn.null2unknown("vpc_Card", responseFields));
		amexResultDto.setOrderInfo(payconn.null2unknown("vpc_OrderInfo", responseFields));
		amexResultDto.setReceiptNo(payconn.null2unknown("vpc_ReceiptNo", responseFields));
		amexResultDto.setMerchantID(payconn.null2unknown("vpc_Merchant", responseFields));
		amexResultDto.setAuthorizeID(payconn.null2unknown("vpc_AuthorizeId", responseFields));
		amexResultDto.setTransactionNo(payconn.null2unknown("vpc_TransactionNo", responseFields));
		amexResultDto.setAcqResponseCode(payconn.null2unknown("vpc_AcqResponseCode", responseFields));
		String txnResponseCode = payconn.null2unknown("vpc_TxnResponseCode", responseFields);
		amexResultDto.setTxnResponseCode(txnResponseCode);

		amexResultDto.setTxnResponseCodeDesc(helper.getResponseDescription(txnResponseCode));

		// Capture Data
		amexResultDto.setShopTransNo(payconn.null2unknown("vpc_ShopTransactionNo", responseFields));
		amexResultDto.setAuthorisedAmount(payconn.null2unknown("vpc_AuthorisedAmount", responseFields));
		amexResultDto.setCapturedAmount(payconn.null2unknown("vpc_CapturedAmount", responseFields));
		amexResultDto.setRefundedAmount(payconn.null2unknown("vpc_RefundedAmount", responseFields));
		amexResultDto.setTicketNumber(payconn.null2unknown("vpc_TicketNo", responseFields));
		amexResultDto.setMerchTxnRef(payconn.null2unknown("vpc_MerchTxnRef", responseFields));

		if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) {
			logger.info("amex gateway response:txnResponseCode is :" + txnResponseCode);
			logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST",
					"response value is error:" + amexResultDto.getTxnResponseCodeDesc()));
			return null;
		}
		//////////////// END copy from sample code ////////////////////////////

		logger.info("TransactionNo=" + amexResultDto.getTransactionNo() + "TxnResponseCode="
				+ amexResultDto.getTxnResponseCode());
		logger.info("==capture pending transaction result end===");
		return amexResultDto;
	}
}
