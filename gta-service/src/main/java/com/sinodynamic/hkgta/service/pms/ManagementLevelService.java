package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import com.sinodynamic.hkgta.dto.pms.ManagementLevelDto;
import com.sinodynamic.hkgta.dto.pms.ManagementStaffDto;

public interface ManagementLevelService {

	public List<ManagementLevelDto> getAllManagementLevel();

	public List<ManagementStaffDto> getARManagementLevel();
}
