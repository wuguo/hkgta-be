package com.sinodynamic.hkgta.service.pms;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.SurveyType;


@Service
public class RoomReservationRecServiceImpl  extends ServiceBase implements RoomReservationRecService{

	@Autowired
	private RoomReservationRecDao		roomReservationRecDao;
	@Override
	@Transactional
	public List<RoomReservationRec> getReservationByOrderNo(Long orderNo) {
		// TODO Auto-generated method stub
		return roomReservationRecDao.getReservationByOrderNo(orderNo);
	}
	@Override
	@Transactional
	public RoomReservationRec getRoomReservationRecByConfirmId(String confirmId) {
		return roomReservationRecDao.getReservationByConfirmId(confirmId);
	}
	
	@Override
	public String createMemberCheckOutSql(String startTime, String endTime) {
		 StringBuilder sqlbd=new StringBuilder();
		 sqlbd.append(" SELECT re.customer_id as customerId,COUNT(*)AS total ,");
		 sqlbd.append("'"+SurveyType.ACCOMMODATION.name()+"' AS surveyType FROM  room_reservation_rec re");
		 sqlbd.append(" WHERE re.status='CKO' ");
		 sqlbd.append(" AND re.checkout_timestamp BETWEEN '"+startTime+"' AND '"+endTime+"' ");
		 sqlbd.append(" GROUP BY re.customer_id" );
		return  sqlbd.toString();
	}
	@Override
	public String createMemberCheckOutSql(Long customerId,String startTime, String endTime) {
		 StringBuilder sqlbd=new StringBuilder();
		 sqlbd.append(" SELECT re.customer_id as customerId,COUNT(*)AS total ,");
		 sqlbd.append("'"+SurveyType.ACCOMMODATION.name()+"' AS surveyType FROM  room_reservation_rec re");
		 sqlbd.append(" WHERE re.status='CKO' ");
		 sqlbd.append(" AND re.checkout_timestamp BETWEEN '"+startTime+"' AND '"+endTime+"' ");
		 sqlbd.append(" AND re.customer_id="+customerId+"");
		return  sqlbd.toString();
	}
	
	
}
