package com.sinodynamic.hkgta.service.onlinepayment;


import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.mms.SpaAppointmentRecDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayCmdDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.paymentGateway.AmexResultDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationInfoDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmd;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmdPK;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.spa.request.CCPaymentRequest;
import com.sinodynamic.hkgta.integration.spa.response.CCPaymentResponse;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.fms.FacilityEmailService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.mms.MMSService;
import com.sinodynamic.hkgta.service.pms.HotelReservationService;
import com.sinodynamic.hkgta.service.pms.HotelReservationServiceFacade;
import com.sinodynamic.hkgta.service.pms.PMSRequestProcessorService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderDetService;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.NoResultCallBack;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PaymentCmd;
import com.sinodynamic.hkgta.util.constant.PaymentGatewayProvider;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.X509TrustManager;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.Dialect.*;

/**
 * payment gateway business logic. Copy from AMEX sample codes + PaymentGateway.java
 * 
 * @author Sam Hui
 *
 */
@Service
public class AmexPaymentGatewayServiceImpl extends ServiceBase<PaymentGateway> implements AmexPaymentGatewayService {
	private Logger						logger	= Logger.getLogger(AmexPaymentGatewayServiceImpl.class);

	@Autowired
	private PaymentGatewayDao					paymentGatewayDao;

	@Autowired
	private PaymentGatewayCmdDao				paymentGatewayCmdDao;
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private CustomerOrderDetService customerOrderDetService;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private RoomReservationRecDao roomReservationRecDao;
	
	@Autowired
	private FacilityEmailService facilityEmailService;
	
	@Autowired
	private PMSRequestProcessorService pmsRequestProcessorService;
	
	@Autowired
	private HotelReservationService hotelReservationService;
	
	@Autowired
	private HotelReservationServiceFacade hotelReservationServiceFacade;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private SpaServcieManager spaServcieManager;
	
	@Autowired
	private MMSService mmsService;
	
	@Autowired
	private SpaAppointmentRecDao spaAppointmentRecDao;

	@Autowired
	private PaymentGatewayService paymentGatewayService;
	
	@Autowired
	private AmexPaymentGatewayManager amexPaymentGatewayManager;
	
	
    //VPC3PartyConnection vpc3conn = new VPC3PartyConnection();
    //VPCPaymentConnection payconn = new VPCPaymentConnection();
    //VPCPaymentCodesHelper helper = new VPCPaymentCodesHelper();
    
	// Define Constants
	// ****************
	// This is secret for encoding the MD5 hash
	// This secret will vary from merchant to merchant

	public static X509TrustManager				s_x509TrustManager	= null;
	public static SSLSocketFactory				s_sslSocketFactory	= null;
	
	static {
		s_x509TrustManager = new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] {};
			}

			public boolean isClientTrusted(X509Certificate[] chain) {
				return true;
			}

			public boolean isServerTrusted(X509Certificate[] chain) {
				return true;
			}
		};

		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		try {
			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, new X509TrustManager[] { s_x509TrustManager }, null);
			s_sslSocketFactory = context.getSocketFactory();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------
	private String translateUrlParams(final Map<String, Object> urlParams) {
		final StringBuilder string = new StringBuilder();

		CollectionUtil.loop(urlParams.keySet(), new NoResultCallBack<String>() {
			@Override
			public void execute(String key, int index) {
				if (index == 0) {
					string.append(key + "=" + urlParams.get(key));
				} else {
					string.append("&" + key + "=" + urlParams.get(key));
				}
			}
		});

		return "?" + string.toString();
	}

	/**
	 * JSP_VPC_3Party_Super_Order.html + JSP_VPC_3Party_Super_Order_DO.jsp \n
	 * Form the url and parameter to pass to AMEX payment gateway.
	 */
	@Transactional
	public String payment(CustomerOrderTrans customerOrderTrans, Map<String, Object> urlParams) {
		logger.info("call AMEX payment service");
		
		PaymentGateway paymentGateway = paymentGatewayDao.getUniqueByCol(PaymentGateway.class, "provider", PaymentGatewayProvider.AMEX.name());
		Long gatewayId = paymentGateway.getGatewayId();
														
		String cmdId = PaymentCmd.PAY.getDesc();
		PaymentGatewayCmdPK pk = new PaymentGatewayCmdPK(gatewayId, cmdId);
		PaymentGatewayCmd paymentGatewayCmd = paymentGatewayCmdDao.get(PaymentGatewayCmd.class, pk);

	    VPC3PartyConnection vpc3conn = new VPC3PartyConnection();
		
		String secureSecret = paymentGateway.getSecureSecret();
		vpc3conn.setSecureSecret(secureSecret);

		String vpc_AccessCode = paymentGateway.getAccessCode();
		String vpc_Version = appProps.getProperty("hkgta.paymentgateway.vpc_Version");// "1"
		String vpc_Command = paymentGatewayCmd.getCommandType();
		String virtualPaymentClientURL = paymentGateway.getGatewayUrl() + "/" + paymentGatewayCmd.getUrlCmd();
		String vpc_Merchant = paymentGateway.getMerchantId();
		// TODO PaidAmount此数小于1时第三方系统会提示错误 所以jack添加容错处理2016年5月19日16:39:44长潘决定删除配置文件判断是否乘于100		
		String vpc_Amount = customerOrderTrans.getPaidAmount().multiply(new BigDecimal(100)).longValue()  + "";
		String vpc_ReturnURL = paymentGatewayCmd.getReturnToPage();
		String vpc_MerchTxnRef = customerOrderTrans.getTransactionNo().toString();
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("vpc_AccessCode", vpc_AccessCode);
		fields.put("vpc_Version", vpc_Version);
		fields.put("vpc_Command", vpc_Command);
		fields.put("virtualPaymentClientURL", virtualPaymentClientURL);
		fields.put("vpc_MerchTxnRef", vpc_MerchTxnRef);
		fields.put("vpc_Merchant", vpc_Merchant);		
		fields.put("vpc_Amount", vpc_Amount);
		fields.put("vpc_Locale","en");
		
		if (urlParams == null || urlParams.isEmpty()) {
			fields.put("vpc_ReturnURL", vpc_ReturnURL);
		} else {
			fields.put("vpc_ReturnURL", vpc_ReturnURL + translateUrlParams(urlParams));
		}
		
		try {
		  fields.put("vpc_OrderInfo", customerOrderTrans.getCustomerOrderHd().getOrderNo().toString() );
		} catch (Exception e) {
			e.getStackTrace();
			logger.error(e);
		}		
		
		String vpcURL = (String) fields.remove("virtualPaymentClientURL");
		// Create MD5 secure hash and insert it into the hash map if it was created. Remember if secureSecret = "" it will not be created
		if (secureSecret != null && secureSecret.length() > 0) {
			String secureHash = vpc3conn.hashAllFields(fields);
			
			fields.put("vpc_SecureHash", secureHash);
			fields.put("vpc_SecureHashType", "SHA256"); // added on 20161031 (new HASH algorithm from AE)
		}
		// Create a redirection URL
		StringBuffer buf = new StringBuffer();
		buf.append(vpcURL).append('?');
		vpc3conn.appendQueryFields(buf, fields);
		logger.info("redit card Redirection URL is :"+buf.toString());
		return buf.toString();
	}

	@Transactional
	public String payment(CustomerOrderTrans customerOrderTrans) {
		logger.info("create credit card Redirection URL start...");
		return payment(customerOrderTrans, null);
	}


	/**
	 * JSP_VPC_3Party_Super_Order_DR.jsp -- process Receipt ReturnURL
	 */
	@Transactional
	public AmexResultDto paymentCallBack(Map<String, String> fields) {
		logger.info("===call back from AMEX payment gateway START===");

		PaymentGateway paymentGateway = paymentGatewayDao.getUniqueByCol(PaymentGateway.class, "provider", PaymentGatewayProvider.AMEX.name());

	    VPC3PartyConnection vpc3conn = new VPC3PartyConnection();
	    VPCPaymentConnection payconn = new VPCPaymentConnection();	    
		String secureSecret = paymentGateway.getSecureSecret();
		vpc3conn.setSecureSecret(secureSecret);
		
		AmexResultDto amexResultDto=new AmexResultDto();
        ////////////////////// BEING: Copy from AMEX sample code //////////////////////////////
	    // retrieve all the incoming parameters into a hash map
	    //Map<String, String> fields = new HashMap<String, String>();
//	    for (Enumeration en = request.getParameterNames(); en.hasMoreElements();) {
//	        String fieldName = (String) en.nextElement();
//	        String fieldValue = request.getParameter(fieldName);
//	        if ((fieldValue != null) && (fieldValue.length() > 0)) {
//	            fields.put(fieldName, fieldValue);
//	        }
//	    }
	    
	/*  If there has been a merchant secret set then sort and loop through all the
	    data in the Virtual Payment Client response. while we have the data, we can
	    append all the fields that contain values (except the secure hash) so that
	    we can create a hash and validate it against the secure hash in the Virtual
	    Payment Client response.

	    NOTE: If the vpc_TxnResponseCode in not a single character then
	    there was a Virtual Payment Client error and we cannot accurately validate
	    the incoming data from the secure hash. */

	    // remove the vpc_TxnResponseCode code from the response fields as we do not 
	    // want to include this field in the hash calculation
	    String vpc_Txn_Secure_Hash = payconn.null2unknownDR((String) fields.remove("vpc_SecureHash"));
	    String hashValidated = null;

	    // defines if error message should be output
	    //boolean errorExists = false;
	    
	    if (secureSecret != null && secureSecret.length() > 0 && 
	        (fields.get("vpc_TxnResponseCode") != null || fields.get("vpc_TxnResponseCode") != "No Value Returned")) {
	        
	        // create secure hash and append it to the hash map if it was created
	        // remember if secureSecret = "" it wil not be created
	        String secureHash = vpc3conn.hashAllFields(fields);
	    
	        // Validate the Secure Hash (remember MD5 hashes are not case sensitive)
	        if (vpc_Txn_Secure_Hash.equalsIgnoreCase(secureHash)) {
	            // Secure Hash validation succeeded, add a data field to be 
	            // displayed later.
	            //hashValidated = "<font color='#00AA00'><strong>CORRECT</strong></font>";
	        	logger.info("AMEX purchase: Correct");
	        	
	        } else {
	            // Secure Hash validation failed, add a data field to be
	            // displayed later.
	            //errorExists = true;
	            //hashValidated = "<font color='#FF0066'><strong>INVALID HASH</strong></font>";
	            logger.error("AMEX purchase: INVALID HASH");
	            return null;
	        }
	    } else {
	        // Secure Hash was not validated, 
	        //hashValidated = "<font color='orange'><strong>Not Calculated - No 'SECURE_SECRET' present.</strong></font>";
	    	logger.error("AMEX purchase: Not Calculated - No 'SECURE_SECRET' present.");
	    	return null;
	    }

	    // Extract the available receipt fields from the VPC Response
	    // If not present then let the value be equal to 'Unknown'
	    // Standard Receipt Data
	    amexResultDto.setTitle( payconn.null2unknownDR((String)fields.get("Title")) );
	    amexResultDto.setAgainLink( payconn.null2unknownDR((String)fields.get("AgainLink")) );
	    amexResultDto.setAmount( payconn.null2unknownDR((String)fields.get("vpc_Amount")) );
	    amexResultDto.setLocale( payconn.null2unknownDR((String)fields.get("vpc_Locale")) );
	    amexResultDto.setBatchNo( payconn.null2unknownDR((String)fields.get("vpc_BatchNo")) );
	    amexResultDto.setCommand( payconn.null2unknownDR((String)fields.get("vpc_Command")) );
	    amexResultDto.setMessage( payconn.null2unknownDR((String)fields.get("vpc_Message")) );
	    amexResultDto.setVersion( payconn.null2unknownDR((String)fields.get("vpc_Version")) );
	    amexResultDto.setCardType( payconn.null2unknownDR((String)fields.get("vpc_Card")) );
	    amexResultDto.setOrderInfo( payconn.null2unknownDR((String)fields.get("vpc_OrderInfo")) );
	    amexResultDto.setReceiptNo( payconn.null2unknownDR((String)fields.get("vpc_ReceiptNo")) );
	    amexResultDto.setMerchantID( payconn.null2unknownDR((String)fields.get("vpc_Merchant")) );
	    amexResultDto.setMerchTxnRef( payconn.null2unknownDR((String)fields.get("vpc_MerchTxnRef")) );
	    amexResultDto.setAuthorizeID( payconn.null2unknownDR((String)fields.get("vpc_AuthorizeId")) );
	    amexResultDto.setTransactionNo( payconn.null2unknownDR((String)fields.get("vpc_TransactionNo")) );
	    amexResultDto.setAcqResponseCode( payconn.null2unknownDR((String)fields.get("vpc_AcqResponseCode")) );
	    amexResultDto.setTxnResponseCode( payconn.null2unknownDR((String)fields.get("vpc_TxnResponseCode")) );

	    // CSC Receipt Data
	    amexResultDto.setCscResultCode( payconn.null2unknownDR((String)fields.get("vpc_CSCResultCode")) );
	    amexResultDto.setACQCSCRespCode( payconn.null2unknownDR((String)fields.get("vpc_AcqCSCRespCode")) );
	    
	    // AVS Receipt Data
	    amexResultDto.setAvsResultCode( payconn.null2unknownDR((String)fields.get("vpc_AVSResultCode")) );
	    amexResultDto.setACQAVSRespCode( payconn.null2unknownDR((String)fields.get("vpc_AcqAVSRespCode")) );
        ////////////////////// END Copy from AMEX sample code //////////////////////////////


	    logger.info("===call back from AMEX payment gateway END===");
		return amexResultDto;

		// return "rest/getProfile"; // temporary only. Will change to the real url page later
	}


	/**
	 * JSP_VPC_Capture.jsp -- capture after purchase success
	 */
	@Transactional
	public AmexResultDto capture(Map<String, String> requestFields) {
		logger.info("== capture authorized  transaction start ==");
		// Define Variables
		// If using a proxy server you must set the following variables
		// If NOT using a proxy server then set the 'useProxy' to false
		boolean useProxy = false;
		String proxyHost = appProps.getProperty("hkgta.paymentgateway.proxyHost");
		int proxyPort = Integer.parseInt(appProps.getProperty("hkgta.paymentgateway.proxyPort"));

		AmexResultDto amexResultDto=new AmexResultDto();
		//////////////// BEGIN copy from sample code JSP_VPC_Refund.jsp ////////////////////////////////		
	    // create new element that uses the VPC Payment Connection class
	    VPCPaymentConnection payconn = new VPCPaymentConnection();
	    VPCPaymentCodesHelper helper = new VPCPaymentCodesHelper();			
		
	    // no need to send the vpcURL, Title and Submit Button to the vpc
//	    String vpcURL = (String) requestFields.remove("virtualPaymentClientURL");
	    String title  = (String) requestFields.remove("Title");
	    requestFields.remove("SubButL");
	   
	    PaymentGateway paymentGateway = paymentGatewayDao.getUniqueByCol(PaymentGateway.class, "provider", PaymentGatewayProvider.AMEX.name());
		Long gatewayId = paymentGateway.getGatewayId();
														
		String cmdId = PaymentCmd.CAPTURE.getDesc();
		PaymentGatewayCmdPK pk = new PaymentGatewayCmdPK(gatewayId, cmdId);
		PaymentGatewayCmd paymentGatewayCmd = paymentGatewayCmdDao.get(PaymentGatewayCmd.class, pk);
		String vpcURL =paymentGateway.getGatewayUrl() + "/" + paymentGatewayCmd.getUrlCmd();
		
		requestFields.put("vpc_Command",paymentGatewayCmd.getCommandType());
	    requestFields.put("vpc_AccessCode", paymentGateway.getAccessCode());
	    requestFields.put("vpc_User", paymentGateway.getOperatorId());
	    requestFields.put("vpc_Password",paymentGateway.getOperatorPassword());
	    //Shopping Transaction Number:
	    String vpc_TransactionNo=requestFields.remove("vpc_TransactionNo");
	    requestFields.put("vpc_TransNo", vpc_TransactionNo);
	    
	    VPC3PartyConnection vpc3conn = new VPC3PartyConnection();
		String secureSecret = paymentGateway.getSecureSecret();
		vpc3conn.setSecureSecret(secureSecret);
		
		// Create MD5 secure hash and insert it into the hash map if it was created. Remember if secureSecret = "" it will not be created
		if (secureSecret != null && secureSecret.length() > 0) {
			String secureHash = vpc3conn.hashAllFields(requestFields);
			requestFields.put("vpc_SecureHash", secureHash);
			requestFields.put("vpc_SecureHashType", "SHA256"); // added on 20161031 (new HASH algorithm from AE)
		}
		// create the post data string to send
		String postData = payconn.createPostDataFromMap(requestFields);

		String resQS = "";
		String message = "";
		logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST", null));
		try {
			// create a URL connection to the Virtual Payment Client
			resQS = payconn.doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);
		}
		catch (Exception ex) {
			logger.error(Log4jFormatUtil.logErrorMessage(vpcURL, postData, "POST", message, ex.getMessage()));
			/***
			 * when  doPost timeout,
			 * the job hander timeout to queryDR  if queryDR is fail and update transaction status PND to fail 
			 */
			// The response is an error message so generate an Error Page
			message = ex.toString();
			logger.error("capture error" + message,ex);
			
		} // try-catch

		
		
		// create a hash map for the response data
		Map<String, String> responseFields = payconn.createMapFromResponse(resQS);

	    // Extract the available receipt fields from the VPC Response
	    // If not present then let the value be equal to 'No Value returned'
	    // Not all data fields will return values for all transactions.

	    // don't overwrite message if any error messages detected
	    if (message.length() == 0) {
	    	amexResultDto.setMessage(payconn.null2unknown("vpc_Message", responseFields));
	    }
	    
	    // Standard Receipt Data
	    amexResultDto.setAmount( payconn.null2unknown("vpc_Amount", responseFields) );
	    amexResultDto.setLocale( payconn.null2unknown("vpc_Locale", responseFields));
	    amexResultDto.setBatchNo(payconn.null2unknown("vpc_BatchNo", responseFields));
	    amexResultDto.setCommand(payconn.null2unknown("vpc_Command", responseFields));
	    amexResultDto.setVersion( payconn.null2unknown("vpc_Version", responseFields));
	    amexResultDto.setCardType( payconn.null2unknown("vpc_Card", responseFields));
	    amexResultDto.setOrderInfo( payconn.null2unknown("vpc_OrderInfo", responseFields));
	    amexResultDto.setReceiptNo(payconn.null2unknown("vpc_ReceiptNo", responseFields));
	    amexResultDto.setMerchantID(payconn.null2unknown("vpc_Merchant", responseFields));
	    amexResultDto.setAuthorizeID(payconn.null2unknown("vpc_AuthorizeId", responseFields));
	    amexResultDto.setTransactionNo(payconn.null2unknown("vpc_TransactionNo", responseFields));
	    amexResultDto.setAcqResponseCode(payconn.null2unknown("vpc_AcqResponseCode", responseFields));
	    String txnResponseCode=payconn.null2unknown("vpc_TxnResponseCode", responseFields);
	    amexResultDto.setTxnResponseCode(txnResponseCode);
	    
	    amexResultDto.setTxnResponseCodeDesc(helper.getResponseDescription(txnResponseCode));
	    
	    // Capture Data
	    amexResultDto.setShopTransNo( payconn.null2unknown("vpc_ShopTransactionNo", responseFields));
	    amexResultDto.setAuthorisedAmount(payconn.null2unknown("vpc_AuthorisedAmount", responseFields));
	    amexResultDto.setCapturedAmount(payconn.null2unknown("vpc_CapturedAmount", responseFields));
	    amexResultDto.setRefundedAmount(payconn.null2unknown("vpc_RefundedAmount", responseFields));
	    amexResultDto.setTicketNumber(payconn.null2unknown("vpc_TicketNo", responseFields));
	    amexResultDto.setMerchTxnRef(payconn.null2unknown("vpc_MerchTxnRef", responseFields));
	    
	    if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) {
	    	logger.info("amex gateway response:txnResponseCode is :"+txnResponseCode);
	    	logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST", "response value is error:"+amexResultDto.getTxnResponseCodeDesc()));
			return null;
		}
	    //////////////// END copy from sample code ////////////////////////////
	    
	    logger.info("TransactionNo="+ amexResultDto.getTransactionNo() + "TxnResponseCode="+amexResultDto.getTxnResponseCode());
	    logger.info("==capture pending transaction result end===");
		return amexResultDto;
	}
	
	
	/**
	 * JSP_VPC_Refund.jsp
	 */
	@Transactional
	public AmexResultDto refund(Map<String, String> requestFields) {
		logger.info("=== Refund captured transaction start===");
		// Define Variables
		// If using a proxy server you must set the following variables
		// If NOT using a proxy server then set the 'useProxy' to false
		boolean useProxy = !true;
		String proxyHost = appProps.getProperty("hkgta.paymentgateway.proxyHost");
		int proxyPort = Integer.parseInt(appProps.getProperty("hkgta.paymentgateway.proxyPort"));
		String vpcURL = requestFields.get("vpcURL");

		AmexResultDto amexResultDto=new AmexResultDto();

		//////////////// BEGIN copy from sample code JSP_VPC_Refund.jsp ////////////////////////////////		
	    // create new element that uses the VPC Payment Connection class
	    VPCPaymentConnection payconn = new VPCPaymentConnection();
	    VPCPaymentCodesHelper helper = new VPCPaymentCodesHelper();			
		
		// create the post data string to send
		String postData = payconn.createPostDataFromMap(requestFields);

		String resQS = "";
		String message = "";

		try {
			// create a URL connection to the Virtual Payment Client
			resQS = payconn.doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);

		} catch (Exception ex) {
			// The response is an error message so generate an Error Page
			message = ex.toString();
		} // try-catch

		
		
		// create a hash map for the response data
		Map<String, String> responseFields = payconn.createMapFromResponse(resQS);

	    // Extract the available receipt fields from the VPC Response
	    // If not present then let the value be equal to 'No Value returned'
	    // Not all data fields will return values for all transactions.

	    // don't overwrite message if any error messages detected
	    if (message.length() == 0) {
	    	amexResultDto.setMessage(payconn.null2unknown("vpc_Message", responseFields));
	    }
	    
	    // Standard Receipt Data
	    amexResultDto.setAmount( payconn.null2unknown("vpc_Amount", responseFields) );
	    amexResultDto.setLocale( payconn.null2unknown("vpc_Locale", responseFields));
	    amexResultDto.setBatchNo(payconn.null2unknown("vpc_BatchNo", responseFields));
	    amexResultDto.setCommand(payconn.null2unknown("vpc_Command", responseFields));
	    amexResultDto.setVersion( payconn.null2unknown("vpc_Version", responseFields));
	    amexResultDto.setCardType( payconn.null2unknown("vpc_Card", responseFields));
	    amexResultDto.setOrderInfo( payconn.null2unknown("vpc_OrderInfo", responseFields));
	    amexResultDto.setReceiptNo(payconn.null2unknown("vpc_ReceiptNo", responseFields));
	    amexResultDto.setMerchantID(payconn.null2unknown("vpc_Merchant", responseFields));
	    amexResultDto.setAuthorizeID(payconn.null2unknown("vpc_AuthorizeId", responseFields));
	    amexResultDto.setTransactionNo(payconn.null2unknown("vpc_TransactionNo", responseFields));
	    amexResultDto.setAcqResponseCode(payconn.null2unknown("vpc_AcqResponseCode", responseFields));
	    String txnResponseCode=payconn.null2unknown("vpc_TxnResponseCode", responseFields);
	    amexResultDto.setTxnResponseCode(txnResponseCode);
	    
	    amexResultDto.setTxnResponseCodeDesc(helper.getResponseDescription(txnResponseCode));
	    
	    // Refund Data
	    amexResultDto.setShopTransNo( payconn.null2unknown("vpc_ShopTransactionNo", responseFields));
	    amexResultDto.setAuthorisedAmount(payconn.null2unknown("vpc_AuthorisedAmount", responseFields));
	    amexResultDto.setCapturedAmount(payconn.null2unknown("vpc_CapturedAmount", responseFields));
	    amexResultDto.setRefundedAmount(payconn.null2unknown("vpc_RefundedAmount", responseFields));
	    amexResultDto.setTicketNumber(payconn.null2unknown("vpc_TicketNo", responseFields));
	    
	    String error = "";
	    if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) {
	        error = "Error ";
			logger.error("Occur error!");
		}
	    //////////////// END copy from sample code ////////////////////////////
	    
		logger.info("====Refund pending transaction result end===============TransactionNo=" + amexResultDto.getTransactionNo() + "==========");
		return amexResultDto;
	}


	/**
	 * There is no void command from AMEX. Cancel transaction must capture and then refund
	 */
//	public AmexResultDto voidpurchase(Map<String, String> requestFields, CustomerOrderTrans customerOrderTrans) {
//
//		logger.info("================Viod pending transaction  start================TransactionNo=" + customerOrderTrans.getTransactionNo() + "========================");
//
//		AmexResultDto amexResultDto=new AmexResultDto();
//		
//		if (capture(requestFields, customerOrderTrans)!=null) {
//		
//		   requestFields.put("vpc_Command", PaymentCmdType.REFUND.getDesc());
//
//		   requestFields.put("vpc_Amount", customerOrderTrans.getPaidAmount().longValue() * 100 + "");
//		   amexResultDto = refund(requestFields, customerOrderTrans);
//	    }
//
//		logger.info("================Void pending transaction end===============TransactionNo=" + customerOrderTrans.getTransactionNo() + "=========================");
//		return amexResultDto;
//	}
	
	/**
	 * JSP_VPC_QueryDR.jsp
	 */
	@Transactional
	public AmexResultDto queryDR(Map<String, String> requestFields) {
		logger.info("===Query pending transaction result start==");
		// If using a proxy server you must set the following variables
		// If NOT using a proxy server then set the 'useProxy' to false
		PaymentGateway paymentGateway = paymentGatewayDao.getUniqueByCol(PaymentGateway.class, "provider", PaymentGatewayProvider.AMEX.name());
		
		boolean useProxy = false;
		// You would need to add you own proxy server URL and port here
		String proxyHost = appProps.getProperty("hkgta.paymentgateway.proxyHost");
		int proxyPort = Integer.parseInt(appProps.getProperty("hkgta.paymentgateway.proxyPort"));

		AmexResultDto amexResultDto=new AmexResultDto();
		
		///////////////////////////////// BEGIN  copy from AMEX sample code JSP_VPC_QueryDR.jsp  /////////////////////////////////////
        // create new element that uses the VPC Payment Connection class
        VPCPaymentConnection payconn = new VPCPaymentConnection();
        VPCPaymentCodesHelper helper = new VPCPaymentCodesHelper();
        
        // This is the receipt type for 'Receipt' page 
        // Either a stardard 2/3-Party style or Capture/Refund style
        //String receiptType = request.getParameter("ReceiptType");        
		
		// retrieve all the parameters into a hash map
/*		Map<String, String> requestFields = new HashMap<String, String>();
	    // retrieve all the parameters into a hash map
	    //Map requestFields = new HashMap();
	    for (Enumeration Enum = request.getParameterNames(); Enum.hasMoreElements();) {
	        String fieldName = (String) Enum.nextElement();
	        String fieldValue = request.getParameter(fieldName);
	        if ((fieldValue != null) && (fieldValue.length() > 0)) {
	            requestFields.put(fieldName, fieldValue);
	        }
	    }*/
	    
	    // no need to send the vpcURL, Title and Submit Button to the vpc
	    String vpcURL = (String) requestFields.remove("virtualPaymentClientURL");
	    String title  = (String) requestFields.remove("Title");
	    requestFields.remove("SubButL");
	    
	    // Retrieve the order page URL from the incoming order page. This is only  
	    // here to give the user the easy ability to go back to the Order page. 
	    // This would not be required in a production system.
	    //String againLink = request.getHeader("Referer");
	    
	    
	    VPC3PartyConnection vpc3conn = new VPC3PartyConnection();
		String secureSecret = paymentGateway.getSecureSecret();
		vpc3conn.setSecureSecret(secureSecret);
		
		// Create MD5 secure hash and insert it into the hash map if it was created. Remember if secureSecret = "" it will not be created
		if (secureSecret != null && secureSecret.length() > 0) {
			String secureHash = vpc3conn.hashAllFields(requestFields);
			requestFields.put("vpc_SecureHash", secureHash);
			requestFields.put("vpc_SecureHashType", "SHA256"); // added on 20161031 (new HASH algorithm from AE)
		}
		
	    // create the post data string to send
	    String postData = payconn.createPostDataFromMap(requestFields);

	    String resQS = "";
	    String message = "";

	    logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST", null));
	    try {
	        // create a URL connection to the Virtual Payment Client
	        resQS = payconn.doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);

	    } catch (Exception ex) {
	        // The response is an error message so generate an Error Page
	        message = ex.toString();
	        logger.error(Log4jFormatUtil.logErrorMessage(vpcURL, postData, "POST", message, ex.getMessage()));
	    } //try-catch
	    // create a hash map for the response data
	    Map responseFields = payconn.createMapFromResponse(resQS);
	    //add response log 
	    logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST", responseFields.toString()));
	    // Extract the available receipt fields from the VPC Response
	    // If not present then let the value be equal to 'No Value returned'
	    // Not all data fields will return values for all transactions.

	    // don't overwrite message if any error messages detected
	    if (message.length() == 0) {
	    	amexResultDto.setMessage(payconn.null2unknown("vpc_Message", responseFields));
	    }
	    
	    // Standard Receipt Data
	    amexResultDto.setAmount( payconn.null2unknown("vpc_Amount", responseFields) );
	    amexResultDto.setLocale( payconn.null2unknown("vpc_Locale", responseFields));
	    amexResultDto.setBatchNo(payconn.null2unknown("vpc_BatchNo", responseFields));
	    amexResultDto.setCommand(payconn.null2unknown("vpc_Command", responseFields));
	    amexResultDto.setVersion( payconn.null2unknown("vpc_Version", responseFields));
	    amexResultDto.setCardType( payconn.null2unknown("vpc_Card", responseFields));
	    amexResultDto.setOrderInfo( payconn.null2unknown("vpc_OrderInfo", responseFields));
	    amexResultDto.setReceiptNo(payconn.null2unknown("vpc_ReceiptNo", responseFields));
	    amexResultDto.setMerchantID(payconn.null2unknown("vpc_Merchant", responseFields));
	    amexResultDto.setAuthorizeID(payconn.null2unknown("vpc_AuthorizeId", responseFields));
	    amexResultDto.setTransactionNo(payconn.null2unknown("vpc_TransactionNo", responseFields));
	    amexResultDto.setAcqResponseCode(payconn.null2unknown("vpc_AcqResponseCode", responseFields));
	    String txnResponseCode=payconn.null2unknown("vpc_TxnResponseCode", responseFields);
	    amexResultDto.setTxnResponseCode(txnResponseCode);
	    
	    amexResultDto.setTxnResponseCodeDesc(helper.getResponseDescription(txnResponseCode));

	    // CSC Receipt Data
	    amexResultDto.setCscResultCode( payconn.null2unknown("vpc_CSCResultCode", responseFields) );
	    amexResultDto.setACQCSCRespCode( payconn.null2unknown("vpc_AcqCSCRespCode", responseFields));	        
	    amexResultDto.setCscResultCodeDesc ( helper.displayCSCResponse(amexResultDto.getCscResultCode()) );
	    
	    // AVS Receipt Data
	    amexResultDto.setAvsResultCode( payconn.null2unknown("vpc_AVSResultCode", responseFields) );
	    amexResultDto.setACQAVSRespCode(payconn.null2unknown("vpc_AcqAVSRespCode", responseFields) );

	    amexResultDto.setAvsResultCodeDesc( helper.displayAVSResponse(amexResultDto.getAvsResultCode()) );
	    
	    // Financial Transaction Data
	    amexResultDto.setShopTransNo( payconn.null2unknown("vpc_ShopTransactionNo", responseFields));
	    amexResultDto.setAuthorisedAmount(payconn.null2unknown("vpc_AuthorisedAmount", responseFields));
	    amexResultDto.setCapturedAmount(payconn.null2unknown("vpc_CapturedAmount", responseFields));
	    amexResultDto.setRefundedAmount(payconn.null2unknown("vpc_RefundedAmount", responseFields));
	    amexResultDto.setTicketNumber(payconn.null2unknown("vpc_TicketNo", responseFields));	    
	    
	    // Specific QueryDR Data
	    amexResultDto.setDrExists(payconn.null2unknown("vpc_DRExists", responseFields));
	    amexResultDto.setMultipleDRs(payconn.null2unknown("vpc_FoundMultipleDRs", responseFields));

        /////////////////////// end copy from AMEX sample code //////////////////////////////	    
	    String error = "";
	    // Show this page as an error page if error condition
	    if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) {
	    	logger.info("amex gateway response:txnResponseCode is :"+txnResponseCode);
	    	logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST", "response value is error:"+amexResultDto.getTxnResponseCodeDesc()));
		}
	    logger.info("packaging AmexResultDto return to Use: "+JSONObject.fromObject(amexResultDto).toString());
	    logger.info("===Query pending transaction result end==");
		return amexResultDto;
	}
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public String processResult(HttpServletRequest request,AmexResultDto resultDto,String userId) {

		String htmlString = "<html><body onload='closeWindow();'>payment success"
				+ " <input type='button' value='ok' onclick='transactionSuccess()' />" + "<script>"
				+ " function transactionSuccess() {" + " window.close();" + " }" + " function closeWindow() {"
				+ " window.close();" + " }" + "</script>" + "</body></html>";

		Map<String, String> fields = new HashMap<String, String>();
		/***
		 * call back reqeust params
		 */
		for (Enumeration enump = request.getParameterNames(); enump.hasMoreElements();) {
			String fieldName = (String) enump.nextElement();
			String fieldValue = request.getParameter(fieldName);
			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				fields.put(fieldName, fieldValue);
			}
		}
		logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, JSONObject.fromObject(resultDto).toString()));

		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class,
				Long.parseLong(fields.get("vpc_MerchTxnRef")));

		if (null != resultDto && "0".equals(resultDto.getTxnResponseCode())) {

			/***
			 * if commit oasis/mms is success after ,capture()
			 * 
			 */
			logger.info("TxnResponseCode is SUCCESS.. update Transcatioin START..");
			if (!updateTranscation(request, userId, resultDto.getMerchTxnRef(), resultDto.getTxnResponseCode(),
					resultDto.getTransactionNo(), fields)) {
				logger.info("TxnResponseCode is SUCCESS.. update Transcatioin status is FAIL..");
				customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.name());
				customerOrderTrans.setUpdateBy("System");
				customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
				customerOrderTransDao.update(customerOrderTrans);
				return htmlString;
			} else {
				logger.info("TxnResponseCode is SUCCESS.. update Transcatioin status is SUC..");
			}
			// call capture
			logger.info(" capture to Gateway the param is :" + fields.toString());
			AmexResultDto capResultDto = amexPaymentGatewayManager.capture(fields);
			if (null != capResultDto) {
				logger.info(Log4jFormatUtil.logInfoMessage(null, null, null,
						JSONArray.fromObject(capResultDto).toString()));

				if ("0".equals(capResultDto.getTxnResponseCode())) {
					// set the transaction status in customer_order_trans from
					// PND to SUC. Prompt the patron the payment is success
					logger.info("update transactin start.......");
					// updateTranscation(request, userId,
					// capResultDto.getMerchTxnRef(),capResultDto.getTxnResponseCode(),
					// capResultDto.getTransactionNo(), fields);
				} else {
					/**
					 * if capture failed, retry 3 times. If still failed, prompt
					 * the patron the payment is failed and leave the authorized
					 * payment void after 7 days. Set the transaction status in
					 * customer_order_trans from PND to FAIL
					 */
					logger.info("retry 3  times  capture start.......");
					boolean _3timesSucess = true;
					for (int i = 1; i <= 3; i++) {
						logger.info("retry capture times:" + i);
						capResultDto = amexPaymentGatewayManager.capture(fields);
						logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, capResultDto.toString()));

						if (null != capResultDto && "0".equals(capResultDto.getTxnResponseCode())) {
							logger.info("retry capture " + capResultDto.getTxnResponseCode());
							// updateTranscation(request, userId,
							// capResultDto.getMerchTxnRef(),capResultDto.getTxnResponseCode(),
							// capResultDto.getTransactionNo(), fields);
							break;
						}

					}
					logger.info("retry capture 3 times end.......");
				}
			} else {
				logger.info("Amex gateway capture is null ........");
				logger.info(" AmexPaymentGatewayManager.capture() :" + fields.toString());
				
				htmlString = "<html><body  onload='closeWindow();'>"
						+ " the patron the payment is failed <input type='button' value='ok' onclick='transactionFail()' />"
						+ "<script>" + " function transactionFail() {" + " app.failure();" + " }"
						+ " function closeWindow() {" + " window.close();" + " }" + "</script>" + "</body></html>";
			}
		} else {
			logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, "paymentCallBack response result is null.. "));
			if (null != customerOrderTrans) {
				CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
				if (null != customerOrderHd) {
					CustomerOrderDet customerOrderDet = customerOrderDetService
							.getCustomerOrderDet(customerOrderHd.getOrderNo());
					if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo())
							&& customerOrderDet.getItemNo().contains(Constant.FACILITY_PRICE_PREFIX)) {
						memberFacilityTypeBookingService.voidMemberFacilityTypeBooking(
								customerOrderTrans.getCustomerOrderHd().getOrderNo(), userId);
					} else if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo())
							&& customerOrderDet.getItemNo().contains(Constant.PMS_ROOMTYPE_ITEM_PREFIX)) {
						// Callback from bank , but bank fails to pay
						// do nothing
						// payment before create reservation Guest room and
						// golf/tennis payment after create reservation
						/***
						 * SGG-3695
						 */
						if ("C".equalsIgnoreCase(resultDto.getTxnResponseCode())) {
							// need update reservation status is RSV
							List<RoomReservationRec> roomRecs = roomReservationRecDao
									.getReservationByOrderNo(customerOrderHd.getOrderNo());
							if (null != roomRecs) {
								RoomReservationRec rec = roomRecs.get(0);
								rec.setStatus(Status.RSV.name());
								rec.setUpdateDate(new Date());
								roomReservationRecDao.update(rec);
							}
						}

					}
				}
				customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
				customerOrderTransDao.saveOrUpdate(customerOrderTrans);
				htmlString = "<html><body  onload='closeWindow();'>payment fail"
						+ " <input type='button' value='ok' onclick='transactionFail()' />" + "<script>"
						+ " function transactionFail() {" + " app.failure();" + " }" + " function closeWindow() {"
						+ " window.close();" + " }" + "</script>" + "</body></html>";
			}

		}
		logger.info("response code to call back :" + htmlString);
		return htmlString;
	}
	@Override
	@Transactional
	public String processResult(HttpServletRequest request, String userId) {
		
		String htmlString = "<html><body onload='closeWindow();'>payment success"
				+ " <input type='button' value='ok' onclick='transactionSuccess()' />" + "<script>" + " function transactionSuccess() {"
				+ " window.close();" + " }" + " function closeWindow() {" + " window.close();" + " }" + "</script>" + "</body></html>";
		
		Map<String, String> fields = new HashMap<String, String>();
		/***
		 * call back reqeust params
		 */
		for (Enumeration enump = request.getParameterNames(); enump.hasMoreElements();) {
			String fieldName = (String) enump.nextElement();
			String fieldValue = request.getParameter(fieldName);
			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				fields.put(fieldName, fieldValue);
			}
		}
		logger.info("call back...."+Log4jFormatUtil.logInfoMessage(request.getRequestURI(), fields.toString(), request.getMethod(), null));
		
		AmexResultDto resultDto=paymentCallBack(fields);
		
		logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, JSONObject.fromObject(resultDto).toString()));
		
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class,
				Long.parseLong(fields.get("vpc_MerchTxnRef")));
		
		if(null!=resultDto&&"0".equals(resultDto.getTxnResponseCode()))
		{
			
			/***
			 * if commit oasis/mms  is success after ,capture()
			 *  
			 */
			logger.info("TxnResponseCode is SUCCESS.. update Transcatioin START..");
			if(!updateTranscation(request, userId, resultDto.getMerchTxnRef(),resultDto.getTxnResponseCode(), resultDto.getTransactionNo(), fields))
			{
				logger.info("TxnResponseCode is SUCCESS.. update Transcatioin status is FAIL..");
				  customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.name());
				  customerOrderTrans.setUpdateBy("System");
				  customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
				  customerOrderTransDao.update(customerOrderTrans);
				  return htmlString;
			}else{
				logger.info("TxnResponseCode is SUCCESS.. update Transcatioin status is SUC..");
			}
			//call capture
			logger.info(" capture to Gateway the param is :"+fields.toString());
			AmexResultDto capResultDto=this.capture(fields);
			if(null!=capResultDto){
				logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, JSONArray.fromObject(capResultDto).toString()));
				
			   if("0".equals(capResultDto.getTxnResponseCode())){
				//set the transaction status in customer_order_trans from PND to SUC. Prompt the patron the payment is success
				   logger.info("update transactin start.......");
//				   updateTranscation(request, userId, capResultDto.getMerchTxnRef(),capResultDto.getTxnResponseCode(), capResultDto.getTransactionNo(), fields);
			  }else{
				  /**
				   * if capture failed, retry 3 times. 
				   * If still failed, prompt the patron the payment is failed 
				   * and leave the authorized payment void after 7 days.
				   *  Set the transaction status in customer_order_trans from PND to FAIL
				   */
				  logger.info("retry 3  times  capture start.......");
				  boolean _3timesSucess=true;
				  for(int i=1;i<=3;i++)
				  {
					  logger.info("retry capture times:"+i);
					  capResultDto=this.capture(fields);
					  logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, capResultDto.toString()));
					  
					  if(null!=capResultDto&&"0".equals(capResultDto.getTxnResponseCode())){
						  logger.info("retry capture "+capResultDto.getTxnResponseCode());
//						  updateTranscation(request, userId, capResultDto.getMerchTxnRef(),capResultDto.getTxnResponseCode(), capResultDto.getTransactionNo(), fields);
						  break;
					  }
					  
				  }
				  logger.info("retry capture 3 times end.......");
			  }
			}else{
				  htmlString = "<html><body  onload='closeWindow();'>"
					    +" the patron the payment is failed <input type='button' value='ok' onclick='transactionFail()' />"
						+ "<script>" + " function transactionFail() {" + " app.failure();" + " }" + " function closeWindow() {" + " window.close();"
						+ " }" + "</script>" + "</body></html>";
			}
		}else{
			logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, "paymentCallBack response result is null.. "));
			if(null!=customerOrderTrans){
				CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
				if (null != customerOrderHd) {
					CustomerOrderDet customerOrderDet = customerOrderDetService.getCustomerOrderDet(customerOrderHd.getOrderNo());
					if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo())
							&& customerOrderDet.getItemNo().contains(Constant.FACILITY_PRICE_PREFIX)) {
						memberFacilityTypeBookingService.voidMemberFacilityTypeBooking(customerOrderTrans.getCustomerOrderHd().getOrderNo(), userId);
					} else if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo())
							&& customerOrderDet.getItemNo().contains(Constant.PMS_ROOMTYPE_ITEM_PREFIX)) {
						// Callback from bank , but bank fails to pay
						// do nothing
						//payment before create reservation Guest room and golf/tennis payment after create reservation
						/***
						 * SGG-3695
						 */
						if("C".equalsIgnoreCase(resultDto.getTxnResponseCode()))
						{
							//need update reservation status is RSV
							List<RoomReservationRec>roomRecs=roomReservationRecDao.getReservationByOrderNo(customerOrderHd.getOrderNo());
							if(null!=roomRecs){
								RoomReservationRec rec=roomRecs.get(0);
								rec.setStatus(Status.RSV.name());
								rec.setUpdateDate(new Date());
								roomReservationRecDao.update(rec);
							}
						}

					}
				}
				customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
				customerOrderTransDao.saveOrUpdate(customerOrderTrans);
				htmlString = "<html><body  onload='closeWindow();'>payment fail" + " <input type='button' value='ok' onclick='transactionFail()' />"
						+ "<script>" + " function transactionFail() {" + " app.failure();" + " }" + " function closeWindow() {" + " window.close();"
						+ " }" + "</script>" + "</body></html>";
			}
			
			
		}
		logger.info("response code to call back :"+htmlString);
		return htmlString;
	}
	private boolean updateTranscation(HttpServletRequest request,String userId,String merchTxnRef,String txnResponseCode,
			String transactionNo,Map<String, String> fields) {
		Boolean blean=Boolean.TRUE;
		String paymentMethod=fields.get("vpc_Card");
		if(paymentMethod !=null && paymentMethod.trim().equals("AE")){
			paymentMethod = PaymentMethod.AMEX.name();
		}
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class,
				Long.parseLong(merchTxnRef));
	    customerOrderTrans.setPaymentMethodCode(paymentMethod);
		customerOrderTrans.setOpgResponseCode(txnResponseCode);
		customerOrderTrans.setAgentTransactionNo(transactionNo);
		customerOrderTrans.setUpdateBy("System");
		customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
		String status = customerOrderTrans.getStatus();
		
		
		if (CustomerTransationStatus.PND.getDesc().equals(status)) {
			customerOrderTrans.setStatus(CustomerTransationStatus.SUC.name());
			customerOrderTransDao.saveOrUpdate(customerOrderTrans);
			logger.info("update CustomerTransationStatus PND to SUC..... The transactionNo :"+customerOrderTrans.getTransactionNo());
			
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			if (null != customerOrderHd) {
				// update member type booking, from PND to RSV
				MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao
						.getMemberFacilityTypeBookingByOrderNo(customerOrderHd.getOrderNo());
				if (memberFacilityTypeBooking != null) {
					memberFacilityTypeBooking.setStatus("RSV");
					memberFacilityTypeBooking.setUpdateDate(new Date());
					memberFacilityTypeBookingDao.update(memberFacilityTypeBooking);
					try {
						facilityEmailService.sendReceiptConfirmationEmail(memberFacilityTypeBooking.getResvId(),
								userId);
					} catch (Exception e) {
						logger.debug("Occurrd exception when send email for facility reservation!", e);
					}
				}

				List<CustomerOrderTrans> customerOrderTranses = customerOrderHd.getCustomerOrderTrans();
				BigDecimal totalAmount = BigDecimal.ZERO;
				for (CustomerOrderTrans tempTrans : customerOrderTranses) {
					if (CustomerTransationStatus.SUC.getDesc().equals(tempTrans.getStatus())) {
						totalAmount = totalAmount.add(tempTrans.getPaidAmount());
					}
				}
				if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount) == 0) {
					customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
					customerOrderHd.setUpdateDate(new Date());
					// customerOrderHd.setUpdateBy(userId);
					customerOrderHdDao.update(customerOrderHd);
				}
				List<CustomerOrderPermitCard> cards = customerOrderHd.getCustomerOrderPermitCards();
				if (null != cards && cards.size() > 0) {
					paymentGatewayService.processDaypassPurchase(cards, customerOrderHd, customerOrderTrans, userId);
				}
				List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
				if (customerOrderDets.size() > 0
						&& Constant.TOPUP_ITEM_NO.equals(customerOrderDets.get(0).getItemNo())) {
					// Process the logic of top up.
					paymentGatewayService.processTopUp(customerOrderTrans);
				} else if (customerOrderDets.size() > 0 && (customerOrderDets.get(0).getItemNo()
						.startsWith(Constant.GOLF_COURSE_PRICE_PREFIX)
						|| customerOrderDets.get(0).getItemNo().startsWith(Constant.TENNIS_COURSE_PRICE_PREFIX))) {
					// Process the logic of payment for tennis.
					paymentGatewayService.processCourseAndTennisPayment(customerOrderHd, userId);
				} else if (customerOrderDets.size() > 0
						&& customerOrderDets.get(0).getItemNo().startsWith(Constant.PMS_ROOMTYPE_ITEM_PREFIX)) {
					// Callback from bank, bank successfully pay, do three
					// things
					/***
					 * when member/ISO payment VISA , after third success send
					 * oasis
					 */
					try {
						if(paymentGatewayService.commitOasisPayment(customerOrderTrans, fields.get("items"))){
							List<RoomReservationRec> bookings = roomReservationRecDao.getByHql(
									"from RoomReservationRec where orderNo = ?",
									Arrays.<Serializable> asList(customerOrderHd.getOrderNo()));
							// 1. Update booking status to SUC
							for (RoomReservationRec booking : bookings) {
								booking.setStatus(RoomReservationStatus.SUC.name());
							}

							// 2. Golfing bay bundle
							List<RoomReservationInfoDto> bundles = new ArrayList<RoomReservationInfoDto>();

							for (RoomReservationRec book : bookings) {
								RoomReservationInfoDto bundle = new RoomReservationInfoDto();
								bundle.setReservationId(book.getConfirmId());
								bundle.setArriveDate(book.getArrivalDate());
								bundle.setDepartDate(book.getDepartureDate());

								bundles.add(bundle);
							}
							Map<String, List<Long>> association = pmsRequestProcessorService
									.giftBundleForReservationsOneNightOneBooking(paymentMethod, customerOrderHd.getCustomerId(),
											bundles);

							// 3. Update room and facility booking association
							hotelReservationService.updateGuessroomAndFacilityAssociation(request.getParameter("userId"),
									association, new HashMap<String, Object>());
							// 4. Send Email
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("userId", request.getParameter("userId"));
							params.put("userName", request.getParameter("userName"));

							hotelReservationServiceFacade.sendGuestroomReservationEmail(customerOrderTrans.getTransactionNo(),
									params);
						}else{
							blean=Boolean.FALSE;
							List<RoomReservationRec> bookings = roomReservationRecDao.getByHql(
									"from RoomReservationRec where orderNo = ?",
									Arrays.<Serializable> asList(customerOrderHd.getOrderNo()));
							// 1. Update booking status to CAN
							for (RoomReservationRec booking : bookings) 
							{
								booking.setStatus(RoomReservationStatus.CAN.name());
								booking.setUpdateDate(new Date());
								roomReservationRecDao.update(booking);
							}
							
							customerOrderHd.setOrderStatus(CustomerTransationStatus.CAN.getDesc());
							customerOrderHd.setUpdateDate(new Date());
							// customerOrderHd.setUpdateBy(userId);
							customerOrderHdDao.update(customerOrderHd);
							
							
						}
					} catch (Exception e) {
						blean=Boolean.FALSE;
						logger.error(e.getMessage());
					}
				} else if (customerOrderDets.size() > 0 && customerOrderDets.get(0).getItemNo().startsWith("MMS")) {
					// added by vicky wang to implement MMS credit card payment
					// after success payment, call MMS API to finish the credit
					// card payment.

					Member member = this.memberDao.getMemberByCustomerId(customerOrderHd.getCustomerId());

					CCPaymentRequest params = new CCPaymentRequest();
					params.setAmountPaid(customerOrderTrans.getPaidAmount());
					params.setCardType(getConfigPaymentCode("MMS", paymentMethod));
					params.setBankName("online");
					params.setGuestCode(member.getAcademyNo());
					params.setInvoiceNo(customerOrderHd.getVendorRefCode());
					CCPaymentResponse response = spaServcieManager.creditCardPayment(params);
					
					logger.debug("payment response from API:" + response);
					
					if (!response.getSuccess()) {
						blean=Boolean.FALSE;
						logger.error(response.getMessage());
						return blean;
					}
					// for MMS payment, the order only canbe closed/completed in
					// MMS.
					customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
					customerOrderHd.setUpdateDate(new Date());
					customerOrderHdDao.update(customerOrderHd);
					// send payment receipt email and message.
					MMSPaymentDto dto = new MMSPaymentDto();
					dto.setInvoiceNo(customerOrderHd.getVendorRefCode());
					dto.setOrderNo(customerOrderHd.getOrderNo().toString());
					try {
						mmsService.sentPaymentReceipt(dto, false);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
					List<SpaAppointmentRec> appointmentList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class,
							"extInvoiceNo", customerOrderHd.getVendorRefCode(), null);
					try {
						mmsService.sentMessage(appointmentList, SceneType.WELLNESS_PAYMENT_SETTLE);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				} else if (customerOrderDets.size() > 0
						&& customerOrderDets.get(0).getItemNo().startsWith(Constant.RENEW_ITEM_NO_PREFIX)) {
					// if the payment is for renew, need to set effective date
					// and expiry date for customer service account.
					paymentGatewayService.handleRenewPayment(customerOrderHd);
					/**
					 * When any status change happens on payment record for
					 * membership renewal, an email notice should be sent to the
					 * Sales Person who is the Membership Officer of the member
					 * enrollment record .
					 */
					paymentGatewayService.sendMailToSalesPerson(Long.parseLong(merchTxnRef));

				}
			}
		}

		return blean;
	}

	@Override
	@Transactional
	public CustomerOrderTrans queryDR(CustomerOrderTrans customerOrderTrans) 
	{
		AmexResultDto resultDto = this
				.queryDR(setAmericanParam(customerOrderTrans.getTransactionNo()));
		
		if (!"0".equals(resultDto.getTxnResponseCode())) {
			customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
//			customerOrderTrans.setOpgResponseCode(resultDto.getTxnResponseCode());
			customerOrderTrans.setAgentTransactionNo(resultDto.getTransactionNo());
			customerOrderTrans.setUpdateBy("System");
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
			return customerOrderTrans;
		}
		return null;
		
	}
	private Map<String, String> setAmericanParam(Long transactionNo) {
		Map<String, String> requestFields = new HashMap<>();
		PaymentGateway paymentGateway = paymentGatewayDao.getUniqueByCol(PaymentGateway.class, "provider",
				PaymentGatewayProvider.AMEX.name());
		Long gatewayId = paymentGateway.getGatewayId();
		String cmdId = PaymentCmd.QUERYDR.getDesc();
		PaymentGatewayCmdPK pk = new PaymentGatewayCmdPK(gatewayId, cmdId);
		PaymentGatewayCmd paymentGatewayCmd = paymentGatewayCmdDao.get(PaymentGatewayCmd.class, pk);
		
		String vpc_AccessCode = paymentGateway.getAccessCode();
		String vpc_Version = appProps.getProperty("hkgta.paymentgateway.vpc_Version");// "1";
		String vpc_Command = paymentGatewayCmd.getCommandType();
		String vpc_Merchant = paymentGateway.getMerchantId();
		String vpc_MerchTxnRef = transactionNo.toString();
		String vpc_User = paymentGateway.getOperatorId();
		String vpc_Password = paymentGateway.getOperatorPassword();
		String vpcURL =paymentGateway.getGatewayUrl() + "/" + paymentGatewayCmd.getUrlCmd();

		requestFields.put("vpc_AccessCode", vpc_AccessCode);
		requestFields.put("vpc_Version", vpc_Version);
		requestFields.put("vpc_Command", vpc_Command);
		requestFields.put("vpc_Merchant", vpc_Merchant);
		requestFields.put("vpc_Password", vpc_Password);
		requestFields.put("vpc_MerchTxnRef", vpc_MerchTxnRef);
		requestFields.put("vpc_User", vpc_User);
		requestFields.put("virtualPaymentClientURL",vpcURL);
		
		return requestFields;
	}
	public static void main(String[] args)throws Exception {
		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, null, null);

		SSLSocketFactory factory = (SSLSocketFactory) context.getSocketFactory();
		SSLSocket socket = (SSLSocket) factory.createSocket();

		String[] protocols = socket.getSupportedProtocols();

		System.out.println("Supported Protocols: " + protocols.length);
		for (int i = 0; i < protocols.length; i++) {
			System.out.println(protocols[i]);
		}

		protocols = socket.getEnabledProtocols();

		System.out.println("Enabled Protocols: " + protocols.length);
		for (int i = 0; i < protocols.length; i++) {
			System.out.println(protocols[i]);
		}

		String[] ciphers = socket.getSupportedCipherSuites();
		System.out.println("Enabled Ciphers: " + ciphers.length);
		for (int i = 0; i < ciphers.length; i++) {
			System.out.println(ciphers[i]);
		}
	}
}
