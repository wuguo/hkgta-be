package com.sinodynamic.hkgta.service.pos;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pos.RestaurantMenuItemDao;
import com.sinodynamic.hkgta.dto.pos.RestaurantMenuItemDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RestaurantMenuItemStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
public class RestaurantMenuItemServiceImpl extends ServiceBase<RestaurantMenuItem> implements RestaurantMenuItemService {

	private static final String[] STATUS_ARRAY = new String[] {RestaurantMenuItemStatus.H.toString(), RestaurantMenuItemStatus.S.toString()};
	
	@Autowired
	private RestaurantMenuItemDao restaurantMenuItemDao;

	@Transactional
	@Override
	public void updateMenuItem(String itemNo, RestaurantMenuItemDto restaurantMenuItemDto, String userId) {
		
		String restaurantId = restaurantMenuItemDto.getRestaurantId();
		String catId = restaurantMenuItemDto.getCatId();
		RestaurantMenuItem restaurantMenuItem = restaurantMenuItemDao.getRestaurantMenuItem(restaurantId, catId, itemNo);
		if (restaurantMenuItem == null) {
			throw new GTACommonException(GTAError.RestaurantError.UNEXPECTED_EXCEPTION);
		}
		restaurantMenuItem.setOnlinePrice(restaurantMenuItemDto.getOnlinePrice());
		restaurantMenuItem.setImageFileName(restaurantMenuItemDto.getImageFileName());
		restaurantMenuItem.setUpdateDate(new Date());
		restaurantMenuItem.setUpdateBy(userId);
		restaurantMenuItemDao.update(restaurantMenuItem);
	}
	
	@Transactional
	@Override
	public void changeStatus(RestaurantMenuItemDto restaurantMenuItemDto, String status, String userId) {
		
		String restaurantId = restaurantMenuItemDto.getRestaurantId();
		String catId = restaurantMenuItemDto.getCatId();
		String itemNo = restaurantMenuItemDto.getItemNo();
		RestaurantMenuItem restaurantMenuItem = restaurantMenuItemDao.getRestaurantMenuItem(restaurantId, catId, itemNo);
		
		if (restaurantMenuItem != null) {
			
			if (!Arrays.asList(STATUS_ARRAY).contains(status)) {
				throw new GTACommonException(GTAError.RestaurantError.UNEXPECTED_EXCEPTION);
			}
			if (RestaurantMenuItemStatus.D.toString().equals(status)) {
				throw new GTACommonException(GTAError.RestaurantError.UNEXPECTED_EXCEPTION);
			}
			restaurantMenuItem.setStatus(status);
			restaurantMenuItem.setUpdateBy(userId);
			restaurantMenuItem.setUpdateDate(new Date());
			restaurantMenuItemDao.update(restaurantMenuItem);
		}
		
	}

	@Transactional
	@Override
	public void deleteMenuItem(RestaurantMenuItemDto restaurantMenuItemDto, String userId) {
		String restaurantId = restaurantMenuItemDto.getRestaurantId();
		String catId = restaurantMenuItemDto.getCatId();
		String itemNo = restaurantMenuItemDto.getItemNo();
		RestaurantMenuItem restaurantMenuItem = restaurantMenuItemDao.getRestaurantMenuItem(restaurantId, catId, itemNo);
		
		if (restaurantMenuItem == null) {
			throw new GTACommonException(GTAError.RestaurantError.UNEXPECTED_EXCEPTION);
		}
		restaurantMenuItem.setStatus(RestaurantMenuItemStatus.D.toString());
		restaurantMenuItem.setUpdateBy(userId);
		restaurantMenuItem.setUpdateDate(new Date());
		restaurantMenuItemDao.update(restaurantMenuItem);
			
	}

	@Transactional
	@Override
	public void changeDisplayOrder(RestaurantMenuItemDto restaurantMenuItemDto, String userId) {
		
		String restaurantId = restaurantMenuItemDto.getRestaurantId();
		String catId = restaurantMenuItemDto.getCatId();
		String itemNo = restaurantMenuItemDto.getItemNo();
		Integer displayOrder = restaurantMenuItemDto.getDisplayOrder();
		RestaurantMenuItem restaurantMenuItem = restaurantMenuItemDao.getRestaurantMenuItem(restaurantId, catId, itemNo);

		if (restaurantMenuItem == null) {
			throw new GTACommonException(GTAError.RestaurantError.UNEXPECTED_EXCEPTION);
		}
		Integer previousDisplayOrder = restaurantMenuItem.getDisplayOrder();
		//swap order
		List<RestaurantMenuItem> restaurantMenuItemList = restaurantMenuItemDao.getRestaurantMenuItembyDisplayOrder(displayOrder);
		if (restaurantMenuItemList != null) {
			for (RestaurantMenuItem swapRestaurantMenuItem : restaurantMenuItemList) {
				swapRestaurantMenuItem.setDisplayOrder(previousDisplayOrder);
				swapRestaurantMenuItem.setUpdateBy(userId);
				swapRestaurantMenuItem.setUpdateDate(new Date());
				restaurantMenuItemDao.update(swapRestaurantMenuItem);
			}
		}
		
		restaurantMenuItem.setDisplayOrder(displayOrder);
		restaurantMenuItem.setUpdateBy(userId);
		restaurantMenuItem.setUpdateDate(new Date());
		restaurantMenuItemDao.update(restaurantMenuItem);
	}

	@Transactional
	@Override
	public Integer[] getMinAndMaxDisplayOrder(String restaurantId, String catId) {
		Integer[] displayOrderArray = new Integer[2];
		
		displayOrderArray[0] = restaurantMenuItemDao.getMinDisplayOrder(restaurantId, catId);
		displayOrderArray[1] = restaurantMenuItemDao.getMaxDisplayOrder(restaurantId, catId);
		return displayOrderArray;
	}



}
