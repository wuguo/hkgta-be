package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.BiMemberCntDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.entity.crm.BiMemberCnt;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class ChangeMemberStatusServiceImpl extends ServiceBase<Member> implements ChangeMemberStatusService {
	
	@Autowired 
	private MemberDao memberDao;
	
	@Autowired 
	private BiMemberCntDao biMemberCntDao;
	
	
	@Transactional
	public ResponseResult updateMemberStatus(String status, Long customerId){
		
		//ResponseResult responseResult = new ResponseResult();
		if(memberDao.updateStatus(status, customerId)){
			//responseResult.setErrorMessageEN("");
			//responseResult.setReturnCode("0");
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else{
			//responseResult.setErrorMessageEN("Update Failed!");
			//responseResult.setReturnCode("1");
			responseResult.initResult(GTAError.MemberError.MEMBER_UPDATE_FAILED);
		}
		return responseResult;
		 
	}
	
	/**
	 * 获取激活/非激活的Member数
	 * @return
	 * @throws Exception
	 */
	@Override
	public Integer getStatisticsActivationOrInactiveMembersNum(boolean isActivation) throws Exception {
		return memberDao.getStatisticsActivationOrInactiveMembersNum(isActivation);
	}
	
	/**
	 * 插入BiMemberCnt
	 * @param bm
	 */
	@Override
	public void seveBiMemberCnt(BiMemberCnt bm){
		biMemberCntDao.save(bm);
	}
	
	/**
	 * 更新BiMemberCnt
	 * @param bm
	 */
	@Override
	public void updateBiMemberCnt(BiMemberCnt bm){
		biMemberCntDao.update(bm);
	}
	/**
	 * 获取BiMemberCnt
	 * @param countDate
	 * @param period
	 * @param isActivation
	 * @return
	 */
	@Override
	public BiMemberCnt getBiMemberCnt(Date countDate,String period, boolean isActivation){
		return biMemberCntDao.getBiMemberCnt(countDate, period, isActivation);
	}

}
