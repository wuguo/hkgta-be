package com.sinodynamic.hkgta.service.crm.setting;

import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface UserPreferenceSettingService extends IServiceBase<UserPreferenceSetting> {

	/***
	 * check user user_preference_setting  ,default is true 
	 * when user set off return false ,else true
	 * @param userId   userId
	 * @param paramId  SMS_NOTIFICATION、BarAndLoungeAlertSwitch and so on
	 * @return
	 */
	public boolean checkUserSettingValue(String userId,String paramId);
	/***
	 * check customerId  user_preference_setting  ,default is true 
	 * when user set off return false ,else true
	 * @param userId   userId
	 * @param paramId  SMS_NOTIFICATION、BarAndLoungeAlertSwitch and so on
	 * @return
	 */
	public boolean sendSMSByCustomerId(Long customerId);

}
