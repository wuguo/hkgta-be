package com.sinodynamic.hkgta.service.onlinepayment;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface PaymentGatewayService extends IServiceBase<PaymentGateway> {

	public String payment(CustomerOrderTrans customerOrderTrans);
	
	public String payment(CustomerOrderTrans customerOrderTrans, Map<String, Object> urlParams);

	public String processResult(HttpServletRequest request,String userId);

	public CustomerOrderTrans queryResult(CustomerOrderTrans customerOrderTrans);
	/***
	 * when user ecr payment ,then commit oasis after callback
	 * @param trans
	 * @param item  (is callback url params ,the format is confrimId|amount||...)
	 */
	public boolean commitOasisPayment(CustomerOrderTrans trans,String item) throws Exception;
	
	public void processDaypassPurchase(List<CustomerOrderPermitCard> cards, CustomerOrderHd customerOrderHd, CustomerOrderTrans customerOrderTrans,String userId);
	
	public void processTopUp(CustomerOrderTrans customerOrderTrans);
	
	public void processCourseAndTennisPayment(CustomerOrderHd customerOrderHd, String userId);
	
	public void handleRenewPayment(CustomerOrderHd customerOrderHd);
	
	public void sendMailToSalesPerson(Long transactionNo);

}
