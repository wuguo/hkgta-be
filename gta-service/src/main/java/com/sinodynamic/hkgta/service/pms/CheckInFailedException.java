package com.sinodynamic.hkgta.service.pms;

public class CheckInFailedException extends RuntimeException {

	public CheckInFailedException() {
		super();
	}

	
	public CheckInFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public CheckInFailedException(String message) {
		super(message);
	}

	public CheckInFailedException(Throwable cause) {
		super(cause);
	}


	
	
}
