package com.sinodynamic.hkgta.service.crm.statement;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.DeliveryRecordDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.DeliveryRecordDetailDto;
import com.sinodynamic.hkgta.dto.crm.DeliveryRecordDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Service
public class DeliveryRecordServiceImpl extends ServiceBase implements DeliveryRecordService {

	@Autowired
	private DeliveryRecordDao deliveryRecordDao;

	@SuppressWarnings("unchecked")
	@Transactional
	public ResponseResult getDeliveryRecord(ListPage page) {
		ListPage<DeliveryRecordDto> listPage = deliveryRecordDao.getDeliveryRecord(page);
		Data data = new Data();
		data.setList(listPage.getDtoList());
		data.setTotalPage(page.getAllPage());
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public ResponseResult getDeliveryRecordDetail(ListPage page, Long jobId, String deliveryStatus) {
		ListPage<DeliveryRecordDetailDto> listPage = deliveryRecordDao.getDeliveryRecordDetailByJobId(page, jobId, deliveryStatus);
		Data data = new Data();
		data.setList(listPage.getDtoList());
		data.setTotalPage(page.getAllPage());
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	public List<AdvanceQueryConditionDto> assembleQueryConditions(String module) {
		SysCode memberTypeOpt1 = new SysCode("Individual","Individual",null);
		SysCode memberTypeOpt2 = new SysCode("Corporate","Corporate",null);
		List<SysCode> memberTypeList = Arrays.asList(memberTypeOpt1,memberTypeOpt2);
		if ("deliveryRecord".equalsIgnoreCase(module)) {
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Delivery Date/Time", "DATE_FORMAT(nativeDeliveryDate,'%Y-%m-%d')", Date.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Job ID", "jobId", Long.class.getName(), "", 2);
			//AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Month of Statement", "Month(nativeMonthOfStatement)", Date.class.getName(), "", 3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Total Count", "totalCount", Long.class.getName(), "", 4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Success Count", "successCount", Long.class.getName(), "", 5);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Failure Count", "failureCount", Long.class.getName(), "", 6);
			AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Send By", "sendBy", String.class.getName(), "", 7);

			return Arrays.asList(condition1, condition2, condition4, condition5, condition6, condition7);
		} else if ("deliveryRecordDetail".equalsIgnoreCase(module)) {
			SysCode deliveryRecordDetailStatus1 = new SysCode("Success","Success",null);
			SysCode deliveryRecordDetailStatu2 = new SysCode("Failure","Failure",null);
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID", "academyId", String.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron", "member", String.class.getName(), "", 2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Patron Type", "memberType", String.class.getName(), memberTypeList, 3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Balance", "balance", BigDecimal.class.getName(), "", 4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Delivery Status", "deliveryStatus", String.class.getName(), Arrays.asList(deliveryRecordDetailStatus1,deliveryRecordDetailStatu2), 5);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("SMTP Error Code", "smtpErrorCode", String.class.getName(), "", 6);

			return Arrays.asList(condition1, condition2, condition3, condition4, condition5, condition6);
		} else if ("monthlyStatement".equalsIgnoreCase(module)) {
			SysCode deliveryStatusOpt1 = new SysCode("Yes","Yes",null);
			SysCode deliveryStatusOpt2 = new SysCode("No","No",null);
			
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID", "academyId", String.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron", "memberName", String.class.getName(), "", 2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Patron Email", "memberEmail", String.class.getName(), "", 3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Patron Type", "memberType", String.class.getName(), memberTypeList, 4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Cash Value Closing Balance", "closingBalance", BigDecimal.class.getName(), "", 5);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Delivery Status", "deliveryStatus", String.class.getName(), Arrays.asList(deliveryStatusOpt1,deliveryStatusOpt2), 6);
			return Arrays.asList(condition1, condition2, condition3, condition4, condition5,condition6);
		}
		return new ArrayList<AdvanceQueryConditionDto>();

	}
	
	@Transactional
	public boolean updateErrorCountInBatchSendStatementHd(Long batchId){
		return deliveryRecordDao.updateErrorCountInBatchSendStatementHd(batchId);
	}
}
