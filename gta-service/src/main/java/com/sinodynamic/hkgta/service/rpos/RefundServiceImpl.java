
package com.sinodynamic.hkgta.service.rpos;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.AbstractCommonDataQueryDao;
import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerOrderDetDto;
import com.sinodynamic.hkgta.dto.crm.DaypassRefundDto;
import com.sinodynamic.hkgta.dto.crm.GuessRoomReservationRefundDto;
import com.sinodynamic.hkgta.dto.fms.CustomerRefundRequestDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;
import com.sinodynamic.hkgta.dto.rpos.PaymentInfoDto;
import com.sinodynamic.hkgta.dto.rpos.RefundInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.cardmanage.PCDayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.pms.HotelReservationService;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.RefundServiceType;
import com.sinodynamic.hkgta.util.constant.ReservationType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RefundServiceImpl extends ServiceBase<CustomerRefundRequest>
		implements RefundService {

	static String SYSTEM = "SYSTEM";
	
	@Autowired
	CustomerRefundRequestDao customerRefundRequestDao;
	
	@Autowired
	@Qualifier("commonBySQLDao")
	AbstractCommonDataQueryDao abstractCommonDataQueryDao;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	@Autowired
	private MemberDao memberDao;
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	@Autowired
	private MessageTemplateDao templateDao;
	@Autowired
	private CustomerProfileDao customerProfileDao;
	@Autowired
	private PCDayPassPurchaseService pcDayPassPurchaseService;
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	@Autowired
	private HotelReservationService hotelReservationService;
	
	@Autowired
	private GlobalParameterDao globalParameterDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private MessageTemplateService messageTemplateService;
	
	@Override
	@Transactional
	public Data getPendingRefundList(Date base, int days,
			String orderColumn, String order, int pageSize, int currentPage,
			AdvanceQueryDto filters) {
		try{
			Data data = new Data();
			String filterSql = null;
			if(filters != null){
				filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
			}
			ListPage<CustomerRefundRequest> listPage = customerRefundRequestDao.getPendingRefundList(base, days, orderColumn,  order,  pageSize,  currentPage,
					filterSql);
			
			data.setCurrentPage(currentPage);
			data.setPageSize(pageSize);
			data.setTotalPage(listPage.getAllPage());
			data.setLastPage(currentPage==listPage.getAllPage());
			data.setRecordCount(listPage.getAllSize());
			
			data.setList(listPage.getDtoList());
			
			return data;
		}catch (Exception e){
			logger.error(e.getMessage());
			throw new GTACommonException(GTAError.RefundError.REFUND_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}

	@Override
	@Transactional
	public Data getServiceRefundList(String status, Long customerId, String orderColumn,
			String order, int pageSize, int currentPage, AdvanceQueryDto filters) {
		try{
			Data data = new Data();
			String filterSql = null;
			if(filters != null){
				filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
			}
			ListPage<CustomerRefundRequest> listPage = customerRefundRequestDao.getServiceRefundList(status, customerId, orderColumn,  order,  pageSize,  currentPage,
					filterSql);
			
			data.setCurrentPage(currentPage);
			data.setPageSize(pageSize);
			data.setTotalPage(listPage.getAllPage());
			data.setLastPage(currentPage==listPage.getAllPage());
			data.setRecordCount(listPage.getAllSize());
			
			data.setList(listPage.getDtoList());
			
			return data;
		}catch (Exception e){
			logger.error(e.getMessage());
			throw new GTACommonException(GTAError.RefundError.REFUND_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}
	
	@Override
	@Transactional
	public Data getCashValueRefundList(Date base, Integer days,String status,Long customerId,
			String orderColumn, String order, int pageSize, int currentPage,
			AdvanceQueryDto filters) {
		try{
			Data data = new Data();
			String filterSql = null;
			if(filters != null){
				filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
			}
			ListPage<CustomerRefundRequest> listPage = customerRefundRequestDao.getCashValueRefundList(base,days,status, customerId, orderColumn,  order,  pageSize,  currentPage,
					filterSql);
			
			data.setCurrentPage(currentPage);
			data.setPageSize(pageSize);
			data.setTotalPage(listPage.getAllPage());
			data.setLastPage(currentPage==listPage.getAllPage());
			data.setRecordCount(listPage.getAllSize());
			
			data.setList(listPage.getDtoList());
			
			return data;
		}catch (Exception e){
			logger.error(e.getMessage());
			throw new GTACommonException(GTAError.RefundError.REFUND_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}
	
	@Override
	public List<AdvanceQueryConditionDto> advanceSearch() {
		return this.customerRefundRequestDao.assembleQueryConditions();
	}
	
	@Override
	public List<AdvanceQueryConditionDto> cashValueAdvanceSearch() {
		return this.customerRefundRequestDao.assembleQueryConditions(null);
	}
	
	
	@Override
	public List<AdvanceQueryConditionDto> serviceRefundTxnAdvanceSearch() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date/Time", "requestDate", "java.util.Date", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Transaction ID", "refundTransactionNo", "java.lang.Long", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Refund From", "refundFrom", "java.lang.String", "", 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Patron", "memberName", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Refund", "approvedAmt", "java.math.BigDecimal", "", 5);
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5);
	}
	
	@Override
	public List<AdvanceQueryConditionDto> cashValueRefundTxnAdvanceSearch() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date/Time", "requestDate", "java.util.Date", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Transaction ID", "refundTransactionNo", "java.lang.Long", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Refund Method", "refundMethod", "java.lang.String", "", 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Approved By", "approvedBy", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Ref #", "refNo", "java.lang.String", "", 5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Request Amount", "requestAmt", "java.math.BigDecimal", "", 6);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Refund Amount", "approvedAmt", "java.math.BigDecimal", "", 7);
		
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5
				,condition6
				,condition7);
	}

	@Override
	@Transactional
	public PaymentInfoDto getPaymentInfo(Long refundId) {
		try{
			return this.customerRefundRequestDao.getPaymentInfo(refundId);
		}catch (Exception e){
			logger.error(e.getMessage());
			throw new GTACommonException(GTAError.RefundError.REFUND_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}

	@Override
	@Transactional
	public RefundInfoDto getRefundInfo(Long refundId) {
		try{
			return this.customerRefundRequestDao.getRefundInfo(refundId);
		}catch (Exception e){
			logger.error(e.getMessage());
			throw new GTACommonException(GTAError.RefundError.REFUND_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}
	@Override
	@Transactional
	public void directionalRefund(final CustomerOrderTrans txn, final RefundServiceType serviceType, final BigDecimal actualRefundAmt, final String loginUserId, Map<String, Object> params) {
		CallBackExecutor executor = new CallBackExecutor(RefundServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				
				CustomerOrderHd currentOrder = txn.getCustomerOrderHd();
				Long customerId = currentOrder.getCustomerId();
				
				// 1.1 do refund to cash value
					
				MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
				
				if(actualRefundAmt != null){
					Assert.isTrue(actualRefundAmt.compareTo(BigDecimal.ZERO)>=0, "Refund amount can't be less than zeror");
				}
				
				BigDecimal refundAmt = actualRefundAmt == null ? txn.getPaidAmount() : actualRefundAmt;
				
				BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().add(refundAmt);
				memberCashvalue.setAvailableBalance(updatedAmount);
				memberCashValueDao.updateMemberCashValue(memberCashvalue);
				
				
				// 1.2 create a new order for refund
				Long fromTransactionNo = txn.getTransactionNo();
				//Map<String, String> codeMapping = new HashMap<String, String>();
				//codeMapping.put("CASHVALUE", Constant.CASH_Value);
				//codeMapping.put("CASH", Constant.CASH);
				
				// CUSTOMER ORDER Header
				Date currentDate = new Date();
				CustomerOrderHd topUpOrder = new CustomerOrderHd();
				topUpOrder.setOrderDate(currentDate);
				topUpOrder.setOrderStatus(Constant.Status.CMP.toString());
				topUpOrder.setStaffUserId(loginUserId);
				topUpOrder.setCustomerId(customerId);
				topUpOrder.setOrderTotalAmount(refundAmt);
				topUpOrder.setOrderRemark(Constant.REFUND_SERVICE_REMARK);
				topUpOrder.setCreateDate(new Timestamp(currentDate.getTime()));
				topUpOrder.setCreateBy(loginUserId);
				topUpOrder.setUpdateBy(loginUserId);
				topUpOrder.setUpdateDate(currentDate);
				customerOrderHdDao.save(topUpOrder);

				// CUSTOMER ORDER Detail
				CustomerOrderDet customerOrderDet = new CustomerOrderDet();
				PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.getByItemNo(Constant.TOPUP_ITEM_NO);
				if(posServiceItemPrice == null ){
					PosServiceItemPrice psip = new PosServiceItemPrice();
					psip.setStatus(Constant.Status.ACT.toString());
					psip.setItemNo(Constant.TOPUP_ITEM_NO);
					psip.setItemCatagory(Constant.PriceCatagory.TOPUP.toString());
					psip.setDescription("Top up for cash value");
					posServiceItemPriceDao.save(psip);
				}
				customerOrderDet.setCustomerOrderHd(topUpOrder);
				customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
				customerOrderDet.setItemRemark(Constant.REFUND_SERVICE_REMARK);
				customerOrderDet.setOrderQty(1L);
				customerOrderDet.setItemTotalAmout(refundAmt);
				customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
				customerOrderDet.setCreateBy(loginUserId);
				customerOrderDet.setUpdateDate(currentDate);
				customerOrderDet.setUpdateBy(loginUserId);
				customerOrderDetDao.saveOrderDet(customerOrderDet);

				// CUSTOMER ORDER TRANS
				CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
				initCustomerOrderTrans(customerOrderTrans);
				customerOrderTrans.setCustomerOrderHd(topUpOrder);
				customerOrderTrans.setTransactionTimestamp(currentDate);
				customerOrderTrans.setPaidAmount(refundAmt);
				customerOrderTrans.setFromTransactionNo(fromTransactionNo);
				customerOrderTrans.setStatus(Constant.Status.RFU.toString());
				customerOrderTrans.setPaymentRecvBy(loginUserId);
				customerOrderTrans.setInternalRemark(Constant.REFUND_SERVICE_REMARK);
				customerOrderTrans.setPaymentMethodCode(Constant.PaymentMethodCode.REFUND.toString());
				customerOrderTransDao.save(customerOrderTrans);
				
				//REFUND RECORD
				CustomerRefundRequest refundRequest = new CustomerRefundRequest();
				refundRequest.setRequesterType(Constant.BizParties.HKGTA.toString());
				refundRequest.setRefundTransactionNo(customerOrderTrans.getTransactionNo());
				refundRequest.setRefundServiceType(serviceType.toString());
				//refundRequest.setCustomerReason(remark);
				refundRequest.setStatus(Constant.CustomerRefundRequest.APR.toString());
				refundRequest.setRequestAmount(refundAmt);
				refundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
				
				refundRequest.setInternalRemark(Constant.REFUND_SERVICE_REMARK);
				refundRequest.setApprovedAmount(refundAmt);
				refundRequest.setAuditDate(DateFormatUtils.format(currentDate, "yyyy-MM-dd"));
				refundRequest.setAuditorUserId(SYSTEM);
				
				refundRequest.setCreateDate(currentDate);
				refundRequest.setCreateBy(SYSTEM);
				refundRequest.setUpdateDate(currentDate);
				refundRequest.setUpdateBy(SYSTEM);
				
				customerRefundRequestDao.save(refundRequest);
				return null;
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,this.getException().getMessage());
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
		
	}
	
	@Override
	@Transactional
	public void refund(final Long refundId, final BigDecimal refundAmt, final String loginUserId, final String remark) {
		CallBackExecutor executor = new CallBackExecutor(RefundServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				CustomerRefundRequest refundRequest = customerRefundRequestDao.get(CustomerRefundRequest.class, refundId);
				Assert.notNull(refundRequest, "No such refund id");
				CustomerOrderTrans transaction = customerOrderTransDao.get(CustomerOrderTrans.class, refundRequest.getRefundTransactionNo());
				Assert.notNull(transaction, "No such transaction id");
				
				if(Constant.CustomerRefundRequest.APR.toString().equals(refundRequest.getStatus())){
					Assert.notNull(null, "This transaction has been refunded");
				}else{
					CustomerOrderHd currentOrder = transaction.getCustomerOrderHd();
					Long customerId = currentOrder.getCustomerId();
					
					// 1.1 do refund to cash value
						
					MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
					
					BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().add(refundAmt);
					memberCashvalue.setAvailableBalance(updatedAmount);
					memberCashValueDao.updateMemberCashValue(memberCashvalue);
					
					
					// 1.2 create a new order for refund
					Long fromTransactionNo = transaction.getTransactionNo();
					//Map<String, String> codeMapping = new HashMap<String, String>();
					//codeMapping.put("CASHVALUE", Constant.CASH_Value);
					//codeMapping.put("CASH", Constant.CASH);
					
					// CUSTOMER ORDER Header
					Date currentDate = new Date();
					CustomerOrderHd topUpOrder = new CustomerOrderHd();
					topUpOrder.setOrderDate(currentDate);
					topUpOrder.setOrderStatus(Constant.Status.CMP.toString());
					topUpOrder.setStaffUserId(loginUserId);
					topUpOrder.setCustomerId(customerId);
					topUpOrder.setOrderTotalAmount(refundAmt);
					topUpOrder.setOrderRemark(Constant.REFUND_SERVICE_REMARK);
					topUpOrder.setCreateDate(new Timestamp(currentDate.getTime()));
					topUpOrder.setCreateBy(loginUserId);
					topUpOrder.setUpdateBy(loginUserId);
					topUpOrder.setUpdateDate(currentDate);
					customerOrderHdDao.save(topUpOrder);

					// CUSTOMER ORDER Detail
					CustomerOrderDet customerOrderDet = new CustomerOrderDet();
					PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.getByItemNo(Constant.SRV_REFUND_ITEM_NO);
					if(posServiceItemPrice == null ){
						PosServiceItemPrice psip = new PosServiceItemPrice();
						psip.setStatus(Constant.Status.ACT.toString());
						psip.setItemNo(Constant.SRV_REFUND_ITEM_NO);
						psip.setItemCatagory(Constant.PriceCatagory.REFUND.toString());
						psip.setDescription(Constant.REFUND_SERVICE_REMARK);
						posServiceItemPriceDao.save(psip);
					}
					customerOrderDet.setCustomerOrderHd(topUpOrder);
					customerOrderDet.setItemNo(Constant.SRV_REFUND_ITEM_NO);
					customerOrderDet.setItemRemark(Constant.REFUND_SERVICE_REMARK);
					customerOrderDet.setOrderQty(1L);
					customerOrderDet.setItemTotalAmout(refundAmt);
					customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
					customerOrderDet.setCreateBy(loginUserId);
					customerOrderDet.setUpdateDate(currentDate);
					customerOrderDet.setUpdateBy(loginUserId);
					//set refuned orderDelId to new customer order trans order detail ExtRefNo
					 if(ReservationType.ROOM.getDesc().toUpperCase().equals(refundRequest.getRefundServiceType())){
						 if(null!=refundRequest)
							{
								CustomerOrderDet orderDet=customerOrderDetDao.get(CustomerOrderDet.class, refundRequest.getOrderDetailId());
								if(null!=orderDet)
								{
									 customerOrderDet.setExtRefNo(orderDet.getExtRefNo());
								}
							}
					   }
					customerOrderDetDao.saveOrderDet(customerOrderDet);

					// CUSTOMER ORDER TRANS
					CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
					initCustomerOrderTrans(customerOrderTrans);
					customerOrderTrans.setCustomerOrderHd(topUpOrder);
					customerOrderTrans.setTransactionTimestamp(currentDate);
					customerOrderTrans.setPaidAmount(refundAmt);
					customerOrderTrans.setFromTransactionNo(fromTransactionNo);
					customerOrderTrans.setStatus(Constant.Status.RFU.toString());
					customerOrderTrans.setPaymentRecvBy(loginUserId);
					customerOrderTrans.setInternalRemark(Constant.REFUND_SERVICE_REMARK);
					customerOrderTrans.setPaymentMethodCode(Constant.PaymentMethodCode.CASHVALUE.toString());
					//if (topUpDto.getPaymentMethod().equalsIgnoreCase("VISA")) {
					//	customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
					//	customerOrderTrans.setTerminalId(terminalId);
					//	customerOrderTrans.setPaymentMethodCode("VISA");
					//	customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
					//} else {
					//	customerOrderTrans.setPaymentMethodCode("CASH");
					//}
					customerOrderTransDao.save(customerOrderTrans);
					
					//CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
					//transaction.setCustomerOrderHd(customerOrderHd);
					//transaction.setPaymentMethodCode(codeMapping.get(refundRequest.getRefundMoneyType()));
					//transaction.setTransactionTimestamp(new Timestamp(new Date().getTime()));
					//transaction.setPaidAmount(refundAmt);
					//transaction.setStatus(Constant.Status.RFU.toString());
					//transaction.setFromTransactionNo(fromTransactionNo);
					//transaction.setPaymentRecvBy(loginUserId);
					//transaction.setInternalRemark(Constant.INTERNAL_REMARK_REFUND);
					//customerOrderTransDao.update(transaction);
					
					// 1.3 update refund request
					refundRequest.setStatus(Constant.CustomerRefundRequest.APR.toString());
					refundRequest.setApprovedAmount(refundAmt);
					refundRequest.setUpdateBy(loginUserId);
					refundRequest.setUpdateDate(new Date());
					refundRequest.setInternalRemark(remark);
					refundRequest.setAuditDate(DateFormatUtils.format(currentDate, "yyyy-MM-dd"));
					refundRequest.setAuditorUserId(loginUserId);
					customerRefundRequestDao.update(refundRequest);
				}
				return null;
			}

			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,this.getException().getMessage());
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
			
			
		});
		
	}
	@Override
	@Transactional
	public void refund(final Long fromTransactionNo,final Long customerId,final BigDecimal refundAmt, final String loginUserId, final String remark) {
		CallBackExecutor executor = new CallBackExecutor(RefundServiceImpl.class);
		executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
					MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
					BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().add(refundAmt);
					memberCashvalue.setAvailableBalance(updatedAmount);
					memberCashValueDao.updateMemberCashValue(memberCashvalue);
					
					Date currentDate = new Date();
					CustomerOrderHd topUpOrder = new CustomerOrderHd();
					topUpOrder.setOrderDate(currentDate);
					topUpOrder.setOrderStatus(Constant.Status.CMP.toString());
					topUpOrder.setStaffUserId(loginUserId);
					topUpOrder.setCustomerId(customerId);
					topUpOrder.setOrderTotalAmount(refundAmt);
					topUpOrder.setOrderRemark(Constant.REFUND_SERVICE_REMARK);
					topUpOrder.setCreateDate(new Timestamp(currentDate.getTime()));
					topUpOrder.setCreateBy(loginUserId);
					topUpOrder.setUpdateBy(loginUserId);
					topUpOrder.setUpdateDate(currentDate);
					customerOrderHdDao.save(topUpOrder);

					// CUSTOMER ORDER Detail
					CustomerOrderDet customerOrderDet = new CustomerOrderDet();
					PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.getByItemNo(Constant.SRV_REFUND_ITEM_NO);
					if(posServiceItemPrice == null ){
						PosServiceItemPrice psip = new PosServiceItemPrice();
						psip.setStatus(Constant.Status.ACT.toString());
						psip.setItemNo(Constant.SRV_REFUND_ITEM_NO);
						psip.setItemCatagory(Constant.PriceCatagory.REFUND.toString());
						psip.setDescription(Constant.REFUND_SERVICE_REMARK);
						posServiceItemPriceDao.save(psip);
					}
					customerOrderDet.setCustomerOrderHd(topUpOrder);
					customerOrderDet.setItemNo(Constant.SRV_REFUND_ITEM_NO);
					customerOrderDet.setItemRemark(Constant.REFUND_SERVICE_REMARK);
					customerOrderDet.setOrderQty(1L);
					customerOrderDet.setItemTotalAmout(refundAmt);
					customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
					customerOrderDet.setCreateBy(loginUserId);
					customerOrderDet.setUpdateDate(currentDate);
					customerOrderDet.setUpdateBy(loginUserId);
					customerOrderDetDao.saveOrderDet(customerOrderDet);

					// CUSTOMER ORDER TRANS
					CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
					initCustomerOrderTrans(customerOrderTrans);
					customerOrderTrans.setCustomerOrderHd(topUpOrder);
					customerOrderTrans.setTransactionTimestamp(currentDate);
					customerOrderTrans.setPaidAmount(refundAmt);
					customerOrderTrans.setFromTransactionNo(fromTransactionNo);
					customerOrderTrans.setStatus(Constant.Status.RFU.toString());
					customerOrderTrans.setPaymentRecvBy(loginUserId);
					customerOrderTrans.setInternalRemark(Constant.REFUND_SERVICE_REMARK);
					customerOrderTrans.setPaymentMethodCode(Constant.PaymentMethodCode.CASHVALUE.toString());
					customerOrderTransDao.save(customerOrderTrans);
				   return null;
			}
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,this.getException().getMessage());
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
		
	}
	
	private MemberCashvalue getMemberCashvalue(Long customerId) {
		Member member = memberDao.get(Member.class, customerId);
		
		if (member == null) {
			throw new GTACommonException(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
		}
		
		MemberCashvalue memberCashvalue = null;
		String type = member.getMemberType();

		if (Constant.memberType.CDM.toString().equals(type) || Constant.memberType.IDM.toString().equals(type)) {
			Long superiorMemberId = member.getSuperiorMemberId();

			if (superiorMemberId != null) {
				memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
			}
		} else {
			memberCashvalue = memberCashValueDao.getMemberCashvalueById(customerId);
		}

		if (memberCashvalue == null) {
			logger.error("member cash value not found for customer" + customerId);
			throw new GTACommonException(GTAError.FacilityError.REFUND_NOT_AVAILABLE);
		}
		return memberCashvalue;

	}

	GuessRoomReservationRefundDto getGuessRoomReservationRefund(Long refundId){
		CustomerRefundRequest refundRequest = customerRefundRequestDao.get(CustomerRefundRequest.class, refundId);
		CustomerOrderDet detail = customerOrderDetDao.get(CustomerOrderDet.class, refundRequest.getOrderDetailId());
		GuessRoomReservationRefundDto refundDto = new GuessRoomReservationRefundDto();
		refundDto.setGuessRoomReservationDetail(hotelReservationService.getHotelReservationDetail(detail.getExtRefNo()));
		return refundDto;
	}
	
	@Override
	@Transactional
	public ResponseResult getBookingDetail(Long refundId, RefundServiceType type)
	{
		switch (type)
		{
		case TCH:
		case GCH:
				{
					String bookingType = type.equals(RefundServiceType.TCH) ? "TENNIS" : "GOLF";
					PrivateCoachBookListDto booking = customerRefundRequestDao.getPrivateCoachBookingDetail(refundId, bookingType);
					this.responseResult.initResult(GTAError.Success.SUCCESS, booking);
					break;
				}
		case GMS:
		case TMS:
				{
					FacilityReservationDto booking = customerRefundRequestDao.getFacilityBookingDetail(refundId);
					this.responseResult.initResult(GTAError.Success.SUCCESS, booking);
					break;
				}
		case GSS:
		case TSS:
				{
					CourseEnrollment courseEnrollment = customerRefundRequestDao.getEnrollment(refundId);
					this.responseResult.initResult(GTAError.Success.SUCCESS, courseEnrollment);
					break;
				}
		case SRV:
			DaypassRefundDto daypassRefundDto = customerRefundRequestDao.getDaypassBookingDetail(refundId);
			Map<String, CustomerOrderDetDto> map = pcDayPassPurchaseService.getPurchaseDaypassList(daypassRefundDto.getOrderNo());
			daypassRefundDto.setItemMap(map);
			this.responseResult.initResult(GTAError.Success.SUCCESS, daypassRefundDto);
			break;
		case ROOM:
			
			this.responseResult.initResult(GTAError.Success.SUCCESS, getGuessRoomReservationRefund(refundId));
			break;
		case WELLNESS:
			
			this.responseResult.initResult(GTAError.Success.SUCCESS, this.customerRefundRequestDao.getWellnessRefundDetail(refundId));
			break;
	}
		return responseResult;
	}

	@Override
	@Transactional
	public void requestCashValueRefund(Long customerId, BigDecimal requestedAmt,
			String loginUserId, String remark) {
		try{
			
			//Check cash value
			MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
			BigDecimal deductedAmount = memberCashvalue.getAvailableBalance().subtract(requestedAmt);
			Assert.isTrue(deductedAmount.compareTo(BigDecimal.ZERO)>=0, "The request amount can't be greater than Cash Value Balance");
			
			// CUSTOMER ORDER Header
			Date currentDate = new Date();
			CustomerOrderHd refundOrder = new CustomerOrderHd();
			refundOrder.setOrderDate(currentDate);
			refundOrder.setOrderStatus(Constant.Status.OPN.toString());
			refundOrder.setStaffUserId(loginUserId);
			refundOrder.setCustomerId(customerId);
			refundOrder.setOrderTotalAmount(requestedAmt);
			refundOrder.setOrderRemark(Constant.REFUND_CASH_VALUE_REMARK);
			refundOrder.setCreateDate(new Timestamp(currentDate.getTime()));
			refundOrder.setCreateBy(loginUserId);
			refundOrder.setUpdateBy(loginUserId);
			refundOrder.setUpdateDate(currentDate);
			customerOrderHdDao.save(refundOrder);

			// CUSTOMER ORDER Detail
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.getByItemNo(Constant.REFUND_ITEM_NO);
			if(posServiceItemPrice == null ){
				PosServiceItemPrice psip = new PosServiceItemPrice();
				psip.setStatus(Constant.Status.ACT.toString());
				psip.setItemNo(Constant.REFUND_ITEM_NO);
				psip.setItemCatagory(Constant.PriceCatagory.REFUND.toString());
				psip.setDescription("Refund for cash value");
				posServiceItemPriceDao.save(psip);
			}
			customerOrderDet.setCustomerOrderHd(refundOrder);
			customerOrderDet.setItemNo(Constant.REFUND_ITEM_NO);
			customerOrderDet.setItemRemark(Constant.REFUND_CASH_VALUE_REMARK);
			customerOrderDet.setOrderQty(1L);
			customerOrderDet.setItemTotalAmout(requestedAmt);
			customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderDet.setCreateBy(loginUserId);
			customerOrderDet.setUpdateDate(currentDate);
			customerOrderDet.setUpdateBy(loginUserId);
			customerOrderDetDao.saveOrderDet(customerOrderDet);

			// CUSTOMER ORDER TRANS
			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			initCustomerOrderTrans(customerOrderTrans);
			customerOrderTrans.setCustomerOrderHd(refundOrder);
			customerOrderTrans.setTransactionTimestamp(currentDate);
			//customerOrderTrans.setPaidAmount(requestedAmt);
			
			//customerOrderTrans.setStatus(Constant.Status.RFU.toString());
			customerOrderTrans.setPaymentRecvBy(loginUserId);
			customerOrderTrans.setInternalRemark(Constant.REFUND_CASH_VALUE_REMARK);
			customerOrderTrans.setPaymentMethodCode(Constant.PaymentMethodCode.CASHVALUE.toString()); 
			customerOrderTransDao.save(customerOrderTrans);
			customerOrderTrans.setFromTransactionNo(customerOrderTrans.getTransactionNo());
			customerOrderTransDao.update(customerOrderTrans);
			
			CustomerRefundRequest refundRequest = new CustomerRefundRequest();
			refundRequest.setRequesterType(Constant.BizParties.CUSTOMER.toString());
			refundRequest.setRefundTransactionNo(customerOrderTrans.getTransactionNo());
			refundRequest.setRefundServiceType(RefundServiceType.CVL.toString());
			refundRequest.setCustomerReason(remark);
			refundRequest.setStatus(Constant.CustomerRefundRequest.PND.toString());
			refundRequest.setRequestAmount(requestedAmt);
			refundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASH.toString());
			refundRequest.setCreateDate(currentDate);
			refundRequest.setCreateBy(loginUserId);
			refundRequest.setUpdateDate(currentDate);
			refundRequest.setUpdateBy(loginUserId);
			
			customerRefundRequestDao.save(refundRequest);
		}catch(Exception e){
			logger.error(e.getMessage());
			throw new GTACommonException(GTAError.RefundError.REFUND_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}

	@Override
	@Transactional
	public void cashValueRefund(Long refundId, BigDecimal refundAmt,
			String loginUserId, String remark, String refNo) {
		
			CustomerRefundRequest refundRequest = customerRefundRequestDao.get(CustomerRefundRequest.class, refundId);
			if(null==refundRequest){
				throw new GTACommonException(GTAError.RefundError.NO_FIND_REFUND);
			}
			CustomerOrderTrans transaction = customerOrderTransDao.get(CustomerOrderTrans.class, refundRequest.getRefundTransactionNo());
			if(null==transaction){
				throw new GTACommonException(GTAError.RefundError.NO_FIND_TRANSCATION);
			}
			Date currentDate = new Date();
			CustomerOrderHd currentOrder = transaction.getCustomerOrderHd();
			Long customerId = currentOrder.getCustomerId();
			//Update order
			currentOrder.setUpdateBy(loginUserId);
			currentOrder.setUpdateDate(currentDate);
			currentOrder.setOrderStatus(Constant.Status.CMP.toString());
			
			//Update transaction
			transaction.setAgentTransactionNo(refNo);
			transaction.setStatus(Constant.Status.RFU.toString());
			transaction.setPaidAmount(refundAmt);
			transaction.setUpdateDate(new Timestamp(new Date().getTime()));
			transaction.setUpdateBy(loginUserId);
			
			//Update refund request
			refundRequest.setStatus(Constant.CustomerRefundRequest.APR.toString());
			refundRequest.setInternalRemark(remark);
			refundRequest.setApprovedAmount(refundAmt);
			refundRequest.setUpdateBy(loginUserId);
			refundRequest.setUpdateDate(currentDate);
			refundRequest.setAuditDate(DateFormatUtils.format(currentDate, "yyyy-MM-dd"));
			refundRequest.setAuditorUserId(loginUserId);
			
			//Update cash value
			MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
			BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().subtract(refundAmt);
			if(updatedAmount.compareTo(BigDecimal.ZERO)<0){
				throw new GTACommonException(GTAError.RefundError.NO_CASHVALU_EBALANCE);
			}
			memberCashvalue.setAvailableBalance(updatedAmount);
		try{
			memberCashValueDao.updateMemberCashValue(memberCashvalue);
		}catch(Exception e){
			
			throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			
		}
		
	}
	/**   
	* @author: Zero_Wang
	* @since: Aug 6, 2015
	* 
	* @description
	* write the description here
	*/  
	@Transactional
	@Override
	public void sendRefundResultEmail(long refundId) {

		if (StringUtils.isEmpty(refundId)) {
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"Refund Confirmation : "+refundId});
		}

		MessageTemplate template = null;
		CustomerRefundRequest refundRequest = this.customerRefundRequestDao.get(CustomerRefundRequest.class, refundId);
		String templateContent = "";
		String content = "";
		if("CVL".equals(refundRequest.getRefundServiceType())){
			template = templateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_CVCL);
			if (template == null) {
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Refund Confirmation e-mail: cvcl"});
			}
			templateContent = template.getContentHtml();
		}else {
			template = templateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_SRCL);
			if (template == null) {
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Refund Confirmation e-mail: srcl"});
			}
			templateContent = template.getContentHtml();
		}
		CustomerOrderTrans trans = this.customerOrderTransDao.get(CustomerOrderTrans.class, refundRequest.getRefundTransactionNo());
		CustomerOrderHd orderHd = trans.getCustomerOrderHd();
		CustomerProfile cp = customerProfileDao.getById(orderHd.getCustomerId());
		if (cp == null) {
			throw new GTACommonException(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_CUSTOMER_PROFILE);
		}
		String salutation = cp.getSalutation();
		String lastName = cp.getSurname();
		String firstName = cp.getGivenName();
		String fullName = getName(cp);
		String refundStatus = "";
		String resultMessage = "";
		if(Constant.CustomerRefundRequest.APR.getName().equals(refundRequest.getStatus())){
			refundStatus = "Success";
			resultMessage = "processed successfully";
		}else if(Constant.CustomerRefundRequest.REJ.getName().equals(refundRequest.getStatus())){
			refundStatus = "Fail";
			resultMessage = "rejected";
		}
		String serviceType = RefundServiceType.valueOf(refundRequest.getRefundServiceType()).getDesc();
		String fefundedDate = refundRequest.getAuditDate();
		String refundAmount = refundRequest.getApprovedAmount().toString();
		String refundMethod = "";
		if("CASHVALUE".equalsIgnoreCase(refundRequest.getRefundMoneyType())){
			refundMethod = "cash value";
		}else if("CASH".equalsIgnoreCase(refundRequest.getRefundMoneyType())){
			refundMethod = "cash";
		}else {
			logger.error("RefundServiceImpl  sendRefundResultEmail  ...refundId:"+refundId+"-serviceType:"+serviceType);
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"unkonw refundMethod:"+refundRequest.getRefundMoneyType()+"-serviceType:"+serviceType});
		}
		String refundNo = refundRequest.getRefundId()+"";
		/**
		 Dear {salutation} {lastName},</p>
		This is to inform you that your refund request has been {refundStatus}, please kindly find the refund details information below. 
		
		
		Private Member Name: {fullName}
		Refund Status: {refundServiceStatus} 
		Service Type: {refundServiceType}
		Refunded Date: {updateDate}
		Refund Amount: {approvedAmt}
		Refund Method: {refundMoneyType}
		Refund No: {refundNo}
		
		Best Regards, HKGTA 

		 */
		String memberEmail = cp.getContactEmail();
		content = templateContent
				.replaceAll(MessageTemplateParams.MemberEmail.getParam(), memberEmail)
				//modified by Kaster 20160426 Message Template的参数有变动
//				.replaceAll(MessageTemplateParams.Salutation.getParam(), salutation+" ")
//				.replaceAll(MessageTemplateParams.FirstName.getParam(), firstName)
//				.replaceAll(MessageTemplateParams.LastName.getParam(), lastName)
				.replaceAll(MessageTemplateParams.UserName.getParam(), firstName + " " + lastName)
				.replaceAll(MessageTemplateParams.FullName.getParam(), fullName)
				.replaceAll(MessageTemplateParams.RefundStatus.getParam(), refundStatus)
				.replaceAll(MessageTemplateParams.ServiceType.getParam(), serviceType)
				.replaceAll(MessageTemplateParams.RefundedDate.getParam(), fefundedDate)
				.replaceAll(MessageTemplateParams.RefundAmount.getParam(), refundAmount)
				.replaceAll(MessageTemplateParams.RefundMethod.getParam(), refundMethod)
				.replaceAll(MessageTemplateParams.RefundNo.getParam(), refundNo)
				.replaceAll(MessageTemplateParams.ResultMessage.getParam(), resultMessage);
		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setContent(content);
		cec.setRecipientEmail(memberEmail);
		cec.setSubject(template.getMessageSubject());
		String noticeType=Constant.NOTICE_TYPE_ENROLLMENT_SUCCESS;
		if(RefundServiceType.TSS.getName().endsWith(serviceType)){
			noticeType=Constant.NOTICE_TYPE_TENNIS_COURSE;
		}else if(RefundServiceType.GCH.getName().endsWith(serviceType)){
			noticeType=Constant.NOTICE_TYPE_GOLF_COACHING;
		}else if(RefundServiceType.GSS.getName().endsWith(serviceType)){
			noticeType=Constant.NOTICE_TYPE_GOLF_COURSE;
		}else if(RefundServiceType.TMS.getName().endsWith(serviceType)){
			noticeType=Constant.NOTICE_TYPE_TENNIS_COURT;
		}else if(RefundServiceType.TCH.getName().endsWith(serviceType)){
			noticeType=Constant.NOTICE_TYPE_TENNIS_COACHING;
		}else if(RefundServiceType.ROOM.getName().endsWith(serviceType)){
			noticeType=Constant.NOTICE_TYPE_GUEST_ROOM;
		}else if(RefundServiceType.WELLNESS.getName().endsWith(serviceType)){
			noticeType=Constant.NOTICE_TYPE_WELLNESS_CENTER;
		}
		cec.setNoticeType(noticeType);
		cec.setSendDate(new Date());
		try {
			logger.debug("RefundServiceImpl  sendRefundResultEmail run start ...refundId:"+refundId+"-status:"+refundStatus);
			MailSender.sendEmail(cec.getRecipientEmail(), null, null, cec.getSubject(), cec.getContent(), null);
			customerEmailContentDao.save(cec);
			logger.debug("RefundServiceImpl  sendRefundResultEmail  run end ...refundId:"+refundId+"-status:"+refundStatus);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("send refund result e-mail error", e);
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Refund Confirmation e-mail error"});
		}
	}
	
	private String getName(CustomerProfile cp) {
		if (cp == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName()).append(" ").append(cp.getSurname());
		return sb.toString();
	}

	/**   
	* @author: Zero_Wang
	* @since: Aug 7, 2015
	* 
	* @description
	* write the description here
	*/  
	@Transactional
	@Override
	public ResponseResult rejectFefund(long refundId,String userId) {
		CustomerRefundRequest refundRequest = this.customerRefundRequestDao.get(CustomerRefundRequest.class, refundId);
		Date currentDate = new Date();
		if(RefundServiceType.CVL.getName().equals(refundRequest.getRefundServiceType())){
			//cash value refund reject
			CustomerOrderTrans transaction = customerOrderTransDao.get(CustomerOrderTrans.class, refundRequest.getRefundTransactionNo());
			Assert.notNull(transaction, "No such transaction id");
			
			CustomerOrderHd currentOrder = transaction.getCustomerOrderHd();
			//Update order
			currentOrder.setUpdateBy(userId);
			currentOrder.setUpdateDate(currentDate);
			currentOrder.setOrderStatus(Constant.Status.REJ.toString());
			//Update transaction
			transaction.setStatus(Constant.Status.REJ.toString());
		} else {
			// refund service reject
		
		}
		//Update refund request
		refundRequest.setStatus(Constant.CustomerRefundRequest.REJ.getName());
		refundRequest.setUpdateBy(userId);
		refundRequest.setUpdateDate(currentDate);
		refundRequest.setApprovedAmount(BigDecimal.ZERO);
		refundRequest.setAuditorUserId(userId);
		refundRequest.setAuditDate(DateFormatUtils.format(currentDate, "yyyy-MM-dd"));
		this.customerRefundRequestDao.update(refundRequest);
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		return  this.responseResult;
	}
	@Override
	@Transactional
	public void refund(final Long fromTransactionNo,final Long customerId,final BigDecimal refundAmt, final String loginUserId,final String extRefNo, final String remark)
	{
		CallBackExecutor executor = new CallBackExecutor(RefundServiceImpl.class);
		executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
					MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
					BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().add(refundAmt);
					memberCashvalue.setAvailableBalance(updatedAmount);
					memberCashValueDao.updateMemberCashValue(memberCashvalue);
					
					Date currentDate = new Date();
					CustomerOrderHd topUpOrder = new CustomerOrderHd();
					topUpOrder.setOrderDate(currentDate);
					topUpOrder.setOrderStatus(Constant.Status.CMP.toString());
					topUpOrder.setStaffUserId(loginUserId);
					topUpOrder.setCustomerId(customerId);
					topUpOrder.setOrderTotalAmount(refundAmt);
					topUpOrder.setOrderRemark(Constant.REFUND_SERVICE_REMARK);
					topUpOrder.setCreateDate(new Timestamp(currentDate.getTime()));
					topUpOrder.setCreateBy(loginUserId);
					topUpOrder.setUpdateBy(loginUserId);
					topUpOrder.setUpdateDate(currentDate);
					customerOrderHdDao.save(topUpOrder);

					// CUSTOMER ORDER Detail
					CustomerOrderDet customerOrderDet = new CustomerOrderDet();
					PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.getByItemNo(Constant.SRV_REFUND_ITEM_NO);
					if(posServiceItemPrice == null ){
						PosServiceItemPrice psip = new PosServiceItemPrice();
						psip.setStatus(Constant.Status.ACT.toString());
						psip.setItemNo(Constant.SRV_REFUND_ITEM_NO);
						psip.setItemCatagory(Constant.PriceCatagory.REFUND.toString());
						psip.setDescription(Constant.REFUND_SERVICE_REMARK);
						posServiceItemPriceDao.save(psip);
					}
					customerOrderDet.setCustomerOrderHd(topUpOrder);
					customerOrderDet.setItemNo(Constant.SRV_REFUND_ITEM_NO);
					customerOrderDet.setItemRemark(Constant.REFUND_SERVICE_REMARK);
					customerOrderDet.setOrderQty(1L);
					customerOrderDet.setItemTotalAmout(refundAmt);
					customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
					customerOrderDet.setCreateBy(loginUserId);
					customerOrderDet.setUpdateDate(currentDate);
					customerOrderDet.setUpdateBy(loginUserId);
					customerOrderDet.setExtRefNo(extRefNo);
					customerOrderDetDao.saveOrderDet(customerOrderDet);
					// CUSTOMER ORDER TRANS
					CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
					initCustomerOrderTrans(customerOrderTrans);
					customerOrderTrans.setCustomerOrderHd(topUpOrder);
					customerOrderTrans.setTransactionTimestamp(currentDate);
					customerOrderTrans.setPaidAmount(refundAmt);
					customerOrderTrans.setFromTransactionNo(fromTransactionNo);
					customerOrderTrans.setStatus(Constant.Status.RFU.toString());
					customerOrderTrans.setPaymentRecvBy(loginUserId);
					customerOrderTrans.setInternalRemark(Constant.REFUND_SERVICE_REMARK);
					customerOrderTrans.setPaymentMethodCode(Constant.PaymentMethodCode.CASHVALUE.toString());
					customerOrderTransDao.save(customerOrderTrans);
				   return null;
			}
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,this.getException().getMessage());
			}
		});
		
	}
	
	@Override
	@Transactional
	public RefundInfoDto getCancelInfo(String resvId) {
		try{
			return this.customerRefundRequestDao.getCancelInfo(resvId);
		}catch (Exception e){
			logger.error(e.getMessage());
			throw new GTACommonException(GTAError.RefundError.REFUND_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}

	@Override
	@Transactional
	public ResponseResult refundCashValueOasis(long transcationId, String academyId) {
		// TODO Auto-generated method stub
		CustomerOrderTrans trans = customerOrderTransDao.get(CustomerOrderTrans.class, transcationId);
		if (null != trans) {
			
//			if (transDateBeforeCurrentDate(trans.getTransactionTimestamp())) 
//			{
//				responseResult.initResult(GTAError.CustomerOrderTrans.CASHVALUE_OASIS_ERROR);
//				
//			} else {
				if(Constant.Status.VOID.toString().equals(trans.getStatus())){
					responseResult.initResult(GTAError.CustomerOrderTrans.CASHVALUE_OASIS_ERROR,"The transcation is Voided.");
				}else{
					Member member=memberDao.getMemberByAcademyNo(academyId);
					Long customerId=member.getCustomerId();
					if(null!=member){
						if(MemberType.IDM.name().equals(member.getMemberType())||MemberType.CDM.name().equals(member.getMemberType()))
						{
							customerId=member.getSuperiorMemberId();
						}
					}
					MemberCashvalue cashvalue = memberCashValueDao.getMemberCashvalueById(customerId);
					BigDecimal updatedAmount = cashvalue.getAvailableBalance().add(trans.getPaidAmount());
					cashvalue.setAvailableBalance(updatedAmount);
					try {
						memberCashValueDao.updateMemberCashValue(cashvalue);
						CustomerOrderTrans newTrans = initCustomerOrderTransByCopy(trans);
						newTrans.setStatus(Constant.Status.VOID.toString());
						customerOrderTransDao.saveOrUpdate(newTrans);
						responseResult.initResult(GTAError.Success.SUCCESS);
					} catch (Exception e) {
						throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
					}
				}
			}
//		}
		return responseResult;
	}
	/***
	 * If the transaction time is before 1am current day, a warning
	 * message will be shown -
	 * "You are try to void a item which already finished the day end procedure"
	 * .
	 */
	private boolean transDateBeforeCurrentDate(Date transDate)
	{
		Date currentDate = DateConvertUtil
				.parseString2Date(DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd"), "yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.HOUR, 1);
		if(transDate.before(calendar.getTime()))
		{
			return true;
		}else{
			return false;
		}
	}
	@Transactional
	public boolean sendServiceRefundMail(String templateFunctionId, String paramId, Long refundId) {
		GlobalParameter param = globalParameterDao.get(GlobalParameter.class, paramId);
		if (null != param && !StringUtils.isEmpty(param.getParamValue())) {
			CustomerRefundRequest refund = customerRefundRequestDao.get(CustomerRefundRequest.class, refundId);
			if (null != refund && (Constant.CustomerRefundRequest.PND.getName().equalsIgnoreCase(refund.getStatus())
					|| Constant.CustomerRefundRequest.NRF.getName().equalsIgnoreCase(refund.getStatus()))) {
				CustomerRefundRequestDto questDto = getRefundDetail(refund);
				if (null != questDto) {
					MessageTemplate template = messageTemplateService.getTemplateByFuncId(templateFunctionId);
					if (null != template) {
						String content = template.getFullContentHtml("", questDto.getMemberName(),
								questDto.getRefundId().toString(), convertServiceName(questDto.getRefundTpye()),
								questDto.getPaidAmt().setScale(2, BigDecimal.ROUND_HALF_UP).toString(),
								questDto.getRequestDate(), "systemAdmin");
						CustomerEmailContent cec = new CustomerEmailContent();
						cec.setSubject(template.getMessageSubject());
						cec.setContent(content);
						cec.setRecipientEmail(param.getParamValue());
						cec.setSendDate(new Date());
						mailThreadService.send(cec, null);
					}
				}
			}
		}
		return true;
	}

	private String convertServiceName(String type)
	{
		if(ReservationType.GMS.name().equalsIgnoreCase(type))
		{
			return ReservationType.GMS.getDesc();
		}else if(ReservationType.GSS.name().equalsIgnoreCase(type)){
			return ReservationType.GSS.getDesc();
		}
		else if(ReservationType.GSS.name().equalsIgnoreCase(type)){
			return ReservationType.GSS.getDesc();
		}
		else if(ReservationType.GCH.name().equalsIgnoreCase(type)){
			return ReservationType.GCH.getDesc();
		}
		else if(ReservationType.TMS.name().equalsIgnoreCase(type)){
			return ReservationType.TMS.getDesc();
		}
		else if(ReservationType.TSS.name().equalsIgnoreCase(type)){
			return ReservationType.GSS.getDesc();
		}else if(ReservationType.TCH.name().equalsIgnoreCase(type)){
			return ReservationType.TCH.getDesc();
		}
		else if(ReservationType.ROOM.name().equalsIgnoreCase(type)){
			return ReservationType.ROOM.getDesc();
		}
		else if(ReservationType.WELLNESS.name().equalsIgnoreCase(type)){
			return ReservationType.WELLNESS.getDesc();
		}
		return type;
	}
	private CustomerRefundRequestDto getRefundDetail(CustomerRefundRequest refund)
	{
		if(null!=refund)
		{
			StringBuilder sb=new StringBuilder();
			sb.append(" SELECT ref.refund_id as refundId,ref.refund_service_type as refundType,CONCAT (cp.salutation, ' ', cp.given_name, ' ' ,cp.surname) as memberName,");
			sb.append(" trans.paid_amount as paidAmt,ref.create_date as requestDate FROM customer_refund_request ref ");
			sb.append(" LEFT JOIN(");
			sb.append(" customer_order_trans  trans ");
			sb.append("  LEFT JOIN customer_order_hd hd");
			sb.append("  ON trans.order_no=hd.order_no");
			sb.append(" INNER JOIN customer_profile cp ON hd.customer_id=cp.customer_id");
			sb.append(") ON ref.refund_transaction_no=trans.transaction_no");
			sb.append(" WHERE ref.refund_id="+refund.getRefundId()+"");
			List<CustomerRefundRequestDto>list=customerRefundRequestDao.getDtoBySql(sb.toString(), null, CustomerRefundRequestDto.class);
			if(null!=list&&list.size()>0)
			{
				return list.get(0);
			}
		}
		return null;
	}
}



