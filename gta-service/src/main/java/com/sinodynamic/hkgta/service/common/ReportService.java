package com.sinodynamic.hkgta.service.common;

import com.sinodynamic.hkgta.dto.ReportRequestDto;

public interface ReportService {

    public byte[] getReportAttach(ReportRequestDto reportRequestDto);
}
