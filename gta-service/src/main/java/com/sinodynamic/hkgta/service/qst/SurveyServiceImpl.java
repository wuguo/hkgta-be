package com.sinodynamic.hkgta.service.qst;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.qst.SurveyDao;
import com.sinodynamic.hkgta.dto.qst.QuestionnaireSettingListDto;
import com.sinodynamic.hkgta.entity.qst.Survey;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class SurveyServiceImpl extends ServiceBase<Survey>implements SurveyService {

	@Autowired
	private SurveyDao surveyDao;
	

	/**
	 * 获取问卷设置
	 * @return
	 */
	@Override
	@Transactional
	public List<Survey> getQuestionnaireSetting()
	{
		return surveyDao.getAll();
	}
	
	
	/**
	 * 更新问卷设置
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult updateQuestionnaireSetting(QuestionnaireSettingListDto listDto, String userId){
		try {
			if (listDto.getList().size() < 1) {
				responseResult.initResult(GTAError.DayPassError.ERRORMSG_PARAM_NULL);
				return responseResult;
			}
			for (Survey dto : listDto.getList()) {
				Survey survey = surveyDao.get(Survey.class, dto.getSurveyId());
				if (survey == null) {
					responseResult.initResult(GTAError.MemberShipError.NO_RECORD_FOUND);
					return responseResult;
				}
				survey.setBufferDay(dto.getBufferDay());
				survey.setForwardUrl(dto.getForwardUrl());
				survey.setStatus(dto.getStatus());
//				survey.setSurveyName(dto.getSurveyName());
				survey.setUpdateBy(userId);
				survey.setUpdateDate(new Date());
				survey.setUtilizeRateHit(dto.getUtilizeRateHit());
				survey.setVerNo(dto.getVerNo());
				surveyDao.update(survey);
			}
			responseResult.initResult(GTAError.Success.SUCCESS);	
			return responseResult;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("------------------错误-----------" + e.getMessage());
			responseResult.initResult(GTAError.SysError.FAIL_UPDATE_ROLE);	
			return responseResult;
		}
	}

}
