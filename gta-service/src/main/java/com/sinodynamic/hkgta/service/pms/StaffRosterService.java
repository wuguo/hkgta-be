package com.sinodynamic.hkgta.service.pms;

import com.sinodynamic.hkgta.dto.pms.StaffRosterDto;
import com.sinodynamic.hkgta.entity.pms.StaffRoster;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface StaffRosterService extends IServiceBase<StaffRoster> {

	public void save(StaffRosterDto staffRosterDto, String userId);
	
	public StaffRosterDto getStaffRoster(String staffUserId, String beginDate);
	
	public StaffRosterDto getStaffRosterByMonth(String staffUserId, String date);
}
