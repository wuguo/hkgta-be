package com.sinodynamic.hkgta.service.crm.sales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.LoginSessionDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dto.Constant.StaffType;
import com.sinodynamic.hkgta.dto.UserLoginRequest;
import com.sinodynamic.hkgta.dto.crm.DependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.HealthCheckingDto;
import com.sinodynamic.hkgta.entity.crm.LoginSession;
import com.sinodynamic.hkgta.entity.crm.LoginSessionPK;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.adm.StaffMasterService;
import com.sinodynamic.hkgta.service.sys.SysUserService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.SessionMap;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.UserType;
import com.sinodynamic.hkgta.util.constant.GTAError.LoginError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class UserMasterServiceImpl extends ServiceBase<UserMaster> implements UserMasterService
{
	@Value(value ="#{appProperties['session.timeout.minute']}")
	private Integer sessionTimeoutMinute;

	@Value(value ="#{appProperties['session.updateInterval.minute']}")
	private Integer sessionUpdateInterval = 1;
	
	@Autowired
	private UserMasterDao userMasterDao;

	@Autowired
	private LoginSessionDao loginSessionDao;

	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private SysUserService sysUserService;
	
	@Autowired
	private StaffMasterService staffMasterService;
	
	
	@Resource(name = "messageSource")
	private MessageSource messageSource;
	
	private static ExecutorService pool = Executors.newCachedThreadPool();
	
	@Transactional
	public void addUserMaster(UserMaster m) throws Exception
	{
		userMasterDao.addUserMaster(m);
	}

	@Transactional
	public void getUserMasterList(ListPage<UserMaster> page, UserMaster m) throws Exception
	{
		String countHql = " select count(*) from UserMaster ";
		String hql = " from UserMaster m ";
		userMasterDao.getUserMasterList(page, countHql, hql, null);

	}

	@Transactional
	public void updateUserMaster(UserMaster m) throws Exception
	{
		userMasterDao.update(m);

	}

	@Transactional
	public void deleteUserMaster(UserMaster m) throws Exception
	{
		userMasterDao.delete(m);

	}

	@Transactional
	public UserMaster getUserByLoginId(String id) throws Exception
	{
		return userMasterDao.getUserByLoginId(id);
	}

	@Override
	@Transactional
	public UserMaster getUserByUserId(String userId) throws Exception
	{
		// TODO Auto-generated method stub
		return userMasterDao.getUserByUserId(userId);
	}

	@Override
	@Transactional
	public void SaveSessionToken(String userId, String authToken, String device) throws Exception
	{
		// LoginSession token = new LoginSession();

		LoginSession token = loginSessionDao.getByUserIdAndDevice(userId, device);
		if (null == token)
		{
			token = new LoginSession();
			LoginSessionPK pk = new LoginSessionPK();
			pk.setUserId(userId);
			pk.setDeviceAccess(device);
			token.setId(pk);
		}

		token.setSessionToken(authToken);
		token.setLastAccessTime(new Date());
		loginSessionDao.save(token);
	}

	/**
	 * @return Error Message.<br>
	 *         If token updated failed , it will returns the relevant error message.
	 * @author Vian Tang
	 * 
	 */
	@Override
	@Transactional
	public GTAError updateSessionToken(String authToken) {
		return updateSessionToken(authToken, true);
	}

	@Override
	@Transactional
	public GTAError updateSessionToken(String authToken, boolean shouldUpdateToken) {

		LoginSession session = loginSessionDao.getByToken(authToken);
		if (null == session) {
			return GTAError.LoginError.TOKEN_NOT_FOUND;
		}

		DateTime dateTime = new DateTime(session.getLastAccessTime());
		long currentTimeMillis = System.currentTimeMillis();
		if (dateTime.plusMinutes(sessionTimeoutMinute).getMillis() < currentTimeMillis) {
			return GTAError.LoginError.TOKEN_EXPIRED;
		} else if (shouldUpdateToken && dateTime.plusMinutes(sessionUpdateInterval).getMillis() < currentTimeMillis) {
			session.setLastAccessTime(new Date());
			loginSessionDao.save(session);
		}
		return GTAError.Success.SUCCESS;
	}
	
	@Override
	@Transactional
	public boolean deleteSessionToken(String userId, String device)
	{
		return loginSessionDao.deleteSessionToken(userId, device);
	}

	@Override
	public boolean requireValidateKaptchaCode(UserMaster usermaster)
	{
		return check(usermaster, Constant.LOGIN_FAIL_TRY_TIME);
	}

	@Override
	public boolean hasBeenLocked(UserMaster usermaster)
	{
		return check(usermaster, Constant.LOGIN_FAIL_TRY_TIME_LOCK);
	}

	private boolean check(UserMaster usermaster, String times)
	{
		int configtimes = Integer.parseInt(appProps.getProperty(times));
		if (configtimes == 0)
		{
			return false;
		}

		Calendar c = Calendar.getInstance();
		if (null == usermaster.getLastLoginFailTime())
		{
			return false;
		}
		c.setTime(usermaster.getLastLoginFailTime());
		c.add(Calendar.MINUTE, Integer.parseInt(appProps.getProperty(Constant.LOGIN_FAIL_TRY_PERIOD)));
		if (c.getTimeInMillis() < System.currentTimeMillis())
		{
			return false;
		}

		if (usermaster.getLoginFailCount() >= configtimes)
		{
			return true;
		}
		return false;
	}

	private Set<String> getDeviceCheckList(String devices)
	{
		return new HashSet<String>(Arrays.asList(devices.split(",")));
	}

	@Override
	public boolean requireValidateKaptchaCode(String device)
	{
		return getDeviceCheckList(appProps.getProperty(Constant.LOGIN_DEVICE_CHECK_LIST)).contains(device);
	}
	
	@Override
	public boolean requireValidateLocation(String device)
	{
		return getDeviceCheckList(appProps.getProperty(Constant.LOCATION_CHECK_DEVICE_LIST)).contains(device);
	}


	@Override
	@Transactional
	public void updateLoginFailTime(UserMaster usermaster) throws Exception
	{
		Long cnt = (usermaster.getLoginFailCount() == null) ? 0L : usermaster.getLoginFailCount();
		Long timeIncrement = CommUtil.getIncTimeInMillis(usermaster.getLastLoginFailTime(), Integer.parseInt(appProps.getProperty(Constant.LOGIN_FAIL_TRY_PERIOD)));
		if (null == timeIncrement || timeIncrement < System.currentTimeMillis())
			cnt = 0L;

		cnt++;
		if (cnt == 1 || cnt == Integer.parseInt(appProps.getProperty(Constant.LOGIN_FAIL_LOCK_PERIOD)))
		{
			usermaster.setLastLoginFailTime(new Date());
		}

		usermaster.setLoginFailCount(cnt);
		updateUserMaster(usermaster);
	}

	
	@Transactional
	public UserMaster getUserMasterByCustomerId(Long customerId){
		Member member = memberDao.getMemberByCustomerId(customerId);
		if(member==null) return null;
		if(StringUtils.isEmpty(member.getUserId())) return null;
		return userMasterDao.getUserByUserId(member.getUserId());
	}
	
	@Override
	@Transactional
	public void setAllLoginIdAndPassword(List<Member>members, String defaultPassword) throws Exception{
		//String sqlString = "select u from userMaster u, member m where u.userId = m.userId and u.userType = 'CUSTOMER'";
		for (Member member : members) {
			UserMaster userMaster = getUserMasterByCustomerId(member.getCustomerId());
			if (userMaster == null || !userMaster.getUserType().equals("CUSTOMER")) continue;
			userMaster.setLoginId(member.getAcademyNo());
			userMaster.setPassword(CommUtil.getMD5Password(member.getAcademyNo(), defaultPassword));
			updateUserMaster(userMaster);
		}
	}
	
	@Override
	@Transactional
	public void SaveSessionToken(String userId, String authToken, String device, String appTypeCode) throws Exception
	{
		// LoginSession token = new LoginSession();

//		 = loginSessionDao.getByUserIdAndAppTypeCode(userId, appTypeCode);
		
		String hql = " FROM LoginSession l WHERE l.id.userId = ? and l.appTypeCode = ?";
		
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userId);
		paramList.add(appTypeCode);
		
		LoginSession token = (LoginSession) loginSessionDao.getUniqueByHql(hql, paramList);
		if (null == token)
		{
			token = new LoginSession();
			LoginSessionPK pk = new LoginSessionPK();
			pk.setUserId(userId);
			pk.setDeviceAccess(device);
			token.setId(pk);
		}

		token.setSessionToken(authToken);
		token.setLastAccessTime(new Date());
		token.setAppTypeCode(appTypeCode);
		loginSessionDao.save(token);
	}

	/**
	 * 线程保存心跳记录
	 */
	@Override
	@Transactional
	public void saveSessionTokenTask(final String userId, final String authToken, final String device, final String appTypeCode) {
		 Runnable saveTask = new Runnable() {
		        public void run() {
			        try {
			        	sysUserService.SaveSessionToken(userId, authToken, device, appTypeCode);
				    } catch (Exception e) {
				    	logger.error(e.getMessage(), e);
				    }
		        }
		    };
		    pool.execute(saveTask);
	}
	/**
	 * 获取心跳检查列表
	 * @param page
	 * @param appTypeCode
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult getHealthCheckingList(ListPage<LoginSession> page, String  appTypeCode) {
		ListPage<LoginSession> listPage = loginSessionDao.getHealthCheckingList(page, appTypeCode);
		List<Object> healthCheckingDtos = listPage.getDtoList();
		Data data = new Data();
		for (Object object : healthCheckingDtos) {
			HealthCheckingDto temp = (HealthCheckingDto) object;
		}
		data.setList(healthCheckingDtos);
		data.setTotalPage(page.getAllPage());
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	/**
	 * This is service logic ...should not place here!
	 * @param usermaster
	 * @param URI
	 */
	private void validateURI(UserMaster usermaster, String URI)
	{
		String userType = usermaster.getUserType();
		
		if ((userType.equals(UserType.STAFF.getType()) || userType.equals(UserType.ADMIN.getType())) && (!(URI.contains("/stafflogin") || URI.contains("/saleskitlogin") || URI.contains("/device/coachapplogin")|| URI.contains("/memberapp/login"))))
		{
			throw new GTACommonException(GTAError.LoginError.LOGIN_FAILED);
		}
		
		if (userType.equals(UserType.CUSTOMER.getType()) && !URI.contains("/login"))
		{
			throw new GTACommonException(GTAError.LoginError.LOGIN_FAILED);
		}
		
	}
	private LoginError validateStaffJobNatureForCoachApp(String URI,String userID) throws Exception{
		//only a coach can login to the Coach APP
		if(URI.contains("/device/coachapplogin"))
		{ 
			String staffType = staffMasterService.getStaffTypeByUserId(userID);
			if(!(StaffType.FTR.toString().equals(staffType)||StaffType.PTR.toString().equals(staffType)||
						StaffType.FTG.toString().equals(staffType)||
						StaffType.PTG.toString().equals(staffType))){
				return GTAError.LoginError.LOGIN_FAILED_NO_PERMISSION;
			}
		}
		return null;
	}
	@Override
	@Transactional
	public ResponseResult doLogin(HttpServletRequest request, HttpServletResponse response,UserLoginRequest loginUser,String URI,String loginDevice) {
		/***
		 * handler multi Thread concurrent
		 */
		ResponseResult responseResult=new ResponseResult(messageSource);
		
		UserMaster usermaster = null;
		try
		{
			String username = loginUser.getUsername().trim();
			usermaster = this.getUserByLoginId(username);
			if (usermaster != null)
			{
				validateURI(usermaster, URI);
				if(usermaster.getUserType().equals(UserType.STAFF.getType()) && StringUtils.isEmpty(loginUser.getLocation()) && URI.contains("/home/stafflogin"))
				{
					responseResult.initResult(GTAError.LoginError.LOCATION_REQUIRED);
					return responseResult;
				}
				//only Coach Account can login to Coach APP
				LoginError result = validateStaffJobNatureForCoachApp(URI,usermaster.getUserId());
				if(result!=null){
					responseResult.initResult(result);
					return responseResult;
				}
				
				if (this.hasBeenLocked(usermaster))
				{
					responseResult.initResult(GTAError.LoginError.USER_HAS_BEEN_LOCKED, new Object[] { appProps.get(Constant.LOGIN_FAIL_LOCK_PERIOD) });
					return responseResult;
				}
				boolean requireValidate = this.requireValidateKaptchaCode(loginDevice) && this.requireValidateKaptchaCode(usermaster);
				if (requireValidate)
				{
					String kaptchaExpected = SessionMap.getInstance().getSession(request.getHeader("sessionId"));
					
					if (StringUtils.isEmpty(loginUser.getValidCode()) || StringUtils.isEmpty(kaptchaExpected) || !kaptchaExpected.equals(loginUser.getValidCode()))
					{
						this.updateLoginFailTime(usermaster);
						responseResult.initResult(StringUtils.isEmpty(loginUser.getValidCode()) ? GTAError.LoginError.VALID_CODE_REQUIRED : GTAError.LoginError.VALID_CODE_ERROR);
						return responseResult;
					}
					
				}
				responseResult.initResult(GTAError.Success.SUCCESS,usermaster);
				return responseResult;
			}
			else
			{
				logger.error("userMaster is null, login failed");
				responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
				return responseResult;
			}
		}
		catch (BadCredentialsException | UsernameNotFoundException e)
		{
			try
			{
				this.updateLoginFailTime(usermaster);
			}
			catch (Exception e1)
			{
				String[] args = { (null!=usermaster) ?usermaster.getUserId():"", DateConvertUtil.getStrFromDate(new Date()) };
				logger.warn(getI18nMessge(GTAError.LoginError.UPDATE_LOGIN_FAIL_TIME_FAILED.getCode(), args), e1);
				logger.error(e);
			}
			if (this.requireValidateKaptchaCode(loginDevice) && this.requireValidateKaptchaCode(usermaster))
			{
				responseResult.initResult(new GTAError[] { GTAError.LoginError.LOGIN_FAILED, GTAError.LoginError.VALID_CODE_REQUIRED });
			}
			else
			{
				logger.error("login faild.",e);
				responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
			}
			return responseResult;

		}
		catch (GTACommonException e)
		{
			if (StringUtils.isEmpty(e.getErrorMsg()))
			{
				responseResult.initResult(e.getError());
			}
			else
			{
				responseResult.initResult(e.getError(), e.getErrorMsg());
			}
			return responseResult;
		}
		catch (Exception e)
		{
			logger.error("login faild.",e);
			responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
			return responseResult;
		}
	}
	

}
