package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.HkArea;
import com.sinodynamic.hkgta.entity.crm.HkDistrict;
import com.sinodynamic.hkgta.service.IServiceBase;

@SuppressWarnings("rawtypes")
public interface HKAreaDistrictService extends IServiceBase{

	public List<HkArea> getHKArea();
	
	public List<HkDistrict> getHKDistrict();
	
}
