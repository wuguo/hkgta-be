package com.sinodynamic.hkgta.service.crm.membercash;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.Constant.memberType;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.crm.MemberCashvalueDto;
import com.sinodynamic.hkgta.dto.crm.PatronTransReportDto;
import com.sinodynamic.hkgta.dto.crm.SourceBookingDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.limitUnit;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.report.JasperUtil;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class MemberCashvalueServiceImpl extends ServiceBase<MemberCashvalue> implements MemberCashvalueService {
	
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	
	@Autowired
	private MemberDao memberDao;

	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;

	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	
	//added by Kaster 20160329
	@Autowired
	private UserMasterDao userMasterDao;

	@Transactional
	public boolean createMemberCashvalue(CustomerProfile profile, String userId) throws Exception {
		
//		if (profile == null || CommUtil.nvl(userId).length() == 0) return false;
//		MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(profile.getCustomerId());
//		if (memberCashValue != null) return false;
//		
//		VirtualAccPool vap = new VirtualAccPool();
//		Date date = new Date();
//		vap.setCreateBy(userId);
//		vap.setCreateDate(date);
//		Long vaAccNo = (Long) virtualAccPoolDao.saveVirtualAccPool(vap);
//		
//		MemberCashvalue mcv = new MemberCashvalue();
//		mcv.setInitialDate(date);
//		mcv.setExchgFactor(new BigDecimal(1));
//		mcv.setCustomerId(profile.getCustomerId());
//		memberCashValueDao.saveMemberCashValue(mcv);
//		
//		//generate csv file, waiting to improve...
//		GlobalParameter clientId = globalParameterDao.get(GlobalParameter.class, Constant.CLIENT_ID);
//		GlobalParameter physicalAccount = globalParameterDao.get(GlobalParameter.class, Constant.PHYSICAL_ACCOUNT);
//		GlobalParameter recordCode = globalParameterDao.get(GlobalParameter.class, Constant.RECORD_CODE);
//		GlobalParameter payerName = globalParameterDao.get(GlobalParameter.class, Constant.PAYER_NAME);
//		
//		String clientIdStr = null;
//		String payerNameStr = null;
//		String recordCodeStr = null;
//		String physicalAccountStr = null;
//		
//		if (clientId != null) {
//			clientIdStr = clientId.getParamValue();
//		}
//		if (payerName != null) {
//			payerNameStr = payerName.getParamValue();
//		}
//		if (recordCode != null) {
//			recordCodeStr = recordCode.getParamValue();
//		}
//		if (physicalAccount != null) {
//			physicalAccountStr = physicalAccount.getParamValue();
//		}
//		
//		String temp = ("00000" + (vaAccNo % 10000));
//		String accountNo = temp.substring(temp.length()-4, temp.length());
//		String digit = CommUtil.generate(clientIdStr + accountNo);
//		String varAccNoStr = clientIdStr + accountNo + digit;
//		
//		MemberPaymentAcc memberPaymentAcc = new MemberPaymentAcc();
//		Member member = new Member();
//		member.setCustomerId(profile.getCustomerId());
//		memberPaymentAcc.setMember(member);
//		memberPaymentAcc.setAccType(PaymentAccType.VIR.name());
//		memberPaymentAcc.setAccountNo(varAccNoStr);
//		memberPaymentAccDao.addMemberPaymentAcc(memberPaymentAcc);
//		
//		String path = appProps.getProperty(Constant.CSV_UPLOAD_PATH, null);
//		if (CommUtil.nvl(path).length() == 0) return false;
//		File dir = new File(path);
//		if (!dir.exists() || !dir.isDirectory()) dir.mkdir();
//		String filename = String.format("%s.%s.H2H-HK-RCVA", accountNo, clientIdStr);
//		
//        File serverFile = new File(dir.getAbsolutePath()+ File.separator + filename);
//        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
//        String fileContent = String.format("%s,%s,%s,%s,%s,,%s,,,,A\n", recordCodeStr, clientIdStr, physicalAccountStr, varAccNoStr, payerNameStr, accountNo + digit);
//        stream.write(fileContent.getBytes());
//        stream.close();
		
		return true;
	}
	

	@Override
	@Transactional
	public MemberCashvalue getMemberCashvalueByVirtualAccNo(String virtualAccNo) throws Exception {
		
		if (CommUtil.nvl(virtualAccNo).length() == 0) return null;
		return memberCashValueDao.getByVirtualAccNo(virtualAccNo);
	}


	@Override
	@Transactional
	public boolean modifyMemberCashvalue(MemberCashvalue memberCashValue) throws Exception {
		return memberCashValueDao.updateMemberCashValue(memberCashValue);
	}


	@Transactional
	public MemberCashvalue getMemberCashvalueByCustomerId(Long CustomerId) throws Exception {
		return memberCashValueDao.getByCustomerId(CustomerId);
	}
	
	
	@Override
	@Transactional
	public MemberCashvalue getMemberCashvalueById(Long customerId)
			throws Exception {
		return memberCashValueDao.getMemberCashvalueById(customerId);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public CustomerOrderTransDto paymentByMemberCashvalue(MemberCashValuePaymentDto memberCashValuePaymentDto) throws Exception
	{
		if(memberCashValuePaymentDto.getCustomerId()==null || (memberCashValuePaymentDto.getItemNos() == null && memberCashValuePaymentDto.getItemNoMap() ==null) || memberCashValuePaymentDto.getTotalAmount()==null)
			throw new GTACommonException(GTAError.MemberError.MEMBER_NOT_FOUND);
		
		if(StringUtils.isEmpty(memberCashValuePaymentDto.getPaymentMethod()))memberCashValuePaymentDto.setPaymentMethod(Constant.CASH_Value);
		
		Long customerId = memberCashValuePaymentDto.getCustomerId();
		Member member = memberDao.getMemberByCustomerId(customerId);
		MemberCashvalue memberCashvalue = null;
		if (StringUtils.isEmpty(memberCashValuePaymentDto.getPaymentMethod()) || memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH_Value)){
			memberCashvalue = updateMemberCashValueData(member,memberCashValuePaymentDto);
		}
		CustomerOrderTransDto customerOrderTransDto = saveCustomerOrderTrans(customerId,memberCashValuePaymentDto);
		if(null != memberCashvalue)customerOrderTransDto.setAvailableBalance(memberCashvalue.getAvailableBalance());
		return customerOrderTransDto;
	}


	@Override
	public MemberCashvalue updateMemberCashValueData(Member member,MemberCashValuePaymentDto memberCashValuePaymentDto) throws GTACommonException
	{
		MemberCashvalue memberCashvalue = null;
		Long customerId = member.getCustomerId();
		String memberType = member.getMemberType();
		if(logger.isDebugEnabled())
		{
			logger.debug("customerID:" + customerId);
			logger.debug("memberType:" + memberType);
		}
		if(!StringUtils.isEmpty(memberType)){
			if((Constant.memberType.IDM.toString().equalsIgnoreCase(memberType) || Constant.memberType.CDM.toString().equalsIgnoreCase(memberType)) && 
					(null != member.getSuperiorMemberId() && member.getSuperiorMemberId() >0) ){
				MemberLimitRule memberLimitRuleTRN = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, LimitType.TRN.name());
				if(null != memberLimitRuleTRN){
					BigDecimal numValue = memberLimitRuleTRN.getNumValue();
					if(limitUnit.EACH.name().equals(memberLimitRuleTRN.getLimitUnit())){
						if(null!= numValue && memberCashValuePaymentDto.getTotalAmount().compareTo(numValue) > 0)
							throw new GTACommonException(GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_PERPAYMENT,new Object[] { numValue });
					}
				}
				memberCashvalue = memberCashValueDao.getMemberCashvalueById(member.getSuperiorMemberId());
				customerId = member.getSuperiorMemberId();
			}else{
				memberCashvalue = memberCashValueDao.getMemberCashvalueById(customerId);
			}
		}
		
		if(logger.isDebugEnabled())
		{
			logger.debug("member cashValue amount:" + memberCashvalue.getAvailableBalance());
			logger.debug("paid amount:" + memberCashValuePaymentDto.getTotalAmount());
		}
		if(memberCashvalue==null){
			memberCashvalue = new MemberCashvalue();
			memberCashvalue.setCustomerId(customerId);
			memberCashvalue.setAvailableBalance(BigDecimal.ZERO);
			memberCashvalue.setInitialValue(BigDecimal.ZERO);
			memberCashvalue.setInitialDate(new Date());
		} else {
		    
		    memberCashvalue.setUpdateBy(memberCashValuePaymentDto.getUserId());
		    memberCashvalue.setUpdateDate(new Date());
		}
		
		if (memberCashvalue.getAvailableBalance().compareTo(memberCashValuePaymentDto.getTotalAmount()) <= 0)
		{
			MemberLimitRule memberLimitRuleCR = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, LimitType.CR.name());
			if(null != memberLimitRuleCR){
				BigDecimal numValue = memberLimitRuleCR.getNumValue();
				if(((memberCashvalue.getAvailableBalance().subtract(memberCashValuePaymentDto.getTotalAmount())).negate()).compareTo(numValue) > 0 ){
					throw new GTACommonException(GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_CREDITLIMIT,new Object[] { memberCashvalue.getAvailableBalance() });
				}
			}else{
				throw new GTACommonException(GTAError.MemberError.MEMBER_INSUFFICIENT_BALANCE);
			}
		}
		memberCashvalue.setAvailableBalance(memberCashvalue.getAvailableBalance().subtract(memberCashValuePaymentDto.getTotalAmount()));
		memberCashValueDao.saveOrUpdate(memberCashvalue);
		return memberCashvalue;
	}


	private CustomerOrderTransDto saveCustomerOrderTrans(Long customerId ,MemberCashValuePaymentDto memberCashValuePaymentDto)
	{
		//added by Kaster 20160329
		String createBy = memberCashValuePaymentDto.getCreatedBy();
		if(createBy!=null && !createBy.equals("")){
			UserMaster um = userMasterDao.get(UserMaster.class, createBy);
			if(um!=null && um.getUserType().equalsIgnoreCase("STAFF")){
				createBy = memberCashValuePaymentDto.getCreatedBy();
			}else{
				createBy = memberCashValuePaymentDto.getUserId();
			}
		}else{
			createBy = memberCashValuePaymentDto.getUserId();
		}
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(new Date());
		customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());
		customerOrderHd.setStaffUserId(memberCashValuePaymentDto.getUserId());
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(memberCashValuePaymentDto.getTotalAmount());
		//modified by Kaster 20160329
		customerOrderHd.setCreateBy(createBy);
		customerOrderHd.setCreateDate(currentTime);
		customerOrderHdDao.save(customerOrderHd);

		if(null != memberCashValuePaymentDto.getItemNoMap() && memberCashValuePaymentDto.getItemNoMap().size() > 0){
			for (Map.Entry<String, Integer> entry : memberCashValuePaymentDto.getItemNoMap().entrySet()) {
				PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class, entry.getKey());
				CustomerOrderDet customerOrderDet = new CustomerOrderDet();
				customerOrderDet.setCustomerOrderHd(customerOrderHd);
				customerOrderDet.setItemNo(entry.getKey());
				customerOrderDet.setOrderQty(entry.getValue());
				customerOrderDet.setItemTotalAmout(posServiceItemPrice.getItemPrice().multiply(new BigDecimal(customerOrderDet.getOrderQty())));
				//modified by Kaster 20160329
				customerOrderDet.setCreateBy(createBy);
				customerOrderDet.setCreateDate(currentTime);
				customerOrderDetDao.save(customerOrderDet);
			}
		}
//		else if(memberCashValuePaymentDto.getItemNos().length > 0){
//			for(int i=0 ;i<memberCashValuePaymentDto.getItemNos().length; i++){
//				PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class, memberCashValuePaymentDto.getItemNos()[i]);
//				CustomerOrderDet customerOrderDet = new CustomerOrderDet();
//				customerOrderDet.setCustomerOrderHd(customerOrderHd);
//				customerOrderDet.setItemNo(memberCashValuePaymentDto.getItemNos()[i]);
//				customerOrderDet.setOrderQty(memberCashValuePaymentDto.getOrderQty());
//				customerOrderDet.setItemTotalAmout(posServiceItemPrice.getItemPrice().multiply(new BigDecimal(memberCashValuePaymentDto.getOrderQty())));
//				customerOrderDet.setCreateBy(memberCashValuePaymentDto.getUserId());
//				customerOrderDet.setCreateDate(currentTime);
//				customerOrderDetDao.save(customerOrderDet);
//			}
//		}

		String remoteType=memberCashValuePaymentDto.getRemoteType();
		if(null != remoteType && (remoteType.equalsIgnoreCase(Constant.PMSTHIRD_ITEM_NO)||remoteType.equalsIgnoreCase(Constant.POSTHIRD_ITEM_NO))){
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(customerOrderHd);
			if(remoteType.equalsIgnoreCase(Constant.PMSTHIRD_ITEM_NO)){
				customerOrderDet.setItemNo(Constant.PMSTHIRD_ITEM_NO);//item_no from table pos_service_item_price
			}else if(remoteType.equalsIgnoreCase(Constant.POSTHIRD_ITEM_NO)){
				customerOrderDet.setItemNo(Constant.POSTHIRD_ITEM_NO);//item_no from table pos_service_item_price
			}			
			customerOrderDet.setOrderQty(memberCashValuePaymentDto.getOrderQty());
			customerOrderDet.setItemTotalAmout(memberCashValuePaymentDto.getTotalAmount());
			//modified by Kaster 20160329
			customerOrderDet.setCreateBy(createBy);
			customerOrderDet.setCreateDate(currentTime);
			customerOrderDetDao.save(customerOrderDet);
		}
		
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		initCustomerOrderTrans(customerOrderTrans);
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		if(!StringUtils.isEmpty(memberCashValuePaymentDto.getLocation())){
			customerOrderTrans.setPaymentLocationCode(memberCashValuePaymentDto.getLocation());
		}
		customerOrderTrans.setPaymentMethodCode(memberCashValuePaymentDto.getPaymentMethod());
		
		customerOrderTrans.setTransactionTimestamp(currentTime);
		customerOrderTrans.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
	
		if(!StringUtils.isEmpty(memberCashValuePaymentDto.getTerminalId())){
			customerOrderTrans.setTerminalId(memberCashValuePaymentDto.getTerminalId());
		}
		customerOrderTrans.setInternalRemark(memberCashValuePaymentDto.getDesc());
		
		if (StringUtils.isEmpty(memberCashValuePaymentDto.getPaymentMethod()) || memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH)){
			customerOrderTrans.setPaymentMedia(PaymentMediaType.NA.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
		}else if(memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH_Value)){
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
		} else {
			customerOrderTrans.setStatus(Constant.Status.PND.toString());
		}

		customerOrderTrans.setPaymentRecvBy(memberCashValuePaymentDto.getUserId());
		customerOrderTransDao.save(customerOrderTrans);
		
		CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
		customerOrderTransDto.setOrderNo(customerOrderHd.getOrderNo());
		customerOrderTransDto.setTransactionNo(customerOrderTrans.getTransactionNo());
		customerOrderTransDto.setTransactionTimestamp(DateConvertUtil.getYMDDateAndDateDiff(currentTime));
		customerOrderTransDto.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTransDto.setPaymentMethodCode(memberCashValuePaymentDto.getPaymentMethod());
		customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
		return customerOrderTransDto;
	}


	@Override
	@Transactional
	public ResponseResult getHighBalanceReport(ListPage page, BigDecimal amount) {
		String hql =this.getHighBalanceHql(amount);
		String countHql="select count(*) from MemberCashvalue mc , CustomerProfile cp, Member m "
				  + "  where mc.customerId=cp.customerId and mc.customerId=m.customerId ";
		
		if(null!=amount){
			countHql+=" and mc.availableBalance <="+amount;
		}
		ListPage listPage = memberCashValueDao.listByHqlDto(page, countHql, hql, null, new MemberCashvalueDto());//.listBySqlDto(page, countSql, sql, null, new MemberCashvalueDto() );
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	private String getHighBalanceHql(BigDecimal amount){
		String hql ="select m.academyNo as academyNo,"
				+" case m.memberType"
				+" when '"+MemberType.IPM.name()+"' then 'Individual Primary Member'"
				+" when '"+MemberType.IDM.name()+"' then 'Individual Dependent Member'"
				+" when '"+MemberType.CPM.name()+"' then 'Corporate Primary Member'"
				+" when '"+MemberType.CDM.name()+"' then 'Corporate Dependent Member'"
				+" when '"+MemberType.MG.name()+"' then 'Member Guest'"
				+" when '"+MemberType.HG.name()+"' then 'House Gues'"
				+" ELSE 'm.memberType'"
				+" end as patronType,"
				+ " mc.availableBalance as availableBalance,CONCAT(cp.givenName, ' ', cp.surname)AS patronName  from MemberCashvalue mc , CustomerProfile cp, Member m "
				+ "  where mc.customerId=cp.customerId and mc.customerId=m.customerId ";
		if(null!=amount){
			hql+=" and mc.availableBalance <="+amount;
		}
		return hql;
	}

	@Override
	@Transactional
	public byte[] getHighBalanceReport(String sortBy, String isAscending, BigDecimal amount, String fileType) {
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "highBalanceReport.jasper";
			
			String hql = this.getHighBalanceHql(amount);
			StringBuilder orderBy=new StringBuilder();
			orderBy.append(hql);
			if(!StringUtils.isEmpty(sortBy)){
				String orderByFiled = sortBy.trim();
				orderBy.append(" order by ");
				if("true".equals(isAscending)){
					orderBy.append(orderByFiled).append(" asc ");
				}else{
					orderBy.append(orderByFiled).append(" desc ");
				}
			}
			List<MemberCashvalueDto>dtos=memberCashValueDao.getDtoByHql(orderBy.toString(), null, MemberCashvalueDto.class);
			Map<String, Object> parameters = new HashMap();
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dtos, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
