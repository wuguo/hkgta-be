package com.sinodynamic.hkgta.service.common;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.service.adm.StaffMasterService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.statement.DeliveryRecordService;
import com.sinodynamic.hkgta.util.EmailResponse;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.EmailStatus;

/**
 * This Component is used to send email asynchronously
 * @author Mianping_Wu
 */
@Component
public class MailThreadServiceImpl implements MailThreadService {
	
	
    private Logger logger = Logger.getLogger(MailThreadServiceImpl.class);
    private static ExecutorService pool = Executors.newCachedThreadPool();
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
    @Autowired
    private DeliveryRecordService deliveryRecordService;
    
    @Autowired
    private StaffMasterService staffMasterService;
    
    @Resource(name = "appProperties")
	Properties appProps;
    
    public void send(final CustomerEmailContent cec, final File[] attachments) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					boolean success = MailSender.sendEmail(
							cec.getRecipientEmail(), null, null,
							cec.getSubject(), cec.getContent(), attachments);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					 cec.setSendDate(new Date());
					 cec.setUpdateDate(new Date());
					 cec.setResendCount(cec.getResendCount()+1);
					//如果邮件是发送给Staff,不需要保存在CustomerEmailContent表中
					if(cec.getRecipientCustomerId() == null || staffMasterService.getStaffMaster(cec.getRecipientCustomerId()) == null){
						customerEmailContentService.modifyCustomerEmailContent(cec);
					}

				} catch (Exception e) {
					logger.error(e.getMessage(),e);
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
		});
		pool.execute(thread);
    }
    
    public void send(final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					boolean success = MailSender.sendEmailWithBytesMineAttach(
							cec.getRecipientEmail(), null, null,
							cec.getSubject(), cec.getContent(),mineTypeList, bytesList,
							fileNameList);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					cec.setSendDate(new Date());
					cec.setUpdateDate(new Date());
					cec.setResendCount(cec.getResendCount()+1);
					customerEmailContentService.modifyCustomerEmailContent(cec);

				} catch (Exception e) {

					logger.error(e.getMessage(),e);
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
			
		});
		pool.execute(thread);
    }
    
    public void sendWithResponse(final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					 EmailResponse response = MailSender.sendEmailWithBytesMineAttachWithResponse(
							cec.getRecipientEmail(), cec.getCopyto(), cec.getBlindCopyTo(),
							cec.getSubject(), cec.getContent(),mineTypeList, bytesList,
							fileNameList);
					if (response!=null&&response.isReportSuccess()) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					cec.setSendDate(new Date());
					cec.setSendDate(new Date());
	    			cec.setUpdateDate(new Date());
					if(response!=null){
						/***
						 * a different object with the same identifier value was already associated with the session:
						 */
						saveOrUpdateCustomerEmailContent(cec, response);
					}

				} catch (Exception e) {
					logger.error(e.getMessage(),e);
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
			
		});
		pool.execute(thread);
    }
	/***
	 * a different object with the same identifier value was already associated with the session:
	 */
    private void saveOrUpdateCustomerEmailContent(final CustomerEmailContent cec,EmailResponse response){
    	if(null==response){
    		if(StringUtils.isNotEmpty(cec.getSendId())){
    			CustomerEmailContent content= customerEmailContentService.getCustomerEmailContent(cec.getSendId());
    			content.setStatus(cec.getStatus());
    			customerEmailContentService.modifyCustomerEmailContent(content);	
    		}else{
    			customerEmailContentService.modifyCustomerEmailContent(cec);
    		}
    		
    	}else{
    		if(StringUtils.isNotEmpty(cec.getSendId())){
    			CustomerEmailContent content= customerEmailContentService.getCustomerEmailContent(cec.getSendId());
    			content.setServerReturnCode(response.getLastReturnCode()!=null?String.valueOf(response.getLastReturnCode()):null);
    			content.setServerReturnMsg(response.getLastServerResponse());
    			content.setServerRespTimestamp(response.getServerRespTimestamp());
    			content.setStatus(cec.getStatus());
    			
    			content.setSendDate(new Date());
    			content.setUpdateDate(new Date());
    			content.setResendCount(cec.getResendCount()+1);
    			customerEmailContentService.modifyCustomerEmailContent(content);
    		}else{
    			cec.setServerReturnCode(response.getLastReturnCode()!=null?String.valueOf(response.getLastReturnCode()):null);
    			cec.setServerReturnMsg(response.getLastServerResponse());
    			cec.setServerRespTimestamp(response.getServerRespTimestamp());
    			customerEmailContentService.modifyCustomerEmailContent(cec);
    		}
    	}
    	
    }
public void sendWithResponse(final Long batchId,final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					 EmailResponse response = MailSender.sendEmailWithBytesMineAttachWithResponse(
							cec.getRecipientEmail(), null, null,
							cec.getSubject(), cec.getContent(),mineTypeList, bytesList,
							fileNameList);
					if (response!=null&&response.isReportSuccess()) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
						deliveryRecordService.updateErrorCountInBatchSendStatementHd(batchId);
					}
					cec.setUpdateDate(new Date());
					if(response!=null){
						saveOrUpdateCustomerEmailContent(cec, response);
					}
				} catch (Exception e) {
					logger.error(e.getMessage(),e);
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
			
		});
		pool.execute(thread);
    }
    
    public void send(final CustomerEmailContent cec) {
    	
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				
				logger.debug("MailThreadServiceImpl email thread run start ...");

				try {

					boolean success = MailSender.sendTextEmail(cec.getRecipientEmail(), null, null, cec.getSubject(),cec.getContent());
					if (success) {
						cec.setStatus(EmailStatus.SENT.getDesc());
					} else {
						cec.setStatus(EmailStatus.FAIL.getDesc());
					}
					cec.setUpdateDate(new Date());
					customerEmailContentService.modifyCustomerEmailContent(cec);

				} catch (Exception e) {

					logger.error(e.getMessage(),e);
				}
				
				logger.debug("MailThreadServiceImpl email thread run end ...");
			}
			
		});
		pool.execute(thread);
    }
}
