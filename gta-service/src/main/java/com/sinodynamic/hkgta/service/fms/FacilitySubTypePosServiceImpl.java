package com.sinodynamic.hkgta.service.fms;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.fms.FacilitySubTypePosDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.fms.FacilitySubtypeDto;
import com.sinodynamic.hkgta.dto.fms.FacilitySubtypePosDto;
import com.sinodynamic.hkgta.dto.fms.FacilitySubtypesDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.fms.FacilitySubTypePos;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;

@Service
public class FacilitySubTypePosServiceImpl extends ServiceBase<FacilitySubTypePos> implements FacilitySubTypePosService {

	@Autowired
	private FacilitySubTypePosDao facilitySubTypePosDao;
	
	@Autowired
	private GlobalParameterDao globalParameterDao;
	
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	
	@Override
	@Transactional
	public void saveFacilitySubTypePos(FacilitySubtypesDto facilitySubtypesDto)
	{
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, Constant.ADVANCEDRESPERIOD_TENNIS);
		if (globalParameter != null) {
			globalParameter.setParamValue(facilitySubtypesDto.getAdvancedResPeriod().toString());
			globalParameterDao.saveOrUpdate(globalParameter);
		}
		List<FacilitySubtypeDto> priceList = facilitySubtypesDto.getPriceList();
		for (FacilitySubtypeDto posDto : priceList) {
			FacilitySubtypePosDto adultHigh = posDto.getAdultHigh();
			FacilitySubtypePosDto adultLow = posDto.getAdultLow();
			FacilitySubtypePosDto childHigh = posDto.getChildHigh();
			FacilitySubtypePosDto childLow = posDto.getChildLow();
			setPosItemPrice(adultHigh,facilitySubtypesDto.getUpdateBy());
			setPosItemPrice(adultLow,facilitySubtypesDto.getUpdateBy());
			setPosItemPrice(childHigh,facilitySubtypesDto.getUpdateBy());
			setPosItemPrice(childLow,facilitySubtypesDto.getUpdateBy());
		}
	}

	private void setPosItemPrice(FacilitySubtypePosDto posDto,String updateBy)
	{
		if(null != posDto){
			FacilitySubTypePos subTypePos = facilitySubTypePosDao.get(FacilitySubTypePos.class, posDto.getSubtypePosId());
			PosServiceItemPrice itemPrice = subTypePos.getPosItemPrice();
			if(itemPrice != null){
				itemPrice.setItemPrice(posDto.getPrice());
				posServiceItemPriceDao.update(itemPrice);
			}else{
				itemPrice = new PosServiceItemPrice();
				itemPrice.setItemNo(Constant.FACILITY_PRICE_PREFIX + FacilityUtilizationPosServiceImpl.addZeroForNum("" + subTypePos, 8));
				itemPrice.setItemCatagory("TFBK");
				itemPrice.setStatus(Constant.General_Status_ACT);
				itemPrice.setItemPrice(posDto.getPrice());
				posServiceItemPriceDao.save(itemPrice);
			}
			if(null ==subTypePos.getCreateBy()){
				subTypePos.setCreateBy(updateBy);
				subTypePos.setCreateDate(new Timestamp(new Date().getTime()));
			}
			subTypePos.setUpdateDate(new Date());
			subTypePos.setUpdateBy(updateBy);
			facilitySubTypePosDao.update(subTypePos);
		}
	}

	@Override
	@Transactional
	public FacilitySubTypePos getFacilitySubTypePos(String subtypeId, String ageRangeCode, String rateType)
	{
		return facilitySubTypePosDao.getFacilitySubTypePos(subtypeId, ageRangeCode, rateType);
	}
}
