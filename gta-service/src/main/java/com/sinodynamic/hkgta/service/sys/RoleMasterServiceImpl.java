package com.sinodynamic.hkgta.service.sys;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.sys.RoleMasterDao;
import com.sinodynamic.hkgta.dao.sys.RoleProgramDao;
import com.sinodynamic.hkgta.dto.sys.RoleDto;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RoleMasterServiceImpl   extends ServiceBase<RoleMaster>  implements RoleMasterService {
	@Autowired
	private RoleMasterDao roleMasterDao;
	
	@Autowired
	private RoleProgramDao roleProgramDao;

	@Override
	@Transactional
	public List<RoleMaster> getgetRoleMasterList() {
		return (List<RoleMaster>) roleMasterDao.getByCol(RoleMaster.class, null, null, "roleId");
	}

	@Override
	@Transactional
	public ResponseResult createRole(RoleMaster role) {
		if (StringUtils.isEmpty(role.getRoleName())) {
			responseResult.initResult(GTAError.SysError.ROLE_NAME_EMPTY);
			return responseResult;
		}
		try {
			RoleMaster existingCourse = roleMasterDao.getUniqueByCol(RoleMaster.class, "roleName", role.getRoleName());
			if (null != existingCourse) {
				responseResult.initResult(GTAError.SysError.ROLE_NAME_ALREADY_EXISTS);
				return responseResult;
			}
			BigInteger roleId = (BigInteger) roleMasterDao.save(role);
			Map<String, BigInteger> newId = new HashMap<String, BigInteger>();
			newId.put("roleId", roleId);
			responseResult.initResult(GTAError.Success.SUCCESS, newId);
		} catch (HibernateException e) {
			logger.error(RoleMasterServiceImpl.class.getName() + " createRole Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_CREATE_NEW_ROLE);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult updateRole(RoleMaster role) {
		if (role.getRoleId()==null) {
			responseResult.initResult(GTAError.SysError.ROLE_ID_EMPTY);
			return responseResult;
		}
		if (StringUtils.isEmpty(role.getRoleName())) {
			responseResult.initResult(GTAError.SysError.ROLE_NAME_EMPTY);
			return responseResult;
		}
		
		if(roleMasterDao.update(role)){
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else{
			responseResult.initResult(GTAError.SysError.FAIL_UPDATE_ROLE);
		}
		return responseResult;
	}
	
	@Transactional
	public boolean validateRole(Long roleId, String roleName) {
		if (roleId == null||roleId == 0)
		{
			/***
			 * when save,check roleName 
			 */
			return roleMasterDao.chkRoleNameExist(roleName);
			
		}
		/***
			 when edit role no check roleName lowercase to uppercase
			 else{
			RoleMaster oldRole = roleMasterDao.get(RoleMaster.class, roleId);
			if(!oldRole.getRoleName().equals(roleName)){
				RoleMaster existingCourse = roleMasterDao.getUniqueByCol(RoleMaster.class, "roleName", roleName);
				if (null != existingCourse) {
					return true;
				}
			}
			}
		*/
		
		
		return false;
	}

	@Override
	@Transactional
	public ResponseResult deleteRole(BigInteger roleId) {
		try {
			
			List<Serializable> param = new ArrayList<Serializable>();
		    param.add(roleId.toString());
			roleProgramDao.hqlUpdate("delete from RoleProgram where id.roleId=?", param);
			Long rolId_longType = Long.parseLong(roleId+"");
			roleMasterDao.deleteById(RoleMaster.class, rolId_longType);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(RoleMasterServiceImpl.class.getName() + " Delete Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_DELETE_ROLE);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult changeStatus(RoleDto dto) {
		if (dto.getRoleId()==null) {
			responseResult.initResult(GTAError.SysError.ROLE_ID_EMPTY);
			return responseResult;
		}
		try {
			RoleMaster r = roleMasterDao.get(RoleMaster.class, dto.getRoleId());
			if(r==null){
				responseResult.initResult(GTAError.SysError.FAIL_CHANGE_ROLE_STATUS);
				return responseResult;
			}
			r.setStatus(dto.getStatus());
			roleMasterDao.update(r);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (HibernateException e) {
			logger.error(RoleMasterServiceImpl.class.getName() + " changeStatus Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_CHANGE_ROLE_STATUS);
		}
		return responseResult;
	}

	@Override
	public String getRoleList()
	{
		return roleMasterDao.getRoleListSQL();
	}

	@Override
	@Transactional
	public List<RoleMaster> getAllRoleList() {
		// TODO Auto-generated method stub
		return roleMasterDao.getByHql("from RoleMaster where status='ACT'");
	}

}
