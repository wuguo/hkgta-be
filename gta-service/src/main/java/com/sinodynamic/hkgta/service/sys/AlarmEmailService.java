package com.sinodynamic.hkgta.service.sys;

import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface AlarmEmailService extends IServiceBase<UserMaster> {

	/**
	 * 线程发送警报邮件
	 */
	public void sendAlarmEmailTask(String taskName, Exception e);

}
