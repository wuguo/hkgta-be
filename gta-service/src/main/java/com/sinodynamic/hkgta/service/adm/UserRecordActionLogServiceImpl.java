package com.sinodynamic.hkgta.service.adm;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.adm.UserRecordActionLogDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileReadLogDao;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.staff.FieldChangeDto;
import com.sinodynamic.hkgta.dto.staff.UserRecordActionLogDto;
import com.sinodynamic.hkgta.entity.adm.UserRecordActionLog;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.SysCodeService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.UserRecordActionType;
import com.sinodynamic.hkgta.util.statement.CustomHeaderFooter;

@Service
public class UserRecordActionLogServiceImpl extends ServiceBase<UserRecordActionLog>
		implements UserRecordActionLogService {
	private Logger logger	= Logger.getLogger(UserRecordActionLogServiceImpl.class);
	@Autowired
	private UserRecordActionLogDao userRecordActionLogDao;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private MailThreadService mailThreadService;

	@Autowired
	private GlobalParameterDao globalParameterDao;

	@Autowired
	private CustomerProfileReadLogDao customerProfileReadLogDao;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Autowired
	private SysCodeService sysCodeService;

	@Resource(name = "appProperties")
	Properties appProps;

	@Override
	public List<UserRecordActionLog> getUserRecordActionLogByDate(Date actionDate) {
		// TODO Auto-generated method stub
		return userRecordActionLogDao.getUserRecordActionLogByDate(actionDate);
	}

	private List<UserRecordActionLogDto> getValidDto(Date actionDate) {
		String hql = "SELECT  m.academy_no AS academyId,u.table_name AS tableName," + " u.action AS action,"
				+ " u.primary_key_value AS primaryKeyValue," + " u.before_image AS beforeImage,"
				+ " u.action_timestamp AS actionTimestamp," + " IFNULL(CONCAT(p.given_name,' ',p.surname ),u.action_user_id) AS actionUserId  "
				+ " FROM user_record_action_log AS u "
				+ "  LEFT JOIN member AS m ON u.primary_key_value=m.customer_id  "
				+ "  LEFT JOIN staff_profile AS p ON p.user_id=u.action_user_id "
				+ " where date_format(u.action_timestamp,'%Y-%m-%d') =DATE_FORMAT(?,'%Y-%m-%d')  and  u.table_name='customer_profile' "
				+ " ORDER BY u.action_timestamp  ASC  ,u.action_user_id ASC ";
		List<Serializable> param = new ArrayList<>();
		param.add(actionDate);

		List<UserRecordActionLogDto> dtos = userRecordActionLogDao.getDtoBySql(hql, param,
				UserRecordActionLogDto.class);
		for (UserRecordActionLogDto logDto : dtos) {
			CustomerProfile newProfile = customerProfileDao
					.getCustomerProfileByCustomerId(Long.valueOf(logDto.getPrimaryKeyValue()));
			
			setFieldChangeList(newProfile, logDto);
		}
		return dtos;

	}
	
	private List<UserRecordActionLogDto> getRecordActionLogList(Date actionDate){
		String sql = "SELECT s.academyId,s.tableName,s.action,s.primaryKeyValue,s.beforeImage,s.actionTimestamp,s.actionUserId,s.readType\n" +
				"FROM (SELECT  m.academy_no AS academyId,u.table_name AS tableName,  u.action AS action,\n" +
				"				u.primary_key_value AS primaryKeyValue,  u.before_image AS beforeImage,\n" +
				"				u.action_timestamp AS actionTimestamp,  IFNULL(CONCAT(p.given_name,' ',p.surname ),u.action_user_id) AS actionUserId,\n" +
				"        '' as readType\n" +
				"				FROM user_record_action_log AS u \n" +
				"				  LEFT JOIN member AS m ON u.primary_key_value=m.customer_id  \n" +
				"				  LEFT JOIN staff_profile AS p ON p.user_id=u.action_user_id \n" +
				"				  where date_format(u.action_timestamp,'%Y-%m-%d') =DATE_FORMAT(?,'%Y-%m-%d')  and  u.table_name='customer_profile' \n" +
				"      UNION ALL\n" +
				"      SELECT m.academy_no AS academyId,\n" +
				"			 'customer_profile' as tableName,\n" +
				"			 'R' as action,\n" +
				"			 '' as primaryKeyValue,\n" +
				"			 '' as beforeImage,\n" +
				"			 lo.read_timestamp AS actionTimestamp,\n" +
				"			 IFNULL(CONCAT(s.given_name,' ',s.surname),lo.reader_user_id) AS actionUserId,\n" +
				"			 lo.read_type AS readType\n" +
				"			 FROM customer_profile_read_log AS lo\n" +
				"			 LEFT JOIN  member AS m ON lo.customer_id=m.customer_id \n" +
				"			 LEFT JOIN staff_profile AS s ON lo.reader_user_id=s.user_id\n" +
				"				 where date_format(lo.read_timestamp,'%Y-%m-%d') =DATE_FORMAT(?,'%Y-%m-%d') \n" +
				") s ORDER BY s.actionTimestamp ASC";
		
		List<Serializable> param = new ArrayList<>();
		param.add(actionDate);
		param.add(actionDate);

		List<UserRecordActionLogDto> dtos = userRecordActionLogDao.getDtoBySql(sql, param,
				UserRecordActionLogDto.class);
		for (UserRecordActionLogDto logDto : dtos) {
			if(!StringUtils.isEmpty(logDto.getPrimaryKeyValue())){
				CustomerProfile newProfile = customerProfileDao
						.getCustomerProfileByCustomerId(Long.valueOf(logDto.getPrimaryKeyValue()));
				setPortraitPhotoAndSignatureMD5(newProfile);
				setFieldChangeList(newProfile, logDto);
			}
		}
		return dtos;
	}
	private void setPortraitPhotoAndSignatureMD5(CustomerProfile newProfile )
	{
		CustomerProfileDto dto=customerProfileDao.getCustomerProfilePhotoAndSinature(newProfile.getCustomerId());
		if(null!=dto){
			String portMd5="";
			String sigMd5="";
			if(StringUtils.isNotEmpty(dto.getPortraitPhoto()))
			{
				portMd5=DigestUtils.md5DigestAsHex(CommUtil.base64ToImage(dto.getPortraitPhoto()));
			}
			if(StringUtils.isNotEmpty(dto.getSignature()))
			{
				sigMd5=DigestUtils.md5DigestAsHex(CommUtil.base64ToImage(dto.getSignature()));
			}
			newProfile.setPortraitPhoto(portMd5);
			newProfile.setSignature(sigMd5);
		}
		
	}

	/***
	 * set Field Change
	 * 
	 * @param originalProfile
	 * @param logDto
	 */
	private void setFieldChangeList(CustomerProfile newProfile, UserRecordActionLogDto logDto) {
		List<FieldChangeDto> fields = new ArrayList<>();
		// get newProfile from user_record_action_log json
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
		CustomerProfile originalProfile = gson.fromJson(logDto.getBeforeImage(), CustomerProfile.class);

		// get all fieldName
		String[] newFieldNames = getFiledName(newProfile);

		for (String newFieldName : newFieldNames) {
			//filter field
			if (newFieldName.equalsIgnoreCase("serialVersionUID")||newFieldName.equalsIgnoreCase("customerAdditionInfos")||newFieldName.equalsIgnoreCase("customerAddresses"))
				continue;
			// get field value
			Object newObject = getFieldValueByName(newFieldName, newProfile);
			FieldChangeDto field = new FieldChangeDto();
			field.setFieldName(newFieldName);
			
			setNewFieldValue(field, newFieldName, newObject);
			// only change ,compare
			if (logDto.getAction().equalsIgnoreCase(UserRecordActionType.C.name())) {
				setOriginaFieldValue(fields, field, originalProfile, newFieldName, newObject);
			} else {
				fields.add(field);
			}
		}
		logDto.setFields(fields);
	}
	private void setNewFieldValue(FieldChangeDto field,String newFieldName,Object newObject){
		if ("createDate".equals(newFieldName) || "updateDate".equals(newFieldName)) {
			Date time = (Date) newObject;
			field.setNewValue(
					(null != newObject ? DateConvertUtil.parseDate2String(time, "yyyy-MM-dd HH:mm:ss") : null));
		}else if ("portraitPhoto".equals(newFieldName) || "signature".equals(newFieldName))
		{
			field.setNewValue(StringUtils.isNotEmpty(newObject.toString()) ? "changed" : null);
		}
		else {
			field.setNewValue((null != newObject ? newObject.toString() : null));
		}
	}
	private void setOriginaFieldValue(List<FieldChangeDto> fields,FieldChangeDto field,CustomerProfile originalProfile,String newFieldName,Object newObject){
		Object orObject = getFieldValueByName(newFieldName, originalProfile);
		if (null != orObject) {
			if ("createDate".equals(newFieldName) || "updateDate".equals(newFieldName)) {
				Date newTime = (Date) newObject;
				Date orTime = (Date) orObject;
				if (newTime.compareTo(orTime) != 0) {
					
					field.setOriginalValue(DateConvertUtil.parseDate2String(orTime, "yyyy-MM-dd HH:mm:ss"));
					fields.add(field);
				}
			}
			else if ("portraitPhoto".equals(newFieldName) || "signature".equals(newFieldName))
			{
				if (!orObject.equals(newObject)) {
					if(StringUtils.isNotEmpty(orObject.toString()))
					{
						field.setOriginalValue("changed");
					}
					if(StringUtils.isNotEmpty(field.getNewValue()))
					{
					  field.setNewValue("changed");	
					}
					fields.add(field);
				}
			}
			else {
				if (!orObject.equals(newObject)) {
					field.setOriginalValue(orObject.toString());
					fields.add(field);
				}
			}
		} else {
			field.setOriginalValue(null);
			fields.add(field);
		}
	}
	@Override
	@Transactional
	public String excute(Date actionDate) {
		String basePath = appProps.getProperty("pdf.server.report");
		File file = new File(basePath + "/patronActivity/");
		String generateMonth = DateConvertUtil.date2String(actionDate, "yyyyMMdd");
		String filePath = file.getPath() + "/DailyPatronProfileActivity_" + generateMonth + ".pdf";
		String logoPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		logoPath = logoPath.substring(0, logoPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/logo_header.jpg";
		if (!file.exists()) {
			file.mkdirs();
		}

		OutputStream out = null;
		try {
			out = new FileOutputStream(filePath);
			Document document = new Document(PageSize.A4, 10, 10, 110, 50);
			PdfWriter writer = PdfWriter.getInstance(document, out);
			this.addHeader(writer, document, logoPath);
			this.addTitle(writer, document, "Daily Patron Profile Activity Report");
			this.addTable(document, 2, new String[] { "Date" },
					new String[] { DateConvertUtil.date2String(actionDate, "yyyy/MM/dd") });
			document.add(this.getParagraph("Activity:", new Font(Font.TIMES_ROMAN, 12, Font.BOLD), 20l, 10l, 60l));
			
			// add customer_profile add/change/read log
			List<UserRecordActionLogDto> logDtos = getRecordActionLogList(actionDate);
			for (UserRecordActionLogDto dto : logDtos) {
				if(UserRecordActionType.R.name().equals(dto.getAction())){
					String[] columnsName = new String[] { "Patron Id", "Operator", "Action", "Time", "ReadType" };
					String[] columnsValue = new String[] { dto.getAcademyId(), dto.getActionUserId(), "Read",
							DateConvertUtil.parseDate2String(dto.getActionTimestamp(), "HH:mm:ss"),
							"F".equals(dto.getReadType()) ? "Full" : "" };
					this.addTable(document, 2, columnsName, columnsValue);
				}else{
					this.addTableContent(document, dto);
				}
			}
			document.close();
		} catch (Exception e) {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR);
			logger.error(e.getMessage(),e);
			return null;
		} finally {
			if (null != out) {
				try {
					out.flush();
					out.close();
				} catch (Exception e) {
					logger.error(e.getMessage(),e);
				}
			}
		}
		return filePath;
	}

	private void addHeader(PdfWriter writer, Document document, String logoPath)
			throws BadElementException, MalformedURLException, IOException {
		Image logo = Image.getInstance(logoPath);
		logo.setAlignment(Image.MIDDLE);
		logo.setAbsolutePosition(0, 0);
		logo.scalePercent(25);
		Chunk chunk = new Chunk(logo, 0, 0, true);
		Phrase phrase = new Phrase();
		phrase.add(chunk);
		Rectangle rect = new Rectangle(36, 54, 559, 835);
		CustomHeaderFooter header = new CustomHeaderFooter();
		writer.setPageEvent(header);
		document.open();
		header.onEndPage(writer, document, rect, phrase);
	}

	private void addTitle(PdfWriter writer, Document document, String titleContent) {
		try {
			Font titleFont = new Font(Font.TIMES_ROMAN, 20, Font.BOLD);
			Color color = new Color(140, 0, 30);
			titleFont.setColor(color);

			Paragraph title = getParagraph(titleContent, titleFont, 20f, 0f, 0f);
			title.setAlignment(Element.ALIGN_CENTER);
			title.setSpacingAfter(50l);
			document.add(title);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addTable(Document document, int numColumns, String[] columnsName, String[] columnsValue)
			throws DocumentException {
		Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		PdfPTable infoPdfPTable = new PdfPTable(numColumns);
		PdfPCell infoCell = null;
		for (int i = 0; i < columnsName.length; i++) {
			// column name
			infoCell = createCell();
			infoCell.setPhrase(new Phrase(columnsName[i], contentFont));
			infoPdfPTable.addCell(infoCell);
			// column value
			infoCell = createCell();
			infoCell.setPhrase(new Phrase(columnsValue[i], contentFont));
			infoPdfPTable.addCell(infoCell);
		}
		infoPdfPTable.setSpacingBefore(20l);
		document.add(infoPdfPTable);

	}

	private void addTableContent(Document document, UserRecordActionLogDto dto) throws Exception {
		Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		PdfPTable infoPdfPTable = new PdfPTable(6);

		PdfPCell infoCell = this.createCell();
		infoCell.setPhrase(new Phrase("Patron Id", contentFont));
		infoCell.setColspan(3);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(new Phrase(dto.getAcademyId(), contentFont));
		infoCell.setColspan(3);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(new Phrase("Operator", contentFont));
		infoCell.setColspan(3);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(new Phrase(dto.getActionUserId(), contentFont));
		infoCell.setColspan(3);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(new Phrase("Action", contentFont));
		infoCell.setColspan(3);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();

		String action = null;
		if (UserRecordActionType.A.name().equals(dto.getAction())) {
			action = UserRecordActionType.A.getDesc();
		} else if (UserRecordActionType.C.name().equals(dto.getAction())) {
			action = UserRecordActionType.C.getDesc();
		} else if (UserRecordActionType.D.name().equals(dto.getAction())) {
			action = UserRecordActionType.D.getDesc();
		} else if (UserRecordActionType.R.name().equals(dto.getAction())) {
			action = UserRecordActionType.R.getDesc();
		}
		infoCell.setPhrase(new Phrase(action, contentFont));
		infoCell.setColspan(3);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(new Phrase("Time", contentFont));
		infoCell.setColspan(3);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(
				new Phrase(DateConvertUtil.parseDate2String(dto.getActionTimestamp(), "HH:mm:ss"), contentFont));
		infoCell.setColspan(3);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(new Phrase("Field", contentFont));
		infoCell.setColspan(2);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(new Phrase("Original Value", contentFont));
		infoCell.setColspan(2);
		infoPdfPTable.addCell(infoCell);

		infoCell = this.createCell();
		infoCell.setPhrase(new Phrase("New Value", contentFont));
		infoCell.setColspan(2);
		infoPdfPTable.addCell(infoCell);

		List<FieldChangeDto> fields = dto.getFields();
		for (FieldChangeDto field : fields) {
			if ((StringUtils.isEmpty(field.getOriginalValue()) && StringUtils.isEmpty(field.getNewValue()))
					|| (field.getOriginalValue().equals("-") && field.getNewValue().equals("-")))
				continue;

			infoCell = this.createCell();
			infoCell.setPhrase(new Phrase(field.getFieldName(), contentFont));
			infoCell.setColspan(2);
			infoPdfPTable.addCell(infoCell);
			
			if("businessNature".equals(field.getFieldName()) || "nationality".equals(field.getFieldName()) || "education".equals(field.getFieldName())
					|| "relationshipCode".equals(field.getFieldName()) || "maritalStatus".equals(field.getFieldName())){
				List<SysCode> configs = sysCodeService.getSysCodeByCategory(field.getFieldName().trim());
				Map<String,String> sysCodeMap = new HashMap<String,String>();
				for (SysCode sc : configs) {
					sysCodeMap.put(sc.getCodeValue(), sc.getCodeDisplay());
				}
	
				infoCell = this.createCell();
				infoCell.setPhrase(new Phrase(field.getOriginalValue().equals("-") ? field.getOriginalValue() : sysCodeMap.get(field.getOriginalValue()), contentFont));
				infoCell.setColspan(2);
				infoPdfPTable.addCell(infoCell);
	
				infoCell = this.createCell();
				infoCell.setPhrase(new Phrase(field.getNewValue().equals("-") ? field.getNewValue() : sysCodeMap.get(field.getNewValue()), contentFont));
				infoCell.setColspan(2);
				infoPdfPTable.addCell(infoCell);
			}
			else{
				infoCell = this.createCell();
				infoCell.setColspan(2);
				infoCell.setPhrase(new Phrase(field.getOriginalValue(), contentFont));
				infoPdfPTable.addCell(infoCell);
				
				infoCell = this.createCell();
				infoCell.setColspan(2);
				infoCell.setPhrase(new Phrase(field.getNewValue(), contentFont));
				infoPdfPTable.addCell(infoCell);
				
			}
		}
		infoPdfPTable.setSpacingBefore(20l);
		document.add(infoPdfPTable);

	}

	private PdfPCell createCell() {
		PdfPCell infoCell = new PdfPCell();
		infoCell.setBorderWidthBottom(0.5f);
		infoCell.setBorderWidthLeft(0.5f);
		infoCell.setBorderWidthRight(0.5f);
		infoCell.setBorderWidthTop(0.5f);
		return infoCell;
	}

	private Paragraph getParagraph(String content, Font font, float before, float after, float indentationLeft) {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Phrase(content, font));
		paragraph.setSpacingBefore(before);
		paragraph.setSpacingAfter(after);
		paragraph.setIndentationLeft(indentationLeft);
		return paragraph;
	}

	/**
	 * get object filedName
	 */
	private String[] getFiledName(Object o) {
		Field[] fields = o.getClass().getDeclaredFields();
		String[] fieldNames = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			fieldNames[i] = fields[i].getName();
		}
		return fieldNames;
	}

	private Object getFieldValueByName(String fieldName, Object o) {
		try {
			String firstLetter = fieldName.substring(0, 1).toUpperCase();
			String getter = "get" + firstLetter + fieldName.substring(1);
			Method method = o.getClass().getMethod(getter, new Class[] {});
			Object value = method.invoke(o, new Object[] {});
			return value;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	@Transactional
	public ResponseMsg save(CustomerProfile customerProfile, String loginUserId, String action) {
		try {
			UserRecordActionLog userRecordActionLog = null;
			// edit
			if (null != customerProfile.getCustomerId()) {
				userRecordActionLog = getUserRecordActionLogByAction(customerProfile, action);
			} else {
				// save
				userRecordActionLog = getUserRecordActionLogByAction(customerProfile, action);
			}
			userRecordActionLog.setActionUserId(loginUserId);
			userRecordActionLogDao.save(userRecordActionLog);
			responseMsg.initResult(GTAError.Success.SUCCESS);

		} catch (Exception ex) {
			responseMsg.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR);
		}
		return responseMsg;
	}

	/***
	 * 
	 * @param action
	 *            A/D/C
	 * @return
	 */
	private UserRecordActionLog getUserRecordActionLogByAction(CustomerProfile customerProfile, String action) {
		UserRecordActionLog userRecordActionLog = new UserRecordActionLog();
		userRecordActionLog.setAction(action);
		userRecordActionLog.setActionTimestamp(new Date());
		userRecordActionLog.setPrimaryKeyValue(customerProfile.getCustomerId().toString());
		userRecordActionLog.setTableName("customer_profile");
		/***
		 * add photo/signature
		 */
		if(UserRecordActionType.C.name().equals(action)){
			String porMd5="";
			String sigMd5="";
			CustomerProfileDto psDto=customerProfileDao.getCustomerProfilePhotoAndSinature(customerProfile.getCustomerId());
			if(null!=psDto){
				if(StringUtils.isNotEmpty(psDto.getPortraitPhoto())){
					porMd5=DigestUtils.md5DigestAsHex(CommUtil.base64ToImage(psDto.getPortraitPhoto()));
				}
				if(StringUtils.isNotEmpty(psDto.getSignature())){
					sigMd5=DigestUtils.md5DigestAsHex(CommUtil.base64ToImage(psDto.getSignature()));
				}
			}
			customerProfile.setPortraitPhoto(porMd5);
			customerProfile.setSignature(sigMd5);
		}
		
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
		userRecordActionLog.setBeforeImage(gson.toJson(customerProfile).toString());
		return userRecordActionLog;
	}

	@Override
	@Transactional
	public void sendEmailActivityReport(String reportPath) {
		/***
		 * get config staff email address
		 */
		String email = null;
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, "STAFF_MAIL_DAILY_PATRON");
		if (null != globalParameter) {
			email = globalParameter.getParamValue();
		}
		if (null == email)
			return;

		File file = new File(reportPath);
		if (file.exists()) {
			try {
				InputStream inputStream = new FileInputStream(reportPath);
				
				List<byte[]> bytesList = new ArrayList<byte[]>();
				List<String> mineTypeList = Arrays.asList("application/pdf");
				List<String> fileNameList = Arrays.asList(file.getName());
				bytesList.add(IOUtils.toByteArray(inputStream));
				
				CustomerEmailContent mail=saveEmailContentOrAttach(email, file.getName());
				
				mailThreadService.sendWithResponse(mail, bytesList, mineTypeList, fileNameList);

			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
	}
	private CustomerEmailContent saveEmailContentOrAttach(String email,String fileName)
	{
		CustomerEmailContent mail = new CustomerEmailContent();
		mail.setRecipientEmail(email);
		mail.setSendDate(new Date());
		mail.setSubject("Daily Patron Profile Activity Report");
		mail.setStatus(EmailStatus.PND.name());
		
		String sendId = (String) customerEmailContentDao.save(mail);
		
		mail.setSendId(sendId);
		CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
		customerEmailAttach.setEmailSendId(mail.getSendId());
		customerEmailAttach.setAttachmentName(fileName);
		customerEmailAttachDao.save(customerEmailAttach);
		
		return mail;
	}
}
