package com.sinodynamic.hkgta.service.rpos;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sinodynamic.hkgta.dao.AbstractCommonDataQueryDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollPoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.DeliveryRecordDao;
import com.sinodynamic.hkgta.dao.crm.HomePageSummaryDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashvalueBalHistoryDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MemberTransactionLogDao;
import com.sinodynamic.hkgta.dao.crm.MemberTypeDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.RemarksDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanOfferPosDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanPosDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.ical.GtaPublicHolidayDao;
import com.sinodynamic.hkgta.dao.onlinepayment.ItemPaymentAccCodeDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerTransactionDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.Constant.Purchase_Daypass_Type;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.CoachesPerformanceDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.ExpiryDateDto;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.crm.MarkMessageDto;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.PatronAccountStatementDto;
import com.sinodynamic.hkgta.dto.crm.PatronInfoDto;
import com.sinodynamic.hkgta.dto.crm.PatronServicePlanDto;
import com.sinodynamic.hkgta.dto.crm.PatronTransReportDto;
import com.sinodynamic.hkgta.dto.crm.ReservationDto;
import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.dto.crm.SettlementReportDto;
import com.sinodynamic.hkgta.dto.crm.SourceBookingDto;
import com.sinodynamic.hkgta.dto.crm.TransactionDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.dto.fms.CourseCoachCalendarDto;
import com.sinodynamic.hkgta.dto.fms.CustomerRefundRequestDto;
import com.sinodynamic.hkgta.dto.fms.PatronActivitiesDto;
import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.rpos.CashValueLogDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerTransactionListDto;
import com.sinodynamic.hkgta.dto.rpos.RefundInfoDto;
import com.sinodynamic.hkgta.dto.rpos.TempDto;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.dto.statement.CustomerStatementBaseInfoDto;
import com.sinodynamic.hkgta.dto.statement.StatementDto;
import com.sinodynamic.hkgta.dto.statement.StatementParamsDto;
import com.sinodynamic.hkgta.entity.crm.BatchSendStatementHd;
import com.sinodynamic.hkgta.entity.crm.BatchSendStatementList;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalueBalHistory;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.crm.MemberType;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.entity.crm.ServicePlanOfferPos;
import com.sinodynamic.hkgta.entity.crm.ServicePlanPos;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.onlinepayment.ItemPaymentAccCode;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.backoffice.coachmanage.CoachManagementService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.fms.MemberReportService;
import com.sinodynamic.hkgta.service.fms.ReservationReportService;
import com.sinodynamic.hkgta.service.fms.ReservationService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Facility;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.School;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.Constant.memberType;
import com.sinodynamic.hkgta.util.constant.CustomerServiceStatus;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.FacilityTypeReport;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.OrderStatus;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.SurveyType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.report.JasperUtil;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.NetValue;
import com.sinodynamic.hkgta.util.response.ResponseResult;
import com.sinodynamic.hkgta.util.response.UsageRageData;
import com.sinodynamic.hkgta.util.statement.CustomHeaderFooter;

import au.com.bytecode.opencsv.CSVWriter;
import net.sf.jasperreports.engine.JRException;

@Service
@Scope("prototype")
public class CustomerOrderTransServiceImpl extends ServiceBase<CustomerOrderTrans>implements CustomerOrderTransService {
	private Logger ecrLog = Logger.getLogger(LoggerType.ECRLOG.getName());
	protected  final Logger logger = Logger.getLogger(CustomerOrderTransServiceImpl.class); 
	@Autowired
	CustomerRefundRequestDao customerRefundRequestDao;
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	@Autowired
	private CustomerTransactionDao customerTransactionDao;
	@Autowired
	CustomerOrderHdDao customerOrderHdDao;
	@Autowired
	private CustomerProfileDao customerProfileDao;
	@Autowired
	private ServicePlanPosDao servicePlanPosDao;
	@Autowired
	private RemarksDao remarkDao;
	@Autowired
	private MemberReportService memberReportService;
	@Autowired
	private ReservationService reservationService;
	@Autowired
	private ReservationReportService reservationReportService;
	@Autowired
	private GtaPublicHolidayDao		gtaPublicHolidayDao;

	@Autowired
	@Qualifier("commonBySQLDao")
	AbstractCommonDataQueryDao abstractCommonDataQueryDao;
	
	@Autowired
	private CoachManagementService coachManagementService;

	@Autowired
	private HomePageSummaryDao homePageSummaryDao;

	@Autowired
	private ServicePlanOfferPosDao servicePlanOfferPosDao;

	@Autowired
	private PaymentGatewayDao paymentGatewayDao;

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;

	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;

	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;

	@Autowired
	private CustomerEnrollPoDao customerEnrollPoDao;

	@Autowired
	private MemberDao memberDao;

	@Autowired
	private MemberTypeDao memberTypeDao;

	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;

	@Autowired
	private MemberCashValueDao memberCashValueDao;

	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;

	@Autowired
	private MemberCashvalueBalHistoryDao memberCashvalueBalHistoryDao;

	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;

	@Autowired
	private MemberTransactionLogDao memberTransactionLogDao;

	@Autowired
	private MessageTemplateDao messageTemplateDao;

	@Autowired
	private MailThreadService mailThreadService;

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;

	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;

	@Autowired
	private DeliveryRecordDao deliveryRecordDao;

	@Autowired
	private ServicePlanDao servicePlanDao;

	@Autowired
	private SysCodeDao sysCodeDao;

	@Autowired
	private CustomerServiceDao customerServiceDao;

	@Autowired
	private CustomerServiceSubscribeDao customerServiceSubscribeDao;

	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;

	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;

	// added by Kaster 20160329
	@Autowired
	private UserMasterDao userMasterDao;

	@Resource(name = "appProperties")
	Properties appProps;
	
	@Resource(name = "ddxProperties")
	protected Properties ddxProps;
	
	@Autowired
	private ItemPaymentAccCodeDao itemPaymentAccCodeDao;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult savecustomerOrderTransService(CustomerOrderTransDto customerOrderTransDto,
			Map<String, Long> transactionNoMap) throws Exception {

		String paymentMethodCode = customerOrderTransDto.getPaymentMethodCode();
		CustomerOrderTrans customerOrderTrans = null;
		boolean newOrUpdateFileFlag = true;

		if (customerOrderTransDto.getTransactionNo() != null) {
			customerOrderTrans = this.customerOrderTransDao.get(CustomerOrderTrans.class,
					customerOrderTransDto.getTransactionNo());
			if ((PaymentMethod.BT.name().equalsIgnoreCase(paymentMethodCode)
					|| PaymentMethod.CHEQUE.name().equalsIgnoreCase(paymentMethodCode))
					&& StringUtils.isEmpty(customerOrderTrans.getTranscriptFileName())
					&& StringUtils.isEmpty(customerOrderTransDto.getTranscriptFileName())) {
				throw new GTACommonException(GTAError.CustomerOrderTrans.ATTACH_REQ);
			}
			String transcriptFileName = customerOrderTrans.getTranscriptFileName();
			int index = transcriptFileName.lastIndexOf("/");
			if (transcriptFileName != null && index >= 0) {
				transcriptFileName = transcriptFileName.substring(index);
			}
			String dtotTanscriptFileName = customerOrderTransDto.getTranscriptFileName();
			index = dtotTanscriptFileName.lastIndexOf("/");
			if (dtotTanscriptFileName != null && index >= 0) {
				dtotTanscriptFileName = dtotTanscriptFileName.substring(index);
			}
			// if exist transcriptFileName equals FE's transcriptFileName
			// no,change for upload transcript file
			if (!transcriptFileName.equals(dtotTanscriptFileName)) {
				customerOrderTrans.setTranscriptFileName(customerOrderTransDto.getTranscriptFileName());
			} else {
				newOrUpdateFileFlag = false;
			}
		} else {
			customerOrderTrans = new CustomerOrderTrans();
			initCustomerOrderTrans(customerOrderTrans);
			if ((PaymentMethod.BT.name().equalsIgnoreCase(paymentMethodCode)
					|| PaymentMethod.CHEQUE.name().equalsIgnoreCase(paymentMethodCode))
					&& StringUtils.isEmpty(customerOrderTransDto.getTranscriptFileName())) {
				throw new GTACommonException(GTAError.CustomerOrderTrans.ATTACH_REQ);
			}
			customerOrderTrans.setCreateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setCreateDate(new Date());
			customerOrderTrans.setTranscriptFileName(customerOrderTransDto.getTranscriptFileName());
		}
		CustomerOrderHd customerOrderHd = customerTransactionDao.get(CustomerOrderHd.class,
				customerOrderTransDto.getOrderNo());
		/*
		 * if ("CMP".equals(customerOrderHd.getOrderStatus())) { throw new
		 * GTACommonException(GTAError.CustomerOrderTrans.TRANS_CMP); //Even if
		 * the order is complete, the transaction of the order can also be void
		 * and change the order status to be OPEN }
		 */

		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setPaidAmount(customerOrderTransDto.getPaidAmount());
		
		if (PaymentMethod.VISA.name().equalsIgnoreCase(paymentMethodCode) || PaymentMethod.CUP.name().equalsIgnoreCase(paymentMethodCode)) {
			// update for SSG-791, when pay by Credit Card, default
			// paymentMediaType should be 'ECR'
			
			if(!StringUtils.isEmpty(customerOrderTransDto.getTerminalType())&&
					PaymentMediaType.ECR.name().equalsIgnoreCase(customerOrderTransDto.getTerminalType()))
			{
				customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());	
			}else{
				customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			}
			
		} else if (!PaymentMethod.CASH.name().equalsIgnoreCase(paymentMethodCode)) {
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		}
		/***
		 * hander offline paymethodcode VISA 
		 */
		if(customerOrderTransDto.getPaymentMethodCode().indexOf("_")!=-1)
		{
			customerOrderTransDto.setPaymentMethodCode(customerOrderTransDto.getPaymentMethodCode().split("_")[0]);
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OF.name());
		}
		
		customerOrderTrans.setPaymentMethodCode(customerOrderTransDto.getPaymentMethodCode());
		
		
		if(!StringUtils.isEmpty(customerOrderTransDto.getTerminalId())){
			customerOrderTrans.setTerminalId(customerOrderTransDto.getTerminalId());
		}
		customerOrderTrans.setTransactionTimestamp(new Date());
		customerOrderTrans.setPaymentRecvBy(customerOrderTransDto.getPaymentReceivedBy());
		customerOrderTrans.setAgentTransactionNo(customerOrderTransDto.getAgentTransactionNo());

		// Validation input
		if (customerOrderTrans.getPaidAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new GTACommonException(GTAError.CustomerOrderTrans.PAID_AMOUNT_LESS_ZERO);
		}
		List<String> validStatusList = Arrays.asList(null, "", "SUC", "FAIL", "VOID");
		if (validStatusList.contains(customerOrderTrans.getStatus()) == false) {
			throw new GTACommonException(GTAError.CustomerOrderTrans.INACCURATE_STATUS);
		}

		// Payment total checking
		BigDecimal balanceDueSalesMan = homePageSummaryDao.countBalanceDue(customerOrderTransDto.getOrderNo());
		BigDecimal balanceDueAccountant = customerTransactionDao
				.countBalanceDueStatus(customerOrderTransDto.getOrderNo());
		if (!StringUtils.isEmpty(customerOrderTransDto.getTransactionNo())) {
			CustomerOrderTrans customerOrderTransOld = customerOrderTransDao.get(CustomerOrderTrans.class,
					customerOrderTransDto.getTransactionNo());
			if (customerOrderTransDto.getStatus().equals("SUC")) {
				if (balanceDueAccountant.subtract(customerOrderTrans.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
					throw new GTACommonException(GTAError.CustomerOrderTrans.PAID_AMOUNT_OVER,
							new Object[] { balanceDueSalesMan });
				}
			} else if (!StringUtils.isEmpty(customerOrderTransDto.getStatus())) {
				if (balanceDueSalesMan.subtract(customerOrderTrans.getPaidAmount())
						.add(customerOrderTransOld.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
					throw new GTACommonException(GTAError.CustomerOrderTrans.PAYMENT_AMOUNT_OVER,
							new Object[] { balanceDueSalesMan });
				}
			}
			customerOrderTransDao.evict(customerOrderTransOld);
		}
		if (customerOrderTransDto.getTransactionNo() == null) {// Add new
																// payment case
			if (balanceDueSalesMan.compareTo(BigDecimal.ZERO) <= 0) {
				throw new GTACommonException(GTAError.CustomerOrderTrans.NO_PAYMENT_REQ);
			} else if (balanceDueSalesMan.subtract(customerOrderTrans.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
				throw new GTACommonException(GTAError.CustomerOrderTrans.PAID_AMOUNT_OVER,
						new Object[] { balanceDueSalesMan });
			}
		}

		// Audit Action for Verify
		boolean auditAction = PaymentMethod.BT.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
				|| PaymentMethod.CHEQUE.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
				|| PaymentMethod.VISA.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
				|| PaymentMethod.CASH.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode());
		if (customerOrderTransDto.getTransactionNo() != null && auditAction) {
			customerOrderTrans.setUpdateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
			customerOrderTrans.setAuditBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setAuditDate(new Timestamp(new Date().getTime()));
		}

		// there is no approved for CASH
		if (("CASH".equalsIgnoreCase(paymentMethodCode) && !"VOID".equals(customerOrderTransDto.getStatus()))
				||customerOrderTrans.getPaymentMedia().equalsIgnoreCase(PaymentMediaType.OF.name())) {
			customerOrderTrans.setStatus(CustomerTransationStatus.SUC.name());
			customerOrderTrans.setUpdateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
			customerOrderTrans.setAuditBy(customerOrderTransDto.getCreateByUserId());
			customerOrderTrans.setAuditDate(new Timestamp(new Date().getTime()));
		} else {
			customerOrderTrans.setStatus(customerOrderTransDto.getStatus());
		}

		// Append payment location code
		if (StringUtils.isEmpty(customerOrderTrans.getPaymentLocationCode())) {
			customerOrderTrans.setPaymentLocationCode(customerOrderTransDto.getPaymentLocationCode());
		}

		// end of payment total checking
		Long transNo = null;
		if (null == customerOrderTrans.getTransactionNo() || 0 == customerOrderTrans.getTransactionNo()) {
			transNo = (Long) customerOrderTransDao.save(customerOrderTrans);
			// add customer order trans to DB and need send mail to accountant
			if (null != transNo) {
				customerEnrollmentService.sendMailToAccountantByCustomerId(customerOrderHd.getCustomerId(),
						customerOrderTrans.getPaidAmount());
			}
		} else {
			customerOrderTransDao.saveOrUpdate(customerOrderTrans);
			transNo = customerOrderTransDto.getTransactionNo();
		}

		// Used to change enrollment status from Approved to Payment Approved or
		// Payment Approved to Approved
		// updateEnrollmentStatusOncePayementChange(transNo);
		updateEnrollmentStatusOncePayementChange(transNo, customerOrderTransDto.getStatus());

		transactionNoMap.put("transactionNo", customerOrderTrans.getTransactionNo());
		transactionNoMap.put("customerId", customerOrderHd.getCustomerId());
		if (null != customerOrderTrans.getTransactionNo() && null != customerOrderHd.getCustomerId()
				&& newOrUpdateFileFlag
				&& !PaymentMethod.CASH.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())) {
			this.moveTranscriptFile(customerOrderTrans.getTransactionNo(), customerOrderHd.getCustomerId());
		}

		CustomerProfile temProfile = customerProfileDao.getCustomerProfileByCustomerId(customerOrderHd.getCustomerId());
		CustomerEnrollment customerEnrollment = customerEnrollmentDao
				.getCustomerEnrollmentByCustomerId(customerOrderHd.getCustomerId());
		if (CustomerTransationStatus.SUC.name().equals(customerOrderTrans.getStatus())) {
			confirmSendEmailAfterPaymentForEnrollmentAndRenewal(transNo, Constant.TEMPLATE_ID_RECEIPT,
					customerOrderTransDto.getCreateByUserId(), customerOrderTransDto.getCreateByUserName());
			// add mobile notification
			devicePushService.pushMessage(new String[] { customerEnrollment.getSalesFollowBy() },
					Constant.TEMPLATE_ID_TRANSACTION_SUCCESS_NOTIFICATION,
					new String[] { temProfile.getGivenName() + " " + temProfile.getSurname() },
					Constant.SALESKIT_PUSH_APPLICATION);
		} else if (CustomerTransationStatus.FAIL.name().equals(customerOrderTrans.getStatus())
				|| CustomerTransationStatus.VOID.name().equals(customerOrderTrans.getStatus())) {
			devicePushService.pushMessage(new String[] { customerEnrollment.getSalesFollowBy() },
					Constant.TEMPLATE_ID_TRANSACTION_FAIL_NOTIFICATION,
					new String[] { temProfile.getGivenName() + " " + temProfile.getSurname() },
					Constant.SALESKIT_PUSH_APPLICATION);
		}
		// If the total amount of all transactions of CustomerOrderHd is equals
		// to CustomerOrderHd's amount, the CustomerOrderHd's status should be
		// changed to Complete(CMP)
		List<CustomerOrderTrans> customerOrderTranses = customerOrderHd.getCustomerOrderTrans();
		BigDecimal totalAmount = BigDecimal.ZERO;
		for (CustomerOrderTrans tempTrans : customerOrderTranses) {
			if (CustomerTransationStatus.SUC.getDesc().equals(tempTrans.getStatus())) {
				totalAmount = totalAmount.add(tempTrans.getPaidAmount());
			}
		}

		if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount) == 0) {
			customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
			customerOrderHd.setUpdateDate(new Date());
			customerOrderHd.setUpdateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderHdDao.update(customerOrderHd);
			List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
			if (customerOrderDets.size() > 0
					&& customerOrderDets.get(0).getItemNo().startsWith(Constant.RENEW_ITEM_NO_PREFIX)) {
				handleRenewPayment(customerOrderHd, customerOrderTransDto.getCreateByUserId());
			}
		} else if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount) != 0
				&& !OrderStatus.REJ.name().equalsIgnoreCase(customerOrderHd.getOrderStatus())
				&& !OrderStatus.CAN.name().equalsIgnoreCase(customerOrderHd.getOrderStatus())) {
			customerOrderHd.setOrderStatus(OrderStatus.OPN.name());
			customerOrderHd.setUpdateDate(new Date());
			customerOrderHd.setUpdateBy(customerOrderTransDto.getCreateByUserId());
			customerOrderHdDao.update(customerOrderHd);
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	private void confirmSendEmailAfterPaymentForEnrollmentAndRenewal(Long transNo, String emailType, String userId,
			String userName) {

		TransactionEmailDto dto = new TransactionEmailDto();
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_RECEIPT);
		CustomerProfile cp = null;
		if (null != transNo) {
			CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transNo);
			cp = customerProfileDao.get(CustomerProfile.class, customerOrderTrans.getCustomerOrderHd().getCustomerId());
		}

		String contentMT = mt.getContentHtml();

		if (null != cp) {
			dto.setEmailContent(modifyContent(contentMT, cp, userName));
			dto.setSendTo(cp.getContactEmail()); // TODO: SAMHUI advised on
													// 20160323: cp is possible
													// null
													// that will raise exception
													// here. plan to fix it!
		}
		dto.setSubject(mt.getMessageSubject());
		dto.setTransactionNO(transNo);
		dto.setEmailType(emailType);
		LoginUserDto userDto = new LoginUserDto();
		userDto.setUserId(userId);
		userDto.setUserName(userName);
		sentTransactionEmail(dto, userDto);
	}

	@Transactional
	public TransactionEmailDto getEmailDeailReady(TransactionEmailDto transactionEmailDto, String userName) {
		if (Constant.TEMPLATE_ID_DAYPASS_RECEIPT.equalsIgnoreCase(transactionEmailDto.getEmailType())) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			MessageTemplate daypassTemplate = messageTemplateDao
					.getTemplateByFunctionId(Constant.TEMPLATE_ID_DAYPASS_PURCHASE_NOTIFY);
			CustomerOrderHd orderHd = this.customerOrderHdDao.get(CustomerOrderHd.class,
					transactionEmailDto.getOrderNO());
			if (null != orderHd.getCustomerId()) {
				CustomerProfile cp = customerProfileDao.get(CustomerProfile.class, orderHd.getCustomerId());
				List<CustomerOrderPermitCard> list = orderHd.getCustomerOrderPermitCards();
				CustomerOrderPermitCard card = list.get(0);
				StringBuilder sb = new StringBuilder();
				HashSet<String> name = new HashSet<String>();
				for (CustomerOrderPermitCard c : list) {
					ServicePlan d = servicePlanDao.get(ServicePlan.class, c.getServicePlanNo());
					name.add(d.getPlanName());
				}
				for (String str : name) {
					sb.append(str).append("; ");
				}
				String type = "";
				if (sb.length() > 0) {
					type = sb.subSequence(0, sb.length() - 2).toString();
				}
				logger.debug(
						"CustomerOrderTransServiceImpl  getEmailDeailReady for purchase daypass run start ...orderNo:"
								+ transactionEmailDto.getOrderNO());
				ServicePlan daypass = servicePlanDao.get(ServicePlan.class, card.getServicePlanNo());

				if (null != cp) {
					String content = daypassTemplate.getContentHtml()
							.replaceAll(Constant.MessageTemplate.UserName.getName(),
									cp.getSalutation() + " " + cp.getGivenName() + " " + cp.getSurname())
							.replaceAll(Constant.MessageTemplate.Confirmation.getName(),
									transactionEmailDto.getOrderNO() + "")
							.replaceAll(Constant.MessageTemplate.StartTime.getName(),
									sdf.format(card.getEffectiveFrom()))
							.replaceAll(Constant.MessageTemplate.EndTime.getName(), sdf.format(card.getEffectiveTo()))
							.replaceAll(Constant.MessageTemplate.OffPass.getName(), list.size() + "")
							.replaceAll(Constant.MessageTemplate.Type.getName(), type);
					transactionEmailDto.setEmailContent(content);
					transactionEmailDto.setSendTo(cp.getContactEmail());
				}
				transactionEmailDto.setSubject(daypassTemplate.getMessageSubject());
				logger.debug(
						"CustomerOrderTransServiceImpl  getEmailDeailReady for purchase daypass run end ...orderNo:"
								+ transactionEmailDto.getOrderNO());
			}
		} else {
			MessageTemplate receiptTemplate = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_RECEIPT);
			CustomerProfile cp = null;
			if (null != transactionEmailDto.getTransactionNO()) {
				CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class,
						transactionEmailDto.getTransactionNO());
				cp = customerProfileDao.get(CustomerProfile.class,
						customerOrderTrans.getCustomerOrderHd().getCustomerId());
			} else if (null != transactionEmailDto.getOrderNO()) {
				CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class,
						transactionEmailDto.getOrderNO());
				cp = customerProfileDao.get(CustomerProfile.class, customerOrderHd.getCustomerId());
			}

			String contentMT = receiptTemplate.getContentHtml();
			if (StringUtils.isEmpty(transactionEmailDto.getEmailContent())) {
				transactionEmailDto.setEmailContent(modifyContent(contentMT, cp, userName));
			}
			if (StringUtils.isEmpty(transactionEmailDto.getSendTo())) {
				transactionEmailDto.setSendTo(null != cp ? cp.getContactEmail() : "");
			}
			if (StringUtils.isEmpty(transactionEmailDto.getSubject())) {
				transactionEmailDto.setSubject(receiptTemplate.getMessageSubject());
			}
		}

		return transactionEmailDto;
	}

	@Transactional
	@Override
	public ResponseResult handlePosPayPrivateCoach(PosResponse posResponse) {
		try {
			Long transactionNo = updateOrderTransAndOrderHd(posResponse);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommomError.DATA_ISSUE);
			ecrLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
		}
		return responseResult;
	}

	@Transactional
	@Override
	public ResponseResult handlePosPayDaypass(PosResponse posResponse) {
		try {
			if (null!=posResponse&&null!=posResponse.getReferenceNo()) {
				Long transactionNo = updateOrderTransAndOrderHd(posResponse);
				responseResult.initResult(GTAError.Success.SUCCESS);
			}else{
				ecrLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, "ReferenceNo is null "));
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommomError.DATA_ISSUE);
			ecrLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
		}
		return responseResult;
	}

	@Transactional
	public ResponseResult handleCreditCardByPos(PosResponse posResponse) {
		try {
			if (null!=posResponse&&null!=posResponse.getReferenceNo()) {
				Long transactionNo = updateOrderTransAndOrderHd(posResponse);
				confirmSendEmailAfterPaymentForEnrollmentAndRenewal(transactionNo, Constant.TEMPLATE_ID_RECEIPT,
						"SYSTEM", "HKGTA");
				// Used to change enrollment status from Approved to Payment
				// Approved or Payment Approved to Approved
				updateEnrollmentStatusOncePayementChange(transactionNo);

				responseResult.initResult(GTAError.Success.SUCCESS);
			}else{
				ecrLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, "ReferenceNo is null "));
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommomError.DATA_ISSUE);
			ecrLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
		}
		return responseResult;
	}

	private Long updateOrderTransAndOrderHd(PosResponse posResponse) throws Exception {
		Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
		CustomerOrderTrans customerOrderTrans = this.customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
		customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
		customerOrderTrans.setInternalRemark(posResponse.getResponseText());
		customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
		String cardType=posResponse.getCardType().trim();
		if(!StringUtils.isEmpty(cardType)){
			customerOrderTrans.setPaymentMethodCode(CommUtil.getCardType(cardType));// Card payment type
		}
		if (posResponse.getResponseCode().equalsIgnoreCase("00")) {
			customerOrderTrans.setStatus(CustomerTransationStatus.SUC.getDesc());
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			BigDecimal balanceDueAccountant = customerTransactionDao
					.countBalanceDueStatus(customerOrderHd.getOrderNo());
			if (balanceDueAccountant.compareTo(BigDecimal.ZERO) <= 0) {
				customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
				List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
				if (customerOrderDets.size() > 0
						&& customerOrderDets.get(0).getItemNo().startsWith(Constant.RENEW_ITEM_NO_PREFIX)) {
					// if the payment is for renew, need to set effective date
					// and expiry date for customer service account and save
					// limit rule and facility right data.
					handleRenewPayment(customerOrderHd, "System");
				}
			}
		} else {
			customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
		}
		customerOrderTransDao.saveOrUpdate(customerOrderTrans);
		
		ecrLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, "transactionNo:"+customerOrderTrans.getTransactionNo()+",paymentMedia:"+customerOrderTrans.getPaymentMedia()
		+",cardType="+customerOrderTrans.getPaymentMethodCode()+",status:"+customerOrderTrans.getStatus()));
		
		return transactionNo;
	}

	private void handleRenewPayment(CustomerOrderHd customerOrderHd, String createBy) throws Exception {

		CustomerServiceAcc csAcc = customerServiceDao.getLatestCustomerService(customerOrderHd.getCustomerId());
		/*
		 * setEffectiveDate 
		 * 1 if the status is ACT,then set effectiveDate is getExpiryDate+1,set ExpiryDate is effectiveDate + servicePlan's contract_length_month - 1 day
		 * 2 if the status is EXP,then set effectiveDate is the first of next month,set ExpiryDate is effectiveDate + servicePlan's contract_length_month - 1 day 
		 */
		if (null != csAcc) {
			CustomerServiceAcc newCustomerServiceAcc = customerServiceDao.getUniqueByCol(CustomerServiceAcc.class,
					"orderNo", customerOrderHd.getOrderNo());
			CustomerServiceSubscribe customerServiceSubscribe = customerServiceSubscribeDao
					.getUniqueByCol(CustomerServiceSubscribe.class, "id.accNo", newCustomerServiceAcc.getAccNo());
			ServicePlan renewalServicePlan = servicePlanDao.get(ServicePlan.class,
					customerServiceSubscribe.getId().getServicePlanNo());
			Calendar calendar = Calendar.getInstance();
			if (CustomerServiceStatus.ACT.getDesc().equals(csAcc.getStatus())) {
				calendar.setTime(csAcc.getExpiryDate());
				calendar.add(Calendar.DAY_OF_MONTH, 1);
				// setEffectiveDate
				newCustomerServiceAcc.setEffectiveDate(calendar.getTime());
				if (renewalServicePlan.getContractLengthMonth() != null) {
					calendar.add(Calendar.MONTH, renewalServicePlan.getContractLengthMonth().intValue());
					calendar.add(Calendar.DAY_OF_MONTH, -1);
				}
				
			} else if (CustomerServiceStatus.EXP.getDesc().equals(csAcc.getStatus())) {
				calendar.setTime(new Date());
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.add(Calendar.MONTH, 1);
				// setEffectiveDate
				newCustomerServiceAcc.setEffectiveDate(calendar.getTime());
				if (renewalServicePlan.getContractLengthMonth() != null) {
					calendar.add(Calendar.MONTH, renewalServicePlan.getContractLengthMonth().intValue());
					calendar.add(Calendar.DAY_OF_MONTH, -1);
				}

			}
			// setEffectiveDate
			if(this.checkServicePlanMultiG(renewalServicePlan.getPlanNo()))
			{
				newCustomerServiceAcc.setExpiryDate(DateConvertUtil.parseString2Date(appProps.getProperty("system.expiryDate"),"yyyy-MM-dd"));
			}else{
				newCustomerServiceAcc.setExpiryDate(calendar.getTime());	
			}
			
		}
	}
	/***
	 * check service plan MultiG is true or false
	 * @param servicePlanNo
	 * @return
	 */
	private boolean checkServicePlanMultiG(Long servicePlanNo){
		ServicePlanAdditionRule rule = servicePlanAdditionRuleDao.getByPlanNoAndRightCode(servicePlanNo, "MGD");
		if(null!=rule&&rule.getInputValue().equalsIgnoreCase("true"))
		{
			return true;
		}
		return false;
	}

	@Transactional(rollbackFor = Exception.class)
	public void moveTranscriptFile(Long transactionNo, Long customerId) {
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		String transcriptFileName = customerOrderTrans.getTranscriptFileName();
		String basePath = "";
		try {
			basePath = FileUpload.getBasePath(FileUpload.FileCategory.USER);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File transFile = new File(basePath + File.separator + customerId.longValue());
		if (!transFile.exists()) {
			boolean isCreated = transFile.mkdirs();
			if (!isCreated) {
				logger.error("Can't create folder!");
			}
		}
		String fileName = "";
		int index = transcriptFileName.lastIndexOf("/");
		if (transcriptFileName != null && index >= 0) {
			fileName = transcriptFileName.substring(index);
		}
		boolean isSuccess = FileUtil.moveFile(basePath + transcriptFileName,
				basePath + File.separator + customerId.longValue() + fileName);
		if (isSuccess) {
			customerOrderTrans.setTranscriptFileName("/" + customerId.longValue() + fileName);
			customerOrderTransDao.update(customerOrderTrans);
		} else {
			logger.error(transcriptFileName + "move failed!");
		}

	}

	@Override
	@Transactional
	public ResponseResult paymentByCreditCard(CustomerOrderTransDto customerOrderTransDto) {
		ResponseResult responseResult = new ResponseResult();
		try {

			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			initCustomerOrderTrans(customerOrderTrans);
			if (customerOrderTransDto.getTransactionNo() != null) {// allow
																	// update
																	// the
																	// existing
				customerOrderTrans.setTransactionNo(customerOrderTransDto.getTransactionNo());
			}
			CustomerOrderHd customerOrderHd = customerTransactionDao.get(CustomerOrderHd.class,
					customerOrderTransDto.getOrderNo());
			if ("CMP".equals(customerOrderHd.getOrderStatus())) {
				responseResult.setReturnCode("1");
				responseResult.setErrorMessageEN("The transaction has been completed!");
				return responseResult;
			}

			customerOrderTrans.setCustomerOrderHd(customerOrderHd);
			customerOrderTrans.setPaidAmount(customerOrderTransDto.getPaidAmount());
			String paymentMethodCode = uc.ifempty(customerOrderTransDto.getPaymentMethodCode(),
					PaymentMethod.VISA.name());
			customerOrderTrans.setPaymentMethodCode(paymentMethodCode);
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			if(!StringUtils.isEmpty(customerOrderTransDto.getTerminalId())){
				customerOrderTrans.setTerminalId(customerOrderTransDto.getTerminalId());
			}
			customerOrderTrans.setStatus("PND");
			customerOrderTrans.setTranscriptFileName(customerOrderTransDto.getTranscriptFileName());
			customerOrderTrans.setTransactionTimestamp(new Date());
			customerOrderTrans.setPaymentRecvBy(customerOrderTransDto.getPaymentReceivedBy());
			customerOrderTrans.setCreateBy(customerOrderTransDto.getCreateByUserId());
			// start of validation
			if (customerOrderTrans.getPaidAmount().compareTo(BigDecimal.ZERO) <= 0) {
				return new ResponseResult("1", "The paid amount can not be less than or equal to 0.");
			}

			// start of payment total checking

			BigDecimal balanceDueSalesMan = homePageSummaryDao.countBalanceDue(customerOrderTransDto.getOrderNo());
			BigDecimal balanceDueAccountant = customerTransactionDao
					.countBalanceDueStatus(customerOrderTransDto.getOrderNo());
			if (customerOrderTransDto.getTransactionNo() != null && customerOrderTransDto.getTransactionNo() > 0) {// Change
																													// payment
																													// to
																													// success
																													// status
				CustomerOrderTrans customerOrderTransOld = customerOrderTransDao.get(CustomerOrderTrans.class,
						customerOrderTransDto.getTransactionNo());
				if (customerOrderTransDto.getStatus().equals("SUC")) {
					if (balanceDueAccountant.subtract(customerOrderTrans.getPaidAmount())
							.compareTo(BigDecimal.ZERO) < 0) {
						return new ResponseResult("1",
								"The sum of successful paid is over the amount due. Current due:$"
										+ balanceDueSalesMan);
					}
				} else if (customerOrderTransDto.getStatus() == null || customerOrderTransDto.getStatus().equals("")) {
					if (balanceDueSalesMan.subtract(customerOrderTrans.getPaidAmount())
							.add(customerOrderTransOld.getPaidAmount()).compareTo(BigDecimal.ZERO) < 0) {
						return new ResponseResult("1",
								"The sum of payment is over the amount due. Current due:$" + balanceDueSalesMan);
					}
				}
				customerOrderTransDao.evict(customerOrderTransOld);
			}
			if (customerOrderTransDto.getTransactionNo() == null) {// Add new
																	// payment
																	// case
				if (balanceDueSalesMan.compareTo(BigDecimal.ZERO) <= 0) {
					return new ResponseResult("1", "No more payment is required.");
				} else if (balanceDueSalesMan.subtract(customerOrderTrans.getPaidAmount())
						.compareTo(BigDecimal.ZERO) < 0) {
					return new ResponseResult("1",
							"The sum of successful paid is over the amount due. Current due:$" + balanceDueSalesMan);
				}
			}
			// end of payment total checking
			if (null == customerOrderTrans.getTransactionNo() || 0 == customerOrderTrans.getTransactionNo()) {
				customerOrderTransDao.save(customerOrderTrans);
			} else {
				customerOrderTransDao.saveOrUpdate(customerOrderTrans);
			}
			responseResult.setReturnCode("0");
			responseResult.setErrorMessageEN("success");
			responseResult.setData(customerOrderTrans);
			return responseResult;
		} catch (Exception e) {
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("save failed!");
			return responseResult;
		}
	}

	@Override
	@Transactional
	public ResponseResult getPaymentDetailsByOrderNo(Long orderNo) {
		try {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(orderNo);
			List<CustomerOrderTransDto> customerOrderTransDtos = new ArrayList<CustomerOrderTransDto>();
			for (CustomerOrderTrans customerOrderTrans : customerOrderTranses) {
				CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
				customerOrderTransDto.setAgentTransactionNo(customerOrderTrans.getAgentTransactionNo());
				customerOrderTransDto.setPaidAmount(customerOrderTrans.getPaidAmount());
				customerOrderTransDto.setPaymentMethodCode(customerOrderTrans.getPaymentMethodCode());
				customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
				customerOrderTransDto.setTerminalId(customerOrderTrans.getTerminalId());
				customerOrderTransDto.setTransactionNo(customerOrderTrans.getTransactionNo());
				customerOrderTransDto.setTransactionTimestamp(
						DateConvertUtil.getYMDDateAndDateDiff(customerOrderTrans.getTransactionTimestamp()));
				customerOrderTransDto.setTranscriptFileName(customerOrderTrans.getTranscriptFileName());
				customerOrderTransDto.setOrderNo(orderNo);
				if (CommUtil.notEmpty(customerOrderTrans.getReadBy())) {
					customerOrderTransDto.setReadBy("");
				} else {
					customerOrderTransDto.setReadBy(customerOrderTrans.getReadBy());
				}
				customerOrderTransDto.setUnReadCount(remarkDao.countUnreadPayment(orderNo));
				customerOrderTransDtos.add(customerOrderTransDto);
			}
			CustomerOrderHd customerOrderHd = customerTransactionDao.get(CustomerOrderHd.class, orderNo);

			Long customerId = customerOrderHd.getCustomerId().longValue();
			CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, customerId);

			BigDecimal orderTotalAmount = customerOrderHd.getOrderTotalAmount();
			String customerSurname = customerProfile.getSurname();
			String customerGivenname = customerProfile.getGivenName();
			String customerSalutation = customerProfile.getSalutation();
			BigDecimal balanceDue = homePageSummaryDao.countBalanceDue(orderNo);

			List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
			CustomerOrderDet customerOrderDet = customerOrderDets.iterator().next();
			String itemNo = customerOrderDet.getItemNo();
			String planName = "";
			if (itemNo.startsWith("RENEW")) {
				ServicePlanOfferPos servicePlanOfferPos = servicePlanOfferPosDao
						.getUniqueByCol(ServicePlanOfferPos.class, "posItemNo", itemNo);
				ServicePlanPos servicePlanPos = servicePlanPosDao.getUniqueByCol(ServicePlanPos.class, "servPosId",
						servicePlanOfferPos.getServPosId());
				ServicePlan servicePlan = servicePlanPos.getServicePlan();
				planName = servicePlan.getPlanName();
			} else {
				ServicePlanPos servicePlanPos = servicePlanPosDao.getUniqueByCol(ServicePlanPos.class, "posItemNo",
						itemNo);
				ServicePlan servicePlan = servicePlanPos.getServicePlan();
				planName = servicePlan.getPlanName();
			}
			TempDto dto = new TempDto();
			dto.setCustomerId(customerId);
			dto.setOrderNo(orderNo);
			dto.setCustomerSurname(customerSurname);
			dto.setCustomerGivenname(customerGivenname);
			dto.setCustomerSalutation(customerSalutation);
			dto.setOrderTotalAmount(orderTotalAmount);
			;
			dto.setBalanceDue(balanceDue);
			dto.setPlanName(planName);
			/*
			 * Map<String, Object> responseMap = new HashMap<String, Object>();
			 * responseMap.put("customerId", customerId);
			 * responseMap.put("orderNo", orderNo);
			 * responseMap.put("customerSurname", customerSurname);
			 * responseMap.put("customerGivenname", customerGivenname);
			 * responseMap.put("customerSalutation", customerSalutation);
			 * responseMap.put("orderTotalAmount", orderTotalAmount.setScale(2,
			 * BigDecimal.ROUND_HALF_UP)); responseMap.put("balanceDue",
			 * balanceDue.setScale(2, BigDecimal.ROUND_HALF_UP));
			 * responseMap.put("planName", planName);
			 */
			Data data = new Data();
			data.setLastPage(true);
			data.setRecordCount(customerOrderTransDtos.size());
			data.setList(customerOrderTransDtos);
			data.setTotalPage(1);
			data.setCurrentPage(1);
			data.setPageSize(customerOrderTransDtos.size());
			// responseMap.put("list", data);
			dto.setList(data);
			ResponseResult responseResult = new ResponseResult();
			responseResult.setReturnCode("0");
			responseResult.setErrorMessageEN("success");
			responseResult.setData(dto);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			ResponseResult responseResult = new ResponseResult();
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("no data exists!");
			return responseResult;
		}
	}

	@Override
	@Transactional
	public ResponseResult getPaymentDetailsAccountantByOrderNo(Long orderNo) {
		try {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(orderNo);
			List<CustomerOrderTransDto> customerOrderTransDtos = new ArrayList<CustomerOrderTransDto>();
			for (CustomerOrderTrans customerOrderTrans : customerOrderTranses) {
				CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
				customerOrderTransDto.setAgentTransactionNo(customerOrderTrans.getAgentTransactionNo());
				customerOrderTransDto.setPaidAmount(customerOrderTrans.getPaidAmount());
				customerOrderTransDto.setPaymentMethodCode(customerOrderTrans.getPaymentMethodCode());
				customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
				customerOrderTransDto.setTerminalId(customerOrderTrans.getTerminalId());
				customerOrderTransDto.setTransactionNo(customerOrderTrans.getTransactionNo());
				customerOrderTransDto.setTransactionTimestamp(
						DateConvertUtil.getYMDDateAndDateDiff(customerOrderTrans.getTransactionTimestamp()));
				customerOrderTransDto.setTranscriptFileName(customerOrderTrans.getTranscriptFileName());
				customerOrderTransDto.setOrderNo(orderNo);
				if (CommUtil.notEmpty(customerOrderTrans.getReadBy())) {
					customerOrderTransDto.setRead(true);
				} else {
					customerOrderTransDto.setRead(false);
				}
				customerOrderTransDtos.add(customerOrderTransDto);
			}
			CustomerOrderHd customerOrderHd = customerTransactionDao.get(CustomerOrderHd.class, orderNo);

			Long customerId = customerOrderHd.getCustomerId().longValue();
			CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, customerId);

			BigDecimal orderTotalAmount = customerOrderHd.getOrderTotalAmount();
			String customerSurname = customerProfile.getSurname();
			String customerGivenname = customerProfile.getGivenName();
			String customerSalutation = customerProfile.getSalutation();
			BigDecimal balanceDue = customerTransactionDao.countBalanceDueStatus(orderNo);

			List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
			CustomerOrderDet customerOrderDet = customerOrderDets.iterator().next();
			String itemNo = customerOrderDet.getItemNo();

			String planName = "";
			if (itemNo.startsWith("RENEW")) {
				ServicePlanOfferPos servicePlanOfferPos = servicePlanOfferPosDao
						.getUniqueByCol(ServicePlanOfferPos.class, "posItemNo", itemNo);
				// ServicePlanPos servicePlanPos =
				// servicePlanPosDao.getUniqueByCol(ServicePlanPos.class,
				// "servPosId", servicePlanOfferPos.getServPosId());
				// ServicePlan servicePlan = servicePlanPos.getServicePlan();
				planName = servicePlanOfferPos.getDescription();
			} else {
				ServicePlanPos servicePlanPos = servicePlanPosDao.getUniqueByCol(ServicePlanPos.class, "posItemNo",
						itemNo);
				ServicePlan servicePlan = servicePlanPos.getServicePlan();
				planName = servicePlan.getPlanName();
			}

			Map<String, Object> responseMap = new HashMap<String, Object>();
			responseMap.put("orderNo", orderNo);
			responseMap.put("customerSurname", customerSurname);
			responseMap.put("customerGivenname", customerGivenname);
			responseMap.put("customerSalutation", customerSalutation);
			responseMap.put("orderTotalAmount", orderTotalAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
			responseMap.put("balanceDue", balanceDue.setScale(2, BigDecimal.ROUND_HALF_UP));
			responseMap.put("planName", planName);
			Data data = new Data();
			data.setLastPage(true);
			data.setRecordCount(customerOrderTransDtos.size());
			data.setList(customerOrderTransDtos);
			data.setTotalPage(1);
			data.setCurrentPage(1);
			data.setPageSize(customerOrderTransDtos.size());
			responseMap.put("list", data);
			ResponseResult responseResult = new ResponseResult();
			responseResult.setReturnCode("0");
			responseResult.setErrorMessageEN("success");
			responseResult.setData(responseMap);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			ResponseResult responseResult = new ResponseResult();
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("no data exists!");
			return responseResult;
		}
	}

	@Override
	@Transactional
	public BigDecimal getFalueUnreadStatus(String type) {
		BigDecimal list = customerOrderTransDao.getFalueUnreadStatus(type);
		return list;
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String serviceType) {

		if (serviceType.equals(Constant.SERVICE_PLAN_TYPE_ENROLLMENT)) {
			List<SysCode> enrollStatusCodes = sysCodeDao.selectSysCodeByCategory("enrollStatusDue");
			return customerTransactionDao.assembleQueryConditions(enrollStatusCodes, serviceType);
		} else if (serviceType.equals(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)) {
			List<SysCode> orderStatusCodes = sysCodeDao.selectSysCodeByCategory("orderStatusRenewal");
			return customerTransactionDao.assembleQueryConditions(orderStatusCodes, serviceType);
		} else {
			List<SysCode> orderStatusCodes = sysCodeDao.selectSysCodeByCategory("orderStatusRejected");
			return customerTransactionDao.assembleQueryConditions(orderStatusCodes, serviceType);
		}

	}

	@Override
	@Transactional
	public ListPage<CustomerOrderHd> getCustomerTransactionList(ListPage<CustomerOrderHd> page, String balanceDue,
			String serviceType, String orderBy, boolean isAscending, String searchText, String filterBySalesMan,
			String mode, String userId, String enrollStatusFilter, String memberType, String device) {
		// String[] validValues = new String[] {"effectiveDate", "customerName",
		// "description" ,"salesFollowBy", "orderTotalAmount", "enrollStatus",
		// "balanceDue" , "enrollCreateDate", "orderNo", "newTransNumber",
		// "academyNo"};
		// if (Arrays.asList(validValues).contains(orderBy) == false) {
		// responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION,
		// "sortBy is not valid value. Allowed values are : effectiveDate,
		// customerName, description, salesFollowBy, orderTotalAmount,
		// balanceDue, enrollStatus, orderNo, academyNo, newTransNumber");
		// return new ListPage<CustomerOrderHd>();
		// }
		ListPage<CustomerOrderHd> customerTransactionListDtoList = customerTransactionDao.getCustomerTransactionList(
				page, balanceDue, serviceType, orderBy, isAscending, searchText, filterBySalesMan, mode,
				enrollStatusFilter, memberType);
		List<CustomerTransactionListDto> customerTransactionListDtoListWithRemark = new ArrayList<CustomerTransactionListDto>();
		for (Object item : customerTransactionListDtoList.getDtoList()) {
			CustomerTransactionListDto customerTransactionListDto = ((CustomerTransactionListDto) item);
			if (remarkDao.countUnreadRemarkByDevice(((CustomerTransactionListDto) item).getCustomerId().longValue(),
					userId, device) > 0) {
				customerTransactionListDto.setHasRemark(true);
			} else {
				customerTransactionListDto.setHasRemark(false);
			}
			if (remarkDao.countUnreadPayment(customerTransactionListDto.getOrderNo().longValue()) > 0) {
				customerTransactionListDto.setHasRead(true);
			} else {
				customerTransactionListDto.setHasRead(false);
			}
			customerTransactionListDtoListWithRemark.add(customerTransactionListDto);
		}
		customerTransactionListDtoList.getDtoList().clear();
		customerTransactionListDtoList.getDtoList().addAll(customerTransactionListDtoListWithRemark);

		return customerTransactionListDtoList;
	}

	@Override
	@Transactional
	public CustomerOrderTrans getCustomerOrderTrans(Long transactionNo) {		
		//SAM 20161006 fix: when use lazy mode to retrieve many detail records by ORM, need call Hibernate.initialize to populate the detail records.
		//return customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		CustomerOrderTrans customerOrderTrans=customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		Hibernate.initialize(customerOrderTrans.getCustomerOrderHd().getCustomerOrderDets());
		return customerOrderTrans;		
	}

	@Override
	@Transactional
	public Long getOrderTimeoutMin() {
		Long gatewayId = 1l;
		PaymentGateway paymentGateway = paymentGatewayDao.get(PaymentGateway.class, gatewayId);
		Long timeoutMin = paymentGateway.getTimeoutMin();
		return timeoutMin;
	}

	@Override
	@Transactional
	public List<CustomerOrderTrans> getTimeoutPayments() {
		Long ercTimeoutMint=Long.valueOf(appProps.getProperty("ECR.timeoutMin"));
		List<CustomerOrderTrans> timeOutTrans=new ArrayList<>();
		
		List<CustomerOrderTrans> opTranses = customerOrderTransDao.getTimeoutPayments(getOrderTimeoutMin(),PaymentMediaType.OP.name());
		for (CustomerOrderTrans optrans : opTranses) {
			timeOutTrans.add(optrans);
		}
		List<CustomerOrderTrans> ecrTranses = customerOrderTransDao.getTimeoutPayments(ercTimeoutMint,PaymentMediaType.ECR.name());
		logger.info("add onlinetPayment set ECR.timeoutMin  ECR.timeoutMin+#############:"+ecrTranses.size());
		for (CustomerOrderTrans ecrtrans : ecrTranses) {
			timeOutTrans.add(ecrtrans);
		}
		return timeOutTrans;
	}

	@Override
	@Transactional
	public void updateCustomerOrderTrans(CustomerOrderTrans customerOrderTrans) {
		// TODO Auto-generated method stub
		customerOrderTransDao.update(customerOrderTrans);
	}

	@Override
	@Transactional
	public CustomerOrderHd getCustomerOrderHdByOrderNo(Long orderNo) {
		return customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
	}

	@Override
	@Transactional
	public void addNewTransactionRecord(CustomerOrderTrans customerOrderTrans) {
		initCustomerOrderTrans(customerOrderTrans);
		customerOrderTransDao.save(customerOrderTrans);
	}

	@Override
	@Transactional
	public List<CustomerOrderTrans> getCustomerOrderTransListByOrderNo(Long orderNo) {
		List<Object> param = new ArrayList<Object>();
		param.add(orderNo);
		List<CustomerOrderTrans> orderTrans = customerOrderTransDao.getByCol(CustomerOrderTrans.class,
				"customerOrderHd.orderNo", orderNo, null);
		return orderTrans;
	}

	@Override
	@Transactional
	public CustomerOrderTransDto payment(MemberCashValuePaymentDto memberCashValuePaymentDto) {
		// added by Kaster 20160329
		String createBy = memberCashValuePaymentDto.getCreatedBy();
		if (createBy != null && !createBy.equals("")) {
			UserMaster um = userMasterDao.get(UserMaster.class, createBy);
			if (um != null && um.getUserType().equalsIgnoreCase("STAFF")) {
				createBy = memberCashValuePaymentDto.getCreatedBy();
			} else {
				createBy = memberCashValuePaymentDto.getUserId();
			}
		} else {
			createBy = memberCashValuePaymentDto.getUserId();
		}
		if (memberCashValuePaymentDto.getCustomerId() == null || memberCashValuePaymentDto.getItemNoMap() == null
				|| memberCashValuePaymentDto.getTotalAmount() == null)
			throw new GTACommonException(GTAError.MemberError.MEMBER_NOT_FOUND);

		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(new Date());
		if (StringUtils.isEmpty(memberCashValuePaymentDto.getPaymentMethod())
				|| memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH))
			customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());
		else if (memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CREDIT_CARD)||
				memberCashValuePaymentDto.getPaymentMethod().equals(PaymentMethod.AMEX.getCardCode()))
			customerOrderHd.setOrderStatus(Constant.Status.OPN.toString());
		//  UNIONPAY
		else if(memberCashValuePaymentDto.getPaymentMethod().equals(Constant.UNION_PAY))
			customerOrderHd.setOrderStatus(Constant.Status.OPN.toString());
		
		customerOrderHd.setStaffUserId(memberCashValuePaymentDto.getUserId());
		customerOrderHd.setCustomerId(memberCashValuePaymentDto.getCustomerId());
		customerOrderHd.setOrderTotalAmount(memberCashValuePaymentDto.getTotalAmount());
		// modified by Kaster 20160329
		customerOrderHd.setCreateBy(createBy);
		customerOrderHd.setCreateDate(currentTime);
		customerOrderHdDao.save(customerOrderHd);

		for (Map.Entry<String, Integer> entry : memberCashValuePaymentDto.getItemNoMap().entrySet()) {
			PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.get(PosServiceItemPrice.class,
					entry.getKey());
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(customerOrderHd);
			customerOrderDet.setItemNo(entry.getKey());
			customerOrderDet.setOrderQty(new Long(entry.getValue()));
			customerOrderDet.setItemTotalAmout(
					posServiceItemPrice.getItemPrice().multiply(new BigDecimal(customerOrderDet.getOrderQty())));
			// modified by Kaster 20160329
			customerOrderDet.setCreateBy(createBy);
			customerOrderDet.setCreateDate(currentTime);
			customerOrderDetDao.save(customerOrderDet);
		}
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		initCustomerOrderTrans(customerOrderTrans);
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setPaymentMethodCode(memberCashValuePaymentDto.getPaymentMethod());
		customerOrderTrans.setTransactionTimestamp(currentTime);
		customerOrderTrans.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTrans.setPaymentLocationCode(memberCashValuePaymentDto.getLocation());
		if (StringUtils.isEmpty(memberCashValuePaymentDto.getPaymentMethod())
				|| memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH)) {
			customerOrderTrans.setPaymentMedia(PaymentMediaType.NA.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
		} else if (memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CASH_Value)) {
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
		} else if (memberCashValuePaymentDto.getPaymentMethod().equals(Constant.CREDIT_CARD)||
				PaymentMethod.AMEX.getCardCode().equals(memberCashValuePaymentDto.getPaymentMethod())) 
		{
			if (null != memberCashValuePaymentDto.getIsOnlinePayment()
					&& memberCashValuePaymentDto.getIsOnlinePayment()){
				if(!StringUtils.isEmpty(memberCashValuePaymentDto.getTerminalType())&&
						PaymentMediaType.ECR.name().equalsIgnoreCase(memberCashValuePaymentDto.getTerminalType())){
					customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
				}else{
					customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
				}
			}
			else{
				if(!StringUtils.isEmpty(memberCashValuePaymentDto.getTerminalType())&&
						PaymentMediaType.ECR.name().equals(memberCashValuePaymentDto.getTerminalType())){
					customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
				}else{
					customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
				}
			}
			customerOrderTrans.setStatus(Constant.Status.PND.toString());
		}
		//ADD Union payment
		else if (memberCashValuePaymentDto.getPaymentMethod().equals(Constant.UNION_PAY)) {
			if (null != memberCashValuePaymentDto.getIsOnlinePayment()
					&& memberCashValuePaymentDto.getIsOnlinePayment())
				customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			else
				customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());

			customerOrderTrans.setStatus(Constant.Status.PND.toString());
		}
//		else if (PaymentMethod.VISA.name().equals(memberCashValuePaymentDto.getPaymentMethod())) {
//			customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
//
//			customerOrderTrans.setStatus(Constant.Status.PND.toString());
//		}
		customerOrderTrans.setPaymentRecvBy(memberCashValuePaymentDto.getUserId());
		customerOrderTransDao.save(customerOrderTrans);

		CustomerOrderTransDto customerOrderTransDto = new CustomerOrderTransDto();
		customerOrderTransDto.setOrderNo(customerOrderHd.getOrderNo());
		customerOrderTransDto.setTransactionNo(customerOrderTrans.getTransactionNo());
		customerOrderTransDto.setTransactionTimestamp(DateConvertUtil.getYMDDateAndDateDiff(currentTime));
		customerOrderTransDto.setPaidAmount(memberCashValuePaymentDto.getTotalAmount());
		customerOrderTransDto.setPaymentMethodCode(memberCashValuePaymentDto.getPaymentMethod());
		customerOrderTransDto.setStatus(customerOrderTrans.getStatus());
		return customerOrderTransDto;
	}

	@Override
	@Transactional
	public ResponseResult getAllCreatedStaff() {
		String sql = "SELECT DISTINCT sp.user_id as userId, CONCAT(sp.given_name,' ',sp.surname) as staffName from  staff_profile sp, customer_order_trans cot where cot.create_by = sp.user_id"
				+ " UNION SELECT DISTINCT cot.create_by as userId,'System' as staffName from  customer_order_trans cot where cot.create_by = 'System'";
		List<StaffDto> sales = customerOrderTransDao.getDtoBySql(sql, null, StaffDto.class);
		responseResult.initResult(GTAError.Success.SUCCESS, sales);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getAllAuditedStaff() {
		String sql = "SELECT DISTINCT sp.user_id as userId, CONCAT(sp.given_name,' ',sp.surname) as staffName from  staff_profile sp, customer_order_trans cot where cot.audit_by = sp.user_id"
				+ " UNION SELECT DISTINCT cot.audit_by as userId,'System' as staffName from  customer_order_trans cot where cot.audit_by = 'System'";
		List<StaffDto> sales = customerOrderTransDao.getDtoBySql(sql, null, StaffDto.class);
		responseResult.initResult(GTAError.Success.SUCCESS, sales);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getSattlementReport(ListPage<CustomerOrderTrans> page, SearchSettlementReportDto dto) {
		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao.getSattlementReport(page, dto);
		List<Object> spendingSummaryDtos = listPage.getDtoList();
		Data data = new Data();
		for (Object object : spendingSummaryDtos) {
			SettlementReportDto temp = (SettlementReportDto) object;

			replaceValue(temp);
		}
		data.setList(spendingSummaryDtos);
		data.setTotalPage(page.getAllPage());
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Transactional
	public void replaceValue(SettlementReportDto temp) {
		if ("ENROLL".equals(temp.getEnrollType())) {
			temp.setEnrollType("Enroll");
		} else if ("RENEW".equals(temp.getEnrollType())) {
			temp.setEnrollType("Renew");
		}

		if (PaymentMethod.BT.name().equals(temp.getPaymentMethod())) {
			temp.setPaymentMethod(PaymentMethod.BT.getDesc());
		} else if (PaymentMethod.CASH.name().equals(temp.getPaymentMethod())) {
			temp.setPaymentMethod(PaymentMethod.CASH.getDesc());
		} else if (PaymentMethod.VISA.name().equals(temp.getPaymentMethod())) {
			temp.setPaymentMethod(PaymentMethod.VISA.getDesc());
		} else if (PaymentMethod.CHEQUE.name().equalsIgnoreCase(temp.getPaymentMethod())) {
			temp.setPaymentMethod(PaymentMethod.CHEQUE.getDesc());
		} else if (PaymentMethod.OTH.name().equals(temp.getPaymentMethod())) {
			temp.setPaymentMethod(PaymentMethod.OTH.getDesc());
		} else if (PaymentMethod.MASTER.name().equals(temp.getPaymentMethod())) {
			temp.setPaymentMethod(PaymentMethod.MASTER.getDesc());
		} 
		 else if (PaymentMethod.CUP.name().equals(temp.getPaymentMethod())) {
				temp.setPaymentMethod(PaymentMethod.CUP.getDesc());
		}
		 else if (PaymentMethod.AMEX.name().equals(temp.getPaymentMethod())) {
				temp.setPaymentMethod(PaymentMethod.AMEX.getDesc());
				
		}else if ("CC".equals(temp.getPaymentMethod())) {
			temp.setPaymentMethod("Credit Card");
		}

		if (null == temp.getPaymentMedia()) {
			temp.setPaymentMedia("N/A");
		} else if (PaymentMediaType.OP.name().equals(temp.getPaymentMedia())) {
			temp.setPaymentMedia(PaymentMediaType.OP.getDesc());
		} else if (PaymentMediaType.OTH.name().equals(temp.getPaymentMedia())) {
			temp.setPaymentMedia(PaymentMediaType.OTH.getDesc());
		} else if (PaymentMediaType.ECR.name().equals(temp.getPaymentMedia())) {
			temp.setPaymentMedia(PaymentMediaType.ECR.getDesc());
		}

		if ("SUC".equals(temp.getTransStatus())) {
			temp.setTransStatus("Success");
		} else if ("FAIL".equals(temp.getTransStatus())) {
			temp.setTransStatus("Fail");
		} else if ("VOID".equals(temp.getTransStatus())) {
			temp.setTransStatus("Void");
		} else if ("FAIL".equals(temp.getTransStatus())) {
			temp.setTransStatus("Fail");
		} else if ("PND".equals(temp.getTransStatus())) {
			temp.setTransStatus("Pending");
		} else if ("RFU".equals(temp.getTransStatus())) {
			temp.setTransStatus("Refund");
		} else if ("PR".equals(temp.getTransStatus())) {
			temp.setTransStatus("Payment Refund");
		}

		if ("OPN".equalsIgnoreCase(temp.getOrderStatus())) {
			temp.setOrderStatus("Open");
		} else if ("CMP".equalsIgnoreCase(temp.getOrderStatus())) {
			temp.setOrderStatus("Complete");
		} else if ("CAN".equalsIgnoreCase(temp.getOrderStatus())) {
			temp.setOrderStatus("Cancelled");
		} else if ("REJ".equalsIgnoreCase(temp.getOrderStatus())) {
			temp.setOrderStatus("Rejected");
		}

		String createBy = temp.getCreateBy();
		String createName = null;
		if ("System".equalsIgnoreCase(createBy)) {
			createName = "System";
		} else {
			createName = customerProfileDao.getNameByUserId(createBy);
		}
		temp.setCreateBy(createName);
		String updateBy = temp.getUpdateBy();
		String updateName = null;
		if ("System".equalsIgnoreCase(updateBy)) {
			updateName = "System";
		} else {
			updateName = customerProfileDao.getNameByUserId(updateBy);
		}
		temp.setUpdateBy(updateName);
		String auditBy = temp.getAuditBy();
		String auditName = null;
		if ("System".equalsIgnoreCase(auditBy)) {
			auditName = "System";
		} else {
			auditName = customerProfileDao.getNameByUserId(auditBy);
		}
		temp.setAuditBy(auditName);

		String locationCode = temp.getLocation();
		String hqlstr = "SELECT s.codeDisplay FROM SysCode s where s.codeValue = ? and s.category = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(locationCode);
		param.add("location");
		String locationDesc = (String) customerProfileDao.getUniqueByHql(hqlstr, param);
		temp.setLocation(locationDesc);
	}

	@Override
	@Transactional
	public ResponseResult getSattlementReportExport(ListPage<CustomerOrderTrans> page, SearchSettlementReportDto dto,
			HttpServletResponse response) throws Exception {
		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao.getSattlementReport(page, dto);
		List<Object> spendingSummaryDtos = listPage.getDtoList();
		for (Object object : spendingSummaryDtos) {
			SettlementReportDto temp = (SettlementReportDto) object;
			replaceValue(temp);
		}

		String timeCondition = null;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if ("0d".equalsIgnoreCase(dto.getTransactionDate())) {
			timeCondition = DateConvertUtil.getYMDFormatDate(date);
		} else if ("7d".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.add(Calendar.DATE, -7);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime()) + " to "
					+ DateConvertUtil.getYMDFormatDate(date);
		} else if ("14d".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.DATE, -14);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime()) + " to "
					+ DateConvertUtil.getYMDFormatDate(date);
		} else if ("1m".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -1);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime()) + " to "
					+ DateConvertUtil.getYMDFormatDate(date);
		} else if ("2m".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -2);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime()) + " to "
					+ DateConvertUtil.getYMDFormatDate(date);
		} else if ("3m".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -3);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime()) + " to "
					+ DateConvertUtil.getYMDFormatDate(date);
		} else if ("6m".equalsIgnoreCase(dto.getTransactionDate())) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -6);
			timeCondition = DateConvertUtil.getYMDFormatDate(calendar.getTime()) + " to "
					+ DateConvertUtil.getYMDFormatDate(date);
		} else if ("customize".equalsIgnoreCase(dto.getTransactionDate())) {
			timeCondition = dto.getStartDate() + " to " + dto.getEndDate();
		}

		String orderStatus = null;
		if ("OPN".equalsIgnoreCase(dto.getOrderStatus())) {
			orderStatus = "Open";
		} else if ("CMP".equalsIgnoreCase(dto.getOrderStatus())) {
			orderStatus = "Complete";
		} else if ("CAN".equalsIgnoreCase(dto.getOrderStatus())) {
			orderStatus = "Cancelled";
		} else if ("REJ".equalsIgnoreCase(dto.getOrderStatus())) {
			orderStatus = "Rejected";
		}

		String transStatus = null;
		if ("SUC".equals(dto.getTransStatus())) {
			transStatus = "Success";
		} else if ("FAIL".equals(dto.getTransStatus())) {
			transStatus = "Fail";
		} else if ("VOID".equals(dto.getTransStatus())) {
			transStatus = "Void";
		} else if ("FAIL".equals(dto.getTransStatus())) {
			transStatus = "Fail";
		} else if ("PND".equals(dto.getTransStatus())) {
			transStatus = "Pending";
		} else if ("RFU".equals(dto.getTransStatus())) {
			transStatus = "Refund";
		} else if ("PR".equals(dto.getTransStatus())) {
			transStatus = "Payment Refund";
		}

		String enrollType = null;
		if ("ENROLL".equals(dto.getEnrollType())) {
			enrollType = "Enroll";
		} else if ("RENEW".equals(dto.getEnrollType())) {
			enrollType = "Renew";
		}

		String location = null;
		String locationCode = dto.getLocation();
		String hqlstr = "SELECT s.codeDisplay FROM SysCode s where s.codeValue = ? and s.category = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(locationCode);
		param.add("location");
		location = (String) customerProfileDao.getUniqueByHql(hqlstr, param);

		String salesMan = customerProfileDao.getNameByUserId(dto.getSalesman());

		String accountant = customerProfileDao.getNameByUserId(dto.getAuditBy());

		String createBy = customerProfileDao.getNameByUserId(dto.getCreateBy());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<String[]> outputList = new ArrayList<String[]>();
		if (dto.getFileType().equalsIgnoreCase("CSV")) {
			outputList.add(new String[] { DateConvertUtil.getStrFromDate(new Date()), null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[21]);
			outputList.add(new String[] { "", "", "", "", "", "", "", "Transaction Settlement(" + timeCondition + ")",
					null, null, null, null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[21]);
			outputList.add(new String[] { "Filter criteria:", null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[] { "Time:", timeCondition, null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[] { "Order Status:", orderStatus, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[] { "Trans Status:", transStatus, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[] { "Enroll Type:", enrollType, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[] { "Location:", location, null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[] { "Salesman:", salesMan, null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[] { "Accountant:", accountant, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null });
			outputList.add(new String[] { "Create By:", createBy, null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null });
			// outputList.add(new String[]{"Filter criteria:","Time","Order
			// Status","Trans Status","Enroll Type","Location","Salesman","Audit
			// By", "Create By", null, null, null, null, null, null,
			// null, null,null,null,null,null});
			outputList.add(new String[21]);
		}
		outputList.add(new String[] { "transaction Date", "Transaction ID", "Order No.", "Order Date", "Academy ID",
				"Patron Name", "Service Plan", "Salesman", "Enroll Type", "Payment Method", "Payment Medium",
				"Location", "Trans Status", "Order Status", "Qty", "Trans Amount", "Create By", "Update By",
				"Update Date", "Audit By", "Audit Date" });
		Long totalQty = 0L;
		BigDecimal totalTransAmount = BigDecimal.ZERO;
		for (Object object : spendingSummaryDtos) {
			SettlementReportDto temp = (SettlementReportDto) object;

			outputList.add(new String[] { temp.getTransactionDate(),
					(temp.getTransactionId() == null ? "" : temp.getTransactionId()).toString(),
					(temp.getOrderNo() == null ? "" : temp.getOrderNo()).toString(), temp.getOrderDate(),
					temp.getAcademyId(), temp.getMemberName(), temp.getServicePlan(), temp.getSalesman(),
					temp.getEnrollType(), temp.getPaymentMethod(), temp.getPaymentMedia(), temp.getLocation(),
					temp.getTransStatus(), temp.getOrderStatus(),
					(temp.getQty() == null ? "" : temp.getQty()).toString(),
					temp.getTransAmount() == null ? ""
							: temp.getTransAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString(),
					temp.getCreateBy(), temp.getUpdateBy(), temp.getUpdateDate(), temp.getAuditBy(),
					temp.getAuditDate() });
			totalQty = totalQty + (temp.getQty() == null ? 0L : temp.getQty());
			totalTransAmount = totalTransAmount
					.add((temp.getTransAmount() == null ? BigDecimal.ZERO : temp.getTransAmount()));
		}
		outputList.add(new String[] { null, null, null, null, null, null, null, null, null, null, null, null, null,
				"Total:", totalQty.toString(), totalTransAmount.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), null,
				null, null, null, null });
		if (dto.getFileType().equalsIgnoreCase("CSV")) {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(baos));
			CSVWriter csvWriter = new CSVWriter(out, ',');
			csvWriter.writeAll(outputList);
			response.setHeader("Content-Disposition",
					"attachment; filename=" + "Transaction Settlement(" + timeCondition + ").csv");
			response.setContentType("text/csv");
			csvWriter.close();
		} else if (dto.getFileType().equalsIgnoreCase("PDF")) {
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition",
					"attachment; filename=" + "Transaction Settlement(" + timeCondition + ").pdf");
			Document document = new Document(PageSize.A3.rotate());
			document.setMargins(-60f, -60f, 40f, 10f);
			PdfWriter.getInstance(document, baos);

			String footerInfor = "Report Generated Time: "
					+ DateConvertUtil.date2String(new Date(), "yyyy/MM/dd HH:mm");
			addFooterContent(document, footerInfor);
			document.open();

			// Font timeFont = new Font(Font.TIMES_ROMAN, 15, Font.NORMAL);

			// Paragraph time = getParagraph(footerInfor, timeFont, 20f, 0f,
			// 130f);
			// document.add(time);

			Font titleFont = new Font(Font.TIMES_ROMAN, 20, Font.BOLD);
			Paragraph title = getParagraph("Transaction Settlement(" + timeCondition + ")", titleFont, 20f, 0f, 0f);
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);

			Font filterFont = new Font(Font.TIMES_ROMAN, 15, Font.NORMAL);
			Paragraph filter = getParagraph("Filter criteria:", filterFont, 20f, 0f, 130f);
			document.add(filter);

			filterFont.setSize(10);
			Paragraph timeFilter = getParagraph("Time:              " + timeCondition, filterFont, 8f, 0f, 130f);
			document.add(timeFilter);
			Paragraph orderfilter = getParagraph("Order Status:      " + (orderStatus == null ? "" : orderStatus),
					filterFont, 8f, 0f, 130f);
			document.add(orderfilter);
			Paragraph transFilter = getParagraph("Trans Status:      " + (transStatus == null ? "" : transStatus),
					filterFont, 8f, 0f, 130f);
			document.add(transFilter);
			Paragraph enrollFilter = getParagraph("Enroll Type:      " + (enrollType == null ? "" : enrollType),
					filterFont, 8f, 0f, 130f);
			document.add(enrollFilter);
			Paragraph locationFilter = getParagraph("Location:      " + (location == null ? "" : location), filterFont,
					8f, 0f, 130f);
			document.add(locationFilter);
			Paragraph salesmanFilter = getParagraph("Salesman:      " + (salesMan == null ? "" : salesMan), filterFont,
					8f, 0f, 130f);
			document.add(salesmanFilter);
			Paragraph accountantFilter = getParagraph("Accountant:      " + (accountant == null ? "" : accountant),
					filterFont, 8f, 0f, 130f);
			document.add(accountantFilter);
			Paragraph createFilter = getParagraph("Create By:      " + (createBy == null ? "" : createBy), filterFont,
					8f, 0f, 130f);
			document.add(createFilter);

			List<String[]> myEntries = outputList;
			PdfPTable my_first_table = new PdfPTable(outputList.get(0).length);
			my_first_table.setSpacingBefore(30f);
			Font font = new Font(Font.TIMES_ROMAN, 10);
			Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
			for (int i = 0; i < myEntries.size(); i++) {
				String[] strings = myEntries.get(i);
				for (int j = 0; j < strings.length; j++) {
					PdfPCell table_cell = new PdfPCell();
					if (i == 0) {
						table_cell.setPhrase(new Phrase(strings[j], firstColumnFont));
					} else {
						table_cell.setPhrase(new Phrase(strings[j], font));
					}
					table_cell.setNoWrap(false);
					table_cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					my_first_table.addCell(table_cell);
				}
			}

			document.add(my_first_table);
			document.close();
		}
		responseResult.initResult(GTAError.Success.SUCCESS, baos.toByteArray());

		return responseResult;
	}

	@Transactional
	public byte[] getEnrollmentRenewal(String fileType, String sortBy, String isAscending) throws Exception {
		return customerOrderTransDao.getRenewalEnrollmentAttachment(fileType, sortBy, isAscending);
	}

	@Override
	@Transactional
	public ResponseResult readTrans(CustomerOrderTransDto customerOrderTransDto) {
		CustomerOrderTrans customerOrderTrans = this.customerOrderTransDao.get(CustomerOrderTrans.class,
				customerOrderTransDto.getTransactionNo());
		String status = customerOrderTrans.getStatus();
		if (!"FAIL".equalsIgnoreCase(status) && !"VOID".equalsIgnoreCase(status)) {
			responseResult.initResult(GTAError.PaymentError.TRANSACTION_NOT_READ);
			return responseResult;
		}
		if (customerOrderTransDto.getRead()) {
			customerOrderTrans.setReadBy(customerOrderTransDto.getReadBy());
		} else {
			customerOrderTrans.setReadBy(null);
		}
		customerOrderTrans.setReadDate(new Timestamp(new Date().getTime()));
		customerOrderTransDao.save(customerOrderTrans);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	/**
	 * Used to change enrollment status from Approved to Payment Approved or
	 * Payment Approved to Approved
	 * 
	 * @param transactionNo
	 * @author Liky_Pan
	 */
	public void updateEnrollmentStatusOncePayementChange(Long transactionNo) {
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		Long orderNo = null;
		if (customerOrderTrans == null) {
			return;
		} else {
			orderNo = customerOrderTrans.getCustomerOrderHd().getOrderNo();
		}
		CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
		BigDecimal validateBalanceDueAfterPayment = customerTransactionDao.countBalanceDueStatus(orderNo);
		CustomerEnrollment customerEnrollment = null;
		if (customerOrderHd != null) {
			customerEnrollment = customerEnrollmentDao
					.getCustomerEnrollmentByCustomerId(customerOrderHd.getCustomerId());
		}
		if (validateBalanceDueAfterPayment.compareTo(BigDecimal.ZERO) <= 0) {
			if (customerEnrollment != null) {
				if (EnrollStatus.APV.name().equals(customerEnrollment.getStatus())) {
					customerEnrollmentService.recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(),
							EnrollStatus.PYA.name(), customerEnrollment.getCustomerId());
					customerEnrollment.setStatus(EnrollStatus.PYA.name());
					customerEnrollmentDao.update(customerEnrollment);
					// add message notification
					CustomerProfile temProfile = customerProfileDao
							.getCustomerProfileByCustomerId(customerOrderHd.getCustomerId());
					devicePushService.pushMessage(new String[] { customerEnrollment.getSalesFollowBy() },
							Constant.TEMPLATE_ID_MEMBER_PAYMENT_APPROVED,
							new String[] { temProfile.getGivenName() + " " + temProfile.getSurname() },
							Constant.SALESKIT_PUSH_APPLICATION);
				}

			}
		} else if (validateBalanceDueAfterPayment.compareTo(BigDecimal.ZERO) > 0) {
			if (customerEnrollment != null) {
				if (EnrollStatus.PYA.name().equals(customerEnrollment.getStatus())
						|| EnrollStatus.TOA.name().equals(customerEnrollment.getStatus())) {

					customerEnrollmentService.recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(),
							EnrollStatus.APV.name(), customerEnrollment.getCustomerId());

					customerEnrollment.setStatus(EnrollStatus.APV.name());
					customerEnrollmentDao.update(customerEnrollment);

					if (EnrollStatus.TOA.name().equals(customerEnrollment.getStatus())) {
						Member member = memberDao.getMemberByCustomerId(customerEnrollment.getCustomerId());
						if (member != null) {
							member.setEffectiveDate(null);
							memberDao.update(member);
						}
					}

				}
			}
		}

		/***
		 * When any status change happens on payment record for membership
		 * enrolment & renewal, an email notice should be sent to the Sales
		 * Person who is the Membership Officer of the member enrollment record
		 * .
		 */
		if (customerEnrollment != null) {
			if (EnrollStatus.APV.name().equals(customerEnrollment.getStatus())) {
				customerEnrollmentService.sendMailToSalesByUserId(customerEnrollment.getSalesFollowBy(),
						customerEnrollment.getCustomerId());
			} // renewal
			else {
				// hander renewal
				List<CustomerOrderDet> orderDetList = customerOrderHd.getCustomerOrderDets();
				if (null != orderDetList && orderDetList.size() > 0) {
					CustomerOrderDet customerOrderDet = orderDetList.get(0);
					if (customerOrderDet.getItemNo().startsWith(Constant.RENEW_ITEM_NO_PREFIX)) {
						customerEnrollmentService.sendMailToSalesByUserId(customerEnrollment.getSalesFollowBy(),
								customerEnrollment.getCustomerId());
					}

				}
			}

		}
	}

	/***
	 * update enrollment status paymentChanged and send mail to person
	 * 
	 * @param transactionNo
	 * @param oldStatus
	 */
	private void updateEnrollmentStatusOncePayementChange(Long transactionNo, String oldTranscationStatus) {
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		Long orderNo = null;
		if (customerOrderTrans == null) {
			return;
		} else {
			orderNo = customerOrderTrans.getCustomerOrderHd().getOrderNo();
		}
		CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
		BigDecimal validateBalanceDueAfterPayment = customerTransactionDao.countBalanceDueStatus(orderNo);
		CustomerEnrollment customerEnrollment = null;
		if (customerOrderHd != null) {
			customerEnrollment = customerEnrollmentDao
					.getCustomerEnrollmentByCustomerId(customerOrderHd.getCustomerId());
		}
		if (validateBalanceDueAfterPayment.compareTo(BigDecimal.ZERO) <= 0) {
			if (customerEnrollment != null) {
				if (EnrollStatus.APV.name().equals(customerEnrollment.getStatus())) {
					customerEnrollmentService.recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(),
							EnrollStatus.PYA.name(), customerEnrollment.getCustomerId());
					customerEnrollment.setStatus(EnrollStatus.PYA.name());
					customerEnrollmentDao.update(customerEnrollment);
					// add message notification
					CustomerProfile temProfile = customerProfileDao
							.getCustomerProfileByCustomerId(customerOrderHd.getCustomerId());
					devicePushService.pushMessage(new String[] { customerEnrollment.getSalesFollowBy() },
							Constant.TEMPLATE_ID_MEMBER_PAYMENT_APPROVED,
							new String[] { temProfile.getGivenName() + " " + temProfile.getSurname() },
							Constant.SALESKIT_PUSH_APPLICATION);
				}

			}
		} else if (validateBalanceDueAfterPayment.compareTo(BigDecimal.ZERO) > 0) {
			if (customerEnrollment != null) {
				if (EnrollStatus.PYA.name().equals(customerEnrollment.getStatus())
						|| EnrollStatus.TOA.name().equals(customerEnrollment.getStatus())) {

					customerEnrollmentService.recordCustomerEnrollmentUpdate(customerEnrollment.getStatus(),
							EnrollStatus.APV.name(), customerEnrollment.getCustomerId());

					customerEnrollment.setStatus(EnrollStatus.APV.name());
					customerEnrollmentDao.update(customerEnrollment);

					if (EnrollStatus.TOA.name().equals(customerEnrollment.getStatus())) {
						Member member = memberDao.getMemberByCustomerId(customerEnrollment.getCustomerId());
						if (member != null) {
							member.setEffectiveDate(null);
							memberDao.update(member);
						}
					}

				}
			}
		}

		/***
		 * When any status change happens on payment record for membership
		 * enrolment & renewal, an email notice should be sent to the Sales
		 * Person who is the Membership Officer of the member enrollment record
		 * .
		 */
		if (customerEnrollment != null) {
			if (EnrollStatus.APV.name().equals(customerEnrollment.getStatus())) {
				if (!StringUtils.isEmpty(oldTranscationStatus)) {
					customerEnrollmentService.sendMailToSalesByUserId(customerEnrollment.getSalesFollowBy(),
							customerEnrollment.getCustomerId());
				} else {
					if (!oldTranscationStatus.equals(customerOrderTrans.getStatus())) {
						customerEnrollmentService.sendMailToSalesByUserId(customerEnrollment.getSalesFollowBy(),
								customerEnrollment.getCustomerId());
					}
				}
			}
			// renewal
			else {
				// hander renewal
				List<CustomerOrderDet> orderDetList = customerOrderHd.getCustomerOrderDets();
				if (null != orderDetList && orderDetList.size() > 0) {
					CustomerOrderDet customerOrderDet = orderDetList.get(0);
					if (customerOrderDet.getItemNo().startsWith(Constant.RENEW_ITEM_NO_PREFIX)) {
						if (!StringUtils.isEmpty(oldTranscationStatus)) {
							customerEnrollmentService.sendMailToSalesByUserId(customerEnrollment.getSalesFollowBy(),
									customerEnrollment.getCustomerId());
						} else {
							if (!oldTranscationStatus.equals(customerOrderTrans.getStatus())) {
								customerEnrollmentService.sendMailToSalesByUserId(customerEnrollment.getSalesFollowBy(),
										customerEnrollment.getCustomerId());
							}
						}
					}

				}
			}

		}
	}

	/***
	 * genrate statement report by customer/startDate/endDate
	 * @param customerId
	 * @param startDateString
	 * @param endDateString
	 * @throws Exception
	 */
	private void genrateStatementByCustomerId(Long customerId,String startDateString, String endDateString)throws Exception
	{

		CustomerStatementBaseInfoDto statementInfo = getCustomerStatementBaseInfoDto(customerId, startDateString,
				endDateString);

		// Calculate last month opening balance
		BigDecimal openingBalance = getOpeningBalanceByLastMonth(customerId, startDateString);

		// Calculate this month opening balance
		BigDecimal closingBalance = getClosingBalanceByThisMonth(customerId, startDateString);

		// Generate PDF file name
		String basePath = appProps.getProperty("pdf.server.report");

		File file = new File(basePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		Date date = DateConvertUtil.parseString2Date(startDateString, "yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Date generateMonthDate = calendar.getTime();
		String generateMonth = DateConvertUtil.date2String(generateMonthDate, "yyyyMM");
		String filePath = basePath + "/Patron Account Statement_" + customerId + "_" + generateMonth + ".pdf";
		logger.info(" customerOrderTrans report  pdf  path:"+filePath);
		OutputStream out = new FileOutputStream(filePath);

		Document document = new Document(PageSize.A4, 10, 10, 110, 50);
		// document.setMargins(10f, 10f, 10f, 10f);
		PdfWriter writer = PdfWriter.getInstance(document, out);

		Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, Color.black);
		Color color = new Color(140, 0, 30);
		Font tableTitleFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, Color.white);

		List<String[]> allCashvalueList = new ArrayList<String[]>();
		allCashvalueList.add(new String[] { "Post Date", "Ref.No", "Description", "Payment Type",
				"Dependent Patron's Name", "Amount(HK$)", "Cash Value Balance(HK$)" });
		allCashvalueList.add(
				new String[] { null, null, "Opening Balance", null, null, null, getFormatAmount(openingBalance) });

		// Get cash value statement list
		List<PatronAccountStatementDto> patronAccountStatementDtos = getCashValueStatementList(customerId,
				startDateString, endDateString);

		//BigDecimal topupTotal = new BigDecimal(0);
		//BigDecimal spendingTotal = new BigDecimal(0);
		BigDecimal cashBalance = openingBalance;
		if(patronAccountStatementDtos != null && patronAccountStatementDtos.size() > 0){
			for (int i = 0; i < patronAccountStatementDtos.size(); i++) {
				PatronAccountStatementDto dto = patronAccountStatementDtos.get(i);
				

				updatePatronAccountStatementDesc(dto);
				if (!Constant.TOPUP_ITEM_NO.equals(dto.getItemNo()) 
						&& !Constant.REFUND_SERVICE_ITEM_NO.equals(dto.getItemNo())
						&& !Constant.CREDIT_ITEM_NO.equals(dto.getItemNo())) {
					/*if (statementInfo.getMemberName().equals(dto.getMemberName()) || "-".equals(dto.getMemberName())) {
						spendingTotal = spendingTotal.add(dto.getAmount());
					}*/
					if(Constant.PMSTHIRD_ITEM_NO.equals(dto.getItemNo()) || Constant.MMSTHIRD_ITEM_NO.equals(dto.getItemNo()) || Constant.POSTHIRD_ITEM_NO.equals(dto.getItemNo())){
						if(Constant.Status.VOID.name().equals(dto.getStatus())){
							//if void, that is equals to refund
							cashBalance = cashBalance.add(dto.getAmount());
							dto.setAmount(dto.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
						}else{
							cashBalance = cashBalance.subtract(dto.getAmount());
							dto.setAmount(dto.getAmount().negate().setScale(2, BigDecimal.ROUND_HALF_UP));
						}
					}else{
						cashBalance = cashBalance.subtract(dto.getAmount());
						dto.setAmount(dto.getAmount().negate().setScale(2, BigDecimal.ROUND_HALF_UP));
					}
					dto.setCashvalueBalance(cashBalance);
				} else{
					/*if (statementInfo.getMemberName().equals(dto.getMemberName()) || "-".equals(dto.getMemberName())) {
						topupTotal = topupTotal.add(dto.getAmount());
					}*/
					cashBalance = cashBalance.add(dto.getAmount());
					dto.setCashvalueBalance(cashBalance);
				}
				/***
				 * christ add filter  dto.getAmount() is zero
				 * SGG-2474
				 */
				if(dto.getAmount().compareTo(BigDecimal.ZERO)!=0)   // SGG-3616 && dto.getPaymentType().equals(PaymentMethod.CASHVALUE.getDesc())
				{
					if (null == statementInfo.getCreditLimit()
							|| statementInfo.getCreditLimit().compareTo(BigDecimal.ZERO) > 0) {
						dto.setPaymentType("City Ledger");
					}
					allCashvalueList.add(new String[] { DateConvertUtil.date2String(dto.getTransactionDate(), "dd/MM/yyyy"),
							dto.getInvoiceNo().toString(), dto.getDescription(), dto.getPaymentType(), dto.getMemberName(),
							getFormatAmount(dto.getAmount()), getFormatAmount(dto.getCashvalueBalance()) });
				}
				if (i == patronAccountStatementDtos.size() - 1) {
					// In case the data has been updated, so set the last
					// PatronAccountStatementDto's CashvalueBalance for the
					// closingBalance.
					closingBalance = dto.getCashvalueBalance();
				}
			}
		}else{
			closingBalance = openingBalance;
		}
		allCashvalueList.add(
				new String[] { null, null, "Closing Balance", null, null, null, getFormatAmount(closingBalance) });
		// In case the data is not correct because of the report is not been
		// generated at first day of current month.
		statementInfo.setCashvalue(closingBalance);

		// Add head statement info
		addStatementBaseInfo(writer, document, statementInfo);
		
		PdfPTable allCashvaluePdfPTable = new PdfPTable(allCashvalueList.get(0).length);
		allCashvaluePdfPTable.setTotalWidth(new float[] { 20f, 15f, 23f, 20f, 20f, 20f, 20f });
		Font font = new Font(Font.TIMES_ROMAN, 10);
		for (int i = 0; i < allCashvalueList.size(); i++) {
			String[] strings = allCashvalueList.get(i);
			for (int j = 0; j < strings.length; j++) {
				PdfPCell cell = new PdfPCell();
				if (i == 0) {
					cell.setBackgroundColor(color);
					cell.setPhrase(new Phrase(strings[j], tableTitleFont));
				} else if (i == 1 || i == allCashvalueList.size() - 1) {
					cell.setBackgroundColor(Color.white);
					cell.setPhrase(new Phrase(strings[j], firstColumnFont));
				} else {
					cell.setBackgroundColor(Color.white);
					cell.setPhrase(new Phrase(strings[j], font));
				}
				cell.setNoWrap(false);
				if (j == strings.length - 1 || j == strings.length - 2) {
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				} else {
					cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				}
				allCashvaluePdfPTable.addCell(cell);
			}
		}
		document.add(allCashvaluePdfPTable);
		Date startDate = DateConvertUtil.parseString2Date(startDateString, "yyyy-MM-dd");

		// Add the latest five month balance
		addBalanceHistoryTable(document, closingBalance, customerId, startDate);
		
		addDescription(document, statementInfo);

		Paragraph otherDetail = getParagraph("For Other Consumption Detail ", firstColumnFont, 10f, 0f, 57f);
		document.add(otherDetail);

		Paragraph pmTitle = getParagraph("Primary Patron - " + statementInfo.getMemberName(), firstColumnFont, 10f,
				5f, 57f);
		document.add(pmTitle);

		// Add other consumption table include primary member and dependent
		// members.
		addOtherConsumptionTable(document, customerId, startDateString, endDateString);
		Paragraph summaryTitle = getParagraph("Summary", firstColumnFont, 0f, 10f, 57f);
		document.add(summaryTitle);

		// Add summary table include primary member and dependent members.
		addSummaryTable(document, customerId, startDateString, endDateString);

		// Add foot content.
		addFootContent(document);

		document.close();
		if (null != out) {
			out.flush();
			out.close();
		}

	
	}
	@Override
	@Transactional
	public void generatePatronAccountStatement(String startDateString, String endDateString) throws Exception {
		// Retrieve Member List
		List<MemberDto> memberDtos = getMemberList(startDateString, endDateString);
		// Calculate the current month closing balance
		calculateClosingBalance(memberDtos, startDateString, endDateString);

		for (MemberDto memberDto : memberDtos) {
			this.genrateStatementByCustomerId(memberDto.getCustomerId(), startDateString, endDateString);
		}
	}
	private void addDescription(Document document,CustomerStatementBaseInfoDto statementInfo){
		Font font = new Font(Font.TIMES_ROMAN, 10);
		String accountName = appProps.getProperty("account.name");
//		String accountNo = appProps.getProperty("account.no");
		String swiftCode = appProps.getProperty("swift.code");
		String accountAddress = appProps.getProperty("account.address");
		String bankName = appProps.getProperty("bank.name");
		String bankAddress = appProps.getProperty("bank.address");
		String financeEmail = appProps.getProperty("finance.email");
		String faxNo = appProps.getProperty("fax.no");
		String content = "If payment by Bank Transfer, please remit to the following bank account:\n" + "Account Name: "
				+ accountName + "\n" + "Account No: " + CommUtil.formatVirtualAcc(statementInfo.getVirtualNo()) + "\n" + "Swift Code: " + swiftCode + "\n"
				+ "Account Address: " + accountAddress + "\n" + "Bank Name: " + bankName + "\n" + "Bank Address: "
				+ bankAddress + "\n" + "Please send the Remittance Advice to our Finance Department via email: "
				+ financeEmail + " or Fax no. " + faxNo + "\n"
				+ "If payment by cheque. Cheque should be crossed, mark \"Not Negotiable A/C Payee Only\" and made payable to \"Hong Kong Golf & Tennis Academy Management Company Limited\".";
		Paragraph accountInfo = getParagraph(content, font, 10f, 0f, 57f);
		try {
			document.add(accountInfo);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private Paragraph getParagraph(String content, Font font, float before, float after, float indentationLeft) {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Phrase(content, font));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		// paragraph.setLeading(15f);
		paragraph.setSpacingBefore(before);
		paragraph.setSpacingAfter(after);
		paragraph.setIndentationLeft(indentationLeft);
		return paragraph;
	}

	private List<MemberDto> getMemberList(String startDateString, String endDateString) {
		String sql = "SELECT  DISTINCT  m.customer_id as customerId  from customer_order_hd coh,member m, customer_order_det cod, pos_service_item_price pos "
				+ " where coh.customer_id = m.customer_id and coh.order_no = cod.order_no AND cod.item_no = pos.item_no "
				+ " AND (LEFT (cod.item_no, 3) != 'SRV' AND LEFT (cod.item_no, 5) != 'RENEW') "
				+ " and coh.order_date BETWEEN '" + startDateString + "' and '" + endDateString
				+ "' and (m.member_type = 'IPM' OR m.member_type = 'CPM') "
				+ " UNION SELECT customer_id as customerId  from member me where me.`status` = 'ACT' and (me.member_type = 'IPM' OR me.member_type = 'CPM') ORDER BY customerId";

		List<MemberDto> memberDtos = memberDao.getDtoBySql(sql, null, MemberDto.class);
		return memberDtos;
	}

	private List<PatronAccountStatementDto> getCashValueStatementList(Long customerId, String startDateString,
			String endDateString) {
		String cashvalueSql = "SELECT trans.transaction_timestamp as transactionDate,trans.transaction_no as invoiceNo,trans.status, (CASE hd.customer_id WHEN "
				+ customerId + " THEN '-' ELSE CONCAT(cp.given_name,' ',cp.surname) END) as memberName,"
				+ " (CASE pos.item_catagory WHEN 'REFUND'  THEN (CASE det.item_no WHEN 'CVR00000001' THEN 'Cash Value Refund' ELSE 'Service Refund' END) WHEN 'MMS' THEN 'Wellness Center'  ELSE pos.item_catagory END) as category,"
				+ " pos.description as description,"
				+ " (CASE trans.payment_method_code WHEN '"+PaymentMethod.VISA.name()+"' THEN '"+PaymentMethod.VISA.getDesc()+"' "
				+ "  WHEN '"+PaymentMethod.MASTER.name()+"' THEN '"+PaymentMethod.MASTER.getDesc()+"'"
				+ "  WHEN '"+PaymentMethod.CASH.name()+"' THEN '"+PaymentMethod.CASH.getDesc()+"'"
				+ "  WHEN '"+PaymentMethod.BT.name()+"' THEN '"+PaymentMethod.BT.getDesc()+"' "
				+ "  when '"+PaymentMethod.CHRGPOST.name() + "' then '"+PaymentMethod.CHRGPOST.getDesc()+"' "
				+ "  WHEN '"+PaymentMethod.CHEQUE.name()+"' THEN '"+PaymentMethod.CHEQUE.getDesc()+"' "
				+ "  WHEN '"+ PaymentMethod.CASHVALUE.name()+ "' THEN '"+PaymentMethod.CASHVALUE.getDesc()+"' "
				+ "  WHEN '"+PaymentMethod.VAC.name()+"' THEN 'Debit via Bank' "
				+ "  WHEN '"+PaymentMethod.CUP.name()+"' THEN '"+PaymentMethod.CUP.getDesc()+"'"
				+ "  WHEN '"+PaymentMethod.AMEX.name()+"' THEN '"+PaymentMethod.AMEX.getDesc()+"'"
				+ "  WHEN '"+PaymentMethod.DDI.name()+"' THEN '"+PaymentMethod.DDI.getDesc()+"' ELSE 'Other' END) as paymentType, "
				+ "trans.paid_amount as amount,det.item_no as itemNo,trans.internal_remark as remark " + " from customer_order_trans trans "
				+ " INNER JOIN customer_order_hd hd ON trans.order_no = hd.order_no "
				+ " INNER JOIN customer_order_det det ON det.order_no = hd.order_no "
				+ " INNER JOIN member m ON hd.customer_id = m.customer_id"
				+ " INNER JOIN customer_profile cp ON cp.customer_id = m.customer_id"
				+ " INNER JOIN pos_service_item_price pos ON det.item_no = pos.item_no "
				+ " WHERE ((det.item_no != 'CVT0000001'  and trans.`status` = 'SUC' and trans.payment_method_code = '"+ PaymentMethod.CASHVALUE.name() + "') "
				+ " OR (trans.`status` = 'RFU' AND det.item_no = 'CVR00000001') "
				+ " OR (trans.`status` = 'SUC' and det.item_no = 'CVT0000001') "
				+ " OR (trans.`status` = 'RFU' AND det.item_no = 'SRR00000001') "
				+ " OR (trans.`status` = 'VOID' AND det.item_no = 'PMS00000001') OR (trans.`status` = 'SUC' AND det.item_no = 'PMS00000001') "
				+ " OR (trans.`status` = 'VOID' AND det.item_no = 'MMS00000001') OR (trans.`status` = 'SUC' AND det.item_no = 'MMS00000001') "
				+ " OR (trans.`status` = 'VOID' AND det.item_no = 'RST00000001') OR (trans.`status` = 'SUC' AND det.item_no = 'RST00000001')) "
				+ " and (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' and '" + endDateString +"') " 
				+ " and (m.customer_id = ? or m.superior_member_id = ?) ORDER BY trans.transaction_timestamp";

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(customerId);
		List<PatronAccountStatementDto> patronAccountStatementDtos = customerOrderTransDao.getDtoBySql(cashvalueSql,
				param, PatronAccountStatementDto.class);
		
		//filter repeat
		Map<Long, Long>map=new HashMap<>();
		List<PatronAccountStatementDto> filterRepeatList=new ArrayList<>();
		for(PatronAccountStatementDto account : patronAccountStatementDtos)
		{
			Long key=account.getInvoiceNo();
		    if(!map.containsKey(key))
		    {
		    	map.put(key,key);
		    	filterRepeatList.add(account);	
		    }
		    
		}
		return filterRepeatList;
	}

	private BigDecimal getOpeningBalanceByLastMonth(Long customerId, String startDateString) {
		BigDecimal openingBalance = BigDecimal.ZERO;
		Member member = memberDao.get(Member.class, customerId);
		MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, customerId);
		String strCutoffDate = getCutOffDateByLastMonth(startDateString);
		MemberCashvalueBalHistory lastMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(
				MemberCashvalueBalHistory.class, new String[] { "customerId", "cutoffDate" },
				new Serializable[] { customerId, DateConvertUtil.getDateFromStr(strCutoffDate) });
		if (null == lastMemberCashvalueBalHistory) {
			Date joinDate = member.getFirstJoinDate();
			String currentMonthString = startDateString.substring(0, 7);
			// if the member's join date is last month, it's opening balance is
			// it's initial cash value.
			if (DateConvertUtil.date2String(joinDate, "yyyy-MM").equals(currentMonthString)) {
				if (null == memberCashvalue) {
					openingBalance = new BigDecimal(0);
				} else {
					openingBalance = memberCashvalue.getInitialValue();
				}
			} else {
				openingBalance = new BigDecimal(0);
			}

		} else {
			openingBalance = lastMemberCashvalueBalHistory.getRecalBalance();
		}
		return openingBalance;
	}

	private BigDecimal getClosingBalanceByThisMonth(Long customerId, String startDateString) {
		// Calculate this month closing balance
		BigDecimal closingBalance = BigDecimal.ZERO;
		String strCurrCutoffDate = startDateString + Constant.REPORT_CUTOFF_DATE_HMS;

		MemberCashvalueBalHistory currMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(
				MemberCashvalueBalHistory.class, new String[] { "customerId", "cutoffDate" },
				new Serializable[] { customerId, DateConvertUtil.getDateFromStr(strCurrCutoffDate) });

		if (null != currMemberCashvalueBalHistory) {
			closingBalance = currMemberCashvalueBalHistory.getRecalBalance();
		}
		return closingBalance;
	}

	private String getCutOffDateByLastMonth(String startDateString) {
		Date date = DateConvertUtil.parseString2Date(startDateString, "yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date cutoffDate = calendar.getTime();
		String strCutoffDate = DateConvertUtil.date2String(cutoffDate, "yyyy-MM-dd") + Constant.REPORT_CUTOFF_DATE_HMS;
		return strCutoffDate;
	}

	@Transactional
	public void calculateClosingBalance(List<MemberDto> memberDtos, String startDateString, String endDateString)
			throws Exception {
		for (MemberDto dto : memberDtos) {
			Long customerId = dto.getCustomerId();
			Member member = memberDao.get(Member.class, customerId);
			if (memberType.CPM.name().equalsIgnoreCase(member.getMemberType())
					|| memberType.IPM.name().equalsIgnoreCase(member.getMemberType())) {
				MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, customerId);
				MemberCashvalueBalHistory memberCashvalueBalHistory = new MemberCashvalueBalHistory();
				BigDecimal openingBalance = getOpeningBalanceByLastMonth(customerId, startDateString);

				// Count the current member's top up and spending
				Date startDate = DateConvertUtil.parseString2Date(startDateString, "yyyy-MM-dd");
				BigDecimal sumTopup = getTotalTopupAmount(customerId, 0, startDate);
				BigDecimal sumSpending = getTotalSpendingAmount(customerId, 0, startDate);
				BigDecimal closingBalance = openingBalance.add(sumTopup).subtract(sumSpending);

				Date currCutoffDate = DateConvertUtil.getDateFromStr(endDateString + Constant.REPORT_CUTOFF_DATE_HMS);
				MemberCashvalueBalHistory existMemberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(
						MemberCashvalueBalHistory.class, new String[] { "customerId", "cutoffDate" },
						new Serializable[] { customerId, currCutoffDate });

				if (null == existMemberCashvalueBalHistory) {
					memberCashvalueBalHistory.setCutoffDate(currCutoffDate);
					memberCashvalueBalHistory.setLastCashvalueBal(
							memberCashvalue == null ? new BigDecimal(0) : memberCashvalue.getAvailableBalance());
					memberCashvalueBalHistory.setLastCashvalueDate(new Date());
					memberCashvalueBalHistory.setProcessDate(new Timestamp(new Date().getTime()));
					memberCashvalueBalHistory.setRecalBalance(closingBalance);
					memberCashvalueBalHistory.setCustomerId(customerId);
					memberCashvalueBalHistoryDao.save(memberCashvalueBalHistory);
					//删除重复数据
					deleteOldData(startDateString, endDateString, customerId);
					/*
					 * select all refer to cash value transaction records insert
					 * into member_transaction_log
					 */
/*					String insertSql = "INSERT INTO member_transaction_log(transaction_no,process_id,order_no,payment_method_code,payment_media,"
							+ " transaction_timestamp,from_transaction_no,paid_amount,status,audit_by,audit_date,internal_remark,"
							+ " payment_location_code,create_by,update_date,update_by)"
							+ " SELECT DISTINCT trans.transaction_no," + memberCashvalueBalHistory.getProcessId()
							+ ",trans.order_no,trans.payment_method_code,trans.payment_media,"
							+ " trans.transaction_timestamp,trans.from_transaction_no,trans.paid_amount,trans.status,trans.audit_by,"
							+ " trans.audit_date,trans.internal_remark,trans.payment_location_code,trans.create_by,trans.update_date,trans.update_by "
							+ " from customer_order_trans trans,customer_order_hd hd,customer_order_det det,member m"
							+ " WHERE ((det.item_no != 'CVT0000001'  and trans.`status` = 'SUC' and trans.payment_method_code = '"
							+ PaymentMethod.CASHVALUE.name()
							+ "') OR (det.item_no = 'CVR00000001' AND trans.`status` = 'RFU')"
							+ " OR (det.item_no = 'SRR00000001' AND trans.`status` = 'RFU')) "
							+ " and trans.order_no = hd. order_no and hd.customer_id = m.customer_id and hd.order_no = det.order_no"
							+ " and (m.customer_id = ? or m.superior_member_id = ?)"
							+ " AND (DATE(trans.transaction_timestamp) BETWEEN '" + startDateString + "' and '"
							+ endDateString + "')";
*/                  // SAM 20161007 revised the SQL
					String insertSql = "INSERT INTO member_transaction_log(transaction_no,process_id,order_no,payment_method_code,payment_media,"
							+ " transaction_timestamp,from_transaction_no,paid_amount,status,audit_by,audit_date,internal_remark,"
							+ " payment_location_code,create_by,update_date,update_by)"
							+ " SELECT  trans.transaction_no," + memberCashvalueBalHistory.getProcessId()
							+ ",trans.order_no,trans.payment_method_code,trans.payment_media,"
							+ " trans.transaction_timestamp,trans.from_transaction_no,trans.paid_amount,trans.status,trans.audit_by,"
							+ " trans.audit_date,trans.internal_remark,trans.payment_location_code,trans.create_by,trans.update_date,trans.update_by "
							+ " from customer_order_trans trans,customer_order_hd hd,member m"
							+ " WHERE trans.payment_method_code = '"+ PaymentMethod.CASHVALUE.name()+ "' "
							+ " and trans.order_no = hd. order_no and hd.customer_id = m.customer_id "
							+ " and (m.customer_id = ? or m.superior_member_id = ?)"
							+ " AND (DATE(trans.transaction_timestamp) BETWEEN '" + startDateString + "' and '"
							+ endDateString + "')"					
					        +  " and (trans.`status` = 'SUC' "
					        +	  " and not exists(select 1 from customer_order_det det "
					        +                     " where det.item_no in ('CVT0000001', 'SCVR00000001') " 
					        		             + " and hd.order_no = det.order_no) "
					              + " or trans.`status` = 'RFU' and "
					                   + " exists(select 1 from customer_order_det det "
					        		     + "where  det.item_no in ('CVR00000001', 'SRR00000001') " 
					        		      + " and hd.order_no = det.order_no))";					        		
					
					List<Serializable> param = new ArrayList<Serializable>();
					param.add(customerId);
					param.add(customerId);
					memberTransactionLogDao.sqlUpdate(insertSql, param);
				} else {
					existMemberCashvalueBalHistory.setCutoffDate(currCutoffDate);
					/***
					 * @Sam say:2017-05-08
					 * 不要把新的 cashvalue balance 覆蓋 last_cahvalue_bal. 因為如果recal 時不是月尾, 月尾的balance 就會被overwrite 而無法知道.
					 */
//					existMemberCashvalueBalHistory.setLastCashvalueBal(
//							memberCashvalue == null ? new BigDecimal(0) : memberCashvalue.getAvailableBalance());
					existMemberCashvalueBalHistory.setLastCashvalueDate(new Date());
					existMemberCashvalueBalHistory.setProcessDate(new Timestamp(new Date().getTime()));
					existMemberCashvalueBalHistory.setRecalBalance(closingBalance);
					existMemberCashvalueBalHistory.setCustomerId(customerId);
					memberCashvalueBalHistoryDao.update(existMemberCashvalueBalHistory);
				}
			}
		}
	}
	/**
	 * 删除重复数据
	 * @param startDateString
	 * @param endDateString
	 * @param customerId
	 */
	@Transactional
	private void deleteOldData(String startDateString, String endDateString, Long customerId){
		String sql = "delete from member_transaction_log  where transaction_no IN ("
		+ " SELECT  * FROM (" 
		+ " SELECT  trans.transaction_no" 
		+ " from customer_order_trans trans,customer_order_hd hd,member m"
		+ " WHERE trans.payment_method_code = '"+ PaymentMethod.CASHVALUE.name()+ "' "
		+ " and trans.order_no = hd. order_no and hd.customer_id = m.customer_id "
		+ " and (m.customer_id = ? or m.superior_member_id = ?)"
		+ " AND (DATE(trans.transaction_timestamp) BETWEEN '" + startDateString + "' and '"
		+ endDateString + "')"					
        +  " and (trans.`status` = 'SUC' "
        +	  " and not exists(select 1 from customer_order_det det "
        +                     " where det.item_no in ('CVT0000001', 'SCVR00000001') " 
        		             + " and hd.order_no = det.order_no) "
              + " or trans.`status` = 'RFU' and "
                   + " exists(select 1 from customer_order_det det "
        		     + "where  det.item_no in ('CVR00000001', 'SRR00000001') " 
        		      + " and hd.order_no = det.order_no))) AS t)";					        		

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(customerId);
		memberTransactionLogDao.sqlUpdate(sql, param);
	}

	@Transactional
	public void modifyCustomerOrderHd(CustomerOrderHd order) {
		customerOrderHdDao.update(order);
	}

	@Transactional
	public CustomerStatementBaseInfoDto getCustomerStatementBaseInfoDto(Long customerId, String startDateString,
			String endDateString) {
		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, customerId);
		final String email = customerProfile.getContactEmail();
		Member member = memberDao.get(Member.class, customerId);
		String memberName = customerProfile.getGivenName() + " " + customerProfile.getSurname();
		String companyName = customerProfile.getCompanyName();
		String address1 = customerProfile.getPostalAddress1();
		String address2 = customerProfile.getPostalAddress2();
		String address3 = customerProfile.getPostalDistrict();
		String academyId = member.getAcademyNo();
		String typeDesc = memberTypeDao.get(MemberType.class, member.getMemberType()).getTypeName();
		MemberPaymentAcc acc = memberPaymentAccDao.getMemberPaymentAccByCustomerId(customerId);
		String virtualNo = acc == null ? "" : acc.getAccountNo();
		MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, customerId);
		BigDecimal cashvalue = memberCashvalue == null ? BigDecimal.ZERO : memberCashvalue.getAvailableBalance();
		BigDecimal creditLimit = null;
		MemberLimitRule memberLimitRule = null;
		if (Constant.memberType.CPM.name().equalsIgnoreCase(member.getMemberType())
				|| Constant.memberType.IPM.name().equalsIgnoreCase(member.getMemberType())) {
			memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, "CR");
		} else {
			memberLimitRule = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, "TRN");
		}
		if (memberLimitRule != null) {
			creditLimit = memberLimitRule.getNumValue();
			// creditLimit = "$"+numValue.setScale(2,
			// BigDecimal.ROUND_HALF_UP).toString();
		}
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		String from = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
		if (null != startDateString) {
			from = startDateString;
		}
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String to = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
		if (null != endDateString) {
			to = endDateString;
		}
		String period = from + " to " + to;
		String printDate = DateConvertUtil.date2String(date, "yyyy-MM-dd");
		String statementBalance = "";
		CustomerStatementBaseInfoDto statementBaseInfoDto = new CustomerStatementBaseInfoDto(memberName, companyName,
				address1, address2, address3, email, academyId, typeDesc, virtualNo, creditLimit, period, printDate,
				statementBalance, cashvalue);
		return statementBaseInfoDto;
	}

	public String getFormatAmount(BigDecimal amount) {
		if (amount == null) {
			amount = BigDecimal.ZERO;
		}
		DecimalFormat format = new DecimalFormat("##,##0.00");
		return format.format(amount.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
	}

	private void addStatementBaseInfo(PdfWriter writer, Document document, CustomerStatementBaseInfoDto statementInfo) {
		try {
			String logoPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			logoPath = logoPath.substring(0, logoPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/logo_header.jpg";
			Image logo = Image.getInstance(logoPath);
			logo.setAlignment(Image.MIDDLE);
			logo.setAbsolutePosition(0, 0);
			logo.scalePercent(25);
			Chunk chunk = new Chunk(logo, 0, 0, true);
			Phrase phrase = new Phrase();
			phrase.add(chunk);
			Rectangle rect = new Rectangle(36, 54, 559, 835);
			CustomHeaderFooter header = new CustomHeaderFooter();
			writer.setPageEvent(header);
			document.open();
			header.onEndPage(writer, document, rect, phrase);
			Font titleFont = new Font(Font.TIMES_ROMAN, 20, Font.BOLD);
			Color color = new Color(140, 0, 30);
			titleFont.setColor(color);
			Paragraph title = getParagraph("Patron Account Statement", titleFont, 20f, 0f, 0f);
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);

			Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
			Paragraph leftParagraph = getParagraph("", contentFont, 20f, 20f, 0f);

			PdfPTable infoPdfPTable = new PdfPTable(3);
			PdfPCell infoCell = new PdfPCell();
			infoCell.setPhrase(new Phrase(statementInfo.getMemberName(), contentFont));
			infoCell.setBorder(0);
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0.5f);
			infoCell.setPhrase(new Phrase("Academy ID", contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0.5f);
			infoCell.setPhrase(new Phrase(statementInfo.getAcademyId(), contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getCompanyName(), contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Patron Type", contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase(statementInfo.getTypeDesc(), contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getAddress1(), contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Virtual Account", contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setPhrase(new Phrase(CommUtil.formatVirtualAcc(statementInfo.getVirtualNo()), contentFont));
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getAddress2(), contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Account Balance", contentFont));
			infoPdfPTable.addCell(infoCell);
			if (null == statementInfo.getCashvalue()) {
				infoCell.setPhrase(new Phrase("$0.00", contentFont));
			} else {
				infoCell.setPhrase(new Phrase("$" + getFormatAmount(statementInfo.getCashvalue()), contentFont));
			}
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getAddress3(), contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Credit Limit", contentFont));
			infoPdfPTable.addCell(infoCell);
			if (null == statementInfo.getCreditLimit()) {
				infoCell.setPhrase(new Phrase("Unlimited", contentFont));
			} else {
				infoCell.setPhrase(new Phrase("$" + getFormatAmount(statementInfo.getCreditLimit()), contentFont));
			}
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase(statementInfo.getEmail(), contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Statement Period", contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setPhrase(new Phrase(statementInfo.getPeriod(), contentFont));
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase("", contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Issue Date", contentFont));
			
			
			infoPdfPTable.addCell(infoCell);

			infoCell.setPhrase(new Phrase(statementInfo.getPrintDate(), contentFont));
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorder(0);
			infoCell.setPhrase(new Phrase("", contentFont));
			infoPdfPTable.addCell(infoCell);

			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0.5f);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoCell.setPhrase(new Phrase("Due Date", contentFont));
			infoPdfPTable.addCell(infoCell);

			Date date = new Date();
			Calendar c = Calendar.getInstance();
			
			String suffix =ddxProps.getProperty("valueDate");
			suffix=!StringUtils.isEmpty(suffix)?suffix.trim():"21";
			if(!suffix.matches("^\\d{1}$")){
				suffix="21";
			}
			if(!(Integer.valueOf(suffix)>=DateCalcUtil.getMonthFirstDay()&&Integer.valueOf(suffix)<=DateCalcUtil.getMonthLastDay()))
			{
				suffix="21";
			}
			c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(suffix));
			date = c.getTime();
			List<Date> holidays = gtaPublicHolidayDao.getNearestHolidays(date);
			SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyy-MM-dd");
			
			List<String> holidayStrs = new ArrayList<String>();
			for (Date d : holidays) {
				holidayStrs.add(sdf_yyyymmdd.format(d));
			}
			while(true){
				if (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7
						|| holidayStrs.contains(sdf_yyyymmdd.format(c.getTime()))) {
					c.add(Calendar.DATE, 1);
				} else {
					break;
				}
			}
			date = c.getTime();
			String valueDate = sdf_yyyymmdd.format(date);
			
//			String issueDates = DateCalcUtil.getDateAfter(new Date(), 14);
			Date issueDate=DateConvertUtil.parseString2Date(valueDate, "yyyy-MM-dd");
			
			infoCell.setPhrase(new Phrase(DateConvertUtil.parseDate2String(issueDate, "yyyy-MM-dd"), contentFont));
			infoCell.setBorderWidthBottom(0.5f);
			infoCell.setBorderWidthLeft(0);
			infoCell.setBorderWidthRight(0.5f);
			infoCell.setBorderWidthTop(0);
			infoPdfPTable.addCell(infoCell);
			leftParagraph.add(infoPdfPTable);

			document.add(leftParagraph);

			Paragraph detailParagraph = getParagraph("For Cash Value Detail (Primary Patron and Dependent Patron)",
					new Font(Font.TIMES_ROMAN, 10, Font.BOLD), 0f, 5f, 57f);
			document.add(detailParagraph);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addFooterContent(Document document, String content) {
		Paragraph foot = new Paragraph();
		Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		foot.setFont(contentFont);
		foot.add(new Phrase("                                                      " + content));
		HeaderFooter footer = new HeaderFooter(foot, true);
		footer.setBorder(0);
		document.setFooter(footer);
	}

	private void addFootContent(Document document) {
		try {
			Paragraph foot = new Paragraph();
			Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
			Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
			foot.setFont(contentFont);
			foot.setAlignment(Element.ALIGN_CENTER);
			foot.setLeading(15f);
			foot.setSpacingBefore(20f);
			foot.add(new Phrase("If you have any questions, please contact our Patron Relationship Management Team at Tel: (852)3187 8936 or email: PRM@hkgta.com."));
			foot.add(new Phrase("\n"));
			foot.add(new Phrase("Thank You For Your Patronage!", firstColumnFont));
			document.add(foot);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult getStatementPDF(StatementParamsDto statementParamsDto) throws Exception {
		String year = statementParamsDto.getYear();
		String month = statementParamsDto.getMonth();
		// Long customerId = statementParamsDto.getCustomerIds()[0];
		String cutOffDateString = year + "-" + month + "-01 23:59:59";
		int mon = Integer.parseInt(month);
		if (mon < 10) {
			cutOffDateString = year + "-0" + month + "-01 23:59:59";
		}
		Date date = DateConvertUtil.getDateFromStr(cutOffDateString);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date date2 = calendar.getTime();
		System.out.println(DateConvertUtil.date2String(date2, "yyyy-MM-dd HH:mm:ss"));
		String strCutoffDate = DateConvertUtil.date2String(date2, "yyyy-MM-dd HH:mm:ss");
		List<Long> customerIdList = new ArrayList<Long>();
		if (statementParamsDto.getIsAll()) {
			String sql = "SELECT  m.customer_id as customerId,m.academy_no as academyId,CONCAT(cp.salutation,' ',cp.given_name,' ',cp.surname) as memberName,"
					+ " (CASE m.member_type WHEN 'IPM' THEN 'Individual' WHEN 'CPM' THEN 'Corporate' ELSE '' END) as memberType,mcbh.recal_balance as closingBalance "
					+ " from member_cashvalue_bal_history mcbh ,customer_profile cp, member m "
					+ " where mcbh.customer_id = m.customer_id and cp.customer_id = m.customer_id "
					+ "  and mcbh.cutoff_date = ?  order by customerId";

			List<StatementDto> statementDtos = memberCashvalueBalHistoryDao.getDtoBySql(sql,
					Arrays.asList(strCutoffDate), StatementDto.class);
			for (StatementDto dto : statementDtos) {
				customerIdList.add(dto.getCustomerId());
			}
			statementParamsDto.setCustomerIds(customerIdList.toArray(new Long[customerIdList.size()]));
		}
		Long[] customerIds = statementParamsDto.getCustomerIds();
		BatchSendStatementHd statementHd = null;
		if (statementParamsDto.isSent()) {
			statementHd = recordStatmentSendBatch(statementParamsDto != null ? statementParamsDto.getUserId() : null,
					date);
		}
		for (int k = 0; k < customerIds.length; k++) {
			Long customerId = customerIds[k];
			CustomerStatementBaseInfoDto statementInfo = getCustomerStatementBaseInfoDto(customerId, null, null);

			String basePath = appProps.getProperty("pdf.server.report");// /opt/file-server/report/statement
																		// or
																		// D:\\fileServer\\report\\statement
			String strMonth = String.valueOf(mon);
			if (mon < 10) {
				strMonth = "0" + month;
			}

			String fileName ="Patron Account Statement_" + customerId + "_" + year + strMonth + ".pdf";
			InputStream inputStream = null;
			try {
				inputStream = new FileInputStream(basePath+File.separatorChar+fileName);

				if (statementParamsDto.isSent()) {
					MessageTemplate template = messageTemplateDao
							.getTemplateByFunctionId(Constant.TEMPLATE_ID_MONTHLY_STATEMENT);

					List<String> fileNameList = new ArrayList<String>();
					fileNameList.add("Patron Account Statement - " + statementInfo.getMemberName() + ".pdf");
					List<String> mineTypeList = new ArrayList<String>();
					mineTypeList.add("application/pdf");
					List<byte[]> bytesList = new ArrayList<byte[]>();
					// bytesList.add(out.toByteArray());
					bytesList.add(IOUtils.toByteArray(inputStream));
					String content = template.getContentHtml();
					content = content.replaceAll(MessageTemplateParams.UserName.getParam(),
							statementInfo.getMemberName());
					String subject = template.getMessageSubject();
					String email = statementInfo.getEmail();
					String userId = statementParamsDto.getUserId();

					CustomerEmailContent customerEmailContent = new CustomerEmailContent();

					customerEmailContent.setContent(content);
					customerEmailContent.setRecipientCustomerId(customerId.toString());
					customerEmailContent.setRecipientEmail(email);
//					customerEmailContent.setSendDate(new Date());
					customerEmailContent.setSenderUserId(userId);
					customerEmailContent.setSubject(subject);
					customerEmailContent.setStatus(EmailStatus.PND.name());
//					customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_ACCOUNT);
					customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_BATCH);
					
					String sendId = (String) customerEmailContentDao.save(customerEmailContent);
					customerEmailContent.setSendId(sendId);

					CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
					customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
					customerEmailAttach.setAttachmentName(fileNameList.get(0));
					customerEmailAttach.setAttachmentPath(basePath+File.separatorChar+fileName);
					
					customerEmailAttachDao.save(customerEmailAttach);
					recordStatmentSendBatchList(userId, Long.valueOf(sendId), customerId, statementHd);
					/***
					 * SGG-3109
					 * auto send mail job
					 */
//					mailThreadService.sendWithResponse(statementHd.getBatchId(), customerEmailContent, bytesList,
//							mineTypeList, fileNameList);

					responseResult.initResult(GTAError.Success.SUCCESS);
				} else {
					responseResult.initResult(GTAError.Success.SUCCESS, IOUtils.toByteArray(inputStream));
				}

				// if (null!=out) {
				// out.flush();
				// out.close();
				// }
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new GTACommonException(GTAError.StatementError.FILE_NOT_EXIST);
			} catch (Exception e) {
				e.printStackTrace();
				throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			} finally {

				if (null != inputStream) {
					inputStream.close();
				}
			}

		}
		return responseResult;
	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseResult getStatementPDFBySearch(StatementParamsDto statementParamsDto, List<StatementDto> list)
			throws Exception {
		String year = statementParamsDto.getYear();
		String month = statementParamsDto.getMonth();
		// Long customerId = statementParamsDto.getCustomerIds()[0];
		String cutOffDateString = year + "-" + month + "-01 23:59:59";
		int mon = Integer.parseInt(month);
		if (mon < 10) {
			cutOffDateString = year + "-0" + month + "-01 23:59:59";
		}
		Date date = DateConvertUtil.getDateFromStr(cutOffDateString);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		List<Long> customerIdList = new ArrayList<Long>();
		if (statementParamsDto.getIsAll()) {
			for (StatementDto dto : list) {
				customerIdList.add(dto.getCustomerId());
			}
			statementParamsDto.setCustomerIds(customerIdList.toArray(new Long[customerIdList.size()]));
		}
		Long[] customerIds = statementParamsDto.getCustomerIds();
		BatchSendStatementHd statementHd = null;
		if (statementParamsDto.isSent()) {
			statementHd = recordStatmentSendBatch(statementParamsDto != null ? statementParamsDto.getUserId() : null,
					date);
		}
		for (int k = 0; k < customerIds.length; k++) {
			Long customerId = customerIds[k];
			CustomerStatementBaseInfoDto statementInfo = getCustomerStatementBaseInfoDto(customerId, null, null);

			String basePath = appProps.getProperty("pdf.server.report");// /opt/file-server/report/statement
																		// or
																		// D:\\fileServer\\report\\statement
			String strMonth = String.valueOf(mon);
			if (mon < 10) {
				strMonth = "0" + month;
			}

			String fileName = "Patron Account Statement_" + customerId + "_" + year + strMonth + ".pdf";
			InputStream inputStream = null;
			try {
				inputStream = new FileInputStream(basePath+File.separatorChar+fileName);

				if (statementParamsDto.isSent()) {
					MessageTemplate template = messageTemplateDao
							.getTemplateByFunctionId(Constant.TEMPLATE_ID_MONTHLY_STATEMENT);

					List<String> fileNameList = new ArrayList<String>();
					fileNameList.add("Patron Account Statement - " + statementInfo.getMemberName() + ".pdf");
					List<String> mineTypeList = new ArrayList<String>();
					mineTypeList.add("application/pdf");
					List<byte[]> bytesList = new ArrayList<byte[]>();
					// bytesList.add(out.toByteArray());
					bytesList.add(IOUtils.toByteArray(inputStream));
					String content = template.getContentHtml();
					content = content.replaceAll(MessageTemplateParams.UserName.getParam(),
							statementInfo.getMemberName());
					String subject = template.getMessageSubject();
					String email = statementInfo.getEmail();
					String userId = statementParamsDto.getUserId();

					CustomerEmailContent customerEmailContent = new CustomerEmailContent();

					customerEmailContent.setContent(content);
					customerEmailContent.setRecipientCustomerId(customerId.toString());
					customerEmailContent.setRecipientEmail(email);
//					customerEmailContent.setSendDate(new Date());
					customerEmailContent.setSenderUserId(userId);
					customerEmailContent.setSubject(subject);
					customerEmailContent.setStatus(EmailStatus.PND.name());
					customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_ACCOUNT);
					String sendId = (String) customerEmailContentDao.save(customerEmailContent);
					customerEmailContent.setSendId(sendId);

					CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
					customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
					customerEmailAttach.setAttachmentPath(basePath+File.separatorChar+fileName);
					customerEmailAttach.setAttachmentName(fileName);
					customerEmailAttachDao.save(customerEmailAttach);
					recordStatmentSendBatchList(userId, Long.valueOf(sendId), customerId, statementHd);
//					mailThreadService.sendWithResponse(statementHd.getBatchId(), customerEmailContent, bytesList,
//							mineTypeList, fileNameList);

					responseResult.initResult(GTAError.Success.SUCCESS);
				} else {
					responseResult.initResult(GTAError.Success.SUCCESS, IOUtils.toByteArray(inputStream));
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new GTACommonException(GTAError.StatementError.FILE_NOT_EXIST);
			} catch (Exception e) {
				e.printStackTrace();
				throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			} finally {

				if (null != inputStream) {
					inputStream.close();
				}
			}

		}
		return responseResult;
	}

	private void addBalanceHistoryTable(Document document, BigDecimal currentClosingBalance, Long customerId,
			Date startDate) {
		Font font = new Font(Font.TIMES_ROMAN, 10);
		PdfPTable balanceTable = new PdfPTable(6);
		balanceTable.setSpacingBefore(15f);
		PdfPCell balanceCell = new PdfPCell();
		balanceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		balanceCell.setPhrase(new Phrase("Within 30 Days", font));
		balanceTable.addCell(balanceCell);

		balanceCell.setPhrase(new Phrase("31-60 Days", font));
		balanceTable.addCell(balanceCell);

		balanceCell.setPhrase(new Phrase("61-90 Days", font));
		balanceTable.addCell(balanceCell);

		balanceCell.setPhrase(new Phrase("91-120 Days", font));
		balanceTable.addCell(balanceCell);

		balanceCell.setPhrase(new Phrase("Over 120 Days", font));
		balanceTable.addCell(balanceCell);

		balanceCell.setPhrase(new Phrase("Total Balance Due", font));
		balanceTable.addCell(balanceCell);

		BigDecimal currentTotalTopup = getTotalTopupAmount(customerId, 0, startDate);
		BigDecimal currentTotalSpending = getTotalSpendingAmount(customerId, 0, startDate);

		BigDecimal last2MonthTotalTopup = getTotalTopupAmount(customerId, 1, startDate);
		BigDecimal last2MonthTotalSpending = getTotalSpendingAmount(customerId, 1, startDate);

		BigDecimal last3MonthTotalTopup = getTotalTopupAmount(customerId, 2, startDate);
		BigDecimal last3MonthTotalSpending = getTotalSpendingAmount(customerId, 2, startDate);

		BigDecimal last4MonthTotalTopup = getTotalTopupAmount(customerId, 3, startDate);
		BigDecimal last4MonthTotalSpending = getTotalSpendingAmount(customerId, 3, startDate);

		BigDecimal last5MonthClosingBalance = getClosingBalance(customerId, 4, startDate);// Over
																							// 120
																							// Days

		BigDecimal totalTopUpAmount = currentTotalTopup.add(last2MonthTotalTopup).add(last3MonthTotalTopup)
				.add(last4MonthTotalTopup);
		BigDecimal last5MonthRemainBalance = totalTopUpAmount.add(last5MonthClosingBalance);

		// added by Kaster 20160310
		BigDecimal last4MonthRemainBalance = BigDecimal.ZERO;
		BigDecimal last3MonthRemainBalance = BigDecimal.ZERO;
		BigDecimal last2MonthRemainBalance = BigDecimal.ZERO;
		BigDecimal currentMonthRemainBalance = BigDecimal.ZERO;

		// 充值4个月还无法还清第五个月以前欠的钱，那么最近四个月的花费都是欠款了。
		if (last5MonthRemainBalance.compareTo(BigDecimal.ZERO) <= 0) {
			last4MonthRemainBalance = BigDecimal.ZERO.subtract(last4MonthTotalSpending);
			last3MonthRemainBalance = BigDecimal.ZERO.subtract(last3MonthTotalSpending);
			last2MonthRemainBalance = BigDecimal.ZERO.subtract(last2MonthTotalSpending);
			currentMonthRemainBalance = BigDecimal.ZERO.subtract(currentTotalSpending);
		} else {
			// 如果第五个月以前的欠款还清了，那么就还第四个月的欠款。
			last4MonthRemainBalance = last5MonthRemainBalance.subtract(last4MonthTotalSpending);
			// 没还清第四个月的，那么最近三个月的花费都是欠款。
			if (last4MonthRemainBalance.compareTo(BigDecimal.ZERO) <= 0) {
				last3MonthRemainBalance = BigDecimal.ZERO.subtract(last3MonthTotalSpending);
				last2MonthRemainBalance = BigDecimal.ZERO.subtract(last2MonthTotalSpending);
				currentMonthRemainBalance = BigDecimal.ZERO.subtract(currentTotalSpending);
			} else {
				// 如果第四个月的欠款还清了，那么就还第三个月的欠款。
				last3MonthRemainBalance = last4MonthRemainBalance.subtract(last3MonthTotalSpending);
				// 没还清第三个月的，那么最近两个月的花费都是欠款。
				if (last3MonthRemainBalance.compareTo(BigDecimal.ZERO) <= 0) {
					last2MonthRemainBalance = BigDecimal.ZERO.subtract(last2MonthTotalSpending);
					currentMonthRemainBalance = BigDecimal.ZERO.subtract(currentTotalSpending);
				} else {
					// 如果第三个月的欠款还清了，那么就还第二个月的欠款。
					last2MonthRemainBalance = last3MonthRemainBalance.subtract(last2MonthTotalSpending);
					// 没还清第二个月的，那么最近一个月（上个月）的花费就是欠款。
					if (last2MonthRemainBalance.compareTo(BigDecimal.ZERO) <= 0) {
						currentMonthRemainBalance = BigDecimal.ZERO.subtract(currentTotalSpending);
					} else {
						// 如果第二个月的欠款还清了，那么就还上个月的欠款。
						currentMonthRemainBalance = last2MonthRemainBalance.subtract(currentTotalSpending);
					}
				}
			}
		}

		balanceCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		/***
		 * SGG-3618
		 * @Sam say :change old currentMonthRemainBalance to currentClosingBalance 
		 * 
		if (currentClosingBalance.compareTo(BigDecimal.ZERO) >= 0) {
			balanceCell.setPhrase(new Phrase("-", font));
		} else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(currentClosingBalance), font));
		}
		*/
		/***
		 * SGG-3772
		 * 2017-08-04
		 */
		if (currentMonthRemainBalance.compareTo(BigDecimal.ZERO) >= 0) {
			balanceCell.setPhrase(new Phrase("-", font));
		} else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(currentMonthRemainBalance), font));
		}
		
		
		balanceTable.addCell(balanceCell);
		if (last2MonthRemainBalance.compareTo(BigDecimal.ZERO) >= 0) {
			balanceCell.setPhrase(new Phrase("-", font));
		} else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(last2MonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);

		if (last3MonthRemainBalance.compareTo(BigDecimal.ZERO) >= 0) {
			balanceCell.setPhrase(new Phrase("-", font));
		} else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(last3MonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);

		if (last4MonthRemainBalance.compareTo(BigDecimal.ZERO) >= 0) {
			balanceCell.setPhrase(new Phrase("-", font));
		} else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(last4MonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);

		if (last5MonthRemainBalance.compareTo(BigDecimal.ZERO) >= 0) {
			balanceCell.setPhrase(new Phrase("-", font));
		} else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(last5MonthRemainBalance), font));
		}
		balanceTable.addCell(balanceCell);

		if (currentClosingBalance.compareTo(BigDecimal.ZERO) >= 0) {
			balanceCell.setPhrase(new Phrase("-", font));
		} else {
			balanceCell.setPhrase(new Phrase(getFormatAmount(currentClosingBalance), font));
		}
		balanceTable.addCell(balanceCell);

		try {
			document.add(balanceTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Calculate member's virtual account spending only for one month
	 * 
	 * @param customerId
	 * @param monthCountAgo
	 * @param startDate
	 * @return
	 */
	@Transactional
	public BigDecimal getTotalSpendingAmount(Long customerId, int monthCountAgo, Date startDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.MONTH, -monthCountAgo);
		String startDateString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String endDateString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");

		// SAM 20161007 changed the SQL, the original SQL will produce duplicate record
		// Count successful cash value payment and the refund of cash value
/*		String totalSpendingSql = "SELECT sum(trans.paid_amount) as totalSpending from customer_order_trans trans, customer_order_hd hd, customer_order_det det, member m"
				+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no  and (DATE(trans.transaction_timestamp) BETWEEN '"
				+ startDateString + "' and '" + endDateString + "')"
				+ " and ((det.item_no != 'CVT0000001' and det.item_no != 'SCVR00000001' and trans.`status` = 'SUC' and trans.payment_method_code = '"
				+ PaymentMethod.CASHVALUE.name() + "') OR (det.item_no = 'CVR00000001' AND trans.`status` = 'RFU'))"
				+ " and hd.customer_id = m.customer_id and (m.customer_id = ? or m.superior_member_id = ? ) ";*/

		//add the start date restriction
		String totalSpendingSql = 
				"SELECT sum(trans.paid_amount) as totalSpending from customer_order_trans trans, customer_order_hd hd, member m"
				+ " where trans.order_no = hd.order_no and  (DATE(trans.transaction_timestamp) BETWEEN '"
				+ startDateString + "' and '" + endDateString + "')"
				+ " and (( trans.payment_method_code = '"
				+ PaymentMethod.CASHVALUE.name() + "') )"
				+ " and hd.customer_id = m.customer_id and (m.customer_id = ? or m.superior_member_id = ? ) "		
		        + " and (trans.`status` = 'SUC'"
		        +       " and not exists (select 1 from customer_order_det det "
		        +                    " where det.order_no = hd.order_no "  
		        +                    " and det.item_no in ('CVT0000001','SCVR00000001')) "
		        +       " or trans.`status` = 'RFU' "
		        +           " AND exists (select 1 from customer_order_det det "
		        +                   " where det.order_no = hd.order_no "
		        +                   "  and det.item_no = 'CVR00000001') )";

		
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(customerId);
		BigDecimal totalSpending = (BigDecimal) customerTransactionDao.getUniqueBySQL(totalSpendingSql, param);
		BigDecimal sumSpending = new BigDecimal(null == totalSpending ? 0 : totalSpending.doubleValue());
		return sumSpending;
	}

	/**
	 * Calculate member's virtual account top up only for one month
	 * 
	 * @param customerId
	 * @param monthCountAgo
	 * @param startDate
	 * @return
	 */
	@Transactional
	public BigDecimal getTotalTopupAmount(Long customerId, int monthCountAgo, Date startDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.MONTH, -monthCountAgo);
		String startDateString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String endDateString = DateConvertUtil.date2String(calendar.getTime(), "yyyy-MM-dd");

		// SAM 20161007 changed the SQL, the original one will produce duplicate record and not cal the balance from day 1
		// There are three ways (CVT0000001,SRR00000001,SCVR00000001) top up to
		// member's virtual account and regardless of the payment method.
/*		String totalTopUpSql = "SELECT sum(trans.paid_amount) as totalTopUp from customer_order_trans trans, customer_order_hd hd, customer_order_det det, member m"
				+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no and (DATE(trans.transaction_timestamp) BETWEEN '"
				+ startDateString + "' and '" + endDateString + "') "
				+ " and ((trans.`status` = 'SUC' and det.item_no = 'CVT0000001') OR (trans.`status` = 'RFU' AND det.item_no = 'SRR00000001') OR (trans.`status` = 'SUC' AND det.item_no = 'SCVR00000001')) and hd.customer_id = m.customer_id "
				+ " and (m.customer_id = ? or m.superior_member_id = ?) ";*/

		// add start date restriction
		String totalTopUpSql = "SELECT sum(trans.paid_amount) as totalTopUp from customer_order_trans trans, customer_order_hd hd, member m"
				+ " where trans.order_no = hd.order_no and (DATE(trans.transaction_timestamp) BETWEEN '"
				+ startDateString + "' and '" + endDateString + "') "
				+ " and hd.customer_id = m.customer_id "
				+ " and (m.customer_id = ? or m.superior_member_id = ?) "		
		        + " and exists (select 1 from customer_order_det det "
		        +		" where det.order_no = hd.order_no " 
		        +	       " and ((trans.`status` = 'SUC' and det.item_no = 'CVT0000001' ) " 
		        + 		    " OR (trans.`status` = 'RFU' AND det.item_no = 'SRR00000001') " 
		        +		    " OR (trans.`status` = 'SUC' AND det.item_no = 'SCVR00000001') "
		        +           " OR (trans.`status` = 'VOID' AND det.item_no = 'PMS00000001') "
		        +           " OR (trans.`status` = 'VOID' AND det.item_no = 'MMS00000001') "
		        +           " OR (trans.`status` = 'VOID' AND det.item_no = 'RST00000001'))) "; 
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(customerId);
		BigDecimal totalTopup = (BigDecimal) customerTransactionDao.getUniqueBySQL(totalTopUpSql, param);
		BigDecimal sumTopup = new BigDecimal(null == totalTopup ? 0 : totalTopup.doubleValue());
		return sumTopup;
	}

	@Transactional
	public BigDecimal getClosingBalance(Long customerId, int monthCountAgo, Date startDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.MONTH, -monthCountAgo);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date cutoffDate = calendar.getTime();
		String strCutoffDate = DateConvertUtil.date2String(cutoffDate, "yyyy-MM-dd") + Constant.REPORT_CUTOFF_DATE_HMS;
		MemberCashvalueBalHistory memberCashvalueBalHistory = memberCashvalueBalHistoryDao.getUniqueByCols(
				MemberCashvalueBalHistory.class, new String[] { "customerId", "cutoffDate" },
				new Serializable[] { customerId, DateConvertUtil.getDateFromStr(strCutoffDate) });

		return memberCashvalueBalHistory == null ? BigDecimal.ZERO : memberCashvalueBalHistory.getRecalBalance();
	}

	@SuppressWarnings("unchecked")
	private BatchSendStatementHd recordStatmentSendBatch(String userId, Date statementMonth) {
		BatchSendStatementHd statementHd = new BatchSendStatementHd();
		statementHd.setCreateBy(userId);
		statementHd.setCreateDate(new Date());
		statementHd.setErrorCount(Long.valueOf(0));
		statementHd.setStatementMonth(statementMonth);
		Long batchId = (Long) deliveryRecordDao.save(statementHd);
		statementHd.setBatchId(batchId);
		return statementHd;
	}

	@SuppressWarnings("unchecked")
	private void recordStatmentSendBatchList(String userId, Long emailSendId, Long customerId,
			BatchSendStatementHd statementHd) {
		BatchSendStatementList statementList = new BatchSendStatementList();
		statementList.setEmailSendId(emailSendId);
		statementList.setBatchSendStatementHd(statementHd);
		statementList.setCreateBy(userId);
		statementList.setCreateDate(new Date());
		MemberCashvalue memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
		statementList.setCashvalueBalance(memberCashvalue != null ? memberCashvalue.getAvailableBalance() : null);
		deliveryRecordDao.save(statementList);

	}

	@Transactional
	public void addSummaryTable(Document document, Long customerId, String startDateString, String endDateString) {
		try {
			Font contentFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
			Color color = new Color(140, 0, 30);
			Font tableTitleFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, Color.white);
			List<Member> allMembers = new ArrayList<Member>();
			Member primaryMember = memberDao.get(Member.class, customerId);
			allMembers.add(primaryMember);
			List<Member> dependentMembers = memberDao.getListMemberBySuperiorId(customerId);
			allMembers.addAll(dependentMembers);
			for (Member member : allMembers) {
				PdfPTable topupTotalTable = new PdfPTable(5);
				PdfPCell topupTotalCell = new PdfPCell();

				CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, member.getCustomerId());

				// Top up total
				String totalTopUpSql = "SELECT sum(trans.paid_amount) as totalTopUp, item_no from customer_order_trans trans, customer_order_hd hd, customer_order_det det "
						+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no "
						+ " and (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' and '"
						+ endDateString + "') "
						// top up, cash value refund, service refund, cash value
						// shall be counted in top up total,also include void cash value from oasis
						+ "AND ((trans.status = 'SUC' AND det.item_no = 'CVT0000001') OR (trans.status = 'RFU' AND det.item_no = 'SRR00000001') "
						+ " OR (trans.status = 'RFU' AND det.item_no = 'CVR00000001')  OR (trans.status = 'SUC' AND det.item_no = 'SCVD00000001') "
						+ " OR (trans.status = 'SUC' AND det.item_no = 'SCVR00000001') OR (trans.`status` = 'VOID' AND det.item_no = 'PMS00000001') "
						+ " OR (trans.`status` = 'VOID' AND det.item_no = 'MMS00000001') "
						+ " OR (trans.`status` = 'VOID' AND det.item_no = 'RST00000001')) "
						+ " and hd.customer_id = ? " + " GROUP BY item_no ";

				// Spending total
				/***
				 * customer_order_det may more than one record. If join the table with customer_order_trans, it will produce multiple transaction. So it should be used exists
				 
				String totalSpendingSql = "SELECT sum(trans.paid_amount) as totalSpending from customer_order_trans trans, customer_order_hd hd, customer_order_det det"
						+ " where trans.order_no = hd.order_no and det.order_no = hd.order_no "
						+ " and (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' and '"+ endDateString + "') "
						+ " AND LEFT (det.item_no, 3) != 'SRV' AND LEFT (det.item_no, 5) != 'RENEW'" 
						+ " AND LEFT (det.item_no, 3) != 'CVR' AND LEFT (det.item_no, 3) != 'CVT' AND LEFT (det.item_no, 3) != 'SCV' "
						+ " AND trans.`status` = 'SUC' and hd.customer_id = ? ";
				*/
				 String totalSpendingSql=" SELECT SUM(trans.paid_amount) AS totalSpending " 
						+"  FROM customer_order_trans trans, customer_order_hd hd "
						+"  WHERE trans.order_no = hd.order_no  "
						+ " and (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' and '"+ endDateString + "') "
						+"  AND EXISTS (SELECT 1 FROM customer_order_det det "
						+"  WHERE det.order_no = hd.order_no "
						+"  AND LEFT (det.item_no, 3) != 'SRV' AND LEFT (det.item_no, 5) != 'RENEW'" 
						+"  AND LEFT (det.item_no, 3) != 'CVR' AND LEFT (det.item_no, 3) != 'CVT' AND LEFT (det.item_no, 3) != 'SCV')" 
						+"  AND trans.status = 'SUC' AND hd.customer_id = ?";
				/*
				 * Display service refund list String refundSql =
				 * "SELECT trans.transaction_no as transactionNo,trans.paid_amount as paidAmount,trans.transaction_timestamp as transactionTimestamp from customer_order_trans trans, customer_order_hd hd, customer_order_det det "
				 * +
				 * " where trans.order_no = hd.order_no and det.order_no = hd.order_no  "
				 * + " and (DATE(trans.transaction_timestamp) BETWEEN  '" +
				 * startDateString + "' and '" + endDateString + "') " +
				 * " and trans.`status` = 'RFU' AND det.item_no = 'SRR00000001' and hd.customer_id = ? "
				 * ;
				 * 
				 * String countSql = "SELECT COUNT(tt.transactionNo) from (" +
				 * refundSql + ") tt";
				 */

				// count by java instead of by sql
				BigDecimal sumTopup = new BigDecimal(0);
				BigDecimal sumServiceRefund = new BigDecimal(0);
				BigDecimal sumCashValueRefund = new BigDecimal(0);
				BigDecimal sumCashValueAdjustment = new BigDecimal(0);
				BigDecimal sumPaymentVoid = new BigDecimal(0);
				List totals = customerTransactionDao.getCurrentSession().createSQLQuery(totalTopUpSql)
						.setLong(0, member.getCustomerId()).list();
				for (int i = 0; i < totals.size(); i++) {
					Object temp[] = (Object[]) totals.get(i);
					/*
					 * CVT0000001 -- top up 
					 * SRR00000001 -- service refund
					 * CVR00000001 -- cash value refund
					 * SCVD00000001-- debit
					 * SCVR00000001-- credit
					 * PMS00000001 -- pms void
					 * RST00000001 -- pos void
					 */
					if ("CVT0000001".equalsIgnoreCase(temp[1].toString())) {
						sumTopup = sumTopup.add(new BigDecimal(temp[0].toString()));
					} else if ("SRR00000001".equalsIgnoreCase(temp[1].toString())) {
						sumTopup = sumTopup.add(new BigDecimal(temp[0].toString()));
						sumServiceRefund = sumServiceRefund.add(new BigDecimal(temp[0].toString()));
					} else if ("CVR00000001".equalsIgnoreCase(temp[1].toString())) {
						sumTopup = sumTopup.subtract(new BigDecimal(temp[0].toString()));
						sumCashValueRefund = sumCashValueRefund.add(new BigDecimal(temp[0].toString()));
					} else if ("SCVD00000001".equalsIgnoreCase(temp[1].toString())) {
						sumTopup = sumTopup.subtract(new BigDecimal(temp[0].toString()));
						sumCashValueAdjustment = sumCashValueAdjustment.subtract(new BigDecimal(temp[0].toString()));
					} else if ("SCVR00000001".equalsIgnoreCase(temp[1].toString())) {
						sumTopup = sumTopup.add(new BigDecimal(temp[0].toString()));
						sumCashValueAdjustment = sumCashValueAdjustment.add(new BigDecimal(temp[0].toString()));
					} else if ("PMS00000001".equalsIgnoreCase(temp[1].toString())) {
						sumTopup = sumTopup.add(new BigDecimal(temp[0].toString()));
						sumPaymentVoid = sumPaymentVoid.add(new BigDecimal(temp[0].toString()));
					} else if ("RST00000001".equalsIgnoreCase(temp[1].toString())) {
						sumTopup = sumTopup.add(new BigDecimal(temp[0].toString()));
						sumPaymentVoid = sumPaymentVoid.add(new BigDecimal(temp[0].toString()));
					}
				}

				// BigDecimal sumTopup = new BigDecimal(null == totalTopup ? 0 :
				// totalTopup.doubleValue());

				BigDecimal totalSpending = (BigDecimal) customerTransactionDao.getUniqueBySQL(totalSpendingSql,
						Arrays.asList((Serializable) member.getCustomerId()));
				BigDecimal sumSpending = new BigDecimal(null == totalSpending ? 0 : totalSpending.doubleValue());

				StringBuilder builder = new StringBuilder();

				if (sumServiceRefund.compareTo(BigDecimal.ZERO) > 0) {
					builder.append("*** HK$" + getFormatAmount(sumServiceRefund) + " \n Refund to cash value");
					builder.append("\n");
				}
				if (sumCashValueRefund.compareTo(BigDecimal.ZERO) > 0) {
					builder.append("*** HK$" + getFormatAmount(sumCashValueRefund) + " \n Cash Withdrawal");
					builder.append("\n");
				}
				if (sumCashValueAdjustment.compareTo(BigDecimal.ZERO) != 0) {
					builder.append("*** HK$" + getFormatAmount(sumCashValueAdjustment) + " \n Cash Value Adjustment");
					builder.append("\n");
				}
				if (sumPaymentVoid.compareTo(BigDecimal.ZERO) != 0) {
					builder.append("*** HK$" + getFormatAmount(sumPaymentVoid) + " \n Cash Value Payment Void");
					builder.append("\n");
				}

				if (com.sinodynamic.hkgta.util.constant.MemberType.IPM.getType().equals(member.getMemberType())
						|| com.sinodynamic.hkgta.util.constant.MemberType.CPM.getType()
								.equals(member.getMemberType())) {
					topupTotalCell.setPhrase(new Phrase("Cash Value Top Up Total(HK$) of Primary Patron - "
							+ customerProfile.getGivenName() + " " + customerProfile.getSurname(), tableTitleFont));
				} else {
					topupTotalCell
							.setPhrase(new Phrase(
									"Cash Value Top Up Total(HK$) of Dependent Patron - "
											+ customerProfile.getGivenName() + " " + customerProfile.getSurname(),
									tableTitleFont));
				}
				topupTotalCell.setBackgroundColor(color);
				topupTotalTable.addCell(topupTotalCell);

				topupTotalCell.setBackgroundColor(Color.white);
				topupTotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				topupTotalCell.setPhrase(new Phrase(getFormatAmount(sumTopup), contentFont));
				topupTotalTable.addCell(topupTotalCell);

				topupTotalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				topupTotalCell.setBorder(0);
				topupTotalCell.setPhrase(new Phrase(builder.toString(), contentFont));
				topupTotalTable.addCell(topupTotalCell);
				topupTotalCell.setBorderWidthBottom(0.5f);
				topupTotalCell.setBorderWidthLeft(0.5f);
				topupTotalCell.setBorderWidthRight(0.5f);
				topupTotalCell.setBorderWidthTop(0.5f);
				topupTotalCell.setBackgroundColor(color);
				if (com.sinodynamic.hkgta.util.constant.MemberType.IPM.getType().equals(member.getMemberType())
						|| com.sinodynamic.hkgta.util.constant.MemberType.CPM.getType()
								.equals(member.getMemberType())) {
					topupTotalCell.setPhrase(new Phrase(
							"Consumption Total(HK$)(All Payment Type) of Primary Patron - "
									+ customerProfile.getGivenName() + " " + customerProfile.getSurname(),
							tableTitleFont));
				} else {
					topupTotalCell.setPhrase(new Phrase(
							"Consumption Total(HK$)(All Payment Type) of Dependent Patron - "
									+ customerProfile.getGivenName() + " " + customerProfile.getSurname(),
							tableTitleFont));
				}
				topupTotalTable.addCell(topupTotalCell);
				topupTotalCell.setBackgroundColor(Color.white);
				topupTotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				topupTotalCell.setPhrase(new Phrase(getFormatAmount(sumSpending), contentFont));
				topupTotalCell.setBorderWidthBottom(0.5f);
				topupTotalCell.setBorderWidthLeft(0);
				topupTotalCell.setBorderWidthRight(0.5f);
				topupTotalCell.setBorderWidthTop(0.5f);
				topupTotalTable.addCell(topupTotalCell);

				topupTotalTable.setSpacingAfter(20f);
				
				/***
				 * SGG-3872
				 * if member type is depentent and status is inacive and  amount is zero no show  
				 */
				if(com.sinodynamic.hkgta.util.constant.MemberType.IDM.getType().equals(member.getMemberType())||
						com.sinodynamic.hkgta.util.constant.MemberType.CDM.getType().equals(member.getMemberType()))
				{
					if(Status.ACT.equals(member.getStatus())||sumSpending.compareTo(new BigDecimal(0))>0)
					{
						document.add(topupTotalTable);	
					}
					
				}else{
					document.add(topupTotalTable);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Calculate other all consumption Excluding enrollment and renew and
	 * virtual account spending (cash value)
	 * 
	 * @param document
	 * @param customerId
	 * @param startDateString
	 * @param endDateString
	 */
	@Transactional
	public void addOtherConsumptionTable(Document document, Long customerId, String startDateString,
			String endDateString) {
		try {
			Font font = new Font(Font.TIMES_ROMAN, 10);
			Font firstColumnFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
			Color color = new Color(140, 0, 30);
			Font tableTitleFont = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, Color.white);
			List<String[]> pmOtherConsumptionList = new ArrayList<String[]>();
			pmOtherConsumptionList
					.add(new String[] { "Post Date", "Ref.No", "Description", "Payment Type", "Amount(HK$)" });

			String otherPaymenetSql = "SELECT DISTINCT trans.transaction_timestamp as transactionDate,trans.transaction_no as 	invoiceNo, "
					+ " (CASE pos.item_catagory WHEN 'REFUND'  THEN (CASE det.item_no WHEN 'CVR00000001' THEN 'Cash Value Refund' ELSE 'Service Refund' END) WHEN 'MMS' THEN 'Wellness Center' ELSE pos.item_catagory END) as category, "
					+ " pos.description as description, "
					+ " (CASE trans.payment_method_code WHEN '"+PaymentMethod.VISA.name()+"' THEN '"+PaymentMethod.VISA.getDesc()+"' "
						+ "  WHEN '"+PaymentMethod.MASTER.name()+"' THEN '"+PaymentMethod.MASTER.getDesc()+"'"
						+ "  WHEN '"+PaymentMethod.CASH.name()+"' THEN '"+PaymentMethod.CASH.getDesc()+"'"
						+ "  WHEN '"+PaymentMethod.BT.name()+"' THEN '"+PaymentMethod.BT.getDesc()+"' "
						+ "  when '"+PaymentMethod.CHRGPOST.name() + "' then '"+PaymentMethod.CHRGPOST.getDesc()+"' "
						+ "  WHEN '"+PaymentMethod.CHEQUE.name()+"' THEN '"+PaymentMethod.CHEQUE.getDesc()+"' "
						+ "  WHEN '"+ PaymentMethod.CASHVALUE.name()+ "' THEN '"+PaymentMethod.CASHVALUE.getDesc()+"' "
					    + "  WHEN '"+PaymentMethod.VAC.name()+"' THEN 'Debit via Bank' "
					    + "  WHEN '"+PaymentMethod.CUP.name()+"' THEN '"+PaymentMethod.CUP.getDesc()+"'"
					    + "  WHEN '"+PaymentMethod.AMEX.name()+"' THEN '"+PaymentMethod.AMEX.getDesc()+"'"
					    + "  WHEN '"+PaymentMethod.DDI.name()+"' THEN '"+PaymentMethod.DDI.getDesc()+"' ELSE 'Other' END) as paymentType, "
					
					+ "  trans.paid_amount as amount,det.item_no as itemNo " + " from customer_order_trans trans "
					+ " INNER JOIN customer_order_hd hd ON trans.order_no = hd.order_no "
					+ " INNER JOIN customer_order_det det ON det.order_no = hd.order_no "
					+ " INNER JOIN pos_service_item_price pos ON det.item_no = pos.item_no "
					+ " WHERE  trans.`status` = 'SUC' " + " AND trans.payment_method_code != '"
					+ PaymentMethod.CASHVALUE.name()
					+ "' AND det.item_no != 'CVT0000001' AND det.item_no != 'SRR00000001' AND det.item_no != 'SCVR00000001' "
					+ " AND LEFT (det.item_no, 3) != 'SRV' AND LEFT (det.item_no, 5) != 'RENEW' "
					+ " AND (DATE(trans.transaction_timestamp) BETWEEN  '" + startDateString + "' AND '" + endDateString
					+ "') " + " AND hd.customer_id = ? ORDER BY trans.transaction_timestamp";

			List<Serializable> param1 = new ArrayList<Serializable>();
			param1.add(customerId);
			List<PatronAccountStatementDto> patronAccountStatementDtos = customerOrderTransDao
					.getDtoBySql(otherPaymenetSql, param1, PatronAccountStatementDto.class);
			for (PatronAccountStatementDto dto : patronAccountStatementDtos) {
				 /* christ add filter  dto.getAmount() is zero
				 * SGG-2474
				 */
				if(dto.getAmount().compareTo(BigDecimal.ZERO)!=0)
				{
					updatePatronAccountStatementDesc(dto);
					pmOtherConsumptionList
							.add(new String[] { DateConvertUtil.date2String(dto.getTransactionDate(), "dd/MM/yyyy"),
									dto.getInvoiceNo().toString(), dto.getDescription(), dto.getPaymentType(),
									getFormatAmount(dto.getAmount()) });	
				}
				 
				
			}
			if (pmOtherConsumptionList.size() < 2) {
				pmOtherConsumptionList.add(new String[] { " ", " ", " ", " ", " " });
			}
			PdfPTable pmOtherConsumptionTable = new PdfPTable(pmOtherConsumptionList.get(0).length);
			for (int i = 0; i < pmOtherConsumptionList.size(); i++) {
				String[] strings = pmOtherConsumptionList.get(i);
				for (int j = 0; j < strings.length; j++) {
					PdfPCell cell = new PdfPCell();
					if (i == 0) {
						cell.setBackgroundColor(color);
						cell.setPhrase(new Phrase(strings[j], tableTitleFont));
					} else {
						cell.setBackgroundColor(Color.white);
						cell.setPhrase(new Phrase(strings[j], font));
					}
					cell.setNoWrap(false);
					if (j == strings.length - 1) {
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					} else {
						cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					}
					pmOtherConsumptionTable.addCell(cell);
				}
			}
			pmOtherConsumptionTable.setSpacingAfter(20f);
			document.add(pmOtherConsumptionTable);

			List<Member> dependentMembers = memberDao.getListMemberBySuperiorId(customerId);
			if (null != dependentMembers && dependentMembers.size() > 0) {
				for (Member dependentMember : dependentMembers) {
					CustomerProfile dependentProfile = customerProfileDao.get(CustomerProfile.class,
							dependentMember.getCustomerId());
					List<String[]> dmOtherConsumptionList = new ArrayList<String[]>();
					dmOtherConsumptionList
							.add(new String[] { "Post Date", "Ref.No", "Description", "Payment Type", "Amount(HK$)" });

					List<Serializable> denParam = new ArrayList<Serializable>();
					denParam.add(dependentMember.getCustomerId());
					List<PatronAccountStatementDto> denPatronAccountStatementDtos = customerOrderTransDao
							.getDtoBySql(otherPaymenetSql, denParam, PatronAccountStatementDto.class);
					//table line content
					for (PatronAccountStatementDto dto : denPatronAccountStatementDtos) {
						updatePatronAccountStatementDesc(dto);
						dmOtherConsumptionList
								.add(new String[] { DateConvertUtil.date2String(dto.getTransactionDate(), "dd/MM/yyyy"),
										dto.getInvoiceNo().toString(), dto.getDescription(), dto.getPaymentType(),
										getFormatAmount(dto.getAmount()) });
					}
					if (dmOtherConsumptionList.size() < 2) {
						dmOtherConsumptionList.add(new String[] { " ", " ", " ", " ", " " });
					}
                   					
					PdfPTable dmOtherConsumptiontTable = new PdfPTable(dmOtherConsumptionList.get(0).length);
					for (int i = 0; i < dmOtherConsumptionList.size(); i++) {
						String[] strings = dmOtherConsumptionList.get(i);
						for (int j = 0; j < strings.length; j++) {
							PdfPCell cell = new PdfPCell();
							if (i == 0) {
								cell.setBackgroundColor(color);
								cell.setPhrase(new Phrase(strings[j], tableTitleFont));
							} else {
								cell.setBackgroundColor(Color.white);
								cell.setPhrase(new Phrase(strings[j], font));
							}
							cell.setNoWrap(false);
							if (j == strings.length - 1) {
								cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							} else {
								cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							}
							dmOtherConsumptiontTable.addCell(cell);
						}
					}
					dmOtherConsumptiontTable.setSpacingAfter(20f);
					
					//SGG-3872 inactive and have no transcation no show
					if(Status.ACT.name().equals(dependentMember.getStatus())||(null!=denPatronAccountStatementDtos&&denPatronAccountStatementDtos.size()>0))
					{
						Paragraph denOtherDetail = getParagraph("Dependent Patron - " + dependentProfile.getGivenName()
						+ " " + dependentProfile.getSurname(), firstColumnFont, 0f, 5f, 57f);
						document.add(denOtherDetail);//titile
						document.add(dmOtherConsumptiontTable); //content	
					}
					
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Transactional
	public void updatePatronAccountStatementDesc(PatronAccountStatementDto dto) {
		dto.setDescription(dto.getCategory());
		for (Constant.StatementDescItem item : Constant.StatementDescItem.values()) {
			if (item.name().equals(dto.getCategory())) {
				dto.setDescription(Constant.StatementDescItem.valueOf(dto.getCategory()).toString());
			}
		}
		
		if("RESTAURANT".equals(dto.getCategory())&&!StringUtils.isEmpty(dto.getRemark())){
			// POS:VariAsia, Chk#:000374
			if(dto.getRemark().startsWith("POS:") && dto.getRemark().contains(","))
			{
				dto.setDescription(dto.getRemark().split(",")[0].split(":")[1]);	
			}else{
				//VariAsia:#000374
				dto.setDescription(dto.getRemark().split(":")[0]);	
			}
		}
		if(Constant.PMSTHIRD_ITEM_NO.equals(dto.getItemNo()) || Constant.POSTHIRD_ITEM_NO.equals(dto.getItemNo())){
			if(Constant.Status.VOID.name().equals(dto.getStatus())){
				dto.setDescription(dto.getDescription() + " Void");
			}
		}
	}

	@Override
	@Transactional
	public byte[] getInvoiceReceipt(Long orderNo, String transactionNo, String receiptType) {
		try {
			return customerOrderTransDao.getInvoiceReceipt(orderNo, transactionNo, receiptType);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public ResponseResult sentTransactionEmail(TransactionEmailDto dto, LoginUserDto userDto) {
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();
		// File[] pdfFiles = readPdf(dto.getEmailType());
		List<byte[]> attach = new ArrayList<byte[]>();
		String fileName = "ATTACHMENT" + ".pdf";
		MessageTemplate mt = null;
		if (!StringUtils.isEmpty(dto.getEmailType())) {
			fileName = dto.getEmailType().toUpperCase();
		}

		//
		String noticeType = "";
		if (Constant.TEMPLATE_ID_RECEIPT.equalsIgnoreCase(dto.getEmailType())) {
			mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_RECEIPT);
			noticeType = Constant.NOTICE_TYPE_ENROLLMENT_SUCCESS;
		} else if (Constant.TEMPLATE_ID_INVOICE.equalsIgnoreCase(dto.getEmailType())) {
			mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_INVOICE);
			noticeType = Constant.NOTICE_TYPE_ENROLLMENT_SUCCESS;
		} else if (Constant.TEMPLATE_ID_DAYPASS_RECEIPT.equalsIgnoreCase(dto.getEmailType())) {
			mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_DAYPASS_PURCHASE_NOTIFY);
			noticeType = Constant.NOTICE_TYPE_DAY_PASS;
		} else if (Constant.TEMPLATE_ID_GUEST_ROOM_BOOK_CONFIRM.equalsIgnoreCase(dto.getEmailType())) {
			mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_GUEST_ROOM_BOOK_CONFIRM);
			noticeType = Constant.NOTICE_TYPE_GUEST_ROOM;
		} else {
			mt = messageTemplateDao.getTemplateByFunctionId(dto.getEmailType());
			noticeType = Constant.NOTICE_TYPE_ENROLLMENT_SUCCESS;
		}

		if (Constant.TEMPLATE_ID_RECEIPT.equalsIgnoreCase(dto.getEmailType())) {
			byte[] receiptAttach = getInvoiceReceipt(null, dto.getTransactionNO().toString(), "serviceplan");
			attach.add(receiptAttach);
			fileName = fileName + "-" + dto.getTransactionNO().toString() + ".pdf";
		} else if (Constant.TEMPLATE_ID_INVOICE.equalsIgnoreCase(dto.getEmailType())) {
			byte[] invoiceAttach = getInvoiceReceipt(dto.getOrderNO(), null, "serviceplan");
			attach.add(invoiceAttach);
			fileName = fileName + "-" + dto.getOrderNO() + ".pdf";
		} else if (Constant.TEMPLATE_ID_DAYPASS_RECEIPT.equalsIgnoreCase(dto.getEmailType())) {
			// mt =
			// messageTemplateDao.getTemplateByFunctionId(EmailType.Purchase_Daypass.getFunctionId());
			CustomerOrderTrans customerOrderTrans = getCustomerOrderTransListByOrderNo(dto.getOrderNO()).get(0);
			byte[] invoiceAttach = getInvoiceReceipt(dto.getOrderNO(), customerOrderTrans.getTransactionNo().toString(),
					"daypass");
			attach.add(invoiceAttach);
			fileName = "DaypassPurchaseReceipt-" + customerOrderTrans.getTransactionNo().toString() + ".pdf";
		} else if (Constant.TEMPLATE_ID_GUEST_ROOM_BOOK_CONFIRM.equalsIgnoreCase(dto.getEmailType())) {
			byte[] invoiceAttach = getInvoiceReceipt(null, dto.getTransactionNO().toString(), "guestroombook");
			attach.add(invoiceAttach);
			fileName = "AccommodationBookingReceipt-" + dto.getTransactionNO().toString() + ".pdf";
		}

		CustomerProfile cp = null;
		if (null != dto.getOrderNO()) {
			CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, dto.getOrderNO());
			cp = customerProfileDao.get(CustomerProfile.class, customerOrderHd.getCustomerId());
		} else if (null != dto.getTransactionNO()) {
			CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class,
					dto.getTransactionNO());
			cp = customerProfileDao.get(CustomerProfile.class, customerOrderTrans.getCustomerOrderHd().getCustomerId());
		}
		if (StringUtils.isEmpty(dto.getEmailContent())) {
			String contentMT = mt.getContentHtml();
			if (cp != null) {
				customerEmailContent.setContent(modifyContent(contentMT, cp, userDto.getUserName()));
			} else {
				customerEmailContent.setContent(contentMT);
			}
		} else {
			customerEmailContent.setContent(dto.getEmailContent());
		}

		if (StringUtils.isEmpty(dto.getSendTo())) {
			customerEmailContent.setRecipientEmail(cp.getContactEmail());
		} else {
			customerEmailContent.setRecipientEmail(dto.getSendTo());
		}

		if (StringUtils.isEmpty(dto.getSubject())) {
			customerEmailContent.setSubject(mt.getMessageSubject());
		} else {
			customerEmailContent.setSubject(dto.getSubject());
		}

		customerEmailContent.setRecipientCustomerId(cp.getCustomerId().toString()); // TODO:
																					// SAMHUI
																					// advised
																					// on
																					// 20160323:
																					// cp
																					// is
																					// possible
																					// null
																					// that
																					// will
																					// raise
																					// exception
																					// here.
																					// plan
																					// to
																					// fix
																					// it!
		customerEmailContent.setSendDate(new Date());
		customerEmailContent.setSenderUserId(userDto.getUserId());
		customerEmailContent.setCopyto(null);
		customerEmailContent.setNoticeType(noticeType);
		customerEmailContentDao.save(customerEmailContent);

		CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
		customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
		customerEmailAttach.setAttachmentName(fileName);
		customerEmailAttachDao.save(customerEmailAttach);

		List<String> fileNameList = new ArrayList<String>();
		fileNameList.add(fileName);
		List<String> mineTypeList = new ArrayList<String>();
		mineTypeList.add("application/pdf");
		mailThreadService.sendWithResponse(customerEmailContent, attach, mineTypeList, fileNameList);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	private String modifyContent(String content, CustomerProfile cp, String inscribe) {

		content = content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, cp.getGivenName() + " " + cp.getSurname());
		content = content.replace(Constant.PLACE_HOLDER_FROM_USER, inscribe);
		return content;
	}

	@Override
	@Transactional
	public void manualGenerateStatement(String year, String mon) throws Exception {
		String startDateString = year + "-" + mon + "-" + "01";
		Date startDate = DateConvertUtil.parseString2Date(startDateString, "yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date endDate = calendar.getTime();
		String endDateString = DateConvertUtil.date2String(endDate, "yyyy-MM-dd");

		generatePatronAccountStatement(startDateString, endDateString);
	}

	@Transactional
	public ResponseResult getMonthlyEnrollment(ListPage<CustomerOrderTrans> page, String year, String month) {
		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao.getMonthlyEnrollment(page, year, month);
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	private NetValue getNetValue(List<CustomerOrderTrans> customerOrderTransList){
		NetValue netValue=new NetValue();
//		VISA("VISA"),MASTER("MASTER"),CASH("Cash"),CASHVALUE("CashValue"),BT("Bank Transfer"),CHEQUE("Cheque"),VAC("Virtual Account Transfer"),CUP("Union Pay"),OTH("Other"),
//		PREAUTH("Payment Upon Arrival"), CHRGPOST("Charge Posting"), REFUND("REFUND"),AMEX("American Express"),DDI("Direct Debit"),JCB("JCB");
		BigDecimal amexNet=BigDecimal.ZERO;
		BigDecimal jcbNet=BigDecimal.ZERO;
		BigDecimal visaNet=BigDecimal.ZERO;
		BigDecimal masterNet=BigDecimal.ZERO;
		BigDecimal cupNet=BigDecimal.ZERO;
		BigDecimal totalNet=BigDecimal.ZERO;
		BigDecimal totalCreditCardNet=BigDecimal.ZERO;
		BigDecimal cashNet=BigDecimal.ZERO;
		BigDecimal cashValueNet=BigDecimal.ZERO;
		BigDecimal otherNet=BigDecimal.ZERO;
		for(CustomerOrderTrans customerOrderTrans: customerOrderTransList){
			if(PaymentMethod.OTH.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
					|| PaymentMethod.BT.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
					|| PaymentMethod.CHEQUE.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
					||PaymentMethod.CHRGPOST.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
					||PaymentMethod.VAC.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
					||PaymentMethod.DDI.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
					||PaymentMethod.REFUND.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())
					||PaymentMethod.PREAUTH.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())){
				otherNet=otherNet.add(customerOrderTrans.getPaidAmount());
			}
			
			if(PaymentMethod.AMEX.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())){
				amexNet=amexNet.add(customerOrderTrans.getPaidAmount());
				totalCreditCardNet=totalCreditCardNet.add(customerOrderTrans.getPaidAmount());
			}
			if(PaymentMethod.JCB.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())){
				jcbNet=jcbNet.add(customerOrderTrans.getPaidAmount());
				totalCreditCardNet=totalCreditCardNet.add(customerOrderTrans.getPaidAmount());
			}
			if(PaymentMethod.VISA.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())){
				visaNet=visaNet.add(customerOrderTrans.getPaidAmount());
				totalCreditCardNet=totalCreditCardNet.add(customerOrderTrans.getPaidAmount());
			}
			if(PaymentMethod.MASTER.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())){
				masterNet=jcbNet.add(customerOrderTrans.getPaidAmount());
				totalCreditCardNet=totalCreditCardNet.add(customerOrderTrans.getPaidAmount());
			}
			if(PaymentMethod.CUP.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())){
				cupNet=cupNet.add(customerOrderTrans.getPaidAmount());
				totalCreditCardNet=totalCreditCardNet.add(customerOrderTrans.getPaidAmount());
			}
			if(PaymentMethod.CASHVALUE.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())){
				cashValueNet=cashValueNet.add(customerOrderTrans.getPaidAmount());
			}
			if(PaymentMethod.CASH.name().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())){
				cashNet=cashNet.add(customerOrderTrans.getPaidAmount());
			}
			totalNet=totalNet.add(customerOrderTrans.getPaidAmount());
		}
		netValue.setAmexNet(amexNet.toPlainString());
		netValue.setJcbNet(jcbNet.toPlainString());
		netValue.setVisaNet(visaNet.toPlainString());
		netValue.setMasterNet(masterNet.toPlainString());
		netValue.setCupNet(cupNet.toPlainString());
		netValue.setTotalCreditCardNet(totalCreditCardNet.toPlainString());
		netValue.setTotalNet(totalNet.toPlainString());
		netValue.setCashNet(cashNet.toPlainString());
		netValue.setCashValueNet(cashValueNet.toPlainString());
		netValue.setOtherNet(otherNet.toPlainString());
		return netValue;
	}
	@Transactional
	public ResponseResult getDailyRevenue(String startTime, String endTime, String location){
		List<CustomerOrderTrans> customerOrderTransList=customerOrderTransDao.getDailyRevenue(startTime, endTime,location);
		NetValue netValue=getNetValue(customerOrderTransList);
		logger.info("netValue:"+netValue.toString());
		responseResult.initResult(GTAError.Success.SUCCESS, netValue);
		return responseResult;
	}

	@Transactional
	public byte[]  getDailyRevenueAttachment(String startTime, String endTime, String location,String fileType){
		try {
			List<CustomerOrderTrans> customerOrderTransList=customerOrderTransDao.getDailyRevenue(startTime, endTime,location);
			NetValue netValue=getNetValue(customerOrderTransList);
			logger.info("netValue:"+netValue.toString());
			return customerOrderTransDao.getDailyRevenueAttachment(startTime, endTime,location, fileType,netValue);
		} catch (JRException e) {
			e.printStackTrace();
			return null;
		}
	}
	


	
	@Transactional
	public ResponseResult getDailyRevenueInDetail(ListPage page ,String paymentMethod, String startDate, String endDate, String type){
		String sql=createDailyRevenueSql(paymentMethod, startDate, endDate, type);
		String countSql  = "select count(1) count from ("+sql +") temp";	
		ListPage list = customerOrderTransDao.listBySqlDto(page, countSql, sql, null, new TransactionDto());
		
		//calculate the sum of all amount after filtering
		String sumAmountSql="select sum(trans.paidAmount) as paidAmount  from ("+sql+") as trans";
		
		Object sumAmount=customerOrderTransDao.getUniqueBySQL(sumAmountSql, null);
		
		UsageRageData data=new UsageRageData();
		data.setList(list.getDtoList());
		data.setLastPage(list.isLast());
		data.setCurrentPage(list.getNumber());
		data.setRecordCount(list.getAllSize());
		data.setPageSize(list.getSize());
		data.setTotalPage(list.getAllPage());
		data.setUsageRage((null!=sumAmount)?sumAmount.toString():"0.00");// sum amount
		
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	private String createDailyRevenueSql(String paymentMethod, String startDate, String endDate, String type)
	{
		StringBuilder sbSql=new StringBuilder();
		if(StringUtils.isEmpty(type) || "ALL".equalsIgnoreCase(type)){
			type=null;
			sbSql.append("select * from ("+getExpensesSql(startDate, endDate, CustomerTransationStatus.SUC.name(),type)+"  UNION ALL "+getTopUpSql(startDate, endDate, CustomerTransationStatus.SUC.name()) +") temp");			
		}else{
			if (Constant.ServiceItem.TOPUP.name().equalsIgnoreCase(type)) {
				sbSql.append("select * from ("+getTopUpSql(startDate, endDate, CustomerTransationStatus.SUC.name()) +") temp \n");	
			}else{
				sbSql.append("select * from ("+getExpensesSql(startDate, endDate, CustomerTransationStatus.SUC.name(),type) +") temp \n");
			}
		}
		sbSql.append("	where 1=1 \n"); 
		
		if(!"ALL".equalsIgnoreCase(paymentMethod))
		{
			sbSql.append(" and temp.methodCode='"+paymentMethod+"'");
		}
		return sbSql.toString();
	}
	
	public String getExpensesSql(String startDate, String endDate, String status, String type){ 
		String typeStr="";
		if(type!=null){
			 typeStr=" and ps.item_catagory ='"+("OASIS_ROOM".equals(type)?"PMS_ROOM":type)+"' ";
		}
		String sql = 
				"	 SELECT                                                                      \n"+
				"	    DATE_FORMAT(                                                             \n"+
				"	    c.transaction_timestamp,                                                 \n"+
				"	    '%Y-%b-%d %H:%i:%s'                                                        \n "+
				"	  ) AS transactionDate,                                                      \n"+
				"    c.transaction_timestamp as transactionTimestamp,     \n"+
				"	  CONCAT(c.transaction_no,'') AS transactionNo,         \n                                                "+	 
				"	  CONCAT(c.order_no,'') AS orderNo,                       \n                                  "+
				"     CASE  WHEN c.payment_Media = 'OP'  \n"
				+ "   AND c.payment_method_code IN ("
				+ "  '"+PaymentMethod.VISA.name()+"',"
				+ " '"+PaymentMethod.MASTER.name()+"',"
				+ " '"+PaymentMethod.CUP.name()+"',"
				+ " '"+PaymentMethod.AMEX.name()+"',"
				+ " '"+PaymentMethod.JCB.name()+"'"
				+ ")   THEN CONCAT(c.payment_method_code, '(online)')  ELSE c.payment_method_code END AS paymentMethodCode,\n"+
				"	c.payment_method_code as methodCode, \n"+
//				" CASE WHEN ps.item_catagory = 'RESTAURANT' OR ps.item_catagory = 'OASIS_ROOM'  THEN c.internal_remark \n" + 
				" CASE WHEN ps.item_catagory = 'RESTAURANT' OR ps.item_catagory = 'OASIS_ROOM' OR ps.item_catagory = 'MMS' or ps.item_catagory='MMS_SPA' THEN c.internal_remark \n"+
				" WHEN ps.item_catagory='GFBK' THEN 'Golfing Bay' \n" +
				" WHEN ps.item_catagory='TFBK' THEN 'Tennis Court' \n" +
				" WHEN ps.item_catagory='TS'  THEN 'Tennis Private Coaching' \n" +
				" WHEN ps.item_catagory='GS'  THEN  'Golf Private Coaching' \n" +
				" WHEN ps.item_catagory='SRV' THEN  'Service Plan' \n" +
				" WHEN ps.item_catagory='DP'  THEN  'Day Pass Purchasing' \n" +
				" WHEN ps.item_catagory='GSS' THEN 'Golf Course Enrollment' \n" +
				" WHEN ps.item_catagory='TSS' THEN 'Tennis Course Enrollment' \n" +
				" WHEN ps.item_catagory='PMS_ROOM' THEN 'Guest Room' \n" +
				" WHEN ps.item_catagory='MMS_SPA'	THEN  'Wellness Center' \n" +
				" ELSE ps.item_catagory  \n" +
				" END AS description,\n" +
				"	  CONCAT(  p.given_Name, ' ', p.surname ) AS memberName,  \n"+
				"	  CONCAT(c.paid_amount,'') AS paidAmount, \n                                                        "+
				"  CASE WHEN ps.item_catagory = 'RESTAURANT' THEN 'POS' \n "+
				"  WHEN ps.item_catagory='OASIS_ROOM' THEN 'PMS'  \n"+
				"  ELSE c.create_by  \n"+
				"	END AS createBy , \n                                               "+
				"  CASE WHEN ps.item_catagory = 'RESTAURANT' THEN 'POS'  \n"+
				"  WHEN ps.item_catagory='OASIS_ROOM' THEN 'PMS' \n "+
				"	 ELSE (CASE c.payment_location_code WHEN 'FD1' THEN 'Front Desk 1' WHEN 'FD2' THEN 'Front Desk 2' WHEN 'WP' THEN 'Patron Web'\n "
				+ " ELSE c.payment_location_code END)  END AS paidmentLocationCode,\n"+
				"   m.academy_no AS patronId, "+
				"  CASE WHEN ps.item_catagory = 'RESTAURANT' THEN 'POS' \n"+
				"  WHEN ps.item_catagory='OASIS_ROOM' THEN 'PMS' \n"+
				"  WHEN ps.item_catagory='GFBK' THEN 'Golfing Bay'\n"+
				"  WHEN ps.item_catagory='TFBK' THEN 'Tennis Court' \n"+
				"  WHEN ps.item_catagory='TS'  THEN 'Tennis Private Coaching' \n"+
				"  WHEN ps.item_catagory='GS'  THEN  'Golf Private Coaching' \n"+
				"  WHEN ps.item_catagory='SRV' THEN  'Patron Enrollment' \n"+
				"  WHEN ps.item_catagory='DP'  THEN  'Day Pass' \n"+
				"  WHEN ps.item_catagory='GSS' THEN 'Golf Course' \n"+
				"  WHEN ps.item_catagory='TSS' THEN 'Tennis Course' \n"+
				"  ELSE ps.item_catagory \n "+
				"  END AS transactionType,\n"+		
				/***
				 * when c.internal_remark is not null display internal_remark
				 * else display GFB-c.transaction_no
				 */
				"  CASE "+
				"  WHEN ps.item_catagory='GFBK' THEN  IFNULL(c.internal_remark,CONCAT('GBF','-',c.transaction_no)) \n"+
				"  WHEN ps.item_catagory='TFBK' THEN  IFNULL(c.internal_remark,CONCAT('TCF','-',c.transaction_no)) \n"+
				"  WHEN ps.item_catagory='TS'  THEN  IFNULL(c.internal_remark,CONCAT('TPC','-',c.transaction_no)) \n"+
				"  WHEN ps.item_catagory='GS'  THEN  IFNULL(c.internal_remark,CONCAT('GPC','-',c.transaction_no))  \n "+
				"  WHEN ps.item_catagory='SRV' THEN  IFNULL(c.internal_remark,CONCAT('SRV','-',c.transaction_no))  \n"+
				"  WHEN ps.item_catagory='DP'  THEN  IFNULL(c.internal_remark,CONCAT('DP','-',c.transaction_no)) \n"+
				"  WHEN ps.item_catagory='GSS' THEN IFNULL(c.internal_remark,CONCAT('GCE','-',c.transaction_no)) \n"+
				"  WHEN ps.item_catagory='TSS' THEN IFNULL(c.internal_remark,CONCAT('TCE','-',c.transaction_no))  \n"+
				"  ELSE ' '  "+
				"   END  AS internalRemark \n"+
				"	FROM             "+
				"	  customer_order_trans c,     \n  "+
				"	  customer_order_hd h,   \n"+
				"	  customer_profile p,      \n "+
				"	  customer_order_det d,  \n "+
				"	  member m,     \n"+
				"	  pos_service_item_price ps \n "+
				"	WHERE c.order_no = h.order_no   \n"+
				"	  AND h.order_no = d.order_no  \n"+
				"	  AND (c.status = '"+status+"' and DATE_FORMAT(c.transaction_timestamp,'%Y-%m-%d') BETWEEN   '"+startDate+"' AND '"+endDate+"'  )\n  "+
				"	  AND h.customer_Id = m.customer_Id    \n"+
				"	  AND m.customer_Id = p.customer_Id    \n "+
				"	  AND d.item_No = ps.item_No    \n"+	
				///SG-3055 
				//Some transactions are missing in Daily Revenue Report
				// old :ps.item_catagory  not in ('TOPUP','REFUND','PMS_ROOM','MMS_SPA')
				"   AND ps.item_catagory  not in ('TOPUP','REFUND','MMS_SPA')   "+ typeStr+
				"	and not exists(         \n"+
				"	SELECT 1 FROM member_facility_type_booking m   \n"+
				"	LEFT JOIN room_facility_type_booking room_booking ON room_booking.facility_type_resv_id = m.resv_id  \n"+
				"	WHERE  room_booking.is_bundle = 'Y' and m.order_no = c.order_no )     \n"+
				"	  AND d.order_Det_Id =     \n"+
				"	  (SELECT    \n "+
				"	    MIN(dd.order_Det_Id)  \n"+
				"	  FROM                  \n "+
				"	    customer_order_det dd    \n"+
				"	  WHERE dd.order_no = h.order_no \n"+
				"	    AND dd.item_No <> 'SCVD00000001' \n"+
				"	    AND dd.item_No <> 'SCVR00000001') \n";
		return sql;
	}
	
	public String getTopUpSql(String startDate, String endDate, String status){
		String sql = 
			"	SELECT                                                                                       "+
			"	  DATE_FORMAT( c.transaction_timestamp,'%Y-%b-%d %H:%i:%s') AS transactionDate,                 "+
			"    c.transaction_timestamp  as transactionTimestamp,     "+
			"	  CONCAT(c.transaction_no,'') AS transactionNo,                                                         "+
			"	  CONCAT(c.order_no,'') AS orderNo,                                                         "+
			"	  CASE                                                                                       "+
			"	  WHEN c.payment_Media = 'OP'                                                                "+
			"	  AND c.payment_Method_Code IN ("
			+ "'"+PaymentMethod.VISA.name()+"', "
			+ "'"+PaymentMethod.MASTER.name()+"', "
			+ "'"+PaymentMethod.AMEX.name()+"', "
			+ "'"+PaymentMethod.CUP.name()+"', "
			+ "'"+PaymentMethod.JCB.name()+"'"
			+ ")                                     "+
			"	  THEN CONCAT(      c.payment_Method_Code,      '(online)'    )                              "+
			"	  ELSE c.payment_Method_Code                                                                 "+
			"	  END AS paymentMethodCode,                                                                  "+
			"	c.payment_method_code as methodCode, \n"+
			"	  'Top Up' description,                                                                            "+
			"	  CONCAT(  p.given_Name, ' ', p.surname ) AS memberName,   "+
			"	  CONCAT(c.paid_Amount,'')  AS paidAmount ,                                                               "+
			"	 c.create_by AS createBy ,                                                "+
			"	CASE c.payment_location_code WHEN 'FD1' THEN 'Front Desk 1' WHEN 'FD2' THEN 'Front Desk 2' WHEN 'WP' THEN 'Patron Web' "+
			 "  ELSE c.payment_location_code END AS paidmentLocationCode,"+
		//	"	 c.payment_location_code AS paidmentLocationCode  ,                                                "+
			"   m.academy_no AS patronId, "+	
			"   'Cash Value Top Up' transactionType, "+
			"  CONCAT('TU','-',c.transaction_no)  AS internalRemark"+
			
			"	FROM                                                                                         "+
			"	  customer_order_trans c,                                                                    "+
			"	  customer_order_hd h,                                                                       "+
			"	  customer_profile p,                                                                        "+
			"	  customer_order_det d,                                                                      "+
			"	  member m                                                                                   "+
			"	WHERE c.order_No = h.order_No                                                                "+
			"	  AND h.order_No = d.order_No                                                                "+
			"	  AND d.item_No = 'CVT0000001'                                                               "+
			"	  AND h.customer_Id = m.customer_Id                                                          "+
			"	  AND (c.status = '"+status+"' and DATE_FORMAT(c.transaction_timestamp,'%Y-%m-%d')  BETWEEN   '"+startDate+"' AND '"+endDate+"' )"+
			"	  AND m.customer_Id = p.customer_Id                                                          "+
			"	  AND d.order_Det_Id =                                                                       "+
			"	  (SELECT                                                                                    "+
			"	    MIN(dd.order_Det_Id)                                                                     "+
			"	  FROM                                                                                       "+
			"	    customer_order_det dd                                                                    "+
			"	  WHERE dd.order_No = h.order_No)                                                            ";
		return sql;
	}
	
	@Transactional
	public byte[] getMonthlyEnrollmentAttachment(String year, String month, String fileType, String sortBy,
			String isAscending) {
		try {
			return customerOrderTransDao.getMonthlyEnrollmentAttachment(year, month, fileType, sortBy, isAscending);
		} catch (JRException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public ResponseResult getDailyMonthlyPrivateCoachingRevenue(String timePeriodType, ListPage<?> page,
			String selectedDate, String facilityType) {

		ListPage<CustomerOrderTrans> listPage = customerOrderTransDao
				.getDailyMonthlyPrivateCoachingRevenue(timePeriodType, page, selectedDate, facilityType);
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Override
	@Transactional
	public byte[] getDailyMonthlyPrivateCoachingRevenueAttach(String timePeriodType, String selectedDate,
			String fileType, String sortBy, String isAscending, String facilityType) {

		try {
			return customerOrderTransDao.getDailyMonthlyPrivateCoachingRevenueAttachment(timePeriodType, selectedDate,
					fileType, sortBy, isAscending, facilityType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * SGG-1384
	 */
	@Transactional
	public ResponseResult getMonthlyTransaction(ListPage page, String reservationMonth) {

		String sql = reservationService.getReserSqlByDate(reservationMonth).toString();
		String countSql = "select count(1) from(" + sql + ") a";
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, new ArrayList(),
				new SourceBookingDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	/*
	 * SGG-1385
	 */
	@Transactional
	@Override
	public byte[] getMonthlyTransactionAttachment(ListPage page, String reservationMonth, String fileType)
			throws Exception {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String jasperPath = reportPath + "SourceOfBooking.jasper";
		List<Serializable> params = new ArrayList<Serializable>();
		String sql = reservationService.getReserSqlByDate(reservationMonth).toString();
		sql = customerOrderTransDao.addOrderBy(page, sql);
		logger.debug("sql:"+sql );
		List<SourceBookingDto> dataList = customerOrderTransDao.getDtoBySql(sql, params, SourceBookingDto.class);
		// JRDataSource dataSource = new JRBeanCollectionDataSource(dataList);
		Map<String, Object> parameters = new HashMap();
		parameters.put("reservationMonth", reservationMonth + "");
		parameters.put("sumQty", dataList.size() + "");
		return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);
	}
	@Transactional
	public ResponseResult getMemberInfo(ListPage page, String status) {

		String sql = memberReportService.getSqlByStatus(status).toString();
		String countSql = "select count(1) from(" + sql + ") a";
		logger.debug("sql:" + sql);
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, new ArrayList(), new PatronInfoDto());
		List<Object> customerArray = listPage.getDtoList();
		List<Object> customerArrayForTrans = new ArrayList<Object>();
		String custIdArray = "";
		String superCustIdArray = "";
		for (Object temp : customerArray) {
			PatronInfoDto patronInfoDto = (PatronInfoDto) temp;
			custIdArray = custIdArray + patronInfoDto.getCustomerID() + ",";
			superCustIdArray = superCustIdArray + patronInfoDto.getSuperCustomerID() + ",";
		}
		custIdArray = custIdArray.substring(0, custIdArray.length() - 1);
		superCustIdArray = superCustIdArray.substring(0, superCustIdArray.length() - 1);
		// Map<String, String> expiryDateMap = new HashMap<String, String>();
		// Map<String, String> servicePlanMap = new HashMap<String, String>();
		// Map<String, String> HKGTADirectMarketingMap = new HashMap<String, String>();
		// Map<String, String> NWDDirectMarketing = new HashMap<String, String>();
		Map<String, org.hibernate.type.Type> expiryDateSuperiorMap = new HashMap<String, org.hibernate.type.Type>();
		expiryDateSuperiorMap.put("customerID", StringType.INSTANCE);
		expiryDateSuperiorMap.put("expiryDate", StringType.INSTANCE);
		
		sql = memberReportService.getExipryDateByCustID(superCustIdArray).toString();
		
		List<Serializable> superParam = new ArrayList<Serializable>();
		// superParam.add(superCustIdArray);
		List<ExpiryDateDto> expirySuperiorList = customerOrderTransDao.getDtoBySql(sql, superParam, ExpiryDateDto.class, expiryDateSuperiorMap);

		Map<String, org.hibernate.type.Type> expiryDateMap = new HashMap<String, org.hibernate.type.Type>();
		expiryDateMap.put("customerID", StringType.INSTANCE);
		expiryDateMap.put("expiryDate", StringType.INSTANCE);
		sql = memberReportService.getExipryDateByCustID(custIdArray).toString();
		List<Serializable> param = new ArrayList<Serializable>();
		// param.add(custIdArray);
		List<ExpiryDateDto> expiryList = customerOrderTransDao.getDtoBySql(sql, param, ExpiryDateDto.class, expiryDateMap);
		

		Map<String, org.hibernate.type.Type> servicePlanMap = new HashMap<String, org.hibernate.type.Type>();
		servicePlanMap.put("customerID", StringType.INSTANCE);
		servicePlanMap.put("servicePlan", StringType.INSTANCE);
		sql = memberReportService.getServicePlanByCustID(custIdArray).toString();
		List<PatronServicePlanDto> servicePlanList = customerOrderTransDao.getDtoBySql(sql, param, PatronServicePlanDto.class, servicePlanMap);

		Map<String, org.hibernate.type.Type> HKGTADirectMarketingMap = new HashMap<String, org.hibernate.type.Type>();
		HKGTADirectMarketingMap.put("customerID", StringType.INSTANCE);
		HKGTADirectMarketingMap.put("customerInput", StringType.INSTANCE);
		sql = memberReportService.getHKGTADirectMarketingByCustID(custIdArray).toString();
		List<MarkMessageDto> hkgtaList = customerOrderTransDao.getDtoBySql(sql, param, MarkMessageDto.class, HKGTADirectMarketingMap);

		Map<String, org.hibernate.type.Type> NWDDirectMarketing = new HashMap<String, org.hibernate.type.Type>();
		NWDDirectMarketing.put("customerID", StringType.INSTANCE);
		NWDDirectMarketing.put("customerInput", StringType.INSTANCE);
		sql = memberReportService.getNWDDirectMarketingByCustID(custIdArray).toString();
		List<MarkMessageDto> nwdList = customerOrderTransDao.getDtoBySql(sql, param, MarkMessageDto.class, NWDDirectMarketing);

		// PatronServicePlanDto

		for (Object member : customerArray) {
			PatronInfoDto patronInfoDto = (PatronInfoDto) member;
			for (ExpiryDateDto expiryDateDto : expiryList) {
				if (patronInfoDto.getCustomerID().equalsIgnoreCase(expiryDateDto.getCustomerID())) {
					patronInfoDto.setExpiryDate(expiryDateDto.getExpiryDate());
				}
			}
			for (ExpiryDateDto expiryDateDto : expirySuperiorList) {
				if (StringUtils.isEmpty(patronInfoDto.getExpiryDate())) {
					if (patronInfoDto.getCustomerID().equalsIgnoreCase(expiryDateDto.getCustomerID())||
							expiryDateDto.getCustomerID().equalsIgnoreCase(patronInfoDto.getSuperCustomerID())) {
						patronInfoDto.setExpiryDate(expiryDateDto.getExpiryDate());
					}
				}
			}
			for (PatronServicePlanDto patronServicePlanDto : servicePlanList) {
				if (patronInfoDto.getCustomerID().equalsIgnoreCase(patronServicePlanDto.getCustomerID())||
						patronServicePlanDto.getCustomerID().equalsIgnoreCase(patronInfoDto.getSuperCustomerID())) {
					patronInfoDto.setServicePlan(patronServicePlanDto.getServicePlan());
				}
			}
			if(null!=hkgtaList&&hkgtaList.size()>0)
			{
				for (MarkMessageDto markMessageDto : hkgtaList) {
					if (patronInfoDto.getCustomerID().equalsIgnoreCase(markMessageDto.getCustomerID())) {
						patronInfoDto.sethKGTADirectMarketing("Yes".equals(markMessageDto.getCustomerInput()) ? "True" : "False");
						break;
					}else{
						patronInfoDto.sethKGTADirectMarketing("True");
					}
				}
			}else{
				patronInfoDto.sethKGTADirectMarketing("True");
			}
			if(null!=nwdList&&nwdList.size()>0){
				for (MarkMessageDto markMessageDto : nwdList) {
					if (patronInfoDto.getCustomerID().equalsIgnoreCase(markMessageDto.getCustomerID())) {
						patronInfoDto.setnWDDirectMarketing("Yes".equals(markMessageDto.getCustomerInput()) ? "True" : "False");
						break;
					}else{
						patronInfoDto.setnWDDirectMarketing("True");
					}
				}
			}else{
				patronInfoDto.setnWDDirectMarketing("True");
			}
			customerArrayForTrans.add(patronInfoDto);
		}
		customerArray = customerArrayForTrans;
		listPage.setDtoList(customerArrayForTrans);

		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(customerArray);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Transactional
	@Override
	public byte[] getMemberInfoAttachement(ListPage page, String fileType, String status) throws Exception {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String jasperPath = reportPath + "PatronInfo.jasper";

		List<Serializable> params = new ArrayList<Serializable>();
		String sql = memberReportService.getSqlByStatus(status).toString();
		sql = customerOrderTransDao.addOrderBy(page, sql);
		logger.debug("sql:" + sql);

		// JRDataSource dataSource = new JRBeanCollectionDataSource(dataList);
		List<PatronInfoDto> customerArray = customerOrderTransDao.getDtoBySql(sql, params, PatronInfoDto.class);
		// ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, new ArrayList(), new PatronInfoDto());
		List<PatronInfoDto> customerArrayForTrans = new ArrayList<PatronInfoDto>();
		String custIdArray = "";
		String superCustIdArray = "";
		for (PatronInfoDto patronInfoDto : customerArray) {
			custIdArray = custIdArray + patronInfoDto.getCustomerID() + ",";
			superCustIdArray = superCustIdArray + patronInfoDto.getSuperCustomerID() + ",";
		}
		custIdArray = custIdArray.substring(0, custIdArray.length() - 1);
		superCustIdArray = superCustIdArray.substring(0, superCustIdArray.length() - 1);
		// Map<String, String> expiryDateMap = new HashMap<String, String>();
		// Map<String, String> servicePlanMap = new HashMap<String, String>();
		// Map<String, String> HKGTADirectMarketingMap = new HashMap<String, String>();
		// Map<String, String> NWDDirectMarketing = new HashMap<String, String>();
		Map<String, org.hibernate.type.Type> expiryDateSuperiorMap = new HashMap<String, org.hibernate.type.Type>();
		expiryDateSuperiorMap.put("customerID", StringType.INSTANCE);
		expiryDateSuperiorMap.put("expiryDate", StringType.INSTANCE);
		sql = memberReportService.getExipryDateByCustID(superCustIdArray).toString();
		List<Serializable> superParam = new ArrayList<Serializable>();
		// superParam.add(superCustIdArray);
		List<ExpiryDateDto> expirySuperiorList = customerOrderTransDao.getDtoBySql(sql, superParam, ExpiryDateDto.class, expiryDateSuperiorMap);

		Map<String, org.hibernate.type.Type> expiryDateMap = new HashMap<String, org.hibernate.type.Type>();
		expiryDateMap.put("customerID", StringType.INSTANCE);
		expiryDateMap.put("expiryDate", StringType.INSTANCE);
		sql = memberReportService.getExipryDateByCustID(custIdArray).toString();
		List<Serializable> param = new ArrayList<Serializable>();
		// param.add(custIdArray);
		List<ExpiryDateDto> expiryList = customerOrderTransDao.getDtoBySql(sql, param, ExpiryDateDto.class, expiryDateMap);

		Map<String, org.hibernate.type.Type> servicePlanMap = new HashMap<String, org.hibernate.type.Type>();
		servicePlanMap.put("customerID", StringType.INSTANCE);
		servicePlanMap.put("servicePlan", StringType.INSTANCE);
		sql = memberReportService.getServicePlanByCustID(custIdArray).toString();
		List<PatronServicePlanDto> servicePlanList = customerOrderTransDao.getDtoBySql(sql, param, PatronServicePlanDto.class, servicePlanMap);

		Map<String, org.hibernate.type.Type> HKGTADirectMarketingMap = new HashMap<String, org.hibernate.type.Type>();
		HKGTADirectMarketingMap.put("customerID", StringType.INSTANCE);
		HKGTADirectMarketingMap.put("customerInput", StringType.INSTANCE);
		sql = memberReportService.getHKGTADirectMarketingByCustID(custIdArray).toString();
		List<MarkMessageDto> hkgtaList = customerOrderTransDao.getDtoBySql(sql, param, MarkMessageDto.class, HKGTADirectMarketingMap);

		Map<String, org.hibernate.type.Type> NWDDirectMarketing = new HashMap<String, org.hibernate.type.Type>();
		NWDDirectMarketing.put("customerID", StringType.INSTANCE);
		NWDDirectMarketing.put("customerInput", StringType.INSTANCE);
		sql = memberReportService.getNWDDirectMarketingByCustID(custIdArray).toString();
		List<MarkMessageDto> nwdList = customerOrderTransDao.getDtoBySql(sql, param, MarkMessageDto.class, NWDDirectMarketing);

		// PatronServicePlanDto

		for (PatronInfoDto patronInfoDto : customerArray) {
			for (ExpiryDateDto expiryDateDto : expiryList) {
				if (patronInfoDto.getCustomerID().equalsIgnoreCase(expiryDateDto.getCustomerID())) {
					patronInfoDto.setExpiryDate(expiryDateDto.getExpiryDate());
				}
			}
			for (ExpiryDateDto expiryDateDto : expirySuperiorList) {
				if (StringUtils.isEmpty(patronInfoDto.getExpiryDate())) {
					if (patronInfoDto.getCustomerID().equalsIgnoreCase(expiryDateDto.getCustomerID())||
							expiryDateDto.getCustomerID().equalsIgnoreCase(patronInfoDto.getSuperCustomerID())) {
						patronInfoDto.setExpiryDate(expiryDateDto.getExpiryDate());
					}
				}
			}
			for (PatronServicePlanDto patronServicePlanDto : servicePlanList) {
				if (patronInfoDto.getCustomerID().equalsIgnoreCase(patronServicePlanDto.getCustomerID())||
						patronServicePlanDto.getCustomerID().equalsIgnoreCase(patronInfoDto.getSuperCustomerID())) {
					patronInfoDto.setServicePlan(patronServicePlanDto.getServicePlan());
				}
			}
			if(null!=hkgtaList&&hkgtaList.size()>0)
			{
				for (MarkMessageDto markMessageDto : hkgtaList) {
					if (patronInfoDto.getCustomerID().equalsIgnoreCase(markMessageDto.getCustomerID())) {
						patronInfoDto.sethKGTADirectMarketing("Yes".equals(markMessageDto.getCustomerInput()) ? "True" : "False");
						break;
					}else{
						patronInfoDto.sethKGTADirectMarketing("True");
					}
				}
			}else{
				patronInfoDto.sethKGTADirectMarketing("True");
			}
			if(null!=nwdList&&nwdList.size()>0){
				for (MarkMessageDto markMessageDto : nwdList) {
					if (patronInfoDto.getCustomerID().equalsIgnoreCase(markMessageDto.getCustomerID())) {
						patronInfoDto.setnWDDirectMarketing("Yes".equals(markMessageDto.getCustomerInput()) ? "True" : "False");
						break;
					}else{
						patronInfoDto.setnWDDirectMarketing("True");
					}
				}
			}else{
				patronInfoDto.setnWDDirectMarketing("True");
			}
			
			customerArrayForTrans.add(patronInfoDto);
		}

		Date day = new Date();
		String theDay = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss").format(day);
		Map<String, Object> parameters = new HashMap();
		parameters.put("theDate", theDay);
		parameters.put("sumQty", customerArrayForTrans.size() + "");
		return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, customerArrayForTrans, fileType);
	}
	@Transactional
	public ResponseResult getReservationReport(ListPage page, String date,String type,String status) {

		String sql = reservationReportService.getReserSqlByDate(date,type,status).toString();
		String countSql = "select count(1) from(" + sql + ") a";
		logger.debug("sql:"+sql );
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, new ArrayList(),
				new ReservationDto());
		List<Object> listBooking = listPage.getDtoList();
		List<Object> listBookingForTrans = new ArrayList<Object>();
		 if(Status.CAN.name().equals(status)){
			 for(Object temp:listBooking){
				 String DAY_PASS = "Day Pass";
				 ReservationDto reservationDto=(ReservationDto)temp;
				 if(!DAY_PASS.equalsIgnoreCase(reservationDto.getServiceType())){					
					 RefundInfoDto refundInfoDto= customerRefundRequestDao.getCancelInfo(reservationDto.getReservationId());
					 if(refundInfoDto.getRequestDateTime()!=null){
						 reservationDto.setUpdateDate(DateUtil.formatDate(refundInfoDto.getRequestDateTime(),"YYYY-MM-dd HH:mm:ss"));
					 }
					 if(refundInfoDto.getRequestedBy()!=null){
						 if(Purchase_Daypass_Type.Member.name().equalsIgnoreCase(refundInfoDto.getRequestedBy())){
							 refundInfoDto.setRequestedBy("Patron");
						 }
					 }
					 reservationDto.setUpdateUser(refundInfoDto.getRequestedBy());
				 }
				 listBookingForTrans.add(reservationDto);
			 }
			 listBooking=listBookingForTrans;
			 listPage.setDtoList(listBookingForTrans);
		 }
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	@Transactional
	@Override
	public byte[] getReservationReportAttachement(ListPage page,String day,String fileType,String type,String status) throws  Exception{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String jasperPath = reportPath + "NoShowReport.jasper";
		if(Status.RSV.name().equals(status)){
			jasperPath = reportPath + "DaiyBooking.jasper";
		}else if(Status.CAN.name().equals(status)){
			jasperPath = reportPath + "Cancellation.jasper";
		}
		List<Serializable> params = new ArrayList<Serializable>();
		String sql = reservationReportService.getReserSqlByDate(day,type,status).toString();
		sql = customerOrderTransDao.addOrderBy(page, sql);
		logger.debug("sql:"+sql );
		List<ReservationDto> dataList = customerOrderTransDao.getDtoBySql(sql, params,ReservationDto.class);
		List<ReservationDto> listBookingForTrans = new ArrayList<ReservationDto>();
		 if(Status.CAN.name().equals(status)){
			 for(ReservationDto reservationDto:dataList){
				 String DAY_PASS = "Day Pass";
				if(!DAY_PASS.equalsIgnoreCase(reservationDto.getServiceType())){
					 RefundInfoDto refundInfoDto= customerRefundRequestDao.getCancelInfo(reservationDto.getReservationId());
					 if(refundInfoDto.getRequestDateTime()!=null){
						 reservationDto.setUpdateDate(DateUtil.formatDate(refundInfoDto.getRequestDateTime(),"YYYY-MM-dd HH:mm:ss"));
					 }
					 if(refundInfoDto.getRequestedBy()!=null){
						 if(Purchase_Daypass_Type.Member.name().equalsIgnoreCase(refundInfoDto.getRequestedBy())){
							 refundInfoDto.setRequestedBy("Patron");
						 }
					 }
					 reservationDto.setUpdateUser(refundInfoDto.getRequestedBy());	
				 }
				 listBookingForTrans.add(reservationDto);
			 }
			 dataList=listBookingForTrans;
		 }
		// JRDataSource dataSource = new JRBeanCollectionDataSource(dataList);
		Map<String, Object> parameters = new HashMap();
		parameters.put("reservationDate", day + "");
		parameters.put("sumQty", dataList.size() + "");
		return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);
	}
	@Transactional
	@Override
	public byte[] getDailyRevenueInDetailAttachement(ListPage page,String paymentMethod, String startDate, String endDate,String fileType,String type) throws  Exception{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String jasperPath = reportPath + "DailyRevenueInDetail.jasper";
		
		String sql =createDailyRevenueSql(paymentMethod, startDate, endDate, type);
		
		String sumAmountSql="select sum(trans.paidAmount) as paidAmount  from ("+sql+") as trans";
		Object sumAmount=customerOrderTransDao.getUniqueBySQL(sumAmountSql, null);
		
		sql = customerOrderTransDao.addOrderBy(page, sql);
		logger.debug("sql:"+sql );
		List<TransactionDto> dataList = customerOrderTransDao.getDtoBySql(sql, null,TransactionDto.class);
		Map<String, Object> parameters = new HashMap();
		parameters.put("reservationDate", startDate + "-" + endDate);
		parameters.put("sumQty", dataList.size() + "");
		parameters.put("totalAmount", (null!=sumAmount?sumAmount.toString():"0"));
		return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);
	}
	/**
	 * SGG-1380
	 */
	@Transactional
	@Override
	public ResponseResult getGolfAndTennisCachesPerform(ListPage page, String reportMonth) throws ParseException {

		String startDate = DateCalcUtil.getMonthStartDate(reportMonth);
		String endDate = DateCalcUtil.getMonthEndDate(reportMonth);
		String sql = coachManagementService.getCoachesPerformanceSQL(startDate, endDate).toString();
		String countSql = "select count(1) count from ( " + sql + " ) a ";
		List param = new ArrayList();
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, param, new CoachesPerformanceDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Transactional
	@Override
	public byte[] getGolfAndTennisCachesPerformAttachment(ListPage page, String reportMonth, String fileType)
			throws Exception {

		Map<String, Object> parameters = new HashMap();
		parameters.put("reportMonth", reportMonth + "");
		List<Serializable> params = new ArrayList<Serializable>();
		String startDate = DateCalcUtil.getMonthStartDate(reportMonth);
		String endDate = DateCalcUtil.getMonthEndDate(reportMonth);

		String sql = coachManagementService.getCoachesPerformanceSQL(startDate, endDate).toString();
		sql = customerOrderTransDao.addOrderBy(page, sql);
		List<CoachesPerformanceDto> dataList = customerOrderTransDao.getDtoBySql(sql, params,
				CoachesPerformanceDto.class);

		BigInteger sumStudentNum = BigInteger.ZERO;
		BigInteger sumReservNum = BigInteger.ZERO;
		BigDecimal sumTotalHourCoach = new BigDecimal(0);
		BigDecimal sumBookAmount = new BigDecimal(0);
		BigInteger sumCourseSessionNum = BigInteger.ZERO;
		BigDecimal sumTotalHourCourse = new BigDecimal(0);

		for (CoachesPerformanceDto dto : dataList) {
			BigInteger studentNum = dto.getStudentNum();
			BigInteger reservNum = dto.getReservNum();
			BigDecimal totalHourCoach = dto.getTotalHourCoach();
			BigDecimal bookAmount = dto.getBookAmount();
			BigInteger courseSessionNum = dto.getCourseSessionNum();
			BigDecimal totalHourCourse = dto.getTotalHourCourse();
			sumStudentNum = sumStudentNum.add(studentNum);
			sumReservNum = sumReservNum.add(reservNum);
			sumTotalHourCoach = sumTotalHourCoach.add(totalHourCoach == null ? new BigDecimal(0) : totalHourCoach);
			sumBookAmount = sumBookAmount.add(bookAmount == null ? new BigDecimal(0) : bookAmount);
			sumCourseSessionNum = sumCourseSessionNum.add(courseSessionNum);
			sumTotalHourCourse = sumTotalHourCourse.add(totalHourCourse == null ? new BigDecimal(0) : totalHourCourse);
		}
		parameters.put("sumStudentNum", sumStudentNum + "");
		parameters.put("sumReservNum", sumReservNum + "");
		parameters.put("sumTotalHourCoach", sumTotalHourCoach + "");
		parameters.put("sumBookAmount", sumBookAmount + "");
		parameters.put("sumCourseSessionNum", sumCourseSessionNum + "");
		parameters.put("sumTotalHourCourse", sumTotalHourCourse + "");

		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String jasperPath = reportPath + "GolfAndTennisCoachesPerform.jasper";

		return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);

	}

	/**
	 * 根据【cuseromerId】获取【day】天内member的CashValue变更日志查询语句
	 * 
	 * @param type
	 * @param customerId
	 * @param day
	 * @return
	 */
	@Override
	@Transactional
	public String getCashValueChangesLog(String type, long customerId, int day) {
		return customerOrderTransDao.getCashValueChangesLog(type, customerId, day);
	}

	/**
	 * SGG-1388
	 */
	@Transactional
	@Override
	public ResponseResult getCoachTraining(ListPage page, String coachUserId, String startDate, String endDate) {
		String sql = coachManagementService.getCoachTrainingSQL(coachUserId, startDate, endDate).toString();
		String countSql = "select count(1) count from ( " + sql + " ) a ";
		List param = new ArrayList();
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, param,
				new CourseCoachCalendarDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	/**
	 * SGG-1388
	 * 
	 * @throws Exception
	 */
	@Transactional
	@Override
	public byte[] getCoachTrainingReport(ListPage page, String fileType, String coachUserId, String coachName,
			String startDate, String endDate) throws Exception {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";

		String parentjasperPath = reportPath + "CoachTrainingReport.jasper";
		Map<String, Object> parameters = new HashMap();
		parameters.put("startDate", startDate + "");
		parameters.put("endDate", endDate + "");
		parameters.put("coachName", coachName + "");
		List<Serializable> params = new ArrayList<Serializable>();
		String sql = coachManagementService.getCoachTrainingSQL(coachUserId, startDate, endDate).toString();
		sql = customerOrderTransDao.addOrderBy(page, sql);
		List<CourseCoachCalendarDto> dataList = customerOrderTransDao.getDtoBySql(sql, params,
				CourseCoachCalendarDto.class);
		if (StringUtils.isEmpty(coachName) && dataList != null) {
			CourseCoachCalendarDto dto = dataList.get(0);
			coachName = dto.getCoachName();
			parameters.put("coachName", coachName + "");
		}
		return JasperUtil.exportByJavaBeanConnection(parentjasperPath, parameters, dataList, fileType);
	}

	@Override
	@Transactional
	public void createFlexPaymentReport(Date dateTime) throws Exception {
		if (null == dateTime) {
			dateTime = new Date();
		}
		String csvPath = appProps.getProperty("flex.account.report");
		String tempPath = appProps.getProperty("flex.account.report.tmp");
		File tempFile = new File(tempPath);
		if (!tempFile.exists()) {
			tempFile.mkdirs();
		}
		String tempFilePath = tempPath + "/S" + DateConvertUtil.parseDate2String(dateTime, "yyMMddhhmmss") + ".csv";
//		JasperUtil.saveCSVFilePath(tempFilePath, setCSVContent(dateTime));
		
		File file=new File(tempFilePath);
		JasperUtil.writerContent(file,convertCSVContent(dateTime).toString());
		// save csv Directory ,copy other Directory ,delete old csv file
		tempFile = new File(tempFilePath);
		logger.info("PaymentFacilityReport filePath:"+csvPath+"/"+tempFile.getName());
		this.copyTempFileToCsvPath(tempFile, csvPath);
	}
	private StringBuilder convertCSVContent(Date dateTime){
		StringBuilder sber=new StringBuilder();
		char r='\r';
		List<PaymentFacilityDto> dtoList = this
				.getPaymentFacilityList(DateConvertUtil.parseDate2String(dateTime, "yyyy-MM-dd HH:mm:ss"));
		//key:itemCatagory+'-'+PaymentMethodCode  value=account
		Map<String, BigDecimal> mapItemCategory=this.mapItemCategory(dtoList);
		
		String row="\"Date:\""+","+
				"\""+DateConvertUtil.parseDate2String(dateTime, "MM/dd/yyyy")+"\""+","+r;
		sber.append(row);
		row="\"TRN_CODE\""+","+"\"Description\""+","+"\"G/L\""+r;
		sber.append(row);
		//get status is act itemPaymentAccCode
		List<ItemPaymentAccCode> items=itemPaymentAccCodeDao.getItemPaymentAccCodeByStatus("ACT",null);
		for (ItemPaymentAccCode item : items) {
			String key=item.getCategoryCode()+'-'+item.getPaymentMethodCode();
			BigDecimal account=mapItemCategory.get(key);
			row="\""+item.getAccountCode()+"\""+","+
					"\""+(item.getDescription() == null ? "" : item.getDescription() )+"\""+","+
					"\""+(null!=account?account.toString():"0")+"\""+r;
			sber.append(row);
		}
		return sber;
	}
	/***
	 * key : Category +'-'+PaymentMethodCode
	 * @param dtoList
	 * @return
	 */
	private Map<String, BigDecimal> mapItemCategory(List<PaymentFacilityDto> dtoList){
		Map<String, BigDecimal> map=new HashMap<>();
		for (PaymentFacilityDto dto : dtoList) {
			String key=dto.getItemCatagory()+"-"+dto.getPaymentMethodCode();
			map.put(key, dto.getAmount());
		}
		return map;
		
	}
	private void copyTempFileToCsvPath(File tempFile, String csvPath) {
		if (tempFile.exists()) {
			File csv = new File(csvPath);
			if (!csv.exists()) {
				csv.mkdirs();
			}
			csv = new File(csvPath + "/" + tempFile.getName());
			try {
				FileCopyUtils.copy(tempFile, csv);
				tempFile.delete();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage(), e);
			}
		}

	}

	@Override
	@Transactional
	public List<PaymentFacilityDto> getPaymentFacilityList(String dateTime) {
		// TODO Auto-generated method stub
		StringBuilder sqlBer = new StringBuilder();
		sqlBer.append(this.createGolfBayPaymentSQL(dateTime));
		sqlBer.append(" UNION ALL  \n ");
		sqlBer.append(this.createAdmissionSalesSQL(dateTime));
		sqlBer.append(" UNION ALL  \n ");
		sqlBer.append(this.createDayPassSalesSQL(dateTime));
		sqlBer.append(" UNION ALL  \n ");
		sqlBer.append(this.createApprovedRefundSQL(dateTime));
		sqlBer.append(" UNION ALL  \n ");
		sqlBer.append(this.createTopUpSql(dateTime));
		
		List<PaymentFacilityDto> dtos=customerOrderTransDao.getDtoBySql(sqlBer.toString(), null, PaymentFacilityDto.class);
		if(null==dtos){
			dtos=new  ArrayList<>();
		}
		//add cashValue
		//dtos.add(this.getCashValueByDate(dateTime));
		return dtos;
	}

	/***
	 * Golf bay/Tennis court/Golf/Tennis course /Golf/tennis coach
	 * 
	 * @param dateTime
	 * @return
	 */
	private String createGolfBayPaymentSQL(String dateTime) {
		StringBuilder bdSql = new StringBuilder();
		bdSql.append("SELECT CASE WHEN trans.from_transaction_no IS NULL THEN trans.payment_method_code ELSE item.item_catagory END AS paymentMethodCode,\n");
		bdSql.append("CASE WHEN trans.from_transaction_no IS NULL THEN item.item_catagory ELSE fromItem.item_catagory END AS itemCatagory, SUM(trans.paid_amount) AS amount,\n");
		bdSql.append("CASE \n");
		bdSql.append("WHEN item.item_catagory='GFBK' THEN  CONCAT('Golfing Bay', ' ', trans.payment_method_code ) \n");
		bdSql.append("WHEN item.item_catagory='TFBK' THEN CONCAT('Tennis Court', ' ', trans.payment_method_code ) \n");
		bdSql.append(
				"WHEN item.item_catagory='GS'   THEN  CONCAT('Golf Private Coaching', ' ', trans.payment_method_code )\n");
		bdSql.append(
				"WHEN item.item_catagory='TS'   THEN CONCAT('Tennis Private Coaching', ' ', trans.payment_method_code )\n");
		bdSql.append("WHEN item.item_catagory='GSS'  THEN  CONCAT('Golf Course', ' ', trans.payment_method_code )\n");
		bdSql.append("WHEN item.item_catagory='TSS'  THEN CONCAT('Tennis Course', ' ', trans.payment_method_code )\n");
		bdSql.append("WHEN item.item_catagory='REFUND'  THEN CONCAT(CASE fromItem.item_catagory WHEN 'GFBK' THEN 'Golfing Bay' WHEN 'TFBK' THEN 'Tennis Court' WHEN 'GS' THEN 'Golf Private Coaching' WHEN 'TS' THEN 'Tennis Private Coaching' WHEN 'GSS' THEN 'Golf Course' WHEN 'TSS' THEN 'Tennis Course' WHEN 'DP' THEN 'Day Pass' END , ' ', 'REFUND' )\n");
		bdSql.append("END AS category\n");
		bdSql.append("FROM  customer_order_trans  AS trans \n");
		bdSql.append("LEFT JOIN customer_order_hd  AS coh ON coh.order_no=trans.order_no \n");
		bdSql.append("RIGHT JOIN customer_order_det AS cod ON coh.order_no=cod.order_no \n");
		bdSql.append("INNER JOIN pos_service_item_price AS item ON cod.item_no=item.item_no \n");
		bdSql.append("LEFT JOIN customer_order_trans AS fromTrans ON trans.from_transaction_no = fromTrans.transaction_no \n");
		bdSql.append("LEFT JOIN customer_order_hd  AS fromCoh ON fromCoh.order_no=fromTrans.order_no \n");
		bdSql.append("LEFT JOIN customer_order_det AS fromCod ON fromCoh.order_no=fromCod.order_no \n");
		bdSql.append("LEFT JOIN pos_service_item_price AS fromItem ON fromCod.item_no=fromItem.item_no \n");
		bdSql.append("WHERE  \n");
		bdSql.append(
				" (trans.status='SUC' OR trans.status='RFU') AND (item.item_catagory='GFBK' OR item.item_catagory='TFBK' OR item.item_catagory='GS' OR item.item_catagory='GSS' OR  item.item_catagory='TS' OR  item.item_catagory='TSS'  OR  item.item_catagory='REFUND' ) \n");
		bdSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = DATE_FORMAT('" + dateTime
				+ "','%Y-%m-%d')\n");
		bdSql.append(" GROUP BY category ");

		return bdSql.toString();

	}

	private String createAdmissionSalesSQL(String dateTime) {
		StringBuilder bdSql = new StringBuilder();
		bdSql.append("SELECT \n ");
		bdSql.append("trans.payment_method_code AS paymentMethodCode, \n ");
		bdSql.append("'SRV' AS itemCatagory, \n ");
		bdSql.append(" SUM(trans.paid_amount) AS amount, \n ");
		bdSql.append(" trans.payment_method_code AS category \n ");
		bdSql.append(" FROM customer_order_trans AS trans \n ");
		bdSql.append(" LEFT JOIN customer_order_hd AS coh ON coh.order_no = trans.order_no \n ");
		bdSql.append(" WHERE \n ");
		bdSql.append("  EXISTS(SELECT 1 FROM customer_order_det d LEFT JOIN  pos_service_item_price p  ON d.item_no=p.item_no  AND p.item_catagory='SRV'  WHERE  p.item_catagory='SRV' AND d.order_no=coh.order_no )");
		bdSql.append(" AND trans. STATUS = 'SUC' \n ");
		bdSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = DATE_FORMAT('"+dateTime+"','%Y-%m-%d') \n ");
		bdSql.append("GROUP BY category \n ");

		return bdSql.toString();

	}
	private String createDayPassSalesSQL(String dateTime) {
		StringBuilder bdSql = new StringBuilder();
		bdSql.append("SELECT \n ");
		bdSql.append("trans.payment_method_code AS paymentMethodCode, \n ");
		bdSql.append("'DP' AS itemCatagory, \n ");
		bdSql.append("SUM(trans.paid_amount) AS amount, \n ");
		bdSql.append("trans.payment_method_code  AS category \n ");
		bdSql.append("FROM customer_order_trans AS trans \n ");
		bdSql.append("LEFT JOIN customer_order_hd AS coh ON coh.order_no = trans.order_no \n ");
		bdSql.append("WHERE \n ");
		bdSql.append(" EXISTS(SELECT 1 FROM customer_order_det d LEFT JOIN  pos_service_item_price p  ON d.item_no=p.item_no  AND p.item_catagory='DP'  WHERE  p.item_catagory='DP' AND d.order_no=coh.order_no )");
		bdSql.append(" AND trans. STATUS = 'SUC' \n ");
		bdSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = DATE_FORMAT('"+dateTime+"','%Y-%m-%d') \n ");
		bdSql.append("GROUP BY category \n ");

		return bdSql.toString();

	}
	private String createApprovedRefundSQL(String dateTime) {
		StringBuilder bdSql = new StringBuilder();
		bdSql.append(" SELECT paymentMethodCode,itemCatagory,SUM(amount),itemCatagory FROM ( \n");
		bdSql.append(" SELECT  'REFUND' AS paymentMethodCode, \n");
		bdSql.append(" 'REFUND' AS itemCatagory, SUM(ref.approved_amt) AS amount,  \n");
		bdSql.append(" 'Approved Refund'  \n");
		bdSql.append(" AS category  \n");
		bdSql.append(" FROM  customer_order_trans  AS trans  \n");
		bdSql.append(
				" RIGHT JOIN customer_refund_request AS ref ON trans.transaction_no=ref.refund_transaction_no  \n");
		bdSql.append(" LEFT JOIN customer_order_hd  AS coh ON coh.order_no=trans.order_no  \n");
		bdSql.append(" RIGHT JOIN customer_order_det AS cod ON coh.order_no=cod.order_no  \n");
		bdSql.append(" INNER JOIN pos_service_item_price AS item ON cod.item_no=item.item_no  \n");
		bdSql.append(" WHERE (trans.status='SUC') AND ref.status='APR'  \n");
		bdSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = DATE_FORMAT('" + dateTime
				+ "','%Y-%m-%d')  \n");
		bdSql.append(" UNION ALL  \n");
		
		bdSql.append(" SELECT  'REFUND' AS paymentMethodCode, \n");
		bdSql.append("	'REFUND' AS itemCatagory, SUM(trans.paid_amount) AS amount, \n");
		bdSql.append(" 'Approved Refund' AS category  \n");
		bdSql.append(" FROM  customer_order_trans  AS trans \n");
		bdSql.append(" LEFT JOIN customer_order_hd  AS coh ON coh.order_no=trans.order_no \n");
		bdSql.append(" RIGHT JOIN customer_order_det AS cod ON coh.order_no=cod.order_no \n");
		bdSql.append(" INNER JOIN pos_service_item_price AS item ON cod.item_no=item.item_no \n");
		bdSql.append(" WHERE \n");
		bdSql.append(" (trans.status='SUC')  AND (item.item_catagory='CREDIT' OR  item.item_catagory='DEBIT' ) \n");
		bdSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') =DATE_FORMAT('" + dateTime
				+ "','%Y-%m-%d')  \n");
		bdSql.append(" ) AS transTable  \n");
		bdSql.append(" GROUP BY category  \n");

		return bdSql.toString();
	}
	
	private  PaymentFacilityDto getCashValueByDate(String dateTime) {
		BigDecimal totalTopUpAmount=(null==customerOrderTransDao.getUniqueBySQL(createTopUpSql(dateTime), null)?BigDecimal.ZERO:(BigDecimal)customerOrderTransDao.getUniqueBySQL(createTopUpSql(dateTime), null));
		BigDecimal cancelAmount=(null==customerOrderTransDao.getUniqueBySQL(createCancelBookingSql(dateTime), null))?BigDecimal.ZERO:(BigDecimal)customerOrderTransDao.getUniqueBySQL(createCancelBookingSql(dateTime), null);
		BigDecimal bookingAmount=(null==customerOrderTransDao.getUniqueBySQL(createBookingSql(dateTime), null)?BigDecimal.ZERO:(BigDecimal)customerOrderTransDao.getUniqueBySQL(createBookingSql(dateTime), null));
		BigDecimal outCashAmount=(null==customerOrderTransDao.getUniqueBySQL(createCashOutCashValueSql(dateTime), null)?BigDecimal.ZERO:(BigDecimal)customerOrderTransDao.getUniqueBySQL(createCashOutCashValueSql(dateTime), null));
		
		BigDecimal amount=totalTopUpAmount.add(cancelAmount).subtract(bookingAmount).subtract(outCashAmount);
		
		PaymentFacilityDto dto=new PaymentFacilityDto();
		dto.setItemCatagory(Constant.CASH_Value);
		dto.setPaymentMethodCode(Constant.CASH_Value);
		dto.setAmount(null==amount?BigDecimal.ZERO:amount);
		return dto;
	}
	
	private String createTopUpSql(String dateTime){
		StringBuilder topUpSql = new StringBuilder();
		topUpSql.append("SELECT	'CASHVALUE' AS paymentMethodCode,\n");
		topUpSql.append(" 'TOPUP' AS itemCatagory,\n");
		topUpSql.append(" sum(trans.paid_amount) AS amount,\n");
		topUpSql.append(" 'TOPUP' AS category \n");
		topUpSql.append(" FROM  customer_order_trans  AS trans \n");
		topUpSql.append("LEFT JOIN customer_order_hd  AS coh ON coh.order_no=trans.order_no \n");
		topUpSql.append("WHERE (trans.status='SUC') \n");
		topUpSql.append("AND EXISTS(SELECT 1 FROM customer_order_det d LEFT JOIN  pos_service_item_price p  ON d.item_no=p.item_no  WHERE  p.item_catagory='TOPUP' AND d.order_no=coh.order_no )\n");
		topUpSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = DATE_FORMAT('"+dateTime+"','%Y-%m-%d') \n");
		return topUpSql.toString();
	}
	private String createCancelBookingSql(String dateTime){
		StringBuilder cancelSql = new StringBuilder();
		cancelSql.append("SELECT  SUM(trans.paid_amount) AS amount	\n");
		cancelSql.append("FROM  customer_order_trans  AS trans \n");
		cancelSql.append(" WHERE trans.status='RFU' \n");
		cancelSql.append(" AND trans.payment_method_code='CASHVALUE'  AND  trans.from_transaction_no IS NOT NULL  \n");
		cancelSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = DATE_FORMAT('"+dateTime+"','%Y-%m-%d') \n");
		return cancelSql.toString();
	}
	private String createBookingSql(String dateTime){
		StringBuilder bookSql = new StringBuilder();
		bookSql.append("SELECT  SUM(trans.paid_amount) AS amount	\n");
		bookSql.append("FROM  customer_order_trans  AS trans \n");
		bookSql.append(" WHERE (trans.status='SUC')  \n");
		bookSql.append(" AND trans.payment_method_code='CASHVALUE'  \n");
		bookSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = DATE_FORMAT('"+dateTime+"','%Y-%m-%d') \n");
		return bookSql.toString();
	}
	private String createCashOutCashValueSql(String dateTime){
		StringBuilder outSql = new StringBuilder();
		outSql.append("SELECT  SUM(trans.paid_amount) AS amount	\n");
		outSql.append("FROM  customer_order_trans  AS trans \n");
		outSql.append(" WHERE trans.status='RFU'   \n");
		outSql.append(" AND trans.payment_method_code='CASHVALUE' AND trans.from_transaction_no IS NULL   \n");
		outSql.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = DATE_FORMAT('"+dateTime+"','%Y-%m-%d') \n");
		return outSql.toString();
	}

	@Override
	public byte[] getCustomerOrderTransAttachment(String startDate, String endDate, String fileType, String sortBy,
			String isAscending) {
		try {
			return customerOrderTransDao.getCustomerOrderTransAttachment(startDate, endDate, fileType, sortBy, isAscending);
		} catch (JRException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public StringBuilder getCustomerOrderTransSQL() {
		//CustomerOrderTransDto
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT item.description AS facilityType,");
		sql.append(" trans.transaction_no as transactionNo,trans.order_no as orderNo,trans.payment_method_code as paymentMethodCode,");
		sql.append(" trans.payment_media as paymentMedia,DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d %h:%i:%s') as transactionTimestamp,trans.paid_amount as paidAmount,trans.status as status,");
		sql.append(" trans.payment_card_no as paymentCardNo,trans.payment_location_code as paymentLocationCode ");
		sql.append(" FROM customer_order_trans AS trans ");
		sql.append(" LEFT JOIN customer_order_det AS cod ON trans.order_no=cod.order_no");
		sql.append(" LEFT JOIN pos_service_item_price AS item ON item.item_no=cod.item_no"); 
		sql.append(" WHERE 1=1 ");
		
		return sql;
	}

	@Override
	public String getCashValueOasis(String date) {
		String sql = "SELECT  "
				+ " CONCAT(trans.transaction_timestamp,'') as transactionDateStr,"
				+ " trans.transaction_timestamp as transactionDate,"
				+ " trans.transaction_no as transactionId,"
				+ " mem.academy_no as academyId,"
				+ " CONCAT_WS(' ', pro.salutation, pro.given_name, pro.surname ) AS memberName, "
				+ " trans.internal_remark AS remark,"
				+"  trans.paid_amount as transAmount ,"
				+"  CONCAT(trans.paid_amount,'') as transAmountStr ,"
				+" '"+PaymentMethod.CASHVALUE.getDesc()+"'as paymentMethod ,"
				+ " trans.status as transStatus "
				+ " FROM  customer_order_trans trans"
				+ " LEFT JOIN customer_order_hd hd  ON trans.order_no = hd.order_no "
				+ " LEFT JOIN customer_order_det det  ON hd.order_no = det.order_no  "
				+ " LEFT JOIN customer_profile pro  ON pro.customer_id = hd.customer_id  "
				+ " LEFT JOIN member mem  ON mem.customer_id = hd.customer_id  "
				+ " LEFT JOIN pos_service_item_price psip  ON psip.item_no = det.item_no"
				+ " WHERE ( (trans.status='SUC' and not exists (select 1  from customer_order_trans s  where s.status ='VOID' and s.from_transaction_no = trans.transaction_no)) or trans.status='VOID' ) and trans.payment_method_code='"+PaymentMethod.CASHVALUE.getCardCode()+"' and psip.item_no in ('"+Constant.PMSTHIRD_ITEM_NO+"' ,'"+Constant.POSTHIRD_ITEM_NO+"')";
		
				if (!StringUtils.isEmpty(date)) {
					 SimpleDateFormat s = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
					 Date time=null;
					try {
						time = s.parse(date);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					if(null!=time){
						sql = sql + " and DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') = '" +DateConvertUtil.parseDate2String(time,"yyyy-MM-dd")+"'";
					}
				}
				String joinSql = "SELECT * FROM (" + sql + " ) sub WHERE 1=1 ";
				
				return joinSql;
	}
	/**
	 * For Cash Value/Cash Value Txn In Oasis
	 * @param date
	 * @return
	 */
	@Override
	public String getCashValueTxnInOasis(String date) {
		String sql = "SELECT * FROM (SELECT  "
				+ " CONCAT(trans.transaction_timestamp,'') as transactionDateStr,"
				+ " CASE WHEN trans.status = 'VOID' THEN "
				+ " (select t.transaction_timestamp from customer_order_trans t where t.transaction_no = trans.from_transaction_no) " 
				+ "	ELSE trans.transaction_timestamp "
				+ " END as transactionDate, "
				+ " trans.transaction_no as transactionId,"
				+ " mem.academy_no as academyId,"
				+ " CONCAT_WS(' ', pro.salutation, pro.given_name, pro.surname ) AS memberName, "
				+ " trans.internal_remark AS remark,"
				+"  trans.paid_amount as transAmount ,"
				+"  CONCAT(trans.paid_amount,'') as transAmountStr ,"
				+" '"+PaymentMethod.CASHVALUE.getDesc()+"'as paymentMethod ,"
				+ " trans.status as transStatus "
				+ " FROM  customer_order_trans trans"
				+ " LEFT JOIN customer_order_hd hd  ON trans.order_no = hd.order_no "
				+ " LEFT JOIN customer_order_det det  ON hd.order_no = det.order_no  "
				+ " LEFT JOIN customer_profile pro  ON pro.customer_id = hd.customer_id  "
				+ " LEFT JOIN member mem  ON mem.customer_id = hd.customer_id  "
				+ " LEFT JOIN pos_service_item_price psip  ON psip.item_no = det.item_no"
				+ " WHERE ( (trans.status='SUC' and not exists (select 1  from customer_order_trans s  where s.status ='VOID' and s.from_transaction_no = trans.transaction_no)) or trans.status='VOID' ) and trans.payment_method_code='"+PaymentMethod.CASHVALUE.getCardCode()+"' and psip.item_no in ('"+Constant.PMSTHIRD_ITEM_NO+"' ,'"+Constant.POSTHIRD_ITEM_NO+"')) AS　ts ";
		
				if (!StringUtils.isEmpty(date)) {
					 SimpleDateFormat s = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
					 Date time=null;
					try {
						time = s.parse(date);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					if(null!=time){
						sql = sql + " WHERE  DATE_FORMAT(transactionDate,'%Y-%m-%d') = '" +DateConvertUtil.parseDate2String(time,"yyyy-MM-dd")+"'";
					}
				}
				String joinSql = "SELECT * FROM (" + sql + " ) sub WHERE 1=1 ";
				
				return joinSql;
	}
	
	@Transactional
	public ResponseResult getPatronActivitiesReport(ListPage page, String date,String academyID) {
		String sql = this.getActivitiesSqlByDate(date,academyID).toString();
		String countSql = "select count(1) from(" + sql + ") a";
		logger.debug("sql:"+sql );
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, new ArrayList(),new PatronActivitiesDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	private Object getActivitiesSqlByDate(String date, String academyID) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		// Golf bay/tennis court/golf coach/tennis coach
		sb.append("SELECT ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("CASE WHEN mb.resv_facility_type = 'GOLF' AND exp_coach_user_id is null THEN 'Golfing Bay' ");
		sb.append(" WHEN mb.resv_facility_type = 'GOLF' AND exp_coach_user_id is not null THEN 'Golf Private Coaching'  ");
		sb.append(" WHEN mb.resv_facility_type = 'TENNIS' AND exp_coach_user_id is null THEN 'Tennis Court' ");
		sb.append(" WHEN mb.resv_facility_type = 'TENNIS' AND exp_coach_user_id is not null THEN 'Tennis Private Coaching' END AS type, ");
		sb.append("CONCAT( ");
		sb.append("CASE WHEN  mb.resv_facility_type = 'GOLF' AND exp_coach_user_id is null THEN cap.caption ");
		sb.append(" WHEN  mb.resv_facility_type = 'TENNIS' AND exp_coach_user_id is null THEN ifnull(fst. NAME, cap.caption) ");
		sb.append(" WHEN  mb.resv_facility_type = 'GOLF' AND exp_coach_user_id is not null THEN CONCAT('Coach:',sp.given_name,' ',sp.surname) ");
		sb.append(" WHEN  mb.resv_facility_type = 'TENNIS' AND exp_coach_user_id is not null THEN CONCAT('Coach:',sp.given_name,' ',sp.surname) END, ");
		sb.append("CASE WHEN room_booking.room_resv_id IS NOT NULL THEN '(Bundled)' ELSE '' END) AS description, ");
		sb.append("CASE mb.status WHEN 'RSV' THEN 'Booking' WHEN 'CAN' THEN 'Cancel' WHEN 'ATN' THEN 'Checked In' END AS activities, ");
		sb.append("mb.begin_datetime_book as activitiesDateFrom, ");
		sb.append("mb.end_datetime_book as activitiesDateTo, ");
		sb.append("mb.facility_type_qty as units, ");
		sb.append("null as amount, ");
		sb.append("CASE mb.STATUS WHEN 'RSV' THEN mb.create_date WHEN 'CAN' THEN mb.update_date WHEN 'ATN' THEN mb.update_date END AS activitiesDate ");
		sb.append("FROM ");
		sb.append("customer_profile cp ");
		sb.append("JOIN member m on cp.customer_id = m.customer_id ");
		sb.append("JOIN ");
		sb.append("(SELECT * FROM member_facility_type_booking mb1 WHERE mb1.status IN ('RSV') and DATE_FORMAT(mb1.create_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append("UNION ");
		sb.append("SELECT * FROM member_facility_type_booking mb2 WHERE mb2.status IN ('CAN','ATN') and DATE_FORMAT(mb2.update_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append(") mb on m.customer_id = mb.customer_id ");
		sb.append("LEFT JOIN member_facility_book_addition_attr attr ON mb.resv_id = attr.resv_id ");
		sb.append("LEFT JOIN facility_attribute_caption cap ON cap.attribute_id = attr.attribute_id ");
		sb.append("LEFT JOIN facility_sub_type fst ON fst.subtype_id = mb.facility_subtype_id ");
		sb.append("LEFT JOIN room_facility_type_booking room_booking on room_booking.facility_type_resv_id = mb.resv_id and room_booking.is_bundle = 'Y' ");
		sb.append("LEFT JOIN staff_profile sp on mb.exp_coach_user_id = sp.user_id ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append("WHERE m.academy_no = '" + academyID + "' ");
		}
		sb.append("UNION ALL ");
		// Course REG/CAN
		sb.append("SELECT ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("CASE cm.course_type WHEN 'GSS' THEN 'Golf Course' WHEN 'TSS' THEN 'Tennis Course' END AS type, ");
		sb.append("cm.course_name AS description, ");
		sb.append("CASE ce.status WHEN 'REG' THEN 'Register' WHEN 'CAN' THEN 'Cancel' END AS activities, ");
		sb.append("ce.enroll_date as activitiesDateFrom, ");
		sb.append("ce.enroll_date as activitiesDateTo, ");
		sb.append("1 as units, ");
		sb.append("null as amount, ");
		sb.append("CASE ce.STATUS WHEN 'REG' THEN ce.create_date WHEN 'CAN' THEN ce.update_date END AS activitiesDate ");
		sb.append("FROM ");
		sb.append("customer_profile cp ");
		sb.append("JOIN member m on cp.customer_id = m.customer_id ");
		sb.append("JOIN ( ");
		sb.append("SELECT * FROM course_enrollment ce1 WHERE ce1.status = 'REG' and DATE_FORMAT(ce1.create_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append("UNION ");
		sb.append("SELECT * FROM course_enrollment ce2 WHERE ce2.status = 'CAN' and DATE_FORMAT(ce2.update_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append(") ce on cp.customer_id = ce.customer_id ");
		sb.append("LEFT JOIN course_master cm on ce.course_id = cm.course_id ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append("WHERE  m.academy_no = '" + academyID + "' ");
		}
		sb.append("UNION ALL ");
		// Course Attend
		sb.append("SELECT ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("CASE cm.course_type WHEN 'GSS' THEN 'Golf Course' WHEN 'TSS' THEN 'Tennis Course' END AS type, ");
		sb.append("concat('Lesson ', cs.session_no, ' of ', cm.course_name) AS description, ");
		sb.append("'Attended' AS activities, ");
		sb.append("attendance.update_date as activitiesDateFrom, ");
		sb.append("attendance.update_date as activitiesDateTo, ");
		sb.append("1 as units, ");
		sb.append("null as amount, ");
		sb.append("attendance.update_date AS activitiesDate ");
		sb.append("FROM ");
		sb.append("customer_profile cp ");
		sb.append("JOIN member m on cp.customer_id = m.customer_id ");
		sb.append("JOIN course_enrollment ce on cp.customer_id = ce.customer_id AND ce. STATUS != 'PND' ");
		sb.append("JOIN course_master cm on ce.course_id = cm.course_id ");
		sb.append("JOIN course_session cs ON cs.course_id = cm.course_id AND (cs. STATUS = 'ACT' OR cs. STATUS = 'CAN') ");
		sb.append("JOIN student_course_attendance attendance ON attendance.course_session_id = cs.sys_id and attendance.status='ATD' ");
		sb.append("WHERE DATE_FORMAT(attendance.update_date,'%Y-%m-%d') = '"+date+"' ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append(" AND m.academy_no = '" + academyID + "' ");
		}
		sb.append("UNION ALL ");
		// Restaurant booking
		sb.append("SELECT ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("'Restaurant' AS type, ");
		sb.append("rm.restaurant_name AS description, ");
		sb.append("CASE booking.status WHEN 'WAIT' THEN 'Booking' WHEN 'CAN' THEN 'Cancel' WHEN 'ATN' THEN 'ATTENDED' END AS activities, ");
		sb.append("booking.book_time as activitiesDateFrom, ");
		sb.append("booking.book_time as activitiesDateTo, ");
		sb.append("1 as units, ");
		sb.append("null as amount, ");
		sb.append("CASE booking.status WHEN 'WAIT' THEN booking.create_date WHEN 'CAN' THEN booking.update_date WHEN 'ATN' THEN booking.update_date END AS activitiesDate ");
		sb.append("FROM ");
		sb.append(" customer_profile cp ");
		sb.append(" JOIN member m on cp.customer_id = m.customer_id ");
		sb.append(" JOIN ( ");
		sb.append("SELECT * FROM restaurant_customer_booking booking WHERE booking.status in ('WAIT') and DATE_FORMAT(booking.create_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append("UNION ");
		sb.append("SELECT * FROM restaurant_customer_booking booking WHERE booking.status in ('CAN','ATN') and DATE_FORMAT(booking.update_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append(" )booking ON cp.customer_id = 5 ");
		sb.append(" LEFT JOIN restaurant_master rm ON booking.restaurant_id = rm.restaurant_id ");
		sb.append(" WHERE booking.status IN ('WAIT','CAN','ATN') ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append(" AND m.academy_no = '" + academyID + "' ");
		}
		sb.append("UNION ALL ");
		// Room reservation
		sb.append("SELECT ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("'Accommodation' AS type, ");
		sb.append("r.type_name AS description, ");
		sb.append("CASE rc.status WHEN 'SUC' THEN 'Booking' WHEN 'CKI' THEN 'Check In' WHEN 'CKO' THEN 'Check Out' WHEN 'CAN' THEN 'Cancel' END AS activities, ");
		sb.append("rc.update_date as activitiesDateFrom, ");
		sb.append("rc.update_date as activitiesDateTo, ");
		sb.append("1 as units, ");
		sb.append("null as amount, ");
		sb.append("rc.update_date AS activitiesDate ");
		sb.append("FROM ");
		sb.append("customer_profile cp ");
		sb.append("JOIN member m ON cp.customer_id = m.customer_id ");
		sb.append("JOIN room_reservation_rec rc ON cp.customer_id = rc.customer_id AND DATE_FORMAT(rc.update_date,'%Y-%m-%d') = '"+date+"'  ");
		sb.append("LEFT JOIN room_type r ON r.type_code = rc.room_type_code ");
		sb.append("WHERE rc.status IN('SUC', 'CKI', 'CKO', 'CAN') ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append("and m.academy_no = '" + academyID + "' ");
		}
		sb.append("UNION ALL ");
		// Wellness reservation
		sb.append("SELECT ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("'Wellness' AS type, ");
		sb.append("rec.service_name AS description, ");
		sb.append("CASE rec.status WHEN 'RSV' THEN 'Booking' WHEN 'CAN' THEN 'Cancel' WHEN 'CLD' THEN 'Closed' END AS activities, ");
		sb.append("rec.start_datetime as activitiesDateFrom, ");
		sb.append("rec.end_datetime as activitiesDateTo, ");
		sb.append("1 as units, ");
		sb.append("null as amount, ");
		sb.append("CASE rec.status WHEN 'RSV' THEN rec.create_date WHEN 'CAN' THEN rec.update_date WHEN 'CLD' THEN rec.update_date END AS activitiesDate ");
		sb.append("FROM ");
		sb.append("customer_profile cp ");
		sb.append("JOIN member m ON cp.customer_id = m.customer_id ");
		sb.append("JOIN ( ");
		sb.append("SELECT * FROM spa_appointment_rec rec1 WHERE rec1.status = 'RSV' AND DATE_FORMAT(rec1.create_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append("UNION ");
		sb.append("SELECT * FROM spa_appointment_rec rec2 WHERE rec2.status in ('CAN','CLD') AND DATE_FORMAT(rec2.update_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append(") rec ON cp.customer_id = rec.customer_id ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append("WHERE m.academy_no = '" + academyID + "' ");
		}
		sb.append("UNION ALL ");
		// Day pass purchase/cancel
		sb.append("SELECT ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("'Day pass' AS type, ");
		sb.append("'Visitor pass' AS description, ");
		sb.append("CASE card.status WHEN 'CMP' THEN 'Purchase' WHEN 'CAN' THEN 'Cancel' END AS activities, ");
		sb.append("card.effective_from as activitiesDateFrom, ");
		sb.append("card.effective_to as activitiesDateTo, ");
		sb.append("1 as units, ");
		sb.append("null as amount, ");
		sb.append("card.activitiesDate AS activitiesDate ");
		sb.append("FROM ");
		sb.append("customer_profile cp ");
		sb.append("JOIN member m ON cp.customer_id = m.customer_id ");
		sb.append("JOIN ( ");
		sb.append("SELECT card1.effective_from as effective_from, card1.effective_to as effective_to, hd1.order_status as status, hd1.customer_id as customer_id,hd1.create_date AS activitiesDate FROM customer_order_permit_card card1 JOIN customer_order_hd hd1 on card1.customer_order_no = hd1.order_no WHERE hd1.order_status = 'CMP' AND DATE_FORMAT(hd1.create_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append("UNION ");
		sb.append("SELECT card2.effective_from as effective_from, card2.effective_to as effective_to, hd2.order_status as status, hd2.customer_id as customer_id,hd2.update_date as activitiesDate FROM customer_order_permit_card card2 JOIN customer_order_hd hd2 on card2.customer_order_no = hd2.order_no WHERE hd2.order_status = 'CAN' AND DATE_FORMAT(hd2.update_date,'%Y-%m-%d') = '"+date+"' ");
		sb.append(") card ON cp.customer_id = card.customer_id ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append("WHERE m.academy_no = '" + academyID + "' ");
		}
		sb.append("UNION ALL ");
		// Top Up
		sb.append("select  ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("'Top Up' AS type, ");
		sb.append("CONCAT('BY ', CASE WHEN c.payment_media = 'OP' AND c.payment_method_code IN ('VISA', 'MASTER', 'JCB') THEN CONCAT(c.payment_method_code,'(online)')ELSE c.payment_method_code END) AS description, ");
		sb.append("'Top Up' AS activities, ");
		sb.append("c.transaction_timestamp as activitiesDateFrom, ");
		sb.append("c.transaction_timestamp as activitiesDateTo, ");
		sb.append("null as units, ");
		sb.append("c.paid_amount as amount, ");
		sb.append("c.transaction_timestamp AS activitiesDate ");
		sb.append("FROM ");
		sb.append("customer_order_trans c, ");
		sb.append("customer_order_hd h, ");
		sb.append("customer_profile p, ");
		sb.append("customer_order_det d, ");
		sb.append("member m, ");
		sb.append("customer_profile cp ");
		sb.append("WHERE ");
		sb.append("c.order_no = h.order_no ");
		sb.append("AND h.order_no = d.order_no ");
		sb.append("AND d.item_no = 'CVT0000001' ");
		sb.append("AND h.customer_id = m.customer_id ");
		sb.append("AND m.customer_id = cp.customer_id ");
		sb.append("AND c. STATUS = 'SUC' ");
		sb.append("AND DATE_FORMAT( ");
		sb.append("c.transaction_timestamp, ");
		sb.append("'%Y-%m-%d' ");
		sb.append(") = '"+date+"' ");
		sb.append("AND m.customer_id = p.customer_id ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append("AND m.academy_no = '" + academyID + "' ");
		}
		sb.append("AND d.order_det_id = ( ");
		sb.append("SELECT ");
		sb.append("min(dd.order_det_id) ");
		sb.append("FROM ");
		sb.append("customer_order_det dd ");
		sb.append("WHERE ");
		sb.append("dd.order_no = h.order_no ");
		sb.append(") ");
		sb.append("UNION ALL ");
		//POS AND PMS
		sb.append("select  ");
		sb.append("m.academy_no as academyId, ");
		sb.append("CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append("CASE pos.item_catagory WHEN 'RESTAURANT' THEN 'RESTAURANT' WHEN 'OASIS_ROOM' THEN 'Room Charges' END AS type, ");
		sb.append("c.internal_remark AS description, ");
		sb.append("CONCAT('Pay by ',c.payment_method_code) AS activities, ");
		sb.append("c.transaction_timestamp as activitiesDateFrom, ");
		sb.append("c.transaction_timestamp as activitiesDateTo, ");
		sb.append("null as units, ");
		sb.append("c.paid_amount as amount, ");
		sb.append("c.transaction_timestamp AS activitiesDate ");
		sb.append("FROM ");
		sb.append("customer_order_trans c, ");
		sb.append("customer_order_hd h, ");
		sb.append("customer_profile p, ");
		sb.append("customer_order_det d, ");
		sb.append("member m, ");
		sb.append("customer_profile cp, ");
		sb.append("pos_service_item_price pos ");
		sb.append("WHERE ");
		sb.append("c.order_no = h.order_no ");
		sb.append("AND h.order_no = d.order_no ");
		sb.append("AND h.customer_id = m.customer_id ");
		sb.append("AND m.customer_id = cp.customer_id ");
		sb.append("AND c.status = 'SUC' ");
		sb.append("AND DATE_FORMAT(c.transaction_timestamp,'%Y-%m-%d') = '"+date+"' ");
		sb.append("AND m.customer_id = p.customer_id ");
		sb.append("AND d.item_no = pos.item_no ");
		if(!StringUtils.isEmpty(academyID)){
			sb.append("AND m.academy_no = '" + academyID + "' ");
		}
		sb.append("AND pos.item_catagory IN ('RESTAURANT', 'OASIS_ROOM') ");
		return sb.toString();
	}

	@Transactional
	@Override
	public byte[] getPatronActivitiesAttachement(ListPage page,String date,String fileType,String academyID) throws  Exception{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String jasperPath = reportPath + "PatronActivities.jasper";


		List<Object> param = new ArrayList<Object>();
		String sql = this.getActivitiesSqlByDate(date,academyID).toString();
		
		sql = customerOrderTransDao.addOrderBy(page, sql);
		logger.debug("sql:"+sql );
		List<PatronActivitiesDto> dataList = customerOrderTransDao.getDtoBySql(sql, param,PatronActivitiesDto.class);
		Map<String, Object> parameters = new HashMap();
		parameters.put("activityDate", date);
		return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);
	}
	
	@Transactional
	public ResponseResult getCashValueVoidReport(ListPage page, String date) {
		String sql = this.getCashValueOasis(date);
		String countSql = "select count(1) from(" + sql + ") a";
		logger.debug("sql:"+sql );
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, new ArrayList(),new CashValueLogDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	
	@Transactional
	@Override
	public byte[] getCashValueVoidAttachement(ListPage page,String date,String fileType) throws  Exception{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String jasperPath = reportPath + "CashValueVoid.jasper";


		List<Object> param = new ArrayList<Object>();
		String sql = this.getCashValueOasis(date);
		
		sql = customerOrderTransDao.addOrderBy(page, sql);
		logger.debug("sql:"+sql );
		List<CashValueLogDto> dataList = customerOrderTransDao.getDtoBySql(sql, param,CashValueLogDto.class);
		Map<String, Object> parameters = new HashMap();
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
		Date time = null;
		try{
			time = s.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		parameters.put("txnDate", DateConvertUtil.parseDate2String(time,"yyyy-MM-dd"));
		return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);
	}
	
	@Override
	@Transactional
	public byte[] generateGuestRoomResvAttach(Long transactionNo) {
		return getInvoiceReceipt(null, transactionNo.toString(), "guestroombook");
	}
	/**
	 * 根据时间获取serviceRefund报表
	 * @param startTime
	 * @param endTime
	 * @param location
	 * @param fileType
	 * @return
	 */
	@Override
	@Transactional
	public byte[]  getServiceRefundReports(String startTime, String endTime,  String fileType){
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "ServiceRefundReports.jasper";
			List<CustomerRefundRequestDto> dataList = customerRefundRequestDao.getServiceRefundList(startTime, endTime);
			Map<String, Object> parameters = new HashMap();
			parameters.put("startDate", startTime);
			parameters.put("endDate", endTime);
			parameters.put("sumQty", dataList.size());
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据时间获取CashValueAdjustment报表
	 * @param startTime
	 * @param endTime
	 * @param location
	 * @param fileType
	 * @return
	 */
	@Override
	@Transactional
	public byte[]  getCashValueAdjustmentReports(String type, Long customerId, String startTime, String endTime,  String fileType){
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "CashValueAdjustmentReports.jasper";
			List<CashValueLogDto> dataList = customerOrderTransDao.getCashValueChangesLog(type, customerId, startTime, endTime);
			Map<String, Object> parameters = new HashMap();
			parameters.put("startDate", startTime);
			parameters.put("endDate", endTime);
			parameters.put("sumQty", dataList.size());
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@Transactional
	public ResponseResult generateStatementByAcademyNo(String year, String month,String academyNo) 
	{
		String startDate=null;
		String endDate=null;
		
		if(!StringUtils.isEmpty(academyNo))
		{
			startDate= year+"-"+month;
			int mon = Integer.parseInt(month);
			if (mon < 10) {
				startDate  = year+"-0"+month;
			}
			try {
				startDate=DateCalcUtil.getMonthStartDate(startDate);
				endDate=DateCalcUtil.getMonthEndDate(startDate);
				
			} catch (ParseException e) {
				logger.error(e.getMessage());
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR);
				return responseResult;
			}
			
			Member member=memberDao.getMemberByAcademyNo(academyNo);
			if(null!=member&&!StringUtils.isEmpty(startDate)&&!StringUtils.isEmpty(endDate))
			{
				try{
					/***
					 * when staff  regenerate report need to excute calculateClosingBalance
					 * @author christ
					 * @date 2018-11-01
					 */
					List<MemberDto> memberDtos=new ArrayList<>();
					MemberDto dto=new MemberDto();
					dto.setCustomerId(member.getCustomerId());
					memberDtos.add(dto);
					calculateClosingBalance(memberDtos, startDate, endDate);
					genrateStatementByCustomerId(member.getCustomerId(), startDate,endDate);
					
					responseResult.initResult(GTAError.Success.SUCCESS);
				}catch(Exception ex)
				{
					logger.error(ex.getMessage());
					responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR);
				}
			}else{
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR,"Don't find member by academyNo"+academyNo);
			}
		}
		return responseResult;
	}

	@Override
	public String createRestaurantSettlingSql(String startTime, String endTime) {
		StringBuilder sqlbd=new StringBuilder();
		sqlbd.append(" SELECT hd.customer_id AS customerId,COUNT(*)AS total ,'"+SurveyType.RESTAURANT.name()+"' AS surveyType FROM customer_order_trans AS trans");
		sqlbd.append(" LEFT JOIN customer_order_hd AS hd  ON trans.order_no=hd.order_no");
		sqlbd.append(" WHERE trans.status='SUC' AND trans.payment_method_code='CASHVALUE'");
		sqlbd.append(" AND trans.transaction_timestamp BETWEEN '"+startTime+"' AND '"+endTime+"'");
		sqlbd.append(" AND EXISTS(");
		sqlbd.append("  SELECT 1 FROM customer_order_det AS det ");
		sqlbd.append("  LEFT JOIN  pos_service_item_price AS item ON det.item_no=item.item_no");
		sqlbd.append("  WHERE det.order_no=trans.order_no AND  item.item_catagory='RESTAURANT'");
		sqlbd.append(" )");
		sqlbd.append("  AND hd.customer_id IS NOT NULL  GROUP BY hd.customer_id ");
		return sqlbd.toString();
	}
	@Override
	public String createRestaurantSettlingSql(Long customerId,String startTime, String endTime) {
		StringBuilder sqlbd=new StringBuilder();
		sqlbd.append(" SELECT hd.customer_id AS customerId,COUNT(*)AS total ,'"+SurveyType.RESTAURANT.name()+"' AS surveyType FROM customer_order_trans AS trans");
		sqlbd.append(" LEFT JOIN customer_order_hd AS hd  ON trans.order_no=hd.order_no");
		sqlbd.append(" WHERE trans.status='SUC' AND trans.payment_method_code='CASHVALUE'");
		sqlbd.append(" AND trans.transaction_timestamp BETWEEN '"+startTime+"' AND '"+endTime+"'");
		sqlbd.append(" AND EXISTS(");
		sqlbd.append("  SELECT 1 FROM customer_order_det AS det ");
		sqlbd.append("  LEFT JOIN  pos_service_item_price AS item ON det.item_no=item.item_no");
		sqlbd.append("  WHERE det.order_no=trans.order_no AND  item.item_catagory='RESTAURANT'");
		sqlbd.append(" )");
		sqlbd.append("  AND hd.customer_id IS NOT NULL  and  hd.customer_id ="+customerId+" ");
		return sqlbd.toString();
	}
	
	/**
	 * 获取用CashValue的MMS支付数据
	 * @param dateRange
	 * @return
	 * @throws Exception 
	 */
	@Override
	public String getCashValueByMMS(String dateRange) throws Exception {
		String sql = "SELECT * FROM (SELECT  "
				+ " CONCAT(trans.transaction_timestamp,'') as transactionDateStr,"
				+ " CASE WHEN trans.status = 'VOID' THEN "
				+ " (select t.transaction_timestamp from customer_order_trans t where t.transaction_no = trans.from_transaction_no) " 
				+ "	ELSE trans.transaction_timestamp "
				+ " END as transactionDate, "
				+ " trans.transaction_no as transactionId,"
				+ " mem.academy_no as academyId,"
				+ " CONCAT_WS(' ', pro.salutation, pro.given_name, pro.surname ) AS memberName, "
				+ " trans.internal_remark AS remark, "
				+ "  trans.paid_amount as transAmount , "
				+ "  CONCAT(trans.paid_amount,'') as transAmountStr , "
				+ " '"+PaymentMethod.CASHVALUE.getDesc()+"'as paymentMethod , "
				+ " trans.ext_ref_no AS invoiceNo, "
				+ " trans.status as transStatus "
				+ " FROM  customer_order_trans trans"
				+ " LEFT JOIN customer_order_hd hd  ON trans.order_no = hd.order_no "
//				+ " LEFT JOIN customer_order_det det  ON hd.order_no = det.order_no  AND (trans.`status` = 'VOID' OR det.create_date = trans.create_date) "
				+ " LEFT JOIN customer_profile pro  ON pro.customer_id = hd.customer_id  "
				+ " LEFT JOIN member mem  ON mem.customer_id = hd.customer_id  "
//				+ " LEFT JOIN pos_service_item_price psip  ON psip.item_no = det.item_no"
//				+ " LEFT JOIN spa_appointment_rec sar  ON sar.order_det_id = det.order_det_id"
				+ " WHERE ( (trans.status='SUC'"
				+ " and exists (select 1 from customer_order_det det where hd.order_no = det.order_no AND det.item_no = '"+Constant.MMSTHIRD_ITEM_NO+"') "
				+ " and not exists (select 1  from customer_order_trans s  where s.status ='VOID' and s.from_transaction_no = trans.transaction_no)) or trans.status='VOID' ) and trans.payment_method_code='"+PaymentMethod.CASHVALUE.getCardCode()+"'   AND hd.order_remark = 'SACV'  ) AS　ts ";
		
//				if (!StringUtils.isEmpty(date)) {
//					 SimpleDateFormat s = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
//					 Date time=null;
//					try {
//						time = s.parse(date);
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//					if(null!=time){
//						sql = sql + " WHERE  DATE_FORMAT(transactionDate,'%Y-%m-%d') = '" +DateConvertUtil.parseDate2String(time,"yyyy-MM-dd")+"'";
//					}
//				}
				String joinSql = "SELECT * FROM (" + sql + " ) sub WHERE 1=1 ";
				if (dateRange.toUpperCase().equals("TODAY")) {
					joinSql = joinSql +" and DATE_FORMAT(transactionDate,'%Y-%m-%d') = '" + DateCalcUtil.formatDate(new Date()) + "' ";
				} else if (dateRange.toUpperCase().equals("30")) {
					joinSql = joinSql + " and DATE_FORMAT(transactionDate,'%Y-%m-%d') >= '" + DateCalcUtil.formatDate(new Date()) + "' "
							  + " and DATE_FORMAT(transactionDate,'%Y-%m-%d') <= '" + DateCalcUtil.formatDate(DateCalcUtil.getNearDay(new Date(), 30)) + "' ";
				} else if (dateRange.toUpperCase().equals("90")) {
					joinSql = joinSql +" and DATE_FORMAT(transactionDate,'%Y-%m-%d') >= '" + DateCalcUtil.formatDate(new Date()) + "' "
							  + " and DATE_FORMAT(transactionDate,'%Y-%m-%d') <= '" + DateCalcUtil.formatDate(DateCalcUtil.getNearDay(new Date(), 90)) + "' ";
				}
				
				
				return joinSql;
	}

	@Override
	@Transactional
	public void saveCustomerOrderTrans(CustomerOrderTrans customerOrderTrans) {
		// TODO Auto-generated method stub
		customerOrderTransDao.save(customerOrderTrans);
	}

	@Override
	@Transactional
	public ResponseResult getSpendingAmountPatron(ListPage page,String startDate, String endDate,String servicePlanName,String paymentMethod)
	{
		String sql = this.createSqlSpendingAmountPatron(startDate, endDate, servicePlanName, paymentMethod);
		String countSql = "select count(1) count from ( " + sql + " ) a ";
		List param = new ArrayList();
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, param,
				new PatronTransReportDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	private String createSqlSpendingAmountPatron(String startDate, String endDate,String servicePlanName,String paymentMethod){
		if(StringUtils.isEmpty(startDate)){
			startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		if(StringUtils.isEmpty(endDate)){
			endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		StringBuilder sb=new StringBuilder();
		sb.append("select * from (");
		sb.append("SELECT  \n");
		sb.append(" ser.plan_name as servicePlanName , \n");
		sb.append(" m.academy_no as academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n"); 
		sb.append("CASE \n");
		sb.append("WHEN m.member_type='IPM' THEN 'Individual Primary Patron'\n");
		sb.append("WHEN m.member_type='CPM' THEN 'Coporate Primary Patron'\n");
		sb.append("WHEN m.member_type='IDM' THEN 'Individual Dependent Patron'\n");
		sb.append("WHEN m.member_type='CDM' THEN 'Coporate Dependent Patron'\n");
		sb.append("END AS memberType, \n");
		sb.append("SUM(trans.paid_amount)AS amount ,\n");
		sb.append("CASE \n");
		PaymentMethod[]methods=PaymentMethod.values();
		for (PaymentMethod method : methods)
		{
			sb.append("WHEN trans.payment_method_code='"+method.name()+"'  THEN '"+method.getDesc()+"' \n");
		}
		sb.append(" ELSE trans.payment_method_code ");
		sb.append(" END AS paymentMethod,\n");
		sb.append("  trans.payment_method_code  as paymentCode, \n");
		sb.append("COUNT(*)AS number\n");
		sb.append("FROM customer_order_trans trans \n");
		sb.append("LEFT JOIN customer_order_hd hd ON trans.order_no=hd.order_no \n");
		sb.append("LEFT JOIN member m ON hd.customer_id=m.customer_id  \n");
		sb.append("LEFT JOIN customer_profile cp ON cp.customer_id=m.customer_id \n");
		sb.append("LEFT JOIN ( \n");
		sb.append("	  SELECT  sp.plan_no,sp.plan_name,ca.customer_id FROM customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp   WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no");
		sb.append("	 \n) ser ON (ser.customer_id=m.customer_id OR ser.customer_id=m.superior_member_id) \n");
		//filter refund/void suc record
		sb.append("  LEFT JOIN customer_order_trans rt ON rt.from_transaction_no=trans.transaction_no  AND rt.status IN ('RFU','VOID') \n");
		sb.append("WHERE DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '"+startDate+"' AND  '"+endDate+"'  \n");
		sb.append("AND rt.status IS NULL \n");
		sb.append("AND trans.paid_amount<>0  \n");
		sb.append("AND trans.status='"+Status.SUC.name()+"' \n");
		sb.append("AND hd.customer_id IS NOT NULL   \n");
		sb.append("AND ser.plan_name IS NOT NULL   \n");
		sb.append(" GROUP BY m.academy_no,trans.payment_method_code  \n");
		sb.append(") as t  where 1=1 ");
		if(!"All".equals(servicePlanName))
		{
			sb.append(" and t.servicePlanName='"+servicePlanName+"'");	
		}
		if(!"All".equals(paymentMethod))
		{
			sb.append(" and t.paymentCode='"+paymentMethod+"'");	
		}
		return sb.toString();
	}
	
	@Override
	@Transactional
	public ResponseResult getTopUpAmountPatron(ListPage page,String startDate, String endDate,String servicePlanName)
	{
		String sql = this.createSqlTopUpAmountPatron(startDate, endDate, servicePlanName);
		String countSql = "select count(1) count from ( " + sql + " ) a ";
		List param = new ArrayList();
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, param,
				new PatronTransReportDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	private String createSqlTopUpAmountPatron(String startDate, String endDate,String servicePlanName){
		if(StringUtils.isEmpty(startDate)){
			startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		if(StringUtils.isEmpty(endDate)){
			endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		StringBuilder sb=new StringBuilder();
		sb.append("select * from (");
		sb.append("SELECT  \n");
		sb.append(" ser.plan_name as  servicePlanName , \n");
		sb.append(" m.academy_no as academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n"); 
		sb.append("CASE \n");
		sb.append("WHEN m.member_type='IPM' THEN 'Individual Primary Patron'\n");
		sb.append("WHEN m.member_type='CPM' THEN 'Coporate Primary Patron'\n");
		sb.append("WHEN m.member_type='IDM' THEN 'Individual Dependent Patron'\n");
		sb.append("WHEN m.member_type='CDM' THEN 'Coporate Dependent Patron'\n");
		sb.append("END AS memberType, \n");
		sb.append("SUM(trans.paid_amount)AS amount ,\n");
		sb.append("COUNT(*)AS number\n");
		sb.append("FROM customer_order_trans trans \n");
		sb.append("LEFT JOIN customer_order_hd hd ON trans.order_no=hd.order_no \n");
		sb.append("LEFT JOIN member m ON hd.customer_id=m.customer_id  \n");
		sb.append("LEFT JOIN customer_profile cp ON cp.customer_id=m.customer_id \n");
		sb.append("LEFT JOIN ( \n");
		sb.append("	  SELECT  sp.plan_no,sp.plan_name,ca.customer_id FROM customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp   WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no");
		sb.append("	 \n) ser ON (ser.customer_id=m.customer_id OR ser.customer_id=m.superior_member_id) \n");
		sb.append("WHERE DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '"+startDate+"' AND  '"+endDate+"'  \n");
    	sb.append("AND trans.paid_amount<>0 \n");
    	sb.append("AND trans.status='"+Status.SUC.name()+"' \n");
		sb.append("AND hd.customer_id IS NOT NULL   \n");
		sb.append(" AND ser.plan_name IS NOT NULL \n");
		sb.append(" AND EXISTS (SELECT 1 FROM customer_order_det d LEFT JOIN  pos_service_item_price p  ON d.item_no=p.item_no AND p.item_catagory='TOPUP'  WHERE  p.item_catagory='TOPUP'  AND  d.order_no=hd.order_no)\n");
		sb.append(" GROUP BY m.academy_no \n");
		sb.append(") as t  where 1=1 ");
		if(!"All".equals(servicePlanName))
		{
			sb.append(" and t.servicePlanName='"+servicePlanName+"'");	
		}
		return sb.toString();
	}

	@Override
	@Transactional
	public byte[] getSpendingAmountPatronReport(String sortBy,String isAscending, String startDate, String endDate, String servicePlanName,
			String paymentMethod, String fileType) {
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "PatronSpendingAmountReports.jasper";
			String sql = this.createSqlSpendingAmountPatron(startDate, endDate, servicePlanName, paymentMethod);
			
			StringBuilder orderBy=new StringBuilder();
			orderBy.append(sql);
			if(!StringUtils.isEmpty(sortBy)){
				String orderByFiled = sortBy.trim();
				orderBy.append(" order by ");
				if("true".equals(isAscending)){
					orderBy.append(orderByFiled).append(" asc ");
				}else{
					orderBy.append(orderByFiled).append(" desc ");
				}
			}
			List<PatronTransReportDto>dtos=customerOrderTransDao.getDtoBySql(orderBy.toString(), null, PatronTransReportDto.class);
			
			if(StringUtils.isEmpty(startDate)){
				startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			if(StringUtils.isEmpty(endDate)){
				endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			
			Map<String, Object> parameters = new HashMap();
			parameters.put("startDate", startDate);
			parameters.put("endDate", endDate);
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dtos, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public byte[] getTopUpAmountPatronReport(String sortBy, String isAscending, String startDate, String endDate,
			String servicePlanName, String fileType) {
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "PatronTopUpAmountReports.jasper";
			String sql = this.createSqlTopUpAmountPatron(startDate, endDate, servicePlanName);
			
			StringBuilder orderBy=new StringBuilder();
			orderBy.append(sql);
			if(!StringUtils.isEmpty(sortBy)){
				String orderByFiled = sortBy.trim();
				orderBy.append(" order by ");
				if("true".equals(isAscending)){
					orderBy.append(orderByFiled).append(" asc ");
				}else{
					orderBy.append(orderByFiled).append(" desc ");
				}
			}
			List<PatronTransReportDto>dtos=customerOrderTransDao.getDtoBySql(orderBy.toString(), null, PatronTransReportDto.class);
			if(StringUtils.isEmpty(startDate)){
				startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			if(StringUtils.isEmpty(endDate)){
				endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			Map<String, Object> parameters = new HashMap();
			parameters.put("startDate", startDate);
			parameters.put("endDate", endDate);
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dtos, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public ResponseResult getTopPatronSpendingFacilities(ListPage page, String startDate, String endDate,
			String servicePlanName, String facilityType) {
		String sql = this.createSqlTopPatronSpendingFacilities(startDate, endDate, servicePlanName, facilityType);
		String countSql = "select count(1) count from ( " + sql + " ) a ";
		List param = new ArrayList();
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, param,
				new PatronTransReportDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	private String createMemberType(){
		StringBuilder sb=new StringBuilder();
		sb.append("CASE \n");
		sb.append("WHEN m.member_type='IPM' THEN 'Individual Primary Patron'\n");
		sb.append("WHEN m.member_type='CPM' THEN 'Coporate Primary Patron'\n");
		sb.append("WHEN m.member_type='IDM' THEN 'Individual Dependent Patron'\n");
		sb.append("WHEN m.member_type='CDM' THEN 'Coporate Dependent Patron'\n");
		return sb.toString();
	}
	@Override
	@Transactional
	public byte[] getTopPatronSpendingFacilitiesReport(String sortBy, String isAscending, String startDate,
			String endDate, String servicePlanName, String facilityType, String fileType) {
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "top100PatronFacilityesReports.jasper";
			String sql = this.createSqlTopPatronSpendingFacilities(startDate, endDate, servicePlanName, facilityType);
			
			StringBuilder orderBy=new StringBuilder();
			orderBy.append(sql);
			if(!StringUtils.isEmpty(sortBy)){
				String orderByFiled = sortBy.trim();
				orderBy.append(" order by ");
				if("true".equals(isAscending)){
					orderBy.append(orderByFiled).append(" asc ");
				}else{
					orderBy.append(orderByFiled).append(" desc ");
				}
			}
			List<PatronTransReportDto>dtos=customerOrderTransDao.getDtoBySql(orderBy.toString(), null, PatronTransReportDto.class);
			
			if(StringUtils.isEmpty(startDate)){
				startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			if(StringUtils.isEmpty(endDate)){
				endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			Map<String, Object> parameters = new HashMap();
			parameters.put("startDate", startDate);
			parameters.put("endDate", endDate);
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dtos, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	private String createSqlTopPatronSpendingFacilities(String startDate, String endDate,String servicePlanName,String facilityType){
		if(StringUtils.isEmpty(startDate)){
			startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		if(StringUtils.isEmpty(endDate)){
			endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		StringBuilder sb=new StringBuilder();
		
		sb.append("SELECT servicePlanName, m.academy_no as academyNo, memberName,memberType,'"+getFacilityTypeDescByType(facilityType)+"' as facilitiyType,SUM(amount)AS amount,SUM(number)AS number");
		sb.append(" from (");
		
		if("All".equals(facilityType))
		{
				sb.append(this.createGolf_Tennis(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
				sb.append("  UNION ALL \n");
				sb.append(this.createGolf_Tennis(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
				sb.append("  UNION ALL \n");
				sb.append(this.createSqlWellness(startDate, endDate));
				sb.append("  UNION ALL \n");
				sb.append(this.createSqlAccommodation(startDate, endDate,facilityType));
				sb.append("  UNION ALL \n");
				sb.append(this.createSqlRestaurant(startDate, endDate));
		}else{
			
			if(FacilityTypeReport.GOLF.getCode().equals(facilityType))
			{
				sb.append(this.createGolf_Tennis(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
			}else if(FacilityTypeReport.TENNIS.getCode().equals(facilityType)){
				sb.append(this.createGolf_Tennis(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
			}else if(FacilityTypeReport.WELLNESS.getCode().equals(facilityType)){
				sb.append(this.createSqlWellness(startDate, endDate));
			}else if(FacilityTypeReport.GUESTROOM.getCode().equals(facilityType)){
				sb.append(this.createSqlAccommodation(startDate, endDate,facilityType));
			}else if(FacilityTypeReport.CENTURIONHOUSE.getCode().equals(facilityType)){
				sb.append(this.createSqlAccommodation(startDate, endDate,facilityType));
			}else if(FacilityTypeReport.RESTAURANT.getCode().equals(facilityType)){
				sb.append(this.createSqlRestaurant(startDate, endDate));
			}
			
		}
		sb.append(" )as t");
		sb.append(" inner join member m on t.academyNo=m.customer_id");
		sb.append(" where 1=1 \n");
		
		if(!"All".equals(servicePlanName))
		{
			sb.append(" and  servicePlanName='"+servicePlanName+"'");
		}
		sb.append(" group by academyNo");
		
		return  sb.toString();
	}
	private String getFacilityTypeDescByType(String type)
	{
		FacilityTypeReport[]facility=FacilityTypeReport.values();
		if("All".equals(type)){
			return "All";
		}
		for (FacilityTypeReport report : facility) {
			if(report.getCode().equals(type))
			{
				return report.getDesc();
			}
		}
		return null;
	}
	
	
	private String createSqlAccommodation(String startDate, String endDate,String facilitype)
	{
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT \n");
		sb.append("ser.plan_name AS  servicePlanName ,\n");
		sb.append("hd.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n");
		sb.append(createMemberType());
		sb.append("END AS memberType, \n");
		
		if(FacilityTypeReport.GUESTROOM.getCode().equals(facilitype)){
			sb.append("'"+FacilityTypeReport.GUESTROOM.getDesc()+"' AS facilitiyType,");	
		}else if(FacilityTypeReport.CENTURIONHOUSE.getCode().equals(facilitype)){
			sb.append("'"+FacilityTypeReport.CENTURIONHOUSE.getDesc()+"' AS facilitiyType,");
		}else{
			sb.append(" 'All' AS facilitiyType,");
		}
		
		sb.append(" SUM(trans.paid_amount) AS amount ,COUNT(*) AS number");
		sb.append(" FROM customer_order_trans  trans \n");
		sb.append("LEFT JOIN customer_order_hd  hd ON trans.order_no=hd.order_no \n");
		sb.append("LEFT JOIN customer_profile cp ON cp.customer_id=hd.customer_id \n");
		sb.append("LEFT JOIN member m ON  m.customer_id=hd.customer_id \n");
		sb.append(" LEFT JOIN ( \n");
		sb.append("	  SELECT  sp.plan_no,sp.plan_name,ca.customer_id FROM customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp   WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no \n");	 
		sb.append(" ) ser ON (ser.customer_id=m.customer_id OR ser.customer_id=m.superior_member_id) \n");
		//filter refund/void
		sb.append(" LEFT JOIN customer_order_trans rt ON rt.from_transaction_no=trans.transaction_no  AND rt.status IN ('RFU','VOID')  \n");
		
		if(!"All".equals(facilitype)){
			sb.append(" LEFT JOIN room_reservation_rec rec on trans.order_no=rec.order_no  \n");
			sb.append(" LEFT JOIN room room  ON rec.room_id=room.room_id  \n");	
		}
		sb.append("WHERE DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '"+startDate+"' AND  '"+endDate+"' \n"); 
		sb.append("AND trans.status='SUC' \n");//AND trans.paid_amount<>0 
		sb.append(" and rt.status is null \n");
		sb.append(" AND ser.plan_name IS NOT NULL  \n");
		sb.append("AND EXISTS(SELECT 1 FROM customer_order_det det LEFT JOIN pos_service_item_price ps ON det.item_no=ps.item_no WHERE det.order_no=trans.order_no AND ps.item_catagory IN ('OASIS_ROOM','PMS_ROOM')) \n");
		sb.append("AND hd.customer_id IS NOT NULL \n"); 
		
		if(!"All".equals(facilitype))
		{
			if(FacilityTypeReport.GUESTROOM.getCode().equals(facilitype)){
				sb.append(" AND (room.room_no IS NULL OR !(room.room_no ='51001' AND room.room_type ='CH')) \n"); 
			}else{
				sb.append(" AND  room.room_no='51001' AND room.room_type='CH' \n"); 
			}
		}
		sb.append("GROUP BY hd.customer_id \n"); 
		
		return sb.toString();  
	}
	private String createSqlRestaurant(String startDate, String endDate)
	{
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT \n");
		sb.append("ser.plan_name AS  servicePlanName ,\n");
		sb.append("hd.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n");
		sb.append(createMemberType());
		sb.append("END AS memberType, \n");
		sb.append("'"+FacilityTypeReport.RESTAURANT.getDesc()+"' AS facilitiyType,");
		sb.append(" SUM(trans.paid_amount) AS amount ,COUNT(*) AS number");
		sb.append(" FROM customer_order_trans  trans \n");
		sb.append("LEFT JOIN customer_order_hd  hd ON trans.order_no=hd.order_no \n");
		sb.append("LEFT JOIN customer_profile cp ON cp.customer_id=hd.customer_id \n");
		sb.append("LEFT JOIN member m ON  m.customer_id=hd.customer_id \n");
		sb.append(" LEFT JOIN ( \n");
		sb.append("	  SELECT  sp.plan_no,sp.plan_name,ca.customer_id FROM customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp   WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no \n");	 
		sb.append(" ) ser ON (ser.customer_id=m.customer_id OR ser.customer_id=m.superior_member_id) \n");
		//filter refund/void
		sb.append(" LEFT JOIN customer_order_trans rt ON rt.from_transaction_no=trans.transaction_no  AND rt.status IN ('RFU','VOID')  \n");
		
		sb.append("WHERE DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '"+startDate+"' AND  '"+endDate+"' \n"); 
		sb.append("AND trans.status='SUC' AND trans.paid_amount<>0 \n");
		sb.append(" and rt.status is null \n");
		sb.append(" AND ser.plan_name IS NOT NULL  \n");
		sb.append("AND EXISTS(SELECT 1 FROM customer_order_det det LEFT JOIN pos_service_item_price ps ON det.item_no=ps.item_no    WHERE det.order_no=trans.order_no AND ps.item_catagory ='"+FacilityTypeReport.RESTAURANT.name()+"') \n");
		sb.append("AND hd.customer_id IS NOT NULL \n"); 
		sb.append("GROUP BY hd.customer_id \n"); 
		return sb.toString();  
	}
	private String createSqlAccommodation_Usage_Rage(String startDate, String endDate,String facilityType)
	{
		StringBuilder sb=new StringBuilder();
		sb.append(" SELECT sp.plan_name AS servicePlanName ,m.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName, \n");
		sb.append(createMemberType());
		sb.append("END AS memberType, \n");
		
		if(FacilityTypeReport.CENTURIONHOUSE.getCode().equals(facilityType)){
			sb.append(" '"+FacilityTypeReport.CENTURIONHOUSE.getDesc()+"' AS facilitiyType, \n");
		}else if(FacilityTypeReport.GUESTROOM.getCode().equals(facilityType)){
			sb.append(" '"+FacilityTypeReport.GUESTROOM.getDesc()+"' AS facilitiyType, \n");
		}else{
			sb.append(" 'All' as facilitiyType ,");
		}
		sb.append(" CASE  \n");
		sb.append(" WHEN booking.number>0 THEN booking.number  \n");
		sb.append(" ELSE 0 \n");
		sb.append(" END AS number  \n");
		sb.append(" FROM (customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp ) \n");
		sb.append(" LEFT JOIN member m ON  m.customer_id=ca.customer_id OR m.superior_member_id=ca.customer_id\n");
		sb.append(" LEFT JOIN customer_profile cp ON cp.customer_id=m.customer_id \n");
		
		sb.append(" LEFT JOIN ( \n");
		sb.append("	SELECT rec.customer_id AS patronId,COUNT(*) AS number FROM room_reservation_rec  rec \n");
		
		if(!"All".equals(facilityType))
		{
			sb.append(" LEFT JOIN room room ON rec.room_id=room.room_id ");
			sb.append("   WHERE 1=1  \n");
			if(FacilityTypeReport.GUESTROOM.getCode().equals(facilityType)){
				sb.append(" AND (room.room_no IS NULL OR !(room.room_no ='51001' AND room.room_type ='CH'))  \n");
			}else{
				sb.append(" AND  room.room_no ='51001' AND room.room_type ='CH'  \n");
			}
		}else{
			sb.append("   WHERE 1=1  \n");
		}
		
		sb.append("  AND rec.arrival_date  BETWEEN '"+startDate+"' AND  '"+endDate+"' and rec.status='CKI' \n ");
		sb.append(" GROUP BY rec.customer_id  \n");
		sb.append(" ) AS booking   \n");
		sb.append(" ON booking.patronId=m.customer_id \n");
		
		sb.append(" WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no AND ca.status<>'"+Status.EXP.name()+"'\n");
		return sb.toString();  
	}
	private String createSqlRestaurant_Usage_Rage(String startDate, String endDate) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT sp.plan_name AS servicePlanName ,m.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n");
		sb.append(createMemberType());
		sb.append("END AS memberType, \n");
		sb.append("'" + FacilityTypeReport.RESTAURANT.getDesc() + "' AS facilitiyType, \n");
		sb.append(" CASE  \n");
		sb.append(" WHEN booking.number>0 THEN booking.number  \n");
		sb.append(" ELSE 0 \n");
		sb.append(" END AS number  \n");
		sb.append(" FROM (customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp )\n");
		sb.append(" LEFT JOIN member m ON  m.customer_id=ca.customer_id OR m.superior_member_id=ca.customer_id \n");
		sb.append(" LEFT JOIN ( \n");
		sb.append(" SELECT hd.customer_id AS patronId,COUNT(*) AS number FROM  customer_order_hd hd \n");
		sb.append("	WHERE hd.order_date BETWEEN '"+startDate+"' AND '"+endDate+"' \n");
		sb.append("	AND hd.order_status='"+Status.CMP.name()+"' \n");
		sb.append("	AND EXISTS ( \n");
		sb.append("		SELECT 1 FROM customer_order_det det  \n");
		sb.append("		LEFT JOIN pos_service_item_price p ON det.item_no=p.item_no \n");
		sb.append("		WHERE det.order_no=hd.order_no AND p.item_catagory='"+FacilityTypeReport.RESTAURANT.name()+"' \n");
		sb.append(") \n");
		sb.append("	GROUP BY hd.customer_id \n");
		sb.append(" ) AS booking  \n");
		sb.append(" ON booking.patronId=m.customer_id \n");
		
		sb.append(" LEFT JOIN customer_profile cp ON cp.customer_id=m.customer_id \n");
		sb.append("  WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no AND ca.status<>'" + Status.EXP.name()+"'\n");

		return sb.toString();
	}
	private String createSqlWellness(String startDate, String endDate)
	{
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT \n");
		sb.append("ser.plan_name AS  servicePlanName ,\n");
		sb.append("hd.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n");
		sb.append(createMemberType());
		sb.append("END AS memberType, \n");
		sb.append("'"+FacilityTypeReport.WELLNESS.getCode()+"' AS facilitiyType,");
		sb.append(" SUM(trans.paid_amount) AS amount ,COUNT(*) AS number");
		sb.append(" FROM customer_order_trans  trans \n");
		sb.append("LEFT JOIN customer_order_hd  hd ON trans.order_no=hd.order_no \n");
		sb.append("LEFT JOIN customer_profile cp ON cp.customer_id=hd.customer_id \n");
		sb.append("LEFT JOIN member m ON  m.customer_id=hd.customer_id \n");
		sb.append(" LEFT JOIN ( \n");
		sb.append("	  SELECT  sp.plan_no,sp.plan_name,ca.customer_id FROM customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp   WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no \n");	 
		sb.append(" ) ser ON (ser.customer_id=m.customer_id OR ser.customer_id=m.superior_member_id) \n");
		//filter refund/void
		sb.append(" LEFT JOIN customer_order_trans rt ON rt.from_transaction_no=trans.transaction_no  AND rt.status IN ('RFU','VOID')  \n");
		sb.append("WHERE DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '"+startDate+"' AND  '"+endDate+"' \n"); 
		sb.append("AND trans.status='SUC' AND trans.paid_amount<>0 \n");
		sb.append(" AND rt.status IS NULL \n ");
		sb.append(" AND ser.plan_name IS NOT NULL  \n");
		sb.append(" AND EXISTS(SELECT 1 FROM customer_order_det det LEFT JOIN pos_service_item_price ps ON det.item_no=ps.item_no where trans.order_no=det.order_no and ps.item_catagory IN ('MMS','MMS_SPA') ) \n");
		sb.append(" AND hd.customer_id IS NOT NULL \n"); 
		sb.append("GROUP BY hd.customer_id \n"); 
		return sb.toString();  
	}
	private String createSqlWellness_Usage_Rage(String startDate, String endDate)
	{
		StringBuilder sb=new StringBuilder();
		sb.append(" SELECT sp.plan_name AS servicePlanName ,m.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName, \n");
		sb.append(this.createMemberType());
		sb.append("END AS memberType, \n");
		sb.append(" '"+FacilityTypeReport.WELLNESS.getDesc()+"' AS facilitiyType, \n");
		sb.append(" CASE  \n");
		sb.append(" WHEN booking.number>0 THEN booking.number  \n");
		sb.append(" ELSE 0 \n");
		sb.append(" END AS number  \n");
		sb.append("FROM (customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp )  \n");
		sb.append(" LEFT JOIN member m ON  m.customer_id=ca.customer_id OR m.superior_member_id=ca.customer_id \n");
		
		sb.append("LEFT JOIN (SELECT rec.customer_id AS patronId,COUNT(*) AS number FROM spa_appointment_rec  rec WHERE DATE_FORMAT(rec.start_datetime,'%Y-%m-%d') BETWEEN '"+startDate+"' AND  '"+endDate+"' and rec.status in ('RSV','ATN') \n");
		sb.append(" GROUP BY rec.customer_id  \n");
		sb.append(" ) AS booking  \n");
		sb.append(" ON booking.patronId=m.customer_id \n");
		
		sb.append(" LEFT JOIN customer_profile cp ON cp.customer_id=m.customer_id \n");
		sb.append("  WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no AND ca.status<>'EXP'  \n");
		return sb.toString();  
	}
	private String createGolf_Tennis(String startDate, String endDate,String Type)
	{
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT servicePlanName, academyNo, memberName,memberType,facilitiyType,SUM(amount)AS amount,SUM(number)AS number \n");
		sb.append(" from (");
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append(this.createSqlGolf_TennisCourse(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
			sb.append(" UNION ALL ");
			sb.append(this.createSqlGolf_Tennis_Coach(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
		}else{
			sb.append(this.createSqlGolf_TennisCourse(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
			sb.append(" UNION ALL ");
			sb.append(this.createSqlGolf_Tennis_Coach(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
		}
		sb.append(")AS tb  where 1=1 \n");
		// when patron reservation Guest room , give free golf bay to patron  
		// need to filter 
		if(FacilityTypeReport.GOLF.getCode().equals(Type)){
			sb.append(" and tb.amount>0 ");
		}
		sb.append(" GROUP BY tb.academyNo \n ");
		
		return sb.toString();
	}
	private String createGolf_Tennis_Usage_Rage(String startDate, String endDate,String Type)
	{
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT servicePlanName,academyNo, memberName,memberType,facilitiyType,SUM(number)AS number \n");
		sb.append(" from (");
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append(this.createSqlGolf_TennisCourse_Usage_Rage(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
			sb.append(" UNION ALL ");
			sb.append(this.createSqlGolf_Tennis_Coach_Usage_Rage(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
		}else{
			sb.append(this.createSqlGolf_TennisCourse_Usage_Rage(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
			sb.append(" UNION ALL ");
			sb.append(this.createSqlGolf_Tennis_Coach_Usage_Rage(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
		}
		sb.append(")AS tb \n");
		sb.append("GROUP BY tb.academyNo \n ");
		
		return sb.toString();
	}
	
	/***
	 * 
	 * @param startDate
	 * @param endDate
	 * @param Type GOLF/TENNIS
	 * @return
	 */
	private String createSqlGolf_TennisCourse(String startDate, String endDate,String Type)
	{
		StringBuilder sb=new StringBuilder();
		// golf/tennis course
		sb.append("SELECT \n");
		sb.append("ser.plan_name AS  servicePlanName ,\n");
		sb.append("ce.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName, \n");
		sb.append(this.createMemberType());
		sb.append("END AS memberType, \n");
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append("'"+FacilityTypeReport.GOLF.getCode()+"' AS facilitiyType,\n");
		}else{
			sb.append("'"+FacilityTypeReport.TENNIS.getCode()+"' AS facilitiyType,\n");
		}
		sb.append(" SUM(trans.paid_amount)AS amount ,COUNT(*)AS number \n");
		sb.append("FROM  customer_order_trans trans \n");
		sb.append("INNER JOIN course_enrollment ce ON ce.cust_order_no=trans.order_no \n");
		sb.append("LEFT JOIN course_master cm ON ce.course_id=cm.course_id \n");
		sb.append("LEFT JOIN customer_profile cp ON cp.customer_id=ce.customer_id  \n");
		sb.append("LEFT JOIN member m ON (m.customer_id=ce.customer_id)  \n");
		sb.append(" LEFT JOIN (  \n");
		sb.append(" SELECT  sp.plan_no,sp.plan_name,ca.customer_id FROM customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp   WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no\n");
		sb.append(" ) ser ON (ser.customer_id=m.customer_id OR ser.customer_id=m.superior_member_id) \n");
		//filter refund/void
		sb.append(" LEFT JOIN customer_order_trans rt ON rt.from_transaction_no=trans.transaction_no  AND rt.status IN ('RFU','VOID')  \n");
		sb.append(" WHERE DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '"+startDate+"' AND  '"+endDate+"'\n");
		sb.append("AND rt.status IS NULL \n");
		sb.append("AND trans.status='SUC' \n");
		
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append("AND cm.course_type='"+School.GSS.getGtaCode()+"' \n");
		}else{
			sb.append("AND cm.course_type='"+School.TSS.getGtaCode()+"' \n");
		}
		
		sb.append("AND ser.plan_name IS NOT NULL \n");
		sb.append("GROUP BY  m.customer_id \n");
		
		return sb.toString();  
	}
	private String createSqlGolf_TennisCourse_Usage_Rage(String startDate, String endDate,String Type)
	{
		StringBuilder sb=new StringBuilder();
		// golf/tennis course
		sb.append("SELECT sp.plan_name AS servicePlanName ,m.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n");
		sb.append(this.createMemberType());
		sb.append("END AS memberType, \n");
		
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append("'"+FacilityTypeReport.GOLF.getCode()+"' AS facilitiyType,\n");
		}else{
			sb.append("'"+FacilityTypeReport.TENNIS.getCode()+"' AS facilitiyType,\n");
		}
		
		sb.append("CASE \n");
		sb.append(" WHEN booking.number>0 THEN booking.number  \n");
		sb.append(" ELSE 0 \n");
		sb.append(" END AS number \n");
		sb.append(" FROM (customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp ) \n");
		sb.append(" LEFT JOIN member m ON  m.customer_id=ca.customer_id OR m.superior_member_id=ca.customer_id\n");

		sb.append(" LEFT JOIN (SELECT rec.customer_id AS patronId,COUNT(*) AS number FROM course_enrollment rec LEFT JOIN course_master cm ON  rec.course_id=cm.course_id \n");
		sb.append(" where 1=1 \n");
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append("AND cm.course_type='"+School.GSS.getGtaCode()+"' \n");
		}else{
			sb.append("AND cm.course_type='"+School.TSS.getGtaCode()+"' \n");
		}
		sb.append("AND DATE_FORMAT(rec.enroll_date,'%Y-%m-%d')  BETWEEN '"+startDate+"' AND  '"+endDate+"' ");
		sb.append("AND rec.status='REG'");
		sb.append(" GROUP BY rec.customer_id  \n");
		sb.append(" ) AS booking \n");
		sb.append(" ON booking.patronId=m.customer_id \n");
		
		sb.append("	LEFT JOIN customer_profile cp ON cp.customer_id=m.customer_id \n");
		sb.append(" WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no AND ca.status<>'"+Status.EXP.name()+"' \n");
		return sb.toString();  
	}
	private String createSqlGolf_Tennis_Coach(String startDate, String endDate,String Type)
	{
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT \n");
		sb.append("ser.plan_name AS  servicePlanName , \n");
		sb.append(" hd.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n");
		sb.append(this.createMemberType());
		sb.append("END AS memberType, \n");
		
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append("'"+FacilityTypeReport.GOLF.getCode()+"' AS facilitiyType,\n");
		}else{
			sb.append("'"+FacilityTypeReport.TENNIS.getCode()+"' AS facilitiyType,\n");
		}
		sb.append("SUM(trans.paid_amount) AS amount,COUNT(*)AS number \n");
		sb.append("FROM customer_order_trans trans  \n");
		sb.append("INNER JOIN member_facility_type_booking mb  ON mb.order_no=trans.order_no  \n");
		sb.append("LEFT JOIN customer_order_hd hd ON trans.order_no=hd.order_no  \n");  
		sb.append("LEFT JOIN customer_profile cp ON cp.customer_id=hd.customer_id  \n");
		sb.append("LEFT JOIN member m ON m.customer_id=hd.customer_id  \n");
		sb.append("LEFT JOIN (   \n");
		sb.append("  SELECT  sp.plan_no,sp.plan_name,ca.customer_id FROM customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp   WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no  \n");	 
		sb.append(") ser ON (ser.customer_id=m.customer_id OR ser.customer_id=m.superior_member_id)   \n");
		sb.append(" LEFT JOIN customer_order_trans rt ON rt.from_transaction_no=trans.transaction_no  AND rt.status IN ('RFU','VOID')  \n");
		sb.append("WHERE DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '"+startDate+"' AND  '"+endDate+"'  \n");
		
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append("AND mb.resv_facility_type ='"+FacilityTypeReport.GOLF.getCode()+"' \n");
		}else{
			sb.append("AND mb.resv_facility_type ='"+FacilityTypeReport.TENNIS.getCode()+"' \n");
		}
		sb.append("AND trans.status='SUC' \n");
		sb.append("AND rt.status IS NULL  \n");
		sb.append("AND ser.plan_name IS NOT NULL  \n");
		sb.append("GROUP BY hd.customer_id \n");
		
		return sb.toString();
	}

	private String createSqlGolf_Tennis_Coach_Usage_Rage(String startDate, String endDate,String Type)
	{
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT sp.plan_name AS servicePlanName ,m.customer_id AS academyNo,CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname)AS memberName,\n");
		sb.append(this.createMemberType());
		sb.append("END AS memberType, \n");
		
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append("'"+FacilityTypeReport.GOLF.getCode()+"' AS facilitiyType,\n");
		}else{
			sb.append("'"+FacilityTypeReport.TENNIS.getCode()+"' AS facilitiyType,\n");
		}
		
		sb.append("CASE \n");
		sb.append(" WHEN booking.number>0 THEN booking.number  \n");
		sb.append(" ELSE 0 \n");
		sb.append(" END AS number \n");
		sb.append(" FROM (customer_service_acc ca ,customer_service_subscribe cb ,service_plan sp ) \n");
		sb.append(" LEFT JOIN member m ON  m.customer_id=ca.customer_id OR m.superior_member_id=ca.customer_id\n");
		sb.append(" LEFT JOIN (SELECT rec.customer_id AS patronId,COUNT(*) AS number FROM member_facility_type_booking  rec  \n");
		sb.append(" where 1=1 \n");
		if(FacilityTypeReport.GOLF.getCode().equals(Type))
		{
			sb.append("AND rec.resv_facility_type='"+FacilityTypeReport.GOLF.getCode()+"' \n");
		}else{
			sb.append("AND rec.resv_facility_type='"+FacilityTypeReport.TENNIS.getCode()+"' \n");
		}
		sb.append("AND DATE_FORMAT(rec.begin_datetime_book,'%Y-%m-%d')  BETWEEN '"+startDate+"' AND  '"+endDate+"' \n");
		sb.append("AND rec.status='"+Constant.Status.ATN.name()+"' \n");
		sb.append(" GROUP BY rec.customer_id  \n");
		sb.append(" ) AS booking \n");
		sb.append(" ON booking.patronId=m.customer_id \n");
		sb.append("	LEFT JOIN customer_profile cp ON cp.customer_id=m.customer_id \n");
		sb.append(" WHERE ca.acc_no=cb.acc_no AND cb.service_plan_no=sp.plan_no AND ca.status<>'"+Status.EXP.name()+"' \n");
		return sb.toString();
	}
	@Override
	@Transactional
	public ResponseResult getUsageRatePatronDiffFacilities(ListPage page, String startDate, String endDate,
			String servicePlanName, String facilityType,String sortBy, String isAscending) {
		String sql=this.createUsageRateFacilities(startDate, endDate, servicePlanName, facilityType);
		String countSql = "select count(1) count from ( " + sql + " ) a ";
		List param = new ArrayList();
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, param,
				new PatronTransReportDto());
		List<Object> listBooking = listPage.getDtoList();
		
		UsageRageData data=new UsageRageData();
//		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		String infors[]=calculateUsageRage(sortBy, isAscending, startDate, endDate, servicePlanName, facilityType);
		data.setUsageRage(infors[0]);
		data.setCountNumber(infors[1]);
		
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	private String[] calculateUsageRage(String sortBy, String isAscending, String startDate,
			String endDate, String servicePlanName, String facilityType){
		String[]infos=new String[2];
		String sql = this.createUsageRateFacilities(startDate, endDate, servicePlanName, facilityType);
		StringBuilder orderBy=new StringBuilder();
		orderBy.append(sql);
		if(!StringUtils.isEmpty(sortBy)){
			String orderByFiled = sortBy.trim();
			orderBy.append(" order by ");
			if("true".equals(isAscending)){
				orderBy.append(orderByFiled).append(" asc ");
			}else{
				orderBy.append(orderByFiled).append(" desc ");
			}
		}
		List<PatronTransReportDto>dtos=customerOrderTransDao.getDtoBySql(orderBy.toString(), null, PatronTransReportDto.class);
		BigDecimal bookNumber=BigDecimal.ZERO;
		BigDecimal sumLine=BigDecimal.ZERO;
		Long countNumber=0l;
		for (PatronTransReportDto dto : dtos)
		{
			countNumber+=dto.getNumber();
			if(dto.getNumber()>0)
			{
				bookNumber=bookNumber.add(BigDecimal.ONE);
			}
			sumLine=sumLine.add(BigDecimal.ONE);
		}
		String rage=null;
		if(sumLine.compareTo(BigDecimal.ZERO)==0&&bookNumber.compareTo(BigDecimal.ZERO)==0){
			rage= "0.00%";
		}else{
			rage=bookNumber.divide(sumLine,4,RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).toString()+"%";	
		}
		 infos[0]=rage;
		 infos[1]=countNumber.toString();
		 
		return infos;
	}
	@Override
	@Transactional
	public byte[] getUsageRatePatronDiffFacilitiesReport(String sortBy, String isAscending, String startDate,
			String endDate, String servicePlanName, String facilityType, String fileType) {
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "usageRateOfPatronDiffFacilities.jasper";
			
			String sql = this.createUsageRateFacilities(startDate, endDate, servicePlanName, facilityType);
			StringBuilder orderBy=new StringBuilder();
			orderBy.append(sql);
			if(!StringUtils.isEmpty(sortBy)){
				String orderByFiled = sortBy.trim();
				orderBy.append(" order by ");
				if("true".equals(isAscending)){
					orderBy.append(orderByFiled).append(" asc ");
				}else{
					orderBy.append(orderByFiled).append(" desc ");
				}
			}
			List<PatronTransReportDto>dtos=customerOrderTransDao.getDtoBySql(orderBy.toString(), null, PatronTransReportDto.class);
			
			if(StringUtils.isEmpty(startDate)){
				startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			if(StringUtils.isEmpty(endDate)){
				endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			Map<String, Object> parameters = new HashMap();
			parameters.put("startDate", startDate);
			parameters.put("endDate", endDate);
			parameters.put("rage", this.calculateUsageRage(sortBy, isAscending, startDate, endDate, servicePlanName, facilityType)[0]);
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dtos, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	private String createUsageRateFacilities(String startDate, String endDate,String servicePlanName,String facilityType)
	{
		if(StringUtils.isEmpty(startDate)){
			startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		if(StringUtils.isEmpty(endDate)){
			endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		StringBuilder sb=new StringBuilder();
		
		sb.append("SELECT servicePlanName,academyNo, memberName,memberType,'"+getFacilityTypeDescByType(facilityType)+"' as facilitiyType,SUM(number)AS number ");
		sb.append(" from (");
		
		if("All".equals(facilityType))
		{
				sb.append(this.createGolf_Tennis_Usage_Rage(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
				sb.append("  UNION ALL \n");
				sb.append(this.createGolf_Tennis_Usage_Rage(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
				sb.append("  UNION ALL \n");
				sb.append(this.createSqlWellness_Usage_Rage(startDate, endDate));
				sb.append("  UNION ALL \n");
				sb.append(this.createSqlAccommodation_Usage_Rage(startDate, endDate,facilityType));
				sb.append("  UNION ALL \n");
				sb.append(this.createSqlRestaurant_Usage_Rage(startDate, endDate));
		}else{
			if(FacilityTypeReport.GOLF.getCode().equals(facilityType))
			{
				sb.append(this.createGolf_Tennis_Usage_Rage(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
			}else if(FacilityTypeReport.TENNIS.getCode().equals(facilityType)){
				sb.append(this.createGolf_Tennis_Usage_Rage(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
			}else if(FacilityTypeReport.WELLNESS.getCode().equals(facilityType)){
				sb.append(this.createSqlWellness_Usage_Rage(startDate, endDate));
			}else if(FacilityTypeReport.GUESTROOM.getCode().equals(facilityType)){
				sb.append(this.createSqlAccommodation_Usage_Rage(startDate, endDate,facilityType));
			}else if(FacilityTypeReport.CENTURIONHOUSE.getCode().equals(facilityType)){
				sb.append(this.createSqlAccommodation_Usage_Rage(startDate, endDate,facilityType));
			}else if(FacilityTypeReport.RESTAURANT.getCode().equals(facilityType)){
				sb.append(this.createSqlRestaurant_Usage_Rage(startDate, endDate));
			}
		}
		sb.append(" )as t");
		sb.append(" where 1=1 \n");
		if(!"All".equals(servicePlanName))
		{
			sb.append(" and  servicePlanName='"+servicePlanName+"'");
		}
		sb.append(" group by academyNo");
		
		return  sb.toString();
	}

	@Override
	@Transactional
	public ResponseResult getSpendingAmountPatronDiffFacilities(ListPage page, String startDate, String endDate,
			String servicePlanName) {
		String sql = this.createSqlSpendingAmountDiffFacilities(startDate, endDate, servicePlanName);
		String countSql = "select count(1) count from ( " + sql + " ) a ";
		List param = new ArrayList();
		ListPage listPage = customerOrderTransDao.listBySqlDto(page, countSql, sql, param,
				new PatronTransReportDto());
		List<Object> listBooking = listPage.getDtoList();
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBooking);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	private String createSqlSpendingAmountDiffFacilities(String startDate, String endDate,
			String servicePlanName){
		if(StringUtils.isEmpty(startDate)){
			startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		if(StringUtils.isEmpty(endDate)){
			endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
		}
		StringBuilder sb=new StringBuilder();
		
		sb.append("SELECT servicePlanName,facilitiyType,SUM(amount)AS amount ");
		sb.append(" from (");
		sb.append(this.createGolf_Tennis(startDate, endDate, FacilityTypeReport.GOLF.getCode()));
		sb.append("  UNION ALL \n");
		sb.append(this.createGolf_Tennis(startDate, endDate, FacilityTypeReport.TENNIS.getCode()));
		sb.append("  UNION ALL \n");
		sb.append(this.createSqlWellness(startDate, endDate));
		sb.append("  UNION ALL \n");
		sb.append(this.createSqlAccommodation(startDate, endDate,FacilityTypeReport.GUESTROOM.getCode()));
		sb.append("  UNION ALL \n");
		sb.append(this.createSqlAccommodation(startDate, endDate,FacilityTypeReport.CENTURIONHOUSE.getCode()));
		sb.append("  UNION ALL \n");
		sb.append(this.createSqlRestaurant(startDate, endDate));
		sb.append(" )as t");
		sb.append(" where 1=1 \n");
		if(!"All".equals(servicePlanName))
		{
			sb.append(" and  servicePlanName='"+servicePlanName+"'");
		}
		sb.append(" group by servicePlanName,facilitiyType");
		return sb.toString();
				
	}

	@Override
	@Transactional
	public byte[] getSpendingAmountPatronDiffFacilitiesReport(String sortBy, String isAscending, String startDate,
			String endDate, String servicePlanName,String fileType) {
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "spendingAmountPatronDiffFacilities.jasper";
			
			String sql = this.createSqlSpendingAmountDiffFacilities(startDate, endDate, servicePlanName);
			StringBuilder orderBy=new StringBuilder();
			orderBy.append(sql);
			if(!StringUtils.isEmpty(sortBy)){
				String orderByFiled = sortBy.trim();
				orderBy.append(" order by ");
				if("true".equals(isAscending)){
					orderBy.append(orderByFiled).append(" asc ");
				}else{
					orderBy.append(orderByFiled).append(" desc ");
				}
			}
			List<PatronTransReportDto>dtos=customerOrderTransDao.getDtoBySql(orderBy.toString(), null, PatronTransReportDto.class);
			
			if(StringUtils.isEmpty(startDate)){
				startDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			if(StringUtils.isEmpty(endDate)){
				endDate=DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");	
			}
			Map<String, Object> parameters = new HashMap();
			parameters.put("startDate", startDate);
			parameters.put("endDate", endDate);
			
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dtos, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
