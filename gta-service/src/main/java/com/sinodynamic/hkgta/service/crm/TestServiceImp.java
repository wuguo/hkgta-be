package com.sinodynamic.hkgta.service.crm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinodynamic.hkgta.dao.crm.TestDao;
import com.sinodynamic.hkgta.entity.crm.PresentationBatch;

@Service
public class TestServiceImp implements TestService {
	@Autowired
	private TestDao dao;
	
	@Override
	public void updatePres() {
		PresentationBatch  pb = dao.get(PresentationBatch.class, "11");
		//pb.getPresentMaterialSeqs().get(0).setPresentSeq(999l);
		dao.save(pb);
	}

	
}
