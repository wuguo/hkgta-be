package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinodynamic.hkgta.dao.adm.AppsVersionDao;
import com.sinodynamic.hkgta.dto.crm.AppsVersionInfoDto;
import com.sinodynamic.hkgta.dto.crm.AppsVersionListDto;
import com.sinodynamic.hkgta.entity.adm.AppsVersion;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.StringUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class AppsVersionServiceImpl  extends ServiceBase<AppsVersion> implements AppsVersionService {
	
	@Autowired
	private AppsVersionDao appsVersionDao;
	/**  版本信息缓存  */
//	private static Map<String, AppsVersionInfoDto> versionMap = new HashMap<>();
	
//	/**
//	 * 初始化数据
//	 * @return 
//	 */
//	public List<AppsVersion> initDate(){
//		System.out.println("------------初始化数据开始-------------");
//		List<AppsVersion> list = appsVersionDao.getAllAppsVersion();
//		if (list.size() > 0) {
//			for (AppsVersion appsVersion : list) {
//				AppsVersionInfoDto dto = new AppsVersionInfoDto(appsVersion,StringUtil.stringtodouble(appsVersion.getLatestVersion()));
//				versionMap.put(appsVersion.getAppId(), dto);
//			}
//		}
//		System.out.println("------------初始化数据结束-------------》》" + JSONArray.toJSONString(list));
//		return list;
//		
//	}
	
	/**
	 * 查询所有app列表版本信息
	 */
	@Transactional
	@Override
	public List<AppsVersion> getAllAppsVersion() {
//		if (versionMap.size() < 1) {
//			return initDate();
//		} else {
			return appsVersionDao.getAllAppsVersion();
//		}
		
	}
	
	/**
	 * 通过appid更新app版本信息
	 * @param dto
	 * @param userId
	 * @return
	 */
	@Transactional
	@Override
	public ResponseResult updateAppsVersionInfo(AppsVersionListDto listDto,String userId){
		try {
			if (listDto.getList().size() < 1) {
				responseResult.initResult(GTAError.DayPassError.ERRORMSG_PARAM_NULL);
				return responseResult;
			}
			for (AppsVersionInfoDto dto : listDto.getList()) {
				AppsVersion appsVersion = appsVersionDao.get(AppsVersion.class, dto.getAppId());
				if (appsVersion == null) {
					responseResult.initResult(GTAError.MemberShipError.NO_RECORD_FOUND);
					return responseResult;
				}
				appsVersion.setDownloadLink(dto.getDownloadLink());
				appsVersion.setLatestVersion(dto.getLatestVersion());
				appsVersion.setUpdateDate(new Date());
				appsVersion.setVerNo("1");
				appsVersion.setVersionInfo(dto.getVersionInfo());
				appsVersion.setUpdateBy(userId);
				appsVersionDao.update(appsVersion);
			}
			responseResult.initResult(GTAError.Success.SUCCESS);	
			return responseResult;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("------------------错误-----------" + e.getMessage());
			responseResult.initResult(GTAError.SysError.FAIL_UPDATE_ROLE);	
			return responseResult;
		}
	}
	
	/**
	 * 是否需要升级版本
	 * @param appId
	 * @param versionNo
	 * @return
	 */
	@Transactional
	@Override
	public ResponseResult isUpgrade(String appId, String versionNo){
		if (appId.isEmpty() || versionNo.isEmpty()) {
			responseResult.initResult(GTAError.DayPassError.ERRORMSG_PARAM_NULL);
			return responseResult;
		}
		AppsVersion appsVersion = appsVersionDao.get(AppsVersion.class, appId);
		if (appsVersion == null) {
			responseResult.initResult(GTAError.MemberShipError.NO_RECORD_FOUND);
			return responseResult;
		}
		Map<String, Object> responseData = new HashMap<>();
		double version = StringUtil.stringtodouble(appsVersion.getLatestVersion());
		if (version > StringUtil.stringtodouble(versionNo)) {
			responseData.put("isUpgrade", true);
			responseData.put("info", new AppsVersionInfoDto(appsVersion,version));
		} else {
			responseData.put("isUpgrade", false);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, responseData);	
		return responseResult;
	}

}
