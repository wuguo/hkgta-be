package com.sinodynamic.hkgta.service.bi;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.cache.spi.TransactionalDataRegion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.bi.BiOasisFlexFileDao;
import com.sinodynamic.hkgta.dao.bi.BiOasisTranCodeDao;
import com.sinodynamic.hkgta.entity.bi.BiOasisFlexFile;
import com.sinodynamic.hkgta.entity.bi.BiOasisTranCode;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.LoggerType;

import net.sf.json.JSONObject;

@Service
public class BiOasisFlexFileServiceImpl extends ServiceBase<BiOasisFlexFile>implements BiOasisFlexFileService {

	private final Logger logger = Logger.getLogger(LoggerType.OASISFLEXFILE.getName());

	@Value(value = "#{oasisProperties['oasis.flex.path']}")
	private String oasisFlexPath;

	@Value(value = "#{oasisProperties['oasis.retry.times']}")
	private int retryTimes;

	@Autowired
	private BiOasisFlexFileDao biOasisFlexFileDao;

	@Autowired
	private BiOasisTranCodeDao biOasisTranCodeDao;

	@Transactional
	public Serializable save(BiOasisFlexFile biOasisFlexFile) {
		return biOasisFlexFileDao.save(biOasisFlexFile);
	}

	private List<BiOasisFlexFile> readFile(String filePath) {
		List<BiOasisFlexFile> list = null;
		File file = new File(filePath);
		if (file.exists()) {
			try {
				logger.info("read oasisFlexFile start...the file name is :" + file.getName());
				list = readBiOasisFlexFile(file);
				logger.info("read oasisFlexFile file  end");
			} catch (Exception ex) {
				logger.error(ex.getMessage());
			}
		}
		return list;
	}

	@Override
	public List<BiOasisFlexFile> readBiOasisFlexFile(File file) {
		List<BiOasisFlexFile> list = new ArrayList<>();
		try {
			LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(file)));
			String currentLine = reader.readLine();
			int i = 1;
			while (currentLine != null) {
				String tranDate = currentLine.substring(10, 20);
				String amount = currentLine.substring(115, 131);
				String transCode = currentLine.substring(221, currentLine.indexOf("-"));
				/***
				 * SGG-3615
				 * System has to check a parameter "<Amount><Type>Front" from data file to identify the number is positive or negative,
					for example:
					"454.00DFront", Amount is 454 and Type is D = Debit, it should be a negative number
					"8,857.00CFront", Amount is 8857 and Type is C = Credit, it should be possitive
				 */
				String type="C";//Credit
				if(currentLine.contains("Front"))
				{
					type=currentLine.substring(currentLine.indexOf("Front")-1, currentLine.indexOf("Front"));	
				}
				if (StringUtils.isEmpty(tranDate)) {
					logger.info("the content is null  ,invalid date at line " + i);
					break;
				}
				if (!checkTransDataFormat(tranDate)) {
					logger.info("invalid date format at line " + i + " ,the character from 10 to 20 is transDate :"
							+ tranDate);
				}
				if (StringUtils.isEmpty(amount)) {
					logger.info("the amount is null  ,invalid amount at line " + i);
					break;
				}
				if (!checkAmountFormat(amount)) {
					logger.info("invalid amount format at line " + i + " ,the character from 115 to 131 is amount :"
							+ amount);
					break;
				}
				if (StringUtils.isEmpty(transCode)) {
					logger.info("the transCode is null  ,invalid transCode at line " + i);
					break;
				}
				DecimalFormat f = new DecimalFormat("0,000.00");
				Double d = Double.parseDouble(f.parseObject(amount.trim()).toString());
				BigDecimal big = new BigDecimal(d);
				if("D".equals(type))
				{
					big=new BigDecimal(-d);
				}
				BiOasisFlexFile oasis = new BiOasisFlexFile();
				oasis.setOasisTransDate(DateConvertUtil.parseString2Date(tranDate.trim(), "d/M/yyyy", Locale.US));
				oasis.setOasisTranCode(transCode.trim());
				oasis.setAmount(big.setScale(2, BigDecimal.ROUND_HALF_UP));
				oasis.setCreateDate(new Date());
				oasis.setFileName(file.getName().substring(0, file.getName().lastIndexOf(".")));
				list.add(oasis);
				currentLine = reader.readLine();
				i++;
			}
			reader.close();
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return list;
	}
	private boolean checkTransDataFormat(String tranDate)
	{
		try{
			DateConvertUtil.parseString2Date(tranDate.trim(), "d/M/yyyy", Locale.US);
			return true;
		}catch(ParseException parse){
			return false;
		}
	}
	private boolean checkAmountFormat(String amount)
	{
		try{
			DecimalFormat f = new DecimalFormat("0,000.00");
		    Double.parseDouble(f.parseObject(amount.trim()).toString());
			return true;
		}catch(Exception parse){
			return false;
		}
	}

	@Transactional
	public boolean excuteImportFile(String filePath, Date time) throws Exception {
		List<BiOasisFlexFile> list = this.readFile(filePath);
		if (null != list && list.size() > 0) {
			biOasisFlexFileDao.deleteByDate(time);
			logger.info("clean  success table :bi_oasis_flex_file  the createTime is :" + time);
			
			logger.info("insert data to table bi_oasis_flex_file start.....");
			Map<String, String> map = this.setBiOasisTranCodeMap();
			for (BiOasisFlexFile biOasisFlexFile : list) {
				if (!map.containsKey(biOasisFlexFile.getOasisTranCode())) {
					logger.info("oasisTranCode not exist bi_oasis_tran_code, set oasisTranCode is Other");
					biOasisFlexFile.setOasisTranCode("Other");
				}
				biOasisFlexFileDao.save(biOasisFlexFile);
				logger.info("insert data to bi_oasis_flex_file :" + JSONObject.fromObject(biOasisFlexFile));
			}
			logger.info("insert data to table bi_oasis_flex_file end.....");
			return true;
		} else {
			logger.info("read file size is zero......");
			return false;
		}
	}
	/**
	 * 导入txt文件进行解析插入处理
	 * 
	 * @param sourceFile
	 * @return
	 */
	@Override
	@Transactional
	public boolean importTxtFile(File sourceFile) {
		biOasisFlexFileDao.deleteByDate(new Date());
		logger.info("clean  success table :bi_oasis_flex_file  the createTime is :" + new Date());
		List<BiOasisFlexFile> list = readBiOasisFlexFile(sourceFile);
		if (null != list && list.size() > 0) {
			logger.info("insert data to table bi_oasis_flex_file start.....");
			Map<String, String> map = this.setBiOasisTranCodeMap();
			for (BiOasisFlexFile biOasisFlexFile : list) {
				if (!map.containsKey(biOasisFlexFile.getOasisTranCode())) {
					biOasisFlexFile.setOasisTranCode("Other");
					logger.info("oasisTranCode not exist bi_oasis_tran_code, set oasisTranCode is Other");
				}
				biOasisFlexFileDao.save(biOasisFlexFile);
				logger.info("insert data to bi_oasis_flex_file :" + JSONObject.fromObject(biOasisFlexFile));
			}
			logger.info("insert data to table bi_oasis_flex_file end.....");
		}
		return true;
	}

	@Transactional
	public boolean saveBiOasisFlexFile(BiOasisFlexFile biOasisFlexFile) {
		if (biOasisFlexFileDao.save(biOasisFlexFile) != Integer.valueOf(0)) {
			return true;
		}
		return false;
	}

	private Map<String, String> setBiOasisTranCodeMap() {
		Map<String, String> map = new HashMap<>();
		List<BiOasisTranCode> codes = biOasisTranCodeDao.getAllBiOasisTranCodeList();
		if (null != codes && codes.size() > 0) {
			for (BiOasisTranCode code : codes) {
				map.put(code.getOasisTranCode(), code.getOasisTranCode());
			}
		}
		return map;
	}

	/**
	 * 获取BiOasisFlexFile每天列表
	 * 
	 * @return
	 */
	@Transactional
	@Override
	public List<BiOasisFlexFile> getBioasisFlexFile() {
		return biOasisFlexFileDao.getBioasisFlexFile();
	}
}

