package com.sinodynamic.hkgta.service.pms;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.pms.RoomStatusScheduleDto;
import com.sinodynamic.hkgta.entity.pms.RoomStatusSchedule;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/** //TODO
 * albert.sit - 2016年8月8日
 * {Please describe your class here}
 *
 */
public interface RoomStatusScheduleService extends IServiceBase<RoomStatusSchedule> {

	public Serializable save(RoomStatusSchedule roomStatusSchedule);

	/***
	 * auto update Room status when room_status_schedule time is expired if room
	 * service_status =OOS then update room service_status is null ,other status
	 * no change if room frontdesk_status=OOO then update room frontdesk_status
	 * is null ,status is D
	 * 
	 * @return
	 */
	public void autoUpdateRoomStatus(Date date);

	/***
	 * Auto setting the room status when currentTime between 
	 * (room_status_schedule) beginDate  and endDate, update room frontdesk_status to
	 * OOO OR service_status to OOS
	 * 
	 * @param date
	 */
	public void autoSettingRoomStatus(Date date);

	/**
	 * 获取schedule list
	 */
	public List<RoomStatusSchedule> getRoomStatusScheduleList(long roomId);

	/**
	 * 根据id删除记录
	 * 
	 * @param scheduleId
	 * @throws Exception 
	 */
	public void deleteRoomStatusScheduleByScheduleId(long scheduleId) throws Exception;


	/**
	 * 创建schedule
	 * @param dto
	 * @param userId
	 * @return
	 * @throws Exception 
	 */
	public ResponseResult createRoomStatusSchedule(RoomStatusScheduleDto dto, String userId) throws Exception;
	
	/** //TODO
	 * Import OOS and OOO
	 * @param userId
	 * @param type
	 * @return
	 * @throws Exception
	 *
	 */
	public ResponseResult  importOoCsv(String userId,String type, File file) throws Exception;
	
	
	public ResponseResult  importDeskStatusCsv(String userId, File file) throws Exception;

	/**
	 * 获取schedule list
	 */
	public List<RoomStatusScheduleDto> getRoomStatusScheduleDtoList(long roomId);
}
