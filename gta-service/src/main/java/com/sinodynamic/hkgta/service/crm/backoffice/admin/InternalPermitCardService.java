package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface InternalPermitCardService extends IServiceBase<InternalPermitCard> {
    
    public InternalPermitCard getCardById(String cardId) throws Exception;
    
    public void saveCard(InternalPermitCard card) throws Exception;
    
    public void updateCard(InternalPermitCard card) throws Exception;

}
