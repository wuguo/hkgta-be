package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.AppsVersionListDto;
import com.sinodynamic.hkgta.entity.adm.AppsVersion;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface AppsVersionService extends IServiceBase<AppsVersion>{
	public List<AppsVersion> getAllAppsVersion();

	/**
	 * 通过appid更新app版本信息
	 * @param dto
	 * @param userId
	 * @return
	 */
	public ResponseResult updateAppsVersionInfo(AppsVersionListDto dto, String userId);

	/**
	 * 是否需要升级版本
	 * @param appId
	 * @param versionNo
	 * @return
	 */
	public ResponseResult isUpgrade(String appId, String versionNo);
}
