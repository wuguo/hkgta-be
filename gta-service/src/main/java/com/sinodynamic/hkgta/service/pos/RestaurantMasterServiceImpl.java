package com.sinodynamic.hkgta.service.pos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dto.pos.RestaurantBookingQuotaDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantOpenHourDto;
import com.sinodynamic.hkgta.entity.pos.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pos.RestaurantCustomerBookingDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantMasterDao;
import com.sinodynamic.hkgta.dto.pos.RestaurantImageDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantMasterDto;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
public class RestaurantMasterServiceImpl extends ServiceBase<RestaurantMaster> implements
        RestaurantMasterService {

    @Autowired
    private RestaurantMasterDao restaurantMasterDao;

    @Autowired
    private RestaurantCustomerBookingDao restaurantCustomerBookingDao;

    @Override
    @Transactional
    public List<RestaurantMasterDto> getRestaurantMasterList(String imageFeatureCode) {
        List<RestaurantMaster> restaurants = restaurantMasterDao.getRestaurantMasterList();
        List<RestaurantMasterDto> result = new ArrayList<>();
        for (RestaurantMaster rest : restaurants) {
            RestaurantMasterDto resDto = new RestaurantMasterDto();
            resDto.setRestaurantId(rest.getRestaurantId());
            resDto.setRestaurantName(rest.getRestaurantName());
            resDto.setCuisineType(rest.getCuisineType());
            resDto.setOnlineReservation(rest.getOnlineReservation());
            List<RestaurantImage> restaurantImages = rest.getRestaurantImages();
            List<RestaurantImageDto> restaurantImageDtos = new ArrayList<>();
            if (restaurantImages != null && restaurantImages.size() > 0) {
                for (RestaurantImage restImage : restaurantImages) {
                    if (!StringUtils.isEmpty(imageFeatureCode) && imageFeatureCode.equals(restImage.getImageFeatureCode())) {
                        RestaurantImageDto restaurantImageDto = new RestaurantImageDto();
                        restaurantImageDto.setImageId(restImage.getImageId());
                        restaurantImageDto.setImageFeatureCode(restImage.getImageFeatureCode());
                        restaurantImageDto.setImageFileName(restImage.getImageFileName());
                        restaurantImageDtos.add(restaurantImageDto);
                        break;
                    }
                }
            }
            resDto.setRestaurantImages(restaurantImageDtos);
            result.add(resDto);
        }
        return result;
    }

    @Override
    @Transactional
    public List<RestaurantCustomerBooking> getRestaurantBookingByRestaurantIdAndCustomerId(String restaurantId, Long customerId) {
        return restaurantCustomerBookingDao.getRestaurantCustomerBookingListByRestaurantIdAndCustomerId(restaurantId, customerId);
    }

    @Override
    @Transactional
    public void updateRestaurant(RestaurantMasterDto restaurantMasterDto, String restaurantId, String userId) {

        if (StringUtils.isEmpty(restaurantId)) {
            throw new GTACommonException(GTAError.RestaurantError.RESTAURANT_ID_INVALID);
        }

        RestaurantMaster restaurantMaster = restaurantMasterDao.getRestaurantMaster(restaurantId);
        if (restaurantMaster == null) {
            throw new GTACommonException(GTAError.RestaurantError.RESTAURANT_ID_INVALID);
        }
        restaurantMaster.setRestaurantId(restaurantId);
        restaurantMaster.setRestaurantName(restaurantMasterDto.getRestaurantName());
        restaurantMaster.setLocation(restaurantMasterDto.getLocation());
        restaurantMaster.setCuisineType(restaurantMasterDto.getCuisineType());
        restaurantMaster.setPhone(restaurantMasterDto.getPhone());
        restaurantMaster.setAllowOnlineOrder(restaurantMasterDto.getAllowOnlineOrder());
        restaurantMaster.setOpenHourText(restaurantMasterDto.getOpenHourText());
        restaurantMaster.setOnlineReservation(restaurantMasterDto.getOnlineReservation());
        restaurantMaster.setUpdateBy(userId);
        restaurantMaster.setUpdateDate(new Date());

        List<RestaurantImageDto> restaurantImageDtos = restaurantMasterDto.getRestaurantImages();

        restaurantMaster.getRestaurantImages().clear();

        if (restaurantImageDtos != null) {
            for (RestaurantImageDto restaurantImageDto : restaurantImageDtos) {
                RestaurantImage restaurantImage = new RestaurantImage();
                restaurantImage.setImageFeatureCode(restaurantImageDto.getImageFeatureCode());
                restaurantImage.setImageFileName(restaurantImageDto.getImageFileName());
                restaurantImage.setRestaurantMaster(restaurantMaster);
                restaurantImage.setCreateBy(userId);
                restaurantImage.setCreateDate(new Date());
                restaurantMaster.getRestaurantImages().add(restaurantImage);
            }
        }

        List<RestaurantBookingQuotaDto> restaurantBookingQuotaDtos = restaurantMasterDto.getRestaurantBookingQuotas();

        restaurantMaster.getRestaurantBookingQuotas().clear();
        restaurantMasterDao.getCurrentSession().flush();

        if (restaurantBookingQuotaDtos != null) {

            for (RestaurantBookingQuotaDto restaurantBookingQuotaDto : restaurantBookingQuotaDtos) {
                RestaurantBookingQuota restaurantBookingQuota = new RestaurantBookingQuota();
                restaurantBookingQuota.setRestaurantMaster(restaurantMaster);
                restaurantBookingQuota.setPeriodCode(restaurantBookingQuotaDto.getPeriodCode());
                restaurantBookingQuota.setStartTime(restaurantBookingQuotaDto.getStartTime());
                restaurantBookingQuota.setEndTime(restaurantBookingQuotaDto.getEndTime());
                restaurantBookingQuota.setQuota(restaurantBookingQuotaDto.getQuota());
                restaurantBookingQuota.setCreateBy(userId);
                restaurantBookingQuota.setCreateDate(new Date());
                restaurantMaster.addRestaurantBookingQuota(restaurantBookingQuota);
            }
        }

        // open hour
        RestaurantOpenHourDto restaurantOpenHourDto = restaurantMasterDto.getRestaurantOpenHour();

        restaurantMaster.getRestaurantOpenHours().clear();
        if (restaurantOpenHourDto != null) {
            RestaurantOpenHour restaurantOpenHour = new RestaurantOpenHour();
            restaurantOpenHour.setRestaurantMaster(restaurantMaster);
            restaurantOpenHour.setAllowOrderTimeFrom(restaurantOpenHourDto.getAllowOrderTimeFrom());
            restaurantOpenHour.setAllowOrderTimeTo(restaurantOpenHourDto.getAllowOrderTimeTo());
            restaurantOpenHour.setAllowOnlineOrder(restaurantOpenHourDto.getAllowOnlineOrder());
            restaurantOpenHour.setCreateBy(userId);
            restaurantOpenHour.setCreateDate(new Date());
            restaurantMaster.getRestaurantOpenHours().add(restaurantOpenHour);
            restaurantMaster.setAllowOnlineOrder(restaurantOpenHour.getAllowOnlineOrder());
        }

        restaurantMasterDao.update(restaurantMaster);
    }

    @Override
    @Transactional
    public RestaurantMasterDto getRestaurant(String restaurantId) {

        if (StringUtils.isEmpty(restaurantId)) {
            throw new GTACommonException(GTAError.RestaurantError.RESTAURANT_ID_INVALID);
        }

        RestaurantMaster restaurantMaster = restaurantMasterDao.getRestaurantMaster(restaurantId);
        if (restaurantMaster == null) {
            throw new GTACommonException(GTAError.RestaurantError.RESTAURANT_ID_INVALID);
        }

        RestaurantMasterDto restaurantMasterDto = new RestaurantMasterDto();
        restaurantMasterDto.setRestaurantId(restaurantMaster.getRestaurantId());
        restaurantMasterDto.setRestaurantName(restaurantMaster.getRestaurantName());
        restaurantMasterDto.setCuisineType(restaurantMaster.getCuisineType());
        restaurantMasterDto.setLocation(restaurantMaster.getLocation());
        restaurantMasterDto.setOpenHourText(restaurantMaster.getOpenHourText());
        restaurantMasterDto.setPhone(restaurantMaster.getPhone());
        restaurantMasterDto.setAllowOnlineOrder(restaurantMaster.getAllowOnlineOrder());
        restaurantMasterDto.setOnlineReservation(restaurantMaster.getOnlineReservation());
        try {
			restaurantMasterDto.setFileBasePath(FileUpload.getBasePath(FileUpload.FileCategory.RESTAURANT));
		} catch (Exception e) {
			throw new GTACommonException(GTAError.RestaurantError.UNEXPECTED_EXCEPTION);
		}
        List<RestaurantImage> restaurantImages = restaurantMaster.getRestaurantImages();

        List<RestaurantImageDto> restaurantImageDtos = new ArrayList<>();
        if (restaurantImages != null) {
            for (RestaurantImage restaurantImage : restaurantImages) {
                RestaurantImageDto restaurantImageDto = new RestaurantImageDto();
                restaurantImageDto.setImageId(restaurantImage.getImageId());
                restaurantImageDto.setForDeviceType(restaurantImage.getForDeviceType());
                restaurantImageDto.setImageFeatureCode(restaurantImage.getImageFeatureCode());
                restaurantImageDto.setImageFileName(restaurantImage.getImageFileName());
                restaurantImageDtos.add(restaurantImageDto);
            }
        }
        restaurantMasterDto.setRestaurantImages(restaurantImageDtos);
        List<RestaurantBookingQuota> restaurantBookingQuotas = restaurantMaster.getRestaurantBookingQuotas();
        List<RestaurantBookingQuotaDto> restaurantBookingQuotaDtos = new ArrayList<>();
        if (restaurantBookingQuotas != null) {
            for (RestaurantBookingQuota restaurantBookingQuota : restaurantBookingQuotas) {
                RestaurantBookingQuotaDto restaurantBookingQuotaDto = new RestaurantBookingQuotaDto();
                restaurantBookingQuotaDto.setRestaurantId(restaurantBookingQuota.getRestaurantMaster().getRestaurantId());
                restaurantBookingQuotaDto.setPeriodCode(restaurantBookingQuota.getPeriodCode());
                restaurantBookingQuotaDto.setStartTime(restaurantBookingQuota.getStartTime());
                restaurantBookingQuotaDto.setEndTime(restaurantBookingQuota.getEndTime());
                restaurantBookingQuotaDto.setQuota(restaurantBookingQuota.getQuota());
                restaurantBookingQuotaDtos.add(restaurantBookingQuotaDto);
            }
        }
        restaurantMasterDto.setRestaurantBookingQuotas(restaurantBookingQuotaDtos);

        List<RestaurantOpenHour> restaurantOpenHours = restaurantMaster.getRestaurantOpenHours();
        if(restaurantOpenHours != null && !restaurantOpenHours.isEmpty()) {
            RestaurantOpenHour restaurantOpenHour = restaurantOpenHours.get(0);
            RestaurantOpenHourDto restaurantOpenHourDto = new RestaurantOpenHourDto();
            restaurantOpenHourDto.setRestaurantId(restaurantMaster.getRestaurantId());
            restaurantOpenHourDto.setAllowOrderTimeFrom(restaurantOpenHour.getAllowOrderTimeFrom());
            restaurantOpenHourDto.setAllowOrderTimeTo(restaurantOpenHour.getAllowOrderTimeTo());
            restaurantOpenHourDto.setAllowOnlineOrder(restaurantOpenHour.getAllowOnlineOrder());
            restaurantMasterDto.setRestaurantOpenHour(restaurantOpenHourDto);
        }
        return restaurantMasterDto;
    }

    @Override
    @Transactional
    public RestaurantMaster getRestaurantMaster(String restaurantId) {
        return restaurantMasterDao.get(RestaurantMaster.class, restaurantId);
    }

	@Override
	@Transactional
	public void updateRestaurantReservationStatus() {
		// TODO Auto-generated method stub
		restaurantMasterDao.updateRestaurantReservationStatus();
	}
}
