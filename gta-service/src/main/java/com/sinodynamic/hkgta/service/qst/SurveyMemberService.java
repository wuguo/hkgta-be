package com.sinodynamic.hkgta.service.qst;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.entity.qst.SurveyMember;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface SurveyMemberService extends IServiceBase<SurveyMember> {
	
	public void autoCountSurveyMember();

	/**
	 * 获取当前符合条件调查问卷的URL
	 * @param academyNo
	 * @return
	 */
	public String getMeetConditionsSurveyQuestionnaireURL(Long customerId, String academyNo);

	/***
	 * check Exist CustomerId /SurveyId 
	 * @param customerId
	 * @param surveyId
	 * @param surveySentDateIsNull 
	 * @return
	 */
	public  List<SurveyMember> checkExistCustomerIdAndSurveyId(Long customerId, Long surveyId, String surveySentDateIsNull);

	
	
	public void autoCountSurveyMemberByCustomerId(Long customerId);

	/**
	 * 更新或保存SurveyMember
	 * @param customerId
	 */
	public void seveOrUpdateSurveyMemberByLogin(Long customerId);
}
