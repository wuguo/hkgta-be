package com.sinodynamic.hkgta.service.common;

import com.sinodynamic.hkgta.dao.fms.CourseMasterDao;
import com.sinodynamic.hkgta.dto.ReportRequestDto;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.util.*;

@Service
@Transactional
public class ReportServiceImpl implements ReportService {

    private Logger logger = Logger.getLogger(ReportServiceImpl.class);

    private final static String JASPER_PATH = "/WEB-INF/jasper/";

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private CourseMasterDao courseMasterDao;

    @Override
    public byte[] getReportAttach(ReportRequestDto reportRequestDto) {
        try {
            File reFile = new File(servletContext.getRealPath(JASPER_PATH + reportRequestDto.getReportFile()));
            Map<String, Object> parameters = reportRequestDto.getParameters();
            DataSource ds = SessionFactoryUtils.getDataSource(courseMasterDao.getsessionFactory());
            Connection dbconn = DataSourceUtils.getConnection(ds);

            String sortBy = reportRequestDto.getSortBy();
            String isAscending = reportRequestDto.getIsAscending();
            String fileType = reportRequestDto.getFileType();
            //Sorting by desired field
            JRDesignSortField sortField = new JRDesignSortField();
            List<JRSortField> sortList = new ArrayList<>();
            sortField.setName(sortBy);
            if ("true".equalsIgnoreCase(isAscending)) {
                sortField.setOrder(SortOrderEnum.ASCENDING);
            } else {
                sortField.setOrder(SortOrderEnum.DESCENDING);
            }
            sortField.setType(SortFieldTypeEnum.FIELD);
            sortList.add(sortField);
            parameters.put(JRParameter.SORT_FIELDS, sortList);
            //Ignore pagination for csv
            if ("csv".equals(fileType)) {
                parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
            }

            parameters.put("REPORT_CONNECTION", dbconn);

            JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

            ByteArrayOutputStream outPut = new ByteArrayOutputStream();
            JRAbstractExporter exporter = exportedByFileType(fileType, jasperPrint, outPut);
            if (exporter == null) {
                return null;
            } else {
                exporter.exportReport();
                return outPut.toByteArray();
            }
        } catch (JRException e) {
            logger.error("error create report", e);
            return null;
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private JRAbstractExporter exportedByFileType(String fileType, JasperPrint jasperPrint, ByteArrayOutputStream outPut) {
        JRAbstractExporter exporter = null;
        switch (fileType) {
            case "pdf":
                exporter = new JRPdfExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));
                break;
            case "csv":
                exporter = new JRCsvExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleWriterExporterOutput(outPut));
                break;
            default:
        }

        return exporter;
    }
}
