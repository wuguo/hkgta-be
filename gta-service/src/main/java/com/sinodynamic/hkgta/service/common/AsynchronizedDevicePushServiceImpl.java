package com.sinodynamic.hkgta.service.common;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("asynchronizedPushService")
public class AsynchronizedDevicePushServiceImpl implements DevicePushService {

	private Logger logger = Logger.getLogger(AsynchronizedDevicePushServiceImpl.class);

	private static ExecutorService pool = Executors.newCachedThreadPool();

	@Autowired
	@Qualifier("synchronizedPushService")
	private DevicePushService devicePushService;

	@Override
	public void pushRegister(String userId, String deviceArn, String token, String platform, String application,
			String version, String updatedBy) {

		devicePushService.pushRegister(userId, deviceArn, token, platform, application, version, updatedBy);
	}

	@Override
	public int deleteUserDevice(String userId, String arnApplication) {

		return devicePushService.deleteUserDevice(userId, arnApplication);
	}

	@Override
	@Deprecated
	public void pushMessage(final String[] userIds, final String message, final String application) {

		pushMessage(userIds, null, message, application);
	}

	@Override
	public void broadcastMessage(final String topicArn, final String message) {

		Runnable smsTask = new Runnable() {

			public void run() {

				try {
					devicePushService.broadcastMessage(topicArn, message);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		};

		pool.execute(smsTask);
	}

	// for mobile notification with message template
	public void pushMessage(final String[] userIds, final String functionId, final String[] replaceParams,
			final String application) {

		Runnable smsTask = new Runnable() {

			public void run() {

				try {
					devicePushService.pushMessage(userIds, functionId, replaceParams, application);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		};

		pool.execute(smsTask);
	}

	@Override
	public void pushMessage(final String[] userIds, final String subject, final String message,
			final String application) {
		Runnable smsTask = new Runnable() {

			public void run() {

				try {
					devicePushService.pushMessage(userIds, subject, message, application);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		};

		pool.execute(smsTask);

	}

	/**
	 * 根据 Application推送所有有效设备
	 */
	@Override
	public void pushMessage(final String functionId, final String[] replaceParams, final String application) {
		Runnable smsTask = new Runnable() {

			public void run() {

				try {
					devicePushService.pushMessage(functionId, replaceParams, application);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		};

		pool.execute(smsTask);
	}

	@Override
	public void pushMessage(final String[] userIds, final String subject, final String message, final String pushBy,
			final String application) {
		// TODO Auto-generated method stub
		Runnable smsTask = new Runnable() {
			public void run() {
				try {
					devicePushService.pushMessage(userIds, subject, message, pushBy, application);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		};
		pool.execute(smsTask);
	}

}
