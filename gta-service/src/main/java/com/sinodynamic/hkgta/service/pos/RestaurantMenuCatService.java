package com.sinodynamic.hkgta.service.pos;

import java.util.List;

import com.sinodynamic.hkgta.dto.pos.RestaurantMenuCatDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuCat;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RestaurantMenuCatService extends IServiceBase<RestaurantMenuCat>
{
	List<RestaurantMenuCat> getRestaurantMenuCatList(String restaurantId);
	String getRestaurantListByRestaurantId();
	boolean updateRestaurantMenuCat(String restaurantId, String catId, String categoryName, String updateBy);
	RestaurantMenuCatDto getRestaurantMenuCat(String restaurantId, String catId);
}
