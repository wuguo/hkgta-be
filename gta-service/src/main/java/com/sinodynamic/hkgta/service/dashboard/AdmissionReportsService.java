package com.sinodynamic.hkgta.service.dashboard;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.UsageRate;
import com.sinodynamic.hkgta.dto.dashboard.ServicePlanSalesPercentageDto;
import com.sinodynamic.hkgta.dto.dashboard.StaffTurnoverDto;
import com.sinodynamic.hkgta.dto.rpos.CreditCardTypeDistributionDto;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 
 * @author Kaster 20160413
 *
 */
public interface AdmissionReportsService {

	List<StaffTurnoverDto> querySalesAmount();

	Map<String,Object> querySalesAmountRanking(Integer topNum, String staffId);

	List<ServicePlanSalesPercentageDto> queryServicePlanSalesPercentage();

	Map<String, Object> queryAdmAndOpeAccRevenue(String startYearMonth, String endYearMonth);

	Map<String, Object> queryActivePatronAndAdmissionRelated();
	
	public List<UsageRate>queryFacilityOccupancy(String year);

	/**
	 * 获取信用卡支付类型数据
	 * @return
	 */
	public List<CreditCardTypeDistributionDto> creditCardPayTypeData();

	 /**
     * 获取topup频率
     * @param year
     * @return
     */
	public ResponseResult getTopupFrequency(String year);

	/**
	 * 获取每月CashValue平衡柱形图分布
	 * @param year
	 * @return
	 */
	public ResponseResult getMonthlyCashValueBalanceDistribution(String year);

	/**
	 * 获取每月topup数量分布
	 * @param year
	 * @return
	 */
	public ResponseResult getMonthlyTopupAmountDistribution(String year);

	/**
	 * 获取每月CashValue平衡饼状图分布
	 * @param year
	 * @return
	 */
	public ResponseResult getMonthlyCashValueBalanceMultidimensionalDistribution(String year);

}
