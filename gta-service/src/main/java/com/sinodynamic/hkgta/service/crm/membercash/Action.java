package com.sinodynamic.hkgta.service.crm.membercash;

public enum Action {
	
	//A-add, M-modify, D-delete
	A("A"), M("M"), D("D");
	
	private String code;

	private Action(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
