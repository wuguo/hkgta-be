package com.sinodynamic.hkgta.service.fms;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.util.constant.Constant.StatementDescItem;
import com.sinodynamic.hkgta.util.constant.Constant.Status;

@Service
public class ReservationReportServiceImpl implements ReservationReportService {
	protected final Logger logger = Logger.getLogger(ReservationReportServiceImpl.class);

	@Override
	public StringBuilder getReserSqlByDate(String date, String type, String status) {
		if (StringUtils.isEmpty(type) || "ALL".equalsIgnoreCase(type)) {
			type = null;
		}
		if (StringUtils.isEmpty(status) || "ALL".equalsIgnoreCase(status)) {
			status = null;
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" select * FROM( ");

		sql.append(" select  * FROM ( ");
		sql.append(createGolfTennisSql(date, type, status));
		sql.append(" )as t_golfTennis");

		if (type == null || StatementDescItem.GSS.name().equalsIgnoreCase(type) || StatementDescItem.TSS.name().equalsIgnoreCase(type)) {
			sql.append(" UNION ALL ");
			sql.append(" select  * FROM (");
			sql.append(createCourseSql(date, type, status));
			sql.append(" )as t_course");
		}

		if (type == null || StatementDescItem.PMS_ROOM.name().equalsIgnoreCase(type)) {
			sql.append(" UNION ALL ");
			sql.append(" select  * FROM (");
			sql.append(createGuestRoomSql(date, type, status));
			sql.append(" )as t_guestRoom");
		}
		if(Status.CAN.name().equals(status) && (type == null || StatementDescItem.DP.name().equalsIgnoreCase(type))){
			sql.append(" UNION ALL ");
			sql.append(" select  * FROM (");
			sql.append(createDayPassSql(date, type, status));
			sql.append(" )as t_dayPass");
			
		}
		sql.append(" ) AS book ");
		sql.append(" ORDER BY starttime ASC ");

		return sql;
	}

	/***
	 * GOLF OR TENNIS or private coach /GSS or TSS
	 * 
	 * @return
	 */
	private StringBuilder createGolfTennisSql(String date, String type, String status) {
		String typeQuery = "";
		if (type != null) {
			if (StatementDescItem.GFBK.name().equalsIgnoreCase(type)) {
				typeQuery = " and (mb.resv_facility_type= 'GOLF'  AND mb.exp_coach_user_id IS NULL) ";
			} else if (StatementDescItem.TFBK.name().equalsIgnoreCase(type)) {
				typeQuery = " and (mb.resv_facility_type= 'TENNIS' AND mb.exp_coach_user_id IS NULL) ";
			} else if (StatementDescItem.GS.name().equalsIgnoreCase(type)) {
				typeQuery = " and (mb.resv_facility_type= 'GOLF' AND mb.exp_coach_user_id IS NOT NULL)  ";
			} else if (StatementDescItem.TS.name().equalsIgnoreCase(type)) {
				typeQuery = " and (mb.resv_facility_type= 'TENNIS' AND mb.exp_coach_user_id IS NOT NULL)";
			} else {
				typeQuery = " and mb.resv_facility_type= '" + type + "'";
			}
		}
		String statusQuery = "";
		if (status != null) {
			if (Status.RSV.name().equals(status)) {
				statusQuery = " and (mb.STATUS= 'ATN' or mb.STATUS= 'PND' or (mb.STATUS= 'RSV' and NOW() <= mb.end_datetime_book))";
			} else if (Status.CAN.name().equals(status)) {
				statusQuery = " and (mb.STATUS= 'CAN')";
			} else if (Status.NAT.name().equals(status)) {
				statusQuery = " and (mb.STATUS= 'NAT' or (mb.STATUS= 'RSV' and  NOW() > mb.end_datetime_book))";
			}
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");

		sql.append(" CASE  ");
		sql.append("  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NULL THEN 'Golfing Bay' ");
		sql.append("  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NULL THEN 'Tennis Court' ");
		sql.append("  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NOT NULL THEN 'Golf Private Coaching' ");
		sql.append("  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NOT NULL THEN 'Tennis Private Coaching' ");
		sql.append("  END AS  serviceType,");
		sql.append("  CASE  ");
		sql.append("  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NULL THEN CONCAT('GBF','-',mb.resv_id) ");
		sql.append("  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NULL THEN  CONCAT('TCF','-',mb.resv_id) ");
		sql.append("  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NOT NULL THEN  CONCAT('GPC','-',mb.resv_id) ");
		sql.append("  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NOT NULL THEN CONCAT('TPC','-',mb.resv_id) ");
		sql.append("  END  AS reservationId,");
		sql.append("  m.academy_no AS patronId,");
		sql.append(" CONCAT(cor.transaction_no,'') AS transactionCode, ");
		sql.append(" CONCAT(cp.given_name,' ',cp.surname)  AS patronName,");
		/***
		 *  SGG-2791
		 */
		sql.append(" DATE_FORMAT(mb.begin_datetime_book,'%Y/%m/%d %H:%i')  AS startTime,");
		sql.append(" DATE_FORMAT(DATE_ADD(mb.end_datetime_book,  INTERVAL 1 SECOND),'%Y/%m/%d %H:%i') AS endTime,");

		sql.append(" CASE  ");
		sql.append(
				"  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NOT NULL THEN CONCAT('Coach: ', kp.given_name, ' ', kp.surname) ");
		sql.append(
				"  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NOT NULL THEN CONCAT('Coach: ', kp.given_name, ' ', kp.surname) ");
		sql.append(" ELSE ");
		sql.append("  ( ");
		sql.append("SELECT ");
		sql.append(
				"   IF(mb.status='ATN',CONCAT(IF((mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NULL),'Bay# ', 'Court# '),GROUP_CONCAT(DISTINCT fm.facility_name ORDER BY CAST(fm.facility_no AS SIGNED) ASC SEPARATOR ',')), ");
		sql.append("	( ");
		sql.append("	CASE mb.resv_facility_type ");
		sql.append("	WHEN 'GOLF' THEN fac.caption ");
		sql.append("	WHEN 'TENNIS' THEN fst.name ");
		sql.append("    END	");
		sql.append("	) ");
		sql.append("   )  FROM member_reserved_facility AS mrf ");
		sql.append("      LEFT JOIN  facility_timeslot AS flt ON  flt.facility_timeslot_id = mrf.facility_timeslot_id ");
		sql.append("      LEFT JOIN facility_master AS fm ON flt.facility_no = fm.facility_no ");
		sql.append("      WHERE mrf.resv_id=mb.resv_id ");
		sql.append(" ) END AS location, ");

		// sql.append(" CASE");
		// sql.append(" WHEN rftb.sys_id IS NULL THEN");
		// sql.append(" CASE ");
		// sql.append(" WHEN um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF' THEN 'in-person' ");
		// sql.append(" WHEN um.user_type='CUSTOMER' THEN 'online' ");
		// sql.append(" WHEN um.user_type IS NULL THEN 'online' ");
		// sql.append(" END AS sourceType, ");
		sql.append(" CASE  WHEN rftb.sys_id IS NOT NULL THEN CONCAT('Bundled','',mb.resv_id) ");
		sql.append(" ELSE ");
		sql.append("	(CASE cor.payment_location_code WHEN 'FD1' THEN 'Front Desk 1' WHEN 'FD2' THEN 'Front Desk 2' WHEN 'WP' THEN 'Patron Web' ");
		sql.append("	ELSE cor.payment_location_code END) END AS sourceType,");
		// sql.append(" cor.payment_location_code END AS sourceType, ");

		// update by Colin begin
		sql.append(" CASE ");
		sql.append(" WHEN mb. STATUS = 'ATN' THEN 'Checked-in' ");
		sql.append(" WHEN mb. STATUS = 'CAN' THEN 'Canceled' ");
		sql.append(" WHEN mb. STATUS = 'PND' AND ADDTIME(mb.create_date, '00:15:00') >= NOW() THEN 'Paying' ");
		sql.append(" WHEN mb. STATUS = 'PND' AND ADDTIME(mb.create_date, '00:15:00') < NOW() THEN 'Pending for Payment' ");
		sql.append(" ELSE ");
		sql.append("   CASE ");
		sql.append("     WHEN NOW() <= mb.end_datetime_book THEN 'Ready to check-in' ");
		sql.append("     ELSE ");
		sql.append("      CASE WHEN NOW() > mb.end_datetime_book THEN 'Overdue' ELSE 'Unknown' END ");
		sql.append("     END ");
		sql.append(" END  AS status, ");

		sql.append(" CASE");
		sql.append("  WHEN rftb.sys_id IS NULL THEN");
		// update by Colin end
		// update christ
		sql.append(" CASE");
		// if createBy is staff show staffName
		sql.append(
				"  WHEN mb.create_by IS NOT NULL AND (um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF')  THEN  CONCAT(sp.given_name,' ',sp.surname)");
		// if createBy is customer show customerName
		sql.append("  WHEN mb.create_by IS NOT NULL AND um.user_type='CUSTOMER' THEN CONCAT(cp.given_name,' ',cp.surname)");
		// if createBy is null show customerName
		sql.append("  WHEN mb.create_by IS NULL  THEN CONCAT(cp.given_name,' ',cp.surname) ");
		sql.append(" END");
		sql.append("  WHEN   rftb.sys_id IS NOT NULL THEN  'System'");
		sql.append(" END AS createBy,");

		sql.append(" mb.update_by AS updateUser, ");
		sql.append(" IF(mb.update_date  IS NULL,'', DATE_FORMAT(mb.update_date,'%Y/%m/%d')) AS updateDate, ");
		sql.append(" DATE_FORMAT(mb.create_date,'%Y/%m/%d') AS createDate, ");
		sql.append("  '1' AS qty  ");
		sql.append(" FROM member_facility_type_booking AS mb ");
		sql.append(" LEFT JOIN room_facility_type_booking AS rftb ON mb.resv_id=rftb.facility_type_resv_id ");
		sql.append(" LEFT JOIN customer_profile AS cp ON mb.customer_id=cp.customer_id ");
		sql.append(" LEFT JOIN user_master AS um  ON mb.create_by =um.user_id ");
		sql.append("  LEFT JOIN staff_profile AS sp ON mb.create_by=sp.user_id ");
		sql.append("  LEFT JOIN staff_profile kp ON mb.exp_coach_user_id = kp.user_id ");
		sql.append("  LEFT JOIN customer_order_trans AS cor ON cor.order_no=mb.order_no ");

		sql.append(" LEFT JOIN facility_sub_type AS fst  ON mb.facility_subtype_id=fst.subtype_id  ");
		sql.append(" LEFT JOIN member_facility_book_addition_attr AS maa ON mb.resv_id=maa.resv_id  ");
		sql.append(" LEFT JOIN facility_attribute_caption fac ON maa.attribute_id=fac.attribute_id ");
		sql.append(" LEFT JOIN member m ON  mb.customer_id=m.customer_id ");
		sql.append(" WHERE DATE_FORMAT(mb.begin_datetime_book,'%Y-%m-%d') = '" + date + "'" + typeQuery + statusQuery);

		logger.debug("sql:" + sql);
		return sql;
	}

	/***
	 * create golf/tennis course
	 * 
	 * @return
	 */
	private StringBuilder createCourseSql(String date, String type, String status) {
		String typeQuery = "";
		if (type != null) {
			typeQuery = " and cm.course_type= '" + type + "'";
		}
		String statusQuery = "";
		if (status != null) {
			if (Status.RSV.name().equals(status)) {
				statusQuery = " and (ce.status= 'ACT' or ce.status= 'REG' or ce.status= 'PND')";
			} else if (Status.CAN.name().equals(status)) {
				statusQuery = " and (ce.status= 'CAN')";
			} else if (Status.NAT.name().equals(status)) {
				statusQuery = " and (ce.status= 'REJ')";
			}
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" CASE  cm.course_type ");
		sql.append(" WHEN 'GSS' THEN 'Golf Course'");
		sql.append(" WHEN 'TSS' THEN 'Tennis Course'");
		sql.append(" END AS serviceType,");
		sql.append(" CASE cm.course_type ");
		sql.append(" WHEN 'GSS' THEN CONCAT('GCE','-',ce.enroll_id)");
		sql.append(" WHEN 'TSS' THEN CONCAT('TCE','-',ce.enroll_id)");
		sql.append(" END AS reservationId,");
		sql.append(" m.academy_no AS patronId, ");
		sql.append(" CONCAT(cor.transaction_no,'') AS transactionCode, ");
		sql.append(" CONCAT(cp.given_name,' ',cp.surname)  AS patronName, ");
		sql.append(" DATE_FORMAT(cm.regist_begin_date,'%Y/%m/%d') AS startTime, ");
		sql.append(" DATE_FORMAT(cm.regist_due_date,'%Y/%m/%d') AS endTime,");
		sql.append(" CASE  cm.course_type ");
		sql.append(" WHEN 'GSS' THEN CONCAT('Course: ', cm.course_name) ");
		sql.append(" WHEN 'TSS' THEN CONCAT('Course: ', cm.course_name) END ");
		sql.append(" AS location, ");

		// sql.append(" CASE ");
		// sql.append(" WHEN um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF' THEN 'in-person' ");
		// sql.append(" WHEN um.user_type='CUSTOMER' THEN 'online'");
		// sql.append(" WHEN um.user_type IS NULL THEN 'online' ");
		// sql.append(" END AS sourceType,");
		sql.append("	CASE cor.payment_location_code WHEN 'FD1' THEN 'Front Desk 1' WHEN 'FD2' THEN 'Front Desk 2' WHEN 'WP' THEN 'Patron Web' ");
		sql.append("	ELSE cor.payment_location_code END AS sourceType, ");
		// sql.append(" cor.payment_location_code AS sourceType, ");

		sql.append("  CASE ce.status ");
		sql.append("  WHEN 'ACT' THEN 'accepted' ");
		sql.append("  WHEN 'REG' THEN 'registered'");
		sql.append("  WHEN 'PND' THEN 'pending'");
		sql.append("  WHEN 'REJ' THEN 'rejected'");
		sql.append("  WHEN 'CAN' THEN 'Cancelled'");
		sql.append("  END  AS status,");

		// christ update createBy
		sql.append(" CASE ");
		sql.append(
				"  WHEN ce.create_by IS NOT NULL AND (um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF') THEN  CONCAT(sp.given_name,' ',sp.surname)");
		sql.append("  WHEN ce.create_by IS NOT NULL AND um.user_type='CUSTOMER'  THEN CONCAT(cp.given_name,' ',cp.surname)");
		sql.append("  WHEN ce.create_by IS NULL  THEN CONCAT(cp.given_name,' ',cp.surname)");
		sql.append("  END  AS createBy,");

		sql.append("  ce.update_by AS updateUser, ");
		sql.append("  IF(ce.update_date  IS NULL,'', DATE_FORMAT(ce.update_date,'%Y/%m/%d')) AS updateDate, ");
		sql.append("  DATE_FORMAT(ce.create_date,'%Y/%m/%d') AS createDate, ");
		sql.append("  '1' AS qty     ");
		sql.append(" FROM  course_master AS cm, ");
		sql.append(" course_enrollment AS ce, ");
		sql.append(" customer_profile AS cp, ");
		sql.append(" user_master AS um, ");
		sql.append(" staff_profile AS sp, ");
		sql.append(" customer_order_trans  cor, ");
		sql.append(" member m ");

		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND cm.course_id = ce.course_id ");
		sql.append(" AND ce.cust_order_no=cor.order_no ");
		sql.append(" AND ce.customer_id = cp.customer_id ");
		sql.append(" AND um.user_id = ce.create_by ");
		sql.append(" AND ce.customer_id = m.customer_id ");
		sql.append(" AND ce.create_by=sp.user_id ");
		sql.append(" AND DATE_FORMAT(ce.enroll_date,'%Y-%m-%d') = '" + date + "'" + typeQuery + statusQuery);
		logger.debug("sql:" + sql);
		return sql;
	}

	/***
	 * guest room booking sql
	 * 
	 * @return
	 */
	private StringBuilder createGuestRoomSql(String date, String type, String status) {
		String statusQuery = "";
		if (status != null) {
			if (Status.RSV.name().equals(status)) {
				statusQuery = " and (rr.status= 'SUC' or rr.status= 'RSV')";
			} else if (Status.CAN.name().equals(status)) {
				statusQuery = " and (rr.status= 'CAN' or rr.status= 'RFU')";
			} else if (Status.NAT.name().equals(status)) {
				statusQuery = " and (rr.status= 'IGN')";
			}
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append("  'Accommodation Reservation' AS serviceType,");
		sql.append(" CONCAT('GRM','-',rr.resv_id) AS reservationId,");
		sql.append(" m.academy_no  AS patronId,");
		sql.append(" CONCAT(cor.transaction_no,'') AS transactionCode, ");
		sql.append(" CONCAT(cp.given_name,' ',cp.surname)  AS patronName,");
		sql.append(" DATE_FORMAT(rr.arrival_date,'%Y/%m/%d') AS startTime, ");
		sql.append(" DATE_FORMAT(rr.departure_date,'%Y/%m/%d') AS endTime,");
		sql.append(" tt.type_name AS location,");
		// sql.append(" rm.room_no AS location,");
		// sql.append(" CASE ");
		// sql.append(" WHEN um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF' THEN 'in-person'");
		// sql.append(" WHEN um.user_type='CUSTOMER' THEN 'online'");
		// sql.append(" WHEN um.user_type IS NULL THEN 'online' ");
		// sql.append(" END AS sourceType,");
		sql.append("	CASE cor.payment_location_code WHEN 'FD1' THEN 'Front Desk 1' WHEN 'FD2' THEN 'Front Desk 2' WHEN 'WP' THEN 'Patron Web' ");
		sql.append("	ELSE cor.payment_location_code END AS sourceType, ");
		// sql.append(" cor.payment_location_code AS sourceType, ");
		sql.append(" CASE rr.status");

		sql.append(" WHEN 'SUC' THEN 'success' ");
		sql.append(" WHEN 'IGN' THEN 'ignore'");
		sql.append(" WHEN 'CAN' THEN 'cancelled'");
		sql.append(" WHEN 'RFU' THEN 'refund' ");
		sql.append(" WHEN 'RSV' THEN 'resevered' ");

		sql.append(" END  AS status,");
		// update christ createBy
		sql.append(" CASE ");
		sql.append(
				"  WHEN rr.create_by IS NOT NULL AND (um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF') THEN  CONCAT(sp.given_name,' ',sp.surname)");
		sql.append("  WHEN rr.create_by IS NOT NULL AND um.user_type='CUSTOMER'  THEN CONCAT(cp.given_name,' ',cp.surname)");
		sql.append("  WHEN rr.create_by IS NULL  THEN CONCAT(cp.given_name,' ',cp.surname)");
		sql.append("  END AS createBy,");

		sql.append(" rr.update_by AS updateUser, ");
		sql.append(" DATE_FORMAT(rr.update_date,'%Y/%m/%d') AS updateDate, ");
		sql.append(" DATE_FORMAT(rr.create_date,'%Y/%m/%d') AS createDate, ");
		sql.append(" '1' AS qty ");
		sql.append(" FROM room_reservation_rec AS rr ");
		sql.append(" LEFT JOIN customer_profile AS cp ON rr.customer_id=cp.customer_id ");
		sql.append(" LEFT JOIN user_master AS um  ON um.user_id =rr.create_by ");
		sql.append(" LEFT JOIN staff_profile AS sp ON rr.create_by=sp.user_id");
		sql.append(" LEFT JOIN room AS rm ON rr.room_id=rm.room_id ");
		sql.append(" LEFT JOIN member m ON  rr.customer_id=m.customer_id ");
		sql.append(" LEFT JOIN room_type tt ON  rr.room_type_code=tt.type_code ");
		sql.append(" LEFT JOIN customer_order_trans AS cor ON cor.order_no=rr.order_no ");

		sql.append(" WHERE DATE_FORMAT(rr.arrival_date,'%Y-%m-%d') = '" + date + "'" + statusQuery);
		logger.debug("sql:" + sql);

		return sql;
	}

	private StringBuilder createDayPassSql(String date, String type, String status) {
		String statusQuery = "";
		if (status != null) {
			if (Status.RSV.name().equals(status)) {
				statusQuery = " and (t.order_status='OPN' or t.order_status= 'CMP')";
			} else if (Status.CAN.name().equals(status)) {
				statusQuery = " and t.order_status='CAN'";
			} else if (Status.NAT.name().equals(status)) {
				statusQuery = " and t.order_status='REJ'";
			}
		}
		StringBuilder sql = new StringBuilder();

		sql.append(" select distinct  * from ( ");
		sql.append(" select 'Day Pass' AS serviceType,CONCAT('DP', '-', t.order_no) AS reservationId,");
		sql.append(" (select m.academy_no from  member m where m.customer_id=t.customer_id) AS patronId , ");
		sql.append(" CONCAT(cor.transaction_no,'') AS transactionCode, ");
		sql.append("  case when t.staff_user_id is null then  (select  concat(c.salutation,' ',c.given_name,' ',c.surname) ");
		sql.append(" from customer_profile c where c.customer_id=t.customer_id )  ");
		sql.append(" else  (select   concat(c.given_name,' ',c.surname)  from staff_profile c where c.user_id=t.staff_user_id)  end patronName,");
		sql.append(" (select DATE_FORMAT(copc.effective_from,'%Y/%m/%d') from customer_order_permit_card copc where   copc.customer_order_no=t.order_no limit 1 ) ");
		sql.append("  startTime, (select DATE_FORMAT(copc.effective_to,'%Y/%m/%d') from customer_order_permit_card copc where   copc.customer_order_no=t.order_no  limit 1) endTime, ");
		sql.append(" 'Day Pass' AS location,");
//		sql.append(" CASE cor.payment_location_code WHEN 'FD1' THEN 'Front Desk 1' WHEN 'FD2' THEN 'Front Desk 2' WHEN 'WP' THEN 'Patron Web' ");
//		sql.append(" ELSE cor.payment_location_code END AS sourceType, ");
		sql.append(" '' AS sourceType, ");
		sql.append(" CASE t.order_status ");
		sql.append(" WHEN 'OPN' THEN 'Open' ");
		sql.append(" WHEN 'CAN' THEN 'Cancelled'");
		sql.append(" WHEN 'CMP' THEN 'Paid and transaction completed'");
		sql.append(" WHEN 'REJ' THEN 'Rejected' ");
		sql.append(" END  AS status,");
		sql.append(" t.create_date AS createBy,");
		sql.append(" t.update_by AS updateUser, ");
		sql.append(" DATE_FORMAT(t.update_date,'%Y/%m/%d')  AS updateDate, ");
		sql.append(" DATE_FORMAT(t.create_date,'%Y/%m/%d') AS createDate, ");
		sql.append(" '1' AS qty ");
		sql.append("  from customer_order_hd  t ");
		sql.append("  inner join customer_order_permit_card card  on  t.order_no=card.customer_order_no ");
		sql.append("  LEFT JOIN customer_order_trans AS cor ON cor.order_no=t.order_no ");
		
		sql.append("  where  DATE_FORMAT(card.effective_from,'%Y-%m-%d') = '" + date + "'" + statusQuery + "  ) purchase ");

		logger.debug("sql:" + sql);

		return sql;
	}

}
