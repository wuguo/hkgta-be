package com.sinodynamic.hkgta.service.crm.backoffice.presentation;

import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.dto.crm.FolderInfoDto;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface UploadPresentMaterialService extends IServiceBase<PresentMaterial> {
	String MEDIA_IMAGE = "picture";
	
	String MEDIA_VIDEO = "video";
	
	void uploadPicture(PresentMaterial presentMaterial);

	void deleteMedia(Long[] ids,String userId);
	
	void recoverMedia(Long[] ids,String userId);

	FolderInfoDto viewFolder(Long folderId, String terminal);
	
	void updateDescription(Long materialId, String description,String userId);
	
	byte[] getMediaAsByteStream(Long materialId, String mediaType, boolean isThumbnail);
	
	void uploadMedia(Long folderId, String materialDesc, MultipartFile file, String mediaType,String userId);
	
	void uploadMedia(Long folderId, String[] materialDesc, MultipartFile[] file, String[] mediaType,String userId);
}

