package com.sinodynamic.hkgta.service.crm.ibeacon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.StaffSitePoiDao;
import com.sinodynamic.hkgta.dao.ibeacon.IbeaconDao;
import com.sinodynamic.hkgta.dao.ibeacon.IbeaconNearbyPersonDao;
import com.sinodynamic.hkgta.dao.ibeacon.SitePoiDao;
import com.sinodynamic.hkgta.dao.pms.RoomDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.BeaconManageDto;
import com.sinodynamic.hkgta.dto.crm.BeaconMemberDto;
import com.sinodynamic.hkgta.dto.crm.BeaconRoomDto;
import com.sinodynamic.hkgta.dto.crm.IBeaconNearbyPersonDto;
import com.sinodynamic.hkgta.dto.crm.ReservationDataDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.StaffSitePoiMonitor;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.ibeacon.Ibeacon;
import com.sinodynamic.hkgta.entity.ibeacon.IbeaconNearbyPerson;
import com.sinodynamic.hkgta.entity.ibeacon.IbeaconNearbyPersonPK;
import com.sinodynamic.hkgta.entity.ibeacon.SitePoi;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.TargetTypeCode;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class IbeaconServiceImpl extends ServiceBase<IbeaconNearbyPerson> implements IbeaconService {
	private static final Logger logger = Logger.getLogger(IbeaconServiceImpl.class);
	private static String UUID = "E2C56DB5-DFFB-48D2-B060-D0F5A71096CC";
	
	@Autowired
	private IbeaconDao ibeaconDao;
	
	@Autowired
	private IbeaconNearbyPersonDao ibeaconNearbyPersonDao;
	
	@Autowired
	private SitePoiDao sitePoiDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private RoomDao roomDao;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private StaffSitePoiDao staffSitePoiDao;
	
	@Transactional
	public ResponseResult addVistorFromApp(IBeaconNearbyPersonDto dto) throws Exception {
		Long customerId = dto.getCustomerId();
		Long minor = dto.getMinor();
		Long major = dto.getMajor();
		Long distance = Long.valueOf(dto.getDistance());
		logger.info("receive the ibeacon with major = "+major+" minor="+ minor + " uuid="+dto.getUuid() +" customerId="+customerId);
		Ibeacon ibeacon = ibeaconDao.getByMinorAndMajor(major, minor);
		if(ibeacon==null){
			responseResult.initResult(GTAError.IbeaconError.NO_BEACON_REG);
			return responseResult;
		}
		Member member = memberDao.getMemberByCustomerId(customerId);
		if(member==null||member.getUserId()==null){
			responseResult.initResult(GTAError.IbeaconError.Activated_Member_REG);
			return responseResult;
		}
		logger.info("member UserId = "+ member.getUserId() + " IbeaconId ="+ ibeacon.getIbeaconId() + " SitePoiId= "+ ibeacon.getSitePoiId());
		IbeaconNearbyPerson ibeaconNearbyPerson = ibeaconDao.getNearbyMemberById(member.getUserId(), ibeacon.getIbeaconId());
		if (ibeaconNearbyPerson == null) {
			IbeaconNearbyPerson nearMember = new IbeaconNearbyPerson();
			IbeaconNearbyPersonPK pk = new IbeaconNearbyPersonPK();
			pk.setUserId(member.getUserId());
			pk.setIbeaconId(ibeacon.getIbeaconId());
			nearMember.setId(pk);
			nearMember.setLastTimestamp(new Date());
			nearMember.setDistance(distance);
			ibeaconNearbyPersonDao.save(nearMember);
		} else {
			ibeaconNearbyPerson.setLastTimestamp(new Date());
			ibeaconNearbyPerson.setDistance(distance);
			ibeaconNearbyPersonDao.update(ibeaconNearbyPerson);
		}
		//push message to staff App
		pushNotificationForStaffApp(member,ibeacon.getSitePoiId());
		responseResult.initResult(GTAError.Success.SUCCESS);

		return responseResult;
		
	}
	
	private void pushNotificationForStaffApp(Member member, Long sitePoiId){
		if(member != null){
			List<StaffSitePoiMonitor> monitors = staffSitePoiDao.listBySiteId(sitePoiId);
			String[] staffs = new String[monitors.size()];
			if(monitors!= null && monitors.size()>0){
				for(int i= 0; i<monitors.size(); i++){
					staffs[i] = monitors.get(i).getStaffUserId();
					logger.info("push message to staffs = "+staffs[i]);
				}
			}
			CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(member.getCustomerId());
			SitePoi poi = sitePoiDao.get(SitePoi.class, sitePoiId);
			String[] location = new String[]{profile.getGivenName()+" "+profile.getSurname(), poi!=null?poi.getDescription():""};
			logger.info("push message to location = "+ poi!=null?poi.getDescription():"");
			devicePushService.pushMessage(staffs, Constant.TEMPLATE_ID_APP_IBEACON_STAFF_NOTIFICATION, location, Constant.STAFFAPP_PUSH_APPLICATION);
		}
	}
	
	@Transactional
	public ResponseResult getRecogBeaconList(ListPage<IbeaconNearbyPerson> page,String byMajor,String status,String targetType){
		ListPage<IbeaconNearbyPerson> listPage = ibeaconNearbyPersonDao.getBeaconList(page,new BeaconManageDto(),byMajor,status,targetType);
		List<Object> listBeacon = listPage.getDtoList();
		
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listBeacon);
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult changeIbeaconStatus(BeaconManageDto dto,String currentUserId){
		String status = dto.getStatus();
		Long ibeaconIdDB = dto.getIbeaconIdDB();
		boolean check = ibeaconDao.updateIbeaconStatus(status, ibeaconIdDB,currentUserId );
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ResponseResult saveOrEditIbeaconInfo(BeaconManageDto dto,String currentUserId) throws Exception{
		String description = dto.getBeaconName();
		Long poiId = dto.getPoiId();
		Long minor = dto.getIbeaconId();
		Long major = dto.getMajor();
		Long ibeaconIdDB = dto.getIbeaconIdDB();
		String venueCode = dto.getVenueCode();
		String targetType = dto.getTargetType();
		String location = dto.getLocation();
		
		if(StringUtils.isEmpty(description)&&TargetTypeCode.MBR.name().equalsIgnoreCase(targetType)){
			throw new GTACommonException(GTAError.IbeaconError.BEACON_NAME_REQ);
		}
		
		if(StringUtils.isEmpty(minor)){
			throw new GTACommonException(GTAError.IbeaconError.BEACON_ID_REQ);
		}
		if(StringUtils.isEmpty(major)){
			throw new GTACommonException(GTAError.IbeaconError.MAJOR_ID_REQ);
		}
		
		boolean minorStatus = ibeaconDao.checkAvailableMinor(major, minor, ibeaconIdDB);
		if (!minorStatus) {
			throw new GTACommonException(GTAError.IbeaconError.BEACON_ALREADY_REG);
		}
		
		if(!TargetTypeCode.MBR.name().equalsIgnoreCase(targetType)&&!TargetTypeCode.HKC.name().equalsIgnoreCase(targetType)){
			throw new GTACommonException(GTAError.IbeaconError.TARGET_TYPE_REQ);
		}
		if(TargetTypeCode.MBR.name().equalsIgnoreCase(targetType) && StringUtils.isEmpty(venueCode)){
			throw new GTACommonException(GTAError.IbeaconError.VENUE_CODE_REQ);
		}
		Room room = null;
		if(TargetTypeCode.HKC.name().equalsIgnoreCase(targetType)){
			if(StringUtils.isEmpty(dto.getPresentRoomId())){
				throw new GTACommonException(GTAError.IbeaconError.PRESENT_ROOM_ID_REQ);
			}
			room = roomDao.getByRoomId(dto.getPresentRoomId());
			if (room ==null) {
				throw new GTACommonException(GTAError.IbeaconError.NO_ROOM_FOUND);
			}
		}
		if(ibeaconIdDB!=null){
			if(StringUtils.isEmpty(poiId)){
				throw new GTACommonException(GTAError.IbeaconError.POI_ID_REQ);
			}
			
			if(StringUtils.isEmpty(ibeaconIdDB)){
				throw new GTACommonException(GTAError.IbeaconError.BEACONDB_ID_REQ);
			}
			Ibeacon ibeacon = ibeaconDao.getIbeaconByPK(ibeaconIdDB);
			if (ibeacon == null) {
				throw new GTACommonException(GTAError.IbeaconError.NO_BEACON_REG);
			}
			if(TargetTypeCode.MBR.name().equalsIgnoreCase(targetType)){
				boolean checkMember = ibeaconDao.updateMemberBeaconDetail(description, venueCode, poiId, currentUserId);
				ibeacon.setMinor(minor);
				ibeacon.setSitePoiId(poiId);
				ibeacon.setUpdateBy(currentUserId);
				ibeacon.setUpdateDate(new Date());
				ibeaconDao.update(ibeacon);
				
			}else if(TargetTypeCode.HKC.name().equalsIgnoreCase(targetType)){
				if(StringUtils.isEmpty(dto.getPastRoomId())){
					throw new GTACommonException(GTAError.IbeaconError.PAST_ROOM_ID_REQ);
				}
//				if(room!=null&&room.getSitePoiId()!=null){
//					throw new GTACommonException(GTAError.IbeaconError.ROOM_ASSIGNED_ALREADY);
//				}
				Long presentPoiId = null;
				if(null!=room){
					if (room.getSitePoiId()== null) {
						SitePoi newSitePoi = new SitePoi();
						newSitePoi.setCreateBy(currentUserId);
						newSitePoi.setCreateDate(new Date());
						newSitePoi.setDescription("Room "+ room.getRoomNo());
						newSitePoi.setTargetTypeCode(TargetTypeCode.HKC.name());
						presentPoiId = (Long) sitePoiDao.save(newSitePoi);
					}else{
						presentPoiId = room.getSitePoiId();
						SitePoi sitePoi = sitePoiDao.get(SitePoi.class, presentPoiId);
						sitePoi.setDescription("Room "+ room.getRoomNo());
						sitePoi.setTargetTypeCode(TargetTypeCode.HKC.name());
						sitePoi.setUpdateBy(currentUserId);
						sitePoi.setUpdateDate(new Date());
						sitePoiDao.update(sitePoi);
					}
				}
				
				ibeacon.setMinor(minor);
				ibeacon.setLocation(location);
				ibeacon.setSitePoiId(presentPoiId);
				ibeacon.setUpdateBy(currentUserId);
				ibeacon.setUpdateDate(new Date());
				ibeaconDao.update(ibeacon);
				
				boolean checkHouseKeep = ibeaconDao.updateHousekeepBeaconDetail("Room "+ room.getRoomNo(), dto.getPastRoomId(),
						dto.getPresentRoomId(), poiId,presentPoiId, currentUserId);
				
				
			}
			
			
		}else{
//			if(TargetTypeCode.HKC.name().equalsIgnoreCase(targetType)){
//				if (room.getSitePoiId() != null) {
//					throw new GTACommonException(GTAError.IbeaconError.ROOM_ASSIGNED_ALREADY);
//				}
//			}
			SitePoi newSitePoi = new SitePoi();
			Long sitePoiId = null;
			if((TargetTypeCode.HKC.name().equalsIgnoreCase(targetType)&&room.getSitePoiId()==null)
					||TargetTypeCode.MBR.name().equalsIgnoreCase(targetType)){
				
				newSitePoi.setCreateBy(currentUserId);
				newSitePoi.setCreateDate(new Date());
				
				if(TargetTypeCode.MBR.name().equalsIgnoreCase(targetType)){
					newSitePoi.setVenueCode(venueCode);
					newSitePoi.setDescription(description);
				}else{
					newSitePoi.setDescription("Room "+room.getRoomNo());
				}
				
				newSitePoi.setTargetTypeCode(targetType);
				sitePoiId = (Long) sitePoiDao.save(newSitePoi);
			}
			
			Ibeacon newBeacon = new Ibeacon();
			if(TargetTypeCode.HKC.name().equalsIgnoreCase(targetType)){
				if(room.getSitePoiId()==null){
					room.setSitePoiId(sitePoiId);
					roomDao.update(room);
				}else{
					sitePoiId = room.getSitePoiId();
				}
				newBeacon.setLocation(location);
			}
			
			newBeacon.setCreateBy(currentUserId);
			newBeacon.setCreateDate(new Date());
			newBeacon.setGuid(UUID);
			newBeacon.setMajor(major);
			newBeacon.setMinor(minor);
			newBeacon.setSitePoiId(sitePoiId);
			newBeacon.setStatus(Constant.General_Status_ACT);
			ibeaconDao.save(newBeacon);
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getByLocationType(String status){
		List<BeaconManageDto> list = ibeaconDao.getListByStatus(status);
		responseResult.initResult(GTAError.Success.SUCCESS,list);
		return responseResult;
	}
	
	@SuppressWarnings("unused")
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public ResponseResult deleteIbeacon(Long ibeaconIdDB,String loginUserId){
		Ibeacon ibeacon = ibeaconDao.getIbeaconByPK(ibeaconIdDB);
		int membercheck = ibeaconDao.deleteIbeaconNearbyPersonByIbeaconId(ibeaconIdDB);
		
		List<Ibeacon> ibeaconList = ibeaconDao.getIbeaconBySitePoi(ibeacon.getSitePoiId());
		if(ibeaconList!=null&&ibeaconList.size()<=1){
			int check = ibeaconDao.deleteIbeaconById(ibeaconIdDB);
			int room = ibeaconDao.removeSitPoiInRoom(loginUserId, ibeacon.getSitePoiId());
			int SitePoi = ibeaconDao.deleteSitePoi(ibeacon.getSitePoiId());
		}else{
			int check = ibeaconDao.deleteIbeaconById(ibeaconIdDB);
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public ResponseResult getBeaconMember(Long[] poiIdArray,String venueCode,Integer beaconMemberCount){
		Map<String, Object> map = ibeaconDao.getMemberByVenueCode(poiIdArray,venueCode,beaconMemberCount);
		
		List<BeaconMemberDto> member = (List<BeaconMemberDto>) map.get("member");
		List<BeaconMemberDto> statusMap = (List<BeaconMemberDto>) map.get("statusCount");
		List<BeaconMemberDto> statusTotalMap = (List<BeaconMemberDto>) map.get("statusTotalCount");
		int enable = 0;
		int all    = 0;
		for(BeaconMemberDto dto: statusMap){
			if(dto.getStatus().equals(Constant.General_Status_ACT)){
				enable++;
			}
		}
		
		all = statusTotalMap.size();
		
		Map<String, Object> mapRes = new HashMap<String, Object>();
		mapRes.put("memberDto", member);
		mapRes.put("enableCount", enable);
		mapRes.put("allCount", all);
		responseResult.initResult(GTAError.Success.SUCCESS,mapRes);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getBeaconByVenueCode(String venueCode){
		List<BeaconManageDto> list = ibeaconDao.getBeaconByVenueCode(venueCode);
		responseResult.initResult(GTAError.Success.SUCCESS,list);
		return responseResult;
	}

	@Transactional
	public ResponseResult checkAvailability(Long major, Long minor,Long ibeaconIdDB){
		boolean status = ibeaconDao.checkAvailableMinor(major, minor, ibeaconIdDB);
		if(status){
			responseResult.initResult(GTAError.Success.SUCCESS,true);
		}else{
			responseResult.initResult(GTAError.Success.SUCCESS,false);
		}
		return responseResult;
	}
	
	@Transactional(rollbackFor=Exception.class)
	public ResponseResult updateMajor(Long major,String targetType){
		boolean  validate = ibeaconDao.checkMajorAvailability(major, targetType);
		if(!validate){
			throw new GTACommonException(GTAError.IbeaconError.MAJOR_UNAVAILABLE);
		}
		boolean status = ibeaconDao.changeMajor(major,targetType);
		if(!status){
			throw new GTACommonException(GTAError.IbeaconError.UPDATE_IBEACON_MAJOR_FAIL);
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String module){
		SysCode statusOpt1 = new SysCode("ACT","Enabled",null);
		SysCode statusOpt2 = new SysCode("NACT","Disabled",null);
		if (TargetTypeCode.MBR.name().equalsIgnoreCase(module)) {
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Beacon Name", "a.description", String.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Location Type", "c.venue_name", String.class.getName(), "", 2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Beacon ID", "b.minor", Integer.class.getName(), "", 3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Status", "b.status", String.class.getName(), Arrays.asList(statusOpt1,statusOpt2), 4);
			return Arrays.asList(condition1, condition2, condition3, condition4);
		} else if (TargetTypeCode.HKC.name().equalsIgnoreCase(module)) {
			AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Beacon Name", "a.description", String.class.getName(), "", 1);
			AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Location", "b.location", String.class.getName(), "", 2);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Room #", "r.room_no", String.class.getName(), "", 3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Beacon ID", "b.minor", Integer.class.getName(), "", 4);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Status", "b.status", String.class.getName(), Arrays.asList(statusOpt1,statusOpt2), 5);
			return Arrays.asList(condition1, condition2, condition3, condition4,condition5);
		}
		return new ArrayList<AdvanceQueryConditionDto>();
	}

	@Override
	@Transactional
	public ResponseResult getRoomBeaconsList() {
		List<BeaconRoomDto> dtoList = ibeaconDao.getRoomBeaconsList();
		responseResult.initResult(GTAError.Success.SUCCESS, dtoList);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getTheNextAvailableMinorId(Long major){
		Long minor = ibeaconDao.getTheNextAvailableMinorId(major);
		responseResult.initResult(GTAError.Success.SUCCESS, minor);
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getMemberByBeaconIdForTablet(ListPage<Ibeacon> page,String resvSortBy,Integer resvPageNumber,Integer resvPageSize, String resvIsAscending,String venueCode) throws Exception{
		
		
		Calendar now = Calendar.getInstance();
		Integer year = now.get(Calendar.YEAR);
		Integer month = now.get(Calendar.MONTH)+1;
		Integer day = now.get(Calendar.DAY_OF_MONTH);
		ListPage<Ibeacon> listPage = ibeaconDao.getMemberByVenueCodeForTablet(page,venueCode);
		List<Object> listMemberForTablet = listPage.getDtoList();
		for(Object object:listMemberForTablet){
			ListPage<MemberFacilityTypeBooking> resvNewPage = new ListPage<MemberFacilityTypeBooking>();
			if(resvIsAscending.equals("true")){
				resvNewPage.addAscending(resvSortBy);
			}else if (resvIsAscending.equals("false")){
				resvNewPage.addDescending(resvSortBy);
			}
			resvNewPage.setNumber(resvPageNumber);
			resvNewPage.setSize(resvPageSize);
			
			BeaconMemberDto dto = (BeaconMemberDto)object;
			ResponseResult bookings = memberFacilityTypeBookingService.getBookingRecordsByCustomerId(resvNewPage, dto.getCustomerId(), "AP", year.toString(), month.toString(), day.toString(),null,null);
			
			Data dataResv = (Data) bookings.getData();
			ReservationDataDto reservationDataDto = new ReservationDataDto();
			
			reservationDataDto.setLastPage(dataResv.isLastPage());
			reservationDataDto.setRecordCount(dataResv.getRecordCount());
			reservationDataDto.setPageSize(dataResv.getPageSize());
			reservationDataDto.setTotalPage(dataResv.getTotalPage());
			reservationDataDto.setCurrentPage(dataResv.getCurrentPage());
			reservationDataDto.setList(dataResv.getList());
			dto.setReservation(reservationDataDto);
			
			/*Leave it for now, may be used later
			List<CustomerAdditionInfoDto> favorList = customerAdditionInfoDao.getFavorByCustomerId(dto.getCustomerId());
			for(CustomerAdditionInfoDto customerAdditionInfo:favorList){
				switch (customerAdditionInfo.getCaptionId()) {
				case "8":
					dto.setAccommodationType(StringUtils.isEmpty(customerAdditionInfo.getCustomerInput())?"":customerAdditionInfo.getCustomerInput());
					break;
				case "9":
					dto.setFoodBeverage(StringUtils.isEmpty(customerAdditionInfo.getCustomerInput())?"":customerAdditionInfo.getCustomerInput());
					break;
				case "10":
					dto.setHobbies(StringUtils.isEmpty(customerAdditionInfo.getCustomerInput())?"":customerAdditionInfo.getCustomerInput());
					break;
				case "11":
					dto.setFoodAllergy(StringUtils.isEmpty(customerAdditionInfo.getCustomerInput())?"":customerAdditionInfo.getCustomerInput());
					break;
				case "12":
					dto.setVegetarian(StringUtils.isEmpty(customerAdditionInfo.getCustomerInput())?"":customerAdditionInfo.getCustomerInput());
					break;
				default:
					break;
				}
			}
			*/
			//Mars, update for 934
			//String timeDifference = DateConvertUtil.getTimeDifferenceHourAndMin(dto.getLastTimestamp());
			String timeDifference = DateConvertUtil.getTimeDifferenceForPast(dto.getLastTimestamp());
			
			dto.setTimeDifference(timeDifference);
		}
		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(listMemberForTablet);
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	}

}
