package com.sinodynamic.hkgta.service.sys;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.sys.ProgramMasterDao;
import com.sinodynamic.hkgta.dao.sys.RoleMasterDao;
import com.sinodynamic.hkgta.dao.sys.RoleProgramDao;
import com.sinodynamic.hkgta.dao.sys.UserRoleDao;
import com.sinodynamic.hkgta.dto.sys.AssignRoleDto;
import com.sinodynamic.hkgta.dto.sys.MenuDto;
import com.sinodynamic.hkgta.dto.sys.ProgramDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.crm.ProgramMaster;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;
import com.sinodynamic.hkgta.entity.crm.RoleProgram;
import com.sinodynamic.hkgta.entity.crm.UserRole;
import com.sinodynamic.hkgta.entity.crm.UserRolePK;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@Service
public class UserRoleServiceImpl extends ServiceBase<UserRole> implements UserRoleService{
    @Autowired
	private UserRoleDao userRoleDao;
    
    @Autowired
	private RoleMasterDao roleMasterDao;
    
    @Autowired
   	private ProgramMasterDao programMasterDao;
    
    @Autowired
   	private RoleProgramDao roleProgramDao;
    
    
	@Autowired
	UserRoleMap userRoleMap;
    
	@Override
	@Transactional(readOnly=true)
	public List<UserRoleDto> getUserRoleListByUserId(String userId) {
		return userRoleDao.getUserRoleListByUserId(userId);
	}

	@Override
	@CacheEvict(value="menuCache",allEntries=true)
	@Transactional
	public ResponseResult assignRole(AssignRoleDto dto,String createBy) {
		if (StringUtils.isEmpty(dto.getUserId())) {
			responseResult.initResult(GTAError.SysError.USER_ID_EMPTY);
			return responseResult;
		}
		if (StringUtils.isEmpty(dto.getRoleIds())) {
			responseResult.initResult(GTAError.SysError.ROLE_IDS_EMPTY);
			return responseResult;
		}
		String[] rIds = dto.getRoleIds().split(",");
		boolean flg = true;
		for (int i = 0; i < rIds.length; i++) {
			UserRole userRole = new UserRole();
			UserRolePK id =  new UserRolePK();
			id.setRoleId(rIds[i]);
			id.setUserId(dto.getUserId());
			userRole.setId(id);
			userRole.setCreateBy(createBy);
			try {
				userRoleDao.saveOrUpdate(userRole);
			} catch (HibernateException e) {
				logger.error(UserRoleServiceImpl.class.getName() + " assignRole Failed!", e);
				flg = false;
			}
		}
		if (flg){
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else{
			responseResult.initResult(GTAError.SysError.ASSIGN_ROLE_FAIL);
		}
		return responseResult;
	}

	@Override
	@CacheEvict(value="menuCache",allEntries=true)
	@Transactional
	public ResponseResult removeRole(AssignRoleDto dto) {
		if (StringUtils.isEmpty(dto.getUserId())) {
			responseResult.initResult(GTAError.SysError.USER_ID_EMPTY);
			return responseResult;
		}
		if (StringUtils.isEmpty(dto.getRoleIds())) {
			responseResult.initResult(GTAError.SysError.ROLE_IDS_EMPTY);
			return responseResult;
		}
		String[] rIds = dto.getRoleIds().split(",");
		boolean flg = true;
		for (int i = 0; i < rIds.length; i++) {
			UserRole userRole = new UserRole();
			UserRolePK id =  new UserRolePK();
			id.setRoleId(rIds[i]);
			id.setUserId(dto.getUserId());
			userRole.setId(id);
			try {
				userRoleDao.delete(userRole);
			} catch (HibernateException e) {
				logger.error(UserRoleServiceImpl.class.getName() + " removeRole Failed!", e);
				flg = false;
			}
		}
		if (flg){
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else{
			responseResult.initResult(GTAError.SysError.REMOVE_ROLE_FAIL);
		}
		return responseResult;
	}
	
	
	@Transactional
	public List<MenuDto> getSubmenu(MenuDto menu){
		List<MenuDto> subMenuList = new ArrayList<MenuDto>();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(menu.getIndex());
		List<ProgramMaster> subLs= programMasterDao.getByHql("from ProgramMaster where status='ACT' and parentId=? order by sortSeq asc",param);
		if(subLs!=null&&subLs.size()>0){
			for(ProgramMaster pm:subLs){
				MenuDto m = new MenuDto();
				m.setIcon(pm.getIcon()==null?"":pm.getIcon());
				m.setIndex(pm.getProgramId()==null?"":pm.getProgramId());
				m.setLink(pm.getUrlPath()==null?"":pm.getUrlPath());
				m.setName(pm.getMenuName()==null?"":pm.getMenuName());
				m.setType(pm.getProgramType()==null?"":pm.getProgramType());
				m.setSubmenu(getSubmenu(m));
				subMenuList.add(m);
			}
		}
		return subMenuList;
	}
	
	@Transactional
	public List<MenuDto> getSubmenu(MenuDto menu, List<ProgramDto> rpList){
		List<MenuDto> subMenuList = new ArrayList<MenuDto>();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(menu.getIndex());
		List<ProgramMaster> subLs= programMasterDao.getByHql("from ProgramMaster where status='ACT' and parentId=? order by sortSeq asc",param);
		if(subLs!=null&&subLs.size()>0){
			for(ProgramMaster pm:subLs){
				ProgramDto tmpProgram = new ProgramDto();
				tmpProgram.setProgramId(pm.getProgramId());
				if (!rpList.contains(tmpProgram)) continue;
				
				MenuDto m = new MenuDto();
				m.setAccessRight(rpList.get(rpList.indexOf(tmpProgram)).getAccessRight());
				m.setIcon(pm.getIcon()==null?"":pm.getIcon());
				m.setIndex(pm.getProgramId()==null?"":pm.getProgramId());
				m.setLink(pm.getUrlPath()==null?"":pm.getUrlPath());
				m.setName(pm.getMenuName()==null?"":pm.getMenuName());
				m.setType(pm.getProgramType()==null?"":pm.getProgramType());
				m.setSubmenu(getSubmenu(m, rpList));
				subMenuList.add(m);
			}
		}
		return subMenuList;
	}
	
	@Transactional
    public List<MenuDto> checkPermissions(String userId,List<MenuDto> subMenuList){
		int size = subMenuList.size()-1;
		for(int i=size;i>=0;i--){
			MenuDto menu = subMenuList.get(i);
			Object[] obj = userRoleDao.getUserCheckPermissions(userId,menu.getIndex());
			if(obj==null){
				if(menu.getSubmenu().size()>0){
					List<MenuDto> afterCheckSubMenu = checkPermissions(userId,menu.getSubmenu());
					if(afterCheckSubMenu.size()>0){
						menu.setSubmenu(afterCheckSubMenu);
					}else{
						subMenuList.remove(menu);
					}
				}else{
					subMenuList.remove(menu);
				}
			}else{
				menu.setAccessRight(obj[1]!=null?obj[1].toString():null);
			}
		} 
    	return subMenuList;
    }
	

	@Override
	@Transactional
	public ResponseResult getUserPermissions(String userId) {
		List<MenuDto> menuList = new ArrayList<MenuDto>();
		try {
			List<ProgramMaster> pmList1 = programMasterDao.getByHql("from ProgramMaster where status='ACT' and (parentId is null or parentId='') order by sortSeq asc");
			for(ProgramMaster pm:pmList1){
				MenuDto menu = new MenuDto();
				menu.setIcon(pm.getIcon()==null?"":pm.getIcon());
				menu.setIndex(pm.getProgramId()==null?"":pm.getProgramId());
				menu.setLink(pm.getUrlPath()==null?"":pm.getUrlPath());
				menu.setName(pm.getMenuName()==null?"":pm.getMenuName());
				menu.setType(pm.getProgramType()==null?"":pm.getProgramType());
				menu.setSubmenu(getSubmenu(menu));
				menuList.add(menu);
			}
			int size = menuList.size()-1;
			for(int i=size;i>=0;i--){
				MenuDto menu = menuList.get(i);
				Object[] obj = userRoleDao.getUserCheckPermissions(userId,menu.getIndex());
				if(obj==null){
					if(menu.getSubmenu().size()>0){
						List<MenuDto> afterCheckSubMenu = checkPermissions(userId,menu.getSubmenu());
						if(afterCheckSubMenu.size()>0){
							menu.setSubmenu(afterCheckSubMenu);
						}else{
							menuList.remove(i);
						}
					}else{
						menuList.remove(i);
					}
				}else{
					menu.setAccessRight(obj[1]!=null?obj[1].toString():null);
				}
			}
			responseResult.initResult(GTAError.Success.SUCCESS,menuList);
		} catch (Exception e) {
			logger.error(UserRoleServiceImpl.class.getName() + " getUserPermissions Failed!", e);
			responseResult.initResult(GTAError.SysError.REMOVE_ROLE_FAIL);
		}	
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult getUserPermissionsV2(String userId) {
		List<MenuDto> menuList = new ArrayList<MenuDto>();
		List<UserRoleDto> userRoles = userRoleDao.getUserRoleListByUserId(userId);
		List<ProgramDto> rpList = new ArrayList<ProgramDto>();
		for(UserRoleDto role : userRoles)
		{
			List<ProgramDto> tmpRPList = roleProgramDao.getRoleProgramsById(role.getRoleId());
			rpList.addAll(tmpRPList);
		}
		
		try {
			List<ProgramMaster> pmList1 = programMasterDao.getByHql("from ProgramMaster where status='ACT' and (parentId is null or parentId='') order by sortSeq asc");
			for(ProgramMaster pm:pmList1){
				
				ProgramDto tmpProgram = new ProgramDto();
				tmpProgram.setProgramId(pm.getProgramId());
				if (!rpList.contains(tmpProgram)) continue;
				
				MenuDto menu = new MenuDto();
				menu.setAccessRight(rpList.get(rpList.indexOf(tmpProgram)).getAccessRight());
				menu.setIcon(pm.getIcon()==null?"":pm.getIcon());
				menu.setIndex(pm.getProgramId()==null?"":pm.getProgramId());
				menu.setLink(pm.getUrlPath()==null?"":pm.getUrlPath());
				menu.setName(pm.getMenuName()==null?"":pm.getMenuName());
				menu.setType(pm.getProgramType()==null?"":pm.getProgramType());
				menu.setSubmenu(getSubmenu(menu, rpList));
				menuList.add(menu);
			}
			
			responseResult.initResult(GTAError.Success.SUCCESS,menuList);
		} catch (Exception e) {
			logger.error(UserRoleServiceImpl.class.getName() + " getUserPermissions Failed!", e);
			responseResult.initResult(GTAError.SysError.REMOVE_ROLE_FAIL);
		}	
		return responseResult;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public ResponseResult loadUserPermissionsV2(String userId, Map<String, List<MenuDto>> roleMenu) {
		List<MenuDto> menuList = new ArrayList<MenuDto>();
		List<UserRoleDto> userRoles = null;
		userRoles = userRoleDao.getUserRoleListByUserId(userId);
		//userRoleMap.getInstance().setUserRole(userId, userRoles);
		for (UserRoleDto role : userRoles) {
		 //Cancel the singleton instance that cannot support cluster server 
		 //List<MenuDto> menusCache = userRoleMap.getInstance().getRoleMenu().get(role.getRoleId().toString());
		 
		 List<MenuDto> menus = roleMenu.get(role.getRoleId().toString());
		 //List<MenuDto> menus = cloneMenus(menusCache);
			for (MenuDto tmpMenu : menus) {
				if (menuList.contains(tmpMenu)) {
					MenuDto originMenu = menuList.get(menuList.indexOf(tmpMenu));
					menuList.remove(tmpMenu);
					menuList.add(combineMenu(originMenu, tmpMenu));
				} else {
					menuList.add(tmpMenu);
				}
			}
		}
		Collections.sort(menuList);
		responseResult.initResult(GTAError.Success.SUCCESS, menuList);
		return responseResult;
	}
	private List<MenuDto>cloneMenus(List<MenuDto>source)
	{
		List<MenuDto>dests=new ArrayList<>();
		for(MenuDto dto:source){
			try {
				dests.add((MenuDto)BeanUtils.cloneBean(dto));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dests;
	}
	private List<MenuDto> getRoleMenu(Long roleId){
		List<ProgramDto> tmpRPList = roleProgramDao.getRoleProgramsById(roleId);
		return this.initMenu(tmpRPList);
	}
	private List<MenuDto> initMenu(List<ProgramDto> rpList) {

		List<MenuDto> menuList = new ArrayList<MenuDto>();

		List<ProgramMaster> pmList1 = programMasterDao.getByHql(
				"from ProgramMaster where status='ACT' and (parentId is null or parentId='') order by sortSeq asc");
		for (ProgramMaster pm : pmList1) {
			if (pm.getProgramType() == "H")
				continue;

			ProgramDto tmpProgram = new ProgramDto();
			tmpProgram.setProgramId(pm.getProgramId());
			
			if (!rpList.contains(tmpProgram))
				continue;

			MenuDto menu = new MenuDto();
			menu.setSeq(pm.getSortSeq());
			menu.setAccessRight(rpList.get(rpList.indexOf(tmpProgram)).getAccessRight());
			menu.setIcon(pm.getIcon() == null ? "" : pm.getIcon());
			menu.setIndex(pm.getProgramId() == null ? "" : pm.getProgramId());
			menu.setLink(pm.getUrlPath() == null ? "" : pm.getUrlPath());
			menu.setName(pm.getMenuName() == null ? "" : pm.getMenuName());
			menu.setType(pm.getProgramType() == null ? "" : pm.getProgramType());
			menu.setSubmenu(getSubmenu(menu, rpList));
			menuList.add(menu);
		}
		Collections.sort(menuList);
		return menuList;
	}

	@SuppressWarnings("unchecked")
	private MenuDto combineMenu(MenuDto originMenu, MenuDto tmpMenu)
	{
		List<MenuDto> subMenu = new ArrayList<MenuDto>();
		if (originMenu.getSubmenu()==null||originMenu.getSubmenu().size()==0)
		{
			if (originMenu.getAccessRight().equals(tmpMenu.getAccessRight()))
			{
				return originMenu;
			}
			else
			{
				return originMenu.getAccessRight().equals("*") ? originMenu : tmpMenu;
			}
		}
		else{
			if (!originMenu.getAccessRight().equals(tmpMenu.getAccessRight()))
			{
				if(!originMenu.getAccessRight().equals("*")){
					try {
						MenuDto cloneMenu=(MenuDto)BeanUtils.cloneBean(originMenu);
						cloneMenu.setAccessRight(tmpMenu.getAccessRight());
						originMenu=cloneMenu;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		subMenu.addAll(originMenu.getSubmenu());
		for (MenuDto menu : tmpMenu.getSubmenu())
		{
			if (subMenu.contains(menu))
			{
				MenuDto originSub = subMenu.get(subMenu.indexOf(menu));//tmpMenu.getSubmenu().get(tmpMenu.getSubmenu().indexOf(menu));
				subMenu.remove(menu);
				subMenu.add(combineMenu(originSub, menu));
			}
			else
			{
				subMenu.add(menu);
			}
		}
		if(null!=subMenu){
			Collections.sort(subMenu);
		}
		
		originMenu.setSubmenu(subMenu);
		return originMenu;
		
	}

	@Override
	@Transactional
	public ResponseResult checkPermissionsByUserId(String userId, String programId) {
		boolean accBlg=Boolean.FALSE;
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		List<UserRoleDto> roles=userRoleDao.getDtoBySql("SELECT s.user_id as userId,s.role_id as roleId,m.role_name as roleName from user_role s INNER JOIN role_master m on(m.role_id=s.role_id) where s.user_id=? ORDER BY s.role_id", param, UserRoleDto.class);
		if(null!=roles)
		{
			for (UserRoleDto dto : roles) {
				if(checkPermissions(dto.getRoleId(), programId)){
					accBlg=Boolean.TRUE;
					break;
				}
			}
		}
		if(accBlg){
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else{
			responseResult.initResult(GTAError.LeadError.NOTE_PERMISSION_ERROR,"No permission for this user ");
		}
		return responseResult;
	}
	private boolean checkPermissions(Long roleId, String programId){
		String hql="SELECT p.role_id AS roleId ,p.program_id AS programIds, p.access_right AS accessRight FROM role_program  p WHERE p.program_id=? AND p.role_id=?";
		List<Serializable>param=new ArrayList<>();
		param.add(programId);
		param.add(roleId);
		List<RoleProgramDto>list=roleProgramDao.getDtoBySql(hql, param, RoleProgramDto.class);
		if(null!=list&&list.size()>0)
		{
			return true;
		}else{
		 return false;	
		}
	}

	/**
	 * clear particular user menu cache of ehcache
	 * @param userId
	 */
	@CacheEvict(value = "menuCache", key="#userId")
	public void clearMenuCache(String userId) {
       logger.info("clear memnuCache user:" + userId);		
	}

	/**
	 * clear all menuCache of ehcache 
	 */
	@CacheEvict(value = "menuCache", allEntries = true)
	public void clearMenuCache() {
	   logger.info("clear all memnuCache");
	}		
}