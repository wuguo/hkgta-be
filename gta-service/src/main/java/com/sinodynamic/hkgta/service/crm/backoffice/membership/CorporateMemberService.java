package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import com.sinodynamic.hkgta.dto.membership.SearchSpendingSummaryDto;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CorporateMemberService extends IServiceBase<CorporateMember> {

	ResponseResult getSpendingSummary(ListPage<CorporateMember> page,
			SearchSpendingSummaryDto dto);
	
	public CorporateMember getCorporateMemberById(Long customerId);

	ResponseResult sendSpendingSummaryReportEmail(ListPage page, SearchSpendingSummaryDto dto);

	byte[] downloadSpendingSummaryReport(ListPage page, SearchSpendingSummaryDto dto,String reportPath);
}
