package com.sinodynamic.hkgta.service.crm.sales;

import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface CustomerEmailAttachService extends IServiceBase<CustomerEmailAttach> {

	public void savecustomerEmailAttach(CustomerEmailAttach attach);

}
