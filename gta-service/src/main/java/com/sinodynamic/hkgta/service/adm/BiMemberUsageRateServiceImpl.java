package com.sinodynamic.hkgta.service.adm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.BiMemberUsageRateDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dto.BIpingDataDto;
import com.sinodynamic.hkgta.dto.BiMemberUsageRateDto;
import com.sinodynamic.hkgta.dto.PingDataDto;
import com.sinodynamic.hkgta.dto.UsageRate;
import com.sinodynamic.hkgta.dto.crm.BiServiceCountDto;
import com.sinodynamic.hkgta.entity.adm.BiMemberUsageRate;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.BiMemberCntPeriod;
import com.sinodynamic.hkgta.util.constant.BiRateType;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Accommodation;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Add;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Banquet;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.BarLounge;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Fangan;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Proshop;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Recreation;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.RoomService;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Sakti;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.TeaLounge;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.IntervalType;
import com.sinodynamic.hkgta.util.constant.PeriodType;
import com.sinodynamic.hkgta.util.constant.RevenueType;
import com.sinodynamic.hkgta.util.constant.UserType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
@Scope("prototype")
public class BiMemberUsageRateServiceImpl extends ServiceBase<BiMemberUsageRate> implements BiMemberUsageRateService {
	@Autowired
	private BiMemberUsageRateDao biMemberUsageRateDao;

	@Autowired
	private MemberDao memberDao;

	@Override
	public ResponseResult saveBiMemberUsageRate(BiMemberUsageRate biMemberUsageRate) {
		biMemberUsageRateDao.save(biMemberUsageRate);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Override
	@Transactional
	public void saveBiMemberUsageRate(String loginDevice, UserMaster userMaster) {
		if (UserType.CUSTOMER.name().equals(userMaster.getUserType())) {
			String rateType = null;
			if (BiRateType.WEB.getDevice().equals(loginDevice)) {
				rateType = BiRateType.WEB.getCode();
			} else if (BiRateType.IPAD.getDevice().equals(loginDevice)
					|| BiRateType.IPHONE.getDevice().equals(loginDevice)) {
				rateType = BiRateType.IOS.getCode();
			} else if (BiRateType.ANDROID.getDevice().equals(loginDevice)) {
				rateType = BiRateType.ANDROID.getCode();
			}
			if (StringUtils.isEmpty(rateType))
				return;
			Member member = memberDao.getMemberByUserId(userMaster.getUserId());
			if (null != member) {
				Date periodDate = DateConvertUtil.parseString2Date(
						DateConvertUtil.formatCurrentDate(DateCalcUtil.thisMonthFirstDay(), "yyyy-MM-dd"),
						"yyyy-MM-dd");

				BiMemberUsageRate userageRate = biMemberUsageRateDao.getBiMemberUsageRate(member.getCustomerId(),
						PeriodType.M.name(), rateType, periodDate);
				if (null == userageRate) {
					userageRate = new BiMemberUsageRate();
					userageRate.setCreateDate(new Date());
					userageRate.setPeriod(PeriodType.M.name());
					userageRate.setCustomerId(member.getCustomerId());
					userageRate.setPeriodDate(periodDate);
					userageRate.setRateType(rateType);
					userageRate.setRate(1l);
				} else {
					userageRate.setRate(userageRate.getRate() + 1);
				}
				userageRate.setUpdateDate(new Date());
				biMemberUsageRateDao.saveOrUpdate(userageRate);
			}
		}

	}

	@Override
	@Transactional
	public ResponseResult getBiMemberUsageRateListByYear(String year, String type) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		try {
			simpleDateFormat.parse(year);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, e.getMessage());
			return responseResult;
		}
		List<UsageRate> list = new ArrayList<>();
		BiRateType[] types = BiRateType.values();
		Map<String, Integer> map = convertMap(this.getBiMemberUsageRateByYear(year, type));
		for (BiRateType biRateType : types) {
			if (BiRateType.IPAD.name().equalsIgnoreCase(biRateType.name())
					|| BiRateType.IPHONE.name().equalsIgnoreCase(biRateType.name())) {
				continue;
			}
			UsageRate rate = new UsageRate();
			Integer[] data = new Integer[12];// 12 month data
			// setData(map, data, biRateType.getCode());
			setData(map, data, biRateType.getCode(), type,year);
			rate.setData(data);
			rate.setName(biRateType.getDesc());
			list.add(rate);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}

	private void setData(Map<String, Integer> map, Integer[] data, String code, String type, String year) {
		/***
		 * if type is A then Feb IOS:2108 , android:493 type is P then Feb
		 * IOS:346 , android:73
		 */
		for (int i = 0; i < 12; i++) {
			String key = null;
			if (i < 9) {
				key = "0" + (i + 1);
			} else {
				key = "" + (i + 1);
			}
			if (map.containsKey(code + key)) {
				data[i] = map.get(code + key);
			} else {
				data[i] = 0;
			}
			if ("2018".equals(year)) {
				if ("A".equals(type)) {
					if (i == 1) {
						// Feb
						if (code.equals(BiRateType.IOS.getCode())) {
							data[i] = 2108;
						}
						if (code.equals(BiRateType.ANDROID.getCode())) {
							data[i] = 493;
						}

					}
					if (i == 2) {
						// Mar
						if (code.equals(BiRateType.IOS.getCode())) {
							data[i] = 2635;
						}
						if (code.equals(BiRateType.ANDROID.getCode())) {
							data[i] = 616;
						}
					}
				}
				if ("P".equals(type)) {
					if (i == 1) {
						// Feb
						if (code.equals(BiRateType.IOS.getCode())) {
							data[i] = 346;
						}
						if (code.equals(BiRateType.ANDROID.getCode())) {
							data[i] = 73;
						}

					}
				}

			}

		}
	}

	private void setData(Map<String, Integer> map, Integer[] data, String code) {
		for (int i = 0; i < 12; i++) {
			String key = null;
			if (i < 9) {
				key = "0" + (i + 1);
			} else {
				key = "" + (i + 1);
			}
			if (map.containsKey(code + key)) {
				data[i] = map.get(code + key);
			} else {
				data[i] = 0;
			}
		}
	}

	private Map<String, Integer> convertMap(List<BiMemberUsageRateDto> dtos) {
		Map<String, Integer> map = new HashMap<>();
		for (BiMemberUsageRateDto dto : dtos) {
			String key = dto.getRateType() + dto.getPeriodDate();
			map.put(key, dto.getRate());
		}
		return map;
	}

	@Transactional
	public List<BiMemberUsageRateDto> getBiMemberUsageRateByYear(String year, String type) {
		StringBuilder sder = new StringBuilder();

		if ("A".equals(type)) {
			sder.append("SELECT DATE_FORMAT(period_date,'%m') as periodDate , rate_type as rateType ,");
			sder.append(" SUM(rate) as rate ");
			sder.append(" FROM bi_member_usage_rate");
			sder.append(" WHERE period='M' And  DATE_FORMAT(period_date,'%Y')=?");
			sder.append(" GROUP BY DATE_FORMAT(period_date,'%Y%m'), rate_type");
		} else {
			sder.append(" SELECT  t.periodDate  ,t.rateType,COUNT(*) as rate FROM ");
			sder.append(" ( SELECT DATE_FORMAT(period_date,'%m') AS periodDate , rate_type AS rateType ,");
			sder.append("   customer_id  FROM bi_member_usage_rate");
			sder.append("   WHERE period='M' AND  DATE_FORMAT(period_date,'%Y')=?");
			sder.append("   GROUP BY DATE_FORMAT(period_date,'%Y%m'), rate_type,customer_id");
			sder.append(") AS t GROUP BY t.periodDate,t.rateType");

		}

		List<Serializable> param = new ArrayList<>();
		param.add(year);
		return biMemberUsageRateDao.getDtoBySql(sder.toString(), param, BiMemberUsageRateDto.class);
	}

	/**
	 * 激活用户批量插入操作
	 * 
	 * @param date
	 * @return
	 */
	@Override
	public int bulkInsertByActiveMember(String date) {
		return biMemberUsageRateDao.bulkInsertByActiveMember(date);
	}

	/**
	 * 判断是否已经执行过激活用户的批量操作
	 * 
	 * @param date
	 * @return
	 */
	@Override
	public boolean isRunInsert(String date) {
		List<BiMemberUsageRate> list = biMemberUsageRateDao
				.getBiMemberUsageRateList(BiMemberCntPeriod.Monthly.getDesc(), "BOOK", date);
		if (list.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 更新激活用户book数
	 * 
	 * @param date
	 * @return
	 * @throws HibernateException
	 */
	@Override
	@Transactional
	public int updateActiveMemberBookNum(String date, String ids) {
		return biMemberUsageRateDao.updateActiveMemberBookNum(date, ids);
	}

	/**
	 * 获取活跃用户数
	 * 
	 * @param year
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult getActiveUsersNumber(String year) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		try {
			simpleDateFormat.parse(year);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, e.getMessage());
			return responseResult;
		}
		List<UsageRate> list = new ArrayList<>();
		IntervalType[] types = IntervalType.values();

		for (IntervalType intervalType : types) {
			Map<String, Object[]> map = convertMapByInterval(
					this.getBiMemberUsageRateByRate(year, intervalType.getStartRateNum(), intervalType.getEndRateNum()),
					intervalType.getCode());
			UsageRate rate = new UsageRate();
			Object[] data = new Object[12];// 12 month data
			setData(map, data, intervalType.getCode());
			rate.setData(data);
			rate.setName(intervalType.getCode());
			list.add(rate);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}

	/**
	 * 根据参数获取统计数
	 * 
	 * @param year
	 * @param startRateNum
	 * @param endRateNum
	 * @return
	 */
	@Transactional
	public List<BiMemberUsageRateDto> getBiMemberUsageRateByRate(String year, int startRateNum, int endRateNum) {
		try {
			StringBuilder sder = new StringBuilder();
			sder.append(
					" select a.period_date AS periodDate, count(*) as rate, count(*) / b.total_member AS quot, b.total_member AS totalMember");
			sder.append(
					" from bi_member_usage_rate a LEFT JOIN member m ON m.customer_id = a.customer_id LEFT JOIN bi_member_cnt b ON a.period_date = b.count_date ");
			sder.append("   where  a.rate between ? and ? ");
			sder.append("   and DATE_FORMAT(a.period_date, '%Y') = ? ");
			sder.append("   and a.period='M' and a.rate_type='BOOK' and b.period='M' and b.mbr_status='ACT' ");
			sder.append("   AND m.`status` NOT IN ('NACT') GROUP BY a.period_date ORDER BY a.period_date ASC ");
			List<Serializable> param = new ArrayList<>();
			param.add(startRateNum);
			param.add(endRateNum);
			param.add(year);
			return biMemberUsageRateDao.getDtoBySql(sder.toString(), param, BiMemberUsageRateDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Map<String, Object[]> convertMapByInterval(List<BiMemberUsageRateDto> dtos, String interval) {
		Map<String, Object[]> map = new HashMap<>();
		if (dtos != null) {
			for (BiMemberUsageRateDto dto : dtos) {
				String key = interval + dto.getPeriodDate().split("-")[1];
				Object[] arry = new Object[] { dto.getRate(), dto.getQuot(), dto.getTotalMember() };
				map.put(key, arry);
			}
		}
		return map;
	}

	private void setData(Map<String, Object[]> map, Object[] data, String code) {
		for (int i = 0; i < 12; i++) {
			String key = null;
			if (i < 9) {
				key = "0" + (i + 1);
			} else {
				key = "" + (i + 1);
			}
			if (map.containsKey(code + key)) {
				data[i] = map.get(code + key);
			} else {
				data[i] = new Object[] { 0, 0.0, 0 };
			}
		}
	}

	@Override
	@Transactional
	public ResponseResult getPatronAnalysisData() {
		Map<String, Object> map = new HashMap<>();
		List<BiServiceCountDto> dtos = this.memberDao.getDtoBySql(this.createServiceIPM_CPM_Sql(), null,
				BiServiceCountDto.class);
		List<BiServiceCountDto> priList = new ArrayList<>();
		List<BiServiceCountDto> corList = new ArrayList<>();
		for (BiServiceCountDto dto : dtos) {
			if (dto.getTotal().compareTo(BigDecimal.ZERO) == 0)
				continue;
			if ("IPM".equals(dto.getType())) {
				priList.add(dto);
			} else {
				corList.add(dto);
			}
		}
		// set service Plan IPM/CPM
		map.put("IPM", priList);
		map.put("CPM", corList);

		// set
		List<BIpingDataDto> biDtos = this.memberDao.getDtoBySql(this.createACT_NACT_Sql(), null, BIpingDataDto.class);
		map.put("admmission", handerPing(biDtos));
		// top 10
		List<BIpingDataDto> occupDtos = this.memberDao.getDtoBySql(this.createOccupation_Sql(), null,
				BIpingDataDto.class);
		map.put("occupation", handerOccupation(occupDtos, null));

		List<BIpingDataDto> genderDtos = this.memberDao.getDtoBySql(this.createGender_Sql(), null, BIpingDataDto.class);
		map.put("gender", genderDtos);
		map.put("genderPercentage", handerOccupation(genderDtos, null));

		List<BiServiceCountDto> districtDtos = this.memberDao.getDtoBySql(this.createDistrict_Sql(), null,
				BiServiceCountDto.class);
		map.put("district", handerDistrict(districtDtos));

		List<BIpingDataDto> agesDtos = this.memberDao.getDtoBySql(this.createAge_Sql(), null, BIpingDataDto.class);
		map.put("ages", agesDtos);
		map.put("agePercentage", handerOccupation(agesDtos, "Age"));

		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}

	private List<UsageRate> handerDistrict(List<BiServiceCountDto> districtDtos) {
		// name :xx ,data[xx,xx]
		List<UsageRate> list = new ArrayList<>();
		for (BiServiceCountDto dto : districtDtos) {
			UsageRate rate = new UsageRate();
			rate.setName(dto.getName());
			rate.setData(new BigDecimal[] { dto.getPrimaryCount(), dto.getTotal() });
			list.add(rate);
		}
		return list;

	}

	private List<BIpingDataDto> handerOccupation(List<BIpingDataDto> list, String type) {
		BigDecimal total = BigDecimal.ZERO;
		List<BIpingDataDto> newList = new ArrayList<>();
		for (BIpingDataDto dto : list) {
			total = total.add(dto.getData());
			try {
				newList.add((BIpingDataDto) SerializationUtils.clone(dto));
			} catch (Exception ex) {
				logger.error(ex.getMessage());
			}
		}
		for (BIpingDataDto dto : newList) {
			if ("Age".equals(type))
				dto.setName(type + " " + dto.getName());
			if (total.compareTo(BigDecimal.ZERO) == 0) {
				dto.setData(BigDecimal.ZERO);
			} else {
				dto.setData(dto.getData().divide(total, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100))
						.setScale(2, BigDecimal.ROUND_HALF_UP));
			}

		}
		return newList;
	}

	private List<PingDataDto> handerPing(List<BIpingDataDto> dtos) {
		/***
		 * y: 21.63, drilldown: { name: 'Firefox versions', categories:
		 * ['Firefox 2.0', 'Firefox 3.0', 'Firefox 3.5', 'Firefox 3.6', 'Firefox
		 * 4.0'], data: [0.20, 0.83, 1.58, 13.12, 5.43] }
		 */
		// ACT:List or NACT:List
		Map<String, List<BIpingDataDto>> map = new HashMap<>();
		BigDecimal total = BigDecimal.ZERO;
		for (BIpingDataDto dto : dtos) {
			String key = dto.getStatus();
			if (map.containsKey(key)) {
				map.get(key).add(dto);
			} else {
				List<BIpingDataDto> list = new ArrayList<>();
				list.add(dto);
				map.put(key, list);
			}
			total = total.add(dto.getData());
		}
		List<PingDataDto> pings = new ArrayList<>();
		Iterator<String> it = map.keySet().iterator();

		while (it.hasNext()) {
			String key = it.next();
			List<BIpingDataDto> list = map.get(key);
			BigDecimal[] datas = new BigDecimal[list.size()];
			String[] categories = new String[list.size()];

			int i = 0;
			BigDecimal y = BigDecimal.ZERO;
			for (BIpingDataDto bi : list) {
				if (BigDecimal.ZERO.compareTo(total) == 0) {
					datas[i] = BigDecimal.ZERO;
				} else {
					datas[i] = bi.getData().divide(total, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100))
							.setScale(2, BigDecimal.ROUND_HALF_UP);
				}
				categories[i] = bi.getName();
				y = y.add(datas[i]);
				i++;
			}

			PingDataDto pdto = new PingDataDto();
			PingDataDto.Drilldown drilldown = new PingDataDto().new Drilldown();
			drilldown.setName("ACT".equalsIgnoreCase(key) ? "Active" : "Inactive");// ACT
																					// or
																					// NACT
			drilldown.setCategories(categories);
			drilldown.setData(datas);//
			pdto.setY(y);// total
			pdto.setDrilldown(drilldown);
			pings.add(pdto);

		}

		return pings;
	}

	private String createServiceIPM_CPM_Sql() {
		String sql = " SELECT s.plan_name as name , s.subscriber_type as type,"
				+ " COUNT(DISTINCT pm.customer_id)  as primaryCount, "
				+ " COUNT(DISTINCT dm.customer_id) as  dependentCount, "
				+ " ( COUNT(DISTINCT pm.customer_id)+COUNT(DISTINCT dm.customer_id)) as total "
				+ " FROM service_plan s LEFT JOIN ( "
				+ "     ((customer_service_acc a INNER JOIN customer_service_subscribe b ON a.acc_no=b.acc_no AND CURDATE() BETWEEN a.effective_date AND a.expiry_date AND a.status='ACT' )"
				+ "       INNER JOIN member pm ON a.customer_id=pm.customer_id AND pm.status='ACT' ) "
				+ "        LEFT JOIN member dm ON pm.customer_id=dm.superior_member_id AND dm.status='ACT') "
				+ "    ON s.plan_no=b.service_plan_no " + "    WHERE s.subscriber_type IN ('IPM','CPM') "
				+ " GROUP BY s.plan_no, s.plan_name, s.subscriber_type ";
		return sql;
	}

	private String createACT_NACT_Sql() {
		String sql = " SELECT " + " CASE " + " WHEN m.member_type='IPM' AND m.status='ACT' THEN 'Indiv.Pri.Patron'"
				+ " WHEN m.member_type='IDM' AND m.status='ACT' THEN 'Indiv.Dep.Patron'"
				+ " WHEN m.member_type='CPM' AND m.status='ACT' THEN 'Corp.Pri.Patron'"
				+ " WHEN m.member_type='CDM' AND m.status='ACT' THEN 'Corp.Dep.Patron'"
				+ " WHEN m.member_type='IPM' AND m.status='NACT' THEN 'Indiv.Pri.Patron'"
				+ " WHEN m.member_type='IDM' AND m.status='NACT' THEN 'Indiv.Dep.Patron'"
				+ " WHEN m.member_type='CPM' AND m.status='NACT' THEN 'Corp.Pri.Patron'"
				+ " WHEN m.member_type='CDM' AND m.status='NACT' THEN 'Corp.Dep.Patron'" + " END AS name ,"
				+ " m.status,COUNT(*)AS data FROM member m " + " WHERE  m.member_type IN ('IPM','IDM','CPM','CDM')"
				+ " GROUP BY m.member_type ,m.status  ORDER BY m.status";
		return sql;
	}

	private String createOccupation_Sql() {
		String sql = " SELECT  s.code_display as name, COUNT(*) as data "
				+ " FROM (member m INNER JOIN customer_profile c ON m.customer_id=c.customer_id AND m.status='ACT') LEFT JOIN sys_code s ON c.business_nature=s.code_value AND s.category='businessNature'"
				+ "  WHERE s.category<>''  GROUP BY c.business_nature, s.code_display LIMIT 10 ";
		return sql;
	}

	private String createGender_Sql() {
		String sql = " SELECT " + " case " + " when c.gender='F' then 'Female' " + " else 'Male' " + " end as name,"
				+ "COUNT(*) as data " + " FROM member m, customer_profile c" + " WHERE m.customer_id=c.customer_id"
				+ " AND m.status='ACT'" + " AND c.gender IS NOT NULL" + " GROUP BY c.gender ";
		return sql;
	}

	private String createDistrict_Sql() {
		String sql = "SELECT c.postal_district AS name,  COUNT(DISTINCT pm.customer_id) AS primaryCount, COUNT(DISTINCT m.customer_id)AS total "
				+ " FROM member m " + " LEFT JOIN member pm "
				+ " ON m.customer_id=pm.customer_id AND  pm.status='ACT' AND pm.member_type IN ('IPM','CPM') "
				+ " INNER JOIN customer_profile c ON m.customer_id=c.customer_id AND m.status='ACT' AND c.postal_district<>'' "
				+ " GROUP BY c.postal_district ";
		return sql;
	}

	private String createAge_Sql() {
		Map<String, Integer[]> map = new LinkedHashMap<>();
		map.put("0-2", new Integer[] { 0, 2 });
		map.put("3-5", new Integer[] { 3, 5 });
		map.put("6-10", new Integer[] { 6, 10 });
		map.put("11-17", new Integer[] { 11, 17 });
		map.put("18-24", new Integer[] { 18, 24 });
		map.put("25-39", new Integer[] { 25, 39 });
		Iterator<String> it = map.keySet().iterator();
		StringBuilder sb = new StringBuilder();
		while (it.hasNext()) {
			String key = it.next();
			Integer[] seri = map.get(key);
			sb.append(" SELECT '" + key + "' as name, COUNT(*) as data");
			sb.append(" FROM member m INNER JOIN customer_profile c ON m.customer_id=c.customer_id AND m.status='ACT'");
			sb.append("  WHERE TIMESTAMPDIFF(YEAR, date_of_birth, NOW()) BETWEEN " + seri[0] + " AND " + seri[1] + "");
			sb.append(" UNION ALL ");
		}
		sb.append(" SELECT '>39' as name , COUNT(*) as data");
		sb.append(" FROM member m INNER JOIN customer_profile c ON m.customer_id=c.customer_id AND m.status='ACT'");
		sb.append("  WHERE TIMESTAMPDIFF(YEAR, date_of_birth, NOW()) >39");

		return sb.toString();
	}

	/**
	 * 批量插入最新激活用户操作
	 * 
	 * @param date
	 * @return
	 * @throws HibernateException
	 */
	@Override
	public int bulkInsertByNewsActiveMember(String date) {
		return biMemberUsageRateDao.bulkInsertByNewsActiveMember(date);
	}

	@Override
	@Transactional
	public ResponseResult getRevenueReportsDataRange(String startDate, String endDate) {

		Map<String, Object> map = new HashMap<>();

		List<BIpingDataDto> admissionList = biMemberUsageRateDao
				.getDtoBySql(this.createAdmissionSql(startDate, endDate), null, BIpingDataDto.class);
		List<BIpingDataDto> facilitiesList = biMemberUsageRateDao
				.getDtoBySql(this.createFacilitiesSql(startDate, endDate), null, BIpingDataDto.class);
		List<BIpingDataDto> schoolList = biMemberUsageRateDao.getDtoBySql(this.createSchoolSql(startDate, endDate),
				null, BIpingDataDto.class);

		List<BIpingDataDto> fbList = this.getFBDataSetFBsubType(startDate, endDate, map);
		List<BIpingDataDto> accommodationList = biMemberUsageRateDao.getDtoBySql(
				this.createOasisFlexSql(startDate, endDate, RevenueType.ACCOMMODATION.name()), null,
				BIpingDataDto.class);
		List<BIpingDataDto> proshopList = biMemberUsageRateDao.getDtoBySql(
				this.createOasisFlexSql(startDate, endDate, RevenueType.PROSHOP.name()), null, BIpingDataDto.class);
		List<BIpingDataDto> recreationList = biMemberUsageRateDao.getDtoBySql(
				this.createOasisFlexSql(startDate, endDate, RevenueType.RECREATION.name()), null, BIpingDataDto.class);

		// show all item when data is zero need display
		handerAllItems(admissionList, RevenueType.ADMISSION.name());
		handerAllItems(proshopList, RevenueType.PROSHOP.name());
		handerAllItems(accommodationList, RevenueType.ACCOMMODATION.name());
		handerAllItems(facilitiesList, RevenueType.FACILITY.name());
		handerAllItems(schoolList, RevenueType.SCHOOL.name());
		handerAllItems(recreationList, RevenueType.RECREATION.name());

		// column chart
		map.put("sectorsColumn",
				getSectorsColumnData(
						new String[] { RevenueType.ADMISSION.getName(), RevenueType.FACILITY.getName(),
								RevenueType.SCHOOL.getName(), "F&B", RevenueType.ACCOMMODATION.getName(),
								RevenueType.PROSHOP.getName(), RevenueType.RECREATION.getName() },
						admissionList, facilitiesList, schoolList, fbList, accommodationList, proshopList,
						recreationList));

		// row
		caculteTotal(admissionList);
		caculteTotal(facilitiesList);
		caculteTotal(schoolList);
		caculteTotal(fbList);
		caculteTotal(accommodationList);
		caculteTotal(proshopList);
		caculteTotal(recreationList);

		map.put("admission", admissionList);
		map.put("facilities", facilitiesList);
		map.put("schools", schoolList);
		map.put("F&B", fbList);
		map.put(RevenueType.ACCOMMODATION.getName(), accommodationList);
		map.put(RevenueType.PROSHOP.getName(), proshopList);
		map.put(RevenueType.RECREATION.getName(), recreationList);

		// pie chart
		List<BIpingDataDto> sectors = new ArrayList<>();
		BIpingDataDto sec = new BIpingDataDto(RevenueType.ADMISSION.getName(),
				(null != admissionList.get(admissionList.size() - 1)
						? admissionList.get(admissionList.size() - 1).getData() : BigDecimal.ZERO));
		sectors.add(sec);
		sec = new BIpingDataDto(RevenueType.FACILITY.getName(), (null != facilitiesList.get(facilitiesList.size() - 1)
				? facilitiesList.get(facilitiesList.size() - 1).getData() : BigDecimal.ZERO));
		sectors.add(sec);
		sec = new BIpingDataDto(RevenueType.SCHOOL.getName(), (null != schoolList.get(schoolList.size() - 1)
				? schoolList.get(schoolList.size() - 1).getData() : BigDecimal.ZERO));
		sectors.add(sec);
		sec = new BIpingDataDto("F&B",
				(null != fbList.get(fbList.size() - 1) ? fbList.get(fbList.size() - 1).getData() : BigDecimal.ZERO));
		sectors.add(sec);
		sec = new BIpingDataDto(RevenueType.ACCOMMODATION.getName(),
				(null != accommodationList.get(accommodationList.size() - 1)
						? accommodationList.get(accommodationList.size() - 1).getData() : BigDecimal.ZERO));
		sectors.add(sec);
		sec = new BIpingDataDto(RevenueType.PROSHOP.getName(), (null != proshopList.get(proshopList.size() - 1)
				? proshopList.get(proshopList.size() - 1).getData() : BigDecimal.ZERO));
		sectors.add(sec);
		sec = new BIpingDataDto(RevenueType.RECREATION.getName(), (null != proshopList.get(recreationList.size() - 1)
				? recreationList.get(recreationList.size() - 1).getData() : BigDecimal.ZERO));
		sectors.add(sec);
		map.put("sectorsPiePercentage", this.handerOccupation(sectors, null));

		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}

	private void handerAllItems(List<BIpingDataDto> dtos, String name) {
		List<BIpingDataDto> seqList = new ArrayList<BIpingDataDto>();
		Map<String, BIpingDataDto> mapDto = new HashMap<>();
		for (BIpingDataDto dto : dtos) {
			mapDto.put(dto.getCode(), dto);
		}
		Map<String, BIpingDataDto> map = this.getAllOasisItem(name);
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();// key
			if (mapDto.containsKey(key)) {
				BIpingDataDto dto = mapDto.get(key);
				dto.setCode(null);
				seqList.add(dto);
			} else {
				seqList.add(map.get(key));
			}
		}
		dtos.clear();
		dtos.addAll(seqList);
	}

	private Map<String, BIpingDataDto> getAllOasisItem(String name) {
		Map<String, BIpingDataDto> map = new LinkedHashMap<>();

		if (RevenueType.FANGUAN.name().equals(name)) {
			BiOasisTypeItem.Fangan[] items = BiOasisTypeItem.Fangan.values();
			for (BiOasisTypeItem.Fangan item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.BARLOUNGE.name().equals(name)) {
			BiOasisTypeItem.BarLounge[] items = BiOasisTypeItem.BarLounge.values();
			for (BiOasisTypeItem.BarLounge item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.SAKTI.name().equals(name)) {
			BiOasisTypeItem.Sakti[] items = BiOasisTypeItem.Sakti.values();
			for (BiOasisTypeItem.Sakti item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.ALLDAYDINING.name().equals(name)) {
			BiOasisTypeItem.Add[] items = BiOasisTypeItem.Add.values();
			for (BiOasisTypeItem.Add item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.PROSHOP.name().equals(name)) {
			BiOasisTypeItem.Proshop[] items = BiOasisTypeItem.Proshop.values();
			for (BiOasisTypeItem.Proshop item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.ACCOMMODATION.name().equals(name)) {
			BiOasisTypeItem.Accommodation[] items = BiOasisTypeItem.Accommodation.values();
			for (BiOasisTypeItem.Accommodation item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.FACILITY.name().equals(name)) {
			BiOasisTypeItem.Facility[] items = BiOasisTypeItem.Facility.values();
			for (BiOasisTypeItem.Facility item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.SCHOOL.name().equals(name)) {
			BiOasisTypeItem.School[] items = BiOasisTypeItem.School.values();
			for (BiOasisTypeItem.School item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.ADMISSION.name().equals(name)) {
			BiOasisTypeItem.Admission[] items = BiOasisTypeItem.Admission.values();
			for (BiOasisTypeItem.Admission item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.RECREATION.name().equals(name)) {
			BiOasisTypeItem.Recreation[] items = BiOasisTypeItem.Recreation.values();
			for (BiOasisTypeItem.Recreation item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.BANQUET.name().equals(name)) {
			BiOasisTypeItem.Banquet[] items = BiOasisTypeItem.Banquet.values();
			for (BiOasisTypeItem.Banquet item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.ROOMSERVICE.name().equals(name)) {
			BiOasisTypeItem.RoomService[] items = BiOasisTypeItem.RoomService.values();
			for (BiOasisTypeItem.RoomService item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		} else if (RevenueType.TEALOUNGE.name().equals(name)) {
			BiOasisTypeItem.TeaLounge[] items = BiOasisTypeItem.TeaLounge.values();
			for (BiOasisTypeItem.TeaLounge item : items) {
				BIpingDataDto data = new BIpingDataDto(item.getDesc(), BigDecimal.ZERO);
				map.put(item.getGtaCode(), data);
			}
		}
		return map;

	}

	private UsageRate getFBUsageRateByName(List<BIpingDataDto> list, String name) {
		BigDecimal[] datas = new BigDecimal[(list.size() > 0 ? list.size() : 1)];
		if (null == list || list.size() == 0 || list.size() == 1)
			datas[0] = BigDecimal.ZERO;
		UsageRate rate = new UsageRate();
		int i = 0;
		for (BIpingDataDto dto : list) {
			datas[i] = dto.getData();
			i++;
		}
		rate.setName(name);
		rate.setData(datas);
		return rate;
	}

	private List<UsageRate> getSectorsColumnData(String[] names, List<BIpingDataDto>... params) {
		// {name:xxx,data:[11,11,11,11]}
		List<UsageRate> usages = new ArrayList<>();
		int j = 0;
		for (List<BIpingDataDto> list : params) {
			BigDecimal[] datas = new BigDecimal[(null != list && list.size() > 0 ? list.size() : 1)];
			if (null == list || list.size() == 0)
				datas[0] = BigDecimal.ZERO;

			int i = 0;
			for (BIpingDataDto dto : list) {
				datas[i] = dto.getData();
				i++;
			}
			UsageRate rate = new UsageRate();
			rate.setName(names[j]);
			rate.setData(datas);
			usages.add(rate);
			j++;
		}
		return usages;
	}

	private List<BIpingDataDto> getFBDataSetFBsubType(String startDate, String endDate, Map<String, Object> map) {
		// F&B
		List<BIpingDataDto> fbList = new ArrayList<>();
		// F&B column data
		List<UsageRate> rates = new ArrayList<>();

		RevenueType[] types = RevenueType.values();
		for (RevenueType revenueType : types) {
			if (revenueType.name().equalsIgnoreCase(RevenueType.ADMISSION.name())
					|| revenueType.name().equalsIgnoreCase(RevenueType.SCHOOL.name())
					|| revenueType.name().equalsIgnoreCase(RevenueType.FACILITY.name())
					|| revenueType.name().equalsIgnoreCase(RevenueType.ACCOMMODATION.name())
					|| revenueType.name().equalsIgnoreCase(RevenueType.PROSHOP.name())
					|| revenueType.name().equalsIgnoreCase(RevenueType.RECREATION.name()))
				continue;

			List<BIpingDataDto> list = biMemberUsageRateDao.getDtoBySql(
					this.createOasisFlexSql(startDate, endDate, revenueType.name()), null, BIpingDataDto.class);

			this.handerAllItems(list, revenueType.name());

			UsageRate rate = getFBUsageRateByName(list, revenueType.getName());
			rates.add(rate);

			caculteTotal(list);
			map.put(revenueType.getName(), list);

			BIpingDataDto fb = new BIpingDataDto();
			fb.setName(revenueType.getName());
			if (null != list && list.size() > 0) {
				fb.setData(list.get(list.size() - 1).getData().setScale(2, RoundingMode.HALF_UP));
			} else {
				fb.setData(BigDecimal.ZERO);
			}
			fbList.add(fb);
		}
		map.put("F&B-Column", rates);

		return fbList;
	}

	/***
	 * add total column to list
	 * 
	 * @param list
	 */
	private void caculteTotal(List<BIpingDataDto> list) {
		BigDecimal total = BigDecimal.ZERO;
		for (BIpingDataDto dto : list) {
			dto.getData().setScale(2, RoundingMode.HALF_UP);
			total = total.add(dto.getData().setScale(2, RoundingMode.HALF_UP));
		}
		BIpingDataDto sum = new BIpingDataDto();
		sum.setName("Total");
		sum.setData(total);
		list.add(sum);
	}

	/***
	 * 
	 * @param yearMonth
	 *            yyyy-MM
	 * @return
	 */
	private String createAdmissionSql(String startDate, String endDate) {
		StringBuilder sbder = new StringBuilder();
		sbder.append("SELECT t.code,t.name,sum(t.data)as data from");
		sbder.append(" ( ");
		sbder.append("SELECT ");
		sbder.append(" CASE ");
		sbder.append(" WHEN sp.subscriber_type='IPM' AND r.input_value='true' THEN 'IPMM'");
		sbder.append(" WHEN sp.subscriber_type='IPM' AND r.input_value='false' THEN 'IPMN'");
		sbder.append(" WHEN sp.subscriber_type='CPM' AND r.input_value='true' THEN 'CPM'");
		sbder.append(" WHEN sp.subscriber_type='CPM' AND r.input_value='false' THEN 'CPM'");
		sbder.append(" END AS code , ");
		sbder.append(" CASE ");
		sbder.append(
				" WHEN sp.subscriber_type='IPM' AND r.input_value='true' THEN 'Individual Service Plan (Multi-Generation)'");
		sbder.append(
				" WHEN sp.subscriber_type='IPM' AND r.input_value='false' THEN 'Individual Service Plan (Non Multi-Generation)'");
		sbder.append(
				" WHEN sp.subscriber_type='CPM' AND r.input_value='true' THEN 'Corporate Service Plan (Multi-Generation)'");
		sbder.append(
				" WHEN sp.subscriber_type='CPM' AND r.input_value='false' THEN 'Corporate Service Plan (Non Multi-Generation)'");
		sbder.append(" END AS name , ");
		sbder.append(" SUM(trans.paid_amount) AS data ");
		sbder.append(" FROM customer_order_trans AS trans");
		sbder.append(" LEFT JOIN ( ");
		sbder.append("   customer_order_det det ");
		sbder.append("	 INNER JOIN customer_service_subscribe csb ON det.order_det_id=csb.order_det_id ");
		sbder.append("	) ON trans.order_no=det.order_no");
		sbder.append(" RIGHT JOIN  ");
		sbder.append(" ( ");
		sbder.append("  service_plan AS sp ");
		sbder.append("  INNER JOIN  service_plan_addition_rule r ON r.plan_no=sp.plan_no AND r.right_code='MGD'");
		sbder.append(" ) ");
		sbder.append("  ON sp.plan_no=csb.service_plan_no AND sp.subscriber_type IN ('IPM','CPM')");
		sbder.append("	 WHERE DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '" + startDate + "' AND  '"
				+ endDate + "'");
		sbder.append("	 AND trans.status='SUC'");
		sbder.append("	 AND csb.acc_no IS NOT NULL");
		sbder.append("	 AND sp.subscriber_type IN ('IPM','CPM')");
		sbder.append("	 GROUP BY sp.subscriber_type,r.input_value");
		sbder.append(" UNION ALL ");
		sbder.append(" SELECT 'DP' as code, 'Day Pass' AS name,");
		sbder.append(" SUM(dt.item_total_amout) AS data FROM customer_order_trans AS trans");
		sbder.append(" LEFT JOIN (");
		sbder.append("  customer_order_det dt ");
		sbder.append(
				"  INNER JOIN pos_service_item_price item ON dt.item_no=item.item_no AND item.item_catagory ='DP'");
		sbder.append(" )  ON trans.order_no=dt.order_no");
		sbder.append("  WHERE  trans.status='SUC' AND  item.item_catagory ='DP' ");
		sbder.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '" + startDate + "' AND  '"
				+ endDate + "' ");
		sbder.append(" GROUP BY item.item_catagory");
		sbder.append(" )as t ");
		sbder.append(" GROUP BY t.code ");

		return sbder.toString();
	}

	/***
	 * 
	 * @param yearMonth
	 *            yyyy-MM
	 * @return
	 */
	private String createFacilitiesSql(String startDate, String endDate) {
		// The revenue of Facilities / School need consider the refund, that is
		// payment amount subtract refund amount base the date range user
		// selected
		StringBuilder sbder = new StringBuilder();
		// SUC
		sbder.append(" select  \n");
		sbder.append("  a.item_catagory as code,\n");
		sbder.append(" CASE a.item_catagory \n");
		sbder.append(" WHEN 'GFBK' THEN 'Golfing Bay'\n");
		sbder.append(" ELSE  'Tennis Court'\n");
		sbder.append(" END AS name,(a.data- (IFNULL(b.data,0)))as data\n");
		sbder.append(" from (\n");
		sbder.append(" SELECT item.item_catagory, \n");
		sbder.append(" SUM(dt.item_total_amout) AS data FROM customer_order_trans AS trans \n");
		sbder.append(" LEFT JOIN ( \n");
		sbder.append("   customer_order_det dt \n");
		sbder.append(
				"   INNER JOIN pos_service_item_price item ON dt.item_no=item.item_no AND item.item_catagory IN ('GFBK','TFBK') \n");
		sbder.append(" ) ON trans.order_no=dt.order_no \n");
		sbder.append("  WHERE  trans.status='SUC' AND item.item_catagory IN ('GFBK','TFBK') \n");
		sbder.append("  AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '" + startDate + "' and '"
				+ endDate + "' \n");
		sbder.append("  GROUP BY item.item_catagory \n");
		sbder.append(" ) as a \n");

		sbder.append(" left join \n");
		// RFU
		sbder.append(" ( \n");
		sbder.append(" SELECT item.item_catagory, \n");
		sbder.append(" SUM(trans.paid_amount)AS data \n");
		sbder.append(" FROM customer_order_trans AS trans \n");
		sbder.append(" INNER JOIN  customer_order_trans strans ON strans.transaction_no=trans.from_transaction_no \n");
		sbder.append(" LEFT JOIN ( \n");
		sbder.append(" customer_order_det dt  \n");
		sbder.append(
				" INNER JOIN pos_service_item_price item ON dt.item_no=item.item_no AND item.item_catagory IN ('GFBK','TFBK') \n");
		sbder.append(" ) ON strans.order_no=dt.order_no \n");
		sbder.append(" WHERE  trans.status='RFU'  AND item.item_catagory IN ('GFBK','TFBK') \n");
		sbder.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '" + startDate + "' and '"
				+ endDate + "'  \n");
		sbder.append(" GROUP BY item.item_catagory \n");
		sbder.append(" ) as b  \n");
		sbder.append(" on a.item_catagory=b.item_catagory \n");
		return sbder.toString();
	}

	/***
	 * 
	 * @param yearMonth
	 *            yyyy-MM
	 * @return
	 */
	private String createSchoolSql(String startDate, String endDate) {
		StringBuilder sbder = new StringBuilder();
		// suc
		sbder.append(" SELECT \n");
		sbder.append(" a.item_catagory as code,\n");
		sbder.append(" CASE a.item_catagory \n");
		sbder.append(" WHEN 'GSS' THEN  'Golf Course' \n");
		sbder.append(" WHEN 'TSS' THEN  'Tennis Course' \n");
		sbder.append(" WHEN 'GS' THEN  'Golf Coaching' \n");
		sbder.append(" WHEN 'TS' THEN  'Tennis Coaching' \n");
		sbder.append(" END AS name , \n");
		sbder.append(" (a.data- (IFNULL(b.data,0)))as data  \n");
		sbder.append(" from ( select item.item_catagory,  \n");
		sbder.append(" SUM(dt.item_total_amout)AS data FROM customer_order_trans AS trans  \n");
		sbder.append(" LEFT JOIN ( \n");
		sbder.append("   customer_order_det dt \n");
		sbder.append(
				"   INNER JOIN pos_service_item_price item ON dt.item_no=item.item_no AND item.item_catagory IN  ('GSS','TSS','TS','GS') \n");
		sbder.append(" ) ON trans.order_no=dt.order_no \n");
		sbder.append(" WHERE  trans.status='SUC' AND item.item_catagory IN  ('GSS','TSS','TS','GS') \n");
		sbder.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '" + startDate + "' and '"
				+ endDate + "' \n");
		sbder.append(" GROUP BY item.item_catagory \n");
		sbder.append(" )as a \n");

		sbder.append(" left join \n");
		// RFU
		sbder.append(" ( \n");
		sbder.append(" SELECT item.item_catagory, \n");
		sbder.append(" SUM(trans.paid_amount)AS data \n");
		sbder.append(" FROM customer_order_trans AS trans \n");
		sbder.append(" INNER JOIN  customer_order_trans strans ON strans.transaction_no=trans.from_transaction_no \n");
		sbder.append(" LEFT JOIN ( \n");
		sbder.append(" customer_order_det dt  \n");
		sbder.append(
				" INNER JOIN pos_service_item_price item ON dt.item_no=item.item_no AND item.item_catagory IN  ('GSS','TSS','TS','GS') \n");
		sbder.append(" ) ON strans.order_no=dt.order_no \n");
		sbder.append(" WHERE  trans.status='RFU'  AND item.item_catagory IN  ('GSS','TSS','TS','GS') \n");
		sbder.append(" AND DATE_FORMAT(trans.transaction_timestamp,'%Y-%m-%d') BETWEEN '" + startDate + "' and '"
				+ endDate + "'  \n");
		sbder.append(" GROUP BY item.item_catagory \n");
		sbder.append(" ) as b  \n");
		sbder.append(" on a.item_catagory=b.item_catagory \n");

		return sbder.toString();
	}

	/***
	 * 
	 * @param yearMonth
	 *            yyyy-MM
	 * @param type
	 *            is case/where
	 * @return
	 */
	private String createItemSqlByParty(String party, String type) {
		StringBuilder sb = new StringBuilder();
		if (party.equals(RevenueType.ACCOMMODATION.name())) {
			Accommodation[] items = BiOasisTypeItem.Accommodation.values();
			for (Accommodation item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}

			}
		} else if (party.equals(RevenueType.PROSHOP.name())) {
			Proshop[] items = BiOasisTypeItem.Proshop.values();
			for (Proshop item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}
			}
		} else if (party.equals(RevenueType.SAKTI.name())) {
			Sakti[] items = BiOasisTypeItem.Sakti.values();
			for (Sakti item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}

			}
		}

		else if (party.equals(RevenueType.ALLDAYDINING.name())) {
			Add[] items = BiOasisTypeItem.Add.values();
			for (Add item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}

			}
		} else if (party.equals(RevenueType.BARLOUNGE.name())) {
			BarLounge[] items = BiOasisTypeItem.BarLounge.values();
			for (BarLounge item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}

			}
		} else if (party.equals(RevenueType.FANGUAN.name())) {
			Fangan[] items = BiOasisTypeItem.Fangan.values();
			for (Fangan item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}

			}
		} else if (party.equals(RevenueType.TEALOUNGE.name())) {
			TeaLounge[] items = BiOasisTypeItem.TeaLounge.values();
			for (TeaLounge item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}
			}
		} else if (party.equals(RevenueType.RECREATION.name())) {
			Recreation[] items = BiOasisTypeItem.Recreation.values();
			for (Recreation item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}

			}
		} else if (party.equals(RevenueType.BANQUET.name())) {
			Banquet[] items = BiOasisTypeItem.Banquet.values();
			for (Banquet item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}

			}
		} else if (party.equals(RevenueType.ROOMSERVICE.name())) {
			RoomService[] items = BiOasisTypeItem.RoomService.values();
			for (RoomService item : items) {
				if (checkCondition(type)) {
					sb.append(" when '" + item.getGtaCode() + "' then '" + item.getDesc() + "' \n");
				} else {
					if (StringUtils.isEmpty(sb.toString())) {
						sb.append("'" + item.getGtaCode() + "'");
					} else {
						sb.append(",'" + item.getGtaCode() + "'");
					}
				}

			}
		}
		return sb.toString();
	}

	/***
	 * if type is case return true else false
	 * 
	 * @param type
	 * @return
	 */
	private boolean checkCondition(String type) {
		if ("case".equals(type)) {
			return true;
		} else {
			return false;
		}
	}

	private String createOasisFlexSql(String startDate, String endDate, String name) {
		StringBuilder sbder = new StringBuilder();
		sbder.append(" SELECT ");
		sbder.append(" a.gta_tag as code,");
		sbder.append(" case a.gta_tag");
		sbder.append(this.createItemSqlByParty(name, "case"));
		sbder.append(" else a.description");
		sbder.append(" end as name,");
		sbder.append(" SUM(amount) AS data ");
		sbder.append(" FROM bi_oasis_tran_code a,");
		sbder.append(" bi_oasis_flex_file b");
		sbder.append(" WHERE a.oasis_tran_code=b.oasis_tran_code");
		sbder.append(" AND a.gta_tag in (");
		sbder.append(this.createItemSqlByParty(name, "where"));
		sbder.append(")");
		sbder.append(
				" AND DATE_FORMAT(b.oasis_trans_date, '%Y-%m-%d') BETWEEN '" + startDate + "' and '" + endDate + "'");
		sbder.append(" GROUP BY  a.gta_tag ");

		return sbder.toString();
	}
}
