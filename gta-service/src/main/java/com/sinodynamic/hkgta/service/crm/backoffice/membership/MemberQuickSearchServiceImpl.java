package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dto.Constant.memberType;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.dto.membership.MemberQuickSearchDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.FacilityCode;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.OtherRightCode;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.TrainingCode;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.cardmanage.PCDayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@Service
public class MemberQuickSearchServiceImpl extends ServiceBase<CustomerProfile> implements MemberQuickSearchService{
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private MemberDao memberDao;
	

	@Autowired
	private PCDayPassPurchaseService		pcDayPassPurchaseService;
	
	@Autowired
	private UsageRightCheckService usageRightCheckService;
	@Autowired
	private CustomerServiceAccService customerServiceAccService;
	@Autowired
	private GlobalParameterService globalParameterService;
	
	
	@Transactional
	public ResponseResult quickSeachMember(String number) {
		List<CustomerProfile> customer =customerProfileDao.quickSearchCustomerIdByAcademyIdOrPassportNo(number);
		if (customer == null || customer.size() == 0)
		{
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
			return responseResult;
		}
		
		//modified by Kaster 20160328
//		Map<String, String> resultMap = new HashMap<String, String>();
		
		CustomerProfile cp = customer.get(0);
//		resultMap.put("customerId", cp.getCustomerId().toString());
//		resultMap.put("salutation", cp.getSalutation());
//		resultMap.put("givenName", cp.getGivenName());
//		resultMap.put("surname", cp.getSurname());
//		resultMap.put("memberType", cp.getMemberType());
//		resultMap.put("memberName", cp.getSalutation() + " " + cp.getGivenName() + " " + cp.getSurname());
//		resultMap.put("academyNo", cp.getAcademyNo());
		//member
		Member m = memberDao.getMemberByCustomerId(cp.getCustomerId());
//		resultMap.put("status", m.getStatus());
		
		MemberQuickSearchDto dto = new MemberQuickSearchDto();
		dto.setAcademyNo(cp.getAcademyNo());
		dto.setCustomerId(cp.getCustomerId());
		dto.setSalutation(cp.getSalutation());
		dto.setGivenName(cp.getGivenName());
		dto.setSurname(cp.getSurname());
		dto.setMemberType(cp.getMemberType());
		dto.setMemberName(cp.getSalutation() + " " + cp.getGivenName() + " " + cp.getSurname());
		dto.setStatus(m.getStatus());
		
		
		/***
		 * CR46 Preferred Payment Method
		 */
		ResponseResult rest=customerServiceAccService.getPaymentMethodByCustomerId(cp.getCustomerId(), null);
		if(null!=rest&&GTAError.Success.SUCCESS.getCode().equals(rest.getReturnCode()))
		{
			dto.setPaymentMethods((List<PaymentFacilityDto>)rest.getDto());
		}
		
		
		responseResult.initResult(GTAError.CommomError.GET_MEMBER_BYID_SUCC, dto);
		return responseResult;
	}
	/***
	 * @time 2016-04-18
	 * @author christ
	 * @desc SGG-1140
	 *  1.No Permission (The highest)  check member facility permission ,is no resverion permission pop message
		2.Service account effective period :check memeber account effective <currentTime pop message
		[Golf/tennis or Golf course/Tennis Course or private Golf/Tennis coaching]
	 */
	@Transactional
	public ResponseResult  checkPerssionOrServiceAccount(Long customerId,String number,String startTime,String endTime,String reservationDate,String facilityType)throws Exception
	{
		CustomerProfile customer=null;
		Member member=null;
		if(StringUtils.isNotEmpty(number)){
			List<CustomerProfile> customers =customerProfileDao.quickSearchCustomerIdByAcademyIdOrPassportNo(number);
			if(CollectionUtils.isNotEmpty(customers)){
				customer=customers.get(0);
			}
		}
		if(null!=customerId){
			customer=customerProfileDao.getById(customerId);
		}
		if(null==customer){
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
			return responseResult;
		}else{
			member = memberDao.getMemberById(customer.getCustomerId());
			// check IDM – Individual Dependent Member;
			// CDM – Corporate Dependent Member
			if(memberType.IDM.name().equals(member.getMemberType())||memberType.CDM.name().equals(member.getMemberType())){
				customerId=member.getSuperiorMemberId();
			}else{
				customerId=customer.getCustomerId();
			}
		}
		/***
		 * check wellness
		 */
		if(UsageRightCheckService.FacilityCode.WELLNESS.name().equals(facilityType)){
			if(!globalParameterService.checkGlobalParameterSwitch(GlobalParameterType.WELLNESS_SWITCH.getCode())){
				responseResult.initResult(GTAError.FacilityError.NO_PERMISSION,"Wellness reservation is not available now !");	
				return responseResult;
			}
		}
		/*
		 * 1.No Permission (The highest)  check member facility permission ,is no resverion permission pop message
		 */
		/***
		 * TRAIN1("Golf Courses"),
		   TRAIN2("Golf Private Coaching"),
		   TRAIN3("Tennis Courses"),
		   TRAIN4("Tennis Private Coaching");
		 */
		if(UsageRightCheckService.TrainingCode.TRAIN1.name().equals(facilityType)
				||UsageRightCheckService.TrainingCode.TRAIN2.name().equals(facilityType)
				||UsageRightCheckService.TrainingCode.TRAIN3.name().equals(facilityType)
				||UsageRightCheckService.TrainingCode.TRAIN4.name().equals(facilityType))
		{
		    TrainingCode trainingCode=convertTrainingCodeByType(facilityType);
		    
		    UsageRightCheckService.TrainingRight traningRight=usageRightCheckService.checkTrainingRight(customerId, trainingCode, null,null,null);
			if(!traningRight.isCanBook()){
				   //no permission access
				if(!usageRightCheckService.checkRenewedTrainingRight(customerId, trainingCode)){
					responseResult.initResult(GTAError.FacilityError.NO_PERMISSION_AC);
					return responseResult;
				}
			}
			
		}
		/***
		 * GOLF bay or TENNIS court
		 */
		//filter  CR46 UsageRightCheckService.FacilityCode.GUEST.name().equals(facilityType)
		else if (UsageRightCheckService.FacilityCode.GOLF.name().equals(facilityType)||
				UsageRightCheckService.FacilityCode.TENNIS.name().equals(facilityType)||
			UsageRightCheckService.FacilityCode.WELLNESS.name().equals(facilityType))
		{
			 FacilityCode facilityCode=convertFacilityCodeByType(facilityType);
			 //default currentTime 
			 UsageRightCheckService.FacilityRight facilityRight=usageRightCheckService
						.checkFacilityRight(customerId,facilityCode,null);
			if (null==facilityRight||!facilityRight.isCanBook()) {
			     //no permission access
				if(!usageRightCheckService.checkRenewedFacilityRight(customerId, facilityCode))
				{
					responseResult.initResult(GTAError.FacilityError.NO_PERMISSION_AC);
					 return responseResult;
				}
			}
			
		}else if(UsageRightCheckService.FacilityCode.DAYPASS.name().equals(facilityType))
		{
			//check the quotas, if day pass quotas per day in service plan <=0, means no permission, can't go into next page
			if (Constant.Status.NACT.toString().equalsIgnoreCase(member.getStatus())) {
				responseResult.initResult(GTAError.CourseError.COURSE_ENROLLMENT_NACT_MEMBER);
				return responseResult;
			}
			DaypassPurchaseDto returnDto = pcDayPassPurchaseService.checkPurchasaeDayPassLimit(member);
			// check member can buy daypass or not
			if (returnDto.getTotalMemberQuota() <= 0) {
				 responseResult.initResult(GTAError.FacilityError.NO_PERMISSION_AC);
				return responseResult;
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		}
		/*
		 * 2.Service account effective period :check memeber account effective <currentTime pop message
		 */
		//GOLF/TENNIS course enrool
		if(StringUtils.isNotEmpty(startTime)&&StringUtils.isNotEmpty(endTime))
		{
			if(!customerServiceAccService.isCustomerServiceAccValidByDate(customerId, startTime,endTime)){
				/**
				 * check rnewwal service  : no MemberLimitRule  only customer_service_acc  ,customer_order_hd status is CMP
				 * check every service plan expiry_date 
				 */
				if(!customerServiceAccService.checkRenewedService(customerId, reservationDate, startTime, endTime)){
					responseResult=convertResponseByFacilityType(facilityType);
				}else{
					responseResult.initResult(GTAError.Success.SUCCESS);
				}
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		}else if(StringUtils.isNotEmpty(reservationDate)){
			if(!customerServiceAccService.isCustomerServiceAccValidByDate(customerId, CommUtil.toDate(reservationDate, "yyyy-MM-dd"))){
				/**
				 * check rnewwal service  : no MemberLimitRule  only customer_service_acc  ,customer_order_hd status is CMP
				 * check every service plan expiry_date 
				 */
				if(!customerServiceAccService.checkRenewedService(customerId, reservationDate, startTime, endTime)){
					responseResult=convertResponseByFacilityType(facilityType);
				}else{
					responseResult.initResult(GTAError.Success.SUCCESS);
				}
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		}
		
		return responseResult;
	}
	
	private ResponseResult convertResponseByFacilityType(String facilityType){
		if(UsageRightCheckService.TrainingCode.TRAIN1.name().equals(facilityType)||UsageRightCheckService.TrainingCode.TRAIN3.name().equals(facilityType)){
			responseResult.initResult(GTAError.CourseError.BEYOND_PERIOD);
		}else if(UsageRightCheckService.FacilityCode.DAYPASS.name().equals(facilityType)){
			responseResult.initResult(GTAError.DayPassError.BEYOND_PERIOD);
		}else if(UsageRightCheckService.FacilityCode.GUEST.name().equals(facilityType)){
			responseResult.initResult(GTAError.GuestRoomError.BEYOND_PERIOD);
		}
		else{
			responseResult.initResult(GTAError.FacilityError.NO_PERMISSION);	
		}
		return responseResult;
	} 
	private TrainingCode  convertTrainingCodeByType(String facilityType){
		TrainingCode trainingCode=null;
		 if(UsageRightCheckService.TrainingCode.TRAIN1.name().equals(facilityType)){
			  trainingCode=UsageRightCheckService.TrainingCode.TRAIN1;
		 }else if (UsageRightCheckService.TrainingCode.TRAIN2.name().equals(facilityType)){
			 trainingCode=UsageRightCheckService.TrainingCode.TRAIN2;
		 }
		 else if (UsageRightCheckService.TrainingCode.TRAIN3.name().equals(facilityType)){
			 trainingCode=UsageRightCheckService.TrainingCode.TRAIN3;
		 }
		 else if (UsageRightCheckService.TrainingCode.TRAIN4.name().equals(facilityType)){
			 trainingCode=UsageRightCheckService.TrainingCode.TRAIN4;
		 }
		 return trainingCode;
	}
	public FacilityCode convertFacilityCodeByType(String facilityType){
		FacilityCode facilityCode=null;
		if(UsageRightCheckService.FacilityCode.GOLF.name().equals(facilityType)){
			 facilityCode=UsageRightCheckService.FacilityCode.GOLF;
		 }else if (UsageRightCheckService.FacilityCode.TENNIS.name().equals(facilityType)){
			 facilityCode=UsageRightCheckService.FacilityCode.TENNIS;
		 }
		 else if (UsageRightCheckService.FacilityCode.WELLNESS.name().equals(facilityType)){
			 facilityCode=UsageRightCheckService.FacilityCode.WELLNESS;
		 }
		 else if (UsageRightCheckService.FacilityCode.GUEST.name().equals(facilityType)){
			 facilityCode=UsageRightCheckService.FacilityCode.GUEST;
		 }
		return facilityCode;
	}
}
