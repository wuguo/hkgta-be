package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.pms.AssignAdHocTaskDto;
import com.sinodynamic.hkgta.dto.pms.AssignMaintenanceTaskDto;
import com.sinodynamic.hkgta.dto.pms.GuestRequestDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskQueryDto;
import com.sinodynamic.hkgta.dto.pms.RoomMemberDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomTaskDto;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RoomHousekeepTaskService extends IServiceBase<RoomHousekeepTask> {

	public void assignRoomHousekeepTask(RoomHousekeepTaskDto roomHousekeepTaskDto) throws Exception;

	public Long cancelRoomHousekeepTask(Long taskId, String userId) throws Exception;

	public Long reassignRoomHousekeepTask(RoomHousekeepTaskDto roomHousekeepTaskDto) throws Exception;

	public Long updateRoomHousekeepTask(RoomHousekeepTaskDto roomHousekeepTaskDto) throws Exception;

	public List<RoomHousekeepTaskQueryDto> getRemindRoomHousekeepTaskList(Integer assistantResTime,Integer inspectorResTime,Integer supervisorResTime);

	public void saveRoomHousekeepTask(RoomHousekeepTask roomHousekeepTask) throws Exception;

	public GuestRequestDto getGuestRequestDto(Long taskId);

	public Long updateRoomHousekeepTask(UpdateRoomTaskDto taskDto, String userId) throws Exception;

	public RoomHousekeepTaskDto getRoomHousekeepTask(Long taskId)  throws Exception;

	public boolean removeGuestRequestDto(Long taskId, String userId);

	public boolean approveGuestRequestDto(Long taskId, String taskDescription, String userId);

	public ResponseResult assignAdHocTask(AssignAdHocTaskDto dto, String userId);

	public ResponseResult assignAdHocTask(AssignAdHocTaskDto dto, String status, String userId);

	public ResponseResult assignMaintenanceTask(AssignMaintenanceTaskDto dto, String userId);
	public ResponseResult getStaffType(String userId);
	
	public void autoRemindRoomAssistant(String roleType,String functionId,Room room,RoomHousekeepTask task);
	
	public RoomHousekeepTask getRoomHousekeepTaskById(Long taskId);
	
	public void update(RoomHousekeepTask roomHousekeepTask);
	
	public List<RoomHousekeepTask> getRoomHousekeepTaskUnCmpList(String jobType);
	
	public List<RoomMemberDto> getMyRoomListForApp(String userId,String roleType,String roomNo);
	
	public List<RoomHousekeepTaskQueryDto> getMyRoomTaskListForApp(String userId, String roleType, String roomNo);
	
	public List<RoomMemberDto> getRoomListForApp(String userId,String roleType,String roomNo);
	
	public List<RoomHousekeepTaskQueryDto> getRoomTaskListForApp(String userId, String roleType, String roomNo);
	
	/**
	 * 会员添加房间任务
	 * @param userId
	 * @param roomId
	 * @param serviceContent  服务详情内容
	 * @return
	 */
	public ResponseResult addRoomTaskByMember(String userId, String roomId, String serviceContent);

}