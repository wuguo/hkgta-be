package com.sinodynamic.hkgta.service.mms;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.entity.mms.SpaFlexDailySummary;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface SpaFlexDailySummaryService extends IServiceBase<SpaFlexDailySummary> 
{
	public Serializable save(SpaFlexDailySummary spaFlexDailySummary);
	
	public boolean deleteByTransactionDate(String transDate);
	
	public List<PaymentFacilityDto>getSpaFlexDailySummaryByDate(String transDate);
}
