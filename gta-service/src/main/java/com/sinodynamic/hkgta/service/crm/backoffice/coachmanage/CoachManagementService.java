package com.sinodynamic.hkgta.service.crm.backoffice.coachmanage;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dto.crm.CoachRosterInfo;
import com.sinodynamic.hkgta.dto.fms.AvailableCoachDto;
import com.sinodynamic.hkgta.dto.fms.AvailableDateDto;
import com.sinodynamic.hkgta.dto.fms.MemberFacilityBookingDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffCoachRoaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CoachManagementService extends IServiceBase<StaffCoachRoaster> {

	public boolean savePresentRoster(String userId, CoachRosterInfo rosterInfo);
	
	public CoachRosterInfo loadPresentRoster(String coachId);
	
	public boolean saveCustomizedRoster(String userId, CoachRosterInfo rosterInfo);
	
	public CoachRosterInfo loadCustomizedRoster(String coachId, Date start, Date end);
	
	public List<CoachRosterInfo> loadMultipleWeekCustomizedRoster(String coachId, Date start, Date end);
	
	public List<AvailableCoachDto> loadAvailableCoach(Date date,String timeslot,String staffType);
	
	public List<AvailableCoachDto> loadAvailableCoach(Date date,String timeslot,String staffType,boolean createOffer,Long originResvId);
	
	public PrivateCoachInfoDto loadCoachProfile(String coachId);
	
	public List<AvailableDateDto> loadInavailableRoster(String coachId, Date begin, Date end);
	
	public ResponseResult calcBookingPrice(MemberFacilityBookingDto booking);
	
	public void changeAttendanceStatus(Long resvId, String attendanceStatus, String loginUser);
	
	public List<AvailableCoachDto> getCoachByStaffType(String date,String startTime,String endTime,String staffType);
	/***
	 * get coaches perforamnce sql by startTime and endTime
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public StringBuffer getCoachesPerformanceSQL(String startTime,String endTime);
	/***
	 * get coachTraining SQL
	 * @param coachUserId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public StringBuffer getCoachTrainingSQL(String coachUserId,String startTime,String endTime);
	/***
	 * 
	 * @param coachId userId
	 * @param date yyyy-MM-dd
	 * @param beginTime 10,11,12
	 * @param endTime 12,14
	 * @return
	 */
	public ResponseResult getCoachPrice(String coachId,String date,String beginTime,String endTime);
}
