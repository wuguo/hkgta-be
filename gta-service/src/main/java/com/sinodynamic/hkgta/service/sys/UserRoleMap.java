package com.sinodynamic.hkgta.service.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.sinodynamic.hkgta.dto.sys.MenuDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;


/**
 * This class contain static variable and cannot replicate across tomcat clusters. Need to rewrite (commented by Sam 2016-2-19)    
 * 
 * @author Vian
 *
 */
@Service
//@Scope("singleton")
//public class UserRoleMap
@Deprecated
public class UserRoleMap 
//  implements Serializable
{
	// static private UserRoleMap instance = null;  // TODO : poor code to use static variable. Need to rewrite later
	private UserRoleMap instance = null;   	

	private Map<String,  List<MenuDto>> roleMenu = new HashMap<String, List<MenuDto>>();
	
	private Map<String, List<UserRoleDto>> userRole = new HashMap<String,List<UserRoleDto>>();

	private Logger logger = Logger.getLogger(UserRoleMap.class);
	//public static synchronized UserRoleMap getInstance()
 
	public synchronized UserRoleMap getInstance()
	{
		if (instance == null)
		{
			instance = new UserRoleMap();
			
			logger.debug("rew UserRoleMap ");
		}
		
		return instance;
	}
	
	// TODO : reload Menu 
	public void destroyUserRoleInstance() {
		instance=null;
	}

	public void setInstance(UserRoleMap instance) {
		this.instance=instance;
	}
	

	public Map<String, List<MenuDto>> getRoleMenu()
	{
		return roleMenu;
	}

	public void setRoleMenu(Map<String, List<MenuDto>> roleMenu)
	{
		this.roleMenu = roleMenu;
	}

	//@Cacheable(value = "menuCache", key="#userId")  // Cache Added by SAM 20160222
	public List<UserRoleDto> getUserRole(String userId)
	{
		
	    //System.out.println("DEBUG getUserRole userId: " + userId + " magic no.: " + Math.random() ); 
	    
		return userRole.get(userId);
	}

	public void setUserRole(String userId, List<UserRoleDto> userRoles)
	{
		userRole.put(userId, userRoles);
	}

	public Map<String, List<UserRoleDto>> getUserRole()
	{
		return this.userRole;
	}
	
	public void setUserRole(Map<String, List<UserRoleDto>> userRoles)
	{
		this.userRole=userRoles;
	}	
	
}


