package com.sinodynamic.hkgta.service.crm.sales.enrollment;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dto.crm.AllowMarketingDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoCaptionDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.dto.memberapp.MemberAppCustomerAdditionInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoCaption;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoPK;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CustomerAdditionInfoServiceImpl extends ServiceBase<CustomerAdditionInfo> implements
		CustomerAdditionInfoService {
    
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfo;
	
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	public CustomerAdditionInfo getCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		return customerAdditionInfo.getCustomerAdditionInfo(t);
	}

	public void saveCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		customerAdditionInfo.saveCustomerAdditionInfo(t);

	}

	public void updateCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		customerAdditionInfo.updateCustomerAdditionInfo(t);

	}

	public void deleteCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		customerAdditionInfo.deleteCustomerAdditionInfo(t);

	}

	@Override
	@Transactional
	public ResponseResult editCustomerAdditionInfo(
			MemberAppCustomerAdditionInfoDto dto, String userId) {
		if(dto.getCustomerAdditionInfoDtos()!=null && dto.getCustomerAdditionInfoDtos().size() >0){
			for(CustomerAdditionInfoDto cust : dto.getCustomerAdditionInfoDtos()){
				CustomerAdditionInfo customerAdditionInfo = new CustomerAdditionInfo();
				CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
				cpk.setCaptionId(Long.parseLong(cust.getCaptionId()));
				cpk.setCustomerId(dto.getCustomerId());
				customerAdditionInfo.setCustomerInput(cust.getCustomerInput());
				customerAdditionInfo.setId(cpk);
				customerAdditionInfo.setUpdateBy(userId);
				customerAdditionInfo.setUpdateDate(new Date());
				CustomerAdditionInfo targetEntity = customerAdditionInfoDao.get(CustomerAdditionInfo.class, cpk);
				if(targetEntity != null){
					String[] ignoreFileds = new String[]{"id","createBy","createDate","sysId"};
					BeanUtils.copyProperties(customerAdditionInfo, targetEntity,ignoreFileds);
					customerAdditionInfoDao.saveOrUpdate(targetEntity);
				}else{
					customerAdditionInfoDao.save(customerAdditionInfo);
				}
			}
		}
		responseResult.initResult(GTAError.CommonError.UPDATE_SUCCESS);
		return responseResult;
	}
	/**
	 * 是否批准营销服务
	 * @param dto
	 * @param userId
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult isAllowMarketing(AllowMarketingDto dto, String userId) {
		initMarketingValue();
		Long captionId = Constant.OVERVIEW_HKGTA_MARKETING;
		long customerId = Long.valueOf(dto.getCustomerId() + "");
		switch (dto.getAllowType()) {
		case "NWD":
			captionId = Constant.OVERVIEW_WMD_MARKETING;
			break;
		default:
			break;
		}
		CustomerAdditionInfo  info = customerAdditionInfoDao.getByCustomerIdAndCaptionId(customerId, captionId);
		if (!dto.isAllow()) {
			if (null == info) {
				info = new CustomerAdditionInfo();
				CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
				cpk.setCaptionId(captionId);
				cpk.setCustomerId(customerId);
				info.setCustomerInput("No");
				info.setId(cpk);
				info.setUpdateBy(userId);
				info.setUpdateDate(new Date());
				CustomerAdditionInfo targetEntity = customerAdditionInfoDao.get(CustomerAdditionInfo.class, cpk);
				if(targetEntity != null){
					String[] ignoreFileds = new String[]{"id","createBy","createDate","sysId"};
					BeanUtils.copyProperties(info, targetEntity,ignoreFileds);
					customerAdditionInfoDao.saveOrUpdate(targetEntity);
				}else{
					customerAdditionInfoDao.save(info);
				}
				
			} else {
				if (!info.getCustomerInput().equals("No")) {
					info.setCustomerInput("No");
					customerAdditionInfoDao.update(info);
				}
			}
			
		} else {
			if (null != info) {
				customerAdditionInfoDao.delete(info);
			}
			
		}
		try {
			Member member = memberService.getMemberById(customerId);
			if (member != null) {
				member.setUpdateDate(new Date());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	/**
	 * 获取Marketing id
	 */
	@Override
	@Transactional
	public void initMarketingValue(){
		if (Constant.OVERVIEW_WMD_MARKETING == null || Constant.OVERVIEW_HKGTA_MARKETING == null) {
			List<CustomerAdditionInfoCaptionDto>  list = customerAdditionInfoDao.getCustomerAdditionInfoCaptionByMarketing();
			for (CustomerAdditionInfoCaptionDto customerAdditionInfoCaption : list) {
				if (customerAdditionInfoCaption.getCaption().equals("HKGTA Marketing")) {
					Constant.OVERVIEW_HKGTA_MARKETING = customerAdditionInfoCaption.getCaptionId();
				} else {
					Constant.OVERVIEW_WMD_MARKETING = customerAdditionInfoCaption.getCaptionId();
				}
			}
		}
	}

}
