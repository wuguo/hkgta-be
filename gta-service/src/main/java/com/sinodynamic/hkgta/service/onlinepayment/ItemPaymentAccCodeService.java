package com.sinodynamic.hkgta.service.onlinepayment;

import java.util.List;

import com.sinodynamic.hkgta.entity.onlinepayment.ItemPaymentAccCode;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface ItemPaymentAccCodeService extends IServiceBase<ItemPaymentAccCode> {
	/***
	 * get ItemPaymentAccCode list 
	 * @param status 
	 * @param categoryCode  when null then get exclude categoryCode is SPA
	 * else get by categoryCode
	 * @return
	 */
	public List<ItemPaymentAccCode> getItemPaymentAccCodeByStatus(String status,String categoryCode);
}
