
package com.sinodynamic.hkgta.service.rpos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.rpos.PaymentInfoDto;
import com.sinodynamic.hkgta.dto.rpos.RefundInfoDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.RefundServiceType;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RefundService extends IServiceBase<CustomerRefundRequest> {
	Data getPendingRefundList(Date base, int days,
			String propertyName, String order, int pageSize, int currentPage,
			AdvanceQueryDto filters);
	
	Data getServiceRefundList(String status,Long customerId,
			String propertyName, String order, int pageSize, int currentPage,
			AdvanceQueryDto filters);
	
	Data getCashValueRefundList(Date base, Integer days, String status,Long customerId,
			String propertyName, String order, int pageSize, int currentPage,
			AdvanceQueryDto filters);
	
	List<AdvanceQueryConditionDto> advanceSearch();
	
	List<AdvanceQueryConditionDto> cashValueAdvanceSearch();
	
	List<AdvanceQueryConditionDto> serviceRefundTxnAdvanceSearch();
	
	List<AdvanceQueryConditionDto> cashValueRefundTxnAdvanceSearch();
	
	PaymentInfoDto getPaymentInfo(Long refundId);
	
	RefundInfoDto getRefundInfo(Long refundId);
	
	ResponseResult getBookingDetail(Long refundId, RefundServiceType type);
	
	void refund(Long refundId, BigDecimal refundAmt, String loginUserId, String remark);
	
	public void refund(Long fromTransactionNo,Long customerId,BigDecimal refundAmt, String loginUserId, String remark);
	/***
	 * 
	 * @param fromTransactionNo
	 * @param customerId
	 * @param refundAmt
	 * @param loginUserId
	 * @param remark
	 */
	public void refund(Long fromTransactionNo,Long customerId,BigDecimal refundAmt, String loginUserId, String extRefNo,String remark);
	
	void directionalRefund(final CustomerOrderTrans txn, RefundServiceType serviceType, final BigDecimal actualRefundAmt, final String loginUserId, Map<String, Object> params);
	
	void requestCashValueRefund(Long customerId, BigDecimal requestedAmt, String loginUserId, String remark);
	
	void cashValueRefund(Long refundId, BigDecimal refundAmt, String loginUserId, String remark, String refNo);
	
	public void sendRefundResultEmail(long refundId);

	/**   
	* @author: Zero_Wang
	* @since: Aug 7, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	ResponseResult rejectFefund(long refundId,String userId);

	RefundInfoDto getCancelInfo(String resvId);
	
	/***
	 * 
	 * @param transcationId
	 * @param customerId
	 * @return
	 */
	ResponseResult refundCashValueOasis(long transcationId,String academyId);
	
	public boolean sendServiceRefundMail(String templateFunctionId,String paramId,Long refundId);
}


