package com.sinodynamic.hkgta.service.adm;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.BiMemberUsageRateDto;
import com.sinodynamic.hkgta.entity.adm.BiMemberUsageRate;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface BiMemberUsageRateService extends IServiceBase<BiMemberUsageRate>
{
	public ResponseResult saveBiMemberUsageRate(BiMemberUsageRate biMemberUsageRate);
	
	public void saveBiMemberUsageRate(String loginDevice, UserMaster userMaster);
	
	public ResponseResult getBiMemberUsageRateListByYear(String year,String type);
	
	public  List<BiMemberUsageRateDto> getBiMemberUsageRateByYear(String year,String type);

	/**
	 *  激活用户批量插入操作
	 * @param date
	 * @return
	 */
	public int bulkInsertByActiveMember(String date);

	/**
	 * 判断是否已经执行过激活用户的批量操作
	 * @param date
	 * @return
	 */
	public boolean isRunInsert(String date);

	/**
     * 更新激活用户book数
     * @param date
     * @return
     * @throws HibernateException
     */
    public int updateActiveMemberBookNum(String date, String ids);

    /**
     * 获取活跃用户数
     * @param year
     * @return
     */
	public ResponseResult getActiveUsersNumber(String year);
	
	public ResponseResult getPatronAnalysisData();

	 /**
     * 批量插入最新激活用户操作
     * @param date
     * @return
     * @throws HibernateException
     */
    public int bulkInsertByNewsActiveMember(String date);
    
    public ResponseResult getRevenueReportsDataRange(String startDate,String endDate);

	
}
