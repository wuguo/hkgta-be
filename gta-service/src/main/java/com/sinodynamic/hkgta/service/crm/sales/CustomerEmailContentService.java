package com.sinodynamic.hkgta.service.crm.sales;

import java.io.File;
import java.util.List;

import com.sinodynamic.hkgta.dto.crm.DaypassIssuedDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;


/**
 * Summarize of email function
 * @author Mianping_Wu
 *
 */
public interface CustomerEmailContentService extends IServiceBase<CustomerEmailContent> {

	public CustomerEmailContent sendPresentationEmailToCustomer(File attach, Long customerID, String emailTitle, String emailBody) throws Exception;
	
	public CustomerEmailContent sendPresentationEmailToSomebody(File attach, String emailTo, String emailTitle, String emailBody) throws Exception;
	
	public boolean modifyCustomerEmailContent(CustomerEmailContent cec);
	
	public CustomerEmailContent sendEmail4Transaction(TransactionEmailDto dto,String userName) throws Exception;
	
	public TransactionEmailDto getOpenEmailTemplate(Long IDNO, String emailType, String userName);
	
	public CustomerEmailContent sendRenewalEmail(Long customerID, String emailTitle, String emailBody) throws Exception;
	
	public CustomerEmailContent sendExtensionLetter(Long customerID, String emailTitle, String emailBody) throws Exception;
	
	public void updateEmailStatusInBatch(List<CustomerEmailContent> emailList) throws Exception;
	
	public ResponseResult daypassIssuedNotify(DaypassIssuedDto daypassIssuedDto) throws Exception;
	
	public CustomerEmailContent getCustomerEmailContent(String id);

	public void saveCustomerEmailContent(CustomerEmailContent customerEmailContent);
	
	public CustomerEmailContent setEmailContentForMemberActivationEmail(Long customerId,String emailType,String sendUserId,String fromName,boolean isInitial,String loginId,String password);

	public CustomerEmailContent sendRestaurantBookingLetter(Long customerId, String subject, String content) throws Exception;
	/***
	 * auto send mail
	 */
	public void autoSendMail();
}
