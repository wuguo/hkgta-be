package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.ApproveTopUpDto;
import com.sinodynamic.hkgta.dto.crm.TopUpDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.rpos.UpadeCashValueDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MemberTransactionService extends IServiceBase<MemberCashvalue>{
	
	public ResponseResult getCurrentBalance(Long customerId);
	
	public ResponseResult topupByCashCard(TopUpDto topUpDto);

	public ResponseResult getTransactionList(Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending, String clientType, AdvanceQueryDto filters);
	
	public ListPage getAllTransactionList(Long customerID,
			ListPage page ,String clientType,AdvanceQueryDto query);
	
	public ListPage getVoidPaymentList(Long customerID,
			ListPage page ,String clientType,AdvanceQueryDto query);
	
	List<AdvanceQueryConditionDto> transactionListAdvanceSearch();
	
	public ResponseResult getTopupHistory(Long customerID, String pageNumber,
			String pageSize, String sortBy, String isAscending, AdvanceQueryDto filters);

	List<AdvanceQueryConditionDto> topupHistoryAdvanceSearch();
	
	public ResponseResult topupByCard(TopUpDto topUpDto);

	public ResponseResult handlePosPaymentResult(PosResponse posResponse);
	
	public ResponseResult getMembersOverviewList(ListPage<Member> page, String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType);
	
	public List<AdvanceQueryConditionDto> assembleQueryConditions();

	public void sentTopupEmail(TopUpDto topUpDto);

	public void saveTopupHistory(TopUpDto topUpDto);

	public Data getRecentServedMemberList(ListPage<Member> page, String sortBy, String isAscending, Long[] customerId);

	public ResponseResult getTotalAmount(Long customerId, String transactionType);

	//added by Kaster 20160127
	public ResponseResult topUpForCorporate(TopUpDto topUpDto);

	//added by Kaster 20160127
	public ResponseResult approveCorporateTopup(ApproveTopUpDto approveTopUpDto);

	//added by Kaster 20160128
	public List<AdvanceQueryConditionDto> topUpSettlementAdvancedSearch();

	/**
	 * staff更新member的CashValue
	 * @param dto
	 * @return
	 */
	public ResponseResult updateMemberCashValueByStaff(UpadeCashValueDto dto);

	/**
	 * 获取加减总数
	 * @param dto
	 * @return
	 */
	public ResponseResult getMemberCashValueSum(long customerId);

	public List getAllTransactionListAdvancedSearch();
	
	public List getgetVoidPaymentListAdvancedSearch();

	public ResponseResult getLiabilityAmount(Long customerId);

	/**
	 * 获取所有member加减总数
	 * @param dto
	 * @return
	 */
	public ResponseResult getAllDebitAndCreditCashValueSum();

	/**
	 * 获取当月生日会员列表
	 * @param page
	 * @param sortBy
	 * @param isAscending
	 * @param customerId
	 * @param status
	 * @param expiry
	 * @param planName
	 * @param memberType
	 * @param month
	 * @return
	 */
	public ResponseResult getMembersOverviewListByBirthday(ListPage<Member> page, String sortBy, String isAscending,
			String customerId, String status, String expiry, String planName, String memberType, Integer month);

	/**
	 * 获取某生日会员报表
	 * @param startTime
	 * @param endTime
	 * @param location
	 * @param fileType
	 * @return
	 */
	public byte[] getMembersOverviewListByBirthdayReports(String sortBy, String isAscending, String customerId, String status,
			String expiry, String planName, String memberType, Integer month, String fileType);
		
}
