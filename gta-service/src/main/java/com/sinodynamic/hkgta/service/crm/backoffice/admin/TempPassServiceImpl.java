package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.ContractHelperDao;
import com.sinodynamic.hkgta.dao.crm.ContractHelperHistoryDao;
import com.sinodynamic.hkgta.dao.crm.GateTerminalDao;
import com.sinodynamic.hkgta.dao.crm.HelperPassTypeDao;
import com.sinodynamic.hkgta.dao.crm.InternalPermitCardDao;
import com.sinodynamic.hkgta.dao.crm.InternalPermitCardLogDao;
import com.sinodynamic.hkgta.dto.crm.AreaAccessRight;
import com.sinodynamic.hkgta.dto.crm.ContractHelperDto;
import com.sinodynamic.hkgta.dto.crm.DropDownDto;
import com.sinodynamic.hkgta.dto.crm.TempPassProfileDto;
import com.sinodynamic.hkgta.dto.crm.TempPassTypeDetailsDto;
import com.sinodynamic.hkgta.dto.crm.TempPassTypeDropDownDto;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.ContractHelperHistory;
import com.sinodynamic.hkgta.entity.crm.GateTerminal;
import com.sinodynamic.hkgta.entity.crm.HelperPassPermission;
import com.sinodynamic.hkgta.entity.crm.HelperPassType;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.ContractHelperStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.InternalPermitCardStatus;
import com.sinodynamic.hkgta.util.constant.PassportType;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
@Scope("prototype")
public class TempPassServiceImpl extends ServiceBase<InternalPermitCard> implements TempPassService
{

	private Logger logger = Logger.getLogger(TempPassServiceImpl.class);
	
	@Autowired
	private InternalPermitCardDao internalPermitCardDao;

	@Autowired
	private ContractHelperHistoryDao contractHelperHistoryDao;

	@Autowired
	private ContractHelperDao contractHelperDao;

	@Autowired
	private HelperPassTypeDao helperPassTypeDao;

	@Autowired
	private GateTerminalDao gateTerminalDao;
	
	@Autowired
	private InternalPermitCardLogDao internalPermitCardLogDao;

	@Override
	@Transactional
	public String getTempPassProfileList(String status) throws Exception {
		return internalPermitCardDao.getTempPassProfileList(status);
	}
	
	@Override
	@Transactional
	public String getTempPassProfileHistoryList(String status) throws Exception {
		return internalPermitCardDao.getTempPassProfileHistoryList(status);
	}

	/**
	 * This method is used to create new TempPassProfile
	 * 
	 * @param ContractHelperDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult createTempPassProfile(ContractHelperDto dto) throws Exception
	{
		logger.info("TempPassServiceImpl.createTempPassProfile invocation start ...");

		if (null == dto)
		{
			responseResult.initResult(GTAError.TempPassError.NO_DATA_FOUND_TO_SAVE);
			return responseResult;
		}

		if (StringUtils.isNotEmpty(dto.getPassportNo()))
		{
			if (PassportType.HKID.name().equals(dto.getPassportType()) && !CommUtil.validateHKID(dto.getPassportNo()))
			{
				responseResult.initResult(GTAError.TempPassError.INVALID_HKID);
				return responseResult;

			}
			else if (PassportType.VISA.name().equals(dto.getPassportType()) && !CommUtil.validateVISA(dto.getPassportNo()))
			{
				responseResult.initResult(GTAError.TempPassError.INVALID_VISA);
				return responseResult;

			}

			ContractHelper existingProfile = contractHelperDao.getTempPassProfileDetailsByHKID(dto.getPassportNo());
			if (null != existingProfile)
			{
				responseResult.initResult(GTAError.TempPassError.PROFILE_ALREADY_EXISTS);
				return responseResult;
			}

		}

		if (StringUtils.isEmpty(dto.getPassportType()))
		{
			responseResult.initResult(GTAError.TempPassError.IDTYPE_MANDATORY);
			return responseResult;
		}

		if (StringUtils.isEmpty(dto.getPassportNo()))
		{
			responseResult.initResult(GTAError.TempPassError.IDVALUE_MANDATORY);
			return responseResult;
		}

		if (StringUtils.isEmpty(dto.getSurname()))
		{
			responseResult.initResult(GTAError.TempPassError.SURNAME_MANDATORY);
			return responseResult;
		}

		if (StringUtils.isEmpty(dto.getGivenName()))
		{
			responseResult.initResult(GTAError.TempPassError.GIVENNAME_MANDATORY);
			return responseResult;
		}

		if (StringUtils.isEmpty(dto.getGender()))
		{
			responseResult.initResult(GTAError.TempPassError.SEX_MANDATORY);
			return responseResult;
		}
		
		if (StringUtils.isEmpty(dto.getHelperPassType()))
		{
			responseResult.initResult(GTAError.TempPassError.PASS_TYPE_MANDATORY);
			return responseResult;
		}

		// Mandatory fields from UI
		ContractHelper newProfile = new ContractHelper();
		newProfile.setCreateBy(dto.getCreateBy());
		newProfile.setUpdateBy(dto.getUpdateBy());
		newProfile.setCreateDate(new Timestamp(System.currentTimeMillis()));
		newProfile.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		newProfile.setPassportType(dto.getPassportType());
		newProfile.setPassportNo(dto.getPassportNo());
		newProfile.setSurname(dto.getSurname());
		newProfile.setGivenName(dto.getGivenName());
		newProfile.setGender(dto.getGender());
		newProfile.setPeriodFrom(DateConvertUtil.parseString2Date(dto.getPeriodFrom(), "yyyy-MM-dd"));
		newProfile.setPeriodTo(DateConvertUtil.parseString2Date(dto.getPeriodTo(), "yyyy-MM-dd"));
		newProfile.setHelperPassType(new Long(dto.getHelperPassType()));

		// Non-mandatory fields
		newProfile.setCompanyName(dto.getCompanyName());
		newProfile.setPhoneMobile(dto.getPhoneMobile());
		newProfile.setPhoneHome(dto.getPhoneHome());
		newProfile.setContactEmail(dto.getContactEmail());
		newProfile.setInternalRemark(dto.getInternalRemark());

		// Active by default
		newProfile.setStatus("ACT");

		Long contractorId = (Long) contractHelperDao.createTempPassProfile(newProfile);
		
//		contractHelperHistoryDao.createContractHelperHistory(newProfile, null, dto.getCreateBy());

		logger.debug("TempPassServiceImpl.createTempPassProfile invocation end ...");

		Map<String, Long> newId = new HashMap<String, Long>();
		newId.put("contractorId", contractorId);
		responseResult.initResult(GTAError.Success.SUCCESS, newId);
		return responseResult;

	}

	@Override
	@Transactional
	public ResponseResult getAllPassType()
	{
		List<HelperPassType> passTypeList = helperPassTypeDao.getAllPassType();

		if (null == passTypeList)
		{
			responseResult.initResult(GTAError.TempPassError.NO_HELPER_PASS_TYPE);
			return responseResult;
		}

		List<DropDownDto> dropdownlist = new ArrayList<DropDownDto>();

		for (HelperPassType passType : passTypeList)
		{
			TempPassTypeDropDownDto dropdown = new TempPassTypeDropDownDto();
			dropdown.setCodeValue(passType.getTypeId().toString());
			dropdown.setCodeDisplay(passType.getTypeName() + "  (" + DateConvertUtil.date2String(passType.getStartDate(), "dd MMM yyyy") + " to " + DateConvertUtil.date2String(passType.getExpiryDate(), "dd MMM yyyy") + " )");
			dropdown.setStartDate(passType.getStartDate());
			dropdown.setExpiryDate(passType.getExpiryDate());
			dropdownlist.add(dropdown);
		}

		responseResult.initResult(GTAError.Success.SUCCESS, dropdownlist);
		return responseResult;

	}

	/**
	 * This method is used to update the status of TempPass Profile
	 * 
	 * @param ContractHelperDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult updateProfile(ContractHelperDto dto) throws Exception {

		if (dto.getContractorId() == null) {
			responseResult.initResult(GTAError.TempPassError.PROVIDE_COTRACTOR_ID_AND_OTHER_DETAILS);
			return responseResult;
		}

		ContractHelper profile = contractHelperDao.getTempPassProfileDetails(dto.getContractorId());
		if (null == profile) {
			responseResult.initResult(GTAError.TempPassError.PROFILE_DOESNT_EXIST);
			return responseResult;
		}

		if (CommUtil.notEmpty(dto.getStatus())) {
			profile.setStatus(dto.getStatus());
			profile.setUpdateBy(dto.getUpdateBy());
			profile.setUpdateDate(dto.getUpdateDate());
		}

		if (null != dto.getCompanyName())
			profile.setCompanyName(dto.getCompanyName());
		if (null != dto.getContactEmail())
			profile.setContactEmail(dto.getContactEmail());
		if (null != dto.getGender())
			profile.setGender(dto.getGender());
		if (null != dto.getGivenName())
			profile.setGivenName(dto.getGivenName());
		boolean isLog = false;
		if (null != dto.getHelperPassType()) {
			if (!dto.getHelperPassType().equalsIgnoreCase(String.valueOf(profile.getHelperPassType()))) {
			    
			    profile.setStatus(ContractHelperStatus.ACT.name());
			    isLog = true;
			}
			profile.setHelperPassType(new Long(dto.getHelperPassType()));
		}
		if (null != dto.getInternalRemark())
			profile.setInternalRemark(dto.getInternalRemark());
		if (null != dto.getPassportNo())
			profile.setPassportNo(dto.getPassportNo());
		if (null != dto.getPassportType())
			profile.setPassportType(dto.getPassportType());
		
		Date periodFrom = null;
		Date periodTo = null;
		Date oldPeriodFrom =  null;
		Date oldPeriodTo = null;
		if (null != dto.getPeriodFrom()) {
		    	oldPeriodFrom = profile.getPeriodFrom();
			periodFrom = DateConvertUtil.parseString2Date(dto.getPeriodFrom().replace('-', '/'), "yyyy/MM/dd");
			if (null == periodFrom) {
				responseResult.initResult(GTAError.TempPassError.FAIL_UPDATE_PROFILE);
				return responseResult;
			}
			profile.setPeriodFrom(periodFrom);
		}
		if (null != dto.getPeriodTo()) {
		    	oldPeriodTo = profile.getPeriodTo();
			periodTo = DateConvertUtil.parseString2Date(dto.getPeriodTo().replace('-', '/'), "yyyy/MM/dd");
			if (null == periodTo) {
				responseResult.initResult(GTAError.TempPassError.FAIL_UPDATE_PROFILE);
				return responseResult;
			}
			profile.setPeriodTo(periodTo);
		}
		
		if (periodFrom != null && periodTo != null && (!DateConvertUtil.isSameDay(periodFrom, oldPeriodFrom) || !DateConvertUtil.isSameDay(periodTo, oldPeriodTo))) {
		    
		    isLog = true;
		    profile.setStatus(ContractHelperStatus.ACT.name());
		}
		
		if (null != dto.getPhoneHome())
			profile.setPhoneHome(dto.getPhoneHome());
		if (null != dto.getPhoneMobile())
			profile.setPhoneMobile(dto.getPhoneMobile());
		if (null != dto.getSurname())
			profile.setSurname(dto.getSurname());
		if (null != dto.getUpdateBy())
			profile.setUpdateBy(dto.getUpdateBy());
		if (null != dto.getUpdateDate())
			profile.setUpdateDate(dto.getUpdateDate());

		contractHelperDao.updateProfile(profile);
		
		List<InternalPermitCard> associatedCards = internalPermitCardDao.getTempCardContractorId(dto.getContractorId());
		InternalPermitCard card = (associatedCards == null || associatedCards.size() == 0 ? null : associatedCards.get(0));
		// add log for modification of contracthelper when pass type change
		if (isLog && card!=null) {
			
			//return old card 
			Long cardId = (card == null ? null : card.getCardId());
			
			List<ContractHelperHistory> historyList = contractHelperHistoryDao.getByCol(ContractHelperHistory.class, "permitCardId", cardId, "sysId desc");
			if(historyList!=null && historyList.size()>0)
			{
				ContractHelperHistory oldHistory = historyList.get(0);
				if(logger.isDebugEnabled())
				{
					logger.debug("old history sysId:" + oldHistory.getSysId());
				}
				
				oldHistory.setPeriodTo(new Date());
				oldHistory.setStatus(InternalPermitCardStatus.RTN.name());
//				oldHistory.setRemark("Returned");
				contractHelperHistoryDao.update(oldHistory);
			}
			//issue new card for the pass type change with same card id.
			
			ContractHelperHistory chh = new ContractHelperHistory();			
			
			chh.setContractorId(profile.getContractorId());
			chh.setCreateDate(new Date());
			chh.setStatus(InternalPermitCardStatus.ISS.name());
			chh.setHelperPassType(profile.getHelperPassType());
			chh.setPassTypeAssignBy(dto.getUpdateBy());
			chh.setPassTypeAssignDate(new Date());
			chh.setPeriodFrom(new Date());
			
			chh.setPermitCardId(cardId);
//			chh.setRemark("Issued");
			contractHelperHistoryDao.save(chh);					
//			contractHelperHistoryDao.createContractHelperHistory(profile, cardId, dto.getCreateBy());
			
		}

		// Update the status of associated card (if exists)
		if (null != card && ContractHelperStatus.NACT.name().equalsIgnoreCase(dto.getStatus())) {
			
        		String oldStatus = card.getStatus();
        		card.setStatus(InternalPermitCardStatus.DPS.name());
        		card.setUpdateBy(dto.getUpdateBy());
        		card.setStatusUpdateDate(dto.getUpdateDate());
        		internalPermitCardDao.updateCardInfo(card);
        				
        		//add internal_permit_card_log info
        		internalPermitCardLogDao.createInternalPermitCardLog(card, oldStatus);
        		
        		//add contract helper history( this table changed only log card change).
        		Long cardId = (card == null ? null : card.getCardId());    			
    			List<ContractHelperHistory> historyList = contractHelperHistoryDao.getByCol(ContractHelperHistory.class, "permitCardId", cardId, "sysId desc");
    			if(historyList!=null && historyList.size()>0)
    			{
    				ContractHelperHistory oldHistory = historyList.get(0);
    				if(logger.isDebugEnabled())
    				{
    					logger.debug("old history sysId:" + oldHistory.getSysId());
    				}
    				
    				oldHistory.setPeriodTo(new Date());
    				oldHistory.setStatus(InternalPermitCardStatus.DPS.name());
    				oldHistory.setRemark("Disposal");
    				contractHelperHistoryDao.update(oldHistory);
    			}
		}

		responseResult.initResult(GTAError.Success.SUCCESS, profile);
		return responseResult;
		
	}

	/**
	 * This method is used to update the status of TempPass Profile
	 * 
	 * @param ContractHelperDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult getProfileDetails(String contractorId)
	{

		try
		{
			if (CommUtil.notEmpty(contractorId))
			{

				ContractHelper profile = contractHelperDao.getTempPassProfileDetails(new Long(contractorId));

				if (null == profile)
					responseResult.initResult(GTAError.TempPassError.PROFILE_DOESNT_EXIST);
				else
					responseResult.initResult(GTAError.Success.SUCCESS, profile);

			}
			return responseResult;

		}
		catch (Exception e)
		{
			logger.error("TempPassServiceImpl : getProfileDetails method ", e);
			responseResult.initResult(GTAError.TempPassError.GET_PROFILE_DETAILS_FAIL);
			return responseResult;
		}
	}

	public ResponseResult listPassType()
	{
		// HelperPassTypeDao dao = new HelperPassTypeDao();
		return null;
	}

	@Override
	@Transactional
	public ResponseResult updateTempPassType(TempPassTypeDetailsDto tempPassTypeDetail,String userId)
	{
		if (null != tempPassTypeDetail.getTempPassName())
		{
			HelperPassType existingPassType = helperPassTypeDao.getUniqueByCol(HelperPassType.class, "typeName", tempPassTypeDetail.getTempPassName());
			if (null != existingPassType)
			{
				if (tempPassTypeDetail.getTempPassId().compareTo(existingPassType.getTypeId()) != 0)
				{
					responseResult.initResult(GTAError.TempPassError.TEMP_PASS_NAME_ALREADY_EXISTS);
					return responseResult;

				}

			}

		}
		HelperPassType passType = helperPassTypeDao.getById(tempPassTypeDetail.getTempPassId());

		initPassTypeData(passType, tempPassTypeDetail, userId, true);
		// helperPassTypeDao.save(passType);
		passType.getHelperPassPermission().clear();
		helperPassTypeDao.getCurrentSession().flush();
		initPassTypePermission(passType, tempPassTypeDetail, true);

		responseResult.initResult(GTAError.TempPassError.TEMP_PASS_TYPE_SAVE_SUCC, passType);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult changeTempPassStatus(TempPassTypeDetailsDto tempPassTypeDetail)
	{
		HelperPassType passType = helperPassTypeDao.getById(tempPassTypeDetail.getTempPassId());

		if (null == passType)
		{
			responseResult.initResult(GTAError.TempPassError.NO_HELPER_PASS_TYPE);
			return responseResult;
		}

		int cnt = contractHelperDao.getCountByPassType(tempPassTypeDetail.getTempPassId());

		if (cnt >= 1)
		{
			responseResult.initResult(GTAError.TempPassError.TEMP_PASS_WAS_USED);
			return responseResult;
		}

		passType.setStatus(tempPassTypeDetail.getStatus());

		responseResult.initResult(GTAError.TempPassError.CHANGE_TEMP_PASS_STATUS_SUCC);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult createTempPassType(TempPassTypeDetailsDto tempPassTypeDetail,String userId)
	{

		if (null != tempPassTypeDetail.getTempPassName())
		{
			HelperPassType existingPassType = helperPassTypeDao.getUniqueByCol(HelperPassType.class, "typeName", tempPassTypeDetail.getTempPassName());
			if (null != existingPassType)
			{
				responseResult.initResult(GTAError.TempPassError.TEMP_PASS_NAME_ALREADY_EXISTS);
				return responseResult;
			}

		}
		HelperPassType passType = new HelperPassType();
		initPassTypeData(passType, tempPassTypeDetail, userId, false);
		Long passTypeId = (Long) helperPassTypeDao.save(passType);
		initPassTypePermission(passType, tempPassTypeDetail, false);
		Map<String, Long> newId = new HashMap<String, Long>();
		newId.put("passTypeId", passTypeId);
		responseResult.initResult(GTAError.TempPassError.TEMP_PASS_TYPE_SAVE_SUCC, newId);
		return responseResult;
	}

	private void initPassTypePermission(HelperPassType passType, TempPassTypeDetailsDto tempPassTypeDetail, Boolean isUpdate)
	{
		Set<HelperPassPermission> passPermissions = new HashSet<HelperPassPermission>();

		List<AreaAccessRight> rights = tempPassTypeDetail.getAreaAccessRight();
		for (AreaAccessRight accessRight : rights)
		{
			HelperPassPermission permission = new HelperPassPermission();
			permission.setTerminalId(accessRight.getTerminalId());
			permission.setHelperPassTypeId(passType.getTypeId());
			passPermissions.add(permission);
		}

		if (isUpdate)
		{
			passType.getHelperPassPermission().addAll(passPermissions);
		}
		else
		{
			passType.setHelperPassPermission(passPermissions);
		}
	}

	private void initPassTypeData(HelperPassType passType, TempPassTypeDetailsDto tempPassTypeDetail, String userId, Boolean isUpdate)
	{
		if (isUpdate)
		{
			passType.setUpdateBy(userId);
			passType.setUpdateDate(new Date());
			passType.setStatus(tempPassTypeDetail.getStatus());
		}
		else
		{
			passType.setCreateBy(userId);
			passType.setCreateDate(new Timestamp(System.currentTimeMillis()));
			passType.setStatus("ACT");
		}

		passType.setStartDate(DateConvertUtil.parseString2Date(tempPassTypeDetail.getStartDate(), "yyyy-MM-dd"));
		passType.setExpiryDate(DateConvertUtil.parseString2Date(tempPassTypeDetail.getExpiryDate(), "yyyy-MM-dd"));
		passType.setTypeName(tempPassTypeDetail.getTempPassName());
	}

	@Override
	@Transactional
	public ResponseResult getTempPassTypeById(Long passTypeId)
	{
		HelperPassType passType = helperPassTypeDao.getById(passTypeId);
		Map<String, String> gateTerminal = getGateTerminal(getAllTerminal());
		TempPassTypeDetailsDto tempPassTypeDetail = new TempPassTypeDetailsDto(passType, gateTerminal);
		responseResult.initResult(GTAError.Success.SUCCESS, tempPassTypeDetail);
		return responseResult;
	}

	/**
	 * This method is to delete a TempPass type
	 * 
	 * @param passTypeId
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult deleteTempPassTypeById(Long passTypeId)
	{

		// No temp pass found with given Id
		HelperPassType passType = helperPassTypeDao.getById(passTypeId);
		if (null == passType)
		{
			responseResult.initResult(GTAError.TempPassError.NO_HELPER_PASS_TYPE);
			return responseResult;
		}

		// Do not delete if there is a TempCard associated.
		List<ContractHelper> profiles = contractHelperDao.getByCol(ContractHelper.class, "helperPassType", passTypeId, "updateDate desc");
		if (profiles.size() > 0)
		{
			responseResult.initResult(GTAError.TempPassError.ASSOCIATED_TEMP_CARD_EXISTS);
			return responseResult;
		}

		// Do not delete if there is history record associated.
		List<ContractHelperHistory> history = contractHelperHistoryDao.getByCol(ContractHelperHistory.class, "helperPassType", passTypeId, "createDate desc");
		if (history.size() > 0)
		{
			responseResult.initResult(GTAError.TempPassError.ASSOCIATED_HISTORY_EXISTS);
			return responseResult;

		}

		boolean isDeleted = false;
		isDeleted = helperPassTypeDao.deleteById(passTypeId);

		if (isDeleted)
		{
			responseResult.initResult(GTAError.TempPassError.DELETE_TEMP_PASS_SUCC);
			return responseResult;

		}
		else
		{
			responseResult.initResult(GTAError.TempPassError.DELETE_TEMP_PASS_FAIL);
			return responseResult;
		}

	}

	@Override
	@Transactional
	public ResponseResult cloneTempPassTypeById(Long passTypeId,String userId)
	{
		HelperPassType passType = helperPassTypeDao.getById(passTypeId);
		if (null == passType)
		{
			responseResult.initResult(GTAError.TempPassError.NO_HELPER_PASS_TYPE);
			return responseResult;
		}

		HelperPassType newPassType = new HelperPassType();
		newPassType.setCreateDate(new Timestamp(new Date().getTime()));
		newPassType.setCreateBy(userId);
		newPassType.setUpdateDate(new Date());
		newPassType.setUpdateBy(userId);
		
		HelperPassType latestone = helperPassTypeDao.getByName(CommUtil.getPrefixName(passType.getTypeName()));
		newPassType.setTypeName(CommUtil.getDuplicateName(latestone.getTypeName()));
		
		newPassType.setStartDate(passType.getStartDate());
		newPassType.setExpiryDate(passType.getExpiryDate());
		newPassType.setStatus(passType.getStatus());
		newPassType.setHelperPassPermission(new HashSet<HelperPassPermission>());

		Long newId = (Long) helperPassTypeDao.save(newPassType);
		HashSet<HelperPassPermission> permission = new HashSet<HelperPassPermission>();
		for (HelperPassPermission p : passType.getHelperPassPermission())
		{
			HelperPassPermission tempPermission = new HelperPassPermission();
			tempPermission.setHelperPassTypeId(newId);
			tempPermission.setTerminalId(p.getTerminalId());
			permission.add(tempPermission);
		}

		newPassType.getHelperPassPermission().addAll(permission);

		Map<String, Long> newPassTypeId = new HashMap<String, Long>();
		newPassTypeId.put("passTypeId", newId);
		responseResult.initResult(GTAError.TempPassError.CLONE_TEMP_PASS_SUCC, newPassTypeId);
		return responseResult;
	}

	private Map<String, String> getGateTerminal(List<GateTerminal> allTerminal)
	{
		Map<String, String> gateTerminal = new HashMap<String, String>();
		for (GateTerminal t : allTerminal)
		{
			if(logger.isDebugEnabled())
			{
				logger.debug("key:" + t.getTerminalId() + " value:" + t.getDescription());
			}
			gateTerminal.put(t.getTerminalId().toString(), t.getDescription());
		}

		return gateTerminal;
	}

	@Override
	@Transactional
	public List<GateTerminal> getAllTerminal()
	{
		return gateTerminalDao.getAllTerminal();
	}

	@Override
	@Transactional
	public ResponseResult getAllTerminalDropDown()
	{
		List<GateTerminal> terminals = gateTerminalDao.getAllTerminal();

		if (null == terminals)
		{
			responseResult.initResult(GTAError.TempPassError.NO_TERMINAL_FOUND);
			return responseResult;
		}

		List<DropDownDto> dropdownlist = new ArrayList<DropDownDto>();

		for (GateTerminal t : terminals)
		{
			DropDownDto dropdown = new DropDownDto();
			dropdown.setCodeValue(t.getTerminalId().toString());
			dropdown.setCodeDisplay(t.getDescription());
			dropdownlist.add(dropdown);
		}

		Data data = new Data();
		data.setList(dropdownlist);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Override
	public String getTempPassTypeList(String status) {
		
	    String sql = " select "
	    	+ "type_id as tempPassId, "
	    	+ "type_name as tempPassName, "
	    	+ "status, "
	    	+ "(case status when 'ACT' then 'Active' when 'NACT' then 'Inactive' when 'EXP' then 'Expired' end) as statusValue,"
	    	+ "date_format(start_date, '%Y-%m-%d') as startDate, "
	    	+ " date_format(expiry_date, '%Y-%m-%d') as expiryDate "
//	    	+ "concat(date_format(expiry_date, '%Y-%m-%d'), ' ', "
//	    	+ "case when datediff(expiry_date, curdate())>0 then concat('(', datediff(expiry_date, curdate()), ' Days Left)') "
//	    	+ "when datediff(expiry_date, curdate())<0 then concat('(', -datediff(expiry_date, curdate()), ' Days Ago)') "
//	    	+ "else '(Today)' end) as expiryDate "
	    	+ "from helper_pass_type";
	    
	    if (!StringUtils.isEmpty(status)) {
		sql = sql + " where status = '" + status + "'";
	    }
		
	    return "select * from (" + sql + ") t";
	}

	
	@Override
	@Transactional
	public ResponseResult linkCard(TempPassProfileDto dto) throws Exception {

		// Check for mandatory parameters
		if (null == dto.getContractorId()) {
			responseResult.initResult(GTAError.TempPassError.CONTRACTORID_MANDATORY);
			return responseResult;
		}

		// Check for mandatory parameters
		if (null == dto.getCardId()) {
			responseResult.initResult(GTAError.TempPassError.CARDNO_MANDATORY);
			return responseResult;
		}

		// Check the profile exists or not
		ContractHelper profile = contractHelperDao.getUniqueByCol(ContractHelper.class, "contractorId", new Long(dto.getContractorId().longValue()));
		if (null == profile) {
			responseResult.initResult(GTAError.TempPassError.PROFILE_DOESNT_EXIST);
			return responseResult;
		}

		// Not allowed to replace card if profile is in-active
		if (!ContractHelperStatus.ACT.name().equals(profile.getStatus())) {
			responseResult.initResult(GTAError.TempPassError.PROFILE_INACTIVE);
			return responseResult;
		}

		// Check new card
		String cardIdStr = dto.getCardId();
		Long cardId = Long.parseLong(CommUtil.cardNoTransfer(cardIdStr));
		InternalPermitCard newCard = internalPermitCardDao.getCardById(cardId);
		if (null == newCard) {
			responseResult.initResult(GTAError.TempPassError.COULD_NOT_FIND_CARD);
			return responseResult;
		}
		
		// Check new card
		if (null != newCard.getContractorId() && InternalPermitCardStatus.ISS.name().equals(newCard.getStatus())) {
			responseResult.initResult(GTAError.TempPassError.COULD_NOT_LINK_WITH_ACTIVE_CARD);
			return responseResult;
		}
		
		// statusUpdateDate
		Date now = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.add(Calendar.SECOND, 1);
		Date dateAdd = calendar.getTime();

		// Check if this operation is replace card
		List<InternalPermitCard> oldCards = internalPermitCardDao.getTempCardContractorId(new Long(dto.getContractorId().toString()));
		if (null != oldCards && oldCards.size() > 0) {
			
			// replace card case
		    	InternalPermitCard oldCard = oldCards.get(0);
			if (oldCard.getCardId().compareTo(new Long(cardId.longValue())) == 0){
				responseResult.initResult(GTAError.TempPassError.TRYING_REPLACE_WITH_SAME_CARD);
				return responseResult;
			}
			
			// Update the current/ Original card details
			oldCard.setContractorId(null);
			String oldCardStatus = oldCard.getStatus();
			oldCard.setStatus(InternalPermitCardStatus.DPS.name());
			oldCard.setStatusUpdateDate(now);
			oldCard.setUpdateBy(dto.getUpdateBy());
			internalPermitCardDao.updateCardInfo(oldCard);
			//log card update info
			internalPermitCardLogDao.createInternalPermitCardLog(oldCard, oldCardStatus);
			
			newCard.setStatusUpdateDate(dateAdd);
			newCard.setUpdateBy(dto.getUpdateBy());
		} 

		// Update new Card with Temp Pass Profile Id
		String newCardStatus = newCard.getStatus();
		newCard.setStatus(InternalPermitCardStatus.ISS.name());
		newCard.setUpdateBy(dto.getUpdateBy());
		newCard.setStatusUpdateDate(dateAdd);
		newCard.setContractorId(new Long(dto.getContractorId()));
		newCard.setEffectiveDate(profile.getPeriodFrom());
		newCard.setExpiryDate(profile.getPeriodTo());
		internalPermitCardDao.updateCardInfo(newCard);
		//log card update info
		internalPermitCardLogDao.createInternalPermitCardLog(newCard, newCardStatus);
		
		//update contract helper history info
//		contractHelperHistoryDao.updateContractHelperHistory(profile, cardId);
		
		//update logic, only log the card change.
		ContractHelperHistory chh = new ContractHelperHistory();			
		
		chh.setContractorId(profile.getContractorId());
		chh.setCreateDate(new Date());
		chh.setStatus(InternalPermitCardStatus.ISS.name());
		chh.setHelperPassType(profile.getHelperPassType());
		chh.setPassTypeAssignBy(dto.getUpdateBy());
		chh.setPassTypeAssignDate(new Date());
		chh.setPeriodFrom(new Date());
		
		chh.setPermitCardId(cardId);
//		chh.setRemark("Issued");
		contractHelperHistoryDao.save(chh);		
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;

	}

	/**
	 * This method is to replace contractor's current card with a new card
	 * 
	 * @param TempPassProfileDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult replaceCard(TempPassProfileDto dto)
	{
		if (null == dto.getContractorId())
		{
			responseResult.initResult(GTAError.TempPassError.CONTRACTORID_MANDATORY);
			return responseResult;
		}

		if (null == dto.getCardId())
		{
			responseResult.initResult(GTAError.TempPassError.CARDNO_MANDATORY);
			return responseResult;
		}

		try
		{
			// Check if the Temp Pass profile exists or not
			ContractHelper profile = contractHelperDao.getTempPassProfileDetails(new Long(dto.getContractorId().longValue()));
			if (null == profile)
			{
				responseResult.initResult(GTAError.TempPassError.PROFILE_DOESNT_EXIST);
				return responseResult;
			}

			// Not allowed to replace card if profile is in-active
			if (!profile.getStatus().equals("ACT"))
			{
				responseResult.initResult(GTAError.TempPassError.PROFILE_INACTIVE);
				return responseResult;
			}

			// Get the card currently associated
			List<InternalPermitCard> associatedCards = internalPermitCardDao.getTempCardContractorId(new Long(dto.getContractorId()));
			if (null == associatedCards || associatedCards.size() == 0)
			{
				responseResult.initResult(GTAError.TempPassError.NO_CARD_ASSOCIATED_CURRENTLY);
				return responseResult;
			}

			// Check if the same card scanned by mistake
			InternalPermitCard originCard = associatedCards.get(0);
			String cardIdStr = dto.getCardId();
			Long cardId = Long.parseLong(CommUtil.cardNoTransfer(cardIdStr));
			if (originCard.getCardId().compareTo(new Long(cardId.longValue())) == 0)
			{
				responseResult.initResult(GTAError.TempPassError.TRYING_REPLACE_WITH_SAME_CARD);
				return responseResult;

			}

			// Get the details of new Card to be associated
			InternalPermitCard newCard = internalPermitCardDao.getCardById(cardId);
			if (null == newCard)
			{
				// responseResult.initResult(GTAError.TempPassError.COULD_NOT_FIND_CARD,
				// new Object[]{dto.getCardId()});
				responseResult.initResult(GTAError.TempPassError.COULD_NOT_FIND_CARD);
				return responseResult;
			}

			// Check if the new card to be associated is active and used by
			// others
			if (null != newCard.getContractorId() && null != newCard.getStatus())
			{
				ContractHelper associatedProfile = contractHelperDao.getUniqueByCol(ContractHelper.class, "contractorId", newCard.getContractorId());
				if (associatedProfile.getStatus().equals("ACT") && newCard.getStatus().equals("ACT"))
				{
					responseResult.initResult(GTAError.TempPassError.COULD_NOT_LINK_WITH_ACTIVE_CARD);
					return responseResult;
				}

			}

			// to distinct the oldCard's statusUpdateDate and newCard's
			// statusUpdateDate
			Date now = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(now);
			calendar.add(Calendar.SECOND, 1);
			Date dateAdd = calendar.getTime();

			// Log history with original card details for future reference
			ContractHelperHistory history = new ContractHelperHistory();
			history.setContractorId(new Long(dto.getContractorId().longValue()));
			history.setCreateDate(new Date());
			history.setPermitCardId(originCard.getCardId());
			history.setHelperPassType(profile.getHelperPassType());
			history.setPeriodFrom(profile.getPeriodFrom());
			history.setPeriodTo(profile.getPeriodTo());
			contractHelperHistoryDao.save(history);

			// Update the current/ Original card details
			originCard.setContractorId(null);
			originCard.setStatus("NACT");
			originCard.setStatusUpdateDate(now);
			originCard.setUpdateBy(dto.getUpdateBy());

			// Update new Card with Temp Pass Profile Id
			newCard.setStatus("ACT");
			newCard.setUpdateBy(dto.getUpdateBy());
			newCard.setStatusUpdateDate(dateAdd);
			newCard.setContractorId(new Long(dto.getContractorId()));
			newCard.setEffectiveDate(profile.getPeriodFrom());
			newCard.setExpiryDate(profile.getPeriodTo());

			responseResult.initResult(GTAError.Success.SUCCESS, newCard);
			return responseResult;

		}
		catch (Exception e)
		{
			logger.error("TempPassServiceImpl : replaceCard method ", e);
			responseResult.initResult(GTAError.TempPassError.FAIL_REPLACE_CARD);
			return responseResult;
		}

	}

	/**
	 * This method is used to update the status of TempPass Profile
	 * 
	 * @param ContractHelperDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@Override
	@Transactional
	public ResponseResult getProfileDetailsByHKID(String HKId)
	{

		try
		{
			if (CommUtil.notEmpty(HKId))
			{

				ContractHelper profile = contractHelperDao.getTempPassProfileDetailsByHKID(HKId);

				if (null == profile)
					responseResult.initResult(GTAError.Success.SUCCESS);
				else
					responseResult.initResult(GTAError.Success.SUCCESS, profile);

			}
			return responseResult;

		}
		catch (Exception e)
		{
			logger.error("TempPassServiceImpl : getProfileDetails method ", e);
			responseResult.initResult(GTAError.TempPassError.GET_PROFILE_DETAILS_BY_HKID_FAIL);
			return responseResult;
		}
	}
	
	
	@Override
	@Transactional
	public ResponseResult returnInternalPermitCard(String cardId, String updateBy, String contractorId) throws Exception {
		String cardNo=null;
		try{
			cardNo = CommUtil.cardNoTransfer(cardId);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.TempPassError.CARDNO_INVALID);
			return responseResult;
		}
		
		InternalPermitCard card = internalPermitCardDao.getCardById(Long.parseLong(cardNo));
		if (card == null) {
			responseResult.initResult(GTAError.TempPassError.COULD_NOT_FIND_CARD);
			return responseResult;
		}
		
		if(card.getContractorId() == null || StringUtils.isEmpty(card.getContractorId().toString()))
		{
			responseResult.initResult(GTAError.TempPassError.CRAD_NOT_LINKED);
			return responseResult;
		}
		
		if (!StringUtils.isEmpty(contractorId) && !contractorId.equalsIgnoreCase(String.valueOf(card.getContractorId()))) {
			responseResult.initResult(GTAError.TempPassError.ERROR_CHOOSE_MEMBER_FOR_CARD);
			return responseResult;
		}
	
		String oldStatus = card.getStatus();
		if (InternalPermitCardStatus.RTN.name().equals(oldStatus)) {
		    
			responseResult.initResult(GTAError.TempPassError.CARD_HAS_BEEN_RETURN);
			return responseResult;
		}
		card.setStatus(InternalPermitCardStatus.RTN.name());
		card.setStatusUpdateDate(new Date());
		card.setUpdateBy(updateBy);
		internalPermitCardDao.updateCardInfo(card);
		
		//add log info
		internalPermitCardLogDao.createInternalPermitCardLog(card, oldStatus);
		
		//add contract helper histry (this table changed to log card change).		
		if(logger.isDebugEnabled())
		{
			logger.debug("cardId:" + Long.valueOf(cardNo));
		}
		List<ContractHelperHistory> historyList = contractHelperHistoryDao.getByCol(ContractHelperHistory.class, "permitCardId", Long.valueOf(cardNo), "sysId desc");
				
		if(historyList!=null && historyList.size()>0)
		{
			ContractHelperHistory oldHistory = historyList.get(0);
			if(logger.isDebugEnabled())
			{
				logger.debug("old history sysId:" + oldHistory.getSysId());
			}
			
			oldHistory.setPeriodTo(new Date());
			oldHistory.setStatus(InternalPermitCardStatus.RTN.name());
//			oldHistory.setRemark("Returned");
			contractHelperHistoryDao.update(oldHistory);
			
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;

	}
	

	@Override
	@Transactional
	public List<ContractHelper> getTempPassProfilesExpiringToday(LocalDate date)
	{
		return contractHelperDao.getTempPassProfilesExpiringToday(date);
	}

	@Override
	@Transactional
	public void updateStatusForExpiredTempPass() throws Exception {

		logger.info("TempPass status update job start...");
		LocalDate date = (new LocalDate()).plusDays(-1);
		List<ContractHelper> expiringProfiles = contractHelperDao.getTempPassProfilesExpiringToday(date);
		logger.info("TempPassProfilesExpiringToday size:"+(null==expiringProfiles?0:expiringProfiles.size()));
		for (ContractHelper contractHelper : expiringProfiles)
		{
			logger.info("update ContractHelper id:"+contractHelper.getContractorId()+" status "+contractHelper.getStatus()+" to "+ContractHelperStatus.NACT.name());
			contractHelper.setStatus(ContractHelperStatus.NACT.name());
			contractHelper.setUpdateBy("ADMIN");
			contractHelper.setUpdateDate(new Date());
			contractHelperDao.update(contractHelper);

			List<InternalPermitCard> associatedCards = internalPermitCardDao.getTempCardContractorId(contractHelper.getContractorId());
			
			//log contract helper history info
			if (associatedCards != null && associatedCards.size() > 0) {
		      for (InternalPermitCard card : associatedCards) {
					String oldStatus = card.getStatus();
					card.setStatus(InternalPermitCardStatus.DPS.name());
					card.setUpdateBy("ADMIN");
					card.setStatusUpdateDate(new Date());
					internalPermitCardDao.update(card);
					
					//log internal permit card info
					internalPermitCardLogDao.createInternalPermitCardLog(card, oldStatus);
			    }
			}
		}
		
		logger.debug("TempPass status update job completed...");
	}

	
	@Override
	@Transactional
	public ResponseResult disposalInternalPermitCard(String cardId, String updateBy) throws Exception {
	    
		InternalPermitCard card = internalPermitCardDao.getCardById(Long.parseLong(cardId));
		if (card == null) {
			responseResult.initResult(GTAError.TempPassError.COULD_NOT_FIND_CARD);
			return responseResult;
		}
	
		String oldStatus = card.getStatus();
		if (!InternalPermitCardStatus.ISS.name().equals(oldStatus)) {
		    
			responseResult.initResult(GTAError.TempPassError.NOT_ISSUED_CARD);
			return responseResult;
		}
		
		card.setStatus(InternalPermitCardStatus.DPS.name());
		card.setStatusUpdateDate(new Date());
		card.setUpdateBy(updateBy);
		internalPermitCardDao.updateCardInfo(card);
		
		//add log info
		internalPermitCardLogDao.createInternalPermitCardLog(card, oldStatus);
		
		//add contract helper histry (this table changed to log card change).				
		List<ContractHelperHistory> historyList = contractHelperHistoryDao.getByCol(ContractHelperHistory.class, "permitCardId", Long.valueOf(cardId), "sysId desc");
		if (historyList != null && historyList.size() > 0) 
		{
			ContractHelperHistory oldHistory = historyList.get(0);
			if (logger.isDebugEnabled()) {
				logger.debug("old history sysId:" + oldHistory.getSysId());
			}

			oldHistory.setPeriodTo(new Date());
			oldHistory.setStatus(InternalPermitCardStatus.DPS.name());
			oldHistory.setRemark("Disposal");
			contractHelperHistoryDao.update(oldHistory);
		}
				
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
}
