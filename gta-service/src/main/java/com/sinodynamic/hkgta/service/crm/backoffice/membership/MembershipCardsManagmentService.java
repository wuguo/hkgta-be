package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * @author Junfeng_Yan
 * @since May 11 2015
 */
public interface MembershipCardsManagmentService extends IServiceBase { 
	
	/**
	 * @author Junfeng_Yan
	 * @param cardNo
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public String linkCardForMember(String cardNo, Long customerId, String updateBy) throws Exception;
	
	/**
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public ResponseResult getMemberCardPrintInfo(Long customerId) throws Exception;
	
	/**
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public byte[] getAcademyCardPrintInfo(Long customerId) throws Exception;


	/**
	 * @author Vineela_Jyothi
	 * @param page
	 * @param dto
	 * @return
	 */
	public String getCards(String status);
	
	public boolean hasIssuedCard(Long customerId);

	boolean orderHasbeenCanceled(Long orderNo);
	
	
}
