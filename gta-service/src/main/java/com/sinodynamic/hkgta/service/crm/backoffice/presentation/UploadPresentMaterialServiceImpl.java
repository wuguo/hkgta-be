package com.sinodynamic.hkgta.service.crm.backoffice.presentation;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;

import net.coobird.thumbnailator.Thumbnailator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.dao.crm.PresentMaterialDao;
import com.sinodynamic.hkgta.dao.crm.ServerFolderMappingDao;
import com.sinodynamic.hkgta.dto.crm.FolderInfoDto;
import com.sinodynamic.hkgta.dto.crm.PresentMaterialDto;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.NoResultCallBack;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.ErrorCodeException;
/*
 * import com.sun.image.codec.jpeg.JPEGCodec; import com.sun.image.codec.jpeg.JPEGImageEncoder;
 */
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

@SuppressWarnings("restriction")
@Service
public class UploadPresentMaterialServiceImpl extends ServiceBase<PresentMaterial> implements UploadPresentMaterialService, ApplicationContextAware {
	protected final Logger	logger						= Logger.getLogger(UploadPresentMaterialServiceImpl.class);

	@Autowired
	PresentMaterialDao		presentMaterialDao;
	@Autowired
	ServerFolderMappingDao	serverFolderMappingDao;

	// WebApplicationContext applicationContext;
	ApplicationContext		applicationContext;

	static String			THUMBNAIL_SUFFIX			= "_small";
	static String			VIDEO_THUMBNAIL_SUFFIX_EXT	= "_small.jpeg";
	static String			JPEG_EXT					= ".jpeg";
	static String[]			EXTENSIONS					= { ".jpg", ".png", ".m4v", ".mp4", ".mov", ".avi", ".pdf" };

	@Transactional
	@Override
	public void uploadPicture(PresentMaterial presentMaterial) {
		this.presentMaterialDao.save(presentMaterial);

	}

	@Transactional
	@Override
	public void deleteMedia(Long[] ids, String userId) {
		try {
			for (long id : ids) {
				PresentMaterial presentMaterial = this.presentMaterialDao.get(PresentMaterial.class, id);
				presentMaterial.setStatus("DEL");
				presentMaterial.setUpdateBy(userId);
				presentMaterial.setUpdateDate(new Timestamp(System.currentTimeMillis()));
				this.presentMaterialDao.saveOrUpdate(presentMaterial);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(this.getI18nMessge("fail.delete.media", Locale.ROOT));
		}
	}

	@Override
	@Transactional
	public void recoverMedia(Long[] ids, String userId) {
		try {
			for (long id : ids) {
				PresentMaterial presentMaterial = this.presentMaterialDao.get(PresentMaterial.class, id);
				presentMaterial.setStatus("ACT");
				presentMaterial.setUpdateBy(userId);
				presentMaterial.setUpdateDate(new Timestamp(System.currentTimeMillis()));
				this.presentMaterialDao.saveOrUpdate(presentMaterial);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(this.getI18nMessge("fail.recover.media", Locale.ROOT));
		}
	}

	@Transactional
	@Override
	public FolderInfoDto viewFolder(Long folderId, String terminal) {
		try {
			ServerFolderMapping serverFolderMapping = null;
			FolderInfoDto folderInfoDto = new FolderInfoDto();
			folderInfoDto.setFolderId(folderId);
			String hql = null;
			List<Serializable> param = new ArrayList<Serializable>();
			if (folderId == null) {
				serverFolderMapping = new ServerFolderMapping();
				hql = "FROM PresentMaterial p where p.status = ? and p.materialType in (?,?)";
				param.add("DEL");
				param.add(MEDIA_IMAGE);
				param.add(MEDIA_VIDEO);
			} else {
				serverFolderMapping = this.serverFolderMappingDao.findById(folderId);
				hql = "FROM PresentMaterial p where p.folderId =? and p.status = ? and p.materialType in (?,?)";
				param.add(folderId);
				param.add("ACT");
				param.add(MEDIA_IMAGE);
				param.add(MEDIA_VIDEO);
				folderInfoDto.setFolderName(serverFolderMapping.getDisplayName());
				String fullPath = this.appProps.getProperty(serverFolderMapping.getPathPrefixKey());
				folderInfoDto.setFolderParentPath(fullPath.substring(fullPath.lastIndexOf("/") + 1));
			}
			List<PresentMaterial> presentMaterials = this.presentMaterialDao.getByHql(hql, param);
			List<PresentMaterialDto> presentMaterialDtos = new ArrayList<PresentMaterialDto>();
			for (PresentMaterial pm : presentMaterials) {
				PresentMaterialDto dto = new PresentMaterialDto();
				dto.setMaterialDesc(pm.getMaterialDesc());
				dto.setMaterialId(pm.getMaterialId());
				String name = pm.getMaterialFilename();
				dto.setMaterialFilename(name);
				String fileSuffix = name.substring(name.lastIndexOf("."));
				String thumbnailName = name.substring(0, name.lastIndexOf(".")) + THUMBNAIL_SUFFIX;
				dto.setThumbnail(thumbnailName + fileSuffix);

				String materialType = pm.getMaterialType();
				if (MEDIA_IMAGE.equals(materialType)) {
					dto.setThumbnail(thumbnailName + fileSuffix);
					dto.setMaterialType(MEDIA_IMAGE);
				}

				if (MEDIA_VIDEO.equals(materialType)) {
					dto.setThumbnail(thumbnailName + JPEG_EXT);
					dto.setMaterialType(MEDIA_VIDEO);
				}

				dto.setThumbnailURL("presentation/get_thumbnail?media_type=" + materialType + "&material_id=" + pm.getMaterialId());
				if (StringUtils.equalsIgnoreCase("pc", terminal)) {
					dto.setMaterialURL("presentation/get_media4pc?media_type=" + materialType + "&material_id=" + pm.getMaterialId());
				} else {
					dto.setMaterialURL("presentation/get_media?media_type=" + materialType + "&material_id=" + pm.getMaterialId());
				}

				presentMaterialDtos.add(dto);
			}
			folderInfoDto.setPresentMaterialDtos(presentMaterialDtos);
			return folderInfoDto;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(this.getI18nMessge("fail.view.folder", Locale.ROOT));
		}

	}

	@Override
	@Transactional
	public void updateDescription(Long materialId, String description, String userId) {
		try {
			PresentMaterial material = presentMaterialDao.get(PresentMaterial.class, materialId);
			material.setMaterialDesc(description);
			material.setUpdateBy(userId);
			material.setUpdateDate(new Date());
			presentMaterialDao.save(material);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(this.getI18nMessge("fail.update.ppt_desc", Locale.ROOT));
		}

	}

	@Override
	@Transactional
	public byte[] getMediaAsByteStream(Long materialId, String mediaType, boolean isThumbnail) {
		try {
			PresentMaterial material = presentMaterialDao.get(PresentMaterial.class, materialId);
			ServerFolderMapping mapping = serverFolderMappingDao.findById(material.getFolderId());

			String root = appProps.getProperty(mapping.getPathPrefixKey());

			String materialName = material.getMaterialFilename();
			if (isThumbnail) { // ONLY THUMBNAIL USE MEDIA TYPE
				String fileSuffix = materialName.substring(materialName.lastIndexOf("."));
				String thumbnailName = materialName.substring(0, materialName.lastIndexOf(".")) + THUMBNAIL_SUFFIX;

				if (MEDIA_IMAGE.equals(mediaType))
					materialName = thumbnailName + fileSuffix;
				if (MEDIA_VIDEO.equals(mediaType))
					materialName = thumbnailName + JPEG_EXT;
			}

			File file = new File(root, new File(mapping.getFolderId() + "", materialName).getPath());
			return IOUtils.toByteArray(new FileInputStream(file));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(this.getI18nMessge("fail.load.media", Locale.ROOT));
		}

	}

	void uploadMedia(Long folderId, String materialDesc, MultipartFile uploadedFile, String mediaType, String filePrefix, int index, String userId) {
		try {
			ServerFolderMapping serverFolderMapping = serverFolderMappingDao.findById(folderId);

			String pathKey = serverFolderMapping.getPathPrefixKey();
			// String path = this.appProps.getProperty(pathKey) + File.separator +serverFolderMapping.getDisplayName();
			String path = this.appProps.getProperty(pathKey) + File.separator + folderId;
			File file = new File(path);
			if (!file.exists() && !file.isDirectory()) {
				file.mkdir();
			}

			PresentMaterial presentMaterial = new PresentMaterial();
			presentMaterial.setFolderId(folderId);
			presentMaterial.setStatus("ACT");
			presentMaterial.setCreateBy(userId);
			presentMaterial.setUpdateBy(userId);
			presentMaterial.setMaterialDesc(materialDesc);
			presentMaterial.setCreateDate(new Timestamp(System.currentTimeMillis()));
			presentMaterial.setUpdateDate(new Timestamp(System.currentTimeMillis()));

			int width = Integer.valueOf(this.appProps.getProperty("thumbnail.length"));
			int length = Integer.valueOf(this.appProps.getProperty("thumbnail.width"));
			String contentType = uploadedFile.getContentType();

			boolean isImage = contentType.toLowerCase().indexOf("image") != -1;
			boolean isVideo = contentType.toLowerCase().indexOf("video") != -1;

			String fileName = uploadedFile.getOriginalFilename();
			if (fileName != null && fileName.length() > 0) {
				String fileSuffix = fileName.substring(fileName.lastIndexOf("."));

				if (!Arrays.asList(EXTENSIONS).contains(fileSuffix.toLowerCase())) {
					throw new ErrorCodeException(GTAError.PresentationError.UNSUPPORTED_MEDIA, new String[] { fileSuffix });
				}

				filePrefix = filePrefix + "_" + index + "_" + userId;
				// SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
				// String timeStr = sdf.format(new Date(System.currentTimeMillis()));
				// TODO need to get userId from session
				// timeStr = timeStr+"_"+userId;

				// process PDF file to image
				if (fileSuffix.toLowerCase().endsWith("pdf")) {
					uploadPDF(uploadedFile, filePrefix, folderId, userId, path, materialDesc, width, length);
				} else {
					String materialFilename = filePrefix + fileSuffix;
					File targetFile = new File(path, materialFilename);
					uploadedFile.transferTo(targetFile);

					if (isImage) {
						// indicate what type this upload file is. It can be picture,pdf,...
						presentMaterial.setMaterialType(MEDIA_IMAGE);
						String thumbnailName = filePrefix + THUMBNAIL_SUFFIX + fileSuffix;
						File thumbnailFile = new File(path, thumbnailName);
						Thumbnailator.createThumbnail(targetFile, thumbnailFile, length, width);
					}
					if (isVideo) {
						presentMaterial.setMaterialType(MEDIA_VIDEO);
						boolean isWindows = System.getProperty("os.name").toLowerCase().lastIndexOf("windows") != -1;

						String ffmpeg = null;
						if (isWindows)
							// ffmpeg = applicationContext.getServletContext().getRealPath("/bin/ffmpeg/wins/ffmpeg");
							ffmpeg = "ffmpeg.exe";
						else {
							// ffmpeg = applicationContext.getServletContext().getRealPath("/bin/ffmpeg/unix/ffmpeg");
							ffmpeg = "ffmpeg";
							// String[] chmod = new String[]{
							// "chmod","777",ffmpeg
							// };
							// ProcessBuilder chmodPB = new ProcessBuilder(chmod);
							// chmodPB.start().waitFor();
						}

						String thumbnailName = filePrefix + VIDEO_THUMBNAIL_SUFFIX_EXT;
						File thumbnailFile = new File(path, thumbnailName);
						String[] cmd = new String[] { ffmpeg, "-i", targetFile.getAbsolutePath(), "-ss", "00:00:01", "-f", "image2", "-vframes", "1",
								thumbnailFile.getAbsolutePath() };

						ProcessBuilder pb = new ProcessBuilder(cmd);
						pb.start().waitFor();

						Thumbnailator.createThumbnail(thumbnailFile, thumbnailFile, length, width);
					}

					presentMaterial.setMaterialFilename(materialFilename);
					this.presentMaterialDao.save(presentMaterial);
				}
			}
		} catch (ErrorCodeException errorCode) {
			logger.error(errorCode.getMessage(), errorCode);
			throw errorCode;
		} catch (Exception e) {// All other exceptions
			logger.error(e.getMessage(), e);
			throw new ErrorCodeException(GTAError.PresentationError.FAIL_UPLOAD_MEDIA);
		}
	}

	/**
	 * PDF file convert to image
	 * 
	 * @param uploadedFile
	 * @param filePrefix
	 * @param fileSuffix
	 */
	@SuppressWarnings({ "resource" })
	public void uploadPDF(MultipartFile uploadedFile, String filePrefix, Long folderId, String userId, String path, String materialDesc, int width,
			int length) {
		FileChannel channel=null;
		try {
			File file = new File(uploadedFile.getOriginalFilename());
			uploadedFile.transferTo(file);
			channel = new RandomAccessFile(file, "r").getChannel();
			ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
			PDFFile pdffile = new PDFFile(buf);
			for (int i = 1; i <= pdffile.getNumPages(); i++) {
				// draw the first page to an image
				PDFPage page = pdffile.getPage(i);
				// get the width and height for the doc at the default zoom
				Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(), (int) page.getBBox().getHeight());
				// generate the image
				Image img = page.getImage(rect.width, rect.height, rect, null, true, true);
				BufferedImage image = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
				image.getGraphics().drawImage(img, 0, 0, rect.width, rect.height, null);
				String materialFilename = filePrefix + "_PDF_" + i + JPEG_EXT;
				String targetPath = path + File.separator + materialFilename;
				FileOutputStream out = new FileOutputStream(targetPath);
				// JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
				// encoder.encode(tag);
				ImageIO.write(image, "jpeg", out);
				out.close();

				PresentMaterial presentMaterial = new PresentMaterial();
				presentMaterial.setMaterialType(MEDIA_IMAGE);
				String thumbnailName = materialFilename.replace(JPEG_EXT, THUMBNAIL_SUFFIX + JPEG_EXT);
				File thumbnailFile = new File(path, thumbnailName);
				Thumbnailator.createThumbnail(new File(targetPath), thumbnailFile, length, width);

				presentMaterial.setFolderId(folderId);
				presentMaterial.setStatus(Constant.Status.ACT.name());
				presentMaterial.setCreateBy(userId);
				presentMaterial.setUpdateBy(userId);
				presentMaterial.setMaterialDesc(materialDesc);
				presentMaterial.setCreateDate(new Timestamp(System.currentTimeMillis()));
				presentMaterial.setUpdateDate(new Timestamp(System.currentTimeMillis()));
				presentMaterial.setMaterialFilename(materialFilename);
				presentMaterialDao.save(presentMaterial);
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}finally {
			if(null!=channel)
			{
				try {
					channel.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	@Transactional
	public void uploadMedia(Long folderId, String materialDesc, MultipartFile uploadedFile, String mediaType, String userId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String timeStr = sdf.format(new Date(System.currentTimeMillis()));
		uploadMedia(folderId, materialDesc, uploadedFile, mediaType, timeStr, 1, userId);
	}

	@Override
	@Transactional
	public void uploadMedia(final Long folderId, final String[] materialDesc, final MultipartFile[] file, final String[] mediaType,
			final String userId) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		final String timeStr = sdf.format(new Date(System.currentTimeMillis()));

		CollectionUtil.loop(file, new NoResultCallBack<MultipartFile>() {
			@Override
			public void execute(MultipartFile t, int index) {
				if (materialDesc != null && materialDesc.length > 0) {
					uploadMedia(folderId, materialDesc[index], t, mediaType[index], timeStr, index, userId);
				} else {
					uploadMedia(folderId, null, t, mediaType[index], timeStr, index, userId);
				}
			}
		});

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		// this.applicationContext = (WebApplicationContext)applicationContext;
		this.applicationContext = applicationContext;
	}
}
