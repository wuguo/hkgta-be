package com.sinodynamic.hkgta.service.pms;


import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.RoomPreassignSyncDao;
import com.sinodynamic.hkgta.entity.pms.RoomPreassignSync;
import com.sinodynamic.hkgta.service.ServiceBase;


@Service
public class RoomPreassignSyncServiceImpl extends ServiceBase<RoomPreassignSync>implements RoomPreassignSyncService{

	@Autowired
	private RoomPreassignSyncDao roomPreassignSyncDao;
	
	@Override
	@Transactional
	public boolean removeAll() {
		// TODO Auto-generated method stub
		return roomPreassignSyncDao.removeAll();
	}

	@Override
	@Transactional
	public boolean saveOrUpdate(RoomPreassignSync sync) {
		// TODO Auto-generated method stub
		return roomPreassignSyncDao.saveOrUpdate(sync);
	}

	@Override
	@Transactional
	public RoomPreassignSync getRoomPreassignSyncByRoomNo(String roomNo) {
		return roomPreassignSyncDao.getUniqueByCols(RoomPreassignSync.class, new String[]{"roomNo"}, new Serializable[]{roomNo});
	}
}
