package com.sinodynamic.hkgta.service.mms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.mms.SpaAppointmentRecDao;
import com.sinodynamic.hkgta.dao.mms.SpaCenterInfoDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dto.mms.MmsOrderItemDto;
import com.sinodynamic.hkgta.dto.mms.SpaCancelledNotificationInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.entity.mms.SpaCenterInfo;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;

import net.sf.json.JSONObject;

@Service
public class SpaAppointmentRecServiceImpl extends ServiceBase<Serializable> implements SpaAppointmentRecService {
    
    @Autowired
    private SpaAppointmentRecDao spaAppointmentRecDao;
    
    @Autowired 
    private ShortMessageService shortMessageService;
    
    @Autowired 
    private UserPreferenceSettingService userPreferenceSettingService;
    
    @Autowired 
    private MessageTemplateDao messageTemplateDao;
    
    @Autowired
    private CustomerProfileDao customerProfileDao;
    
    @Autowired
    @Qualifier("asynchronizedPushService")
    private DevicePushService devicePushService;
    
    @Autowired
    private CustomerOrderHdDao customerOrderHdDao;
    
    @Autowired
    private SpaCenterInfoDao spaCenterInfoDao;

    @Override
    public String getMmsAdvanceSql(String beginDate, String endDate, String customerId, String status) throws Exception {
	
	String sql = "select * from (select "
		+ "sar.sys_id as resvId, "
		+ "coh.order_no as orderNO, "
		+ "count(1) as itemCount, "
		+ "m.academy_no as memberId, "
		+ "coh.vendor_reference_code as extInvoiceNo, "
		+ "concat(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as memberName, "
		+ "date_format(coh.order_date, '%Y-%m-%d') as orderDate, "
		+ "coh.order_total_amount as totalAmount, "
		+ "(select sum(cot.paid_amount) from customer_order_trans cot where cot.order_no = coh.order_no and cot.status = 'SUC') as totalPaid, "
		+ "coh.order_status as status, "
		+ " (case coh.order_status when 'OPN' then 'Open' when 'CAN' then 'Cancelled' when 'CMP' then 'Completed' end) as statusValue "
		+ "from customer_order_hd coh "
		+ "left join customer_profile cp on cp.customer_id = coh.customer_id "
		+ "left join member m on m.customer_id = coh.customer_id "
		+ "left join customer_order_det det ON det.order_no = coh.order_no "
		+ "left join spa_appointment_rec sar ON det.order_det_id = sar.order_det_id  AND sar.order_det_id IS NOT NULL "
		+ "where sar.ext_appointment_id is not null ";
	
	if (!StringUtils.isEmpty(customerId)) {
	    sql = sql + "and coh.customer_id = " + customerId + " ";
	}
	if (!StringUtils.isEmpty(status)) {
	    sql = sql + "and coh.order_status = '" + status + "' ";
	}
	
	if (!StringUtils.isEmpty(beginDate) && !StringUtils.isEmpty(endDate)) {
	    
	    beginDate = DateCalcUtil.formatDatetime(DateCalcUtil.getBeginDateTime(DateCalcUtil.parseDate(beginDate)));
	    endDate = DateCalcUtil.formatDatetime(DateCalcUtil.GetEndTime(DateCalcUtil.parseDate(endDate)));
	    sql = sql + "and coh.order_date between '" + beginDate + "' and '" + endDate + "' ";
	}
	
	sql = sql + "group by coh.order_no) t";

	return sql;
    }

    @Transactional
    public List<MmsOrderItemDto> getSpaAppointmentRecByExtinvoiceNo(String extInvoiceNo) {
	
	if (StringUtils.isEmpty(extInvoiceNo)) return null;
	List<MmsOrderItemDto> items = spaAppointmentRecDao.getAppointmentItemsByExtinvoiceNo(extInvoiceNo);
	return items;
    }

    @Override
    @Transactional
    public void sendSmsWhenCancelSpaOrder(String extInvoiceNo) throws Exception {
	
	MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_MMS_ORDER_CANCEL_NOTIFICATION);
	if (mt == null) return;
	
	List<SpaAppointmentRec> spaAppointmentRecs = spaAppointmentRecDao.getAppointmentRecsByInvoiceNo(extInvoiceNo);
	if (spaAppointmentRecs == null || spaAppointmentRecs.size() == 0) return;
	Long customerId = spaAppointmentRecs.get(0).getCustomerId();
	
	StringBuilder orderDetail = new StringBuilder("");
	for (SpaAppointmentRec appointment : spaAppointmentRecs) {
	    
		String onDate = DateCalcUtil.formatDate(appointment.getStartDatetime());
		String startTime = DateCalcUtil.getHourAndMinuteOfDay(appointment.getStartDatetime());
		String endTime = DateCalcUtil.getHourAndMinuteOfDay(appointment.getEndDatetime());
		String serviceName = appointment.getServiceName();
		String therapist = appointment.getTherapistFirstname() + " " + appointment.getTherapistLastname();
		orderDetail
		.append("Booking Date:").append(onDate).append(" ").append(startTime).append("-").append(endTime).append("  ")
		.append("Service Name:").append(serviceName).append("  ")
		.append("Therapist Name:").append(therapist).append("\n");
	}
	
	String content = mt.getFullContent(new String[] { orderDetail.toString() });
	CustomerProfile cp = customerProfileDao.getById(customerId);
	if (cp == null || StringUtils.isEmpty(cp.getPhoneMobile())) return;
	
	if(userPreferenceSettingService.sendSMSByCustomerId(cp.getCustomerId())){
		String mobilePhone = cp.getPhoneMobile();
		List<String> phonenumbers = new ArrayList<String>();
		phonenumbers.add(mobilePhone);
		shortMessageService.sendSMS(phonenumbers, content, new Date());
	}
	
    }

    @Override
    @Transactional
    public void sendSmsWhenCancelSpaOrderItem(String extAppointmentId) throws Exception {
	
	MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_MMS_ITEM_CANCEL_NOTIFICATION);
	if (mt == null) return;

	
	List<SpaAppointmentRec> spaAppointmentRecs = spaAppointmentRecDao.getAppointmentRecsByAppointmentId(extAppointmentId);
	if (spaAppointmentRecs == null || spaAppointmentRecs.size() == 0) return;
	SpaAppointmentRec spaAppointmentRec = spaAppointmentRecs.get(0);
	Long customerId = spaAppointmentRec.getCustomerId();
	if (customerId == null) return;
	CustomerProfile cp = customerProfileDao.getById(customerId);
	if (cp == null || StringUtils.isEmpty(cp.getPhoneMobile())) return;
	
	String[] params = new String[5];
	params[0] = DateCalcUtil.formatDate(spaAppointmentRec.getStartDatetime());
	params[1] = DateCalcUtil.getHourAndMinuteOfDay(spaAppointmentRec.getStartDatetime());
	params[2] = DateCalcUtil.getHourAndMinuteOfDay(spaAppointmentRec.getEndDatetime());
	params[3] = spaAppointmentRec.getServiceName();
	params[4] = spaAppointmentRec.getTherapistFirstname() + " " + spaAppointmentRec.getTherapistLastname();
	String content = mt.getFullContent(params);
	if(userPreferenceSettingService.sendSMSByCustomerId(cp.getCustomerId())){
		String mobilePhone = cp.getPhoneMobile();
		List<String> phonenumbers = new ArrayList<String>();
		phonenumbers.add(mobilePhone);
		shortMessageService.sendSMS(phonenumbers, content, new Date());
	}
	
	
    }

    @Override
    @Transactional
    public void spaAppointmentBeginNotification() throws Exception {
	
	List<SpaCancelledNotificationInfoDto> notificationList = spaAppointmentRecDao.getUserForSpaAppointmentBeginNotification();
	if (notificationList == null || notificationList.size() == 0) return;
	logger.info("SpaCancelledNotificationInfoDto list size:"+notificationList.size());
	
	for (SpaCancelledNotificationInfoDto dto : notificationList) {
	    String[] userIDs = { dto.getUserId() };
	    String[] params = new String[6];
	    params[0] = "1 hour";
	    params[1] = DateCalcUtil.formatDate(dto.getStartDatetime());
	    params[2] = DateCalcUtil.getHourAndMinuteOfDay(dto.getStartDatetime());
	    params[3] = DateCalcUtil.getHourAndMinuteOfDay(dto.getEndDatetime());
	    params[4] = dto.getServiceName();
	    params[5] = dto.getEmployFN() + " " + dto.getEmployLN();
	    devicePushService.pushMessage(userIDs, Constant.TEMPLATE_ID_BOOKING_START_REMINDER, params, "Member");
	    
	    logger.info("SpaCancelledNotificationInfoDto:"+JSONObject.fromObject(dto).toString());
	}
	
    }

    @Override
    @Transactional
    public void spaAppointmentEndNotification() throws Exception {
	
	List<SpaCancelledNotificationInfoDto> notificationList = spaAppointmentRecDao.getUserForSpaAppointmentEndNotification();
	if (notificationList == null || notificationList.size() == 0) return;
	
	for (SpaCancelledNotificationInfoDto dto : notificationList) {
	    
	    String[] userIDs = { dto.getUserId() };
	    String[] params = new String[6];
	    params[0] = "10 minutes";
	    params[1] = DateCalcUtil.formatDate(dto.getStartDatetime());
	    params[2] = DateCalcUtil.getHourAndMinuteOfDay(dto.getStartDatetime());
	    params[3] = DateCalcUtil.getHourAndMinuteOfDay(dto.getEndDatetime());
	    params[4] = dto.getServiceName();
	    params[5] = dto.getEmployFN() + " " + dto.getEmployLN();
	    devicePushService.pushMessage(userIDs, "booking_end_reminder", params, "Member");
	}
    }
    
    @Override
    @Transactional
    public SpaCenterInfo getSpaCenterInfo() {
	
		List<SpaCenterInfo> SpaCenterInfoList = this.spaCenterInfoDao.getByHql("From " + SpaCenterInfo.class.getName());
		if (SpaCenterInfoList != null && SpaCenterInfoList.size() != 0)
			return SpaCenterInfoList.get(0);
		return null;
		
    }
	
}
