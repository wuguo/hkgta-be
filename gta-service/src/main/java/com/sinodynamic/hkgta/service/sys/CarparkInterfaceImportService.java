package com.sinodynamic.hkgta.service.sys;

import java.io.Serializable;

import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceImport;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface CarparkInterfaceImportService extends IServiceBase<CarparkInterfaceImport> {

	/***
	 * save CarparkInterfaceImport
	 * @param carparkInterfaceImport
	 * @return
	 */
	public Serializable save(CarparkInterfaceImport carparkInterfaceImport)throws Exception;
	
	public boolean update(CarparkInterfaceImport carparkInterfaceImport)throws Exception;
	
	public boolean checkExistByFileName(String fileName)throws Exception;
}
