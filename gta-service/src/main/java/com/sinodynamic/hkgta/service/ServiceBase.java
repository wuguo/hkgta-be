package com.sinodynamic.hkgta.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.SessionMap;
import com.sinodynamic.hkgta.util.SmartDatetimeFormat;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.response.ResponseResult;
/**
 * @author refactor by Johnmax_Wang
 * @date Apr 28, 2015
 * @param <T>
 */
public class ServiceBase<T extends Serializable> implements IServiceBase<T>
{
	protected final Logger logger = Logger.getLogger(ServiceBase.class);
	
	protected CommUtil uc = new CommUtil();

	@Resource(name = "appProperties")
	protected Properties appProps;

	@Resource(name = "messageSource")
	protected MessageSource messageSource;

	@Autowired
	protected ResponseMsg responseMsg;

	@Autowired
	protected ResponseResult responseResult;
	
	@Value(value ="#{spaProperties['spa.VISTA']}")
	private String spaVISA;
	
	@Value(value ="#{spaProperties['spa.MASTER']}")
	private String spaMASTER;
	
	@Value(value ="#{spaProperties['spa.AMEX']}")
	private String spaAMEX;
	
	@Value(value ="#{spaProperties['spa.CUP']}")
	private String spaCUP;
	
	@Value(value ="#{oasisProperties['oasis.VISTA']}")
	private String oasisVISA;
	
	@Value(value ="#{oasisProperties['oasis.MASTER']}")
	private String oasisMASTER;
	
	@Value(value ="#{oasisProperties['oasis.AMEX']}")
	private String oasisAMEX;
	
	@Value(value ="#{oasisProperties['oasis.CUP']}")
	private String oasisCUP;
	
	public String getConfigPaymentCode(String type,String paymentMethod)
	{
		if(PaymentMethod.VISA.name().equalsIgnoreCase(paymentMethod)
		  ||PaymentMethod.MASTER.name().equalsIgnoreCase(paymentMethod)
		  ||PaymentMethod.AMEX.name().equalsIgnoreCase(paymentMethod)
		  ||PaymentMethod.CUP.name().equalsIgnoreCase(paymentMethod))
		{
		   if("MMS".equalsIgnoreCase(type))
		   {
			   if(PaymentMethod.VISA.name().equalsIgnoreCase(paymentMethod))
			   {
				  return spaVISA; 
			   }else if (PaymentMethod.MASTER.name().equalsIgnoreCase(paymentMethod)){
				   return spaMASTER;
			   }
			   else if (PaymentMethod.AMEX.name().equalsIgnoreCase(paymentMethod)){
				   return spaAMEX;
			   }
			   else if (PaymentMethod.CUP.name().equalsIgnoreCase(paymentMethod)){
				   return spaCUP;
			   }
		   }else if("PMS".equalsIgnoreCase(type)){
			   //PMS
			   if(PaymentMethod.VISA.name().equalsIgnoreCase(paymentMethod))
			   {
				  return oasisVISA; 
			   }else if (PaymentMethod.MASTER.name().equalsIgnoreCase(paymentMethod)){
				   return oasisMASTER;
			   }
			   else if (PaymentMethod.AMEX.name().equalsIgnoreCase(paymentMethod)){
				   return oasisAMEX;
			   }
			   else if (PaymentMethod.CUP.name().equalsIgnoreCase(paymentMethod)){
				   return oasisCUP;
			   }
		   }
		}else{
			String cardCode = null;
			if (paymentMethod.equalsIgnoreCase(PaymentMethod.JCB.name())) {
				cardCode = PaymentMethod.JCB.getCardCode();
			}else if (paymentMethod.equalsIgnoreCase(PaymentMethod.DINERSCLUB.name())) {
				cardCode = PaymentMethod.DINERSCLUB.getCardCode();
			} else if (paymentMethod.equalsIgnoreCase(PaymentMethod.CASH.name())) {
				cardCode = PaymentMethod.CASH.getCardCode();
			} else if (paymentMethod.equalsIgnoreCase(PaymentMethod.CASHVALUE.name())) {
				cardCode = PaymentMethod.CASHVALUE.getCardCode();
			}
			return cardCode;
		}
		
		return paymentMethod;
	}

	/**
	 * To customize the format for date parameter within this controller. 
	 * To avoid error when passing empty date string parameter when submit from view page  
	 * @param binder
	 */
	@InitBinder 
	public void initBinder(WebDataBinder binder)
	{
		SimpleDateFormat dateFormat = new SmartDatetimeFormat(appProps.getProperty("format.datetime"));
		dateFormat.setLenient(false);
		// true passed to CustomDateEditor constructor means convert empty
		// String to null
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	/**
	 * get i18n message
	 * 
	 * @param msgCode
	 * @return
	 */
	public String getI18nMessge(String msgCode)
	{
		return this.getI18nMessge(msgCode, null, LocaleContextHolder.getLocale());
	}

	/**
	 * get i18 message with parameters, just like {0} {1}
	 * 
	 * @param msgCode
	 * @param args
	 * @return
	 */
	public String getI18nMessge(String msgCode, Object[] args)
	{
		return this.getI18nMessge(msgCode, args, LocaleContextHolder.getLocale());
	}

	public String getI18nMessge(String msgCode, Locale locale)
	{
		return this.getI18nMessge(msgCode, null, locale);
	}

	public String getI18nMessge(String msgCode, Object[] args, Locale locale)
	{
		return messageSource.getMessage(msgCode, args, locale);
	}
	
	/**
	 * Get the current request object
	 * @return
	 */
	public static HttpServletRequest getRequest(){  
	    try{  
	        return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
	    }catch(Exception e){  
	        return null;  
	    }  
	}  
	
	/**
	 * Initial the CustomerOrderTrans object
	 * @param trans
	 * @return
	 */
	public CustomerOrderTrans initCustomerOrderTrans(CustomerOrderTrans trans){
		if(trans == null){
			trans = new CustomerOrderTrans();
		}
		String userId = (String)getRequest().getAttribute("UserId");
		String ipAddress = getRequest().getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
		    ipAddress = getRequest().getRemoteAddr();
		}
		if(ipAddress.contains(","))
		{
		    ipAddress=ipAddress.split(",")[0];
		}
		trans.setTerminalId(ipAddress);	
		trans.setPaymentLocationCode(SessionMap.getInstance().getLocation(userId));
		trans.setCreateDate(new Date());
		trans.setCreateBy(userId);
		trans.setUpdateDate(new Timestamp(new Date().getTime()));
		trans.setUpdateBy(userId);
		
		return trans;
	}
	
	/**
	 * Initial the CustomerOrderTrans object
	 * @param trans
	 * @return
	 */
	public CustomerOrderTrans initCustomerOrderTransByCopy(CustomerOrderTrans oldTrans){
		CustomerOrderTrans trans = new CustomerOrderTrans();
		BeanUtils.copyProperties(oldTrans, trans, new String[] { "transactionNo" });
		String userId = (String)getRequest().getAttribute("UserId");
		trans.setTerminalId(getRequest().getRemoteAddr());	
		trans.setPaymentLocationCode(SessionMap.getInstance().getLocation(userId));
		trans.setCreateDate(new Date());
		trans.setCreateBy(userId);
		trans.setUpdateDate(new Timestamp(new Date().getTime()));
		trans.setUpdateBy(userId);
		trans.setFromTransactionNo(oldTrans.getTransactionNo());
		trans.setTransactionTimestamp(new Date());
		return trans;
	}
	
	/**
	 * Initial the CustomerOrderTrans object 【job专用】
	 * @param trans
	 * @return
	 */
	public CustomerOrderTrans initCustomerOrderTransForCronjob(CustomerOrderTrans trans){
		if(trans == null){
			trans = new CustomerOrderTrans();
		}
		trans.setTerminalId("127.0.0.1");	
		trans.setPaymentLocationCode("SYSTEM");
		trans.setCreateDate(new Date());
		trans.setCreateBy("[SysAdmin001]");
		trans.setUpdateDate(new Timestamp(new Date().getTime()));
		trans.setUpdateBy("[SysAdmin001]");
		return trans;
	}
	
	/**
	 * 
	 * define LoginUser to convert the attribute object
	 *
	 */
	public class LoginUser extends User
	{
		
		private static final long serialVersionUID = -4565267201666023250L;
		private String userId;  
		private String defaultModule;
		private String userType;
		private String userName;
		private String fullname;
		
		public String getFullname()
		{
			return fullname;
		}

		public void setFullname(String fullname)
		{
			this.fullname = fullname;
		}

		private String staffType;
		
		public String getStaffType()
		{
			return staffType;
		}

		public void setStaffType(String staffType)
		{
			this.staffType = staffType;
		}

		public String getUserName()
		{
			return userName;
		}

		public void setUserName(String userName)
		{
			this.userName = userName;
		}

		public String getUserNo()
		{
			return userNo;
		}

		public void setUserNo(String userNo)
		{
			this.userNo = userNo;
		}

		private String userNo;
		
		public String getUserId()
		{
			return userId;
		}

		public void setUserId(String userId)
		{
			this.userId = userId;
		}

		public String getDefaultModule()
		{
			return defaultModule;
		}

		public void setDefaultModule(String defaultModule)
		{
			this.defaultModule = defaultModule;
		}

		public String getUserType()
		{
			return userType;
		}

		public void setUserType(String userType)
		{
			this.userType = userType;
		}

		public LoginUser(String userid, String password, boolean enabled,
				boolean accountNonExpired, boolean credentialsNonExpired,
				boolean accountNonLocked, Collection<GrantedAuthority> authorities) {
			super(userid, password, enabled, accountNonExpired, credentialsNonExpired,
					accountNonLocked, authorities);
		}

		public LoginUser(String userid, String password, Collection<? extends GrantedAuthority> authorities)
		{
			super(userid, password, authorities);
		}
		
	}
}
