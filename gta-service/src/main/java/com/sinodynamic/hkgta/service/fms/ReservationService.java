package com.sinodynamic.hkgta.service.fms;

public interface ReservationService {
	/***
	 * get reservation booking by monthDate 
	 * Dto:SourceBookingDto
	 * @param monthDate  2016-04
	 * @return  sql
	 */
	public StringBuilder getReserSqlByDate(String monthDate);

}
