package com.sinodynamic.hkgta.service.fms;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class MemberReportServiceImpl implements MemberReportService {
	protected final Logger logger = Logger.getLogger(MemberReportServiceImpl.class);

	@Override
	public StringBuilder getSqlByStatus(String status) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" concat('',member.customer_id) as customerID, ");
		sql.append(" concat('',member.superior_member_id) as superCustomerID, ");
		sql.append(" member.academy_no as academyID, ");
		sql.append(" customer_profile.surname AS lastName, ");
		sql.append(" customer_profile.given_name AS firstName, ");
		sql.append(" IF(customer_profile.date_of_birth  IS NULL,'', DATE_FORMAT(customer_profile.date_of_birth,'%d/%m')) AS dateOfBirth, ");
		sql.append(" IF(customer_profile.date_of_birth  IS NULL,'', DATE_FORMAT(customer_profile.date_of_birth,'%m')) AS monthOfBirth, ");
		sql.append(" customer_profile.contact_email AS email, ");
		sql.append("'' AS expiryDate, ");
		sql.append("'' AS servicePlan, ");

		//sql.append(" IF(member.relationship_code is null,'Primary','Corporate') as memberType, ");
		sql.append(" case  ");
		sql.append(" when member.member_type='IPM' OR member.member_type='CPM'  then 'Primary' ");
		sql.append(" when member.member_type='IDM' OR member.member_type='CDM'  then 'Dependent' ");
		sql.append(" END AS memberType ,");
		
		sql.append(" IF(member.member_type='IPM' or member.member_type='IDM','Individual','Corporate') as servicePlanType, ");
		
		sql.append(" concat('',customer_profile.gender) AS gender, ");
		sql.append("'False' AS hKGTADirectMarketing, ");
		sql.append("'False' AS nWDDirectMarketing ");
		sql.append("FROM ");
		sql.append("  customer_profile customer_profile, member member ");
		sql.append("WHERE ");
		sql.append(" customer_profile.customer_id = member.customer_id ");
		sql.append("  and member.status='ACT' and member.member_type<>'MG' ");

		logger.debug("sql:" + sql);
		return sql;
	}

	@Override
	public StringBuilder getServicePlanByCustID(String customerIDArray) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT concat('',ce.customer_id) as customerID , sp.plan_name AS servicePlan FROM customer_enrollment ce, ");
		sql.append("service_plan_pos pos, pos_service_item_price ps, service_plan sp WHERE pos.plan_no = sp.plan_no ");
		sql.append("AND pos.pos_item_no = ps.item_no AND ps.item_catagory = 'SRV' ");
		sql.append("AND ps.item_no LIKE 'SRV%' AND ce.subscribe_plan_no = sp.plan_no AND ce.customer_id in (" + customerIDArray + ") ");

		logger.debug("sql:" + sql);
		return sql;
	}

	@Override
	public StringBuilder getExipryDateByCustID(String customerIDArray) {
		StringBuilder sql = new StringBuilder();
		sql.append(
				"SELECT concat('',cus.customer_id) as customerID,DATE_FORMAT(acc.expiry_date,'%d/%m/%Y') AS expiryDate FROM customer_service_acc acc, customer_profile ");
		sql.append("cus where cus.customer_id=acc.customer_id  AND cus.customer_id in (" + customerIDArray + ") ");

		logger.debug("sql:" + sql);
		return sql;
	}

	@Override
	public StringBuilder getHKGTADirectMarketingByCustID(String customerIDArray) {
		StringBuilder sql = new StringBuilder();
		sql.append(
				"SELECT concat('',sys.customer_id) as customerID,sys.customer_input as customerInput FROM customer_addition_info sys where sys.caption_id=45 and sys.customer_id in ("
						+ customerIDArray + ") ");

		logger.debug("sql:" + sql);
		return sql;
	}

	@Override
	public StringBuilder getNWDDirectMarketingByCustID(String customerIDArray) {
		StringBuilder sql = new StringBuilder();
		sql.append(
				"SELECT concat('',sys.customer_id) as customerID,sys.customer_input as customerInput FROM customer_addition_info sys where sys.caption_id=46 and sys.customer_id in ("
						+ customerIDArray + ") ");

		logger.debug("sql:" + sql);
		return sql;
	}
}
