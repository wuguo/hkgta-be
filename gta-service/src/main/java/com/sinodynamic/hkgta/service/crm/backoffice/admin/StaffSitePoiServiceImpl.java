package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.StaffSitePoiDao;
import com.sinodynamic.hkgta.entity.crm.StaffSitePoiMonitor;
import com.sinodynamic.hkgta.entity.ibeacon.Ibeacon;
import com.sinodynamic.hkgta.entity.ibeacon.SitePoi;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;
import com.sinodynamic.hkgta.dao.ibeacon.IbeaconDao;
import com.sinodynamic.hkgta.dao.ibeacon.SitePoiDao;
import com.sinodynamic.hkgta.dto.crm.SitePoiDto;

@Service
public class StaffSitePoiServiceImpl extends ServiceBase<StaffSitePoiMonitor> implements StaffSitePoiService {

	@Autowired
	StaffSitePoiDao staffSitePoiDao;
	
	@Autowired
	SitePoiDao SitePoiDao;
	
	@Autowired
	private IbeaconDao ibeaconDao;
	
	@Transactional
	public void saveStaffSitePoiMonitor(StaffSitePoiMonitor staffSitePoiMonitor)
			throws GTACommonException {
		StaffSitePoiMonitor rs = staffSitePoiDao.findByStaff(staffSitePoiMonitor.getStaffUserId());
		if (rs == null){
		    staffSitePoiDao.save(staffSitePoiMonitor);
		}
		else {
			rs.setSitePoiId(staffSitePoiMonitor.getSitePoiId());
			staffSitePoiDao.update(rs);
		}
	}
	
	@Transactional
	public List<StaffSitePoiMonitor> listByUserId(String userId)
			throws GTACommonException {
		StaffSitePoiMonitor sm = staffSitePoiDao.findByStaff(userId);
		List<StaffSitePoiMonitor> rs = new LinkedList<StaffSitePoiMonitor>();
		rs.add(sm);
		return rs;
	}


	@Transactional
	public List<StaffSitePoiMonitor> listBySiteId(Long sitePoiId)
			throws GTACommonException {
		return staffSitePoiDao.listBySiteId(sitePoiId);
	}

	@Transactional
	public void deleteById(Long sysId)
			throws GTACommonException {
		staffSitePoiDao.deleteById(StaffSitePoiMonitor.class, sysId);
	}

	@Transactional
	public List<SitePoi> listLocations() throws GTACommonException {
		return SitePoiDao.listSitePois();
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public boolean deleteByBeaconId(Long ibeaconIdDB) {
		Ibeacon ibeacon = ibeaconDao.getIbeaconByPK(ibeaconIdDB);
		boolean result = true;
		if (ibeacon != null) {
			List<StaffSitePoiMonitor> monitorList = staffSitePoiDao.listBySiteId(ibeacon.getSitePoiId());
			if (monitorList != null && monitorList.size() > 0) {
				for (StaffSitePoiMonitor item : monitorList){
					result = (result && staffSitePoiDao.delete(item));
				}
			}
		}
		return result;
	}
}
