package com.sinodynamic.hkgta.service.crm.account;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.DDInterfaceFileLogDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccLogDao;
import com.sinodynamic.hkgta.dto.account.DDAFailReportDetailDto;
import com.sinodynamic.hkgta.dto.account.DDAFailReportDto;
import com.sinodynamic.hkgta.dto.account.DDASuccessReportDetailDto;
import com.sinodynamic.hkgta.dto.account.DDASuccessReportDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.DdInterfaceFileLog;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAccLog;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MandateStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.report.JasperUtil;

@Service
public class DDATaskServiceImpl extends ServiceBase implements DDATaskService {

	private Logger ddxLogger = LoggerFactory.getLogger(LoggerType.DDX.getName());

	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;
	
	@Autowired
	private MemberPaymentAccLogDao memberPaymentAccLogDao;
	
	@Autowired
	private DDInterfaceFileLogDao ddInterfaceFileLogDao;
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private GlobalParameterService globalParameterService;

	private String SCB_BANK_ID="SCB";
	

	private void readDDASuccessHeader(String[] csvItems, DDASuccessReportDto ddaSuccessReport) throws Exception {
		if (!"CURR_DDA".equals(csvItems[1]))
			throw new Exception("is not AUTH_DDA report");
		ddaSuccessReport.setSystemDate(csvItems[2]);
		ddaSuccessReport.setTransactionTime(csvItems[3]);
		ddaSuccessReport.setOriginatingNo(csvItems[4]);
		ddaSuccessReport.setCustomerName(csvItems[5]);
	}

	private void readDDASuccessDetail(String[] csvItems, DDASuccessReportDto ddaSuccessReport) {
		if(csvItems.length!=40){
			ddxLogger.error("DDA error:the RCMS_E_64 detail record format incorrect ,Separated by ',' array length must be 40 ");
			return;
		}
		DDASuccessReportDetailDto detail = new DDASuccessReportDetailDto();
		detail.setOriginatorId(csvItems[1]);
		detail.setOriginatorBankCode(csvItems[2]);
		detail.setOriginatorAccountNumber(csvItems[3]);
		detail.setDdaType(csvItems[4]);
		detail.setBuyerCode(csvItems[5]);
		detail.setBuyerName(csvItems[6]);
		detail.setDebtorBankCode(csvItems[7]);
		detail.setDebtorBankName(csvItems[8]);
		detail.setDraweeBranchCode(csvItems[9]);
		detail.setDebtorAccountNumber(csvItems[10]);
		detail.setDebtorAccountName(csvItems[11]);
		detail.setStartDate(csvItems[12]);
		detail.setEndDate(csvItems[13]);
		detail.setMandateType(csvItems[14]);
		detail.setAmount(csvItems[16]);
		detail.setRemarks(csvItems[17]);
		detail.setRejectCode(csvItems[18]);
		detail.setRejectReason(csvItems[19]);
		detail.setValueDate(csvItems[20]);
		detail.setTransactionId(csvItems[21]);
		detail.setMandateRemarks(csvItems[22]);
		detail.setDebtorReferenceNumber(csvItems[23]);
		detail.setDebtorId(csvItems[24]);
		detail.setRcmsMandateManagementId(csvItems[25]);
		detail.setReference(csvItems[26]);
		detail.setActionIndicator(csvItems[27]);
		detail.setMandateStatus(csvItems[29]);
		detail.setMandateStatusCode(csvItems[30]);
		detail.setCurrencyCode(csvItems[31]);
		detail.setDebtorIdType2(csvItems[32]);
		detail.setDebtorIdNumber2(csvItems[33]);
		detail.setRevisedReference(csvItems[39]);
		ddaSuccessReport.getDetails().add(detail);
	}

	private void readDDASuccessTail(String[] csvItems, DDASuccessReportDto ddaSuccessReport) {
		ddaSuccessReport.setNoOfRecord(Integer.parseInt(csvItems[1]));
		ddaSuccessReport.setTotalTransactionAmount(Integer.parseInt(csvItems[2]));
	}

	private void readDDAFailHeader(String[] csvItems, DDAFailReportDto ddaFailReport) {
		ddaFailReport.setSystemDate(csvItems[2]);
		ddaFailReport.setTransactionTime(csvItems[3]);
		ddaFailReport.setBankNo(csvItems[4]);
		ddaFailReport.setCustomerName(csvItems[5]);
	}

	private void readDDAFailDetail(String[] csvItems, DDAFailReportDto ddiFailReport) {
		DDAFailReportDetailDto ddaFailReportDetail = new DDAFailReportDetailDto();
		ddaFailReportDetail.setAmount(csvItems[16]);
		ddaFailReportDetail.setBuyerCode(csvItems[5]);
		ddaFailReportDetail.setBuyerName(csvItems[6]);
		ddaFailReportDetail.setDdaType(csvItems[4]);
		ddaFailReportDetail.setDebitAccountName(csvItems[11]);
		ddaFailReportDetail.setDebitAccountNumber(csvItems[10]);
		ddaFailReportDetail.setDraweeBankCode(csvItems[7]);
		ddaFailReportDetail.setDraweeBankName(csvItems[8]);
		ddaFailReportDetail.setDraweeBranchCode(csvItems[9]);
		ddaFailReportDetail.setEndDate(csvItems[13]);
		ddaFailReportDetail.setFrequency(csvItems[15]);
		ddaFailReportDetail.setMandateType(csvItems[14]);
		ddaFailReportDetail.setOriginatorAccountNumber(csvItems[3]);
		ddaFailReportDetail.setOriginatorBankCode(csvItems[2]);
		ddaFailReportDetail.setOriginatorId(csvItems[1]);
		ddaFailReportDetail.setRejectCode(csvItems[18]);
		ddaFailReportDetail.setRejectDesc(csvItems[19]);
		ddaFailReportDetail.setRemarks(csvItems[17]);
		ddaFailReportDetail.setStartDate(csvItems[12]);
		ddaFailReportDetail.setValueDate(csvItems[20]);
		ddaFailReportDetail.setMicrCode(csvItems[21]);
		ddaFailReportDetail.setDebtorId(csvItems[22]);
		ddaFailReportDetail.setReference(csvItems[23]);
		ddaFailReportDetail.setDeletionDate(csvItems[24]);
		ddaFailReportDetail.setReactivaitonDate(csvItems[25]);
		ddaFailReportDetail.setLastStatusUpdateDate(csvItems[26]);
		ddaFailReportDetail.setActionIndicator(csvItems[27]);
		ddaFailReportDetail.setResultIndicator(csvItems[28]);
		ddaFailReportDetail.setMandateStatus(csvItems[29]);
		ddaFailReportDetail.setMandateStatusCode(csvItems[30]);
		ddaFailReportDetail.setCurrencyCode(csvItems[31]);
		ddaFailReportDetail.setDebtorIdType1(csvItems[32]);
		ddaFailReportDetail.setDebtorIdNumber1(csvItems[33]);
		ddaFailReportDetail.setDebtorIdType2(csvItems[34]);
		ddaFailReportDetail.setDebtorIdNumber2(csvItems[35]);

		ddiFailReport.getDetails().add(ddaFailReportDetail);
	}

	private void readDDAFailTail(String[] items, DDAFailReportDto ddiFailReport) {
		ddiFailReport.setNoOfRecord(Integer.parseInt(items[1]));
		ddiFailReport.setTotalTransctionAmount(items[2]);
	}
	/***
	 * format split , and replace " to null
	 * @param line
	 * @return
	 */
	private String[] itemFormat(String line)
	{
		String[] items = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		for(int i=0;i<items.length;i++)
		{
			items[i]=items[i].replace("\"", "");
		}
		return items;
	}
	private DDASuccessReportDto parseDDASuccessReport(File file) throws Exception {
		DDASuccessReportDto ddaSuccessReport = new DDASuccessReportDto();
		ddaSuccessReport.setDetails(new ArrayList<DDASuccessReportDetailDto>());
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				String items[]=itemFormat(sCurrentLine);
				if (items.length > 0 && "H".equals(items[0])) {
					readDDASuccessHeader(items, ddaSuccessReport);
				} else if (items.length > 0 && "D".equals(items[0])) {
					readDDASuccessDetail(items, ddaSuccessReport);	
				} else if (items.length > 0 && "T".equals(items[0])) {
					readDDASuccessTail(items, ddaSuccessReport);
				}
			}
			if (ddaSuccessReport.getDetails().size() != ddaSuccessReport.getNoOfRecord()) {
				ddxLogger.error("ERROR !DDA file : total record size not equals record lines count .........");
				throw new GTACommonException("ERROR !DDA file : total record size not equals record lines count ........."); 
			}
		} finally {
			if (null != br)
				br.close();
		}
		return ddaSuccessReport;
	}

	private DDAFailReportDto parseRejectReport(File file) throws Exception {
		DDAFailReportDto ddaFailReport = new DDAFailReportDto();
		ddaFailReport.setDetails(new ArrayList<DDAFailReportDetailDto>());
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				String[] items = sCurrentLine.split(",");
				if (items.length > 0 && "H".equals(items[0]))
					readDDAFailHeader(items, ddaFailReport);
				else if (items.length > 0 && "D".equals(items[0]))
					readDDAFailDetail(items, ddaFailReport);
				else if (items.length > 0 && "T".equals(items[0]))
					readDDAFailTail(items, ddaFailReport);
			}

			if (ddaFailReport.getDetails().size() != ddaFailReport.getNoOfRecord())
				logger.info("ERROR ! no. of record not equals");

			for (DDAFailReportDetailDto ddaFailReportDetail : ddaFailReport.getDetails()) {
				logger.info(ddaFailReportDetail.toString());
			}
			// TODO:

		} finally {
			if (null != br)
				br.close();
		}
		return ddaFailReport;
	}
	@Transactional
	public void parseDDAConfirmReporter(File file)throws Exception {
		ddxLogger.info("DDA file parse DDA and confirm data start......");
		StringBuilder errorMsg = new StringBuilder();
		DDASuccessReportDto dda = null;
		try {
			dda = parseDDASuccessReport(file);
		} catch (Exception e) {
			throw e;
		}
		//only include valide data 
		List<DDASuccessReportDetailDto>mailReports=new ArrayList<>();
		
		for (DDASuccessReportDetailDto details : dda.getDetails()) {
			/***
			 * if the field "Mandate status code" is blank,
				1、check if the field Action Indicator='C',  mark the status  of the existing DDA record in the table member_payment_acc to 'CAN'
				2、if the field "Action Indicator" is 'R', 
				 overwrite the existing DDA record in the table member_payment_acc by replacing the new information from the E_64 report. 
			 */
			if (MandateStatus.A.name().equalsIgnoreCase(details.getMandateStatusCode())
					||
					 (StringUtils.isEmpty(details.getMandateStatusCode())&&
							 (
									 MandateStatus.C.name().equals(details.getActionIndicator())||MandateStatus.R.name().equals(details.getActionIndicator())
							 )
					 )
				) {
				try {
					 this.saveMemberPaymentAcc(details);
				} catch (Exception e) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					e.printStackTrace(new PrintStream(baos));
					ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
					errorMsg.append(e.getMessage());
					details.setGtaStatus("FAIL");
					details.setGtaRemark(e.getMessage());
				}
				mailReports.add(details);
			} else {
				ddxLogger.info("DDA parse the MandateStatus status isn't A or  blank "
						+ details.getDebtorReferenceNumber() + " mandateStatusCode:" + details.getMandateStatusCode());
				details.setGtaStatus("FAIL");
				details.setGtaRemark("MandateStatus status isn't A or  blank");
			}
		}
		this.saveDDALog(file.getName(), errorMsg.toString());
		ddxLogger.info("DDA file parse DDA and confirm data end......");
		if(null!=mailReports&&mailReports.size()>0){
			this.sendMailtoSales(mailReports);	
		}
	}
	@Transactional
	public void saveDDALog(String fileName,String errorMsg) {
		DdInterfaceFileLog ddInterfaceFileLog = (DdInterfaceFileLog) ddInterfaceFileLogDao.getUniqueByHql(
				"from DdInterfaceFileLog log where log.filename='" + fileName + "'");
		Date date=new Date();
		if (null == ddInterfaceFileLog) {
			ddInterfaceFileLog = new DdInterfaceFileLog();
		}
		ddInterfaceFileLog.setProcessTimestamp(date);
		ddInterfaceFileLog.setAckTimestamp(date);
		ddInterfaceFileLog.setStatusUpdateDate(date);
		ddInterfaceFileLog.setFilename(fileName);
		ddInterfaceFileLog.setTransactionType("DDA");
		ddInterfaceFileLog.setStatus(StringUtils.isEmpty(errorMsg)?Status.SUC.name():Status.FAIL.name());
		ddInterfaceFileLog.setBankMsg(errorMsg);
		ddInterfaceFileLog.setStatusUpdateBy("[SysAdmin001]");
		// TODO Auto-generated method stub
		ddInterfaceFileLogDao.saveOrUpdate(ddInterfaceFileLog);
		
	}
	/***
	 * mapping DDA+DDI+Techinal+Spec
	 * https://jira-sinodynamic.atlassian.net/wiki/spaces/HKGTA/pages/98631999/DDA+DDI+Techincal+Spec
	 * @param details
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void saveMemberPaymentAcc(DDASuccessReportDetailDto details) {
		//1、 check exist  SINO db accountNo eq  BANK post DDA file of  debtorReferenceNumber and acc.accType='VA' 
		//if status is ACT then handler flow  else log error
		MemberPaymentAcc memberVA = (MemberPaymentAcc) memberPaymentAccDao.getUniqueByHql("from MemberPaymentAcc acc where acc.accountNo='" + details.getDebtorReferenceNumber()+ "' and acc.accType='VA' and acc.status='"+Status.ACT.name()+"'");
		if (null == memberVA) {
			details.setGtaStatus(Status.FAIL.name());
			details.setGtaRemark("No exist virtual account status is ACT record");
			ddxLogger.error("No exist virtual account status is ACT record Then don't create MemberPaymentAcc . The accountNo:"+details.getDebtorReferenceNumber());
		}else{
			String hql="from MemberPaymentAcc acc "
						+ "	where acc.bankCustomerId='" + details.getDebtorReferenceNumber()+ "' "
						+ " and acc.accType='DD' "
						+ "	and acc.bankId='"+SCB_BANK_ID+"' "
						+ "	and acc.status='"+Status.ACT.name()+"'";
			MemberPaymentAcc memberPaymentAcc = (MemberPaymentAcc) memberPaymentAccDao.getUniqueByHql(hql);
			//only one record of DDA status is ACT
			//if no exists when mandateStatusCode is A: Create DDA  and R:Error and C: Error
			//handler no exist and NACT condition  include  mandateStatusCode is A/R/C
			if(null==memberPaymentAcc){
				//check unique(bank_id,account_no,bank_customer_id ) NACT DDA 
				//include exist NACT and  no exist
				String historyHql="from MemberPaymentAcc acc "
						+ " where acc.bankCustomerId='" + details.getDebtorReferenceNumber()+ "' "
						+ " and acc.accType='DD' "
						+ " and acc.bankId='"+SCB_BANK_ID+"' "
						+ " and acc.accountNo='"+details.getDebtorAccountNumber()+"'";
				memberPaymentAcc =(MemberPaymentAcc)memberPaymentAccDao.getUniqueByHql(historyHql);
				//include exist NACT and  no exist 
				if(MandateStatus.A.name().equals(details.getMandateStatusCode()))
				{
					ddxLogger.info("save or update memberPaymentAcc bankCustomerId: "+details.getDebtorReferenceNumber()+" mandateStatusCode is :"+details.getMandateStatus());
					// no exist create new record
					if(null==memberPaymentAcc){
						// to search exist account_no 
//						memberPaymentAcc = (MemberPaymentAcc) memberPaymentAccDao.getUniqueByHql("from MemberPaymentAcc acc where acc.accountNo='" + details.getDebtorAccountNumber()+ "'");
//						if(null!=memberPaymentAcc)
//						{
//							details.setGtaStatus(Status.FAIL.name());
//							details.setGtaRemark("DebtorAccountNum already occupied by other patron");
//							ddxLogger.error("com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry 'SCB-'"+details.getDebtorAccountNumber() +" for key uk_member_payment_acct_type_no");
//							return;
//						}else{
//							memberPaymentAcc=new MemberPaymentAcc();							
//						}
						memberPaymentAcc=new MemberPaymentAcc();
					}else{
						//Different DDA with NACT, when mandateStatusCode is A: Create DDA  and R:Error and C: Error
						//exist NACT and diff accountNo create new  else update
						if(!memberPaymentAcc.getAccountNo().equals(details.getDebtorAccountNumber()))
						{
							memberPaymentAcc=new MemberPaymentAcc();
						}
					}
				}else{
					//include (bank_id,account_no,bank_customer_id)  exist NACT and no exist
					if(MandateStatus.C.name().equals(details.getActionIndicator())||
						  MandateStatus.R.name().equals(details.getActionIndicator()))
					{
						  //no exist(bank_id,account_no,bank_customer_id) 
						   //when mandateStatusCode is R:Error or  C: Error
						if (null == memberPaymentAcc) {
							details.setGtaStatus(Status.FAIL.name());
							details.setGtaRemark("No exist DDA record");
							ddxLogger.error("no exist DDA record,no hander For :the acc.bankCustomerId='"+details.getDebtorReferenceNumber() + "' the status is :"+ details.getMandateStatusCode()+" , actionIndicator is :"+details.getActionIndicator()+", accountNO:"+details.getDebtorAccountNumber());
							return;
						}else{
							//if exist different DDA with NACT mandateStatusCode IS :R/C  error 
							if(!memberPaymentAcc.getAccountNo().equals(details.getDebtorAccountNumber()))
							{
								details.setGtaStatus(Status.FAIL.name());
								details.setGtaRemark("Different DDA with NACT");
								ddxLogger.error("Different DDA with NACT");
								return ;
							}else{
								//if same  DDA account but cancelled (NACT)  C error
								if(MandateStatus.C.name().equals(details.getActionIndicator()))
								{
									details.setGtaStatus(Status.FAIL.name());
									details.setGtaRemark("Same DDA account but cancelled (NACT)");
									ddxLogger.error("Same DDA account but cancelled (NACT)");
									return ;
								}
							}
							this.saveMemberPaymentAccLog(memberPaymentAcc);
						}
					}
				}
			}else{
				//handler  exist ACT 
				//Same/Different exist DDA and handler mandateStatusCode is A/R/C
				if(MandateStatus.A.name().equals(details.getMandateStatusCode()))
				{
					ddxLogger.error("the patron with the virtual account number "+details.getDebtorReferenceNumber()+" already has a DDA record !");
					details.setGtaStatus(Status.FAIL.name());
					details.setGtaRemark("the patron with the virtual account number already has a DDA record !");
					return ;
				}else{
					if(MandateStatus.C.name().equals(details.getActionIndicator())||
							  MandateStatus.R.name().equals(details.getActionIndicator()))
					{
						//when mandateStatus is R then update SUC
						if(MandateStatus.R.name().equals(details.getActionIndicator())){
							// if diff accountNO compare debtoraccountnumber
							/*
							if(!memberPaymentAcc.getAccountNo().equals(details.getDebtorAccountNumber()))
							{
								MemberPaymentAcc existAcc = (MemberPaymentAcc) memberPaymentAccDao.getUniqueByHql("from MemberPaymentAcc acc where acc.accountNo='" + details.getDebtorAccountNumber()+ "'");
								if(null!=existAcc)
								{
									ddxLogger.error("Duplicate entry 'SCB-"+details.getDebtorAccountNumber()+"' for key 'uk_member_payment_acct_type_no'");
									details.setGtaStatus(Status.FAIL.name());
									details.setGtaRemark("DebtorAccountNum already occupied by other patron");
									return;
								}
							}else{
								ddxLogger.info("change original memberPaymentAcc status to  ACT the DDA FILE MandateStatusCode null and actionIndicator is  :"+MandateStatus.R.name());
							}
							*/
							ddxLogger.info("change original memberPaymentAcc status to  ACT the DDA FILE MandateStatusCode null and actionIndicator is  :"+MandateStatus.R.name());
						}else{
							ddxLogger.info("change original memberPaymentAcc status to NACT the DDA FILE MandateStatusCode null and actionIndicator is  :"+MandateStatus.C.name());
							///when mandateStatus is C then logger error
							//Different DDA exist with ACT status when C logger error
							if(!memberPaymentAcc.getAccountNo().equals(details.getDebtorAccountNumber()))
							{
								ddxLogger.error("DDA exist with ACT status and Different accountNo,old accountNO:"+memberPaymentAcc.getAccountNo()+" new:"+details.getDebtorAccountNumber());
								details.setGtaStatus(Status.FAIL.name());
								details.setGtaRemark("DDA exist with ACT status and Different DDA");
								return;
							}
						}
						this.saveMemberPaymentAccLog(memberPaymentAcc);
					}
				}
			}
			//save or update MemberPaymentAcc
			handlerMemberPaymentAcc(memberVA.getCustomerId(), details, memberPaymentAcc);
		    
		}
	}
	/***
	 * save or update memberPaymentAcc
	 * @param customerId
	 * @param details
	 * @param memberPaymentAcc
	 */
	private void handlerMemberPaymentAcc(Long customerId,DDASuccessReportDetailDto details,MemberPaymentAcc memberPaymentAcc){
		Member member = memberDao.getMemberById(customerId);
		memberPaymentAcc.setBankId(SCB_BANK_ID);
		memberPaymentAcc.setCustomerId(customerId);
		memberPaymentAcc.setAccType("DD");
		memberPaymentAcc.setAccountNo(details.getDebtorAccountNumber());
		memberPaymentAcc.setStatus(MandateStatus.A.name().equals(details.getMandateStatusCode())?Status.ACT.name():getMemberPaymentStatusByActionIndicator(details.getActionIndicator()));// TODO
		memberPaymentAcc.setOriginatorBankCode(details.getOriginatorBankCode());
		memberPaymentAcc.setDrawerBankCode(details.getDebtorBankCode());
		memberPaymentAcc.setBankCustomerId(details.getDebtorReferenceNumber());
		memberPaymentAcc.setBankAccountName(details.getDebtorAccountName());
		memberPaymentAcc.setMember(member);
		if(null!=memberPaymentAcc.getAccId())
		{
			memberPaymentAcc.setUpdateDate(new Timestamp(new Date().getTime()));
		}
		try{
			memberPaymentAccDao.saveOrUpdate(memberPaymentAcc);	
			details.setGtaStatus("SUCCESS");
		}catch(Exception e){
			memberPaymentAccDao.getsessionFactory().getCurrentSession().clear();
			throw e;
		}
	}
	
	
	/**
	 * when DDA cancel(C) or change(M) information
		copy the before change image to the table member_payment_acc_log (remember to update the log_timestamp to the current system date).
		log member_payment_acc_log
	 */
	private void saveMemberPaymentAccLog(MemberPaymentAcc memberPaymentAcc)
	{
		MemberPaymentAccLog log=new MemberPaymentAccLog();
		BeanUtils.copyProperties(memberPaymentAcc, log, new String[] {"member"});
		log.setMemberPaymentAcc(memberPaymentAcc);
		log.setLogTimestamp(new Timestamp(new Date().getTime()));
		memberPaymentAccLogDao.save(log);
	}
	/***
	 * if MandateStatusCode is C then status is CAN
	else if is A then add
	else if is M then overwrite the existing DDA record in the table member_payment_acc by replacing the new
	**/
	private String getMemberPaymentStatusByActionIndicator(String actionIndicator)
	{
		String status = null;
		if (MandateStatus.C.name().equals(actionIndicator)) {
			status = Status.NACT.name();
		} else if (MandateStatus.R.name().equals(actionIndicator)) {
			status = Status.ACT.name();
		}
		return status;
	}
	private void sendMailtoSales(List<DDASuccessReportDetailDto> details)throws Exception{
		try {
			String fileName="DDA Report of "+DateConvertUtil.parseDate2String(new Date(), "dd-MM-yyyy");
			CustomerEmailContent cec=new CustomerEmailContent();
			GlobalParameter globalParameter=globalParameterService.getGlobalParameter(GlobalParameterType.DDA_DDIREPORTRECIPIENTEMAIL.getCode());
			if(null!=globalParameter&&!StringUtils.isEmpty(globalParameter.getParamValue())){
				cec.setRecipientEmail(globalParameter.getParamValue().replace(",", ";"));
			}
			cec.setCreateDate(new Date());
			cec.setStatus(EmailStatus.PND.name());
			cec.setSendDate(new Date());
			cec.setSubject(fileName);
			List<byte[]>bytesList=new ArrayList<>();
			List<String>mineTypeList=new ArrayList<>();
			List<String>fileNameList=new ArrayList<>();
			mineTypeList.add("application/csv");
			fileNameList.add(fileName+".csv");
			byte[]attachment=this.createMailCSV(details);
			if(null!=attachment&&attachment.length>0){
				bytesList.add(attachment);
				mailThreadService.sendWithResponse(cec, bytesList, mineTypeList, fileNameList);
			}
		} catch (Exception e) {
			e.printStackTrace();  
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	        e.printStackTrace(new PrintStream(baos));  
			ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
		}
	}
	private byte[]createMailCSV(List<DDASuccessReportDetailDto> details){
		try {
			
			setAttribute(details);
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "DDASummaryReport.jasper";
			Map<String, Object> parameters = new HashMap();
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, details, "CSV");
			
		} catch (Exception e) {
			ddxLogger.error("DDA createMail csv had happend exception"+e.getMessage());
		}
		return null;
		
	}
	private void setAttribute(List<DDASuccessReportDetailDto>dtos)
	{
		
		String hql="select m.academyNo as patronId,CONCAT(cp.givenName,' ',cp.surname) as patronName from MemberPaymentAcc acc ,Member m,CustomerProfile cp where acc.accountNo=?"
				+ " and acc.customerId=m.customerId and m.customerId=cp.customerId";
		for (DDASuccessReportDetailDto dto : dtos) 
		{
			List<Serializable>param=new ArrayList<>();
			param.add(dto.getDebtorReferenceNumber());
			List<DDASuccessReportDetailDto>attributes=memberPaymentAccDao.getDtoByHql(hql, param, DDASuccessReportDetailDto.class);
			if(null!=attributes&&attributes.size()>0){
				dto.setPatronId(attributes.get(0).getPatronId());
				dto.setPatronName(attributes.get(0).getPatronName());
			}
			if(StringUtils.isEmpty(dto.getMandateStatusCode()))
			{
				if(MandateStatus.C.name().equals(dto.getActionIndicator()))
				{
					dto.setMandateStatus(MandateStatus.C.getDesc());
				}else if(MandateStatus.R.name().equals(dto.getActionIndicator())){
					dto.setMandateStatus(MandateStatus.R.getDesc());
				}
			}
		}
	}
}
