package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.CustomerActionContentDto;
import com.sinodynamic.hkgta.dto.crm.HomePageSummaryDto;
import com.sinodynamic.hkgta.service.IServiceBase;


public interface HomePageSummaryService extends IServiceBase<String> {
	
	public HomePageSummaryDto countNumber(String staffName) throws Exception;
	
	public List<CustomerActionContentDto> getLatestActionContent(int totalSize) throws Exception;

}
