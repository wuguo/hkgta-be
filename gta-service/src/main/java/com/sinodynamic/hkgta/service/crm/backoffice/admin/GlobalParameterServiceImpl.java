package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dto.crm.AppsVersionInfoDto;
import com.sinodynamic.hkgta.dto.crm.AppsVersionListDto;
import com.sinodynamic.hkgta.dto.crm.GlobalParameterListDto;
import com.sinodynamic.hkgta.entity.adm.AppsVersion;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class GlobalParameterServiceImpl extends ServiceBase<GlobalParameter> implements GlobalParameterService {

	
	@Autowired
	private GlobalParameterDao globalParameterDao;
	
	
	@Override
	@Transactional
	public ResponseResult saveGlobalParameter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public ResponseResult getGlobalParameterById(String paramId) {
		ResponseResult result = new ResponseResult();	
		try {
			GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, paramId);
			if(null != globalParameter)globalParameter.setUserPreferenceSettings(null);
			result = new ResponseResult("0", "success", globalParameter);
		} catch (HibernateException e) {
			logger.error(e.getMessage(),e);
			result = new ResponseResult("-1", "", "fail");
		}catch (Exception e) {
			logger.error(e.getMessage(),e);
			result = new ResponseResult("-1", "", "fail");
		}
		return result;
	}

	@Override
	@Transactional
	public ResponseResult setGlobalQuota(String paramValue, String userId) {
		// TODO Auto-generated method stub
		ResponseResult result = null;	
		String paramId = "DPDAYQUOTA";
		try {
			GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, paramId);
			globalParameter.setParamValue(paramValue);
			globalParameter.setUpdateBy(userId);
			globalParameterDao.saveOrUpdate(globalParameter);
			return result = new ResponseResult("0", "cheng gong", "success");
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(),e);
			return result = new ResponseResult("-1", "", "fail");
		}

	}

	@Override
	@Transactional
	public GlobalParameter getGlobalParameter(String paramId) {
		return globalParameterDao.get(GlobalParameter.class, paramId);
	}
	@Override
	@Transactional
	public List<GlobalParameter> getGlobalParameterListByParamCat(String paramCat){
		String hqlstr = "FROM GlobalParameter g where g.paramCat = ? order by g.displayOrder";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(paramCat);
		List<GlobalParameter> list = this.globalParameterDao.getByHql(hqlstr, param);
		if(list!=null&&list.size()>0){
			for(GlobalParameter g : list){
				g.setUserPreferenceSettings(null);
			}
		}
//		else{
//			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"GlobalParameter "+paramCat});
//		}
		return list;
	}
	@Override
	@Transactional
	public void saveOrUpdateGlobalParameters(List<GlobalParameter> globalParameters){
		for(GlobalParameter g : globalParameters){
			this.globalParameterDao.saveOrUpdate(g);
		}
	}

	@Override
	@Transactional
	public ListPage<GlobalParameter> getGlobalParameterList(String paramCat, String sortBy, Boolean isAscending, ListPage<GlobalParameter> pListPage)
	{
		StringBuffer hql = new StringBuffer();
		StringBuffer hqlCount = new StringBuffer();
		hql.append("select p from GlobalParameter p where p.paramCat = ? ");
		hqlCount.append("select count(*) from GlobalParameter p where p.paramCat = ? ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(paramCat);
		String orderByCol = "";
		switch (sortBy)
		{
		case "paramId":
			orderByCol = "p.paramId";
			break;
		case "paramValue":
			orderByCol = "p.paramValue";
			break;
		}
		if (!isAscending)
		{
			pListPage.addDescending(orderByCol);
		} else
		{
			pListPage.addAscending(orderByCol);
		}
		pListPage = globalParameterDao.getGlobalParameterList(pListPage, hql.toString(), hqlCount.toString(), param);
		return pListPage;
	
	}

	@Override
	@Transactional
	public void updateGlobalParameter(GlobalParameter globalParameter)
	{
		globalParameterDao.update(globalParameter);
	}
	/**
	 * 更新GuestRoom系统配置
	 * @param isCheckPush
	 * @param pushTime
	 * @param userId
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult setGuestRoomSysConfig(String isCheckPush, String pushTime, String userId) {
		try {
			GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, "SILENTCHKIN-PUSHMSG");
			globalParameter.setParamValue(isCheckPush);
			globalParameter.setUpdateBy(userId);
			globalParameter.setUpdateDate(new Date());
			globalParameterDao.saveOrUpdate(globalParameter);
			globalParameter = globalParameterDao.get(GlobalParameter.class, "SILENTCHKIN-PUSHTIME");
			globalParameter.setParamValue(pushTime);
			globalParameter.setUpdateBy(userId);
			globalParameter.setUpdateDate(new Date());
			globalParameterDao.saveOrUpdate(globalParameter);
			Constant.GUEST_ROOM_CHECK_IN_PUSH_MESSAGE = isCheckPush;
			Constant.GUEST_ROOM_PUSH_MESSAGE_TIME = pushTime;
			this.responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult; 
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(),e);
			this.responseResult.initResult(GTAError.SysError.FAIL_UPDATE_ROLE);
			return responseResult; 
		}

	}
	/**
	 * 初始化GuestRoom配置数据入缓存中
	 * @return
	 */
	@Override
	@Transactional
	public boolean initGuestRoomSysConfig() {
		try {
			if (Constant.GUEST_ROOM_CHECK_IN_PUSH_MESSAGE == null || Constant.GUEST_ROOM_PUSH_MESSAGE_TIME == null) {
				String paramCat ="SILENTCHKIN";
				List<GlobalParameter> list = getGlobalParameterListByParamCat(paramCat);
				if (list.size() > 0) {
					for (GlobalParameter globalParameter : list) {
						if (globalParameter.getParamId().equals("SILENTCHKIN-PUSHMSG")) {
							Constant.GUEST_ROOM_CHECK_IN_PUSH_MESSAGE = globalParameter.getParamValue();
						} else if (globalParameter.getParamId().equals("SILENTCHKIN-PUSHTIME")) {
							Constant.GUEST_ROOM_PUSH_MESSAGE_TIME = globalParameter.getParamValue();
						}
					}
					return true;
				} else {
					return false;
				}
			}
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(),e);
		}
		return true;
	}

	@Override
	@Transactional
	public ResponseResult saveGlobalParameter(GlobalParameter globalParameter) {
		globalParameter.setCreateDate(new Timestamp(new Date().getTime()));
		globalParameter.setStatus("ACT");
		 if(globalParameterDao.saveOrUpdate(globalParameter)){
			 responseResult.initResult(GTAError.Success.SUCCESS);
		 }else{
			 responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR);
		 }
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult saveGlobalParameters(GlobalParameterListDto listDto, String userId) {
		try {
			if (listDto.getList().size() < 1) {
				responseResult.initResult(GTAError.DayPassError.ERRORMSG_PARAM_NULL);
				return responseResult;
			}
			for (GlobalParameter dto : listDto.getList()) {
				GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, dto.getParamId());
				if (globalParameter == null) {
					responseResult.initResult(GTAError.MemberShipError.NO_RECORD_FOUND);
					return responseResult;
				}
				globalParameter.setParamValue(dto.getParamValue());
				globalParameter.setUpdateDate(new Timestamp(new Date().getTime()));
				globalParameter.setUpdateBy(userId);
				globalParameterDao.update(globalParameter);
			}
			responseResult.initResult(GTAError.Success.SUCCESS);	
			return responseResult;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR);	
			return responseResult;
		}
	}
	
	@Override
	@Transactional
	public List<GlobalParameter> getGeneralGlobalParameterList(){
		String hqlstr = "FROM GlobalParameter g where g.paramCat in ('"+GlobalParameterType.STAFF_MAIL_DAILY_PATRON.getCode()+"','"+GlobalParameterType.WELLNESS_SWITCH.getCode()+"','"+GlobalParameterType.PAYMENT_TYPE_AMEX.getCode()+"','"+GlobalParameterType.IBEACON_UUID.getCode()+"','"+GlobalParameterType.REFUNDREQUESTRECIPIENTMAIL.getCode()+"','"+GlobalParameterType.REPORT_ISSUE.getCode()+"','"+GlobalParameterType.ACCOMMODATION_SWITCH.getCode()+"','"+GlobalParameterType.DDA_DDIREPORTRECIPIENTEMAIL.getCode()+"','"+GlobalParameterType.WELLNESS_RESERVATION_BY_MOBILE_APP.getCode()+"') order by g.displayOrder";
		List<GlobalParameter> list = this.globalParameterDao.getByHql(hqlstr);
		if(list!=null&&list.size()>0){
			for(GlobalParameter g : list){
				g.setUserPreferenceSettings(null);
			}
		}
		return list;
	}

	@Override
	@Transactional
	public boolean checkGlobalParameterSwitch(String paramId) {
    	GlobalParameter globalParameter= globalParameterDao.get(GlobalParameter.class, GlobalParameterType.WELLNESS_SWITCH.getCode());
		 if(null!=globalParameter&&globalParameter.getParamValue().equals("Y"))
		 {
			 return true;
		 }
		return false;
	}

	@Transactional
	public ResponseResult checkPaymentCardTypeTurnOn(String paramId) {
		GlobalParameter globalParameter= globalParameterDao.get(GlobalParameter.class, paramId);
		if(null!=globalParameter){
			if("OFF".equalsIgnoreCase(globalParameter.getParamValue())){
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR,"The American Express is not available ,please use other payment ");	
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		}else{
			responseResult.initResult(GTAError.Success.SUCCESS);	
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult checkReportIssue() {
		GlobalParameter globalParameter= globalParameterDao.get(GlobalParameter.class, GlobalParameterType.REPORT_ISSUE.getCode());
		if(null!=globalParameter){
			if("Y".equalsIgnoreCase(globalParameter.getParamValue())){
				responseResult.initResult(GTAError.Success.SUCCESS,true);	
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS,false);
			}
		}else{
			responseResult.initResult(GTAError.Success.SUCCESS,false);	
		}
		return responseResult;
	}
}
