package com.sinodynamic.hkgta.service.crm.backoffice.presentation;

import static com.sinodynamic.hkgta.util.CollectionUtil.map;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dao.crm.ServerFolderMappingDao;
import com.sinodynamic.hkgta.dto.crm.FolderInfoDto;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;

@Service
public class PresentationFolderOperationServiceImpl extends ServiceBase<ServerFolderMapping> implements PresentationFolderOperationService {
	static Logger logger = Logger.getLogger(PresentationFolderOperationServiceImpl.class);
	@Autowired 
	ServerFolderMappingDao serverFolderMappingDao;
	
	String defaultRoot ;
	
	@PostConstruct
	void init(){
		defaultRoot= appProps.getProperty(DEFAULT_PATH_PREFIX_KEY);//use default root
	}
	
	
	@Override
	@Transactional
	public synchronized boolean createFolder(final String folderName,final String userId) throws Exception {
		//check folderName exist in server_folder_mapping or not
		/*try{
			ServerFolderMapping exsitedFolder = serverFolderMappingDao.findByName(folderName);
			if(exsitedFolder != null || folderName.toLowerCase().equals("trash")){
				throw new Exception("folder in db or on server has been there");
			}
		}catch(Exception e){
			logger.error(e);
			throw new RuntimeException(this.getI18nMessge("exist.folder", new String[]{folderName}, Locale.ROOT));
		}*/
		
		CallBackExecutor executor = new CallBackExecutor(PresentationFolderOperationServiceImpl.class);
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				ServerFolderMapping exsitedFolder = serverFolderMappingDao.findByName(folderName);
				if(exsitedFolder != null || folderName.toLowerCase().equals("trash")){
					throw new Exception("folder has been there");
				}
				return null;
			}

			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.PresentationError.EXIST_FOLDER, new String[]{folderName});
			}
		});
		
		Boolean result = (Boolean)executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				ServerFolderMapping folderMapping = new ServerFolderMapping();
				Date current = new Date();
				folderMapping.setDisplayName(folderName);
				folderMapping.setPathPrefixKey(DEFAULT_PATH_PREFIX_KEY);
				folderMapping.setCreateBy(userId);
				folderMapping.setCreateDate(current);
				folderMapping.setUpdateBy(userId);
				folderMapping.setUpdateDate(new Date());
				serverFolderMappingDao.save(folderMapping);
				//try to save record
				File file = new File(defaultRoot,folderMapping.getFolderId()+"");
				this.getContextValues().put("file", file);
				
				if(file.exists())
					return true;
				
				if(!file.mkdirs())
					throw new Exception("fail to create folder");
				return true;
			}
			
			@Override
			public void doCatch() throws Exception {
				/*File file = (File)(this.getContextValues().get("file"));
				
				if(!file.delete()){//rollback fail
					throw new Exception("rollback");
				}*/
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.PresentationError.FAIL_CREATE_FOLDER, new String[]{folderName});
			}

			/*@Override
			protected ErrorCodeException newCatchException() {
				return new ErrorCodeException(GTAError.PresentationError.FAIL_SYNC_FOLDER);
			}*/

			
			
		});
		return result.booleanValue();
		/*File file = null;
		try {
			ServerFolderMapping folderMapping = new ServerFolderMapping();
			Date current = new Date();
			folderMapping.setDisplayName(folderName);
			folderMapping.setPathPrefixKey(DEFAULT_PATH_PREFIX_KEY);
			folderMapping.setCreateBy(userId);
			folderMapping.setCreateDate(current);
			folderMapping.setUpdateBy(userId);
			folderMapping.setUpdateDate(current);
			serverFolderMappingDao.save(folderMapping);
			//try to save record
			file = new File(defaultRoot,folderMapping.getFolderId()+"");
			file.mkdirs();
			return true;
		}catch(Exception e){
			logger.error(e);
			if(!file.delete()){//rollback fail
				throw new RuntimeException(this.getI18nMessge("db_fs.fail.sync.folder", Locale.ROOT));
			}
			throw new RuntimeException(this.getI18nMessge("fail.create.folder", new String[]{folderName}, Locale.ROOT));
		}*/
	}

	@Override
	@Transactional
	public synchronized boolean renameFolder(String originalFolderName, String newFolderName,String userId) throws Exception {

		//check original folder
		try{
			ServerFolderMapping oldFolderMapping = serverFolderMappingDao.findByName(originalFolderName);
			//originalFolder not exist
			if(oldFolderMapping == null){
				throw new Exception();
			}
		}catch(Exception e){
			logger.error(e);
			throw new RuntimeException(this.getI18nMessge("not_exist.folder", new String[]{originalFolderName}, Locale.ROOT));
		}
		
		//check new folder
		try{
			ServerFolderMapping newFolderMapping = serverFolderMappingDao.findByName(newFolderName);
			if(newFolderMapping != null){//if one of them exist
				throw new Exception();
			}
		}catch(Exception e){
			logger.error(e);
			throw new RuntimeException(this.getI18nMessge("exist.folder", new String[]{newFolderName}, Locale.ROOT));
		}
		
		//try to rename
		try{
			ServerFolderMapping folderMapping = serverFolderMappingDao.findByName(originalFolderName);
			folderMapping.setDisplayName(newFolderName);
			folderMapping.setUpdateBy(userId);
			folderMapping.setUpdateDate(new Date());
			serverFolderMappingDao.update(folderMapping);//try to update record
			return true;
		}catch(Exception e){
			logger.error(e);
			throw new RuntimeException(this.getI18nMessge("fail.rename.folder", new String[]{originalFolderName,newFolderName}, Locale.ROOT));
		}
	}

	private String findRootPath(String prefixKey){
		String parent = appProps.getProperty(prefixKey);//get the parent by path_prefix_key column
		if(parent==null || parent.isEmpty())//if nothing found
			parent = defaultRoot;//then use default
		return parent;
	}
	
	@Override
	@Transactional
	public Data getFoldersInfo(String propertyName, String order,int pageSize, int currentPage) throws Exception {
		try{
			ListPage<ServerFolderMapping> pageParam = new ListPage<ServerFolderMapping>(pageSize, currentPage);
			
			if(order == null) {
				order = "asc";
			}
			if(order.equals("asc")){
				pageParam.addAscending(propertyName);
			}
			if(order.equals("desc")){
				pageParam.addDescending(propertyName);
			}
			
			String sqlSelect = "select "
					+ "mapping.folder_id as folderId, "
					+ "mapping.display_name as folderName, "
					+ "mapping.path_prefix_key as folderPrefixKey, "
					+ "ifnull(material.count,0) as folderFileNumber, "
					+ "mapping.create_date as folderCreatedDate, "
					+ "mapping.update_date as folderUpdatedDate "
					+ "from server_folder_mapping mapping left outer join "
					+ "(select m.folder_id, count(m.folder_id) as count, m.status from present_material m group by m.folder_id,m.status having m.status='ACT') material on mapping.folder_id=material.folder_id";
			
			String sqlCount = "select count(*) from server_folder_mapping";
			
			Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
			typeMap.put("folderId", LongType.INSTANCE);
			typeMap.put("folderName", StringType.INSTANCE);
			typeMap.put("folderPrefixKey", StringType.INSTANCE);
			typeMap.put("folderFileNumber", IntegerType.INSTANCE);
			typeMap.put("folderCreatedDate", DateType.INSTANCE);
			typeMap.put("folderUpdatedDate", DateType.INSTANCE);
			
			ListPage<ServerFolderMapping> pageList = ((GenericDao)serverFolderMappingDao).listBySqlDto(pageParam, sqlCount, sqlSelect, null, new FolderInfoDto(), typeMap);
			Data result = new Data();
			result.setCurrentPage(currentPage);
			result.setPageSize(pageSize);
			result.setTotalPage(pageList.getAllPage());
			result.setLastPage(currentPage==pageList.getAllPage());
			result.setRecordCount(pageList.getAllSize());
			
			
			Collection<FolderInfoDto> foldersInfo = map(pageList.getDtoList(), new CallBack<Object, FolderInfoDto>(){
				@Override
				public FolderInfoDto execute(Object folderInfoDto, int index) {
					FolderInfoDto dto = (FolderInfoDto)folderInfoDto;
					dto.setFolderParentPath(findRootPath(dto.getFolderPrefixKey()));
					return dto;
				}
			});
			result.setList(new ArrayList<FolderInfoDto>(foldersInfo));
			return result;
		}catch(Exception e){
			logger.error(e);
			throw new RuntimeException(this.getI18nMessge("db.fail.load.folders", Locale.ROOT));
		}
	}

	@Override
	@Transactional
	public FolderInfoDto getFolderInfo(Long id) throws Exception {
		try{
			ServerFolderMapping mapping = serverFolderMappingDao.findById(id);
			FolderInfoDto dto = new FolderInfoDto();
			dto.setFolderId(mapping.getFolderId());//Id
			dto.setFolderName(mapping.getDisplayName());//Folder Name
			dto.setFolderParentPath(findRootPath(mapping.getPathPrefixKey()));
			dto.setFolderPrefixKey(mapping.getPathPrefixKey());
			dto.setFolderUpdatedDate(mapping.getUpdateDate());
			dto.setFolderCreatedDate(mapping.getCreateDate());
			dto.setFolderFileNumber(serverFolderMappingDao.findFileNumber(id)); 
			return dto;
		}catch(Exception e){
			logger.error(e);
			throw new RuntimeException(this.getI18nMessge("db.fail.load.folder", new Long[]{id}, Locale.ROOT));
		}
		
	}

	@Transactional
	@Override
	public ServerFolderMapping getServerFolderMappingById(Long id) {
		try{
			return this.serverFolderMappingDao.findById(id);
		}catch(Exception e){
			logger.error(e);
			throw new RuntimeException(this.getI18nMessge("db.fail.load.folder", new Long[]{id}, Locale.ROOT));
		}
		
	}

}
