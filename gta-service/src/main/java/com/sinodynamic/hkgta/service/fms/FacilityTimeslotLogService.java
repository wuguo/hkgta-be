package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotLogDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityTimeslotLogService extends IServiceBase<FacilityTimeslotLog> {

	public List<FacilityTimeslotLogDto> getLogsByFacilityNo(String facilityType, Long facilityNo);
	/***
	 * get all records starting from past  6 months.
	 * @param facilityType
	 * @param facilityNo
	 * @param month 
	 * @return
	 */
	public List<FacilityTimeslotLogDto> getLogsByFacilityNo(String facilityType, Long facilityNo,Integer month);

}
