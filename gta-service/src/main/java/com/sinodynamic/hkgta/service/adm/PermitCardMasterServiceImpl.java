package com.sinodynamic.hkgta.service.adm;

/*
 * @Author Becky
 * @Date 4-29
*/
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.CommonDataQueryByHQLDao;
import com.sinodynamic.hkgta.dao.adm.PermitCardLogDao;
import com.sinodynamic.hkgta.dao.adm.PermitCardMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.mms.SpaMemberSyncDao;
import com.sinodynamic.hkgta.dto.crm.CustomerAccountInfoDto;
import com.sinodynamic.hkgta.dto.crm.PaymentOrderDto;
import com.sinodynamic.hkgta.dto.crm.PermitCardMasterDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardLog;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.PermitCardMasterEnumStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@Service
public class PermitCardMasterServiceImpl extends ServiceBase<PermitCardMaster> implements PermitCardMasterService {


	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;

	@Autowired
	private PermitCardMasterDao permitCardMasterDao;
	
	@Autowired
	private PermitCardLogDao permitCardLogDao;
	
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private CommonDataQueryByHQLDao commonByHQLDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private SpaMemberSyncDao spaMemberSyncDao;
	
	/**
	 * This function is used for checking the permit card status.
	 * @return "0" represents success, "1" represents status is not ISS,"2" represents no enrollment
	 * @throws HibernateException
	 *
	 */
	@Transactional
	public String checkPermitCardStatus(Long enrollId){

	try{
	
		CustomerEnrollment customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByEnrollId(enrollId);

		List<PermitCardMaster> permitCardMasterList = permitCardMasterDao.getPermitCardMasterByCustomerId(customerEnrollment.getCustomerId());

		for(PermitCardMaster permitCardMaster : permitCardMasterList){
			if(permitCardMasterList.size()>0){
				if(permitCardMaster.getStatus().equals("ISS")){
					return "0";
				}else return "1";
			}else return "2";
		}
		}catch(Exception e){
			
		}
	return "2";
	}


	@Transactional
	public Boolean changePermitCardMasterStatus(String cardNo, PermitCardMasterDto param) throws Exception {
		
		PermitCardMaster pcm = permitCardMasterDao.getPermitCardMasterByCardNo(cardNo);
		if (pcm == null) return false;
		
		String status = param.getStatus();
		Long customerId = param.getCustomerId();
		String updateBy = param.getUpdateBy();
		
		if (status != null && status.trim().length() != 0 && PermitCardMasterEnumStatus.isValid(status)) {
			
			pcm.setStatus(status);
			Date date = new Date();
			pcm.setStatusUpdateDate(date);
			if (PermitCardMasterEnumStatus.ISS.getCode().equals(status)) {
				pcm.setCardIssueDate(CommUtil.dateFormat(date, "yyyy-MM-dd HH:mm:ss"));
			}
		}
		
		if (customerId != null) {
			pcm.setMappingCustomerId(customerId);
		}
		
		pcm.setUpdateBy(updateBy);
		
		return permitCardMasterDao.updatePermitCardMaster(pcm);
	}


	@Transactional
	public String replaceOldPermitCardMaster(String oldCardNo, PermitCardMasterDto param) throws Exception {
		
		if (oldCardNo.equals(param.getCardNo()))
		{
			throw new GTACommonException(GTAError.DayPassPurchaseError.CAN_REPLACE_WITH_ORIGIN_CARD);
		}
		String updateBy = param.getUpdateBy();
		String newCardNo = param.getCardNo();
		
		PermitCardMaster oldCard = permitCardMasterDao.getPermitCardMasterByCardNo(oldCardNo);
		if (oldCard == null || oldCard.getMappingCustomerId() == null) {
			throw new GTACommonException(GTAError.DayPassPurchaseError.LINK_CARD_FAILED, "The card you want to be replaced does not exist!");
		}
		
		Date date = new Date();
		//update old card status info
		oldCard.setStatus(PermitCardMasterEnumStatus.DPS.getCode());
		oldCard.setStatusUpdateDate(date);
		oldCard.setUpdateBy(updateBy);
		permitCardMasterDao.updatePermitCardMaster(oldCard);
		
		PermitCardMaster newCard = permitCardMasterDao.getPermitCardMasterByCardNo(newCardNo);
		if (newCard == null) {
			throw new GTACommonException(GTAError.DayPassPurchaseError.LINK_CARD_FAILED, "The card you want to replace does not exist!");
		}
		if (PermitCardMasterEnumStatus.ISS.getCode().equals(newCard.getStatus())) {
			throw new GTACommonException(GTAError.DayPassPurchaseError.LINK_CARD_FAILED, "The card status is ISS and being used by others, please use another card!");
		}
		newCard.setMappingCustomerId(oldCard.getMappingCustomerId());
		
		//to distinct the oldCard's statusUpdateDate and newCard's statusUpdateDate
		 Calendar calendar = Calendar.getInstance();
		 calendar.setTime(date);
		 calendar.add(Calendar.SECOND, 1);
		 Date dateAdd = calendar.getTime();
		//
		newCard.setStatusUpdateDate(dateAdd);
		newCard.setUpdateBy(updateBy);
		newCard.setStatus(PermitCardMasterEnumStatus.ISS.getCode());
		newCard.setCardIssueDate(CommUtil.dateFormat(dateAdd, "yyyy-MM-dd HH:mm:ss"));
		permitCardMasterDao.updatePermitCardMaster(newCard);
		
		return "0";
	}


	@Override
	@Transactional
	public PermitCardMaster getPermitCardMaster(String cardNo) throws Exception {
		return permitCardMasterDao.getPermitCardMasterByCardNo(cardNo);
	}


	@Override
	@Transactional
	public ResponseResult linkCard(String cardNo, PermitCardMasterDto dto, String createBy) throws Exception {
		
		Long customerId = dto.getCustomerId();
		if (StringUtils.isEmpty(cardNo) || StringUtils.isEmpty(customerId + "")) {
			responseResult.initResult(GTAError.AcademyCardError.PARAMETAR_MISSING_ERROR);
			return responseResult;
		}
		
		Long memberID = customerId;
		boolean isActive = memberDao.isMemberActive(memberID);
		if (!isActive) {
			responseResult.initResult(GTAError.AcademyCardError.NOT_ACTIVE_MEMBER);
			return responseResult;
		}
		
		PermitCardMaster newCard = permitCardMasterDao.getPermitCardMasterByCardNo(CommUtil.cardNoTransfer(cardNo));
		if (newCard == null) {
		    responseResult.initResult(GTAError.AcademyCardError.NO_SUCH_CARD_FOUND);
		    return responseResult;
		}
		
		if ((PermitCardMasterEnumStatus.ISS.getCode().equals(newCard.getStatus()) || PermitCardMasterEnumStatus.DPS.getCode().equals(newCard.getStatus()))
				&& newCard.getMappingCustomerId() != null) {
			responseResult.initResult(GTAError.AcademyCardError.CARD_HAS_BEEN_ISSUED);
			return responseResult;
		}
		
//		else if (PermitCardMasterEnumStatus.DPS.getCode().equals(newCard.getStatus())) {
//			responseResult.initResult(GTAError.AcademyCardError.CARD_HAS_BEEN_DISPOSAL);
//			return responseResult;
//		}
		
		//disposal the old card when replace card
		Date date = new Date();
		List<PermitCardMaster> oldCards = permitCardMasterDao.getIssuedCardByCustomerId(memberID);
		if (oldCards != null && oldCards.size() != 0) {
			
			if (oldCards.size() > 1) {
				responseResult.initResult(GTAError.AcademyCardError.DATA_ERROR_FOUND);
				return responseResult;
			} else {
				PermitCardMaster oldCard = oldCards.get(0);
				oldCard.setStatus(PermitCardMasterEnumStatus.DPS.getCode());
				oldCard.setStatusUpdateDate(date);
				oldCard.setUpdateBy(createBy);
				permitCardMasterDao.linkCardForMember(newCard);
			}
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.SECOND, 1);
		Date dateAdd = calendar.getTime();
		
		newCard.setStatus(PermitCardMasterEnumStatus.ISS.getCode());
		newCard.setStatusUpdateDate(dateAdd);
		newCard.setMappingCustomerId(memberID);
		newCard.setCardIssueDate(CommUtil.dateFormat(dateAdd, "yyyy-MM-dd HH:mm:ss"));
		newCard.setUpdateBy(createBy);
		permitCardMasterDao.linkCardForMember(newCard);
		
		//new add
		CustomerEnrollment ce = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(memberID);
		if (ce != null && "ANC".equals(ce.getStatus())) {
		    ce.setStatus("CMP");
		    ce.setUpdateBy(createBy);
		    ce.setUpdateDate(new Date());
		    customerEnrollmentDao.updateCustomerEnrollment(ce);
		}

		//add link card info that need sync to MMS 
		spaMemberSyncDao.addSpaMemberSyncWhenLinkCard(customerId.toString());
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Override
	@Transactional
	public void disposeCard(String cardNo, String updateBy) throws Exception {
		changeCardStatus(cardNo, updateBy, PermitCardMasterEnumStatus.DPS.getCode());
	}
	
	@Override
	@Transactional
	public void returnCard(String cardNo, String updateBy) throws Exception {
		changeCardStatus(cardNo, updateBy, PermitCardMasterEnumStatus.RTN.getCode());
	}
	
	private void changeCardStatus(String cardNo, String updateBy, String newStatus)
	{
		if (StringUtils.isEmpty(cardNo)) {
			throw new GTACommonException(GTAError.DayPassPurchaseError.CARD_NO_IS_INVALID);
		}
		
		PermitCardMaster card = permitCardMasterDao.getPermitCardMasterByCardNo(cardNo);
		if (card==null) return;
		if (newStatus.equals(PermitCardMasterEnumStatus.RTN.getCode()) && card.getStatus().equals(PermitCardMasterEnumStatus.OH.getCode()))
		{
			return;
		}
		PermitCardLog cardLog = new PermitCardLog(card, newStatus, updateBy);
		permitCardLogDao.save(cardLog);
		card.setStatus(newStatus);
		card.setStatusUpdateDate(new Date());
		card.setCardIssueDate(null);
		card.setUpdateBy(updateBy);
	}
	
	@Override
	@Transactional
	public void disposeCardforDayPassByOrderNo(String orderNo, String updateBy)
	{
		StringBuffer tmpSql = new StringBuffer();
		   
		tmpSql.append(" ")
		.append("SELECT cardNo ")
		.append(",customerId ")
        .append(",paymentNo ")
		.append(",activationDate ")
		.append(",planName ")
		.append(",ageRange ")
		.append(",orderNo ")
		.append(",cardStatus ")
	   	.append(",CONCAT (c.salutation,' ',c.given_Name,' ',c.surname) AS memberName ")
		.append(",CONCAT (t.planName,'(',t.ageRange,')') AS daypassType ")
		.append("FROM ( ")
			.append("SELECT * ")
			.append("FROM ( ")
				.append("SELECT copc.linked_card_no cardNo ")
					.append(",copc.cardholder_customer_id customerId ")
					.append(",copc.qr_code paymentNo ")
					.append(",copc.effective_from activationDate ")
					.append(",sp.plan_name planName ")
					.append(",spp.age_range_code ageRange ")
					.append(",copc.customer_order_no orderNo ")
					.append(",card.card_status_to cardStatus ")
				.append("FROM customer_order_permit_card copc ")
				.append("LEFT JOIN permit_card_log card ON copc.linked_card_no = card.card_no ")
					.append("AND copc.cardholder_customer_id = card.customer_id ")
				.append("INNER JOIN service_plan sp ")
				.append("INNER JOIN service_plan_pos spp ")
				.append("WHERE copc.service_plan_no = sp.plan_no ")
					.append("AND copc.service_plan_no = spp.plan_no ")
					.append("[replaceCondition]")
				.append("ORDER BY card.status_update_date DESC ")
				.append(") subtab ")
			.append("GROUP BY paymentNo ")
			.append(") t ")
		.append("LEFT JOIN customer_profile c ON c.customer_id = t.customerId ")
		.append("LEFT JOIN sys_code s ON s.code_value = t.ageRange ")
			.append("AND s.category = 'ageRange' ");

		String sql = "";
	   
		tmpSql.append(" where t.orderNo=" + orderNo);
		sql = tmpSql.toString().replace("[replaceCondition]", "AND copc.customer_order_no = " + orderNo + " ");
		
		List<PaymentOrderDto> order = commonByHQLDao.getDtoBySql(sql, null, PaymentOrderDto.class);
		for (PaymentOrderDto payment : order)
		{
			if (null == payment.getCardNo()) continue;
			try
			{
				disposeCard(payment.getCardNo().toString(), updateBy);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@Transactional
	public ResponseResult getPreviewCardInfo(Long customerId) {
		CustomerAccountInfoDto info = customerProfileDao.getPreviewCardInfoByNearestServiceAccount(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS, info);
		return responseResult;
	}
	@Transactional
	public PermitCardMaster getPermitCardMasterBySerialNo(String serialNo) throws HibernateException {
		Object object=customerProfileDao.getUniqueByHql("from PermitCardMaster p where p.factorySerialNo='"+serialNo+"'");
		if(null!=object){
			PermitCardMaster per=(PermitCardMaster)object;
			return per;
		}
		return null;
	}
}
