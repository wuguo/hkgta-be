
package com.sinodynamic.hkgta.service.crm.backoffice.admin;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.NotificationCenterDao;
import com.sinodynamic.hkgta.dto.staff.NotificationDto;
import com.sinodynamic.hkgta.dto.staff.NotificationMemberDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.notification.SMSService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@SuppressWarnings("rawtypes")
@Service
public class NotificationCenterServiceImpl extends ServiceBase implements NotificationCenterService {
	@Autowired
	private NotificationCenterDao notificationDao;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	SMSService smsService;
	@Autowired
	private UserPreferenceSettingService userPreferenceSettingService;
	
	@Resource(name = "appProperties")
	Properties appProps;
	
	@Autowired
	private MessageTemplateService messageTemplateService;

	private static final Logger logger = Logger.getLogger(NotificationCenterServiceImpl.class);
	
	@Override
	@Transactional
	public ResponseResult getNotificationMemberList(NotificationDto notificationDto) {
		try {
			//modified by Kaster 20160517
//			List<NotificationMemberDto> dtoList = notificationDao.getNotificationMemberList(notificationDto);
//			List<NotificationMemberDto> dtoList = notificationDao.getNotificationMembers(notificationDto);
			List<NotificationMemberDto> dtoList = notificationDao.getRecipients(notificationDto);
			responseResult.initResult(GTAError.Success.SUCCESS,dtoList);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			//modified by Kaster 20160510 将抛出异常统一规范化
			responseResult.initResult(GTAError.NotificationError.GET_MEMBER_LIST_ERROR);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public void sendNotificationByEmail(NotificationDto notificationDto,
			List<NotificationMemberDto> memList, Map<String, String> map) {
        String[] paths = notificationDto.getAttachPaths();
        String basePath = appProps.getProperty("notification.server.material");
        if(paths !=null && paths.length >0){
	        File dir = new File(basePath);
	        if(!dir.exists())dir.mkdir();
        }
        String serviceMailbox = appProps.getProperty("serviceMailbox");
        String serviceTel = appProps.getProperty("serviceTel");
        String domain = appProps.getProperty("domain");
		String webportalLoginPage = appProps.getProperty("webportal.login.page");
        String registrationUrl = webportalLoginPage==null||domain==null?"":" "+domain+webportalLoginPage;
		if(map !=null && map.size()>0){
			for(String id : map.keySet()){
				CustomerEmailContent content = customerEmailContentDao.get(CustomerEmailContent.class, id);
				if(content != null && StringUtils.isNotBlank(content.getContent())){
					content.setContent(content.getContent().replace("{visitOurwebsite}", "<a  target='_blank' href='www.hkgta.com'>Visit our website</a>")
							.replace("{unsubscribe}", "<a  target='_blank' href='" + registrationUrl + "/web/template/message.html?email=" + serviceMailbox + "&tel=" + serviceTel + "'>Unsubscribe</a>"));
				}
				if(content.getRecipientEmail() == null || content.getRecipientEmail().trim().length() == 0){
			        	continue;
			    }
		        logger.info("Send Email Task for customer id ( " + content.getRecipientCustomerId() + ") start ...");
		        if(null!=paths&&paths.length>0){
		        	mailThreadService.sendWithResponse(content, this.getFileBytes(basePath, paths), this.getMineTypes(paths), this.getFileNames(paths));
		        }else{
		        	mailThreadService.sendWithResponse(content,null,null, null);
		        }
		        logger.info("Send Email Task for customer id ( " + content.getRecipientCustomerId() + ") end ...");
			}
		}
	}
	
	@Override
	@Transactional
	public ResponseResult sendNotificationBySMS(NotificationDto notificationDto) {
		try {
			List<NotificationMemberDto> memList = notificationDao.getRecipients(notificationDto);
			List<String> phonenumbers = new ArrayList<String>();
			if(memList!= null && memList.size()>0){
				for(NotificationMemberDto dto : memList){
					boolean sendSms=true;
					if(!StringUtils.isEmpty(dto.getCustomerId())){
						sendSms=userPreferenceSettingService.sendSMSByCustomerId(Long.valueOf(dto.getCustomerId()));
					}
					if(dto.getPhoneMobile() != null){
						if(sendSms) phonenumbers.add(dto.getPhoneMobile());
					}
				}
			}
			smsService.sendSMS(phonenumbers, notificationDto.getContent(), new Date());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			//modified by Kaster 20160510 将抛出异常统一规范化
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.NotificationError.SEND_SMS_FAILED);
		}
		return responseResult;
	}
	
	@Override
	@Transactional
	public Map<String, String> saveEmailRecord(
			NotificationDto notificationDto, List<NotificationMemberDto> memList)throws Exception{
		Map<String, String> map = new HashMap<String, String>();
		if(memList!=null && memList.size()>0){
			for(NotificationMemberDto profile : memList){
				CustomerEmailContent ca = new CustomerEmailContent();
				ca.setSubject(notificationDto.getSubject());
//		    	ca.setContent(notificationDto.getContent());
//				ca.setNoticeType(Constant.NOTICE_TYPE_SERVICE_PLAN_TRANSACTION);
				ca.setContent(replaceMailContentLable(notificationDto.getContent()));
				ca.setNoticeType(Constant.NOTICE_TYPE_BATCH);
		    	ca.setRecipientCustomerId(profile.getCustomerId()+"");
//		    	ca.setSendDate(new Date());
		    	ca.setSenderUserId(notificationDto.getSenderUserId());
		    	ca.setRecipientEmail(profile.getEmail());
		    	ca.setStatus(EmailStatus.PND.name());
		    	ca.setCreateDate(new Date());
		    	String contentId = (String)customerEmailContentDao.addCustomerEmail(ca);
		    	
		    	String[] attachs = notificationDto.getAttachPaths();
		    	if(attachs != null && attachs.length >0){
		    		for(int i=0; i<attachs.length; i++){
		        		CustomerEmailAttach attach = new CustomerEmailAttach();
		        		String fName=attachs[i];
		        		attach.setAttachmentName(fName.substring(fName.indexOf("@")+1, fName.length()));
		        		attach.setAttachmentPath(appProps.getProperty("notification.server.material")+attachs[i]);
		        		attach.setEmailSendId(ca.getSendId()+"");
		        		customerEmailAttachDao.save(attach);
		    		}
		    	}
		    	map.put(contentId,ca.getRecipientCustomerId());
			}
		}
		return map;
    }
	/***
	 * 
	 */
	private  String  replaceMailContentLable(String mailContent)
	{
		   	MessageTemplate template = messageTemplateService.getTemplateByFuncId("marketing_footer_email");
		  	String serviceMailbox = appProps.getProperty("serviceMailbox");
	        String serviceTel = appProps.getProperty("serviceTel");
	        String domain = appProps.getProperty("domain");
			String webportalLoginPage = appProps.getProperty("webportal.login.page");
	        String registrationUrl = webportalLoginPage==null||domain==null?"":" "+domain+webportalLoginPage;
	        String content=null;
		    if(StringUtils.isNotEmpty(mailContent)&&mailContent.contains(template.getMacroTag().replace(",", "|")))
		    {
		    	String site="<a  target='_blank' href='www.hkgta.com'>Visit our website</a>";
		    	String scribe="<a  target='_blank' href='" + registrationUrl + "/web/template/message.html?email=" + serviceMailbox + "&tel=" + serviceTel + "'>Unsubscribe</a>";
		    	String lables[]=template.getMacroTag().split(",");
		    	content=mailContent.replace(lables[0].trim(),site).replace(lables[1].trim(),scribe);
		    	
			}else{
				content=mailContent;
			}
		    return content;
	}
	    
	    /**
	     * 
	     * @param basePath
	     * @param paths
	     * @return file byte list
	     * @throws Exception
	     */
	    public List<byte[]> getFileBytes(String basePath, String[] paths){
	    	List<byte[]> fileBytes = new ArrayList<byte[]>();
	    	   try {
				if(paths !=null && paths.length >0){
					   for(int i=0; i<paths.length; i++){
						   File item = new File(basePath + File.separator + paths[i]);
						   if(item.exists()){
							   InputStream inputStream = new FileInputStream(item);
							   fileBytes.add(IOUtils.toByteArray(inputStream));
						   }
					   }
				   }
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
	    	return fileBytes;
	    }
	    
	    /**
	     * 
	     * @param paths
	     * @return file name list
	     */
	    public List<String> getFileNames(String[] paths){
	    	List<String> fileNames = new ArrayList<String>();
	    	if(paths !=null && paths.length >0){
    		   for(int i=0; i<paths.length; i++){
    			   if(paths[i] != null && paths[i].length()>0){
    				   fileNames.add(paths[i].substring(paths[i].indexOf("@")+1));
    			   }
    		   }
	    	}
	    	return fileNames;
	    }
	    /**
	     * 
	     * @param paths
	     * @return mine type list
	     */
	    public List<String> getMineTypes(String[] paths){
	    	List<String> mineTypes = new ArrayList<String>();
	    	if(paths !=null && paths.length >0){
    		   for(int i=0; i<paths.length; i++){
    			   if(paths[i] != null && paths[i].length()>0){
    				   	String fileSuffix = paths[i].substring(paths[i].lastIndexOf("."));
    				   	if(fileSuffix.endsWith("pdf")){
    						mineTypes.add("application/pdf");
    					}else if(fileSuffix.endsWith("jpeg")){
    						mineTypes.add("image/jpeg");
    					}else if(fileSuffix.endsWith("gif")){
    						mineTypes.add("image/gif");
    					}else if (fileSuffix.endsWith("png")){
    						mineTypes.add("image/png");
    					}else if(fileSuffix.endsWith("html")){
    						mineTypes.add("text/html");
    					}else{
    						mineTypes.add("application/octet-stream");  
    					}
    			   }
    		   }
	    	}
	    	return mineTypes;
	    }
}


