package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.ServicePlanRightMasterDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class ServicePlanRightMasterServiceImpl  extends ServiceBase<ServicePlanRightMaster> implements ServicePlanRightMasterService {
	
	@Autowired
	private ServicePlanRightMasterDao dao;
	@Transactional
	@Override
	public List<ServicePlanRightMaster> getAllServicePlanRightMaster() {
		String hqlstr = "FROM ServicePlanRightMaster s order by s.description";
		return this.dao.excuteByHql(ServicePlanRightMaster.class, hqlstr);
	}

}
