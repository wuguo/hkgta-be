package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotLogDao;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotLogDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateCalcUtil;

@Service
public class FacilityTimeslotLogServiceImpl extends ServiceBase<FacilityTimeslotLog> implements
		FacilityTimeslotLogService {

	@Autowired
	private FacilityTimeslotLogDao dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityTimeslotLogDto> getLogsByFacilityNo(String facilityType, Long facilityNo) {
		List<FacilityTimeslotLog> entityList = dao.getByFacilityNo(facilityType, facilityNo);
		return FacilityTimeslotLogDto.parseEntities(entityList);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityTimeslotLogDto> getLogsByFacilityNo(String facilityType, Long facilityNo,Integer month) {
		String hqlstr = "from FacilityTimeslotLog where facilityType = ? and facilityNo = ? and createDate>=? order by createDate desc";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(facilityNo);
		paramList.add(DateCalcUtil.getDateBeforeMonth(new Date(), month));
		List<FacilityTimeslotLog> entityList = dao.getByHql(hqlstr, paramList);
		return FacilityTimeslotLogDto.parseEntities(entityList);
	}

}
