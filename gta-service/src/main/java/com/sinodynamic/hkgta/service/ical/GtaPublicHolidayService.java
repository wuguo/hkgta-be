package com.sinodynamic.hkgta.service.ical;

import java.util.List;

import com.sinodynamic.hkgta.dto.ical.PublicHolidayDto;
import com.sinodynamic.hkgta.entity.ical.PublicHoliday;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface GtaPublicHolidayService extends IServiceBase<PublicHoliday> {
	
	public PublicHoliday viewHolidayDetail(Long holiday_id) throws Exception;

	public void deleteById(Long holiday_id) throws Exception ;

	public void saveHoliday(List<PublicHoliday> holidays) throws Exception;
	
	public void updateHoliday(PublicHoliday holiday) throws Exception;
	
	public List<PublicHolidayDto> getHolidayList(PublicHolidayDto publicHolidayDto) throws Exception;
	
	public void sendPublicHolidayEmail();

}
