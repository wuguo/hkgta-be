package com.sinodynamic.hkgta.service.pms;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.HousekeepTaskFileDao;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class HousekeepTaskFileServiceImpl extends
		ServiceBase<HousekeepTaskFile> implements HousekeepTaskFileService {

	@Autowired
	private HousekeepTaskFileDao housekeepTaskFileDao;
	
	@Override
	@Transactional
	public byte[] getMediaAsByteStream(Long fileId,Boolean isSmall)
	{
		FileInputStream fis = null;
		
		try{
			HousekeepTaskFile housekeepTaskFile = housekeepTaskFileDao.get(HousekeepTaskFile.class, fileId);
			if(null != housekeepTaskFile){
				String root = appProps.getProperty(DEFAULT_PATH_PREFIX_KEY);
				String fileName = housekeepTaskFile.getServerFile();
				if(null != isSmall && isSmall){
					fileName = fileName.substring(0,fileName.lastIndexOf(".")) +"_small."+fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
				}
				String realFileName = fileName.substring(fileName.lastIndexOf("/")+1,fileName.length());
				String filePath = root + File.separator + realFileName;
				File file = new File(filePath);
				if (!file.exists()) return null;
				
				fis = new FileInputStream(file);
				
 				return IOUtils.toByteArray(new FileInputStream(file));
			}
		}catch (Exception e){
			throw new RuntimeException(this.getI18nMessge("fail.load.media", Locale.ROOT));
		}finally{
			
			if(null != fis){
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	@Transactional
	public HousekeepTaskFile getHousekeepTaskFile(Long fileId)
	{
		return housekeepTaskFileDao.get(HousekeepTaskFile.class, fileId);
	}

}

