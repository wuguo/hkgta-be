package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import com.sinodynamic.hkgta.entity.crm.HelperPassType;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface HelperPassTypeService extends IServiceBase<HelperPassType> {
    
    public void checkIfHelperPassTypeExpired();

}
