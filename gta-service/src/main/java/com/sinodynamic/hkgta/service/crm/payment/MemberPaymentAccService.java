package com.sinodynamic.hkgta.service.crm.payment;

import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface MemberPaymentAccService extends IServiceBase<MemberPaymentAcc> {
	
	public String getVccNoByCustomerId(Long CustomerId);

}
