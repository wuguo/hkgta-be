package com.sinodynamic.hkgta.service.pms;

import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface HousekeepTaskFileService extends IServiceBase<HousekeepTaskFile> {

	String DEFAULT_PATH_PREFIX_KEY = "image.server.material";
	
	byte[] getMediaAsByteStream(Long fileId,Boolean isSmall);
	
	HousekeepTaskFile getHousekeepTaskFile(Long fileId);

}