package com.sinodynamic.hkgta.service.onlinepayment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.onlinepayment.ItemPaymentAccCodeDao;
import com.sinodynamic.hkgta.entity.onlinepayment.ItemPaymentAccCode;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class ItemPaymentAccCodeServiceImpl extends ServiceBase<ItemPaymentAccCode>implements ItemPaymentAccCodeService {

	@Autowired
	private ItemPaymentAccCodeDao itemPaymentAccCodeDao;
	@Override
	@Transactional
	public List<ItemPaymentAccCode> getItemPaymentAccCodeByStatus(String status, String categoryCode) {
		// TODO Auto-generated method stub
		return itemPaymentAccCodeDao.getItemPaymentAccCodeByStatus(status, categoryCode);
	}

}
