package com.sinodynamic.hkgta.service.fms;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.fms.FacilityAdditionAttributeDao;
import com.sinodynamic.hkgta.dao.fms.FacilityAttributeCaptionDao;
import com.sinodynamic.hkgta.dao.fms.FacilityMasterDao;
import com.sinodynamic.hkgta.dao.fms.FacilitySubTypeDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityAttendanceDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityBookAdditionAttrDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dto.fms.AdvancedQueryConditionsDto;
import com.sinodynamic.hkgta.dto.fms.FacilityCheckInDto;
import com.sinodynamic.hkgta.dto.fms.FacilityCheckInRequestDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.fms.FacilityAdditionAttribute;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilitySubType;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendancePK;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.integration.zoomtech.service.GolfMachineService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.util.AccessDatabaseUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MemberFacilityTypeBookingStatus;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.ReservationType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@Service
public class FacilityCheckInServiceImpl extends ServiceBase<FacilityMaster> implements FacilityCheckInService {

	private Logger zoomtechLog = Logger.getLogger(LoggerType.ZOOMTECH.getName()); 
	
    @Autowired
    private FacilityMasterDao facilityMasterDao;

    @Autowired
    private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;

    @Autowired
    private FacilityMasterService facilityMasterService;

    @Autowired
    private FacilityTimeslotDao facilityTimeslotDao;

    @Autowired
    private CustomerProfileDao customerProfileDao;

    @Autowired
    private MemberFacilityAttendanceDao memberFacilityAttendanceDao;

    @Autowired
    private FacilityAdditionAttributeDao facilityAdditionAttributeDao;

    @Autowired
    private FacilitySubTypeDao facilitySubTypeDao;

    @Autowired
    private FacilityAttributeCaptionDao facilityAttributeCaptionDao;

    @Autowired
    private MessageTemplateDao messageTemplateDao;

    @Autowired
    @Qualifier("asynchronizedPushService")
    private DevicePushService devicePushService;
    
    //added by Kaster 20160225
    @Autowired
    private MemberFacilityBookAdditionAttrDao memberFacilityBookAdditionAttrDao;
    
	@Autowired
	private GolfMachineService golfMachineService;    
	@Autowired
	private FacilitySubTypeDao FacilitySubTypeDao;

    protected final Logger logger = Logger.getLogger(FacilityCheckInServiceImpl.class);

    @Transactional
    @Override
    public FacilityCheckInDto<Object> getCheckAvailability(long resvId, boolean isMember) {
        MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao
                .getMemberFacilityTypeBooking(resvId);

        validateCheckInRecord(memberFacilityTypeBooking);
        isMember = isMember(isMember, memberFacilityTypeBooking);

        FacilityCheckInDto<Object> facilityCheckInDto = new FacilityCheckInDto<>();
        facilityCheckInDto.setBeginDate(memberFacilityTypeBooking.getBeginDatetimeBook());
        String facilityType = memberFacilityTypeBooking.getResvFacilityType();

        List<FacilityTimeslot> facilityTimeslots = memberFacilityTypeBooking.getFacilityTimeslots();
        Map<Long, List<FacilityTimeslot>> originalTimeslotsMap = getFacilityTimeslotsMap(facilityTimeslots);

        long totalFacilityTypeQty = memberFacilityTypeBookingDao.getTotalFacilityTypeQty(resvId);
        facilityCheckInDto.setCount(totalFacilityTypeQty);

        Set<Long> checkedFacilityNum = getCheckedInFacilityNum(originalTimeslotsMap);
        long totalCheckedInFacilityTypeQty = memberFacilityTypeBookingDao.getTotalCheckedInFacilityTypeQty(resvId);

        facilityCheckInDto.setCheckedInCount(totalCheckedInFacilityTypeQty);
        getCheckedInFacilities(facilityCheckInDto, checkedFacilityNum);
        
        //added by Kaster 20160229 用来判断是私人教练check in还是会员check in，以便前端判断显示哪一种bay（会员练习的bay还是私人教练使用的bay）
        if(memberFacilityTypeBooking.getExpCoachUserId() != null){
        	facilityCheckInDto.setZoneCoaching(true);
        }else{
        	facilityCheckInDto.setZoneCoaching(false);
        }

        if (Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())) {

            Map<Integer, List<FacilityMasterDto>> floorMap = new LinkedHashMap<>();

            for (int i = 0; i <= 1; i++) {
                List<FacilityMaster> facilityMasterList = facilityMasterService.getFacilityMasterList(i, facilityType);

                List<FacilityMasterDto> facilityMasterDtosList = getCheckInMasterDto(memberFacilityTypeBooking, facilityMasterList, isMember);
                floorMap.put(i, facilityMasterDtosList);
                facilityCheckInDto.setAvailabilities(floorMap);
            }

        } else {
            List<FacilityMaster> facilityMasterList = facilityMasterService.getFacilityMasterList(null, facilityType);
            List<FacilityMasterDto> retList = new ArrayList<>(facilityMasterList.size());
            List<FacilityMasterDto> facilityMasterDtosList = getCheckInMasterDto(memberFacilityTypeBooking,
                    facilityMasterList, isMember);
            retList.addAll(facilityMasterDtosList);
            facilityCheckInDto.setAvailabilities(retList);
        }
        return facilityCheckInDto;
    }

    /***
     *  get MemberFacilityTypeBooking by resvId and  excluded booking status Cancel
     */
    @Transactional
    public ResponseResult returnCheckedInFacilitiesByResvId(long resvId) {
        MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(resvId);
        FacilityCheckInDto<Object> facilityCheckInDto = new FacilityCheckInDto<>();
        facilityCheckInDto.setCount(memberFacilityTypeBooking.getFacilityTypeQty());
        List<FacilityTimeslot> facilityTimeslots = memberFacilityTypeBooking.getFacilityTimeslots();
        Map<Long, List<FacilityTimeslot>> originalTimeslotsMap = getFacilityTimeslotsMap(facilityTimeslots);
        Set<Long> checkedFacilityNum = getCheckedInFacilityNum(originalTimeslotsMap);
        facilityCheckInDto.setCheckedInCount((long) checkedFacilityNum.size());

        getCheckedInFacilities(facilityCheckInDto, checkedFacilityNum);

        responseResult.initResult(GTAError.Success.SUCCESS, facilityCheckInDto);
        return responseResult;
    }

    @Transactional
    @Override
    public List<FacilityMasterDto> getFacilityAllotment(long resvId) {

        MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.getMemberFacilityTypeBookingIncludeAllStatus(resvId);
        List<FacilityTimeslot> facilityTimeslots = memberFacilityTypeBooking.getFacilityTimeslots();
        List<FacilityMasterDto> facilityMasterDtoList = new ArrayList<>();
        if (facilityTimeslots != null) {
            Map<Long, List<FacilityTimeslot>> originalTimeslotsMap = getFacilityTimeslotsMap(facilityTimeslots, false);
            for (Entry<Long, List<FacilityTimeslot>> entry : originalTimeslotsMap.entrySet()) {
                List<FacilityTimeslot> timeslots = entry.getValue();
                Long facilityNo = entry.getKey();
                FacilityMasterDto facilityMasterDto = new FacilityMasterDto();
                facilityMasterDto.setFacilityNo(facilityNo);
                String timeslotStatus = getTimeslotStatus(timeslots, false);
                facilityMasterDto.setStatus(timeslotStatus);
                Long facilityTimeslotId = this.getTimeslotId(timeslots, timeslotStatus, false);
                facilityMasterDto.setFacilityTimeslotId(facilityTimeslotId);
                if (timeslotStatus.equals(FacilityStatus.CAN.name())) {
                	//反向查询获得关联的最新的预订订单id
                	BigInteger newBookingId = memberFacilityTypeBookingDao.getNewBookingDataId(facilityTimeslotId);
                	facilityMasterDto.setResvId(newBookingId == null ? null : newBookingId.longValue());
				}
                if(Constant.FACILITY_TYPE_TENNIS.equalsIgnoreCase(memberFacilityTypeBooking.getResvFacilityType()))
                {
	                FacilityMaster master= facilityMasterDao.get(FacilityMaster.class, facilityNo);
	                facilityMasterDto.setFacilityName(master.getFacilityName());
                }
                facilityMasterDtoList.add(facilityMasterDto);
            }
        }
        return facilityMasterDtoList;
    }

    private boolean isPrivateCoach(MemberFacilityTypeBooking memberFacilityTypeBooking) {
        return memberFacilityTypeBooking.getExpCoachUserId() != null;
    }

    private boolean isMember(boolean isMember, MemberFacilityTypeBooking memberFacilityTypeBooking) {
        // tennis can't change bay when check-in
        if (Constant.FACILITY_TYPE_TENNIS.equals(memberFacilityTypeBooking.getResvFacilityType())) {
            isMember = true;
        }
        return isMember;
    }

    private void validateCheckInRecord(MemberFacilityTypeBooking memberFacilityTypeBooking) {
        if (memberFacilityTypeBooking == null) {
            throw new GTACommonException(GTAError.FacilityError.RESVID_NOT_FOUND);
        }
    }

    private void validateCheckInStatus(MemberFacilityTypeBooking memberFacilityTypeBooking) {
        if (MemberFacilityTypeBookingStatus.ATN.name().equals(memberFacilityTypeBooking.getStatus())) {
            throw new GTACommonException(GTAError.FacilityError.ALREADY_CHECK_IN);
        }
    }

    private void validateCheckInDate(MemberFacilityTypeBooking memberFacilityTypeBooking) {
        //validation check-in date
        Timestamp endDatetimeBook = memberFacilityTypeBooking.getEndDatetimeBook();
        Date today = new Date();
        if (endDatetimeBook != null && endDatetimeBook.before(today)) {
            throw new GTACommonException(GTAError.FacilityError.CHECK_IN_OVERDUE);
        }
    }

    private Map<Long, List<FacilityTimeslot>> getFacilityTimeslotsMap(List<FacilityTimeslot> facilityTimeslots) {
        return getFacilityTimeslotsMap(facilityTimeslots, true);
    }

    private Map<Long, List<FacilityTimeslot>> getFacilityTimeslotsMap(List<FacilityTimeslot> facilityTimeslots, boolean excludeCancel) {
        Map<Long, List<FacilityTimeslot>> timeslotsMap = new HashMap<>();
        for (FacilityTimeslot facilityTimeslot : facilityTimeslots) {

        	if (null == facilityTimeslot.getFacilityMaster()) {
        		continue;
			}
            Long facilityNo = facilityTimeslot.getFacilityMaster().getFacilityNo();
            String status = facilityTimeslot.getStatus();
            //exclude cancel status
            if (excludeCancel && FacilityStatus.CAN.name().equals(status) || null == facilityNo) {
                continue;
            }
            List<FacilityTimeslot> facilityTimeslotList = timeslotsMap.get(facilityNo);
            if (facilityTimeslotList == null) {
                facilityTimeslotList = new ArrayList<>();
            }

            facilityTimeslotList.add(facilityTimeslot);
            timeslotsMap.put(facilityNo, facilityTimeslotList);
        }
        return timeslotsMap;
    }


    private Set<Long> getCheckedInFacilityNum(Map<Long, List<FacilityTimeslot>> originalTimeslotsMap) {
        Set<Long> checkedFacilityNum = new HashSet<>();
        for (Entry<Long, List<FacilityTimeslot>> entry : originalTimeslotsMap.entrySet()) {
            List<FacilityTimeslot> timeslots = entry.getValue();
            String timeslotStatus = getTimeslotStatus(timeslots);
            if (FacilityStatus.OP.getDesc().equals(timeslotStatus)) {
                checkedFacilityNum.add(entry.getKey());
            }
        }
        return checkedFacilityNum;
    }

    private String getTimeslotStatus(List<FacilityTimeslot> facilityTimeslotList) {
        return getTimeslotStatus(facilityTimeslotList, true);
    }

    private String getTimeslotStatus(List<FacilityTimeslot> facilityTimeslotList, boolean excludeCancel) {

        String timeslotStatus = FacilityStatus.VC.getDesc();
        if (facilityTimeslotList != null) {

            for (FacilityTimeslot facilityTimeslot : facilityTimeslotList) {
                String status = facilityTimeslot.getStatus();
                if (FacilityStatus.MT.name().equals(status)) {
                    timeslotStatus = FacilityStatus.MT.name();
                    break;
                } else if (FacilityStatus.OP.name().equals(status)) {
                    timeslotStatus = FacilityStatus.OP.name();
                    break;
                } else if (FacilityStatus.RS.name().equals(status)) {
                    timeslotStatus = FacilityStatus.RS.name();
                    break;
                } else if (!excludeCancel && FacilityStatus.CAN.name().equals(status)) {
                    timeslotStatus = FacilityStatus.CAN.name();
                    continue;
                }
            }
        }
        return timeslotStatus;
    }

    private Long getTimeslotId(List<FacilityTimeslot> facilityTimeslotList, String timeslotStatus, boolean excludeCancel) {

        Long timeslotId = null;

        if (facilityTimeslotList != null) {

            for (FacilityTimeslot facilityTimeslot : facilityTimeslotList) {
                String status = facilityTimeslot.getStatus();
                if (timeslotStatus.equals(status)) {
                    timeslotId = facilityTimeslot.getFacilityTimeslotId();
                    break;
                } else if (!excludeCancel && FacilityStatus.CAN.name().equals(status)) {
                    timeslotId = facilityTimeslot.getFacilityTimeslotId();
                    continue;
                }
            }
        }
        return timeslotId;
    }

    private void getCheckedInFacilities(FacilityCheckInDto<Object> facilityCheckInDto,
                                        Set<Long> checkedFacilityNum) {

        List<FacilityMasterDto> checkedInFacilities = new ArrayList<>();
        if (!checkedFacilityNum.isEmpty()) {

            Set<Long> treeSet = new TreeSet<>(checkedFacilityNum);
            for (Long facilityNo : treeSet) {
                FacilityMaster facilityMaster = facilityMasterDao.get(FacilityMaster.class, facilityNo);

                FacilityMasterDto dto = new FacilityMasterDto();
                dto.setFacilityName(facilityMaster.getFacilityName());
                dto.setFacilityNo(facilityNo);
                checkedInFacilities.add(dto);

            }
        }

        facilityCheckInDto.setCheckedInFacilities(checkedInFacilities);
    }

    private List<FacilityMasterDto> getCheckInMasterDto(MemberFacilityTypeBooking memberFacilityTypeBooking,
                                                        List<FacilityMaster> facilityMasterList, boolean isMember) {
        List<FacilityMasterDto> facilityMasterDtosList = new ArrayList<>();

        List<FacilityTimeslot> reservationTimeslot = memberFacilityTypeBooking.getFacilityTimeslots();
        Map<Long, List<FacilityTimeslot>> timeslotsMap = getFacilityTimeslotsMap(reservationTimeslot);
        Set<Long> facilityNoSet = timeslotsMap.keySet();

        for (FacilityMaster master : facilityMasterList) {
            FacilityMasterDto dto = new FacilityMasterDto();
            dto.setFacilityName(master.getFacilityName());
            Long facilityNo = master.getFacilityNo();
            dto.setFacilityNo(facilityNo);

            String facilityType = null;
            if (master.getFacilityTypeBean() != null) {
                facilityType = master.getFacilityTypeBean().getTypeCode();
            }

            String attributeId = getFacilityAttribute(facilityNo, facilityType);
            getFacilitySubName(facilityType, attributeId, dto);

            setZoneAttr(dto, facilityNo, facilityType);
            String timeslotStatus = getTimeslotStatus(memberFacilityTypeBooking, facilityNoSet, facilityNo, null, isMember);

            //already checked-in bay
            if (facilityNoSet.contains(facilityNo) && FacilityStatus.OP.getDesc().equals(timeslotStatus)) {
                dto.setIsSelected(true);
            } else {
                dto.setIsSelected(false);
            }

            dto.setStatus(timeslotStatus);
            facilityMasterDtosList.add(dto);
        }

        return facilityMasterDtosList;
    }

    private void setZoneAttr(FacilityMasterDto dto, Long facilityNo, String facilityType) {
        if (Constant.FACILITY_TYPE_GOLF.equals(facilityType)) {
            FacilityAdditionAttribute zoneAttribute = facilityAdditionAttributeDao.getFacilityAdditionAttribute(facilityNo, Constant.ZONE_ATTRIBUTE);
            if (zoneAttribute != null) {
                dto.setZone(zoneAttribute.getId().getAttributeId());
            }

        }
    }

    private String getFacilityAttribute(Long facilityNo, String facilityType) {
        String attributeType;
        if (Constant.FACILITY_TYPE_GOLF.equals(facilityType)) {
            FacilityAdditionAttribute attribute = facilityAdditionAttributeDao.getFacilityAdditionAttribute(facilityNo, Constant.GOLFBAYTYPE);
            attributeType = attribute.getId().getAttributeId();
        } else {
            FacilityMaster facilityMaster = facilityMasterDao.get(FacilityMaster.class, facilityNo);
            attributeType = facilityMaster.getFacilitySubtypeId();
        }
        return attributeType;
    }

    private void getFacilitySubName(String facilityType, String attributeId, FacilityMasterDto dto) {
        if (Constant.FACILITY_TYPE_GOLF.equals(facilityType)) {
            FacilityAttributeCaption caption = facilityAttributeCaptionDao.getFacilityAttributeCaptionByAttributeId(attributeId);
            if (null != caption)
                dto.setVenueName(caption.getCaption());
        } else {
            FacilitySubType subType = facilitySubTypeDao.get(FacilitySubType.class, attributeId);
            dto.setVenueName(subType.getName());
        }
        dto.setVenueCode(attributeId);
        dto.setFacilityAttribute(attributeId);
    }

    private String getTimeslotStatus(MemberFacilityTypeBooking memberFacilityTypeBooking,
                                     Set<Long> originalFacilityNoSet, Long targetfacilityNo,
                                     Map<Long, List<FacilityTimeslot>> timeslotsMap,
                                     boolean isMember) {
        List<FacilityTimeslot> facilityTimeslots = memberFacilityTypeBooking.getFacilityTimeslots();
        Map<Long, List<FacilityTimeslot>> originalTimeslotsMap = getFacilityTimeslotsMap(facilityTimeslots);

        Set<Long> checkedFacilityNum = getCheckedInFacilityNum(originalTimeslotsMap);

        //modified by Kaster
        String facilityType = memberFacilityTypeBooking.getResvFacilityType();
        Set<String> originalAttributeMap = getOriginalAttributeMap(memberFacilityTypeBooking, originalFacilityNoSet);
        String attributeId = getFacilityAttribute(targetfacilityNo, facilityType);
        
        //added by Kaster 20160225 比较预定的golf bay与当前的golf bay是否同为左手或者右手，如果不同，则timeSlotStatus为不可访问。
        MemberFacilityBookAdditionAttr attr = memberFacilityBookAdditionAttrDao.getMemberFacilityBookAdditionAttr(memberFacilityTypeBooking.getResvId(), facilityType);
        if(attr!=null && !attr.getId().getAttributeId().equalsIgnoreCase(attributeId)){
        	return FacilityStatus.OP.getDesc();
        }
        
        String timeSlotStatus;
        if (checkedFacilityNum.contains(targetfacilityNo)) {
            timeSlotStatus = FacilityStatus.OP.getDesc();
            return timeSlotStatus;
        }
        // already selected
        else if (originalFacilityNoSet.contains(targetfacilityNo)) {
            timeSlotStatus = FacilityStatus.TA.getDesc();
            return timeSlotStatus;
        }


        // co.studio only available for private coach
        if (!isPrivateCoach(memberFacilityTypeBooking) && Constant.GOLFBAYTYPE_CAR.equals(attributeId)) {
            timeSlotStatus = FacilityStatus.OP.getDesc();
            return timeSlotStatus;
        }

        if (isMember) {
            if (!originalAttributeMap.contains(attributeId)) {
                timeSlotStatus = FacilityStatus.OP.getDesc();
                return timeSlotStatus;
            }
        }

        // zone check
        FacilityAdditionAttribute srcZoneAttribute = facilityAdditionAttributeDao
                .getFacilityAdditionAttribute(targetfacilityNo, Constant.ZONE_ATTRIBUTE);

        Map<String, Timestamp> dateTimeBookMap = getDateTimeBook(memberFacilityTypeBooking, targetfacilityNo);

        Timestamp beginDatetimeBook = dateTimeBookMap.get("min");
        Timestamp endDatetimeBook = dateTimeBookMap.get("max");
        // check target

        boolean targetAvail = checkCheckInAvail(targetfacilityNo, beginDatetimeBook, endDatetimeBook, timeslotsMap);

        if (targetAvail) {
            for (Long originalFacilityNo : originalFacilityNoSet) {
                // except checked In bay
                if (!isCheckedInBay(checkedFacilityNum, originalFacilityNo)
                        && (Constant.FACILITY_TYPE_GOLF.equals(facilityType) && isDiffZone(originalFacilityNo, srcZoneAttribute)
                        || !checkCheckInAvail(originalFacilityNo, beginDatetimeBook, endDatetimeBook, timeslotsMap))) {
                    return FacilityStatus.OP.getDesc();
                }
            }

            return FacilityStatus.VC.getDesc();
        } else {
            return FacilityStatus.OP.getDesc();
        }
    }

    private boolean isCheckedInBay(Set<Long> checkedFacilityNum, Long originalFacilityNo) {
        return checkedFacilityNum.contains(originalFacilityNo);
    }

    private boolean isDiffZone(Long originalFacilityNo, FacilityAdditionAttribute srcZoneAttribute) {
        FacilityAdditionAttribute targetZoneAttribute = facilityAdditionAttributeDao.getFacilityAdditionAttribute(originalFacilityNo, Constant.ZONE_ATTRIBUTE);

        return srcZoneAttribute != null && targetZoneAttribute != null && !srcZoneAttribute.getId().getAttributeId().equals(targetZoneAttribute.getId().getAttributeId());
    }

    private Set<String> getOriginalAttributeMap(MemberFacilityTypeBooking memberFacilityTypeBooking,
                                                Set<Long> facilityNoSet) {
        Set<String> originalAttributeMap = new HashSet<>();
        for (Long facilityNo : facilityNoSet) {
            String attribute = getFacilityAttribute(facilityNo, memberFacilityTypeBooking.getResvFacilityType());
            originalAttributeMap.add(attribute);
        }
        return originalAttributeMap;
    }

    private Map<String, Timestamp> getDateTimeBook(MemberFacilityTypeBooking memberFacilityTypeBooking, Long targetfacilityNo) {

        Map<String, Timestamp> dateTimeBookMap = new HashMap<>();

        Timestamp beginDatetimeBook = memberFacilityTypeBooking.getBeginDatetimeBook();
        Timestamp endDatetimeBook = memberFacilityTypeBooking.getEndDatetimeBook();

        List<FacilityTimeslot> facilityTimeslotList = facilityTimeslotDao.getFacilityTimeslotByBeginDate(targetfacilityNo,
                beginDatetimeBook, endDatetimeBook);

        Set<MemberFacilityTypeBooking> targetMemberFacilityTypeBookingSet = new HashSet<>();
        if (facilityTimeslotList != null) {
            for (FacilityTimeslot facilityTimeslot : facilityTimeslotList) {

                MemberFacilityTypeBooking targetMemberFacilityTypeBooking = facilityTimeslot.getMemberFacilityTypeBooking();
                if (targetMemberFacilityTypeBooking != null) {
                    targetMemberFacilityTypeBookingSet.add(targetMemberFacilityTypeBooking);
                }
            }
        }

        Iterator<MemberFacilityTypeBooking> bookingIterator = targetMemberFacilityTypeBookingSet.iterator();
        if (bookingIterator.hasNext()) {
            MemberFacilityTypeBooking targetBooking = bookingIterator.next();

            Timestamp targetBeginDatetimeBook = targetBooking.getBeginDatetimeBook();
            Timestamp targetDatetimeBook = targetBooking.getEndDatetimeBook();

            if (targetBeginDatetimeBook.before(beginDatetimeBook)) {
                beginDatetimeBook = targetBeginDatetimeBook;
            }

            if (targetDatetimeBook.after(endDatetimeBook)) {
                endDatetimeBook = targetDatetimeBook;
            }

        }
        dateTimeBookMap.put("min", beginDatetimeBook);
        dateTimeBookMap.put("max", endDatetimeBook);
        return dateTimeBookMap;
    }

    private boolean checkCheckInAvail(Long facilityNo, Timestamp beginDatetime, Timestamp endDatetime, Map<Long, List<FacilityTimeslot>> targetTimeslotsMap) {

        // check target
        List<FacilityTimeslot> targetFacilityTimeslotList = facilityTimeslotDao
                .getFacilityTimeslotByBeginDate(facilityNo, beginDatetime, endDatetime);

        if (targetTimeslotsMap != null) {
            targetTimeslotsMap.put(facilityNo, targetFacilityTimeslotList);
        }

        String timeslotStatus = getTimeslotStatus(targetFacilityTimeslotList);
        if (!FacilityStatus.VC.getDesc().equals(timeslotStatus)) {
            return false;
        } else {

            Set<MemberFacilityTypeBooking> targetFacilityTypeBookingSet = new HashSet<>();

            if (targetFacilityTimeslotList != null) {
                for (FacilityTimeslot facilityTimeslot : targetFacilityTimeslotList) {

                    MemberFacilityTypeBooking targetMemberFacilityTypeBooking = facilityTimeslot
                            .getMemberFacilityTypeBooking();
                    targetFacilityTypeBookingSet.add(targetMemberFacilityTypeBooking);
                }

                return targetFacilityTypeBookingSet.size() < 2;
            }
        }
        return false;
    }

    private void validateFacilityTypeQty(Long[] facilityNoList, MemberFacilityTypeBooking memberFacilityTypeBooking) {

        if (facilityNoList == null || facilityNoList.length <= 0) {
            throw new GTACommonException(GTAError.FacilityError.BAY_COUNT_FAIL);
        }

        Long facilityTypeQty = memberFacilityTypeBooking.getFacilityTypeQty();
        // check bay count
        if (facilityTypeQty == null || facilityTypeQty < facilityNoList.length) {
            throw new GTACommonException(GTAError.FacilityError.BAY_COUNT_FAIL);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void checkIn(FacilityCheckInRequestDto requestDto, String userId, boolean isMember) {
        long resvId = requestDto.getResvId();
        Long[] facilityNoList = requestDto.getFacilityNo();
        MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao
                .getMemberFacilityTypeBooking(resvId);

        validateCheckInRecord(memberFacilityTypeBooking);
        validateCheckInStatus(memberFacilityTypeBooking);
        validateCheckInDate(memberFacilityTypeBooking);
        validateFacilityTypeQty(facilityNoList, memberFacilityTypeBooking);

        isMember = isMember(isMember, memberFacilityTypeBooking);
        Set<Long> targetFacilityNoSet = new HashSet<>(Arrays.asList(facilityNoList));
        List<FacilityTimeslot> facilityTimeslots = memberFacilityTypeBooking.getFacilityTimeslots();
        Map<Long, List<FacilityTimeslot>> originalTimeslotsMap = getFacilityTimeslotsMap(facilityTimeslots);

        Set<Long> checkedFacilityNum = getCheckedInFacilityNum(originalTimeslotsMap);
        int checkedInCount = checkedFacilityNum.size();
        Set<Long> originalFacilityNoSet = originalTimeslotsMap.keySet();

        Map<Long, List<FacilityTimeslot>> timeslotsMap = new HashMap<>();
        timeslotsMap.putAll(originalTimeslotsMap);
        Map<Long, Long> bayMap = new HashMap<>();
        // to find the change one
        for (Long targetFacilityNo : facilityNoList) {
            // no change bay
            if (originalFacilityNoSet.contains(targetFacilityNo)) {
                bayMap.put(targetFacilityNo, targetFacilityNo);
                originalFacilityNoSet.remove(targetFacilityNo);
                targetFacilityNoSet.remove(targetFacilityNo);
            }
        }

        originalFacilityNoSet.removeAll(checkedFacilityNum);
        Iterator<Long> originalIterator = originalFacilityNoSet.iterator();
        Iterator<Long> targetIterator = targetFacilityNoSet.iterator();

        while (originalIterator.hasNext() && targetIterator.hasNext()) {
            Long targetFacilityNo = targetIterator.next();
            Long originalFacilityNo = originalIterator.next();
            bayMap.put(targetFacilityNo, originalFacilityNo);
        }

        validateCheckInStatus(isMember, memberFacilityTypeBooking, bayMap, timeslotsMap);

        for (Map.Entry<Long, Long> entry : bayMap.entrySet()) {
            Long targetfacilityNo = entry.getKey();
            Long originalFacilityNo = entry.getValue();

            FacilityMaster targetFacilityMaster = facilityMasterDao.get(FacilityMaster.class, targetfacilityNo);
            FacilityMaster originalFacilityMaster = facilityMasterDao.get(FacilityMaster.class, originalFacilityNo);

            if (targetFacilityMaster == null) {
                throw new GTACommonException(GTAError.FacilityError.CHECK_IN_ERROR);
            }

            if (!targetfacilityNo.equals(originalFacilityNo)) {
                // update target
                updateFacilityTimeslot(userId, targetfacilityNo, originalFacilityMaster, timeslotsMap, null);

            }
            // update original
            updateFacilityTimeslot(userId, originalFacilityNo, targetFacilityMaster, timeslotsMap, FacilityStatus.OP.getDesc());

            saveAttendance(memberFacilityTypeBooking, timeslotsMap, originalFacilityNo);
            /***
             * @author christ 2016-06-29
             * @desc when golf-bay check in ,if beginTime <=current<=endTime turn on golf bay and add record to the ZoomTech Database
             * SGG-1889
             */
            if(Constant.FACILITY_TYPE_GOLF.equals(memberFacilityTypeBooking.getResvFacilityType())){
            	turnOnGolfBay(memberFacilityTypeBooking, targetFacilityMaster.getFacilityNo());
            }
           /* Cancel the operate with golf middle ware
            * if (Constant.FACILITY_TYPE_GOLF.equals(memberFacilityTypeBooking.getResvFacilityType())) {
                try {
                	// Change to send a closed command if overtime, besides, would not check-in function in front end if overtime 
                	if(memberFacilityTypeBooking.getBeginDatetimeBook().before(new Date())){
                		golfMachineService.setTeeupMachineStatsbyCancelTime(targetFacilityMaster.getFacilityNo(), memberFacilityTypeBooking.getBeginDatetimeBook(), memberFacilityTypeBooking.getEndDatetimeBook());
                	}
                    //golfMachineService.setTeeupMachineStatsbyCheckinTime(targetFacilityMaster.getFacilityNo(), memberFacilityTypeBooking.getBeginDatetimeBook(), memberFacilityTypeBooking.getEndDatetimeBook());
                    
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    throw new GTACommonException(GTAError.FacilityError.CHECK_IN_ERROR);
                }
            }*/

            checkedInCount++;
            //push  msg to member and  coach
            pushFacilityCheckedInMsg(memberFacilityTypeBooking, targetfacilityNo);
        }

        if (checkedInCount == memberFacilityTypeBooking.getFacilityTypeQty()) {
            memberFacilityTypeBooking.setStatus(MemberFacilityTypeBookingStatus.ATN.name());
            memberFacilityTypeBooking.setUpdateBy(userId);
            memberFacilityTypeBooking.setUpdateDate(new Date());
            memberFacilityTypeBookingDao.update(memberFacilityTypeBooking);
        }

    }
    /***
     * when golf-bay check in ,if beginTime <=current<=endTime  turn on  golf bay  and add record to the ZoomTech Database  
     * @param memberFacilityTypeBooking
     * @param targetFacilityMaster
     */
    
	 private void turnOnGolfBay(MemberFacilityTypeBooking memberFacilityTypeBooking, Long facilityNo){
    	try {
        	if(memberFacilityTypeBooking.getBeginDatetimeBook().before(new Date())&&memberFacilityTypeBooking.getEndDatetimeBook().after(new Date())){
               golfMachineService.setTeeupMachineStatsbyCheckinTime(facilityNo, memberFacilityTypeBooking.getBeginDatetimeBook(), memberFacilityTypeBooking.getEndDatetimeBook());
        	}
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            zoomtechLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
            throw new GTACommonException(GTAError.FacilityError.CHECK_IN_ERROR);
        }
    	
    }

    private void validateCheckInStatus(boolean isMember, MemberFacilityTypeBooking memberFacilityTypeBooking,
                                       Map<Long, Long> bayMap, Map<Long, List<FacilityTimeslot>> timeslotsMap) {

        Set<Long> originalFacilityNoSet = new HashSet<>(bayMap.values());
        for (Map.Entry<Long, Long> entry : bayMap.entrySet()) {
            Long targetfacilityNo = entry.getKey();

            String timeslotStatus = getTimeslotStatus(memberFacilityTypeBooking, originalFacilityNoSet, targetfacilityNo, timeslotsMap, isMember);
            // check availability
            if (!(FacilityStatus.VC.getDesc().equals(timeslotStatus) || FacilityStatus.TA.getDesc().equals(timeslotStatus))) {
                throw new GTACommonException(GTAError.FacilityError.CHECK_IN_ERROR);
            }
        }
    }

    private void updateFacilityTimeslot(String userId, Long originalFacilityNo, FacilityMaster targetFacilityMaster,
                                        Map<Long, List<FacilityTimeslot>> timeslotsMap, String status) {

        List<FacilityTimeslot> originalFacilityList = timeslotsMap.get(originalFacilityNo);
        // update original
        for (FacilityTimeslot facilityTimeslot : originalFacilityList) {
            facilityTimeslot.setFacilityMaster(targetFacilityMaster);
            if (status != null) {
                facilityTimeslot.setStatus(status);
            }
            facilityTimeslot.setUpdateBy(userId);
            facilityTimeslot.setUpdateDate(new Date());
            facilityTimeslotDao.update(facilityTimeslot);
        }
    }

    @SuppressWarnings("unchecked")
    private void saveAttendance(MemberFacilityTypeBooking memberFacilityTypeBooking,
                                Map<Long, List<FacilityTimeslot>> timeslotsMap, Long originalFacilityNo) {
        // add attendance data
        MemberFacilityAttendance memberFacilityAttendance = new MemberFacilityAttendance();
        MemberFacilityAttendancePK attendancePK = new MemberFacilityAttendancePK();
        attendancePK.setCustomerId(memberFacilityTypeBooking.getCustomerId());
        List<FacilityTimeslot> originalFacilityList = timeslotsMap.get(originalFacilityNo);

        if (!originalFacilityList.isEmpty()) {
            FacilityTimeslot facilityTimeslot = originalFacilityList.get(0);
            attendancePK.setFacilityTimeslotId(facilityTimeslot.getFacilityTimeslotId());
            memberFacilityAttendance.setId(attendancePK);
            memberFacilityAttendanceDao.saveOrUpdate(memberFacilityAttendance);
        }
    }

    private void pushFacilityCheckedInMsg(MemberFacilityTypeBooking memberFacilityTypeBooking, Long targetfacilityNo) {
        if (memberFacilityTypeBooking != null && memberFacilityTypeBooking.getCustomerId() > 0
                && memberFacilityTypeBooking.getExpCoachUserId() != null) {

            try {
                CustomerProfile customerProfile = customerProfileDao.getById(memberFacilityTypeBooking.getCustomerId());
                if (customerProfile != null) {
                    Date begin = new Date(memberFacilityTypeBooking.getBeginDatetimeBook().getTime());
                    memberFacilityTypeBooking.getResvFacilityType();
                    /*
                     * Your facility for private coaching of tennis is checked in. Please remember to come to 
                     * {facilityNo} at {startTime} on {bookingDate}
                     */
                    MessageTemplate messageTemplate;
                    if (memberFacilityTypeBooking.getResvFacilityType().equalsIgnoreCase("GOLF")) {
                        // change to same as front end
                        messageTemplate = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_GOLF_COACH_CHANGE);
                    } else {
                        messageTemplate = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_TENNIS_COACH_CHANGE);
                    }

                    String content = messageTemplate.getContent();
                    String startTime = Integer.toString(DateCalcUtil.getHourOfDay(begin)) + ":00";
                    content = replaceMsgContent(targetfacilityNo, begin,
                            content, startTime);
                    logger.error("FacilityMasterServiceImpl  pushFacilityCheckedInMsg to member run start ...resvId:"
                            + memberFacilityTypeBooking.getResvId());
                    devicePushService.pushMessage(new String[]{memberFacilityTypeBooking.getCustomerId() + ""}, content, "member");
                    logger.error("FacilityMasterServiceImpl  pushFacilityCheckedInMsg to member run end ...resvId:"
                            + memberFacilityTypeBooking.getResvId());
                    //push checkin msg to member end 

                }
            } catch (Exception e) {
                logger.error("FacilityMasterServiceImpl pushFacilityCheckedInMsg failed!", e);
            }
        }
    }

    private String replaceMsgContent(Long targetfacilityNo, Date begin,
                                     String content, String startTime) throws Exception {
        content = content.replaceAll(MessageTemplateParams.BookingDate.getParam(), DateCalcUtil.formatDate(begin))
                .replaceAll(MessageTemplateParams.StartTime.getParam(), startTime)
                .replaceAll(MessageTemplateParams.FacilityNo.getParam(), Long.toString(targetfacilityNo));
        return content;
    }
    
    /**
     * 重载方法 【添加aqcdto可控制是否显示canelled】
     * dto  isShowCancelled:
     *  if true  include all booking status
     *  else false the excluded booking status Cancel
     * @param resvId
     * @param dto
     * @return
     */
    @Transactional
    @Override
    public ResponseResult returnCheckedInFacilitiesByResvId(long resvId, AdvancedQueryConditionsDto dto) {
        MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(resvId, dto);

        FacilityCheckInDto<Object> facilityCheckInDto = new FacilityCheckInDto<>();
        facilityCheckInDto.setCount(memberFacilityTypeBooking.getFacilityTypeQty());
        List<FacilityTimeslot> facilityTimeslots = memberFacilityTypeBooking.getFacilityTimeslots();
        Map<Long, List<FacilityTimeslot>> originalTimeslotsMap = getFacilityTimeslotsMap(facilityTimeslots);
        Set<Long> checkedFacilityNum = getCheckedInFacilityNum(originalTimeslotsMap);
        facilityCheckInDto.setCheckedInCount((long) checkedFacilityNum.size());

        getCheckedInFacilities(facilityCheckInDto, checkedFacilityNum);

        responseResult.initResult(GTAError.Success.SUCCESS, facilityCheckInDto);
        return responseResult;
    }
    /***
     * get MemberFacilityTypeBooking by resvId 
     * include all status
     * 
     */
    @Override
    @Transactional
	public ResponseResult getFacilitiesVenueByResvId(long resvId) {
    	//get MemberFacilityTypeBooking by resvId
		MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao
				.get(MemberFacilityTypeBooking.class, resvId);
		FacilityCheckInDto<Object> facilityCheckInDto = new FacilityCheckInDto<>();
		//set count
		facilityCheckInDto.setCount(memberFacilityTypeBooking.getFacilityTypeQty());
		//get  facilityLot
		List<FacilityTimeslot> facilityTimeslots = memberFacilityTypeBooking.getFacilityTimeslots();
		Map<Long, List<FacilityTimeslot>> originalTimeslotsMap = getFacilityTimeslotsMap(facilityTimeslots);
		
		Set<Long> checkedFacilityNum = getCheckedInFacilityNum(originalTimeslotsMap);
		facilityCheckInDto.setCheckedInCount((long) checkedFacilityNum.size());

		//set checkedInFacilities to facilityCheckInDto
		getCheckedInFacilities(facilityCheckInDto, checkedFacilityNum);

		responseResult.initResult(GTAError.Success.SUCCESS, facilityCheckInDto);
		return responseResult;
	}
    
}
