package com.sinodynamic.hkgta.service.crm.backoffice.presentation;

import com.sinodynamic.hkgta.dto.crm.FolderInfoDto;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.Data;


public interface PresentationFolderOperationService extends IServiceBase<ServerFolderMapping> {
	String DEFAULT_PATH_PREFIX_KEY = "image.server.material";
	boolean createFolder(String folderName,String userId) throws Exception;
	boolean renameFolder(String originalFolderName, String newFolderName,String userId) throws Exception;
	Data getFoldersInfo(String propertyName, String order,int pageSize, int currentPage) throws Exception;
	FolderInfoDto getFolderInfo(Long id) throws Exception;
	public ServerFolderMapping getServerFolderMappingById(Long id);
}
