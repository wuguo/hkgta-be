package com.sinodynamic.hkgta.service.crm.sales.leads;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.CustomerLeadNotesDao;
import com.sinodynamic.hkgta.dto.crm.CustomerLeadNoteDto;
import com.sinodynamic.hkgta.entity.crm.CustomerLeadNote;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class LeadCustomerNoteServiceImpl extends ServiceBase<CustomerLeadNote> implements LeadCustomerNoteService {

	@Autowired
	private CustomerLeadNotesDao customerLeadNotesDao;

	@Transactional
	public Data listCustomerLeadNotes(Long customerId, ListPage<CustomerLeadNote> page) throws Exception {
		
		ListPage<CustomerLeadNote> notes = customerLeadNotesDao.listByCustomerId(customerId, page);
		List<Object> noteDtos = notes.getDtoList();;
		
		Data data = new Data();
		data.setList(noteDtos);
		data.setTotalPage(page.getAllPage());;
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		
		return data;
	}

	@Transactional(rollbackFor = Exception.class)
	public ResponseResult saveCustomerLeadNote(
			CustomerLeadNoteDto customerLeadNoteDto) throws GTACommonException, Exception {
		Long sysId = customerLeadNoteDto.getSysId();
		
		CustomerLeadNote customerLeadNote = null;
		Date currentDate = new Date();
		String userId = customerLeadNoteDto.getLoginUserId();
		String subject = customerLeadNoteDto.getSubject();
		String content = customerLeadNoteDto.getContent();
		String noteDateStr = customerLeadNoteDto.getNoteDate();
		Date noteDate = null;
		if (StringUtils.isEmpty(noteDateStr)) {
			noteDate = currentDate;
		} else {
			noteDate = DateCalcUtil.parseDate(noteDateStr);
		}
        //if sysId is null, then create a new Note.
        if (sysId == null){
        	customerLeadNote = new CustomerLeadNote();
        	customerLeadNote.setContent(content);
        	customerLeadNote.setSubject(subject);
        	customerLeadNote.setNoteDate(noteDate);
        	customerLeadNote.setCustomerId(Long.valueOf(customerLeadNoteDto.getCustomerId()));
        	customerLeadNote.setCreateDate(currentDate);
        	customerLeadNote.setCreateBy(userId);
        	customerLeadNote.setUpdateDate(currentDate);
        	customerLeadNote.setUpdateBy(userId);
        	
        	customerLeadNotesDao.save(customerLeadNote);	
        	
        //else find and edit the Note.	
        } else {
         	customerLeadNote = customerLeadNotesDao.getCustomerLeadNoteById(sysId);
        	if (customerLeadNote == null){
        		responseResult.initResult(GTAError.LeadError.NOTE_IS_NOT_EXISTING);
        		return responseResult;
        	}
        	String author = customerLeadNote.getCreateBy();
        	if (!author.equals(userId)){
        		responseResult.initResult(GTAError.LeadError.NOTE_PERMISSION_ERROR);
        		return responseResult;
        	}

        	customerLeadNote.setContent(content);
        	customerLeadNote.setSubject(subject);
        	customerLeadNote.setNoteDate(noteDate);
        	customerLeadNote.setUpdateDate(currentDate);
        	customerLeadNote.setUpdateBy(userId);
        	
        	customerLeadNotesDao.update(customerLeadNote);
        }
        //no exception, return success
        responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Transactional
	public boolean deleteCustomerLeadNote(Long id, String deleteBy) throws GTACommonException, Exception {
		
		CustomerLeadNote customerLeadNote = customerLeadNotesDao.getCustomerLeadNoteById(id);
		if (customerLeadNote == null){
			throw new GTACommonException(GTAError.LeadError.NOTE_IS_NOT_EXISTING);
    	}
		
		String author = customerLeadNote.getCreateBy();
    	if (!deleteBy.equals(author)){
    		throw new GTACommonException(GTAError.LeadError.NOTE_PERMISSION_ERROR);
    	}
		
		return customerLeadNotesDao.deleteById(id);
	}

	@Transactional
	public CustomerLeadNote getCustomerLeadNoteDetail(Long id) throws Exception{
		return customerLeadNotesDao.getCustomerLeadNoteById(id);
	}

}
