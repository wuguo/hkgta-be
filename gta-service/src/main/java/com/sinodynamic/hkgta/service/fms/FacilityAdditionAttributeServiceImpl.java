package com.sinodynamic.hkgta.service.fms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityAdditionAttributeDao;
import com.sinodynamic.hkgta.dao.fms.FacilityAttributeCaptionDao;
import com.sinodynamic.hkgta.entity.fms.FacilityAdditionAttribute;
import com.sinodynamic.hkgta.entity.fms.FacilityAdditionAttributePK;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;

@Service
public class FacilityAdditionAttributeServiceImpl extends
		ServiceBase<FacilityAdditionAttribute> implements FacilityAdditionAttributeService {
	@Autowired
	private FacilityAttributeCaptionDao facilityAttributeCaptionDao;
	
	@Autowired
	private FacilityAdditionAttributeDao facilityAdditionAttributeDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveFacilityAdditionAttribute(int facilityNo, String ballFeedingStatus)
	{
		FacilityAttributeCaption facilityAttributeCaption = facilityAttributeCaptionDao.get(FacilityAttributeCaption.class,Constant.BALLFEEDSWITCH);
		if(null == facilityAttributeCaption){
			facilityAttributeCaption = new FacilityAttributeCaption();
			facilityAttributeCaption.setAttributeId(Constant.BALLFEEDSWITCH);
			facilityAttributeCaption.setFacilityType("GOLF");
			facilityAttributeCaption.setCaption("Ball Feeding Machine");
			facilityAttributeCaption.setAttrValueType("TEXT");
			facilityAttributeCaption.setStatus(Constant.General_Status_ACT);
			facilityAttributeCaption.setOnBookScreen("Y");
			facilityAttributeCaption.setDefaultValue("OFF");
			facilityAttributeCaptionDao.save(facilityAttributeCaption);
		}
		FacilityAdditionAttributePK attributepk = new FacilityAdditionAttributePK();
		attributepk.setAttributeId(Constant.BALLFEEDSWITCH);
		attributepk.setFacilityNo(new Long(facilityNo));
		FacilityAdditionAttribute facilityAdditionAttribute = facilityAdditionAttributeDao.get(FacilityAdditionAttribute.class, attributepk);
		if(null == facilityAdditionAttribute){
			facilityAdditionAttribute = new FacilityAdditionAttribute();
			facilityAdditionAttribute.setId(attributepk);
			facilityAdditionAttribute.setInputValue(ballFeedingStatus);
			facilityAdditionAttributeDao.save(facilityAdditionAttribute);
		}else{
			facilityAdditionAttribute.setInputValue(ballFeedingStatus);
			facilityAdditionAttributeDao.update(facilityAdditionAttribute);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public FacilityAdditionAttribute getFacilityAdditionAttribute(int facilityNo, String attributeId)
	{
		FacilityAdditionAttributePK attributepk = new FacilityAdditionAttributePK();
		attributepk.setAttributeId(attributeId);
		attributepk.setFacilityNo(new Long(facilityNo));
		return facilityAdditionAttributeDao.get(FacilityAdditionAttribute.class, attributepk);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public FacilityAdditionAttribute getFacilityAdditionAttributeLike(Long facilityNo,String attributeId)
	{
		return facilityAdditionAttributeDao.getFacilityAdditionAttribute(facilityNo, attributeId);
	}

	@Override
	@Transactional
	public List<FacilityAdditionAttribute> getFacilityAdditionAttributeList(String attributeId)
	{
		return facilityAdditionAttributeDao.getFacilityAdditionAttributeList(attributeId);
	}

	@Override
	@Transactional
	public List<Long> getFacilityAttributeFacilityNoList(String attributeId)
	{
		List<FacilityAdditionAttribute> attributes = facilityAdditionAttributeDao.getFacilityAdditionAttributeList(attributeId);
		List<Long> facilityNos = new ArrayList<Long>();
		if(null != attributes && attributes.size()>0){
			for(FacilityAdditionAttribute attri : attributes){
				facilityNos.add(attri.getId().getFacilityNo());
			}
		}
		return facilityNos;
	}


}
