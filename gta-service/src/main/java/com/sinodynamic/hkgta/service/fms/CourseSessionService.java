package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.util.Date;

import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CourseSessionService extends IServiceBase<Serializable> {
	
	public ResponseResult getCoachSessionList(String staffId, Date fromDate, Date endDate);
	
	public ResponseResult getAttendanceMemberList(String sessionId);
	
	public ResponseResult getSessionMembers(String courseId, String sessionNo);
}
