package com.sinodynamic.hkgta.service.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.CommonDataQueryByHQLDao;
import com.sinodynamic.hkgta.dao.CommonDataQueryBySQLDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GTAErrorCode;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service("advanceQueryService")
@Scope("prototype")
public class AdvanceQueryServiceImpl implements AdvanceQueryService {

	@Autowired
	private CommonDataQueryByHQLDao	commonByHQLDao;

	@Autowired
	private CommonDataQueryBySQLDao	commonBySQLDao;

	@Autowired
	@Qualifier("transactionlistcondition")
	AdvanceQueryConditionDao		advanceQueryConditionDao;

	@Autowired
	@Qualifier("courseListConditions")
	AdvanceQueryConditionDao		advanceQueryConditionCourseDao;

	@Autowired
	@Qualifier("courseMembersConditions")
	AdvanceQueryConditionDao		advanceQueryConditionCourseMembersDao;

	@Autowired
	@Qualifier("restaurantMenuCondition")
	AdvanceQueryConditionDao		advanceQueryConditionRestaurantMenuCatDao;

	@Autowired
	@Qualifier("tempPassConditions")
	AdvanceQueryConditionDao		TempPassConditionsDao;

	@Autowired
	@Qualifier("tempPassHistoryConditions")
	AdvanceQueryConditionDao		TempPassHistoryConditionsDao;

	@Autowired
	@Qualifier("permitCardConditions")
	AdvanceQueryConditionDao		permitCardConditionsDao;

	@Autowired
	@Qualifier("helpPassTypeListConditions")
	AdvanceQueryConditionDao		helpPassTypeListConditionsDao;

	@Autowired
	@Qualifier("mmsOrderListConditions")
	AdvanceQueryConditionDao		mmsOrderListConditionsDao;

	@Autowired
	@Qualifier("spaRetreatConditions")
	AdvanceQueryConditionDao		spaRetreatConditionsDao;

	@Autowired
	@Qualifier("spaRetreatItemConditions")
	AdvanceQueryConditionDao		spaRetreatItemConditionsDao;

	@Autowired
	@Qualifier("spaRefundItemConditions")
	AdvanceQueryConditionDao		spaRefundItemConditionsDao;

	@Autowired
	private SysCodeDao				sysCodeDao;

	@Autowired
	private MessageSource			messageSource;

	@Override
	@Transactional
	public ResponseResult getAdvanceQueryResultByHQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page) {
		commonByHQLDao.getAdvanceQueryResult(queryDto, joinSQL, page);
		return assembleResponseResult(page);
	}

	@Override
	@Transactional
	public ResponseResult getAdvanceQueryResultByHQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto) {
		commonByHQLDao.getAdvanceQueryResult(queryDto, joinSQL, page, dto);
		return assembleResponseResult(page);
	}

	@Override
	@Transactional
	public ResponseResult getAdvanceQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto) {
		commonBySQLDao.getAdvanceQueryResult(queryDto, joinSQL, page, dto);
		return assembleResponseResult(page);
	}

	@Override
	@Transactional
	public ResponseResult getAdvanceQuerySpecificResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dtoClass) {
		commonBySQLDao.getAdvanceQuerySpecificResult(queryDto, joinSQL, page, dtoClass);
		return assembleResponseResult(page);
	}

	@Override
	@Transactional
	public ResponseResult getInitialQueryResultByHQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page) {
		commonByHQLDao.getInitialQueryResultByHQL(queryDto, joinSQL, page);
		return assembleResponseResult(page);
	}

	@Transactional
	public ResponseResult getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page) {
		commonByHQLDao.getInitialQueryResultBySQL(queryDto, joinSQL, page);
		return assembleResponseResult(page);
	}

	public ResponseResult assembleResponseResult(ListPage page) {
		ResponseResult responseResult = new ResponseResult(messageSource);

		if (page!=null  && page.getList() != null && page.getList().size() >= 0) {
			Data data = new Data();
			data.setList(page.getList());
			data.setTotalPage(page.getAllPage());
			data.setCurrentPage(page.getNumber());
			data.setPageSize(page.getSize());
			data.setRecordCount(page.getAllSize());
			data.setLastPage(page.isLast());

			responseResult.initResult(GTAError.Success.SUCCESS, data);
		} else {
			responseResult.initResult(GTAError.CommomError.NO_DATA_FOUND);
		}

		return responseResult;
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category) throws Exception {
		List<AdvanceQueryConditionDto> list = commonByHQLDao.assembleQueryConditions(category);
		return setSysCodeData(list);
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category, AdvanceQueryConditionDao advanceQueryDao) throws Exception {
		List<AdvanceQueryConditionDto> list = advanceQueryDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	/**
	 * 根据类型构造不同的高级查询条件
	 * 
	 * @param category
	 * @param advanceQueryDao
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category, AdvanceQueryConditionDao advanceQueryDao, String type)
			throws Exception {
		List<AdvanceQueryConditionDto> list = advanceQueryDao.assembleQueryConditions(type);
		return setSysCodeData(list);
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category, String type) throws Exception {
		List<AdvanceQueryConditionDto> list = advanceQueryConditionDao.assembleQueryConditions(type);
		return setSysCodeData(list);
	}

	private List<AdvanceQueryConditionDto> setSysCodeData(List<AdvanceQueryConditionDto> list) throws Exception {
		List<AdvanceQueryConditionDto> queryConditions = new ArrayList<>();
		for (AdvanceQueryConditionDto dto : list) {
			queryConditions.add(dto);
			if (dto.getOptions() == null) {
				String selectName = dto.getSelectName();
				if (StringUtils.isNotEmpty(selectName) && !selectName.contains(",")) {
					dto.setOptions(sysCodeDao.selectSysCodeByCategory(selectName));
				}
				if (selectName.contains(",")) {
					List<SysCode> options = new ArrayList<>();
					String[] codes = selectName.split(",");
					for (String code : codes) {
						SysCode sysCode = new SysCode();
						sysCode.setCategory(dto.getDisplayName());
						sysCode.setCodeDisplay(code.trim());
						sysCode.setCodeValue(code.trim());
						options.add(sysCode);
					}
					dto.setSelectName(dto.getDisplayName());
					dto.setOptions(options);
				}
			}
		}
		return queryConditions;
	}

	@Override
	@Transactional
	public ResponseResult mappingCustomerByCardNo(Long cardNo) {
		ResponseResult responseResult = new ResponseResult(messageSource);
		if (!StringUtils.isNumeric(String.valueOf(cardNo))) {
			throw new RuntimeException(GTAErrorCode.CARD_MAPPING_FAILED.getCode());
		}
//		String joinHQL = "select   new com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto( p.cardPurpose as cardType,m.customerId as customerId, c.salutation as salutation, c.givenName as givenName, c.surname as surname, m.memberType as memberType, m.academyNo as academyNo, c.portraitPhoto as portraitPhoto, m.status as status) "
//				+ " from PermitCardMaster p ,Member m ,CustomerProfile c "
//				+ "	where  p.mappingCustomerId=m.customerId and c.customerId=m.customerId  and p.cardNo=" + cardNo;
//		
		String joinSql=" SELECT   p.card_purpose AS cardType,m.customer_id AS customerId,"
		 +" c.salutation AS salutation, c.given_name AS givenName, c.surname AS surname, m.member_type AS memberType,"
		 +" m.academy_no AS academyNo, c.portrait_photo AS portraitPhoto, m.status AS status"
		 +" FROM permit_card_master p ,member m ,customer_profile c "
		 +" WHERE p.mapping_customer_id=m.customer_id"
		 +" AND c.customer_id=m.customer_id  AND p.card_no="+cardNo;
		 
		List<ScanCardMappingDto>list=commonByHQLDao.getDtoBySql(joinSql, null, ScanCardMappingDto.class);
		 //(ScanCardMappingDto) commonByHQLDao.getUniqueBySQL(joinSql,null);
		
		if (null==list||list.size()==0)
		{
			responseResult.initResult(GTAError.CommonError.MEMBER_NO_CARD);
			return responseResult;
		}
		ScanCardMappingDto cardMapping =list.get(0);
		cardMapping.setMemberName();
		responseResult.initResult(GTAError.CommomError.GET_MEMBER_BYQRCODE_SUCC, cardMapping);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto) {
		commonBySQLDao.getInitialQueryResultBySQL(queryDto, sql, page, dto);
		return assembleResponseResult(page);
	}

	@Override
	@Transactional
	public ResponseResult getAdvanceQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, List<Object> paramList) {
		commonBySQLDao.getAdvanceQueryResult(queryDto, joinSQL, page, dto, paramList);
		return assembleResponseResult(page);
	}

	@Override
	@Transactional
	public ResponseResult getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto, List<Object> paramList) {
		commonBySQLDao.getInitialQueryResultBySQL(queryDto, sql, page, dto, paramList);
		return assembleResponseResult(page);
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleCourseQueryConditions(String courseType) throws Exception {
		List<AdvanceQueryConditionDto> list = advanceQueryConditionCourseDao.assembleQueryConditions(courseType);
		return setSysCodeData(list);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleTemppassQueryConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = TempPassConditionsDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleTemppassHistoryQueryConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = TempPassHistoryConditionsDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleHelpPassTypeQueryConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = helpPassTypeListConditionsDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleMmsOrderQueryConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = mmsOrderListConditionsDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assemblePermitCardQueryConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = permitCardConditionsDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	public String getSearchCondition(AdvanceQueryDto queryDto, String joinSQL) {
		return commonBySQLDao.getSearchCondition(queryDto, joinSQL);
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleCourseMembersQueryConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = advanceQueryConditionCourseMembersDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	@Transactional
	public ResponseResult getAdvanceQueryCustomizedResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto) {
		commonBySQLDao.getAdvanceQueryResult(queryDto, joinSQL, page, dto);
		return assembleCustomizedResponseResult(page);
	}

	public ResponseResult assembleCustomizedResponseResult(ListPage page) {
		ResponseResult responseResult = new ResponseResult(messageSource);

		Data data = new Data();
		data.setList(page.getList());
		data.setTotalPage(page.getAllPage());
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());

		responseResult.initResult(GTAError.Success.SUCCESS, data);

		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getInitialQueryCustomizedResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto) {
		commonBySQLDao.getInitialQueryResultBySQL(queryDto, sql, page, dto);
		return assembleCustomizedResponseResult(page);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleRestaurantMenuQueryConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = advanceQueryConditionRestaurantMenuCatDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	@Transactional
	public ResponseResult getAdvanceQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, Map<String, Type> typeMap) {
		commonBySQLDao.getAdvanceQueryResult(queryDto, joinSQL, page, dto, typeMap);
		return assembleResponseResult(page);
	}

	@Override
	@Transactional
	public ResponseResult getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, Map<String, Type> typeMap) {
		commonBySQLDao.getInitialQueryResultBySQL(queryDto, joinSQL, page, dto, typeMap);
		return assembleResponseResult(page);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleSpaRetreatConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = spaRetreatConditionsDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleSpaRetreatItemConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = spaRetreatItemConditionsDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleSpaRefundItemConditions() throws Exception {
		List<AdvanceQueryConditionDto> list = spaRefundItemConditionsDao.assembleQueryConditions();
		return setSysCodeData(list);
	}

}