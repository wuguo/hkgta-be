package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.AbstractCommonDataQueryDao;
import com.sinodynamic.hkgta.dao.crm.CashvalueTopupHistoryDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.pms.RoomTypeDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.AllTransactionDto;
import com.sinodynamic.hkgta.dto.crm.ApproveTopUpDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.TopUpDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.rpos.CashValueLogDto;
import com.sinodynamic.hkgta.dto.rpos.CashValueSumDto;
import com.sinodynamic.hkgta.dto.rpos.UpadeCashValueDto;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.ReservationType;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.report.JasperUtil;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.jasperreports.engine.JRException;

@Service
@Scope("prototype")
public class MemberTransactionServiceImpl extends ServiceBase<MemberCashvalue> implements MemberTransactionService{
	private Logger ecrLog = Logger.getLogger(LoggerType.ECRLOG.getName());
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private CashvalueTopupHistoryDao topupHistoryDao;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;

	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private RoomTypeDao roomTypeDao;
	
	@Autowired
	@Qualifier("commonBySQLDao")
	AbstractCommonDataQueryDao abstractCommonDataQueryDao;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;

	@Transactional
	public ResponseResult getCurrentBalance(Long customerId){
		Member member = memberDao.getMemberById(customerId);
		if(member==null){
			//return new ResponseResult("1", "Cannot retrieve the member informartion!");
			responseResult.initResult(GTAError.MemberShipError.CANNOT_RETRIEVE_MEMBER);
			return responseResult;
		}
		String memberType = member.getMemberType();
		if(memberType.equals("IDM")||memberType.equals("CDM")){
			customerId = member.getSuperiorMemberId();//superior customer id
		}
		MemberCashvalue memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
		
		if(memberCashvalue==null){
			//return new ResponseResult("0", "Successfully Retrieved!", 0);
			responseResult.initResult(GTAError.Success.SUCCESS);
			responseResult.setData(0);
			return responseResult;
		}
        
		//return new ResponseResult("0", "Successfully Retrieved!", memberCashvalue.getAvailableBalance());
		responseResult.initResult(GTAError.Success.SUCCESS);
		responseResult.setData(memberCashvalue.getAvailableBalance());
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult getTransactionList(Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending,String clientType, AdvanceQueryDto filters){
			try {
				ListPage<CustomerOrderTrans> page = new ListPage<CustomerOrderTrans>();
				if(CommUtil.notEmpty(pageNumber)){
					page.setNumber(Integer.parseInt(pageNumber));
				}
				if(CommUtil.notEmpty(pageSize)){
					page.setSize(Integer.parseInt(pageSize));
				}
				
				String filterSql = null;
				if(filters != null){
					filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
				}
				ListPage<CustomerOrderTrans> list = customerOrderTransDao.getTransactionList(page,customerID,pageNumber,pageSize,sortBy,isAscending,clientType,filterSql);
				Data data = new Data();
				data.setList(list.getDtoList());
				data.setTotalPage(page.getAllPage());;
				data.setCurrentPage(page.getNumber());
				data.setPageSize(page.getSize());
				data.setRecordCount(page.getAllSize());
				data.setLastPage(page.isLast());
				responseResult.initResult(GTAError.Success.SUCCESS,data);
				return responseResult;
			} catch (NumberFormatException e) {
				e.printStackTrace();
				responseResult.initResult(GTAError.CommomError.DATA_ISSUE);
				return responseResult;
			}
		
	}
	@Transactional
	public ListPage getAllTransactionList(Long customerID,
			ListPage page ,String clientType,AdvanceQueryDto query){
		 String filterSql = "";
		  if(query != null){
			    filterSql = abstractCommonDataQueryDao.getSearchCondition(query, "");
			    if(!StringUtils.isEmpty(filterSql)){
			    	filterSql =" AND "+filterSql;
			    }
		 }        
		List<Object> param = new ArrayList<Object>();
		
		String sql = "select * from ("+getExpensesSql(param,customerID)
				+ "   UNION ALL "+getPopUpSql(param,customerID)
				+ "   UNION ALL "+getServiceRefundAndCatchValueRefund(param,customerID)
				+ "   UNION ALL "+getCachValueAdjuestment(param,customerID)
				+ "   UNION ALL "+getVoidPaymentSql(param,customerID,"ALL")
				+ "  ) temp where 1=1 "+filterSql;
		
		String countSql = "select count(1) count from ("+sql +") temp";
		ListPage list = customerOrderTransDao.listBySqlDto(page, countSql, sql, param, new AllTransactionDto());
		//set type
		for(Object object:list.getDtoList()){
			AllTransactionDto temp = (AllTransactionDto)object;
			for(Constant.ServiceItem item : Constant.ServiceItem.values()){
				if(item.name().equals(temp.getItemCatagory())){
					temp.setItemCatagory(Constant.ServiceItem.valueOf(temp.getItemCatagory()).toString());
				}
			}
			if(temp.getItemCatagory().equals("RESTAURANT"))
			{
				if(temp.getTransactionType().equals("Void")){
					temp.setItemCatagory(temp.getDescription()+"(Void)");
					temp.setDescription(temp.getDescription()+"(Void)");
				}else{
					temp.setItemCatagory(temp.getDescription());	
				}
			}
			if(temp.getItemCatagory().equals(Constant.ServiceItem.MMS_SPA.toString()))
			{
				if(temp.getTransactionType().equals("Void")){
					temp.setItemCatagory(temp.getItemCatagory()+"(Void)");
					temp.setDescription(temp.getDescription()+"(Void)");
				}
			}
			//MMS:Wellness Center-incoice:2003 to MMS Invoice #201
			if(!StringUtils.isEmpty(temp.getDescription())&&temp.getDescription().contains("MMS:Wellness Center-incoice"))
			{
				String descs[]=temp.getDescription().split(":");
				temp.setDescription(descs[0]+" Invoice #"+descs[2]);
			}
		}
		return list;
	}
	
	@Transactional
	public ListPage getVoidPaymentList(Long customerID,
			ListPage page ,String clientType,AdvanceQueryDto query){
		 String filterSql = "";
		  if(query != null){
			    filterSql = abstractCommonDataQueryDao.getSearchCondition(query, "");
			    if(!StringUtils.isEmpty(filterSql)){
			    	filterSql =" AND "+filterSql;
			    }
		 }        
		List<Object> param = new ArrayList<Object>();
		
		String sql = "select * from ("+getVoidPaymentSql(param,customerID,null)
				+ "  ) temp where 1=1 "+filterSql;
		
		String countSql = "select count(1) count from ("+sql +") temp";
		ListPage list = customerOrderTransDao.listBySqlDto(page, countSql, sql, param, new AllTransactionDto());
		//set type
		for(Object object:list.getDtoList()){
			AllTransactionDto temp = (AllTransactionDto)object;
			for(Constant.ServiceItem item : Constant.ServiceItem.values()){
				if(item.name().equals(temp.getItemCatagory())){
					temp.setItemCatagory(Constant.ServiceItem.valueOf(temp.getItemCatagory()).toString());
				}
			}
		}
		return list;
	}
	private String getVoidPaymentSql(List param,Long customerID,String display){
		StringBuilder sb=new StringBuilder();
		sb.append(" SELECT" );  
				sb.append("  DATE_FORMAT(trans.transaction_timestamp,'%Y-%b-%d %H:%i') AS transactionDate," ); 
				sb.append(" trans.transaction_timestamp AS transactionTimestamp," ); 
				sb.append(" trans.transaction_no AS transactionNo," ); 
				sb.append("   'Void' AS transactionType ," ); 
				sb.append("     trans.payment_method_code AS  paymentMethodCode," ); 
			    sb.append("      trans.internal_remark AS description," ); 
				sb.append("  CONCAT_WS(' ', pro.salutation, pro.given_name, pro.surname ) AS memberName," ); 
				sb.append("  trans.paid_amount AS paidAmount," ); 
				sb.append("  (SELECT psip.item_catagory FROM pos_service_item_price psip, customer_order_det det WHERE hd.order_no = det.order_no AND psip.item_no = det.item_no LIMIT 1) AS itemCatagory " );	
				if(!"ALL".equals(display)){
					sb.append(" ,trans.from_transaction_no as fromTransactionNo ");	
				}
				sb.append("  FROM  customer_order_trans trans" ); 
				sb.append("  LEFT JOIN customer_order_hd hd  ON trans.order_no = hd.order_no " ); 
//				sb.append("  LEFT JOIN customer_order_det det  ON hd.order_no = det.order_no  " ); 
				sb.append("  LEFT JOIN customer_profile pro  ON pro.customer_id = hd.customer_id" ); 
				sb.append("  LEFT JOIN member mem  ON mem.customer_id = hd.customer_id  " ); 
//				sb.append("  LEFT JOIN pos_service_item_price psip  ON psip.item_no = det.item_no" ); 
				sb.append("  WHERE 1=1 ");
				sb.append("  AND trans.payment_method_code='"+PaymentMethod.CASHVALUE.getCardCode()+"'");
				sb.append("  and exists (select 1 from customer_order_det det where hd.order_no = det.order_no AND det.item_no in ( '"+Constant.PMSTHIRD_ITEM_NO+"' ,'"+Constant.POSTHIRD_ITEM_NO+"', '"+Constant.MMSTHIRD_ITEM_NO+"'))");
//				sb.append("  AND psip.item_no in ('"+Constant.PMSTHIRD_ITEM_NO+"' ,'"+Constant.POSTHIRD_ITEM_NO+"', '"+Constant.MMSTHIRD_ITEM_NO+"')");
				sb.append("  AND ( mem.customer_id=? OR mem.superior_member_id=?)");
				sb.append("  AND  trans.status='VOID'  ");	
				
		param.add(customerID);
		param.add(customerID);
		return sb.toString();
	}
	
	public String getExpensesSql(List param,Long customerID){ 
		
		StringBuilder bderSql=new StringBuilder();
		
		bderSql.append(" SELECT DATE_FORMAT(c.transaction_timestamp,  '%Y-%b-%d %H:%i') AS transactionDate,\n");
		bderSql.append("  c.transaction_timestamp as transactionTimestamp, \n");
		bderSql.append("  c.transaction_no AS transactionNo,\n");
		bderSql.append("  'Expenses' transactionType, \n");
		bderSql.append(" CASE  WHEN c.payment_Media = 'OP'  AND c.payment_method_code IN \n");
		bderSql.append(" ( ");
	    bderSql.append("   '"+PaymentMethod.VISA.name()+"',\n");
		bderSql.append("  '"+PaymentMethod.MASTER.name()+"',\n");
		bderSql.append("  '"+PaymentMethod.CUP.name()+"',\n");
		bderSql.append("  '"+PaymentMethod.AMEX.name()+"',\n");
		bderSql.append("  '"+PaymentMethod.JCB.name()+"'\n");
		bderSql.append("  )  THEN CONCAT(c.payment_method_code, '(online)')  ELSE c.payment_method_code END AS paymentMethodCode,\n");
		bderSql.append(" CASE ");
		bderSql.append(" WHEN c.ext_ref_no<>'' and c.internal_remark<>'' THEN c.internal_remark \n");
		bderSql.append(" WHEN c.ext_ref_no<>'' and c.internal_remark is null THEN   CONCAT('MMS:Wellness Center-incoice:',c.ext_ref_no)  \n");
		bderSql.append(" WHEN ps.item_catagory = 'RESTAURANT' OR ps.item_catagory = 'OASIS_ROOM'  THEN c.internal_remark \n");
		bderSql.append(" WHEN ps.item_catagory='GFBK' THEN 'Golfing Bay' \n");
		bderSql.append(" WHEN ps.item_catagory='TFBK' THEN 'Tennis Court' \n");
		bderSql.append(" WHEN ps.item_catagory='TS'  THEN 'Tennis Private Coaching' \n");
		bderSql.append(" WHEN ps.item_catagory='GS'  THEN  'Golf Private Coaching' \n");
		bderSql.append(" WHEN ps.item_catagory='SRV' THEN  'Service Plan' \n");
		bderSql.append(" WHEN ps.item_catagory='DP'  THEN  'Day Pass Purchasing' \n");
		bderSql.append(" WHEN ps.item_catagory='GSS' THEN 'Golf Course Enrollment' \n");
		bderSql.append(" WHEN ps.item_catagory='TSS' THEN 'Tennis Course Enrollment' \n");
		bderSql.append(" WHEN ps.item_catagory='PMS_ROOM' THEN 'Guest Room' \n");
		
		bderSql.append( " ELSE ps.description \n" );
		bderSql.append( " END as description,\n");
		bderSql.append(	" CONCAT(  p.salutation, ' ', p.given_Name, ' ', p.surname ) AS memberName, \n");
		bderSql.append("  c.paid_amount AS paidAmount , \n");
		bderSql.append("  case when ps.item_catagory = 'POS_THIRD' or ps.item_catagory = 'PMS_THIRD' then c.internal_remark else ps.item_catagory end as itemCatagory ");
		bderSql.append("  FROM \n");
		bderSql.append("  customer_order_trans c,   \n");
		bderSql.append("  customer_order_hd h, \n");
		bderSql.append("  customer_profile p,  \n");
		bderSql.append("  customer_order_det d,\n");
		bderSql.append("  member m,   \n");
		bderSql.append("  pos_service_item_price ps \n");
		bderSql.append("  WHERE c.order_no = h.order_no  AND h.order_no = d.order_no  \n");
		bderSql.append("  AND (c.status = 'SUC'  OR c.status = 'RFU')   \n");
		bderSql.append("  AND h.customer_Id = m.customer_Id \n");
		bderSql.append("  AND m.customer_Id = p.customer_Id \n");
		bderSql.append("  AND ( m.customer_Id = ?   OR m.superior_Member_Id = ? ) \n");
		bderSql.append("  AND d.item_No = ps.item_No     \n");
		bderSql.append("  AND ps.item_catagory  not in ('TOPUP','REFUND')  \n");
		bderSql.append("  and not exists " );
		bderSql.append("  ( ");
		bderSql.append("    SELECT 1 FROM member_facility_type_booking m  \n");
		bderSql.append("    LEFT JOIN room_facility_type_booking room_booking ON room_booking.facility_type_resv_id = m.resv_id  \n");
		bderSql.append("    WHERE m.customer_id = ?  AND room_booking.is_bundle = 'Y' and m.order_no = c.order_no \n");
		bderSql.append("  ) \n");
		bderSql.append(" AND d.order_Det_Id = \n");
		bderSql.append(" ( ");
		bderSql.append("  SELECT   MIN(dd.order_Det_Id)   \n");
		bderSql.append("   FROM  \n");
		bderSql.append("    customer_order_det dd   \n");
	    bderSql.append("   WHERE dd.order_no = h.order_no  \n");
	    bderSql.append("  AND dd.item_No <> 'SCVD00000001' \n");
	    bderSql.append("  AND dd.item_No <> 'SCVR00000001' \n" );
	    bderSql.append(" )  \n");
		param.add(customerID);
		param.add(customerID);
		param.add(customerID);
		return bderSql.toString();
	}
	
	public String getPopUpSql(List param,Long customerID){
		String sql = 
			"	SELECT                                                                                       "+
			"	  DATE_FORMAT( c.transaction_timestamp,'%Y-%b-%d %H:%i') AS transactionDate,                 "+
			"    c.transaction_timestamp  as transactionTimestamp,     "+
			"	  c.transaction_No AS transactionNo,                                                         "+
			"	  'Top Up' transactionType,                                                                  "+
			"	  CASE                                                                                       "+
			"	  WHEN c.payment_Media = 'OP'                                                                "+
			"	  AND c.payment_Method_Code IN ("
			+ "'"+PaymentMethod.VISA.name()+"', "
			+ "'"+PaymentMethod.MASTER.name()+"', "
			+ "'"+PaymentMethod.AMEX.name()+"', "
			+ "'"+PaymentMethod.CUP.name()+"', "
			+ "'"+PaymentMethod.JCB.name()+"'"
			+ ")                                     "+
			"	  THEN CONCAT(      c.payment_Method_Code,      '(online)'    )                              "+
			"	  ELSE c.payment_Method_Code                                                                 "+
			"	  END AS paymentMethodCode,                                                                  "+
			"	  'Top Up' description,                                                                            "+
			"	  CONCAT(    p.salutation,    ' ',    p.given_Name,    ' ',    p.surname  ) AS memberName,   "+
			"	  c.paid_Amount AS paidAmount ,                                                               "+
			"     'Top Up' as itemCatagory "+
			"	FROM                                                                                         "+
			"	  customer_order_trans c,                                                                    "+
			"	  customer_order_hd h,                                                                       "+
			"	  customer_profile p,                                                                        "+
			"	  customer_order_det d,                                                                      "+
			"	  member m                                                                                   "+
			"	WHERE c.order_No = h.order_No                                                                "+
			"	  AND h.order_No = d.order_No                                                                "+
			"	  AND d.item_No = 'CVT0000001'                                                               "+
			"	  AND h.customer_Id = m.customer_Id                                                          "+
			"	  AND c.status = 'SUC'                                                                       "+
			"	  AND m.customer_Id = p.customer_Id                                                          "+
			"	  AND (                                                                                      "+
			"	    m.customer_Id = ?                                                                       "+
			"	    OR m.superior_Member_Id = ?                                                             "+
			"	  )                                                                                          "+
			"	  AND d.order_Det_Id =                                                                       "+
			"	  (SELECT                                                                                    "+
			"	    MIN(dd.order_Det_Id)                                                                     "+
			"	  FROM                                                                                       "+
			"	    customer_order_det dd                                                                    "+
			"	  WHERE dd.order_No = h.order_No)                                                            ";
		param.add(customerID);
		param.add(customerID);
		return sql;
	}
	
	public String getServiceRefundAndCatchValueRefund(List param,Long customerID){
		String sql = 
			"	SELECT  DATE_FORMAT( requestDate,'%Y-%b-%d %H:%i') AS transactionDate, \n"+
			"    requestDate as transactionTimestamp,    \n"+
			"	  newTransactionNo transactionNo,        \n"+
			"	  CASE WHEN refundFrom <> 'CVL' THEN 'Service Refund' ELSE 'Cash Value Refund' END  transactionType,   \n"+
			"	  '"+PaymentMethod.CASHVALUE.name()+"' paymentMethodCode,                 \n"+
			"	   refundFrom AS description , memberName, approvedAmt paidAmount, itemCatagory \n"+
			"	FROM    "+
			"	  (SELECT  "+
			"	    req.refund_id refundId,   \n "+
			"	    hd.customer_id customerId, \n"+
			"	    req.create_date requestDate, \n"+
			"	    req.refund_transaction_no refundTransactionNo,  \n"+
			"		CASE req.refund_service_type  \n"+
			"		WHEN '"+ReservationType.ROOM.getDesc().toUpperCase()+"' THEN \n"+
			" 		( 	"+
		    "  		 SELECT   trans.transaction_no  \n"+
			"	 	 FROM customer_order_trans AS  trans  \n"+
			"	 	 INNER JOIN customer_order_hd AS coh ON coh.order_no=trans.order_no \n"+
			"	 	 INNER JOIN customer_order_det AS det ON det.order_no=coh.order_no \n"+
			"	     WHERE  trans.from_transaction_no=req.refund_transaction_no \n"+
			"	     AND det.ext_ref_no=cod.ext_ref_no \n"+              
		    "        ) \n"+
			"		ELSE  \n"+
			"	    (SELECT t_.transaction_no FROM customer_order_trans t_ WHERE t_.from_transaction_no = req.refund_transaction_no)     \n"+
			"		END as newTransactionNo ,\n"
//			"	    req.refund_service_type refundFrom, "
			+ " CASE req.refund_service_type"
			+ " WHEN '"+ReservationType.ROOM.getDesc().toUpperCase()+"' THEN  req.refund_service_type \n"
			+ " ELSE "
			+ "  ("
			+ "			SELECT "
			+"				CASE WHEN t_.internal_remark IS NULL THEN CONCAT('MMS Invoice #',t_.ext_ref_no)	"
			+"				ELSE t_.internal_remark  end as internal_remark "
			//+ "				t_.internal_remark"
			+ "			 FROM customer_order_trans t_ WHERE t_.transaction_no = req.refund_transaction_no"
			+ "	 ) \n"
			+ "  end as refundFrom,  \n"+
			"	    CONCAT( pro.salutation, ' ', pro.given_name, ' ', pro.surname ) memberName,  \n"+
			"	    req.status,                         \n"+
			"	    tran.paid_amount paidAmt,           \n"+
			"	    req.request_amt requestAmt,         \n"+
			"	    req.approved_amt approvedAmt,       \n"+
			"	    req.internal_remark remark   ,      \n"+
//			"       'Refund' as itemCatagory \n"+
			"        CASE req.refund_service_type  when '"+ReservationType.WELLNESS.name()+"' then CONCAT('Refund From ','"+ReservationType.WELLNESS.getDesc()+"')    \n"+
			"        else CONCAT('Refund From ',req.refund_service_type)"
			+ "      end as  itemCatagory	" +

			"	  FROM                                  \n"+
			"	    customer_refund_request req         \n"+
			"		LEFT JOIN customer_order_det cod  ON req.order_det_id=cod.order_det_id \n"+
			"	    JOIN customer_order_trans tran      \n"+
			"	      ON tran.transaction_no = req.refund_transaction_no    \n"+
			"	    JOIN customer_order_hd hd           \n"+
			"	      ON hd.order_no = tran.order_no    \n"+
			"	    LEFT JOIN customer_profile pro      \n"+
			"	      ON pro.customer_id = hd.customer_id  \n"+
			"	  WHERE 1=1    "
			+ "     AND req.status = 'APR'     \n"+
			"	    AND hd.customer_id IN               \n"+
			"	    (SELECT                             \n"+
			"	      customer_id                       \n"+
			"	    FROM                                \n"+
			"	      member                            \n"+
			"	    WHERE customer_id = ?              \n"+
			"	      OR superior_member_id = ?)) sub  ";
		param.add(customerID);
		param.add(customerID);
		return sql;
	}
	
	public String getCachValueAdjuestment(List param,Long customerID){
		
		String sql = 
			"	SELECT                                                  "+
			"	  DATE_FORMAT(trans.transaction_timestamp,'%Y-%b-%d %H:%i') AS transactionDate,       "+
			"    trans.transaction_timestamp as transactionTimestamp,     "+
			"	  trans.transaction_no AS transactionNo,                "+
			"	  CASE WHEN det.item_no = 'SCVR00000001' THEN 'Credit' ELSE 'Debit' END AS transactionType,             "+
			"	  trans.payment_method_code AS paymentMethodCode,       "+
			"	   CONCAT('Cash Value - Adjustment - ', CASE WHEN det.item_no = 'SCVR00000001' THEN 'Credit' ELSE 'Debit' END ) description,       "+
			"	    CONCAT_WS(                                          "+
			"	    ' ',                                                "+
			"	    pro.salutation,                                     "+
			"	    pro.given_name,                                     "+
			"	    pro.surname                                         "+
			"	  ) AS memberName,                                      "+
			"	  trans.paid_amount AS transAmount,                      "+
			"	 'Cach Value Adjustment' as  itemCatagory       		"+
			"	                                                        "+
			"	FROM                                                    "+
			"	  customer_order_trans trans                            "+
			"	  LEFT JOIN customer_order_hd hd                        "+
			"	    ON trans.order_no = hd.order_no                     "+
			"	  LEFT JOIN customer_order_det det                      "+
			"	    ON hd.order_no = det.order_no                       "+
			"	  LEFT JOIN customer_profile pro                        "+
			"	    ON pro.customer_id = hd.customer_id                 "+
			"	  LEFT JOIN member mem                                  "+
			"	    ON mem.customer_id = hd.customer_id                 "+
			"	  LEFT JOIN customer_enrollment enroll                  "+
			"	    ON enroll.customer_id = hd.customer_id              "+
			"	  LEFT JOIN staff_profile sp                            "+
			"	    ON sp.user_id = hd.create_by                        "+
			/***
			 * SGG-3845
			 * have no relation  member_limit_rule and service_plan and service_plan_pos and pos_service_item_price
			 * and no relation to search
			 */
//			"	  LEFT JOIN member_limit_rule mlr                       "+
//			"	    ON mlr.customer_id = hd.customer_id   AND mlr.limit_type = 'CR'  AND mlr.expiry_date >=  curdate()   "+
//			"	  LEFT JOIN service_plan plan                           "+
//			"	    ON plan.plan_no =                                   "+
//			"	    (SELECT                                             "+
//			"	      service_plan                                      "+
//			"	    FROM                                                "+
//			"	      member_plan_facility_right mpfr                   "+
//			"	    WHERE mpfr.customer_id = hd.customer_id             "+
//			"	    LIMIT 1)                                            "+
//			"	  LEFT JOIN service_plan_pos pos                        "+
//			"	    ON pos.plan_no = plan.plan_no                       "+
//			"	  LEFT JOIN pos_service_item_price psip                 "+
//			"	    ON psip.item_no = pos.pos_item_no                   "+
			"	WHERE hd.customer_id = ?                               "+
			"	  AND (                                                 "+
			"	    det.item_no = 'SCVR00000001'                        "+
			"	    OR det.item_no = 'SCVD00000001'                     "+
			"	  )	                                                    ";
				
		param.add(customerID);		
		return sql;		
	} 
	
	
	
	
	
	
	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> transactionListAdvanceSearch() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date/Time", "date_format(ps.transactionTimestamp,'%Y-%m-%d')", "java.util.Date", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Transaction ID", "ps.transactionNo", "java.lang.Long", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Payment Method", "ps.paymentMethodCode", "java.lang.String", sysCodeDao.selectSysCodeByCategory("PAYMENTMETHOD"), 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Description", "ps.itemCatagory", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Patron", "ps.memberName", "java.lang.String", "", 5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Expenses", "ps.paidAmount", "java.math.BigDecimal", "", 6);
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5
				,condition6);
	}
	
	@Override
	@Transactional
	public List getAllTransactionListAdvancedSearch() {  
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date/Time", "STR_TO_DATE(transactionDate,'%Y-%M-%d')", "java.util.Date", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Transaction ID", "transactionNo", "java.lang.Long", "", 2);
		List<SysCode> list = new ArrayList();
		SysCode s1 = new SysCode("Expenses","Expenses","TransactionType");
		SysCode s2 = new SysCode("Top Up","Top Up","TransactionType");
		SysCode s3 = new SysCode("Service Refund","Service Refund","TransactionType");
		SysCode s4 = new SysCode("Catch Value Refund","Catch Value Refund","TransactionType");
		SysCode s5 = new SysCode("Cach Value Adjustment","Cach Value Adjustment","TransactionType");
		SysCode s6 = new SysCode("Void","Void Payment","TransactionType");
		
		list.add(s1);list.add(s2);list.add(s3);list.add(s4);list.add(s5);list.add(s6);
		
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Transaction Type", "transactionType", "java.lang.String", list, 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Transaction Method", "paymentMethodCode", "java.lang.String", sysCodeDao.selectSysCodeByCategory("PAYMENTMETHOD"), 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Description", "description", "java.lang.String", "", 5);
//		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Member", "CONCAT(  p.salutation, ' ', p.given_Name, ' ', p.surname )", "java.lang.String", "", 6);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Patron", "memberName", "java.lang.String", "", 6);
//		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Amount", "c.paid_amount", "java.math.BigDecimal", "", 7);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Amount", "paidAmount", "java.math.BigDecimal", "", 7);
		
		return Arrays.asList(condition1 ,condition2 ,condition3 ,condition4 ,condition5 ,condition6, condition7);
	}

	@Override
	@Transactional
	public ResponseResult getTopupHistory(Long customerID, String pageNumber,
			String pageSize, String sortBy, String isAscending, AdvanceQueryDto filters) {
		try {
			//ResponseResult responseResult = new ResponseResult("0","","",new Data());
			responseResult.initResult(GTAError.Success.SUCCESS, new Data());
			ListPage<CustomerOrderTrans> page = new ListPage<CustomerOrderTrans>();
			if(CommUtil.notEmpty(pageNumber)){
				page.setNumber(Integer.parseInt(pageNumber));
			}
			if(CommUtil.notEmpty(pageSize)){
				page.setSize(Integer.parseInt(pageSize));
			}
			
			String filterSql = null;
			if(filters != null){
				filterSql = abstractCommonDataQueryDao.getSearchCondition(filters, "");
			}
			ListPage<CustomerOrderTrans> list = customerOrderTransDao.getTopupHistory(page,customerID,pageNumber,pageSize,sortBy,isAscending,filterSql);
			List<Object> memberTransactions = list.getDtoList();
			
			Data data = new Data();
			data.setList(memberTransactions);
			data.setTotalPage(page.getAllPage());;
			data.setCurrentPage(page.getNumber());
			data.setPageSize(page.getSize());
			data.setRecordCount(page.getAllSize());
			data.setLastPage(page.isLast());
			responseResult.initResult(GTAError.Success.SUCCESS, data);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			//return new ResponseResult("1","Retrieve failed!");
			responseResult.initResult(GTAError.MemberShipError.RETRIEVE_FAILED);
			return responseResult;
		}
	}
	
	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> topupHistoryAdvanceSearch() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date/Time", "date_format(c.transactionTimestamp,'%Y-%b-%d')", "java.util.Date", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Transaction ID", "c.transactionNo", "java.lang.Long", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Payment Method", "c.paymentMethodCode", "java.lang.String", sysCodeDao.selectSysCodeByCategory("PAYMENTMETHOD"), 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Patron", "CONCAT(p.salutation, ' ',p.givenName, ' ',p.surname )", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Top Up", "c.paidAmount", "java.math.BigDecimal", "", 5);
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5);
	}
	
	@Transactional
	public ResponseResult topupByCashCard(TopUpDto topUpDto) {
		Date currentDate = new Date();
		String staffUserId = topUpDto.getStaffUserId();
		Long customerId = topUpDto.getCustomerId();
		String terminalId = null;
		String agentTransactionNo = null;
		BigDecimal topUpAmount = null;
		if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())
			||PaymentMethod.CUP.name().equalsIgnoreCase(topUpDto.getPaymentMethod())
			||PaymentMethod.AMEX.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			terminalId = topUpDto.getTerminalId();
			agentTransactionNo = topUpDto.getAgentTransactionNo();
			topUpAmount = topUpDto.getTopUpAmount();
		} else if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			topUpAmount = topUpDto.getTopUpAmount();
		}
		else {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_PAYMENT_METHOD);
			return responseResult;
		}
		int validate = topUpAmount.compareTo(BigDecimal.ZERO);
		if (validate!=1) {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_TOPUP_AMOUNT);
			return responseResult;
		}
		Member member = memberDao.getMemberById(topUpDto.getCustomerId());
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		// CUSTOMER ORDER Header
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(currentDate);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
		}else if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		}
		customerOrderHd.setStaffUserId(staffUserId);
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(topUpAmount);
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(staffUserId);
		customerOrderHd.setUpdateBy(staffUserId);
		customerOrderHd.setUpdateDate(currentDate);
		Long orderNo = (Long) customerOrderHdDao
				.addCustomreOrderHd(customerOrderHd);
		CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);

		// CUSTOMER ORDER Detail
		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.getByItemNo(Constant.TOPUP_ITEM_NO);
		if(posServiceItemPrice == null ){
			PosServiceItemPrice psip = new PosServiceItemPrice();
			psip.setStatus(Constant.General_Status_ACT);
			psip.setItemNo(Constant.TOPUP_ITEM_NO);
			psip.setItemCatagory("TOPUP");
			psip.setDescription("Top up for cash value");
			posServiceItemPriceDao.save(psip);
		}
		customerOrderDet.setCustomerOrderHd(temp);
		customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
		customerOrderDet.setItemRemark("");
		customerOrderDet.setOrderQty(1L);
		customerOrderDet.setItemTotalAmout(topUpAmount);
		customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderDet.setCreateBy(staffUserId);
		customerOrderDet.setUpdateDate(currentDate);
		customerOrderDet.setUpdateBy(staffUserId);
		customerOrderDetDao.saveOrderDet(customerOrderDet);

		// CUSTOMER ORDER TRANS
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		initCustomerOrderTrans(customerOrderTrans);
		
		customerOrderTrans.setCustomerOrderHd(temp);
		customerOrderTrans.setTransactionTimestamp(currentDate);
		customerOrderTrans.setPaidAmount(topUpAmount);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
		}else if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderTrans.setStatus(Constant.Status.PND.name());
			customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
			if(!StringUtils.isEmpty(terminalId)){
				customerOrderTrans.setTerminalId(terminalId);
			}
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
			if(!StringUtils.isEmpty(topUpDto.getTerminalType())&&
					PaymentMediaType.ECR.name().equalsIgnoreCase(topUpDto.getTerminalType())){
				customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
			}else{
				customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			}
		}
		if (CommUtil.notEmpty(staffUserId)) {
			customerOrderTrans.setPaymentRecvBy(staffUserId);
			customerOrderTrans.setCreateBy(staffUserId);
		}else {
			customerOrderTrans.setCreateBy(member.getUserId());
		}
		customerOrderTrans.setPaymentLocationCode(topUpDto.getTopUpLocation());
		customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			
			Long superiorMemberId = member.getSuperiorMemberId();
			MemberCashvalue memberCashvalue = null;
			if(superiorMemberId!=null){
				//if current member is a dependent member,should update superior member's cash value.
				memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
			}else {
				memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
			}
			if (memberCashvalue == null) {
				memberCashvalue = new MemberCashvalue();
				memberCashvalue.setInitialDate(currentDate);
				memberCashvalue.setInitialValue(topUpAmount);
				memberCashvalue.setExchgFactor(new BigDecimal(1));
				memberCashvalue.setInternalRemark("");
				memberCashvalue.setUpdateDate(currentDate);
				memberCashvalue.setUpdateBy(staffUserId);
				
				if (superiorMemberId != null) {
					//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
					memberCashvalue.setCustomerId(superiorMemberId);
				}else {
					memberCashvalue.setCustomerId(customerId);
				}
				memberCashvalue.setAvailableBalance(topUpAmount);
				memberCashValueDao.saveMemberCashValue(memberCashvalue);
			} else {
				memberCashvalue.setUpdateDate(currentDate);
				memberCashvalue.setUpdateBy(staffUserId);
				BigDecimal updatedAmount = memberCashvalue
						.getAvailableBalance().add(topUpAmount);
				memberCashvalue.setAvailableBalance(updatedAmount);
				memberCashValueDao.updateMemberCashValue(memberCashvalue);
			}
			topUpDto.setCustomerId(memberCashvalue.getCustomerId());//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.

			topUpDto.setTransactionNo(customerOrderTrans.getTransactionNo()); // SAM 20161013 added
			
			saveTopupHistory(topUpDto);
		}
		topUpDto.setCustomerId(customerId);//receipt email should be sent to current member.
		topUpDto.setTransactionNo(customerOrderTrans.getTransactionNo());
		responseResult.initResult(GTAError.Success.SUCCESS,customerOrderTrans);
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult topupByCard(TopUpDto topUpDto) {
		logger.info("create topUpByCard record start....");
		Date currentDate = new Date();
		String staffUserId = topUpDto.getStaffUserId();
		Long customerId = topUpDto.getCustomerId();
		String terminalId = topUpDto.getTerminalId();
		String agentTransactionNo = topUpDto.getAgentTransactionNo();
		BigDecimal topUpAmount = topUpDto.getTopUpAmount();
		int validate = topUpAmount.compareTo(BigDecimal.ZERO);
		if (validate!=1) {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_TOPUP_AMOUNT);
			return responseResult;
		}
		Member member = memberDao.getMemberById(topUpDto.getCustomerId());
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		logger.info("create  CustomerOrderHd status is "+Constant.Status.OPN.name()+" record.");
		// CUSTOMER ORDER Header
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(currentDate);
		customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		customerOrderHd.setStaffUserId(staffUserId);
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(topUpAmount);
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(staffUserId);
		customerOrderHd.setUpdateBy(staffUserId);
		customerOrderHd.setUpdateDate(currentDate);
		Long orderNo = (Long) customerOrderHdDao
				.addCustomreOrderHd(customerOrderHd);
		CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);

		// CUSTOMER ORDER Detail
		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		customerOrderDet.setCustomerOrderHd(temp);
		customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
		customerOrderDet.setItemRemark("");
		customerOrderDet.setOrderQty(1L);
		customerOrderDet.setItemTotalAmout(topUpAmount);
		customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderDet.setCreateBy(staffUserId);
		customerOrderDet.setUpdateDate(currentDate);
		customerOrderDet.setUpdateBy(staffUserId);
		customerOrderDetDao.saveOrderDet(customerOrderDet);
		logger.info("create  CustomerOrderDet  record. ");
		// CUSTOMER ORDER TRANS
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		initCustomerOrderTrans(customerOrderTrans);
		customerOrderTrans.setCustomerOrderHd(temp);
		customerOrderTrans.setTransactionTimestamp(currentDate);
		customerOrderTrans.setPaidAmount(topUpAmount);
		customerOrderTrans.setStatus(Constant.Status.PND.name());
		customerOrderTrans.setPaymentRecvBy(staffUserId);
		customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
		if(!StringUtils.isEmpty(terminalId)){
			customerOrderTrans.setTerminalId(terminalId);
		}
		if(PaymentMethod.AMEX.getCardCode().equals(topUpDto.getPaymentMethod()))
		{
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.AMEX.name());	
		}else{
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
		}
		customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
		customerOrderTrans.setCreateBy(staffUserId);
		customerOrderTrans.setCreateDate(currentDate);
		customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		logger.info("create  CustomerOrderTrans status is "+Constant.Status.PND.name()+" record..");
		responseResult.initResult(GTAError.Success.SUCCESS);
		responseResult.setData(customerOrderTrans);
		logger.info("create topUpByCard record end....");
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult handlePosPaymentResult(PosResponse posResponse) {
		Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
//		BigDecimal topUpAmount = new BigDecimal(Double.parseDouble(posResponse.getAmount().trim()));
		if (null == customerOrderTrans) {
			responseResult.initResult(GTAError.MemberShipError.TRANSACTION_NOT_EXISTS);
			ecrLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, responseResult.getErrorMessageEN(), null));
			return responseResult;
		}
		BigDecimal topUpAmount = customerOrderTrans.getPaidAmount();
		customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode().trim());
		customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber().trim());
		customerOrderTrans.setInternalRemark(posResponse.getResponseText().trim());
		//customerOrderTrans.setPaymentMethodCode(CommUtil.getCardType(posResponse.getCardType()));
		customerOrderTrans.setStatus(CustomerTransationStatus.SUC.getDesc());
		customerOrderTrans.setUpdateBy("System");
		customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
		
		customerOrderTrans.setPaymentMethodCode(CommUtil.getCardType(posResponse.getCardType()));// Card Type may be Master Visa or Union pay.
		
		CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
		if (null != customerOrderHd) {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderHd.getCustomerOrderTrans();
			BigDecimal totalAmount = BigDecimal.ZERO;
			for (CustomerOrderTrans tempTrans: customerOrderTranses) {
				if (CustomerTransationStatus.SUC.getDesc().equals(tempTrans.getStatus())) {
					totalAmount = totalAmount.add(tempTrans.getPaidAmount());
				}
			}
			if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount)==0) {
				customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
				customerOrderHd.setUpdateDate(new Date());
				customerOrderHdDao.update(customerOrderHd);
			}
		}
		Long customerId = customerOrderHd.getCustomerId();
		Member member = memberDao.getMemberById(customerId);
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			ecrLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, responseResult.getErrorMessageEN(), null));
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			ecrLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, responseResult.getErrorMessageEN(), null));
			return responseResult;
		}
		MemberCashvalue memberCashvalue = null;
		Member mb=null;
		Long superiorMemberId = member.getSuperiorMemberId();
		if(superiorMemberId!=null){
			memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
		    mb=memberDao.getMemberByCustomerId(superiorMemberId);
		}else {
			memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
		    mb=memberDao.getMemberByCustomerId(customerId);
		}
		
		Date currentDate = new Date();
		if (memberCashvalue == null) {
			memberCashvalue = new MemberCashvalue();
			memberCashvalue.setInitialDate(currentDate);
			memberCashvalue.setInitialValue(topUpAmount);
			memberCashvalue.setExchgFactor(new BigDecimal(1));
			memberCashvalue.setInternalRemark("");
			if (superiorMemberId != null) {
				//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
				memberCashvalue.setCustomerId(superiorMemberId);
			}else {
				memberCashvalue.setCustomerId(customerId);
			}
			memberCashvalue.setAvailableBalance(topUpAmount);
			memberCashValueDao.saveMemberCashValue(memberCashvalue);
			/***
			 * logger info
			 */
			ecrLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, "update memberCashvalue:member acadeNo:"+(null!=mb?mb.getAcademyNo():null))+",topUpAmount:"+topUpAmount+
					",customerId:"+(null!=memberCashvalue.getCustomerId()?memberCashvalue.getCustomerId():null));
		} else {
			BigDecimal updatedAmount = memberCashvalue
					.getAvailableBalance().add(topUpAmount);
			memberCashvalue.setAvailableBalance(updatedAmount);
			
			memberCashValueDao.updateMemberCashValue(memberCashvalue);
			/***
			 * logger info
			 */
			ecrLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, "update memberCashvalue:member acadeNo:"+(null!=mb?mb.getAcademyNo():null))+",topUpAmount:"+topUpAmount+
					",customerId:"+(null!=memberCashvalue.getCustomerId()?memberCashvalue.getCustomerId():null));
		}
		TopUpDto topUpDto = new TopUpDto();
		topUpDto.setCustomerId(memberCashvalue.getCustomerId());//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
		topUpDto.setTransactionNo(transactionNo);
		topUpDto.setPaymentMethod(CommUtil.getCardType(posResponse.getCardType()));
		topUpDto.setTopUpAmount(topUpAmount);
		saveTopupHistory(topUpDto);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	

	@Override
	@Transactional
	public Data getRecentServedMemberList(ListPage<Member> page, String sortBy, String isAscending, Long[] customerId) {
		ListPage<Member> memberOverviewList = memberDao.getRecentServedMemberList(page, sortBy, isAscending, customerId);

		Data data = new Data();
		data.setList(memberOverviewList.getDtoList());
		data.setLastPage(memberOverviewList.isLast());
		data.setCurrentPage(memberOverviewList.getNumber());
		data.setRecordCount(memberOverviewList.getAllSize());
		data.setPageSize(memberOverviewList.getSize());
		data.setTotalPage(memberOverviewList.getAllPage());
		
		return data;
	}
	
	@Override
	@Transactional
	public ResponseResult getMembersOverviewList(ListPage<Member> page, String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType){
		
		ListPage<Member> memberOverviewList = memberDao.getMemberList(page, sortBy, isAscending, customerId, status, expiry,planName,memberType);
		
		Data data = new Data();
		for(Object object: memberOverviewList.getDtoList()){
			DependentMemberDto dto = (DependentMemberDto) object;
			if(Constant.memberType.IPM.name().equals(dto.getMemberType())||Constant.memberType.CPM.name().equals(dto.getMemberType())){
				boolean dependentCreationRight = customerProfileService.getDependentCreationRight(dto.getEnrollStatus(), dto.getPlanNo(), dto.getCustomerId());
				dto.setDependentCreationRight(dependentCreationRight);
			}else{
				dto.setDependentCreationRight(false);
			}
			
			if(Constant.General_Status_NACT.equals(dto.getStatus())){
				dto.setDependentCreationRight(false);
				if(dto.getPassportNo().contains("-")){
					dto.setClosed(true);
				}
			}
		}
		data.setList(memberOverviewList.getDtoList());
		data.setLastPage(memberOverviewList.isLast());
		data.setCurrentPage(memberOverviewList.getNumber());
		data.setRecordCount(memberOverviewList.getAllSize());
		data.setPageSize(memberOverviewList.getSize());
		data.setTotalPage(memberOverviewList.getAllPage());
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
		
	}

	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> assembleQueryConditions(){
		List<SysCode> genderCodes = sysCodeDao.selectSysCodeByCategory("gender");
		List<SysCode> memberStatusCodes = sysCodeDao.selectSysCodeByCategory("memberstatus");
		return memberDao.assembleQueryConditions(genderCodes,memberStatusCodes);
	}

	@Override
	@Transactional
	public void sentTopupEmail(TopUpDto topUpDto) {
		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_TOPUP);
		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, topUpDto.getCustomerId());
		String to = customerProfile.getContactEmail();
		String copyto = null;
		String subject = template.getMessageSubject();
		String username = customerProfile.getSalutation()+" "+customerProfile.getGivenName()+" "+customerProfile.getSurname();
		Member member = memberDao.get(Member.class, topUpDto.getCustomerId());
		MemberCashvalue memberCashvalue = null;
		//if current member is a dependent member,should get superior member's cashvalue balance.
		if (null == member.getSuperiorMemberId()) {
			memberCashvalue = memberCashValueDao.getByCustomerId(topUpDto.getCustomerId());
		}else {
			memberCashvalue = memberCashValueDao.getByCustomerId(member.getSuperiorMemberId());
			CustomerProfile superiorCustomer = customerProfileDao.get(CustomerProfile.class, member.getSuperiorMemberId());
			copyto = superiorCustomer.getContactEmail();
		}
		String content = template.getContentHtml().replaceAll(MessageTemplateParams.UserName.getParam(), username);
		content = content.replaceAll(MessageTemplateParams.TopupMethod.getParam(), topUpDto.getPaymentMethod());
		
		/**
		 * SGG-3320
		 * if getAvailableBalance less 0
		   the minus sign should be placed before currency sign. i.e. "-HK$99.99" not "HK$-99.99 
		 */
		String currentBalance=null;
		if(memberCashvalue.getAvailableBalance().compareTo(BigDecimal.ZERO)<0)
		{
			currentBalance =  "-HK\\$ "+new DecimalFormat("#,###.00").format(memberCashvalue.getAvailableBalance().abs());
		}else{
			 currentBalance =  "HK\\$ "+new DecimalFormat("#,###.00").format(memberCashvalue.getAvailableBalance());
		}
		
		content = content.replaceAll(MessageTemplateParams.CurrentBalance.getParam(),currentBalance);
		byte[] receiptAttach = null;
		try {
			receiptAttach = customerOrderTransDao.getInvoiceReceipt(null, topUpDto.getTransactionNo().toString(),"topup");
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<byte[]> attachList = new ArrayList<byte[]>();
		attachList.add(receiptAttach);
		List<String> fileNameList = new ArrayList<String>();
		fileNameList.add("HKGTA_Receipt-"+topUpDto.getTransactionNo().toString()+".pdf");
		List<String> mineTypeList = new ArrayList<String>();
		mineTypeList.add("application/pdf");
		try {
//			MailSender.sendEmailWithBytesMineAttach(to, copyto,null,subject,content, mineTypeList,attachList,fileNameList);
			CustomerEmailContent customerEmailContent = new CustomerEmailContent();
			
			customerEmailContent.setContent(content);
			customerEmailContent.setRecipientCustomerId(member.getCustomerId().toString());
			customerEmailContent.setRecipientEmail(to);
			customerEmailContent.setSendDate(new Date());
			customerEmailContent.setSenderUserId(topUpDto.getStaffUserId());
			customerEmailContent.setSubject(subject);
			customerEmailContent.setCopyto(copyto);
			customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_ACCOUNT);
			customerEmailContentDao.save(customerEmailContent);
			
			CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
			customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
			customerEmailAttach.setAttachmentName(fileNameList.get(0));
			customerEmailAttachDao.save(customerEmailAttach);
			mailThreadService.sendWithResponse(customerEmailContent, attachList, mineTypeList, fileNameList);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Sent topup successfully email to user fail!");
		}
	}

	@Override
	@Transactional
	public void saveTopupHistory(TopUpDto topUpDto) {
		MemberCashvalue memberCashvalue = memberCashValueDao.get(MemberCashvalue.class, topUpDto.getCustomerId());
		CashvalueTopupHistory topupHistory = new CashvalueTopupHistory();
		Date currentDate = new Date();
		topupHistory.setAmount(topUpDto.getTopUpAmount());
		if (null != memberCashvalue && null != memberCashvalue.getExchgFactor()) {
			topupHistory.setCashvalueAmt(topUpDto.getTopUpAmount().multiply(memberCashvalue.getExchgFactor()));
		}else {
			topupHistory.setCashvalueAmt(topUpDto.getTopUpAmount());
		}
		topupHistory.setCustomerId(topUpDto.getCustomerId());
		topupHistory.setSuccessDate(currentDate);
		topupHistory.setTopupDate(currentDate);
		topupHistory.setCreateDate(currentDate);
		topupHistory.setTopupMethod(topUpDto.getPaymentMethod());
		
		// SAM 20161013 add: should set topup status and transaction no. as well
		topupHistory.setStatus(Constant.Status.SUC.toString() ); 
		topupHistory.setTransactionNo(topUpDto.getTransactionNo()); 
		
		topupHistoryDao.save(topupHistory);
		
		ecrLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, "update CashvalueTopupHistory TopupMethod:"+topUpDto.getPaymentMethod()+
				",customerId:"+(null!=topUpDto.getCustomerId()?topUpDto.getCustomerId():null)));
	}

	@Override
	@Transactional
	public ResponseResult getTotalAmount(Long customerId, String transactionType) {
		List<Serializable> param = new ArrayList<Serializable>();
		String sql = "";
		if ("EXP".equalsIgnoreCase(transactionType)) {
			sql = "SELECT\n" +
					"	SUM(c.paid_amount) AS totalAmount\n" +
					"FROM\n" +
					"	customer_order_trans c,\n" +
					"	customer_order_hd h,\n" +
					"	customer_order_det d,\n" +
					"	member m\n" +
					"WHERE\n" +
					"	h.order_no= c.order_no\n" +
					"AND h.order_no = d.order_no\n" +
					"AND d.item_no != 'CVT0000001'\n" +
					"AND d.item_no != 'SRR00000001'\n" +
					"AND d.item_no != 'CVR00000001'\n" +
					"AND d.item_no != 'SCVD00000001'\n" +
					"AND d.item_no != 'SCVR00000001'\n" +
					"AND (c. STATUS = 'SUC' OR c. STATUS = 'RFU')\n" +
					"AND h.customer_id = m.customer_id\n" +
					"AND (\n" +
					"	m.customer_id = ? \n" +
					"	OR m.superior_member_id = ? \n" +
					")\n" +
					"AND d.order_det_id = (\n" +
					"	SELECT\n" +
					"		min(dd.order_det_id)\n" +
					"	FROM\n" +
					"		customer_order_det dd\n" +
					"	WHERE\n" +
					"		dd.order_no = h.order_no\n" +
					")";
			param.add(customerId);
			param.add(customerId);
		}
		if ("TOP".equalsIgnoreCase(transactionType)) {
			sql = "SELECT\n" +
					"	SUM(c.paid_amount) AS totalAmount\n" +
					"FROM\n" +
					"	customer_order_trans c,\n" +
					"	customer_order_hd h,\n" +
					"	customer_order_det d,\n" +
					"	member m\n" +
					"WHERE\n" +
					"	h.order_no= c.order_no\n" +
					"AND h.order_no = d.order_no\n" +
					"AND d.item_no = 'CVT0000001'\n" +
					"AND c. STATUS = 'SUC' \n" +
					"AND h.customer_id = m.customer_id\n" +
					"AND (\n" +
					"	m.customer_id = ? \n" +
					"	OR m.superior_member_id = ? \n" +
					")\n" +
					"AND d.order_det_id = (\n" +
					"	SELECT\n" +
					"		min(dd.order_det_id)\n" +
					"	FROM\n" +
					"		customer_order_det dd\n" +
					"	WHERE\n" +
					"		dd.order_no = h.order_no\n" +
					")";
			param.add(customerId);
			param.add(customerId);
		}
		if ("SR".equalsIgnoreCase(transactionType)) {
			sql = "SELECT \n" +
					"    SUM(approvedAmt) AS totalAmount\n" +
					"FROM\n" +
					"    (SELECT \n" +
					"            req.refund_id refundId,\n" +
					"            hd.customer_id customerId,\n" +
					"            req.create_date requestDate,\n" +
					"            req.refund_transaction_no refundTransactionNo,\n" +
					"            req.refund_service_type refundFrom,\n" +
					"            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) memberName,\n" +
					"            req.status,\n" +
					"            tran.paid_amount paidAmt,\n" +
					"            req.request_amt requestAmt,\n" +
					"            req.approved_amt approvedAmt,\n" +
					"            req.internal_remark remark\n" +
					"    FROM\n" +
					"        customer_refund_request req\n" +
					"    JOIN customer_order_trans tran ON tran.transaction_no = req.refund_transaction_no\n" +
					"    JOIN customer_order_hd hd ON hd.order_no = tran.order_no\n" +
					"    LEFT JOIN customer_profile pro ON pro.customer_id = hd.customer_id\n" +
					"    WHERE req.refund_service_type <> 'CVL' \n" +
					//  update AND hd.customer_id = ?  to line 784 
					//  add dependent member totalAmount
					" AND hd.customer_id in ( SELECT customer_id FROM  member WHERE customer_id=? OR superior_member_id=? ) \n"+
					") sub";
			param.add(customerId);
			param.add(customerId);
		}
		if ("CVR".equalsIgnoreCase(transactionType)) {
			sql = "SELECT \n" +
					"    SUM(approvedAmt) AS totalAmount\n" +
					"FROM\n" +
					"    (SELECT \n" +
					"            req.refund_id refundId,\n" +
					"            hd.customer_id customerId,\n" +
					"            req.create_date requestDate,\n" +
					"            req.refund_transaction_no refundTransactionNo,\n" +
					"            req.refund_money_type refundMethod,\n" +
					"            req.refund_service_type refundFrom,\n" +
					"            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) memberName,\n" +
					"            req.status,\n" +
					"            tran.paid_amount paidAmt,\n" +
					"            tran.agent_transaction_no refNo,\n" +
					"            mem.academy_no academyNo,\n" +
					"            req.request_amt requestAmt,\n" +
					"            req.approved_amt approvedAmt,\n" +
					"            (select concat(staff.given_name, ' ', staff.surname) from staff_profile staff where staff.user_id = req.auditor_user_id) approvedBy,\n" +
					"            req.internal_remark remark\n" +
					"    FROM\n" +
					"        customer_refund_request req\n" +
					"    JOIN customer_order_trans tran ON tran.transaction_no = req.refund_transaction_no\n" +
					"    JOIN customer_order_hd hd ON hd.order_no = tran.order_no\n" +
					"    LEFT JOIN member mem ON mem.customer_id = hd.customer_id\n" +
					"    JOIN customer_profile pro ON pro.customer_id = mem.customer_id\n" +
					"    WHERE req.refund_service_type = 'CVL' AND hd.customer_id = ? AND req.status = 'APR'\n" +
					") sub";
			param.add(customerId);
		}
		
		
		
		BigDecimal totalAmount = (BigDecimal)memberCashValueDao.getUniqueBySQL(sql, param);
		responseResult.initResult(GTAError.Success.SUCCESS,totalAmount);
		return responseResult;
	}

	/*
	 * added by Kaster 20160127
	 */
	@Override
	@Transactional
	public ResponseResult topUpForCorporate(TopUpDto topUpDto) {
		Date currentDate = new Date();
		String staffUserId = topUpDto.getStaffUserId();
		Long customerId = topUpDto.getCustomerId();
		String terminalId = null;
		String agentTransactionNo = null;
		BigDecimal topUpAmount = null;
		
		if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			terminalId = topUpDto.getTerminalId();
			agentTransactionNo = topUpDto.getAgentTransactionNo();
			topUpAmount = topUpDto.getTopUpAmount();
		} else if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			topUpAmount = topUpDto.getTopUpAmount();
		} else if (PaymentMethod.CHEQUE.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			topUpAmount = topUpDto.getTopUpAmount();
			agentTransactionNo = topUpDto.getAgentTransactionNo();
		} else if (PaymentMethod.BT.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			topUpAmount = topUpDto.getTopUpAmount();
			agentTransactionNo = topUpDto.getAgentTransactionNo();
		} else {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_PAYMENT_METHOD);
			return responseResult;
		}
		int validate = topUpAmount.compareTo(BigDecimal.ZERO);
		if (validate != 1) {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_TOPUP_AMOUNT);
			return responseResult;
		}
		Member member = memberDao.getMemberById(topUpDto.getCustomerId());
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		if ((!member.getMemberType().equalsIgnoreCase(Constant.memberType.CPM.name())) && (!member.getMemberType().equalsIgnoreCase(Constant.memberType.CDM.name())) && (!member.getMemberType().equalsIgnoreCase(Constant.memberType.IPM.name())) && (!member.getMemberType().equalsIgnoreCase(Constant.memberType.IDM.name()))) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_TYPE_CANNOT_TOP_UP);
			return responseResult;
		}
		
		// CUSTOMER ORDER Header
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(currentDate);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
		} else if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		} else if (PaymentMethod.CHEQUE.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		} else if (PaymentMethod.BT.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		}
		customerOrderHd.setStaffUserId(staffUserId);
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(topUpAmount);
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(staffUserId);
		customerOrderHd.setUpdateBy(staffUserId);
		customerOrderHd.setUpdateDate(currentDate);
		Long orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(customerOrderHd);
		CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);

		// CUSTOMER ORDER Detail
		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		PosServiceItemPrice posServiceItemPrice = posServiceItemPriceDao.getByItemNo(Constant.TOPUP_ITEM_NO);
		if (posServiceItemPrice == null) {
			PosServiceItemPrice psip = new PosServiceItemPrice();
			psip.setStatus(Constant.General_Status_ACT);
			psip.setItemNo(Constant.TOPUP_ITEM_NO);
			psip.setItemCatagory("TOPUP");
			psip.setDescription("Top up for cash value");
			posServiceItemPriceDao.save(psip);
		}
		customerOrderDet.setCustomerOrderHd(temp);
		customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
		customerOrderDet.setItemRemark("");
		customerOrderDet.setOrderQty(1L);
		customerOrderDet.setItemTotalAmout(topUpAmount);
		customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderDet.setCreateBy(staffUserId);
		customerOrderDet.setUpdateDate(currentDate);
		customerOrderDet.setUpdateBy(staffUserId);
		customerOrderDetDao.saveOrderDet(customerOrderDet);

		// CUSTOMER ORDER TRANS
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		initCustomerOrderTrans(customerOrderTrans);
		customerOrderTrans.setCustomerOrderHd(temp);
		customerOrderTrans.setTransactionTimestamp(currentDate);
		customerOrderTrans.setPaidAmount(topUpAmount);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
		} else if (PaymentMethod.VISA.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderTrans.setStatus(Constant.Status.PND.name());
			customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
			if(!StringUtils.isEmpty(terminalId)){
				customerOrderTrans.setTerminalId(terminalId);
			}
			
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
			customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
		} else if (PaymentMethod.CHEQUE.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CHEQUE.name());
			customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
			customerOrderTrans.setStatus(Constant.Status.PND.name());
		} else if (PaymentMethod.BT.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.BT.name());
			customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
			customerOrderTrans.setStatus(Constant.Status.PND.name());
		}
		if (CommUtil.notEmpty(staffUserId)) {
			customerOrderTrans.setPaymentRecvBy(staffUserId);
			customerOrderTrans.setCreateBy(staffUserId);
		} else {
			customerOrderTrans.setCreateBy(member.getUserId());
		}
		customerOrderTrans.setPaymentLocationCode(topUpDto.getTopUpLocation());
		customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
			MemberCashvalue memberCashvalue = null;
			//added by Kaster 20160216
			//如果member是IDM/CDM，则找出对应的IPM/CPM，cash value的值是充到IPM/CPM的账户的。
			if(member.getMemberType().equalsIgnoreCase(Constant.memberType.IDM.name()) || member.getMemberType().equalsIgnoreCase(Constant.memberType.CDM.name())){
				customerId = member.getSuperiorMemberId();
			}
			memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
			if (memberCashvalue == null) {
				memberCashvalue = new MemberCashvalue();
				memberCashvalue.setInitialDate(currentDate);
				memberCashvalue.setInitialValue(topUpAmount);
				memberCashvalue.setExchgFactor(new BigDecimal(1));
				memberCashvalue.setInternalRemark("");
				memberCashvalue.setCustomerId(customerId);
				memberCashvalue.setAvailableBalance(topUpAmount);
				memberCashValueDao.saveMemberCashValue(memberCashvalue);
			} else {
				BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().add(topUpAmount);
				memberCashvalue.setAvailableBalance(updatedAmount);
				memberCashValueDao.updateMemberCashValue(memberCashvalue);
			}
			topUpDto.setCustomerId(memberCashvalue.getCustomerId());
			
			topUpDto.setTransactionNo(customerOrderTrans.getTransactionNo()); // SAM 20161013 added
			
			saveTopupHistory(topUpDto);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, customerOrderTrans);
		return responseResult;
	}

	/*
	 * added by Kaster 20160127
	 */
	@Override
	@Transactional
	public ResponseResult approveCorporateTopup(ApproveTopUpDto approveTopUpDto) {
		//modified by Kaster 20160216 新需求：IPM和IDM充值（非cash方式）也需要approve.
		//检查该customerId对应的member是否存在，客户类型是否为CPM/IPM/IDM且status为ACT
		Long customerId = approveTopUpDto.getCustomerId();
		Member member = memberDao.getMemberByCustomerId(customerId);
		if(member == null){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_IS_NOT_EXISTS);
			return responseResult;
		}else if((!member.getMemberType().equalsIgnoreCase(Constant.memberType.CPM.name())) && (!member.getMemberType().equalsIgnoreCase(Constant.memberType.CDM.name())) && (!member.getMemberType().equalsIgnoreCase(Constant.memberType.IPM.name())) && (!member.getMemberType().equalsIgnoreCase(Constant.memberType.IDM.name()))){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_IS_NOT_PRIMARY);
			return responseResult;
		}else if(!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		
		//检查当前的status与前端传递过来的status是否可以转换
		Long transactionNo = approveTopUpDto.getTransactionNo();
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		String oldStatus = customerOrderTrans.getStatus();
		String newStatus = approveTopUpDto.getStatus();
		//原状态为Approved(数据库中为SUC)的记录不能继续更改status；原状态为Rejected(数据库中为FAIL)的记录不能更改为Approved。
		if(oldStatus.equalsIgnoreCase("SUC") || (oldStatus.equalsIgnoreCase("FAIL") && newStatus.equalsIgnoreCase("SUC"))){
			responseResult.initResult(GTAError.CustomerOrderTrans.STATUS_CANNOT_TRANSFORM);
			return responseResult;
		}
		
		//更新customer_order_trans表的status
		if(newStatus.equalsIgnoreCase("SUC")){
			customerOrderTrans.setStatus("SUC");
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
			
			//更新customer_order_hd表中的order_status
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
			
			//在member_cashvalue表中更新available_balance
			MemberCashvalue memberCashvalue = null;
			//如果member type是IDM/CDM，则找出对应IPM/CPM，充值是充到IPM/CPM的账户上的。
			if(member.getMemberType().equalsIgnoreCase(Constant.memberType.IDM.name()) || member.getMemberType().equalsIgnoreCase(Constant.memberType.CDM.name())){
				customerId = member.getSuperiorMemberId();
			}
			memberCashvalue = memberCashValueDao.getMemberCashvalueById(customerId);
			if(memberCashvalue != null){
				BigDecimal oldAvailableBalance = memberCashvalue.getAvailableBalance();
				BigDecimal approvedAmount = customerOrderTrans.getPaidAmount();
				memberCashvalue.setAvailableBalance(oldAvailableBalance.add(approvedAmount));
				memberCashvalue.setUpdateDate(new Timestamp(new Date().getTime()));
			}else{
				memberCashvalue = new MemberCashvalue();
				memberCashvalue.setCustomerId(customerId);
				memberCashvalue.setInitialValue(new BigDecimal(0.00));
				memberCashvalue.setInitialDate(new Timestamp(new Date().getTime()));
				memberCashvalue.setAvailableBalance(customerOrderTrans.getPaidAmount());
				memberCashvalue.setExchgFactor(new BigDecimal(1.0000));
				
				memberCashValueDao.saveMemberCashValue(memberCashvalue);
			}
		}else if(newStatus.equalsIgnoreCase("FAIL")){
			customerOrderTrans.setStatus("FAIL");
		}else if(newStatus.equalsIgnoreCase("PND")){
			customerOrderTrans.setStatus("PND");
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	//added by Kaster 20160128
	@Override
	@Transactional
	public List<AdvanceQueryConditionDto> topUpSettlementAdvancedSearch() {
		SysCode statusOpt1 = new SysCode("SUC", "Approved", "Approved");
		SysCode statusOpt2 = new SysCode("FAIL", "Rejected", "Rejected");
		SysCode statusOpt3 = new SysCode("PND", "Pending", "Pending");
		
		SysCode topUpMethodOpt1 = new SysCode("BT", "Transcript", "Transcript");
		SysCode topUpMethodOpt2 = new SysCode("VISA", "Credit Card", "Credit Card");
		SysCode topUpMethodOpt3 = new SysCode("CASH", "Cash", "Cash");
		SysCode topUpMethodOpt4 = new SysCode("CHEQUE", "Cheque", "Cheque");
		
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Transaction ID", "c.transaction_no", BigInteger.class.getName(), "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Date/Time", "c.transaction_timestamp", Date.class.getName(), "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Patron ID", "m.academy_no", String.class.getName(), "", 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Patron Name", "CONCAT(p.salutation,' ',p.surname,' ',p.given_name)", String.class.getName(), "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Top Up Method", "c.payment_method_code", String.class.getName(), Arrays.asList(topUpMethodOpt1,topUpMethodOpt2,topUpMethodOpt3,topUpMethodOpt4), 5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Reference#", "c.agent_transaction_no", String.class.getName(), "", 6);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Top Up Amount","c.paid_amount",BigDecimal.class.getName(),"",7);
		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Status","c.status",String.class.getName(),Arrays.asList(statusOpt1,statusOpt2,statusOpt3),8);
		
		return Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8);
	}	
	
	/**
	 * staff更新member的CashValue
	 * @param dto
	 * @return
	 */
	@Transactional
	@Override
	public ResponseResult updateMemberCashValueByStaff(UpadeCashValueDto dto) {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date currentDate = new Date();
		String staffUserId = dto.getStaffUserId();
		Long customerId = dto.getCustomerId();
		String agentTransactionNo = dto.getAgentTransactionNo();
		BigDecimal topUpAmount =  dto.getAdjustmentAmount();
		int validate = topUpAmount.compareTo(BigDecimal.ZERO);
		if (validate!=1) {
			responseResult.initResult(GTAError.MemberShipError.INCORRECT_TOPUP_AMOUNT);
			return responseResult;
		}
		Member member = memberDao.getMemberById(customerId);
		if (member == null) {
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
			return responseResult;
		}
		if (!member.getStatus().equalsIgnoreCase(Constant.Member_Status_ACT)){
			responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		
//		if (PaymentMethod.CASH.name().equalsIgnoreCase(topUpDto.getPaymentMethod())) {
		Long superiorMemberId = member.getSuperiorMemberId();
		MemberCashvalue memberCashvalue = null;
		if(superiorMemberId!=null){
			//if current member is a dependent member,should update superior member's cash value.
			memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
		}else {
			memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
		}
		BigDecimal amount = new BigDecimal(0);
			if (memberCashvalue == null) {
				
				if (dto.getAdjustmentType().equals("debit")) {//扣款 即系减钱操作
					BigDecimal creditValue = memberLimitRuleDao.getMemberCreditValue(customerId);
					amount.subtract(topUpAmount);
					if (null == creditValue) {
						creditValue = new BigDecimal(0);
					}
					if (amount.compareTo(BigDecimal.ZERO) == -1) {
						if (creditValue.compareTo(amount.abs()) == -1) {//取计算后当前金额的绝对值进行比较看是否超过透支限额
							responseResult.initResult(GTAError.CommonError.EXCEEDING_CREDIT_LIMITS);
							return responseResult;
						}
					}
					
				} else {//退款 即系加钱操作
					amount = topUpAmount;
				}
				memberCashvalue = new MemberCashvalue();
				memberCashvalue.setInitialDate(currentDate);
				memberCashvalue.setInitialValue(amount);
				memberCashvalue.setExchgFactor(new BigDecimal(1));
				memberCashvalue.setInternalRemark(dto.getRemark());
				memberCashvalue.setUpdateDate(currentDate);
				memberCashvalue.setUpdateBy(staffUserId);
				
				if (superiorMemberId != null) {
					//if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
					memberCashvalue.setCustomerId(superiorMemberId);
				}else {
					memberCashvalue.setCustomerId(customerId);
				}
				memberCashvalue.setAvailableBalance(amount);
				memberCashValueDao.saveMemberCashValue(memberCashvalue);
			} else {
				if (dto.getAdjustmentType().equals("debit")) {//扣款 即系减钱操作
					amount = memberCashvalue.getAvailableBalance().subtract(topUpAmount);
					BigDecimal creditValue = memberLimitRuleDao.getMemberCreditValue(customerId);
					if (null == creditValue) {
						creditValue = new BigDecimal(0);
					}
					if (amount.compareTo(BigDecimal.ZERO) == -1) {
						if (creditValue.compareTo(amount.abs()) == -1) {//取计算后当前金额的绝对值进行比较看是否超过透支限额
							responseResult.initResult(GTAError.CommonError.EXCEEDING_CREDIT_LIMITS);
							return responseResult;
						}
					}
				} else {//退款 即系加钱操作
					amount = memberCashvalue.getAvailableBalance().add(topUpAmount);
				}
				memberCashvalue.setUpdateDate(currentDate);
				memberCashvalue.setUpdateBy(staffUserId);
				
				memberCashvalue.setAvailableBalance(amount);
				memberCashValueDao.updateMemberCashValue(memberCashvalue);
			}
			// CUSTOMER ORDER Header
			CustomerOrderHd customerOrderHd = new CustomerOrderHd();
			customerOrderHd.setOrderDate(currentDate);
			customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
			customerOrderHd.setStaffUserId(staffUserId);
			customerOrderHd.setCustomerId(customerId);
			customerOrderHd.setOrderTotalAmount(topUpAmount);
			customerOrderHd.setOrderRemark(dto.getRemark());
			customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderHd.setCreateBy(staffUserId);
			customerOrderHd.setUpdateBy(staffUserId);
			customerOrderHd.setUpdateDate(currentDate);
			Long orderNo = (Long) customerOrderHdDao
					.addCustomreOrderHd(customerOrderHd);
			CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);

			// TODO 前期为了避免人工导数据，所以自动生成，提交代码1-2周后注释此段代码
			
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(temp);
			customerOrderDet.setItemNo(dto.getAdjustmentType().equals("debit") ? Constant.STAFF_DEBITED_FOR_CASH_VALUE : Constant.STAFF_REFUND_FOR_CASH_VALUE);
			customerOrderDet.setItemRemark(dto.getRemark());
			customerOrderDet.setOrderQty(1L);
			customerOrderDet.setItemTotalAmout(topUpAmount);
			customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderDet.setCreateBy(staffUserId);
			customerOrderDet.setUpdateDate(currentDate);
			customerOrderDet.setUpdateBy(staffUserId);
			customerOrderDetDao.saveOrderDet(customerOrderDet);

			// CUSTOMER ORDER TRANS
			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			initCustomerOrderTrans(customerOrderTrans);
			customerOrderTrans.setCustomerOrderHd(temp);
			customerOrderTrans.setTransactionTimestamp(currentDate);
			customerOrderTrans.setPaidAmount(topUpAmount);
			if (agentTransactionNo != null && !agentTransactionNo.equals("")) {
				customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
			}
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASHVALUE.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			customerOrderTrans.setPaymentRecvBy(staffUserId);
			customerOrderTrans.setCreateBy(staffUserId);
			customerOrderTrans.setUpdateBy(staffUserId);
			customerOrderTrans.setUpdateDate(ts);
			customerOrderTrans.setInternalRemark(dto.getRemark());
			customerOrderTrans.setPaymentLocationCode(dto.getTopUpLocation());
			customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		responseResult.initResult(GTAError.Success.SUCCESS,customerOrderTrans);
		return responseResult;
	}
	
	/**
	 * 获取加减总数
	 * @param dto
	 * @return
	 */
	@Transactional
	@Override
	public ResponseResult getMemberCashValueSum(long customerId) {
		
		CashValueSumDto dto = customerOrderTransDao.getMemberDebitCashValueSum(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS,dto);
		return responseResult;
	}

	/**
	 * 获取需要退款金额
	 * @param dto
	 * @return
	 */
	@Transactional
	@Override
	public ResponseResult getLiabilityAmount(Long customerId) {
		// TODO Auto-generated method stub
		List<Serializable> param = new ArrayList<Serializable>();
		String sql = "select sum(ifnull(a.topupAmount,0)) - sum(ifnull(b.debitAmount,0)) - sum(ifnull(c.spentAmount,0)) from (\n" + 
				"select sum(p.paid_amount) topupAmount from (\n" + 
				"select pos.item_catagory,pos.item_no,t.paid_amount from\n" + 
				"customer_order_trans t,\n" + 
				"customer_order_hd hd,\n" + 
				"customer_order_det det,\n" + 
				"pos_service_item_price pos,\n" + 
				"customer_profile cp\n" + 
				"where t.order_no = hd.order_no\n" + 
				"and hd.order_no = det.order_no\n" + 
				"and det.item_no = pos.item_no\n" + 
				"and hd.customer_id = cp.customer_id\n" + 
				"and (t.`status`='SUC' or t.`status`='RFU')\n" + 
				"and hd.order_status='CMP'\n" + 
				"and pos.`status`='ACT'\n" +
				"and hd.customer_id = ?) p \n" + 
				"where (p.`item_no`='SRR00000001' or p.item_catagory in ('TOPUP'))) a,\n" + //退款分cancel的退款和cash value refund(也就是提现），cancel的应该为topupamount
				"\n" + 
				"(select sum(p.paid_amount) debitAmount from (\n" + 
				"select pos.item_catagory,pos.item_no,t.paid_amount from\n" + 
				"customer_order_trans t,\n" + 
				"customer_order_hd hd,\n" + 
				"customer_order_det det,\n" + 
				"pos_service_item_price pos,\n" + 
				"customer_profile cp\n" + 
				"where t.order_no = hd.order_no\n" + 
				"and hd.order_no = det.order_no\n" + 
				"and det.item_no = pos.item_no\n" + 
				"and hd.customer_id = cp.customer_id\n" + 
				"and t.payment_method_code='CASHVALUE'\n" + 
				"and t.`status`='SUC'\n" + 
				"and hd.order_status='CMP'\n" + 
				"and pos.`status`='ACT'\n" + 
				"and hd.customer_id = ?) p \n" + 
				"where p.item_catagory in ('DEBIT')) b,\n" +
				"\n" +
				"(select sum(p.paid_amount) spentAmount from (\n" +
				"select pos.item_catagory,pos.item_no,t.paid_amount from\n" +
				"customer_order_trans t,\n" +
				"customer_order_hd hd,\n" +
				"customer_order_det det,\n" +
				"pos_service_item_price pos,\n" +
				"customer_profile cp\n" +
				"where t.order_no = hd.order_no\n" +
				"and hd.order_no = det.order_no\n" +
				"and det.item_no = pos.item_no\n" +
				"and hd.customer_id = cp.customer_id\n" +
				"and t.payment_method_code='CASHVALUE'\n" +
				"and (t.`status`='SUC' or t.`status`='RFU')\n" + //退款分cancel的退款和cash value refund(也就是提现），提现的应该为spent amount
				"and hd.order_status='CMP'\n" +
				"and pos.`status`='ACT'\n" +
				"and hd.customer_id = ?) p \n" +
				"where (p.`item_no`='CVR00000001' or p.item_catagory not in ('TOPUP','REFUND','DEBIT','CREDIT'))) c";
		param.add(customerId);
		param.add(customerId);
		param.add(customerId);
		
		BigDecimal totalAmount = (BigDecimal)memberCashValueDao.getUniqueBySQL(sql, param);
		responseResult.initResult(GTAError.Success.SUCCESS,totalAmount);
		return responseResult;
	}
	
	/**
	 * 获取所有member加减总数
	 * @param dto
	 * @return
	 */
	@Transactional
	@Override
	public ResponseResult getAllDebitAndCreditCashValueSum() {
		
		CashValueSumDto dto = customerOrderTransDao.getAllDebitAndCreditCashValueSum();
		responseResult.initResult(GTAError.Success.SUCCESS,dto);
		return responseResult;
	}

	@Override
	@Transactional
	public List getgetVoidPaymentListAdvancedSearch() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Date/Time", "date_format(transactionTimestamp,'%Y-%m-%d')", "java.util.Date", "", 1);
		
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Original Transaction ID", "fromTransactionNo", "java.lang.Long", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("New Transaction ID", "transactionNo", "java.lang.Long", "", 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Payment Method", "paymentMethodCode", "java.lang.String", sysCodeDao.selectSysCodeByCategory("PAYMENTMETHOD"), 3);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Description", "itemCatagory", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Patron", "memberName", "java.lang.String", "", 5);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Void", "paidAmount", "java.math.BigDecimal", "", 6);
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5
				,condition6,condition7);
	}
	
	/**
	 * 获取当月生日会员列表
	 * @param page
	 * @param sortBy
	 * @param isAscending
	 * @param customerId
	 * @param status
	 * @param expiry
	 * @param planName
	 * @param memberType
	 * @param month
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult getMembersOverviewListByBirthday(ListPage<Member> page, String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType, Integer month){
		
		ListPage<Member> memberOverviewList = memberDao.getMemberList(page, sortBy, isAscending, customerId, status, expiry, planName, memberType, month);
		Data data = new Data();
		data.setList(memberOverviewList.getDtoList());
		data.setLastPage(memberOverviewList.isLast());
		data.setCurrentPage(memberOverviewList.getNumber());
		data.setRecordCount(memberOverviewList.getAllSize());
		data.setPageSize(memberOverviewList.getSize());
		data.setTotalPage(memberOverviewList.getAllPage());
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
		
	}
	
	/**
	 * 获取某生日会员报表
	 * @param startTime
	 * @param endTime
	 * @param location
	 * @param fileType
	 * @return
	 */
	@Override
	@Transactional
	public byte[]  getMembersOverviewListByBirthdayReports( String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType, Integer month,  String fileType){
		try {
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "PatronBirthdayReports.jasper";
			List<DependentMemberDto> dataList = memberDao.getMemberList(sortBy, isAscending, customerId, status, expiry, planName, memberType, month);
			Map<String, Object> parameters = new HashMap();
//			parameters.put("startDate", startTime);
			parameters.put("month", DateCalcUtil.numberConversionMonth(month.toString()));
			parameters.put("sumQty", dataList.size());
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, dataList, fileType);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
