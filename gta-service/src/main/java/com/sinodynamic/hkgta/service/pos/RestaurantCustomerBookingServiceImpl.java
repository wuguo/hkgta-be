package com.sinodynamic.hkgta.service.pos;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantCustomerBookingDao;
import com.sinodynamic.hkgta.dto.pos.RestaurantBookingPushDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantCustomerBookingDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.pos.RestaurantCustomerBooking;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.NotificationOnOffService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RestaurantCustomerBookingStatus;
import com.sinodynamic.hkgta.util.constant.UserPreferenceSettingParam;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RestaurantCustomerBookingServiceImpl extends ServiceBase<RestaurantCustomerBooking>
		implements RestaurantCustomerBookingService {
	@Autowired
	private RestaurantCustomerBookingDao restaurantCustomerBookingDao;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private UserPreferenceSettingService userPreferenceSettingService;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private RestaurantMasterService restaurantMasterService;

	@Autowired
	private MemberDao memberDao;

	@Autowired
	private MessageTemplateDao messageTemplateDao;

	@Autowired
	private ShortMessageService smsService;
	
	
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
	@Autowired
	private NotificationOnOffService notificationOnOffService;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;

	@Override
	@Transactional
	public ResponseResult saveRestaurantCustomerBooking(String createBy, RestaurantCustomerBookingDto bookingDto)
			throws Exception {
		Member member = memberDao.getMemberByCustomerId(bookingDto.getCustomerId());
		if (member != null && member.getStatus().equals(Constant.General_Status_NACT)) {
			responseResult.initResult(GTAError.FacilityError.MEMBER_NOT_ACTIVE);
			return responseResult;
		}
		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, bookingDto.getCustomerId());
		if (null != customerProfile) {
			RestaurantCustomerBooking booking = new RestaurantCustomerBooking();
			RestaurantMaster restaurantMaster = restaurantMasterService
					.getRestaurantMaster(bookingDto.getRestaurantId());
			if (null == restaurantMaster)
				responseResult.initResult(GTAError.RestaurantError.RESTAURANT_ID_INVALID);
			booking.setRestaurantMaster(restaurantMaster);
			booking.setCustomerProfile(customerProfile);
			booking.setBookTime(DateCalcUtil.parseDateTime(bookingDto.getBookTime()));
			booking.setPartySize(bookingDto.getPartySize());
			booking.setStatus(RestaurantCustomerBookingStatus.WAIT.name());
			booking.setBookVia(bookingDto.getBookVia());
			booking.setRemark(bookingDto.getRemark());
			if(bookingDto.getCreatedBy()!=null){
				booking.setCreateBy(bookingDto.getCreatedBy());	
			}else{
				booking.setCreateBy(createBy);
			}	
					
			booking.setCreateDate(new Timestamp(new Date().getTime()));
			restaurantCustomerBookingDao.save(booking);
			RestaurantCustomerBookingDto result = createRestaurantCustomerBookingDto(booking);
			responseResult.initResult(GTAError.Success.SUCCESS, result);
		} else {
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
		}

		return responseResult;
	}

	private RestaurantCustomerBookingDto createRestaurantCustomerBookingDto(RestaurantCustomerBooking booking) {
		CustomerProfile customerProfile = booking.getCustomerProfile();
		Member member = memberDao.get(Member.class, customerProfile.getCustomerId());
		RestaurantCustomerBookingDto queryDto = new RestaurantCustomerBookingDto();
		queryDto.setResvId(booking.getResvId());
		if("0001".equals(booking.getRestaurantMaster().getRestaurantId())){
			queryDto.setResvIdStr("CAF-" + booking.getResvId());
		}else if("0002".equals(booking.getRestaurantMaster().getRestaurantId())){
			queryDto.setResvIdStr("BAR-" + booking.getResvId());
		}else if("0003".equals(booking.getRestaurantMaster().getRestaurantId())){
			queryDto.setResvIdStr("RES-" + booking.getResvId());
		}else if("0004".equals(booking.getRestaurantMaster().getRestaurantId())){
			queryDto.setResvIdStr("SKD-" + booking.getResvId());
		}else if("0005".equals(booking.getRestaurantMaster().getRestaurantId())){
			queryDto.setResvIdStr("RTL-" + booking.getResvId());
		}
		queryDto.setRestaurantId(booking.getRestaurantMaster().getRestaurantId());
		queryDto.setBookTime(DateCalcUtil.formatDatetime(booking.getBookTime()));
		queryDto.setCustomerId(booking.getCustomerProfile().getCustomerId());
		queryDto.setBookVia(booking.getBookVia());
		queryDto.setPartySize(booking.getPartySize());
		queryDto.setRemark(booking.getRemark());
		queryDto.setStatus(booking.getStatus());
		queryDto.setAcademyNo(member.getAcademyNo());
		queryDto.setMemberName(customerProfile.getSalutation() + " " + customerProfile.getGivenName() + " "
				+ customerProfile.getSurname());
		return queryDto;
	}

	@Override
	@Transactional
	public ResponseResult updateRestaurantCustomerBooking(String updateBy, RestaurantCustomerBookingDto bookingDto)
			throws Exception {
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class,
				bookingDto.getResvId().longValue());
		if (null != booking) {
			booking.setBookTime(DateCalcUtil.parseDateTime(bookingDto.getBookTime()));
			booking.setPartySize(bookingDto.getPartySize());
			booking.setRemark(bookingDto.getRemark());
			booking.setUpdateBy(updateBy);
			booking.setUpdateDate(new Timestamp(new Date().getTime()));
			restaurantCustomerBookingDao.update(booking);
			RestaurantCustomerBookingDto result = createRestaurantCustomerBookingDto(booking);
			responseResult.initResult(GTAError.Success.SUCCESS, result);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public RestaurantCustomerBookingDto getRestaurantCustomerBookingByResvId(Long resvId) {
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		RestaurantCustomerBookingDto result = createRestaurantCustomerBookingDto(booking);
		return result;
	}

	@Override
	@Transactional
	public RestaurantCustomerBooking getRestaurantCustomerBooking(Long resvId) throws Exception {
		return restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
	}

	@Override
	@Transactional
	public ResponseResult cancelRestaurantCustomerBooking(String updateBy, Long resvId) throws Exception {
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		if (null != booking) {
			booking.setStatus(RestaurantCustomerBookingStatus.CAN.name());
			booking.setUpdateBy(updateBy);
			booking.setUpdateDate(new Timestamp(new Date().getTime()));
			restaurantCustomerBookingDao.update(booking);
			RestaurantCustomerBookingDto result = createRestaurantCustomerBookingDto(booking);
			responseResult.initResult(GTAError.Success.SUCCESS, result);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public int updateRestaurantCustomerBookingExpired() throws Exception {
		return restaurantCustomerBookingDao.updateRestaurantCustomerBookingExpired();
	}

	@Override
	@Transactional
	public List<RestaurantCustomerBooking> getRestaurantCustomerBookingConfirmedList() {
		return restaurantCustomerBookingDao.getRestaurantCustomerBookingConfirmedList();
	}

	@Override
	@Transactional
	public void sendRestaurantBookingSMS(Long resvId, String functionId) throws Exception {
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		if (null != booking) {
			CustomerProfile customerProfile = booking.getCustomerProfile();
			if (customerProfile != null) {
				//add logger
				StringBuilder sb=new StringBuilder();
				sb.append("\n Restaurant status is "+getRestaurantBookStatus(functionId)+" send SMS start......\n");
				sb.append(" send SMS  the phoneMobile:"+customerProfile.getPhoneMobile()+"\n");
				boolean sendSMS=userPreferenceSettingService.sendSMSByCustomerId(customerProfile.getCustomerId());
				sb.append(" check userSetting receive SMS  "+sendSMS+"\n");
				if(!sendSMS){
					sb.append(" no send SMS to patron  ,the Restaurant status is "+getRestaurantBookStatus(functionId)+" send SMS end......\n");
					logger.info(sb.toString());
					return ;
				}else{
					String mobilePhone = customerProfile.getPhoneMobile();
					List<String> phonenumbers = new ArrayList<String>();
					phonenumbers.add(mobilePhone);
					MessageTemplate messageTemplate = messageTemplateDao.getTemplateByFunctionId(functionId);
					if (null != messageTemplate) {
						String content = messageTemplate.getContent();
						String message = content.replace("{timeLeft}", 120 + "")
								.replace("{bookingTime}", DateCalcUtil.formatDatetime(booking.getBookTime()))
								.replace("{restaurantName}", booking.getRestaurantMaster().getRestaurantName())
								.replace("{partySize}", booking.getPartySize().toString());
					  smsService.sendSMS(phonenumbers, message,
								DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
					  sb.append(" SMS message is :"+message+"\n");
					  sb.append(" the Restaurant status is "+getRestaurantBookStatus(functionId)+" send SMS end......\n");
					  logger.info(sb.toString());
				    }
				}	
			}
		}
	}
	private String getRestaurantBookStatus(String functionId){
		String status=null;
		switch (functionId) {
		case Constant.TEMPLATE_ID_RESTAURANT_BOOK_CANCELLED:
			status="cancel";
			break;
		case Constant.TEMPLATE_ID_RESTAURANT_BOOK_UPDATED:
			status="booking update";
			break;
		case Constant.TEMPLATE_ID_RESTAURANT_BOOK_CONFIRM:
			status="confrim ";
			break;
		case Constant.TEMPLATE_ID_RESTAURANT_BOOK_REJECT:
			status="reject ";
			break;
		case Constant.TEMPLATE_ID_RESTAURANT_BOOK_REQUEST:
			status="booking request ";
			break;
		default:
			break;
		}
		return status;
	}

	@Override
	@Transactional
	public void sendRestaurantBookingMail(Long resvId, String functionId) throws Exception {
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		if (null != booking) {
			CustomerProfile customerProfile = booking.getCustomerProfile();
			if (customerProfile != null) {
				String email = customerProfile.getContactEmail();
				MessageTemplate messageTemplate = messageTemplateDao.getTemplateByFunctionId(functionId);
				if (null != messageTemplate) {
					String content =  messageTemplate.getContentHtml();// mail content
					String subject = messageTemplate.getMessageSubject();// subject
					String patronName = customerProfile.getGivenName() + " " + customerProfile.getSurname();
					content = content.replace("{memberName}", patronName)
							.replace("{restaurantName}", booking.getRestaurantMaster().getRestaurantName())
							.replace("{bookingTime}", DateCalcUtil.formatDatetime(booking.getBookTime()))
							.replace("{partySize}", booking.getPartySize().toString());
					CustomerEmailContent cec = customerEmailContentService.sendRestaurantBookingLetter(customerProfile.getCustomerId(), subject, content);
					boolean success = MailSender.sendEmail(email, null, null, subject, content, null);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					customerEmailContentService.modifyCustomerEmailContent(cec);
				}
			}
		}

	}
	
	@Override
	@Transactional
	public ResponseResult updateRestaurantCustomerBooking(String updateBy, Long resvId, String status)
			throws Exception {
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		if (null != booking) {
			if (!StringUtils.isEmpty(status)) {
				if (!(status.equals(RestaurantCustomerBookingStatus.CFM.name())
						|| status.equals(RestaurantCustomerBookingStatus.WAIT.name())
						|| status.equals(RestaurantCustomerBookingStatus.REJ.name()))) {
					responseResult.initResult(GTAError.RestaurantError.INVALID_STATUS);
				} else {
					booking.setStatus(status);
					booking.setUpdateBy(updateBy);
					booking.setUpdateDate(new Timestamp(new Date().getTime()));
					if (restaurantCustomerBookingDao.saveOrUpdate(booking)) {
						// status is confirmed or Rejected send sms and mail
						if (status.equals(RestaurantCustomerBookingStatus.CFM.name())) {
							this.sendRestaurantBookingSMS(booking.getResvId(), Constant.TEMPLATE_ID_RESTAURANT_BOOK_CONFIRM);
							this.sendRestaurantBookingMail(booking.getResvId(), Constant.TEMPLATE_ID_RESTAURANT_BOOK_CONFIRM_EMAIL);
						}
						if (status.equals(RestaurantCustomerBookingStatus.REJ.name())) {
							this.sendRestaurantBookingSMS(booking.getResvId(), Constant.TEMPLATE_ID_RESTAURANT_BOOK_REJECT);
							this.sendRestaurantBookingMail(booking.getResvId(), Constant.TEMPLATE_ID_RESTAURANT_BOOK_REJECT_EMAIL);
						}
						responseResult.initResult(GTAError.Success.SUCCESS, true);
					} else {
						responseResult.initResult(GTAError.RestaurantError.CHANGE_STATUS_FAIL);
					}
				}
			}
		} else {
			responseResult.initResult(GTAError.RestaurantError.NO_FIND_RESTAURANT_BOOKING);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public void sendNotificationToRestaurantTablet(Long resvId) throws Exception {
		// TODO Auto-generated method stub
		RestaurantCustomerBooking booking = restaurantCustomerBookingDao.get(RestaurantCustomerBooking.class, resvId);
		if (null != booking) {
			List<String> userIds = this.notificationOnOffService.getPushNotificationStaffByRestaurantId(booking.getRestaurantMaster().getRestaurantId());
			if(userIds != null && userIds.size() != 0){
				RestaurantBookingPushDto dto = new RestaurantBookingPushDto();
				dto.setPushType("restaurantBooking");
				dto.setDefaultTitle("There is a new table reservation request.");
				dto.setPopUpTitle("New table reservation is received");
				CustomerProfile customerProfile = booking.getCustomerProfile();
				String patronName = customerProfile.getSalutation() + " " +customerProfile.getGivenName() + " " + customerProfile.getSurname();
				dto.setMemberName(patronName);
				dto.setBookingTime(DateCalcUtil.formatDatetime(booking.getBookTime()));
				dto.setPartySize(booking.getPartySize());
				this.devicePushService.pushMessage((String [])userIds.toArray(new String[userIds.size()]), new Gson().toJson(dto), Constant.RESTAURANTTABLETAPP_PUSH_APPLICATION);
			}
		}
	}
}
