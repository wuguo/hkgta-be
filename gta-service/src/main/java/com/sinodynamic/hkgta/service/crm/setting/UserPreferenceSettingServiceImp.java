package com.sinodynamic.hkgta.service.crm.setting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.setting.UserPreferenceSettingDao;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.UserPreferenceSettingParam;

/**
 * @author Mason_Yang
 *
 */
@Service
public class UserPreferenceSettingServiceImp extends ServiceBase<UserPreferenceSetting> implements UserPreferenceSettingService {

	@Autowired
	private UserPreferenceSettingDao userPreferenceDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Override
	@Transactional
	public boolean checkUserSettingValue(String userId, String paramId) {
		// TODO Auto-generated method stub
		UserPreferenceSetting setting=userPreferenceDao.checkUserSettingValue(userId, paramId);
		if(null!=setting){
			if("OFF".equalsIgnoreCase(setting.getParamValue())){
				return false;
			}
		}
		return true;
	}
	@Override
	@Transactional
	public boolean sendSMSByCustomerId(Long customerId)
	{
		boolean sendSMS=true;
		if(null!=customerId){
			Member member=memberDao.getMemberByCustomerId(customerId);
			if(null!=member){
				sendSMS=this.checkUserSettingValue(member.getUserId(), UserPreferenceSettingParam.NOTIFICATION.getType());
			}
		}
		return sendSMS;
	}

}
