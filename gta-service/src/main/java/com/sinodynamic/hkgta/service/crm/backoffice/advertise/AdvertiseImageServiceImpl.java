package com.sinodynamic.hkgta.service.crm.backoffice.advertise;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.AdvertiseImageDao;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageDto;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageUploadDto;
import com.sinodynamic.hkgta.entity.crm.AdvertiseImage;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.AdvertiseAppType;
import com.sinodynamic.hkgta.util.constant.Constant.AdvertiseDispLoc;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class AdvertiseImageServiceImpl extends ServiceBase<AdvertiseImage> implements AdvertiseImageService {

	@Autowired
	private AdvertiseImageDao advertiseImageDao;

	@Override
	@Transactional
	public AdvertiseImage get(long imgId) {
		try {
			return advertiseImageDao.get(new AdvertiseImage(), imgId);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return new AdvertiseImage();
		}
	}

	@Override
	@Transactional
	public ResponseResult getList(String appType, String dispLoc) {
		if (!StringUtils.isEmpty(appType)) {
			appType = appType.toUpperCase();
		}
		if (!isValidAppType(appType)) {
			responseResult.initResult(GTAError.AdvertiseImageError.INVALID_APP_TYPE);
			return responseResult;
		}

		if (!StringUtils.isEmpty(dispLoc)) {
			dispLoc = dispLoc.toUpperCase();
			if (!isValidDispLoc(dispLoc)) {
				responseResult.initResult(GTAError.AdvertiseImageError.INVALID_DISP_LOC);
				return responseResult;
			}
			Map<String, Object> retMap = new HashMap<String, Object>();
			retMap.put(dispLoc.toLowerCase(), advertiseImageDao.getByAppTypeDispLoc(appType, dispLoc));
			responseResult.initResult(GTAError.Success.SUCCESS, retMap);
			return responseResult;

		} else {
			Map<String, Object> retMap = new HashMap<String, Object>();
			for (AdvertiseDispLoc d : Constant.AdvertiseDispLoc.values()) {
				dispLoc = d.name().toUpperCase();
				retMap.put(dispLoc.toLowerCase(), advertiseImageDao.getByAppTypeDispLoc(appType, dispLoc));
			}
			responseResult.initResult(GTAError.Success.SUCCESS, retMap);
			return responseResult;
		}
	}

	@Override
	@Transactional
	public ResponseResult create(AdvertiseImageDto dto) {
		if (!StringUtils.isEmpty(dto.getAppType())) {
			dto.setAppType(dto.getAppType().toUpperCase());
		}
		if (!StringUtils.isEmpty(dto.getDispLoc())) {
			dto.setDispLoc(dto.getDispLoc().toUpperCase());
		}
		if (!isValidAppType(dto.getAppType())) {
			responseResult.initResult(GTAError.AdvertiseImageError.INVALID_APP_TYPE);
			return responseResult;
		}
		if (!isValidDispLoc(dto.getDispLoc())) {
			responseResult.initResult(GTAError.AdvertiseImageError.INVALID_DISP_LOC);
			return responseResult;
		}
		Date now = new Date();
		AdvertiseImage a = new AdvertiseImage();
		a.setApplicationType(dto.getAppType());
		a.setDisplayLocation(dto.getDispLoc());
		a.setServerFilename(dto.getFilepath());
		a.setDisplayOrder(advertiseImageDao.getMaxDispOrder(dto.getAppType(), dto.getDispLoc()) + 1);
		a.setStatus(Constant.General_Status_ACT);
		a.setCreateBy(dto.getCreateBy());
		a.setCreateDate(now);
		a.setUpdateBy(dto.getUpdateBy());
		a.setUpdateDate(now);
		try {
			advertiseImageDao.save(a);
			dto.setStatus(a.getStatus());
			dto.setCreateDate(a.getCreateDate());
			dto.setUpdateDate(a.getUpdateDate());
			dto.setImgId(a.getImgId());
			responseResult.initResult(GTAError.Success.SUCCESS, dto); // expose dto in API.
			return responseResult;

		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.AdvertiseImageError.AD_SAVE_ERROR);
			return responseResult;
		}
	}

	@Override
	@Transactional
	public ResponseResult delete(AdvertiseImage a) {
		try {
			advertiseImageDao.delete(a);
			if (a.getDisplayOrder() != null) {
				List<AdvertiseImageDto> dtoList = advertiseImageDao.getByAppTypeDispLoc(a.getApplicationType(), a.getDisplayLocation());
				if (dtoList != null && dtoList.size() > 0) {
					int dispOrder = 1;
					for (AdvertiseImageDto dto : dtoList) {
						if (dto.getDispOrder() != dispOrder) {
							advertiseImageDao.updateDispOrder(dto.getImgId(), dispOrder);
						}
						if (dto.getImgId() != null && !dto.getImgId().equals(a.getImgId())) {
							dispOrder++;
						}
					}
				}
			}
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;

		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.AdvertiseImageError.AD_DELETE_ERROR);
			return responseResult;
		}
	}

	private boolean isValidAppType(String appType) {
		if (!StringUtils.isEmpty(appType)) {
			for (AdvertiseAppType t : Constant.AdvertiseAppType.values()) {
				if (t.name().equals(appType)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isValidDispLoc(String dispLoc) {
		if (!StringUtils.isEmpty(dispLoc)) {
			for (AdvertiseDispLoc d : Constant.AdvertiseDispLoc.values()) {
				if (d.name().equals(dispLoc)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	@Transactional
	public ResponseResult createAdvertiseImages(String dispLoc, AdvertiseImageUploadDto uploadDto, String userId) {
		try {
			if (!isValidDispLoc(dispLoc)) {
				responseResult.initResult(GTAError.AdvertiseImageError.INVALID_DISP_LOC);
				return responseResult;
			}
			advertiseImageDao.deleteAdvertiseImages(dispLoc);
			if (null != uploadDto && null != uploadDto.getAdvertiseImages() && uploadDto.getAdvertiseImages().size() > 0) {
				for (AdvertiseImageDto dto : uploadDto.getAdvertiseImages()) {
					if (!isValidAppType(dto.getAppType().toUpperCase())) {
						responseResult.initResult(GTAError.AdvertiseImageError.INVALID_APP_TYPE);
						return responseResult;
					}
					if (dto.getLinkLevel() == null) {
						responseResult.initResult(GTAError.AdvertiseImageError.LINKLEVEL_ISNULL);
						return responseResult;
					}
					if (dto.getDispOrder() == null) {
						responseResult.initResult(GTAError.AdvertiseImageError.DISPORDER_ISNULL);
						return responseResult;
					}
					dto.setDispLoc(dispLoc);
				}

				List<AdvertiseImageDto> imageDtos = new ArrayList<AdvertiseImageDto>();
				for (AdvertiseImageDto dto : uploadDto.getAdvertiseImages()) {
					AdvertiseImage advertiseImage = advertiseImageDao
							.getAdvertiseImage(dto.getAppType().toUpperCase(), dispLoc, dto.getLinkLevel(), dto.getDispOrder());
					if (null != advertiseImage) {
						advertiseImage.setServerFilename(dto.getFilepath());
						advertiseImage.setUpdateBy(userId);
						advertiseImage.setUpdateDate(new Date());
						advertiseImageDao.update(advertiseImage);
					} else {
						advertiseImage = createAdvertiseImage(userId, dto);
						advertiseImageDao.save(advertiseImage);
					}
					AdvertiseImageDto imageDto = createAdvertiseImageDto(advertiseImage);
					imageDtos.add(imageDto);
				}
				AdvertiseImageUploadDto result = new AdvertiseImageUploadDto();
				result.setAdvertiseImages(imageDtos);
				responseResult.initResult(GTAError.Success.SUCCESS, result);
				return responseResult;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.AdvertiseImageError.AD_SAVE_ERROR);
			return responseResult;
		}
		return responseResult;
	}

	private AdvertiseImage createAdvertiseImage(String userId, AdvertiseImageDto dto) {
		Date now = new Date();
		AdvertiseImage a = new AdvertiseImage();
		a.setApplicationType(dto.getAppType().toUpperCase());
		a.setDisplayLocation(dto.getDispLoc().toUpperCase());
		a.setServerFilename(dto.getFilepath());
		a.setDisplayOrder(
				null == dto.getDispOrder() ? (advertiseImageDao.getMaxDispOrder(dto.getAppType(), dto.getDispLoc()) + 1) : dto.getDispOrder());
		a.setLinkLevel(dto.getLinkLevel());
		a.setStatus(Constant.General_Status_ACT);
		a.setCreateBy(userId);
		a.setCreateDate(now);
		a.setUpdateBy(userId);
		a.setUpdateDate(now);
		return a;
	}

	private AdvertiseImageDto createAdvertiseImageDto(AdvertiseImage a) {
		AdvertiseImageDto imageDto = new AdvertiseImageDto();
		imageDto.setImgId(a.getImgId());
		imageDto.setFilepath(a.getServerFilename());
		imageDto.setDispOrder(a.getDisplayOrder());
		imageDto.setAppType(a.getApplicationType());
		imageDto.setStatus(a.getStatus());
		imageDto.setLinkLevel(a.getLinkLevel());
		imageDto.setDispLoc(a.getDisplayLocation());
		return imageDto;
	}

	@Override
	@Transactional
	public ResponseResult getAdvertiseImageList(String appType, String dispLoc) {
		if (!StringUtils.isEmpty(appType)) {
			appType = appType.toUpperCase();
		}
		dispLoc = dispLoc.toUpperCase();
		if (!isValidDispLoc(dispLoc)) {
			responseResult.initResult(GTAError.AdvertiseImageError.INVALID_DISP_LOC);
			return responseResult;
		}
		List<AdvertiseImageDto> advertiseImageList = advertiseImageDao.getByAppTypeDispLoc(appType, dispLoc);
		Map<String, AdvertiseImageDto> retMapApp = new HashMap<String, AdvertiseImageDto>();
		Map<String, AdvertiseImageDto> retMapPortal = new HashMap<String, AdvertiseImageDto>();
		if (null != advertiseImageList && advertiseImageList.size() > 0) {
			for (AdvertiseImageDto imageDto : advertiseImageList) {
				setAdvertiseImageMap(("MAPP".equals(imageDto.getAppType())) ? retMapApp : retMapPortal, imageDto);
			}
		}
		List<AdvertiseImageDto> advertiseImagesApp = new ArrayList<AdvertiseImageDto>();
		List<AdvertiseImageDto> advertiseImagesPortal = new ArrayList<AdvertiseImageDto>();
		if (null != retMapApp && retMapApp.size() > 0) {
			for (Object object : retMapApp.values()) {
				advertiseImagesApp.add((AdvertiseImageDto) object);
			}
		}
		if (null != retMapPortal && retMapPortal.size() > 0) {
			for (Object object : retMapPortal.values()) {
				advertiseImagesPortal.add((AdvertiseImageDto) object);
			}
		}

		Map<String, List<AdvertiseImageDto>> result = new HashMap<String, List<AdvertiseImageDto>>();
		if (null != advertiseImagesApp && advertiseImagesApp.size() > 0)
			result.put("MAPP", advertiseImagesApp);
		if (null != advertiseImagesPortal && advertiseImagesPortal.size() > 0)
			result.put("MPORT", advertiseImagesPortal);
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}

	private void setAdvertiseImageMap(Map<String, AdvertiseImageDto> retMapApp, AdvertiseImageDto imageDto) {
		if (null != imageDto.getLinkLevel() && imageDto.getLinkLevel() == 1) {
			retMapApp.put(imageDto.getAppType() + imageDto.getDispLoc() + imageDto.getDispOrder() + imageDto.getLinkLevel(), imageDto);
		} else if (null != imageDto.getLinkLevel() && imageDto.getLinkLevel() == 0) {
			AdvertiseImageDto adsImage = retMapApp.get(imageDto.getAppType() + imageDto.getDispLoc() + imageDto.getDispOrder() + "1");
			if (null != adsImage)
				adsImage.setParentFilepath(imageDto.getFilepath());
		}
	}

}
