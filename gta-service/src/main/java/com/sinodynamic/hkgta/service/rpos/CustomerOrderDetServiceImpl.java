package com.sinodynamic.hkgta.service.rpos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
@Scope("prototype")
public class CustomerOrderDetServiceImpl extends
		ServiceBase<CustomerOrderDet> implements CustomerOrderDetService {
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	@Autowired
	private CustomerOrderHdDao  customerOrderHdDao;
	@Override
	@Transactional
	public CustomerOrderDet getCustomerOrderDet(Long orderNo)
	{
		return customerOrderDetDao.getCustomerOrderDet(orderNo);
	}
	
	@Override
	@Transactional
	public CustomerOrderHd getCustomerOrderHdByOrderNo(Long orderNo)
	{
		return customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
	}

	@Override
	@Transactional
	public void saveCustomerOrderDet(CustomerOrderDet customerOrderDet) {
		// TODO Auto-generated method stub
		customerOrderDetDao.save(customerOrderDet);
		
	}
}
