package com.sinodynamic.hkgta.service.pos;

import java.util.List;

import com.sinodynamic.hkgta.dto.pos.RestaurantCustomerBookingDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantCustomerBooking;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RestaurantCustomerBookingService extends IServiceBase<RestaurantCustomerBooking> {

	public ResponseResult saveRestaurantCustomerBooking(String createBy, RestaurantCustomerBookingDto bookingDto)
			throws Exception;

	public ResponseResult updateRestaurantCustomerBooking(String updateBy, RestaurantCustomerBookingDto bookingDto)
			throws Exception;

	public RestaurantCustomerBookingDto getRestaurantCustomerBookingByResvId(Long resvId);

	public RestaurantCustomerBooking getRestaurantCustomerBooking(Long resvId) throws Exception;

	public ResponseResult cancelRestaurantCustomerBooking(String updateBy, Long resvId) throws Exception;

	public int updateRestaurantCustomerBookingExpired() throws Exception;

	public List<RestaurantCustomerBooking> getRestaurantCustomerBookingConfirmedList();

	public void sendRestaurantBookingSMS(Long resvId, String functionId) throws Exception;
	
	public void sendRestaurantBookingMail(Long resvId, String functionId)throws Exception;

	/***
	 * update RestaurantCustomerBooking
	 * @param updateBy
	 * @param resvId
	 * @param status is REJ or PND or CFM
	 * @return ResponseResult
	 * @throws Exception
	 */
	public ResponseResult updateRestaurantCustomerBooking(String updateBy, Long resvId,String status) throws Exception;

	public void sendNotificationToRestaurantTablet(Long resvId) throws Exception;
}
