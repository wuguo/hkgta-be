package com.sinodynamic.hkgta.service.fms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.RoomHousekeepStatusLogDao;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepStatusLog;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class RoomHousekeepStatusLogServiceImpl extends ServiceBase<RoomHousekeepStatusLog> implements RoomHousekeepStatusLogService {
	@Autowired
	private RoomHousekeepStatusLogDao roomHousekeepStatusLogDao;

	@Override
	@Transactional
	public void saveRoomHousekeepStatusLog(RoomHousekeepStatusLog log)
	{
		roomHousekeepStatusLogDao.save(log);
	}
	
}
