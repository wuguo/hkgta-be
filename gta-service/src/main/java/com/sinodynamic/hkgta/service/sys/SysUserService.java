package com.sinodynamic.hkgta.service.sys;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.sys.SysUserDto;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface SysUserService extends IServiceBase<UserMaster> {
	void addSysUser(SysUserDto user);
	void editSysUser(SysUserDto user);
	SysUserDto viewSysUser(String userId);
	Data loadAllUsers(Long roleId,String propertyName, String order, int pageSize, int currentPage,
			AdvanceQueryDto filters,String userType);
	
	List<AdvanceQueryConditionDto> userListAdvanceSearch();
	void refreshUserRole(String userId);
	ResponseResult checkPasswordLegal(String password);
	public void SaveSessionToken(String userId, String authToken, String device, String appTypeCode) throws Exception;
}
