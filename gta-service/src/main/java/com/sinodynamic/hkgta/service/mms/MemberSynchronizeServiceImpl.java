package com.sinodynamic.hkgta.service.mms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;

import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.mms.SpaMemberSyncDao;
import com.sinodynamic.hkgta.dto.crm.ActivateMemberDto;
import com.sinodynamic.hkgta.dto.crm.AdultDependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAccountInfoDto;
import com.sinodynamic.hkgta.dto.mms.GuestRequestDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.mms.SpaMemberSync;
import com.sinodynamic.hkgta.integration.spa.request.AddGuestRequest;
import com.sinodynamic.hkgta.integration.spa.request.ServicesListByCategoryCodeRequest;
import com.sinodynamic.hkgta.integration.spa.request.UpdateGuestRequest;
import com.sinodynamic.hkgta.integration.spa.response.InvoiceCreateResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryServiceListResponse;
import com.sinodynamic.hkgta.integration.spa.response.RegistGuestResponse;
import com.sinodynamic.hkgta.integration.spa.response.UpdateGuestResponse;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.integration.util.MMSAPIException;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@Service
public class MemberSynchronizeServiceImpl implements MemberSynchronizeService {
    
    private Logger logger = Logger.getLogger(MemberSynchronizeServiceImpl.class);
    private Logger mmsLog = Logger.getLogger(LoggerType.MMS.getName());
    
    @Autowired
    private SpaMemberSyncDao spaMemberSyncDao;
    
    @Autowired
    private SpaServcieManager spaServcieManager;
    
    @Autowired
    private CustomerServiceDao customerServiceDao;
    
    @Autowired
    private MemberDao memberDao;
    
    @Transactional
    public void excuteSynchGtaMember2Mms(GuestRequestDto dto){

		String action = dto.getAction();	    
	    String guestId = dto.getGuestId();
		if (StringUtils.isEmpty(action)) return;
		
		if (Constant.MEMBER_SYNCHRONIZE_INSERT.equals(action)) {
			if (!StringUtils.isEmpty(guestId)){
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
				entity.setLastResponseMsg(Constant.MEMBER_SYN_ACACEMY_NO_ALREADY_EXISTS);
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			}else{			
			    AddGuestRequest guestRequest = new AddGuestRequest();
//			    guestRequest.setGuestId(String.valueOf(dto.getGuestId()));
			    guestRequest.setGuestCode(dto.getGuestCode());
			    guestRequest.setGender(dto.getGender());
			    guestRequest.setFirstName(dto.getFirstName());
			    guestRequest.setLastName(dto.getLastName());
//			    guestRequest.setEmail(dto.getEmail());
			    /**
			     * SGG-3285
			     */
			   // guestRequest.setMobilePhone(dto.getMobilePhone());
			    /**
			     * don't syn address1 /address2
			     */
			    //guestRequest.setAddress1(dto.getAddress1());
			    //guestRequest.setAddress2(dto.getAddress2());
			    guestRequest.setCheckEmailPhoneExists("false");	
			    RegistGuestResponse response = spaServcieManager.registGuest(guestRequest);
			    if (response != null) {
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				if (response.isSuccess()) entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
				entity.setLastResponseMsg(response.getMessage());
				if (!StringUtils.isEmpty(response.getUserId())) entity.setSpaGuestId(response.getUserId());
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			    }
			}
		    
		} else if (Constant.MEMBER_SYNCHRONIZE_UPDATE.equals(action)) {
		    if (StringUtils.isEmpty(guestId)){
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				entity.setLastResponseMsg(Constant.MEMBER_SYN_SPA_GUEST_ID_NULL);
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			}else{	
		    
			    UpdateGuestRequest updateGuestRequest = new UpdateGuestRequest();
			    updateGuestRequest.setGuestId(guestId);
			    updateGuestRequest.setGuestCode(dto.getGuestCode());
			    updateGuestRequest.setGender(dto.getGender());
			    updateGuestRequest.setFirstName(dto.getFirstName());
			    updateGuestRequest.setLastName(dto.getLastName());
//			    updateGuestRequest.setEmail(dto.getEmail());
			    /**
			     * SGG-3285
			     */
			    //updateGuestRequest.setMobilePhone(dto.getMobilePhone());
//			    updateGuestRequest.setAddress1(dto.getAddress1());
//			    updateGuestRequest.setAddress2(dto.getAddress2());
			    updateGuestRequest.setCheckEmailPhoneExists("false");	
			    UpdateGuestResponse response = spaServcieManager.updateGuest(updateGuestRequest);
			    
			    if (response != null) {
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				if (response.isSuccess()) entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
				entity.setLastResponseMsg(response.getMessage());
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			    }
			}
		} else if (Constant.MEMBER_SYNCHRONIZE_LINKCARD.equals(action)) {
		    if (StringUtils.isEmpty(guestId)){
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				entity.setLastResponseMsg(Constant.MEMBER_SYN_SPA_GUEST_ID_NULL);
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			}else{			    
				//get service plan price
				CustomerAccountInfoDto customerAccountInfo = customerServiceDao.getNearestServiceAccountByCustomerId(dto.getCustomerId());
				if(null!=customerAccountInfo){
					dto.setOrderTotalAmount(customerAccountInfo.getOrderTotalAmount());
			    }else{
					BigDecimal totalAmount = new BigDecimal(2000.00);	
					dto.setOrderTotalAmount(totalAmount);
			    }
				
			    InvoiceCreateResponse response = spaServcieManager.invoiceCreate(dto);
			    if (response != null) {
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				if (response.isSuccess()) entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
				entity.setLastResponseMsg(String.valueOf(response.isSuccess()));
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
				}

			}
		}
		Log.info("synchronize Member   guestId:"+guestId+" action:"+action+"--params:"+JSONObject.fromObject(dto).toString());
		mmsLog.info("synchronize Member   guestId:"+guestId+" action:"+action+"--params:"+JSONObject.fromObject(dto).toString());
		logger.info("synchronize Member   guestId:"+guestId+" action:"+action+"--params:"+JSONObject.fromObject(dto).toString());
	  
    }
    
    @Transactional
    public  List<GuestRequestDto> getMemberInfoList2Synchronize(String academyNos){
    	 return spaMemberSyncDao.getMemberInfoList2Synchronize(Constant.STATUS_WAITING_TO_SYNCHRONIZE, academyNos);
    }
    @Override
    @Transactional
    public void synchronizeGtaMember2Mms(String academyNos) {
	
	try {
	    
	    List<GuestRequestDto> guestToSyncList = spaMemberSyncDao.getMemberInfoList2Synchronize(Constant.STATUS_WAITING_TO_SYNCHRONIZE, academyNos);
	    if (guestToSyncList == null || guestToSyncList.size() == 0) return;
	    for (GuestRequestDto dto : guestToSyncList) {
		String action = dto.getAction();	    
	    String guestId = dto.getGuestId();
		if (StringUtils.isEmpty(action)) continue;
		
		if (Constant.MEMBER_SYNCHRONIZE_INSERT.equals(action)) {
			if (!StringUtils.isEmpty(guestId)){
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
				entity.setLastResponseMsg(Constant.MEMBER_SYN_ACACEMY_NO_ALREADY_EXISTS);
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			}else{			
			    AddGuestRequest guestRequest = new AddGuestRequest();
//			    guestRequest.setGuestId(String.valueOf(dto.getGuestId()));
			    guestRequest.setGuestCode(dto.getGuestCode());
			    guestRequest.setGender(dto.getGender());
			    guestRequest.setFirstName(dto.getFirstName());
			    guestRequest.setLastName(dto.getLastName());
//			    guestRequest.setEmail(dto.getEmail());
			    /**
			     * SGG-3285
			     */
			   // guestRequest.setMobilePhone(dto.getMobilePhone());
			    guestRequest.setAddress1(dto.getAddress1());
			    guestRequest.setAddress2(dto.getAddress2());
			    guestRequest.setCheckEmailPhoneExists("false");	
			    RegistGuestResponse response = spaServcieManager.registGuest(guestRequest);
			    if (response != null) {
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				if (response.isSuccess()) entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
				entity.setLastResponseMsg(response.getMessage());
				if (!StringUtils.isEmpty(response.getUserId())) entity.setSpaGuestId(response.getUserId());
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			    }
			}
		    
		} else if (Constant.MEMBER_SYNCHRONIZE_UPDATE.equals(action)) {
		    if (StringUtils.isEmpty(guestId)){
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				entity.setLastResponseMsg(Constant.MEMBER_SYN_SPA_GUEST_ID_NULL);
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			}else{	
		    
			    UpdateGuestRequest updateGuestRequest = new UpdateGuestRequest();
			    updateGuestRequest.setGuestId(guestId);
			    updateGuestRequest.setGuestCode(dto.getGuestCode());
			    updateGuestRequest.setGender(dto.getGender());
			    updateGuestRequest.setFirstName(dto.getFirstName());
			    updateGuestRequest.setLastName(dto.getLastName());
//			    updateGuestRequest.setEmail(dto.getEmail());
			    /**
			     * SGG-3285
			     */
			    //updateGuestRequest.setMobilePhone(dto.getMobilePhone());
			    updateGuestRequest.setAddress1(dto.getAddress1());
			    updateGuestRequest.setAddress2(dto.getAddress2());
			    updateGuestRequest.setCheckEmailPhoneExists("false");	
			    UpdateGuestResponse response = spaServcieManager.updateGuest(updateGuestRequest);
			    
			    if (response != null) {
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				if (response.isSuccess()) entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
				entity.setLastResponseMsg(response.getMessage());
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			    }
			}
		} else if (Constant.MEMBER_SYNCHRONIZE_LINKCARD.equals(action)) {
		    if (StringUtils.isEmpty(guestId)){
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				entity.setLastResponseMsg(Constant.MEMBER_SYN_SPA_GUEST_ID_NULL);
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
			}else{			    
				//get service plan price
				CustomerAccountInfoDto customerAccountInfo = customerServiceDao.getNearestServiceAccountByCustomerId(dto.getCustomerId());
				if(null!=customerAccountInfo){
					dto.setOrderTotalAmount(customerAccountInfo.getOrderTotalAmount());
			    }else{
					BigDecimal totalAmount = new BigDecimal(2000.00);	
					dto.setOrderTotalAmount(totalAmount);
			    }
				
			    InvoiceCreateResponse response = spaServcieManager.invoiceCreate(dto);
			    if (response != null) {
				SpaMemberSync entity = spaMemberSyncDao.get(SpaMemberSync.class, dto.getSysId());
				entity.setRetryCount(entity.getRetryCount() + 1);
				if (response.isSuccess()) entity.setStatus(Constant.STATUS_FINISHED_SYNCHRONIZE);
				entity.setLastResponseMsg(String.valueOf(response.isSuccess()));
				entity.setUpdateDate(new Date());
				spaMemberSyncDao.update(entity);
				}

			}
		}
		Log.info("synchronize Member   guestId:"+guestId+" action:"+action+"--params:"+JSONObject.fromObject(dto).toString());
		mmsLog.info("synchronize Member   guestId:"+guestId+" action:"+action+"--params:"+JSONObject.fromObject(dto).toString());
		logger.info("synchronize Member   guestId:"+guestId+" action:"+action+"--params:"+JSONObject.fromObject(dto).toString());
	  } 	    
	} catch (Exception e) {
	    e.printStackTrace();
	    logger.error(e.getMessage(), e);
	    mmsLog.error(e.getMessage(), e);
	    throw new RuntimeException(e);
	}
	
    }
    
    /**
     * 查询所有未同步的member列表
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
	public List<AdultDependentMemberDto> getSyncFailureMemberList()throws Exception {
		  List<AdultDependentMemberDto> list = memberDao.getSyncFailureMemberList(null);
		return list;
	}
    
    /**
	 * 手动同步所有Member到MMS
     * @return 
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional
	public int manualSyncMemberByMMS(String academyNos) throws Exception{
		if (null == academyNos || academyNos.equals("")) {
			return 0;
		}
		StringBuilder sql = new StringBuilder(" INSERT INTO spa_member_sync (customer_id, action, status, create_date) VALUES ");
		List<AdultDependentMemberDto> list = memberDao.getSyncFailureMemberList(academyNos);
		int i = 0;
		for (AdultDependentMemberDto member : list) {
			if (i > 0) {
				sql.append(", ");
			}
			sql.append("(").append(member.getCustomerId()).append(", 'INSERT', 'OPN', NOW())");
			i++;
		}
//		List<AdultDependentMemberDto> updateList = memberDao.getNeedUpdateSyncMember();
//		for (AdultDependentMemberDto member : updateList) {
//			if (i > 0) {
//				sql.append(", ");
//			}
//			sql.append("(").append(member.getCustomerId()).append(", 'UPDATE', 'OPN', NOW())");
//			i++;
//		}
		return spaMemberSyncDao.bulkInsertBySql(sql.toString());
	}
	
	   /**
	    * 根据academyNos指定查询member等数据进行同步
	    * @param academyNos
	    * @return
	    * @throws Exception
	    */
		@Override
		@Transactional
		public List<AdultDependentMemberDto> manualSyncMemberByAcademyNos(String academyNos) throws Exception{
			List<AdultDependentMemberDto> list = new ArrayList<>();
			if (!academyNos.isEmpty()) {
				 list = memberDao.getMemberListByacademyNos(academyNos);
				StringBuilder ids = new StringBuilder();
				for (AdultDependentMemberDto member : list) {
					if (ids.length() > 0) {
						ids.append(",");
					}
					ids.append(member.getCustomerId());
				}
				spaMemberSyncDao.deleteMmsSync(ids.toString());
			} else {
				 list = memberDao.getSyncFailureMemberList(null);
			}
			
			return list;
		}

	@Transactional
	public List<SpaMemberSync> getAllSpaMemberSynchThenGuestIdIsNotNull() {
		// TODO Auto-generated method stub
		return spaMemberSyncDao.getAllSpaMemberSynchThenGuestIdIsNotNull();
	}

	@Override
	@Transactional
	public void updateSpaMemberSync(Long sysId, String lastResponseMsg) {
		// TODO Auto-generated method stub
		SpaMemberSync sync=spaMemberSyncDao.get(SpaMemberSync.class, sysId);
		if(StringUtils.isNotEmpty(sync.getLastResponseMsg()))
		{
			sync.setLastResponseMsg(sync.getLastResponseMsg()+"/n "+lastResponseMsg);	
		}else{
			sync.setLastResponseMsg(lastResponseMsg);
		}
		sync.setUpdateDate(new Date());
		spaMemberSyncDao.update(sync);
	}

    
}
