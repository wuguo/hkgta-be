package com.sinodynamic.hkgta.service.pos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pos.RestaurantBookingQuotaDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantBookingQuota;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class RestaurantBookingQuotaServiceImpl extends ServiceBase<RestaurantBookingQuota> implements
		RestaurantBookingQuotaService {
	
	@Autowired
	private RestaurantBookingQuotaDao restaurantBookingQuotaDao;
	
	@Override
	@Transactional
	public List<RestaurantBookingQuota> getRestaurantBookingQuotaList(String restaurantId,Integer bookingTime)
	{
		return restaurantBookingQuotaDao.getRestaurantBookingQuotaList(restaurantId,bookingTime);
	}
}
