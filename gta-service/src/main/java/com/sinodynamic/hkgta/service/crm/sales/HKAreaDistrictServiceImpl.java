package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.HKAreaDistrictDao;
import com.sinodynamic.hkgta.entity.crm.HkArea;
import com.sinodynamic.hkgta.entity.crm.HkDistrict;
import com.sinodynamic.hkgta.service.ServiceBase;

@SuppressWarnings("rawtypes")
@Service
public class HKAreaDistrictServiceImpl extends ServiceBase implements HKAreaDistrictService {

	@Autowired
	private HKAreaDistrictDao hkAreaDistrictDao;
	
	@Transactional
	public List<HkArea> getHKArea() {
		return hkAreaDistrictDao.getHKArea();
	}

	@Transactional
	public List<HkDistrict> getHKDistrict() {
		return hkAreaDistrictDao.getHKDistrict();
	}


}
