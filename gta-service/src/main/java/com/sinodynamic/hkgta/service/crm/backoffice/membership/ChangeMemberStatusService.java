package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.util.Date;

import com.sinodynamic.hkgta.entity.crm.BiMemberCnt;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface ChangeMemberStatusService extends IServiceBase<Member>{

	public ResponseResult updateMemberStatus(String status, Long customerId);

	/**
	 * 获取激活/非激活的Member数
	 * @return
	 * @throws Exception
	 */
	public Integer getStatisticsActivationOrInactiveMembersNum(boolean isActivation) throws Exception;

	/**
	 * 更新BiMemberCnt
	 * @param bm
	 */
	public void updateBiMemberCnt(BiMemberCnt bm);

	/**
	 * 获取BiMemberCnt
	 * @param countDate
	 * @param period
	 * @param isActivation
	 * @return
	 */
	public BiMemberCnt getBiMemberCnt(Date countDate, String period, boolean isActivation);

	/**
	 * 插入BiMemberCnt
	 * @param bm
	 */
	public void seveBiMemberCnt(BiMemberCnt bm);
}
