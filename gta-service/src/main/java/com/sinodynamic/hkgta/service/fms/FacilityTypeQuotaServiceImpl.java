package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTypeDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTypeQuotaDao;
import com.sinodynamic.hkgta.dto.fms.FacilityTypeQuotaDto;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.fms.FacilityType;
import com.sinodynamic.hkgta.entity.fms.FacilityTypeQuota;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
public class FacilityTypeQuotaServiceImpl extends ServiceBase<FacilityTypeQuota> implements FacilityTypeQuotaService {

	@Autowired
	private FacilityTypeQuotaDao facilityTypeQuotaDao;
	
	@Autowired
	private FacilityTypeDao facilityTypeDao;
	
	@Autowired
	private StaffProfileDao staffProfileDao;
	
	@Autowired
	private FacilityMasterService facilityMasterService;
	
	@Autowired
	private FacilityAdditionAttributeService facilityAdditionAttributeService;
	
	@Override
	@Transactional
	public List<FacilityTypeQuotaDto> getFacilityTypeQuotaList(String facilityTypeCode,String sortBy, String isAscending)
	{
		StringBuffer hql = new StringBuffer();
		hql.append("SELECT q.sysId AS sysId, ")
	    .append(" q.facilityTypeCode AS facilityType, ")
	    .append(" q.specificDate AS specificDate, ")
	    .append(" q.quota AS quota, ")
	    .append(" q.createDate AS createDate, ")
	    .append(" q.updateDate AS updateDate, ")
	    .append(" q.createBy as createBy, ")
	    .append(" q.updateBy as updateBy from FacilityTypeQuota q ")
		.append(" where q.facilityTypeCode = ? ")
		.append(" order by ");
		switch (sortBy)
		{
		case "specificDate":
			hql.append(" specificDate ");
			break;
		case "quota":
			hql.append(" quota ");
			break;
		case "updateBy":
			hql.append(" updateBy ");
			break;
		case "createDate":
			hql.append(" createDate ");
			break;
		default :
			hql.append(" specificDate ");
			break;
		}
		if ("true".equals(isAscending))
			hql.append(" asc ");
		else
			hql.append(" desc ");
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(facilityTypeCode);
		List<FacilityTypeQuotaDto> dtos = facilityTypeQuotaDao.getFacilityTypeQuotaList(hql.toString(),params);
		for(FacilityTypeQuotaDto dto : dtos){
			StaffProfile createBy = staffProfileDao.get(StaffProfile.class, dto.getCreateBy());
			if(null != createBy)dto.setCreateBy(createBy.getGivenName() +" "+createBy.getSurname());
			StaffProfile updateBy = staffProfileDao.get(StaffProfile.class, dto.getUpdateBy());
			if(null != updateBy)dto.setUpdateBy(updateBy.getGivenName() +" "+updateBy.getSurname());
		}
		return dtos;
	}

	@Override
	@Transactional
	public FacilityTypeQuotaDto saveFacilityTypeQuota(FacilityTypeQuotaDto quotaDto,String userId)
	{
		if (!checkWithFacilityMaxQuota(quotaDto.getFacilityType(), quotaDto.getQuota())) {
			throw new GTACommonException(GTAError.FacilityError.OPTIONAL_STADIUM_INSUFFICIENT);
		}
		
		FacilityTypeQuota quota  = facilityTypeQuotaDao.getFacilityTypeQuota(quotaDto.getFacilityType(), quotaDto.getSpecificDate(), null);
		if(null == quota){
			quota = new FacilityTypeQuota();
			quota.setSpecificDate(quotaDto.getSpecificDate());
			quota.setFacilityTypeCode(quotaDto.getFacilityType());
			quota.setCreateBy(userId);
			quota.setCreateDate(new Timestamp(new Date().getTime()));
		}
		quota.setUpdateBy(userId);
		quota.setUpdateDate(new Date());
		quota.setQuota(quotaDto.getQuota());
		facilityTypeQuotaDao.saveOrUpdate(quota);
		
		return getFacilityTypeQuotaDto(quota,quotaDto.getDefaultQuota());
	}
	
	@Override
	@Transactional
	public FacilityTypeQuotaDto saveFacilityTypeDefaultQuota(FacilityTypeQuotaDto quotaDto) {

		if (!checkWithFacilityMaxQuota(quotaDto.getFacilityType(), quotaDto.getDefaultQuota())) {
			throw new GTACommonException(GTAError.FacilityError.OPTIONAL_STADIUM_INSUFFICIENT);
		}
		
		FacilityType facilityType = facilityTypeDao.get(FacilityType.class, quotaDto.getFacilityType());
		if (null == facilityType) {
			facilityType = new FacilityType();
			facilityType.setTypeCode(quotaDto.getFacilityType());
		}
		if(null == facilityType.getCreateBy()){
			facilityType.setCreateBy(quotaDto.getCreateBy());
			facilityType.setCreateDate(new Timestamp(new Date().getTime()));
		}
		facilityType.setUpdateBy(quotaDto.getCreateBy());
		facilityType.setUpdateDate(new Date());
		facilityType.setDefaultQuota(quotaDto.getDefaultQuota());
		facilityTypeDao.saveOrUpdate(facilityType);

		return quotaDto;
	}

	private boolean checkWithFacilityMaxQuota(String facilityType, Long quota) {
		List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
		int totalFacilityCount = facilityMasterService.countFacilityMasterByTypeAndFacilityNos(facilityType,facilityNos);
		return (quota <= totalFacilityCount);
	}

	@Override
	@Transactional
	public FacilityTypeQuota getFacilityTypeQuota(String facilityType, Date specificDate,String subtypeId)
	{
		return facilityTypeQuotaDao.getFacilityTypeQuota(facilityType, specificDate,subtypeId);
	}

	@Override
	@Transactional
	public FacilityTypeQuotaDto getFacilityTypeQuotaById(long sysId)
	{
		FacilityTypeQuota quota = facilityTypeQuotaDao.get(FacilityTypeQuota.class, sysId);
		FacilityType facilityType = facilityTypeDao.get(FacilityType.class, quota.getFacilityTypeCode());
		return getFacilityTypeQuotaDto(quota,facilityType.getDefaultQuota());
	}

	private FacilityTypeQuotaDto getFacilityTypeQuotaDto(FacilityTypeQuota quota,Long defaultQuota)
	{
		FacilityTypeQuotaDto dto = getFacilityTypeQuotaDto(quota);
		dto.setDefaultQuota(defaultQuota);
		return dto;
	}
	
	private FacilityTypeQuotaDto getFacilityTypeQuotaDto(FacilityTypeQuota quota)
	{
		FacilityTypeQuotaDto dto = new FacilityTypeQuotaDto();
		if(quota != null){
			dto.setSysId(quota.getSysId());
			dto.setFacilityType(quota.getFacilityTypeCode());
			dto.setSpecificDate(quota.getSpecificDate());
			dto.setQuota(quota.getQuota());
			dto.setCreateDate(quota.getCreateDate());
			dto.setUpdateDate(quota.getUpdateDate());
			dto.setCreateBy(quota.getCreateBy());
			dto.setUpdateBy(quota.getUpdateBy());				
		}
		return dto;
	}

	@Override
	@Transactional
	public FacilityTypeQuotaDto updateFacilityTypeQuota(FacilityTypeQuotaDto quotaDto, String userId)
	{
		FacilityTypeQuota quota  = facilityTypeQuotaDao.get(FacilityTypeQuota.class, quotaDto.getSysId());
		if(null != quota){
			quota.setUpdateBy(userId);
			quota.setUpdateDate(new Date());
			quota.setQuota(quotaDto.getQuota());
			facilityTypeQuotaDao.update(quota);
		}
		FacilityType facilityType = facilityTypeDao.get(FacilityType.class, quotaDto.getFacilityType());
		return getFacilityTypeQuotaDto(quota,facilityType.getDefaultQuota());
	}

	@Override
	@Transactional
	public void deleteFacilityTypeQuota(long sysId, String userId)
	{
		FacilityTypeQuota quota  = facilityTypeQuotaDao.get(FacilityTypeQuota.class, sysId);
		if(null != quota){
			facilityTypeQuotaDao.delete(quota);
		}
	}

	@Override
	@Transactional
	public Long getFacilityQuota(String facilityType,Date specificDate)
	{
		Long quota = null;
		FacilityTypeQuota facilityQuota = facilityTypeQuotaDao.getFacilityTypeQuota(facilityType, specificDate, null);
		if(null != facilityQuota && null != facilityQuota.getQuota()){
			quota = facilityQuota.getQuota();
		}else{
			FacilityType type = facilityTypeDao.get(FacilityType.class,facilityType);
			if(null != type && null != type.getDefaultQuota())
				quota = type.getDefaultQuota();
		}
		return quota;
	}
}
