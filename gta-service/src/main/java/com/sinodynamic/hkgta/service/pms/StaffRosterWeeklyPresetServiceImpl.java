package com.sinodynamic.hkgta.service.pms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.StaffRosterWeeklyPresetDao;
import com.sinodynamic.hkgta.dto.pms.StaffRosterTimeDto;
import com.sinodynamic.hkgta.dto.pms.StaffRosterWeeklyPresetDto;
import com.sinodynamic.hkgta.dto.pms.StaffRosterWeeklyPresetRecordDto;
import com.sinodynamic.hkgta.entity.pms.StaffRosterWeeklyPreset;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
public class StaffRosterWeeklyPresetServiceImpl implements StaffRosterWeeklyPresetService {

	@Autowired
	private StaffRosterWeeklyPresetDao staffRosterWeeklyPresetDao;

	@Transactional
	@Override
	public StaffRosterWeeklyPresetDto getStaffRoster(String presetName) {

		StaffRosterWeeklyPresetDto staffRosterWeeklyPresetDto = new StaffRosterWeeklyPresetDto();

		List<StaffRosterWeeklyPreset> staffRosterWeeklyPresetList = staffRosterWeeklyPresetDao
				.getStaffRosterWeeklyPreset(presetName);

		List<StaffRosterWeeklyPresetRecordDto> list = new ArrayList<>();
		Map<Long, List<StaffRosterWeeklyPreset>> map = new HashMap<>();
		for (StaffRosterWeeklyPreset staffRosterWeeklyPreset : staffRosterWeeklyPresetList) {

			Long weekDay = staffRosterWeeklyPreset.getWeekDay();
			List<StaffRosterWeeklyPreset> value = map.get(weekDay);
			if (value == null) {
				value = new ArrayList<>();
			}
			value.add(staffRosterWeeklyPreset);
			map.put(weekDay, value);

		}

		Iterator<Entry<Long, List<StaffRosterWeeklyPreset>>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Long, List<StaffRosterWeeklyPreset>> entry = iterator.next();
			Long weekDay = entry.getKey();

			List<StaffRosterWeeklyPreset> value = entry.getValue();

			StaffRosterWeeklyPresetRecordDto staffRosterWeeklyPresetRecordDto = new StaffRosterWeeklyPresetRecordDto();

			List<StaffRosterTimeDto> timeList = new ArrayList<>();

			for (StaffRosterWeeklyPreset staffRosterWeeklyPreset : value) {
				StaffRosterTimeDto staffRosterTimeDto = new StaffRosterTimeDto();
				staffRosterTimeDto.setBeginTime(staffRosterWeeklyPreset.getBeginTime());
				staffRosterTimeDto.setEndTime(staffRosterWeeklyPreset.getEndTime());
				staffRosterWeeklyPresetRecordDto.setOffDuty(staffRosterWeeklyPreset.getOffDuty());
				timeList.add(staffRosterTimeDto);
			}
			staffRosterWeeklyPresetRecordDto.setTimeList(timeList);
			staffRosterWeeklyPresetRecordDto.setWeekDay(weekDay);

			list.add(staffRosterWeeklyPresetRecordDto);

		}
		staffRosterWeeklyPresetDto.setPresetName(presetName);
		staffRosterWeeklyPresetDto.setList(list);

		return staffRosterWeeklyPresetDto;
	}

	@Transactional
	@Override
	public void save(StaffRosterWeeklyPresetDto staffRosterDto, String userId) {
		String presetName = staffRosterDto.getPresetName();
		
		if (StringUtils.isEmpty(presetName)) {
			throw new GTACommonException(GTAError.HouseKeepError.PRESET_ROSTER_NAME_REQUIRE);
		}
		
		List<StaffRosterWeeklyPreset> staffRosterWeeklyPresetList = staffRosterWeeklyPresetDao.getStaffRosterWeeklyPreset(presetName);
		if (staffRosterWeeklyPresetList != null && !staffRosterWeeklyPresetList.isEmpty()) {
			throw new GTACommonException(GTAError.HouseKeepError.PRESET_ROSTER_EXIST);
		}

		List<StaffRosterWeeklyPresetRecordDto> list = staffRosterDto.getList();
		
		if (list.isEmpty()) {
			throw new GTACommonException(GTAError.HouseKeepError.PRESET_ROSTER_NO_DATA);
		}
		
		for (Iterator<StaffRosterWeeklyPresetRecordDto> iterator = list.iterator(); iterator.hasNext();) {
			StaffRosterWeeklyPresetRecordDto staffRosterWeeklyPresetRecordDto = (StaffRosterWeeklyPresetRecordDto) iterator
					.next();

			// validate
			if (staffRosterWeeklyPresetRecordDto.getWeekDay() <= 0 || staffRosterWeeklyPresetRecordDto.getWeekDay() >= 8) {
				throw new GTACommonException(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			}
			List<StaffRosterTimeDto> timeList = staffRosterWeeklyPresetRecordDto.getTimeList();

			if (timeList != null && !timeList.isEmpty()) {
				for (Iterator<StaffRosterTimeDto> timeIterator = timeList.iterator(); timeIterator.hasNext();) {
					StaffRosterTimeDto staffRosterTimeDto = (StaffRosterTimeDto) timeIterator.next();
					StaffRosterWeeklyPreset staffRosterWeeklyPreset = new StaffRosterWeeklyPreset();
					staffRosterWeeklyPreset.setPresetName(presetName);
					staffRosterWeeklyPreset.setWeekDay(staffRosterWeeklyPresetRecordDto.getWeekDay());
					staffRosterWeeklyPreset.setBeginTime(staffRosterTimeDto.getBeginTime());
					staffRosterWeeklyPreset.setEndTime(staffRosterTimeDto.getEndTime());
					staffRosterWeeklyPreset.setCreateDate(new Date());
					staffRosterWeeklyPreset.setCreateBy(userId);
					staffRosterWeeklyPreset.setOffDuty(false);
					staffRosterWeeklyPresetDao.save(staffRosterWeeklyPreset);
				}
			} else {

				StaffRosterWeeklyPreset staffRosterWeeklyPreset = new StaffRosterWeeklyPreset();
				staffRosterWeeklyPreset.setPresetName(presetName);
				staffRosterWeeklyPreset.setWeekDay(staffRosterWeeklyPresetRecordDto.getWeekDay());
				staffRosterWeeklyPreset.setCreateDate(new Date());
				staffRosterWeeklyPreset.setCreateBy(userId);
				staffRosterWeeklyPreset.setOffDuty(staffRosterWeeklyPresetRecordDto.isOffDuty());
				staffRosterWeeklyPresetDao.save(staffRosterWeeklyPreset);
			}

		}
	}

	@Transactional
	@Override
	public Set<String> searchStaffRoster(String presetName) {
		Set<String> set = new LinkedHashSet<>();
		List<StaffRosterWeeklyPreset>  presetList = staffRosterWeeklyPresetDao.searchByPresetName(presetName);
		for (StaffRosterWeeklyPreset staffRosterWeeklyPreset: presetList) {
			set.add(staffRosterWeeklyPreset.getPresetName());
		}
		return set;
	}

	@Transactional
	@Override
	public void deleteStaffRoster(String presetName) {
		staffRosterWeeklyPresetDao.delete(presetName);
	}

	@Transactional(rollbackFor = {GTACommonException.class, RuntimeException.class})
	@Override
	public void update(StaffRosterWeeklyPresetDto staffRosterDto, String userId, String presetName) {
		if (presetName != null) {
			staffRosterWeeklyPresetDao.delete(presetName);
		}
				
		save(staffRosterDto, userId);
	}

}
