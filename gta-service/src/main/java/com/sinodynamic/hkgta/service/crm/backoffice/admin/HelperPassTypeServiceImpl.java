package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.HelperPassTypeDao;
import com.sinodynamic.hkgta.entity.crm.HelperPassType;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;

@Service
public class HelperPassTypeServiceImpl extends ServiceBase<HelperPassType>
	implements HelperPassTypeService {

    @Autowired
    private HelperPassTypeDao helperPassTypeDao;

    @Override
    @Transactional
    public void checkIfHelperPassTypeExpired() {

	Date curDate = new Date();
	Date yesDate = CommUtil.addDays(curDate, -1);
	String paramDate = DateConvertUtil.getYMDFormatDate(yesDate);

	List<HelperPassType> passTypes = helperPassTypeDao.getExpiryPassTypeByCurdate(paramDate);
	logger.info("get HelperPassType expired size:"+(null==passTypes?0:passTypes.size())+",and update status is EXP");
	if (passTypes != null && passTypes.size() > 0) {

	    for (HelperPassType passType : passTypes) {
	    logger.info("update HelperPassType  expired  set status is EXP,  typeId:"+passType.getTypeId());
		passType.setStatus("EXP");
		passType.setUpdateBy("ADMIN");
		passType.setUpdateDate(curDate);
		helperPassTypeDao.updatePassType(passType);
	    }
	}

    }

}
