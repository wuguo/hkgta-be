package com.sinodynamic.hkgta.service.qst;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.qst.QuestionnaireSettingListDto;
import com.sinodynamic.hkgta.entity.qst.Survey;
import com.sinodynamic.hkgta.entity.qst.SurveyMember;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface SurveyService extends IServiceBase<Survey> {
	

	/**
	 * 获取问卷设置
	 * @return
	 */
	public List<Survey> getQuestionnaireSetting();

	/**
	 * 更新问卷设置
	 * @return
	 */
	public ResponseResult updateQuestionnaireSetting(QuestionnaireSettingListDto listDto, String userId);
	
}
