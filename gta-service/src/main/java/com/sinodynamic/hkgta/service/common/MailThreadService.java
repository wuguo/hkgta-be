package com.sinodynamic.hkgta.service.common;

import java.io.File;
import java.util.List;

import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;

public interface MailThreadService {
	
    public void send(final CustomerEmailContent cec, final File[] attachments);
    
    public void send(final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList);
    
    public void send(final CustomerEmailContent cec);
    
    public void sendWithResponse(final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList);
    
    public void sendWithResponse(final Long batchId,final CustomerEmailContent cec, final List<byte[]> bytesList, final List<String> mineTypeList,final List<String> fileNameList) ;

}
