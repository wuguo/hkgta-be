package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import com.sinodynamic.hkgta.dto.pms.HouseKeepTaskDto;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;

public interface HouseKeepTaskService {
	
	public StringBuilder getListSQLByJobType(String jobType);
	/***
	 * get HouseKeepTask by jobType 
	 * @param jobType ADH\RUT and so on
	 * @param Day
	 * @return
	 */
	public List<HouseKeepTaskDto> getListByJobType(String jobType,String Day);
	
	/***
	 *  weekly 
	 * @param userId Room Attedant name
	 * @param startTime 
	 * @param endTime
	 * @return sql
	 */
	public StringBuilder getRoomAttedantSql(String userId,String startTime,String endTime);
	
	
	public byte[] getHouseKeepTaskAttach(String jobType, String startTime, String endTime, String fileType, String roomNumber,
			String sortBy, String isAscending);
	
	public byte[] getRoomWeeklyAttendantAttach(String userId, String userName, String dateSql, String startTime, String endTime,
			String fileType, String sortBy, String isAscending);
	
}
