package com.sinodynamic.hkgta.service.crm.sales;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.FileUpload;

@Service
public class UploadFileServiceImpl extends ServiceBase<String> implements UploadFileService {

	private static Logger				logger			= Logger.getLogger(UploadFileServiceImpl.class);
	private static final List<String>	IMAGE_FORMAT	= Arrays.asList(
																".png",
																".jpeg",
																".bmp",
																".jpg",
																".gif",
																".BMP",
																".JPG",
																".JPEG",
																".PNG",
																".GIF");

	@Resource(name = "appProperties")
	Properties							appProps;

	public String uploadFiles(MultipartFile file) {

		String orinalName = file.getOriginalFilename();
		String suffix = orinalName.substring(orinalName.lastIndexOf("."));

		if (!IMAGE_FORMAT.contains(suffix)) {
			return "{\"warning\":\"file format error!\"}";
		}
		String filePath = null;

		try {

			filePath = FileUpload.upload(file, FileUpload.FileCategory.USER);
			logger.info("UploadFileService.uploadImages() invocation finished ...");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return filePath;
	}

}
