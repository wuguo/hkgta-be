package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.FacilityCode;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MemberQuickSearchService extends IServiceBase<CustomerProfile>{
	public ResponseResult quickSeachMember(String number);
	/***
	 * check member reservation perssion or Service Account expiry_date at reservationDate 
	 * @param number acadeId or 
	 * @param reservationDate
	 * @param facilityType
	 * @return
	 * @throws Exception
	 */
	public ResponseResult  checkPerssionOrServiceAccount(Long customerId,String number,String reservationDate,String startTime,String endTime,String facilityType)throws Exception;
	
	public FacilityCode convertFacilityCodeByType(String facilityType);

}
