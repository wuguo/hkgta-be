package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateListDto;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateTime;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityUtilizationRateTimeService extends IServiceBase<FacilityUtilizationRateTime> {

	public void saveFacilityUtilizationRateTime(FacilityUtilizationRateListDto facilityUtilizationRateDateListDto,String userId);
	
	List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeList(String facilityType,String weekDay, String subType);
	
	List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeList(String facilityType);
	
	List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeListBySubType(String facilityType,String facilitySubtypeId);
}
