package com.sinodynamic.hkgta.service.fms;

import com.sinodynamic.hkgta.util.response.ResponseResult;

public class CourseSessionCustomizeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private ResponseResult responseResult;

    public CourseSessionCustomizeException() {
    }

    public CourseSessionCustomizeException(ResponseResult responseResult) {
	this.responseResult = responseResult;
    }

    public ResponseResult getResponseResult() {
	return responseResult;
    }

    public void setResponseResult(ResponseResult responseResult) {
	this.responseResult = responseResult;
    }

}
