package com.sinodynamic.hkgta.service.mms;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.AdultDependentMemberDto;
import com.sinodynamic.hkgta.dto.mms.GuestRequestDto;
import com.sinodynamic.hkgta.entity.mms.SpaMemberSync;

public interface MemberSynchronizeService {

	public void synchronizeGtaMember2Mms(String academyNos);

	/**
	 * 查询所有未同步的member列表
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<AdultDependentMemberDto> getSyncFailureMemberList() throws Exception;

	/**
	 * 手动同步所有Member到MMS
	 * 
	 * @return
	 * @throws Exception
	 */
	public int manualSyncMemberByMMS(String academyNos) throws Exception;

	/**
	 * 根据academyNos指定查询member等数据进行同步
	 * 
	 * @param academyNos
	 * @return
	 * @throws Exception
	 */
	public List<AdultDependentMemberDto> manualSyncMemberByAcademyNos(String academyNos) throws Exception;

	public void excuteSynchGtaMember2Mms(GuestRequestDto dto);

	public List<GuestRequestDto> getMemberInfoList2Synchronize(String academyNos);
	
	public List<SpaMemberSync>getAllSpaMemberSynchThenGuestIdIsNotNull();
	
	public void updateSpaMemberSync(Long sysId,String lastResponseMsg); 
}
