package com.sinodynamic.hkgta.service.sys;

import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface CheckServerService extends IServiceBase<UserMaster> {
	void check();


}
