package com.sinodynamic.hkgta.service.pos;

import com.sinodynamic.hkgta.dto.pos.RestaurantMenuItemDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RestaurantMenuItemService  extends IServiceBase<RestaurantMenuItem> {

	void updateMenuItem(String itemNo, RestaurantMenuItemDto restaurantMenuItemDto, String userId);
	
	void changeStatus(RestaurantMenuItemDto restaurantMenuItemDto, String status, String userId);
	
	void deleteMenuItem(RestaurantMenuItemDto restaurantMenuItemDto, String userId);
	
	void changeDisplayOrder(RestaurantMenuItemDto restaurantMenuItemDto, String userId);
	
	Integer[] getMinAndMaxDisplayOrder(String restaurantId, String catId);
}
