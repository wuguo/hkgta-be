package com.sinodynamic.hkgta.service.sys;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.sys.ProgramMasterDao;
import com.sinodynamic.hkgta.entity.crm.ProgramMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
@Service
public class ProgramMasterServiceImpl extends ServiceBase<ProgramMaster> implements ProgramMasterService {
	@Autowired
	private ProgramMasterDao programMasterDao;

	@Override
	@Transactional
	public List<ProgramMaster> getAvailablePrograms(BigInteger roleId) {
		 List<Serializable> param = new ArrayList<Serializable>();
    	 param.add(roleId);
		return (List<ProgramMaster>) programMasterDao.getByHql("from ProgramMaster where status='ACT' and programType in ('P', 'R') and programId not in (select programMaster.programId from RoleProgram where roleMaster.roleId=?) order by programId, sortSeq",param);
	}
}
