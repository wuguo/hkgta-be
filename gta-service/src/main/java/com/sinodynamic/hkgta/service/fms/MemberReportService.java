package com.sinodynamic.hkgta.service.fms;

public interface MemberReportService {

	public StringBuilder getSqlByStatus(String status);

	public StringBuilder getServicePlanByCustID(String customerIDArray);

	public StringBuilder getExipryDateByCustID(String customerIDArray);

	public StringBuilder getNWDDirectMarketingByCustID(String customerIDArray);

	public StringBuilder getHKGTADirectMarketingByCustID(String customerIDArray);
}
