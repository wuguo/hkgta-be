package com.sinodynamic.hkgta.service.sys;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.sys.CarparkInterfaceImportDao;
import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceImport;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class CarparkInterfaceImportServiceImpl extends ServiceBase<CarparkInterfaceImport>  implements CarparkInterfaceImportService {

	@Autowired
	private CarparkInterfaceImportDao carparkInterfaceImportDao;
	
	@Transactional
	public Serializable save(CarparkInterfaceImport carparkInterfaceImport)throws Exception{
		// TODO Auto-generated method stub
		return carparkInterfaceImportDao.save(carparkInterfaceImport);
	}

	@Transactional
	public boolean update(CarparkInterfaceImport carparkInterfaceImport) throws Exception {
		// TODO Auto-generated method stub
		return carparkInterfaceImportDao.update(carparkInterfaceImport);
	}

	@Transactional
	public boolean checkExistByFileName(String fileName) throws Exception {
		Object object=carparkInterfaceImportDao.getUniqueByHql("from CarparkInterfaceImport where importFilename='"+fileName+"'");
		if(null!=object){
			return true;
		}
		return false;
	}

}
