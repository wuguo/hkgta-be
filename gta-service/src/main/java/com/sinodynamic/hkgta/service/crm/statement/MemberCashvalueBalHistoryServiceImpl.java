package com.sinodynamic.hkgta.service.crm.statement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.DeliveryRecordDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashvalueBalHistoryDao;
import com.sinodynamic.hkgta.dto.statement.SearchStatementsDto;
import com.sinodynamic.hkgta.dto.statement.StatementDto;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalueBalHistory;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class MemberCashvalueBalHistoryServiceImpl extends ServiceBase<MemberCashvalueBalHistory> implements
		MemberCashvalueBalHistoryService {
	
	@Autowired
	private MemberCashvalueBalHistoryDao memberCashvalueBalHistoryDao;
	
	@Autowired
	private DeliveryRecordDao deliveryRecordDao;
	
	@Override
	@Transactional
	public ResponseResult getStatements(ListPage page, SearchStatementsDto dto) {
		ListPage<MemberCashvalueBalHistory> listPage = memberCashvalueBalHistoryDao.getStatements(page,dto);
		Data data = new Data();
		data.setList(listPage.getDtoList());
		data.setTotalPage(page.getAllPage());;
		data.setCurrentPage(page.getNumber());
		data.setPageSize(page.getSize());
		data.setRecordCount(page.getAllSize());
		data.setLastPage(page.isLast());
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}
	@Transactional
	public ResponseResult getAllCademyNos(ListPage page,SearchStatementsDto dto) {
		String  sql = "   SELECT\n" +
				"        customerId,\n" +
				"        academyId,\n" +
				"        memberName,\n" +
				"       (CASE memberType \n" +
				"           WHEN 'IPM' THEN 'Individual' \n" +
				"          WHEN 'CPM' THEN 'Corporate' \n" +
				"            ELSE '' \n" +
				"            END) as memberType,\n" +
				"        closingBalance,\n" +
				"        if(IFNULL(successCount,0)>0,'Yes','No') as deliveryStatus,\n" +
				"        memberEmail\n"+
				"    FROM\n" +
				"        (  SELECT\n" +
				"            m.customer_id as customerId,\n" +
				"            m.academy_no as academyId,\n" +
				"            CONCAT(cp.salutation,\n" +
				"            ' ',\n" +
				"            cp.given_name,\n" +
				"            ' ',\n" +
				"            cp.surname) as memberName,\n" +
				"       	 m.member_type as memberType,\n" +
				"            mcbh.recal_balance as closingBalance,\n" +
				"            mcbh.cutoff_date as cutOffDate,\n" +
				"            cp.contact_email as memberEmail\n"+
				"        from\n" +
				"            member_cashvalue_bal_history mcbh ,\n" +
				"            customer_profile cp,\n" +
				"            member m  \n" +
				"        where\n" +
				"            mcbh.customer_id = m.customer_id \n" +
				"            and cp.customer_id = m.customer_id ) result \n" +
				"   left join \n" +
				"   (\n" +
				"       select sum(case when content.status = 'SENT' then 1 else 0 end) as successCount,content.recipient_customer_id as recipientCustomerId\n" +
				"       FROM\n" +
				"        batch_send_statement_hd hd,\n" +
				"        batch_send_statement_list list,\n" +
				"        customer_email_content content \n" +
				"    WHERE\n" +
				"        hd.batch_id = list.batch_id \n" +
				"        AND list.email_send_id = content.send_id \n" +
				"        AND DATE_FORMAT(hd.statement_month, '%Y-%m') = ?  \n" +
				"        group by content.recipient_customer_id\n" +
				"   ) deliveryRecord \n" +
				"   on  deliveryRecord.recipientCustomerId = result.customerId\n" +
				"   where\n" +
				"        1=1 \n" +
				"        <QueryCondition> ";
		
		StringBuilder queryCondition = new StringBuilder();
		List<Serializable> param = new ArrayList<>();
		String year = dto.getYear();
		String month = dto.getMonth();
		String cutOffDateString  = year+"-"+month+"-01 23:59:59";
		String monthOfStatement = year+"-"+month;
		int mon = Integer.parseInt(month);
		if (mon < 10) {
			cutOffDateString  = year+"-0"+month+"-01 23:59:59";
			monthOfStatement = year+"-0"+month;
		}
		param.add(monthOfStatement);
		Date date = DateConvertUtil.getDateFromStr(cutOffDateString);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date date2 = calendar.getTime();
		String cutOffDate = DateConvertUtil.date2String(date2, "yyyy-MM-dd HH:mm:ss");
		
		if (CommUtil.notEmpty(year) && CommUtil.notEmpty(month)) {
			queryCondition.append(" and cutOffDate = ? ");
			param.add(cutOffDate);
		}
		
		String memberType = dto.getMemberType();
		String deliveryStatus = dto.getDeliveryStatus();
		
		if (CommUtil.notEmpty(memberType) && !"ALL".equalsIgnoreCase(memberType)) {
			queryCondition.append(" and memberType = ? ");
			param.add(memberType);
		}
		
		if (CommUtil.notEmpty(deliveryStatus) && !"ALL".equalsIgnoreCase(deliveryStatus)) {
			if("Yes".equalsIgnoreCase(deliveryStatus)){
				queryCondition.append(" and IFNULL(deliveryRecord.successCount,0) >0 ");
			}else{
				queryCondition.append(" and IFNULL(deliveryRecord.successCount,0) = 0 ");
			}
		}
		sql = sql.replace("<QueryCondition>", queryCondition.toString());
		sql = "select finalSql.* from ( "+ sql+" ) finalSql";
		if(!StringUtils.isBlank(page.getCondition())){
			String advanceCondition = " where "+page.getCondition();
			sql = sql+advanceCondition;
		}
		List<StatementDto>list=memberCashvalueBalHistoryDao.getDtoBySql(sql,param,StatementDto.class);
		
		Long[]datas=new Long[(null!=list&&list.size()>0?list.size():0)];
		if(null!=list&&list.size()>0)
		{
			int i=0;
			for (StatementDto statementDto : list) {
				datas[i]=statementDto.getCustomerId();
				i++;
			}
		}
	   responseResult.setData(datas);
	   return responseResult;
	}

}
