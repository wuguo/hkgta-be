package com.sinodynamic.hkgta.service.crm.sales.enrollment;

import com.sinodynamic.hkgta.dto.crm.RemarksDto;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;


public interface RemarksService extends IServiceBase<CustomerPreEnrollStage>{
	
	public ResponseResult addRemark(RemarksDto remarksDto, String loginUserId,String staffType);
	
	public ResponseResult getRemarkList(ListPage<CustomerPreEnrollStage> page,Long customerId,String userId,String staffType);
	
	public ResponseResult changeRemarksStatus(Long customerId,String loginUserId);
	
	public ResponseResult checkUnreadRemark(Long customerId,String userId);
	
	public ResponseResult getRemarkListByDevice(ListPage<CustomerPreEnrollStage> page, Long customerId,String loginUserId,String loginDevice);

	public ResponseResult addRemarkByDevice(RemarksDto remarksDto, String loginUserId, String loginDevice);
	
	public ResponseResult checkUnreadRemarkByDevice(Long customerId, String loginUserId, String device);
}
