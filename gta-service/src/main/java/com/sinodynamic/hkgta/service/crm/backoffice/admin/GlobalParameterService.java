package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.crm.GlobalParameterListDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface GlobalParameterService extends IServiceBase<GlobalParameter>{

	public  ResponseResult saveGlobalParameter();
	
	public  ResponseResult getGlobalParameterById(String paramId);
	
	public ResponseResult setGlobalQuota(String paramValue, String userId);
	
	public  GlobalParameter getGlobalParameter(String paramId);
	
	public List<GlobalParameter> getGlobalParameterListByParamCat(String paramCat);
	
	public void saveOrUpdateGlobalParameters(List<GlobalParameter> globalParameters);

	public ListPage<GlobalParameter> getGlobalParameterList(String paramCat, String sortBy, Boolean isAscending, ListPage<GlobalParameter> page);
	
	public void updateGlobalParameter(GlobalParameter globalParameter);

	/**
	 * 更新GuestRoom系统配置
	 * @param isCheckPush
	 * @param pushTime
	 * @param userId
	 * @return
	 */
	public ResponseResult setGuestRoomSysConfig(String isCheckPush, String pushTime, String userId);

	/**
	 * 初始化GuestRoom配置数据入缓存中
	 * @return
	 */
	public boolean initGuestRoomSysConfig();
	
	public  ResponseResult saveGlobalParameter(GlobalParameter GlobalParameter);

	public ResponseResult saveGlobalParameters(GlobalParameterListDto info, String userId);

	public List<GlobalParameter> getGeneralGlobalParameterList();
	
	public boolean checkGlobalParameterSwitch(String paramId);
	
	/***
	 * check payment type turn on /off
	 * @param paramId
	 * @return
	 */
	public ResponseResult checkPaymentCardTypeTurnOn(String paramId);
	
	public  ResponseResult checkReportIssue();
}
