package com.sinodynamic.hkgta.service.common;

import com.sinodynamic.hkgta.dto.EntityHistoryDto;

public interface EntityHistoryService {

    EntityHistoryDto getEntityHistory(String route, String[] names, String[] values);

}
