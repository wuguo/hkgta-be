package com.sinodynamic.hkgta.service.fms;

import com.sinodynamic.hkgta.entity.crm.MsgPushHistory;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface MsgPushHistoryService extends IServiceBase<MsgPushHistory>{
    
    public String getSqlForPullMessages(String userId, String deviceArn, String application);
}
