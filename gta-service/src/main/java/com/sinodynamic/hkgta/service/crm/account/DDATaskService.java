package com.sinodynamic.hkgta.service.crm.account;

import java.io.File;

import com.sinodynamic.hkgta.service.IServiceBase;

public interface DDATaskService extends IServiceBase{
	
	public void parseDDAConfirmReporter(File file)throws Exception;
	public void saveDDALog(String fileName,String errorMsg);
	
}
