package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.CorporateProfileDto;
import com.sinodynamic.hkgta.entity.crm.CorporateProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CorporateService extends IServiceBase<CorporateProfile>{
	
	public ResponseResult createCorporateAcc(CorporateProfileDto corporateProfile);
	
	public ResponseResult viewCorporateAcc(Long corporateId);
	
	public ResponseResult deactivateCorporateAcc(Long corporateId,String loginUserId,String status,Long accNo,String userName, Integer corporateProfileVersion, Integer corporateServiceAccVersion);
	
	public ResponseResult editCorporateAcc(CorporateProfileDto corporateProfileDto) throws Exception;
	
	public ResponseResult editCorporateServicePlan(Long corporateId,Long planNo,String loginUserId );
	
	public ResponseResult createCorporateMember(CustomerProfile dto, String loginUserId, String fromName) throws Exception;
	
	public ResponseResult viewMember(CustomerProfile profile,Long customerId);
	
	public ResponseResult editCorporateMember (CustomerProfile customerProfile, String loginUserId);
	
	public ResponseResult getMemberList(ListPage<CorporateProfile> page,String byAccount,String status,Long filterByCustomerId);
	
	public ResponseResult getCorporateAccountDropList();
	
	public ResponseResult getMemberDropList(Long corporateId);

	public ResponseResult changeMemberStatus(Long customerId, String loginUserId,String status,String changeMemberStatus);
	
	public ResponseResult checkCreditLimit(@PathVariable Long corporateId);
	
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String module);
	
	public ResponseResult getValidServicePlanIncludeItself(Long corporateId);
	
	public ResponseResult listCorporateAcc(ListPage<CorporateProfile> page);
	
	public ResponseResult getCorporateMembershipSettlement(ListPage<CustomerOrderTrans> page,String status, String offerCode, Long filterByCustomerId);

	public ResponseResult getMemberListByAccount(ListPage page, String sortBy, String isAscending, Long corporateId, String status);
	
	public ResponseResult getCorporateContactByCorporateIdAndContactType(Long corporateId,String contactType);

	//added by Kaster 20160509
	public ResponseResult assignToSalesMan(Long corporateId, String salesStaffId);

	public ResponseResult archiveMember(Long customerId, String userId, String status, String fullName) throws Exception;

	/**
	 * guest room checkin后是否需要发送push message消息开关设置
	 * @param customerId
	 * @param isSilentCheckinPush  Y / N
	 * @return
	 */
	public ResponseResult silentCheckinPushSwitch(Long customerId, String isSilentCheckinPush);

}
