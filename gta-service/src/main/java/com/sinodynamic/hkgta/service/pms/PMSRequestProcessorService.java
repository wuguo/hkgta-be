package com.sinodynamic.hkgta.service.pms;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.pms.RoomReservationInfoDto;
import com.sinodynamic.hkgta.dto.pms.xml.input.changeroom.HTNGHotelRoomMoveNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.checkin.HTNGHotelCheckInNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.checkout.HTNGHotelCheckOutNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.epayment.HTNGChargePostingRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.facility.OTAGolfCourseAvailRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.membership.HTNGReadRQ;
import com.sinodynamic.hkgta.dto.pms.xml.output.changroom.HTNGHotelRoomMoveNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.checkin.HTNGHotelCheckInNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.checkout.HTNGHotelCheckOutNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.epayment.HTNGChargePostingRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.facility.OTAGolfCourseAvailRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.HTNGProfileReadRS;

/**
 * GTA provide API for PMS to call
 * @author Nick_Xiong
 *
 */
public interface PMSRequestProcessorService {
      
	public HTNGHotelCheckInNotifRS checkIn(HTNGHotelCheckInNotifRQ request);
	
	public HTNGHotelRoomMoveNotifRS changeRoom(HTNGHotelRoomMoveNotifRQ request);
	
	public HTNGHotelCheckOutNotifRS checkOut(HTNGHotelCheckOutNotifRQ request);
	
	public HTNGProfileReadRS  membership(HTNGReadRQ request);
	
	public OTAGolfCourseAvailRS queryBundle(OTAGolfCourseAvailRQ request) throws Exception;
	
	@Deprecated
	public Map<Date,Long> giftBundleByPeriod(Long customerId,Date arriveDate,Date departDate,String facilityType,String attributeId,String status);
	
	public Map<String,Long> giftBundleForReservations(Long customerId,List<RoomReservationInfoDto> reservations);
	
	public Long giftBundleForOneRoom(Long customerId,RoomReservationInfoDto reservation);
	
	public List<Long> giftBundleForOneRoomOneNightOneBooking(Long customerId,RoomReservationInfoDto reservation);
	
	public List<Long> giftBundleForOneRoomOneNightOneBooking(String paymentMethod, Long customerId,RoomReservationInfoDto reservation);
	
	public Map<String,List<Long>> giftBundleForReservationsOneNightOneBooking(Long customerId,List<RoomReservationInfoDto> reservations);
	
	public Map<String,List<Long>> giftBundleForReservationsOneNightOneBooking(String paymentMethod, Long customerId,List<RoomReservationInfoDto> reservations);
	
	public HTNGChargePostingRS epayment(HTNGChargePostingRQ request) throws Exception;
	
}