package com.sinodynamic.hkgta.service.pos;

import java.util.Date;

import com.sinodynamic.hkgta.entity.pos.RestaurantOpenHour;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RestaurantOpenHourService extends IServiceBase<RestaurantOpenHour> {
	
	public boolean isAvailableRestaurant(String restaurantId,Integer partySize, Date bookTime,String bookVia) throws Exception;
	public ResponseResult checkBookingQuotaPeriod(String restaurantId, Integer partySize, Date bookTime, String bookVia)
			throws Exception;
}
