package com.sinodynamic.hkgta.service.crm.sales.leads;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.lowagie.text.DocumentException;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAnalysisReportDto;
import com.sinodynamic.hkgta.dto.crm.CustomerCheckExsitDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEmailDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentSearchDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.membership.QuestionAnswerOrBirthDateDto;
import com.sinodynamic.hkgta.dto.staff.StaffPasswordDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerProfileReadLog;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;


public interface CustomerProfileService extends IServiceBase<CustomerProfile>{
	
	public ResponseResult getCustomerProfileList(ListPage<CustomerProfile> page, CustomerProfileDto t, String userId, String device);
	
	public Serializable addCustomerProfile(CustomerProfile t);
	
	public void deleteCustomerProfile(CustomerProfile t);
	
	public void updateCustomerProfile(CustomerProfile t);
	
	public CustomerProfile getCustomerProfile(CustomerProfile t);
	
	public CustomerProfile getById(Long customerId);
	
	public ResponseResult getEnrollments(ListPage<CustomerProfile> page,
			CustomerEnrollmentSearchDto dto,String userId,String device);
	
	/**
	 * @author Sam_Pang 
	 * @Description: check whether exist input passportType and passportNO
	 * 				 if it exists return CustomerProfile
	 *				 or return null
	 */
	public CustomerCheckExsitDto checkExistPassportNO(String passportType,String passportNO);
	
	public CustomerProfile getByCustomerID(Long customerID) throws Exception;
	/***
	 * get customerInfo by customerID and supCustomerId
	 * @param customerID
	 * @param supCustomerId
	 * @return
	 * @throws Exception
	 */
	public CustomerProfile getCustomerByID(Long customerID,Long supCustomerId) throws Exception;
	
	public CustomerProfile getCustomerInfoByCustomerID(Long customerID) throws Exception;

	public ResponseResult getAccountInfo(Long customerID);

	public void moveProfileAndSignatureFile(CustomerProfile cProfile) throws Exception;
	public ResponseResult getCustomerIdByUserId(String userId);

	public ResponseResult getDependentMmeberInforByCustomerId(Long customerId);

	public ResponseResult updateDependentMembersRight(String userId,Long customerId,
			String facilityRight, String trainingRight, String eventRight,
			String dayPass, BigDecimal tran);
	
	public ResponseResult getMmeberRightsByCustomerId(Long customerId);

	public ResponseResult editLimitValue(Long customerID, BigDecimal limitValue,String loginUserId,Integer memberLimitRuleVersion) throws Exception;

	CustomerProfile getCustomerProfileByPassportTypeAndPassportNo(String passportType, String passportNO);
	
	public ResponseResult getAcademyNoReserveStatus(Long customerId);
	
	public ResponseResult sendEnrollmentFormEmail(CustomerEmailDto emailDto,String userName,String userId);
	
	public byte[] getEnrollFormInOnePdf(Long customerId,String userId) throws DocumentException, IOException;
	
	public boolean checkAvailablePassportNo(String passportType, String passportNO,Long customerId);

	public List<CustomerProfile> getCustomers();
	
	public byte[] getEnrollFormReport(Long custonerId)throws Exception;

	public ResponseResult verifyMemberInfo(String surname, String givenName, String passportType, String passportNo);

	public ResponseResult validateBirthDateOrQuestionAnswer(QuestionAnswerOrBirthDateDto questionAnswerOrBirthDateDto);

	public ResponseResult updateMemberPwd(StaffPasswordDto staffPasswordDto);

	public ResponseResult getUserActiveRequest(String userId);

	/**   
	* @author: Zero_Wang
	* @since: Sep 18, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	public ResponseResult checkMemberExistByPassportTypeAndPassportNo(
			String passportType, String passportNo);

	/**
	 * Used to get the dependent creation right for
	 * @author Liky_Pan
	 * @param enrollStatus
	 * @param planNo
	 * @param customerId
	 * @return
	 */
	public boolean getDependentCreationRight(String enrollStatus,Long planNo ,Long customerId);
	
	public Map<String, Object> getPersonalInfoForTablet(Long customerId, String device);
	
	public CustomerProfile importPrimaryDeailForAutofillingDependentForm(Long customerId);
	
	public List<CustomerAdditionInfoDto> getAnalysisInfo(Long customerId);

	/**
	 * 根据类型获取查询Overview列表语句
	 * @param type
	 * @return
	 */
	public String getOverviewList(String type);

	/**
	 * 根据类型获取用户分析报告
	 * @param categoryType
	 * @return
	 */
	public Map<String, Map<String, List<CustomerAnalysisReportDto>>> getCustomerAnalysisReport(String categoryType);

	public void archivePassportNo(Long customerId) throws Exception;

	public void unArchivePassportNo(Long customerId) throws Exception;

	public ResponseResult archiveMember(Long customerId, String loginUserId, String status, String userName) throws Exception;

	public ResponseResult unArchiveMember(Long customerId, String loginUserId, String status, String userName);
	
	
	public ResponseResult addCustomerProfileReadLog(CustomerProfileReadLog customerProfileReadLog);
	
	public List<CustomerProfileReadLog> getListByReadTime(Date readTime);
	

	public CustomerProfileDto getCustomerProfilePhotoAndSinature(Long customerId);
	
	public void createBase64Image()throws Exception;

	/***
	 * check customer setting TRN/CR limit value ,
	 * if service plan month length is one ,use add depend member ,the trn/cr limit value is null 
	 * then edit limit exception 
	 * so do deal with it
	 * get customer service plan ,if limit is null then setting limit value expride date is add service plan month length
	 * 
	 */
	public  void setMemberLimitExpridate(Long customerId);

	
	public boolean checkCloseCustomerProfile(Long customerId);
	
	/***
	 * 
	 * @param condition is acadeNo,First Name, Last Name, First Name + Last Name or Last Name + First Name
	 * @return
	 */
	public ResponseResult getPersonalInfoListByCondition(String condition);
	
}
