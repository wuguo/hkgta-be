package com.sinodynamic.hkgta.service.crm.sales.renewal;

import com.sinodynamic.hkgta.dto.crm.ExpiredMemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberInfoDto;
import com.sinodynamic.hkgta.dto.crm.RenewalDto;
import com.sinodynamic.hkgta.dto.crm.RenewalEmailDto;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.MemberExpiredOrderByCol;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RenewalService extends IServiceBase{
	/***
	 * add isMyclient  and userId to search
	 * @param searchText
	 * @param expiringDays
	 * @param orderByCol
	 * @param sortType
	 * @param pListPage
	 * @param isMyClient
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public ListPage<ExpiredMemberDto> getExpiringMemberList(String searchText,Integer expiringDays, MemberExpiredOrderByCol orderByCol, String sortType, ListPage<ExpiredMemberDto> pListPage,String isMyClient,String userId) throws Exception;
	
	/***
	 * add isMyclient  and userId to search
	 * @param searchText
	 * @param expiredDays 
	 * @param orderByCol
	 * @param sortType
	 * @param pListPage
	 * @param isMyClient
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public ListPage<ExpiredMemberDto> getExpiredMemberList(String searchText,String expiredDays, MemberExpiredOrderByCol orderByCol, String sortType, ListPage<ExpiredMemberDto> pListPage,String isMyClient,String userId) throws Exception;
	
	
//	private String surName;
//	
//	private String givenName;
//	
//	private String companyName;
	
	/*public ListPage<ExpiredMemberDto> getExpiredMemberList(String searchCondition, MemberExpiredOrderByCol orderByCol,
			String sortType, ListPage<ExpiredMemberDto> pListPage) throws Exception;*/

	ResponseResult startUpMemberAcc(RenewalDto renewalDto) throws Exception;

	MemberInfoDto getMemberInfo(Long customerId);

	public ResponseResult sendRenewWithInvoiceEmail(RenewalEmailDto mailReq, String userId, String userName);

	public boolean checkExistsByNewPlanStatus(Long customerId, String status);

}

