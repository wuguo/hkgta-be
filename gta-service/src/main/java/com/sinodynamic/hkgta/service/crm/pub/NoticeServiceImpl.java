package com.sinodynamic.hkgta.service.crm.pub;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.crm.pub.NoticeDao;
import com.sinodynamic.hkgta.dto.Constant.Status;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.pub.NoticeDto;
import com.sinodynamic.hkgta.dto.crm.pub.NoticeFileDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.Notice;
import com.sinodynamic.hkgta.entity.crm.NoticeFile;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.NoticeFileType;
import com.sinodynamic.hkgta.util.constant.NoticeStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;

/**
 * @author Mason_Yang
 *
 */
@Service
@Transactional
public class NoticeServiceImpl extends ServiceBase<Notice> implements NoticeService {

	@Autowired
	private NoticeDao noticeDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private SysCodeDao syscodeDao;
	
	@Autowired
	private StaffProfileDao staffProfileDao;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	
	@Override
	public long save(NoticeDto noticeDto, String createBy) {
		if (StringUtils.isEmpty(noticeDto.getSubject())) {
			throw new GTACommonException(GTAError.PublicationError.SUBJECT_IS_EMPTY);
		} else if (StringUtils.isEmpty(noticeDto.getContent())) {
			throw new GTACommonException(GTAError.PublicationError.CONTENT_IS_EMPTY);
		}
		
		String noticeType = noticeDto.getNoticeType();
		
		if (noticeType == null || syscodeDao.getByCategoryAndCodeValue("noticeType", noticeType) == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_TYPE);
		}
		
		Notice notice = new Notice();
		notice.setSubject(noticeDto.getSubject());
		notice.setContent(noticeDto.getContent());
		notice.setNoticeType(noticeType);
		notice.setNoticeBegin(noticeDto.getNoticeBegin());
		notice.setNoticeEnd(noticeDto.getNoticeEnd());
		setStatus(noticeDto.getStatus(), notice);
		
		notice.setCreateDate(new Date());
		notice.setCreateBy(createBy);
		NoticeFileDto noticeFileDto = noticeDto.getNoticeFile();
		if (noticeFileDto != null) {
			NoticeFile noticeFile = new NoticeFile();
			noticeFile.setCreateBy(createBy);
			noticeFile.setCreateDate(new Date());
			noticeFile.setNotice(notice);
			noticeFile.setFileName(noticeFileDto.getFileName());
			noticeFile.setFileType(NoticeFileType.IMG.toString());
			notice.addNoticeFiles(noticeFile);
		}
		Long excu=(long) noticeDao.save(notice);
		if(Status.ACT.name().equals(notice.getStatus())&&checkNoticeDate(notice)){
			this.pushNotice(notice,createBy);
		}
		return excu;
	}

	/***
	 * if noticeBegin equal NoticeEnd and equal currentDate  pushNotice all parton
	 * @param notice
	 */
	private boolean checkNoticeDate(Notice notice)
	{
		/***
		 * SGG-3696
		 */
//		if(notice.getNoticeBegin().compareTo(notice.getNoticeEnd())==0)
//		{
			Date currentDate=DateConvertUtil.parseString2Date(DateConvertUtil.formatCurrentDate(new Date(),"yyyy-MM-dd"), "yyyy-MM-dd");
			Date noticeDate=DateConvertUtil.parseString2Date(DateConvertUtil.parseDate2String(notice.getNoticeBegin(), "yyyy-MM-dd"),"yyyy-MM-dd");
			if(currentDate.compareTo(noticeDate)==0)
			{
				return true;
			}else{
				return false;
			}
//		}else{
//			return false;
//		}
	}
	private void setStatus(String status, Notice notice) {
		if (NoticeStatus.ACT.toString().equals(status)) {
			notice.setStatus(NoticeStatus.ACT.toString());
		} else if (NoticeStatus.NACT.toString().equals(status)) {
			notice.setStatus(NoticeStatus.NACT.toString());
		} else {
			throw new GTACommonException(GTAError.PublicationError.STATUS_ERROR);
		}
	}

	@Override
	public NoticeDto getNotice(long noticeId) {
		
		Notice notice = noticeDao.get(Notice.class, noticeId);
		
		if(notice == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_NOT_FOUND);
		}
		return getNoticeDto(notice);
	}

	private NoticeDto getNoticeDto(Notice notice) { 
		NoticeDto noticeDto = new NoticeDto();
		noticeDto.setNoticeId(notice.getNoticeId());
		noticeDto.setSubject(notice.getSubject());
		String noticeType = notice.getNoticeType();
		noticeDto.setNoticeType(noticeType);
		noticeDto.setRecipientType(notice.getRecipientType());
		noticeDto.setContent(notice.getContent());
		noticeDto.setNoticeBegin(notice.getNoticeBegin());
		noticeDto.setStatus(notice.getStatus());
		noticeDto.setCreateBy(notice.getCreateBy());
		noticeDto.setNoticeEnd(notice.getNoticeEnd());
		
		StaffProfile staffProfile = staffProfileDao.getByUserId(notice.getCreateBy());
		
		if (staffProfile != null) {
			noticeDto.setCreator(staffProfile.getGivenName() + " " + staffProfile.getSurname());
		}
		noticeDto.setCreateDate(notice.getCreateDate());
		//categoryName
		if (noticeType != null) {
			SysCode sysCode = syscodeDao.getByCategoryAndCodeValue("noticeType", noticeType);
			
			if (sysCode != null) {
				noticeDto.setCategoryName(sysCode.getCodeDisplay());
			}
		}
		
		List<NoticeFile> noticeFiles = notice.getNoticeFiles();
		if (noticeFiles != null && !noticeFiles.isEmpty()) {
			for (NoticeFile noticeFile : noticeFiles) {
				NoticeFileDto noticeFileDto = new NoticeFileDto();
				noticeFileDto.setFileId(noticeFile.getFileId());
				noticeFileDto.setFileName(noticeFile.getFileName());
				noticeDto.setNoticeFile(noticeFileDto);
			}

		}
		return noticeDto;
	}

	@Override
	public void update(NoticeDto noticeDto, String createBy) {
		if (StringUtils.isEmpty(noticeDto.getSubject())) {
			throw new GTACommonException(GTAError.PublicationError.SUBJECT_IS_EMPTY);
		} else if (StringUtils.isEmpty(noticeDto.getContent())) {
			throw new GTACommonException(GTAError.PublicationError.CONTENT_IS_EMPTY);
		}
		
		String noticeType = noticeDto.getNoticeType();
		
		if (noticeType == null || syscodeDao.getByCategoryAndCodeValue("noticeType", noticeType) == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_TYPE);
		}
		
		Notice notice = noticeDao.get(Notice.class, noticeDto.getNoticeId());
		
		if(notice == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_NOT_FOUND);
		}
		
		Date start=notice.getNoticeBegin();
		Date end=notice.getNoticeEnd();
		
		notice.setSubject(noticeDto.getSubject());
		notice.setContent(noticeDto.getContent());
		notice.setNoticeType(noticeType);
		notice.setNoticeBegin(noticeDto.getNoticeBegin());
		notice.setNoticeEnd(noticeDto.getNoticeEnd());
		setStatus(noticeDto.getStatus(), notice);
		notice.setUpdateDate(new Date());
		notice.setUpdateBy(createBy);

		notice.getNoticeFiles().clear();
		NoticeFileDto noticeFileDto = noticeDto.getNoticeFile();

		if (noticeFileDto != null) {
			NoticeFile noticeFile = new NoticeFile();
			noticeFile.setCreateBy(createBy); 
			noticeFile.setCreateDate(new Date());
			noticeFile.setNotice(notice);
			noticeFile.setFileName(noticeFileDto.getFileName());
			noticeFile.setFileType(NoticeFileType.IMG.toString());

			notice.addNoticeFiles(noticeFile);
		}
		noticeDao.update(notice);
		
		if(checkNoticeChange(start,end,noticeDto)&&checkNoticeDate(notice))
		{
			this.pushNotice(notice,createBy);	
		}
	}

	private boolean checkNoticeChange(Date start,Date end,NoticeDto noticeDto){
		Date orgBegin=DateConvertUtil.parseString2Date(DateConvertUtil.formatCurrentDate(start,"yyyy-MM-dd"), "yyyy-MM-dd");
		Date orgEnd=DateConvertUtil.parseString2Date(DateConvertUtil.formatCurrentDate(end, "yyyy-MM-dd"),"yyyy-MM-dd");
		Date dtoBegin=DateConvertUtil.parseString2Date(DateConvertUtil.formatCurrentDate(noticeDto.getNoticeBegin(),"yyyy-MM-dd"), "yyyy-MM-dd");
		Date dtoEnd=DateConvertUtil.parseString2Date(DateConvertUtil.formatCurrentDate(noticeDto.getNoticeEnd(), "yyyy-MM-dd"),"yyyy-MM-dd");
		if(Status.NACT.name().equals(noticeDto.getStatus()))
		{
			return false;
		}
		if(dtoBegin.compareTo(orgBegin)==0&&dtoEnd.compareTo(orgEnd)==0){
			return false;
		}
		return true;
	}
	
	@Override
	public void updateStatus(Long noticeId, String status, String userId) {

		Notice notice = noticeDao.get(Notice.class, noticeId);
		
		if(notice == null) {
			throw new GTACommonException(GTAError.PublicationError.NOTICE_NOT_FOUND);
		}
		
		setStatus(status, notice);
		notice.setUpdateDate(new Date());
		notice.setUpdateBy(userId);
		noticeDao.update(notice);
	}

	@Override
	public Data getNoticeList(Integer pageNumber, Integer pageSize) {

		ListPage<Notice> listPage = noticeDao.queryNoticeList(null, pageNumber, pageSize);
		List<Notice> notices = listPage.getList();
		List<NoticeDto> noticeDtos = new ArrayList<>();
		if (notices != null && !notices.isEmpty()) {
			for (Notice notice: notices) {
				NoticeDto noticeDto  = getNoticeDto(notice);
				noticeDtos.add(noticeDto);
			}
		}

		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(noticeDtos);
		
		return data;
	}

	@Override
	public void updateExpiredNewsToInactive() {
		noticeDao.updateExpiredNewsToInactive();
		
	}

	@Override
	@Transactional
	public List<Notice> getNoticeByDate(Date date) {
		   String hql="from Notice n where n.status='ACT' and ?  between n.noticeBegin and  n.noticeEnd";
		   List<Serializable>param=new ArrayList<>();
		   param.add(date);
		   return noticeDao.excuteByHql(Notice.class, hql, param);
	}
	/**
	 * SGG-3716
	 */
	@Transactional
	public List<Notice> getPushNoticeSubjectByDate(Date date) {
	   String hql="SELECT n.subject AS subject FROM notice n WHERE n.status='ACT' AND  DATE_FORMAT(n.notice_begin,'%Y-%m-%d')= ?";
	   List<Serializable>param=new ArrayList<>();
	   param.add(DateConvertUtil.formatCurrentDate(date, "yyyy-MM-dd"));
	   return noticeDao.getDtoBySql(hql, param, Notice.class);
	}

	@Override
	@Transactional
	public void pushNotice(Notice notice,String pushBy)
	{
		String hql="SELECT distinct m.user_id  as userId FROM member m LEFT JOIN user_device u ON  m.user_id=u.user_id WHERE  m.status='ACT' AND u.arn_endpoint IS NOT NULL  GROUP BY arn_endpoint ";
		List<Member>members=memberDao.getDtoBySql(hql, null, Member.class);
		String []userIds = null;//  new String[]{"0000426-1d6", "0000430-1da"};
		if(null!=members&&members.size()>0)
		{
			userIds=new String[members.size()];
			int i=0;
			for (Member me : members) 
			{
				userIds[i]=me.getUserId();
				i++;
			}
		}
		devicePushService.pushMessage(userIds, notice.getSubject(), notice.getContent(), pushBy, "member");
		
	}

}
