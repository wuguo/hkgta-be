package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.PermitCardLogDao;
import com.sinodynamic.hkgta.dao.adm.PermitCardMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.PrintMemberWithCardDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dto.crm.CustomerServiceAccDto;
import com.sinodynamic.hkgta.dto.crm.MemberCardPrintDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardLog;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.PermitCardMasterEnumStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRPropertiesUtil;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;


@Service
public class MembershipCardsManagmentServiceImpl extends ServiceBase implements MembershipCardsManagmentService{

	
	private static final String SCC = "0";
		
	@Autowired
	private PermitCardMasterDao permitCardMasterDao;
	
	@Autowired
	private PermitCardLogDao permitCardLogDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private PrintMemberWithCardDao printMemberWithCardDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CustomerServiceDao customerServiceDao;
	
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	
	
	
	@Override
	@Transactional
	public String linkCardForMember(String cardNo, Long customerId, String createBy)
			throws Exception {
		// TODO Auto-generated method stub
		PermitCardMaster permitCardMaster = permitCardMasterDao.getPermitCardMasterByCardNo(cardNo);
		if (null==permitCardMaster)
		{
			throw new GTACommonException(GTAError.DayPassPurchaseError.CARD_NO_IS_INVALID);
		}
		String status = permitCardMaster.getStatus();
		if (PermitCardMasterEnumStatus.ISS.getCode().equals(status)) {
			throw new GTACommonException(GTAError.DayPassPurchaseError.CARD_HAS_BEEN_ISSUED);
		}
		
		if (PermitCardMasterEnumStatus.DPS.getCode().equals(status)) {
			throw new GTACommonException(GTAError.DayPassPurchaseError.CARD_HAS_BEEN_DISPOSAL);
		}
		PermitCardLog cardLog = new PermitCardLog(permitCardMaster, PermitCardMasterEnumStatus.ISS.getCode(), createBy);
		cardLog.setCustomerId(customerId.toString());
		permitCardLogDao.save(cardLog);
		
		permitCardMaster.setStatus(PermitCardMasterEnumStatus.ISS.getCode());
		permitCardMaster.setStatusUpdateDate(new Date());
		permitCardMaster.setMappingCustomerId(customerId);
		permitCardMaster.setCardIssueDate(CommUtil.dateFormat(new Date(), "yyyy-MM-dd HH:mm:ss"));
		
		permitCardMaster.setUpdateBy(createBy);
		permitCardMasterDao.linkCardForMember(permitCardMaster);
		
		//new add
		CustomerEnrollment ce = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
		if (ce != null && EnrollStatus.ANC.name().equals(ce.getStatus())) {
			
			customerEnrollmentService.recordCustomerEnrollmentUpdate(ce.getStatus(),EnrollStatus.CMP.name(),customerId);
			
		    ce.setStatus(EnrollStatus.CMP.name());
		    ce.setUpdateBy(createBy);
		    ce.setUpdateDate(new Date());
		    customerEnrollmentDao.updateCustomerEnrollment(ce);
		}
		
		return SCC;
	}
	

	
	@Transactional
	public byte[] getAcademyCardPrintInfo(Long customerId) throws Exception{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/"+"jasper/";
		String parentjasperPath = reportPath+"AcademyCard.jasper";

		logger.info("getAcademyCardPrintInfo");
		File reFile = new File(parentjasperPath);
		Map<String,Object> parameters = new HashMap<String,Object>();
		DataSource ds=SessionFactoryUtils.getDataSource(customerProfileDao.getsessionFactory());
	    Connection dbconn=DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("customerId", customerId);

		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		DefaultJasperReportsContext context = DefaultJasperReportsContext.getInstance();
		JRPropertiesUtil.getInstance(context).setProperty("net.sf.jasperreports.default.font.name", "DejaVu Sans");
		JRPropertiesUtil.getInstance(context).setProperty("net.sf.jasperreports.default.pdf.embedded", "true");
		JRPropertiesUtil.getInstance(context).setProperty("net.sf.jasperreports.default.pdf.font.name", "DejaVu Sans");

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));

		exporter.exportReport();
		return outPut.toByteArray();
	}
	
	@Override
	@Transactional
	public ResponseResult getMemberCardPrintInfo(Long customerId)
			throws Exception {
		// TODO Auto-generated method stub
		Member member = memberDao.getMemberById(customerId);
		if(null == member){
			//return new ResponseResult("1","", "Member with customerId : " +customerId + " is not exist!");
			responseResult.initResult(GTAError.MemberShipError.MEMBER_IS_NOT_EXISTS, new Object[]{customerId});
			return responseResult;
		}
			
		
		MemberCardPrintDto m = null;
		
		
		if("IPM".equals(member.getMemberType()) || "CPM".equals(member.getMemberType())){
//			sql.append("	SELECT m.academy_no academyNo, CONCAT(IFNULL(p.salutation,''),' ',IFNULL(p.surname,''),' ',IFNULL(p.given_name,'')) customerName, c.expiry_date expiryDate FROM")
//			.append("	member m JOIN customer_profile p ON m.customer_id = p.customer_id JOIN (SELECT customer_id,")
//			.append(" MAX(acc.effective_date), DATE_FORMAT(expiry_date, '%Y/%m/%d') expiry_date FROM")
//			.append(" customer_service_acc acc WHERE acc.`status` = 'ACT' GROUP BY")
//			.append("	acc.customer_id) AS c ON m.customer_id = c.customer_id WHERE m.customer_id = ?");
			
			CustomerServiceAccDto latestAcc = customerServiceDao.getLatestServiceAccByCustomerId(customerId);
			CustomerProfile cp = customerProfileDao.getCustomerProfileByCustomerId(customerId.toString());
			
			if(null == latestAcc){
				//return new ResponseResult("1","", "Member with customerId : " +customerId + " has no latest Service Account!");
				responseResult.initResult(GTAError.MemberShipError.NO_LATEST_SERVICE_ACC_FOR_MEMBER, new Object[]{customerId});
				return responseResult;
			}
			
			if(null == cp){
				//return new ResponseResult("1","", "Member with customerId : " +customerId + " profile is null!");
				responseResult.initResult(GTAError.MemberShipError.MEMBER_PROFILE_IS_NULL, new Object[]{customerId});
				return responseResult;
			}	
				m = new MemberCardPrintDto();
				m.setAcademyNo(member.getAcademyNo());
				m.setCustomerName(cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname());
				
				if(null != latestAcc.getExpiryDate()){
					m.setExpiryDate(latestAcc.getExpiryDate());
				}
				
//			paramList.add(customerId);
			
//			m = printMemberWithCardDao.getMemberCardPrint(sql.toString(), paramList);
			
		}else if("IDM".equals(member.getMemberType()) || "CDM".equals(member.getMemberType())){
//			sql.append("		SELECT m.academy_no academyNo, CONCAT(IFNULL(p.salutation,''),' ',IFNULL(p.surname,''),' ',IFNULL(p.given_name,'')) customerName, c.expiry_date expiryDate")
//			.append("		FROM member m JOIN customer_profile p ON m.customer_id = p.customer_id JOIN ( SELECT")
//			.append("				mem.customer_id, MAX(acc.effective_date), DATE_FORMAT(expiry_date, '%Y/%m/%d') expiry_date FROM customer_service_acc acc,")
//			.append("				member mem WHERE acc.`status` = 'ACT' AND mem.superior_member_id = ? ")
//			.append("			AND mem.superior_member_id = acc.customer_id GROUP BY acc.customer_id")
//			.append("		) AS c ON m.customer_id = c.customer_id WHERE m.customer_id = ? ");
			
			Long superiorMemberId = member.getSuperiorMemberId();
			
			if(null == superiorMemberId){
				//return new ResponseResult("2", "The departmentmember has no primary member!");
				responseResult.initResult(GTAError.MemberShipError.DEPARTMENT_MEMBER_HAS_NO_PRIMARY_NUMBER);
				return responseResult;
			}	
			
			CustomerServiceAccDto latestAcc = customerServiceDao.getLatestServiceAccByCustomerId(superiorMemberId);
			
			CustomerProfile cp = customerProfileDao.getCustomerProfileByCustomerId(customerId.toString());
			
			
			if(null == latestAcc){
				//return new ResponseResult("1","", "Member's primary member has no latest Service Account!");
				responseResult.initResult(GTAError.MemberShipError.PRIMARY_MEMBER_HAS_NO_SERVICE_ACC);
				return responseResult;
			}	
			
			if(null == cp){
				//return new ResponseResult("1","", "Member with customerId : " +customerId + " profile is null!");
				responseResult.initResult(GTAError.MemberShipError.MEMBER_PROFILE_IS_NULL, new Object[]{customerId});
				return responseResult;
			}	
			
				m = new MemberCardPrintDto();
				m.setAcademyNo(member.getAcademyNo());
				m.setCustomerName(cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname());
				
				if(null != latestAcc.getExpiryDate()){
					m.setExpiryDate(latestAcc.getExpiryDate());
				}
			
			
		}else{
			//return new ResponseResult("2", "", "Member type M or PM can't find!");
			responseResult.initResult(GTAError.MemberShipError.MEMBER_TYPE_INVALID);
			return responseResult;
		}
		
		if(null == m){
			//return new ResponseResult("1", "", "Can't find the member");
			responseResult.initResult(GTAError.MemberShipError.CANNOT_FIND_MEMBER);
			return responseResult;
		}	
		
		//return new ResponseResult("0", "sucess", m);
		responseResult.initResult(GTAError.Success.SUCCESS, "sucess");
		responseResult.setData(m);
		return responseResult;
	}
	
	
//	/**
//	 * This method is used to get card information for members
//	 * @param page 	- ListPage to hold the pagination, sorting details etc.
//	 * @param dto 	- to hold the response
//	 * @return ResponseResult 
//	 * @author Vineela_Jyothi
//	 */
//	@Override
//	@Transactional
//	public ResponseResult getCards(ListPage<PermitCardMaster> page,
//			PermitCardMasterDto dto) {
//		
//		try {
//			if(CommUtil.notEmpty(String.valueOf(dto.getPageNumber()))){
//				page.setNumber(dto.getPageNumber());
//			}
//			if(CommUtil.notEmpty(String.valueOf(dto.getPageSize()))){
//				page.setSize(dto.getPageSize());
//			}
//			ListPage<PermitCardMaster> list = permitCardMasterDao.getCards(page, dto);
//			int count = list.getDtoList().size();
//			Data data = new Data();
//			if (count == 0) {
//				data.setList(new ArrayList<>());
//				responseResult.initResult(GTAError.Success.SUCCESS, data);
//				return responseResult;
//
//			    }
//								
//			data.setList(list.getDtoList());
//		    data.setTotalPage(page.getAllPage());
//		    data.setCurrentPage(page.getNumber());
//		    data.setPageSize(page.getSize());
//		    data.setRecordCount(page.getAllSize());
//		    data.setLastPage(page.isLast());
//		    
//		    responseResult.initResult(GTAError.Success.SUCCESS, data);
//		    return responseResult;
//			
//		} catch (Exception e) {
//			logger.error(MembershipCardsManagmentServiceImpl.class.getName() + " getCards Failed!", e);
//			responseResult.initResult(GTAError.AcademyCardError.FAIL_RETRIEVE_MEMBER_LIST);
//			return responseResult;
//		}
//		
//	}
	
	
	@Override
	@Transactional
	public boolean hasIssuedCard(Long customerId)
	{
		List<PermitCardMaster> issuedCard = permitCardMasterDao.getIssuedCardByCustomerId(customerId);
		
		return issuedCard == null ? false : issuedCard.size() == 1;
	}
	
	@Override
	@Transactional
	public boolean orderHasbeenCanceled(Long orderNo)
	{
		CustomerOrderHd order = customerOrderHdDao.getOrderById(orderNo);
		
		return "CAN".equalsIgnoreCase(order.getOrderStatus());
	}

	@Override
	public String getCards(String status) {
	    
	    return permitCardMasterDao.getCards(status);
	}
	
}






















