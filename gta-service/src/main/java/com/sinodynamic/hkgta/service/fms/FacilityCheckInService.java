package com.sinodynamic.hkgta.service.fms;

import com.sinodynamic.hkgta.dto.fms.AdvancedQueryConditionsDto;
import com.sinodynamic.hkgta.dto.fms.FacilityCheckInDto;
import com.sinodynamic.hkgta.dto.fms.FacilityCheckInRequestDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface FacilityCheckInService {

	public FacilityCheckInDto<?> getCheckAvailability(long resvId, boolean isMember);

	public void checkIn(FacilityCheckInRequestDto requestDto, String userId, boolean isMember);

	public ResponseResult returnCheckedInFacilitiesByResvId(long resvId);

	public List<FacilityMasterDto> getFacilityAllotment(long resvId);

	/**
     * 重载方法 【添加aqcdto可控制是否显示canelled】
     * @param resvId
     * @param dto
     * @return
     */
    public ResponseResult returnCheckedInFacilitiesByResvId(long resvId, AdvancedQueryConditionsDto dto);
    
    /***
     * get Golfing Bay #/Tennis Court #  by resvId
     * @param resvId
     * @return
     */
    public ResponseResult getFacilitiesVenueByResvId(long resvId);
}
