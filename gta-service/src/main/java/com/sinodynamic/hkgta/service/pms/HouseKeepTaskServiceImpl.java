package com.sinodynamic.hkgta.service.pms;

import java.util.Date;
import java.util.List;


import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinodynamic.hkgta.dao.pms.HousekeepTaskFileDao;
import com.sinodynamic.hkgta.dto.pms.HouseKeepTaskDto;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;


@Service
public class HouseKeepTaskServiceImpl implements HouseKeepTaskService {
	@Autowired
	private HousekeepTaskFileDao housekeepTaskFileDao;

	@Override
	public StringBuilder getListSQLByJobType(String jobType) {

		StringBuilder sql = new StringBuilder();

		if (!StringUtils.isEmpty(jobType)) {
			if (RoomHousekeepTaskJobType.ADH.name().equals(jobType)) {
				sql.append(createAdHocSql());
			} else if ((RoomHousekeepTaskJobType.RUT.name().equals(jobType))) {
				sql.append(createRoutineSql());
			} else if ((RoomHousekeepTaskJobType.MTN.name().equals(jobType))) {
				sql.append(createMaintenanceSql());
			} else if ((RoomHousekeepTaskJobType.SCH.name().equals(jobType))) {
				sql.append(createScheduledSql());
			}
		} else {
			/* room assignment */
			sql.append(createRoomAssignmentSql());
		}
		return sql;
	}

	/***
	 * create ad hoc task sql
	 * 
	 * @return
	 */
	private StringBuilder createAdHocSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(createHeaderSql());
		sql.append(" DATE_FORMAT(rh.start_timestamp,'%Y-%M-%D %H:%i:%S') as respondTime,");
		sql.append(addHouseKeepStatus());
		sql.append("( ");
		sql.append("  SELECT CONCAT(sp.given_name,' ',sp.surname) ");
		sql.append("  FROM room_housekeep_status_log  AS rlog ");
		sql.append("  LEFT JOIN staff_profile AS sp ON rlog.status_chg_by=sp.user_id");
		sql.append("  WHERE rlog.task_id=rh.task_id   AND rlog.status_from='OPN' AND rlog.status_to ='ATN'");
		sql.append("  )  AS handlingStaff ");
		sql.append(" FROM room_housekeep_task as rh ");
		sql.append(" LEFT JOIN (select * from room_housekeep_status_log t where not exists (select 1 from room_housekeep_status_log t1 where t.task_id=t1.task_id and t1.status_date > t.status_date )) log on rh.task_id=log.task_id and log.status_from='OPN' and log.status_to='ATN' ");
		sql.append(" LEFT JOIN room AS r ON  rh.room_id=r.room_id  WHERE 1=1   ");
		sql.append(" and rh.job_type='ADH' and rh.status<>'PND' ");
		return sql;

	}

	/***
	 * create routine task sql
	 * 
	 * @return
	 */
	private StringBuilder createRoutineSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" rh.task_id AS taskId,");
		sql.append(" DATE_FORMAT(rh.schedule_datetime,'%Y-%M-%D %H:%i:%S')  AS assignedDate,r.room_no AS roomNumber,");
		sql.append(" rh.task_description AS taskDetails," + " rh.monitor_detected AS sbeaconDesc ,");

		sql.append(addHouseKeepStatus());
		sql.append(" rh.from_task_id AS originalTaskID,");
		sql.append(" DATE_FORMAT(rh.start_timestamp,'%Y-%M-%D %H:%i:%S') as arrivalTime,");
		addSqlToSQL(sql);
		sql.append(" CONCAT(sp.given_name,' ',sp.surname) AS ispStaff ");
		sql.append(" FROM room_housekeep_task as rh ");
		sql.append(" LEFT JOIN (select * from room_housekeep_status_log t where not exists (select 1 from room_housekeep_status_log t1 where t.task_id=t1.task_id and t1.status_date > t.status_date )) log on rh.task_id=log.task_id and log.status_from='OPN' and log.status_to='ATN' ");
		sql.append(" LEFT JOIN staff_profile AS sp ON rh.inspector_user_id=sp.user_id ");
		sql.append(" LEFT JOIN room AS r ON  rh.room_id=r.room_id  WHERE 1=1  ");
		sql.append(" and rh.job_type='RUT'");
		return sql;
	}

	/***
	 * create Maintenance task sql
	 * 
	 * @return
	 */
	private StringBuilder createMaintenanceSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(createHeaderSql());
		sql.append(" um.nickname AS assignee,");
		sql.append(addHouseKeepStatus());
		sql.append(" rh.from_task_id AS originalTaskID,");
		sql.append(" DATE_FORMAT(rh.start_timestamp,'%Y-%M-%D %H:%i:%S') as arrivalTime,");
		addSqlToSQL(sql);
		sql.append(" CONCAT(sp.given_name,' ',sp.surname) AS ispStaff ");
		sql.append(" FROM room_housekeep_task as rh ");
		sql.append(" LEFT JOIN (select * from room_housekeep_status_log t where not exists (select 1 from room_housekeep_status_log t1 where t.task_id=t1.task_id and t1.status_date > t.status_date )) log on rh.task_id=log.task_id and log.status_from='OPN' and log.status_to='ATN' ");
		sql.append(" LEFT JOIN user_master AS um ON rh.create_by=um.user_id ");
		sql.append(" LEFT JOIN staff_profile AS sp ON rh.inspector_user_id=sp.user_id");
		sql.append(" LEFT JOIN room AS r ON  rh.room_id=r.room_id  WHERE 1=1  ");
		sql.append(" and rh.job_type='MTN'");
		return sql;
	}

	/***
	 * create Scheduled task sql
	 * 
	 * @return
	 */
	private StringBuilder createScheduledSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(createHeaderSql());
		sql.append(" DATE_FORMAT(rh.schedule_datetime,'%Y-%M-%D %H:%i:%S')    AS beginDate,");
		sql.append(
				" DATE_FORMAT(rh.target_complete_datetime,'%Y-%M-%D %H:%i:%S') AS expireDate ,");
		sql.append(addHouseKeepStatus());
		sql.append(" rh.from_task_id AS originalTaskID,");
		sql.append(" DATE_FORMAT(rh.start_timestamp,'%Y-%M-%D %H:%i:%S') as arrivalTime,");
		addSqlToSQL(sql);
		sql.append(" CONCAT(sp.given_name,' ',sp.surname) AS ispStaff ");
		sql.append(" FROM room_housekeep_task as rh ");
		sql.append(" LEFT JOIN (select * from room_housekeep_status_log t where not exists (select 1 from room_housekeep_status_log t1 where t.task_id=t1.task_id and t1.status_date > t.status_date )) log on rh.task_id=log.task_id and log.status_from='OPN' and log.status_to='ATN' ");
		sql.append(" LEFT JOIN staff_profile AS sp ON rh.inspector_user_id=sp.user_id");
		sql.append(" LEFT JOIN room AS r ON  rh.room_id=r.room_id  WHERE 1=1  ");
		sql.append(" and rh.job_type='SCH'");
		return sql;
	}
	
	private StringBuilder addHouseKeepStatus() {
		StringBuilder sql = new StringBuilder();
		sql.append(" CASE rh.status");
		sql.append(" WHEN 'OPN' THEN if(log.log_id,'Arrived','New') ");
		sql.append(" WHEN 'RTI' THEN 'Completed' ");
		sql.append(" WHEN 'IRJ' THEN 'Inspection Rejected' ");
		sql.append(" WHEN 'CMP' THEN 'Inspection Completed' ");
		sql.append(" WHEN 'CAN' THEN 'Cancelled' ");
		sql.append(" WHEN 'DND' THEN 'Do Not Disturb' ");
		sql.append(" END as taskStatus,");
		return sql;
	}

	/***
	 * create Room Assignment sql
	 * 
	 * @return
	 */
	private StringBuilder createRoomAssignmentSql() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT  r.room_no AS roomNumber,");
		sql.append("DATE_FORMAT(rh.schedule_datetime,'%Y-%M-%D %H:%i:%S')  AS assignedDate,");
		sql.append("  CASE rh.job_type ");
		sql.append("  WHEN 'ADH' THEN 'Ad Hoc'");
		sql.append("  WHEN 'RUT' THEN 'Routine'");
		sql.append("  WHEN 'SCH' THEN 'Scheduled'");
		sql.append("  WHEN 'MTN' THEN 'Maintenance'");
		sql.append("  ELSE 'other'");
		sql.append("  END AS taskType,");
		sql.append("  rh.task_description AS taskDetails, ");
		sql.append("  rh.monitor_detected AS sbeaconDesc ,");
		sql.append("  DATE_FORMAT(rh.start_timestamp,'%Y-%M-%D %H:%i:%S') as arrivalTime,");
		addSqlToSQL(sql);
		sql.append(" CONCAT(sp.given_name,' ',sp.surname) AS ispStaff ");
		sql.append(" FROM room_housekeep_task AS rh  ");
		sql.append(" LEFT JOIN (select * from room_housekeep_status_log t where not exists (select 1 from room_housekeep_status_log t1 where t.task_id=t1.task_id and t1.status_date > t.status_date )) log on rh.task_id=log.task_id and log.status_from='OPN' and log.status_to='ATN' ");
		sql.append(" LEFT JOIN staff_profile AS sp ON rh.inspector_user_id=sp.user_id");
		sql.append(" LEFT JOIN room AS r ON  rh.room_id=r.room_id  WHERE 1=1  ");
		return sql;
	}

	/***
	 * create common Sql
	 * 
	 * @return
	 */
	private StringBuilder createHeaderSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" rh.task_id AS taskId,");
		sql.append(" DATE_FORMAT(rh.schedule_datetime,'%Y-%M-%D %H:%i:%S')  AS assignedDate,r.room_no AS roomNumber,");
		sql.append(" rh.task_description AS taskDetails," + " rh.monitor_detected AS sbeaconDesc ,");
		return sql;
	}

	private void addSqlToSQL(StringBuilder sb) {
		sb.append(" ( ");
		sb.append(" SELECT CONCAT(sp.given_name,' ',sp.surname) ");
		sb.append("  FROM room_housekeep_status_log  AS rlog ");
		sb.append("  LEFT JOIN staff_profile AS sp ON rlog.status_chg_by=sp.user_id ");
		sb.append("  WHERE rlog.task_id=rh.task_id   AND rlog.status_from='OPN' AND rlog.status_to ='ATN' ");
		sb.append("  )  AS arrivalStaff,   ");

		sb.append("  ( ");
		sb.append("  SELECT CONCAT(sp.given_name,' ',sp.surname)");
		sb.append("  FROM room_housekeep_status_log  AS rlog ");
		sb.append("  LEFT JOIN staff_profile AS sp ON rlog.status_chg_by=sp.user_id ");
		sb.append("  WHERE rlog.task_id=rh.task_id   AND rlog.status_from='ATN' AND (rlog.status_to ='RTI' or rlog.status_to ='CMP') ");
		sb.append("  ) AS completeStaff,");

		sb.append("  (");
		sb.append("   SELECT  DATE_FORMAT(rlog.status_date,'%Y-%M-%D %H:%i:%S')");
		sb.append(" FROM room_housekeep_status_log  AS rlog");
		sb.append("   LEFT JOIN staff_profile AS sp ON rlog.status_chg_by=sp.user_id ");
		sb.append("  WHERE rlog.task_id=rh.task_id   AND rlog.status_from='ATN' AND (rlog.status_to ='RTI' or rlog.status_to ='CMP')  ");
		sb.append("  ) ");

		sb.append("  AS completeTime , ");
		sb.append("   (");
		sb.append("   SELECT  DATE_FORMAT(rlog.status_date,'%Y-%M-%D %H:%i:%S')");
		sb.append("   FROM room_housekeep_status_log  AS rlog ");
		sb.append("  LEFT JOIN staff_profile AS sp ON rlog.status_chg_by=sp.user_id ");
		sb.append(
				"  WHERE rlog.task_id=rh.task_id   AND rlog.status_from='RTI' AND (rlog.status_to ='CMP' OR rlog.status_to='IRJ') ");
		sb.append("  ) AS ispCompleteTime,");

	}

	@Override
	public StringBuilder getRoomAttedantSql(String userId, String startTime, String endTime) {
		StringBuilder sql = new StringBuilder();
		if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
			sql.append(" SELECT c.on_date as date ,");
			sql.append(" IF(d.duties,d.duties,'Off')  as duties");
			
			sql.append(" FROM( ");
			List<Date> dates = DateCalcUtil.getDates(DateConvertUtil.parseString2Date(startTime, "yyyy-MM-dd"),
					DateConvertUtil.parseString2Date(endTime, "yyyy-MM-dd"));
			int i = 0;
			for (Date date : dates) {
				i++;
				String time = DateConvertUtil.parseDate2String(date, "yyyy-MM-dd");
				sql.append(" SELECT");
				sql.append(" '" + time + "' AS on_date");
				if (dates.size() != i) {
					sql.append(" UNION");
				}
			}
			sql.append(" ) as c ");
			sql.append(" LEFT JOIN ( ");

			sql.append("SELECT b.on_date,");
			sql.append("(SELECT");
			sql.append(
					"    CONCAT(GROUP_CONCAT(DISTINCT  CONCAT(a.begin_time,'-',a.end_time + 100) ORDER BY CAST(end_time AS SIGNED) ASC SEPARATOR ','),'')");
			sql.append("  FROM staff_roster AS a ");
			sql.append("   WHERE a.on_date=b.on_date ");
			sql.append("   AND a.staff_user_id=b.staff_user_id ");
			
			sql.append(" )  AS duties");
			sql.append(" FROM staff_roster AS b ");
			sql.append(" WHERE 1=1 ");

			if (StringUtils.isNotEmpty(userId)) {
				sql.append(" AND b.staff_user_id='" + userId + "' ");
			}
//			if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
//				sql.append(" AND b.on_date BETWEEN '" + startTime + "' AND  '" + endTime + "'");
//			}
			sql.append("GROUP BY on_date");
			sql.append(" ) AS d ");
			sql.append(" ON  c.on_date=d.on_date ");

		}
		return sql;
	}

	@Override
	public byte[] getHouseKeepTaskAttach(String jobType, String startTime, String endTime, String fileType, String roomNumber,
			String sortBy, String isAscending) {
		try {
			return housekeepTaskFileDao.getHouseKeepTaskAttach(jobType, startTime, endTime,
					fileType, roomNumber, sortBy, isAscending);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public byte[] getRoomWeeklyAttendantAttach(String userId, String userName, String dateSql, String startTime, String endTime,
			String fileType, String sortBy, String isAscending) {
		try {
			return housekeepTaskFileDao.getRoomWeeklyAttendantAttach(userId, userName, dateSql, startTime, endTime,
					fileType, sortBy, isAscending);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<HouseKeepTaskDto> getListByJobType(String jobType, String Day) {
		// TODO Auto-generated method stub
		return null;
	}

}
