package com.sinodynamic.hkgta.service.bi;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.entity.bi.BiOasisFlexFile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface BiOasisFlexFileService extends IServiceBase<BiOasisFlexFile> {

	/***
	 * 
	 * @param time YYYYmmdd
	 * @return
	 */
	public boolean excuteImportFile(String filePath, Date time) throws Exception;
	
	public Serializable save(BiOasisFlexFile biOasisFlexFile);
	
	/***
	 * import BiOasisFlexFile file
	 * @param file
	 * @return
	 */
	public List<BiOasisFlexFile> readBiOasisFlexFile(File file);
	
	/***
	 * save biOasisFlexFile  if found the oasis transaction code is not exist in master data, default use gta code "Other" instead 
	 * @param biOasisFlexFile
	 * @return
	 */
	public boolean saveBiOasisFlexFile(BiOasisFlexFile biOasisFlexFile);

	/**
	 * 获取BiOasisFlexFile每天列表
	 * @return
	 */
	public List<BiOasisFlexFile> getBioasisFlexFile();

	/**
	 * 导入txt文件进行解析插入处理
	 * @param sourceFile
	 * @return
	 */
	public  boolean importTxtFile(File sourceFile);
}
