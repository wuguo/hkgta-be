package com.sinodynamic.hkgta.service.fms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilitySubTypeDao;
import com.sinodynamic.hkgta.dto.fms.FacilitySubtypeDto;
import com.sinodynamic.hkgta.dto.fms.FacilitySubtypePosDto;
import com.sinodynamic.hkgta.entity.fms.FacilitySubType;
import com.sinodynamic.hkgta.entity.fms.FacilitySubTypePos;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.AgeRangeCode;
import com.sinodynamic.hkgta.util.constant.Constant.RateType;

@Service
public class FacilitySubTypeServiceImpl extends ServiceBase<FacilitySubType> implements FacilitySubTypeService {

	@Autowired
	private FacilitySubTypeDao facilitySubTypeDao;
	
	@Override
	@Transactional
	public List<FacilitySubtypeDto> getFacilitySubType(String facilityType,String subType)
	{
		List<FacilitySubtypeDto> subtypeDtos = new ArrayList<FacilitySubtypeDto>();
		List<FacilitySubType>  subTypes = facilitySubTypeDao.getFacilitySubType(facilityType.toUpperCase(),subType);
		FacilitySubtypePosDto adultHigh = null;
		FacilitySubtypePosDto adultLow = null;
		FacilitySubtypePosDto childHigh = null;
		FacilitySubtypePosDto childLow = null;
		for(FacilitySubType type : subTypes){
			FacilitySubtypeDto subtypeDto = new FacilitySubtypeDto();
			subtypeDto.setSubtypeId(type.getSubTypeId());
			subtypeDto.setCourtName(type.getName());
			List<FacilitySubTypePos> posList = type.getFacilitySubTypePosList();
			if(null != posList && posList.size() > 0){
				for(FacilitySubTypePos pos : posList){
					if(AgeRangeCode.ADULT.name().equals(pos.getAgeRangeCode()) && RateType.HI.name().equals(pos.getRateType())){
						adultHigh = new FacilitySubtypePosDto();
						adultHigh.setPrice(pos.getPosItemPrice().getItemPrice());
						adultHigh.setSubtypePosId(pos.getSubtypePosId());
						subtypeDto.setAdultHigh(adultHigh);
					}else if(AgeRangeCode.ADULT.name().equals(pos.getAgeRangeCode()) && RateType.LO.name().equals(pos.getRateType())){
						adultLow = new FacilitySubtypePosDto();
						adultLow.setPrice(pos.getPosItemPrice().getItemPrice());
						adultLow.setSubtypePosId(pos.getSubtypePosId());
						subtypeDto.setAdultLow(adultLow);
					}else if(AgeRangeCode.CHILD.name().equals(pos.getAgeRangeCode()) && RateType.HI.name().equals(pos.getRateType())){
						childHigh = new FacilitySubtypePosDto();
						childHigh.setPrice(pos.getPosItemPrice().getItemPrice());
						childHigh.setSubtypePosId(pos.getSubtypePosId());
						subtypeDto.setChildHigh(childHigh);
					}else if(AgeRangeCode.CHILD.name().equals(pos.getAgeRangeCode()) && RateType.LO.name().equals(pos.getRateType())){
						childLow = new FacilitySubtypePosDto();
						childLow.setPrice(pos.getPosItemPrice().getItemPrice());
						childLow.setSubtypePosId(pos.getSubtypePosId());
						subtypeDto.setChildLow(childLow);
					}
				}
			}
			subtypeDtos.add(subtypeDto);
		}
		
		return subtypeDtos;
	}

	@Override
	@Transactional
	public List<FacilitySubType> getFacilitySubTypeByFacilityType(String facilityType)
	{
		return facilitySubTypeDao.getFacilitySubType(facilityType.toUpperCase());
	}

	@Override
	@Transactional
	public FacilitySubType getFacilitySubTypeBySubTypeId(String subTypeId)
	{
		return facilitySubTypeDao.get(FacilitySubType.class, subTypeId);
	}
	
}
