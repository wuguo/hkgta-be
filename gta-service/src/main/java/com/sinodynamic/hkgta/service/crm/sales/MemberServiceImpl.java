package com.sinodynamic.hkgta.service.crm.sales;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dao.crm.UserActivateQuestDao;
import com.sinodynamic.hkgta.dao.pms.UserConfirmReservedFacilityDao;
import com.sinodynamic.hkgta.dto.crm.AdultDependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.GoingToAdultDependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.staff.QuestionAndAnswer;
import com.sinodynamic.hkgta.dto.staff.SecurityQuestionDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuest;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuestPK;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.pms.UserConfirmReservedFacility;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.ReservationType;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.PopMessage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service

public class MemberServiceImpl extends ServiceBase<Member> implements MemberService {
	@Autowired
	private MemberDao memberDao;

	@Autowired
	private SysCodeDao sysCodeDao;

	@Autowired
	private UserActivateQuestDao userActivateQuestDao;

	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;

	@Autowired
	private MessageTemplateDao messageTemplateDao;

	@Autowired
	private PermitCardMasterService permitCardMasterService;

	@Autowired
	private UserConfirmReservedFacilityDao userConfirmReservedFacilityDao;
	@Autowired
	private UserMasterService userMasterService;

	@Autowired
	private CustomerProfileService customerProfileService;

	private Logger staffEmailLogger = Logger.getLogger(LoggerType.STAFFEMAIL.getName());

	@Transactional
	public void addMember(Member m) throws Exception {
		memberDao.addMember(m);
	}

	@Transactional
	public void updateMember(Member m) throws Exception {
		memberDao.updateMemberer(m);

	}

	public void deleteMember(Member m) throws Exception {
		memberDao.addMember(m);

	}

	@Transactional
	public void getMemberList(ListPage<Member> page, Member m) throws Exception {
		String countHql = " select count(*) from Member ";
		String hql = " from Member m ";
		memberDao.getMemberList(page, countHql, hql, null);

	}

	@Transactional
	public Member getMemberById(Long id) throws Exception {
		return memberDao.getMemberById(id);
	}

	@Override
	@Transactional
	public ResponseResult getDependentMembers(Long customerID, ListPage page, String platform) {
		ListPage<Member> listPage = memberDao.getDependentMembers(page, customerID, platform);
		List<Object> dependentMemberDtos = listPage.getDtoList();

		Data data = new Data();
		data.setLastPage(listPage.isLast());
		data.setRecordCount(listPage.getAllSize());
		data.setPageSize(listPage.getSize());
		data.setTotalPage(listPage.getAllPage());
		data.setCurrentPage(listPage.getNumber());
		data.setList(dependentMemberDtos);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult checkDMCreditLimit(Long customerId) {
		try {
			Member member = memberDao.getMemberById(customerId);
			if (member == null) {
				return new ResponseResult("-1", "Member is not exist!");
			}
			if (MemberType.IDM.name().equals(member.getMemberType())
					|| MemberType.CDM.name().equals(member.getMemberType())) {
				MemberDto memberDto = memberDao.checkDMCreditLimit(member.getSuperiorMemberId());
				return new ResponseResult("0", "", memberDto);
			}
			return new ResponseResult("-1", "No record Found!");
		} catch (Exception e) {
			logger.error("checkDMCreditLimit method ", e);
			return new ResponseResult("-1", "System Exception!");
		}
	}

	@Override
	@Transactional
	public ResponseResult checkTransactionLimit(Long customerId, BigDecimal spending) {
		try {
			Member member = memberDao.getMemberById(customerId);
			if (member == null) {
				responseResult.initResult(GTAError.MemberShipError.MEMBER_NOT_EXISTS);
				return responseResult;
			}

			MemberDto memberDto = memberDao.getMemberPurchaseLimit(customerId);
			if (null != memberDto) {
				if (memberDto.getExpiryDate() == null || memberDto.getExpiryDate().after(new Date())) {
					if (memberDto.getRemainSum().compareTo(spending) != -1) {
						if (MemberType.IDM.name().equals(member.getMemberType())
								|| MemberType.CDM.name().equals(member.getMemberType())) {
							memberDto = memberDao.getMemberTransactionLimit(customerId);
							if (null != memberDto) {
								// if (null != memberDto.getCreditLimit() ||
								// "".equals(memberDto.getCreditLimit())) {
								if (null != memberDto.getCreditLimit()) { // SAMHUI
																			// fixed
																			// on
																			// 20160323:
																			// CreditLimit
																			// is
																			// BigDecimal
																			// object
																			// cannot
																			// compare
																			// to
																			// empty
																			// string
																			// ""
									if (memberDto.getCreditLimit().compareTo(spending) != -1) {
										responseResult.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_ALLOWED);
										return responseResult;
									} else {
										responseResult
												.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_NOT_ALLOWED);
										return responseResult;
									}
								}
							} else {
								responseResult.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_ALLOWED);
								return responseResult;
							}
						} else if (MemberType.IPM.name().equals(member.getMemberType())
								|| MemberType.CPM.name().equals(member.getMemberType())) {
							responseResult.initResult(GTAError.MemberShipError.BALANCE_AVAILABLE);
							return responseResult;
						}
					} else {
						responseResult.initResult(GTAError.MemberShipError.BALANCE_NOT_AVAILBALE);
						return responseResult;
					}
				} else if (memberDto.getExpiryDate().before(new Date())) {
					if (memberDto.getAvailableBalance().compareTo(spending) != -1) {
						responseResult.initResult(GTAError.MemberShipError.TRANSACTION_LIMIT_ALLOWED);
						return responseResult;

					} else {
						responseResult.initResult(GTAError.MemberShipError.BALANCE_NOT_AVAILBALE);
						return responseResult;
					}
				}
			}
			responseResult.initResult(GTAError.MemberShipError.NO_RECORD_FOUND);
			return responseResult;

		} catch (Exception e) {
			logger.error("checkDMTransactionLimit method ", e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION);
			return responseResult;
		}
	}

	@Override
	@Transactional
	public ResponseResult setSecurityQuestion(SecurityQuestionDto questionDto) throws Exception {
		List<SysCode> questionList = sysCodeDao.getSysCodeByCategory(Constant.CATEGORY_4_SECURITY_QUESTION);

		List<QuestionAndAnswer> answers = questionDto.getAnswers();
		for (QuestionAndAnswer answer : answers) {
			SysCode question = new SysCode(answer.getCode(), Constant.CATEGORY_4_SECURITY_QUESTION);
			String q = questionList.get(questionList.indexOf(question)).getCodeDisplay();
			String a = answer.getAnswer();

			UserActivateQuestPK id = new UserActivateQuestPK();
			id.setUserId(questionDto.getUserId());
			id.setQuestionNo(answer.getQuestionNo());

			UserActivateQuest userQuestion = userActivateQuestDao.getById(id);

			if (null == userQuestion) {
				userQuestion = new UserActivateQuest();
				userQuestion.initUserActivateQuest(questionDto.getUserId(), answer.getQuestionNo(), q, a, true);
				userActivateQuestDao.save(userQuestion);
			} else {
				userQuestion.initUserActivateQuest(questionDto.getUserId(), answer.getQuestionNo(), q, a, false);
			}
		}

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Override
	@Transactional
	public List<Member> getAllMembers() throws Exception {

		List<Member> members = memberDao.getAllMembers();

		return members;
	}

	@Override
	@Transactional
	public String getMemberTypeByCustomerId(Long customerID) {
		Member member = memberDao.get(Member.class, customerID);
		return member.getMemberType();
	}

	@Override
	@Transactional
	public Member getMemberByAcademyNo(String academyNo) {
		return memberDao.getMemberByAcademyNo(academyNo);
	}

	@Transactional
	public Boolean checkMemberServicePlanMulGeneration(Long customerId) {
		boolean hasMultiGenerationRight = false;
		// hasMultiGenerationRight =
		// memberLimitRuleDao.getMemberMultiGenerationRight(customerId);
		Member member = memberDao.getMemberByCustomerId(customerId);
		if ((MemberType.IDM.name().equals(member.getMemberType())
				|| MemberType.CDM.name().equals(member.getMemberType())) && null != member.getSuperiorMemberId()) {
			customerId = member.getSuperiorMemberId();
		}
		List<ServicePlanAdditionRule> servicePlanAdditionRules = servicePlanAdditionRuleDao
				.getServicePlanAdditionRuleByCustomerId(customerId);
		if (CollectionUtils.isNotEmpty(servicePlanAdditionRules)) {
			Iterator<ServicePlanAdditionRule> iterator = servicePlanAdditionRules.iterator();
			while (iterator.hasNext()) {
				ServicePlanAdditionRule servicePlanAdditionRule = iterator.next();
				if ("MGD".equals(servicePlanAdditionRule.getRightCode())
						&& ("true".equals(servicePlanAdditionRule.getInputValue()))) {
					hasMultiGenerationRight = true;
					return hasMultiGenerationRight;
				}
			}
		}
		return hasMultiGenerationRight;
	}

	/**
	 * 根据customerId查询member
	 * 
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@Override
	public List<Member> getMembersByCustomerId(String ids) throws Exception {
		return memberDao.getMembersByCustomerId(ids);
	}

	/**
	 * inactivate Dependent Member with relationship as 'Child' when he/she
	 * reached 18 years old birthday.
	 * 
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@Deprecated
	@Override
	public void AdultDependentSendEamil() {
		// List<AdultDependentMemberDto> memberlist =
		// memberDao.getAdultDependent();
		// if (memberlist.size() > 0) {
		// MessageTemplate template =
		// messageTemplateDao.getTemplateByFunctionId(Constant.ADULT_DPENDENT_18YEAR_TIPS);
		//
		// if (template == null) {
		// logger.error("can't find the template");
		// return;
		// }
		// CustomerEmailContent cec = new CustomerEmailContent();
		// if(memberlist != null && memberlist.size() > 0){
		// for(AdultDependentMemberDto dto : memberlist){
		// String content = template.getContentHtml().replace("{userName}",
		// dto.getMemberName()).replace("{academyID}", dto.getAcademyNo());//
		// mail content
		// String subject = template.getMessageSubject().replace("{userName}",
		// dto.getMemberName());// subject
		//// content = replaceEmailContent4Adult(content, dto.getSalesName(),
		// dto.getDependentName(), dto.getPatronName(), dto.getInactiveDate());
		// try {
		// cec = this.saveCustomerEmailContent(content,
		// dto.getSuperiorMemberEmail(), subject, "[SysAdmin001]", "");
		// boolean success = MailSender.sendEmail(dto.getSuperiorMemberEmail(),
		// null, null, subject, content, null);
		// if (success) {
		// cec.setStatus(EmailStatus.SENT.getName());
		// } else {
		// cec.setStatus(EmailStatus.FAIL.getName());
		// }
		//// customerEmailContentService.modifyCustomerEmailContent(cec);
		// staffEmailLogger.info(Log4jFormatUtil.logStaffEmailInfo(success,
		// DateCalcUtil.formatDatetime(cec.getSendDate()),
		// cec.getSenderUserId(), cec.getRecipientCustomerId(),
		// cec.getRecipientEmail(), cec.getNoticeType(), cec.getSubject()));
		// logger.info("AdultDependentSendEamil sucess. Subject:" + subject +
		// ",To: going to adult dependentName:" + dto.getMemberName());
		// } catch (Exception e) {
		// staffEmailLogger.info(Log4jFormatUtil.logStaffEmailInfo(false,
		// DateCalcUtil.formatDatetime(cec.getSendDate()),
		// cec.getSenderUserId(), cec.getRecipientCustomerId(),
		// cec.getRecipientEmail(), cec.getNoticeType(), cec.getSubject()));
		// logger.info("AdultDependentSendEamil fail. Subject:" + subject +
		// ",To: going to adult dependentName:" + dto.getMemberName() +
		// ",Exception:" + e.getMessage());
		// }
		// }
		// }
		// }
	}

	@Transactional
	@Override
	public void pushEmail4GoingToAdultMember() {
		// TODO Auto-generated method stub
		List<GoingToAdultDependentMemberDto> expired3MonthLater = this.memberDao.getGoingToAdultMember(3);

		MessageTemplate template = messageTemplateDao
				.getTemplateByFunctionId(Constant.INACTIVE_DPENDENT_18YEAR_OLD_3MONTH_NOTICE);

		if (template == null) {
			logger.error("can't find the template");
			return;
		}

		if (expired3MonthLater != null && expired3MonthLater.size() != 0) {
			for (GoingToAdultDependentMemberDto dto : expired3MonthLater) {
				String content = template.getContentHtml();// mail content
				String subject = template.getMessageSubject();// subject
				content = replaceEmailContent4Adult(content, dto.getSalesName(), dto.getDependentName(),
						dto.getPatronName(), dto.getInactiveDate());
				try {
					CustomerEmailContent cec = this.saveCustomerEmailContent(content, dto.getSalesPersonEmail(),
							subject, "", dto.getUserId());
					boolean success = MailSender.sendEmail(dto.getSalesPersonEmail(), null, null, subject, content,
							null);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					// customerEmailContentService.modifyCustomerEmailContent(cec);
					staffEmailLogger.info(
							Log4jFormatUtil.logStaffEmailInfo(success, DateCalcUtil.formatDatetime(cec.getSendDate()),
									cec.getSenderUserId(), cec.getRecipientCustomerId(), cec.getRecipientEmail(),
									cec.getNoticeType(), cec.getSubject()));
					logger.info("pushEmail4GoingToAdultMember sucess. Subject:" + subject + ",To:" + dto.getSalesName()
							+ ",going to adult dependentName:" + dto.getDependentName());
				} catch (Exception e) {
					logger.info("pushEmail4GoingToAdultMember fail. Subject:" + subject + ",To:" + dto.getSalesName()
							+ ",going to adult dependentName:" + dto.getDependentName() + ",Exception:"
							+ e.getMessage());
				}
			}
		}

		List<GoingToAdultDependentMemberDto> expired1MonthLater = this.memberDao.getGoingToAdultMember(1);

		template = messageTemplateDao.getTemplateByFunctionId(Constant.INACTIVE_DPENDENT_18YEAR_OLD_1MONTH_NOTICE);

		if (template == null) {
			logger.error("can't find the template");
			return;
		}

		if (expired1MonthLater != null && expired1MonthLater.size() != 0) {
			for (GoingToAdultDependentMemberDto dto : expired1MonthLater) {
				String content = template.getContentHtml();// mail content
				String subject = template.getMessageSubject();// subject
				content = replaceEmailContent4Adult(content, dto.getSalesName(), dto.getDependentName(),
						dto.getPatronName(), dto.getInactiveDate());
				try {
					CustomerEmailContent cec = this.saveCustomerEmailContent(content, dto.getSalesPersonEmail(),
							subject, "", dto.getUserId());
					boolean success = MailSender.sendEmail(dto.getSalesPersonEmail(), null, null, subject, content,
							null);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					// customerEmailContentService.modifyCustomerEmailContent(cec);
					staffEmailLogger.info(
							Log4jFormatUtil.logStaffEmailInfo(success, DateCalcUtil.formatDatetime(cec.getSendDate()),
									cec.getSenderUserId(), cec.getRecipientCustomerId(), cec.getRecipientEmail(),
									cec.getNoticeType(), cec.getSubject()));
					logger.info("pushEmail4GoingToAdultMember sucess. Subject:" + subject + ",To:" + dto.getSalesName()
							+ ",going to adult dependentName:" + dto.getDependentName());
				} catch (Exception e) {
					logger.info("pushEmail4GoingToAdultMember fail. Subject:" + subject + ",To:" + dto.getSalesName()
							+ ",going to adult dependentName:" + dto.getDependentName() + ",Exception:"
							+ e.getMessage());
				}
			}
		}

		List<GoingToAdultDependentMemberDto> expired0MonthLater = this.memberDao.getGoingToAdultMember(0);

		template = messageTemplateDao.getTemplateByFunctionId(Constant.INACTIVE_DPENDENT_18YEAR_OLD_0MONTH_NOTICE);

		if (template == null) {
			logger.error("can't find the template");
			return;
		}

		if (expired1MonthLater != null && expired0MonthLater.size() != 0) {
			for (GoingToAdultDependentMemberDto dto : expired0MonthLater) {
				String content = template.getContentHtml();// mail content
				String subject = template.getMessageSubject().replace("{patronID}", dto.getAcademyId());// subject
				content = replaceEmailContent4Adult(content, dto.getSalesName(), dto.getDependentName(),
						dto.getPatronName(), dto.getInactiveDate(), dto.getAcademyId());
				try {
					CustomerEmailContent cec = this.saveCustomerEmailContent(content, dto.getSalesPersonEmail(),
							subject, "", dto.getUserId());
					boolean success = MailSender.sendEmail(dto.getSalesPersonEmail(), null, null, subject, content,
							null);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					// customerEmailContentService.modifyCustomerEmailContent(cec);
					staffEmailLogger.info(
							Log4jFormatUtil.logStaffEmailInfo(success, DateCalcUtil.formatDatetime(cec.getSendDate()),
									cec.getSenderUserId(), cec.getRecipientCustomerId(), cec.getRecipientEmail(),
									cec.getNoticeType(), cec.getSubject()));
					logger.info("pushEmail4GoingToAdultMember sucess. Subject:" + subject + ",To:" + dto.getSalesName()
							+ ",going to adult dependentName:" + dto.getDependentName());
				} catch (Exception e) {
					logger.info("pushEmail4GoingToAdultMember fail. Subject:" + subject + ",To:" + dto.getSalesName()
							+ ",going to adult dependentName:" + dto.getDependentName() + ",Exception:"
							+ e.getMessage());
				}
			}
		}

	}

	@Transactional
	@Override
	public void pushEmail4ExpiringMember() {
		// TODO Auto-generated method stub
		List<GoingToAdultDependentMemberDto> expired3MonthLater = this.memberDao.getExpiringMember(3);

		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(Constant.MEMBERSHIP_EXPIRE_3_MONTHS);

		if (template == null) {
			logger.error("can't find the template");
			return;
		}

		if (expired3MonthLater != null && expired3MonthLater.size() != 0) {
			for (GoingToAdultDependentMemberDto dto : expired3MonthLater) {
				String content = template.getContentHtml();// mail content
				content = replaceEmailContent4Expring(content, dto.getSaleName(), dto.getAcademyId(),
						dto.getPatronName(), dto.getMembershipExpireDate());
				String subject = template.getMessageSubject().replace("{patronName}", dto.getPatronName());// subject
				try {
					CustomerEmailContent cec = this.saveCustomerEmailContent(content, dto.getSalesPersonEmail(),
							subject, "", dto.getUserId());
					boolean success = MailSender.sendEmail(dto.getSalesPersonEmail(), null, null, subject, content,
							null);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					// customerEmailContentService.modifyCustomerEmailContent(cec);
					staffEmailLogger.info(
							Log4jFormatUtil.logStaffEmailInfo(success, DateCalcUtil.formatDatetime(cec.getSendDate()),
									cec.getSenderUserId(), cec.getRecipientCustomerId(), cec.getRecipientEmail(),
									cec.getNoticeType(), cec.getSubject()));
					logger.info("pushEmail4ExpiringMember success. Subject:" + subject + ",To:" + dto.getSaleName()
							+ ",Expiring AcademyId:" + dto.getAcademyId());
				} catch (Exception e) {
					logger.info("pushEmail4ExpiringMember fail. Subject:" + subject + ",To:" + dto.getSaleName()
							+ ",Expiring AcademyId:" + dto.getAcademyId() + ",Exception:" + e.getMessage());
				}
			}
		}

		List<GoingToAdultDependentMemberDto> expired1MonthLater = this.memberDao.getExpiringMember(1);

		template = messageTemplateDao.getTemplateByFunctionId(Constant.MEMBERSHIP_EXPIRE_1_MONTH);

		if (template == null) {
			logger.error("can't find the template");
			return;
		}

		if (expired1MonthLater != null && expired1MonthLater.size() != 0) {
			for (GoingToAdultDependentMemberDto dto : expired1MonthLater) {
				String content = template.getContentHtml();// mail content
				content = replaceEmailContent4Expring(content, dto.getSaleName(), dto.getAcademyId(),
						dto.getPatronName(), dto.getMembershipExpireDate());
				String subject = template.getMessageSubject().replace("{patronName}", dto.getPatronName());// subject
				try {
					CustomerEmailContent cec = this.saveCustomerEmailContent(content, dto.getSalesPersonEmail(),
							subject, "", dto.getUserId());
					boolean success = MailSender.sendEmail(dto.getSalesPersonEmail(), null, null, subject, content,
							null);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					// customerEmailContentService.modifyCustomerEmailContent(cec);
					staffEmailLogger.info(
							Log4jFormatUtil.logStaffEmailInfo(success, DateCalcUtil.formatDatetime(cec.getSendDate()),
									cec.getSenderUserId(), cec.getRecipientCustomerId(), cec.getRecipientEmail(),
									cec.getNoticeType(), cec.getSubject()));
					logger.info("pushEmail4ExpiringMember success. Subject:" + subject + ",To:" + dto.getSaleName()
							+ ",Expiring AcademyId:" + dto.getAcademyId());
				} catch (Exception e) {
					logger.info("pushEmail4ExpiringMember fail. Subject:" + subject + ",To:" + dto.getSaleName()
							+ ",Expiring AcademyId:" + dto.getAcademyId() + ",Exception:" + e.getMessage());
				}
			}
		}

		List<GoingToAdultDependentMemberDto> expiredToday = this.memberDao.getExpiringMember(0);

		template = messageTemplateDao.getTemplateByFunctionId(Constant.MEMBERSHIP_EXPIRE_TODAY);

		if (template == null) {
			logger.error("can't find the template");
			return;
		}

		if (expiredToday != null && expiredToday.size() != 0) {
			for (GoingToAdultDependentMemberDto dto : expiredToday) {
				String content = template.getContentHtml();// mail content
				content = replaceEmailContent4Expring(content, dto.getSaleName(), dto.getAcademyId(),
						dto.getPatronName(), dto.getMembershipExpireDate());
				String subject = template.getMessageSubject().replace("{patronName}", dto.getPatronName());// subject
				try {
					CustomerEmailContent cec = this.saveCustomerEmailContent(content, dto.getSalesPersonEmail(),
							subject, "", dto.getUserId());
					boolean success = MailSender.sendEmail(dto.getSalesPersonEmail(), null, null, subject, content,
							null);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					// customerEmailContentService.modifyCustomerEmailContent(cec);
					staffEmailLogger.info(
							Log4jFormatUtil.logStaffEmailInfo(success, DateCalcUtil.formatDatetime(cec.getSendDate()),
									cec.getSenderUserId(), cec.getRecipientCustomerId(), cec.getRecipientEmail(),
									cec.getNoticeType(), cec.getSubject()));
				} catch (Exception e) {
					logger.info("pushEmail4ExpiringMember fail. Subject:" + subject + ",To:" + dto.getSaleName()
							+ ",Expiring AcademyId:" + dto.getAcademyId() + ",Exception:" + e.getMessage());
				}
			}
		}

	}

	public static String replaceEmailContent4Adult(String content, String salesName, String dependentName,
			String patronName, Date inactiveDate) {
		content = content.replace("{salesName}", salesName).replace("{dependentName}", dependentName)
				.replace("{patronName}", patronName)
				.replace("{inactiveDate}", new SimpleDateFormat("yyyy-MM-dd").format(inactiveDate))
				.replace("\\n", "\n");
		return content;
	}

	public static String replaceEmailContent4Adult(String content, String salesName, String dependentName,
			String patronName, Date inactiveDate, String patronID) {
		content = content.replace("{salesName}", salesName).replace("{dependentName}", dependentName)
				.replace("{patronName}", patronName).replace("{patronID}", patronID)
				.replace("{inactiveDate}", new SimpleDateFormat("yyyy-MM-dd").format(inactiveDate))
				.replace("\\n", "\n");
		return content;
	}

	public static String replaceEmailContent4Expring(String content, String saleName, String academyId,
			String patronName, Date membershipExpireDate) {
		content = content.replace("{saleName}", saleName).replace("{academyId}", academyId)
				.replace("{patronName}", patronName)
				.replace("{membershipExpireDate}", new SimpleDateFormat("yyyy-MM-dd").format(membershipExpireDate))
				.replace("\\n", "\n");
		return content;
	}

	@Transactional
	public CustomerEmailContent saveCustomerEmailContent(String content, String contactEmail, String messageSubject,
			String sendUserId, String customerId) {
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();

		customerEmailContent.setContent(content);
		customerEmailContent.setRecipientEmail(contactEmail);
		customerEmailContent.setSubject(messageSubject);
		customerEmailContent.setStatus(EmailStatus.PND.name());
		customerEmailContent.setSenderUserId(sendUserId);
		customerEmailContent.setRecipientCustomerId(customerId);
		customerEmailContent.setSendDate(new Date());
		// String sendID =
		// (String)customerEmailContentDao.save(customerEmailContent);
		// customerEmailContent.setSendId(sendID);

		return customerEmailContent;
	}

	@Override
	@Transactional
	public Member getMemberByCardNo(String cardNo) throws Exception {
		Member member = null;
		PermitCardMaster cardMaster = permitCardMasterService.getPermitCardMaster(cardNo);
		if (null != cardMaster) {
			member = memberDao.getMemberByCustomerId(cardMaster.getMappingCustomerId());
		}
		return member;
	}

	@Transactional
	public ResponseResult checkConfirmReservedFacility(String userId, String type) throws Exception {
		String sql = "SELECT * FROM user_confirm_reserved_facility AS u WHERE u.user_id= ? and u.service_type=? and DATE_FORMAT(u.confirm_timestamp,'%Y-%m-%d')=?";
		String resvType = null;
		List<Serializable> param = new ArrayList<>();
		param.add(userId);
		String step1 = null;
		if (ReservationType.WELLNESS.name().equals(type)) {
			resvType = ReservationType.WELLNESS.name();
			step1 = responseResult.getI18nMessge(GTAError.ConfirmFacilityMsg.POPWELLNESS.getCode());

		} else if (ReservationType.ROOM.name().equals(type)) {
			resvType = ReservationType.ROOM.name();
			step1 = responseResult.getI18nMessge(GTAError.ConfirmFacilityMsg.POPACCOMMODATION.getCode());
		} else {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR,
					"input param type is error,please user type is ROOM OR WELLNESS.");
			return responseResult;
		}
		param.add(resvType);
		param.add(DateConvertUtil.formatCurrentDate(new Date(), "yyyy-MM-dd"));

		Object obj = userConfirmReservedFacilityDao.getUniqueBySQL(sql, param);
		// exist
		PopMessage msg = new PopMessage();
		if (null != obj) {
			msg.setPop(false);
			responseResult.initResult(GTAError.Success.SUCCESS, msg);
		} else {
			// clear old history
			String hql = "delete FROM user_confirm_reserved_facility  WHERE user_id= ? and service_type=?";
			userConfirmReservedFacilityDao.deleteByHql(hql, userId, resvType);
			msg.setPop(true);
			msg.setStep1(step1);
			msg.setStep2(responseResult.getI18nMessge(GTAError.ConfirmFacilityMsg.POPFACILITY.getCode()));
			responseResult.initResult(GTAError.Success.SUCCESS, msg);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult saveConfirmReservedFacility(UserConfirmReservedFacility confirm) throws Exception {
		confirm.setConfirmTimestamp(new Date());
		userConfirmReservedFacilityDao.save(confirm);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Transactional
	public ResponseResult verificationBirthDate(String patronNo,String academyNo, String birth, String password, String phone,
			String lastName) throws Exception {
		//only exist ACT return success else return fail
		if(!StringUtils.isEmpty(patronNo)&&!StringUtils.isEmpty(academyNo))
		{
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed, only input one of the patronID and academyID.");
			return responseResult;
		}
		if(!StringUtils.isEmpty(patronNo)){
			if(!StringUtils.isEmpty(birth)||!StringUtils.isEmpty(password)||!StringUtils.isEmpty(phone)||!StringUtils.isEmpty(lastName))
			{
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed, only input patronID verify and other parameter Not Required. ");
				return responseResult;
			}
			return verificationPatronNo(patronNo);
		}
		if(StringUtils.isEmpty(academyNo))
		{
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed, the academyNo required .");
			return responseResult;
		}
		if (academyNo.length() < 7) {
			academyNo = String.format("%07d", Integer.valueOf(academyNo));
		}
		if(StringUtils.isEmpty(birth)&&StringUtils.isEmpty(password)&&StringUtils.isEmpty(phone)&&StringUtils.isEmpty(lastName))
		{
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed, Must Enter at least one parameter.");
			return responseResult;
		}
		Member member = this.getMemberByAcademyNo(academyNo);
		if (null == member) {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed, Incorrect Academy ID");
			return responseResult;
		} else {
			if (Status.NACT.name().equals(member.getStatus())) {
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,Patron is inactive.");
				return responseResult;
			}
			UserMaster user = userMasterService.getUserByUserId(member.getUserId());
			if (null == user) {
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,Login User cannot found");
				return responseResult;
			}
			if (Status.NACT.name().equals(user.getStatus())) {
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,Login User is inactive.");
				return responseResult;
			}

			ResponseResult result = null;
			if (!StringUtils.isEmpty(birth)) {
				result = verificationBirthDay(member.getCustomerId(), birth);
			}  if (!StringUtils.isEmpty(password)) {
				result = verificationPassword(user, password);
			}  if (!StringUtils.isEmpty(phone)) {
				result = verificationPhone(member.getCustomerId(), phone);
			}  if (!StringUtils.isEmpty(lastName)) {
				result = verificationLastName(member.getCustomerId(), lastName);

			}  if (!StringUtils.isEmpty(birth) && !StringUtils.isEmpty(password)) {
				result = verificationBirthDay(member.getCustomerId(), birth);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationPassword(user, password);
				}
			}  if (!StringUtils.isEmpty(birth) && !StringUtils.isEmpty(phone)) {
				result = verificationBirthDay(member.getCustomerId(), birth);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationPhone(member.getCustomerId(), phone);
				}
			}  if (!StringUtils.isEmpty(birth) && !StringUtils.isEmpty(lastName)) {
				result = verificationBirthDay(member.getCustomerId(), birth);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationLastName(member.getCustomerId(), lastName);
				}

			}  if (!StringUtils.isEmpty(password) && !StringUtils.isEmpty(phone)) {
				result = verificationPassword(user, password);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationPhone(member.getCustomerId(), phone);
				}

			}  if (!StringUtils.isEmpty(password) && !StringUtils.isEmpty(lastName)) {
				result = verificationPassword(user, password);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationLastName(member.getCustomerId(), lastName);
				}

			}  if (!StringUtils.isEmpty(phone) && !StringUtils.isEmpty(lastName)) {
				result = verificationPhone(member.getCustomerId(), phone);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationLastName(member.getCustomerId(), lastName);
				}

			}  if (!StringUtils.isEmpty(birth) && !StringUtils.isEmpty(password) && !StringUtils.isEmpty(phone)) {
				result = verificationBirthDay(member.getCustomerId(), birth);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationPassword(user, password);
					if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
						result = verificationPhone(member.getCustomerId(), phone);
					}
				}
			}  if (!StringUtils.isEmpty(birth) && !StringUtils.isEmpty(password)&& !StringUtils.isEmpty(lastName)) {
				result = verificationBirthDay(member.getCustomerId(), birth);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationPassword(user, password);
					if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
						result = verificationLastName(member.getCustomerId(), lastName);
					}
				}
			}  if (!StringUtils.isEmpty(birth) && !StringUtils.isEmpty(phone) && !StringUtils.isEmpty(lastName)) {
				result = verificationBirthDay(member.getCustomerId(), birth);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationPhone(member.getCustomerId(), phone);
					if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
						result = verificationLastName(member.getCustomerId(), lastName);
					}
				}

			}  if (!StringUtils.isEmpty(password) && !StringUtils.isEmpty(phone)&& !StringUtils.isEmpty(lastName)) {
				result = verificationPassword(user, password);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationPhone(member.getCustomerId(), phone);
					if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
						result = verificationLastName(member.getCustomerId(), lastName);
					}
				}
			}  if (!StringUtils.isEmpty(birth) && !StringUtils.isEmpty(password) && !StringUtils.isEmpty(phone)&& !StringUtils.isEmpty(lastName)) {
				result = verificationBirthDay(member.getCustomerId(), birth);
				if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
					result = verificationPassword(user, password);
					if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
						result = verificationPhone(member.getCustomerId(), phone);
						if (result.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
							result = verificationLastName(member.getCustomerId(), lastName);
						}
					}
				}
			}
			if(null!=result){
				return result;
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
		}
	}
	private ResponseResult verificationPatronNo(String patronNo) throws Exception {
		if (patronNo.length() < 7) {
			patronNo = String.format("%07d", Integer.valueOf(patronNo));
		}
		Member member = this.getMemberByAcademyNo(patronNo);
		if (null == member) {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed, Incorrect Patron ID");
			return responseResult;
		} else {
			if (Status.NACT.name().equals(member.getStatus())) {
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,Patron is inactive.");
				return responseResult;
			} else {
				UserMaster user = userMasterService.getUserByUserId(member.getUserId());
				if (null == user) {
					responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR,
							"Failed,Login User cannot found");
					return responseResult;
				}
				if (Status.NACT.name().equals(user.getStatus())) {
					responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR,
							"Failed,Login User is inactive.");
					return responseResult;
				} else {
					responseResult.initResult(GTAError.Success.SUCCESS);
					return responseResult;
				}
			}
		}
	}
	private ResponseResult verificationLastName(Long customerId, String lastName) throws Exception {
		CustomerProfile customerProfile = customerProfileService.getByCustomerID(customerId);
		if (null == customerProfile) {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,Cannot found Patron Profile.");
		}
		if (lastName.equals(customerProfile.getSurname())) {
			responseResult.initResult(GTAError.Success.SUCCESS);
		} else {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,LastName incorrect!");
		}
		return responseResult;
	}

	private ResponseResult verificationPhone(Long customerId, String phone) throws Exception {
		CustomerProfile customerProfile = customerProfileService.getByCustomerID(customerId);
		if (null == customerProfile) {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,Cannot found Patron Profile.");
		}
		if (!CommUtil.validatePhoneNo(phone.toString())) {
			responseResult.initResult(GTAError.LeadError.MOBILE_PHONE_INVALID);
		} else {
			if (phone.toString().equals(customerProfile.getPhoneMobile())) {
				responseResult.initResult(GTAError.Success.SUCCESS);
			} else {
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,phone incorrect!");
			}
		}

		return responseResult;
	}
	private ResponseResult verificationPassword(UserMaster user, String password) throws Exception {
		String username = user.getLoginId();
		String psw = CommUtil.getMD5Password(username, password);
		if (psw.equals(user.getPassword())) {
			responseResult.initResult(GTAError.Success.SUCCESS);
		} else {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,password incorrect!");
		}
		return responseResult;
	}

	private ResponseResult verificationBirthDay(Long customerId, String birthDay) throws Exception {
		ResponseResult resVer = verificationBirth(birthDay);
		if (null != resVer) {
			return resVer;
		}
		CustomerProfile customerProfile = customerProfileService.getByCustomerID(customerId);
		if (null == customerProfile) {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, "Failed,Cannot found Patron Profile.");
			return responseResult;
		}
		Date orBirth = DateConvertUtil.parseString2Date(customerProfile.getDateOfBirth(), "yyyy-MM-dd");
		Date inputBirth = DateConvertUtil.parseString2Date(birthDay, "yyyyMMdd");
		if (orBirth.compareTo(inputBirth) == 0) {
			responseResult.initResult(GTAError.Success.SUCCESS);
		} else {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR,
					"Failed,cannot found the date of birth ,please retry!");
		}
		return responseResult;
	}

	private ResponseResult verificationBirth(String birth) {
		// DDMMYYYY
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		try {
			if (birth.length() == 8) {
				simpleDateFormat.parse(birth);
			} else {
				responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR,
						"Failed, Incorrect Date of Birth format");
				return responseResult;
			}
		} catch (ParseException e) {
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR,
					"Failed, Incorrect Date of Birth format");
			return responseResult;
		}
		return null;
	}
}
