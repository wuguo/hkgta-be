package com.sinodynamic.hkgta.service.crm.sales.renewal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.adm.PermitCardMasterDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceAccDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.dao.ical.GtaPublicHolidayDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerTransactionDao;
import com.sinodynamic.hkgta.dto.Constant.memberType;
import com.sinodynamic.hkgta.dto.crm.ServicePlanAdditionRuleDto;
import com.sinodynamic.hkgta.dto.fms.CalendarViewDto;
import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.dto.ical.PublicHolidayDto;
import com.sinodynamic.hkgta.dto.rpos.UpdateServiceAccountStatusDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.bi.BiOasisTranCode;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRightPK;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.ical.PublicHoliday;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.ical.GtaPublicHolidayService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.constant.ServicePlanRightTypeStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CustomerServiceAccServiceImpl extends ServiceBase<CustomerServiceAcc>implements CustomerServiceAccService {
	
	@Autowired
	private CustomerServiceDao customerServiceDao;

	@Autowired
	CustomerTransactionDao customerTransactionDao;

	@Autowired
	private MemberDao memberDao;

	@Autowired
	private PermitCardMasterDao permitCardMasterDao;

	@Autowired
	private CustomerServiceAccDao customerServiceAccDao;

	@Autowired
	private UserMasterDao userMasterDao;

	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;

	@Autowired
	private CustomerServiceSubscribeDao customerServiceSubscribeDao;

	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;

	@Autowired
	private ServicePlanFacilityDao servicePlanFacilityDao;

	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;

	@Autowired
	private ServicePlanDao servicePlanDao;

	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	 @Autowired
	private GtaPublicHolidayDao	gtaPublicHolidayDao;

	@Override
	@Transactional
	public ResponseResult updateCustomerServiceAccStatus(Long orderNo) {
		ResponseResult result = new ResponseResult();
		UpdateServiceAccountStatusDto updateServiceAccountStatusDto = new UpdateServiceAccountStatusDto();
		BigInteger customerId = customerTransactionDao.getCustomerID(orderNo);
		System.out.println(customerId);
		BigDecimal b = customerTransactionDao.countBalanceDueStatus(orderNo);
		if (b.compareTo(BigDecimal.ZERO) <= 0) {
			System.out.println(customerId);
			int accNo = customerServiceDao.updateCustomerServiceAccStatus(customerId);
			System.out.println(accNo);
			updateServiceAccountStatusDto.setAccNo(accNo);
			updateServiceAccountStatusDto.setStatus("ACT");
			result.setData(updateServiceAccountStatusDto);
			result.setReturnCode(ResponseMsgConstant.SUCESSCODE);
			result.setErrorMessageEN("");
			return result;
		}
		int accNo = customerServiceDao.getAccNo(customerId);
		updateServiceAccountStatusDto.setAccNo(accNo);
		updateServiceAccountStatusDto.setStatus("NACT");
		result.setData(updateServiceAccountStatusDto);
		result.setReturnCode("0");
		result.setErrorMessageEN("");
		return result;

	}

	@Override
	@Transactional
	public List<CustomerServiceAcc> getExpiringCustomers() throws Exception {

		String expiringPeriod = appProps.getProperty("member.expire.period", "30");
		return customerServiceDao.selectExpiringMember(Integer.parseInt(expiringPeriod));
	}

	@Override
	@Transactional
	public void handleExpiredCustomerServiceAcc() throws Exception {
		List<CustomerServiceAcc> customerServiceAccs = customerServiceDao.getExpiredCustomerServiceAccs();
		logger.info("Expired Customer ServiceAccs size :"+(null==customerServiceAccs?0:customerServiceAccs.size())+"  update member status to "+Constant.Member_Status_NACT);
		
		for (CustomerServiceAcc customerServiceAcc : customerServiceAccs) {
			Member member = memberDao.get(Member.class, customerServiceAcc.getCustomerId());
			List<Member> dependentMemebers = memberDao.getByCol(Member.class, "superiorMemberId",
					member.getCustomerId(), null);
			if (dependentMemebers != null && dependentMemebers.size() > 0) {
				for (Member dependentMember : dependentMemebers) {
					dependentMember.setStatus(Constant.Member_Status_NACT);
					memberDao.update(dependentMember);
					changePermitCard2Expired(dependentMember.getCustomerId());
					if (CommUtil.notEmpty(dependentMember.getUserId())) {
						UserMaster denUserMaster = userMasterDao.get(UserMaster.class, dependentMember.getUserId());
						denUserMaster.setStatus(Constant.Status.NACT.name());
						denUserMaster.setUpdateBy("System");
						denUserMaster.setUpdateDate(new Date());
						userMasterDao.update(denUserMaster);
					}
				}
			}
			logger.info("update member status :"+member.getStatus()+" to "+Constant.Member_Status_NACT);
			member.setStatus(Constant.Member_Status_NACT);
			memberDao.update(member);
			
			UserMaster userMaster = userMasterDao.get(UserMaster.class, member.getUserId());
			logger.info("update UserMaster status :"+userMaster.getStatus()+" to "+Constant.Status.NACT.name());
			
			userMaster.setStatus(Constant.Status.NACT.name());
			userMaster.setUpdateBy("System");
			userMaster.setUpdateDate(new Date());
			userMasterDao.update(userMaster);
			
			changePermitCard2Expired(member.getCustomerId());
			
			logger.info("update customerServiceAcc status :"+customerServiceAcc.getStatus()+" to "+Constant.Status.EXP.name());
			customerServiceAcc.setStatus(Constant.Status.EXP.name());
			customerServiceDao.update(customerServiceAcc);
			
		}
	}

	private void changePermitCard2Expired(Long customerId) {

		String hql = "from PermitCardMaster where status = 'ISS' and mappingCustomerId = " + customerId;
		List<PermitCardMaster> cards = permitCardMasterDao.getByHql(hql);
		if (cards == null || cards.size() == 0)
			return;
		for (PermitCardMaster card : cards) {

			card.setStatus("EXP");
			card.setUpdateBy("ADMIN");
			card.setStatusUpdateDate(new Date());
			logger.info("update PermitCardMaster status is 'EXP' ,customerId="+customerId);
			permitCardMasterDao.updatePermitCardMaster(card);
		}
	}

	/**
	 * @author: Zero_Wang
	 * @since: Sep 9, 2015
	 * 
	 * @description write the description here
	 */
	@Override
	@Transactional
	public CustomerServiceAcc getAactiveByCustomerId(Long customerId) {
		CustomerServiceAcc acc = this.customerServiceAccDao.getAactiveByCustomerId(customerId);
		if (null == acc) {
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE,
					new String[] { "CustomerServiceAcc customerId not right:" + customerId });
		}
		return acc;

	}

	/**
	 * @author: Zero_Wang
	 * @since: Sep 9, 2015
	 * 
	 * @description write the description here
	 */
	@Override
	@Transactional
	public CustomerServiceAcc getActiveByCustomerId(Long customerId) {
		CustomerServiceAcc acc = this.customerServiceAccDao.getAactiveByCustomerId(customerId);
		return acc;

	}

	@Override
	@Transactional
	public boolean isCustomerServiceAccValid(Long customerId, ContractHelper obj) {

		if (customerId == null || obj == null)
			return false;

		Member member = memberDao.getMemberByCustomerId(customerId);
		if (member == null)
			return false;

		Long cID = null;
		String memberType = member.getMemberType();
		if (MemberType.IDM.name().equals(memberType) || MemberType.IPM.name().equals(memberType)) {

			if (MemberType.IDM.name().equals(memberType)) {
				cID = member.getSuperiorMemberId();
			} else {
				cID = customerId;
			}

		} else if (MemberType.CDM.name().equals(memberType) || MemberType.CPM.name().equals(memberType)) {

			if (MemberType.CDM.name().equals(memberType)) {
				cID = member.getSuperiorMemberId();
			} else {
				cID = customerId;
			}
		}

		String sql = "select distinct 1 from " + "( " + "SELECT "
				+ "min(effective_date) as joinDate, max(expiry_date) as expiryDate " + "FROM "
				+ "customer_service_acc c, " + "customer_order_hd hd " + "WHERE "
				+ "hd.order_status NOT IN ('REJ', 'CAN') " + "AND c.order_no = hd.order_no "
				+ "AND c. STATUS IN ('ACT', 'NACT') " + "AND c.effective_date IS NOT NULL "
				+ "AND c.expiry_date IS NOT NULL " + "AND c.customer_id = ? " + ") t " + "where t.joinDate <= ? "
				+ "and date_add(t.expiryDate, interval 1 day) >= ? ";

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(cID);
		param.add(obj.getPeriodFrom());
		param.add(obj.getPeriodTo());

		boolean result = customerServiceAccDao.isCustomerServiceValid(sql, param);
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void startRenewServiceAccount() throws Exception {
		List<CustomerServiceAcc> customerServiceAccs = customerServiceDao.getEffctiveRenewCustomerServiceAccs();
		logger.info("RenewServiceAccount size:"+(null==customerServiceAccs?0:customerServiceAccs.size()));
		for (CustomerServiceAcc customerServiceAcc : customerServiceAccs) {
			// try {
			startCurrentServiceAccount(customerServiceAcc);
			/*
			 * } catch (Exception e) { e.printStackTrace(); logger.debug(
			 * "Set facility right or limit rule failed!", e); }
			 */
		}

	}

	private void startCurrentServiceAccount(CustomerServiceAcc customerServiceAcc) throws Exception {
		// if (null == customerServiceAcc.getOrderNo() ||
		// "".equals(customerServiceAcc.getOrderNo())) {//Handle the dirty data
		// before.
		if (null == customerServiceAcc.getOrderNo()) { // SAMHUI Fixed on
														// 20160323: orderNo is
														// Long object cannot
														// compare to String
														// type
			return;
		}
		CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class,
				customerServiceAcc.getOrderNo());
		if (null == customerOrderHd) {// Handle the dirty data before.
			return;
		}
		CustomerServiceSubscribe customerServiceSubscribe = customerServiceSubscribeDao
				.getUniqueByCol(CustomerServiceSubscribe.class, "id.accNo", customerServiceAcc.getAccNo());
		ServicePlan renewalServicePlan = servicePlanDao.get(ServicePlan.class,
				customerServiceSubscribe.getId().getServicePlanNo());
		if (null != customerOrderHd && Constant.Status.CMP.name().equalsIgnoreCase(customerOrderHd.getOrderStatus())) {
			customerServiceAcc.setStatus(Constant.Status.ACT.name());
			customerServiceDao.update(customerServiceAcc);
			if(null!=customerServiceAcc){
				Member member = memberDao.get(Member.class, customerServiceAcc.getCustomerId());
				member.setStatus(Constant.Status.ACT.name());
				memberDao.update(member);
				if(null!=member&&!StringUtils.isEmpty(member.getUserId())){
					UserMaster userMaster = userMasterDao.get(UserMaster.class, member.getUserId());
					userMaster.setStatus(Constant.Status.ACT.name());
					userMaster.setUpdateBy("System");
					userMaster.setUpdateDate(new Date());
					userMasterDao.update(userMaster);
				}
				// save member_plan_facility_right
				starUpCustomerPlanFacilityRight(customerOrderHd.getCustomerId(), renewalServicePlan.getPlanNo(),
						customerServiceAcc, "System");

				// save member_limit_rule
				startUpMemberLimitRule(customerOrderHd.getCustomerId(), renewalServicePlan.getPlanNo(), customerServiceAcc,
						"System");

				// save member_plan_facility_right and member_limit_rule for
				// dependent members.
				startUpLimitRuleAndFacilityRightForDependentMember(customerOrderHd.getCustomerId(),
						customerServiceAcc.getEffectiveDate(), customerServiceAcc.getExpiryDate());
			}
		
		}
	}

	private void startUpLimitRuleAndFacilityRightForDependentMember(Long customerId, Date effectiveDate,
			Date expiryDate) {

		List<Member> listDependent = memberDao.getListMemberBySuperiorId(customerId);

		List<MemberLimitRule> primaryLimitRule = memberLimitRuleDao.getLastEffectiveListByCustomerId(customerId);

		for (Member tempDependent : listDependent) {
			Long dependentCustomerId = tempDependent.getCustomerId();
			setMemberLimitRule(dependentCustomerId, "TRN", BigDecimal.ZERO, null, "System", expiryDate, effectiveDate);

			// member limit rule
			if (null != primaryLimitRule && primaryLimitRule.size() > 0) {
				for (MemberLimitRule rule : primaryLimitRule) {
					if (null != primaryLimitRule && !"CR".equals(rule.getLimitType())
							&& !"G1".equals(rule.getLimitType())) {
						MemberLimitRule r = new MemberLimitRule();
						BeanUtils.copyProperties(rule, r,
								new String[] { "limitId", "customerId", "updateBy", "updateDate" });
						r.setEffectiveDate(effectiveDate);
						r.setExpiryDate(expiryDate);
						r.setCustomerId(dependentCustomerId);
						r.setUpdateBy("System");
						r.setUpdateDate(new Date());
						memberLimitRuleDao.save(r);
					}
				}
			}
			// save MemberPlanFacilityRight
			List<MemberPlanFacilityRight> mfr = memberPlanFacilityRightDao
					.getEffectiveFacilityRightByCustomerId(customerId);
			if (null != mfr && mfr.size() > 0) {
				for (MemberPlanFacilityRight right : mfr) {
					if (null != right) {
						MemberPlanFacilityRight r = new MemberPlanFacilityRight();
						BeanUtils.copyProperties(right, r, new String[] { "sysId", "customerId" });
						r.setCustomerId(dependentCustomerId);
						r.setEffectiveDate(effectiveDate);
						r.setExpiryDate(expiryDate);
						memberPlanFacilityRightDao.save(r);
					}
				}
			}

			/***
			 * renew service active dependent member 
			 */
			Member member = memberDao.get(Member.class, dependentCustomerId);
			member.setStatus(Constant.Status.ACT.name());
			memberDao.update(member);
			if(null!=member&&!StringUtils.isEmpty(member.getUserId())){
				UserMaster userMaster = userMasterDao.get(UserMaster.class, member.getUserId());
				userMaster.setStatus(Constant.Status.ACT.name());
				userMaster.setUpdateBy("System");
				userMaster.setUpdateDate(new Date());
				userMasterDao.update(userMaster);
			}
			
		}
	}

	/**
	 * save the customer's CustomerPlanFacilityRight as follows:
	 * 
	 * 1. get the right data from service_plan_facility by servicePlanNo 2. save
	 * the search data from service_plan_facility into
	 * member_plan_facility_right
	 * 
	 * @param servicePlanNo
	 *            planNo
	 * @throws Exception
	 */
	private void starUpCustomerPlanFacilityRight(Long customerId, Long servicePlanNo,
			CustomerServiceAcc customerServiceAcc, String createBy) throws Exception {
		/*
		 * these variable is not sure,then change later
		 *
		 */
		// String createBy = "Li_Chen"; //get this by session user

		List<ServicePlanFacility> planFacilityList = servicePlanFacilityDao.getByCol(ServicePlanFacility.class,
				"servicePlan.planNo", servicePlanNo, null);
		if (null != planFacilityList && planFacilityList.size() > 0) {
			for (ServicePlanFacility facility : planFacilityList) {
				MemberPlanFacilityRight memberPlanFacilityRight = new MemberPlanFacilityRight();
				MemberPlanFacilityRightPK pk = new MemberPlanFacilityRightPK();
				pk.setCustomerId(customerId);
				pk.setServicePlan(servicePlanNo);
				pk.setFacilityTypeCode(facility.getFacilityTypeCode());
				memberPlanFacilityRight.setEffectiveDate(customerServiceAcc.getEffectiveDate());
				memberPlanFacilityRight.setExpiryDate(customerServiceAcc.getExpiryDate());
				// memberPlanFacilityRight.setId(pk);
				memberPlanFacilityRight.setCustomerId(customerId);
				memberPlanFacilityRight.setServicePlan(servicePlanNo);
				memberPlanFacilityRight.setFacilityTypeCode(facility.getFacilityTypeCode());

				memberPlanFacilityRight.setPermission(facility.getPermission());
				memberPlanFacilityRight.setCreateBy(createBy);
				memberPlanFacilityRight.setCreateDate(new Timestamp(new Date().getTime()));
				memberPlanFacilityRightDao.save(memberPlanFacilityRight);
			}
		}
	}

	/**
	 * 
	 * @param servicePlanNo
	 */
	private void startUpMemberLimitRule(Long customerId, Long servicePlanNo, CustomerServiceAcc customerServiceAcc,
			String createBy) throws Exception {
		List servicePlanAdditionRuleList = servicePlanAdditionRuleDao.getServicePlanAdditionRuleByPlanNo(servicePlanNo);
		if (null != servicePlanAdditionRuleList && servicePlanAdditionRuleList.size() > 0) {
			for (int i = 0; i < servicePlanAdditionRuleList.size(); i++) {
				Object[] objectArry = (Object[]) servicePlanAdditionRuleList.get(i);
				MemberLimitRule memberLimitRule = new MemberLimitRule();
				memberLimitRule.setCustomerId(customerId);
				memberLimitRule.setLimitType((String) objectArry[0]);
				String inputValueType = (String) objectArry[1];
				if (!StringUtils.isEmpty(inputValueType))
					inputValueType = inputValueType.trim();
				if (ServicePlanRightTypeStatus.TXT.getDesc().equals(inputValueType))
					memberLimitRule.setTextValue((String) objectArry[2]);
				else if (ServicePlanRightTypeStatus.INT.getDesc().equals(inputValueType)) {
					memberLimitRule.setLimitUnit("MONTH");
					memberLimitRule.setNumValue(new BigDecimal((String) objectArry[2]));
				}
				memberLimitRule.setDescription((String) objectArry[3]);
				memberLimitRule.setEffectiveDate(customerServiceAcc.getEffectiveDate());
				memberLimitRule.setExpiryDate(customerServiceAcc.getExpiryDate());
				memberLimitRule.setCreateDate(new Timestamp(new Date().getTime()));
				memberLimitRule.setCreateBy(createBy);
				memberLimitRule.setUpdateDate(new Date());
				memberLimitRule.setUpdateBy(createBy);
				memberLimitRuleDao.save(memberLimitRule);
			}
		}
		// member limit rule
		setMemberLimitRule(customerId, "CR", BigDecimal.ZERO, "Credit Limit for Individual Primary Member", createBy,
				customerServiceAcc.getExpiryDate(), customerServiceAcc.getEffectiveDate());
	}

	private void setMemberLimitRule(Long customerId, String limitType, BigDecimal limitValue, String description,
			String userId, Date expiryDate, Date effectiveDate) {
		Date currentDate = new Date();
		MemberLimitRule memberLimitRuleCR = new MemberLimitRule();
		memberLimitRuleCR.setCustomerId(customerId);
		if ("TRN".equals(limitType)) {
			memberLimitRuleCR.setLimitUnit("EACH");
		}
		memberLimitRuleCR.setLimitType(limitType);
		memberLimitRuleCR.setNumValue(limitValue);
		memberLimitRuleCR.setDescription(description);
		memberLimitRuleCR.setExpiryDate(expiryDate);
		memberLimitRuleCR.setEffectiveDate(effectiveDate);
		memberLimitRuleCR.setCreateDate(new Timestamp(currentDate.getTime()));
		memberLimitRuleCR.setCreateBy(userId);
		memberLimitRuleCR.setUpdateBy(userId);
		memberLimitRuleCR.setUpdateDate(currentDate);
		memberLimitRuleDao.saveMemberLimitRuleImpl(memberLimitRuleCR);
	}

	/**
	 * 根据服务时间判断该会员是否有权限预订服务
	 * 
	 * @param customerId
	 * @param date
	 *            服务时间
	 * @return
	 */
	@Override
	@Transactional
	public boolean isCustomerServiceAccValidByDate(Long customerId, Date date) {

		if (customerId == null || date == null)
			return false;

		Member member = memberDao.getMemberByCustomerId(customerId);
		if (member == null)
			return false;

		Long cID = null;
		String memberType = member.getMemberType();
		if (MemberType.IDM.name().equals(memberType) || MemberType.IPM.name().equals(memberType)) {

			if (MemberType.IDM.name().equals(memberType)) {
				cID = member.getSuperiorMemberId();
			} else {
				cID = customerId;
			}

		} else if (MemberType.CDM.name().equals(memberType) || MemberType.CPM.name().equals(memberType)) {

			if (MemberType.CDM.name().equals(memberType)) {
				cID = member.getSuperiorMemberId();
			} else {
				cID = customerId;
			}
		}

		String sql = "select distinct 1 " + "from customer_service_acc " + "where status = 'ACT' "
		// + "and effective_date <= ?" TODO 该条件可能需要
				+ " and date_add(expiry_date, interval 1 day) >= ?" + " and customer_id = ?";

		List<Serializable> param = new ArrayList<Serializable>();
		// param.add(obj.getPeriodFrom());
		param.add(date);
		param.add(cID);

		boolean result = customerServiceAccDao.isCustomerServiceValid(sql, param);
		return result;
	}

	@Override
	@Transactional
	public boolean isCustomerServiceAccValidByDate(Long customerId, String startTime, String endTime) {
		if (customerId == null || null == startTime || null == endTime)
			return false;

		Member member = memberDao.getMemberByCustomerId(customerId);
		if (member == null)
			return false;

		Long cID = null;
		String memberType = member.getMemberType();
		if (MemberType.IDM.name().equals(memberType) || MemberType.IPM.name().equals(memberType)) {

			if (MemberType.IDM.name().equals(memberType)) {
				cID = member.getSuperiorMemberId();
			} else {
				cID = customerId;
			}

		} else if (MemberType.CDM.name().equals(memberType) || MemberType.CPM.name().equals(memberType)) {

			if (MemberType.CDM.name().equals(memberType)) {
				cID = member.getSuperiorMemberId();
			} else {
				cID = customerId;
			}
		}

		String sql = "select distinct 1 " + "from customer_service_acc " + "where status = 'ACT' "
				+ " and customer_id = ? " + " and ( date_add(expiry_date, interval 1 day) >= ? "
				+ " and date_add(expiry_date, interval 1 day) >= ?  )";

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(cID);
		param.add(DateConvertUtil.parseString2Date(startTime, "yyyy-MM-dd"));
		param.add(DateConvertUtil.parseString2Date(endTime, "yyyy-MM-dd"));

		boolean result = customerServiceAccDao.isCustomerServiceValid(sql, param);
		return result;
	}

	@Override
	@Transactional
	public boolean isExpiredAndNotRenewalMember(Long customerId) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String sql = " select 1 from customer_service_acc acc where acc.customer_id = " + customerId
				+ " and acc.status='EXP' "
				+ " and not EXISTS (select 1 from customer_service_acc acc1 where acc1.status='ACT' and acc1.customer_id=acc.customer_id)  "
				+ " and EXISTS (select 1 from customer_service_acc acc2 where acc2.status='EXP' and acc2.customer_id=acc.customer_id)  and not EXISTS (select 1 from customer_service_acc acc1 where acc1.status='ACT' and acc1.customer_id=acc.customer_id)  "
				+ " and not EXISTS (select 1 from customer_service_acc acc3 where acc3.status='NACT' and acc3.remark='RENEW' and acc3.customer_id=acc.customer_id) ";
		Object object = this.customerServiceAccDao.getUniqueBySQL(sql, null);
		if (object != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	@Transactional
	public boolean checkRenewedService(Long customerId, String date, String startTime, String endTime) {
		// get all service plan is renewed by customerId
		if (customerId == null )
			return false;
		Member member = memberDao.getMemberByCustomerId(customerId);
		if (member == null)
			return false;
		Long cID = null;
		String memberType = member.getMemberType();
		if (MemberType.IDM.name().equals(memberType) || MemberType.IPM.name().equals(memberType)) {
			if (MemberType.IDM.name().equals(memberType)) {
				cID = member.getSuperiorMemberId();
			} else {
				cID = customerId;
			}

		} else if (MemberType.CDM.name().equals(memberType) || MemberType.CPM.name().equals(memberType)) {

			if (MemberType.CDM.name().equals(memberType)) {
				cID = member.getSuperiorMemberId();
			} else {
				cID = customerId;
			}
		}
		StringBuilder sqlber = new StringBuilder();
		sqlber.append(" SELECT DISTINCT 1 FROM customer_service_acc  AS acc \n ");
		sqlber.append(" LEFT JOIN customer_order_hd AS coh ON acc.order_no=coh.order_no \n ");
		sqlber.append(" WHERE 1=1  AND acc.customer_id = ?  AND coh.order_status='CMP' AND acc. remark='RENEW' \n");
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(cID);
		if (!StringUtils.isEmpty(date)) {
			sqlber.append(" AND DATE_ADD(acc.expiry_date, INTERVAL 1 DAY) >= ? \n");
			try {
				param.add(CommUtil.toDate(date, "yyyy-MM-dd"));
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if(!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)) {
				sqlber.append(" AND DATE_ADD(acc.expiry_date, INTERVAL 1 DAY) >= ? \n");
				sqlber.append(" AND DATE_ADD(acc.expiry_date, INTERVAL 1 DAY) >= ? \n");
				try {
					param.add(CommUtil.toDate(startTime, "yyyy-MM-dd"));
					param.add(CommUtil.toDate(endTime, "yyyy-MM-dd"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}else{
			sqlber.append(" AND DATE_ADD(acc.expiry_date, INTERVAL 1 DAY) >= ? \n");
			param.add(DateConvertUtil.parseDate2String(new Date(),"yyyy-MM-dd"));
		}
		boolean result = customerServiceAccDao.isCustomerServiceValid(sqlber.toString(), param);
		return result;
	}
	@Transactional
	public ResponseResult checkServicePlanBookingPerssion(Long customerId,String reservationDate,String view,String device)
	{
		if("Calendar".equals(view)){
			return showView(customerId);
		}
		return checkBookingPerssion(customerId, reservationDate, view, device);
	}
	private  ResponseResult checkBookingPerssion(Long customerId,String reservationDate,String view,String device)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(DateConvertUtil.parseString2Date(reservationDate,"yyyy-MM-dd"));
		responseResult=showView(customerId);
		CalendarViewDto dto=(CalendarViewDto)responseResult.getDto();
		//weekend
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) 
		{
			if(dto.getWeekend()){
				 responseResult.initResult(GTAError.Success.SUCCESS);
			}else{
				 responseResult.initResult(GTAError.FacilityError.NO_BOOKING_PERSSION,new String[]{getI18nMessge(GTAError.BookPerssion.WEEKEND.getCode())});
			}
		}else
		{
			//weekday
			if(dto.getWeekday())
			{
				if(!dto.getHoliday()&&this.checkDateExistHoliday(dto, cal.getTime())){
					responseResult.initResult(GTAError.FacilityError.NO_BOOKING_PERSSION,new String[]{getI18nMessge(GTAError.BookPerssion.PUBLIC_HOLIDAY.getCode())});
				}else{
					 responseResult.initResult(GTAError.Success.SUCCESS);
				}
			}else{
					//holiday
				if(dto.getHoliday()&&this.checkDateExistHoliday(dto, cal.getTime()))
				{
					 responseResult.initResult(GTAError.Success.SUCCESS);
				}else{
					if(this.checkDateExistHoliday(dto, cal.getTime())){
						responseResult.initResult(GTAError.FacilityError.NO_BOOKING_PERSSION,new String[]{getI18nMessge(GTAError.BookPerssion.PUBLIC_HOLIDAY.getCode())});
					}else{
						responseResult.initResult(GTAError.FacilityError.NO_BOOKING_PERSSION,new String[]{getI18nMessge(GTAError.BookPerssion.WEEKDAY.getCode())});	
					}
					
				}
			}
		}
		return responseResult;
	}
	private boolean checkDateExistHoliday(CalendarViewDto dto,Date date){
		for(String holiday:dto.getPublicHoliday()){
			if(holiday.equals(DateConvertUtil.parseDate2String(date, "MM-dd")))
			{
				return true;
			}
		}
		return false;
	}
	
	private ResponseResult showView(Long customerId)
	{
		/***
		 * {
		 *  Weekday : true,
		 *  Weekend:true,
		 *  PublicHoliday:{'04-01',05-01',xxxx}
		 * }
		 */
		String sql=" SELECT r.right_code as rightCode,r.input_value as inputValue FROM service_plan_addition_rule r WHERE r.plan_no=( SELECT sub.service_plan_no FROM customer_service_acc acc ,customer_service_subscribe sub WHERE acc.customer_id="+customerId+" AND acc.acc_no=sub.acc_no AND acc.status='ACT') AND r.right_code IN ('WKD','WKE','PH')";
		List<ServicePlanAdditionRule>rules=this.servicePlanAdditionRuleDao.getDtoBySql(sql, null, ServicePlanAdditionRule.class);
		CalendarViewDto calend=new CalendarViewDto();
		if(null!=rules&&rules.size()>0)
		{
			for (ServicePlanAdditionRule rule : rules)
			{
				//('WKD','WKE','PH')
				if(Constant.WEEKDAY.equals(rule.getRightCode())){
					calend.setWeekday("true".equals(rule.getInputValue())?true:false);
				}else if(Constant.WEEKEND.equals(rule.getRightCode())){
					calend.setWeekend("true".equals(rule.getInputValue())?true:false);
				}else if(Constant.PUBLIC_HOLIDAY.equals(rule.getRightCode()))
				{
					calend.setHoliday("true".equals(rule.getInputValue())?true:false);
					setPublicHoliday(calend);
				}
			}
		}else{
			calend.setWeekday(true);
			calend.setWeekend(true);
			calend.setHoliday(true);
			setPublicHoliday(calend);
		}
		responseResult.initResult(GTAError.Success.SUCCESS,calend);
		return responseResult;
	}
	private void setPublicHoliday(CalendarViewDto calend)
	{
		
		String hql="from PublicHoliday p where p.holidayDate >=CURRENT_DATE()";
		List<PublicHoliday>list=gtaPublicHolidayDao.getByHql(hql);
		if(null!=list&&list.size()>0)
		{
			String holiday[]=new String[list.size()];
			int i=0;
			for (PublicHoliday  day : list)
			{
				holiday[i]=DateConvertUtil.parseDate2String(day.getHolidayDate(),"MM-dd");
				i++;
			}
			calend.setPublicHoliday(holiday);
		}else{
			calend.setPublicHoliday(new String[0]);
		}
	}
	@Override
	@Transactional
	public ResponseResult getPaymentMethodByCustomerId(Long customerId,String type) {
		// TODO Auto-generated method stub
		Long newCustomerId =customerId;
		Member member=memberDao.getMemberByCustomerId(customerId);
		if (null == member) {
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
			return responseResult;
		} else {
			if (memberType.IDM.name().equals(member.getMemberType())
				|| memberType.CDM.name().equals(member.getMemberType())) {
				newCustomerId = member.getSuperiorMemberId();
			}
		}
				
		CustomerServiceAcc servicePlan=customerServiceAccDao.getAactiveByCustomerId(newCustomerId);
		List<PaymentFacilityDto>paymentMethods=new ArrayList<>();
		if(null!=servicePlan){
			 List<CustomerServiceSubscribe> subs=customerServiceSubscribeDao.getCustomerServiceSubscribeByAccNo(servicePlan.getAccNo());
			 if(null!=subs&&subs.size()>0)
			 {
				 //ServicePlanAdditionRule
				 String hql="from ServicePlanAdditionRule r where r.rightCode IN ('"+PaymentMethod.VISA.name()+"','"+PaymentMethod.AMEX.name()+"','"+PaymentMethod.CASHVALUE.name()+"') and r.servicePlan.planNo=?";
				 List<Serializable>param=new ArrayList<>();
				 param.add(subs.get(0).getId().getServicePlanNo());
				 List<ServicePlanAdditionRule>rules=servicePlanAdditionRuleDao.getByHql(hql, param);
				 //before no setting paymentPerssion ,default have three payemnt
				 /***
				  * filter only one payment method cashvalue
				  */
				 handerRules(rules, paymentMethods,type);
			 }
		}
		 if(null==paymentMethods||paymentMethods.size()==0)
		 {
			 //check staff (param paymentMethod ) or patron
				 //member portal/ app   other service payment
			 if(StringUtils.isEmpty(type))
			 {
				 responseResult.initResult(GTAError.MemberShipError.NO_AVAILABLE_PAYMENT_METHOD);	 
			 }else{
				 //topUp message pop
			   responseResult.initResult(GTAError.MemberShipError.TOPUP_CASH_VALUE_ERROR);	
			 }
			  
		 }else{
			 responseResult.initResult(GTAError.Success.SUCCESS,paymentMethods);
				 
		 }
		return responseResult;
	}
	/***
	 * type is topUp filter cashvalue  else on filter
	 * @param rules
	 * @param paymentMethods
	 * @param type
	 */
	private void handerRules(List<ServicePlanAdditionRule>rules,List<PaymentFacilityDto>paymentMethods,String type){
		 if(null==rules||rules.size()==0)
		 {
			 PaymentFacilityDto dto=new PaymentFacilityDto();
			 dto.setPaymentMethodCode(PaymentMethod.VISA.name());
			 dto.setCategory(getI18nMessge(GTAError.PaymentMethod.VISAMASTER.getCode()));
			 paymentMethods.add(dto);
			 
			 if(StringUtils.isEmpty(type))
			 {
				 dto=new PaymentFacilityDto();
				 dto.setPaymentMethodCode(PaymentMethod.CASHVALUE.name());
				 dto.setCategory(getI18nMessge(GTAError.PaymentMethod.CASHVALUE.getCode()));
				 paymentMethods.add(dto);
			 }
			 
			 dto=new PaymentFacilityDto();
			 dto.setPaymentMethodCode(PaymentMethod.AMEX.name());
			 dto.setCategory(getI18nMessge(GTAError.PaymentMethod.AMEX.getCode()));
			 paymentMethods.add(dto);
			 
		 }else{
			 for (ServicePlanAdditionRule plan : rules) 
			 {
				 if("true".equals(plan.getInputValue()))
				 {
					 PaymentFacilityDto dto=new PaymentFacilityDto();
					 dto.setPaymentMethodCode(plan.getRightCode());
					 if(PaymentMethod.VISA.name().equals(plan.getRightCode()))
					 {
						 dto.setCategory(getI18nMessge(GTAError.PaymentMethod.VISAMASTER.getCode()));
					 }else if(PaymentMethod.CASHVALUE.name().equals(plan.getRightCode())){
						 dto.setCategory(getI18nMessge(GTAError.PaymentMethod.CASHVALUE.getCode()));
						 if(!StringUtils.isEmpty(type)){
							 continue;
						 }
						 
					 }else if(PaymentMethod.AMEX.name().equals(plan.getRightCode())){
						 dto.setCategory(getI18nMessge(GTAError.PaymentMethod.AMEX.getCode()));
					 }
					 //topUp
					 paymentMethods.add(dto);	 
						 
					 
				 }
			 }
		 }
	}

	@Override
	@Transactional
	public ResponseResult getPaymentMethodByPatron(Long customerId, String type) {
		// TODO Auto-generated method stub
		List<PaymentFacilityDto>paymentMethods=new ArrayList<>();
		ResponseResult rest=this.getPaymentMethodByCustomerId(customerId, type);
		if(null!=rest&&GTAError.Success.SUCCESS.getCode().equals(rest.getReturnCode()))
		{
			//toup up no change
			if(!StringUtils.isEmpty(type)){
				 return rest;
			}else{
				List<PaymentFacilityDto> orgPaymentMethods=(List<PaymentFacilityDto>)rest.getDto();
				for (PaymentFacilityDto method : orgPaymentMethods)
				{
					if(!(PaymentMethod.VISA.name().equals(method.getPaymentMethodCode())||
							   PaymentMethod.AMEX.name().equals(method.getPaymentMethodCode())))
					{
								paymentMethods.add(method);
					}
				}
			}
			 responseResult.initResult(GTAError.Success.SUCCESS,paymentMethods);
		}
		 return responseResult;
	}
}
