package com.sinodynamic.hkgta.service.fms;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sinodynamic.hkgta.dto.fms.CourseReservationDto;
import com.sinodynamic.hkgta.dto.fms.CourseTransactionDto;
import com.sinodynamic.hkgta.dto.qst.CustomerAttendanceCountDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CourseEnrollmentService extends IServiceBase<CourseEnrollment> {
    
    public ResponseResult modifyCourseEnrollmentStatus(String enrollId, String status, String userName, String internalRemark, String cancelRequesterType, boolean refundFlag) throws Exception;
    
    public ResponseResult confirmCourseReservation(CourseReservationDto dto) throws Exception;
    
    public ResponseResult sendReceiptForCoursePayment(CourseReservationDto dto) throws Exception;
    
    public ResponseResult courseEnrollCallBackService (Long orderNo, String userId, String cardType) throws Exception;
    
    public String getEnrollIdBySessionIdAndAcademyCardNo(String sessionId, String cardNo);
    
    public CourseTransactionDto getTransactionInfoByEnrollId(String enrollId);
    
    public CustomerEmailContent getCourseEnrollEmailContent(Long orderNo) throws Exception;
    
    public Map<String, Object> notifyEnrollMember(SceneType sceneType, CourseEnrollment ce, CourseMaster course);
    
    public String getEnrollIdBySessionIdAndAcademyNo(String sessionId, String academyNO);
    
    public Map<String, List<UserDevice>> getAppMemberAndReminderMsg(Date begin, Date end, String application) throws Exception;
    public Map<String, Set<String>> getAppCoachAndReminderMsg(Date begin, Date end, String application) throws Exception ;
    
    public  CourseEnrollment getCourseEnrollmentById(String enrollId)throws Exception;
    
    public void sendEmailCourseEnroll(String enrollId,String status)throws Exception;
    
    /***
	 * create member attendance golf /tennis course session sql
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public String createStudentAttendanceSql(String startTime,String endTime);
	
	/***
	 * 
	 * @param customerId
	 * @param code  GSSC/TSSC
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public String createStudentAttendanceSql(Long customerId,String code,String startTime, String endTime);
}