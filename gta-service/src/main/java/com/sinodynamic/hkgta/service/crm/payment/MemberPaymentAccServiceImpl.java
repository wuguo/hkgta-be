package com.sinodynamic.hkgta.service.crm.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class MemberPaymentAccServiceImpl extends ServiceBase<MemberPaymentAcc> implements MemberPaymentAccService {
	
	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;

	@Transactional
	public String getVccNoByCustomerId(Long CustomerId) {
		
		MemberPaymentAcc acc = memberPaymentAccDao.getMemberPaymentAccByCustomerId(CustomerId);
		return acc == null ? null : acc.getAccountNo();
	}

}
