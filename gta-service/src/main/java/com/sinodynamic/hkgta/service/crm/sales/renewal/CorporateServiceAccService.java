package com.sinodynamic.hkgta.service.crm.sales.renewal;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.CorporateServiceAccDto;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceAcc;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;


public interface CorporateServiceAccService extends IServiceBase<CorporateServiceAcc>{
	

	    
	public CorporateServiceAccDto getAactiveByCustomerId(Long customerId);

}
