package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface AgeRangeService extends IServiceBase<AgeRange>{
	public List<AgeRange> getAllAgeRange();
}
