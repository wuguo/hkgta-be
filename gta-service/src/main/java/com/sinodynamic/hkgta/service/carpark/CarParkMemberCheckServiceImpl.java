package com.sinodynamic.hkgta.service.carpark;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;




import com.sinodynamic.hkgta.dao.carpark.CarParkMemberDao;
import com.sinodynamic.hkgta.dto.carpark.CarParkMemberCheckDto;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class CarParkMemberCheckServiceImpl extends ServiceBase implements CarParkMemberCheckService{

	@Autowired
	private CarParkMemberDao carParkMemberDao;
	
	@Transactional
	public List<CarParkMemberCheckDto> getCarParkMemberCheckList(){
		
		return carParkMemberDao.getCarParkMemberList();
	}
	@Transactional
	public List<CarParkMemberCheckDto> getCarParkStaffCheckList(){
		return carParkMemberDao.getCarParkStaffList();
	}
}
