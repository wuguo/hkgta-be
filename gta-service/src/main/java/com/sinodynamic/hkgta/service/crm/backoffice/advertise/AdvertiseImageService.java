package com.sinodynamic.hkgta.service.crm.backoffice.advertise;

import com.sinodynamic.hkgta.dto.crm.AdvertiseImageDto;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageUploadDto;
import com.sinodynamic.hkgta.entity.crm.AdvertiseImage;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface AdvertiseImageService extends IServiceBase<AdvertiseImage> {

	public AdvertiseImage get(long imgId);

	public ResponseResult getList(String appType, String dispLoc);

	public ResponseResult create(AdvertiseImageDto dto);

	public ResponseResult delete(AdvertiseImage a);
	
	public ResponseResult createAdvertiseImages(String dispLoc,AdvertiseImageUploadDto dto,String userId);
	
	public ResponseResult getAdvertiseImageList(String appType, String dispLoc);

}
