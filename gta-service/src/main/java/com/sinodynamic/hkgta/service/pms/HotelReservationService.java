package com.sinodynamic.hkgta.service.pms;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


import com.sinodynamic.hkgta.dto.crm.GuessRoomReservationDetailDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationCancelDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.HotelReservationItemInfoDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationDto;
import com.sinodynamic.hkgta.dto.push.RegisterDto;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface HotelReservationService extends IServiceBase {
	CustomerOrderTrans hotelReservationOnlinePayment(HotelReservationPaymentDto paymentDto);
	CustomerOrderTrans hotelReservationCashvaluePayment(HotelReservationPaymentDto paymentDto);
	void bookHotel(List<RoomReservationDto> reservations);
	void updateRoomReservationStatus(String confirmId, RoomReservationStatus status);
	void updateRoomReservationStatusInSameTxn(String confirmId, RoomReservationStatus status);
	void requestCancelPayment(String confirmId, HotelReservationCancelDto cancelDto);
	List<RoomReservationRec> getAllFailedReservations();
	List<RoomReservationRec> getAllFailedReservations(Integer timeoutMin);
	List<Long> getBundledFacilityBookings(String confirmId);
	void cancelBundledFacilityBookings(String confirmId, HotelReservationCancelDto cancelDto);
	GuessRoomReservationDetailDto getHotelReservationDetail(String confirmId);
	void updateGuessroomAndFacilityAssociation(String loginUserId, Map<String, List<Long>> association, Map<String, Object> params);
	Long getRefundIdIfAvailable(String confirmId);
	Map<String, Object> exposeTxnInfo(String confirmId);
	
	/**
	 * 根据resvId查询预定好的房间信息
	 * @param resvId
	 * @return RoomReservationRec
	 */
	public RoomReservationRec getReservationByResvId(String resvId);
	
	public List<RoomReservationRec> getReservationByCustomerID(Long customerid);
	public List<RoomReservationRec> getReservationByOrderNo(Long orderNo);
	public void preAssignRoom(Long roomId, RoomReservationRec rec, String updateBy);
	public RoomReservationRec getReservationByConfirmId(String confirmId);
	public boolean checkRoomOccupy(Room room, RoomReservationRec rec);
	
	/***
	 *  set guestRoom stay  night
	 * @param reservationId
	 * @param night
	 * @return
	 */
	public ResponseResult extendGuestRoomStay(Long reservationId,Long night,String userId,String roomNo);
	public MemberCashvalue getMemberCashvalue(Long customerId);
	public BigDecimal getTotalOrderAmount(List<HotelReservationItemInfoDto> items);
	/**
	 * 验证该Member 的cash value或credit limit是否足以预定任何房间
	 * @param paymentDto
	 * @throws GTACommonException
	 */
	public void verificationCashValue(HotelReservationPaymentDto paymentDto) throws GTACommonException;
	
}
