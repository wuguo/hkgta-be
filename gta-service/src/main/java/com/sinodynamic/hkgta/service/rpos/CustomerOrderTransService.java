package com.sinodynamic.hkgta.service.rpos;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.crm.ReservationDto;
import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.dto.crm.SourceBookingDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.qst.CustomerAttendanceCountDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.dto.statement.StatementDto;
import com.sinodynamic.hkgta.dto.statement.StatementParamsDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.jasperreports.engine.JRException;

public interface CustomerOrderTransService extends IServiceBase<CustomerOrderTrans> {
	public ListPage<CustomerOrderHd> getCustomerTransactionList(ListPage<CustomerOrderHd> page, String balanceDue, String serviceType, String orderBy,
			boolean isAscending, String searchText, String filterBySalesMan, String mode, String userId, String enrollStatusFilter, String memberType,
			String device);

	public ResponseResult savecustomerOrderTransService(CustomerOrderTransDto customerOrderTransDto, Map<String, Long> transactionNoMap)
			throws Exception;

	public ResponseResult getPaymentDetailsByOrderNo(Long orderNo);

	public ResponseResult getPaymentDetailsAccountantByOrderNo(Long orderNo);

	public BigDecimal getFalueUnreadStatus(String type);

	public void moveTranscriptFile(Long transactionNo, Long customerId);

	public CustomerOrderTrans getCustomerOrderTrans(Long transactionNo);

	public ResponseResult paymentByCreditCard(CustomerOrderTransDto customerOrderTransDto);

	public Long getOrderTimeoutMin();

	public List<CustomerOrderTrans> getTimeoutPayments();

	public void updateCustomerOrderTrans(CustomerOrderTrans customerOrderTrans);

	public CustomerOrderHd getCustomerOrderHdByOrderNo(Long orderNo);

	public void addNewTransactionRecord(CustomerOrderTrans customerOrderTrans);

	public List<CustomerOrderTrans> getCustomerOrderTransListByOrderNo(Long orderNo);

	public CustomerOrderTransDto payment(MemberCashValuePaymentDto memberCashValuePaymentDto);

	public List<AdvanceQueryConditionDto> assembleQueryConditions(String serviceType);

	public ResponseResult getAllCreatedStaff();

	public ResponseResult getAllAuditedStaff();

	public ResponseResult getSattlementReport(ListPage<CustomerOrderTrans> page, SearchSettlementReportDto dto);

	public ResponseResult getSattlementReportExport(ListPage<CustomerOrderTrans> page, SearchSettlementReportDto dto, HttpServletResponse response)
			throws Exception;

	public ResponseResult readTrans(CustomerOrderTransDto customerOrderTransDto);

	public ResponseResult handleCreditCardByPos(PosResponse posResponse);

	/**
	 * Used to change enrollment status from Approved to Payment Approved or Payment Approved to Approved
	 * 
	 * @param transactionNo
	 * @author Liky_Pan
	 */
	public void updateEnrollmentStatusOncePayementChange(Long transactionNo);

	public void generatePatronAccountStatement(String startDateString, String endDateString) throws Exception;

	public void manualGenerateStatement(String year, String month) throws Exception;

	public void modifyCustomerOrderHd(CustomerOrderHd order);

	public ResponseResult getStatementPDF(StatementParamsDto statementParamsDto) throws Exception;

	public ResponseResult getStatementPDFBySearch(StatementParamsDto statementParamsDto, List<StatementDto> list) throws Exception;

	public byte[] getInvoiceReceipt(Long orderNo, String transactionNo, String receiptType);

	public ResponseResult sentTransactionEmail(TransactionEmailDto dto, LoginUserDto userDto);

	public TransactionEmailDto getEmailDeailReady(TransactionEmailDto transactionEmailDto, String userName);

	public ResponseResult handlePosPayDaypass(PosResponse posResponse);

	public ResponseResult handlePosPayPrivateCoach(PosResponse posResponse);

	public ResponseResult getMonthlyEnrollment(ListPage<CustomerOrderTrans> page, String year, String month);

	public ResponseResult getDailyRevenue(String startTime, String endTime, String location);

	public byte[] getDailyRevenueAttachment(String startTime, String endTime, String location, String fileType);

	public ResponseResult getDailyRevenueInDetail(ListPage page,String paymentMethod,String startDate, String endDate, String type);
	
	public byte[] getDailyRevenueInDetailAttachement(ListPage page,String paymentMethod,String startDate, String endDate, String fileType, String type) throws Exception;
	
	/*
	 * SGG-1384
	 */
	public ResponseResult getMonthlyTransaction(ListPage<SourceBookingDto> page, String reservationMonth);

	public byte[] getMonthlyTransactionAttachment(ListPage page, String reservationMonth, String fileType) throws Exception;

	public ResponseResult getReservationReport(ListPage<ReservationDto> page, String day, String type, String status);

	public ResponseResult getMemberInfo(ListPage<ReservationDto> page, String status);

	public byte[] getReservationReportAttachement(ListPage page, String day, String fileType, String type, String status) throws Exception;

	public byte[] getMemberInfoAttachement(ListPage page, String fileType, String status) throws Exception;

	public byte[] getMonthlyEnrollmentAttachment(String year, String month, String fileType, String sortBy, String isAscending);

	public ResponseResult getDailyMonthlyPrivateCoachingRevenue(String timePeriodType, ListPage<?> page, String selectedDate, String facilityType);

	public byte[] getDailyMonthlyPrivateCoachingRevenueAttach(String timePeriodType, String selectedDate, String fileType, String sortBy,
			String isAscending, String facilityType);

	public byte[] getEnrollmentRenewal(String fileType, String sortBy, String isAscending) throws Exception;

	/* SGG-1380 */
	public ResponseResult getGolfAndTennisCachesPerform(ListPage page, String reportMonth) throws ParseException;

	public byte[] getGolfAndTennisCachesPerformAttachment(ListPage page, String reservationMonth, String fileType)
			throws JRException, ParseException, Exception;

	public ResponseResult getCoachTraining(ListPage page, String coachUserId, String startDate, String endDate);

	public byte[] getCoachTrainingReport(ListPage page, String fileType, String coachUserId, String coachName, String startDate, String endDate)
			throws Exception;

	/**
	 * 根据【cuseromerId】获取【day】天内member的CashValue变更日志查询语句
	 * 
	 * @param type
	 * @param customerId
	 * @param day
	 * @return
	 */
	public String getCashValueChangesLog(String type, long customerId, int day);

	public void createFlexPaymentReport(Date dateTime) throws Exception;

	public List<PaymentFacilityDto> getPaymentFacilityList(String dateTime);

	public byte[] getCustomerOrderTransAttachment(String startDate, String endDate, String fileType, String sortBy, String isAscending);

	public StringBuilder getCustomerOrderTransSQL();
	
	public ResponseResult getPatronActivitiesReport(ListPage<ReservationDto> page, String day, String academyID);

	public byte[] getPatronActivitiesAttachement(ListPage page, String day, String fileType, String academyID) throws Exception;
	/***
	 * get daily cashValueOasis 
	 * @param date yyyy-mm-dd
	 * @return
	 */
	public String getCashValueOasis(String date);

	public ResponseResult getCashValueVoidReport(ListPage page, String day);

	public byte[] getCashValueVoidAttachement(ListPage page, String day, String fileType) throws Exception;

	public byte[] generateGuestRoomResvAttach(Long transactionNo);

	/**
	 * 根据时间获取serviceRefund报表
	 * @param startTime
	 * @param endTime
	 * @param fileType
	 * @return
	 */
	public byte[] getServiceRefundReports(String startTime, String endTime, String fileType);


	/**
	 * 根据时间获取CashValueAdjustment报表
	 * @param type
	 * @param customerId
	 * @param startTime
	 * @param endTime
	 * @param fileType
	 * @return
	 */
	public byte[] getCashValueAdjustmentReports(String type, Long customerId, String startTime, String endTime, String fileType);

	/**
	 * For Cash Value/Cash Value Txn In Oasis
	 * @param date
	 * @return
	 */
	public String getCashValueTxnInOasis(String date);
	
	/***
	 * generateStatement report by params
	 * @param startDateString
	 * @param endDateString
	 * @param academyNo
	 * @return
	 */
	public ResponseResult generateStatementByAcademyNo(String year, String month,String academyNo) ;
	
	/***
	 * create Settling the payment by cash value in restaurant sql
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public String  createRestaurantSettlingSql(String startTime,String endTime);
	
	public String  createRestaurantSettlingSql(Long customerId,String startTime,String endTime);

	/**
	 * 获取用CashValue的MMS支付数据
	 * @param dateRange
	 * @return
	 * @throws Exception 
	 */
	public String getCashValueByMMS(String dateRange) throws Exception;
	
	public void saveCustomerOrderTrans(CustomerOrderTrans customerOrderTrans);
	
	public ResponseResult getSpendingAmountPatron(ListPage page, String startDate, String endDate,String servicePlanName,String paymentMethod);
	public byte[] getSpendingAmountPatronReport(String sortBy,String isAscending,String startDate, String endDate, String servicePlanName,String paymentMethod, String fileType);
	
	public ResponseResult getTopUpAmountPatron(ListPage page, String startDate, String endDate,String servicePlanName);
	
	public byte[] getTopUpAmountPatronReport(String sortBy,String isAscending,String startDate, String endDate, String servicePlanName,String fileType);
	
	public ResponseResult getTopPatronSpendingFacilities(ListPage page, String startDate, String endDate,String servicePlanName,String facilityType);
	
	public byte[] getTopPatronSpendingFacilitiesReport(String sortBy,String isAscending,String startDate, String endDate, String servicePlanName, String facilityType ,String fileType);
	
	public ResponseResult getUsageRatePatronDiffFacilities(ListPage page, String startDate, String endDate,String servicePlanName,String facilityType,String sortBy,String isAscending);
	
	public byte[] getUsageRatePatronDiffFacilitiesReport(String sortBy,String isAscending,String startDate, String endDate, String servicePlanName, String facilityType ,String fileType);
	
	public ResponseResult getSpendingAmountPatronDiffFacilities(ListPage page, String startDate, String endDate,String servicePlanName);
	
	public byte[] getSpendingAmountPatronDiffFacilitiesReport(String sortBy,String isAscending, String startDate, String endDate,String servicePlanName,String fileType);

}
