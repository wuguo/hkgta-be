package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityAttributeCaptionDao;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class FacilityAttributeCaptionServiceImpl extends
		ServiceBase<FacilityAttributeCaption> implements FacilityAttributeCaptionService {
	@Autowired
	private FacilityAttributeCaptionDao facilityAttributeCaptionDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityAttributeCaption> getFacilityAttributeCaptionByFuzzy(String attributeId)
	{
		return facilityAttributeCaptionDao.getFacilityAttributeCaptionByFuzzy(attributeId);
	}

	@Override
	@Transactional
	public FacilityAttributeCaption getFacilityAttributeCaptionByAttributeId(String attributeId)
	{
		return facilityAttributeCaptionDao.getFacilityAttributeCaptionByAttributeId(attributeId);
	}




}
