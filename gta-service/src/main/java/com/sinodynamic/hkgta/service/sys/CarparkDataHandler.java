package com.sinodynamic.hkgta.service.sys;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.sys.CarparkDataDto;
import com.sinodynamic.hkgta.dto.sys.CarparkDataDto.DataDetail;
import com.sinodynamic.hkgta.dto.sys.CarparkDataDto.Header;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceImport;
import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceIo;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.constant.CarParkCSVRowType;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Component
public class CarparkDataHandler {

	private Logger logger = Logger.getLogger(CarparkDataHandler.class);
	
	private final static String carparkInterfacePath = "carparkInterface.in.path";
	private final static String carparkInterfaceSucPath = "carparkInterface.success.path";
	private final static String carparkInterfaceFailPath = "carparkInterface.fail.path";
	private final static String carparkInterfaceBackPath = "carparkInterface.back.path";

	@Resource(name = "appProperties")
	private Properties appProps;

	@Autowired
	private CarparkInterfaceIoService carparkInterfaceIoService;

	@Autowired
	private CarparkInterfaceImportService carparkInterfaceImportService;
	
	@Autowired
	private PermitCardMasterService permitCardMasterService;

	public void excute() {
		String carParkPath = appProps.getProperty(carparkInterfacePath) + File.separatorChar;
		String carParkBackPath = appProps.getProperty(carparkInterfaceBackPath) + File.separatorChar;
		//read multiple file from path
		File[] carParkFiles = new File(carParkPath).listFiles();
		for (File carParkFile : carParkFiles) {
			logger.info("read carParkInterface file :" + carParkFile.getAbsolutePath());
			//filter valid file 
			if(carParkFile.isDirectory())continue;
			String fileName = carParkFile.getName();
			//check file format correct
			if (checkFileNameLegal(fileName)) {
				try {
					//check exist db table CarparkInterfaceImport
					if(carparkInterfaceImportService.checkExistByFileName(fileName)){
						logger.error("the fileName had handler......");
						continue;
					}
					//read file content 
					CarparkDataDto data = readFile(carParkFile);
					//matches file content header time eq file format 
					if (matchedHeaderFileName(data.getHeader().getTime(), fileName)) {
						//save data to DB
						this.saveDataToDB(data, fileName);
					} else {
						logger.error("The file name data format no  matched header data format........");
						continue;
					}
				} catch (Exception e) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					e.printStackTrace(new PrintStream(baos));
					logger.error(baos.toString());
					continue;
				}
				File back=new File(carParkBackPath);
				if(!back.exists()){
					back.mkdirs();
				}
				//back up file to back path
				FileUtil.moveFile(carParkFile.getAbsolutePath(), carParkBackPath + "back_"
						+ DateConvertUtil.formatDate(new Date(), "yyyyMMddHHmmss") + "_" + fileName);
			} else {
				logger.error("the carParkInterfacefile name format incorrect.." + fileName);
			}

		}

	}
	private void saveDataToDB(CarparkDataDto data, String fileName) {
		if (null != data) {
			CarparkInterfaceImport entity = new CarparkInterfaceImport();
			try {
				entity.setFileHeaderDatetime(
						DateConvertUtil.parseString2Date(data.getHeader().getTime(), "yyyyMMddHHmmss"));
				entity.setImportFilename(fileName);
				entity.setStatus(Status.SUC.name());
				entity.setCreateDate(new Date());
				Long importId = (Long) carparkInterfaceImportService.save(entity);
				logger.info("save CarparkInterfaceImport success..........");
				if(!this.saveDetail(data, importId)){
					entity.setStatus(Status.FAIL.name());
					entity.setUpdateDate(new Date());
					carparkInterfaceImportService.update(entity);
					logger.info("update CarparkInterfaceImport status is fail success..........");
				}
			} catch (Exception e) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				e.printStackTrace(new PrintStream(baos));
				logger.error("save carparkInterface import have exception :" + baos.toString());
			}
		}else{
			logger.error("read cardpark content null......");
		}

	}
	private Long getCustomerIdByCardId(String cardId)
	{
		PermitCardMaster per=permitCardMasterService.getPermitCardMasterBySerialNo(cardId);
		if(null!=per){
			return per.getMappingCustomerId();
		}
		return null;
	}
	private boolean saveDetail(CarparkDataDto dto, Long importId) {
		Header header = dto.getHeader();
		List<DataDetail> fails = new ArrayList<>();
		List<DataDetail> sucs = new ArrayList<>();
		for (DataDetail data : dto.getDetail()) {
			CarparkInterfaceIo io = new CarparkInterfaceIo();
			io.setCardType(data.getCardType());
			io.setCreateDate(new Date());
			io.setStatus(data.getEventCode());//YES OR NO
			io.setImportId(importId);
			io.setImportCardId(data.getCardId());
			
			//02 is Patron card
			if("02".equals(data.getCardType())){
				Long customerId=getCustomerIdByCardId(data.getCardId());
				if(null!=customerId){
					io.setCustomerId(customerId);
				}
			}
			String vehicleNum=data.getVehiclePlateNumber();
			if(StringUtils.isNotEmpty(vehicleNum))
			{
				if(vehicleNum.contains("/"))
				{
					vehicleNum=data.getVehiclePlateNumber().split("/")[0];
				} 
			}
			io.setVehiclePlateNo(vehicleNum);
			
			if (StringUtils.isNotEmpty(data.getInTime())) {
				if (parseDate(data.getInTime())) {
					io.setInTime(DateConvertUtil.parseString2Date(data.getInTime(), "yyyy-MM-dd HH:mm:ss"));
				} else {
					// error intime format incorrect
					data.setErrorCode("0001");//
					fails.add(data);
					continue;
				}
			}
			if (StringUtils.isNotEmpty(data.getOutTime())) {
				if (parseDate(data.getOutTime())) {
					io.setOutTime(DateConvertUtil.parseString2Date(data.getOutTime(), "yyyy-MM-dd HH:mm:ss"));
				} else {
					// error intime format incorrect
					data.setErrorCode("0002");//
					fails.add(data);
					continue;
				}
			}
			data.setResponseDate(DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd HH:mm:ss"));
			try {
				carparkInterfaceIoService.saveCarparkInterfaceIo(io);
				logger.info("save CarparkInterfaceIo sucess...");
				
				data.setErrorCode("0");
				sucs.add(data);
			} catch (Exception e) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				e.printStackTrace(new PrintStream(baos));
				logger.error("save carpark Interface Io happend exception:" + baos.toString());
				data.setErrorCode("0003");//
				fails.add(data);
			}
		}
		this.writerFileFolder(header, sucs, Status.SUC.name());
		if (null == fails || fails.size() == 0) {
			return true;
		}
		this.writerFileFolder(header, fails, Status.FAIL.name());
		return false;
	}

	private void writerFileFolder(Header header, List<DataDetail> details, String type) {
		// “Row Type”,“Card Type”,” Card SN”,”In Time”,”Out Time”,”Licence Plate Number”,”Event Code”,”Error Code”,”Response Date/Time” Char(13) +// Char(10)
		if(null==details||details.size()==0) return ;
		StringBuilder sbContent = new StringBuilder();
		char r = '\r';
		char n = '\n';
		String headerContent = "\"" + CarParkCSVRowType.HEADER.getRowType() + "\"" + "," + "\"" + header.getTime()
				+ "\"" + "," + "\"" + "RETURN" + "\"" + r + n;
		sbContent.append(headerContent);
		for (DataDetail detail : details) {
			String row = "\"" + CarParkCSVRowType.DETAIL.getRowType() + "\""
					+ "," + "\"" + detail.getCardType() + "\"" + "," + "\"" + detail.getCardId() + "\"" + "," + "\""
					+ detail.getInTime() + "\"" + "," + "\"" + detail.getOutTime() + "\"" + "," + "\""
					+ detail.getVehiclePlateNumber() + "\"" + "," + "\"" + detail.getEventCode() + "\"" + "," + "\""
					+ detail.getErrorCode() + "\"" + "," + "\"" + detail.getResponseDate() + "\"" + r + n;
			sbContent.append(row);
		}
		String footer = "\"" + CarParkCSVRowType.FOOTER.getRowType() + "\"" + "," + "\"" + details.size() + "\"";
		sbContent.append(footer);
		String pathName = null;
		String date =header.getTime();
		String suffix = File.separatorChar + "ACCESS" + date + ".Return.txt";
		if (Status.SUC.name().equals(type)) {
			// save loca file to Success file ACCESSyyyyMMddHHmmss.Return.txt
			pathName = appProps.getProperty(carparkInterfaceSucPath);
		} else {
			// save local file to Fail file ACCESSyyyyMMddHHmmss.Return.txt
			pathName = appProps.getProperty(carparkInterfaceFailPath);
		}
		File file = new File(pathName);
		if (!file.exists()) {
			file.mkdirs();
		}
		file=new File(pathName + suffix);
		this.writerContent(file, sbContent.toString());
		logger.info("save local "+type+" catalog "+ file.getAbsolutePath());
	}

	private void writerContent(File file, String content) {
		FileWriter wr = null;
		try {
			wr = new FileWriter(file);
			wr.write(content);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != wr) {
				try {
					wr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private boolean parseDate(String time) {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dateFormat.parse(time);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	private CarparkDataDto readFile(File file) throws Exception {
		CarparkDataDto dto = new CarparkDataDto();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				String items[] = itemFormat(sCurrentLine);
				if (items.length > 0 && "H".equals(items[0])) {
					readHeader(items, dto);
				} else if (items.length > 0 && "D".equals(items[0])) {
					readDetail(items, dto);
				} else if (items.length > 0 && "F".equals(items[0])) {
					readFooter(items, dto);
				}
			}
			if (dto.getDetail().size() != dto.getFooter().getTotalLine()) {
				logger.error("ERROR !CarParkInterface file: total record size not equals record lines count ..");
				throw new GTACommonException(
						"ERROR !CarParkInterface file: total record size not equals record lines count ..");
			}
		} finally {
			if (null != br)
				br.close();
		}
		return dto;
	}

	private void readDetail(String items[], CarparkDataDto dto) throws Exception {
		if (items.length < 9) {
			throw new GTACommonException("the Detail content format incorrect...");
		}
		CarparkDataDto.DataDetail detail = dto.new DataDetail();
		detail.setRowType(items[0]);
		detail.setCardType(items[1]);
		detail.setCardId(items[2]);
		detail.setInTime(items[3]);
		detail.setOutTime(items[4]);
		detail.setVehiclePlateNumber(items[5]);
		detail.setEventCode(items[6]);
		detail.setErrorCode(items[7]);
		detail.setResponseDate(items[8]);
		dto.getDetail().add(detail);
	}

	private void readFooter(String items[], CarparkDataDto dto) throws Exception {
		if (items.length < 2) {
			throw new GTACommonException("the Footer content format incorrect...");
		}
		CarparkDataDto.Footer footer = dto.new Footer();
		footer.setRowType(items[0]);
		if (items[1].matches("^\\d*$")) {
			footer.setTotalLine(Integer.valueOf(items[1]));
		} else {
			throw new GTACommonException("the Footer total isn't number.....");
		}
		dto.setFooter(footer);
	}

	private void readHeader(String items[], CarparkDataDto dto) throws Exception {
		if (items.length < 3) {
			throw new GTACommonException("the header content format incorrect...");
		}
		CarparkDataDto.Header header = dto.new Header();
		header.setRowType(items[0]);
		header.setTime(items[1]);
		header.setStatus(items[2]);
		dto.setHeader(header);
	}

	private String[] itemFormat(String line) {
		String[] items = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		for (int i = 0; i < items.length; i++) {
			items[i] = items[i].replace("\"", "");
		}
		return items;
	}

	/***
	 * match header time with fileName format
	 * @param headerTime
	 * @param fileName
	 * @return true or false
	 */
	private boolean matchedHeaderFileName(String headerTime, String fileName) {
		String name = fileName.split("\\.")[0];
		name = name.replace("ACCESS", "");
		if (headerTime.equals(name)) {
			return true;
		}
		return false;
	}
	
	/***
	 * check file name valid
	 * file format 
	 * @param fileName ACCESSyyymmddhhmmss.SEND.txt
	 * @return  true or false
	 */
	private boolean checkFileNameLegal(String fileName) {
		String exr=fileName.toLowerCase();
		if(!exr.matches("(access\\d{14}.send.txt)")){
			return false;
		}
		String name = exr.split("\\.")[0];
		name = name.replace("access", "");
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			dateFormat.parse(name);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
}
