package com.sinodynamic.hkgta.service.crm.sales.renewal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;

import org.apache.commons.lang.StringUtils;
import org.hibernate.type.BigIntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.ExpireMemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MemberPlanFacilityRightDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanAdditionRuleDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanFacilityDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.PosServiceItemPriceDao;
import com.sinodynamic.hkgta.dto.crm.ExpiredMemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberInfoDto;
import com.sinodynamic.hkgta.dto.crm.RenewalDto;
import com.sinodynamic.hkgta.dto.crm.RenewalEmailDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribePK;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CustomerServiceStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberExpiredOrderByCol;
import com.sinodynamic.hkgta.util.constant.OrderStatus;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.constant.ServicePlanOfferCode;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RenewalServiceImpl extends ServiceBase implements RenewalService {
	
	@Autowired
	private CustomerServiceDao customerServiceDao;
	
	@Autowired
	private CustomerServiceSubscribeDao customerServiceSubscribeDao;
	
	@Autowired
	private ServicePlanDao servicePlanDao;
	
	@Autowired
	private ExpireMemberDao expireMemberDao;

	
	@Autowired
	private PosServiceItemPriceDao posItemPriceDao;
	
	@Autowired
	private  CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private PosServiceItemPriceDao posServiceItemPriceDao;
	
	@Autowired
	private MemberPlanFacilityRightDao memberPlanFacilityRightDao;
	
	@Autowired
	private ServicePlanFacilityDao servicePlanFacilityDao;
	
	@Autowired
	private ServicePlanAdditionRuleDao servicePlanAdditionRuleDao;
		
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	/**
	 * 1. search the customer's customerServiceAcc, if the statue is "InActivate" ,then don't allow renewal
	 * 2. update customerServiceAcc AND customerServiceSubcribe 
	 * 3. get the total_amout of the servicePlan
	 * 4. update customer's account,include customer_order_hd and customer_oder_det
	 * 5. create member_plan_facility_right
	 * 6. create customer_limit_rule 
	 */
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class) 
	@Override
	public ResponseResult startUpMemberAcc(RenewalDto renewalDto) throws Exception{
//		ResponseResult responseResult = new ResponseResult();
		/*
		 * these variable is not sure,then change later
		 *
		 */
		String accCat = "IDV"; // COP-Corporate/IDV - Individual
//		String createBy = "Li_Chen"; //get this by session user
		String createBy = renewalDto.getCreateBy(); 
		//get the latest customerServiceAcc by the big accNo
		CustomerServiceAcc csAcc = customerServiceDao.getLatestCustomerService(renewalDto.getCustomerId());
		//get servicePlan
		ServicePlan renewalServicePlan = servicePlanDao.get(ServicePlan.class,renewalDto.getServicePlanNo());
		//if don't exists this cssAcc
		if (null == csAcc) {
			responseResult.initResult(GTAError.EnrollmentAdvanceError.SERVICE_ACCOUNT_NOT_EXISTS);
			return responseResult;
		}
		if (null == renewalServicePlan ){
			responseResult.initResult(GTAError.EnrollmentAdvanceError.SERVICE_PLAN_NOT_EXISTS);
			return responseResult;
		}
		//get status return information
		if (CustomerServiceStatus.NACT.getDesc().equals(csAcc.getStatus()) ) {
			responseResult.initResult(GTAError.EnrollmentAdvanceError.SERVICE_ACCOUNT_IS_INACTIVE);
			return responseResult;
		}
		//create a new customerServicAcc 
		CustomerServiceAcc newCustomerServiceAcc = new CustomerServiceAcc();
		newCustomerServiceAcc.setCustomerId(renewalDto.getCustomerId());
		newCustomerServiceAcc.setStatus(CustomerServiceStatus.NACT.getDesc());
		
		newCustomerServiceAcc.setAccCat(accCat);
		newCustomerServiceAcc.setPeriodCode(renewalServicePlan.getPassPeriodType());
		newCustomerServiceAcc.setRemark("RENEW");
		newCustomerServiceAcc.setStatementDeliveryBy("EMAIL");
		/*
		 *setEffectiveDate
		 *1 if the status is  ACT,then set effectiveDate is getExpiryDate+1,set ExpiryDate is effectiveDate + servicePlan's contract_length_month
		 *2 if the status is  EXP,then set effectiveDate is today,set ExpiryDate is today + servicePlan's contract_length_month
		 */
		//Set the effectiveDate and expiryDate after complete payment.
		/*Calendar calendar = Calendar.getInstance();
		if (CustomerServiceStatus.ACT.getDesc().equals(csAcc.getStatus())) {
			calendar.setTime(csAcc.getExpiryDate());
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			//setEffectiveDate
			newCustomerServiceAcc.setEffectiveDate(calendar.getTime());
			if(renewalServicePlan.getContractLengthMonth()!=null) {
				calendar.add(Calendar.MONTH, renewalServicePlan.getContractLengthMonth().intValue());
			}
			//setEffectiveDate
			newCustomerServiceAcc.setExpiryDate(calendar.getTime());
		} else if (CustomerServiceStatus.EXP.getDesc().equals(csAcc.getStatus())) {
			calendar.setTime(new Date());
			//setEffectiveDate
			newCustomerServiceAcc.setEffectiveDate(calendar.getTime());
			if(renewalServicePlan.getContractLengthMonth()!=null) {
				calendar.add(Calendar.MONTH, renewalServicePlan.getContractLengthMonth().intValue());
			}
			//setEffectiveDate
			newCustomerServiceAcc.setExpiryDate(calendar.getTime());
		}*/
		//save the new CustomerServiceAcc	
		Long accNo = (Long)customerServiceDao.save(newCustomerServiceAcc);
		
		CustomerServiceSubscribe csSub = new CustomerServiceSubscribe();
		//set primary ID
		CustomerServiceSubscribePK cspk = new CustomerServiceSubscribePK();
		cspk.setServicePlanNo((long)renewalServicePlan.getPlanNo());
		cspk.setAccNo(newCustomerServiceAcc.getAccNo());
		csSub.setId(cspk);
		csSub.setSubscribeDate(new Date());
		csSub.setCreateDate(new Timestamp(new Date().getTime()));
		/* get the sessionUser to do */
		csSub.setCreateBy(createBy);
		csSub.setUpdateDate(new Date());
		csSub.setUpdateBy(createBy);
		
		
		//save  customer's order,include customer_order_hd and customer_oder_det
		RenewalDto dot = new RenewalDto();
		Map<String,String>  resultMap = null;
	

		resultMap = startUpCustomerOrder( renewalDto.getCustomerId(),renewalDto.getServicePlanNo(),createBy);
		
		if(null == resultMap)
			return new ResponseResult(ResponseMsgConstant.ERRORCODE, "can't find the service plan!");
	
		csSub.setOrderDetId(Long.parseLong(resultMap.get("coDetId")));
		customerServiceSubscribeDao.save(csSub);
		
		/*
		 * StartUp limit rule and facility right when active this customer service account
		 * // save member_plan_facility_right 
		starUpCustomerPlanFacilityRight(Long.valueOf(renewalDto.getCustomerId()),Long.valueOf(renewalDto.getServicePlanNo()),newCustomerServiceAcc,createBy);
		
		//save member_limit_rule
		startUpMemberLimitRule(Long.valueOf(renewalDto.getCustomerId()),Long.valueOf(renewalDto.getServicePlanNo()),newCustomerServiceAcc,createBy);*/
		
		Long orderNo = Long.valueOf(resultMap.get("orderNo"));
		newCustomerServiceAcc.setOrderNo(orderNo);
		customerServiceDao.update(newCustomerServiceAcc);
		dot.setOrderNo(orderNo);
		dot.setAccNo(accNo);
		dot.setCustomerId(renewalDto.getCustomerId());
		dot.setServicePlanNo(renewalDto.getServicePlanNo());
		responseResult.setData(dot);
		responseResult.initResult(GTAError.Success.SUCCESS, dot);
		byte[] attchment = customerOrderTransDao.getInvoiceReceipt(Long.valueOf(resultMap.get("orderNo")), null, "invocie");	
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();
		CustomerProfile cp = null;
		if (null !=orderNo) {
			CustomerOrderHd customerOrderHd = customerOrderHdDao.get(CustomerOrderHd.class, orderNo);
			cp = customerProfileDao.get(CustomerProfile.class, customerOrderHd.getCustomerId());
		}
		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_RENEWAL);
		String content= mt.getContent();
		if(cp!=null){
			customerEmailContent.setContent(modifyContent(content, cp, renewalDto.getUserName()));
			customerEmailContent.setRecipientEmail(cp.getContactEmail());
			customerEmailContent.setRecipientCustomerId(cp.getCustomerId().toString());
		}else{
			customerEmailContent.setContent(content);
		}
		customerEmailContent.setSubject(mt.getMessageSubject());
		customerEmailContent.setSendDate(new Date());
		customerEmailContent.setSenderUserId(renewalDto.getCreateBy());
		customerEmailContent.setCopyto(null);
		customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_ENROLLMENT_SUCCESS);
		customerEmailContentDao.save(customerEmailContent);
		
		CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
		customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
		customerEmailAttach.setAttachmentName("invoice.pdf");
		customerEmailAttachDao.save(customerEmailAttach);
		
		
		List<String> fileNameList = new ArrayList<String>();
		fileNameList.add("invoice.pdf");
		List<String> mineTypeList = new ArrayList<String>();
		mineTypeList.add("application/pdf");
		mailThreadService.sendWithResponse(customerEmailContent, Arrays.asList(attchment), mineTypeList, fileNameList);
				
		return responseResult;
	}
	
	private String modifyContent(String content, CustomerProfile cp, String inscribe) {
		
		content = content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, cp.getGivenName() + " " + cp.getSurname());
		content = content.replace(Constant.PLACE_HOLDER_FROM_USER, inscribe);
		return content;
	}

	/**
	 * 
	 * @param customerId
	 * @param servicePlanNo
	 * @throws Exception
	 * 
	 * save  CustomerOrderHd and CustomerOrderDet
	 */

	private Map<String,String> startUpCustomerOrder(Long customerId,Long servicePlanNo,String createBy) throws Exception{

		/*
		 * these variable is not sure,then change later
		 *
		 */
//		String createBy = "Li_Chen"; //get this by session user
//		String userId = "ADMIN"; //get this by session user
		
		Map<String,String> resultMap = new HashMap<String,String>();
		//get the servicePlanPos
		List itemPrice = posItemPriceDao.getRenewItemPrice(servicePlanNo, ServicePlanOfferCode.RENEW.getDesc());
		
		if(null == itemPrice || itemPrice.size() == 0)
			return null;
		Object[] cOrder = (Object[])itemPrice.get(0);
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(new Date()); //today
		customerOrderHd.setOrderStatus(OrderStatus.OPN.getDesc()); //OPN
		/* get the sessionUser to do */
		customerOrderHd.setStaffUserId(createBy);
		customerOrderHd.setOrderTotalAmount((BigDecimal)cOrder[0]);
		/* get the sessionUser to do */
		customerOrderHd.setCreateBy(createBy);
		customerOrderHd.setCreateDate(new Timestamp(new Date().getTime()));
		customerOrderHd.setUpdateDate(new Date());
		customerOrderHd.setUpdateBy(createBy);
		customerOrderHd.setCustomerId(customerId);
		Long orderNo = (Long)customerOrderHdDao.save(customerOrderHd);
		
		CustomerOrderDet coDet = new CustomerOrderDet();
		coDet.setCustomerOrderHd(customerOrderHd);
		coDet.setItemNo(posServiceItemPriceDao.get(PosServiceItemPrice.class, (String)cOrder[1]).getItemNo());
		coDet.setOrderQty(1L);
		coDet.setItemTotalAmout((BigDecimal)cOrder[0]);
		coDet.setCreateBy(createBy);
		coDet.setCreateDate(new Timestamp(new Date().getTime()));
		coDet.setUpdateDate(new Date());
		coDet.setUpdateBy(createBy);
		Long coDetId = (Long)customerOrderDetDao.save(coDet);
		
		resultMap.put("orderNo", String.valueOf(orderNo));
		resultMap.put("coDetId", String.valueOf(coDetId));
		
		return resultMap;
	}
	
	
	@Transactional
	@Override
	public ListPage<ExpiredMemberDto> getExpiringMemberList(String searchText,Integer expiringDays,
			MemberExpiredOrderByCol orderByCol, String sortType,
			ListPage<ExpiredMemberDto> pListPage,String isMyClient,String userId) throws Exception {
		String sql = "SELECT\n" +
				"	m.user_id AS userId,\n" +
				"	nsp.orderNo AS orderNo,\n" +
				"	m.customer_id AS customerId,\n" +
				"	CONCAT(\n" +
				"		IFNULL(cp.salutation, ''),\n" +
				"		' ',\n" +
				"		IFNULL(cp.given_name, ''),\n" +
				"		' ',\n" +
				"		IFNULL(cp.surname, '')\n" +
				"	) customerName,\n" +
				"	cp.contact_email contactEmail,\n" +
				"	csp.expiryDate AS expiryDate,\n" +
				"	csp.cspname AS currentPlanName,\n" +
				"	nsp.nspname AS newPlanName\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			acc.customer_id ccid,\n" +
				"			sp.plan_name cspname,\n" +
				"			DATE_FORMAT(acc.expiry_date, '%Y/%m/%d') expiryDate\n" +
				"		FROM\n" +
				"			customer_service_acc acc,\n" +
				"			customer_service_subscribe cs,\n" +
				"			service_plan sp\n" +
				"		WHERE\n" +
				"			acc.acc_no = cs.acc_no\n" +
				"		AND cs.service_plan_no = sp.plan_no\n" +
				"		AND acc.`status` = 'ACT'\n" +
				"		AND TO_DAYS(acc.expiry_date) - TO_DAYS(NOW()) <= ? \n" +
				"		AND DATE_FORMAT(NOW(), '%Y-%m-%d') BETWEEN acc.effective_date\n" +
				"		AND acc.expiry_date\n" +
				"	) AS csp\n" +
				"JOIN member m ON csp.ccid = m.customer_id  \n" +
				"JOIN customer_profile cp ON m.customer_id = cp.customer_id\n" + " <<conditions>> " + 
			//add join on customer_enrollment e where e.customer_id = p.customer_id
				"\n <<isMyClient>> \n"+
				"LEFT JOIN (\n" +
				"	SELECT\n" +
				"		acc.customer_id ncid,\n" +
				"		sp.plan_name nspname,\n" +
				"		acc.order_no AS orderNo\n" +
				"	FROM\n" +
				"		customer_service_acc acc,\n" +
				"		customer_service_subscribe cs,\n" +
				"		customer_order_hd coh,\n" +
				"		customer_order_det cod,\n" +
				"		service_plan sp\n" +
				"	WHERE\n" +
				"		acc.acc_no = cs.acc_no\n" +
				"	AND coh.order_no = acc.order_no\n" +
				"	AND coh.order_no = cod.order_no\n" +
				"	AND cod.item_no LIKE 'RENEW%'\n" +
				"	AND coh.order_status != 'CAN'\n" +
				"	AND coh.order_status != 'REJ'\n" +
				"	AND cs.service_plan_no = sp.plan_no\n" +
				"	AND acc.`status` IN ('ACT', 'NACT')\n" +
				"	AND (\n" +
				"		TO_DAYS(acc.effective_date) > TO_DAYS(NOW())\n" +
				"		OR acc.effective_date IS NULL\n" +
				"	)\n" +
				") AS nsp ON csp.ccid = nsp.ncid WHERE  m.member_type NOT IN ('CPM', 'CDM')";
		
		List<Serializable> paramList = null;
		if(null != expiringDays){
			paramList = new ArrayList<Serializable>();
			paramList.add(expiringDays);
		}
		if(StringUtils.isNotEmpty(searchText)){
			sql = sql.replace("<<conditions>>" , " and ((cp.surname LIKE ? or cp.given_name LIKE ? or cp.company_name LIKE ?))");
			paramList.add("%"+searchText+"%");
			paramList.add("%"+searchText+"%");
			paramList.add("%"+searchText+"%");
		}else {
			sql = sql.replace("<<conditions>>" , "");
		}
		if(CommUtil.notEmpty(isMyClient)&&Boolean.parseBoolean(isMyClient)){
			sql=sql.replace("<<isMyClient>>","JOIN customer_enrollment cel ON cel.customer_id=cp.customer_id  and cel.sales_follow_by= ? ");
			paramList.add(userId);
		}else{
			sql = sql.replace("<<isMyClient>>" , "");
		}
		
		String sqlCount = "SELECT count(1) FROM ( "+sql + " ) x";
		
		

		if(null != orderByCol && null != pListPage && StringUtils.isNotEmpty(sortType))
			addSortConditon(pListPage, orderByCol, sortType);
		
		Map<String, org.hibernate.type.Type> map = new HashMap<String, org.hibernate.type.Type>();
		map.put("userId", StringType.INSTANCE);
		map.put("customerId", BigIntegerType.INSTANCE);
		map.put("customerName", StringType.INSTANCE);
		map.put("contactEmail", StringType.INSTANCE);
		map.put("expiryDate", StringType.INSTANCE);
		map.put("currentPlanName", StringType.INSTANCE);
		map.put("newPlanName", StringType.INSTANCE);
		map.put("orderNo", BigIntegerType.INSTANCE);
		return expireMemberDao.getExpiredMember(pListPage, sqlCount, sql, paramList, new ExpiredMemberDto(),map);
	}
	
	private String getOrderByColString(MemberExpiredOrderByCol orderByCol){
		String orderBy = "";
		if(null != orderByCol){
			switch (orderByCol) {
			case NAME:
				orderBy = "cp.surname, cp.given_name";
				break;
			case EXPIREDATE:
				orderBy = "csp.expiryDate";
				break;
			case CURRENTSERVICENAME:
				orderBy = "csp.cspname";
				break;
			case NEWSERVICENAME:
				orderBy = "nsp.nspname";
				break;
			}
		}
		return orderBy;
	}
	
	
	
	private void addSortConditon(ListPage<ExpiredMemberDto> pListPage,MemberExpiredOrderByCol orderByCol, String sortType){
		String orderBy = "";
		String orderBySurname = "";
		String orderByGivenName = "";
		switch (orderByCol){
		case NAME:
			orderBySurname = "cp.surname";
			orderByGivenName = "cp.given_name";
			if("desc".equals(sortType)){
				pListPage.addDescending(orderBySurname);
				pListPage.addDescending(orderByGivenName);
			}else if("asc".equals(sortType)){
				pListPage.addAscending(orderBySurname);
				pListPage.addAscending(orderByGivenName);
			}else{
				pListPage.addAscending("");
			}
			break;
		case EXPIREDATE:
			orderBy = "csp.expiryDate";
//			orderBy = "expiryDate";
			if("desc".equals(sortType)){
				pListPage.addDescending(orderBy);
			}else if("asc".equals(sortType)){
				pListPage.addAscending(orderBy);
			}else{
				pListPage.addAscending("");
			}
			break;
			
		case CURRENTSERVICENAME:
			orderBy = "csp.cspname";
			if("desc".equals(sortType)){
				pListPage.addDescending(orderBy);
			}else if("asc".equals(sortType)){
				pListPage.addAscending(orderBy);
			}else{
				pListPage.addAscending("");
			}
			break;
			
		case NEWSERVICENAME:
			orderBy = "nsp.nspname";
			if("desc".equals(sortType)){
				pListPage.addDescending(orderBy);
			}else if("asc".equals(sortType)){
				pListPage.addAscending(orderBy);
			}else{
				pListPage.addAscending("");
			}
			break;
		}
		
	}
	

	@Transactional
	@Override
	public ListPage<ExpiredMemberDto> getExpiredMemberList(String searchText, String expiredDays,
			MemberExpiredOrderByCol orderByCol, String sortType,
			ListPage<ExpiredMemberDto> pListPage,String isMyClient,String userId) throws Exception {
		// TODO Auto-generated method stub
		String sql = "SELECT\n" +
				" m.user_id AS userId,\n" +
				" nsp.orderNo AS orderNo,\n" +
				" csp.customerId AS customerId,\n" +
//				" csp.customerName AS customerName,\n" +
				"			CONCAT(\n" +
				"				IFNULL(cp.salutation, ''),\n" +
				"				' ',\n" +
				"				IFNULL(cp.given_name, ''),\n" +
				"				' ',\n" +
				"				IFNULL(cp.surname, '')\n" +
				"			) customerName,\n" +
				" cp.contact_email contactEmail,\n" +
				" csp.expiryDate AS expiryDate,\n" +
				" csp.cspname AS currentPlanName,\n" +
				" nsp.nspname AS newPlanName \n" +
				" FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			acc.customer_id ccid,"+
				"			acc.order_no AS orderNo,\n" +
				"			acc.customer_id AS customerId,\n" +
				"			acc. STATUS AS STATUS,\n" +
				"			acc.acc_no AS accNo,\n" +
				"			DATE_FORMAT(acc.expiry_date, '%Y/%m/%d') expiryDate,\n" +
				"			sp.plan_name AS cspname\n" +
				"		FROM\n" +
				"			customer_service_acc acc,\n" +
				"			customer_service_subscribe cs,\n" +
				"			service_plan sp \n" +
//				"			customer_profile cp,\n" +
//				"			member m ,\n" +
				//add join on customer_enrollment
//				" customer_enrollment cel "+
				"		WHERE\n" +
				"			acc.acc_no = cs.acc_no\n" +
				"		AND cs.service_plan_no = sp.plan_no \n" +
//				"		AND cp.customer_id = acc.customer_id\n" +
//				"		AND m.customer_id = cp.customer_id\n" +
//				"       <<conditions>> " +
//				"       AND  cel.customer_id=cp.customer_id <<isMyClient>> "+
				"	) csp\n" +
				" JOIN member m ON csp.ccid = m.customer_id \n"+
				" JOIN customer_profile cp ON m.customer_id = cp.customer_id  \n"+
				" <<conditions>> \n"+
				" <<isMyClient>> \n"+
				" JOIN (\n" +
				"	SELECT\n" +
				"		MAX(csa.acc_no) AS accNo\n" +
				"	FROM\n" +
				"		customer_service_acc csa,\n" +
				"		customer_order_hd hd\n" +
				"	WHERE\n" +
				"		csa.order_no = hd.order_no\n" +
				"AND (\n" +
				"	(\n" +
				"		TO_DAYS(csa.expiry_date) < TO_DAYS(NOW())\n" +
				"		AND csa.`status` = 'EXP'\n" +
				"	)\n" +
				"	OR (\n" +
				"		csa.`status` = 'ACT'\n" +
				"		AND TO_DAYS(NOW()) BETWEEN TO_DAYS(csa.effective_date)\n" +
				"		AND TO_DAYS(csa.expiry_date)\n" +
				"	)\n" +
				")" +
				"	AND hd.order_status = 'CMP'\n" +
				"	GROUP BY\n" +
				"		csa.customer_id\n" +
				") csap ON csap.accNo = csp.accNo\n" +
				"LEFT JOIN (\n" +
				"	SELECT\n" +
				"		acc.customer_id customerId,\n" +
				"		sp.plan_name nspname,\n" +
				"		acc.order_no AS orderNo\n" +
				"	FROM\n" +
				"		customer_service_acc acc,\n" +
				"		customer_service_subscribe cs,\n" +
				"		customer_order_hd coh,\n" +
				"		customer_order_det cod,\n" +
				"		service_plan sp\n" +
				"	WHERE\n" +
				"		acc.acc_no = cs.acc_no\n" +
				"	AND coh.order_no = acc.order_no\n" +
				"	AND coh.order_no = cod.order_no\n" +
				"	AND cod.item_no LIKE 'RENEW%'\n" +
				"	AND coh.order_status != 'CAN'\n" +
				"	AND coh.order_status != 'REJ'\n" +
				"	AND cs.service_plan_no = sp.plan_no\n" +
				"	AND acc.`status` IN ('ACT', 'NACT')\n" +
				"	AND (\n" +
				"		TO_DAYS(acc.effective_date) > TO_DAYS(NOW())\n" +
				"		OR acc.effective_date IS NULL\n" +
				"	)\n" +
				" ) AS nsp ON nsp.customerId = csp.customerId\n" +
				" WHERE\n" +
				" csp. STATUS = 'EXP'   AND m.member_type NOT IN ('CPM', 'CDM') \n" +
			    " AND TO_DAYS(NOW()) - TO_DAYS(csp.expiryDate) " + expiredDays + "\n";
				//+
				//" ORDER BY\n" +
				//" asp.customerId";	
		
		List<Serializable> paramList = new ArrayList<Serializable>();
		
		if(StringUtils.isNotEmpty(searchText)){
			sql = sql.replace("<<conditions>>" , " and ((cp.surname LIKE ? or cp.given_name LIKE ? or cp.company_name LIKE ?))");
			paramList.add("%"+searchText+"%");
			paramList.add("%"+searchText+"%");
			paramList.add("%"+searchText+"%");
		}else {
			sql = sql.replace("<<conditions>>" , "");
		}
		
		if(CommUtil.notEmpty(isMyClient)&&Boolean.parseBoolean(isMyClient)){
			sql=sql.replace("<<isMyClient>>"," JOIN customer_enrollment cel ON cel.customer_id=cp.customer_id  and cel.sales_follow_by= ? ");
			paramList.add(userId);
		}else{
			sql = sql.replace("<<isMyClient>>" , "");
		}
		
		String sqlCount = "SELECT count(1) FROM ( "+sql + " ) x";
		
		
		
		if(null != orderByCol && null != pListPage && StringUtils.isNotEmpty(sortType))
			addSortConditon(pListPage, orderByCol, sortType);
		
		Map<String, org.hibernate.type.Type> map = new HashMap<String, org.hibernate.type.Type>();
		map.put("userId", StringType.INSTANCE);
		map.put("customerId", BigIntegerType.INSTANCE);
		map.put("customerName", StringType.INSTANCE);
		map.put("contactEmail", StringType.INSTANCE);
		map.put("expiryDate", StringType.INSTANCE);
		map.put("currentPlanName", StringType.INSTANCE);
		map.put("newPlanName", StringType.INSTANCE);
		map.put("orderNo", BigIntegerType.INSTANCE);
		return expireMemberDao.getExpiredMember(pListPage, sqlCount, sql, paramList, new ExpiredMemberDto(),map);
	}
	
	@Override
	@Transactional
	public MemberInfoDto getMemberInfo(Long customerId) {
		MemberInfoDto dto = customerProfileDao.getCustomerInfo(customerId);
		return dto;
	}

	/*@Override
	@Transactional
	public ListPage<ExpiredMemberDto> getExpiredMemberList(String searchCondtion,
			MemberExpiredOrderByCol orderByCol, String sortType,
			ListPage<ExpiredMemberDto> pListPage) throws Exception {
		// TODO Auto-generated method stub
		
		StringBuffer hql = new StringBuffer();	//search data
		StringBuffer hqlCount = new StringBuffer();	//total count
		

//		hql.append(" SELECT m.user_id AS userId, m.customer_id AS customerId, cp.surname AS surName, cp.given_name AS givenName, cp.gender AS gender, csp.expiryDate AS expiryDate,")
//			.append(" csp.cspname AS currentPlanName, nsp.nspname AS newPlanName FROM ( SELECT acc.customer_id ccid, sp.plan_name cspname, DATE_FORMAT(acc.expiry_date, '%Y/%m/%d')")
//			.append(" expiryDate FROM customer_service_acc acc, customer_service_subscribe cs, service_plan sp WHERE acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no")
//			.append(" AND acc.`status` = 'ACT' AND DATE_FORMAT(NOW(), '%Y-%m-%d') BETWEEN acc.effective_date AND acc.expiry_date ) AS csp JOIN member m ON csp.ccid = m.customer_id")
//			.append(" JOIN customer_profile cp ON m.customer_id = cp.customer_id ")
//			.append(" <conditons> ").append(" LEFT JOIN ( SELECT acc.customer_id ncid, sp.plan_name nspname,")
//			.append(" MIN( DATE_FORMAT( acc.effective_date, '%Y/%m/%d' ) ) effectiveDate FROM customer_service_acc acc, customer_service_subscribe cs,") 
//			.append(" service_plan sp WHERE acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no AND acc.`status` IN ('ACT', 'NACT')")
//			.append("AND TO_DAYS(acc.effective_date) > TO_DAYS(NOW()) ) AS nsp ON m.customer_id = nsp.ncid");
		
		hql.append(" SELECT m.user_id AS userId, m.customer_id AS customerId, 	CONCAT(IFNULL(cp.salutation,''),' ',IFNULL(cp.given_name,''),' ',IFNULL(cp.surname,'')) customerName,")
		.append("cp.contact_email contactEmail, csp.expiryDate AS expiryDate, csp.cspname AS currentPlanName,nsp.nspname AS newPlanName")
		.append(" FROM ( SELECT acc.customer_id ccid, sp.plan_name cspname, DATE_FORMAT(acc.expiry_date, '%Y/%m/%d') expiryDate")
		.append("	FROM customer_service_acc acc, customer_service_subscribe cs, service_plan sp")
		.append("	WHERE acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no AND acc.`status` = 'ACT' AND TO_DAYS(acc.expiry_date) - TO_DAYS(NOW()) <= 90")
		.append("	AND DATE_FORMAT(NOW(), '%Y-%m-%d') BETWEEN acc.effective_date AND acc.expiry_date UNION SELECT acc.customer_id ccid,")
		.append("	sp.plan_name cspname, MAX( DATE_FORMAT(acc.expiry_date, '%Y/%m/%d') ) expiryDate")
		.append("	FROM customer_service_acc acc, customer_service_subscribe cs, service_plan sp WHERE")
		.append("	acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no AND acc.`status` = 'EXP' AND acc.customer_id NOT IN (")
		.append(" SELECT acc.customer_id ccid FROM customer_service_acc acc, customer_service_subscribe cs, service_plan sp")
		.append(" WHERE acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no AND acc.`status` = 'ACT' AND TO_DAYS(acc.expiry_date) - TO_DAYS(NOW()) <= 90")
		.append(" AND DATE_FORMAT(NOW(), '%Y-%m-%d') BETWEEN acc.effective_date AND acc.expiry_date ) GROUP BY")
		.append(" acc.customer_id HAVING TO_DAYS(expiryDate) < TO_DAYS(NOW()) ) AS csp")
		.append(" JOIN member m ON csp.ccid = m.customer_id JOIN customer_profile cp ON m.customer_id = cp.customer_id ").append("<conditons>")
		.append("	LEFT JOIN (SELECT DISTINCT hd.order_no AS orderNo, hd.customer_id customerId FROM customer_order_hd hd, customer_order_det det,customer_service_acc acc 	WHERE hd.order_no = det.order_no AND det.item_no LIKE 'RENEW%' "
				+ " AND hd.order_status != 'CAN' AND hd.order_status != 'REJ' and (TO_DAYS(acc.effective_date) > TO_DAYS(NOW()) OR acc.effective_date is NULL)) AS coh ON coh.customerId = csp.ccid ")
		.append("	LEFT JOIN ( SELECT	acc.customer_id ncid, sp.plan_name nspname, MIN( DATE_FORMAT( acc.effective_date, '%Y/%m/%d' )")
		.append(" ) effectiveDate FROM customer_service_acc acc, customer_service_subscribe cs, customer_order_hd coh, customer_order_det cod, ")
		.append(" service_plan sp WHERE acc.acc_no = cs.acc_no and coh.customer_id = acc.customer_id and coh.order_no = cod.order_no and cod.item_no LIKE 'RENEW%' and coh.order_status != 'CAN' AND coh.order_status != 'REJ' AND cs.service_plan_no = sp.plan_no")
		.append("	AND acc.`status` IN ('ACT', 'NACT') AND (TO_DAYS(acc.effective_date) > TO_DAYS(NOW()) or acc.effective_date is NULL) GROUP BY acc.customer_id , acc.effective_date) AS nsp ON m.customer_id = nsp.ncid");
		
		
		
		hqlCount.append("	SELECT count(1) ")
		.append(" FROM ( SELECT acc.customer_id ccid, sp.plan_name cspname, DATE_FORMAT(acc.expiry_date, '%Y/%m/%d') expiryDate")
		.append("	FROM customer_service_acc acc, customer_service_subscribe cs, service_plan sp")
		.append("	WHERE acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no AND acc.`status` = 'ACT' AND TO_DAYS(acc.expiry_date) - TO_DAYS(NOW()) <= 90")
		.append("	AND DATE_FORMAT(NOW(), '%Y-%m-%d') BETWEEN acc.effective_date AND acc.expiry_date UNION SELECT acc.customer_id ccid,")
		.append("	sp.plan_name cspname, MAX( DATE_FORMAT(acc.expiry_date, '%Y/%m/%d') ) expiryDate")
		.append("	FROM customer_service_acc acc, customer_service_subscribe cs, service_plan sp WHERE")
		.append("	acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no AND acc.`status` = 'EXP' AND acc.customer_id NOT IN (")
		.append(" SELECT acc.customer_id ccid FROM customer_service_acc acc, customer_service_subscribe cs, service_plan sp")
		.append(" WHERE acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no AND acc.`status` = 'ACT' AND TO_DAYS(acc.expiry_date) - TO_DAYS(NOW()) <= 90")
		.append(" AND DATE_FORMAT(NOW(), '%Y-%m-%d') BETWEEN acc.effective_date AND acc.expiry_date ) GROUP BY")
		.append(" acc.customer_id HAVING TO_DAYS(expiryDate) < TO_DAYS(NOW()) ) AS csp")
		.append(" JOIN member m ON csp.ccid = m.customer_id JOIN customer_profile cp ON m.customer_id = cp.customer_id ").append("<conditons>")
		.append("	LEFT JOIN (SELECT DISTINCT hd.order_no AS orderNo, hd.customer_id customerId FROM customer_order_hd hd, customer_order_det det,customer_service_acc acc 	WHERE hd.order_no = det.order_no AND det.item_no LIKE 'RENEW%' "
				+ "  AND hd.order_status != 'CAN' AND hd.order_status != 'REJ' and (TO_DAYS(acc.effective_date) > TO_DAYS(NOW()) OR acc.effective_date is NULL) ) AS coh ON coh.customerId = csp.ccid ")
		.append("	LEFT JOIN ( SELECT	acc.customer_id ncid, sp.plan_name nspname, MIN( DATE_FORMAT( acc.effective_date, '%Y/%m/%d' )")
		.append(" ) effectiveDate FROM customer_service_acc acc, customer_service_subscribe cs, customer_order_hd coh, customer_order_det cod, ")
		.append(" service_plan sp WHERE acc.acc_no = cs.acc_no and coh.customer_id = acc.customer_id and coh.order_no = cod.order_no and cod.item_no LIKE 'RENEW%' and coh.order_status != 'CAN' AND coh.order_status != 'REJ' AND cs.service_plan_no = sp.plan_no")
		.append("	AND acc.`status` IN ('ACT', 'NACT') AND (TO_DAYS(acc.effective_date) > TO_DAYS(NOW()) or acc.effective_date is NULL) GROUP BY acc.customer_id , acc.effective_date) AS nsp ON m.customer_id = nsp.ncid");
		
//		hqlCount.append(" SELECT count(1)")
//		.append(" FROM ( SELECT acc.customer_id ccid, sp.plan_name cspname, DATE_FORMAT(acc.expiry_date, '%Y/%m/%d')")
//		.append(" expiryDate FROM customer_service_acc acc, customer_service_subscribe cs, service_plan sp WHERE acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no")
//		.append(" AND acc.`status` = 'ACT' AND DATE_FORMAT(NOW(), '%Y-%m-%d') BETWEEN acc.effective_date AND acc.expiry_date ) AS csp JOIN member m ON csp.ccid = m.customer_id")
//		.append(" JOIN customer_profile cp ON m.customer_id = cp.customer_id ")
//		.append(" <conditons> ").append(" LEFT JOIN ( SELECT acc.customer_id ncid, sp.plan_name nspname,")
//		.append(" MIN( DATE_FORMAT( acc.effective_date, '%Y/%m/%d' ) ) effectiveDate FROM customer_service_acc acc, customer_service_subscribe cs,")
//		.append(" service_plan sp WHERE acc.acc_no = cs.acc_no AND cs.service_plan_no = sp.plan_no AND acc.`status` IN ('ACT', 'NACT')")
//		.append("AND TO_DAYS(acc.effective_date) > TO_DAYS(NOW()) ) AS nsp ON m.customer_id = nsp.ncid");
//	
//		if (presentName != null && !presentName.isEmpty()) {
//			hql.append(" and p.presentName like '%").append(presentName)
//					.append("%'");
//			hqlCount.append(" and p.presentName like '%").append(presentName)
//					.append("%'");
//		}
//		abcabcabc%'))
		
		StringBuffer conditons = new StringBuffer();
		
		if(StringUtils.isNotEmpty(searchCondtion)){
			conditons.append(" and ((cp.surname LIKE ? or cp.given_name LIKE ? or cp.company_name LIKE ?))");
		}
		
		String excutedSql = "";
		String excutedSqlCount = "";
		
		if(StringUtils.isNotEmpty(searchCondtion)){
			excutedSql = hql.toString().replaceAll("<conditons>", conditons.toString());
			excutedSqlCount = hqlCount.toString().replaceAll("<conditons>", conditons.toString());
		}else{
			excutedSql = hql.toString().replaceAll("<conditons>", "");
			excutedSqlCount = hqlCount.toString().replaceAll("<conditons>", "");
		}
		
	
//		String orderBy =  getOrderByColString(orderByCol);
//		
//		if(StringUtils.isNotEmpty(orderBy) && StringUtils.isNotEmpty(sortType)){
//			if("desc".equals(sortType)){
//				pListPage.addDescending(orderBy);
//			}else if("asc".equals(sortType)){
//				pListPage.addAscending(orderBy);
//			}else{
//				pListPage.addAscending("");
//			}
//		}
		
		if(null != orderByCol && null != pListPage && StringUtils.isNotEmpty(sortType))
			addSortConditon(pListPage, orderByCol, sortType);
		
		Map<String, org.hibernate.type.Type> map = new HashMap<String, org.hibernate.type.Type>();
		map.put("userId", StringType.INSTANCE);
		map.put("customerId", BigIntegerType.INSTANCE);
		map.put("customerName", StringType.INSTANCE);
		map.put("contactEmail", StringType.INSTANCE);
		map.put("expiryDate", StringType.INSTANCE);
		map.put("currentPlanName", StringType.INSTANCE);
		map.put("newPlanName", StringType.INSTANCE);
		map.put("orderNo", BigIntegerType.INSTANCE);
		return expireMemberDao.getExpiredMember(pListPage, excutedSqlCount, excutedSql, Arrays.asList(new String[] { "%" + searchCondtion + "%", "%" + searchCondtion + "%", "%" + searchCondtion + "%" }) , new ExpiredMemberDto(),map);
	}*/

	@Override
	@Transactional
	public ResponseResult sendRenewWithInvoiceEmail(RenewalEmailDto mailReq, String userId, String userName) {
		
		byte[] attchment = null;
		Long orderNo = mailReq.getOrderNo();
		if (null != orderNo) {
			try {
				attchment = customerOrderTransDao.getInvoiceReceipt(orderNo, null, "invocie");
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.debug("Generate invoice attachment failed!", e);
				responseResult.initResult(GTAError.EnrollmentAdvanceError.GENERATE_INVOICE_ATTACHMENT_FAIL);
				return responseResult;
			}	
		}else {
			responseResult.initResult(GTAError.EnrollmentAdvanceError.GENERATE_INVOICE_ATTACHMENT_FAIL);
			return responseResult;
		}
		CustomerEmailContent customerEmailContent = new CustomerEmailContent();
		//modified by Kaster 20160518 将RenewalEmailDto的customerId类型换成了Long。
		CustomerProfile cp = customerProfileDao.get(CustomerProfile.class, mailReq.getCustomerID());
//		MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(EmailType.INVOICE.getFunctionId());
//		String content= mt.getContent();
		String content= mailReq.getEmailBody();
		if(cp!=null){
			customerEmailContent.setContent(modifyContent(content, cp, userName));
			customerEmailContent.setRecipientEmail(cp.getContactEmail());   
			customerEmailContent.setRecipientCustomerId(cp.getCustomerId().toString());
			
		}else{
			customerEmailContent.setContent(content);
		}
		
		// TODO : SAMHUI commented on 20160323. This is a critical bug where cp is possible null will raise exception!! Plan to fix.
//		customerEmailContent.setSubject(mt.getMessageSubject());
		customerEmailContent.setSubject(mailReq.getEmailTitle() == null ? mailReq.getMessageSubject() : mailReq.getEmailTitle());
		
		customerEmailContent.setSendDate(new Date());
		customerEmailContent.setSenderUserId(userId);
		customerEmailContent.setCopyto(null);
		customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_ENROLLMENT_SUCCESS);
		customerEmailContentDao.save(customerEmailContent);
		
		CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
		customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
		customerEmailAttach.setAttachmentName("invoice.pdf");
		customerEmailAttachDao.save(customerEmailAttach);
		
		
		List<String> fileNameList = new ArrayList<String>();
		fileNameList.add("invoice.pdf");
		List<String> mineTypeList = new ArrayList<String>();
		mineTypeList.add("application/pdf");
		mailThreadService.sendWithResponse(customerEmailContent, Arrays.asList(attchment), mineTypeList, fileNameList);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Override
	@Transactional
	public boolean checkExistsByNewPlanStatus(Long customerId, String status) {
		// TODO Auto-generated method stub
		String sql = " select 1 from customer_service_acc acc where acc.customer_id = " + customerId + " "
				+ " and not EXISTS (select 1 from customer_service_acc acc1 where acc1.status='ACT' and acc1.customer_id=acc.customer_id) " 
				+ " and EXISTS (select 1 from customer_service_acc acc2 where acc2.status='EXP' and acc2.customer_id=acc.customer_id)  and acc.status='NACT' and acc.remark='RENEW' ";
		if("toPay".equals(status)){
			sql += " and acc.effective_date is null ";
		}else if("toActive".equals(status)){
			sql += " and date_format(now(), '%Y-%m-%d') < acc.effective_date ";
		}
		Object object = this.customerProfileDao.getUniqueBySQL(sql, null);
		if(object!=null){
			return true;
		}else{
			return false;
		}
	}
	
}