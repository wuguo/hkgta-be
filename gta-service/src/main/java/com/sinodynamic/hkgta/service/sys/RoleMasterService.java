package com.sinodynamic.hkgta.service.sys;

import java.math.BigInteger;
import java.util.List;

import com.sinodynamic.hkgta.dto.sys.RoleDto;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RoleMasterService extends IServiceBase<RoleMaster> {

	public List<RoleMaster> getgetRoleMasterList();

	public ResponseResult createRole(RoleMaster role);

	public ResponseResult updateRole(RoleMaster role);
	public boolean validateRole(Long roleId, String roleName);

	public ResponseResult deleteRole(BigInteger roleId);

	public ResponseResult changeStatus(RoleDto dto);
	
	public String getRoleList();
	
	public List<RoleMaster> getAllRoleList();
   
}
