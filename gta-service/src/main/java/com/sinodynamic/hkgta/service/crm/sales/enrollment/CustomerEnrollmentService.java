
package com.sinodynamic.hkgta.service.crm.sales.enrollment;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import net.sf.jasperreports.engine.JRException;

import com.sinodynamic.hkgta.dto.crm.ActivateMemberDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 
 * @author Mianping_Wu
 *
 */

public interface CustomerEnrollmentService extends IServiceBase<CustomerEnrollment>{
	
	/**
	 * This method is used to search all customer enrollment
	 * @return 
	 */
	public ListPage<CustomerEnrollment> searchAllCustomerEnrollments(ListPage<CustomerEnrollment> page);
	
	
	/**
	 * This method is used to search customer member brief info
	 */
	public ListPage<CustomerEnrollment> searchCustomerEnrollmentInfoByName(ListPage<CustomerEnrollment> page, String operType, String name);
	
	
	/**
	 * This function is used for checking the selected lucky number for academy Id is available or not and set it.
	 * The range of lucky number is 8000000-9999999.
	 */
	public boolean checkAvailableAcademyID(String academyId);
	
	/**
	 * @author Liky_Pan
	 * The method is used to reserve academy Id for future use.
	 * @param dto
	 * @return ResponseResult
	 */
	public ResponseResult reserveAcademId(MemberDto dto,String userId);
	
	/**
	 * @author Liky_Pan
	 * The method is used to remove the reserved academyNo
	 * @param customerId
	 * @return
	 */
	public ResponseResult removeReservedAcademyId(Long customerId);
	
	public boolean setAcademyID(String academyId,Long customerId);
	/**
	 * This function is used to search view history enrollments
	 */
	public ListPage<CustomerEnrollment> viewEnrollmentActivationHistory(ListPage<CustomerEnrollment> page,String type);
	
	
	/**
	 * Allow to manual active the member once all the details are included
	 */
	public ResponseResult activateMember(ActivateMemberDto activeMemberDto,String loginUserId,String loginUserName) throws Exception;
	
	
	/**
	 * Action to update status of member once the payment or card has been
	 * settled.(will be used in displaying the pending list and activated list)
	 */
	public ResponseMsg removeActivedMemberInPendingList(Long customerId);
	
	
	/**
	 * Action to check status of customer enrollment once the payment or card has been
	 * settled.(will be used in displaying the pending list and activated list)
	 */
//	public void deleteNewEnrollments(int enrollmentID);
	
	public void addCustomerEnrollment(CustomerEnrollment ce);

	public String checkEnrollmentStatus(Long enrollId);

	public boolean deleteEnrollmentsWithoutPayment(Long enrollId);

	public boolean checkTransactionsStatusAsSuccessful(Long enrollId);
	
	public boolean checkBalanceDueAsZero(Long enrollId);
	
	public int updateEnrollmentStatusToComplete(Long enrollId, String status,String userId);

	
	public void assignSaleperson(Long enrollmentId,String saleperson, String userId);
	
	public void updateEnrollmentStatus(Long enrollmentId,String status,String userId, String userName);

	/**
	 * filter the enrollment member with status
	 * @param status
	 * @return
	 */
	public ResponseResult viewEnrollmentRecords(CustomerEnrollmentDto dto,String userId,String device);


	public CustomerEnrollment getEnrollmentById(Long enrollId);
	
	public CustomerEnrollment getEnrollmentByCustomerId(Long customerId);
	
	public ResponseResult getMemberTypeByAcademyNo(String academyNo);
	
	public ResponseResult getDependentBySuperiorId(Long superiorCustomerId);
	
	public ResponseResult approveMember(Long customerId,String userId);
	
	public ResponseResult regEnrollment(CustomerProfile dto, String userId) throws Exception;
	
	public ResponseResult deleteDependentMemebr(Long customerId,String userId);
	
	public ResponseMsg editEnrollment(CustomerProfile dto, String userId,String fromName) throws Exception;


	public void updateEnrollmentStatusJob(Long enrollId, String status);
	
	public ResponseResult setActivationDateForMember(ActivateMemberDto activeMemberDto,String loginUserId,String userName) throws Exception;

	public ResponseResult getAllSalesmen();
	
	public List<CustomerEnrollment> getEnrollmentMembersByStatus(String status);
	
	/**
	 * Method used to record all the enrollment status change, saved to the table customer_enrollment_log
	 * @author Liky_Pan
	 * @param statusFrom
	 * @param statusTo
	 * @param customerId
	 * @return Primary key of customer_enrollment_log
	 */
	public Long recordCustomerEnrollmentUpdate(String statusFrom,String statusTo,Long customerId);

	public String getEnrollmentRecordsSQL(String offerCode);

	public ResponseResult encapsulateActivationMember(ActivateMemberDto activeMemberDto, String loginUserId,String loginUserName) throws Exception;

	public ResponseMsg updateRenewalStatus(Long orderNo, String status, String userId, String userName);

	public byte[] getDailyEnrollmentAttach(String enrollDate,String fileType,String sortBy,String isAscending) throws JRException;

	public ResponseResult getDailyEnrollment(ListPage page, String enrollDate);

	public ResponseResult getDailyLeads(ListPage page, String selectedDate);

	public byte[] getDailyLeadsAttach(String selectedDate, String fileType, String sortBy, String isAscending) throws JRException;
	/***
	 * send mail to sales  by userId
	 * @param userId
	 */
	public void sendMailToSalesByUserId(String userId,Long customerId);
	/***
	 * @param AccountantMail
	 * @param customerId
	 */
	public void sendMailToAccountantByCustomerId(Long customerId,BigDecimal amount);


	/**
	 * 获取CustomerTransfor列表语句
	 * @return
	 */
	public String getCustomerTransforList();


	/**
	 * 批量更新销售人员
	 * @param enrollmentIds
	 * @param salepersonId
	 * @param userId
	 */
	public void assignSaleperson(String enrollmentIds, String salepersonId, String userId, AdvanceQueryDto advanceQueryDto);


	public ResponseMsg editCorporateEnrollment(CustomerProfile cProfile, String userId, String userName) throws Exception;


	public void mainBodySeparate(CustomerProfile cp);


}