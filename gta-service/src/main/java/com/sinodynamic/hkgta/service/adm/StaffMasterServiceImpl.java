package com.sinodynamic.hkgta.service.adm;

import java.io.File;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import com.sinodynamic.hkgta.dao.adm.DepartmentStaffDao;
import com.sinodynamic.hkgta.dao.adm.PermitCardMasterDao;
import com.sinodynamic.hkgta.dao.adm.StaffCoachInfoDao;
import com.sinodynamic.hkgta.dao.adm.StaffMasterDao;
import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.InternalPermitCardDao;
import com.sinodynamic.hkgta.dao.crm.InternalPermitCardLogDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.StaffDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.crm.StaffCardMasterDto;
import com.sinodynamic.hkgta.dto.staff.RequestStaffDto;
import com.sinodynamic.hkgta.dto.staff.StaffCoachInfoDto;
import com.sinodynamic.hkgta.dto.staff.StaffListDto;
import com.sinodynamic.hkgta.dto.staff.StaffMasterDto;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.dto.staff.StaffPasswordDto;
import com.sinodynamic.hkgta.dto.staff.StaffProfileDto;
import com.sinodynamic.hkgta.dto.staff.UserDto;
import com.sinodynamic.hkgta.dto.staff.UserMasterDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.DepartmentStaff;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCardLog;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.StaffCoachInfo;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.sys.SysUserService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.FileUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PassportType;
import com.sinodynamic.hkgta.util.constant.PermitCardMasterEnumStatus;
import com.sinodynamic.hkgta.util.constant.StaffMasterOrderByCol;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class StaffMasterServiceImpl extends ServiceBase<StaffMasterInfoDto>implements StaffMasterService {

	@Autowired
	private StaffMasterInfoDtoDao staffMasterInfoDtoDao;
	@Autowired
	private StaffMasterDao staffMasterDao;
	@Autowired
	private StaffDao staffDao;

	@Autowired
	private DepartmentStaffDao departmentStaffDao;

	@Autowired
	private UserMasterDao userMasterDao;

	@Autowired
	private StaffProfileDao staffProfileDao;

	@Autowired
	private StaffCoachInfoDao staffCoachInfoDao;

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;

	@Autowired
	private  MessageTemplateDao messageTemplateDao;

	@Autowired
	private SysCodeDao sysCodeDao;
	
	@Autowired
	private InternalPermitCardDao internalPermitCardDao;
	
	@Autowired
	private InternalPermitCardLogDao internalPermitCardLogDao;
	
	@Autowired
	private PermitCardMasterDao permitCardMasterDao;

	private Logger logger = Logger.getLogger(StaffMasterServiceImpl.class);
	
	private Logger staffEmailLogger = Logger.getLogger(LoggerType.STAFFEMAIL.getName());
	
	@Autowired
	SysUserService sysUserService;

	@Override
	@Transactional
	public ListPage<StaffMasterInfoDto> getStaffMasterList(ListPage<StaffMasterInfoDto> pListPage,
			StaffMasterOrderByCol orderCol, String sortType, StaffListDto dto) throws Exception {

		StringBuffer hql = new StringBuffer();
		StringBuffer conditionHql = new StringBuffer();
		hql.append("SELECT sub.*").append(", sum(CASE pos.rate_type WHEN 'HI' THEN price.item_price END) 'highPrice'")
				.append(", sum(CASE pos.rate_type WHEN 'LO' THEN price.item_price END) 'lowPrice'").append("FROM (")
				.append("SELECT u.user_id userId").append(",s.create_date createDate")
				.append(",p.contact_email contactEmail").append(",concat( p.given_name,' ', p.surname) staffName")
				.append(",s.staff_no staffNo").append(",s.staff_type staffType").append(",s.`status` `status`")
				.append(",s.position_title_code positionTitle").append(",position.position_name positionTitleName")
				.append(",DATE_FORMAT(s.employ_date, '%Y/%m/%d') employDate")
				.append(",DATE_FORMAT(s.quit_date, '%Y/%m/%d') quitDate")
				.append(",count(booking.exp_coach_user_id) bookings").append(",sc.code_display as staffTypeDisplay")
				.append(" FROM user_master AS u  INNER JOIN staff_master AS s  INNER JOIN  staff_profile AS p ")
				.append("  LEFT JOIN sys_code sc ON  sc.code_value = s.staff_type AND ( sc.category = 'HK-CREW-ROLE' OR sc.category = 'staffType') ")
				.append(" LEFT JOIN member_facility_type_booking AS booking ON booking.exp_coach_user_id = p.user_id")
				.append(" LEFT JOIN position_title AS position ON position.position_code = s.position_title_code")
				.append(" WHERE u.user_id = s.user_id").append(" AND s.user_id = p.user_id")
				.append(" AND u.user_type = 'STAFF'").append(" [replace_condition]").append(" GROUP BY p.user_id")
				.append(" ) sub").append(" LEFT JOIN staff_coach_rate_pos AS pos ON sub.userId = pos.user_id")
				.append(" LEFT JOIN pos_service_item_price AS price ON pos.pos_item_no = price.item_no")
				.append(" GROUP BY sub.userId");

		StringBuffer countHql = new StringBuffer(" 		SELECT count(1)")
				.append(" 	 FROM user_master AS u, staff_master AS s,")
				.append("	staff_profile AS p WHERE u.user_id = s.user_id AND s.user_id = p.user_id AND u.user_type = 'STAFF'");

		// AND p.contact_email LIKE '%HKGTA

		if (null != dto && StringUtils.isNotEmpty(dto.getEmail())) {
			conditionHql.append(" AND p.contact_email LIKE '%").append(dto.getEmail()).append("%' ");
			countHql.append(" AND p.contact_email LIKE '%").append(dto.getEmail()).append("%' ");
		}

		// AND s.staff_no = 'Ray'
		if (null != dto && StringUtils.isNotEmpty(dto.getStaffNo())) {
			conditionHql.append("AND s.staff_no = '").append(dto.getStaffNo()).append("'");
			countHql.append("AND s.staff_no = '").append(dto.getStaffNo()).append("'");
		}

		if (null != dto && StringUtils.isNotEmpty(dto.getStaffType()) && !"ALL".equalsIgnoreCase(dto.getStaffType())) {
			conditionHql.append("AND s.staff_type = '").append(dto.getStaffType()).append("'");
			countHql.append("AND s.staff_type = '").append(dto.getStaffType()).append("'");
		}

		if (null != dto && StringUtils.isNotEmpty(dto.getStatus())) {
			conditionHql.append("AND s.`status` = '").append(dto.getStatus()).append("'");
			countHql.append("AND s.`status` = '").append(dto.getStatus()).append("'");
		}

		if (null != dto && StringUtils.isNotEmpty(dto.getPositionTitle())) {
			conditionHql.append("AND s.position_title_code = '").append(dto.getPositionTitle()).append("'");
			countHql.append("AND s.position_title_code = '").append(dto.getPositionTitle()).append("'");
		}

		if (null != dto && StringUtils.isNotEmpty(dto.getEmployeeBeginDate())) {
			conditionHql.append("AND s.employ_date >= '").append(dto.getEmployeeBeginDate()).append("'");
			countHql.append("AND s.employ_date >= '").append(dto.getEmployeeBeginDate()).append("'");
		}

		if (null != dto && StringUtils.isNotEmpty(dto.getEmployeeEndDate())) {
			conditionHql.append("AND s.employ_date <= '").append(dto.getEmployeeEndDate()).append("'");
			countHql.append("AND s.employ_date <= '").append(dto.getEmployeeEndDate()).append("'");
		}

		if (null != dto && StringUtils.isNotEmpty(dto.getQuitBeginDate())) {
			conditionHql.append("AND s.quit_date >= '").append(dto.getQuitBeginDate()).append("'");
			countHql.append("AND s.quit_date >= '").append(dto.getQuitBeginDate()).append("'");
		}

		if (null != dto && StringUtils.isNotEmpty(dto.getEmployeeEndDate())) {
			conditionHql.append("AND s.quit_date <= '").append(dto.getEmployeeEndDate()).append("'");
			countHql.append("AND s.quit_date <= '").append(dto.getEmployeeEndDate()).append("'");
		}

		// add order condition
		addSortConditon(pListPage, orderCol, sortType);

		String queryHql = hql.toString();
		queryHql = queryHql.replace("[replace_condition]",
				StringUtils.isEmpty(conditionHql.toString()) ? "" : conditionHql.toString());
		ListPage<StaffMasterInfoDto> staffList = staffMasterInfoDtoDao.getStaffMasterList(pListPage,
				countHql.toString(), queryHql);

		for (Object staff : staffList.getDtoList()) {
			((StaffMasterInfoDto) staff).setEmployDate(DateConvertUtil.getYMDDateAndDateDiff(
					DateConvertUtil.parseString2Date(((StaffMasterInfoDto) staff).getEmployDate(), "yyyy/MM/dd")));
			((StaffMasterInfoDto) staff).setQuitDate(DateConvertUtil.getYMDDateAndDateDiff(
					DateConvertUtil.parseString2Date(((StaffMasterInfoDto) staff).getQuitDate(), "yyyy/MM/dd")));
		}

		return staffList;
	}

	/**
	 * @author Junfeng_Yan
	 * @param orderByCol
	 * @return order String param
	 */
	private String getOrderByColString(StaffMasterOrderByCol orderByCol) {
		String orderBy = "";
		if (null == orderByCol)
			return orderBy;
		switch (orderByCol) {
		case STAFFNAME:
			orderBy = "staffName";
			break;

		case STAFFID:
			orderBy = "userId";
			break;

		case STAFFTYPE:
			orderBy = "staffType";
			break;

		case STATUS:
			orderBy = "`status`";
			break;

		case POSITIONTITLE:
			orderBy = "positionTitle";
			break;

		case EMPLOYEEDATE:
			orderBy = "employDate";
			break;

		case QUITDATE:
			orderBy = "quitDate";
			break;

		case BOOKINGS:
			orderBy = "bookings";
			break;

		case HIGHPRICE:
			orderBy = "highPrice";
			break;

		case LOWPRICE:
			orderBy = "lowPrice";
			break;

		default:
			orderBy = "";
			break;
		}
		return orderBy;
	}

	/**
	 * @author Junfeng_Yan
	 * @since may 28 2015
	 * @param pListPage
	 * @param orderByCol
	 * @param sortType
	 * 
	 */
	private void addSortConditon(ListPage<StaffMasterInfoDto> pListPage, StaffMasterOrderByCol orderByCol,
			String sortType) {
		String orderBy = getOrderByColString(orderByCol);

		if (StringUtils.isEmpty(sortType) || StringUtils.isEmpty(orderBy)) {
			orderBy = "sub.createDate";
			pListPage.addDescending(orderBy);
		} else {
			if ("asc".equals(sortType)) {
				pListPage.addAscending(orderBy);
			} else {
				pListPage.addDescending(orderBy);
			}
		}
	}

	@Transactional
	public ResponseResult getStaffMasterByUserId(String userId) throws Exception {

		if (StringUtils.isEmpty(userId))
			return new ResponseResult("1", "parameter userId is null!");
		// StaffMaster staffMaster = staffDao.getStaffMasterByUserId(userId);
		StaffMasterInfoDto dto = staffDao.getStaffMasterDtoByUserId(userId);

		UserMaster userMaster = userMasterDao.getUserByUserId(userId);

		StaffProfile staffProfile = staffProfileDao.getStaffProfileByUserId(userId);

		StaffCoachInfo coachInfo = staffCoachInfoDao.getByUserId(userId);

		if (null == dto)
			return new ResponseResult("1", "staffMaster with userId: " + userId + " is not exist!");

		if (null == userMaster)
			return new ResponseResult("1", "userMaster with userId: " + userId + " is not exist!");

		if (null == staffProfile)
			return new ResponseResult("1", "staffProfile with userId: " + userId + " is not exist!");
		Map dataMap = new HashMap();

		dto.setEmployDate(DateConvertUtil.date2String(dto.getColEmployDate(), "yyyy-MMM-dd"));
		dto.setQuitDate(DateConvertUtil.date2String(dto.getColQuitDate(), "yyyy-MMM-dd"));

		UserMasterDto userMasterDto = new UserMasterDto();
		BeanUtils.copyProperties(userMaster, userMasterDto, new String[] { "password" });

		StaffProfileDto staffProfileDto = new StaffProfileDto();
		BeanUtils.copyProperties(staffProfile, staffProfileDto, new String[] { "dateOfBirth" });
		staffProfileDto.setDateOfBirth(DateConvertUtil.date2String(staffProfile.getDateOfBirth(), "yyyy-MMM-dd"));

		// String employDate = dto.getEmployDate();
		// String quitDate = dto.getQuitDate();
		//
		// if(StringUtils.isEmpty(employDate)){
		// dto.setEmployDate("");
		// }else{
		// Date edate = DateConvertUtil.parseString2Date(employDate,
		// "yyyy/MM/dd");
		// String enoOfDays = DateConvertUtil.getNoOfDays(edate);
		// dto.setEmployDate(employDate+" "+ enoOfDays);
		// }
		//
		// if(StringUtils.isEmpty(quitDate)){
		// dto.setQuitDate(quitDate);
		// }else{
		// Date qdate = DateConvertUtil.parseString2Date(quitDate,
		// "yyyy/MM/dd");
		// String qnoOfDays = DateConvertUtil.getNoOfDays(qdate);
		// dto.setQuitDate(quitDate+" "+ qnoOfDays);
		// }

		SysCode code = sysCodeDao.getByCategoryAndCodeValue("nationality", staffProfile.getNationality());
		dataMap.put("staffMaster", dto);
		dataMap.put("userMaster", userMasterDto);
		dataMap.put("staffProfile", staffProfileDto);
		dataMap.put("nationalityName", code.getCodeDisplay());
		if (isCoach(dto.getStaffType()) && coachInfo != null) {
			StaffCoachInfoDto coachInfoDto = new StaffCoachInfoDto();
			BeanUtils.copyProperties(coachInfo, coachInfoDto);
			dataMap.put("staffCoachInfo", coachInfoDto);
		}

		return new ResponseResult("0", "", dataMap);
	}

	@Transactional
	public ResponseResult getStaffMasterOnlyByUserId(String userId) {

		if (StringUtils.isEmpty(userId))
			return new ResponseResult("1", "parameter userId is null!");
		StaffMasterInfoDto dto = staffDao.getStaffMasterDtoByUserId(userId);

		if (null == dto)
			return new ResponseResult("1", "staffMaster with userId: " + userId + " is not exist!");

		Map<String, Object> dataMap = new HashMap<String, Object>();

		dto.setEmployDate(DateConvertUtil.date2String(dto.getColEmployDate(), "yyyy-MMM-dd"));
		dto.setQuitDate(DateConvertUtil.date2String(dto.getColQuitDate(), "yyyy-MMM-dd"));

		dataMap.put("staffMaster", dto);

		responseResult.initResult(GTAError.Success.SUCCESS, dataMap);
		return responseResult;
	}

	@Deprecated
	@Override
	@Transactional
	public ResponseMsg saveStaffMaster(RequestStaffDto staffDto, String createBy) throws Exception {
		// TODO Auto-generated method stub
		logger.debug("save staffMaster now!");
		if (null == staffDto || null == staffDto.getStaffProfile() || null == staffDto.getStaffMaster()) {
			return new ResponseMsg("1", "Please input correct parameters json format!");
		}
		ResponseMsg msg = checkInputData(staffDto);
		if (!"0".equals(msg.getReturnCode()))
			return msg;
		// save userMaster
		UserMaster um = createUserMaster(staffDto, createBy);
		userMasterDao.saveUserMaster(um);
		// save staffMaster
		staffDao.addStaffMaster(setStaffMaster(staffDto, createBy));
		// save staffStaffProfile
		StaffProfile sp = setStaffProfile(staffDto, createBy);
		String userId = (String) staffProfileDao.save(sp);

		StaffProfile profile = staffProfileDao.getStaffProfileByUserId(userId);

		if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(profile.getPortraitPhoto())) {
			String path = moveProfilePhoto(userId, profile.getPortraitPhoto());
			if (StringUtils.isEmpty(path)) {
				throw new RuntimeException("move profilephoto failed!");
			} else {
				profile.setPortraitPhoto(path);
				staffProfileDao.updateStaffProfile(profile);
			}
		}

		// save coach info start
		if (isCoach(staffDto.getStaffMaster().getStaffType())) {
			StaffCoachInfo coachInfo = staffDto.getStaffCoachInfo();
			if (null == coachInfo) {
				throw new RuntimeException(
						"coach information is missing, please provide the coach information while you create a coach!");
			}
			coachInfo.setUserId(userId);
			saveCoachInfo(coachInfo, createBy);
		}
		// save coach info end

		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_ACTSTAFF);
		CustomerEmailContent email = setCustomerEmailContent(template, profile, um, "");

		if (null != email) {
			boolean result = MailSender.sendEmail(email.getRecipientEmail(), null, null, email.getSubject(),
					email.getContent(), null);
			if (result)
				email.setStatus(EmailStatus.SENT.getName());
			else
				email.setStatus(EmailStatus.FAIL.getName());
//			customerEmailContentDao.addCustomerEmail(email);
			staffEmailLogger.info(Log4jFormatUtil.logStaffEmailInfo(result, DateCalcUtil.formatDatetime(email.getSendDate()), email.getSenderUserId(), email.getRecipientCustomerId(),
					email.getRecipientEmail(), email.getNoticeType(), email.getSubject()));
		}

		return new ResponseMsg("0", "");
	}

	@Deprecated
	@Override
	@Transactional
	public ResponseMsg saveStaffMasterOld(UserDto staffDto, String createBy) throws Exception {
		// TODO Auto-generated method stub
		logger.debug("save staffMaster now!");
		if (null == staffDto || null == staffDto.getStaffProfile() || null == staffDto.getStaffMaster()) {
			responseMsg.initResult(GTAError.StaffMgrError.INVALID_JSON_DATA);
			return responseMsg;
		}
		ResponseMsg msg = checkInputData(staffDto);
		if (!"0".equals(msg.getReturnCode()))
			return msg;
		// save userMaster
		UserMaster um = createUserMaster(staffDto, createBy);
		userMasterDao.saveUserMaster(um);
		// save staffMaster
		staffDao.addStaffMaster(setStaffMaster(staffDto, createBy));
		// save staffStaffProfile
		StaffProfile sp = setStaffProfile(staffDto, createBy);
		String userId = (String) staffProfileDao.save(sp);

		StaffProfile profile = staffProfileDao.getStaffProfileByUserId(userId);

		if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(profile.getPortraitPhoto())) {
			String path = moveProfilePhoto(userId, profile.getPortraitPhoto());
			if (StringUtils.isEmpty(path)) {
				throw new RuntimeException("move profilephoto failed!");
			} else {
				profile.setPortraitPhoto(path);
				staffProfileDao.updateStaffProfile(profile);
			}
		}

		// save coach info start
		if (isCoach(staffDto.getStaffMaster().getStaffType())) {
			StaffCoachInfoDto coachInfo = staffDto.getStaffCoachInfo();
			if (null == coachInfo) {
				throw new RuntimeException(
						"coach information is missing, please provide the coach information while you create a coach!");
			}
			coachInfo.setUserId(userId);
			saveCoachInfo(coachInfo, createBy);
		}
		// save coach info end

		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_ACTSTAFF);
		CustomerEmailContent email = setCustomerEmailContent(template, profile, um, "");

		if (null != email) {
			boolean result = MailSender.sendEmail(email.getRecipientEmail(), null, null, email.getSubject(),
					email.getContent(), null);
			if (result)
				email.setStatus(EmailStatus.SENT.getName());
			else
				email.setStatus(EmailStatus.FAIL.getName());
//			customerEmailContentDao.addCustomerEmail(email);
			staffEmailLogger.info(Log4jFormatUtil.logStaffEmailInfo(result, DateCalcUtil.formatDatetime(email.getSendDate()), email.getSenderUserId(), email.getRecipientCustomerId(),
					email.getRecipientEmail(), email.getNoticeType(), email.getSubject()));
		}

		responseMsg.initResult(GTAError.StaffMgrError.SAVE_OR_UPDATE_SUCC, new String[] { "Save staff information" });
		return responseMsg;
	}

	@Override
	@Transactional
	public ResponseMsg saveStaffMaster(UserDto staffDto, String createBy) throws Exception {
		// TODO Auto-generated method stub
		logger.debug("save staffMaster now!");
		if (null == staffDto || null == staffDto.getStaffProfile() || null == staffDto.getStaffMaster()) {
			responseMsg.initResult(GTAError.StaffMgrError.INVALID_JSON_DATA);
			return responseMsg;
		}
		ResponseMsg msg = checkInputData(staffDto);
		if (!"0".equals(msg.getReturnCode()))
			return msg;
		// save userMaster
		UserMaster um = createUserMaster(staffDto, createBy);
		userMasterDao.saveUserMaster(um);
		// save staffMaster
		staffDao.addStaffMaster(setStaffMaster(staffDto, createBy));
		departmentStaffDao.save(setStaffDepartment(staffDto, createBy));
		// save staffStaffProfile
		StaffProfile sp = setStaffProfile(staffDto, createBy);
		String userId = (String) staffProfileDao.save(sp);

		StaffProfile profile = staffProfileDao.getStaffProfileByUserId(userId);

		if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(profile.getPortraitPhoto())) {
			String path = moveProfilePhoto(userId, profile.getPortraitPhoto());
			if (StringUtils.isEmpty(path)) {
				throw new RuntimeException("move profilephoto failed!");
			} else {
				profile.setPortraitPhoto(path);
				staffProfileDao.updateStaffProfile(profile);
			}
		}

		// save coach info start
		if (isCoach(staffDto.getStaffMaster().getStaffType())) {
			StaffCoachInfoDto coachInfo = staffDto.getStaffCoachInfo();
			coachInfo.setUserId(userId);
			saveCoachInfo(coachInfo, createBy);
		}
		// save coach info end
		responseMsg.initResult(GTAError.StaffMgrError.SAVE_OR_UPDATE_SUCCESS, new String[] { userId });
		return responseMsg;
	}

	@Override
	@Transactional
	public void sendEmail2Staff(String userId, UserDto staffDto) throws Exception {
		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_ACTSTAFF);
		StaffProfile profile = staffProfileDao.getStaffProfileByUserId(userId);
		UserMaster um = userMasterDao.get(UserMaster.class, userId);
		CustomerEmailContent email = setCustomerEmailContent(template, profile, um, staffDto.getRandomPsw());

		if (null != email) {
			boolean result = MailSender.sendEmail(email.getRecipientEmail(), null, null, email.getSubject(),
					email.getContent(), null);
			if (result)
				email.setStatus(EmailStatus.SENT.getName());
			else
				email.setStatus(EmailStatus.FAIL.getName());
//			customerEmailContentDao.addCustomerEmail(email);
			staffEmailLogger.info(Log4jFormatUtil.logStaffEmailInfo(result, DateCalcUtil.formatDatetime(email.getSendDate()), email.getSenderUserId(), email.getRecipientCustomerId(),
					email.getRecipientEmail(), email.getNoticeType(), email.getSubject()));
		}

	}

	private boolean isCoach(String staffType) {
		if (!StringUtils.isEmpty(staffType) && (staffType.equalsIgnoreCase(Constant.StaffType.FTG.toString())
				|| staffType.equalsIgnoreCase(Constant.StaffType.FTR.toString()))) {
			return true;
		}
		return false;
	}

	private void saveCoachInfo(StaffCoachInfo coachInfoDto, String userId) {
		StaffCoachInfo coachinfo = new StaffCoachInfo();
		coachinfo.setCreateBy(userId);
		coachinfo.setCreateDate(new Date());
		coachinfo.setPersonalInfo(coachInfoDto.getPersonalInfo());
		coachinfo.setSpeciality(coachInfoDto.getSpeciality());
		coachinfo.setUserId(coachInfoDto.getUserId());
		staffCoachInfoDao.save(coachinfo);
		// TODO Auto-generated method stub

	}

	private void saveCoachInfo(StaffCoachInfoDto coachInfoDto, String userId) {
		StaffCoachInfo coachinfo = new StaffCoachInfo();
		coachinfo.setCreateBy(userId);
		coachinfo.setCreateDate(new Date());
		coachinfo.setPersonalInfo(coachInfoDto.getPersonalInfo());
		coachinfo.setSpeciality(coachInfoDto.getSpeciality());
		coachinfo.setUserId(coachInfoDto.getUserId());
		staffCoachInfoDao.save(coachinfo);
	}

	private CustomerEmailContent setCustomerEmailContent(MessageTemplate template, StaffProfile profile, UserMaster um,
			String randomPsw) {
		if (null != template) {
			CustomerEmailContent email = new CustomerEmailContent();
			email.setRecipientCustomerId(profile.getUserId());
			email.setRecipientEmail(profile.getContactEmail());
			email.setSubject(template.getMessageSubject());
			email.setNoticeType(Constant.NOTICE_TYPE_SERVICE_PLAN_TRANSACTION);
			// email.setContent(template.getContent().replace("{fullName}",
			// profile.getGivenName() + " " +
			// profile.getSurname()).replace("{loginUrl}",
			// this.appProps.getProperty("domain") +
			// this.appProps.getProperty("portal.login.page")).replace("{account}",
			// um.getLoginId()).replace("{password}", randomPsw));
			email.setContent(template.getFullContentHtml(profile.getGivenName() + " " + profile.getSurname(),
					um.getLoginId(), randomPsw,
					this.appProps.getProperty("domain") + this.appProps.getProperty("portal.login.page")));
			email.setSendDate(new Date());
			return email;
		}
		return null;
	}

	private String moveProfilePhoto(String userId, String filename) {
		String basePath = "";
		try {
			basePath = FileUpload.getBasePath(FileUpload.FileCategory.USER);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File photoFile = new File(basePath + File.separator + userId);
		if (!photoFile.exists()) {
			boolean isCreated = photoFile.mkdirs();
			if (!isCreated) {
				logger.error("Can't create folder!");
			}
		}
		boolean isSuccess = FileUtil.moveFile(basePath + filename, basePath + File.separator + userId + filename);
		if (isSuccess) {
			return "/" + userId + filename;
		} else {
			return "";
		}
	}

	@Deprecated
	@Override
	@Transactional
	public ResponseMsg updateStaffMasterOld(RequestStaffDto staffDto, String updateBy) throws Exception {
		// TODO Auto-generated method stub
		logger.debug("update staffMaster now!");

		if (null == staffDto || null == staffDto.getStaffProfile() || null == staffDto.getStaffMaster()) {
			return new ResponseMsg("1", "Please input correct parameters json format!");
		}

		if (null == staffDto.getStaffProfile() || StringUtils.isEmpty(staffDto.getStaffProfile().getUserId()))
			return new ResponseMsg("1", "StaffProfile's userId is null!");

		if (null == staffDto.getStaffMaster() || StringUtils.isEmpty(staffDto.getStaffMaster().getUserId()))
			return new ResponseMsg("1", "StaffMaster's userId is null!");

		ResponseMsg msg = checkInputData(staffDto);

		if (!"0".equals(msg.getReturnCode()))
			return msg;

		StaffMaster staff = staffDto.getStaffMaster();
		StaffMaster originStaff = staffDao.getStaffMasterByUserId(staff.getUserId());
		staff.setCreateDate(originStaff.getCreateDate());
		staff.setUpdateBy(updateBy);
		staff.setUpdateDate(new Date());
		staffDao.getCurrentSession().evict(originStaff);

		boolean updateStaff = staffDao.update(staff);

		StaffProfile staffProfile = staffDto.getStaffProfile();
		staffProfile.setUpdateBy(updateBy);
		staffProfile.setUpdateDate(new Date());

		StaffProfile oldProfile = staffProfileDao.getStaffProfileByUserId(staffProfile.getUserId());

		if (!StringUtils.isEmpty(staffProfile.getPortraitPhoto())
				&& !staffProfile.getPortraitPhoto().equals(oldProfile.getPortraitPhoto())) {
			String path = moveProfilePhoto(staffProfile.getUserId(), staffProfile.getPortraitPhoto());
			if (StringUtils.isEmpty(path)) {
				throw new RuntimeException("move profilephoto failed!");
			} else {
				staffProfile.setPortraitPhoto(path);
			}
		}
		staffProfileDao.getCurrentSession().evict(oldProfile);
		staffProfileDao.update(staffProfile);

		if (isCoach(staff.getStaffType())) {
			StaffCoachInfo coachInfo = staffDto.getStaffCoachInfo();
			if (null == coachInfo) {
				throw new RuntimeException("Coach Information is empty, please provide the information.");
			}
			coachInfo.setCreateDate(originStaff.getCreateDate());
			coachInfo.setCreateBy(originStaff.getCreateBy());
			coachInfo.setUserId(staff.getUserId());
			coachInfo.setUpdateBy(updateBy);
			coachInfo.setUpdateDate(new Date());
			staffCoachInfoDao.update(coachInfo);
		}

		responseMsg.initResult(GTAError.StaffMgrError.SAVE_OR_UPDATE_SUCC, new String[] { "Update staff information" });
		return responseMsg;
	}

	@Deprecated
	@Override
	@Transactional
	public ResponseMsg updateStaffMaster(UserDto staffDto, String updateBy) throws Exception {
		// TODO Auto-generated method stub
		logger.debug("update staffMaster now!");

		if (null == staffDto || null == staffDto.getStaffProfile() || null == staffDto.getStaffMaster()) {
			responseMsg.initResult(GTAError.StaffMgrError.INVALID_JSON_DATA);
			return responseMsg;
		}

		if (null == staffDto.getStaffProfile() || StringUtils.isEmpty(staffDto.getStaffProfile().getUserId()))
			return new ResponseMsg("1", "StaffProfile's userId is null!");

		if (null == staffDto.getStaffMaster() || StringUtils.isEmpty(staffDto.getStaffMaster().getUserId()))
			return new ResponseMsg("1", "StaffMaster's userId is null!");

		responseMsg = checkInputData(staffDto);

		if (!"0".equals(responseMsg.getReturnCode()))
			return responseMsg;

		StaffMasterDto staff = staffDto.getStaffMaster();
		StaffMaster originStaff = staffDao.getStaffMasterByUserId(staff.getUserId());

		BeanUtils.copyProperties(staff, originStaff, new String[] { "quitDate", "employDate", "status" });
		originStaff.setEmployDate(DateConvertUtil.parseString2Date(staff.getEmployDate(), "yyyy-MM-dd"));
		originStaff.setQuitDate(DateConvertUtil.parseString2Date(staff.getQuitDate(), "yyyy-MM-dd"));

		// staff.setCreateDate(originStaff.getCreateDate());
		originStaff.setUpdateBy(updateBy);
		originStaff.setUpdateDate(new Date());
		// staffDao.getCurrentSession().evict(originStaff);

		// boolean updateStaff = staffDao.update(staff);

		StaffProfileDto staffProfileDto = staffDto.getStaffProfile();
		staffProfileDto.setUpdateBy(updateBy);
		// staffProfileDto.setUpdateDate(new Date());

		StaffProfile oldProfile = staffProfileDao.getStaffProfileByUserId(staffProfileDto.getUserId());

		if (!StringUtils.isEmpty(staffProfileDto.getPortraitPhoto())
				&& !staffProfileDto.getPortraitPhoto().equals(oldProfile.getPortraitPhoto())) {
			String path = moveProfilePhoto(staffProfileDto.getUserId(), staffProfileDto.getPortraitPhoto());
			if (StringUtils.isEmpty(path)) {
				throw new RuntimeException("move profilephoto failed!");
			} else {
				staffProfileDto.setPortraitPhoto(path);
			}
		}

		BeanUtils.copyProperties(staffProfileDto, oldProfile, new String[] { "dateOfBirth" });
		oldProfile.setDateOfBirth(DateConvertUtil.parseString2Date(staffProfileDto.getDateOfBirth(), "yyyy-MM-dd"));

		// staffProfileDao.getCurrentSession().evict(oldProfile);
		// staffProfileDao.update(staffProfileDto);
		//
		if (isCoach(staff.getStaffType())) {
			StaffCoachInfoDto coachInfo = staffDto.getStaffCoachInfo();
			if (null == coachInfo) {
				throw new RuntimeException("Coach Information is empty, please provide the information.");
			}
			StaffCoachInfo coach = staffCoachInfoDao.getByUserId(staff.getUserId());

			coach.setUpdateBy(updateBy);
			coach.setUpdateDate(new Date());
			coach.setPersonalInfo(coachInfo.getPersonalInfo());
			coach.setSpeciality(coachInfo.getSpeciality());
			// coachInfo.setCreateDate(originStaff.getCreateDate());
			// coachInfo.setCreateBy(originStaff.getCreateBy());
			// coachInfo.setUserId(staff.getUserId());
			// coachInfo.setUpdateBy(updateBy);
			// coachInfo.setUpdateDate(new Date());
			// staffCoachInfoDao.update(coach);
		}
		responseMsg.initResult(GTAError.StaffMgrError.SAVE_OR_UPDATE_SUCC, new String[] { "Update staff information" });
		return responseMsg;
	}

	@Override
	@Transactional
	public ResponseMsg updateStaff(UserDto staffDto, String updateBy) throws Exception {
		// TODO Auto-generated method stub
		logger.debug("update staffMaster now!");

		if (null == staffDto || null == staffDto.getStaffProfile() || null == staffDto.getStaffMaster()) {
			responseMsg.initResult(GTAError.StaffMgrError.INVALID_JSON_DATA);
			return responseMsg;
		}

		if (null == staffDto.getStaffProfile() || StringUtils.isEmpty(staffDto.getStaffProfile().getUserId()))
			return new ResponseMsg("1", "StaffProfile's userId is null!");

		if (null == staffDto.getStaffMaster() || StringUtils.isEmpty(staffDto.getStaffMaster().getUserId()))
			return new ResponseMsg("1", "StaffMaster's userId is null!");

		responseMsg = checkInputData(staffDto);

		if (!"0".equals(responseMsg.getReturnCode()))
			return responseMsg;

		StaffMasterDto staff = staffDto.getStaffMaster();
		StaffMaster originStaff = staffDao.getStaffMasterByUserId(staff.getUserId());
		
		if(StringUtils.isEmpty(staff.getPositionTitle())){
			BeanUtils.copyProperties(staff, originStaff, new String[] { "quitDate", "employDate", "status", "positionTitle" });
		}else{
			BeanUtils.copyProperties(staff, originStaff, new String[] { "quitDate", "employDate", "status" });
		}
		
		originStaff.setEmployDate(DateConvertUtil.parseString2Date(staff.getEmployDate(), "yyyy-MM-dd"));
		originStaff.setQuitDate(DateConvertUtil.parseString2Date(staff.getQuitDate(), "yyyy-MM-dd"));

		// staff.setCreateDate(originStaff.getCreateDate());
		originStaff.setUpdateBy(updateBy);
		originStaff.setUpdateDate(new Date());
		// staffDao.getCurrentSession().evict(originStaff);

		// boolean updateStaff = staffDao.update(staff);

		DepartmentStaff depart = departmentStaffDao.getDepart(staff.getUserId());
		if (null == depart) {
			departmentStaffDao.save(setStaffDepartment(staffDto, updateBy));
		} else {
			depart.setDepartId(staff.getDepartId());
			depart.setUpdateBy(updateBy);
			depart.setUpdateDate(new Date());
		}
		StaffProfileDto staffProfileDto = staffDto.getStaffProfile();
		staffProfileDto.setUpdateBy(updateBy);
		// staffProfileDto.setUpdateDate(new Date());

		StaffProfile oldProfile = staffProfileDao.getStaffProfileByUserId(staffProfileDto.getUserId());

		if (staffDto.getLicencePlateNo() != null && !staffDto.getLicencePlateNo().equals("")) {
			if (oldProfile != null) {
				oldProfile.setLicencePlateNo(staffDto.getLicencePlateNo());
			}
		}
		if (!StringUtils.isEmpty(staffProfileDto.getPortraitPhoto())
				&& !staffProfileDto.getPortraitPhoto().equals(oldProfile.getPortraitPhoto())) {
			String path = moveProfilePhoto(staffProfileDto.getUserId(), staffProfileDto.getPortraitPhoto());
			if (StringUtils.isEmpty(path)) {
				throw new RuntimeException("move profilephoto failed!");
			} else {
				staffProfileDto.setPortraitPhoto(path);
			}
		}
		

		BeanUtils.copyProperties(staffProfileDto, oldProfile, new String[] { "dateOfBirth" });
		oldProfile.setDateOfBirth(DateConvertUtil.parseString2Date(staffProfileDto.getDateOfBirth(), "yyyy-MM-dd"));

		// staffProfileDao.getCurrentSession().evict(oldProfile);
		// staffProfileDao.update(staffProfileDto);
		//
		if (isCoach(staff.getStaffType())) {
			StaffCoachInfoDto coachDto = staffDto.getStaffCoachInfo();
			coachDto.setUserId(staffDto.getStaffMaster().getUserId());
			saveCoachDescription(coachDto, updateBy);
		}
		
		responseMsg.initResult(GTAError.StaffMgrError.SAVE_OR_UPDATE_SUCC, new String[] { "Update staff information" });
		return responseMsg;
	}

	@Override
	@Transactional
	public ResponseMsg changeStaffMasterStatus(RequestStaffDto staffDto, String updateBy) throws Exception {
		// TODO Auto-generated method stub
		logger.debug("changeStaffMasterStatus staffMaster now!");
		if (null == staffDto || null == staffDto.getStaffMaster()
				|| StringUtils.isEmpty(staffDto.getStaffMaster().getStatus())) {
			return new ResponseMsg("1", "Please input correct parameters json format!");
		}

		if (null == staffDto.getStaffMaster() || StringUtils.isEmpty(staffDto.getStaffMaster().getUserId()))
			return new ResponseMsg("1", "StaffProfile's userId is null!");

		StaffMaster staff = staffDao.getStaffMasterByUserId(staffDto.getStaffMaster().getUserId());

		staff.setStatus(staffDto.getStaffMaster().getStatus().toUpperCase());
		boolean updateStatus = staffDao.update(staff);

		UserMaster user = userMasterDao.getUserByUserId(staffDto.getStaffMaster().getUserId());

		user.setStatus(staffDto.getStaffMaster().getStatus());

		boolean updateUserStatus = userMasterDao.update(user);

		if (updateStatus && updateUserStatus)
			return new ResponseMsg("0", "change status successful!");
		else
			return new ResponseMsg("1", "change status failed!");
	}

	private StaffProfile setStaffProfile(RequestStaffDto staffDto, String createBy) {

		StaffProfile staffProfile = staffDto.getStaffProfile();
		StaffMaster staff = staffDto.getStaffMaster();
		staffProfile.setUserId("[" + staff.getStaffNo() + "]");

		staffProfile.setCreateDate(new Timestamp(System.currentTimeMillis()));
		staffProfile.setCreateBy(createBy);
		staffProfile.setUpdateDate(new Date());
		staffProfile.setUpdateBy(createBy);

		return staffProfile;
	}

	private StaffProfile setStaffProfile(UserDto staffDto, String createBy) {
		StaffProfileDto staffProfileDto = staffDto.getStaffProfile();
		String email = staffProfileDto.getContactEmail();
		Boolean emailHasbeenUsed = staffProfileDao.isEmailUsed(email);
		if (emailHasbeenUsed) {
			throw new GTACommonException(GTAError.StaffMgrError.EMAIL_HAS_BEEN_USED, new String[] { email });
		}
		StaffMasterDto staff = staffDto.getStaffMaster();
		staffProfileDto.setUserId("[" + staff.getStaffNo() + "]");

		staffProfileDto.setCreateBy(createBy);
		staffProfileDto.setUpdateBy(createBy);

		StaffProfile staffProfile = new StaffProfile();
		BeanUtils.copyProperties(staffProfileDto, staffProfile, new String[] { "dateOfBirth" });
		staffProfile.setCreateDate(new Date());
		staffProfile.setUpdateDate(new Date());
		staffProfile.setLicencePlateNo(staffDto.getLicencePlateNo());
		staffProfile.setDateOfBirth(DateConvertUtil.parseString2Date(staffProfileDto.getDateOfBirth(), "yyyy-MM-dd"));
		return staffProfile;
	}

	private StaffMaster setStaffMaster(RequestStaffDto staffDto, String createBy) {
		StaffMaster staff = staffDto.getStaffMaster();
		staff.setUserId("[" + staff.getStaffNo() + "]");
		staff.setStatus("ACT");

		staff.setCreateDate(new Timestamp(System.currentTimeMillis()));
		staff.setCreateBy(createBy);
		staff.setUpdateDate(new Date());
		staff.setUpdateBy(createBy);

		return staff;

	}

	private StaffMaster setStaffMaster(UserDto staffDto, String createBy) {
		StaffMasterDto staffMasterDto = staffDto.getStaffMaster();
		staffMasterDto.setUserId("[" + staffMasterDto.getStaffNo() + "]");
		staffMasterDto.setStatus("ACT");
		staffMasterDto.setCreateBy(createBy);
		staffMasterDto.setUpdateBy(createBy);
		StaffMaster staffMaster = new StaffMaster();
		BeanUtils.copyProperties(staffMasterDto, staffMaster, new String[] { "employDate", "quitDate" });

		staffMaster.setEmployDate(DateConvertUtil.parseString2Date(staffMasterDto.getEmployDate(), "yyyy-MM-dd"));
		staffMaster.setQuitDate(DateConvertUtil.parseString2Date(staffMasterDto.getQuitDate(), "yyyy-MM-dd"));
		staffMaster.setCreateDate(new Timestamp(System.currentTimeMillis()));
		staffMaster.setUpdateDate(new Date());
		return staffMaster;

	}

	private DepartmentStaff setStaffDepartment(UserDto staffDto, String createBy) {
		StaffMasterDto staffMasterDto = staffDto.getStaffMaster();
		DepartmentStaff depart = new DepartmentStaff();
		depart.setCreateBy(createBy);
		depart.setUpdateBy(createBy);
		Date now = new Date();
		depart.setUpdateDate(now);
		depart.setCreateDate(now);
		depart.setDepartId(staffMasterDto.getDepartId());
		depart.setStaffUserId(staffMasterDto.getUserId());
		return depart;
	}

	private UserMaster createUserMaster(RequestStaffDto staffDto, String createBy) {

		UserMaster userMaster = new UserMaster();
		StaffMaster staff = staffDto.getStaffMaster();
		StaffProfile staffProfile = staffDto.getStaffProfile();

		userMaster.setUserId("[" + staff.getStaffNo() + "]");
		userMaster.setLoginId(staff.getStaffNo());
		userMaster.setNickname(staffProfile.getGivenName() + "_" + staffProfile.getSurname());

		userMaster.setPassword(DigestUtils.md5DigestAsHex(("123456" + staff.getStaffNo()).getBytes()));
		userMaster.setUserType("STAFF");
		userMaster.setDefaultModule("CRM");
		userMaster.setStatus("ACT");

		userMaster.setCreateDate(new Date());
		userMaster.setCreateBy(createBy);
		userMaster.setUpdateDate(new Date());
		userMaster.setUpdateBy(createBy);
		userMaster.setPasswdChangeDate(new Date());
		return userMaster;
	}

	private UserMaster createUserMaster(UserDto staffDto, String createBy) {

		UserMaster userMaster = new UserMaster();
		StaffMasterDto staff = staffDto.getStaffMaster();
		StaffProfileDto staffProfile = staffDto.getStaffProfile();

		userMaster.setUserId("[" + staff.getStaffNo() + "]");
		userMaster.setLoginId(staff.getStaffNo());
		userMaster.setNickname(staffProfile.getGivenName() + " " + staffProfile.getSurname());
		String randomPassword = CommUtil.generateRandomPassword();
		userMaster.setPassword(CommUtil.getMD5Password(staff.getStaffNo().toLowerCase(), randomPassword));
		staffDto.setRandomPsw(randomPassword);
		userMaster.setUserType("STAFF");
		userMaster.setDefaultModule("CRM");
		userMaster.setStatus("ACT");

		userMaster.setCreateDate(new Date());
		userMaster.setCreateBy(createBy);
		userMaster.setUpdateDate(new Date());
		userMaster.setUpdateBy(createBy);
		// userMaster.setPasswdChangeDate(new Date());
		return userMaster;
	}

	/**
	 * validate data for add and edit enrollment
	 * 
	 * @param dto
	 * @return
	 */
	@Deprecated
	private ResponseMsg checkInputData(RequestStaffDto staff) {

		StaffMaster m = staff.getStaffMaster();

		if (null == m || null == m.getEmployDate())
			return new ResponseMsg("1", "staffMaster's employDate is required!");

		if (null == m || null == m.getStaffNo())
			return new ResponseMsg("1", "staffno is required!");

		StaffProfile p = staff.getStaffProfile();

		StringBuilder ms = new StringBuilder();
		if (StringUtils.isEmpty(p.getPassportType())) {
			ms.append("Passport Type, ");
		}
		if (StringUtils.isEmpty(p.getPassportNo())) {
			ms.append("Passport No, ");
		}
		// if(StringUtils.isEmpty(p.getSurname())){
		// ms.append("Surname, ");
		// }
		// if(StringUtils.isEmpty(p.getGivenName())){
		// ms.append("Given Name, ");
		// }

		// if(StringUtils.isEmpty(p.getGender())){
		// ms.append("Gender, ");
		// }
		if (StringUtils.isEmpty(p.getPhoneMobile())) {
			ms.append("Phone Mobile, ");
		}

		if (StringUtils.isEmpty(p.getPhoneHome())) {
			ms.append("Phone Home, ");
		}
		if (StringUtils.isEmpty(p.getContactEmail())) {
			ms.append("Contact Email, ");
		}
		if (StringUtils.isEmpty(p.getPostalAddress1())) {
			ms.append("Postal Address1, ");
		}

//		if (StringUtils.isEmpty(p.getPostalAddress2())) {
//			ms.append("Postal Address2, ");
//		}

		if (null == p.getDateOfBirth()) {
			ms.append("DateOfBirth, ");
		}
		if (StringUtils.isEmpty(p.getNationality())) {
			ms.append("Nationality, ");
		}

		// if(StringUtils.isEmpty(p.getPostalArea())){
		// ms.append("Postal Area, ");
		// }

		if (StringUtils.isEmpty(p.getPostalDistrict())) {
			ms.append("Postal District, ");
		}

		if (ms.length() > 0) {
			return new ResponseMsg("1", ms.toString() + " are required parameters!");
		}

		if (!PassportType.HKID.name().equals(p.getPassportType())
				&& !PassportType.VISA.name().equals(p.getPassportType())) {
			return new ResponseMsg("1", "Passport Type incorrect, please input HKID or VISA! ");
		}

		if (PassportType.HKID.name().equals(p.getPassportType()) && !CommUtil.validateHKID(p.getPassportNo())) {
			return new ResponseMsg("1", "Passport Number: Invalid!,please input a new Passport Number!)");
		}
		if (PassportType.VISA.name().equals(p.getPassportType()) && !CommUtil.validateVISA(p.getPassportNo())) {
			return new ResponseMsg("1", "VISA Number: Invalid!,Please input a new VISA Number!");
		}
		if (!CommUtil.validateEmail(p.getContactEmail())) {
			return new ResponseMsg("1", "Email Address  : Invalid!,please input a new Email address!");
		}

		return new ResponseMsg("0", "");
	}

	private ResponseMsg checkInputData(UserDto staff) {

		StaffMasterDto m = staff.getStaffMaster();

		if (null == m || null == m.getEmployDate() || StringUtils.isEmpty(m.getEmployDate())) {
			responseMsg.initResult(GTAError.StaffMgrError.EMPLOY_DATE_REQUIRE);
			return responseMsg;
		}

		if (null == m || null == m.getStaffNo()) {
			responseMsg.initResult(GTAError.StaffMgrError.STAFF_NO_REQUIRE);
			return responseMsg;
		}

		if (null == m.getDepartId()) {
			responseMsg.initResult(GTAError.StaffMgrError.DEPART_ID_REQUIRE);
			return responseMsg;
		}

		StaffProfileDto p = staff.getStaffProfile();

		StringBuilder ms = new StringBuilder();
		if (StringUtils.isEmpty(p.getPassportType())) {
			ms.append("Passport Type, ");
		}
		if (StringUtils.isEmpty(p.getPassportNo())) {
			ms.append("Passport No, ");
		}
		// if(StringUtils.isEmpty(p.getSurname())){
		// ms.append("Surname, ");
		// }
		// if(StringUtils.isEmpty(p.getGivenName())){
		// ms.append("Given Name, ");
		// }

		// if(StringUtils.isEmpty(p.getGender())){
		// ms.append("Gender, ");
		// }
		if (StringUtils.isEmpty(p.getPhoneMobile())) {
			ms.append("Phone Mobile, ");
		}

		if (StringUtils.isEmpty(p.getPhoneHome())) {
			ms.append("Phone Home, ");
		}
		if (StringUtils.isEmpty(p.getContactEmail())) {
			ms.append("Contact Email, ");
		}
		if (StringUtils.isEmpty(p.getPostalAddress1())) {
			ms.append("Postal Address1, ");
		}

//		if (StringUtils.isEmpty(p.getPostalAddress2())) {
//			ms.append("Postal Address2, ");
//		}

		if (StringUtils.isEmpty(p.getDateOfBirth())) {
			ms.append("DateOfBirth, ");
		}
		if (StringUtils.isEmpty(p.getNationality())) {
			ms.append("Nationality, ");
		}

		// if(StringUtils.isEmpty(p.getPostalArea())){
		// ms.append("Postal Area, ");
		// }

		if (StringUtils.isEmpty(p.getPostalDistrict())) {
			ms.append("Postal District, ");
		}

		if (ms.length() > 0) {
			responseMsg.initResult(GTAError.StaffMgrError.DATA_MISSING, new String[] { ms.toString() });
			return responseMsg;
		}

		if (!PassportType.HKID.name().equals(p.getPassportType())
				&& !PassportType.VISA.name().equals(p.getPassportType())) {
			responseMsg.initResult(GTAError.StaffMgrError.PASSPORT_TYPE_INCORRECT);
			return responseMsg;
		}

		if (PassportType.HKID.name().equals(p.getPassportType()) && !CommUtil.validateHKID(p.getPassportNo())) {
			responseMsg.initResult(GTAError.StaffMgrError.DATA_INCORRECT, new String[] { "HKID" });
			return responseMsg;
		}
		if (PassportType.VISA.name().equals(p.getPassportType()) && !CommUtil.validateVISA(p.getPassportNo())) {
			responseMsg.initResult(GTAError.StaffMgrError.DATA_INCORRECT, new String[] { "Passport number" });
			return responseMsg;
		}
		if (!CommUtil.validateEmail(p.getContactEmail())) {
			responseMsg.initResult(GTAError.StaffMgrError.DATA_INCORRECT, new String[] { "Email" });
			return responseMsg;
		}

		responseMsg.initResult(GTAError.Success.SUCCESS);
		return responseMsg;
	}

	@Override
	@Transactional
	public ResponseMsg updateProfilePhoto(StaffProfile p) throws Exception {
		// TODO Auto-generated method stub

		if (null == p || StringUtils.isEmpty(p.getUserId()) || StringUtils.isEmpty(p.getPortraitPhoto())) {
			responseMsg.initResult(GTAError.StaffMgrError.DATA_MISSING, new String[] { "userId, portraitPhoto" });
			return responseMsg;
		}

		StaffProfile staffProfile = staffProfileDao.getStaffProfileByUserId(p.getUserId());
		staffProfile.setPortraitPhoto(p.getPortraitPhoto());

		boolean result = staffProfileDao.update(staffProfile);

		if (result) {
			responseMsg.initResult(GTAError.StaffMgrError.SAVE_OR_UPDATE_SUCC, new String[] { "UPDATE PortraitPhoto" });
			return responseMsg;
		} else {
			responseMsg.initResult(GTAError.StaffMgrError.SAVE_OR_UPDATE_FAILED,
					new String[] { "UPDATE PortraitPhoto" });
			return responseMsg;
		}

	}

	@Override
	@Transactional
	public ResponseResult staffNoIsUsed(String staffNo) throws Exception {
		// TODO Auto-generated method stub
		if (StringUtils.isEmpty(staffNo)) {
			return new ResponseResult("1", "staffNo is null!");
		}
		Boolean result = staffDao.isStaffNoUsed(staffNo);
		Map<String, Boolean> resultMap = new HashMap<String, Boolean>();
		resultMap.put("isExists", result);
		responseResult.initResult(GTAError.Success.SUCCESS, resultMap);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult emailIsUsed(String email) throws Exception {
		// TODO Auto-generated method stub
		if (StringUtils.isEmpty(email)) {
			return new ResponseResult("1", "staffNo is null!");
		}
		Boolean result = staffProfileDao.isEmailUsed(email);
		Map<String, Boolean> resultMap = new HashMap<String, Boolean>();
		resultMap.put("isExists", result);
		responseResult.initResult(GTAError.Success.SUCCESS, resultMap);
		return responseResult;
	}

	@Override
	@Transactional
	public void changeExpiredStaffMasterStatus() throws Exception {
		// TODO Auto-generated method stub
		List<StaffMaster> expiredActStaffMasterList = staffDao.getEexpiredActStaffMaster();
		if (null == expiredActStaffMasterList || expiredActStaffMasterList.size() == 0)
			return;
		for (StaffMaster s : expiredActStaffMasterList) {
			s.setStatus("NACT");
			staffDao.update(s);
			UserMaster user = userMasterDao.getUserByUserId(s.getUserId());
			user.setStatus("NACT");
			userMasterDao.update(user);
		}
	}

	@Override
	@Transactional
	public ResponseResult changePsw(StaffPasswordDto staffPsw) throws Exception {
		UserMaster user = userMasterDao.getUserByUserId(staffPsw.getUserId());

		if (null == user) {
			throw new GTACommonException(GTAError.StaffMgrError.STAFF_NOT_FOUND);
		}

		String originPsw = CommUtil.getMD5Password(user.getLoginId(), staffPsw.getOldPsw());

		if (!staffPsw.getNewPsw().equals(staffPsw.getRepeatPsw())) {
			throw new GTACommonException(GTAError.StaffMgrError.PSW_NOT_SAME);
		}

//		if (!CommUtil.validatePassword(staffPsw.getNewPsw())) {
//			throw new GTACommonException(GTAError.StaffMgrError.PSW_NOT_VALID);
//		}

		if (!originPsw.equals(user.getPassword())) {
			throw new GTACommonException(GTAError.StaffMgrError.PASSWORD_INCORRECT);
		}
		
		responseResult = sysUserService.checkPasswordLegal(staffPsw.getNewPsw());
		if(!GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())){
			return responseResult;
		}

		String psw = CommUtil.getMD5Password(user.getLoginId(), staffPsw.getNewPsw());

		if (psw.equals(user.getPassword())) {
			throw new GTACommonException(GTAError.StaffMgrError.NEWPSW_CANNOT_SAMEAS_OLDPSW);
		}

		user.setPassword(psw);
		user.setPasswdChangeDate(new Date());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult validatePassport(String loginId, String passportType, String passportNo) throws Exception {
		UserMaster user = userMasterDao.getUserByLoginId(loginId);
		if (null == user) {
			throw new GTACommonException(GTAError.StaffMgrError.STAFF_NOT_FOUND);
		}

		StaffProfile staff = staffProfileDao.getByUserId(user.getUserId());

		if (null == staff) {
			throw new GTACommonException(GTAError.StaffMgrError.STAFF_NOT_FOUND);
		}

		if (!(staff.getPassportType().equals(passportType) && staff.getPassportNo().equals(passportNo))) {
			throw new GTACommonException(GTAError.StaffMgrError.DATA_INCORRECT, new String[] { "Passport No/HKID" });
		}

		Map<String, String> resultMap = new HashMap<String, String>();
		resultMap.put("userId", user.getUserId());
		responseResult.initResult(GTAError.Success.SUCCESS, resultMap);
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseMsg saveCoachDescription(StaffCoachInfoDto coachDto, String createBy) throws Exception {
		StaffMaster staffMaster = staffDao.getStaffMasterByUserId(coachDto.getUserId());
		if (null == staffMaster) {
			responseResult.initResult(GTAError.StaffMgrError.STAFF_NOT_FOUND);
			return responseResult;
		}

		if (!isCoach(staffMaster.getStaffType())) {
			responseResult.initResult(GTAError.StaffMgrError.STAFF_TYPE_INVALID);
			return responseResult;
		}

		StaffCoachInfo coach = staffCoachInfoDao.getByUserId(coachDto.getUserId());

		if (null == coach) {
			StaffCoachInfo coachinfo = new StaffCoachInfo();
			coachinfo.setCreateBy(createBy);
			coachinfo.setCreateDate(new Date());
			coachinfo.setPersonalInfo(coachDto.getPersonalInfo());
			coachinfo.setSpeciality(coachDto.getSpeciality());
			coachinfo.setUserId(coachDto.getUserId());
			staffCoachInfoDao.save(coachinfo);
		} else {
			coach.setUpdateBy(createBy);
			coach.setUpdateDate(new Date());
			coach.setPersonalInfo(coachDto.getPersonalInfo());
			coach.setSpeciality(coachDto.getSpeciality());
		}

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@Override
	@Transactional
	public StaffMaster getStaffMaster(String userId) {
		return staffDao.get(StaffMaster.class, userId);
	}

	@Override
	@Transactional
	public ResponseResult getCoachInfoByUserId(String userId) {
		StaffCoachInfo coachInfo = staffCoachInfoDao.getByUserId(userId);
		if (null == coachInfo) {
			//modified by Kaster 20160510 将抛出异常统一规范化
//			responseResult.initResult(GTAError.CommonError.Error, "There is no coach info for the user : " + userId);
			responseResult.initResult(GTAError.CoachMgrError.COACH_NOT_FOUND_FOR_USER_ID,userId);
			return responseResult;
		}
		StaffCoachInfoDto coachInfoDto = new StaffCoachInfoDto();
		BeanUtils.copyProperties(coachInfo, coachInfoDto);
		responseResult.initResult(GTAError.Success.SUCCESS, coachInfoDto);
		return responseResult;
	}

	@Override
	public String getStaffs(AdvanceQueryDto advanceQueryDto) {
		StringBuffer sql = new StringBuffer();
		List<SearchRuleDto> rules=advanceQueryDto.getRules();
		boolean searchRule=false;
		for(SearchRuleDto rule:rules){
			if("eq".equals(rule.getOp())&&"positionTitle".equals(rule.getField())){
				searchRule=true;
			}
		}
		
		sql.append("SELECT * FROM (").append("SELECT u.user_id userId ").append(",s.create_date createDate ")
				.append(",p.contact_email contactEmail ").append(",concat( p.given_name,' ', p.surname) staffName ")
				.append(",s.staff_no staffNo ").append(",s.staff_type staffType ").append(",s.`status` `status` ");
				if(searchRule){
					sql.append(",position.position_code as positionTitle ").append(",position.position_name positionTitleName ");	
				}else{
					sql.append(",position.position_name as positionTitle ").append(",position.position_name positionTitleName ");	
				}
		sql.append(",DATE_FORMAT(s.employ_date, '%Y-%m-%d') employDate ")
				.append(",DATE_FORMAT(s.quit_date, '%Y-%m-%d') quitDate ")
				.append(",sc.code_display as staffTypeDisplay ")
				.append("FROM user_master AS u  INNER JOIN staff_master AS s  INNER JOIN  staff_profile AS p ")
				.append("LEFT JOIN sys_code sc ON  sc.code_value = s.staff_type AND ( sc.category = 'HK-CREW-ROLE' OR sc.category = 'staffType') ")
				.append("LEFT JOIN position_title AS position ON position.position_code = s.position_title_code ")
				.append("WHERE u.user_id = s.user_id ").append("AND s.user_id = p.user_id ")
				//add or u.user_type='admin' 
				.append("AND ( u.user_type = 'STAFF' or u.user_type='ADMIN') ").append("GROUP BY p.user_id ").append(" ) sub WHERE 1=1 ");

		return sql.toString();
	}

	/**
	 * 根据查询条件获取staff card的查询语句
	 * 
	 * @return
	 */
	@Override
	public String getStaffsCard(String status) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT u.user_id userId ").append(",s.create_date createDate ").append(",concat( p.given_name,' ', p.surname) staffName ")
				.append(",s.staff_no staffNo ").append(",s.staff_type staffType ")
				.append(",c.card_id cardNo ").append(",c.`status` `status` ")
				.append(",s.position_title_code positionTitle ").append(",position.position_name positionTitleName ")
				.append(",DATE_FORMAT(c.effective_date, '%Y-%m-%d') effectiveDate ")
				.append(",sc.code_display as staffTypeDisplay ")
				.append("FROM user_master AS u  INNER JOIN staff_master AS s  INNER JOIN  staff_profile AS p ")
				.append("LEFT JOIN sys_code sc ON  sc.code_value = s.staff_type AND ( sc.category = 'HK-CREW-ROLE' OR sc.category = 'staffType') ")
				.append("LEFT JOIN internal_permit_card AS c  ON s.user_id = c.staff_user_id  AND c.status = 'ISS' ")
				.append("LEFT JOIN position_title AS position ON position.position_code = s.position_title_code ")
				.append("WHERE u.user_id = s.user_id ").append("AND s.user_id = p.user_id  AND s.status = 'ACT'")
				.append("AND u.user_type = 'STAFF' ");
		if ("NCD".equalsIgnoreCase(status)) {
			sql.append(" AND (c.status is null or c.status <> 'ISS')");

		} else if ("ISS".equalsIgnoreCase(status)) {
			sql.append(" AND (c.status = 'ISS')");
		}

		String joinSql = "SELECT * FROM (" + sql.toString() + " GROUP BY p.user_id ) sub WHERE 1=1 ";
		return joinSql;
	}

	@Override
	@Transactional
	public void autoQuitExpireStaff() {
		List<StaffMaster> list = this.staffMasterDao.getExpireStaff();
		if (list != null && list.size() > 0) {
			logger.info("staff master quitExpired size:"+list.size());
			for (StaffMaster s : list) {
				logger.info("update statffMaster status  from "+s.getStatus()+" to  NACT , userId:"+s.getUserId());
				s.setStatus("NACT");
				s.setUpdateBy("system");
				s.setUpdateDate(new Date());
				this.staffMasterDao.update(s);
			}
		}

	}

	@Override
	public String getCoachs() {
		StringBuffer hql = new StringBuffer();
		//StringBuffer conditionHql = new StringBuffer();

		hql.append(
				"SELECT userId,createDate,contactEmail,staffName,staffNo,staffType,status,positionTitle,positionTitleName,employDate,quitDate,bookings,highPrice,lowPrice,staffTypeDisplay ")
				.append("FROM ( ").append("SELECT sub.*")
				.append(", sum(CASE pos.rate_type WHEN 'HI' THEN price.item_price END) 'highPrice'")
				.append(", sum(CASE pos.rate_type WHEN 'LO' THEN price.item_price END) 'lowPrice'").append("FROM (")
				.append("SELECT u.user_id userId").append(",s.create_date createDate")
				.append(",p.contact_email contactEmail").append(",concat( p.given_name,' ', p.surname) staffName")
				.append(",s.staff_no staffNo").append(",s.staff_type staffType").append(",s.`status` `status`")
				.append(",s.position_title_code positionTitle").append(",position.position_name positionTitleName")
				.append(",DATE_FORMAT(s.employ_date, '%Y/%m/%d') employDate")
				.append(",DATE_FORMAT(s.quit_date, '%Y/%m/%d') quitDate")
				.append(",count(booking.exp_coach_user_id) bookings").append(",sc.code_display as staffTypeDisplay")
				.append(" FROM user_master AS u  INNER JOIN staff_master AS s  INNER JOIN  staff_profile AS p ")
				.append("  LEFT JOIN sys_code sc ON  sc.code_value = s.staff_type AND ( sc.category = 'HK-CREW-ROLE' OR sc.category = 'staffType') ")
				.append(" LEFT JOIN member_facility_type_booking AS booking ON booking.exp_coach_user_id = p.user_id")
				.append(" LEFT JOIN position_title AS position ON position.position_code = s.position_title_code")
				.append(" WHERE u.user_id = s.user_id").append(" AND s.user_id = p.user_id")
				.append(" AND u.user_type = 'STAFF'").append(" GROUP BY p.user_id").append(" ) sub")
				.append(" LEFT JOIN staff_coach_rate_pos AS pos ON sub.userId = pos.user_id")
				.append(" LEFT JOIN pos_service_item_price AS price ON pos.pos_item_no = price.item_no")
				.append(" GROUP BY sub.userId").append(" ) tb WHERE 1=1 ");

		return hql.toString();
	}
	
	//added by Kaster 20160217
	@Override
	@Transactional
	public void autoDisposeQuitStaff() {
		List<StaffMaster> list = this.staffMasterDao.getExpireStaff();
		logger.info("get staff master expire staff size:"+(null==list?0:list.size()));
		if(list!=null && list.size()>0){
			StringBuffer sb = new StringBuffer("(");
			for(StaffMaster s : list){
				s.setStatus("NACT");
				s.setUpdateBy("system");
				s.setUpdateDate(new Date());
				logger.info("update staffMaster status to  NACT  s:"+s.getUserId());
				this.staffMasterDao.update(s);
				
				sb.append("'"+s.getUserId()).append("',");
			}
			String userIds = sb.substring(0, sb.lastIndexOf(","));
			userIds += ")";
			List<InternalPermitCard> internalPermitCards = internalPermitCardDao.getByStaffUserIds(userIds);
			if(internalPermitCards!=null && internalPermitCards.size()>0){
				for(InternalPermitCard ipc : internalPermitCards){
					logger.info("update InternalPermitCard ipc status from "+ipc.getStatus()+" to  DPS.......InternalPermitCard cardId:"+ipc.getCardId());
					//更新internal_permit_card表
					ipc.setStatus("DPS");
					ipc.setStatusUpdateDate(new Date());
					ipc.setUpdateBy("sysUser");
					internalPermitCardDao.update(ipc);
					
					//在internal_permit_card_log表中插入一条记录
					InternalPermitCardLog internalPermitCardLog = new InternalPermitCardLog();
					internalPermitCardLog.setCardId(ipc.getCardId());
					internalPermitCardLog.setStaffUserId(ipc.getStaffUserId());
					internalPermitCardLog.setContractorId(ipc.getContractorId());
					internalPermitCardLog.setEffectiveDate(ipc.getEffectiveDate());
					internalPermitCardLog.setExpiryDate(ipc.getExpiryDate());
					internalPermitCardLog.setStatusFrom("ACT");
					internalPermitCardLog.setStatusTo("DPS");
					internalPermitCardLog.setStatusUpdateDate(new Date());
					internalPermitCardLog.setUpdateBy("sysUser");
					internalPermitCardLogDao.save(internalPermitCardLog);
				}
			}
		}
	}

	
	/**
	 * 绑定卡
	 * @param dto
	 * @param createBy
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional
	public ResponseResult linkCard(StaffCardMasterDto dto, String createBy) throws Exception {
		
		String staffId = "[" + dto.getStaffId() + "]";
		//必填参数不可为空
		if (StringUtils.isEmpty(dto.getCardNo()) || StringUtils.isEmpty(staffId)) {
			responseResult.initResult(GTAError.AcademyCardError.PARAMETAR_MISSING_ERROR);
			return responseResult;
		}
		//验证staff是否有效
		boolean isActive = staffMasterDao.isStaffActive(staffId);
		if (!isActive) {//无效staff用户不可linkCard
			responseResult.initResult(GTAError.AcademyCardError.NOT_ACTIVE_MEMBER);
			return responseResult;
		}
		
		//验证此cardNo是否存在在卡池中即系是否有效       TODO 现阶段需求很模糊，此处有可能需求会变更
		InternalPermitCard newCard = internalPermitCardDao.getCardById(Long.parseLong(CommUtil.staffCardNoTransfer(dto.getCardNo())));
		if (newCard == null) {
		    responseResult.initResult(GTAError.AcademyCardError.NO_SUCH_CARD_FOUND);
		    return responseResult;
		}
		//验证此卡的状态同是否已经被绑定
		if ((PermitCardMasterEnumStatus.ISS.getCode().equals(newCard.getStatus()) || PermitCardMasterEnumStatus.DPS.getCode().equals(newCard.getStatus()))
				&& newCard.getStaffUserId() != null) {
			responseResult.initResult(GTAError.AcademyCardError.CARD_HAS_BEEN_ISSUED);
			return responseResult;
		}
		
//		else if (PermitCardMasterEnumStatus.DPS.getCode().equals(newCard.getStatus())) {
//			responseResult.initResult(GTAError.AcademyCardError.CARD_HAS_BEEN_DISPOSAL);
//			return responseResult;
//		}
		
		//判断该staff是否已经绑定过Card，如果绑定过将之前卡的状态变更为Disposal
		Date date = new Date();
		List<InternalPermitCard> oldCards = internalPermitCardDao.getIssuedCardByStaffUserId(staffId);
		if (oldCards != null && oldCards.size() != 0) {
			if (oldCards.size() > 1) { //每个用户只可绑定一张有效的卡
				responseResult.initResult(GTAError.AcademyCardError.DATA_ERROR_FOUND);
				return responseResult;
			} else {//将原来的旧卡状态变为Disposal
				InternalPermitCard oldCard = oldCards.get(0);
				String oldStatus = oldCard.getStatus();
				oldCard.setStatus(PermitCardMasterEnumStatus.DPS.getCode());
				oldCard.setStatusUpdateDate(date);
				oldCard.setUpdateBy(createBy);
				internalPermitCardDao.update(oldCard);
				internalPermitCardLogDao.createInternalPermitCardLog(oldCard, oldStatus);
			}
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.SECOND, 1);
		Date dateAdd = calendar.getTime();
		String oldStatus = newCard.getStatus() == null ? "" : newCard.getStatus();
		newCard.setStatus(PermitCardMasterEnumStatus.ISS.getCode());
		newCard.setStatusUpdateDate(dateAdd);
		newCard.setStaffUserId(staffId);
		newCard.setEffectiveDate(dateAdd);
		newCard.setUpdateBy(createBy);
		internalPermitCardDao.update(newCard);
		internalPermitCardLogDao.createInternalPermitCardLog(newCard, oldStatus);

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@Override
	@Transactional
	public String getStaffTypeByUserId(String userId) throws Exception {
		StaffMasterInfoDto dto = staffDao.getStaffMasterDtoByUserId(userId);
		return dto.getStaffType();
	}
	
	
}
