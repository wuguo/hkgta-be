package com.sinodynamic.hkgta.service.onlinepayment;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sinodynamic.hkgta.dto.paymentGateway.AmexResultDto;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.IServiceBase;

/**
 * payment gateway business logic. Copy from AMEX sample codes + PaymentGateway.java
 * @author sam.hui
 *
 */
public interface AmexPaymentGatewayService extends IServiceBase<PaymentGateway> {

	/**
	 * captures the input value from frontend and form form the url and parameter to pass to AMEX payment gateway.
	 * @param customerOrderTrans
	 * @return return the redirect link of card center with transaction detail parameters
	 */
	public String payment(CustomerOrderTrans customerOrderTrans);
	
	/**
	 * captures the input value from frontend and form form the url and parameter to pass to AMEX payment gateway.
	 * @param customerOrderTrans
	 * @param urlParams
	 * @return return the redirect link of card center with transaction detail parameters
	 */
	public String payment(CustomerOrderTrans customerOrderTrans, Map<String, Object> urlParams);

	/**
	 *  based on the return link to call the controller API url and the controller function will call this function to extract the result for further implementation. 
	 * @param fields The return parameters from card center as request parameter of the call back url
	 * @return The DTO object to store the response result from card center
	 */
	public AmexResultDto paymentCallBack(Map<String, String> fields);
	
	/**
	 * send capture command to confirm the authorized payment is capture
	 * @param requestFields The hashmap of all request fields 
	 * @return The DTO object to store the response result from card center
	 */
	public AmexResultDto capture(Map<String, String> requestFields);
	

	/**
	 * can only be used after capture success. Refund is not equal to void. In normal case, refund should not be used. HKGTA has no the business function of card payment refund. 
	 * @param requestFields The hashmap of all request fields
	 * @return The DTO object to store the response result from card center
	 */
	public AmexResultDto refund(Map<String, String> requestFields);
	
	/**
	 * to query the transaction result from card center
	 * @param requestFields The hashmap of all request fields 
	 * @return The DTO object to store the response result from card center
	 */
	public AmexResultDto queryDR(Map<String, String> requestFields);	
	
	public CustomerOrderTrans queryDR(CustomerOrderTrans customerOrderTrans);
	
	public String processResult(HttpServletRequest request,AmexResultDto amexResultDto,String userId);
	
	public String processResult(HttpServletRequest request, String userId);
//	public AmexResultDto voidpurchase(Map<String, String> requestFields, CustomerOrderTrans customerOrderTrans);

}
