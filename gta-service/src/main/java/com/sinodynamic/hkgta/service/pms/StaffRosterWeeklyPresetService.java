package com.sinodynamic.hkgta.service.pms;

import java.util.Set;

import com.sinodynamic.hkgta.dto.pms.StaffRosterWeeklyPresetDto;

public interface StaffRosterWeeklyPresetService {

	public void save(StaffRosterWeeklyPresetDto staffRosterDto, String userId);

	public void update(StaffRosterWeeklyPresetDto staffRosterDto, String userId, String presetName);
	
	StaffRosterWeeklyPresetDto getStaffRoster(String presetName);
	
	Set<String> searchStaffRoster(String presetName);
	
	void deleteStaffRoster(String presetName);
}
