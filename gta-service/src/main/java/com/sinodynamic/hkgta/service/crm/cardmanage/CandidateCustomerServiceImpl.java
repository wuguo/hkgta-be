package com.sinodynamic.hkgta.service.crm.cardmanage;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.memberapp.CandidateCustomerDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderPermitCardDao;
import com.sinodynamic.hkgta.dto.memberapp.CandidateCustomerDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.rpos.CandidateCustomer;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CandidateCustomerServiceImpl extends ServiceBase<CandidateCustomer> implements CandidateCustomerService {
	
	@Autowired
	private CustomerOrderPermitCardDao permitCardDao;
	
	@Autowired
	private CandidateCustomerDao candidateCustomerDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Override
	@Transactional
	public ResponseResult getCandidateCustomer(Long daypassId) {
		CustomerOrderPermitCard permitCard = permitCardDao.get(CustomerOrderPermitCard.class, daypassId);
		if (null == permitCard) {
//			responseResult.initResult(GTAError.DayPassError.DAYPASS_NOT_EXISTS__Error);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
		CandidateCustomer candidateCustomer = permitCard.getCandidateCustomer();
		if (null == candidateCustomer) {
//			responseResult.initResult(GTAError.DayPassError.NOT_EXISTS__CANDIDATE_CUSTOMER_Error);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
		Data data = new Data();
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setSurname(candidateCustomer.getSurname());
		customerProfile.setSurnameNls(candidateCustomer.getSurnameNls());
		customerProfile.setGivenName(candidateCustomer.getGivenName());
		customerProfile.setGivenNameNls(candidateCustomer.getGivenNameNls());
		customerProfile.setContactEmail(candidateCustomer.getContactEmail());
		customerProfile.setPhoneMobile(candidateCustomer.getPhoneMobile());
		customerProfile.setGender(candidateCustomer.getGender());
		customerProfile.setCustomerAdditionInfos(null);
		customerProfile.setCustomerAddresses(null);
		customerProfile.setCustomerEnrollments(null);
		List<CustomerProfile> list = new ArrayList<CustomerProfile>();
		list.add(customerProfile);
		data.setList(list);
		data.setLastPage(true);
		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
		
		
	}
	

	@Override
	@Transactional
	public ResponseResult getCandidateCustomerInfo(Long daypassId) {
		CustomerOrderPermitCard permitCard = permitCardDao.get(CustomerOrderPermitCard.class, daypassId);
		if (null == permitCard) {
//			responseResult.initResult(GTAError.DayPassError.DAYPASS_NOT_EXISTS__Error);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
		CandidateCustomer candidateCustomer = permitCard.getCandidateCustomer();
		if (null == candidateCustomer&&permitCard.getCardholderCustomerId()==null) {
//			responseResult.initResult(GTAError.DayPassError.NOT_EXISTS__CANDIDATE_CUSTOMER_Error);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
		
		CandidateCustomerDto candidateCustomerDto = new CandidateCustomerDto();
		Long candidateId = null;
		String givenName = null;
		String givenNameNls = null;
		String phoneMobile = null;
		String surname = null;
		String surnameNls = null;
		
		Long customerId = permitCard.getCardholderCustomerId();
		CustomerProfile customerProfile = customerProfileDao.getCustomerProfileByCustomerId(customerId);
		if(customerId==null||customerProfile==null){
			candidateId = candidateCustomer.getCandidateId();
			givenName = candidateCustomer.getGivenName();
			givenNameNls = candidateCustomer.getGivenNameNls();
			phoneMobile = candidateCustomer.getPhoneMobile();
			surname = candidateCustomer.getSurname();
			surnameNls = candidateCustomer.getSurnameNls();
		}else{
			givenName = customerProfile.getGivenName();
			givenNameNls = customerProfile.getGivenNameNls();
			phoneMobile = customerProfile.getPhoneMobile();
			surname = customerProfile.getSurname();
			surnameNls = customerProfile.getSurnameNls();
		}
		candidateCustomerDto.setCandidateId(candidateId);
		candidateCustomerDto.setGivenName(givenName);
		candidateCustomerDto.setGivenNameNls(givenNameNls);
		candidateCustomerDto.setPhoneMobile(phoneMobile);
		candidateCustomerDto.setSurname(surname);
		candidateCustomerDto.setSurnameNls(surnameNls);
		responseResult.initResult(GTAError.Success.SUCCESS, candidateCustomerDto);
		return responseResult;
		
		
	}


	@Override
	@Transactional
	public ResponseResult editCandidateCustomer(CandidateCustomerDto candidateCustomerDto) {
		//Has to provide the day pass id to update the 

		String givenName = candidateCustomerDto.getGivenName();
		String givenNameNls = candidateCustomerDto.getGivenNameNls();
		String phoneMobile = candidateCustomerDto.getPhoneMobile();
		String surname = candidateCustomerDto.getSurname();
		String surnameNls = candidateCustomerDto.getSurnameNls();
		
		CustomerOrderPermitCard permitCard = null;
//		Long candidateId = null;
		CandidateCustomer candidateCustomer = null;
		if(candidateCustomerDto.getDayPassId() != null){
			permitCard = permitCardDao.get(CustomerOrderPermitCard.class,candidateCustomerDto.getDayPassId());
			candidateCustomer = permitCard.getCandidateCustomer();
		}
		
//		if(candidateId!=null){
//			candidateCustomer = candidateCustomerDao.get(CandidateCustomer.class, candidateId);
//		}
		
		Long customerId = null;
		
		if(permitCard!=null){
			customerId = permitCard.getCardholderCustomerId();
		}
		CustomerProfile customerProfile = customerProfileDao.getCustomerProfileByCustomerId(customerId);
		
		if(customerProfile==null&&candidateCustomer==null){
			candidateCustomer = new CandidateCustomer();
			candidateCustomer.setGivenName(givenName);
			candidateCustomer.setGivenNameNls(givenNameNls);
			candidateCustomer.setPhoneMobile(phoneMobile);
			candidateCustomer.setSurname(surname);
			candidateCustomer.setSurnameNls(surnameNls);
			candidateCustomer.setCustomerType("DP");
			Long daypassId = candidateCustomerDto.getDayPassId();
			if(daypassId!=null){
				Long id = (Long) candidateCustomerDao.save(candidateCustomer);
				CandidateCustomer temp= candidateCustomerDao.get(CandidateCustomer.class, id);
//				CustomerOrderPermitCard permitCard = permitCardDao.get(CustomerOrderPermitCard.class,daypassId);
				permitCard.setCandidateCustomer(temp);
				permitCardDao.update(permitCard);
			}
			
		}else{

			if(customerProfile!=null){
				customerProfile.setGivenName(givenName);
				customerProfile.setGivenNameNls(givenNameNls);
				customerProfile.setPhoneMobile(phoneMobile);
				customerProfile.setSurname(surname);
				customerProfile.setSurnameNls(surnameNls);
				customerProfileDao.update(customerProfile);
			}else{
				candidateCustomer.setGivenName(givenName);
				candidateCustomer.setGivenNameNls(givenNameNls);
				candidateCustomer.setPhoneMobile(phoneMobile);
				candidateCustomer.setSurname(surname);
				candidateCustomer.setSurnameNls(surnameNls);
				candidateCustomerDao.update(candidateCustomer);
			}
			
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

}
