package com.sinodynamic.hkgta.service.sys;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayCmdDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayDao;
import com.sinodynamic.hkgta.dao.quartz.QrtzTriggersDao;
import com.sinodynamic.hkgta.dao.quartz.QrtzTriggersDaoImpl;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmd;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmdPK;
import com.sinodynamic.hkgta.entity.quartz.QrtzTriggers;
import com.sinodynamic.hkgta.util.CommUtil;

@Service
public class SysAdminService {
	Logger logger = Logger.getLogger(SysAdminService.class);
	private final Pattern IPPATTERN = Pattern.compile(
	        "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
	
	@Resource(name="appProperties") Properties appProp;
	@Resource(name="smsProperties") Properties smsProp;
	@Resource(name="spaProperties") Properties spaProp;
	@Resource(name="pushProperties") Properties pushProp;
	@Resource(name="oasisProperties") Properties oasisProp;
	@Resource(name="ddxProperties") Properties ddxProp;
	@Autowired
	QrtzTriggersDao qrtzTriggersDao;
	
	@Autowired
	PaymentGatewayDao paymentGatewayDao;

	@Autowired
	PaymentGatewayCmdDao paymentGatewayCmdDao;
		
	SSLContext sslContext=null;	

	
	public boolean chkUrl(String value) {
        try {

        	int status=0;
            java.net.URL url = new java.net.URL(value);            
            
        	if (value.indexOf("https:") >= 0) { 
        		if (value.contains("sinodynamic") || value.contains("migs.mastercard.com.au")) { // only test these url to prevent too long to request timeout        		
	               javax.net.ssl.HttpsURLConnection conn = (javax.net.ssl.HttpsURLConnection) url.openConnection();
        		   conn.setSSLSocketFactory(sslContext.getSocketFactory());
	            
	               conn.setReadTimeout(1000);
	               conn.connect();	
	               status = conn.getResponseCode();		  
        		}   
        	} else 
            {
	            java.net.HttpURLConnection conn = (java.net.HttpURLConnection) url.openConnection();
	        	
	            conn.setReadTimeout(1000);
	            conn.connect();	
	            status = conn.getResponseCode();
        	}    		        	
        	
            //System.out.println(status);
            switch (status) {            	 
              case java.net.HttpURLConnection.HTTP_OK:
              case java.net.HttpURLConnection.HTTP_FORBIDDEN :	  
              case java.net.HttpURLConnection.HTTP_INTERNAL_ERROR:  // just want to test the remote site reachable and not care remote site status	
            	 return true;
              case java.net.HttpURLConnection.HTTP_NOT_FOUND :
           	     return false; 
              default:    
            	return false;
            }
 
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        	return false;
        } catch (java.io.IOException e) {        	  
            e.printStackTrace();
            return false;
        }					
		

	}
	
	public String formatChkDetail(String pName, String value) {

		if (pName==null)
			return "";
		
		String html="<li>"+ pName + " = ";  
		
		try {
			if (value!=null && value.indexOf("/") >= 0 && CommUtil.nvl(pName).toLowerCase().indexOf("date") < 0  || value.indexOf("www.")>=0 ) { // is directory or url
			   html += value;
			   if ((new File(value)).exists()) 
				   html+= " &nbsp; &nbsp; &nbsp; (pass)";
		       else if (value!=null && (value.indexOf("http") >= 0 &&  value.length() > 8  || value.indexOf("www.")>=0 ) ) { 			        
			        if (chkUrl(value))
			        	html+= " &nbsp; &nbsp; &nbsp; (pass)";
			        else
			        	html+= " &nbsp; &nbsp; &nbsp; <font color=red>(failed)</font>";
			        	
				} else {			    	   			    	   			    	   
		    	   html+= " &nbsp; &nbsp; &nbsp; <font color=red>(failed)</font>";			    	   
		       }	   
	
			} else if (pName.indexOf("password")>=0)
				html+=CommUtil.nvl(value, "????").substring(0,2) + "*****";
			else
				html += value;
	   } catch (Exception e) {
		   e.getStackTrace();
		   logger.error("System Error: " + e.getMessage());
	   }					
		
		return html+="</li>";					
	}
	
	public String chkAppProp() {
		String html="<h2>app.properties</h2><ul>";
		
		for (String pp : appProp.stringPropertyNames()) 
			html+=formatChkDetail(pp, appProp.getProperty(pp));			
		
		return html + "</ul>";		
	}
	
	public String chkSpaProp() {
		String html="<hr><h2>spa.properties</h2><br>";
		
		for (String pp : spaProp.stringPropertyNames()) 
			html+=formatChkDetail(pp, spaProp.getProperty(pp));			
		
		return html + "</ul>";		
	}	

	public String chkSmsProp() {
		String html="<hr><h2>sms.properties</h2><br>";
		
		for (String pp : smsProp.stringPropertyNames()) 
			html+=formatChkDetail(pp, smsProp.getProperty(pp));			
		
		return html + "</ul>";		
	}

	public String chkPushProp() {
		String html="<hr><h2>push.properties</h2><br>";
		
		for (String pp : pushProp.stringPropertyNames()) {
            if (pp.contains("pushapi.url")) {
            	html=html + "<li>" + pp + "=" + pushProp.getProperty(pp);
			   if (chkUrl(pushProp.getProperty(pp).trim() + "/test.api" ))
				    html+= " &nbsp; &nbsp; &nbsp; (pass)";
				else
				  	html+= " &nbsp; &nbsp; &nbsp; <font color=red>(failed)</font>";            	
            	
            }            
            else
			   html+=formatChkDetail(pp, pushProp.getProperty(pp));
		}	
		
		return html + "</ul>";		
	}	
	

	public String chkOasisProp() {
		String html="<hr><h2>oasis.properties</h2><br>";
		
		for (String pp : oasisProp.stringPropertyNames()) 
			html+=formatChkDetail(pp, oasisProp.getProperty(pp));			
		
		return html + "</ul>";		
	}		
	
	public String chkDdxProp() {
		String html="<hr><h2>dda_i.properties</h2><br>";
		
		for (String pp : ddxProp.stringPropertyNames()) {
			String value=ddxProp.getProperty(pp);
			if (IPPATTERN.matcher(value).matches()) { // if it is an IP address, 
				html+="<li>" + pp + " = " + value;
				// this get too long and timeout so no longer test it
//			    try {  
//			    	int port= Integer.parseInt(CommUtil.nvl(ddxProp.getProperty("hkgta.virtual.account.sftp.port"), "22"));
//			    	
//				    Socket  testSocket = new Socket(value, port); 
//			        html+= " &nbsp; &nbsp; &nbsp; (pass)";
//			    }
//			    catch (IOException e) {
//			        logger.error(e.getMessage());
//		        	html+= " &nbsp; &nbsp; &nbsp; <font color=red>(failed)</font>";
//			    } 

			}
			else 
			   html+=formatChkDetail(pp, ddxProp.getProperty(pp));
		}	
		
		return html + "</ul>";		
	}		
	
	
	/**
	 * verify property files and display the result in html page  
	 * @return html
	 */
	public String chkProperties() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { 
			    new X509TrustManager() {     
			        public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
			            return new X509Certificate[0];
			        } 
			        public void checkClientTrusted( 
			            java.security.cert.X509Certificate[] certs, String authType) {
			            } 
			        public void checkServerTrusted( 
			            java.security.cert.X509Certificate[] certs, String authType) {
			        }
			    } 
			}; 		
		
		// Install the all-trusting trust manager
		try {
		
		    this.sslContext = SSLContext.getInstance("SSL");		
		    sslContext.init(null, trustAllCerts, new java.security.SecureRandom()); 
//		    HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			
		} catch (GeneralSecurityException e) {
		}		
		
	    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
	    new javax.net.ssl.HostnameVerifier(){

	        public boolean verify(String hostname,
	                javax.net.ssl.SSLSession sslSession) {
	            if (hostname.equals("localhost")) {
	                return true;
	            }
	            return false;
	        }
	    });		
		
		
		String html="<html><body>v 0.5<br>";
		
		html += chkAppProp();
		html += chkSpaProp();
		html += chkPushProp();
		html += chkSmsProp();
		html += chkOasisProp();
		html += chkDdxProp();
		
		return html + "</body></html>";		
	}
	

	
	/**
	 * Check payment gateway return to page
	 * @return
	 */
	@Transactional
	public String chkPaymentGatewayCmd() {
		String html="<hr><h2>Payment Gateway</h2><ul>";
		html+="<li>gateway address=";
	   	Long gatewayId;
	   	gatewayId=Long.parseLong(appProp.getProperty("hkgta.paymentgateway.gatewayId"));
	   	if (gatewayId==null)
	   		return html+="failed</ul>"; 
	   	
	   	try {
			
			//PaymentGatewayCmd paymentGatewayCmd=paymentGatewayCmdDao.getUniqueByCols(PaymentGatewayCmd.class, new String[] {"id.gatewayId", "id.cmdId"}, new Serializable[] {gatewayId, "PAY"} );
			
			PaymentGatewayCmdPK pk = new PaymentGatewayCmdPK(gatewayId, "PAY");
			//PaymentGateway paymentGateway = paymentGatewayDao.get(PaymentGateway.class, gatewayId);
			PaymentGatewayCmd paymentGatewayCmd = paymentGatewayCmdDao.get(PaymentGatewayCmd.class, pk);			
			
			if  (paymentGatewayCmd!=null) {
				html+= paymentGatewayCmd.getPaymentGateway().getGatewayUrl() + " &nbsp; &nbsp; operator=" + paymentGatewayCmd.getPaymentGateway().getOperatorId();
				
				if (chkUrl(paymentGatewayCmd.getPaymentGateway().getGatewayUrl() + "/ma") )
				    html+= " &nbsp; &nbsp; &nbsp; (pass)";
				else
				  	html+= " &nbsp; &nbsp; &nbsp; <font color=red>(failed)</font>";				
				
                //////////////////	
				html+="<li>return to page=";
			   	html+=paymentGatewayCmd.getReturnToPage();
			   	if (chkUrl(paymentGatewayCmd.getReturnToPage()) )
			        html+= " &nbsp; &nbsp; &nbsp; (pass)";
			    else
			      	html+= " &nbsp; &nbsp; &nbsp; <font color=red>(failed)</font>";
			}
		} catch (Exception e) {			
			html+= "<font color=red>Failed</font>";
			logger.error("System Error: " + e.getMessage());
		}	   		   	
	   	
	   return html+="</ul>";
	}	
	
	/**
	 * get full list of cronjob triggers
	 * @param db
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<QrtzTriggers> getCronTriggers() {
	      //GenericDao  genericDao=new GenericDao();

	      try{
		         List<QrtzTriggers> qrtzTriggers=(List<QrtzTriggers>) qrtzTriggersDao.getByCol(QrtzTriggers.class, null, null, "jobName");
		          //logger.debug("userLs json" + json);
		         return qrtzTriggers;
	      }catch(Exception e){
	    	 //System.out.println(e.getMessage());
	         logger.error(logger.getName() + " : " + e);
	         throw e;
	         //return null;
	      }		      
	      //return "{ \"records\":"+json + "}";
	   }

	/**
	 * for loadbalancer to health check the app server connection DB status. Select the least record table (payment_gateway) to prove the DB connection alive    
	 * @throws Exception
	 */
	@Transactional
	public void dbHealthChk() throws Exception {
		try {
		   Long gatewayId=Long.parseLong(appProp.getProperty("hkgta.paymentgateway.gatewayId"));
		   PaymentGateway paymentGateway = paymentGatewayDao.get(PaymentGateway.class, gatewayId);
		   paymentGateway.getGatewayId();		
		} catch (Exception e) {			
			logger.error(e.getMessage());
			throw new Exception("DB connection error");
		}
		
	}
	
}
