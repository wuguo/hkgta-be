package com.sinodynamic.hkgta.service.crm.sales.renewal;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 
 * @author Spring_Zheng
 *
 */

public interface CustomerServiceAccService extends IServiceBase<CustomerServiceAcc>{
	
	 public ResponseResult updateCustomerServiceAccStatus(Long orderNo);
	 
	 public List<CustomerServiceAcc> getExpiringCustomers() throws Exception;

	public void handleExpiredCustomerServiceAcc() throws Exception;

	/**   
	* @author: Zero_Wang
	* @since: Sep 9, 2015
	* 
	* @description
	* write the description here
	*/  
	    
	public CustomerServiceAcc getAactiveByCustomerId(Long customerId);
	
	
	public boolean isCustomerServiceAccValid(Long customerId, ContractHelper obj);

	public void startRenewServiceAccount() throws Exception;
	
	/**
	 * 根据服务时间判断该会员是否有权限预订服务
	 * @param customerId
	 * @param date   服务时间
	 * @return
	 */
	public boolean isCustomerServiceAccValidByDate(Long customerId, Date date);
	
	/**
	 * 根据服务时间判断该会员是否有权限预订服务
	 * @param customerId
	 * @param  startTime
	 * @param endTime
	 * @return
	 */
	public boolean isCustomerServiceAccValidByDate(Long customerId, String startTime,String endTime);

	CustomerServiceAcc getActiveByCustomerId(Long customerId);
	
	boolean isExpiredAndNotRenewalMember(Long customerId);
	
	/***
	 * check renewed service expiry
	 * @param customerId
	 * @param date
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public boolean checkRenewedService(Long customerId,String date, String startTime,String endTime);
	/***
	 * check parton booking perssion  if return true then have perssion else no
	 * @param customerId
	 * @param reservationDate 
	 * @return
	 */
	public ResponseResult checkServicePlanBookingPerssion(Long customerId,String reservationDate,String view,String device);
	
	public ResponseResult getPaymentMethodByCustomerId(Long customerId,String type);
	/***
	 * (hide both "VISA/Master" and "American Express" for Facilities Booking and purchasing Day Pass)
	 * no need to hide credit card payment method for Cash Value Top Up
	 * @param customerId
	 * @param type
	 * @return
	 */
	public ResponseResult getPaymentMethodByPatron(Long customerId,String type);

}
