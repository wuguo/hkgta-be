package com.sinodynamic.hkgta.service.crm.cardmanage;

import com.sinodynamic.hkgta.dto.memberapp.CandidateCustomerDto;
import com.sinodynamic.hkgta.entity.rpos.CandidateCustomer;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CandidateCustomerService extends IServiceBase<CandidateCustomer> {

	ResponseResult getCandidateCustomer(Long daypassId);

	ResponseResult getCandidateCustomerInfo(Long daypassId);

	ResponseResult editCandidateCustomer(CandidateCustomerDto candidateCustomerDto);

}
