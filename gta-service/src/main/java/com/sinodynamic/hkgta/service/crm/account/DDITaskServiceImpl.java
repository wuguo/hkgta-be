package com.sinodynamic.hkgta.service.crm.account;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;

import com.sinodynamic.hkgta.dao.crm.CashvalueTopupHistoryDao;
import com.sinodynamic.hkgta.dao.crm.DDITransactionLogDao;
import com.sinodynamic.hkgta.dao.crm.DDInterfaceFileLogDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.ical.GtaPublicHolidayDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dto.account.DDASuccessReportDetailDto;
import com.sinodynamic.hkgta.dto.account.DDIDto;
import com.sinodynamic.hkgta.dto.account.DDIReportDetailDto;
import com.sinodynamic.hkgta.dto.account.DDIReportDto;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.DdInterfaceFileLog;
import com.sinodynamic.hkgta.entity.crm.DdiTransactionLog;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.ical.PublicHoliday;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.SftpByJsch;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.report.JasperUtil;

import net.sf.json.JSONObject;

@Service
public class DDITaskServiceImpl extends ServiceBase implements DDITaskService {

	private Logger ddxLogger = LoggerFactory.getLogger(LoggerType.DDX.getName());

	@Resource(name = "ddxProperties")
	protected Properties ddxProps;

	@Autowired
	private DDInterfaceFileLogDao ddInterfaceFileLogDao;
	@Autowired
	private DDITransactionLogDao ddiTransactionLogDao;
	@Autowired
	private GtaPublicHolidayDao gtaPublicHolidayDao;
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	@Autowired
	private MemberDao memberDao;
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	@Autowired
	private CashvalueTopupHistoryDao topupHistoryDao;
	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;
	@Autowired
	private MailThreadService mailThreadService;
	@Autowired
	private GlobalParameterService globalParameterService;
	
	private String ftpHost;
	private String port;
	private String ftpUserName;
	private String ftpPassword;
	private String timeout;

	private String uploadPath;
	private String receivePath;
	private String receiveBackPath;
	private String downLoadPath; // store all the remote files in local
	private String downLoadBackPath; // store the success processed files in
										// local

	private String scbCreditAccountNumber;// sino scb bank number

	@Autowired
	private AlarmEmailService alarmEmailService;

	@PostConstruct
	public void init() {
		ftpHost = ddxProps.getProperty(Constant.SFTP_HOST, null);
		port = ddxProps.getProperty(Constant.SFTP_PORT, null);
		ftpUserName = ddxProps.getProperty(Constant.SFTP_USERNAME, null);
		ftpPassword = ddxProps.getProperty(Constant.SFTP_PASSWORD, null);
		timeout = ddxProps.getProperty(Constant.SFTP_TIMEOUT, null);
		uploadPath = ddxProps.getProperty(Constant.SFTP_SNED_PATH, null);// remote upload dir
		receivePath = ddxProps.getProperty(Constant.SFTP_RECEIVE_PATH, null);// remote downloand dir
		receiveBackPath = ddxProps.getProperty(Constant.SFTP_RECEIVE_BACK_PATH, null);// remote back dir,after processed move to this dir
		downLoadPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_PATH, null);// store all the remote files in local
		downLoadBackPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_BACK_PATH, null);// store the success processed files in local
		scbCreditAccountNumber = ddxProps.getProperty("scb.credit.account.number", null);

	}

	/**
	 * replaced by getNextWorkingDayFromDB
	 * 
	 * @param date
	 * @return
	 */
	@Deprecated
	private String getNextWorkingDayFromPropFile(Date date) {
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		HashSet holidaySet = new HashSet();
		String holidayListStr = ddxProps.getProperty("hk.holiday", null);
		StringTokenizer st = new StringTokenizer(holidayListStr, ",");
		while (st.hasMoreTokens()) {
			holidaySet.add(st.nextToken());
		}

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		int i=0;
		while(true){
			if (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7
					|| holidaySet.contains(sdf_yyyymmdd.format(c.getTime()))) {
			} else {
				i++;
			}
			if(i==2)break;
			c.add(Calendar.DATE, 1);
		}
//		while (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7
//				|| holidaySet.contains(sdf_yyyymmdd.format(c.getTime()))) {
//			c.add(Calendar.DATE, 1);
//		}
		date = c.getTime();
		String valueDate = sdf_yyyymmdd.format(date);

		return valueDate;

	}

//	private String getNextWorkingDayFromDB(Date date) {
//		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
//		List<Date> holidays = gtaPublicHolidayDao.getNearestHolidays(date);
//		List<String> holidayStrs = new ArrayList<String>();
//		for (Date d : holidays) {
//			holidayStrs.add(sdf_yyyymmdd.format(d));
//		}
//		Calendar c = Calendar.getInstance();
//		c.setTime(date);
//		c.add(Calendar.DATE, 1);
//		while (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7
//				|| holidayStrs.contains(sdf_yyyymmdd.format(c.getTime()))) {
//			c.add(Calendar.DATE, 1);
//		}
//		date = c.getTime();
//		String valueDate = sdf_yyyymmdd.format(date);
//
//		return valueDate;
//	}
	/***
	 * Value Date should be 21st of each month working day (excluding holiday)
	 * if 21st is not working day, system can calculate next working day,
	   e.g: 21st is Friday which is public holiday, value date should be 24th Monday non working day
	 * @return
	 */
	private String getValueDate(){
		Date date=new Date();
		String suffix =ddxProps.getProperty("valueDate");
		suffix=StringUtils.isNotEmpty(suffix)?suffix.trim():"21";
		if(!suffix.matches("^\\d{1}$")){
			suffix="21";
		}
		if(!(Integer.valueOf(suffix)>=DateCalcUtil.getMonthFirstDay()&&Integer.valueOf(suffix)<=DateCalcUtil.getMonthLastDay()))
		{
			suffix="21";
		}
		String valueDate=DateConvertUtil.parseDate2String(date, "yyyy-MM")+"-"+suffix;
		Calendar c = Calendar.getInstance();
		c.setTime(DateConvertUtil.parseString2Date(valueDate, "yyyy-MM-dd"));
		
		Date validate=this.calculateDate(c,this.getHolidayByDate(c.getTime()));
		return DateConvertUtil.parseDate2String(validate, "yyyyMMdd"); 
	}
	private Date calculateDate(Calendar c,Map<Date, Date>map){
		if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
				||map.containsKey(c.getTime()))
		{
			c.add(Calendar.DATE, 1);
			calculateDate(c,map);
		}
		return c.getTime();
	}
	private Map<Date, Date> getHolidayByDate(Date date)
	{
		Map<Date, Date>map=new TreeMap<>();
		List<PublicHoliday>list=gtaPublicHolidayDao.getHolidayGeDate(date);
		if(null!=list&&list.size()>0){
			for (PublicHoliday holiday : list) {
				map.put(holiday.getHolidayDate(), holiday.getHolidayDate());
			}
		}
		return map;
	}
	
//	private String getNextWorkingDayFromDB(Date date) {
//		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
//		List<Date> holidays = gtaPublicHolidayDao.getNearestHolidays(date);
//		List<String> holidayStrs = new ArrayList<String>();
//		for (Date d : holidays) {
//			holidayStrs.add(sdf_yyyymmdd.format(d));
//		}
//		Calendar c = Calendar.getInstance();
//		c.setTime(date);
//		c.add(Calendar.DATE, 1);
//		int i=0;
//		while(true){
//			if (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7
//					|| holidayStrs.contains(sdf_yyyymmdd.format(c.getTime()))) {
//			} else {
//				i++;
//			}
//			if(i==2)break;
//			c.add(Calendar.DATE, 1);
//		}
//		date = c.getTime();
//		String valueDate = sdf_yyyymmdd.format(date);
//		return valueDate;
//	}

	private void readDDIDetail(String[] csvItems, DDIReportDto ddiReport) {
		if (csvItems.length != 14) {
			ddxLogger.error(
					"DDI error:the RCMS_E_72 detail record content format incorrect ,need to Separated by ',' array length is 14 ");
			return;
		}
		DDIReportDetailDto detail = new DDIReportDetailDto();
		detail.setSeqNo(csvItems[1]);
		detail.setDebtorBankCode(csvItems[2]);
		detail.setDebtorBankBranchCode(csvItems[3]);
		detail.setDebtorAccountNumber(csvItems[4]);
		detail.setDebtorName(csvItems[5]);
		detail.setDebtorReference(csvItems[6]);
		detail.setAmount(csvItems[7]);
		detail.setValueDate(csvItems[8]);
		detail.setParticular(csvItems[9]);//transcationNo
		detail.setReturnDate(csvItems[10]);
		detail.setReturnStatusCode(csvItems[11]);
		detail.setReturnReason(csvItems[12]);
		ddiReport.getDetails().add(detail);
	}

	public void readDDIHeader(String[] csvItems, DDIReportDto ddiReport) {
		ddiReport.setStartDate(csvItems[3]);
		ddiReport.setEndDate(csvItems[4]);

	}

	public void readDDIBTHeader(String[] csvItems, DDIReportDto ddiReport) {
		ddiReport.setCreditAccountNumber(csvItems[6]);
	}

	public void readDDITail(String[] csvItems, DDIReportDto ddiReport) {
		ddiReport.setNoOfRecord(Integer.parseInt(csvItems[1]));
	}
	/***
	 * format split , and replace " to null
	 * @param line
	 * @return
	 */
	private String[] itemFormat(String line)
	{
		String[] items = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		for(int i=0;i<items.length;i++)
		{
			items[i]=items[i].replace("\"", "");
		}
		return items;
	}
	private DDIReportDto parseRejectReport(File file) throws Exception {
		DDIReportDto ddiReport = new DDIReportDto();
		ddiReport.setDetails(new ArrayList<DDIReportDetailDto>());
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String sCurrentLine;
			ddxLogger.info("RCMS_E_72:read DDI file content " + file.getName() + " start.......");
			while ((sCurrentLine = br.readLine()) != null) {
				ddxLogger.info("line content is :" + sCurrentLine);
				//exist "","aa,bb"
				String[] items = itemFormat(sCurrentLine);
				if (items.length > 0 && "FH".equals(items[0])) {
					readDDIHeader(items, ddiReport);
				} else if (items.length > 0 && "BH".equals(items[0])) {
					readDDIBTHeader(items, ddiReport);
				} else if (items.length > 0 && "BD".equals(items[0])) {
					readDDIDetail(items, ddiReport);
				} else if (items.length > 0 && "BT".equals(items[0])) {
					readDDITail(items, ddiReport);
				}
			}
			ddxLogger.info("RCMS_E_72 :read  DDI file content end ");
			if (ddiReport.getDetails().size() != ddiReport.getNoOfRecord())
			{
				ddxLogger.error("ERROR !DDA file : total record size not equals record lines count .........");
				throw new GTACommonException("ERROR !DDA file : total record size not equals record lines count ........."); 
			}

		} finally {
			if (null != br)
				br.close();
		}
		return ddiReport;
	}

	private void uploadDDIReport(List<DDIDto> ddiList, String fileName) throws Exception {
		// Start
		File ddiFile = null;
		ddxLogger.info("upload DDI file start........" + fileName);
		StringBuilder fileContent = new StringBuilder();
		String resultRemark = "";
//		String valueDate = StringUtils.isBlank(getNextWorkingDayFromDB(new Date()))
//				? getNextWorkingDayFromPropFile(new Date()) : getNextWorkingDayFromDB(new Date());
		BigDecimal bigDecimal = BigDecimal.ZERO;
		try {
			// header
			fileContent.append(String.format("%s,%s,%s%n", "H", "003", scbCreditAccountNumber));
			int detailCount = 0;
			for (DDIDto ddi : ddiList) {
				String reg = "^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){2})?$";
				if (!ddi.getAmount().toString().matches(reg)) {
					ddxLogger.info("the valid amount format incorrect ,must be 2 digits :" + ddi.getAmount());
					continue;
				}
				detailCount++;
				fileContent.append(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s%n", "D", // 1.Identifier
						ddi.getDebtorBankCode() == null ? "" : ddi.getDebtorBankCode(), // 2.Debtor Bank/ACH Code
						"", // 3.Debtor Branch Code
						ddi.getDebtorAccountNumber() == null ? "" : ddi.getDebtorAccountNumber(), // 4.Account Number
						ddi.getDebtorAccountName() == null ? "" : ddi.getDebtorAccountName(), // 5.Account  Name
						ddi.getReference() == null ? "" : ddi.getReference(), // 6.Reference customerId
						ddi.getAmount() == null ? "" : ddi.getAmount(), // 7.Amount  N(16,2)
						getValueDate()// 8.valueDate
						, ddi.getCustomerOrderTransNo()// ""//9.Particulars customer_order_trans no
				));
				bigDecimal = bigDecimal.add(ddi.getAmount());
				ddxLogger.info("dda : " + JSONObject.fromObject(ddi).toString());
			}
			// Trailer
			fileContent.append(
					String.format("%s,%s,%s,%s,%s%n", "T", detailCount, 0, bigDecimal.setScale(2).toPlainString(), 0));
			if (detailCount != ddiList.size())
				resultRemark += " Some item(s) are skiped! ";
		} catch (Exception e) {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	        e.printStackTrace(new PrintStream(baos));  
			ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
			throw new GTACommonException(e);
		}
		// Creating the directory to store file
		// String rootPath = System.getProperty("catalina.home");
		File dir = new File("hkgta");
		if (!dir.exists())
			dir.mkdirs();
		// Create the file on server
		try {
			ddiFile = new File(dir.getAbsolutePath() + File.separator + fileName);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(ddiFile));
			stream.write(fileContent.toString().getBytes());
			stream.close();
			ddxLogger.info(Log4jFormatUtil.logInfoMessage(
					"Server File Location=" + ddiFile.getAbsolutePath() + File.separator + fileName, null, null,
					fileContent.toString()));
			if (this.checkFileMaxSize(ddiFile)) {
				ddxLogger.error("the DDI file beyond 2M ,please retry again...");
				throw new RuntimeException("the DDI file beyond 2M ,please retry again...");
			}
			try {
				this.uploadDDI(ddiFile);
			} catch (Exception e) {
		        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		        e.printStackTrace(new PrintStream(baos));  
				ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
				// if upload file failed, retry 3 times with 5 minutes interval.
				// If still failed after retry, send alert email to system
				// admin.
				runTaskTimes(ddiFile);
			}
		} catch (Exception e) {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	        e.printStackTrace(new PrintStream(baos));  
			ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
			throw new GTACommonException(e);
		}
		ddxLogger.info("upload DDI file end........" + fileName);
	}

	private void runTaskTimes(File file) throws Exception {
		for (int times = 1; times <= 3; times++) {
			try {
				ddxLogger.info("DDI upload fail wait 5 minutes to excute upload........excute time:" + times);
				Thread.sleep(5 * 1000l);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			try {
				this.uploadDDI(file);
				ddxLogger.info("DDI file upload........excute time:" + times + " success..");
				break;
			} catch (Exception e) {
		        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		        e.printStackTrace(new PrintStream(baos));  
				ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
				if (times == 3) {
					// If still failed after retry, send alert email to system
					// admin.
					alarmEmailService.sendAlarmEmailTask("upload DDI FILE to remote have happend exception", e);
					throw new GTACommonException("upload DDI file fail excute task 3 times  :" + e.getMessage());
				}
			}
		}

	}

	private void uploadDDI(File file) throws Exception {
		SftpByJsch sftp = new SftpByJsch(ftpHost, ftpUserName, ftpPassword, Integer.valueOf(port).intValue(), null,
				null, Integer.valueOf(timeout).intValue());
		sftp.upload(uploadPath, file.getName(), file);
		file.delete();
		ddxLogger.info("DDI file upload success......"
				+ Log4jFormatUtil.logInfoMessage(ftpHost + ",ftpUserName:" + ftpUserName + ",port:" + port,
						file.getAbsolutePath() + File.separator + file.getName(), "SFTP", null));
	}

	/***
	 * check upload DDI file max is 2M
	 * 
	 * @param file
	 * @return
	 */
	private boolean checkFileMaxSize(File file) {
		Boolean valid = Boolean.TRUE;
		FileChannel fc = null;
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
			fc = fis.getChannel();
			// fc.size is B
			long num = fc.size();
			if (num <= 2 * 1024 * 1024) {
				valid = Boolean.FALSE;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ddxLogger.error("calculate DDI file size have exception" + e.getMessage());
		} finally {
			try {
				fc.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return valid;
	}

	private void generateDDIReporter(Date date) throws Exception {
		String formatDate = DateConvertUtil.parseDate2String(date, "yyyyMM");
		String filename = String.format("hkgta.%s01.H2H-DDI-ICOL", formatDate);
		//get ddalog when status!=FAIL ,if exist log error msg 
		DdInterfaceFileLog ddAFile = getDdInterfaceFileLogByFileNameNoEquesStatus(filename,Status.FAIL.name());
		List<DDIDto> ddiList = null;
		if (null != ddAFile) {
			ddxLogger.error(filename + " already exported before");
			return;
		}
		//date is String endDateString = DateCalcUtil.previousMonthLastDay();
		String dateData = DateCalcUtil.previousMonthLastDay(date)+" 23:59:59";//yyyy-MM-dd
		ddiList = ddiTransactionLogDao.queryDDIData(dateData);
		ddxLogger.info("query ddi data from db size:" + (null != ddiList && ddiList.size() > 0 ? ddiList.size() : 0));
		if (ddiList != null && !ddiList.isEmpty()) {
			saveDataToDB(ddiList, filename, date);
			uploadDDIReport(ddiList, filename);
		} else {
			ddxLogger.info("DDI: No find valide record has been generated.......");
		}
	}
	
	private DdInterfaceFileLog getDdInterfaceFileLogByFileNameNoEquesStatus(String fileName,String status)
	{
		DdInterfaceFileLog ddInterfaceFileLog = (DdInterfaceFileLog) ddInterfaceFileLogDao
				.getUniqueByHql("from DdInterfaceFileLog  where filename='" + fileName + "' and status!='"+status+"'");
		return ddInterfaceFileLog;
	}

	private void saveDataToDB(List<DDIDto> ddiList, String fileName, Date date) {
		try {
			this.clearData(date);
			// save DdiTransactionLog
			ddxLogger.info("save ddi payment to db [customerOrderHd/customerOrderDet/customerOrderTrans]");
			Set<DdiTransactionLog> set = new HashSet<>();
			Timestamp currentTimesTamp = new Timestamp(date.getTime());
			String createBy="[SysAdmin001]";
			for (DDIDto ddiDto : ddiList) {
				String reg = "^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){2})?$";
				if (!ddiDto.getAmount().toString().matches(reg)) {
					ddxLogger.info("the valid amount format incorrect ,must be 2 digits :" + ddiDto.getAmount());
					continue;
				}
				CustomerOrderHd customerOrderHd = new CustomerOrderHd();
				customerOrderHd.setCustomerId(ddiDto.getCustomerId());
				customerOrderHd.setOrderTotalAmount(ddiDto.getAmount());
				customerOrderHd.setCreateDate(currentTimesTamp);
				customerOrderHd.setUpdateDate(date);
				customerOrderHd.setOrderStatus(Status.OPN.name());
				customerOrderHd.setOrderDate(date);
				customerOrderHd.setCreateBy(createBy);
				Long orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(customerOrderHd);
				CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);
				// CUSTOMER ORDER Detail
				CustomerOrderDet customerOrderDet = new CustomerOrderDet();
				customerOrderDet.setCustomerOrderHd(temp);
				customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
				customerOrderDet.setOrderQty(1L);
				customerOrderDet.setItemTotalAmout(ddiDto.getAmount());
				customerOrderDet.setCreateDate(currentTimesTamp);
				customerOrderDet.setUpdateDate(date);
				customerOrderDet.setCreateBy(createBy);
				customerOrderDetDao.saveOrderDet(customerOrderDet);
				// CUSTOMER ORDER TRANS
				CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
				customerOrderTrans.setCustomerOrderHd(temp);
				customerOrderTrans.setTransactionTimestamp(currentTimesTamp);
				customerOrderTrans.setPaidAmount(ddiDto.getAmount());
				customerOrderTrans.setStatus(Status.PND.name());
				customerOrderTrans.setPaymentMethodCode("DDI");
				customerOrderTrans.setPaymentMedia("NA");
				customerOrderTrans.setCreateDate(date);
				customerOrderTrans.setUpdateDate(currentTimesTamp);
				customerOrderTrans.setCreateBy(createBy);
				Long transNo = (Long) customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
				DdiTransactionLog ddiTransactionLog = new DdiTransactionLog();
				ddiTransactionLog.setTransactionNo(transNo);
				set.add(ddiTransactionLog);
				ddiDto.setCustomerOrderTransNo(transNo);
			}
			this.saveDDILog(fileName, set,Status.PND.name(),date);
		} catch (Exception e) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(baos));
			ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
			throw new GTACommonException(e);
		}
	}

	/***
	 * clear history record [customerOrderHd customerOrderDet
	 * customerOrderTrans] status is PND AND paymentMethodCode is DDI
	 */
	private void clearData(Date date) {
		String hql = "from CustomerOrderTrans t where t.paymentMethodCode='DDI' and t.paymentMedia='NA' and t.status='"
				+ Status.PND.name() + "' and DATE_FORMAT(t.transactionTimestamp,'%Y-%m')='"+DateConvertUtil.parseDate2String(date, "yyyy-MM")+"'";
		List<CustomerOrderTrans> trans = customerOrderTransDao.getByHql(hql, null);
		if (null != trans) {
			for (CustomerOrderTrans tran : trans) {
				ddiTransactionLogDao.hqlUpdate("DELETE from DdiTransactionLog where transactionNo="+tran.getTransactionNo()+"", null);
				CustomerOrderHd hd = tran.getCustomerOrderHd();
				List<CustomerOrderDet> dets = hd.getCustomerOrderDets();
				customerOrderTransDao.delete(tran);
				for (CustomerOrderDet de : dets) {
					customerOrderDetDao.delete(de);
				}
			}
		}
	}

	private void saveDDILog(String fileName, Set<DdiTransactionLog> set,String status,Date date) {
		ddxLogger.info("save DdInterfaceFileLog....");
		DdInterfaceFileLog  ddInterfaceFileLog = new DdInterfaceFileLog();
		ddInterfaceFileLog.setFilename(fileName);
		ddInterfaceFileLog.setTransactionType("DDI");
		ddInterfaceFileLog.setStatus(status);
		ddInterfaceFileLog.setProcessTimestamp(date);
		ddInterfaceFileLog.setStatusUpdateBy("[SysAdmin001]");
		ddInterfaceFileLog.setStatusUpdateDate(date);
		if(null!=set){
			for (DdiTransactionLog ddi : set) {
				ddInterfaceFileLog.addOrderItem(ddi);
			}
		}
		ddInterfaceFileLogDao.saveOrUpdate(ddInterfaceFileLog);

	}

	@Transactional
	public String updateCustomerOrderTransByDDI(DDIReportDetailDto detail) {
		// look up member_payment_acc account_no
		String errorMsg = null;
		String debtorReferenceNumber = detail.getDebtorReference();// patron virtual  account number
		try {
			if (StringUtils.isNotEmpty(debtorReferenceNumber)) {
				List<MemberPaymentAcc> accs = memberPaymentAccDao
						.getByHql("from MemberPaymentAcc acc where acc.accType='DD' and acc.bankCustomerId='"
								+ debtorReferenceNumber + "' and acc.status='" + Status.ACT.name() + "'");
				if (null != accs && accs.size() > 0) {
					BigDecimal bankAmount = new BigDecimal(detail.getAmount());
					bankAmount = bankAmount.divide(new BigDecimal("1000000")).setScale(2, RoundingMode.HALF_UP);
					detail.setAmount(bankAmount.toString());
					CustomerOrderTrans trans = getCustomerOrderTransByCustomerId(accs.get(0).getCustomerId(),bankAmount);
					if (null != trans) {
						if(StringUtils.isNotEmpty(detail.getParticular()))
						{
							if(detail.getParticular().matches("\\d*"))
							{
								CustomerOrderTrans oldTrans = customerOrderTransDao.get(CustomerOrderTrans.class, Long.valueOf(detail.getParticular()));
								if(null!=oldTrans){
									trans=oldTrans;
								}
							} 
						}
						if (trans.getPaidAmount().compareTo(bankAmount) != 0) {
							ddxLogger.error(
									"DDI RCMS_E_72 amount not match CustomerOrderTrans paidAmount,don't touUp amount to patron memberCashvalue..");
							detail.setGtaStatus(Status.FAIL.name());
							detail.setGtaRemark("amount not match CustomerOrderTrans paidAmount.");
							return errorMsg;
						}
						CustomerOrderHd customerOrderHd = trans.getCustomerOrderHd();
						// look up customer_order_trans
						if (StringUtils.isNotEmpty(detail.getReturnStatusCode())) {
							trans.setStatus(Status.FAIL.name());
							trans.setInternalRemark(detail.getReturnReason());
							trans.setUpdateDate(new Timestamp(new Date().getTime()));
							customerOrderTransDao.update(trans);
							customerOrderHd.setOrderStatus(Status.CAN.name());
							customerOrderHd.setUpdateDate(new Date());
							customerOrderHdDao.update(customerOrderHd);
							errorMsg = "Fail error:" + detail.getReturnReason();
							detail.setGtaStatus(Status.FAIL.name());
							detail.setGtaRemark(detail.getReturnReason());
							
						} else {
							if (!Status.CMP.name().equals(customerOrderHd.getOrderStatus())) {
								this.topUpByCustomerTrans(trans);
							}
							detail.setGtaStatus("SUCCESS");
							
							trans.setStatus(Status.SUC.name());
							trans.setUpdateDate(new Timestamp(new Date().getTime()));
							customerOrderTransDao.update(trans);
							customerOrderHd.setOrderStatus(Status.CMP.name());
							customerOrderHd.setUpdateDate(new Date());
							customerOrderHdDao.update(customerOrderHd);
						
						}
					} else {
						ddxLogger
								.error("no find customerOrderTrans by customerId  and paymentMethod is DDA and status is PND : the customerId is :"
										+ accs.get(0).getCustomerId() + " ,accountNo:" + debtorReferenceNumber+" ,amount:"+bankAmount);
						detail.setGtaStatus(Status.FAIL.name());
						detail.setGtaRemark("no find valid customerOrderTrans by customerId and amount.");
					}
				} else {
					ddxLogger
							.error("not match MemberPaymentAcc for accType is DD and acc.status is ACT and accountNo is "
									+ debtorReferenceNumber);
					detail.setGtaStatus(Status.FAIL.name());
					detail.setGtaRemark("not match ACT DDA record");
				}
			} else {
				ddxLogger.error("the DDI DebtorReference is null... ");
				detail.setGtaStatus(Status.FAIL.name());
				detail.setGtaRemark("the DDI DebtorReference is null... ");
			}
		} catch (Exception e) {
			memberPaymentAccDao.getsessionFactory().getCurrentSession().clear();
			throw e;
		}
		return errorMsg;
	}
	private void updateDDInterfaceLog(String errorMsg) {
		String formatDate = DateConvertUtil.parseDate2String(new Date(), "yyyyMM");
		String logFileName = String.format("hkgta.%s01.H2H-DDI-ICOL", formatDate);
		DdInterfaceFileLog ddLog = getDdInterfaceFileLogByFileNameNoEquesStatus(logFileName, Status.FAIL.name());
		if (null!= ddLog) 
		{
			if(StringUtils.isNotEmpty(errorMsg)){
				ddLog.setStatus(Status.FAIL.name());
			}else{
				if(checkDdiAllTransactionStatus(ddLog))
				{
					ddLog.setStatus(Status.SUC.name());
				}
			}
			ddLog.setBankMsg(errorMsg);
			ddLog.setStatusUpdateDate(new Date());
			ddLog.setStatusUpdateBy("[SysAdmin001]");
			ddInterfaceFileLogDao.saveOrUpdate(ddLog);	
		}
	}
	private boolean checkDdiAllTransactionStatus(DdInterfaceFileLog ddLog)
	{
		Set<DdiTransactionLog> ddiSet=ddLog.getDdiTransactionLogs();
		if(null!=ddiSet)
		{
			for (DdiTransactionLog ddiTrans : ddiSet)
			{
				CustomerOrderTrans trans =customerOrderTransDao.get(CustomerOrderTrans.class, ddiTrans.getTransactionNo());
				if(!Status.SUC.name().equals(trans.getStatus()))
				{
					return false;
				}
			}
			return true;
		}else{
			return true;
		}
	}

	private CustomerOrderTrans getCustomerOrderTransByCustomerId(Long customerId,BigDecimal amount) {
		String hql = " SELECT trans FROM CustomerOrderTrans AS trans,CustomerOrderHd AS hd "
				+ "  WHERE trans.customerOrderHd.orderNo=hd.orderNo " + "	and trans.paymentMethodCode=?"
				+ " and trans.status=? and hd.customerId=? and trans.paidAmount=? order by trans.transactionTimestamp asc";
		List<Serializable> param = new ArrayList<>();
		param.add("DDI");
		param.add("PND");
		param.add(customerId);
		param.add(amount);
		List<CustomerOrderTrans> trans = customerOrderTransDao.getByHql(hql, param);
		if (null != trans && trans.size() > 0) {
			return trans.get(0);
		}
		return null;

	}

	private void topUpByCustomerTrans(CustomerOrderTrans trans) {
		Member member = memberDao.getMemberByCustomerId(trans.getCustomerOrderHd().getCustomerId());
		MemberCashvalue memberCashvalue = null;
		if (member.getSuperiorMemberId() != null) {
			// if current member is a dependent member,should update superior
			// member's cash value.
			memberCashvalue = memberCashValueDao.getByCustomerId(member.getSuperiorMemberId());
		} else {
			memberCashvalue = memberCashValueDao.getByCustomerId(member.getCustomerId());
		}
		BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().add(trans.getPaidAmount());
		memberCashvalue.setAvailableBalance(updatedAmount);
		memberCashvalue.setUpdateDate(new Date());
		memberCashvalue.setUpdateBy("[SysAdmin001]");
		memberCashValueDao.updateMemberCashValue(memberCashvalue);
		this.saveTopupHistory(memberCashvalue, trans);

	}

	private void saveTopupHistory(MemberCashvalue memberCashvalue, CustomerOrderTrans trans) {
		CashvalueTopupHistory topupHistory = new CashvalueTopupHistory();
		Date currentDate = new Date();
		topupHistory.setAmount(trans.getPaidAmount());

		if (null != memberCashvalue && null != memberCashvalue.getExchgFactor()) {
			topupHistory.setCashvalueAmt(trans.getPaidAmount().multiply(memberCashvalue.getExchgFactor()));
		} else {
			topupHistory.setCashvalueAmt(trans.getPaidAmount());
		}
		topupHistory.setCustomerId(memberCashvalue.getCustomerId());
		topupHistory.setSuccessDate(currentDate);
		topupHistory.setTopupDate(currentDate);
		topupHistory.setCreateDate(currentDate);
		topupHistory.setTopupMethod("DDI");
		topupHistory.setStatus(Constant.Status.SUC.toString());
		topupHistory.setTransactionNo(trans.getTransactionNo());
		topupHistoryDao.save(topupHistory);

	}

	@Transactional
	public void processDataDDI(File file) throws Exception {
		StringBuilder errorMsg = new StringBuilder();
		DDIReportDto rejectDDIReport = null;
		try {
			rejectDDIReport = parseRejectReport(file);
		} catch (Exception e) {
			this.updateDDInterfaceLog("the DDA report file contains error!");
			throw e;
		}
		if (null != rejectDDIReport) {
			if (scbCreditAccountNumber.equals(rejectDDIReport.getCreditAccountNumber())) {
				String msg = null;
				if(null==rejectDDIReport.getDetails()){
					ddxLogger.error("no have valid DDI data.......");
					return ;
				}
				for (DDIReportDetailDto detail : rejectDDIReport.getDetails()) {
					// add reject reason
					try {
						msg = this.updateCustomerOrderTransByDDI(detail);
						if (StringUtils.isNotEmpty(msg)) {
							errorMsg.append(msg);
						}
					} catch (Exception e) {
						ByteArrayOutputStream errorOut = new ByteArrayOutputStream();
						e.printStackTrace(new PrintStream(errorOut));
						ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, errorOut.toString()));
						errorMsg.append(e.getMessage());
					}
				}
				this.sendMailtoSales(rejectDDIReport.getDetails());
				
			} else {
				ddxLogger.error("CreditAccountNumber no equals originatorAccountNumber ,the originatorAccountNumber:"
						+ scbCreditAccountNumber + " and DDI file CreditAccountNumbe is :"
						+ rejectDDIReport.getCreditAccountNumber());
				errorMsg.append("CreditAccountNumber no equals originatorAccountNumber , Reson originatorAccountNumber:"
						+ scbCreditAccountNumber + " and DDI file CreditAccountNumbe is :"+ rejectDDIReport.getCreditAccountNumber());
			}
			this.updateDDInterfaceLog(errorMsg.toString());
		}else{
			ddxLogger.error("no find valid data from DDI file..");
		}
	}

	/***
	 * backup local and remote dda file
	 * 
	 * @param file
	 */
	private void backUpLocal_Remote_DDiFile(File file) throws Exception {
		try {
			// backup sftp file to bak directory
			SftpByJsch sftp = new SftpByJsch(ftpHost, ftpUserName, ftpPassword, Integer.valueOf(port).intValue(), null,
					null, Integer.valueOf(timeout).intValue());
//			sftp.move(file.getName(), receivePath, receiveBackPath);
			String currentTime=DateConvertUtil.parseDate2String(new Date(),"yyyyMMddHHmmss");
			sftp.move(file.getName(), receivePath, currentTime+"-"+file.getName(), receiveBackPath);
			ddxLogger.info("backup remote DDI file ..." + file.getName() + " the path :" + receiveBackPath);

			File outFile = new File(downLoadBackPath + File.separator + file.getName());
			FileCopyUtils.copy(file, outFile);
			ddxLogger.info(
					"backup local DDI file ..." + file.getName() + " the path :" + downLoadBackPath + File.separator);

		} catch (Exception e) {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	        e.printStackTrace(new PrintStream(baos));  
			ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, "backUpLocal_Remote_DDiFile happend Exception:"+baos.toString()));
			throw new GTACommonException(e);
		}
	}
	private void setFileListSortByTime(List<File> files)
	{
		Collections.sort(files, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				String fTime = o1.getName().split("\\.")[2];
				String eTime = o2.getName().split("\\.")[2];
				Long fNum = Long.valueOf(fTime.replaceAll("[^\\d]", ""));
				Long eNum = Long.valueOf(eTime.replaceAll("[^\\d]", ""));
				return fNum.compareTo(eNum);
			}
		});
	}
	private void sendMailtoSales(List<DDIReportDetailDto> details) {
		
		try {
			String fileName = "DDI Report of " + DateConvertUtil.parseDate2String(new Date(), "MM-yyyy");
			ddxLogger.info("send mail  DDI Report of "+fileName+" to administrator start..........");
			CustomerEmailContent cec = new CustomerEmailContent();
			GlobalParameter globalParameter = globalParameterService
					.getGlobalParameter(GlobalParameterType.DDA_DDIREPORTRECIPIENTEMAIL.getCode());
			if (null != globalParameter && !StringUtils.isEmpty(globalParameter.getParamValue())) {
				cec.setRecipientEmail(globalParameter.getParamValue().replace(",", ";"));//seperate each recipient" ;"
				cec.setCreateDate(new Date());
				cec.setStatus(EmailStatus.PND.name());
				cec.setSendDate(new Date());
				cec.setSubject(fileName);
				List<byte[]> bytesList = new ArrayList<>();
				List<String> mineTypeList = new ArrayList<>();
				List<String> fileNameList = new ArrayList<>();
				bytesList.add(this.createMailCSV(details));
				mineTypeList.add("application/csv");
				fileNameList.add(fileName + ".csv");
				mailThreadService.sendWithResponse(cec, bytesList, mineTypeList, fileNameList);
				ddxLogger.info("send mail  DDI Report of "+fileName+" to administrator  end..........");
			}
		} catch (Exception e) {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	        e.printStackTrace(new PrintStream(baos));  
			ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, "DDI report send Mail to admin happend Exception:"+baos.toString()));
		}
	}

	private byte[] createMailCSV(List<DDIReportDetailDto> details) {
		try {
			ddxLogger.info("create DDI report  csv start....");
			setAttribute(details);
			String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
			String jasperPath = reportPath + "DDISummaryReport.jasper";
			Map<String, Object> parameters = new HashMap();
			ddxLogger.info("create DDI report  csv end....");
			return JasperUtil.exportByJavaBeanConnection(jasperPath, parameters, details, "CSV");

		} catch (Exception e) {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	        e.printStackTrace(new PrintStream(baos));  
	    	ddxLogger.error("DDA createMail csv had happend exception");
	        ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
		
		}
		return null;

	}

	private void setAttribute(List<DDIReportDetailDto> dtos) {
		String hql = "select m.academyNo as patronId,CONCAT(cp.givenName,' ',cp.surname) as patronName from MemberPaymentAcc acc ,Member m,CustomerProfile cp where acc.accountNo=?"
				+ " and acc.customerId=m.customerId and m.customerId=cp.customerId";
		for (DDIReportDetailDto dto : dtos) {
			List<Serializable> param = new ArrayList<>();
			param.add(dto.getDebtorReference());
			List<DDASuccessReportDetailDto> attributes = memberPaymentAccDao.getDtoByHql(hql, param,
					DDASuccessReportDetailDto.class);
			if (null != attributes && attributes.size() > 0) {
				dto.setPatronId(attributes.get(0).getPatronId());
				dto.setPatronName(attributes.get(0).getPatronName());
			}
		}
	}

	@Transactional(rollbackFor = { Exception.class })
	public void generateDDIReport(Date date) throws Exception {
		generateDDIReporter(date);
	}

	@Transactional(rollbackFor = { Exception.class })
	public void parseDDIRejectReporter() throws Exception {
		String ddiRejectRepoterFileReg = ddxProps.getProperty("ddi.payments.rcmsE72fileReg");
		SftpByJsch sftp = new SftpByJsch(ftpHost, ftpUserName, ftpPassword, Integer.valueOf(port).intValue(), null,
				null, Integer.valueOf(timeout).intValue());
		List<File> files = sftp.dowloadByFileNameRegex(receivePath, ddiRejectRepoterFileReg, downLoadPath);
		ddxLogger.info("DDI file dowload start..");
		if (null != files && files.size() > 0) {
			ddxLogger.info("DDI file dowload complete..");
			this.setFileListSortByTime(files);// ascending time
			for (File file : files) {
				ddxLogger.info("DDI RCMS file name:" + file.getName());
				ddxLogger.info("handler ddi workflow start.....");
				try {
					this.processDataDDI(file);
				} catch (Exception e) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					e.printStackTrace(new PrintStream(baos));
					ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
					alarmEmailService.sendAlarmEmailTask("DDI Task excute happend exception", e);
				}
				this.backUpLocal_Remote_DDiFile(file);
				ddxLogger.info("handler ddi workflow end.....");
			}
		} else {
			ddxLogger.info(
					"don't find any DDI RCMS_E_72 file in sftp server ......please check sftp server have exist file ");
		}
	}
}
