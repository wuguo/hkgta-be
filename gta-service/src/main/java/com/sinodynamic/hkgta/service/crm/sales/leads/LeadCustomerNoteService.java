package com.sinodynamic.hkgta.service.crm.sales.leads;

import com.sinodynamic.hkgta.dto.crm.CustomerLeadNoteDto;
import com.sinodynamic.hkgta.entity.crm.CustomerLeadNote;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface LeadCustomerNoteService extends IServiceBase<CustomerLeadNote>{

	public Data listCustomerLeadNotes(Long customerId, ListPage<CustomerLeadNote> page) throws Exception;
	
	public ResponseResult saveCustomerLeadNote(CustomerLeadNoteDto customerLeadNote) throws GTACommonException, Exception;
	
	public boolean deleteCustomerLeadNote(Long id, String deleteBy) throws GTACommonException, Exception;
	
	public CustomerLeadNote getCustomerLeadNoteDetail(Long id) throws Exception;	
}
