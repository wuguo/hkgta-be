package com.sinodynamic.hkgta.service.common;

import java.util.Date;
import java.util.List;

public interface ShortMessageService {
	
	/**
	 * 
	 * @param sendTime: The time to send SMS
	 * @param phonenumbers: The receiver phone number collection,the format is as:+86134xxxxxxxx, it must add the country code +xxx
	 * @param content
	 * @return
	 * @throws Exception 
	 */
	public void sendSMS(List<String> phonenumbers,String content,Date sendDateTime);

}
