package com.sinodynamic.hkgta.service.pms;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.pms.RoomStayDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.entity.pms.RoomPicPath;
import com.sinodynamic.hkgta.entity.pms.RoomType;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant.AppType;
import com.sinodynamic.hkgta.util.constant.Constant.Terminal;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RoomTypeService extends IServiceBase<RoomType> {

	String DEFAULT_PATH_PREFIX_KEY = "image.server.roomType";
	
	public List<RoomTypeDto> getRoomListByTypes(String roomType);
	
	public List<RoomTypeDto> getRoomListByTypes(RoomStayDto roomType);
	
	public void setRoomPrice(List<RoomTypeDto> roomList, Map<String,Double> roomPrice);
	
	public ResponseResult createRoomType(RoomTypeDto room, String createBy) throws Exception;

	public ResponseResult updateRoomType(RoomTypeDto room, String updateBy) throws Exception;
	
	public RoomPicPath getRoomPicByPicId(Long picId) throws Exception;
	
	public ResponseResult getRoomTypeByCode(String roomTypeCode) throws Exception;

	String getRoomList();

	void setRoomPics(List<RoomTypeDto> roomList);

	public void deleteRoomType(String roomTypeCode, String updateBy);

	void saveRoomTypeItemPrice(String roomTypeCode);

	ResponseResult retrieveGuestRoom(String roomTypeCode) throws Exception;

	List<RoomTypeDto> getRoomListByTypes(RoomStayDto roomType, Terminal terminal, AppType appType);
}
