package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityAttributeCaptionService extends IServiceBase<FacilityAttributeCaption> {
	
	List<FacilityAttributeCaption> getFacilityAttributeCaptionByFuzzy(String attributeId);
	
	FacilityAttributeCaption getFacilityAttributeCaptionByAttributeId(String attributeId);
}
