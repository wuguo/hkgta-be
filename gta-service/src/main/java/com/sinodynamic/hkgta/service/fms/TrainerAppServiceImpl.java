package com.sinodynamic.hkgta.service.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.fms.CourseSessionDao;
import com.sinodynamic.hkgta.dao.fms.FacilityAttributeCaptionDao;
import com.sinodynamic.hkgta.dao.fms.FacilitySubTypeDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityAttendanceDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityBookAdditionAttrDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.fms.TrainerAppDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto;
import com.sinodynamic.hkgta.dto.crm.TrainingRecordDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.staff.CoachMonthyAchivementDto;
import com.sinodynamic.hkgta.dto.staff.CoachReservationDto;
import com.sinodynamic.hkgta.dto.staff.CoachSessionListDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.fms.CourseSession;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.sales.StaffProfileService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityName;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.UserPreferenceSettingParam;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class TrainerAppServiceImpl extends ServiceBase implements
		TrainerAppService {
	
	@Autowired
	TrainerAppDao trainerAppDao;
	/** cardNo长度*/
	private static final int			NINE_DIGIT	= 9;
	/** academyNo长度*/
	private static final int			SEVEN_DIGIT	= 7;

	@Autowired
	MemberFacilityAttendanceDao memberFacilityAttendanceDao;
	@Autowired
	CourseSessionDao courseSessionDao;
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	@Autowired
	MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;
	@Autowired
	private MemberFacilityBookAdditionAttrDao memberFacilityBookAdditionAttrDao;
	@Autowired
	private FacilityAttributeCaptionDao facilityAttributeCaptionDao;
	
	@Autowired
	MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private UserPreferenceSettingService userPreferenceSettingService;
	
	@Autowired
	private MessageTemplateDao templateDao;
	@Autowired
	private CustomerProfileDao customerProfileDao;
	@Autowired
    private ShortMessageService  smsService;
	
	@Autowired
    private AdvanceQueryService  advanceQueryService;
	@Autowired
	private FacilitySubTypeDao facilitySubTypeDao;
	@Autowired
	private StaffProfileService staffProfileService;
	@Override
	@Transactional
	public Data getReservationList(final String coachId,
			final String orderColumn, final String order, final int pageSize, final int currentPage,
			final AdvanceQueryDto filters) {
		
		CallBackExecutor exe = new CallBackExecutor(TrainerAppServiceImpl.class);
		
		Data result = (Data)exe.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				ListPage<CoachReservationDto> listPage = trainerAppDao.reservationList(coachId, orderColumn, order, pageSize, currentPage, null);
				Data data = new Data();
				data.setCurrentPage(currentPage);
				data.setPageSize(pageSize);
				data.setTotalPage(listPage.getAllPage());
				data.setLastPage(currentPage==listPage.getAllPage());
				data.setRecordCount(listPage.getAllSize());
				
				data.setList(listPage.getDtoList());
				return data;
			}

			@Override
			protected GTACommonException newTryException() {
				//modified by Kaster 20160510 将抛出异常统一规范化
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{getException().toString()});
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,getException().toString());
			}
			
			
		});
		
		return result;
	}
	
	@Override
	@Transactional
	public Data getReservationListByDate(final String coachId,
			final String orderColumn, final String order, final int pageSize, final int currentPage,
			final String resvDate) {
		
		CallBackExecutor exe = new CallBackExecutor(TrainerAppServiceImpl.class);
		
		Data result = (Data)exe.execute(new AbstractCallBack(){

			@SuppressWarnings("unchecked")
			@Override
			public Object doTry() throws Exception {
				String filterSql = ""; 
				if(resvDate.length()==7)
				{
					filterSql =  "WHERE DATE_FORMAT(staffBegin, '%Y-%m') = '" + resvDate +"'";
				}
				else
				{
					filterSql = "WHERE DATE_FORMAT(staffBegin, '%Y-%m-%d') = '" + resvDate +"'";
				}
				//add filter status <> Cancelled
//				filterSql+="  and status <> '"+MemberFacilityTypeBookingStatus.CAN.getDesc()+"'";
				
				
				ListPage<CoachReservationDto> listPage = trainerAppDao.reservationList(coachId, orderColumn, order, pageSize, currentPage, filterSql);
				
				Data data = new Data();
				data.setCurrentPage(currentPage);
				data.setPageSize(pageSize);
				data.setTotalPage(listPage.getAllPage());
				data.setLastPage(currentPage==listPage.getAllPage());
				data.setRecordCount(listPage.getAllSize());
				List<CoachReservationDto> list = new ArrayList<CoachReservationDto>();
				List<Object> tmp = listPage.getDtoList();
				for(Object obj : tmp)
				{
					list.add((CoachReservationDto) obj);
				}
				Collections.sort(list);
				data.setList(list);
//				data.setList(listPage.getDtoList());
				
				return data;
			}

			@Override
			protected GTACommonException newTryException() {
				//modified by Kaster 20160510 将抛出异常统一规范化
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{getException().toString()});
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,getException().toString());
			}
			
			
		});
		
		return result;
	}


	/**   
	* @author: Zero_Wang
	* @since: Sep 7, 2015
	* 
	* @description
	* this method will do follow two things
	* private coach Auto send MSM remind member InAdvance1Hour
	* private coach Auto push msg remind coach InAdvance1Hour
	*/  
	@Override
	@Transactional
	public void autoRemindInAdvance1Hour() {
		Calendar calendar = Calendar.getInstance();
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		Timestamp begin = new Timestamp(calendar.getTimeInMillis());
		String hour = this.appProps.getProperty("advance.remind.coach");
		calendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
		Timestamp end = new Timestamp(calendar.getTimeInMillis());
		String hql = "FROM  MemberFacilityTypeBooking m where  beginDatetimeBook between ? and ?  and status<> 'CAN'  and expCoachUserId is not null";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(begin);
		param.add(end);
		List<MemberFacilityTypeBooking> list =this.memberFacilityTypeBookingDao.getByHql(hql, param);
		for(MemberFacilityTypeBooking booking : list){
			
			sendRemindSMS2Member(booking);
			
			pushRemindMsg2Coach(booking);
		}
	}
	
	@Override
	@Transactional
	public void pushRemindMsg2Coach(MemberFacilityTypeBooking booking) {
		CustomerProfile cp = customerProfileDao.getById(booking.getCustomerId());
//		String username = cp.getSalutation()+" "+cp.getGivenName()+" "+cp.getSurname();
		MessageTemplate messageTemplate = templateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_APP_COACH_NOTICE);
		//Mars, bug fix
		FacilityReservationDto reservationDto= memberFacilityTypeBookingService.getFacilityReservationCourtTypeByResvId(booking.getResvId());
		String facilityType = booking.getResvFacilityType();
		if(null!=reservationDto){
			facilityType=reservationDto.getBayType();//left hand, right hand
		}
//		
//		String content = messageTemplate.getContent();
//		content = content
//				.replaceAll(MessageTemplateParams.PatronName.getParam(), username);
//		messageTemplate.setContent(content);
		String content = assemblyContent(booking, messageTemplate, facilityType,"MSG");
		logger.info("TrainerAppServiceImpl  pushRemindMsg2Coach run start ...resvId:"+booking.getResvId());
		devicePushService.pushMessage(new String[]{booking.getExpCoachUserId()},content, "trainer");
		logger.info("TrainerAppServiceImpl  pushRemindMsg2Coach run end ...resvId:"+booking.getResvId());
		
	}
	@Override
	@Transactional
	public void sendRemindSMS2Member(MemberFacilityTypeBooking booking) {
		List<String> phonenumbers = new ArrayList<String>();
		CustomerProfile customerProfile = customerProfileDao.getById(booking.getCustomerId());
		phonenumbers.add(customerProfile.getPhoneMobile());
		MessageTemplate messageTemplate = null;
		
		//Mars, bug fix: left hand right hand instead of Golf/Tennis 
		FacilityReservationDto reservationDto= memberFacilityTypeBookingService.getFacilityReservationCourtTypeByResvId(booking.getResvId());
		String facilityType = booking.getResvFacilityType();
		if(null!=reservationDto){
			facilityType=reservationDto.getBayType();//left hand, right hand
		}
		
		if("GOLF".equals(booking.getResvFacilityType())){
			messageTemplate = templateDao.getTemplateByFunctionId("golf_coach_remind");
		}else if("TENNIS".equals(booking.getResvFacilityType())){
			messageTemplate = templateDao.getTemplateByFunctionId("tennis_coach_remind");
		}
		String content = assemblyContent(booking, messageTemplate, facilityType,"SMS");
		try {
			logger.info("TrainerAppServiceImpl  sendRemindSMS2Member run start ...resvId:"+booking.getResvId());
			boolean sendSMS=true;
			if(null!=booking){
			   sendSMS=userPreferenceSettingService.sendSMSByCustomerId(booking.getCustomerId());
			}
			if(sendSMS)smsService.sendSMS(phonenumbers, content, DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
			
			logger.info("TrainerAppServiceImpl  sendRemindSMS2Member run end ...resvId:"+booking.getResvId());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	private String assemblyContent(MemberFacilityTypeBooking booking,
			MessageTemplate messageTemplate, String facilityName,String pushType) {
		String temp = booking.getBeginDatetimeBook().toString();
		String startTime = temp.substring(0, temp.indexOf("."));
		String bookingDate = startTime.substring(0,10);
		startTime = startTime.substring(11,16);
		temp = booking.getEndDatetimeBook().toString();
		String endTime = temp.substring(0, temp.indexOf("."));
		endTime = endTime.substring(11,16);
//		String templateContent = messageTemplate.getContent();
		
		Long timeLeft = (booking.getBeginDatetimeBook().getTime()-System.currentTimeMillis())/60/1000;
		String coachName = "";
		if(booking != null){
			StaffProfile sp = staffProfileService.getStaffProfileById(booking.getExpCoachUserId());
			if(null!=sp){
				String salu=null;
				if(sp.getGender().equals("M")){
					salu="Mr";
				}else{
					salu="Mrs";
				}
				coachName =salu+" "+sp.getGivenName() + " " + sp.getSurname();
			}
		}
		/*
		//Coach:{coachFullName} Time: {bookingDate} {startTime} Number of facility:{facilityTypeQty} Facility Type:{facilityType} HKGTA
		 */
		String content=null;
		if(pushType.equals("SMS")){
			 content=messageTemplate.getFullContent(coachName,bookingDate,startTime,booking.getFacilityTypeQty().toString(),facilityName);
		}else{
			//{patronName},{timeLeft},{bookingDate},{startTime},{facilityType}
			content=messageTemplate.getFullContent(coachName,timeLeft.toString(),bookingDate,startTime,booking.getFacilityTypeQty().toString(),facilityName);
		}
		return content;
	}

	/**   
	* @author: Zero_Wang
	* @since: Aug 27, 2015
	* 
	* @description
	*	Auto push message to coach in adavance 2 days
	*/  
	@Override
	@Transactional
	public void autoPushMsgInAdvance2Day() {
		Calendar calendar = Calendar.getInstance();
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String day = this.appProps.getProperty("advance.pushmessage.coach");
		calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
		Timestamp end = new Timestamp(calendar.getTimeInMillis());
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Timestamp begin = new Timestamp(calendar.getTimeInMillis());
		String hql = "FROM  MemberFacilityTypeBooking m where  beginDatetimeBook between ? and ? and endDatetimeBook between ? and ? and status='RSV'";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(begin);
		param.add(end);
		param.add(begin);
		param.add(end);
		List<MemberFacilityTypeBooking> list =this.memberFacilityTypeBookingDao.getByHql(hql, param);
		for(MemberFacilityTypeBooking booking : list){
			MessageTemplate template = null;
			String templateContent = "";
			String content = "";
			String facilityName	= "";
			String caption ="";
			if (null != booking && Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(booking.getResvFacilityType())){
				MemberFacilityBookAdditionAttr attr = memberFacilityBookAdditionAttrDao.getMemberFacilityBookAdditionAttrList(booking.getResvId()).get(0);
				FacilityAttributeCaption facilityAttributeCaption = this.facilityAttributeCaptionDao.get(FacilityAttributeCaption.class, attr.getId().getAttributeId());
				caption = facilityAttributeCaption.getCaption();
			}else if(null != booking && Constant.FACILITY_TYPE_TENNIS.equalsIgnoreCase(booking.getResvFacilityType())){
				com.sinodynamic.hkgta.entity.fms.FacilitySubType facilitySubType = facilitySubTypeDao.getById(booking.getFacilitySubtypeId());
				caption = facilitySubType.getName();
			}
			if("GOLF".equals(booking.getResvFacilityType())){
				template = templateDao.getTemplateByFunctionId("golf_coach_auto");
				if (template == null) {
					throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Private Coaching auto remind: golf_coach_auto"});
				}
				templateContent = template.getContent();
				facilityName = "golf";
			}else if("TENNIS".equals(booking.getResvFacilityType())){
				template = templateDao.getTemplateByFunctionId("tennis_coach_auto");
				if (template == null) {
					throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"send Private Coaching auto remind: tennis_coach_auto"});
				}
				templateContent = template.getContent();
				facilityName = "tennis";
			}
			CustomerProfile cp = customerProfileDao.getById(booking.getCustomerId());
			if (cp == null) {
				throw new GTACommonException(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_CUSTOMER_PROFILE);
			}
			String salutation = cp.getSalutation();
			String lastName = cp.getSurname();
			Long confirmation = booking.getOrderNo();
			String fullName = getName(cp);
			//Timestamp's toString() String object in yyyy-mm-dd hh:mm:ss.fffffffff format
			String temp = booking.getBeginDatetimeBook().toString();
			String startTime = temp.substring(0, temp.indexOf("."));
			String bookingDate = startTime.substring(0,10);
			startTime = startTime.substring(11,16);
			temp = booking.getEndDatetimeBook().toString();
			String endTime = temp.substring(0, temp.indexOf("."));
			endTime = endTime.substring(11,16);
		
			/**
			Tennis
			Dear {username} your private tennis coach booking information is: {bookingDate} {startTime}-{endTime} facility type: {tennisCourtType} HKGTA
			Golf
			Dear {username} your private golf coach booking information is: {bookingDate} {startTime}-{endTime} facility type: {golfBayType} HKGTA
			
			 * 
			 */
			content = templateContent
					.replaceAll(MessageTemplateParams.BookingDate.getParam(), bookingDate)
					.replaceAll(MessageTemplateParams.UserName.getParam(), fullName)
					.replaceAll(MessageTemplateParams.StartTime.getParam(), startTime)
					.replaceAll(MessageTemplateParams.EndTime.getParam(), endTime);
			if(FacilityName.GOLF.name().equals(booking.getResvFacilityType())){
				content = content.replaceAll(MessageTemplateParams.BayType.getParam(), caption);
			}else if(FacilityName.TENNIS.name().equals(booking.getResvFacilityType())){
				content = content.replaceAll(MessageTemplateParams.TennisType.getParam(), caption);
			}
			content = content.replace("{", "");
			content = content.replace("}", "");
			logger.info("TrainerAppServiceImpl  autoPushMsgInAdvance2Day run start ...resvId:"+booking.getResvId());
			devicePushService.pushMessage(new String[]{booking.getCustomerId()+""}, template.getMessageSubject(), content, "trainer");
			logger.info("TrainerAppServiceImpl  autoPushMsgInAdvance2Day run end ...resvId:"+booking.getResvId());
			
		}
	}	
	private String getName(CustomerProfile cp) {
		if (cp == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName()).append(" ").append(cp.getSurname());
		return sb.toString();
	}
	private int hours(Object value){
		return value == null ? 0 : Integer.parseInt(value.toString());
	}
	
	@Override
	@Transactional
	public CoachMonthyAchivementDto calculateCoachAchievement(
			final Map<String, Object> params) {
		CallBackExecutor exe = new CallBackExecutor(TrainerAppServiceImpl.class);
		
		CoachMonthyAchivementDto result = (CoachMonthyAchivementDto)exe.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				CoachMonthyAchivementDto achievement = new CoachMonthyAchivementDto();
				
				Object coaching = trainerAppDao.calculateCoachAchievement(TrainerAppDao.AchievementClassification.HOUR_OF_CLASSES, params);
				Object course = trainerAppDao.calculateCoachAchievement(TrainerAppDao.AchievementClassification.HOUR_OF_COURSES, params);
				Object students = trainerAppDao.calculateCoachAchievement(TrainerAppDao.AchievementClassification.TOTAL_OF_STUDENTS, params);
				Object newStudents = trainerAppDao.calculateCoachAchievement(TrainerAppDao.AchievementClassification.TOTAL_OF_NEW_STUDENTS, params);
				achievement.setSumClassesHours(new Long(hours(coaching)));
				achievement.setSumCourseHours(new Long(hours(course)));
				achievement.setSumNewStudent(new Long(hours(newStudents)));
				achievement.setSumStudent(new Long(hours(students)));
				
				return achievement;
			}
			
		});
		return result;
	}


	@Override
	@Transactional
	public ResponseResult rollCallforCoaching(Long resvId, String qrCode) throws Exception
	{
		MemberFacilityAttendance attendance = memberFacilityAttendanceDao.getAttendaneByResvId(resvId);
		if (null == attendance)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.CHECKING_RECORD_CANNOT_FIND, new Object[] { resvId });
		}
		
		Date now = new Date();
		MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBooking(resvId);
		Date startDate = booking.getBeginDatetimeBook();
		Date endDate = booking.getEndDatetimeBook();
		
		Calendar beginCalendar = Calendar.getInstance();
		beginCalendar.setTime(startDate);
		beginCalendar.add(Calendar.MINUTE, -30);
		Date rollCallStartDate = beginCalendar.getTime();
		
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		endCalendar.add(Calendar.MINUTE, 30);
		Date rollCallEndDate = endCalendar.getTime();
		
		if(now.before(rollCallStartDate) || rollCallEndDate.before(now)){
			throw new GTACommonException(GTAError.PrivateCoachingError.NOT_ROLLCALL_TIME);
		}
		
		ScanCardMappingDto memberInfo = new ScanCardMappingDto();
		if (qrCode.length()==7)
		{
			Member member = memberDao.getMemberByAcademyNo(qrCode);
			if (null == member || !member.getCustomerId().equals(attendance.getId().getCustomerId()))
			{
				throw new GTACommonException(GTAError.PrivateCoachingError.ROLLCALL_FOR_WRONG_MEMBER);
			}
			CustomerProfile p = customerProfileDao.getCustomerProfileByCustomerId(member.getCustomerId());
			memberInfo.setAcademyNo(member.getAcademyNo());
			memberInfo.setPortraitPhoto(p.getPortraitPhoto());
			memberInfo.setGivenName(p.getGivenName());
			memberInfo.setSurname(p.getSurname());
			memberInfo.setSalutation(p.getSalutation());
			memberInfo.setCustomerId(member.getCustomerId());
		}
		else
		{
			ResponseResult tmpResult = advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(qrCode)));
			if (null == tmpResult.getDto() || !((ScanCardMappingDto) tmpResult.getDto()).getCustomerId().equals(attendance.getId().getCustomerId()))
			{
				throw new GTACommonException(GTAError.PrivateCoachingError.ROLLCALL_FOR_WRONG_MEMBER);
			}
			memberInfo = (ScanCardMappingDto)tmpResult.getDto();
		}
		
		
		if (attendance.getAttendTime() == null)
		{
			attendance.setAttendTime(new Timestamp(new Date().getTime()));
			attendance.setMachineId("ROLLCALL");
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS, memberInfo);
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult hasRollCall(Long resvId) throws Exception
	{
		MemberFacilityAttendance attendance = memberFacilityAttendanceDao.getAttendaneByResvId(resvId);
		
		if (null == attendance)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.CHECKING_RECORD_CANNOT_FIND, new Object[] { resvId.toString() });
		}
		
		Map<String,String> result = new HashMap<String,String>();
		result.put("hasRollCall", attendance.getAttendTime() != null ? "true" : "");
		
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}



	/**   
	* @author: Zero_Wang
	* @since: Aug 31, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional
	public List<CustomerProfileDto> getPrivateCoachTrainedMemberList(
			String staffNo, String isAscending) {
		
		return this.trainerAppDao.getPrivateCoachTrainedMemberList(staffNo, isAscending);
	}
	@Override
	@Transactional
	public ListPage<TrainingRecordDto> getTrainingRecord(ListPage<TrainingRecordDto> pListPage, Long customerId, String coachId, String sortBy, String isAscending, String filterBy) throws Exception {
		
		StringBuffer hql = new StringBuffer();
		StringBuffer conditionHql = new StringBuffer();
		

		hql.append("SELECT atte.facility_timeslot_id as facilityTimeslotId, ")
					.append("atte.customer_id as customerId, ")
			        .append("atte.attend_time as attendTime, ")
			        .append("DATE_FORMAT(booking.begin_datetime_book,'%Y/%m/%d %H:%i') AS beginBookTime, ")
			        .append("DATE_FORMAT(DATE_ADD(booking.end_datetime_book, INTERVAL 1 SECOND), '%Y/%m/%d %H:%i')AS endBookTime, ")
			        .append("concat( sp.given_name,' ', sp.surname) coachName, ")
			        .append("atte.trainer_comment AS comment, ")
			        .append(" booking.resv_facility_type AS facilityType , ")
			        .append("atte.score AS score ");
		conditionHql.append("FROM member_facility_attendance atte  ")
			.append("LEFT JOIN member_reserved_facility res on res.facility_timeslot_id = atte.facility_timeslot_id ")
			.append("LEFT JOIN member_facility_type_booking booking on res.resv_id = booking.resv_id ")
			.append("LEFT JOIN staff_profile sp on sp.user_id = booking.exp_coach_user_id ")
			.append("WHERE atte.attend_time is not null ");
		
		List<Serializable> param = new ArrayList<Serializable>();
		if (null != customerId)
		{
			conditionHql.append(" AND atte.customer_id = ? ");
			param.add(customerId);
		}
		
		if (!StringUtils.isEmpty(coachId))
		{
			conditionHql.append(" AND booking.exp_coach_user_id = ? ");
			param.add(coachId);
		}
		if (!StringUtils.isEmpty(filterBy)){
			
			conditionHql.append(" AND booking.resv_facility_type = ? ");
			param.add(filterBy);
		}
		
		String queryHql ="Select * from (" + hql.append(conditionHql.toString()).toString() + ") t where 1=1 ";
		
		if (!StringUtils.isEmpty(sortBy)) {
			if(isAscending.equals("false")){
				pListPage.addDescending("t." + sortBy);
			}
			else {
				pListPage.addAscending("t." + sortBy);
			}
		}
		
		//pListPage.addDescending("atte.attend_time");
		StringBuffer countHql = new StringBuffer("SELECT count(1) from (");
		countHql.append(queryHql.toString()).append(") c");
		
		return  memberFacilityAttendanceDao.getTrainingRecordList(pListPage, param, countHql.toString(), queryHql);

	}


	@Override
	@Transactional
	public ResponseResult getTrainingComments(Long timeslotId, Long customerId) throws Exception
	{
		//MemberFacilityAttendance attendance = memberFacilityAttendanceDao.getAttendaneById(timeslotId, customerId);
		List<TrainingRecordDto> trainingRecordDtos = memberFacilityAttendanceDao.getTrainingCommentDetail(timeslotId, customerId);
		TrainingRecordDto trainingRecordDto = null;
		if (null != trainingRecordDtos && trainingRecordDtos.size() > 0){
			trainingRecordDto = trainingRecordDtos.get(0);
		}
		
		if (null == trainingRecordDto)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.NO_TRAINING_RECORD);
		}
		
		responseResult.initResult(GTAError.Success.SUCCESS, trainingRecordDto);
		return responseResult;
	}


	@Override
	@Transactional
	public ResponseResult updateTrainingComments(Long timeslotId, Long customerId, Double score, String comments, String assignment) throws Exception
	{
		MemberFacilityAttendance attendance = memberFacilityAttendanceDao.getAttendaneById(timeslotId, customerId);
		
		if (null == attendance)
		{
			throw new GTACommonException(GTAError.PrivateCoachingError.NO_TRAINING_RECORD);
		}
		
		attendance.setScore(score);
		attendance.setAssignment(assignment);
		attendance.setTrainerComment(comments);
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
		
	}


	/**   
	* @author: Zero_Wang
	* @since: Sep 1, 2015
	* 
	* @description
	* write the description here
	*/  
	@Transactional
	@Override
	public List<CustomerProfileDto> getPrivateCoachTrainedMemberListByName(
			String staffNo, String name, String isAsending) {
		
		return this.trainerAppDao.getPrivateCoachTrainedMemberListByName(staffNo, name, isAsending);
	}

	
	/**
	 * 根据条件参数查询课程列表
	 * @param coachId    staffId
	 * @param showType	0：查询私人教练课程同大学堂课程    1：查询私人教练课程   2：大学堂课程 
	 * @return
	 */
	@Override
	@Transactional
	public List<CoachSessionListDto> getReservationListBySessionDate(final String coachId, final int showType) {
		
		CallBackExecutor exe = new CallBackExecutor(TrainerAppServiceImpl.class);
		
		List<CoachSessionListDto> result = (List<CoachSessionListDto>)exe.execute(new AbstractCallBack(){

			@SuppressWarnings("unchecked")
			@Override
			public Object doTry() throws Exception {
				
				
				List<CoachSessionListDto> list = trainerAppDao.reservationListBySessionDate(coachId, showType);
				return list;
			}

			@Override
			protected GTACommonException newTryException() {
				//modified by Kaster 20160510 将抛出异常统一规范化
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{getException().toString()});
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,getException().toString());
			}
			
			
		});
		
		return result;
	}
	
	/**
	 * 根据条件参数查询课程列表
	 * @param QRCode    academyNo/cardNo
	 * @return
	 * @throws Exception 
	 */
	@Override
	@Transactional
	public ResponseResult getMembersPresentCurriculumList(String QRCode) throws Exception{
		if(QRCode == null || QRCode.equals("")){
			responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_MEMBER_LIST, "NO QRCode!");
		} else {
			int QRCodeType = 0;  //QRCodeType	1：academyNo    2：cardNo 
			if (QRCode.length() == NINE_DIGIT) {
				QRCode = CommUtil.cardNoTransfer(QRCode);
				QRCodeType = 2;
			} else if (QRCode.length() == SEVEN_DIGIT) {
				QRCodeType = 1;
			}
			if (StringUtils.isEmpty(QRCode)) {
				throw new GTACommonException(GTAError.PrivateCoachingError.NO_TRAINING_RECORD);
			} else {
				responseResult.initResult(GTAError.Success.SUCCESS, trainerAppDao.getMembersPresentCurriculumList(QRCode, QRCodeType));
				return responseResult;
			}
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult isRollCallPeriod(String id, String sessionId) {
		// TODO Auto-generated method stub
		Date startDate = new Date();
		Date endDate = new Date();
		String status = "";
		if(sessionId == null){
			MemberFacilityTypeBooking book = this.memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, Long.valueOf(id));
			if(book == null){
				throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			}
			startDate = book.getBeginDatetimeBook();
			endDate = book.getEndDatetimeBook();
			status = book.getStatus();
		}else{
			CourseSession cs = this.courseSessionDao.get(CourseSession.class, Long.valueOf(sessionId));
			if(cs == null){
				throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			}
			startDate = cs.getBeginDatetime();
			endDate = cs.getEndDatetime();
			status = cs.getStatus();
		}
		if (endDate.before(new Date())){
			status = "COMPLETE";
		}
		if("RSV".equals(status) || "CAN".equals(status)){
			responseResult.initResult(GTAError.Success.SUCCESS, false);
			return responseResult;
		}else{
			Calendar beginCalendar = Calendar.getInstance();
			beginCalendar.setTime(startDate);
			beginCalendar.add(Calendar.MINUTE, -30);
			Date rollCallStartDate = beginCalendar.getTime();
			
			Calendar endCalendar = Calendar.getInstance();
			endCalendar.setTime(endDate);
			endCalendar.add(Calendar.MINUTE, 30);
			Date rollCallEndDate = endCalendar.getTime();
			
			Date now = new Date();
			if(rollCallStartDate.before(now) && now.before(rollCallEndDate)){
				responseResult.initResult(GTAError.Success.SUCCESS, true);
				return responseResult;
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS, false);
				return responseResult;
			}
		}
		
	}

}
