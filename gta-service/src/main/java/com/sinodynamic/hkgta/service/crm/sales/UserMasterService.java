package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import com.sinodynamic.hkgta.dto.UserLoginRequest;
import com.sinodynamic.hkgta.entity.crm.LoginSession;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface UserMasterService extends IServiceBase<UserMaster> {
	
	public void addUserMaster(UserMaster m) throws Exception;
	
	public void getUserMasterList(ListPage<UserMaster> page,UserMaster m) throws Exception;

	public void updateUserMaster(UserMaster m) throws Exception;

	public void deleteUserMaster(UserMaster m) throws Exception;	
	
	public UserMaster getUserByLoginId(String id) throws Exception;
	
	public UserMaster getUserByUserId(String userId) throws Exception;
	
	public void SaveSessionToken(String userId, String authToken, String device) throws Exception;
	
	public GTAError updateSessionToken(String authToken);
	
	public GTAError updateSessionToken(String authToken, boolean shouldUpdateToken);
	
	public boolean deleteSessionToken(String userId, String device);
	
	public boolean requireValidateKaptchaCode(UserMaster userMaster);
	
	public boolean requireValidateKaptchaCode(String device);
	
	public boolean requireValidateLocation(String device);
	
	public boolean hasBeenLocked(UserMaster usermaster);
	
	public void updateLoginFailTime(UserMaster usermaster) throws Exception;
	
	public UserMaster getUserMasterByCustomerId(Long customerId);
	
	public void setAllLoginIdAndPassword(List<Member>customerIdList, String defaultPassword) throws Exception;

	public void SaveSessionToken(String userId, String authToken, String device, String appTypeCode) throws Exception;

	/**
	 * 线程保存心跳记录
	 */
	public void saveSessionTokenTask(String userId, String authToken, String device, String appTypeCode);

	/**
	 * 获取心跳检查列表
	 * @param page
	 * @param appTypeCode
	 * @return
	 */
	public ResponseResult getHealthCheckingList(ListPage<LoginSession> page, String appTypeCode);
	
	public ResponseResult doLogin(HttpServletRequest request, HttpServletResponse response,UserLoginRequest loginUser,String URI,String device);
}
