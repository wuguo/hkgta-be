package com.sinodynamic.hkgta.service.rpos;

import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface CustomerOrderDetService extends IServiceBase<CustomerOrderDet> {
	
	public CustomerOrderDet getCustomerOrderDet(Long orderNo);
	public CustomerOrderHd getCustomerOrderHdByOrderNo(Long orderNo);
	public void saveCustomerOrderDet(CustomerOrderDet customerOrderDet);
	
	
}
