package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.staff.NotificationDto;
import com.sinodynamic.hkgta.dto.staff.NotificationMemberDto;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
public interface NotificationCenterService extends IServiceBase{

	public ResponseResult getNotificationMemberList(NotificationDto notificationDto);
	
	public Map<String,String> saveEmailRecord(NotificationDto notificationDto,List<NotificationMemberDto> memList) throws Exception;

	public void sendNotificationByEmail(NotificationDto notificationDto,List<NotificationMemberDto> memList, Map<String, String> map);
	
	public ResponseResult sendNotificationBySMS(NotificationDto notificationDto);
	
	 public List<String> getMineTypes(String[] paths);
	
}