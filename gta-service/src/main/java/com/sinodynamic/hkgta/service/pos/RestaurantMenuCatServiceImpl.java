package com.sinodynamic.hkgta.service.pos;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pos.RestaurantMenuCatDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantMenuItemDao;
import com.sinodynamic.hkgta.dto.pos.RestaurantMenuCatDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuCat;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;

@Repository
public class RestaurantMenuCatServiceImpl implements RestaurantMenuCatService
{
	@Autowired
	private RestaurantMenuCatDao restaurantMenuCatDao;
	
	@Autowired
	private RestaurantMenuItemDao restaurantMenuItemDao;
	
	@Override
	@Transactional
	public List<RestaurantMenuCat> getRestaurantMenuCatList(String restaurantId)
	{
		return restaurantMenuCatDao.getRestaurantMenuCatListByRestaurantId(restaurantId);
	}

	@Override
	public String getRestaurantListByRestaurantId()
	{
		return restaurantMenuCatDao.getRestaurantListByRestaurantId();
	}

	@Override
	@Transactional
	public boolean updateRestaurantMenuCat(String restaurantId, String catId, String categoryName, String updateBy)
	{
		RestaurantMenuCat restaurantMenuCat = restaurantMenuCatDao.getRestaurantMenuCat(restaurantId, catId);
		restaurantMenuCat.setCategoryName(categoryName);
		restaurantMenuCat.setUpdateBy(updateBy);
		restaurantMenuCat.setUpdateDate(new Date());
		restaurantMenuCatDao.update(restaurantMenuCat);
		return true;
	}

	@Override
	@Transactional
	public RestaurantMenuCatDto getRestaurantMenuCat(String restaurantId, String catId)
	{
		RestaurantMenuCatDto restaurantMenuCatDto = new RestaurantMenuCatDto();
		RestaurantMenuCat restaurantMenuCat = restaurantMenuCatDao.getRestaurantMenuCat(restaurantId, catId);
		List<RestaurantMenuItem> restaurantMenuItemList = null;
		if (restaurantMenuCat!=null) {
			restaurantMenuItemDao.getByCol(RestaurantMenuItem.class, "restaurantMenuCat.catId", restaurantMenuCat.getCatId(), null);
			restaurantMenuCatDto.setCat_id(restaurantMenuCat.getCatId());
			restaurantMenuCatDto.setCategory_name(restaurantMenuCat.getCategoryName());
			restaurantMenuCatDto.setCnt(new BigInteger(restaurantMenuItemList==null ? "0" : Integer.toString(restaurantMenuItemList.size())));
		}
		return restaurantMenuCatDto;
	}

}
