package com.sinodynamic.hkgta.service.fms;

import com.sinodynamic.hkgta.entity.pms.RoomHousekeepStatusLog;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RoomHousekeepStatusLogService extends IServiceBase<RoomHousekeepStatusLog> {

	void saveRoomHousekeepStatusLog(RoomHousekeepStatusLog log);
}
