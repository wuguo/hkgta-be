package com.sinodynamic.hkgta.service.fms;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.fms.FacilityUtilizationRateTimeDao;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateDateDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateListDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateTimeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateTimeListDto;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateTime;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant.RateType;

@Service
public class FacilityUtilizationRateTimeServiceImpl extends ServiceBase<FacilityUtilizationRateTime> implements FacilityUtilizationRateTimeService
{

	@Autowired
	private FacilityUtilizationRateTimeDao facilityUtilizationRateTimeDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveFacilityUtilizationRateTime(FacilityUtilizationRateListDto rateDto,String userId)
	{
		for (FacilityUtilizationRateTimeListDto rateTime : rateDto.getDayRateList())
		{
			if (StringUtils.isEmpty(rateDto.getSubType()))
				facilityUtilizationRateTimeDao.removeFacilityUtilizationRateTime(rateDto.getFacilityType(), rateTime.getWeekDay());
			else
				facilityUtilizationRateTimeDao.removeFacilityUtilizationRateTime(rateDto.getFacilityType(), rateDto.getSubType(), rateTime.getWeekDay());
			saveRateTime(rateTime,rateDto.getFacilityType(),rateDto.getSubType(),userId);
		}
	}
	
	private void saveRateTime(FacilityUtilizationRateTimeListDto rateTime,String facilityType,String subType,String userId)
	{
		for (FacilityUtilizationRateTimeDto rateTimeDto : rateTime.getRateList())
		{
			if (RateType.HI.name().equals(rateTimeDto.getRateType()))
			{
				FacilityUtilizationRateTime facilityUtilizationRateTime = new FacilityUtilizationRateTime();
				facilityUtilizationRateTime.setCreateDate(new Timestamp(new Date().getTime()));
				facilityUtilizationRateTime.setCreateBy(userId);
				facilityUtilizationRateTime.setRateType(rateTimeDto.getRateType());
				facilityUtilizationRateTime.setBeginTime(rateTimeDto.getBeginTime());
				facilityUtilizationRateTime.setEndTime(rateTimeDto.getBeginTime() + 1);
				facilityUtilizationRateTime.setFacilityType(facilityType);
				facilityUtilizationRateTime.setFacilityDateId(null);
				facilityUtilizationRateTime.setWeekDay(rateTime.getWeekDay());
				if (!StringUtils.isEmpty(subType))
					facilityUtilizationRateTime.setFacilitySubtypeId(subType);
				facilityUtilizationRateTimeDao.save(facilityUtilizationRateTime);
			}
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeList(String facilityType, String weekDay, String subType)
	{
		return facilityUtilizationRateTimeDao.getFacilityUtilizationRateTimeList(facilityType, weekDay, subType);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeList(String facilityType)
	{
		return facilityUtilizationRateTimeDao.getFacilityUtilizationRateTimeList(facilityType);
	}

	@Override
	@Transactional
	public List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeListBySubType(String facilityType, String facilitySubtypeId)
	{
		return facilityUtilizationRateTimeDao.getFacilityUtilizationRateTimeListBySubType(facilityType, facilitySubtypeId);
	}

}
