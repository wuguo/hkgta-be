package com.sinodynamic.hkgta.service.pms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.pms.RoomDao;
import com.sinodynamic.hkgta.dao.pms.RoomFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dto.crm.GuessRoomReservationDetailDto;
import com.sinodynamic.hkgta.dto.crm.SourceBookingDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationCancelDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.HotelReservationItemInfoDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.HotelReservationPaymentInfoDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.rpos.RefundService;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.constant.GTAError.GuestRoomError;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.RefundServiceType;
import com.sinodynamic.hkgta.util.constant.ReservationType;
import com.sinodynamic.hkgta.util.constant.limitUnit;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.report.JasperUtil;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class HotelReservationServiceImpl extends ServiceBase implements
		HotelReservationService {
	static Logger log = Logger.getLogger(HotelReservationServiceImpl.class);
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private RoomReservationRecDao roomReservationRecDao;
	
	@Autowired
	private CustomerRefundRequestDao customerRefundRequestDao;
	
	@Autowired
	private RoomFacilityTypeBookingDao roomFacilityTypeBookingDao;
	
	@Autowired
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	//added by Kaster 20160328
	@Autowired
	private UserMasterDao userMasterDao;
	
    @Autowired
	private  RefundService refundService;
    
    @Autowired
	private PMSRequestProcessorService pmsRequestProcessorService;
    
    @Autowired
    private RoomDao roomDao;
    
	// TODO
	CustomerOrderTrans nonsplitPayment(HotelReservationPaymentDto paymentDto){
		Date currentDate = paymentDto.getCurrentDate();
		String staffUserId = paymentDto.getUserId();
		Long customerId = paymentDto.getCustomerId();
		Long orderNo = null;
		
		//added by Kaster 20160328
		if(paymentDto.getCreatedBy()!=null && !paymentDto.getCreatedBy().equals("")){
			UserMaster um = userMasterDao.get(UserMaster.class, paymentDto.getCreatedBy());
			if(um!=null && um.getUserType().equalsIgnoreCase("STAFF")){
				staffUserId = paymentDto.getCreatedBy();
			}
		}
		// CUSTOMER ORDER Header
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(currentDate);
		//modified by Kaster 20160331
		customerOrderHd.setStaffUserId(paymentDto.getUserId());
		customerOrderHd.setCustomerId(customerId);
		customerOrderHd.setOrderTotalAmount(getTotalOrderAmount(paymentDto.getHotelReservationItemsInfo()));
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
		customerOrderHd.setCreateBy(staffUserId);
		customerOrderHd.setUpdateBy(staffUserId);
		customerOrderHd.setUpdateDate(currentDate);
		if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH_Value) ||
			StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.PRE_AUTH) ||
			StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH)||
			StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.UNION_PAY)){
			customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
		}else{
			customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		}
		orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(customerOrderHd);
		
		CustomerOrderHd temp = customerOrderHdDao.getOrderById(orderNo);

		for(HotelReservationItemInfoDto itemInfo : paymentDto.getHotelReservationItemsInfo()){
			BigDecimal itemAmount = itemInfo.getAmountAfterTax();//.getItemAmount();
			
			
			// CUSTOMER ORDER Detail
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(temp);
			customerOrderDet.setItemNo(itemInfo.getItemNo());
			customerOrderDet.setItemRemark("");
			customerOrderDet.setOrderQty(itemInfo.getNights());
//			customerOrderDet.setItemTotalAmout(itemAmount.multiply(new BigDecimal(itemInfo.getNights())));
			customerOrderDet.setItemTotalAmout(itemAmount);
			customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderDet.setCreateBy(staffUserId);
			customerOrderDet.setUpdateDate(currentDate);
			customerOrderDet.setUpdateBy(staffUserId);
			customerOrderDet.setExtRefNo(itemInfo.getReservationId());
			customerOrderDetDao.saveOrderDet(customerOrderDet);

			RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", itemInfo.getReservationId());
			book.setStatus(RoomReservationStatus.PAY.name());
			book.setOrderNo(orderNo);
		}	
			
		Assert.isTrue(paymentDto.getHotelReservationPaymentsInfo().size() != 0, "No transaction is available");
		Assert.isTrue(paymentDto.getHotelReservationPaymentsInfo().size() == 1, "Multiple transactions are not available");
		HotelReservationPaymentInfoDto paymentInfo = paymentDto.getHotelReservationPaymentsInfo().get(0);
		
//		BigDecimal paidAmount = paymentInfo.getPaidAmount()==null ? 
//				getTotalOrderAmount(paymentDto.getHotelReservationItemsInfo()):paymentInfo.getPaidAmount();
//				
		BigDecimal paidAmount = getTotalOrderAmount(paymentDto.getHotelReservationItemsInfo());
						
		//UPDATE CASHVALUE
		if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH_Value)||
				StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.UNION_PAY)){
			MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
			BigDecimal deductedAmount = memberCashvalue.getAvailableBalance().subtract(paidAmount);
			
			if (memberCashvalue.getAvailableBalance().compareTo(paidAmount) < 0)
			{
				MemberLimitRule memberLimitRuleCR = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, LimitType.CR.name());
				MemberLimitRule memberLimitRuleTRN = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId, LimitType.TRN.name());
				
				MemberLimitRule memberLimitRule = memberLimitRuleCR!=null ? memberLimitRuleCR : memberLimitRuleTRN;
				
				if(null != memberLimitRule){
					BigDecimal numValue = memberLimitRule.getNumValue();
					if(((memberCashvalue.getAvailableBalance().subtract(paidAmount)).negate()).compareTo(numValue) > 0 ){
						throw new GTACommonException(GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_CREDITLIMIT,new Object[]{memberCashvalue.getAvailableBalance()});
					}
				}else{
					throw new GTACommonException(GTAError.MemberError.MEMBER_INSUFFICIENT_BALANCE);
				}
			}
			
			//Assert.isTrue(deductedAmount.compareTo(BigDecimal.ZERO)>=0, "The order amount can't be greater than Cash Value Balance");
			memberCashvalue.setAvailableBalance(deductedAmount);
			memberCashValueDao.updateMemberCashValue(memberCashvalue);
		}
		
		// CUSTOMER ORDER TRANS
		String agentTransactionNo = paymentInfo.getAgentTransactionNo();
		String terminalId = paymentInfo.getTerminalId();
		
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		initCustomerOrderTrans(customerOrderTrans);
		
		customerOrderTrans.setCustomerOrderHd(temp);
		customerOrderTrans.setTransactionTimestamp(currentDate);
		customerOrderTrans.setPaidAmount(paidAmount);
		//modified by Kaster 20160331
		customerOrderTrans.setPaymentRecvBy(paymentDto.getUserId());
		customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
		
		if(!StringUtils.isEmpty(terminalId)){
			initCustomerOrderTrans(customerOrderTrans);
		}
		if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH_Value)){
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASHVALUE.name());
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		}else if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH)){
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASH.name());
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		}else if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.PRE_AUTH)){
			customerOrderTrans.setStatus(Constant.Status.SUC.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.PREAUTH.name());
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
		}else if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CREDIT_CARD)){
            customerOrderTrans.setStatus(Constant.Status.PND.name());
            customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
            
            if(StringUtils.isNotEmpty(paymentDto.getTerminalType())&&
            		PaymentMediaType.ECR.name().equalsIgnoreCase(paymentDto.getTerminalType()))
            {
            	customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());	
            }else{
            	customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
            }
		}else if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), PaymentMethod.AMEX.getCardCode())){
            customerOrderTrans.setStatus(Constant.Status.PND.name());
            customerOrderTrans.setPaymentMethodCode(PaymentMethod.AMEX.getCardCode());
            if(StringUtils.isNotEmpty(paymentDto.getTerminalType())&&
            		PaymentMediaType.ECR.name().equalsIgnoreCase(paymentDto.getTerminalType()))
            {
            	customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());	
            }else{
            	customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
            }
		}
		else if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.UNION_PAY)){
            customerOrderTrans.setStatus(Constant.Status.PND.name());
            customerOrderTrans.setPaymentMethodCode(PaymentMethod.CUP.name());
            customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
		}
		
		//Only CASH , CASHVALUE, CREDITCARD need to be create txn record
		if(StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH_Value) ||
				StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CREDIT_CARD) ||
				StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.CASH)||
				StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), Constant.UNION_PAY)||
				StringUtils.equalsIgnoreCase(paymentDto.getPaymentMethod(), PaymentMethod.AMEX.getCardCode())){
			
			customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
		}
		
		
		return customerOrderTrans;
	}
	
	@Override
	@Transactional
	public CustomerOrderTrans hotelReservationOnlinePayment(final HotelReservationPaymentDto paymentDto) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (CustomerOrderTrans)executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				
				
				return nonsplitPayment(paymentDto);
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,this.getException().getMessage());
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
	}

	@Override
	@Transactional
	public MemberCashvalue getMemberCashvalue(Long customerId) {
		Member member = memberDao.get(Member.class, customerId);
		
		if (member == null) {
			throw new GTACommonException(GTAError.FacilityError.CUSTOMER_NOT_FOUND);
		}
		
		MemberCashvalue memberCashvalue = null;
		String type = member.getMemberType();

		if (Constant.memberType.CDM.toString().equals(type) || Constant.memberType.IDM.toString().equals(type)) {
			Long superiorMemberId = member.getSuperiorMemberId();

			if (superiorMemberId != null) {
				memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
			}
		} else {
			memberCashvalue = memberCashValueDao.getMemberCashvalueById(customerId);
		}

		if (memberCashvalue == null) {
			logger.error("member cash value not found for customer" + customerId);
			throw new GTACommonException(GTAError.FacilityError.REFUND_NOT_AVAILABLE);
		}
		return memberCashvalue;

	}
	@Override
	public BigDecimal getTotalOrderAmount(List<HotelReservationItemInfoDto> items){
		BigDecimal totalAmount = BigDecimal.ZERO;
		for(HotelReservationItemInfoDto item : items){
//			BigDecimal nights = new BigDecimal(item.getNights());
//			totalAmount = totalAmount.add(item.getItemAmount().multiply(nights));
			// FE  calculate amount nights for items
			totalAmount = totalAmount.add(item.getAmountAfterTax());
		}
		return totalAmount;
	}
	
	@Override
	@Transactional
	public CustomerOrderTrans hotelReservationCashvaluePayment(final HotelReservationPaymentDto paymentDto) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (CustomerOrderTrans)executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				
				
				return nonsplitPayment(paymentDto);
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,this.getException().getMessage());
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
	}

	@Override
	@Transactional
	public void bookHotel(final List<RoomReservationDto> reservations) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				for(RoomReservationDto reservation: reservations){
					RoomReservationRec resvRec = new RoomReservationRec();
					resvRec.setConfirmId(reservation.getReservationId());
					resvRec.setArrivalDate(DateUtils.parseDate(reservation.getStartDate(), new String[]{"yyyy-MM-dd"}));
					resvRec.setDepartureDate(DateUtils.parseDate(reservation.getEndDate(), new String[]{"yyyy-MM-dd"}));
					resvRec.setNight(reservation.getNoOfStayingNight());
					resvRec.setRoomTypeCode(reservation.getRoomTypeCode());
					resvRec.setCreateBy(reservation.getCreateBy());
					resvRec.setCreateDate(reservation.getCreateDate());
					resvRec.setUpdateBy(reservation.getUpdateBy());
					resvRec.setUpdateDate(reservation.getUpdateDate());
					resvRec.setCustomerId(reservation.getCustomerId());
					resvRec.setRequestDate(reservation.getRequestDate());
					resvRec.setStatus(Constant.RoomReservationStatus.RSV.name());
					resvRec.setRateCode(reservation.getRateCode());
					resvRec.setChildrenQty(reservation.getChildCount());
					resvRec.setAdultQty(reservation.getAdultCount());
					roomReservationRecDao.save(resvRec);
				}
				return null;
			}
		});
		
	}
	
	void _updateRoomReservationStatus(final String confirmId, final RoomReservationStatus status){
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){

			@Override
			public Object doTry() throws Exception {
				RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId);
				
				book.setStatus(status.name());
				roomReservationRecDao.save(book);
				
				return null;
			}
			
			@Override
			protected GTACommonException newTryException() {
				return new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR,this.getException().getMessage());
//				return new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{this.getException().getMessage()});
			}
		});
	}
	
	/**   
	* @author: Ray_Liang
	* @since: Nov 2, 2015
	* 
	* @description
	* Please call session.refresh() out of this method if you want to update the state of the entity 
	*/  
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void updateRoomReservationStatus(final String confirmId, final RoomReservationStatus status){
		_updateRoomReservationStatus(confirmId, status);
	}

	@Override
	@Transactional
	public List<RoomReservationRec> getAllFailedReservations() {
		try{
			return roomReservationRecDao.getAllFailedReservations(20);
		}catch(Exception e){
			log.error("getAllFailedReservations fail", e);
			throw new GTACommonException(GTAError.GuestRoomError.ROOM_RESERVATION_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}

	
	
	@Override
	@Transactional
	public List<RoomReservationRec> getAllFailedReservations(Integer timeoutMin) {
		try{
			return roomReservationRecDao.getAllFailedReservations(timeoutMin);
		}catch(Exception e){
			log.error("getAllFailedReservations(timeout) fail", e);
			throw new GTACommonException(GTAError.GuestRoomError.ROOM_RESERVATION_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}

	@Override
	@Transactional
	public void updateGuessroomAndFacilityAssociation(String loginUserId, Map<String, List<Long>> association, Map<String, Object> params) {
		try{
			Date now = new Date();
			if(null==association||association.keySet().size()==0)return ;
			for(String confirmId : association.keySet()){
				if(null==association.get(confirmId)||association.get(confirmId).size()==0)continue;
				for(Long facilityBookingId : association.get(confirmId)){
					RoomFacilityTypeBooking relation = new RoomFacilityTypeBooking();
					relation.setCreateBy(loginUserId);
					relation.setCreateDate(now);
					relation.setIsBundle("Y");
					relation.setFacilityTypeBookingId(facilityBookingId);
					
					RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId);
					relation.setRoomBookingId(new Long(book.getResvId()));
					roomFacilityTypeBookingDao.save(relation);
				}
			}
		}catch(Exception e){
			log.error("updateGuessroomAndFacilityAssociation fail", e);
			throw new GTACommonException(GTAError.GuestRoomError.ROOM_RESERVATION_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
	}

	@Override
	@Transactional
	public void updateRoomReservationStatusInSameTxn(String confirmId, RoomReservationStatus status) {
		_updateRoomReservationStatus(confirmId, status);
	}

	CustomerOrderDet getOriginDetail(RoomReservationRec resv){
		Assert.notNull(resv.getOrderNo(), "No order info for this reservation");
		
		List<CustomerOrderDet> details = customerOrderHdDao.getOrderById(resv.getOrderNo()).getCustomerOrderDets();
		CustomerOrderDet result = null;
		for(CustomerOrderDet detail : details){
			if(ObjectUtils.equals(detail.getExtRefNo(), resv.getConfirmId()) &&
					ObjectUtils.equals(detail.getCustomerOrderHd().getOrderNo(), resv.getOrderNo())	){
				result = detail;
				break;
			}
		}
		
		Assert.notNull(result, "No order item info for this reservation");
		return result;
	}
	
	BigDecimal getOriginItemAmount(RoomReservationRec resv){
		CustomerOrderDet det = getOriginDetail(resv);
		return det.getItemTotalAmout();
	}
	
	Long getOriginTxn(RoomReservationRec resv){
//		Long txnNo = getOriginTxnInfo(resv).getTransactionNo();
//		return txnNo;
		CustomerOrderTrans originTxnInfo = getOriginTxnInfo(resv);
		
		if(null != originTxnInfo){
			return originTxnInfo.getTransactionNo();
		}
		return null;
	}
	
	CustomerOrderTrans getOriginTxnInfo(RoomReservationRec resv){
//		Assert.notNull(resv.getOrderNo(), "No order info for this reservation");
//		List<CustomerOrderTrans> txns = customerOrderHdDao.getOrderById(resv.getOrderNo()).getCustomerOrderTrans();
//		Assert.notEmpty(txns, "No transaction info for this reservation");
//		return txns.get(0);
		
		if(null != resv.getOrderNo()){
			Long orderNo = resv.getOrderNo();
			CustomerOrderHd order = customerOrderHdDao.getOrderById(orderNo);
			
			List<CustomerOrderTrans> txns = order.getCustomerOrderTrans();
			
			if(null != txns && txns.size() > 0){
				return txns.get(0);
			}
			
			return null;
		}
		return null;
	}
	
	/**   
	* transactionItemDetail  -->  CustomerOrderDet
	* transactionDetail  -->  CustomerOrderTrans 
	* @since: Jan 11, 2016
	* 
	*/  
	@Override
	@Transactional
	public Map<String, Object> exposeTxnInfo(String confirmId) {
		Map<String, Object> txnInfo = new HashMap<String, Object>();
		RoomReservationRec book = roomReservationRecDao.getReservationByConfirmId(confirmId);
		
		txnInfo.put("transactionItemDetail", getOriginDetail(book));
		txnInfo.put("transactionDetail", getOriginTxnInfo(book));
		
		return txnInfo;
	}

	Long getOriginDetailItem(RoomReservationRec resv){
		CustomerOrderDet det = getOriginDetail(resv);
		return det.getOrderDetId();
	}
	
	@Override
	@Transactional
	public void requestCancelPayment(String confirmId, HotelReservationCancelDto cancelDto) {
		try{
			RoomReservationRec book = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId.toString());
			
			CustomerRefundRequest refundRequest = new CustomerRefundRequest();
			/*
			refundRequest.setRefundTransactionNo(getOriginTxn(book));
			refundRequest.setRequesterType(cancelDto.getRequesterType());
			refundRequest.setRefundServiceType(RefundServiceType.ROOM.name());
			refundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
			refundRequest.setCustomerReason(cancelDto.getCancelReason());
			refundRequest.setStatus(Status.PND.name());
			refundRequest.setRequestAmount(getOriginItemAmount(book));
			refundRequest.setCreateBy(cancelDto.getUserId());
			refundRequest.setCreateDate(cancelDto.getCurrentDate());
			refundRequest.setOrderDetailId(getOriginDetailItem(book));
			customerRefundRequestDao.save(refundRequest);
			*/
			if(null != getOriginTxn(book)){
				refundRequest.setRefundTransactionNo(getOriginTxn(book));
				refundRequest.setRequesterType(cancelDto.getRequesterType());
				refundRequest.setRefundServiceType(RefundServiceType.ROOM.name());
				refundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
				refundRequest.setCustomerReason(cancelDto.getCancelReason());
				//check refund period
//				sendMemberCashValue(book, refundRequest);
				Timestamp startTime = new Timestamp(book.getArrivalDate().getTime());
				if(memberFacilityTypeBookingService.checkRefundDate(startTime, null, "guestRoom"))
				{
					refundRequest.setStatus(Constant.CustomerRefundRequest.APR.getName());
					/***
					 * udpate old refund method
					 */
					// refundService.refund(getOriginTxn(book), book.getCustomerId(), getOriginItemAmount(book), cancelDto.getUserId(), null);
					Long orderDetailId=getOriginDetailItem(book);
					CustomerOrderDet det=customerOrderDetDao.get(CustomerOrderDet.class, orderDetailId);
					if(null!=det){
						refundService.refund(getOriginTxn(book), book.getCustomerId(), getOriginItemAmount(book), cancelDto.getUserId(),det.getExtRefNo(), null);
					}else{
						refundService.refund(getOriginTxn(book), book.getCustomerId(), getOriginItemAmount(book), cancelDto.getUserId(), null);
					}
					refundRequest.setApprovedAmount(getOriginItemAmount(book));
					//add send auto refund mail notice
					memberFacilityTypeBookingService.sendCancelAutoRefundMail(book.getCustomerId(), ReservationType.ROOM.getName()+"-"+ book.getResvId(),
							DateConvertUtil.parseDate2String(book.getArrivalDate(), "yyyy-MM-dd"),
							RefundServiceType.ROOM.getDesc(), getOriginItemAmount(book).toString());
					
				}else{
					refundRequest.setStatus(Constant.CustomerRefundRequest.PND.getName());
				}
				refundRequest.setRequestAmount(getOriginItemAmount(book));
				refundRequest.setCreateBy(cancelDto.getUserId());
				refundRequest.setCreateDate(cancelDto.getCurrentDate());
				refundRequest.setOrderDetailId(getOriginDetailItem(book));
				
				Long refundId=(Long)customerRefundRequestDao.save(refundRequest);
				
				
				/***
				 * SGG-2858
				 * send mail to accountant 
				 * All the new refund request which status is "Pending" "No Refund" will be sent to the above recipient email
				 */
				if( Constant.CustomerRefundRequest.PND.getName().equalsIgnoreCase(refundRequest.getStatus())||
						 Constant.CustomerRefundRequest.NRF.getName().equalsIgnoreCase(refundRequest.getStatus()))
				{
				      refundService.sendServiceRefundMail(Constant.TEMPLATE_ID_REFUND_REQUEST_RECIPIENT_EMAIL, GlobalParameterType.REFUNDREQUESTRECIPIENTMAIL.getCode(),refundId);
				}
				
				
			}

		}catch(Exception e){
			log.error("requestCancelPayment fail", e);
			throw new GTACommonException(GTAError.GuestRoomError.ROOM_RESERVATION_RELATED_ERROR);
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION,new String[]{e.getMessage()});
		}
		
	}
	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Long> getBundledFacilityBookings(final String confirmId) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (List<Long>)executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				RoomReservationRec book = roomReservationRecDao.getReservationByConfirmId(confirmId);
				List<RoomFacilityTypeBooking> bundles = roomFacilityTypeBookingDao.getByHql("from RoomFacilityTypeBooking where roomBookingId = ?", Arrays.<Serializable>asList(book.getResvId()) );
				
				List<Long> facilityResvIdList = new ArrayList<Long>();
				
				for(RoomFacilityTypeBooking bundle : bundles){
					MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBookingIncludeAllStatus(bundle.getFacilityTypeBookingId());//get facility booking
					
					facilityResvIdList.add(booking.getResvId());
				}
				
				return facilityResvIdList;
			}
		});
	}

	@Override
	@Transactional
	public GuessRoomReservationDetailDto getHotelReservationDetail(final String confirmId) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (GuessRoomReservationDetailDto)executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				RoomReservationRec book = roomReservationRecDao.getReservationByConfirmId(confirmId);
				GuessRoomReservationDetailDto guessRoomDetail = new GuessRoomReservationDetailDto();
				guessRoomDetail.setArrivalDate(book.getArrivalDate());
				guessRoomDetail.setDepartureDate(book.getDepartureDate());
				guessRoomDetail.setItemAmount(getOriginItemAmount(book));
				guessRoomDetail.setNights(book.getNight());
				guessRoomDetail.setRoomType(book.getRoomTypeCode());
				guessRoomDetail.setStatus(book.getStatus());
				
				List<Date[]> golfingBayPeriod = new ArrayList<Date[]>();
				List<Long> resvIds = getBundledFacilityBookings(confirmId);
				for(Long id : resvIds){
					MemberFacilityTypeBooking booking = memberFacilityTypeBookingDao.getMemberFacilityTypeBookingIncludeAllStatus(id);
					
					List<FacilityTimeslot> timeslots = booking.getFacilityTimeslots();
					
					if(CollectionUtils.isNotEmpty(timeslots)){
						FacilityTimeslot slot = timeslots.get(0);//This facility booking only has one timeslot
						Date[] begin_end = new Date[2];
						begin_end[0] = slot.getBeginDatetime();
						begin_end[1] = slot.getEndDatetime();
						golfingBayPeriod.add(begin_end);
					}
				}
				
				guessRoomDetail.setGolfingBayPeriod(golfingBayPeriod);
				return guessRoomDetail;
			}
		});
	}

	@Override
	@Transactional
	public void cancelBundledFacilityBookings(final String confirmId, final HotelReservationCancelDto cancelDto) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				for(Long facilityResvId : getBundledFacilityBookings(confirmId)){
					memberFacilityTypeBookingService.cancelMemberFacilityTypeBooking(facilityResvId, cancelDto.getUserId(), cancelDto.getCancelReason(), cancelDto.getRequesterType(), "false");
				}
				return null;
			}
		});
	}

	@Override
	@Transactional
	public Long getRefundIdIfAvailable(final String confirmId) {
		CallBackExecutor executor = new CallBackExecutor(HotelReservationServiceImpl.class);
		
		return (Long)executor.execute(new AbstractCallBack(){
			@Override
			public Object doTry() throws Exception {
				RoomReservationRec roomReservationRec = roomReservationRecDao.getReservationByConfirmId(confirmId);
				Long detailItemId = getOriginDetailItem(roomReservationRec);
				CustomerRefundRequest refundRequest = customerRefundRequestDao.getUniqueByCol(CustomerRefundRequest.class, "orderDetailId", detailItemId);
				if(refundRequest == null){
					return null;
				}else{
					return refundRequest.getRefundId();
				}
			}
		});
		
	}
	
	/**
	 * 根据resvId查询预定好的房间信息
	 * @param resvId
	 * @return RoomReservationRec
	 */
	@Override
	@Transactional
	public RoomReservationRec getReservationByResvId(String resvId) {
		return roomReservationRecDao.getReservationByResvId(resvId);
	}
	
	/**
	 * 根据memberId查询预定好的房间信息
	 * @param memberId
	 * @return RoomReservationRec
	 */
	@Override
	@Transactional
	public List<RoomReservationRec> getReservationByCustomerID(Long customerid) {
		return roomReservationRecDao.getReservationByCustomerID(customerid);
	}
	
	/**
	 * 根据resvId查询预定好的房间信息
	 * @param resvId
	 * @return RoomReservationRec
	 */
	@Override
	@Transactional
	public List<RoomReservationRec> getReservationByOrderNo(Long orderNo) {
		return roomReservationRecDao.getReservationByOrderNo(orderNo);
	}
	
	@Override
	@Transactional
	public RoomReservationRec getReservationByConfirmId(String confirmId) {
		// TODO Auto-generated method stub
		RoomReservationRec rec = this.roomReservationRecDao.getReservationByConfirmId(confirmId);
		return rec;
	}
	
	@Override
	@Transactional
	public boolean checkRoomOccupy(Room room, RoomReservationRec rec) {
		// TODO Auto-generated method stub
		Date date = rec.getArrivalDate();
		while(true){
			if(this.roomReservationRecDao.checkOccupyByDateAndRoomId(date, room.getRoomId())){
				return true;
			}
			date = DateCalcUtil.getNearDay(date, 1);
			if(date.equals(rec.getDepartureDate())){
				return false;
			}
		}
	}
	@Transactional
	public void preAssignRoom(Long roomId, RoomReservationRec rec, String updateBy) {
		// TODO Auto-generated method stub
		rec.setRoomId(roomId);
		rec.setUpdateDate(new Date());
		rec.setUpdateBy(updateBy);
		this.roomReservationRecDao.update(rec);
		
	}

	@Override
	@Transactional
	public ResponseResult extendGuestRoomStay(Long reservationId, Long night, String userId,String roomNo) {
		
		if(!StringUtils.isEmpty(roomNo))
		{
			Room room=roomDao.getByRoomNo(roomNo);
			if(null==room){
				responseResult.initResult(GuestRoomError.ROOM_NOT_EXIST);
				return responseResult;
			}
			RoomReservationRec rec = roomReservationRecDao.getReservationByResvId(reservationId);     
			try {
				if (null != rec && null != rec.getDepartureDate()) {
					Date oldDepartureDate = rec.getDepartureDate();
					Date extendDate = DateConvertUtil.parseString2Date(
							DateCalcUtil.getDateAfter(rec.getDepartureDate(), Integer.valueOf(night.toString())),
							"yyyy-MM-dd");
					
					if(validRoomNoExtend(room.getRoomId(), oldDepartureDate, extendDate))
					{
						responseResult.initResult(GuestRoomError.ROOM_OCCUPY,new String[]{roomNo});
						return responseResult;
						
					}else{
						rec.setDepartureDate(extendDate);
						rec.setUpdateBy(userId);
						rec.setUpdateDate(new Date());
						rec.setNight(rec.getNight() + night);
						roomReservationRecDao.saveOrUpdate(rec);
						this.bundleGolf(rec,oldDepartureDate);
						responseResult.initResult(GTAError.Success.SUCCESS);
						
					}
				}
			} catch (Exception ex) {
				responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION,ex.getMessage());
			}
		}else{
			responseResult.initResult(GuestRoomError.ROOM_NOT_EXIST,"roomNo is not null");
		}
	
		return responseResult;
	}
	/***
	 * check roomNo in extend night ,is OC
	 * 
	 */
	public boolean validRoomNoExtend(Long roomId,Date departureDate,Date extendDate)
	{
		String sql="SELECT confirm_id as confirmId FROM room_reservation_rec WHERE room_id =?  AND ( (? <arrival_date  AND  arrival_date <= ?) OR ( ? < departure_date  AND departure_date <= ?))";
		 List<Serializable> param=new ArrayList<>();
		 param.add(roomId);
		 param.add(departureDate);
		 param.add(extendDate);
		 param.add(departureDate);
		 param.add(extendDate);
		List<RoomReservationRec> rec=roomReservationRecDao.getDtoBySql(sql, param, RoomReservationRec.class);
		if(null!=rec&&rec.size()>0){
			return true ;
		}else{
			return false;
		}
		
	}
	/***
	 * bundle the golfing bay accordingly
	 */
	private void bundleGolf(RoomReservationRec rec, Date arrivalDate) {
		if (rec != null) {
			List<RoomReservationInfoDto> bundles = new ArrayList<RoomReservationInfoDto>();
			RoomReservationInfoDto bundle = new RoomReservationInfoDto();
			bundle.setReservationId(rec.getConfirmId());
			bundle.setArriveDate(arrivalDate);
			bundle.setDepartDate(rec.getDepartureDate());
			bundle.setEarlestBay("true");
			bundles.add(bundle);
			Map<String, List<Long>> association = pmsRequestProcessorService
					.giftBundleForReservationsOneNightOneBooking(rec.getCustomerId(), bundles);
			
			if (rec.getUpdateBy() != null && !rec.getUpdateBy().equals("")) {
				UserMaster um = userMasterDao.get(UserMaster.class, rec.getUpdateBy());
				if (um != null && um.getUserType().equalsIgnoreCase("STAFF")) {
					updateGuessroomAndFacilityAssociation(um.getUserId(), association, new HashMap<String, Object>());
				} else {
					updateGuessroomAndFacilityAssociation(rec.getUpdateBy(), association,
							new HashMap<String, Object>());
				}
			} else {
				updateGuessroomAndFacilityAssociation(rec.getCreateBy(), association, new HashMap<String, Object>());
			}
		}
	}
	/**
	 * 验证该Member 的cash value或credit limit是否足以预定任何房间
	 * @param paymentDto
	 * @throws GTACommonException
	 */
	@Override
	@Transactional
	public void verificationCashValue(HotelReservationPaymentDto paymentDto) throws GTACommonException{
		Long customerId = paymentDto.getCustomerId();
		//for (HotelReservationItemInfoDto itemInfo : paymentDto.getHotelReservationItemsInfo()) {
			//BigDecimal itemAmount = itemInfo.getItemAmount();
			//HotelReservationPaymentInfoDto paymentInfo = paymentDto.getHotelReservationPaymentsInfo().get(0);
			//BigDecimal paidAmount = paymentInfo.getPaidAmount() == null? getTotalOrderAmount(paymentDto.getHotelReservationItemsInfo()) : paymentInfo.getPaidAmount();
		
		    //calculate total payment amount
			BigDecimal paidAmount = getTotalOrderAmount(paymentDto.getHotelReservationItemsInfo());
			
			MemberCashvalue memberCashvalue = getMemberCashvalue(customerId);
			//BigDecimal deductedAmount = memberCashvalue.getAvailableBalance().subtract(paidAmount);

			if (memberCashvalue.getAvailableBalance().compareTo(paidAmount) < 0) {
				MemberLimitRule memberLimitRuleCR = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId,
						LimitType.CR.name());
				MemberLimitRule memberLimitRuleTRN = memberLimitRuleDao.getEffectiveMemberLimitRule(customerId,
						LimitType.TRN.name());
				
				//check spending limit if dependent patron
				Member member = memberDao.getMemberById(customerId);
				String memberType = member.getMemberType();
				if(!StringUtils.isEmpty(memberType)){
					if((Constant.memberType.IDM.toString().equalsIgnoreCase(memberType) || Constant.memberType.CDM.toString().equalsIgnoreCase(memberType)) && 
							(null != member.getSuperiorMemberId() && member.getSuperiorMemberId() >0) ){
						if(null != memberLimitRuleTRN){
							BigDecimal numValue = memberLimitRuleTRN.getNumValue();
							if(limitUnit.EACH.name().equals(memberLimitRuleTRN.getLimitUnit())){
								if(null!= numValue && paidAmount.compareTo(numValue) > 0)
									throw new GTACommonException(GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_PERPAYMENT,new Object[]{numValue});
							}
						}
						memberLimitRuleCR = memberLimitRuleDao.getEffectiveMemberLimitRule(member.getSuperiorMemberId(),
								LimitType.CR.name());
					}
				}
				
                //check credit limit of primary patron
				if (null != memberLimitRuleCR) {
					BigDecimal numValue = memberLimitRuleCR.getNumValue();
					if (((memberCashvalue.getAvailableBalance().subtract(paidAmount)).negate())
							.compareTo(numValue) > 0) {
						throw new GTACommonException(GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_CREDITLIMIT,new Object[]{memberCashvalue.getAvailableBalance()});
					}
				} else {
					throw new GTACommonException(GTAError.MemberError.MEMBER_INSUFFICIENT_BALANCE);
				}
			}
		//}
	}
}
