package com.sinodynamic.hkgta.service.crm.sales;

import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.service.IServiceBase;

public interface UploadFileService extends IServiceBase<String> {
	
	public String uploadFiles(MultipartFile file);
}
