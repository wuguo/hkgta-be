package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.AgeRangeDao;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class AgeRangeServiceImpl  extends ServiceBase<AgeRange> implements AgeRangeService {
	
	@Autowired
	private AgeRangeDao ageRangeDao;
	@Transactional
	@Override
	public List<AgeRange> getAllAgeRange() {
		String hqlstr = "FROM AgeRange";
		return this.ageRangeDao.excuteByHql(AgeRange.class, hqlstr);
	}

}
