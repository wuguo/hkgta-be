package com.sinodynamic.hkgta.service.pms;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.StaffRosterDao;
import com.sinodynamic.hkgta.dto.pms.StaffRosterDto;
import com.sinodynamic.hkgta.dto.pms.StaffRosterRecordDto;
import com.sinodynamic.hkgta.dto.pms.StaffRosterTimeDto;
import com.sinodynamic.hkgta.entity.pms.StaffRoster;
import com.sinodynamic.hkgta.util.CalendarUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

/**
 * @author Mason_Yang
 *
 */
@Service
public class StaffRosterServiceImpl implements StaffRosterService {

	private static final String DATE_FORMAT = "yyyy-MM-dd";

	private static final String MONTH_FORMAT = "yyyy-MM";

	@Autowired
	private StaffRosterDao staffRosterDao;

	@Transactional
	@Override
	public StaffRosterDto getStaffRoster(String staffUserId, String beginDate) {
		StaffRosterDto staffRosterDto = new StaffRosterDto();

		// validate begin date
		try {
			DateFormat df = new SimpleDateFormat(DATE_FORMAT);
			Date date = df.parse(beginDate);

			Calendar lastSundayCalendar = CalendarUtil.getLastSunday(date);
			Date lastSunday = lastSundayCalendar.getTime();
			Calendar saturdayCalendar = CalendarUtil.getNextSaturday(lastSunday);
			setStaffRosterRecord(staffUserId, staffRosterDto, lastSundayCalendar, saturdayCalendar);

		} catch (ParseException e) {
			throw new GTACommonException(GTAError.HouseKeepError.ROSTER_GET_ERROR);
		}

		return staffRosterDto;
	}

	@Transactional
	@Override
	public void save(StaffRosterDto staffRosterDto, String userId) {

		try {
			String staffUserId = staffRosterDto.getStaffUserId();

			List<StaffRosterRecordDto> staffRosterRecordList = staffRosterDto.getStaffRosterRecord();
			if (staffUserId == null) {
				throw new GTACommonException(GTAError.HouseKeepError.ROSTER_GET_ERROR);
			} else if (staffRosterRecordList == null || staffRosterRecordList.isEmpty()) {
				throw new GTACommonException(GTAError.HouseKeepError.ROSTER_GET_ERROR);
			}

			for (StaffRosterRecordDto recordDto : staffRosterRecordList) {
				Date date = DateUtils.parseDate(recordDto.getOnDate(), new String[] { DATE_FORMAT });

				deleteRosterByOnDate(staffUserId, date);
				List<StaffRosterTimeDto> timeList = recordDto.getTimeList();
				if (timeList != null && !timeList.isEmpty()) {

					for (StaffRosterTimeDto timeDto : timeList) {
						StaffRoster staffRoster = new StaffRoster();
						staffRoster.setStaffUserId(staffUserId);
						staffRoster.setCreateDate(new Date());
						staffRoster.setCreateBy(userId);
						staffRoster.setOnDate(date);
						staffRoster.setEndTime(timeDto.getEndTime());
						staffRoster.setBeginTime(timeDto.getBeginTime());
						staffRoster.setOffDuty(recordDto.getOffDuty());
						staffRosterDao.save(staffRoster);
					}

				} else if (recordDto.getOffDuty()) {
					StaffRoster staffRoster = new StaffRoster();
					staffRoster.setStaffUserId(staffUserId);
					staffRoster.setCreateDate(new Date());
					staffRoster.setCreateBy(userId);
					staffRoster.setOnDate(date);
					staffRoster.setOffDuty(recordDto.getOffDuty());
					staffRosterDao.save(staffRoster);
				}

			}
		} catch (ParseException e) {
			throw new GTACommonException(GTAError.HouseKeepError.ROSTER_GET_ERROR);
		}

	}

	private List<StaffRosterTimeDto> getTimeList(Map<String, List<StaffRoster>> timeMap, String dateStr,
			StaffRosterRecordDto recordDto) {
		List<StaffRoster> value = timeMap.get(dateStr);
		List<StaffRosterTimeDto> timeList = new ArrayList<>();
		if (value != null) {

			for (StaffRoster staffRoster : value) {
				StaffRosterTimeDto staffRosterTimeDto = new StaffRosterTimeDto();

				staffRosterTimeDto.setBeginTime(staffRoster.getBeginTime());
				staffRosterTimeDto.setEndTime(staffRoster.getEndTime());
				recordDto.setOffDuty(staffRoster.getOffDuty());
				timeList.add(staffRosterTimeDto);
			}

		}
		return timeList;
	}

	private void deleteRosterByOnDate(String staffUserId, Date date) {
		List<StaffRoster> staffRosterList = staffRosterDao.getStaffRoster(staffUserId, date);
		if (staffRosterList != null) {
			for (StaffRoster staffRoster : staffRosterList) {
				staffRosterDao.delete(staffRoster);
			}
		}
	}

	private Map<String, List<StaffRoster>> getTimeMap(List<StaffRoster> staffRosterList) {
		Map<String, List<StaffRoster>> timeMap = new HashMap<>();
		for (StaffRoster staffRoster : staffRosterList) {
			String dateStr = DateFormatUtils.format(staffRoster.getOnDate(), DATE_FORMAT);

			List<StaffRoster> value = timeMap.get(dateStr);
			if (value == null) {
				value = new ArrayList<>();
			}
			value.add(staffRoster);
			timeMap.put(dateStr, value);
		}
		return timeMap;
	}

	@Transactional
	@Override
	public StaffRosterDto getStaffRosterByMonth(String staffUserId, String month) {
		StaffRosterDto staffRosterDto = new StaffRosterDto();
		// validate begin date
		try {
			DateFormat df = new SimpleDateFormat(MONTH_FORMAT);
			Date date = df.parse(month);

			Calendar firstDayCal = Calendar.getInstance();
			firstDayCal.setTime(date);
			firstDayCal.set(Calendar.DAY_OF_MONTH, 1);

			Calendar lastDayCal = Calendar.getInstance();
			lastDayCal.setTime(date);
			lastDayCal.set(Calendar.DATE, lastDayCal.getActualMaximum(Calendar.DATE));

			setStaffRosterRecord(staffUserId, staffRosterDto, firstDayCal, lastDayCal);

		} catch (ParseException e) {
			throw new GTACommonException(GTAError.HouseKeepError.ROSTER_GET_ERROR);
		}

		return staffRosterDto;
	}

	private void setStaffRosterRecord(String staffUserId, StaffRosterDto staffRosterDto, Calendar firstDayCal,
			Calendar lastDayCal) {
		Date firstDayOfMonth = firstDayCal.getTime();
		Date lastDayOfMonth = lastDayCal.getTime();

		List<StaffRoster> staffRosterList;
		staffRosterList = staffRosterDao.getStaffRoster(staffUserId, firstDayOfMonth, lastDayOfMonth);
		staffRosterDto.setStaffUserId(staffUserId);
		Map<String, List<StaffRoster>> timeMap = getTimeMap(staffRosterList);

		List<StaffRosterRecordDto> staffRosterRecordList = new ArrayList<>();

		do {
			Date indexDate = firstDayCal.getTime();
			String dateStr = DateFormatUtils.format(indexDate, DATE_FORMAT);
			StaffRosterRecordDto recordDto = new StaffRosterRecordDto();
			recordDto.setOnDate(dateStr);

			List<StaffRosterTimeDto> timeList = getTimeList(timeMap, dateStr, recordDto);
			recordDto.setTimeList(timeList);
			staffRosterRecordList.add(recordDto);
			firstDayCal.add(Calendar.DAY_OF_MONTH, 1);
		} while (firstDayCal.compareTo(lastDayCal) <= 0);

		staffRosterDto.setStaffRosterRecord(staffRosterRecordList);
	}

}
