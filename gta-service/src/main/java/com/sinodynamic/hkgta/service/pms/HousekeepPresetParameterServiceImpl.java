package com.sinodynamic.hkgta.service.pms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.pms.HousekeepPresetParameterDao;
import com.sinodynamic.hkgta.dto.pms.HousekeepPresetParameterDto;
import com.sinodynamic.hkgta.entity.pms.HousekeepPresetParameter;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Service
public class HousekeepPresetParameterServiceImpl extends
		ServiceBase<HousekeepPresetParameter> implements HousekeepPresetParameterService {
	@Autowired
	private HousekeepPresetParameterDao housekeepPresetParameterDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<HousekeepPresetParameter> getHousekeepPresetParameterList() {
		return housekeepPresetParameterDao.getHousekeepPresetParameterList();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<HousekeepPresetParameter> getHousekeepPresetParameterCatList() {
		return housekeepPresetParameterDao.getHousekeepPresetParameterCatList();
	}

	@Override
	@Transactional
	public ListPage<HousekeepPresetParameter> getHousekeepPresetParameterList(HousekeepPresetParameterDto queryParam,String sortBy, boolean isDesc, ListPage<HousekeepPresetParameter> pListPage) throws Exception
	{
		StringBuffer hql = new StringBuffer();
		StringBuffer hqlCount = new StringBuffer();
		hql.append("select p from HousekeepPresetParameter p where p.paramId > 0");
		hqlCount.append("select count(*) from HousekeepPresetParameter p where p.paramId > 0");
		List<Serializable> param = new ArrayList<Serializable>();
		if(!StringUtils.isEmpty(queryParam.getParamCat())){
			hql.append(" and p.paramCat like ? ");
			hqlCount.append(" and p.paramCat like ? ");
			param.add("%"+queryParam.getParamCat().trim()+"%");
		}else if(!StringUtils.isEmpty(queryParam.getDescription())){
			hql.append(" and p.description like ? ");
			hqlCount.append(" and p.description like ? ");
			param.add("%"+queryParam.getDescription().trim()+"%");
		}else if(!StringUtils.isEmpty(queryParam.getDisplayScope())){
			hql.append(" and p.displayScope = ? ");
			hqlCount.append(" and p.displayScope = ? ");
			param.add(queryParam.getDisplayScope().trim());
		}else if(!StringUtils.isEmpty(queryParam.getMaxInt())){
			hql.append(" and p.maxInt = ? ");
			hqlCount.append(" and p.maxInt = ? ");
			param.add(queryParam.getMaxInt());
		}
		
		String orderByCol = "";
		switch (sortBy)
		{
		case "paramCat":
			orderByCol = "p.paramCat";
			break;
		case "description":
			orderByCol = "p.description";
			break;
		case "maxInt":
			orderByCol = "p.maxInt";
			break;
		case "displayScope":
			orderByCol = "p.displayScope";
			break;
		}
		if (!isDesc)
		{
			pListPage.addDescending(orderByCol);
		} else
		{
			pListPage.addAscending(orderByCol);
		}
		pListPage = housekeepPresetParameterDao.getHousekeepPresetParameterList(pListPage, hql.toString(), hqlCount.toString(), param);
		return pListPage;
	
	}

	@Override
	@Transactional
	public HousekeepPresetParameter saveHousekeepPresetParameter(HousekeepPresetParameterDto parameterDto)
	{
		HousekeepPresetParameter housekeepPresetParameter = new HousekeepPresetParameter();
		housekeepPresetParameter.setParamCat(parameterDto.getParamCat());
		housekeepPresetParameter.setDescription(parameterDto.getDescription());
		housekeepPresetParameter.setMaxInt(parameterDto.getMaxInt());
		housekeepPresetParameter.setDisplayScope(parameterDto.getDisplayScope());
		housekeepPresetParameter.setStatus(Constant.General_Status_ACT);
		housekeepPresetParameter.setCreateBy(parameterDto.getUpdateBy());
		housekeepPresetParameter.setCreateDate(new Timestamp(new Date().getTime()));
		housekeepPresetParameterDao.save(housekeepPresetParameter);
		return housekeepPresetParameter;
	}

	@Override
	@Transactional
	public HousekeepPresetParameter editHousekeepPresetParameter(HousekeepPresetParameterDto parameterDto)
	{
		HousekeepPresetParameter housekeepPresetParameter = housekeepPresetParameterDao.get(HousekeepPresetParameter.class, parameterDto.getParamId());
		if(null != housekeepPresetParameter){
			housekeepPresetParameter.setParamCat(parameterDto.getParamCat());
			housekeepPresetParameter.setDescription(parameterDto.getDescription());
			housekeepPresetParameter.setMaxInt(parameterDto.getMaxInt());
			housekeepPresetParameter.setDisplayScope(parameterDto.getDisplayScope());
			housekeepPresetParameter.setUpdateBy(parameterDto.getUpdateBy());
			housekeepPresetParameter.setUpdateDate(new Date());
			housekeepPresetParameterDao.update(housekeepPresetParameter);
		}
		return housekeepPresetParameter;
	}

	@Override
	@Transactional
	public HousekeepPresetParameter getHousekeepPresetParameter(Long paramId)
	{
		return housekeepPresetParameterDao.get(HousekeepPresetParameter.class, paramId);
	}

	@Override
	@Transactional
	public void deleteHousekeepPresetParameter(Long paramId, String userId)
	{
		HousekeepPresetParameter housekeepPresetParameter = housekeepPresetParameterDao.get(HousekeepPresetParameter.class, paramId);
		if(null != housekeepPresetParameter){
			housekeepPresetParameter.setStatus(Constant.General_Status_NACT);
			housekeepPresetParameter.setUpdateBy(userId);
			housekeepPresetParameter.setUpdateDate(new Date());
			housekeepPresetParameterDao.update(housekeepPresetParameter);
		}
	}

}
