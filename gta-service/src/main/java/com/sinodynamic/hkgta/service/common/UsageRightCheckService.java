package com.sinodynamic.hkgta.service.common;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.TrainingCode;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.TrainingRight;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * Only for Service Plan
 * 
 * @author: Ray_Liang
 * @since: Nov 25, 2015
 */

public interface UsageRightCheckService extends IServiceBase {
	enum FacilityCode {
		GOLF, GUEST, GYM, SWIM, TENNIS, WELLNESS, DAYPASS
	};

	enum TrainingCode {

		TRAIN1("Golf Courses"), TRAIN2("Golf Private Coaching"), TRAIN3("Tennis Courses"), TRAIN4(
				"Tennis Private Coaching");

		private String description;

		TrainingCode(String description) {
			this.description = description;
		}

		public String getDescription() {
			return this.description;
		}
	};

	enum OtherRightCode {
		D1("Day Pass Purchasing"), EVENT1("Events"), G1("Dependent Creation");

		private String description;

		OtherRightCode(String description) {
			this.description = description;
		}

		public String getDescription() {
			return this.description;
		}
	};

	class Right {
		private boolean canBook = false;

		public boolean isCanBook() {
			return canBook;
		}

		public void setCanBook(boolean canBook) {
			this.canBook = canBook;
		}
	}

	class FacilityRight extends Right {
		private boolean canAccess = false;

		public boolean isCanAccess() {
			return canAccess;
		}

		public void setCanAccess(boolean canAccess) {
			this.canAccess = canAccess;
		}
	}

	class TrainingRight extends Right {
	}

	class OtherRight extends Right {
		private Object additionalAttribute = null;

		public Object getAdditionalAttribute() {
			return additionalAttribute;
		}

		public void setAdditionalAttribute(Object additionalAttribute) {
			this.additionalAttribute = additionalAttribute;
		}
	}

	FacilityRight checkFacilityRight(Long customerId, FacilityCode facilityCode, Date effectiveDate);

	FacilityRight checkFacilityRight(Long customerId, FacilityCode facilityCode, String startTime, String endTime);

	TrainingRight checkTrainingRight(Long customerId, TrainingCode trainingCode, Date effectiveDate, String startTime,
			String endTime);
	/***
	 * check renewed service training right 
	 * @param customerId
	 * @param trainingCode
	 * @return true or false
	 */
	public boolean  checkRenewedTrainingRight(Long customerId, TrainingCode trainingCode);
	/***
	 * check renewed facility right
	 * @param customerId
	 * @param facilityCode
	 * @return true or false
	 */
	public boolean checkRenewedFacilityRight(Long customerId, FacilityCode facilityCode);

	OtherRight checkOtherRight(Long customerId, OtherRightCode otherRightCode);

	public ResponseResult getEffectiveFacilityRightByCustomerId(Long customerId);

	public ResponseResult getTrainingRight(Long customerId);
	/***
	 *  get trainingRight renewed service plan ,customer_order_hd status is CMP
	 * @param customerId
	 * @return
	 */
	public ResponseResult getRenewedFacilityRightByCustomerId(Long customerId);
	/***
	 *   get checkRenewed trainingRight renewed service plan  ,customer_order_hd status is CMP
	 * @param customerId
	 * @return
	 */
	public ResponseResult getRenewedTrainingRight(Long customerId);
}
