package com.sinodynamic.hkgta.service.crm.sales;

import java.math.BigDecimal;
import java.util.List;

import com.sinodynamic.hkgta.dto.staff.SecurityQuestionDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.pms.UserConfirmReservedFacility;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface MemberService extends IServiceBase<Member>{
	
	public void addMember(Member m) throws Exception;
	
	public void getMemberList(ListPage<Member> page, Member m) throws Exception;

	public void updateMember(Member m) throws Exception;

	public void deleteMember(Member m) throws Exception;
	
	public Member getMemberById(Long id) throws Exception;

	public ResponseResult getDependentMembers(Long customerID,ListPage page,String platform);
	
	public ResponseResult checkDMCreditLimit(Long customerId);

	public ResponseResult checkTransactionLimit(Long customerId, BigDecimal spending);
	
	public ResponseResult setSecurityQuestion(SecurityQuestionDto questions) throws Exception;
	
	public List<Member> getAllMembers() throws Exception;

	public String getMemberTypeByCustomerId(Long customerID);
	
	public Member getMemberByAcademyNo(String academyNo);
	
	/****
	 * check Member ServicePlan MulGeneration true or false
	 * @param customerID
	 * @return true or false
	 */
	public Boolean checkMemberServicePlanMulGeneration(Long customerID);
	/**
	 * 根据customerId查询member
	 * @param ids
	 * @return
	 * @throws Exception 
	 */
	public List<Member> getMembersByCustomerId(String ids) throws Exception;

//	public void updateAdultDependentToInactive();

	public void pushEmail4GoingToAdultMember();

	public void pushEmail4ExpiringMember();
	/***
	 * get member by acadeCardNo
	 * @param cardNo
	 * @return
	 */
	public Member getMemberByCardNo(String cardNo)throws Exception;

	public void AdultDependentSendEamil();
	/***
	 * 
	 * @param userId
	 * @param type  (SPA OR ROOM)
	 * @return
	 * @throws Exception
	 */
	public ResponseResult checkConfirmReservedFacility(String userId,String type) throws Exception;
	
	public ResponseResult saveConfirmReservedFacility(UserConfirmReservedFacility confirm) throws Exception;
	/***
	 * @desc academyNo+(birth or password or phone or lastName)
	 * @param acadeNo 
	 * @param birth  yyyymmdd
	 * @return
	 * @throws Exception
	 */
	public ResponseResult verificationBirthDate(String patronNo,String academyNo,String birth,String password,String phone,String lastName) throws Exception;
	
}
