package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import com.sinodynamic.hkgta.entity.fms.FacilityAdditionAttribute;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityAdditionAttributeService extends IServiceBase<FacilityAdditionAttribute> {
	
	public void saveFacilityAdditionAttribute(int facilityNo,String ballFeedingStatus);
	
	public FacilityAdditionAttribute getFacilityAdditionAttribute(int facilityNo,String attributeId);

	public FacilityAdditionAttribute getFacilityAdditionAttributeLike(Long facilityNo, String attributeId);
	
	public List<FacilityAdditionAttribute> getFacilityAdditionAttributeList(String attributeId);
	
	public List<Long> getFacilityAttributeFacilityNoList(String attributeId);
}
