package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RoomReservationRecService extends IServiceBase{
	public List<RoomReservationRec> getReservationByOrderNo(Long orderNo);
	
	public RoomReservationRec getRoomReservationRecByConfirmId(String confirmId);
	
	/***
	 * create member checkOut  sql
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public String  createMemberCheckOutSql(String startTime,String endTime);
	
	/***
	 * create member checkOut  sql
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public String  createMemberCheckOutSql(Long customerId,String startTime,String endTime);
}
