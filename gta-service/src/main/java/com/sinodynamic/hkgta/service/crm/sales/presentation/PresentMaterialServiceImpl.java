package com.sinodynamic.hkgta.service.crm.sales.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.PresentMaterialDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class PresentMaterialServiceImpl extends ServiceBase<PresentMaterial>  implements PresentMaterialService {
	@Autowired
	private PresentMaterialDao   presentMaterialDao;

	@Override
	@Transactional
	public PresentMaterial getById(Long materialId) {
		return presentMaterialDao.get(PresentMaterial.class, materialId);
	}

	@Override
	public void save() {
		
	}
}
