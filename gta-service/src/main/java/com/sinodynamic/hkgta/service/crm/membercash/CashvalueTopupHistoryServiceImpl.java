package com.sinodynamic.hkgta.service.crm.membercash;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CashvalueTopupHistoryDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;

@Service
public class CashvalueTopupHistoryServiceImpl extends ServiceBase<CashvalueTopupHistory> implements CashvalueTopupHistoryService {
	
	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired 
	private CashvalueTopupHistoryDao cashvalueTopupHistoryDao;
	

	@Transactional
	public void topupThroughVirtualAccInBatch(List<Map<String, String>> params) {
		
		if (params == null || params.size() == 0) return;
		
		for (Map<String, String> param : params) {
			
			String transDate = param.get("transDate");
			String accNo = param.get("accNo");
			String amount = param.get("amount");
			String fileName = param.get("fileName");
			
			MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByAccountNo(accNo);
			if (mpa == null) {
				
				logger.error("No MemberPaymentAcc with accountNo = " + accNo + " can be found!");
				continue;
			}
			
			Long cusomerId = mpa.getCustomerId();
			MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
			if (memberCashValue == null) {
				
				logger.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
				continue;
			}

			CashvalueTopupHistory cth = new CashvalueTopupHistory();
			cth.setCustomerId(cusomerId);
			if (CommUtil.nvl(transDate).length() != 0) {
				cth.setTopupDate(DateConvertUtil.parseString2Date(transDate, "dd/MM/yyyy"));
				cth.setSuccessDate(DateConvertUtil.parseString2Date(transDate, "dd/MM/yyyy"));
			}
			
			cth.setAmount(new BigDecimal(amount));
			cth.setTopupMethod(Constant.VIRTUAL);
			cth.setBankFileReturn(fileName);
			cashvalueTopupHistoryDao.addCashvalueTopupHistory(cth);

				
			BigDecimal oldBalance = memberCashValue.getAvailableBalance();
			BigDecimal exchgFactor = memberCashValue.getExchgFactor();
			BigDecimal addBalance = new BigDecimal(amount);
			if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
				addBalance = new BigDecimal(amount).multiply(exchgFactor);
			}
			BigDecimal newBalance = oldBalance.add(addBalance);	
			memberCashValue.setAvailableBalance(newBalance.setScale(2, BigDecimal.ROUND_HALF_UP));
			memberCashValueDao.updateMemberCashValue(memberCashValue);
			
		}
		
	}

	

}
