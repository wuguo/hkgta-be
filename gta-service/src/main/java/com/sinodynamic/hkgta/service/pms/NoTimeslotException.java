package com.sinodynamic.hkgta.service.pms;

public class NoTimeslotException extends RuntimeException {

	public NoTimeslotException() {
		super();
	}

	public NoTimeslotException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoTimeslotException(String message) {
		super(message);
	}

	public NoTimeslotException(Throwable cause) {
		super(cause);
	}

}
