package com.sinodynamic.hkgta.service.crm.sales;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.service.ServiceBase;

/**
 * 
 * @author Mianping_Wu
 * @since 5/4/2015
 */
@Service
public class StaffProfileServiceImpl extends ServiceBase<StaffProfile> implements StaffProfileService {
	
	private Logger logger = Logger.getLogger(StaffProfileServiceImpl.class);
	
	@Autowired
	private StaffProfileDao staffProfileDao;
	
	@Transactional
	public List<StaffProfile> searchAllStaffProfile(Long enrollId) throws Exception {
		
		logger.info("StaffProfileServiceImpl.searchAllStaffProfile() invocation start ...");
		
		List<StaffProfile> staffList = staffProfileDao.selectAllStaffProfile(enrollId);
		
		logger.info("StaffProfileServiceImpl.searchAllStaffProfile() invocation end ...");
		return staffList;
		
	}

	@Transactional
	public StaffProfile getStaffProfileById(String userId) {
		// TODO Auto-generated method stub
		return this.staffProfileDao.get(StaffProfile.class, userId);
	}

	/**
	 * added by Kaster 20160509
	 */
	@Override
	@Transactional
	public List<StaffProfile> searchAllStaffIdsForAssignTo(String salesStaffId) {
		List<StaffProfile> staffList = staffProfileDao.selectAllStaffIdsForAssignTo(salesStaffId);
		return staffList;
	}

}
