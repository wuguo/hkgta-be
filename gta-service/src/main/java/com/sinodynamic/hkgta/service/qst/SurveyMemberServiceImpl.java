package com.sinodynamic.hkgta.service.qst;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.qst.SurveyDao;
import com.sinodynamic.hkgta.dao.qst.SurveyMemberDao;
import com.sinodynamic.hkgta.dto.qst.CustomerAttendanceCountDto;
import com.sinodynamic.hkgta.dto.qst.SurveyQuestionnaireStatisticsDataDto;
import com.sinodynamic.hkgta.entity.qst.Survey;
import com.sinodynamic.hkgta.entity.qst.SurveyMember;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.fms.CourseEnrollmentService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.pms.RoomReservationRecService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.SurveyType;

import net.sf.json.JSONObject;

@Service
public class SurveyMemberServiceImpl extends ServiceBase<SurveyMember>implements SurveyMemberService {
	private final Logger	logger	= Logger.getLogger("schedulerLog");
	@Autowired
	private SurveyMemberDao surveyMemberDao;
	
	@Autowired
	private SurveyDao surveyDao;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private CourseEnrollmentService courseEnrollmentService;
	
	@Autowired
	private RoomReservationRecService roomReservationRecService;
	

	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	

	@Override
	@Transactional
	public void autoCountSurveyMember() {
		List<CustomerAttendanceCountDto>dtos=getAllCustomerAttendanceCountDto(null,null);
		if(null!=dtos&&dtos.size()>0)
		{
			List<Survey>surveryList=surveyDao.getAll();
			Map<String, Long>map=new HashMap<>();
			for (Survey survey : surveryList)
			{
				String key=getSurveyCodeByName(survey.getSurveyName());
				map.put(key,survey.getSurveyId());
			}
			
			for (CustomerAttendanceCountDto dto : dtos)
			{
				SurveyMember member=convertSurverMember(dto,map);
				if(null!=member){
					surveyMemberDao.saveOrUpdate(member);	
				}
			}
		}
	}
	private String getSurveyCodeByName(String serveyName)
	{
		 if(SurveyType.GOLF.getDesc().equalsIgnoreCase(serveyName))
		 {
			 return SurveyType.GOLF.name();
		 }else if(SurveyType.TENNIS.getDesc().equalsIgnoreCase(serveyName)){
			 return SurveyType.TENNIS.name();
		 }
		 else if(SurveyType.GSSC.getDesc().equalsIgnoreCase(serveyName)){
			 return SurveyType.GSSC.name();
		 }
		 else if(SurveyType.TSSC.getDesc().equalsIgnoreCase(serveyName)){
			 return SurveyType.TSSC.name();
		 }
		 else if(SurveyType.ACCOMMODATION.getDesc().equalsIgnoreCase(serveyName)){
			 return SurveyType.ACCOMMODATION.name();
		 }
		 else if(SurveyType.RESTAURANT.getDesc().equalsIgnoreCase(serveyName)){
			 return SurveyType.RESTAURANT.name();
		 }
		 return serveyName;
	}
	/**
	 * 		Base table survey_member
			1. if exist the record (customer_id, survey_id) and survey_sent_date is null , then update the count +1
			2. if exist the record (customer_id, survey_id) and survey_sent_date is not null , then insert the record and count +1
			3. if not exist the record (customer_id, survey_id), then insert the record and count +1
	 */
	private SurveyMember convertSurverMember(CustomerAttendanceCountDto dto,Map<String, Long>map)
	{
		Date date=new Date();
		SurveyMember surveyMember=new SurveyMember();
		surveyMember.setCustomerId(dto.getCustomerId());
		surveyMember.setSurveyId(map.get(dto.getSurveyType()));
		surveyMember.setCreateBy("system");
		surveyMember.setCreateDate(date);
		surveyMember.setStartHitDate(date);
	    List<SurveyMember> origs=this.checkExistCustomerIdAndSurveyId(surveyMember.getCustomerId(), surveyMember.getSurveyId(),null);
	    if(null!=origs&&origs.size()>0)
		{
			//1、if exist the record (customer_id, survey_id) and survey_sent_date is null , then update the count +1	
	    	origs=this.checkExistCustomerIdAndSurveyId(surveyMember.getCustomerId(), surveyMember.getSurveyId(),"true");
	    	 if(null!=origs&&origs.size()>0)
	    	 {
	    		 SurveyMember upSurvey=origs.get(0);
	    		 upSurvey.setUtilizeCount(upSurvey.getUtilizeCount()+dto.getTotal());
	    		 upSurvey.setUpdateBy("system");
	    		 upSurvey.setUpdateDate(date);
	    		 upSurvey.setLastHitDate(date);
	    		 upSurvey.setVerNo(upSurvey.getVerNo()+1);
	    		 return upSurvey;
	    	 }
			//2、if exist the record (customer_id, survey_id) and survey_sent_date is not null , then insert the record and count +1	
	    	 origs=this.checkExistCustomerIdAndSurveyId(surveyMember.getCustomerId(), surveyMember.getSurveyId(),"false");
	    	 if(null!=origs&&origs.size()>0)
	    	 {
	    		surveyMember.setVerNo(1l);
	 			surveyMember.setUtilizeCount(dto.getTotal());
				return  surveyMember;
			 }
		}
	   // 3. if not exist the record (customer_id, survey_id), then insert the record and count +1
	    else{
			surveyMember.setVerNo(1l);
			surveyMember.setUtilizeCount(dto.getTotal());
			return surveyMember;
		}
	    return null;
	}
	private SurveyMember convertSurverMember(CustomerAttendanceCountDto dto,Long surveyId,Date date)
	{
		SurveyMember surveyMember=new SurveyMember();
		surveyMember.setCustomerId(dto.getCustomerId());
		surveyMember.setSurveyId(surveyId);
		surveyMember.setCreateBy("system");
		surveyMember.setCreateDate(date);
		surveyMember.setStartHitDate(date);
	    List<SurveyMember> origs=this.checkExistCustomerIdAndSurveyId(surveyMember.getCustomerId(), surveyMember.getSurveyId(),null);
	    if(null!=origs&&origs.size()>0)
		{
			//1、if exist the record (customer_id, survey_id) and survey_sent_date is null , then update the count +1	
	    	origs=this.checkExistCustomerIdAndSurveyId(surveyMember.getCustomerId(), surveyMember.getSurveyId(),"true");
	    	 if(null!=origs&&origs.size()>0)
	    	 {
	    		 SurveyMember upSurvey=origs.get(0);
	    		 upSurvey.setUtilizeCount(upSurvey.getUtilizeCount()+dto.getTotal());
	    		 upSurvey.setUpdateBy("system");
	    		 upSurvey.setUpdateDate(date);
	    		 upSurvey.setLastHitDate(date);
	    		 upSurvey.setVerNo(upSurvey.getVerNo()+1);
	    		 return upSurvey;
	    	 }
			//2、if exist the record (customer_id, survey_id) and survey_sent_date is not null , then insert the record and count +1	
	    	 origs=this.checkExistCustomerIdAndSurveyId(surveyMember.getCustomerId(), surveyMember.getSurveyId(),"false");
	    	 if(null!=origs&&origs.size()>0)
	    	 {
	    		surveyMember.setVerNo(1l);
	 			surveyMember.setUtilizeCount(dto.getTotal());
				return  surveyMember;
			 }
		}
	   // 3. if not exist the record (customer_id, survey_id), then insert the record and count +1
	    else{
			surveyMember.setVerNo(1l);
			surveyMember.setUtilizeCount(dto.getTotal());
			return surveyMember;
		}
	    return null;
	}
	
	private List<CustomerAttendanceCountDto> getAllCustomerAttendanceCountDto(String startTime,String endTime){
		StringBuilder bdSql=new StringBuilder();
		String facilityAttendSql=memberFacilityTypeBookingService.createMemberFacilityAttendanceSql(startTime, endTime);
		String courseAttendsSql=courseEnrollmentService.createStudentAttendanceSql(startTime, endTime);
		String roomCheckOutsSql=roomReservationRecService.createMemberCheckOutSql(startTime, endTime);
		String restaurantSql=customerOrderTransService.createRestaurantSettlingSql(startTime, endTime);
		bdSql.append("SELECT  t.customerId,SUM(t.total)AS total,t.surveyType FROM (");
		bdSql.append(facilityAttendSql);
		bdSql.append(" UNION ALL ");
		bdSql.append(courseAttendsSql);
		bdSql.append(" UNION ALL ");
		bdSql.append(roomCheckOutsSql);
		bdSql.append(" UNION ALL ");
		bdSql.append(restaurantSql);
		bdSql.append(")as t ");
		bdSql.append(" GROUP BY t.customerId,t.surveyType");
		
		return surveyMemberDao.getDtoBySql(bdSql.toString(), null, CustomerAttendanceCountDto.class);
		
	}
	private List<CustomerAttendanceCountDto> getAllCustomerAttendanceCountDtoByCustomerId(String code,Long customerId,String startTime,String endTime){
		StringBuilder bdSql=new StringBuilder();
		if(SurveyType.GOLF.name().equals(code)||SurveyType.TENNIS.name().equals(code)){
			bdSql.append(memberFacilityTypeBookingService.createMemberFacilityAttendanceSql(customerId, code, startTime, endTime));
		}else if(SurveyType.GSSC.name().equals(code)||SurveyType.TSSC.name().equals(code))
		{
			bdSql.append(" SELECT  t.customerId,SUM(t.total)AS total,t.surveyType FROM (");
			bdSql.append(memberFacilityTypeBookingService.createMemberFacilityAttendanceSql(customerId, code, startTime, endTime));
			bdSql.append(" UNION ALL ");
			bdSql.append(courseEnrollmentService.createStudentAttendanceSql(customerId, code, startTime, endTime));
			bdSql.append(")as t ");
			bdSql.append(" GROUP BY t.customerId,t.surveyType");
		}else if(SurveyType.ACCOMMODATION.name().equals(code)){
			bdSql.append(roomReservationRecService.createMemberCheckOutSql(customerId,startTime, endTime));
		}else if(SurveyType.RESTAURANT.name().equals(code)){
			bdSql.append(customerOrderTransService.createRestaurantSettlingSql(customerId,startTime, endTime));
		}
		
		return surveyMemberDao.getDtoBySql(bdSql.toString(), null, CustomerAttendanceCountDto.class);
		
	}
	/***
	 * check Exist CustomerId /SurveyId 
	 * @param customerId
	 * @param surveyId
	 * @param surveySentDateIsNull 
	 * @return
	 */
	@Override
	@Transactional
	public  List<SurveyMember> checkExistCustomerIdAndSurveyId(Long customerId, Long surveyId,String surveySentDateIsNull)
	{
		String hql="from SurveyMember s where s.customerId=? and s.surveyId=? ";
		if(!StringUtils.isEmpty(surveySentDateIsNull))
		{
			if("true".equals(surveySentDateIsNull)){
				hql+=" and s.surveySentDate is null";
			}else{
				hql+=" and s.surveySentDate is not null ";
			}
		}
		List<Serializable>param=new ArrayList<>();
		param.add(customerId);
		param.add(surveyId);
		return   surveyMemberDao.getByHql(hql, param);
	}
	
	/**
	 * 获取当前符合条件调查问卷的URL
	 * @param customerId
	 * @return
	 */
	@Override
	@Transactional
	public String getMeetConditionsSurveyQuestionnaireURL(Long customerId, String academyNo){
//		Member member = memberDao.getMemberByAcademyNo(academyNo);
//		if (member == null) {//容错处理
//			return  "";
//		}
		// SGG-3277 Cool Down period should be triggered based on the SAME type of questionnaires instead of any type
		List<SurveyQuestionnaireStatisticsDataDto> listDto = surveyMemberDao.getMeetConditionsSurveyQuestionnaireStatisticsData(customerId);
		String url = "";
		long sysId = 0;
		Long surveyId=null;
		
		for (SurveyQuestionnaireStatisticsDataDto dto : listDto) {
			if (dto.getSendTime() == null) {
				url = dto.getUrl();
				sysId = dto.getSysId();
				surveyId=dto.getSurveyId();
				SurveyMember surveyMember = surveyMemberDao.get(SurveyMember.class, sysId);
				surveyMember.setSurveySentDate(new Date());
				surveyMemberDao.update(surveyMember);
				break;
			} else {
				if (new Date().compareTo(DateConvertUtil.afterNowNDay(dto.getSendTime(), dto.getBufferDay())) == 1) {
					url = dto.getUrl();
					sysId = dto.getSysId();
					surveyId=dto.getSurveyId();
					SurveyMember surveyMember = surveyMemberDao.get(SurveyMember.class, sysId);
					surveyMember.setSurveySentDate(new Date());
					surveyMemberDao.update(surveyMember);
					break;
				}
			}
		}
		
//		if (sysId > 0) {
//			if(null!=surveyId)
//			{
//				if(checkAfterSettlementTime(surveyId, sysId)){
//					SurveyMember surveyMember = surveyMemberDao.get(SurveyMember.class, sysId);
//					surveyMember.setSurveySentDate(new Date());
//					surveyMemberDao.update(surveyMember);
//				}else{
//					url="";
//				}
//			}
//		}
		//SGG-3212 Please help to replace "AcademyID" to patron academy ID in the URL. 
		if (!url.isEmpty()) {
			url=url.replace("[AcademyID]", academyNo);
		}
		return url;
	}
	/***
	 * check 
	 * No questionnaire pop up. No manual run settlement job. 
	 * should be no questionnaire pop up before settlement time
	 * 13:01 /23:01
	 */
	private boolean checkAfterSettlementTime(Long surveyId,Long sysId)
	{
		boolean sendBlg=Boolean.TRUE;
		Date currentTime=new Date();
		Survey survey=surveyDao.get(Survey.class, surveyId);
		//Mobile App login
		Date jobStartTime=null;
		Date jobEndTime=null;
		if(null!=survey&&SurveyType.APP.getDesc().equals(survey.getSurveyName()))
		{
			SurveyMember surveyMember = surveyMemberDao.get(SurveyMember.class, sysId);
			if(null!=surveyMember)
			{
				String start=DateConvertUtil.formatCurrentDate(currentTime, "yyyy-MM-dd")+" 13:01:00";
				String end=DateConvertUtil.formatCurrentDate(currentTime, "yyyy-MM-dd")+" 23:01:00";
				//13:01 or 23:01
				jobStartTime=DateConvertUtil.parseString2Date(start, "yyyy-MM-dd HH:mm:ss");
				jobEndTime=DateConvertUtil.parseString2Date(end, "yyyy-MM-dd HH:mm:ss");
				if(null==surveyMember.getLastHitDate())
				{
					currentTime=surveyMember.getStartHitDate();
				}else{
					currentTime=surveyMember.getLastHitDate();
				}
				
				if(!(currentTime.after(jobStartTime)||currentTime.after(jobEndTime)))
				{
					sendBlg=false;
				}
				
			}else{
				sendBlg=false;
			}
		}
		return sendBlg;
	}
	@Override
	@Transactional
	public void autoCountSurveyMemberByCustomerId(Long customerId) 
	{
		List<Survey>surveryList=surveyDao.getAll();
		for (Survey survey : surveryList)
		{
			if(survey.getSurveyName().equalsIgnoreCase(SurveyType.APP.getDesc()))continue;
			String dateTimes[]=caculateStartTimeEndTime(customerId, survey.getSurveyId());
			List<CustomerAttendanceCountDto>dtos=getAllCustomerAttendanceCountDtoByCustomerId(getCodeBySurveyName(survey.getSurveyName()),customerId,dateTimes[0],dateTimes[1]);
			if(null!=dtos&&dtos.size()>0)
			{
				logger.info("count Survey Member survey:"+survey.getSurveyName()+";surveyId:"+survey.getSurveyId()+ "; customerId="+customerId+";startTime:"+dateTimes[0]+";endTime:"+dateTimes[1]);				
				for (CustomerAttendanceCountDto dto : dtos)
				{
					if(dto.getTotal()==0)continue;
					//log
					logger.info("surveyType:"+dto.getSurveyType()+";count:"+dto.getTotal());
					SurveyMember member=convertSurverMember(dto,survey.getSurveyId(),DateConvertUtil.parseString2Date(dateTimes[1], "yyyy-MM-dd HH:mm:ss"));
					if(null!=member){
						logger.info("insert into DB ..save or update SurveyMember param:"+JSONObject.fromObject(member).toString());
						surveyMemberDao.saveOrUpdate(member);	
					}
				}
			}
		}
	}
	/***
	 * get customerId,survey_sent_date IS NULL ,start_hit_date IS NOT NULL 
	 * if exist get one data for excute startTime   
	 * if last_hit_date is null get one data for excute startTime  currentTime to endTime
	 * else   get one data  last_hit_date for excute startTime  ,currentTime to endTime
	 */
	private String[] caculateStartTimeEndTime(Long customerId,Long surveyId){
		Date currentTime=new Date();
		String []dateTime=new String[2];
		String startTime=null;
		String endTime=DateConvertUtil.formatCurrentDate(currentTime,"yyyy-MM-dd HH:mm:ss");
		List<SurveyMember>list=getSurveyMemberListByCustomerId(customerId,surveyId);
		if(null!=list&&list.size()>0)
		{
			//exist
			startTime=getSurveyMemberStartTime(list.get(0));
		}else{
			SurveyMember suerveyMember=getSurveyMemberByCustomerIdSentDateIsNotNull(customerId, surveyId);
			if(null!=suerveyMember)
			{
				startTime=getSurveyMemberStartTime(suerveyMember);
			}else{
				Integer hour=Integer.valueOf(DateConvertUtil.formatCurrentDate(currentTime, "HH"));
				if(hour<14){
					startTime=DateConvertUtil.formatCurrentDate(currentTime, "yyyy-MM-dd 00:00:00");
					endTime=DateConvertUtil.formatCurrentDate(currentTime, "yyyy-MM-dd HH:mm:00");
				}else{
					startTime=DateConvertUtil.formatCurrentDate(currentTime, "yyyy-MM-dd 00:00:00");
					endTime=DateConvertUtil.formatCurrentDate(currentTime, "yyyy-MM-dd HH:mm:ss");
				}
			}
			
		}
		dateTime[0]=startTime;
		dateTime[1]=endTime;
		return dateTime;
	}
	private String getSurveyMemberStartTime(SurveyMember surveyMember){
		String startTime=null;
		if(null!=surveyMember.getLastHitDate())
		{
			startTime=DateConvertUtil.formatCurrentDate(surveyMember.getLastHitDate(),"yyyy-MM-dd HH:mm:ss");
		}else{
			//if last_hit_date is null get one data for excute  startHitDate to startTime  currentTime to endTime
			startTime=DateConvertUtil.formatCurrentDate(surveyMember.getStartHitDate(),"yyyy-MM-dd HH:mm:ss");
		}
		return startTime;
	}
	private String getCodeBySurveyName(String name)
	{
		String code=null;
			if(SurveyType.GOLF.getDesc().equals(name))
			{
				code=SurveyType.GOLF.name();
			}else if(SurveyType.TENNIS.getDesc().equals(name)){
				code=SurveyType.TENNIS.name();
			}else if(SurveyType.GSSC.getDesc().equals(name)){
				code=SurveyType.GSSC.name();
			}else if(SurveyType.TSSC.getDesc().equals(name)){
				code=SurveyType.TSSC.name();
			}else if(SurveyType.ACCOMMODATION.getDesc().equals(name)){
				code=SurveyType.ACCOMMODATION.name();
			}else if(SurveyType.RESTAURANT.getDesc().equals(name)){
				code=SurveyType.RESTAURANT.name();
			}
		return code;
	}
	/***
	 * get customerId,survey_sent_date IS NULL ,start_hit_date IS NOT NULL 
	 * @param customerId
	 * @return
	 */
	@Transactional
	public List<SurveyMember> getSurveyMemberListByCustomerId(Long customerId ,Long surveyId){
		String hql="from SurveyMember s where s.customerId=? and s.surveyId=? and s.surveySentDate is null and s.startHitDate is not null";
		List<Serializable>param=new ArrayList<>();
		param.add(customerId);
		param.add(surveyId);
		return surveyMemberDao.getByHql(hql, param);
		
	}
	/***
	 * get last sendDate is not null  startHitDate for startTime
	 * @param customerId
	 * @param surveyId
	 * @return
	 */
	private SurveyMember getSurveyMemberByCustomerIdSentDateIsNotNull(Long customerId ,Long surveyId){
		String hql="FROM SurveyMember s WHERE s.customerId= ? AND s.surveyId=?  AND s.surveySentDate IS NOT  NULL	 ORDER BY s.surveySentDate DESC ";
		List<Serializable>param=new ArrayList<>();
		param.add(customerId);
		param.add(surveyId);
		List<SurveyMember> list=surveyMemberDao.getPageDatalist(1, 1, hql, param);
		return (null!=list&&list.size()>0)?list.get(0):null;
		
	}
	
	/**
	 * 更新或保存SurveyMember
	 * @param customerId
	 */
	@Override
	@Transactional
	public void seveOrUpdateSurveyMemberByLogin(Long customerId ){
		List<SurveyMember> list = checkExistCustomerIdAndSurveyId(customerId, 7l, "true");
		Date toDatTime = new Date();
		SurveyMember surveyMember = null;
		if (list.size() > 0) {
			surveyMember = list.get(0);
			surveyMember.setUtilizeCount(surveyMember.getUtilizeCount() + 1);
			surveyMember.setLastHitDate(toDatTime);
			surveyMember.setUpdateDate(toDatTime);
			surveyMember.setUpdateBy("system");
		} else {
			surveyMember = new SurveyMember();
			surveyMember.setUtilizeCount(1l);
			surveyMember.setCreateDate(toDatTime);
			surveyMember.setCustomerId(customerId);
			surveyMember.setStartHitDate(toDatTime);
			surveyMember.setSurveyId(7l);
			surveyMember.setCreateBy("system");
			surveyMember.setLastHitDate(toDatTime);
			surveyMember.setUpdateDate(toDatTime);
			surveyMember.setUpdateBy("system");
			surveyMember.setVerNo(0l);
		}
		surveyMemberDao.saveOrUpdate(surveyMember);
	}

}
