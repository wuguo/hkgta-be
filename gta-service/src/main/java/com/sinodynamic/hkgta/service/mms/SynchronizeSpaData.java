package com.sinodynamic.hkgta.service.mms;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dto.mms.SpaAppointmentSynDto;
import com.sinodynamic.hkgta.integration.spa.response.Payment;

public interface SynchronizeSpaData {
    
    public void synchronizePayments();
    
    public void synchronizePaymentsNew(Map.Entry<String, List<Payment>> entry);
    
    public void synchronizeAppointments();
    
    public Date getAppointmentsLatestSynSuccessTime(Date current);
    
    public Date getPaymentsLatestSynSuccessTime(Date current);
    
    public void synchronizeAppointmentsNew(Map.Entry<String, List<SpaAppointmentSynDto>> entry);
}
