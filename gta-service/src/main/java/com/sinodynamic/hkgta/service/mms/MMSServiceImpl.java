
package com.sinodynamic.hkgta.service.mms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.mms.SpaAppointmentRecDao;
import com.sinodynamic.hkgta.dao.mms.SpaCategoryDao;
import com.sinodynamic.hkgta.dao.mms.SpaCenterInfoDao;
import com.sinodynamic.hkgta.dao.mms.SpaPicPathDao;
import com.sinodynamic.hkgta.dao.mms.SpaRetreatDao;
import com.sinodynamic.hkgta.dao.mms.SpaRetreatItemDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerRefundRequestDao;
import com.sinodynamic.hkgta.dto.Constant.Status;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.crm.RefundedSpaPaymentDto;
import com.sinodynamic.hkgta.dto.crm.SpaRefundDto;
import com.sinodynamic.hkgta.dto.fms.MemberCashvalueDto;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDetailDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDto;
import com.sinodynamic.hkgta.dto.mms.SpaCategoryDto;
import com.sinodynamic.hkgta.dto.mms.SpaCenterInfoDto;
import com.sinodynamic.hkgta.dto.mms.SpaPicPathDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatItemDto;
import com.sinodynamic.hkgta.dto.mms.ThreapistDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.entity.mms.SpaCategory;
import com.sinodynamic.hkgta.entity.mms.SpaCenterInfo;
import com.sinodynamic.hkgta.entity.mms.SpaPicPath;
import com.sinodynamic.hkgta.entity.mms.SpaRetreat;
import com.sinodynamic.hkgta.entity.mms.SpaRetreatItem;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.integration.spa.em.CustomPaymentMethod;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.AppointmentDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.AvailableRoomsRequest;
import com.sinodynamic.hkgta.integration.spa.request.BookAppointmentRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetAvailableTimeSlotsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetCenterTherapistsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetServiceDetailsRequest;
import com.sinodynamic.hkgta.integration.spa.request.InvoiceCancelRequest;
import com.sinodynamic.hkgta.integration.spa.request.PaymentByCustomRequest;
import com.sinodynamic.hkgta.integration.spa.request.ServiceInfoItem;
import com.sinodynamic.hkgta.integration.spa.request.ServicesListByCategoryCodeRequest;
import com.sinodynamic.hkgta.integration.spa.request.SpaRefundRequest;
import com.sinodynamic.hkgta.integration.spa.request.SubCategoriesListRequest;
import com.sinodynamic.hkgta.integration.spa.request.TherapistRequestType;
import com.sinodynamic.hkgta.integration.spa.request.TherapistsAvailableForServiceRequest;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetail;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentServiceCancelResponse;
import com.sinodynamic.hkgta.integration.spa.response.AvailableRoomsResponse;
import com.sinodynamic.hkgta.integration.spa.response.BookAppointmentResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetAvailableTimeSlotsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetRefundsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetSubcategoriesResponse;
import com.sinodynamic.hkgta.integration.spa.response.GuestDetails;
import com.sinodynamic.hkgta.integration.spa.response.PaymentByCustomResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCategoryResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryCenterTherapistsResponse;
import com.sinodynamic.hkgta.integration.spa.response.QueryServiceListResponse;
import com.sinodynamic.hkgta.integration.spa.response.Refund;
import com.sinodynamic.hkgta.integration.spa.response.Room;
import com.sinodynamic.hkgta.integration.spa.response.ServiceDetail;
import com.sinodynamic.hkgta.integration.spa.response.ServiceDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.response.Slot;
import com.sinodynamic.hkgta.integration.spa.response.Subcategory;
import com.sinodynamic.hkgta.integration.spa.response.Therapist;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.integration.util.MMSAPIException;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.membercash.MemberCashvalueService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.service.onlinepayment.AmexPaymentGatewayService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.AdvancePeriodType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.RefundServiceType;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sinodynamic.hkgta.util.encrypt.EncryptUtil;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.pagination.PageBean;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;
/**
 * 
 * @author Nick_Xiong
 */
@Service
public class MMSServiceImpl extends ServiceBase<SpaAppointmentRec> implements MMSService{

	@Autowired
	private SpaServcieManager spaServcieManager;

	private static Logger logger = LoggerFactory.getLogger(MMSServiceImpl.class);
	
	private Logger						mmsLog	= LoggerFactory.getLogger(LoggerType.MMS.getName());
	
	private static String WP_APP_CATEGORY="RETREATS001,RTL001,CUISINE001,SPECIALS001";
	
	@Autowired
	private CustomerOrderHdDao customerOrderHdDao;
	
	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;
	
	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;
	
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	@Autowired
	private SpaAppointmentRecDao spaAppointmentRecDao;
	
	
	@Autowired
	private GlobalParameterDao globalParameterDao;
	
	@Autowired
	private SpaCenterInfoDao spaCenterInfoDao;
	
	@Autowired
	private SpaCategoryDao spaCategoryDao;
	
	@Autowired
	private SpaPicPathDao spaPicPathDao;
	
	@Autowired
	private SpaRetreatDao spaRetreatDao;
	
	@Autowired
	private SpaRetreatItemDao spaRetreatItemDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private MemberCashValueDao memberCashValueDao;
	
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	
	@Autowired
	private AmexPaymentGatewayService amexPaymentGatewayService;
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private MemberCashvalueService memberCashvalueService;
	
	@Autowired
	private CustomerRefundRequestDao customerRefundRequestDao;
	
	@Autowired
	private MemberLimitRuleDao memberLimitRuleDao;
	
	@Autowired
	private MessageTemplateDao templateDao;

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private ShortMessageService smsService;
	
	@Autowired
	private UserPreferenceSettingService userPreferenceSettingService;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Value(value ="#{spaProperties['refundAPI.url']}")
	private String refundAPI;
	
	@Value(value ="#{spaProperties['userName']}")
	private String userName;
	
	@Value(value ="#{spaProperties['organizationId']}")
	private String organizationId;
	
	@Value(value ="#{spaProperties['centerId']}")
	private String centerId;
	
	@Value(value ="#{spaProperties['centerCode']}")
	private String centerCode;

	@Value(value ="#{spaProperties['appVersion']}")
	private String appVersion;
	
	@Value(value ="#{spaProperties['userPassword']}")
	private String userPassword;
	
	@Value(value ="#{spaProperties['accountName']}")
	private String accountName;
	
	//added by Kaster 20160331
	@Autowired
	private UserMasterDao userMasterDao;
	
	
	private final static int INTERVAL_NUM = 15; //间隔分钟数
	
	/**
	 * This method will return the first level categories only and the category must at least has one service.
	 */
	public ResponseResult queryTopCategories() throws Exception {
		try{
			mmsLog.info("queryTopCategories start...");
			QueryCategoryResponse response = spaServcieManager.getCategoriesList();
			
			if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		mmsLog.info("queryTopCategories end...");
		 return responseResult;
	}

	
	@Transactional
	public ResponseResult getLocalTopCategory(String device) throws Exception
	{
		mmsLog.info("getLocalTopCategory start...");
		mmsLog.info("device:" + device);
		List<SpaCategoryDto> spaCategoryList = getSpaCategoryList();
		List<SpaCategoryDto> resultList = new ArrayList<SpaCategoryDto>();
		if(spaCategoryList!=null && spaCategoryList.size()>0)
		{
			for(SpaCategoryDto category: spaCategoryList)
			{
				List<SpaPicPathDto> picList = category.getCategoryPicList();
				List<SpaPicPathDto> tmpList = new ArrayList<SpaPicPathDto>();
				if("PC".equals(device)) //if PC, remove those catetory which apply for Webportal & APP 
				{					
					if(WP_APP_CATEGORY.indexOf(category.getCategoryId())>=0)
					    continue;
					resultList.add(category);
				}
				//for webportal and APP, need return the mapped picture.
				if(picList!=null && picList.size()>0)
				{
					for(SpaPicPathDto pic : picList)
					{
						if(("APP".equals(pic.getPicType()) && "APP".equals(device))
					        || ("WP".equals(pic.getPicType()) && "WP".equals(device)))
						{
							tmpList.add(pic);
							break;
						}
					}
				}
				category.setCategoryPicList(tmpList);
			}
			if("PC".equals(device))
			{
				responseResult.initResult(GTAError.Success.SUCCESS, resultList);
			}else
			{
				responseResult.initResult(GTAError.Success.SUCCESS, spaCategoryList);
			}
			
		}else
		{
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "Get Top Category List failed.");
		}		
		mmsLog.info("getLocalTopCategory end...");
		 return responseResult;
	}
	
	
	/**
	 * get sub category by parent_category_id
	 */
	@Transactional
	public ResponseResult getLocalCategory(String device,String parentCatId) throws Exception
	{
		mmsLog.info("getLocalCategory start...");
		mmsLog.info("device:" + device);
		List<SpaCategoryDto> spaCategoryList = getSpaCategoryList(parentCatId);
		List<SpaCategoryDto> resultList = new ArrayList<SpaCategoryDto>();
		if(spaCategoryList!=null && spaCategoryList.size()>0)
		{
			for(SpaCategoryDto category: spaCategoryList)
			{
				List<SpaPicPathDto> picList = category.getCategoryPicList();
				List<SpaPicPathDto> tmpList = new ArrayList<SpaPicPathDto>();
				if("PC".equals(device)) //if PC, remove those catetory which apply for Webportal & APP 
				{					
					if(WP_APP_CATEGORY.indexOf(category.getCategoryId())>=0)
					    continue;
					resultList.add(category);
				}
				//for webportal and APP, need return the mapped picture.
				if(picList!=null && picList.size()>0)
				{
					for(SpaPicPathDto pic : picList)
					{
						if(("APP".equals(pic.getPicType()) && "APP".equals(device))
					        || ("WP".equals(pic.getPicType()) && "WP".equals(device)))
						{
							tmpList.add(pic);
							break;
						}
					}
				}
				category.setCategoryPicList(tmpList);
			}
			if("PC".equals(device))
			{
				responseResult.initResult(GTAError.Success.SUCCESS, resultList);
			}else
			{
				responseResult.initResult(GTAError.Success.SUCCESS, spaCategoryList);
			}
			
		}else
		{
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "Get Top Category List failed.");
		}		
		mmsLog.info("getLocalCategory end...");
		 return responseResult;
	}
	
	/**
	 * This method will return the center's all therapists, include female, male which is available 
	 * on the specified Date.
	 */
	public ResponseResult getCenterTherapists(Date requestDate)throws Exception {
		try
		{
		 mmsLog.info("getLocalCategory start...");
		 mmsLog.info("requestDate :"+requestDate);
		 GetCenterTherapistsRequest request = new GetCenterTherapistsRequest();
		 request.setRequestDate(requestDate);
		 QueryCenterTherapistsResponse response =  spaServcieManager.getCenterTherapists(request);
		 
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		mmsLog.info("getLocalCategory end...");
		return responseResult;
	}

	public ResponseResult therapistsAvailableForService(Date requestDate, String serviceCode, TherapistRequestType type)throws Exception {
		try
		{
		  mmsLog.info("therapistsAvailableForService start...");
		  mmsLog.info("requestDate :"+requestDate+" serviceCode:"+serviceCode+" type"+type);
		  TherapistsAvailableForServiceRequest request = new TherapistsAvailableForServiceRequest();
		  request.setRequestDate(requestDate);
		  request.setServiceCode(serviceCode);
		  request.setType(type);
		  GetAvailableTherapistsResponse apiResponse = spaServcieManager.therapistsAvailableForService(request);
		  
		  String protocal = this.appProps.getProperty("mms.img.protocal");
		  if(protocal==null)protocal = "http";
		  
		  if(!apiResponse.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, apiResponse.getMessage());
			}else
			{
				List<Therapist> therapists = apiResponse.getTherapists();
				if(therapists!=null && therapists.size()>0)
				{
				  for(Therapist therapist : therapists)
				  {
					  String imageUrl = therapist.getImageURL();
					  if(!StringUtils.isEmpty(imageUrl))
					  {
							String httpStr = imageUrl.substring(0,imageUrl.indexOf(":"));
							imageUrl = imageUrl.replaceFirst(httpStr, protocal);
							therapist.setImageURL(imageUrl);
					  }
					  if(StringUtils.isEmpty(therapist.getUserCode()) && !StringUtils.isEmpty(therapist.getEmployeeCode()))
					  {
						  therapist.setUserCode(therapist.getEmployeeCode());
					  }
				  }
				}
				apiResponse.setTherapists(therapists);
			 responseResult.initResult(GTAError.Success.SUCCESS, apiResponse);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		mmsLog.info("therapistsAvailableForService end...");
		  return responseResult;
	}

	@Override
	public ResponseResult getAvailableTimeSlots(GetAvailableTimeSlotsRequest parameter)throws Exception {
		
		try
		{
		 mmsLog.info("getAvailableTimeSlots start...");
		 GetAvailableTimeSlotsResponse response =  spaServcieManager.getAvailableTimeSlots(parameter);
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		mmsLog.info("getAvailableTimeSlots end...");
		return responseResult;
	}

	@Override
	public ResponseResult getFormatAvailableTimeSlots(GetAvailableTimeSlotsRequest parameter)throws Exception {
		
		try
		{
		 mmsLog.info("getFormatAvailableTimeSlots start...");
		 GetAvailableTimeSlotsResponse response =  spaServcieManager.getAvailableTimeSlots(parameter);
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
				
				List<List<Slot>> slots = response.getSlots();
				Set<String> timeSet = new TreeSet<String>();
				SimpleDateFormat sdformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.ENGLISH);
				
			List<ThreapistDto>threapistList=new ArrayList<>();
			
			if (slots != null && slots.size() > 0) 
			{
				
				for (List<Slot> subSlots : slots) 
				{
					if (subSlots != null && subSlots.size() > 0) 
					{
						for (Slot slot : subSlots) 
						{
							ThreapistDto threapist=new ThreapistDto();
							
							String startTime = slot.getStarttimestring();
							String endTime = slot.getEndtimestring();
							Date startDate = sdformat.parse(startTime);
							//SGG-2976 24小时之后web同app端才可以预订
							if (!isAfter24Hours(startDate)) {
								continue;
							}
							Date endDate = sdformat.parse(endTime);
							threapist.setTimeslot(DateCalcUtil.getHourAndMinuteOfDay(startDate));
//							while (startDate.before(endDate)) 
//							{
//								threapist.setTimeslot(DateCalcUtil.getHourAndMinuteOfDay(startDate));
//								timeSet.add(DateCalcUtil.getHourAndMinuteOfDay(startDate));
//								startDate = DateCalcUtil.getNextHalfHour(startDate);
//							}
							threapist.setTherapistid(slot.getTherapistid());
							threapistList.add(threapist);
						}
					}
				}
			}
			this.settingTherapistCode(threapistList, parameter.getStartTime(), parameter.getServiceCodeList().get(0));
			//设置是否可用
			this.settingIsAvailable(threapistList, parameter.getServiceHours());
			responseResult.initResult(GTAError.Success.SUCCESS, threapistList);
		  }
		}catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		catch(Exception e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		mmsLog.info("getFormatAvailableTimeSlots end...");
		return responseResult;
	}
	
	/**
	 * 设置是否可用
	 * @param list
	 * @param serviceHours
	 * @return
	 */
	private List<ThreapistDto> settingIsAvailable(List<ThreapistDto> list, int serviceHours){
		int intervalCycleBout = serviceHours / INTERVAL_NUM;   //间隔循环次数
		Map<String, String> map = new HashMap<>();
		for (ThreapistDto threapistDto : list) {
			map.put(threapistDto.getTimeslot(), threapistDto.getTherapistCode());
		}
		for (ThreapistDto dto : list) {
//			String therapistCode = dto.getTherapistCode();
//			String[] time = dto.getTimeslot().split(":");
//			String[] arr = gettingIntervalTime(time[0], time[1]);
			boolean isAvailable = true;
//			for (int i = 0; i < intervalCycleBout; i++) {
//				if (null == map.get(arr[2]) || !therapistCode.equals(map.get(arr[2]))) {
////					isAvailable = false;
//					break;
//				} 
//				arr = gettingIntervalTime(arr[0], arr[1]);
//			}
			dto.setIsAvailable(isAvailable);
		}
		return list;
	}
	/**
	 * 处理时间间隔
	 * @param hours
	 * @param minute
	 * @return
	 */
	public final static String[] gettingIntervalTime(String hours, String minute){
		String[] nextTime = new String[3];
		switch (minute) {
		case "00":
			nextTime[0] = hours;
			nextTime[1] = "15";
			nextTime[2] = hours + ":15";
			break;
		case "15":
			nextTime[0] = hours;
			nextTime[1] = "30";
			nextTime[2] = hours + ":30";
			break;
		case "30":
			nextTime[0] = hours;
			nextTime[1] = "45";
			nextTime[2] = hours + ":45";
			break;
		case "45":
			int num = Integer.parseInt(hours) + 1;
			nextTime[0] = (num > 12 ? num : "0" + num) + "";
			nextTime[1] = "00";
			nextTime[2] = nextTime[0] + ":00";
			break;
		}
		return nextTime;
	}

	private void settingTherapistCode(List<ThreapistDto> list, Date requestDate, String serviceCode) throws Exception {
		responseResult = this.therapistsAvailableForService(requestDate, serviceCode, TherapistRequestType.ANY);
		if (responseResult.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
			GetAvailableTherapistsResponse apiResponse = (GetAvailableTherapistsResponse) responseResult.getDto();
			List<Therapist> therapists = apiResponse.getTherapists();
			if (null != therapists && therapists.size() > 0) {
				Map<String, String> map = new HashMap<>();
				for (Therapist therapist : therapists) {
					map.put(therapist.getEmployeeId(), therapist.getUserCode());
				}
				for (ThreapistDto dto : list) {
					if (map.containsKey(dto.getTherapistid())) {
						dto.setTherapistCode(map.get(dto.getTherapistid()));
						dto.setTherapistid(null);
					}
				}
			}
		}
	}

	@Override
	public ResponseResult queryServiceList(String categoryCode)throws Exception {
	
	  try
	  {
		mmsLog.info("queryServiceList start...");
		mmsLog.info("categoryCode :"+categoryCode);
		ServicesListByCategoryCodeRequest request = new ServicesListByCategoryCodeRequest();
		request.setCategoryCode(categoryCode);
		QueryServiceListResponse response = spaServcieManager.getServicesListByCategoryCode(request);
		if(!response.getSuccess()){
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		}else{
		 responseResult.initResult(GTAError.Success.SUCCESS, response);
		}
	  }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(), e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(), e);
		}
	  	mmsLog.info("queryServiceList end...");
		return responseResult;
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public ResponseResult bookAppointment(SpaAppointmentDto spaAppointmentDto)throws Exception {
		
		mmsLog.info("bookAppointment start...");
		checkAppointmentBookingDate(spaAppointmentDto);
		
		//added by Kaster 20160222 检查会员的cash value是否足够以及credit limit是否足够，否则无法预约。
	   try
	   {
		BookAppointmentRequest bookAppointmentParameters;		
	    bookAppointmentParameters = convertRequestToApiParam(spaAppointmentDto);
		BookAppointmentResponse response = spaServcieManager.bookAppointment(bookAppointmentParameters);	
			
		if (!response.getSuccess()) 
		{
			mmsLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, response.toString(), response.getMessage()));
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		} else 
		{
				createLocalAppointment(spaAppointmentDto, response);
				// for PC reservation without payment

				String invoiceNo = response.getInvoiceNo();

				String paymentMethod = spaAppointmentDto.getPaymentMethod();
				
				//resvId
				String resvIdStr = "";
				String[] appointmentId = response.getAppointmentId().split(",");
				if(appointmentId != null && appointmentId.length > 0){
					for(int i=0; i<appointmentId.length; i++){
						resvIdStr += "SPA-" + appointmentId[i].substring(appointmentId[i].length()-6, appointmentId[i].length()) + ",";
					}
				}
				resvIdStr = resvIdStr.substring(0, resvIdStr.length()-1);
				
				if (paymentMethod != null && !"".equals(paymentMethod)) {
					// for webportal reservation include payment
					if (invoiceNo != null && !"".equals(invoiceNo)) {
						MMSPaymentDto paymentDto = new MMSPaymentDto();
						paymentDto.setResvIdStr(resvIdStr);
						paymentDto.setCustomerId(spaAppointmentDto.getCustomerId());
						paymentDto.setInvoiceNo(invoiceNo);
						paymentDto.setPaymentMethod(paymentMethod);
						paymentDto.setIsRemote("N");
						ResponseResult paymentResult = payment(paymentDto);
						paymentDto = (MMSPaymentDto) paymentResult.getDto();					
						
						responseResult.initResult(GTAError.Success.SUCCESS,paymentDto);
						mmsLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, JSONObject.fromObject(paymentDto).toString()));//
						
					} else {
						mmsLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, response.toString(), "InvoiceNo cannot be null for payment."));
						responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION,"InvoiceNo cannot be null for payment.");
					}
				} else {
					MMSPaymentDto paymentDto = new MMSPaymentDto();
					paymentDto.setInvoiceNo(invoiceNo);
					paymentDto.setResvIdStr(resvIdStr);
					mmsLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, JSONObject.fromObject(paymentDto).toString()));//
					responseResult.initResult(GTAError.Success.SUCCESS, paymentDto);
				}

			}	
	   }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			mmsLog.error(e.getMessage(),e);
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
	   	mmsLog.info("bookAppointment end...");	
		return responseResult;
	}
	
	//added by Kaster 20160222
	private void checkCashValueAndCreditLimit(SpaAppointmentDto spaAppointmentDto) {
		MemberCashvalue memberCashvalue = memberCashValueDao.getByCustomerId(spaAppointmentDto.getCustomerId());
		String hql = "from MemberLimitRule where limitType='CR' and customerId=?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(spaAppointmentDto.getCustomerId());
		MemberLimitRule memberLimitRule = (MemberLimitRule) memberLimitRuleDao.getUniqueByHql(hql, param);
		
		BigDecimal spaTotalAmount = new BigDecimal(0);
		List<SpaAppointmentDetailDto> serviceDetails = spaAppointmentDto.getServiceDetails();
		for(int i=0;i<serviceDetails.size();i++){
			spaTotalAmount = spaTotalAmount.add(serviceDetails.get(i).getServiceCost());
		}
		if(memberCashvalue != null){
			//余额大于0
			if(memberCashvalue.getAvailableBalance().compareTo(new BigDecimal(0))>0){
				//可用余额不够支付而且信用额度也不够支付，那就抛出异常！
				if(memberCashvalue.getAvailableBalance().compareTo(spaTotalAmount)<0){
					if(memberLimitRule==null || memberLimitRule.getNumValue().compareTo(spaTotalAmount)<0){
						throw new GTACommonException(GTAError.PaymentError.BALANCE_INSUFFICIENT);
					}
				}
			}else{
				//余额小于0,说明已赊账，那就计算剩余的可赊账的数目是否足够支付，不够的话也是抛出异常！
				if(memberLimitRule!=null){
					if(memberLimitRule.getNumValue().subtract(memberCashvalue.getAvailableBalance().abs()).compareTo(spaTotalAmount)<0){
						throw new GTACommonException(GTAError.PaymentError.BALANCE_INSUFFICIENT);
					}
				}else{
					throw new GTACommonException(GTAError.PaymentError.BALANCE_INSUFFICIENT);
				}
			}
		}else{
			if(memberLimitRule==null || memberLimitRule.getNumValue().compareTo(spaTotalAmount)<0){
				throw new GTACommonException(GTAError.PaymentError.BALANCE_INSUFFICIENT);
			}
		}
	}


	@Transactional
	public void sentPaymentReceipt(MMSPaymentDto dto, Boolean isSynchronized) throws Exception
	{
		CustomerEmailContent customerEmailContent = getEmailContent(dto.getInvoiceNo(),isSynchronized);
		if (customerEmailContent != null) {
			List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(Long.valueOf(dto.getOrderNo()));
			CustomerOrderTrans customerOrderTrans = customerOrderTranses.get(0);
			byte[] attachment = customerOrderTransDao.getInvoiceReceipt(null, customerOrderTrans.getTransactionNo().toString(), "wellness");
			List<byte[]> attachmentList = Arrays.asList(attachment);
			List<String> mineTypeList = Arrays.asList("application/pdf");
			List<String> fileNameList = Arrays.asList("MMSPaymentReceipt-"+customerOrderTrans.getTransactionNo().toString()+".pdf");
			mailThreadService.sendWithResponse(customerEmailContent, attachmentList, mineTypeList, fileNameList);
		}
	}
	
	@Transactional
	public void sentMessage(List<SpaAppointmentRec> appointmentList, SceneType messageType) throws Exception
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("sentMessage start.");
		}		
			SpaAppointmentRec rec = appointmentList.get(0);
			String invoiceNo = rec.getExtInvoiceNo();			
			Long customerId = rec.getCustomerId();
			String message = getMessageContent(invoiceNo, customerId, messageType);
			if(StringUtils.isEmpty(message))return;
			CustomerProfile cp = customerProfileDao.getById(customerId);
			String phoneNumber = cp.getPhoneMobile();
			if(logger.isDebugEnabled())
			{
				logger.debug("phoneNumber:" + phoneNumber);
				logger.debug("message content:" + message);
			}
			if(null!=cp){
				if(userPreferenceSettingService.sendSMSByCustomerId(cp.getCustomerId())){
					List<String> phoneList = new ArrayList<String>();
					phoneList.add(phoneNumber);
					smsService.sendSMS(phoneList, message, new Date());		
				}
			}
				
		
		if(logger.isDebugEnabled())
		{
			logger.debug("sentMessage end.");
		}
	}
	
	public String getMessageContent(String invoiceNo, Long customerId, SceneType messageType)throws Exception
	{
		String message = null;
			
		String functionId = null;
		if(SceneType.WELLNESS_PAYMENT_SETTLE.equals(messageType))
		{
			functionId = Constant.TEMPLATE_ID_SMS_WELLNESS_PAYMENT_CONFIRM;
		}
		
		if(logger.isDebugEnabled())
		{
			logger.debug("functionId:" + functionId);
		}
		if (functionId == null) return null;
		MessageTemplate template = templateDao.getTemplateByFunctionId(functionId);
		if (template == null)
		{
			logger.error("Database data error for sending message!");
			return null;
		}
		List<SpaAppointmentRec> recList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
		if (recList == null)
		{
		   logger.error("Database data error for sending receipt!");
		   return null;
		}
		
		String detail = "";		
		for(SpaAppointmentRec rec : recList)
		{			
			String serviceName = rec.getServiceName();
			String serviceTime = DateCalcUtil.formatDatetime(rec.getStartDatetime()) + " to " + DateCalcUtil.formatDatetime(rec.getEndDatetime());
			detail += serviceName + " (" + serviceTime + " ) ";			
		}						
			message = template.getFullContent(detail);
		
		return message;
	}
	
	private  CustomerEmailContent getEmailContent(String invoiceNo,Boolean isSynchronized) throws Exception {
		
		if (invoiceNo ==null ) return null;

		List<SpaAppointmentRec> recList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
		if (recList == null)
		{
		   logger.error("Database data error for sending receipt!");
		   return null;
		}
		String detail = "";
		Long customerId = null;
		double amount = 0;
		for(SpaAppointmentRec rec : recList)
		{
			customerId = rec.getCustomerId();
			String serviceName = rec.getServiceName();
			String serviceTime = DateCalcUtil.formatDatetime(rec.getStartDatetime()) + " to " + DateCalcUtil.formatDatetime(rec.getEndDatetime());
			detail += serviceName + " (" + serviceTime + " ) ";
			
			amount += rec.getServiceInternalCost().doubleValue();
		}
		
		if(customerId == null)
		{
			logger.error("CustomerId is empty.");
			return null;
		}
		CustomerProfile cp = customerProfileDao.getById(customerId);
		if (cp == null)
		{
			logger.error("Database data error for sending receipt!");
			   return null;
		}
		
		String functionId = null;
		if(isSynchronized)
			functionId = Constant.TEMPLATE_ID_EMAIL_WELLNESS_PAYMENT_RECEIPT_SYNCHRONIZED;
		else
		    functionId = Constant.TEMPLATE_ID_EMAIL_WELLNESS_PAYMENT_RECEIPT;
		
		MessageTemplate template = templateDao.getTemplateByFunctionId(functionId);
		if (template == null)
		{
			logger.error("Database data error for sending receipt!");
			   return null;
		}

		String recipientEmail = cp.getContactEmail();
		String subjectName = template.getMessageSubject();
//		String templateContent = template.getContent();
		String userName = getName(cp);
		String memberName = userName;			
		
		
		String content = template.getFullContent(userName, memberName,invoiceNo,detail,String.valueOf(amount));
		
		CustomerEmailContent cec = new CustomerEmailContent();
		cec.setContent(content);
		cec.setStatus(EmailStatus.PND.name());
		cec.setNoticeType(Constant.NOTICE_TYPE_WELLNESS_CENTER);
		cec.setRecipientCustomerId(String.valueOf(customerId));
		cec.setRecipientEmail(recipientEmail);
		cec.setSubject(subjectName);
		cec.setSendDate(new Date());

		String sendId = (String) customerEmailContentDao.addCustomerEmail(cec);
		cec.setSendId(sendId);
		return cec;
	}
	
	private String getName(CustomerProfile cp) {

		if (cp == null)
			return null;
		StringBuilder sb = new StringBuilder();
		sb.append(cp.getSalutation()).append(" ").append(cp.getGivenName())
				.append(" ").append(cp.getSurname());
		return sb.toString();
	}
	private void checkAppointmentBookingDate(SpaAppointmentDto spaAppointmentDto) throws Exception
	{
		List<SpaAppointmentDetailDto> serviceList = spaAppointmentDto.getServiceDetails();	
		SpaAppointmentDetailDto detail = serviceList.get(0);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
						
		if (format.parse(detail.getStartDatetime()).before(format.parse(format.format(new Date()))))
			throw new GTACommonException(GTAError.FacilityError.DATE_LESSTHAN_TODAY );
	
		int parameterValue = 0;
		
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
		if (null != globalParameter)
		{
			if (!StringUtils.isEmpty(globalParameter.getParamValue()))
				parameterValue = Integer.parseInt(globalParameter.getParamValue());
		}
		if (parameterValue > 0)
		{
			Date futureDate = DateCalcUtil.getNearDay(new Date(), parameterValue);
			if (format.parse(detail.getStartDatetime()).after(DateCalcUtil.getEndDateTime(futureDate)))
				throw new GTACommonException(GTAError.FacilityError.DATE_OVER_ADVANCEDPERIOD,new Object[]{parameterValue});
		}
	}
	
	
	private BookAppointmentRequest convertRequestToApiParam(SpaAppointmentDto spaAppointmentDto)throws Exception{
		
		mmsLog.info("convert the input param to API parameter start ...");
		Long customerId = spaAppointmentDto.getCustomerId();	
		Member member = memberDao.getMemberByCustomerId(customerId);
		
		
		if(!"ACT".equals(member.getStatus())){
			mmsLog.info("The member is not active. AcademyNo:"+member.getAcademyNo());	
	  		throw new GTACommonException(null, "The member is not active.");
		}		
		
		String invoiceNo = spaAppointmentDto.getInvoiceNo();
		
		BookAppointmentRequest bookAppointmentParameters = new BookAppointmentRequest();
		
		if(invoiceNo!=null && !"".equals(invoiceNo)){
			AppointmentDetailsRequest request = new AppointmentDetailsRequest();
			request.setInvoiceNo(invoiceNo);
			AppointmentDetailsResponse  appointmentDetailResponse = spaServcieManager.appointmentDetails(request);
			GuestDetails guestDetail = appointmentDetailResponse.getGuestDetails();
			String existingGuestCode = guestDetail.getUsercode();
			if(!member.getAcademyNo().equals(existingGuestCode)){
				mmsLog.info("The appointment is expected for user " + existingGuestCode);		
		  		throw new GTACommonException(null, "The appointment is not match the existing user " + existingGuestCode);
			}
		}
		bookAppointmentParameters.setGuestCode(member.getAcademyNo());
		bookAppointmentParameters.setInvoiceNo(invoiceNo);
		List<SpaAppointmentDetailDto> serviceList = spaAppointmentDto.getServiceDetails();		
		List<ServiceInfoItem> serviceInfo = new ArrayList<ServiceInfoItem>();
		
		for(SpaAppointmentDetailDto service : serviceList){
		
			ServiceInfoItem item = new ServiceInfoItem();			
		  	item.setServiceCode(service.getServiceCode());
		  	
		  try{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");			
			bookAppointmentParameters.setStartTime(df.parse(service.getStartDatetime()));	///if multiple services, which startTime should be used?	
		  	item.setStartTime(df.parse(service.getStartDatetime())); 
		  	
		  	mmsLog.info("get available rooms based on the service code and time");
			AvailableRoomsRequest request = new AvailableRoomsRequest();
			request.setServiceCode(service.getServiceCode());
			request.setStartDate(df.parse(service.getStartDatetime()));
			request.setTherapistCode(service.getTherapistCode());
            AvailableRoomsResponse roomResponse = spaServcieManager.queryAvailableRooms(request);
				if (roomResponse != null) {
					for (Room room : roomResponse.getRooms()) {
						Map<String, Object> additionalProperties = room.getAdditionalProperties();
						String roomCode = additionalProperties.get("RoomCode")!=null? additionalProperties.get("RoomCode").toString() : "";
						mmsLog.info("roomCode:" + roomCode);
						if (roomCode != null && !"".equals(roomCode)) {
							mmsLog.info("selected roomCode:" + roomCode);
							item.setRoomCode(roomCode);
							break;
						}
					}
				}else{
			  		mmsLog.info("no available rooms  or get available room failed.");		
			  		throw new GTACommonException(null, "No room avalibale for the service.");
				}
				// As the roomCode from API may all null, add below check to avoid this issue.
				if(item.getRoomCode() == null || "".equals(item.getRoomCode())){					
					mmsLog.info("no available rooms  or get available room failed.");		
			  		throw new GTACommonException(null, "No room avalibale for the service.");				
				}
				//test add 15mins
				Date time=df.parse(service.getEndDatetime());
				if(null!=service.getRecoveryTime()){
					item.setEndTime(DateCalcUtil.getDateAddMins(time,service.getRecoveryTime()));	
				}else{
					item.setEndTime(df.parse(service.getEndDatetime()));
				}
			
		  }catch(ParseException e){
			  
			  mmsLog.error("The startTime/endTime format error, the correct format should be yyyy-MM-dd HH:mm:ss",e);
		  }
		  
		  	item.setTherapistCode(service.getTherapistCode());
		  	
		  	if(service.getTherapistType()!=null && "0".equals(service.getTherapistType())){	  	
		  		item.setTherapistRequestType(TherapistRequestType.FEMALE);
		  	}else if(service.getTherapistType()!=null && "1".equals(service.getTherapistType())){
		  		item.setTherapistRequestType(TherapistRequestType.MALE);
		  	}else
		  	{
		  		item.setTherapistRequestType(TherapistRequestType.ANY);
		  	}
		  	serviceInfo.add(item);	
		}
	
	  	bookAppointmentParameters.setServiceInfo(serviceInfo);
	  	mmsLog.info("convert the input param to API parameter end ...");
	  	return bookAppointmentParameters;
	}
	
//	@Transactional
	private void createLocalAppointment(SpaAppointmentDto param, BookAppointmentResponse response) throws Exception{
		mmsLog.info("createLocalAppointment start...");
		String[] appointmentId = null;
		String invoiceNo = "";
		if(response!=null)
		{
		
		appointmentId = response.getAppointmentId().split(","); 
		invoiceNo = response.getInvoiceNo();		
		}else
		{
			invoiceNo = param.getInvoiceNo();
		}
		
		AppointmentDetailsResponse  appointmentDetailResponse=null;
		//if the call from MMS , get the appointment info directly, will not call spaServcieManager.appointmentDetails 
		if(param!=null&&Constant.MMSTHIRD_ITEM_NO.equals(param.getRemoteType())){
			ObjectMapper mapper = new ObjectMapper(); 
			appointmentDetailResponse = mapper.readValue(param.getAppointmentInfo(), AppointmentDetailsResponse.class);	
		}else{
			AppointmentDetailsRequest request = new AppointmentDetailsRequest();
			request.setInvoiceNo(invoiceNo);
			appointmentDetailResponse = spaServcieManager.appointmentDetails(request);	
		}
		
		mmsLog.info("AppointmentDetailsResponse from MMS:" + appointmentDetailResponse.toString());
		if(appointmentDetailResponse!=null){
			List<CustomerOrderHd>  orderList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode", invoiceNo, null);
			CustomerOrderHd customerOrderHd = new CustomerOrderHd();			
					
			Long customerId = param.getCustomerId();	
			double totalAmount = 0;
			if(orderList!=null && orderList.size()>0){	
				customerOrderHd = orderList.get(0);
				totalAmount = customerOrderHd.getOrderTotalAmount().doubleValue();
			}	
			
			GuestDetails  guestDetail = appointmentDetailResponse.getGuestDetails();		

				for(int i=0; i<param.getServiceDetails().size(); i++){
					
					SpaAppointmentDetailDto detail = param.getServiceDetails().get(i);
					
					totalAmount = totalAmount + detail.getServiceCost().doubleValue();			
		        	customerOrderHd.setOrderDate(new Date());
					customerOrderHd.setOrderStatus(Constant.Status.OPN.name());			
					customerOrderHd.setCustomerId(customerId);
					customerOrderHd.setOrderTotalAmount(new BigDecimal(totalAmount));
					customerOrderHd.setOrderRemark("");
					//added by Kaster 20160331
					String createdBy = param.getCreatedBy();
					boolean isStaffApp = false;
					if(createdBy!=null && !createdBy.equals("")){
						UserMaster um = userMasterDao.get(UserMaster.class, createdBy);
						if(um!=null && um.getUserType().equalsIgnoreCase("STAFF")){
							customerOrderHd.setCreateBy(createdBy);
							isStaffApp = true;
						}
					}
					customerOrderHd.setCreateBy(createdBy);		
					customerOrderHd.setCreateDate(new Timestamp(new Date().getTime()));	
					customerOrderHd.setUpdateBy(createdBy);	
					customerOrderHd.setUpdateDate(new Date());
					customerOrderHd.setVendorRefCode(invoiceNo);
					customerOrderHdDao.saveOrUpdate(customerOrderHd);

					// CUSTOMER ORDER Detail				
					
					CustomerOrderDet customerOrderDet = new CustomerOrderDet();
									
					customerOrderDet.setCustomerOrderHd(customerOrderHd);
					String remoteType=param.getRemoteType();
					if(null != remoteType && remoteType.equalsIgnoreCase(Constant.MMSTHIRD_ITEM_NO)){
						customerOrderDet.setItemNo(Constant.MMSTHIRD_ITEM_NO);  //call from MMS remote payment
					}else{
						customerOrderDet.setItemNo("MMS2BRS");   //???	
					}
					
					customerOrderDet.setItemRemark("");  //????
					customerOrderDet.setOrderQty(1L);
					customerOrderDet.setItemTotalAmout(detail.getServiceCost());
					customerOrderDet.setExtRefNo(detail.getServiceCode()); //set ext ref no as the service code
					//added by Kaster 20160331
					if(isStaffApp){
						customerOrderDet.setCreateBy(createdBy);
					}
					customerOrderDet.setCreateDate(new Timestamp(new Date().getTime()));			
					customerOrderDet.setUpdateDate(new Date());	               
					Long orderDetId = (Long) customerOrderDetDao.saveOrderDet(customerOrderDet);
					
					//mmsLog.info("orderNo:" + customerOrderHd.getOrderNo()+ "orderDetId:" + orderDetId);
					// SPA_APPOINTMENT_REC
					SpaAppointmentRec spaAppointmentRec = new SpaAppointmentRec();
					
					if(null!=guestDetail.getRebooked()){
						spaAppointmentRec.setRebooked(guestDetail.getRebooked()? new Long(1) : new Long(0));
					}
					
					if(null!=appointmentId){
						spaAppointmentRec.setExtAppointmentId(appointmentId[i]);
					}
					
					//if the call from MMS , get the appointment id from MMS detail info 
					if(param!=null&&Constant.MMSTHIRD_ITEM_NO.equals(param.getRemoteType())){
						spaAppointmentRec.setExtAppointmentId(detail.getAppointmentId());
					}
					
		    		spaAppointmentRec.setExtInvoiceNo(invoiceNo);
		    		
		    		String serviceCode = detail.getServiceCode();
		    		GetServiceDetailsRequest queryServiceDetailsReq = new GetServiceDetailsRequest();
		    		queryServiceDetailsReq.setServiceCode(serviceCode);
		    		
		    		mmsLog.info("QueryServiceDetailsReq ServiceCode:" + serviceCode);
		    		ServiceDetailsResponse serviceDetailResponse = spaServcieManager.queryServiceDetails(queryServiceDetailsReq);
		    		mmsLog.info("ServiceDetailsResponse from MMS:" + serviceDetailResponse.toString());
		    		ServiceDetail serviceDetail = serviceDetailResponse.getServiceDetail();
		    		
		    		spaAppointmentRec.setServiceTime(detail.getServiceTime());
					spaAppointmentRec.setServiceInternalCost(detail.getServiceCost());
		    		spaAppointmentRec.setExtServiceCode(serviceCode);
		    		spaAppointmentRec.setServiceName(serviceDetail.getName());
		    		spaAppointmentRec.setServiceDescription(serviceDetail.getDescription());
//		    		spaAppointmentRec.setOrderNo(orderNo);
		    		spaAppointmentRec.setOrderDetId(orderDetId);
		    		
		    		String therapistCode = detail.getTherapistCode();
					String startTime = detail.getStartDatetime();
					String endTime = detail.getEndDatetime();
					
					if(therapistCode!=null)
		    		{
						/***
						 * SGG-3800
						 */
						Therapist therapist = getTherapistInfo(therapistCode,
								DateConvertUtil.parseString2Date(startTime, "yyyy-MM-dd"));// update New date to detail.getStartDatetime()
						if(null!=therapist)
						{
							spaAppointmentRec.setTherapistFirstname(therapist
									.getFirstName());
							spaAppointmentRec.setTherapistLastname(therapist
									.getLastName());
						}
						spaAppointmentRec.setTherapistCode(therapistCode);
					}
					
					spaAppointmentRec.setStartDatetime(Timestamp.valueOf(startTime));	
					spaAppointmentRec.setEndDatetime(Timestamp.valueOf(endTime));
					if(null != detail.getRecoveryTime()){
						spaAppointmentRec.setRecoveryTime(Long.valueOf(detail.getRecoveryTime().toString()));
					}
		    		spaAppointmentRec.setStatus(Constant.Status.RSV.name());    		
		    		spaAppointmentRec.setCreateDate(new Timestamp(new Date().getTime()));
		    		spaAppointmentRec.setCreateBy(createdBy);
		    		//added by Kaster 20160331
		    		if(isStaffApp){
		    			spaAppointmentRec.setCreateBy(createdBy);
		    		}
		    		spaAppointmentRec.setCustomerId(customerId);
		    		spaAppointmentRecDao.saveSpaAppointmenRec(spaAppointmentRec); 
		    		
		    		mmsLog.info("Save spaAppointmentRec record, orderNo:"+customerOrderHd.getOrderNo()+" orderDetId: "+orderDetId+" startTime:" + startTime + " endTime:" + endTime + "serviceCode" + serviceCode + "serviceName()" + serviceDetail.getName());
				}   
	        
		}
		mmsLog.info("createLocalAppointment end...");
	}

	@Override
	public ResponseResult queryServiceDetails(String serviceCode)throws Exception {
	
	  try
	  {
		GetServiceDetailsRequest request = new GetServiceDetailsRequest();
		request.setServiceCode(serviceCode);
		 ServiceDetailsResponse response = spaServcieManager.queryServiceDetails(request);
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
	  }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		return responseResult;
	}


	@Override
	public ResponseResult getSubcategories(String categoryCode)throws Exception {
	
	 try
	 {
		SubCategoriesListRequest request = new SubCategoriesListRequest();
		request.setCategoryCode(categoryCode);
		 GetSubcategoriesResponse response =  spaServcieManager.getSubCategoriesListByCode(request);
		 
		 if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
				List<Subcategory> list = new ArrayList<>();
				for (Subcategory subcategory : response.getSubcategories()) {
					if (Constant.initDate().messageCodeMap.get(subcategory.getName()) != null) {
						subcategory.setName(responseResult.getI18nMessge(Constant.initDate().messageCodeMap.get(subcategory.getName())));
					} 
					list.add(subcategory);
				}
				response.setSubcategories(list);
			 responseResult.initResult(GTAError.Success.SUCCESS, response);
			}
	 }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		return responseResult;
	}
	
	@Override
	public ResponseResult getAppointmentDetail(String invoiceNo)throws Exception {
	 
	 try
	 {
		AppointmentDetailsRequest request = new AppointmentDetailsRequest();
		request.setInvoiceNo(invoiceNo);
		AppointmentDetailsResponse response =  spaServcieManager.appointmentDetails(request);		
		if(!response.getSuccess()){
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		}else{
		 responseResult.initResult(GTAError.Success.SUCCESS, response);
		}
	 }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		return responseResult;
	}
	
	
	//this method added for PC function -> add a new service into existing invoice no, and only return the newly added servcie detail.
	@Override
	@Transactional
	public ResponseResult getLatestAppointmentDetail(String invoiceNo)throws Exception {
		 mmsLog.info("getLatestAppointmentDetail start...");
		 mmsLog.info("invoiceNo :"+invoiceNo);
         List<SpaAppointmentRec> resultList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo, "sysId desc");
         SpaAppointmentRec record = resultList.get(0);
         SpaAppointmentDto appointmentDto = new SpaAppointmentDto();
         
         List<SpaAppointmentDetailDto> detailList = new ArrayList<SpaAppointmentDetailDto>();
         
         SpaAppointmentDetailDto detailDto = new SpaAppointmentDetailDto();
         
         Long customerId = record.getCustomerId();
         Member member = this.memberDao.getMemberByCustomerId(customerId);
         appointmentDto.setAccademyNo(member.getAcademyNo());
         
         CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(customerId);
                       
         appointmentDto.setMemberName(profile.getSalutation() + " " + profile.getGivenName() + " " + profile.getSurname());
         
         String serviceCode = record.getExtServiceCode();
         GetServiceDetailsRequest request = new GetServiceDetailsRequest();
         request.setServiceCode(serviceCode);
         ServiceDetailsResponse serviceResponse = spaServcieManager.queryServiceDetails(request);
         
         ServiceDetail serviceDetail = serviceResponse.getServiceDetail();
         detailDto.setServiceName(serviceDetail.getName());
         
         String therapistCode = record.getTherapistCode();
         Therapist therapist = (Therapist)getTherapistInfo(therapistCode, new Date());
         mmsLog.info("customerId:"+customerId+" therapist information:" + therapist);
         detailDto.setTherapistName(therapist.getName());
         
         DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         
         detailDto.setStartDatetime(df.format(record.getStartDatetime()));
         detailDto.setEndDatetime(df.format(record.getEndDatetime()));
         detailDto.setServiceCost(record.getServiceInternalCost());    
         detailList.add(detailDto);            
         
         appointmentDto.setServiceDetails(detailList);
         responseResult.initResult(GTAError.Success.SUCCESS, appointmentDto);
		
		 mmsLog.info("getLatestAppointmentDetail end...");			
		 return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult canCelAppointment(String invoiceNo, String userId) throws Exception{
	  try
	  {
		InvoiceCancelRequest request = new InvoiceCancelRequest();
		request.setInvoiceNo(invoiceNo);
		//hard code first, it should be passed by param like invoiceNo
		request.setComments("cancel");
		AppointmentCancelResponse response = spaServcieManager.appointmentCancel(request);
		if(!response.getSuccess()){
			mmsLog.error(Log4jFormatUtil.logErrorMessage("appointmentCancel", request.toString(), null, response.toString(), response.getMessage()));
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		}else{
			 mmsLog.info(Log4jFormatUtil.logInfoMessage("appointmentCancel", request.toString(), null, response.toString()));
			 return cancelLocalAppointment(invoiceNo,userId, response);
		}
	  }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			mmsLog.error(e.getMessage(),e);
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		return responseResult;
		
	}
	
	@Transactional
	public ResponseResult canCelAppointmentService(String appointmentId, String userId) throws Exception
	{		
		mmsLog.info("cancelAppointmentService start...");
		mmsLog.info("appointmentId:" + appointmentId);
		  try
		  {
			  AppointmentCancelRequest request = new AppointmentCancelRequest();
			  request.setAppointmentId(appointmentId);
			  request.setComments("cancelled by HKGTA request");
			  AppointmentServiceCancelResponse response = spaServcieManager.appointmentServiceCancel(request);
			if(!response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else
			{		
				responseResult = cancelLocalService(appointmentId,userId);				
			}
		  }catch(ResourceAccessException e)
			{
				if (e.getCause() instanceof java.net.ConnectException) {
					responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
				   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
					   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
				   }else
				   {
					   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
				   }
				mmsLog.info(e.getMessage(),e);
			}catch(MMSAPIException e)
			{
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
				mmsLog.info(e.getMessage(),e);
			}
		  	mmsLog.info("cancelAppointmentService end...");
			return responseResult;
			
		}
	
	
	@Transactional
	public ResponseResult cancelLocalService(String appointmentId, String userId)
	{
		try
		{
			SpaAppointmentRec appointment = spaAppointmentRecDao.getUniqueByCol(SpaAppointmentRec.class,"extAppointmentId", appointmentId);
			boolean isCancelOrder = true;
			String invoiceNO=null;
			if (appointment != null) {
				appointment.setStatus(Constant.Status.CAN.name());
				appointment.setUpdateBy(userId);
				appointment.setUpdateDate(new Timestamp(new Date().getTime()));
				spaAppointmentRecDao.update(appointment);
				
				invoiceNO = appointment.getExtInvoiceNo();
				List<SpaAppointmentRec> appointmentList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo",invoiceNO, null);
				if (appointmentList.size() > 1) {
					for (SpaAppointmentRec record : appointmentList) {
						if (!record.getExtAppointmentId().equals(appointment.getExtAppointmentId())) {
							if (!record.getStatus().equals(Constant.Status.CAN.name())) {
								isCancelOrder = false;
								break;
							}
						}
					}
				}
				
			}
			if (isCancelOrder) {
				List<CustomerOrderHd> customerOrderHdList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode",invoiceNO, null);
				CustomerOrderHd customerOrderHd = new CustomerOrderHd();
				if (customerOrderHdList != null
						&& customerOrderHdList.size() > 0) {
					customerOrderHd = customerOrderHdList.get(0);
				}
				// add auto refuned 
				if(customerOrderHd.getOrderStatus().equals(Constant.Status.CMP.name())){
					List<CustomerOrderDet> customerOrderDetList = customerOrderHd.getCustomerOrderDets();
					for(CustomerOrderDet detail : customerOrderDetList){
						generateRefundRequester(userId,detail);
					}
				}
				
				customerOrderHd.setOrderStatus(Constant.Status.CAN.name());
				customerOrderHd.setUpdateBy(userId);
				customerOrderHd.setUpdateDate(new Date());
				customerOrderHdDao.update(customerOrderHd);
			}
			responseResult.initResult(GTAError.Success.SUCCESS);
			
//			List<SpaAppointmentRec> messageAppointmentList = new ArrayList<SpaAppointmentRec>();
//			messageAppointmentList.add(appointment);
//			sentMessage(messageAppointmentList, SceneType.WELLNESS_CANCEL_SERVICE);
			
		}catch(Exception e)
		{
			mmsLog.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION, "Cancel local service failed.");
		}
		return responseResult;
	}
	
	
	@Transactional
	public ResponseResult cancelLocalAppointment(String invoiceNo,String userId, AppointmentCancelResponse response) throws Exception{
		mmsLog.info("MMS cancelLocalAppointment start...");
		List<CustomerOrderHd> customerOrderHdList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode",invoiceNo, null);
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		if (customerOrderHdList != null && customerOrderHdList.size() > 0) {
			customerOrderHd = customerOrderHdList.get(0);
		}				
		if(customerOrderHd.getOrderStatus().equals(Constant.Status.CMP.name())){
			List<CustomerOrderDet> customerOrderDetList = customerOrderHd.getCustomerOrderDets();
			for(CustomerOrderDet detail : customerOrderDetList){
				generateRefundRequester(userId,detail);
			}
		}
		customerOrderHd.setOrderStatus(Constant.Status.CAN.name());	
		customerOrderHd.setUpdateBy(userId);
		customerOrderHd.setUpdateDate(new Date());
		customerOrderHdDao.update(customerOrderHd);		
		
		
		List<SpaAppointmentRec> appointList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo, null);
		for(SpaAppointmentRec appoint : appointList)
		{
			appoint.setStatus(Constant.Status.CAN.name());
			appoint.setUpdateBy(userId);
			appoint.setUpdateDate(new Timestamp(new Date().getTime()));
			spaAppointmentRecDao.update(appoint);
		}
		
		mmsLog.info("MMS cancelLocalAppointment end...");
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		responseResult.setData(invoiceNo);
		
//		sentMessage(appointList, SceneType.WELLNESS_CANCEL_APPOINTMENT);
		
		return responseResult;
	}
	
   private void generateRefundRequester(String userId, CustomerOrderDet detail) {
		CustomerRefundRequest customerRefundRequest = new CustomerRefundRequest();
		customerRefundRequest.setRequesterType("HKGTA");
		customerRefundRequest.setCreateBy(userId);
		customerRefundRequest.setCreateDate(new Date());
		customerRefundRequest.setRefundServiceType("WELLNESS");
		customerRefundRequest.setOrderDetailId(detail.getOrderDetId());
		customerRefundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
		customerRefundRequest.setUpdateBy(userId);
		customerRefundRequest.setUpdateDate(new Date());
		CustomerOrderHd order = detail.getCustomerOrderHd();
		List<CustomerOrderTrans> txns = order.getCustomerOrderTrans();
		if (txns.size() > 0) {
			customerRefundRequest.setRefundTransactionNo(txns.get(0).getTransactionNo());
			customerRefundRequest.setRequestAmount(txns.get(0).getPaidAmount());
			customerRefundRequest.setStatus(Constant.CustomerRefundRequest.PND.getName());
		}
		customerRefundRequestDao.save(customerRefundRequest);
	}
   
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public ResponseResult payment(MMSPaymentDto paymentDto) throws Exception{
			
	 try
	 {
		String paymentMethod = paymentDto.getPaymentMethod();
	    String invoiceNo = paymentDto.getInvoiceNo();
	    
	    mmsLog.info("payment method invoiceNo:" + invoiceNo + " paymentMethod:" + paymentMethod);
	    //for MMS remote call, if the appointment is not exist, create local appointment.
	    List<SpaAppointmentRec> appointmentList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
		if(appointmentList ==null || appointmentList.size()<=0)
		{
			synchronizeRemoteAppoint(invoiceNo,paymentDto);
		}
		if(StringUtils.isEmpty(paymentMethod)){
			paymentDto.setPaymentMethod(Constant.CASH_Value);
		}
        	
		MMSPaymentDto result = mmsPayment(paymentDto);
		/***
		 * the payment isn't same member payment
		 */
		if(null==result)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			return responseResult;
		}
		if (result.getPaymentMethod().equals(Constant.CREDIT_CARD)
				||result.getPaymentMethod().equals(Constant.UNION_PAY)
				||PaymentMethod.AMEX.getCardCode().equals(result.getPaymentMethod()))
		{
			CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, result.getTransNo());
			String redirectUrl = null;
			if(PaymentMethod.AMEX.getCardCode().equals(result.getPaymentMethod())){
				redirectUrl = amexPaymentGatewayService.payment(customerOrderTrans);
			}else{
				redirectUrl = paymentGatewayService.payment(customerOrderTrans);
			}
			mmsLog.info("setting gateway redirectUrl is"+ redirectUrl);
			result.setRedirectUrl(redirectUrl);
		}			
			
		mmsLog.info("payment method order_no:" + result.getOrderNo() + " transNo:" + result.getTransNo());
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		
		if (result.getPaymentMethod().equals(Constant.CASH_Value))
		{
			// for remote payment from MMS invoke, the initial appointment list is null, need get data again.
			if("Y".equals(paymentDto.getIsRemote()))
			{
				appointmentList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
			}
		}
		//member portal/app/iso payment 
		if("N".equals(paymentDto.getIsRemote()))
		{
			 /** SGG-2995
			   **Do not send email and SMS to patron if he pay cash value in ManageMySpa
			   *---------------------------
			  * SGG-3248
			  * Member need receives a confirmation email,when bookong wellness in Member APP(IOS and Android)
			 */
			  sentPaymentReceipt(result, false);//for webporatal payment, need send receipt email to user.
			  sentMessage(appointmentList, SceneType.WELLNESS_PAYMENT_SETTLE); //send confirm message		
		}
		
	 }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e)
		{
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		return responseResult;
	}
	
//	@Transactional
	private void synchronizeRemoteAppoint(String invoiceNo,MMSPaymentDto paymentDto)throws Exception
	{
		mmsLog.info("synchronizeRemoteAppoint for MMS payment by cashValue start...");
		
		AppointmentDetailsResponse  response=null;
		//if the call from MMS , get the appointment info directly, will not call spaServcieManager.appointmentDetails 
		if(paymentDto!=null&&Constant.MMSTHIRD_ITEM_NO.equals(paymentDto.getRemoteType())){
			ObjectMapper mapper = new ObjectMapper(); 
			response = mapper.readValue(paymentDto.getAppointmentInfo(), AppointmentDetailsResponse.class);	
		}else{
			AppointmentDetailsRequest request = new AppointmentDetailsRequest();
			request.setInvoiceNo(invoiceNo);
			response = spaServcieManager.appointmentDetails(request); 	
		}
					
		if(response!=null && response.getSuccess())
		{
			mmsLog.info("AppointmentDetailsResponse from MMS:" + response.toString());
		}else
		{
			mmsLog.info("Get appointment data from MMS failed.");
			throw new Exception("Get appointment data from MMS failed.");
		}
		GuestDetails guestDetail = response.getGuestDetails();
		String guestCode = guestDetail.getUsercode();
		List<AppointmentDetail> detailList = response.getAppointmentDetails();
		
		Member member  = new Member();
		 member = memberDao.getMemberByAcademyNo(guestCode);
		 
		 if(member== null || StringUtils.isEmpty(member.getAcademyNo()))
		 {
			 mmsLog.info("Member not found in HKGTA.");
			 throw new Exception("Member not found in HKGTA.");
		 }
		 //更新最终扣费用户
		 List<CustomerOrderHd>  orderList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode", invoiceNo, null);
		 if (orderList.size() > 0) {
			 CustomerOrderHd order =  orderList.get(0);
			 order.setCustomerId(member.getCustomerId());
			 customerOrderHdDao.update(order);
			
		}
		SpaAppointmentDto localDto = new SpaAppointmentDto();
		
		List<SpaAppointmentDetailDto> detailDtoList = new ArrayList<SpaAppointmentDetailDto>();
		
		localDto.setCustomerId(member.getCustomerId());
		localDto.setInvoiceNo(invoiceNo);
		localDto.setPaymentMethod(Constant.CASH_Value);
		localDto.setRemoteType(paymentDto.getRemoteType());
		localDto.setCreatedBy(paymentDto.getCreatedBy());
		
		SimpleDateFormat sdformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		
		if(detailList!=null && detailList.size()>0)
		{
			for(AppointmentDetail detail : detailList)
			{
				SpaAppointmentDetailDto detailDto = new SpaAppointmentDetailDto();
				detailDto.setAppointmentId(detail.getAppointmentId());
				detailDto.setServiceCode(detail.getAdditionalProperties().get("servicecode").toString());
				detailDto.setServiceName(detail.getItemName());
				detailDto.setServiceTime(Long.valueOf(detail.getSrvcTime().toString()));
				detailDto.setServiceDescription(detail.getItemName());
//				detailDto.setTherapistCode();
				Date startDatetime = sdformat.parse(detail.getStarttime());
				Date endDatetime = sdformat.parse(detail.getEndtime());	
				String startTimeStr = df.format(startDatetime);
				String endTimeStr = df.format(endDatetime);
				
				detailDto.setStartDatetime(startTimeStr);
				detailDto.setEndDatetime(endTimeStr);
				
				detailDto.setServiceCost(new BigDecimal(detail.getFinalPrice().doubleValue()));
				detailDto.setTherapistName(detail.getTherapistName());
				detailDtoList.add(detailDto);							
				mmsLog.info("startTime:" + startTimeStr + "  endTime:" + endTimeStr);
			}
		}
		localDto.setServiceDetails(detailDtoList);
		localDto.setAppointmentInfo(paymentDto.getAppointmentInfo());	
		if(null==detailDtoList||detailDtoList.size()==0)
		{
			/***
			 *  SGG-3464
			 */
			createNoAppointCustomerOrderDet(paymentDto,member);
			mmsLog.info("have no appointment ,excute create CustomerOrder/CustomerOrderDet");
		}else{
			mmsLog.info("have appointment ,excute createLocalAppointment");
			createLocalAppointment(localDto, null);	
		}
		
		mmsLog.info("synchronizeRemoteAppoint for MMS payment by cashValue end...");
	}
	/***
	 * when member payment  no appoint,the need create CustomerOrder /customerOrderDet
	 * eg. SGG-3464 description
	 * @param paymentDto
	 * @param member
	 */
	private void createNoAppointCustomerOrderDet(MMSPaymentDto paymentDto,Member member){
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderDate(new Date());
		customerOrderHd.setOrderStatus(Constant.Status.OPN.name());			
		customerOrderHd.setCustomerId(member.getCustomerId());
		customerOrderHd.setOrderTotalAmount(paymentDto.getAmount());
		customerOrderHd.setCreateBy(member.getUserId());		
		customerOrderHd.setCreateDate(new Timestamp(new Date().getTime()));	
		customerOrderHd.setUpdateBy(member.getUserId());	
		customerOrderHd.setUpdateDate(new Date());
		customerOrderHd.setVendorRefCode(paymentDto.getInvoiceNo());
		customerOrderHdDao.save(customerOrderHd);
		
		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		customerOrderDet.setCustomerOrderHd(customerOrderHd);
		customerOrderDet.setItemNo(Constant.MMSTHIRD_ITEM_NO); 
		customerOrderDet.setOrderQty(1L);
		customerOrderDet.setItemTotalAmout(paymentDto.getAmount());
		customerOrderDet.setExtRefNo(paymentDto.getInvoiceNo()); //set ext ref no as the service code
		customerOrderDet.setCreateDate(new Timestamp(new Date().getTime()));			
		customerOrderDet.setUpdateDate(new Date());	    
		customerOrderDet.setCreateBy(member.getUserId());
		customerOrderDet.setUpdateBy(member.getUserId());
	    customerOrderDetDao.saveOrderDet(customerOrderDet);
	}
	
//	@Transactional
	private MMSPaymentDto mmsPayment(MMSPaymentDto paymentDto)throws Exception{

		mmsLog.info("mmsPayment method process start...");
		
		Long customerId = paymentDto.getCustomerId();
		String invoiceNo = paymentDto.getInvoiceNo();
		String paymentMethod = paymentDto.getPaymentMethod();
		String isRemote = paymentDto.getIsRemote();

		
		AppointmentDetailsResponse  appointmentDetailResponse=null;
		//if the call from MMS , get the appointment info directly, will not call spaServcieManager.appointmentDetails 
		if(paymentDto!=null&&Constant.MMSTHIRD_ITEM_NO.equals(paymentDto.getRemoteType())){
			ObjectMapper mapper = new ObjectMapper(); 
			appointmentDetailResponse = mapper.readValue(paymentDto.getAppointmentInfo(), AppointmentDetailsResponse.class);	
		}else{
			AppointmentDetailsRequest request = new AppointmentDetailsRequest();
			request.setInvoiceNo(invoiceNo);
			appointmentDetailResponse = spaServcieManager.appointmentDetails(request); 
		}
		
		mmsLog.info("Get appointmentResponse for payment:" + appointmentDetailResponse);
		
		if(!appointmentDetailResponse.getSuccess()){
			throw new GTACommonException(GTAError.FacilityError.MMS_API_EXCEPTION, appointmentDetailResponse.getMessage());		
		}
		List<AppointmentDetail> detailList = appointmentDetailResponse.getAppointmentDetails();
		GuestDetails guestDetail = appointmentDetailResponse.getGuestDetails();
		String guestCode = guestDetail.getUsercode();
		
		/***
		 * SGG-3345
		 */
		if(!checkPaymentSameMember(guestCode, invoiceNo)){
			mmsLog.info("hkgta call mms payment API response: the payment isn't same member payment");
			return null;
		}
		
		double totalAmount = 0;
		Member member  = new Member();
		if("N".equals(isRemote))//payment by hkgta webportal
		{
			for (AppointmentDetail detail : detailList) {
				totalAmount = totalAmount + detail.getFinalPrice().doubleValue();
			}
			 member = memberDao.getMemberByCustomerId(paymentDto.getCustomerId());
			 
		}else if("Y".equals(isRemote)){ //payment by MMS remote API.
			
		   totalAmount = paymentDto.getAmount().doubleValue();
		   member = memberDao.getMemberByAcademyNo(guestCode);
		   customerId = member.getCustomerId();
		}
		BigDecimal amount = new BigDecimal(totalAmount);		
		
		String remoteTransId = "";
				
		// CUSTOMER ORDER Header
		List<CustomerOrderHd> customerOrderHdList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode",invoiceNo, null);
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		if (customerOrderHdList != null && customerOrderHdList.size() > 0) {
			customerOrderHd = customerOrderHdList.get(0);
		}
		
		//for cash value, update the member cash value balance.
				if(Constant.CASH_Value.equals(paymentMethod)){

					if("N".equals(isRemote))
					{
						PaymentByCustomRequest parameters = new PaymentByCustomRequest();
						parameters.setInvoiceNo(invoiceNo);
						parameters.setAmountPaid(amount);
						parameters.setTipAmount(new BigDecimal(0));
						parameters.setPaymentMethod(CustomPaymentMethod.CASHVALUE);
						
						PaymentByCustomResponse response = spaServcieManager.paymentByCustom(parameters);
						
						if(!response.getSuccess()){
							throw new GTACommonException(null, response.getMessage());				
						} 
						mmsLog.info("hkgta call mms payment API response:" + response);
						remoteTransId = response.getTransactionID();
					}				
					
					MemberCashValuePaymentDto cashValuePaymentDto = new MemberCashValuePaymentDto();
					cashValuePaymentDto.setCustomerId(customerId);										
					cashValuePaymentDto.setOrderQty(amount.intValue());
					cashValuePaymentDto.setPaymentMethod(paymentMethod);					
					cashValuePaymentDto.setTotalAmount(amount);
					cashValuePaymentDto.setUserId(member.getUserId());

					MemberCashvalue cashValue = memberCashvalueService.updateMemberCashValueData(member, cashValuePaymentDto);										
					
				}
				
//		if(Constant.CASH_Value.equals(paymentMethod) && amount.compareTo(customerOrderHd.getOrderTotalAmount())>=0){
			//keep the same status with MMS, the appointment only can be closed in MMS .
		customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
		customerOrderHd.setOrderRemark("");
		customerOrderHd.setUpdateBy(member.getUserId());
		customerOrderHd.setUpdateDate(new Date());
		/***
		 * @author christ
		 * 
		 */
		/***
		 * christ 2017-03-22
		 * when customerOrderHd is new then orderNo is null 
		 * customerOrderHdDao.update(customerOrderHd); happendException
		 * [ The given object has a null identifier: com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd] 
		 * change saveOrUpdate
		 */
		customerOrderHdDao.saveOrUpdate(customerOrderHd);

		paymentDto.setOrderNo(customerOrderHd.getOrderNo().toString());		
				
	    CustomerOrderTrans customerOrderTrans  = new CustomerOrderTrans();
	    initCustomerOrderTrans(customerOrderTrans);
	    
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setTransactionTimestamp(new Date());
		customerOrderTrans.setPaidAmount(amount);
			
		if(Constant.CASH_Value.equals(paymentMethod)){
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASHVALUE.name());
		}else if(Constant.CREDIT_CARD.equals(paymentMethod)||Constant.UNION_PAY.equals(paymentMethod)){
			customerOrderTrans.setStatus(Constant.Status.PND.name());	
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
		}
		else if(PaymentMethod.AMEX.getCardCode().equals(paymentMethod)){
			customerOrderTrans.setStatus(Constant.Status.PND.name());	
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.AMEX.getCardCode());
		}
		else if(PaymentMethod.CHRGPOST.name().equals(paymentMethod)){
				customerOrderTrans.setStatus(Constant.Status.PND.name());	
				customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
				customerOrderTrans.setPaymentMethodCode(PaymentMethod.CHRGPOST.name());
		}		
		customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
		customerOrderTrans.setPaymentRecvBy(member.getAcademyNo());
		customerOrderTrans.setAgentTransactionNo(remoteTransId);			
		customerOrderTrans.setCreateBy(member.getUserId());
		customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
		customerOrderTrans.setExtRefNo(invoiceNo);
		
		if(Constant.MMSTHIRD_ITEM_NO.equals(paymentDto.getRemoteType()))
		{
			//MMS:Wellness Center-invoice:1287
			//MMS Invoice #2017  SGG-3418
			customerOrderTrans.setInternalRemark("MMS Invoice #"+invoiceNo);
		}
		
		Long transNo = (Long)customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);	

		CustomerOrderTrans result = customerOrderTransDao.getUniqueByCol(CustomerOrderTrans.class, "transactionNo", transNo);
		
		//SpaAppointmentRec rec = spaAppointmentRecDao.getUniqueByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo);
		List<SpaAppointmentRec> recList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
		paymentDto.setTransNo(result.getTransactionNo());
		if(recList != null && recList.size()>0){
			//paymentDto.setReservationId("SPA-" + rec.getSysId() + "-" + paymentDto.getOrderNo());
			//take last 6 characters as reservationId
			StringBuilder sbr  = new StringBuilder();
			for(SpaAppointmentRec rec : recList){
				if(sbr.length()>0) sbr.append(",");
				sbr.append("SPA-" + rec.getExtAppointmentId().substring(rec.getExtAppointmentId().length()-6,rec.getExtAppointmentId().length()) + "-" + rec.getExtInvoiceNo());
			}
			paymentDto.setReservationId(sbr.toString());
		}
		paymentDto.setSettleTime(df.format(result.getTransactionTimestamp()));
			
		/***
		 * IOS 预定wellness ,payment success dispaly totalPrice 
		 * add by christ
		 */
		paymentDto.setTotalPrice(amount);
		mmsLog.info("mmsPayment method process end...");
		return paymentDto;

	}
	/***
	 * SGG-3345
	 * @param academyNo
	 * @param invoiceNo
	 * @return
	 */
	private boolean checkPaymentSameMember(String academyNo,String invoiceNo)
	{
		boolean blg=Boolean.TRUE;
		if(StringUtils.isNotEmpty(academyNo))
		{
			Member member = memberDao.getMemberByAcademyNo(academyNo);	
			if(null!=member)
			{
				List<CustomerOrderHd> customerOrderHdList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode",invoiceNo, null);
				if (null!=customerOrderHdList&& customerOrderHdList.size() > 0) {
					CustomerOrderHd customerOrderHd = customerOrderHdList.get(0);
					if((null!=customerOrderHd.getCustomerId()&&null!=member.getCustomerId())&&
						!customerOrderHd.getCustomerId().equals(member.getCustomerId()))
					{
						blg=Boolean.FALSE;
					}
				}
				
			}
		}
		return blg;
	}
	
	@Override
	public Therapist getTherapistInfo(String therapistCode, Date requestDate){
		
	 try
	 {
		GetCenterTherapistsRequest request = new GetCenterTherapistsRequest();
		request.setRequestDate(requestDate);
		QueryCenterTherapistsResponse response =  spaServcieManager.getCenterTherapists(request);
		if(!response.getSuccess()){
			throw new GTACommonException(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
		}else{
			List<Therapist> therapistList = response.getTherapists(); 
			
			for(Therapist therapist : therapistList){
				
				String userCode = therapist.getUserCode();
				if(StringUtils.isEmpty(userCode)  && therapist.getEmployeeCode() !=null)
				{
                   userCode = therapist.getEmployeeCode();
				}
				if(therapistCode.equals(userCode)){
					return therapist;
				}
			}
		}			
	 }catch(ResourceAccessException e)
		{
			if (e.getCause() instanceof java.net.ConnectException) {
				throw new GTACommonException(GTAError.FacilityError.MMS_CONNECTION_FAILED);		    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   throw new GTACommonException(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else
			   {
				   throw new GTACommonException(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
		}catch(MMSAPIException e)
		{
			throw new GTACommonException(GTAError.FacilityError.MMS_API_EXCEPTION);
		}
		
		return null;
	}
	
	@Override
	@Transactional
	public ResponseResult getMemberInfo(String customerId) throws Exception{
	    
	    MemberCashvalueDto memberCashvalueDto = courseService.getMemberInfo(customerId);		
		responseResult.initResult(GTAError.Success.SUCCESS, memberCashvalueDto);
		return responseResult;
	
	}
	
	@Override
	public String aesEncode(String res, String key)
	{
		if(logger.isDebugEnabled())
		{
			logger.debug("Content for encode:" + res);
		}
	 	String encrypt = EncryptUtil.getInstance().AESencode(res, key);
	 	
	 	if(logger.isDebugEnabled())
		{
			logger.debug("Content encoded:" + encrypt);
		}
	 	return encrypt;
	}
	
	@Override
	public String aesDecode(String res, String key)
	{
	 	String encrypt = EncryptUtil.getInstance().AESdecode(res, key);
	 	
	 	return encrypt;
	}
	
	@Override
	@Transactional
	public SpaCenterInfoDto getSpaCenterInfo()
	{
		SpaCenterInfo spaCenterInfo =  spaCenterInfoDao.getSpaCenterInfo();
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
		
		SpaCenterInfoDto dto = new SpaCenterInfoDto();
		dto.setAdvanceBookPeriod(globalParameter.getParamValue());
		dto.setAppPicFilename(spaCenterInfo.getAppPicFilename());
		dto.setCenterId(spaCenterInfo.getCenterId());
		dto.setCenterName(spaCenterInfo.getCenterName());
		dto.setLocation(spaCenterInfo.getLocation());
		dto.setOpenHour(spaCenterInfo.getOpenHour());
		dto.setPhone(spaCenterInfo.getPhone());
		
		return dto;
		
	}
	
	@Override
	@Transactional
	public ResponseResult updateSpaCenterInfo(SpaCenterInfoDto spaCenterInfoDto, String userId )
	{
//		SpaCenterInfo entity = new SpaCenterInfo();			
		mmsLog.info("updateSpaCenterInfo start...");
		mmsLog.info("username:" + userId);
		SpaCenterInfo  entity = spaCenterInfoDao.getSpaCenterInfo();
		if(entity == null)
		{
			entity = new SpaCenterInfo();
			entity.setCreateBy(userId);
			entity.setCreateDate(new Timestamp(new Date().getTime()));			
		}else
		{
			entity.setUpdateBy(userId);
			entity.setUpdateDate(new Timestamp(new Date().getTime()));
		}
		
		entity.setLocation(spaCenterInfoDto.getLocation());
		entity.setOpenHour(spaCenterInfoDto.getOpenHour());
		entity.setPhone(spaCenterInfoDto.getPhone());
		entity.setAppPicFilename(spaCenterInfoDto.getAppPicFilename());
		entity.setCenterName(spaCenterInfoDto.getCenterName());
				
		spaCenterInfoDao.saveOrUpdate(entity);		
		
		String advanceBookPeriod = spaCenterInfoDto.getAdvanceBookPeriod();
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
		globalParameter.setParamValue(advanceBookPeriod);
		globalParameter.setUpdateBy(userId);
		globalParameter.setUpdateDate(new Date());
		globalParameterDao.update(globalParameter);
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		mmsLog.info("updateSpaCenterInfo end...");
		return responseResult;
	}
	
	@Override
	@Transactional
	public ResponseResult updateSpaCategoryPic(List<SpaPicPathDto> dtoList, String userId)
	{
		mmsLog.info("updateSpaCategoryPic start...");
		mmsLog.info("userId :" +userId);
		if(dtoList!=null && dtoList.size()>0)
		{
			for(SpaPicPathDto dto : dtoList)
			{
				Long picId = dto.getPicId();
				SpaPicPath picPath = spaPicPathDao.getByPicId(picId);
				picPath.setCategoryFileName(dto.getPicFileName());
				picPath.setUpdateBy(userId);
				picPath.setUpdateDate(new Timestamp(new Date().getTime()));
				spaPicPathDao.update(picPath);
			}
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		mmsLog.info("updateSpaCategoryPic end...");
		return responseResult;
	}
	
	@Override
	@Transactional
	public List<SpaCategoryDto> getSpaCategoryList()
	{
		mmsLog.info("getSpaCategoryList start...");
		List<SpaCategory> categoryList = spaCategoryDao.getRootSpaCategorys();
		List<SpaCategoryDto> resultList = new ArrayList<SpaCategoryDto>();
		
		if(categoryList!=null && categoryList.size()>0)
		{
			for(SpaCategory category : categoryList)
			{
				SpaCategoryDto dto = new SpaCategoryDto();
				dto.setCategoryId(category.getCategoryId());
				dto.setCategoryName(category.getCategoryName());
				
				List<SpaPicPath> picList = category.getSpaPicPaths();				
				
				List<SpaPicPathDto> picDtoList = new ArrayList<SpaPicPathDto>();
				if(picList!=null && picList.size()>0)
				{
					for(SpaPicPath pic : picList)
					{
						SpaPicPathDto picDto = new SpaPicPathDto();
						picDto.setPicFileName(pic.getCategoryFileName());
						picDto.setPicId(pic.getPicId());
						picDto.setPicType(pic.getPicType());
						picDtoList.add(picDto);
					}
				}
				
				dto.setCategoryPicList(picDtoList);
				
				resultList.add(dto);
			}
		}
		mmsLog.info("getSpaCategoryList end...");
		return resultList;
	}
	
	@Override
	@Transactional
	public List<SpaCategoryDto> getALLSpaCategoryList()
	{
		mmsLog.info("getSpaCategoryList start...");
		List<SpaCategory> categoryList = spaCategoryDao.getAllSpaCategorys();
		List<SpaCategoryDto> resultList = new ArrayList<SpaCategoryDto>();
		
		if(categoryList!=null && categoryList.size()>0)
		{
			for(SpaCategory category : categoryList)
			{
				SpaCategoryDto dto = new SpaCategoryDto();
				dto.setCategoryId(category.getCategoryId());
				dto.setCategoryName(category.getCategoryName());
				
				List<SpaPicPath> picList = category.getSpaPicPaths();				
				
				List<SpaPicPathDto> picDtoList = new ArrayList<SpaPicPathDto>();
				if(picList!=null && picList.size()>0)
				{
					for(SpaPicPath pic : picList)
					{
						SpaPicPathDto picDto = new SpaPicPathDto();
						picDto.setPicFileName(pic.getCategoryFileName());
						picDto.setPicId(pic.getPicId());
						picDto.setPicType(pic.getPicType());
						picDtoList.add(picDto);
					}
				}
				
				dto.setCategoryPicList(picDtoList);
				
				resultList.add(dto);
			}
		}
		mmsLog.info("getSpaCategoryList end...");
		return resultList;
	}
	
	
	/**
	 * get sub category by parent_category_id
	 */
	@Override
	@Transactional
	public List<SpaCategoryDto> getSpaCategoryList(String parentCategoryId)
	{
		mmsLog.info("getSpaCategoryList start...");
		List<SpaCategory> categoryList = spaCategoryDao.getSubtSpaCategorys(parentCategoryId);
		List<SpaCategoryDto> resultList = new ArrayList<SpaCategoryDto>();
		
		if(categoryList!=null && categoryList.size()>0)
		{
			for(SpaCategory category : categoryList)
			{
				SpaCategoryDto dto = new SpaCategoryDto();
				dto.setCategoryId(category.getCategoryId());
				dto.setCategoryName(category.getCategoryName());
				
				List<SpaPicPath> picList = category.getSpaPicPaths();				
				
				List<SpaPicPathDto> picDtoList = new ArrayList<SpaPicPathDto>();
				if(picList!=null && picList.size()>0)
				{
					for(SpaPicPath pic : picList)
					{
						SpaPicPathDto picDto = new SpaPicPathDto();
						picDto.setPicFileName(pic.getCategoryFileName());
						picDto.setPicId(pic.getPicId());
						picDto.setPicType(pic.getPicType());
						picDtoList.add(picDto);
					}
				}
				
				dto.setCategoryPicList(picDtoList);
				
				resultList.add(dto);
			}
		}
		mmsLog.info("getSpaCategoryList end...");
		return resultList;
	}
	
	
	
	@Override
	@Transactional
	public Long getLatestUpdatedPicId()
	{
		List<SpaPicPath> picList = spaPicPathDao.getByHql("from SpaPicPath order by updateDate desc");
		if(picList!=null && picList.size()>0)
		{
			return picList.get(0).getPicId();
		}else
		{
			return null;
		}
	}
	
	@Transactional
	public void updateSpaRetreat(SpaRetreatDto dto, String userId)
	{
		mmsLog.info("updateSpaRetreat start...");
		mmsLog.info("userId :"+userId);
		SpaRetreat retreat = null;
		if(dto.getRetId()!=null)
		    retreat = spaRetreatDao.getByRetId(dto.getRetId());
		
		if(retreat == null)
		{	 
			retreat = new SpaRetreat();		
			retreat.setCreateBy(userId);
			retreat.setStatus("Show");
			retreat.setDisplayOrder(spaRetreatDao.getMaxDisplayOrder(dto.getCategoryId())+1);
			//retreat.setCategoryId("RETREATS001");//Mars update for CR31, 2016/03/28
			retreat.setCategoryId(dto.getCategoryId());
			retreat.setCreateDate(new Timestamp(new Date().getTime()));
		}else
		{
			retreat.setUpdateBy(userId);
			retreat.setUpdateDate(new Timestamp(new Date().getTime()));			
		}
		if(!StringUtils.isEmpty(dto.getStatus())) //for change status
		{
		  retreat.setStatus(dto.getStatus());
		}else
		{
			retreat.setDescription(dto.getDescription());
			retreat.setPicPath(dto.getPicPath());
			retreat.setRetreatName(dto.getRetName());	
		}		
		
		spaRetreatDao.saveOrUpdate(retreat);
		
		mmsLog.info("updateSpaRetreat end...");
	}
	
	@Transactional
	public void updateSpaRetreatItem(SpaRetreatItemDto dto, String userId)
	{
		mmsLog.info("updateSpaRetreatItem start...");
		mmsLog.info("userId :"+userId);
		SpaRetreatItem item = null;
		if(dto.getItemId()!=null)
		 item = spaRetreatItemDao.getByItemId(dto.getItemId());
		
		if(item == null)
		{
			item = new SpaRetreatItem();
			item.setCreateBy(userId);
			item.setCreateDate(new Timestamp(new Date().getTime()));
			item.setStatus("Show");
			SpaRetreat retreat = spaRetreatDao.getByRetId(dto.getRetId());
			item.setSpaRetreat(retreat);
			
		}else
		{
			item.setUpdateBy(userId);
			item.setUpdateDate(new Timestamp(new Date().getTime()));
		}
		if(!StringUtils.isEmpty(dto.getStatus()))
		{
		   item.setStatus(dto.getStatus());
		}else
		{
			item.setDescription(dto.getDescription());
			 item.setItemName(dto.getItemName());
		}
		
		spaRetreatItemDao.saveOrUpdate(item);		
		mmsLog.info("updateSpaRetreatItem end.");
		
	}
	
	@Transactional
	public String getSpaRetreatListSql(String categoryId)
	{
		String condition = StringUtils.isEmpty(categoryId)?"":" where a.category_id='"+categoryId+"' ";
		String sql = "select * from ( select a.ret_id as retId, ret_name as retName, a.status, a.description, pic_path as picPath, a.display_order as displayOrder,"
				+ " (select count(1) from spa_retreat_item b where b.ret_id = a.ret_id) as itemSize "
				+ " from spa_retreat a "
				+  condition
				+ ") m ";
		return sql;
	}
	
	@Transactional
	public String getSpaRetreatItemListSql(Long retId)
	{
		String sql = "select * from ( select item.item_id as itemId, item.item_name as itemName, "
				+ " item.status as status, item.description as description, r.ret_name as retreatName"
				+ " from spa_retreat_item item, spa_retreat r"
				+ " where item.ret_id = '" + retId + "' and item.ret_id = r.ret_id ) a";
		return sql;
	}
	
	@Transactional
	public void switchRetreatOrder(Long sourceRetId, String moveType, String userId)
	{
		mmsLog.info("switchRetreatStatus start...");
		mmsLog.info("userId :"+userId+" moveType:"+moveType);
		if("top".equals(moveType))
		{
			SpaRetreat sourceRet = spaRetreatDao.getByRetId(sourceRetId);
			String hql = "from SpaRetreat t where t.displayOrder <= ? and t.categoryId=? ";
			List param = new ArrayList();
			param.add(sourceRet.getDisplayOrder());
			param.add(sourceRet.getCategoryId());
			List<SpaRetreat> retreatList = spaRetreatDao.getByHql(hql,param);
			if(retreatList!=null && retreatList.size()>0)
			{
				for(SpaRetreat retreat:retreatList)
				{
					if(retreat.getRetId().longValue() == sourceRetId.longValue())
					{
						retreat.setDisplayOrder(new Long(1));
					}else
					{
						Long displayOrder = retreat.getDisplayOrder();
						Long newOrder = Long.valueOf(displayOrder.longValue() + 1);
						retreat.setDisplayOrder(newOrder);
					}
					spaRetreatDao.update(retreat);
				}
			}
		}else
		{
			SpaRetreat source = spaRetreatDao.getByRetId(sourceRetId);
			
			Long sourceOrder = source.getDisplayOrder();
			Long targetOrder = null;
			 if("up".equals(moveType))
			 {
			    targetOrder = Long.valueOf(sourceOrder.longValue() -1);
			 }else //move down
			 {
				 targetOrder = Long.valueOf(sourceOrder.longValue() +1);
			 }
			
			//SpaRetreat target = spaRetreatDao.getUniqueByCol(SpaRetreat.class, "displayOrder", targetOrder);		
			SpaRetreat target = spaRetreatDao.getUniqueByCols(SpaRetreat.class, new String[]{"displayOrder","categoryId"}, new Serializable[]{targetOrder,source.getCategoryId()});

			source.setDisplayOrder(targetOrder);
			spaRetreatDao.update(source);
			
			if(null!=target){
				target.setDisplayOrder(sourceOrder);
				spaRetreatDao.update(target);
			}
		}
		
		mmsLog.info("switchRetreatStatus end...");
	}
	
	@Transactional
	public ResponseResult sendReceipt(String invoiceNo)throws Exception
	{
		mmsLog.info("sendReceipt start...");
		mmsLog.info("invoiceNo :"+invoiceNo);
		MMSPaymentDto dto = new MMSPaymentDto();
		dto.setInvoiceNo(invoiceNo);
		CustomerOrderHd order = this.customerOrderHdDao.getUniqueByCol(CustomerOrderHd.class, "vendorRefCode", invoiceNo);
		if(order!=null)
		{
			dto.setOrderNo(order.getOrderNo().toString());
			sentPaymentReceipt(dto, false);
			responseResult.initResult(GTAError.Success.SUCCESS);
		}else
		{
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION,"No order record found for this reservation.");
		}
		
		mmsLog.info("sendReceipt end...");
		return responseResult;
	}
	
	@Transactional
	public ResponseResult getSpaRefundList(Integer pageNumber, Integer pageSize, String sortBy, String isAscending,
			String status, String filters) {
		// TODO Auto-generated method stub
		try{
			SpaRefundRequest params =  new SpaRefundRequest();
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			params.setFromDate(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
			params.setToDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			params.setOffset(pageSize);
			params.setPage(pageNumber);
			GetRefundsResponse response =  spaServcieManager.getSpaRefundList(params);
			if(response.getSuccess() == null || !response.getSuccess()){
				responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION, response.getMessage());
			}else{
				List<Refund> refunds = response.getRefunds();
				if(refunds != null && refunds.size() > 0){
					//通过paymentId去匹配到requestId
					List<String> paymentIds = new ArrayList<String>();
					for(int i=0 ; i<refunds.size() ; i++){
						paymentIds.add(refunds.get(i).getRefPaymentId());
					}
					List<SpaRefundDto> dtos =  this.customerRefundRequestDao.matchRequestId(paymentIds);
					Refund refund;
					SpaRefundDto dto;
					for(int i=0 ; i<refunds.size() ; i++){
						for(int j=0 ; j<dtos.size() ; j++){
							refund = refunds.get(i);
							dto = dtos.get(j);
							if(refund.getRefPaymentId().equals(dto.getPaymentId())){
								if(dto.getRefundId() != null){
									refund.setRefundRequest(dto.getRefundId().toString());
								}
							}
						}
					}
					
					//通过academyNo去匹配到customerName
					List<String> academyNos = new ArrayList<String>();
					for(int i=0 ; i<refunds.size() ; i++){
						academyNos.add(refunds.get(i).getPatronId());
					}
					List<SpaRefundDto> customerDto =  this.customerRefundRequestDao.matchCustomerName(academyNos);
					for(int i=0 ; i<refunds.size() ; i++){
						for(int j=0 ; j<customerDto.size() ; j++){
							refund = refunds.get(i);
							dto = customerDto.get(j);
							if(refund.getPatronId().equals(dto.getAcademyNo())){
								refund.setPatronName(dto.getCustomerName());
							}
						}
					}
				}
				for (Refund refund : refunds) {
					if(StringUtils.isNotEmpty(refund.getPaymentType())&&StringUtils.isNotEmpty(refund.getRemark()))
					{
						if(refund.getPaymentType().contains("Custom")&&refund.getRemark().toUpperCase().contains(PaymentMethod.CASHVALUE.getCardCode()))
						{
							refund.setPaymentType(PaymentMethod.CASHVALUE.getDesc());
						}
					}
					
				}
				response.setRefunds(refunds);
				PageBean pageBean =  new PageBean(pageNumber, pageSize, response.getRefunds(), response.getTotal());
				Data data = new Data();
				data.setList(pageBean.getRecordList());
				data.setLastPage(pageBean.getPageCount() > pageBean.getCurrentPage() ? false : true);
				data.setCurrentPage(pageNumber);
				data.setRecordCount(pageBean.getRecordCount());
				data.setPageSize(pageSize);
				data.setTotalPage(pageBean.getPageCount());
				responseResult.initResult(GTAError.Success.SUCCESS, data);
			}
		}catch(ResourceAccessException e){
			if (e.getCause() instanceof java.net.ConnectException) {
				responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_FAILED);			    
			   } else if (e.getCause() instanceof java.net.SocketTimeoutException) {
				   responseResult.initResult(GTAError.FacilityError.MMS_CONNECTION_TIMEOUT);			    
			   }else{
				   responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			   }
			mmsLog.error(e.getMessage(),e);
		}catch(MMSAPIException e){
			responseResult.initResult(GTAError.FacilityError.MMS_API_EXCEPTION);
			mmsLog.error(e.getMessage(),e);
		}
		return responseResult;
	}


	@Transactional
	public ResponseResult generateRefundRequest(String DateTime, String patronId, String paymentType, BigDecimal amount, 
			String refInvoiceId, String refPaymentId, String creditCard, String userId, Long refundRequestId, String remark,String centerrcptno) {
		// TODO Auto-generated method stub
		//一个customerOrderHd有多个customerOrderTrans,customerOrderTrans的状态为success时才可以对该customerOrderTrans创建退款申请
//		CustomerProfile customer = this.customerProfileDao.getById(patronId);
		Member member = this.memberDao.getUniqueByCol(Member.class, "academyNo", patronId);
		if(member == null){
			throw new GTACommonException(GTAError.SPARefundError.NO_EXIST_CUSTOMER);
		}
//		CustomerOrderTrans tran = this.customerOrderTransDao.getUniqueByCol(CustomerOrderTrans.class, "extRefNo", refPaymentId);
		if(refundRequestId != null){
			throw new GTACommonException(GTAError.SPARefundError.REFUND_ALREADY_LAUND);
		}

		/***
		 * SGG-2994
		 */
		CustomerOrderTrans origTrans=null;
		if(StringUtils.isNotEmpty(centerrcptno))
		{
			List<CustomerOrderTrans> trans=customerOrderTransDao.getByHql("from CustomerOrderTrans c  where c.extRefNo='"+centerrcptno+"' and c.paymentMethodCode='"+paymentType+"' and c.paidAmount="+amount+" and c.status='"+Status.SUC.name()+"'" , null);
			if(null==trans||trans.size()==0)
			{
				responseResult.initResult(GTAError.SPARefundError.NO_EXIST_CUSTOMER,"No refund the centerrcptno :"+centerrcptno+" no find in System ");
				return responseResult;
			}else{
				for (CustomerOrderTrans existTrans : trans) {
					/**
					 * 3825
					 */
					origTrans=existTrans;
					break;
					/*
					//if not null ,then next trans for origTrans(refund wellness set repaymentId )
					if(StringUtils.isNotEmpty(existTrans.getAgentTransactionNo()))
					{
						continue;
					}else{
						origTrans=existTrans;
						break;
					}*/
				}
			}
			if(null==origTrans){
				responseResult.initResult(GTAError.SPARefundError.NO_EXIST_CUSTOMER,"No refund the centerrcptno :"+centerrcptno+" no find  valid Customer order trans");
				return responseResult;
			}
			origTrans.setAgentTransactionNo(refPaymentId);
			customerOrderTransDao.saveOrUpdate(origTrans);
			
		}else{
			responseResult.initResult(GTAError.SPARefundError.NO_EXIST_CUSTOMER," The centerrcptno is required ");
			return responseResult;
		}
		/*
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		customerOrderHd.setOrderNo(refInvoiceId);
		customerOrderHd.setVendorRefCode(refInvoiceId);
		customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());
		customerOrderHd.setStaffUserId(userId);
		customerOrderHd.setOrderTotalAmount(amount);
		customerOrderHd.setCustomerId(member.getCustomerId());
		customerOrderHdDao.save(customerOrderHd);
		
		
		CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
		initCustomerOrderTrans(customerOrderTrans);
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setExtRefNo(refPaymentId);
		customerOrderTrans.setPaymentMethodCode(paymentType);
		customerOrderTrans.setStatus(Constant.Status.SUC.toString());
		customerOrderTrans.setPaidAmount(amount);
		try {
			customerOrderTrans.setTransactionTimestamp(new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US).parse(DateTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		customerOrderTransDao.save(customerOrderTrans);
		*/
		
		
		CustomerRefundRequest refundRequest = new CustomerRefundRequest();
		refundRequest.setRequesterType("CUSTOMER");
		refundRequest.setRefundTransactionNo(origTrans.getTransactionNo());
		refundRequest.setRefundServiceType(RefundServiceType.WELLNESS.getName());
		refundRequest.setStatus(Constant.CustomerRefundRequest.PND.getName());
		refundRequest.setRequestAmount(amount);
		refundRequest.setCustomerReason(remark);
//		refundRequest.setRefundMoneyType(Constant.PaymentMethodCode.CASHVALUE.toString());
		refundRequest.setCreateBy(userId);
		refundRequest.setCreateDate(new Date());
		this.customerRefundRequestDao.save(refundRequest);
		
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	/** 
	 * @Description:把list转换为一个用逗号分隔的字符串 
	 */  
	public String listToString(List list) {  
	    StringBuilder sb = new StringBuilder();  
	    if (list != null && list.size() > 0) {  
	        for (int i = 0; i < list.size(); i++) {  
	            if (i < list.size() - 1) {  
	                sb.append("'" + list.get(i) + "',");  
	            } else {  
	                sb.append("'" + list.get(i) + "'");  
	            }  
	        }  
	    }  
	    return sb.toString();  
	}


	@Transactional
	public ResponseResult getRefundedSpaPaymentList(ListPage page) {
		// TODO Auto-generated method stub
		ListPage<RefundedSpaPaymentDto> memberOverviewList = this.customerOrderTransDao.getRefundedSpaPaymentList(page);
		Data data = new Data();
		data.setList(memberOverviewList.getDtoList());
		data.setLastPage(memberOverviewList.isLast());
		data.setCurrentPage(memberOverviewList.getNumber());
		data.setRecordCount(memberOverviewList.getAllSize());
		data.setPageSize(memberOverviewList.getSize());
		data.setTotalPage(memberOverviewList.getAllPage());
		responseResult.initResult(GTAError.Success.SUCCESS,data);
		return responseResult;
	} 
	
	/**
	 * 是否为24小时之后的时间
	 * @param startTimeStr
	 * @return
	 */
	public boolean isAfter24Hours(Date startTime){
		try {
			String after24HoursTimeStr = DateCalcUtil.getDateAfter(new Date(), 1);
//			Date startTime = DateCalcUtil.parseDateTime(startTimeStr);
			Date after24HoursTime = DateCalcUtil.parseDateTime(after24HoursTimeStr);
			if (startTime.compareTo(after24HoursTime) == 1) {
				return true;
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 手动添加MMS付款记录
	 * @param paymentDto
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional
	public MMSPaymentDto manualAddMmsPayment(MMSPaymentDto paymentDto, String userId)throws Exception{

		mmsLog.info("mmsPayment method process start...");
		
		Long customerId = paymentDto.getCustomerId();
		String invoiceNo = paymentDto.getInvoiceNo();
		String paymentMethod = paymentDto.getPaymentMethod();
		Member member  =  memberDao.getMemberByCustomerId(paymentDto.getCustomerId());
		BigDecimal amount = paymentDto.getAmount();	
		
		String remoteTransId = "";
				
		// CUSTOMER ORDER Header
		List<CustomerOrderHd> customerOrderHdList = customerOrderHdDao.getByCol(CustomerOrderHd.class, "vendorRefCode",invoiceNo, null);
		CustomerOrderHd customerOrderHd = new CustomerOrderHd();
		if (customerOrderHdList != null && customerOrderHdList.size() > 0) {
			customerOrderHd = customerOrderHdList.get(0);
		}
		//扣费
		MemberCashValuePaymentDto cashValuePaymentDto = new MemberCashValuePaymentDto();
		cashValuePaymentDto.setCustomerId(customerId);										
		cashValuePaymentDto.setOrderQty(amount.intValue());
		cashValuePaymentDto.setPaymentMethod(paymentMethod);					
		cashValuePaymentDto.setTotalAmount(amount);
		cashValuePaymentDto.setUserId(member.getUserId());
		MemberCashvalue cashValue = memberCashvalueService.updateMemberCashValueData(member, cashValuePaymentDto);										
		if (null == customerOrderHd.getCustomerId() || customerOrderHd.getCustomerId() != member.getCustomerId()) {
			customerOrderHd = new CustomerOrderHd();
			customerOrderHd.setCustomerId(member.getCustomerId());
		}
//		if(Constant.CASH_Value.equals(paymentMethod) && amount.compareTo(customerOrderHd.getOrderTotalAmount())>=0){
			//keep the same status with MMS, the appointment only can be closed in MMS .
		customerOrderHd.setOrderStatus(Constant.Status.CMP.name());
		customerOrderHd.setOrderRemark("SACV");
		
		customerOrderHd.setVendorRefCode(invoiceNo);
		customerOrderHd.setUpdateBy(userId);
//		if (null == customerOrderHd.getCustomerId() || customerOrderHd.getCustomerId() != member.getCustomerId()) {
//			customerOrderHd.setCustomerId(member.getCustomerId());
//		}
		if (null == customerOrderHd.getOrderTotalAmount()) {
			customerOrderHd.setOrderTotalAmount(paymentDto.getAmount());
		} else {
			customerOrderHd.setOrderTotalAmount(customerOrderHd.getOrderTotalAmount().add(paymentDto.getAmount()));
		}
		Date today = new Date();
		Timestamp date = new Timestamp(today.getTime());
		if (null == customerOrderHd.getCreateDate()) {
			customerOrderHd.setCreateDate(date);
		}
		if (null == customerOrderHd.getCreateBy()) {
			customerOrderHd.setCreateBy(userId);
		}
		if (null == customerOrderHd.getOrderDate()) {
			customerOrderHd.setOrderDate(today);
		}
		customerOrderHd.setUpdateDate(today);
		if (customerOrderHd.getCustomerId() != member.getCustomerId()) {
			customerOrderHdDao.save(customerOrderHd);
		} else {
			customerOrderHdDao.saveOrUpdate(customerOrderHd);
		}
		CustomerOrderDet customerOrderDet = new CustomerOrderDet();
		customerOrderDet.setCustomerOrderHd(customerOrderHd);
		customerOrderDet.setCreateBy(userId);
		customerOrderDet.setCreateDate(new Timestamp(new Date().getTime()));
		customerOrderDet.setItemNo(Constant.MMSTHIRD_ITEM_NO);
		customerOrderDet.setItemTotalAmout(amount);
		customerOrderDet.setExtRefNo(invoiceNo);
		customerOrderDetDao.saveOrUpdate(customerOrderDet);
		paymentDto.setOrderNo(customerOrderHd.getOrderNo().toString());		
				
	    CustomerOrderTrans customerOrderTrans  = new CustomerOrderTrans();
	    initCustomerOrderTrans(customerOrderTrans);
	    
		customerOrderTrans.setCustomerOrderHd(customerOrderHd);
		customerOrderTrans.setTransactionTimestamp(new Date());
		customerOrderTrans.setPaidAmount(amount);
		
			
		if(Constant.CASH_Value.equals(paymentMethod)){
			customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.toString());
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.CASHVALUE.name());
		}	
		customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
		customerOrderTrans.setPaymentRecvBy(member.getAcademyNo());
		customerOrderTrans.setAgentTransactionNo(remoteTransId);			
		customerOrderTrans.setCreateBy(userId);
		customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
		customerOrderTrans.setExtRefNo(invoiceNo);
		
		if(Constant.MMSTHIRD_ITEM_NO.equals(paymentDto.getRemoteType()))
		{
			// TODO SGG-3404 说把MMS:Wellness Center-invoice:'Invoice' 的格式变成 MMS Invoice # 'Invoice ID'
			customerOrderTrans.setInternalRemark("MMS Invoice #"+invoiceNo + (null == paymentDto.getResvIdStr() ?  "": "@#;" + paymentDto.getResvIdStr()));
		}
		
		Long transNo = (Long)customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);	

		CustomerOrderTrans result = customerOrderTransDao.getUniqueByCol(CustomerOrderTrans.class, "transactionNo", transNo);
		
		//SpaAppointmentRec rec = spaAppointmentRecDao.getUniqueByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo);
		List<SpaAppointmentRec> recList = spaAppointmentRecDao.getByCol(SpaAppointmentRec.class, "extInvoiceNo", invoiceNo,null);
		return paymentDto;

	}


	@Override
	@Transactional
	public void sendMailSystemConfigMail(MMSPaymentDto dto) 
	{
		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_WELLNESS_RESERVATION_BY_MOBILE_APP);
		GlobalParameter global=globalParameterDao.get(GlobalParameter.class, GlobalParameterType.WELLNESS_RESERVATION_BY_MOBILE_APP.getCode());
		if(null!=template&&null!=global)
		{
			CustomerEmailContent cec = getCustomerEmailContentByParam(template, dto);
			cec.setRecipientEmail(global.getParamValue());
			cec.setNoticeType("RCWC");
			cec.setCreateDate(new Date());
			customerEmailContentService.saveCustomerEmailContent(cec);
			mailThreadService.send(cec, null);	
		}
	}
	private CustomerEmailContent getCustomerEmailContentByParam(MessageTemplate template,MMSPaymentDto dto){
		CustomerEmailContent cec = null;
		Member member = memberDao.getMemberByCustomerId(dto.getCustomerId());
		CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(dto.getCustomerId());
		List<SpaAppointmentRec> recs = spaAppointmentRecDao.getAppointmentRecsByInvoiceNo(dto.getInvoiceNo());
		if (null != recs) {
			SpaAppointmentRec rec=recs.get(0);
			String htmlContent = null;
			if (null != member && null != profile) {
				cec = new CustomerEmailContent();
				String date = DateConvertUtil.formatCurrentDate(rec.getStartDatetime(), "yyyy-MM-dd HH:mm:ss");
				htmlContent = template.getFullContentHtml(member.getAcademyNo(),
						profile.getGivenName() + " " + profile.getSurname(), dto.getInvoiceNo(), rec.getServiceName(),
						date);
				cec.setContent(htmlContent);
				cec.setSubject(template.getMessageSubject().replace("{transaction_complete_datetime}", date));
			}
		}
		return cec;
	}
}
