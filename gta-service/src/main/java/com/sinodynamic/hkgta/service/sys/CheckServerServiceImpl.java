package com.sinodynamic.hkgta.service.sys;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.http.HttpUrl;

@Repository
public class CheckServerServiceImpl implements CheckServerService {
	Logger				logger	= Logger.getLogger(CheckServerServiceImpl.class);
	@Resource(name = "appProperties")
	private Properties	appProps;
	 private static ExecutorService pool = Executors.newCachedThreadPool();
	public void check() {

		try {
			String url = appProps.getProperty(Constant.CHECK_SERVER_URL);
			logger.info("start checking:[" + url + "]");
			String response = HttpUrl.sendGet(url, "");
			if (StringUtils.isBlank(response)) {
				String toAddress = appProps.getProperty(Constant.CHECK_SERVER_TO_EMAIL);

				if (!StringUtils.isBlank(toAddress)) {					
					String subject = "Server Alert";										
					String content = "The server link " + url + " is unreachable. Please check the server power / service process / network status.\n\nTime: " + new Date();
					
					if (url.contains("ZOOMTECH")) {
						toAddress = appProps.getProperty(Constant.EMAIL_ALERT_GROUP[0]);
						subject="[ALERT] Zoomtech PC middleware out of control";
						content = "\n The middleware running on zoomtech PC is out of control.\n Please check the server status. If the issue still happens, you may resume the service by restarting the PC.\n\nTime: " + new Date();
					}	
						
					logger.debug("start sending email:" + toAddress);
					
					boolean isOK = MailSender.sendEmail(toAddress, null, null, subject, content, null);
					logger.info("email has been sent:" + isOK);
				}
			}

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

}
