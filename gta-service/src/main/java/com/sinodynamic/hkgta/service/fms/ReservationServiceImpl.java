package com.sinodynamic.hkgta.service.fms;

import org.springframework.stereotype.Service;

@Service
public class ReservationServiceImpl implements ReservationService {

	@Override
	public StringBuilder getReserSqlByDate(String monthDate) {

		StringBuilder sql = new StringBuilder();
		sql.append(" select * FROM( "); 
		
		 sql.append(" select  * FROM ( ");
		 sql.append(createGolfTennisSql(monthDate));
		 sql.append(" )as t_golfTennis");
		 sql.append(" UNION ALL ");
		 
		 sql.append(" select  * FROM (");
		 sql.append(createCourseSql(monthDate));
		 sql.append(" )as t_course");
		 
		 sql.append(" UNION ALL ");
		 sql.append(" select  * FROM (");
		 sql.append(createRestaurantSql(monthDate));
		 sql.append(" )as t_restaurant");
		 
		 sql.append(" UNION ALL ");
		 sql.append(" select  * FROM (");
		 sql.append(createGuestRoomSql(monthDate));
		 sql.append(" )as t_guestRoom");
		 
		 sql.append(" UNION ALL ");
		 sql.append(" select  * FROM (");
		 sql.append(createWellnessSql(monthDate));
		 sql.append(" )as t_wellness");
		 sql.append(" ) AS book "); 
		 sql.append(" ORDER BY starttime ASC "); 
		 
		return sql;
	}

	/***
	 * GOLF OR TENNIS or private coach /GSS or TSS
	 * 
	 * @return
	 */
	private StringBuilder createGolfTennisSql(String monthDate) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" CASE  ");
		sql.append("  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NULL THEN 'Golfing Bay' ");
		sql.append("  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NULL THEN 'Tennis Court' ");
		sql.append("  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NOT NULL THEN 'Golf private Coaching' ");
		sql.append("  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NOT NULL THEN 'Tennis private Coaching' ");
		sql.append("  END AS  serviceType,");
		sql.append("  CASE  ");
		sql.append("  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NULL THEN CONCAT('GBF','-',mb.resv_id) ");
		sql.append("  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NULL THEN  CONCAT('TCF','-',mb.resv_id) ");
		sql.append("  WHEN mb.resv_facility_type='GOLF' AND mb.exp_coach_user_id IS NOT NULL THEN  CONCAT('GPC','-',mb.resv_id) ");
		sql.append("  WHEN mb.resv_facility_type='TENNIS' AND mb.exp_coach_user_id IS NOT NULL THEN CONCAT('TPC','-',mb.resv_id) ");
		sql.append("  END  AS reservationId,");
		sql.append("  m.academy_no AS patronId,");
		sql.append(" CONCAT(cp.given_name,' ',cp.surname)  AS patronName,");
		sql.append(" DATE_FORMAT(mb.begin_datetime_book,'%Y/%m/%d')  AS startTime,");
		sql.append(" DATE_FORMAT(mb.end_datetime_book,'%Y/%m/%d') AS endTime,");
		sql.append("  ( ");
		sql.append("SELECT ");
		sql.append("   IF(mb.status='ATN',CONCAT(IF(mb.resv_facility_type='GOLF','Bay# ', 'Court# '),GROUP_CONCAT(DISTINCT fm.facility_name ORDER BY CAST(fm.facility_no AS SIGNED) ASC SEPARATOR ',')), ");
		sql.append("	( ");
		sql.append("	CASE mb.resv_facility_type ");
		sql.append("	WHEN 'GOLF' THEN fac.caption ");
		sql.append("	WHEN 'TENNIS' THEN fst.name ");
		sql.append("    END	");
		sql.append("	) ");
		sql.append("   )  FROM member_reserved_facility AS mrf ");
		sql.append("      LEFT JOIN  facility_timeslot AS flt ON  flt.facility_timeslot_id = mrf.facility_timeslot_id ");
		sql.append("      LEFT JOIN facility_master AS fm ON flt.facility_no = fm.facility_no ");
		sql.append("      WHERE mrf.resv_id=mb.resv_id ");
		sql.append(" )AS location, ");
		
		sql.append(" CASE");
		sql.append("  WHEN rftb.sys_id IS NULL THEN");
			sql.append(" CASE ");
			sql.append(" WHEN um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF' THEN 'in-person' ");
			sql.append(" WHEN um.user_type='CUSTOMER' THEN 'online' ");
			sql.append(" WHEN um.user_type IS NULL THEN 'online'  ");
			sql.append(" END");
		sql.append("  WHEN rftb.sys_id IS NOT NULL THEN CONCAT('Bundled','',mb.resv_id) ");
		sql.append(" END  AS sourceType, ");
		
		//update by Colin begin 
		sql.append(" CASE ");
		sql.append(" WHEN mb. STATUS = 'ATN' THEN 'Checked-in' ");
		sql.append(" WHEN mb. STATUS = 'CAN' THEN 'Canceled' ");
		sql.append(" WHEN mb. STATUS = 'PND' AND ADDTIME(mb.create_date, '00:15:00') >= NOW() THEN 'Paying' ");
		sql.append(" WHEN mb. STATUS = 'PND' AND ADDTIME(mb.create_date, '00:15:00') < NOW() THEN 'Pending for Payment' ");
		sql.append(" ELSE ");
		sql.append("   CASE ");
		sql.append("     WHEN NOW() <= mb.end_datetime_book THEN 'Ready to check-in' ");
		sql.append("     ELSE ");
		sql.append("      CASE WHEN NOW() > mb.end_datetime_book THEN 'Overdue' ELSE 'Unknown' END ");
		sql.append("     END ");
		sql.append(" END  AS status, ");
		
		sql.append(" CASE");
		sql.append("  WHEN rftb.sys_id IS NULL THEN");
		//update by Colin end 
		//update christ 
			sql.append(" CASE");
			//if createBy is staff  show staffName 
			sql.append("  WHEN mb.create_by IS NOT NULL AND (um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF')  THEN  CONCAT(sp.given_name,' ',sp.surname)");
			// if createBy is customer show customerName
			sql.append("  WHEN mb.create_by IS NOT NULL AND um.user_type='CUSTOMER' THEN CONCAT(cp.given_name,' ',cp.surname)");
			//if createBy is null show customerName
			sql.append("  WHEN mb.create_by IS NULL  THEN CONCAT(cp.given_name,' ',cp.surname) ");
			sql.append(" END");
		sql.append("  WHEN   rftb.sys_id IS NOT NULL THEN  'System'");
		sql.append(" END AS createBy,");
  
		sql.append(" DATE_FORMAT(mb.create_date,'%Y/%m/%d') AS createDate, ");
		sql.append("  '1' AS qty  ");
		sql.append(" FROM member_facility_type_booking AS mb ");
		sql.append(" LEFT JOIN room_facility_type_booking AS rftb ON mb.resv_id=rftb.facility_type_resv_id ");
		sql.append(" LEFT JOIN customer_profile AS cp ON mb.customer_id=cp.customer_id ");
		sql.append(" LEFT JOIN user_master AS um  ON mb.create_by =um.user_id ");
		sql.append("  LEFT JOIN staff_profile AS sp ON mb.create_by=sp.user_id ");
		
		sql.append(" LEFT JOIN facility_sub_type AS fst  ON mb.facility_subtype_id=fst.subtype_id  ");
		sql.append(" LEFT JOIN member_facility_book_addition_attr AS maa ON mb.resv_id=maa.resv_id  ");
		sql.append(" LEFT JOIN facility_attribute_caption fac ON maa.attribute_id=fac.attribute_id ");
		sql.append(" LEFT JOIN member m ON  mb.customer_id=m.customer_id ");
		sql.append(" WHERE DATE_FORMAT(mb.begin_datetime_book,'%Y-%m') = '"+monthDate+"'");

		return sql;
	}

	/***
	 * create golf/tennis course
	 * 
	 * @return
	 */
	private StringBuilder createCourseSql(String monthDate) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" CASE  cm.course_type ");
		sql.append(" WHEN 'GSS' THEN 'Golf Course'");
		sql.append(" WHEN 'TSS' THEN 'Tennis Course'");
		sql.append(" END AS serviceType,");
		sql.append(" CASE cm.course_type ");
		sql.append(" WHEN 'GSS' THEN CONCAT('GCE','-',ce.enroll_id)");
		sql.append(" WHEN 'TSS' THEN CONCAT('TCE','-',ce.enroll_id)");
		sql.append(" END AS reservationId,");
		sql.append(" m.academy_no AS patronId, ");
		sql.append(" CONCAT(cp.given_name,' ',cp.surname)  AS patronName, ");
		sql.append(" DATE_FORMAT(cm.regist_begin_date,'%Y/%m/%d') AS startTime, ");
		sql.append(" DATE_FORMAT(cm.regist_due_date,'%Y/%m/%d') AS endTime,");
		sql.append(" CASE  cm.course_type ");
		sql.append(" WHEN 'GSS' THEN 'Golf Bay'");
		sql.append(" WHEN 'TSS' THEN 'Tennis Court' END ");
		sql.append(" AS location, ");
		
		sql.append(" CASE  ");
		sql.append(" WHEN um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF' THEN 'in-person' ");
		sql.append(" WHEN um.user_type='CUSTOMER' THEN 'online'");
		sql.append(" WHEN um.user_type IS NULL THEN 'online'  ");
		sql.append(" END  AS sourceType,");
		
		sql.append("  CASE ce.status ");
		sql.append("  WHEN 'ACT' THEN 'accepted' ");
		sql.append("  WHEN 'REG' THEN 'registered'");
		sql.append("  WHEN 'PND' THEN 'pending'");
		sql.append("  WHEN 'REJ' THEN 'rejected'");
		sql.append("  WHEN 'CAN' THEN 'Cancelled'");
		sql.append("  END  AS status,");
		
		//christ update createBy 
		sql.append(" CASE ");
		sql.append("  WHEN ce.create_by IS NOT NULL AND (um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF') THEN  CONCAT(sp.given_name,' ',sp.surname)");
		sql.append("  WHEN ce.create_by IS NOT NULL AND um.user_type='CUSTOMER'  THEN CONCAT(cp.given_name,' ',cp.surname)");
		sql.append("  WHEN ce.create_by IS NULL  THEN CONCAT(cp.given_name,' ',cp.surname)")	;
		sql.append("  END  AS createBy,");
		  
		sql.append("  DATE_FORMAT(ce.create_date,'%Y/%m/%d') AS createDate, ");
		sql.append("  '1' AS qty     ");
		sql.append(" FROM  course_master AS cm, ");
		sql.append(" course_enrollment AS ce, ");
		sql.append(" customer_profile AS cp, ");
		sql.append(" user_master AS um, ");
		sql.append(" staff_profile AS sp, ");
		sql.append(" member m ");
		
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND cm.course_id = ce.course_id ");
		sql.append(" AND ce.customer_id = cp.customer_id ");
		sql.append(" AND um.user_id = ce.create_by ");
		sql.append(" AND ce.customer_id = m.customer_id ");
		sql.append(" AND ce.create_by=sp.user_id ");
		sql.append(" AND DATE_FORMAT(ce.enroll_date,'%Y-%m') = '"+monthDate+"'");
		return sql;
	}

	/***
	 * restaurant booking sql
	 * 
	 * @return
	 */
	private StringBuilder createRestaurantSql(String monthDate) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append("  'Restaurant' AS serviceType,");
		sql.append("  CASE rb.restaurant_id");
		sql.append(" WHEN '0001' THEN CONCAT('CAF','-',rb.resv_id)");
		sql.append(" WHEN '0002' THEN CONCAT('BAR','-',rb.resv_id)");
		sql.append(" WHEN '0003' THEN CONCAT('RES','-',rb.resv_id)");
		sql.append(" WHEN '0004' THEN CONCAT('SKD','-',rb.resv_id)");
		sql.append(" WHEN '0005' THEN CONCAT('RTL','-',rb.resv_id)");
		sql.append(" END as reservationId ,");
		sql.append(" m.academy_no AS patronId,");
		sql.append(" CONCAT(cp.given_name,' ',cp.surname)  AS patronName,");
		sql.append(" DATE_FORMAT(rb.book_time,'%Y/%m/%d') AS startTime, ");
		sql.append(" DATE_FORMAT(rb.book_time,'%Y/%m/%d') AS endTime,");
		sql.append(" rm.restaurant_name AS location,");
		sql.append(" CASE ");
		sql.append(" WHEN um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF' THEN 'in-person' ");
		sql.append(" WHEN um.user_type='CUSTOMER' THEN 'online' ");
		sql.append(" WHEN um.user_type IS NULL THEN 'online'  ");
		sql.append(" END  AS sourceType,");
		sql.append(" CASE rb.status ");
		sql.append(" WHEN 'RSV' THEN 'resevered' ");
		sql.append(" WHEN 'ATN' THEN 'attended' ");
		sql.append(" WHEN 'NAT' THEN 'not attended' ");
		sql.append(" WHEN 'CAN' THEN 'cancel booking' ");
		sql.append(" WHEN 'PND' THEN 'pending' ");
		sql.append(" WHEN 'REJ' THEN 'Rejected' ");
		sql.append(" WHEN 'WAIT' THEN 'Wait for confirmation' ");
		sql.append(" WHEN 'EXP' THEN 'Expired' ");
		
		sql.append(" END  AS status,");
		
		//update christ createBy
		sql.append(" CASE ");
		sql.append(" WHEN rb.create_by IS NOT NULL AND (um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF') THEN  CONCAT(sp.given_name,' ',sp.surname)");
		sql.append(" WHEN rb.create_by IS NOT NULL AND um.user_type='CUSTOMER'  THEN CONCAT(cp.given_name,' ',cp.surname)");
		sql.append(" WHEN rb.create_by IS NULL  THEN CONCAT(cp.given_name,' ',cp.surname)");	
		sql.append(" END  AS createBy,");
		
		sql.append(" DATE_FORMAT(rb.create_date,'%Y/%m/%d') AS createDate,");
		sql.append(" '1' AS qty ");
		sql.append(" FROM restaurant_customer_booking AS rb ");
		sql.append(" LEFT JOIN customer_profile AS cp ON rb.customer_id=cp.customer_id ");
		sql.append(" LEFT JOIN user_master AS um  ON um.user_id =rb.create_by ");
		sql.append("  LEFT JOIN staff_profile AS sp ON rb.create_by=sp.user_id ");
		sql.append(" LEFT JOIN restaurant_master rm ON rm.restaurant_id=rb.restaurant_id ");
		sql.append(" LEFT JOIN member m ON  rb.customer_id=m.customer_id ");
		
		
		sql.append(" WHERE DATE_FORMAT(rb.book_time,'%Y-%m')='"+monthDate+"'");
		return sql;
	}

	/***
	 * guest room booking sql
	 * 
	 * @return
	 */
	private StringBuilder createGuestRoomSql(String monthDate) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append("  'Guest Room' AS serviceType,");
		sql.append(" CONCAT('GRM','-',rr.resv_id) AS reservationId,");
		sql.append(" m.academy_no  AS patronId,");
		sql.append(" CONCAT(cp.given_name,' ',cp.surname)  AS patronName,");
		sql.append(" DATE_FORMAT(rr.arrival_date,'%Y/%m/%d') AS startTime, ");
		sql.append(" DATE_FORMAT(rr.departure_date,'%Y/%m/%d') AS endTime,");
		sql.append(" rm.room_no AS location,");
		sql.append(" CASE ");
		sql.append(" WHEN um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF' THEN 'in-person'");
		sql.append(" WHEN um.user_type='CUSTOMER' THEN 'online'");
		sql.append(" WHEN um.user_type IS NULL THEN 'online' ");
		sql.append(" END  AS sourceType,");
		sql.append(" CASE rr.status");
		
		sql.append(" WHEN 'SUC' THEN 'success' ");
		sql.append(" WHEN 'IGN' THEN 'ignore'");
		sql.append(" WHEN 'CAN' THEN 'cancelled'");
		sql.append(" WHEN 'RFU' THEN 'refund' ");
		sql.append(" WHEN 'RSV' THEN 'resevered' ");
		
		sql.append(" END  AS status,"); 
		//update christ createBy
		sql.append(" CASE ");
		sql.append("  WHEN rr.create_by IS NOT NULL AND (um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF') THEN  CONCAT(sp.given_name,' ',sp.surname)");
		sql.append("  WHEN rr.create_by IS NOT NULL AND um.user_type='CUSTOMER'  THEN CONCAT(cp.given_name,' ',cp.surname)");
		sql.append("  WHEN rr.create_by IS NULL  THEN CONCAT(cp.given_name,' ',cp.surname)");	
		sql.append("  END AS createBy,");
		
		sql.append(" DATE_FORMAT(rr.create_date,'%Y/%m/%d') AS createDate, ");
		sql.append(" '1' AS qty ");
		sql.append(" FROM room_reservation_rec AS rr ");
		sql.append(" LEFT JOIN customer_profile AS cp ON rr.customer_id=cp.customer_id ");
		sql.append(" LEFT JOIN user_master AS um  ON um.user_id =rr.create_by ");
		sql.append(" LEFT JOIN staff_profile AS sp ON rr.create_by=sp.user_id");
		sql.append(" LEFT JOIN room AS rm ON rr.room_id=rm.room_id ");
		sql.append(" LEFT JOIN member m ON  rr.customer_id=m.customer_id ");
		sql.append(" WHERE DATE_FORMAT(rr.arrival_date,'%Y-%m') = '"+monthDate+"'");

		return sql;
	}

	/***
	 * wellness center booking sql
	 * 
	 * @return
	 */
	private StringBuilder createWellnessSql(String monthDate) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" 'Wellness Center' AS serviceType,");
		sql.append("  CONCAT('SPA','-',spa.sys_id) AS reservationId, ");
		sql.append("  m.academy_no AS patronId,");
		sql.append("  CONCAT(cp.given_name,' ',cp.surname)  AS patronName,");
		sql.append("  DATE_FORMAT(spa.start_datetime,'%Y/%m/%d') AS startTime,");
		sql.append("  DATE_FORMAT(spa.end_datetime,'%Y/%m/%d') AS endTime,");
		//sql.append("  rm.room_no AS location, "); 
		sql.append(" 'Wellness centre' AS location, ");
		sql.append("  CASE ");
		sql.append("  WHEN um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF' THEN 'in-person' ");
		sql.append("  WHEN um.user_type='CUSTOMER' THEN 'online' ");
		sql.append("  WHEN um.user_type IS NULL THEN 'online'  ");
		
		sql.append("  END  AS sourceType, ");
		sql.append("  CASE spa.status ");
		sql.append("  WHEN 'OPN' THEN 'open appointment' ");
		sql.append("  WHEN 'SUC' THEN 'success' ");
		sql.append("  WHEN 'RSV' THEN 'resevered' ");
		sql.append("  WHEN 'CAN' THEN 'cancel booking' ");
		sql.append("  WHEN 'CLD' THEN 'closed' ");
		sql.append("  END  AS status,");
 		 
//		sql.append("  um.nickname AS createBy,");
		sql.append(" CASE ");
		sql.append("  WHEN coh.create_by  IS NOT NULL AND (um.user_type='ADMIN' OR um.user_type='SYSADMIN' OR um.user_type='STAFF') THEN  CONCAT(sp.given_name,' ',sp.surname)");
		sql.append("  WHEN coh.create_by IS NOT NULL AND um.user_type='CUSTOMER'  THEN CONCAT(cp.given_name,' ',cp.surname)");
		sql.append("  WHEN coh.create_by IS NOT NULL AND um.user_type IS NULL  THEN coh.create_by");
		sql.append("  WHEN coh.create_by IS NULL  THEN CONCAT(cp.given_name,' ',cp.surname)	");
		sql.append("  END AS createBy,");
		sql.append("  DATE_FORMAT(spa.create_date,'%Y/%m/%d') AS createDate, ");
		sql.append("  '1' AS qty  ");
		sql.append("  FROM spa_appointment_rec AS spa ");
		sql.append("  LEFT JOIN customer_profile AS cp ON spa.customer_id=cp.customer_id ");
		 
		sql.append(" LEFT JOIN customer_order_det cod ON spa.order_det_id = cod.order_det_id");
		sql.append(" LEFT JOIN customer_order_hd coh ON cod.order_no = coh.order_no");
		sql.append(" LEFT JOIN user_master AS um ON um.user_id = coh.create_by ");
		
		sql.append("  LEFT JOIN staff_profile AS sp ON coh.create_by =sp.user_id ");
		sql.append("  LEFT JOIN room AS rm ON spa.room_id=rm.room_id ");
		sql.append("  LEFT JOIN member m ON  spa.customer_id=m.customer_id ");
		sql.append("  WHERE  DATE_FORMAT(spa.start_datetime,'%Y-%m') = '"+monthDate+"'");
		return sql;
	}

}
