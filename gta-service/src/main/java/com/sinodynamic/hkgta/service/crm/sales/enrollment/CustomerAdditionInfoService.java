package com.sinodynamic.hkgta.service.crm.sales.enrollment;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.crm.AllowMarketingDto;
import com.sinodynamic.hkgta.dto.memberapp.MemberAppCustomerAdditionInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CustomerAdditionInfoService extends IServiceBase<CustomerAdditionInfo>{
	
	public CustomerAdditionInfo getCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public void saveCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public void updateCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public void deleteCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;

	public ResponseResult editCustomerAdditionInfo(
			MemberAppCustomerAdditionInfoDto additionInfoDto, String  userId);
	/**
	 * 是否批准营销服务
	 * @param dto
	 * @param userId
	 * @return
	 */
	public ResponseResult isAllowMarketing(AllowMarketingDto dto, String userId);

	/**
	 * 获取Marketing id
	 */
	public void initMarketingValue();
}
