package com.sinodynamic.hkgta.service.mms;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.mms.SpaFlexDailySummaryDao;
import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.entity.mms.SpaFlexDailySummary;

@Service
public class SpaFlexDailySummaryServiceImpl implements SpaFlexDailySummaryService {

	@Autowired
	private SpaFlexDailySummaryDao spaFlexDailySummaryDao;
	
	@Override
	@Transactional
	public Serializable save(SpaFlexDailySummary spaFlexDailySummary) 
	{
		return spaFlexDailySummaryDao.save(spaFlexDailySummary);
	}

	@Override
	@Transactional
	public boolean deleteByTransactionDate(String transDate) {
		
		return spaFlexDailySummaryDao.deleteByTransactionDate(transDate);
	}

	@Override
	@Transactional
	public List<PaymentFacilityDto> getSpaFlexDailySummaryByDate(String transDate) {
		// TODO Auto-generated method stub
		String hql="SELECT (SUM(spa.credit)-SUM(spa.debit))AS amount,spa.account_code AS accountCode FROM spa_flex_daily_summary spa "
				+ " WHERE  spa.transaction_date ='"+transDate+"' GROUP BY spa.account_code ";
		return spaFlexDailySummaryDao.getDtoBySql(hql, null, PaymentFacilityDto.class);
	}

}
