package com.sinodynamic.hkgta.service.crm.membercash;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface CashvalueTopupHistoryService extends IServiceBase <CashvalueTopupHistory> {
	
	public void topupThroughVirtualAccInBatch(List<Map<String,String>> param);
	
	
}
