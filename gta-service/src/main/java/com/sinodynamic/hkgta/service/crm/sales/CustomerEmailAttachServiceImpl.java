package com.sinodynamic.hkgta.service.crm.sales;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class CustomerEmailAttachServiceImpl extends ServiceBase<CustomerEmailAttach> implements
		CustomerEmailAttachService {
	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;
	
	@Override
	@Transactional
	public void savecustomerEmailAttach(CustomerEmailAttach attach) {
		// TODO Auto-generated method stub
		customerEmailAttachDao.save(attach);
	}

}
