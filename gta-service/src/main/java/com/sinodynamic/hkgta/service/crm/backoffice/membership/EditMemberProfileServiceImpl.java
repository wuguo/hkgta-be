package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.CustomerAdditionInfoDao;
import com.sinodynamic.hkgta.dao.crm.CustomerAddressDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.StaffProfileDao;
import com.sinodynamic.hkgta.dao.mms.SpaMemberSyncDao;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoPK;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;
import com.sinodynamic.hkgta.entity.crm.CustomerAddressPK;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.notification.SMSService;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.adm.UserRecordActionLogService;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileServiceImpl;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.EnrollmentStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PassportType;
import com.sinodynamic.hkgta.util.constant.UserRecordActionType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Service
@SuppressWarnings("deprecation")
public class EditMemberProfileServiceImpl extends ServiceBase<CustomerProfile> implements EditMemberProfileService{
	
	private Logger logger = Logger.getLogger(CustomerProfileServiceImpl.class);
    
	@Autowired
	private CustomerProfileDao customerProfileDao;
	
	@Autowired
	private CustomerAdditionInfoDao customerAdditionInfoDao;
	
	@Autowired
	private CustomerAddressDao customerAddressDao;
	
	@Autowired 
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private SMSService smsService;
	
	@Autowired 
	private SpaMemberSyncDao spaMemberSyncDao;
	
	@Autowired 
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired 
	private StaffProfileDao staffProfileDao;
	
	@Autowired
	private CustomerEmailContentDao		customerEmailContentDao;
	
	@Autowired
	private MailThreadService mailThreadService;
	
	@Autowired
	private UserRecordActionLogService userRecordActionLogService;
	
	public ResponseMsg checkInputData(CustomerProfile dto) {
		if("IPM".equals(dto.getMemberType()) || "IDM".equals(dto.getMemberType())){
			List<CustomerAdditionInfo> ciList = dto.getCustomerAdditionInfos();
			if (ciList != null && ciList.size() > 0) {
				for (CustomerAdditionInfo info : ciList) {
					if (!CommUtil.notEmpty(info.getCaptionId() + "")) {
						//return new ResponseMsg("1", "Caption Id is required!");
						responseMsg.initResult(GTAError.EnrollError.CAPTION_ID_EMPTY);
						return responseMsg;
					}
					if (!CommUtil.notEmpty(info.getSysId() + "")) {
						//return new ResponseMsg("1", "Sys Id is required!");
						responseMsg.initResult(GTAError.EnrollError.SYS_ID_EMPTY);
						return responseMsg;
					}
				}
			}
			CustomerEnrollment customerEnrollment = new CustomerEnrollment();
			if("IPM".equals(dto.getMemberType())){
				customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(dto.getCustomerId());
			}else if("IDM".equals(dto.getMemberType())){
				customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(this.memberDao.getMemberByCustomerId(dto.getCustomerId()).getSuperiorMemberId());
			}
			
	//		if ((customerEnrollment.getStatus().equals(EnrollmentStatus.NEW.getDesc()) ||
	//				customerEnrollment.getStatus().equals(EnrollmentStatus.ANC.getDesc()) ||
	//				customerEnrollment.getStatus().equals(EnrollmentStatus.CMP.getDesc()) ||
	//				customerEnrollment.getStatus().equals(EnrollmentStatus.PYF.getDesc()) ||
	//				customerEnrollment.getStatus().equals(EnrollmentStatus.APV.getDesc())) &&
	//				dto.getSuperiorMemberId() == null && dto.getCustomerEnrollments() != null
	//				&& dto.getCustomerEnrollments().get(0) != null) {
	//			CustomerEnrollment enrollment = dto.getCustomerEnrollments().get(0);
	//			if(StringUtils.isEmpty(enrollment.getSubscribePlanNo())){
	//				responseResult.initResult(GTAError.EnrollError.SERVICE_PLAN_MANDATORY);
	//				return responseResult;
	//			}
	//		}
			
			String mandatoryLevel = "";
			
			if(customerEnrollment.getEnrollId() != null){
				if(customerEnrollment.getStatus().equals(EnrollStatus.OPN.getName()) || customerEnrollment.getStatus().equals(EnrollStatus.REJ.getName()) || customerEnrollment.getStatus().equals(EnrollStatus.CAN.getName())){
					mandatoryLevel = "Low";
				}
				if(customerEnrollment.getStatus().equals(EnrollStatus.NEW.getName())){
					mandatoryLevel = "Medium";
				}
				if(customerEnrollment.getStatus().equals(EnrollStatus.APV.getName()) || customerEnrollment.getStatus().equals(EnrollStatus.ANC.getName()) || customerEnrollment.getStatus().equals(EnrollStatus.CMP.getName())
						|| customerEnrollment.getStatus().equals(EnrollStatus.PYF.getName()) || customerEnrollment.getStatus().equals(EnrollStatus.PYA.getName()) || customerEnrollment.getStatus().equals(EnrollStatus.TOA.getName())){
					mandatoryLevel = "High";
				}
			}
			
			if (("Medium".equals(mandatoryLevel) || "High".equals(mandatoryLevel))
					&& dto.getCustomerEnrollments() != null
					&& dto.getCustomerEnrollments().get(0) != null) {
				CustomerEnrollment enrollment = dto.getCustomerEnrollments().get(0);
				if(StringUtils.isEmpty(enrollment.getSubscribePlanNo())){
					responseResult.initResult(GTAError.EnrollError.SERVICE_PLAN_MANDATORY);
					return responseResult;
				}
			}
			
			StringBuilder ms = new StringBuilder();
			
			if ("High".equals(mandatoryLevel)){
				List<CustomerAddress> caList = dto.getCustomerAddresses();
				if ("false".equalsIgnoreCase(dto.getCheckBillingAddress())) {
					if (caList != null && caList.size() > 0) {
						for (CustomerAddress ca : caList) {
							if (!CommUtil.notEmpty(ca.getAddress1())) {
								//return new ResponseMsg("1", "Bill Address is required!");
								responseMsg.initResult(GTAError.EnrollError.BILL_ADDRESS_EMPTY);
								return responseMsg;
							}
							if (!CommUtil.notEmpty(ca.getHkDistrict())) {
								//return new ResponseMsg("1", "Bill District is required!");
								responseMsg.initResult(GTAError.EnrollError.BILL_DISTRICT_EMPTY);
								return responseMsg;
							}
							if (!CommUtil.notEmpty(ca.getAddressType())) {
								//return new ResponseMsg("1", "Bill Address Type is required!");
								responseMsg.initResult(GTAError.EnrollError.BILL_ADDRESS_TYPE_EMPTY);
								return responseMsg;
							}
						}
					}
		
				}
				
				if (!CommUtil.notEmpty(dto.getPassportNo())) {
					ms.append("Passport No, ");
				}
				if(StringUtils.isEmpty(dto.getContactEmail())){
					ms.append("Contact Email, ");
				}
				if (!CommUtil.notEmpty(dto.getGender())) {
					ms.append("Gender, ");
				}
				if (!CommUtil.notEmpty(dto.getPhoneMobile())) {
					ms.append("Phone Mobile, ");
				}
				if (!CommUtil.notEmpty(dto.getPostalAddress1())) {
					ms.append("Postal Address1, ");
				}
//				if (!CommUtil.notEmpty(dto.getPostalAddress2())) {
//					ms.append("Postal Address2, ");
//				}
				if (!CommUtil.notEmpty(dto.getPostalDistrict())) {
					ms.append("Postal District, ");
				}
				if (!CommUtil.notEmpty(dto.getDateOfBirth())) {
					ms.append("DateOfBirth, ");
				}
			}
			
			if (!CommUtil.notEmpty(dto.getPassportType())) {
				ms.append("Passport Type, ");
			}
			if (!CommUtil.notEmpty(dto.getSalutation())) {
				ms.append("Salutation, ");
			}
			if (!CommUtil.notEmpty(dto.getSurname())) {
				ms.append("Last Name(English), ");
			}
			if (!CommUtil.notEmpty(dto.getGivenName())) {
				ms.append("First Name(English), ");
			}
			if (!CommUtil.notEmpty(dto.getCheckBillingAddress())) {
				ms.append("CheckBillingAddress, ");
			}
	
			if (ms.length() > 0) {
				responseResult.initResult(GTAError.EnrollError.GENERAL_REQ,new String[]{ms.toString().substring(0, ms.length() - 2)});
				return responseResult;
			}
	
			if (!PassportType.HKID.name().equals(dto.getPassportType())
					&& !PassportType.VISA.name().equals(dto.getPassportType())) {
				responseResult.initResult(GTAError.EnrollError.PASS_TYPE_INCORR);
				return responseResult;
			}
	
			if (PassportType.HKID.name().equals(dto.getPassportType()) && !StringUtils.isEmpty(dto.getPassportNo()) && !CommUtil.validateHKID(dto.getPassportNo())) {
				responseResult.initResult(GTAError.EnrollError.HKID_INVAILD);
				return responseResult;
			}
			if (PassportType.VISA.name().equals(dto.getPassportType())&& !StringUtils.isEmpty(dto.getPassportNo()) && !CommUtil.validateVISA(dto.getPassportNo())) {
				responseResult.initResult(GTAError.EnrollError.PASSPORT_INVALID);
				return responseResult;
			}
			if (!StringUtils.isEmpty(dto.getPhoneMobile()) && !CommUtil.validatePhoneNo(dto.getPhoneMobile())) {
				responseResult.initResult(GTAError.EnrollError.MOBILE_PHONE_INVALID);
				return responseResult;
			}
			if (!StringUtils.isEmpty(dto.getPhoneBusiness()) && !CommUtil.validatePhoneNo(dto.getPhoneBusiness())) {
				responseResult.initResult(GTAError.EnrollError.BUSINESS_PHONE_INVALID);
				return responseResult;
			}
			if (!StringUtils.isEmpty(dto.getPhoneHome()) && !CommUtil.validatePhoneNo(dto.getPhoneHome())) {
				responseResult.initResult(GTAError.EnrollError.HOME_PHONE_INVALID);
				return responseResult;
			}
			if (!StringUtils.isEmpty(dto.getContactEmail()) && !CommUtil.validateEmail(dto.getContactEmail())) {
				responseResult.initResult(GTAError.EnrollError.EMAIL_INVALID);
				return responseResult;
			}
		}else if("CPM".equals(dto.getMemberType()) || "CDM".equals(dto.getMemberType())){
			List<CustomerAdditionInfo> ciList = dto.getCustomerAdditionInfos();
			if (ciList != null && ciList.size() > 0) {
				for (CustomerAdditionInfo info : ciList) {
					if (!CommUtil.notEmpty(info.getCaptionId() + "")) {
						//return new ResponseMsg("1", "Caption Id is required!");
						responseMsg.initResult(GTAError.EnrollError.CAPTION_ID_EMPTY);
						return responseMsg;
					}
					if (!CommUtil.notEmpty(info.getSysId() + "")) {
						//return new ResponseMsg("1", "Sys Id is required!");
						responseMsg.initResult(GTAError.EnrollError.SYS_ID_EMPTY);
						return responseMsg;
					}
				}
			}

			List<CustomerAddress> caList = dto.getCustomerAddresses();
			if ("false".equalsIgnoreCase(dto.getCheckBillingAddress())) {
				if (caList != null && caList.size() > 0) {
					for (CustomerAddress ca : caList) {
						if (!CommUtil.notEmpty(ca.getAddress1())) {
							//return new ResponseMsg("1", "Bill Address is required!");
							responseMsg.initResult(GTAError.EnrollError.BILL_ADDRESS_EMPTY);
							return responseMsg;
						}
						if (!CommUtil.notEmpty(ca.getHkDistrict())) {
							//return new ResponseMsg("1", "Bill District is required!");
							responseMsg.initResult(GTAError.EnrollError.BILL_DISTRICT_EMPTY);
							return responseMsg;
						}
//						if (!CommUtil.notEmpty(ca.getAddressType())) {
//							//return new ResponseMsg("1", "Bill Address Type is required!");
//							responseMsg.initResult(GTAError.EnrollError.BILL_ADDRESS_TYPE_EMPTY);
//							return responseMsg;
//						}
					}
				}

			}
			
			StringBuilder ms = new StringBuilder();
			if (!CommUtil.notEmpty(dto.getPhoneMobile())) {
				ms.append("Phone Mobile, ");
			}
			if (!CommUtil.notEmpty(dto.getContactEmail())) {
				ms.append("Contact Email, ");
			}
			if (!CommUtil.notEmpty(dto.getPostalDistrict())) {
				ms.append("Postal District, ");
			}
			if (!CommUtil.notEmpty(dto.getPassportType())) {
				ms.append("PassportType, ");
			}
			if (!CommUtil.notEmpty(dto.getPassportNo())) {
				ms.append("PassportNo, ");
			}

			if (ms.length() > 0) {
				responseMsg.initResult(GTAError.EnrollError.GENERAL_REQ,new String[]{ms.toString().substring(0, ms.length() - 2)});
				return responseMsg;
			}

			if (!StringUtils.isEmpty(dto.getPhoneMobile()) && !CommUtil.validatePhoneNo(dto.getPhoneMobile())) {
				responseMsg.initResult(GTAError.EnrollError.MOBILE_PHONE_INVALID);
				return responseMsg;
			}
			if (!StringUtils.isEmpty(dto.getPhoneBusiness()) && !CommUtil.validatePhoneNo(dto.getPhoneBusiness())) {
				responseMsg.initResult(GTAError.EnrollError.BUSINESS_PHONE_INVALID);
				return responseMsg;
			}
			if (!StringUtils.isEmpty(dto.getPhoneHome()) && !CommUtil.validatePhoneNo(dto.getPhoneHome())) {
				responseMsg.initResult(GTAError.EnrollError.HOME_PHONE_INVALID);
				return responseMsg;
			}
			
		}
		
		responseMsg.initResult(GTAError.Success.SUCCESS);
		return responseMsg;
	}
	
	public StringBuffer createEmailContent(CustomerProfile originProfile,CustomerProfile targetProfile){
		StringBuffer sb = new StringBuffer();

		if(isValueChange(originProfile.getPhoneMobile(),targetProfile.getPhoneMobile())){
			sb.append("<p>Mobile Phone:"+originProfile.getPhoneMobile()+"</p>\r \r");
		}
		if(isValueChange(originProfile.getPostalAddress1(),targetProfile.getPostalAddress1())){
			sb.append("<p>Residential Address:"+originProfile.getPostalAddress1()+"</p>\r \r");
		}
		if(isValueChange(originProfile.getContactEmail(),targetProfile.getContactEmail())){
			sb.append("<p>Pri.Email Address:"+originProfile.getContactEmail()+"</p>\r \r");
		}
		if(isValueChange(originProfile.getPhoneBusiness(),targetProfile.getPhoneBusiness())){
			sb.append("<p>Business Phone:"+originProfile.getPhoneBusiness()+"</p>\r \r");
		}
		if(isValueChange(originProfile.getPhoneHome(),targetProfile.getPhoneHome())){
			sb.append("<p>Home Phone:"+originProfile.getPhoneHome()+"</p>\r \r");
		}
		String customerAddress1="";
		String targetCustomerAddress1="";
		if(originProfile.getCustomerAddresses()!=null && originProfile.getCustomerAddresses().size() >0){
			for(CustomerAddress address: originProfile.getCustomerAddresses()){
				CustomerAddressPK apk = new CustomerAddressPK();
				apk.setAddressType(address.getAddressType());
				apk.setCustomerId(originProfile.getCustomerId());
				address.setId(apk);
				customerAddress1=address.getAddress1();
				//get target customer address
				CustomerAddress targetCustomerAddress = customerAddressDao.get(CustomerAddress.class, apk);
				if(targetCustomerAddress !=null){
					targetCustomerAddress1=targetCustomerAddress.getAddress1();
				}
				break;
			}
		}

		if(isValueChange(customerAddress1,targetCustomerAddress1)){
			sb.append("<p>Business Address:"+customerAddress1+"</p>\r \r");
		}
		if(isValueChange(originProfile.getInternalRemark(),targetProfile.getInternalRemark())){
			sb.append("<p>Remarks:"+originProfile.getInternalRemark()+"</p>\r \r");
		}
		
		return sb;
	}
	
	public Boolean isValueChange(String originValue,String targetValue){
		if(originValue==null&&targetValue==null){
			return false;
		}else if(originValue==null&&targetValue!=null){
			return true;
		}else if(originValue!=null&&targetValue==null){
			return true;
		}else if(!originValue.equals(targetValue)){
			return true;
		}
					
		return false;
	}
	
	@Transactional
	public ResponseMsg editMemberProfile(CustomerProfile dto,String userId){
		Long customerId = dto.getCustomerId();
		if(customerId==null){
			//return new ResponseMsg("1","Customer Id is required for edit customer profile!");
			responseMsg.initResult(GTAError.EnrollError.CUSTOMER_ID_EMPTY);
			return responseMsg;
		}
		
		ResponseMsg res = checkInputData(dto);
		if(res.getReturnCode() != "0") return res;
		CustomerProfile targetProfile = customerProfileDao.get(CustomerProfile.class, customerId);
		
//		/***
//		 * log user_record_action_log  
//		 */
//		userRecordActionLogService.save(targetProfile,userId,UserRecordActionType.C.name());
		
		boolean checkForID = customerProfileDao.checkAvailablePassportNo(customerId, dto.getPassportType(),dto.getPassportNo());
		if (!checkForID) {
			throw new GTACommonException(GTAError.EnrollError.ID_EXIST_IN_REG,new Object[]{dto.getPassportType(),dto.getPassportNo()});
		}
		
		//check whether the filed is change 
		StringBuffer contentSB=createEmailContent(dto,targetProfile);
		
		dto.setUpdateDate(new Date());
		dto.setUpdateBy(userId);
			
		Member member = memberDao.get(Member.class, customerId);
		if (null != member.getAcademyNo()) {
			spaMemberSyncDao.addSpaMemberSyncWhenUpdate(targetProfile, dto);
		}
		List<CustomerAddress> customerAddresses = customerAddressDao.getByCol(CustomerAddress.class, "id.customerId", customerId, null);
		//Persistent instance to detached instance for updating 
		customerProfileDao.getCurrentSession().evict(targetProfile);
		targetProfile.setCustomerAddresses(customerAddresses);
		boolean isUpdate = true;
		if (dto.equals(targetProfile)) {
			isUpdate = false;
		}
		String[] ignoreFileds = new String[]{"portraitPhoto","signature","id","contactClassCode","isDeleted","companyName","createBy","createDate"};
		BeanUtils.copyProperties(dto, targetProfile,ignoreFileds);
		//Add version control
		targetProfile.setVersion(dto.getVersion());		
		targetProfile.setDateOfBirth(DateConvertUtil.parseString2Date(dto.getDateOfBirth(),"yyyy-MM-dd"));
		customerProfileDao.update(targetProfile);
		if(dto.getCustomerAdditionInfos()!=null && dto.getCustomerAdditionInfos().size()>0){
			for(CustomerAdditionInfo cust : dto.getCustomerAdditionInfos()){
				CustomerAdditionInfoPK cpk = new CustomerAdditionInfoPK();
				cpk.setCaptionId(Long.valueOf(cust.getCaptionId()));
				cpk.setCustomerId(customerId);
				cust.setId(cpk);
				cust.setCreateBy(userId);
				cust.setUpdateDate(new Date());
				CustomerAdditionInfo targetEntity = customerAdditionInfoDao.get(CustomerAdditionInfo.class, cpk);
				
				if(targetEntity != null){
					cust.setUpdateBy(userId);
					cust.setUpdateDate(new Date());
					//Persistent instance to detached instance for updating 
					customerAdditionInfoDao.getCurrentSession().evict(targetEntity);;
					String[] ignoreFileds1 = new String[]{"id","createBy","createDate"};
					BeanUtils.copyProperties(cust, targetEntity,ignoreFileds1);
					//Add version control
					targetEntity.setVersion(cust.getVersion());
					customerAdditionInfoDao.update(targetEntity);
				}else{
					customerAdditionInfoDao.save(cust);
				}
			}
		}

		if(dto.getCustomerAddresses()!=null && dto.getCustomerAddresses().size() >0){
			for(CustomerAddress address: dto.getCustomerAddresses()){
				CustomerAddressPK apk = new CustomerAddressPK();
				apk.setAddressType(address.getAddressType());
				apk.setCustomerId(customerId);
				address.setId(apk);
				if("true".equalsIgnoreCase(dto.getCheckBillingAddress())){
					address.setAddress1(dto.getPostalAddress1());
					address.setAddress2(dto.getPostalAddress2());
					address.setHkDistrict(dto.getPostalDistrict());
				}
				CustomerAddress targetEntity = customerAddressDao.get(CustomerAddress.class, apk);
				if(targetEntity != null){
					String[] ignoreFileds2 = new String[]{"id"};
					BeanUtils.copyProperties(address,targetEntity,ignoreFileds2);
					customerAddressDao.update(targetEntity);
				}else{
					customerAddressDao.save(address);
				}
			}
		}
		
		member.setRelationshipCode(dto.getRelationshipCode());
		member.setVip(dto.getVip());
		memberDao.update(member);
		if (isUpdate && Constant.Status.ACT.name().equalsIgnoreCase(member.getStatus())&&contentSB.length()>0&&Constant.REMOTE_TYPE.equalsIgnoreCase(dto.getRemoteType())) {
			try {
				MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_UPDATE_USER_PROFILE);							
				if (mt == null) {
					responseResult.initResult(GTAError.TemplateError.MESSAGE_TEMPLATE_NO_FOUND);
					return responseResult;
				}
				
				CustomerEnrollment customerEnrollment=customerEnrollmentDao.getCustomerEnrollmentByCustomerId(customerId);
				if (customerEnrollment == null && member.getSuperiorMemberId() != null) {
					customerEnrollment = customerEnrollmentDao.getCustomerEnrollmentByCustomerId(member.getSuperiorMemberId());
				}
				
				/***
				 *  the messageTemplate function_id 'update_user_profile' add {academyId}
				 */
				String salesFollowBy=customerEnrollment.getSalesFollowBy();
				StaffProfile staffProfile=staffProfileDao.getStaffProfileByUserId(salesFollowBy);
				String contentMT = mt.getFullContentHtml(staffProfile.getGivenName()+" "+ staffProfile.getSurname(),member.getAcademyNo(),contentSB.toString());
				
				CustomerEmailContent customerEmailContent = new CustomerEmailContent();
				customerEmailContent.setRecipientCustomerId(customerId.toString());
				customerEmailContent.setSendDate(new Date());
				customerEmailContent.setSenderUserId(userId);
				customerEmailContent.setCopyto(null);
				customerEmailContent.setSubject(mt.getMessageSubject());
				customerEmailContent.setContent(contentMT);
				customerEmailContent.setRecipientEmail(staffProfile.getContactEmail());
				customerEmailContent.setStatus(EmailStatus.PND.name());
				customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_ACCOUNT);				
				String sendId = (String) customerEmailContentDao.save(customerEmailContent);
				customerEmailContent.setSendId(sendId);
				mailThreadService.sendWithResponse(customerEmailContent, null, null, null);
				
			} catch (Exception e) {
				logger.debug("Send email failed!", e);
			}
		}
		responseMsg.initResult(GTAError.Success.SUCCESS);
		return responseMsg;
	} 

	}

