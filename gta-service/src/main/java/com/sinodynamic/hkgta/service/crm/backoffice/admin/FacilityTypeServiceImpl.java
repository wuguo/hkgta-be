package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityTypeDao;
import com.sinodynamic.hkgta.entity.fms.FacilityType;
import com.sinodynamic.hkgta.service.ServiceBase;

@Service
public class FacilityTypeServiceImpl  extends ServiceBase<FacilityType> implements FacilityTypeService {
	
	@Autowired
	private FacilityTypeDao dao;
	@Transactional
	@Override
	public List<FacilityType> getAllFacilityType() {
		String hqlstr = "FROM FacilityType";
		List<FacilityType> retList = this.dao.excuteByHql(FacilityType.class, hqlstr);
		for(FacilityType temp : retList){
			temp.setFacilityMasters(null);
		}
		return retList;
	}
	
	@Override
	@Transactional
	public FacilityType getFacilityType(String typeCode)
	{
		return dao.get(FacilityType.class, typeCode);
	}

}
