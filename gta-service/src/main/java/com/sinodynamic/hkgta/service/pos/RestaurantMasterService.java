package com.sinodynamic.hkgta.service.pos;

import java.util.List;

import com.sinodynamic.hkgta.dto.pos.RestaurantMasterDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantCustomerBooking;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RestaurantMasterService extends IServiceBase<RestaurantMaster> {
	
	public List<RestaurantMasterDto> getRestaurantMasterList(String imageFeatureCode);
	
	List<RestaurantCustomerBooking> getRestaurantBookingByRestaurantIdAndCustomerId(String restaurantId, Long customerId);

	void updateRestaurant(RestaurantMasterDto restaurantMasterDto, String restaurantId, String userId);

	RestaurantMasterDto getRestaurant(String restaurantId);
	
	RestaurantMaster getRestaurantMaster(String restaurantId);

	public void updateRestaurantReservationStatus();
}
