package com.sinodynamic.hkgta.service.crm.pub;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dto.crm.pub.NoticeDto;
import com.sinodynamic.hkgta.entity.crm.Notice;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.Data;

public interface NoticeService extends IServiceBase<Notice> {

	long save(NoticeDto noticeDto, String createBy);
	
	void update(NoticeDto noticeDto, String createBy);
	
	NoticeDto getNotice(long noticeId);
	
	void updateStatus(Long noticeId, String status, String userId);
	
	Data getNoticeList(Integer pageNumber, Integer pageSize);

	void updateExpiredNewsToInactive();
	
	public List<Notice> getNoticeByDate(Date date);
	
	public void pushNotice(Notice notice,String pushBy);
	/**
	 * get notice subject to push
	 * @param date
	 * @return
	 */
	public List<Notice> getPushNoticeSubjectByDate(Date date);
	
}
