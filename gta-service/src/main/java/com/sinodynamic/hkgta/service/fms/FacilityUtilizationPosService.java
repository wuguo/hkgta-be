package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationPosDto;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationPos;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface FacilityUtilizationPosService extends IServiceBase<FacilityUtilizationPos> {

	List<FacilityUtilizationPos> getFacilityUtilizationPosList(String facilityType);
	
	void saveFacilityUtilizationPos(FacilityUtilizationPosDto facilityUtilizationPosDto);
}
