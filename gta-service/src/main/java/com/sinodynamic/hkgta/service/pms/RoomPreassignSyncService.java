package com.sinodynamic.hkgta.service.pms;

import com.sinodynamic.hkgta.entity.pms.RoomPreassignSync;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface RoomPreassignSyncService extends IServiceBase<RoomPreassignSync> {

	public boolean removeAll();

	public boolean saveOrUpdate(RoomPreassignSync sync);
	
	public RoomPreassignSync getRoomPreassignSyncByRoomNo(String roomNo); 
	
}
