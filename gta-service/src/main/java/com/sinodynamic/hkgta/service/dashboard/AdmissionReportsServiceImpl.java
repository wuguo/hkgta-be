package com.sinodynamic.hkgta.service.dashboard;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dto.BiMemberUsageRateDto;
import com.sinodynamic.hkgta.dto.UsageRate;
import com.sinodynamic.hkgta.dto.dashboard.AdmAndOpeAccRevenueDto;
import com.sinodynamic.hkgta.dto.dashboard.ServicePlanSalesPercentageDto;
import com.sinodynamic.hkgta.dto.dashboard.StaffSalesAmountByMonthDto;
import com.sinodynamic.hkgta.dto.dashboard.StaffSalesAmountDto;
import com.sinodynamic.hkgta.dto.dashboard.StaffTurnoverDto;
import com.sinodynamic.hkgta.dto.fms.BIFacilitiesDto;
import com.sinodynamic.hkgta.dto.rpos.CreditCardTypeDistributionDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.FacilityCode;
import com.sinodynamic.hkgta.util.constant.CashValueBalanceDistributionPieChartType;
import com.sinodynamic.hkgta.util.constant.CashValueBalanceDistributionType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CourseType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.TopupAmountDistributionType;
import com.sinodynamic.hkgta.util.constant.TopupFrequencyType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 
 * @author Kaster 20160413
 *
 */
@Service
public class AdmissionReportsServiceImpl extends ServiceBase<CustomerEnrollment> implements AdmissionReportsService {
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;

	@Override
	@Transactional
	public List<StaffTurnoverDto> querySalesAmount() {
		String sql = "SELECT t.sales_follow_by staffId,SUM(itemprice) turnover "
				+ " FROM "
				+ " 	(SELECT ce.sales_follow_by,ce.subscribe_plan_no,pos.pos_item_no,IFNULL(price.item_price,0) itemprice "
				+ " 	FROM customer_enrollment ce,staff_master master,service_plan_pos pos,pos_service_item_price price "
				+ " 	WHERE ce.sales_follow_by=master.user_id "
				+ "		AND ce.subscribe_plan_no=pos.plan_no "
				+ " 	AND pos.pos_item_no=price.item_no "
				+ "		AND master.status='ACT' "
				+ " 	AND ce.STATUS in ('PYA','TOA','ANC','CMP') ORDER BY ce.sales_follow_by) t " 
				+ " GROUP BY t.sales_follow_by ORDER BY turnover DESC ";
		
		List<StaffTurnoverDto> list = customerEnrollmentDao.getDtoBySql(sql, null, StaffTurnoverDto.class);
		return list;
	}

	@Override
	@Transactional
	public Map<String,Object> querySalesAmountRanking(Integer topNum, String staffId) {
		Map<String,Object> map = new HashMap<String,Object>();
		List<StaffSalesAmountByMonthDto> byMonthList = new ArrayList<StaffSalesAmountByMonthDto>();
		String sqlByMonth = "SELECT t.staffId staffId,t.mon yearMonth,COUNT(t.staffId) numOfPatron,SUM(t.itemPrice) salesAmount FROM "
				+" (SELECT DATE_FORMAT(ce.enroll_date,'%Y-%m') mon,"
				+ "ce.subscribe_plan_no planNo,"
				+ "ce.sales_follow_by staffId,"
				+ "pos.pos_item_no posItemNo,"
				+ "IFNULL(price.item_price,0) itemPrice " 
				+" FROM customer_enrollment ce,staff_master master,service_plan_pos pos,pos_service_item_price price "
				+" WHERE 	ce.subscribe_plan_no=pos.plan_no "
				+" AND ce.enroll_date>= DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - DAY(CURDATE()) + 1 DAY),INTERVAL -12 MONTH) "
				+" AND ce.sales_follow_by=master.user_id "
				+" AND pos.pos_item_no=price.item_no "
				+" AND master.status='ACT' "
				+" AND ce.sales_follow_by='<STAFFID>' " 
				+" AND ce.status in ('PYA','TOA','ANC','CMP') "
				+" ORDER BY mon) t "
				+" GROUP BY t.mon ";
		
		if(staffId == null){
			String sql = "SELECT t1.staffId staffId,t1.salesAmount salesAmount,t2.numOfPatron numOfPatron FROM "
					+" 	(SELECT t.sales_follow_by staffId,SUM(itemprice) salesAmount	FROM "
					+" 		(SELECT ce.sales_follow_by,ce.subscribe_plan_no,pos.pos_item_no,IFNULL(price.item_price,0) itemprice " 
					+" 		FROM customer_enrollment ce,staff_master mas,service_plan_pos pos,pos_service_item_price price "
					+" 		WHERE 	ce.sales_follow_by=mas.user_id "
					+" 		AND ce.subscribe_plan_no=pos.plan_no "
					+" 		AND pos.pos_item_no=price.item_no "
					+" 		AND mas.status='ACT' "
					+" 		AND ce.STATUS in ('PYA','TOA','ANC','CMP') ORDER BY ce.sales_follow_by) t " 
					+" 	GROUP BY t.sales_follow_by ORDER BY salesAmount DESC) t1,"
					+" 	(SELECT sales_follow_by,COUNT(enroll_id) numOfPatron FROM "
					+" 	customer_enrollment WHERE STATUS in ('PYA','TOA','ANC','CMP') GROUP BY sales_follow_by  ORDER BY sales_follow_by) t2 "
					+" WHERE t1.staffId=t2.sales_follow_by ";
			if(topNum!=null && topNum.intValue()>0){
				sql = sql + " LIMIT 0," + topNum.intValue();
			}else{
				//默认查TOP10
				sql = sql + " LIMIT 0,10";
			}
			List<StaffSalesAmountDto> list = customerEnrollmentDao.getDtoBySql(sql, null, StaffSalesAmountDto.class);
			
			String totalSql = "SELECT SUM(salesAmount) totalSalesAmount,SUM(numOfPatron) totalPatrons FROM ( " + sql + " ) total ";
			Object[] obj = (Object[]) customerEnrollmentDao.getUniqueBySQL(totalSql, null);
			map.put("salesRanking", list);
			map.put("totalSalesAmount", obj[0]);
			map.put("totalPatrons", obj[1]);
			
			//查询所有在职的有销售业绩的员工，返回给前端填充Sales Person下拉列表。
			//modified by Kaster 20160426 需求有变，查询job nature为“Sales(membership/Service)”的员工。
//			String sqlStaffs = "SELECT DISTINCT(user_id) staffId FROM staff_master mas WHERE mas.status='ACT' ORDER BY mas.user_id";
			String sqlStaffs = "SELECT DISTINCT(user_id) staffId FROM staff_master mas WHERE mas.status='ACT' AND mas.staff_type='SALESMBR' ORDER BY mas.user_id";
			List<StaffSalesAmountDto> staffList = customerEnrollmentDao.getDtoBySql(sqlStaffs, null, StaffSalesAmountDto.class);
			List<String> staffIdList = new ArrayList<String>();
			for(StaffSalesAmountDto dto : staffList){
				staffIdList.add(dto.getStaffId());
			}
			map.put("staffIds", staffIdList);
			
			if(list!=null && list.size()>0){
				sqlByMonth = sqlByMonth.replace("<STAFFID>", list.get(0).getStaffId());
				byMonthList = customerEnrollmentDao.getDtoBySql(sqlByMonth,null,StaffSalesAmountByMonthDto.class);
			}
		}else{
			//查询指定员工的当月之前13个月（包含当月）的销售数量及销售额
			sqlByMonth = sqlByMonth.replace("<STAFFID>", staffId);
			byMonthList = customerEnrollmentDao.getDtoBySql(sqlByMonth,null,StaffSalesAmountByMonthDto.class);
		}
		
		List<String> genMonthList = gen13MonthsBeforeCurrent();
		//以下5个list传回前端方便前端展示数据
		List<String> yearMonthList = new ArrayList<String>();
		List<Integer> upOrDownNumList = new ArrayList<Integer>();
		List<Integer> numOfAdmissionList = new ArrayList<Integer>();
		List<Integer> upOrDownAdmList = new ArrayList<Integer>();
		List<BigDecimal> accAdmRevenueList = new ArrayList<BigDecimal>();
		
		
		
		for(int i=0;i<genMonthList.size();i++){
			boolean match = false;
			String yearMonth = genMonthList.get(i);
			yearMonthList.add(yearMonth);
			for(int j=0;j<byMonthList.size();j++){
				StaffSalesAmountByMonthDto dto = byMonthList.get(j);
				if(yearMonth.equals(dto.getYearMonth())){
					numOfAdmissionList.add(dto.getNumOfPatron().intValue());
					accAdmRevenueList.add(dto.getSalesAmount());
					match = true;
					break;
				}
			}
			
			//dtoList中没有找到某个月的数据
			if(!match){
				numOfAdmissionList.add(0);
				accAdmRevenueList.add(new BigDecimal(0));
			}
		}
		
		//计算对比上一个月的增减情况
		for(int i=1;i<genMonthList.size();i++){
			if(numOfAdmissionList.get(i).intValue() > numOfAdmissionList.get(i-1).intValue()){
				upOrDownNumList.add(1);
			}else if(numOfAdmissionList.get(i).intValue() < numOfAdmissionList.get(i-1).intValue()){
				upOrDownNumList.add(-1);
			}else{
				upOrDownNumList.add(0);
			}
			
			if(accAdmRevenueList.get(i).compareTo(accAdmRevenueList.get(i-1)) > 0){
				upOrDownAdmList.add(1);
			}else if(accAdmRevenueList.get(i).compareTo(accAdmRevenueList.get(i-1)) < 0){
				upOrDownAdmList.add(-1);
			}else{
				upOrDownAdmList.add(0);
			}
		}
		
		yearMonthList.remove(0);
		numOfAdmissionList.remove(0);
		accAdmRevenueList.remove(0);
		
		map.put("yearMonthList", yearMonthList);
		map.put("upOrDownNumList", upOrDownNumList);
		map.put("numOfAdmissionList", numOfAdmissionList);
		map.put("upOrDownAdmList", upOrDownAdmList);
		map.put("accAdmRevenueList", accAdmRevenueList);
		
		BigDecimal totalNumOfAdm = new BigDecimal(0);
		BigDecimal totalAdmRevenue = new BigDecimal(0);
		for(int i=0;i<yearMonthList.size();i++){
			totalNumOfAdm = totalNumOfAdm.add(new BigDecimal(numOfAdmissionList.get(i)));
			totalAdmRevenue = totalAdmRevenue.add(accAdmRevenueList.get(i));
		}
		map.put("totalNumOfAdm", totalNumOfAdm);
		map.put("totalAdmRevenue", totalAdmRevenue);
		return map;
	}

	@Override
	@Transactional
	public List<ServicePlanSalesPercentageDto> queryServicePlanSalesPercentage() {
		String sqlTotalNumAndRevenue = "SELECT SUM(t1.num) totalNumber,SUM(t1.revenue) totalRevenue "  
				+" FROM (SELECT t.num*price.item_price revenue,t.num num "
				+" 		FROM "
				+"		(SELECT COUNT(enroll_id) num,ce.subscribe_plan_no planNo,plan_name planName FROM customer_enrollment ce,service_plan plan WHERE ce.subscribe_plan_no=plan.plan_no AND ce.status IN ('PYA','TOA','ANC','CMP') AND plan.status='ACT' GROUP BY ce.subscribe_plan_no) t,"
				+" 		service_plan_pos pos, "
				+" 		pos_service_item_price price " 
				+" 		WHERE t.planNo=pos.plan_no AND price.item_no=pos.pos_item_no) t1 ";
		Object[] obj = (Object[]) customerEnrollmentDao.getUniqueBySQL(sqlTotalNumAndRevenue, null);
		BigDecimal totalNumber = (BigDecimal) obj[0];
		BigDecimal totalRevenue = (BigDecimal) obj[1];
		
		if(totalNumber!=null && totalRevenue!=null && totalNumber.intValue()!=0 && totalRevenue.compareTo(new BigDecimal(0))>0){
			String sqlPct = "SELECT t.*,price.item_price itemPrice,t.num*price.item_price revenue "
					+" FROM "
					+"		(SELECT COUNT(enroll_id) num,ce.subscribe_plan_no planNo,plan_name planName FROM customer_enrollment ce,service_plan plan WHERE ce.subscribe_plan_no=plan.plan_no AND ce.status IN ('PYA','TOA','ANC','CMP') AND plan.status='ACT' GROUP BY ce.subscribe_plan_no) t,"
					+" 		service_plan_pos pos, "
					+" 		pos_service_item_price price " 
					+" WHERE t.planNo=pos.plan_no AND price.item_no=pos.pos_item_no ";
			List<ServicePlanSalesPercentageDto> list = new ArrayList<ServicePlanSalesPercentageDto>();
			list = customerEnrollmentDao.getDtoBySql(sqlPct, null, ServicePlanSalesPercentageDto.class);
			for(ServicePlanSalesPercentageDto dto : list){
				dto.setPctOfNumber(new BigDecimal(dto.getNum()).divide(totalNumber,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP));
				dto.setPctOfRevenue(dto.getRevenue().divide(totalRevenue,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			return list;
		}
		return null;
	}

	@Override
	@Transactional
	public Map<String, Object> queryAdmAndOpeAccRevenue(String startYearMonth, String endYearMonth) {
		//用两个左连接（两个表的前后顺序不一样）再union达到全连接的效果。因为某些月份可能有admission而没有operational，而某些月份有operational但没有admission，保留两边月份的并集。
		//默认查询当月之前12个月（包含当月共12个月）的数据,为了判断每个月业绩比上一个月业绩的增减，所以查多一个月用来给第一个月作对比。一共查出13个月的数据（如果每月都有的话）。
		String sql = "SELECT tb1.yearMonth yearMonth,tb1.numOfAdmission numOfAdmission,tb1.accAdmRevenue accAdmRevenue,IFNULL(tb2.accOpeRevenue,0) accOpeRevenue "
				+" FROM "
				+" 		(SELECT DATE_FORMAT(cel.status_update_date,'%Y-%m') yearMonth,COUNT(ce.enroll_id) numOfAdmission,SUM(price.item_price) accAdmRevenue "
				+" 		FROM customer_enrollment ce,customer_enrollment_log cel,staff_master mas,service_plan_pos pos,pos_service_item_price price "
				+" 		WHERE ce.enroll_id = cel.enroll_id "
				+ "     AND ce.status in ('PYA','TOA','ANC','CMP') "
				+ "     AND cel.status_to = 'PYA' "
				+" 		AND cel.status_update_date>= DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - DAY(CURDATE()) + 1 DAY),INTERVAL -12 MONTH) "
				+" 		AND ce.sales_follow_by=mas.user_id "
				+" 		AND mas.status='ACT' "
				+" 		AND ce.subscribe_plan_no=pos.plan_no "
				+" 		AND pos.pos_item_no=price.item_no "
				+" 		GROUP BY DATE_FORMAT(cel.status_update_date,'%Y-%m') "
				+" 		ORDER BY yearMonth) tb1 "
				+" LEFT JOIN "
				+" 		(SELECT t.yearMonth yearMonth,SUM(t.paidAmount) accOpeRevenue "
				+" 		FROM "
				+" 			(SELECT trans.order_no orderNo,DATE_FORMAT(trans.transaction_timestamp,'%Y-%m') yearMonth,trans.paid_amount paidAmount,det.item_no itemNo,price.item_catagory itemCatagory "
				+" 			FROM customer_order_trans trans,customer_order_det det,pos_service_item_price price "
				+" 			WHERE 	trans.order_no=det.order_no "
				+" 				AND trans.transaction_timestamp>= DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - DAY(CURDATE()) + 1 DAY),INTERVAL -12 MONTH) "
				+" 				AND det.item_no=price.item_no "
				+" 				AND trans.status='SUC' "
				+" 				AND price.item_catagory IN ('GFBK','TFBK','GS','TS','GSS','TSS','DP','MMS_SPA','PMS_ROOM') "	
				+" 			ORDER BY DATE_FORMAT(trans.transaction_timestamp,'%Y-%m')) t "
				+" 		GROUP BY t.yearMonth "
				+" 		ORDER BY t.yearMonth) tb2 "
				+" ON tb1.yearMonth=tb2.yearMonth "
				+" UNION "
				+" SELECT tb1.yearMonth yearMonth,IFNULL(tb2.numOfAdmission,0) numOfAdmission,IFNULL(tb2.accAdmRevenue,0) accAdmRevenue,tb1.accOpeRevenue accOpeRevenue "
				+" FROM "
				+" 		(SELECT t.yearMonth yearMonth,SUM(t.paidAmount) accOpeRevenue "
				+" 		FROM "
				+" 			(SELECT trans.order_no orderNo,DATE_FORMAT(trans.transaction_timestamp,'%Y-%m') yearMonth,trans.paid_amount paidAmount,det.item_no itemNo,price.item_catagory itemCatagory "
				+" 			FROM customer_order_trans trans,customer_order_det det,pos_service_item_price price "
				+" 			WHERE 	trans.order_no=det.order_no "
				+" 				AND trans.transaction_timestamp>= DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - DAY(CURDATE()) + 1 DAY),INTERVAL -12 MONTH) "
				+" 				AND det.item_no=price.item_no "
				+" 				AND trans.status='SUC' "
				+" 				AND price.item_catagory IN ('GFBK','TFBK','GS','TS','GSS','TSS','DP','MMS_SPA','PMS_ROOM') "	
				+" 			ORDER BY DATE_FORMAT(trans.transaction_timestamp,'%Y-%m')) t "
				+" 		GROUP BY t.yearMonth "
				+" 		ORDER BY t.yearMonth) tb1 "
				+" LEFT JOIN "
				+" 		(SELECT DATE_FORMAT(cel.status_update_date,'%Y-%m') yearMonth,COUNT(ce.enroll_id) numOfAdmission,SUM(price.item_price) accAdmRevenue "
				+" 		FROM customer_enrollment ce,customer_enrollment_log cel,staff_master mas,service_plan_pos pos,pos_service_item_price price "
				+" 		WHERE ce.enroll_id = cel.enroll_id "
				+" 		AND ce.status in ('PYA','TOA','ANC','CMP') "
				+ "     AND cel.status_to = 'PYA' "
				+" 		AND cel.status_update_date>= DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - DAY(CURDATE()) + 1 DAY),INTERVAL -12 MONTH) "
				+" 		AND ce.sales_follow_by=mas.user_id "
				+" 		AND mas.status='ACT' "
				+" 		AND ce.subscribe_plan_no=pos.plan_no "
				+" 		AND pos.pos_item_no=price.item_no "
				+" 		GROUP BY DATE_FORMAT(cel.status_update_date,'%Y-%m') "
				+" 		ORDER BY yearMonth) tb2 "
				+" ON tb1.yearMonth=tb2.yearMonth "
				+" ORDER BY yearMonth ";
		List<AdmAndOpeAccRevenueDto> dtoList = customerEnrollmentDao.getDtoBySql(sql, null, AdmAndOpeAccRevenueDto.class);
		List<String> genMonthList = gen13MonthsBeforeCurrent();
		//以下七个list传回前端方便前端展示数据
		List<String> yearMonthList = new ArrayList<String>();
		
		List<Integer> upOrDownNumList = new ArrayList<Integer>();
		List<Integer> numOfAdmissionList = new ArrayList<Integer>();
		
		List<Integer> upOrDownAdmList = new ArrayList<Integer>();
		List<BigDecimal> accAdmRevenueList = new ArrayList<BigDecimal>();
		
		List<Integer> upOrDownOpeList = new ArrayList<Integer>();
		List<BigDecimal> accOpeRevenueList = new ArrayList<BigDecimal>();
		
		for(int i=0;i<genMonthList.size();i++){
			boolean match = false;
			String yearMonth = genMonthList.get(i);
			yearMonthList.add(yearMonth);
			for(int j=0;j<dtoList.size();j++){
				AdmAndOpeAccRevenueDto dto = dtoList.get(j);
				if(yearMonth.equals(dto.getYearMonth())){
					numOfAdmissionList.add(dto.getNumOfAdmission().intValue());
					accAdmRevenueList.add(dto.getAccAdmRevenue());
					accOpeRevenueList.add(dto.getAccOpeRevenue());
					match = true;
					break;
				}
			}
			
			//dtoList中没有找到某个月的数据
			if(!match){
				numOfAdmissionList.add(0);
				accAdmRevenueList.add(new BigDecimal(0));
				accOpeRevenueList.add(new BigDecimal(0));
			}
		}
		
		//计算对比上一个月的增减情况
		for(int i=1;i<genMonthList.size();i++){
			if(numOfAdmissionList.get(i).intValue() > numOfAdmissionList.get(i-1).intValue()){
				upOrDownNumList.add(1);
			}else if(numOfAdmissionList.get(i).intValue() < numOfAdmissionList.get(i-1).intValue()){
				upOrDownNumList.add(-1);
			}else{
				upOrDownNumList.add(0);
			}
			
			if(accAdmRevenueList.get(i).compareTo(accAdmRevenueList.get(i-1)) > 0){
				upOrDownAdmList.add(1);
			}else if(accAdmRevenueList.get(i).compareTo(accAdmRevenueList.get(i-1)) < 0){
				upOrDownAdmList.add(-1);
			}else{
				upOrDownAdmList.add(0);
			}
			
			if(accOpeRevenueList.get(i).compareTo(accOpeRevenueList.get(i-1)) > 0){
				upOrDownOpeList.add(1);
			}else if(accOpeRevenueList.get(i).compareTo(accOpeRevenueList.get(i-1)) < 0){
				upOrDownOpeList.add(-1);
			}else{
				upOrDownOpeList.add(0);
			}
		}
		
		yearMonthList.remove(0);
		numOfAdmissionList.remove(0);
		accAdmRevenueList.remove(0);
		accOpeRevenueList.remove(0);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("yearMonthList", yearMonthList);
		map.put("upOrDownNumList", upOrDownNumList);
		map.put("numOfAdmissionList", numOfAdmissionList);
		map.put("upOrDownAdmList", upOrDownAdmList);
		map.put("accAdmRevenueList", accAdmRevenueList);
		map.put("upOrDownOpeList", upOrDownOpeList);
		map.put("accOpeRevenueList", accOpeRevenueList);
		
		BigDecimal totalNumOfAdm = new BigDecimal(0);
		BigDecimal totalAdmRevenue = new BigDecimal(0);
		BigDecimal totalOpeRevenue = new BigDecimal(0);
		for(int i=0;i<yearMonthList.size();i++){
			totalNumOfAdm = totalNumOfAdm.add(new BigDecimal(numOfAdmissionList.get(i)));
			totalAdmRevenue = totalAdmRevenue.add(accAdmRevenueList.get(i));
			totalOpeRevenue = totalOpeRevenue.add(accOpeRevenueList.get(i));
		}
		map.put("totalNumOfAdm", totalNumOfAdm);
		map.put("totalAdmRevenue", totalAdmRevenue);
		map.put("totalOpeRevenue", totalOpeRevenue);
		
		//added by Kaster 20160519
		List<Integer> numOfAdmissionForLineList = new ArrayList<Integer>();
		List<BigDecimal> accAdmRevenueForLineList = new ArrayList<BigDecimal>();
		List<BigDecimal> accOpeRevenueForLineList = new ArrayList<BigDecimal>();
		for(int i=0;i<accAdmRevenueList.size();i++){
			numOfAdmissionForLineList.add(numOfAdmissionList.get(i));
			accAdmRevenueForLineList.add(accAdmRevenueList.get(i));
			accOpeRevenueForLineList.add(accOpeRevenueList.get(i));
		}
		for(int i=1;i<accAdmRevenueForLineList.size();i++){
			numOfAdmissionForLineList.set(i, numOfAdmissionForLineList.get(i) + numOfAdmissionForLineList.get(i-1));
			accAdmRevenueForLineList.set(i, accAdmRevenueForLineList.get(i).add(accAdmRevenueForLineList.get(i-1)));
			accOpeRevenueForLineList.set(i, accOpeRevenueForLineList.get(i).add(accOpeRevenueForLineList.get(i-1)));
		}
		
		map.put("numOfAdmissionForLineList", numOfAdmissionForLineList);
		map.put("accAdmRevenueForLineList", accAdmRevenueForLineList);
		map.put("accOpeRevenueForLineList", accOpeRevenueForLineList);
		return map;
	}
	
	/**
	 * 生成当月之前13个月（包含当月）的年月字符串，格式如2015-04(yyyy-MM)
	 */
	public List<String> gen13MonthsBeforeCurrent(){
		List<String> monthList = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		for(int i=0;i<=12;i++){
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -(12-i));
			String yearMonth = sdf.format(cal.getTime());
			monthList.add(yearMonth);
		}
		return monthList;
	}

	@Override
	@Transactional
	public Map<String, Object> queryActivePatronAndAdmissionRelated() {
		Map<String, Object> map = new HashMap<String, Object>();
		//查询Total Active Patron
		String sqlTAP = "SELECT COUNT(customer_id) totalActivePatron FROM member WHERE STATUS='ACT' AND member_type NOT IN ('MG','HG')";
		Object totalActivePatron = customerEnrollmentDao.getUniqueBySQL(sqlTAP, null);
		map.put("TotalActivePatron", totalActivePatron);
		
		//查询New Admission This Month
		String sqlNewAdmThisMonth = "SELECT COUNT(ce.enroll_id) newAdmThisMonth FROM customer_enrollment ce, customer_enrollment_log cel WHERE ce.enroll_id=cel.enroll_id AND ce.status IN ('PYA','TOA','ANC','CMP') AND cel.status_to='PYA' AND DATE_FORMAT(cel.status_update_date,'%Y-%m')=DATE_FORMAT(CURRENT_DATE,'%Y-%m') ORDER BY cel.status_update_date ";
		Object newAdmThisMonth = customerEnrollmentDao.getUniqueBySQL(sqlNewAdmThisMonth, null);
		map.put("NewAdmThisMonth", newAdmThisMonth);
		
		//查询New Admission Last Month
		String sqlNewAdmLastMonth = "SELECT COUNT(ce.enroll_id) newAdmLastMonth FROM customer_enrollment ce, customer_enrollment_log cel WHERE ce.enroll_id=cel.enroll_id AND ce.status IN ('PYA','TOA','ANC','CMP') AND cel.status_to='PYA' AND DATE_FORMAT(cel.status_update_date,'%Y-%m')=DATE_FORMAT(DATE_ADD(CURRENT_DATE,INTERVAL -1 MONTH),'%Y-%m') ORDER BY cel.status_update_date ";
		Object newAdmLastMonth = customerEnrollmentDao.getUniqueBySQL(sqlNewAdmLastMonth, null);
		map.put("NewAdmLastMonth", newAdmLastMonth);
		
		//查询Admission Revenue This Month
		String sqlAdmRevThisMonth = "SELECT SUM(t.itemPrice) admRevThisMonth FROM "
				+" (SELECT price.item_price itemPrice "
				+" FROM customer_enrollment ce,service_plan_pos pos,pos_service_item_price price "
				+" WHERE ce.status in ('PYA','TOA','ANC','CMP') "
				+" AND ce.subscribe_plan_no=pos.plan_no "
				+" AND pos.pos_item_no=price.item_no "
				+" AND DATE_FORMAT(ce.enroll_date,'%Y-%m')=DATE_FORMAT(CURRENT_DATE,'%Y-%m')) t ";
		
		Object admRevThisMonth = customerEnrollmentDao.getUniqueBySQL(sqlAdmRevThisMonth, null);
		map.put("AdimissionRevenueThisMonth", admRevThisMonth!=null ? admRevThisMonth : 0);
		
		//查询Accumulative Admission Revenue
		String sqlAAR = "SELECT SUM(t.itemPrice) accAdmRev FROM "
				+" (SELECT price.item_price itemPrice "
				+" FROM customer_enrollment ce,service_plan_pos pos,pos_service_item_price price "
				+" WHERE ce.status in ('PYA','TOA','ANC','CMP') "
				+" AND ce.subscribe_plan_no=pos.plan_no "
				+" AND pos.pos_item_no=price.item_no "
				+" ) t ";
		
		Object accumulativeAdmissionRevenue = customerEnrollmentDao.getUniqueBySQL(sqlAAR, null);
		map.put("AccumulativeAdmissionRevenue", accumulativeAdmissionRevenue!=null ? accumulativeAdmissionRevenue : 0);
		return map;
	}
	/**
	 * old facilityOccupancy
	@Override
	@Transactional
	public List<FacilityOccupancyDto> queryFacilityOccupancy() {
		String hql=this.createFacilityOccupancySQL();
		return customerEnrollmentDao.getDtoBySql(hql, null, FacilityOccupancyDto.class);
	}
	*/
	
	@Override
	@Transactional
	public List<UsageRate> queryFacilityOccupancy(String year) {
		List<UsageRate>list=new ArrayList<>();
		String []facilitypes=new String[]{"GOLF","TENNIS"};
		for (String type:facilitypes) {
			UsageRate rate = new UsageRate();
			BigDecimal[] data = new BigDecimal[12];// 12 month data 
			handerDate(year, data, type);
			rate.setData(data);
			rate.setName((type.equals("GOLF")?"Golfing Bay":"Tennis Court"));
			list.add(rate);
		}
		return list;
	}
	
	private void handerDate(String year,BigDecimal[] data,String type)
	{
		String bookSql=this.createSqlBookFacilityTotal(year);
		Map<String, BigDecimal>mapBookTotal=new HashMap<>();
		//key:faciltiyType+month  value:total  [golf01:50,golf02:20,tennis03:10]
		List<BIFacilitiesDto>bookTotalList=customerEnrollmentDao.getDtoBySql(bookSql, null, BIFacilitiesDto.class);
		for (BIFacilitiesDto dto : bookTotalList) {
			mapBookTotal.put(dto.getFacilityType()+dto.getMonth(), dto.getTotalAvailable());
		}
		String sql=this.createTotalSqlFacilityTotal(year);
		List<BIFacilitiesDto>availableTotalList=customerEnrollmentDao.getDtoBySql(sql, null, BIFacilitiesDto.class);
		//key:faciltiyType+month  value:total  [golf01:50,golf02:20,golf03:10]
		Map<String, BigDecimal>mapUseage=new HashMap<>();
				
		for (BIFacilitiesDto dto : availableTotalList) {
			String key=dto.getFacilityType()+dto.getMonth();
			BigDecimal useage=BigDecimal.ZERO;
			if(mapBookTotal.containsKey(key)){
				//The ratio = total_usage / total_available
				useage=(mapBookTotal.get(key).divide(dto.getTotalAvailable(),6,RoundingMode.HALF_UP)).multiply(new BigDecimal(100));
				useage=useage.setScale(2, RoundingMode.HALF_UP);
			}
			mapUseage.put(key, useage);
		}
		this.setData(mapUseage, data, type);
		
		
	}
	private void setData(Map<String, BigDecimal> map, BigDecimal[] data, String type) {
		for (int i = 0; i < 12; i++) {
			String key=null;
			if (i < 9) {
				key = "0" + (i + 1);
			} else {
				key = "" + (i + 1);
			}
			if (map.containsKey(type + key)) {
				data[i] =  map.get(type + key);
			} else {
				data[i] =BigDecimal.ZERO;
			}
		}
	}
	private String createFacilityOccupancySQL(String year){
		StringBuffer buffer=new StringBuffer();
		//guest room rates
		buffer.append(createGuestRateSQL());
		buffer.append(" UNION ALL ");
		//golf driving rates
		buffer.append(createVenueSqlByType(FacilityCode.GOLF.name()));
		buffer.append(" UNION ALL ");
		//tennis court rates
		buffer.append(createVenueSqlByType(FacilityCode.TENNIS.name()));
		buffer.append(" UNION ALL ");
		//golf/private coach rates
		buffer.append(createCoachByTypeSQL(CourseType.GOLFCOURSE.getDesc()));
		buffer.append(" UNION ALL ");
		//tennis/private coach rates
		buffer.append(createCoachByTypeSQL(CourseType.TENNISCOURSE.getDesc()));
		return buffer.toString();
	}
	/***
	 * all month AvailableTotal  golf/tennis
	 * @param year
	 * @return
	 */
	private String createTotalSqlFacilityTotal(String year){
		 StringBuilder bdf=new StringBuilder();
		 bdf.append("SELECT * FROM ( ");
		 for (int i=1;i<=12;i++){
			if(i<10)
			{
				bdf.append(this.createSqlFacilityAvailableTotal(year+"-0"+i+"-01"));				
			}else{
				bdf.append(this.createSqlFacilityAvailableTotal(year+"-"+i+"-01"));
			}
			if(i<12){
				bdf.append("UNION ALL ");
			}
		  }
		 bdf.append(") as t  ORDER BY t.facilityType,t.month  ");
		return bdf.toString();
	}
	/***
	 * create sql  yyyy-mm-dd
	 * @param dateTime yyyy-mm-dd
	 * @return  faciltity_type  and total_available
	 */
	private String  createSqlFacilityAvailableTotal(String dateTime) {
		/**
		String totalSql="SELECT b.facility_type as facilityType,COUNT(DISTINCT b.facility_no) *  DAY(LAST_DAY('"+dateTime+"')) * 16- COUNT(a.facility_timeslot_id) AS totalAvailable,DATE_FORMAT('"+dateTime+"', '%m') as month "
		+" FROM facility_master b "
		+" LEFT JOIN facility_timeslot a ON a.facility_no=b.facility_no" 
		
		+" AND a.status IN ('MT') AND DATE_FORMAT(a.begin_datetime, '%Y-%m')=DATE_FORMAT('"+dateTime+"','%Y-%m')"
		+" WHERE "
		+" b.facility_type IN('"+Constant.FACILITY_TYPE_GOLF+"', '"+Constant.FACILITY_TYPE_TENNIS+"')"
		+" GROUP BY b.facility_type ";
		**/
		/***
		 * update filter facility_timeslot status is MT repetition data
		 * @time 2018-10-10
		 * @author christ 
		 */
		StringBuilder sbSql=new StringBuilder();
		sbSql.append(" SELECT b.facility_type as facilityType,");
		sbSql.append("COUNT(DISTINCT b.facility_no) *  DAY(LAST_DAY('"+dateTime+"')) * 16- COUNT(a.facility_no) AS totalAvailable,");
		sbSql.append("DATE_FORMAT('"+dateTime+"', '%m') as month \n");
		sbSql.append(" FROM facility_master b \n ");
		sbSql.append(" LEFT JOIN ");
		sbSql.append(" ( ");
		sbSql.append(" SELECT DISTINCT f.facility_no,f.begin_datetime,f.end_datetime,f.status FROM facility_timeslot f ");
		sbSql.append(" ) as a \n");
		sbSql.append(" ON a.facility_no=b.facility_no  ");
		sbSql.append(" AND a.status IN ('MT') AND DATE_FORMAT(a.begin_datetime, '%Y-%m')=DATE_FORMAT('"+dateTime+"','%Y-%m') ");
		sbSql.append(" WHERE ");
		sbSql.append(" b.facility_type IN('"+Constant.FACILITY_TYPE_GOLF+"', '"+Constant.FACILITY_TYPE_TENNIS+"') ");
		sbSql.append(" GROUP BY b.facility_type ");
		return sbSql.toString();
	}
	
	/***
	 * get all book facility total
	 * @param year 
	 * @return  
	 */
	private String  createSqlBookFacilityTotal(String year) {
		String sql=" SELECT b.facility_type as facilityType , DATE_FORMAT(a.begin_datetime, '%m')as month, COUNT(*) totalAvailable "
		+"  FROM facility_timeslot a, facility_master b "
		+"  WHERE a.status IN ('OP', 'TA')"
		+"  AND a.facility_no=b.facility_no"
		+"  AND b.facility_type IN ('"+Constant.FACILITY_TYPE_GOLF+"', '"+Constant.FACILITY_TYPE_TENNIS+"')"
		+"  AND DATE_FORMAT(a.begin_datetime, '%Y')='"+year+"'"
		+" GROUP BY b.facility_type,DATE_FORMAT(begin_datetime, '%Y-%m')";
	   return sql;
	}
	/***
	 * create guest room occupany
	 * @return
	 */
	private String createGuestRateSQL(){
		StringBuffer bfSql=new StringBuffer();
		bfSql.append("SELECT 'GuestRoom' AS facilityType, 'Guest House Occupancy Rate' as description,");
        bfSql.append("FORMAT((SELECT COUNT(*) FROM room WHERE  frontdesk_status='O')/(SELECT COUNT(*) FROM room WHERE  frontdesk_status<>'OO' OR frontdesk_status IS NULL )*100,2) AS occupancy ");
        return bfSql.toString();
	}
	/***
	 * create Golf Driving /Tennis court
	 * @param type GOFL/TENNIS
	 * @return SQL
	 */
	private String createVenueSqlByType(String type){
		StringBuffer bfSql=new StringBuffer();
		if(FacilityCode.GOLF.name().equals(type))
		{
			bfSql.append(" SELECT 'Golf' AS facilityType, \n");	
			bfSql.append(" 'Golf Driving Range Occupancy Rate' as description,\n");
		}else if(FacilityCode.TENNIS.name().equals(type)){
			bfSql.append(" SELECT 'Tennis' AS facilityType,\n");
			bfSql.append(" 'Tennis Court Occupany Rate' as description,\n");
		}
		bfSql.append("  FORMAT( (SELECT COUNT(*) FROM facility_timeslot AS ft \n"); 
		bfSql.append("  LEFT JOIN  facility_master AS fm ON fm.facility_no=ft.facility_no \n");
		bfSql.append("  WHERE fm.facility_type= '"+type+"' \n");
		bfSql.append("  AND ( ft.status='OP' OR ft.status='TA')  \n");
	    bfSql.append("  AND ft.begin_datetime<=NOW() \n");
	    bfSql.append("	AND DATE_ADD(ft.end_datetime,INTERVAL 1 SECOND ) >=NOW() \n");
	    bfSql.append(" )/ \n");
		bfSql.append(" (SELECT COUNT(*) FROM facility_master AS fm  \n");
		bfSql.append(" WHERE fm.facility_type='"+type+"' AND fm.status='ACT' \n");
		bfSql.append(" AND fm.facility_no NOT IN ( \n");
		bfSql.append(" SELECT ft.facility_no FROM facility_timeslot AS ft \n");
		bfSql.append(" WHERE  ft.status='MT' \n");
		bfSql.append(" AND ft.begin_datetime<=NOW() \n");
		bfSql.append(" AND DATE_ADD(ft.end_datetime,INTERVAL 1 SECOND )>=NOW() \n");
		bfSql.append(" ))*100 ,2) \n");
		bfSql.append(" AS occupancy");
		return bfSql.toString();
	}
	/***
	 * create private coach(golf/tennis) Course
	 * @param type GSS/TSS
	 * @return sql
	 */
	private String createCoachByTypeSQL(String type) 
	{
		StringBuffer bfSql=new StringBuffer();
		//GSS
		String coachType=null;
		String partTime = null;
		String fullTime = null;
		if(CourseType.GOLFCOURSE.getDesc().equals(type))
		{
			coachType=FacilityCode.GOLF.name();
			partTime = "PTG";
			fullTime = "FTG";
			bfSql.append(" SELECT 'Golf' AS facilityType,\n");
			bfSql.append(" 'Golf Coaches Engagement Rate' as description,\n");
		}//TSS
		else if(CourseType.TENNISCOURSE.getDesc().equals(type)){
			coachType=FacilityCode.TENNIS.name();
			partTime = "PTR";
			fullTime = "FTR";
			bfSql.append(" SELECT 'Tennis' AS facilityType,\n");
			bfSql.append(" 'Tennis Coaches Engagement Rate' as description,\n");
		}
		bfSql.append("FORMAT((\n");
		bfSql.append(" (\n");
		bfSql.append("  SELECT COUNT(DISTINCT coach_user_id) FROM \n");
		bfSql.append("	(  \n");
		bfSql.append("    SELECT exp_coach_user_id AS coach_user_id FROM member_facility_type_booking AS mb \n");
		bfSql.append("    WHERE mb.begin_datetime_book <= NOW() \n");
		bfSql.append("	  AND DATE_ADD(mb.end_datetime_book,INTERVAL 1 SECOND )>=NOW() \n");
		bfSql.append("    AND mb.resv_facility_type='"+coachType+"' \n");
		bfSql.append("    AND mb.status<>'CAN'\n");
		bfSql.append("	  AND exp_coach_user_id IS NOT NULL \n");
		bfSql.append("    UNION ALL \n");
			 //-- course_session coach
		bfSql.append("    SELECT cs.coach_user_id  FROM course_session AS cs \n");
		bfSql.append("    LEFT JOIN course_master AS cm  ON cs.course_id=cm.course_id \n");
		bfSql.append("    WHERE cs.begin_datetime<=NOW() \n"); 
		bfSql.append("    AND   cs.end_datetime>=NOW() \n");
		bfSql.append("    AND cm.course_type='"+type+"' \n"); 
		bfSql.append("    AND cs.status='ACT' \n");
		bfSql.append("	)AS coach \n");   
		bfSql.append(" )/");
		bfSql.append("( \n");
		bfSql.append("  SELECT  COUNT(*) \n");
		bfSql.append("  FROM	staff_master m  \n");
		bfSql.append("  WHERE m.staff_type IN ('"+partTime+"','"+fullTime+"') \n");
		bfSql.append("  AND m.employ_date < NOW()    AND m.STATUS = 'ACT' ");
		bfSql.append("  AND (m.quit_date IS NULL OR m.quit_date>NOW() ) \n");
		bfSql.append("    AND EXISTS ( ");
		bfSql.append("          SELECT 1 FROM staff_coach_rate_pos r ");			 
		bfSql.append("          WHERE r.user_id=m.user_id  AND r.rate_type IN('HI','LO') GROUP BY r.rate_type HAVING COUNT(r.rate_type)>=1 ");
		bfSql.append("   ) ");
		bfSql.append("  AND m.user_id NOT IN \n"); 
		bfSql.append("  ( \n");
		//week setting Y or B
		bfSql.append(" SELECT    coach_user_id FROM staff_coach_roster  WHERE (off_duty='Y' OR off_duty='B') \n" );
		bfSql.append("  AND begin_time= DATE_FORMAT(NOW(),'%H')  AND week_day=");
		bfSql.append("	CASE  \n");
		bfSql.append("	WHEN DATE_FORMAT(NOW(),'%w')=0 THEN  1 \n");
		bfSql.append("	WHEN DATE_FORMAT(NOW(),'%w')<>0 THEN  \n");
		bfSql.append("	DATE_FORMAT(NOW(),'%w')+1  \n");
		bfSql.append("	END  \n");
		bfSql.append("  UNION ALL \n");
		// day setting Y OR B
		bfSql.append("	SELECT   coach_user_id FROM staff_coach_roster  WHERE (off_duty='Y' OR off_duty='B') \n");
		bfSql.append("   AND begin_time= DATE_FORMAT(NOW(),'%H') \n");
		bfSql.append("	 AND (on_date=DATE_FORMAT(NOW(),'%Y-%m-%d')) \n");
		
		bfSql.append("	 ) \n");
		bfSql.append(" )*100 \n");
		bfSql.append("),2) \n");
		bfSql.append("AS occupancy");
		return bfSql.toString();
	}
	
	/**
	 * 获取信用卡支付类型数据
	 * @return
	 */
	@Override
	@Transactional
	public List<CreditCardTypeDistributionDto> creditCardPayTypeData(){
		String sql = "SELECT t.payment_method_code AS payCode, count(*) AS countNum, count(*) / n.totalNum AS accountingNum"
				+ " FROM customer_enrollment ce, customer_enroll_po cep, customer_order_trans t, customer_order_hd h "
				+ "		, ( select  count(*) AS totalNum from customer_enrollment ce, customer_enroll_po cep, customer_order_trans t, customer_order_hd h "
				+ "			where ce.status in ('PYA','TOA','ANC','CMP')  AND ce.enroll_id = cep.enroll_id AND cep.order_no = h.order_no AND h.order_no=t.order_no and t.status='SUC' and h.order_status='CMP' "
				+ "			and t.payment_method_code in ('VISA', 'MASTER', 'AMEX','CUP'))  AS n"
				+ " WHERE ce.status in ('PYA','TOA','ANC','CMP')  AND ce.enroll_id = cep.enroll_id AND cep.order_no = h.order_no AND h.order_no=t.order_no "
				+ "		AND t.status='SUC' "
				+ " 	AND h.order_status='CMP' "
				+ "		AND t.payment_method_code in ('VISA', 'MASTER', 'AMEX','CUP') "
				+ " GROUP BY t.payment_method_code ";
		
		List<CreditCardTypeDistributionDto> list = customerEnrollmentDao.getDtoBySql(sql, null, CreditCardTypeDistributionDto.class);
		return list;
	}
	
	
	
	 /**
     * 获取topup频率
     * @param year
     * @return
     */
	@Override
	@Transactional
	public ResponseResult getTopupFrequency(String year) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		try {
			 simpleDateFormat.parse(year);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, e.getMessage());
			return responseResult;
		}
		List<UsageRate> list = new ArrayList<>();
		TopupFrequencyType[] types = TopupFrequencyType.values();
		
		
		for (TopupFrequencyType intervalType : types) {
			Map<String, Object> map = convertMapByInterval(this.getTopupFrequencyByRate(year, intervalType.getStartRateNum(), intervalType.getEndRateNum()), intervalType.getCode(), 1);
			UsageRate rate = new UsageRate();
			List<Object> data = new ArrayList<>();// 12 month data 
			setData(map, data, intervalType.getCode(), 1);
			rate.setData(data.toArray());
			rate.setName(intervalType.getCode());
			list.add(rate);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}
	
	/**
	 * 根据参数获取统计数
	 * @param year
	 * @param startRateNum
	 * @param endRateNum
	 * @return
	 */
	@Transactional
	public List<BiMemberUsageRateDto> getTopupFrequencyByRate(String year, int startRateNum, int endRateNum) {
		try {
			StringBuilder sder = new StringBuilder();
			//统计没有充值过，没有意义的数据导致代码又臭又长！
			if (startRateNum == 0) {
				sder.append(" select c.periodDate, c.rate  AS rate from ( select periodDate, ( SELECT bmc.total_member AS rate FROM bi_member_cnt AS bmc WHERE date_format (bmc.count_date, '%Y-%m') = b.periodDate AND bmc.mbr_status = 'ACT' AND bmc.period = 'M')  -  b.rate AS rate from ( ");
			}
			sder.append(" select periodDate, count(*) AS rate from ");
			sder.append("  ( ");
			sder.append(" 	 select h.customer_id, date_format(t.transaction_timestamp, '%Y-%m') periodDate, count(*) freq ");
			sder.append("   	from customer_order_trans t, customer_order_det d, customer_order_hd h ");
			sder.append("    where h.order_no=d.order_no and h.order_no=t.order_no and d.item_no='CVT0000001' and DATE_FORMAT(t.transaction_timestamp, '%Y') = ?  and t.status='SUC'  and h.order_status='CMP' group by h.customer_id, date_format(t.transaction_timestamp, '%Y-%m') ");
			sder.append("   ) a ");
			sder.append("   where freq between ? and ? group by periodDate  ");
			//统计没有充值过，没有意义的数据导致代码又臭又长！
			if (startRateNum == 0) {
				sder.append(" ) b ) c WHERE c.rate IS NOT NULL ");
			}
			List<Serializable> param = new ArrayList<>();
			param.add(year);
			param.add(startRateNum);
			param.add(endRateNum);
			return customerEnrollmentDao.getDtoBySql(sder.toString(), param, BiMemberUsageRateDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 获取每月CashValue平衡柱形图分布
	 * @param year
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult getMonthlyCashValueBalanceDistribution(String year) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		try {
			 simpleDateFormat.parse(year);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, e.getMessage());
			return responseResult;
		}
		List<UsageRate> list = new ArrayList<>();
		CashValueBalanceDistributionType[] types = CashValueBalanceDistributionType.values();
		for (CashValueBalanceDistributionType intervalType : types) {
			Map<String, Object> map = convertMapByInterval(this.getMonthlyCashValueBalanceDistributionByRate(year, intervalType.getStartRateNum(), intervalType.getEndRateNum(), intervalType.getConditionsType()), intervalType.getCode(), 1);
			UsageRate rate = new UsageRate();
			List<Object> data = new ArrayList<>();// 12 month data 
			setData(map, data, intervalType.getCode(), 1);
			rate.setData(data.toArray());
			rate.setName(intervalType.getCode());
			list.add(rate);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}
	
	/**
	 * 根据参数获取统计数
	 * @param year
	 * @param startRateNum
	 * @param endRateNum
	 * @return
	 */
	@Transactional
	public List<BiMemberUsageRateDto> getMonthlyCashValueBalanceDistributionByRate(String year, int startRateNum, int endRateNum, int conditionsType) {
		try {
			StringBuilder sder = new StringBuilder();
			sder.append(" select periodDate, count(*) AS rate from ");
			sder.append("  ( ");
			sder.append(" 	 select h.customer_id, date_format(h.cutoff_date, '%Y-%m') periodDate, count(*) freq ");
			sder.append("   	from member_cashvalue_bal_history h ");
			sder.append("    where  DATE_FORMAT(h.cutoff_date, '%Y') = ?   ");
			List<Serializable> param = new ArrayList<>();
			param.add(year);
			switch (conditionsType) {
			case 1:
				sder.append("  AND  recal_balance < ? ");
				param.add(endRateNum);
				break;
			case 3:
				sder.append("  AND  recal_balance > ? ");
				param.add(startRateNum);
				break;
			default:
				if (endRateNum == 5000) {
					sder.append("  AND  recal_balance between ? and  ? ");
				} else {
					sder.append("  AND  recal_balance >= ? and recal_balance < ? ");
				}
				param.add(startRateNum);
				param.add(endRateNum);
				break;
			}
			sder.append("   GROUP BY h.customer_id, periodDate ");
			sder.append("   ) a   group by periodDate");
			
			return customerEnrollmentDao.getDtoBySql(sder.toString(), param, BiMemberUsageRateDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 获取每月CashValue平衡饼状图分布
	 * @param year
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult getMonthlyCashValueBalanceMultidimensionalDistribution(String year) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
		try {
			 simpleDateFormat.parse(year);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, e.getMessage());
			return responseResult;
		}
		List<UsageRate> list = new ArrayList<>();
		CashValueBalanceDistributionPieChartType[] types = CashValueBalanceDistributionPieChartType.values();
		for (CashValueBalanceDistributionPieChartType intervalType : types) {
			Map<String, Object> map = convertMapByInterval(this.getMonthlyCashValueBalanceDistributionByMultidimensional(year, intervalType.getStartRateNum(), intervalType.getEndRateNum(), intervalType.getConditionsType()), intervalType.getCode(), 0);
			UsageRate rate = new UsageRate();
			List<Object> data = new ArrayList<>();// 12 month data 
			setData(map, data, intervalType.getCode(), 0);
			rate.setData(data.toArray());
			rate.setName(intervalType.getCode());
			list.add(rate);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}
	
	/**
	 * 根据参数获取统计数
	 * @param year
	 * @param startRateNum
	 * @param endRateNum
	 * @return
	 */
	@Transactional
	public List<BiMemberUsageRateDto> getMonthlyCashValueBalanceDistributionByMultidimensional(String year, int startRateNum, int endRateNum, int conditionsType) {
		try {
			StringBuilder sder = new StringBuilder();
			sder.append(" select a.periodDate, count(*) AS rate , count(*)/t.totalNum AS quot, t.totalNum AS totalNum from ");
			sder.append("  ( ");
			sder.append(" 	 select h.customer_id, date_format(h.cutoff_date, '%Y-%m') periodDate, count(*) freq ");
			sder.append("   	from member_cashvalue_bal_history h ");
			sder.append("    where  DATE_FORMAT(h.cutoff_date, '%Y-%m') = ?   ");
			List<Serializable> param = new ArrayList<>();
			param.add(year);
			switch (conditionsType) {
			case 1:
				sder.append("  AND  recal_balance < ? ");
				param.add(endRateNum);
				break;
			case 3:
				sder.append("  AND  recal_balance > ? ");
				param.add(startRateNum);
				break;
			default:
				if (endRateNum == 30000) {
					sder.append("  AND  recal_balance between ? and  ? ");
				} else {
					sder.append("  AND  recal_balance >= ? and recal_balance < ? ");
				}
				param.add(startRateNum);
				param.add(endRateNum);
				break;
			}
			sder.append("   GROUP BY h.customer_id, periodDate ");
			sder.append("   ) a,  (select date_format(cutoff_date, '%Y-%m') AS yearmonth , count(*) AS totalNum from member_cashvalue_bal_history m GROUP BY yearmonth ) AS t ");
			sder.append(" WHERE a.periodDate = t.yearmonth group by a.periodDate ");
			
			return customerEnrollmentDao.getDtoBySql(sder.toString(), param, BiMemberUsageRateDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	/**
	 * 获取每月topup数量分布
	 * @param year
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult getMonthlyTopupAmountDistribution(String year) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		try {
			 simpleDateFormat.parse(year);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, e.getMessage());
			return responseResult;
		}
		List<UsageRate> list = new ArrayList<>();
		TopupAmountDistributionType[] types = TopupAmountDistributionType.values();
		for (TopupAmountDistributionType intervalType : types) {
			Map<String, Object> map = convertMapByInterval(this.getMonthlyTopupAmountDistributionByRate(year, intervalType.getStartRateNum(), intervalType.getEndRateNum(), intervalType.getConditionsType()), intervalType.getCode(), 1);
			UsageRate rate = new UsageRate();
			List<Object> data = new ArrayList<>();// 12 month data 
			setData(map, data, intervalType.getCode(), 1);
			rate.setData(data.toArray());
			rate.setName(intervalType.getCode());
			list.add(rate);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}
	
	/**
	 * 根据参数获取统计数
	 * @param year
	 * @param startRateNum
	 * @param endRateNum
	 * @return
	 */
	@Transactional
	public List<BiMemberUsageRateDto> getMonthlyTopupAmountDistributionByRate(String year, int startRateNum, int endRateNum, int conditionsType) {
		try {
			StringBuilder sder = new StringBuilder();
			sder.append(" select date_format(t.transaction_timestamp, '%Y-%m') AS periodDate, count(*) AS rate ");
			sder.append("  from customer_order_trans t, customer_order_det d, customer_order_hd h ");
			sder.append(" 	 where  h.order_no=d.order_no  and h.order_no=t.order_no and d.item_no='CVT0000001' and t.status='SUC' and h.order_status='CMP' AND DATE_FORMAT(t.transaction_timestamp, '%Y') = ?");
			List<Serializable> param = new ArrayList<>();
			param.add(year);
			switch (conditionsType) {
			case 1:
				sder.append("  AND  t.paid_amount < ? ");
				param.add(endRateNum);
				break;
			case 3:
				sder.append("  AND  t.paid_amount > ? ");
				param.add(startRateNum);
				break;
			default:
				sder.append("  AND  t.paid_amount >= ? and  t.paid_amount < ? ");
				param.add(startRateNum);
				param.add(endRateNum);
				break;
			}
			sder.append("  group by periodDate");
			
			return customerEnrollmentDao.getDtoBySql(sder.toString(), param, BiMemberUsageRateDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Map<String, Object> convertMapByInterval(List<BiMemberUsageRateDto> dtos, String interval, int type) {
		Map<String, Object> map = new HashMap<>();
		if (dtos != null) {
			for (BiMemberUsageRateDto dto : dtos) {
				String key = interval + dto.getPeriodDate().split("-")[1];
				switch (type) {
				case 0:
					Object[] arry = new Object[]{dto.getRate(), dto.getQuot(), dto.getTotalNum()};
					map.put(key, arry);
					break;

				default:
					map.put(key, dto.getRate());
					break;
				}
				
			}
		}
		return map;
	}
	
	/**
	 *  根据map进行数据封装
	 * @param map
	 * @param data
	 * @param code
	 * @param type  0:饼状图data返回数组  其它：条形图data返回单个值
	 */
	private void setData(Map<String, Object> map, List<Object> data, String code, int type) {
		for (int i = 0; i < 12; i++) {
			String key=null;
			if (i < 9) {
				key = "0" + (i + 1);
			} else {
				key = "" + (i + 1);
			}
			if (map.containsKey(code + key)) {
				data.add(map.get(code + key));
				if (type == 0) {
					continue;
				}
			} else  if(type != 0) {
//				switch (type) {
//				case 0:
//					data.add(new Object[]{0,0.0,0});
//					break;
//				default:
					data.add(0);
//					break;
//				}
				
			}
		}
		if (data.size() == 0 && type == 0) {
			data.add(new Object[]{0,0.0,0});
		}
		
	}
	public static void main(String[] args) {
		BigDecimal useage=BigDecimal.valueOf(0.054984);
		 useage=useage.setScale(2, RoundingMode.HALF_UP);
		 System.out.println(useage);
		
		
	}

}
