package com.sinodynamic.hkgta.service.crm.membercash;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;

import com.sinodynamic.hkgta.dao.crm.CashvalueTopupHistoryDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberPaymentAccDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.ical.GtaPublicHolidayDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.SftpByJsch;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;

@Service
public class VirtualAccountTaskServiceImpl extends ServiceBase implements VirtualAccountTaskService {

	private final Logger logger = LoggerFactory.getLogger(VirtualAccountTaskServiceImpl.class);

	private final Logger virtualAccTopupLog = LoggerFactory.getLogger(LoggerType.VIRTUALACC.getName());

	@Autowired
	private MemberPaymentAccDao memberPaymentAccDao;

	@Autowired
	private CashvalueTopupHistoryDao cashvalueTopupHistoryDao;

	@Autowired
	private MemberCashValueDao memberCashValueDao;

	@Autowired
	private CustomerOrderDetDao customerOrderDetDao;

	@Autowired
	private CustomerOrderTransDao customerOrderTransDao;

	@Autowired
	private MessageTemplateDao messageTemplateDao;

	@Autowired
	private CustomerProfileDao customerProfileDao;

	@Autowired
	private MailThreadService mailThreadService;

	@Autowired
	private CustomerEmailContentDao customerEmailContentDao;

	@Autowired
	private CustomerEmailAttachDao customerEmailAttachDao;

	@Autowired
	private GtaPublicHolidayDao gtaPublicHolidayDao;
	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Autowired
	private MemberDao memberDao;

	@Resource(name = "ddxProperties")
	protected Properties ddxProps;

	private Logger ddxLogger = LoggerFactory.getLogger(LoggerType.DDX.getName());

	private MessageTemplate template;

	private String ftpHost;
	private String port;
	private String ftpUserName;
	private String ftpPassword;
	private String timeout;

	private String uploadPath;
	private String receivePath;
	private String receiveBackPath;
	private String downLoadPath; // store all the remote files in
									// local
	private String downLoadBackPath; // store the success processed
										// files in local
	private String connectTimeout;

	@PostConstruct
	public void init() {
		ftpHost = ddxProps.getProperty(Constant.SFTP_HOST, null);
		port = ddxProps.getProperty(Constant.SFTP_PORT, null);
		ftpUserName = ddxProps.getProperty(Constant.SFTP_USERNAME, null);
		ftpPassword = ddxProps.getProperty(Constant.SFTP_PASSWORD, null);
		timeout = ddxProps.getProperty(Constant.SFTP_TIMEOUT, null);

		uploadPath = ddxProps.getProperty(Constant.SFTP_SNED_PATH, null);// remote
																			// upload
																			// dir
		receivePath = ddxProps.getProperty(Constant.SFTP_RECEIVE_PATH, null);// remote
																				// downloand
																				// dir
		receiveBackPath = ddxProps.getProperty(Constant.SFTP_RECEIVE_BACK_PATH, null);// remote
																						// back
																						// dir,
																						// after
																						// processed
																						// move
																						// to
																						// this
																						// dir
		downLoadPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_PATH, null);// store
																				// all
																				// the
																				// remote
																				// files
																				// in
																				// local
		downLoadBackPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_BACK_PATH, null);// store
																						// the
																						// success
																						// processed
																						// files
																						// in
																						// local
		connectTimeout = ddxProps.getProperty(Constant.SFTP_CONNECT_TIMEOUT, null);

	}

	@Override
	@Transactional
	public void topupCashValueByVirtualAccInBatch(List<CashvalueTopupHistory> list) {

		for (CashvalueTopupHistory topUp : list) {
			String accNo = topUp.getAccNo();
			BigDecimal amount = topUp.getAmount();

			MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByAccountNo(accNo);
			if (mpa == null) {
				logger.error("No MemberPaymentAcc with accountNo = " + accNo + " can be found!");
				continue;
			}
			Assert.notNull(mpa);

			Long cusomerId = mpa.getCustomerId();
			MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
			if (memberCashValue == null) {
				logger.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
			}
			Assert.notNull(memberCashValue);

			// { SAM 20161007 moved this code to bottom because need transaction no  
//			topUp.setCustomerId(cusomerId);
//			cashvalueTopupHistoryDao.addCashvalueTopupHistory(topUp);  
//			end move}

			BigDecimal oldBalance = memberCashValue.getAvailableBalance();
			BigDecimal exchgFactor = memberCashValue.getExchgFactor();
			BigDecimal addBalance = amount;
			if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
				addBalance = amount.multiply(exchgFactor);
			}
			BigDecimal newBalance = oldBalance.add(addBalance);
			memberCashValue.setAvailableBalance(newBalance.setScale(2, BigDecimal.ROUND_HALF_UP));
			memberCashValueDao.updateMemberCashValue(memberCashValue);

			// CUSTOMER ORDER Header
			CustomerOrderHd customerOrderHd = new CustomerOrderHd();
			Date currentDate = new Date();
			customerOrderHd.setCustomerId(cusomerId);
			customerOrderHd.setOrderTotalAmount(addBalance);
			customerOrderHd.setOrderRemark("");
			customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderHd.setUpdateDate(currentDate);
			customerOrderHd.setOrderStatus(Constant.Status.CMP.toString());

			// CUSTOMER ORDER Detail
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(customerOrderHd);
			customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
			customerOrderDet.setItemRemark("");
			customerOrderDet.setOrderQty(1L);
			customerOrderDet.setItemTotalAmout(addBalance);
			customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderDet.setUpdateDate(currentDate);

			// CUSTOMER ORDER TRANS
			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			/** job专用，默认值，以防报空指针 */
			initCustomerOrderTransForCronjob(customerOrderTrans);
			customerOrderTrans.setCustomerOrderHd(customerOrderHd);
			customerOrderTrans.setTransactionTimestamp(currentDate);
			customerOrderTrans.setPaidAmount(addBalance);
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VAC.name());
			customerOrderTrans.setStatus(Constant.Status.SUC.name());

			customerOrderDetDao.saveOrderDet(customerOrderDet);
			customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
			
			// SAM 20161007 added: update transaction_no to corresponding Topup history record
			Long transactionNo=customerOrderTrans.getTransactionNo();			
			topUp.setTransactionNo(transactionNo);
			
			// SAM 20161013 added: missing cashvalue_amt
			if (exchgFactor!=null && topUp.getAmount()!=null)
			   topUp.setCashvalueAmt(topUp.getAmount().multiply(exchgFactor) );
			
			topUp.setCustomerId(cusomerId);			
			cashvalueTopupHistoryDao.addCashvalueTopupHistory(topUp);					
			
			virtualAccTopupLog.info("Success topup cash value. Transaction no is " + transactionNo);			

			sendTopUpEmail(topUp, memberCashValue);
		}

	}

	@Transactional
	public void sendTopUpEmail(CashvalueTopupHistory topUp, MemberCashvalue memberCashvalue) {
		if (template == null) {
			template = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_TOPUP);
		}

		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, topUp.getCustomerId());
		if (customerProfile != null) {

			String to = customerProfile.getContactEmail();

			String subject = template.getMessageSubject();
			String username = customerProfile.getSalutation() + " " + customerProfile.getGivenName() + " "
					+ customerProfile.getSurname();
			String content = template.getContentHtml();
			content = content.replaceAll(MessageTemplateParams.UserName.getParam(), username);
			content = content.replaceAll(MessageTemplateParams.TopupMethod.getParam(), topUp.getTopupMethod());

			content = content.replaceAll(MessageTemplateParams.CurrentBalance.getParam(),
					"HK\\$" + memberCashvalue.getAvailableBalance());

			CustomerEmailContent customerEmailContent = new CustomerEmailContent();

			customerEmailContent.setContent(content);
			customerEmailContent.setRecipientCustomerId(topUp.getCustomerId().toString());
			customerEmailContent.setRecipientEmail(to);
			customerEmailContent.setSendDate(new Date());
			// customerEmailContent.setSenderUserId();
			customerEmailContent.setSubject(subject);
			customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_ACCOUNT);

			customerEmailContentDao.save(customerEmailContent);

			mailThreadService.sendWithResponse(customerEmailContent, null, null, null);
		}
	}

	/**
	 * look for reject list by comparing the customer ID and amount. If match, that means the pending transaction should be reject by RCQ.\n
	 * The RCQ doesn't care which transaction of the cheque as long as the amount is the same. 
	 * @param rejList
	 * @param pndTran
	 * @return 
	 */
	private boolean findByCustomerIdAndAmount(List<CashvalueTopupHistory> rejList, CashvalueTopupHistory pndTran) {
		if (null == rejList || rejList.isEmpty()) {
			return false;
		}
		boolean flag = false;
		for (CashvalueTopupHistory rejTran : rejList) {
			if (rejTran.getCustomerId().equals(pndTran.getCustomerId())
					&& rejTran.getAmount().equals(pndTran.getAmount())) {
				return true;
			}
		}
		return flag;
	}

	@Override
	public void processRejChTransactions(List<CashvalueTopupHistory> rejList) {

		// SAM added comment:loop topup file reject list
		for (CashvalueTopupHistory rejTran : rejList) {
			String accNo = rejTran.getAccNo();

			// SAM added comment: if the virtual account not exist in DB, log the error and continue next account
			MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByAccountNo(accNo);
			if (mpa == null) {
				logger.error("No MemberPaymentAcc with accountNo = " + accNo + " can be found!");
				virtualAccTopupLog.error("No MemberPaymentAcc with accountNo = " + accNo + " can be found! RCQ customer id="+rejTran.getCustomerId().toString() + " Amount=" +  rejTran.getAmount().toString()); //SAM added
				continue;
			}
			Assert.notNull(mpa);
			Long cusomerId = mpa.getCustomerId();
			rejTran.setCustomerId(cusomerId);


			MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
			if (memberCashValue == null) {
				logger.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
				virtualAccTopupLog.error("No memberCashValue with cusomerId = " + cusomerId + " can be found! RCQ customer id="+rejTran.getCustomerId().toString() + " Amount=" +  rejTran.getAmount().toString()); //SAM added
			}
			
			Assert.notNull(memberCashValue);
			BigDecimal exchgFactor = memberCashValue.getExchgFactor();
			BigDecimal addBalance = rejTran.getAmount();
			if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
				addBalance = rejTran.getAmount().multiply(exchgFactor);
			}

			//SAM added comment: lookup related pending customer order transaction record
			List<CustomerOrderTrans> orderTrans = customerOrderTransDao.getByCols(CustomerOrderTrans.class,
					new String[] { "paidAmount", "status" ,"paymentMethodCode"}, new Serializable[] { addBalance, "PND","VAC" },null);			
			//SAM added comment: if pending transaction record found, set the transaction status to FAIL and customer order status to REJ 
			if (null != orderTrans&&orderTrans.size()>0) {
				CustomerOrderTrans orderTran=orderTrans.get(0);
				
				//SAM 20161007 Changed: there is no REJ status for customer_order_trans, use FAIL instead
				//orderTran.setStatus(Constant.Status.REJ.name());
				orderTran.setStatus(Constant.Status.FAIL.name());
				
				CustomerOrderHd orderHd = orderTran.getCustomerOrderHd();
				orderHd.setOrderStatus(Constant.Status.REJ.toString());
				orderHd.setUpdateDate(new Date());
				//orderHd.setUpdateBy("[System job]");
				orderHd.setUpdateBy("[sysAdmin001]");
				
				customerOrderTransDao.update(orderTran);
				
				// SAM 20161007 added: update related transaction no
				// update the related PND transaction history record
				CashvalueTopupHistory topUpRec = cashvalueTopupHistoryDao.getUniqueByCols(CashvalueTopupHistory.class,
						new String[] { "customerId", "amount", "status" },
						new Serializable[] { cusomerId, rejTran.getAmount(), "PND" });
				// SAM added comment: if there are pending topup history record matched to be rejected, update the related topup history status to REJ  
				if (null != topUpRec) {
					// topUpRec is the pending history to be rejected, rejTran is the new RCQ history record 
					topUpRec.setStatus(Constant.Status.REJ.name());
					cashvalueTopupHistoryDao.update(topUpRec);

					rejTran.setTransactionNo(orderTran.getTransactionNo());
					rejTran.setCashvalueTopupHistory(topUpRec);					
					cashvalueTopupHistoryDao.addCashvalueTopupHistory(rejTran);
					
					virtualAccTopupLog.info("Reject Cheque topup cash value. History id=" + rejTran.getSysId() + ". Pending Transaction no is " + orderTran.getTransactionNo());					
				}				
	
			}
			else { //SAM 20161011 add: if RCQ found but not match corresponding Pending Transaction with the same amount,				
				virtualAccTopupLog.error("No pending cheque topup transaction found to match the rejected Cheque (RCQ) topup cash value. RCQ customer id="+rejTran.getCustomerId().toString() + " Amount=" +  rejTran.getAmount().toString() );
				// to be implemented: send alter email or report
			}
		}
	}

	/***
	 * SAM added comment:loop topup history pending list
	 * 
	 * @param rejList
	 */
	private void handerHistoryPndTopUp(List<CashvalueTopupHistory> rejList) {
		virtualAccTopupLog.info("hander two days history pnd topUp ......start  ");
		String hql = "from CashvalueTopupHistory where topupDate <= ? and status =?";
		List<Serializable> param = new ArrayList();
		Date twoDaysAgoWorkingDate = getTwoDaysAgoWorkingDayFromDB(new Date());
		// SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		param.add(twoDaysAgoWorkingDate);
		param.add("PND");
		List<CashvalueTopupHistory> pendingList = cashvalueTopupHistoryDao.getByHql(hql, param);
		
		virtualAccTopupLog.info("get history topup  topupDate <="+twoDaysAgoWorkingDate+"  the data size :"+(null!=pendingList?pendingList.size():0));
		
		for (CashvalueTopupHistory pndTran : pendingList) {
			boolean flag = findByCustomerIdAndAmount(rejList, pndTran);
			if (!flag) {
				// no RCQ
				pndTran.setStatus(Constant.Status.SUC.toString());
				// SAM 20161013 add: should set sccues date as well
				pndTran.setSuccessDate(new Date());

				MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByCustomerId(pndTran.getCustomerId());
				if (mpa == null) {
					virtualAccTopupLog.error("No MemberPaymentAcc with customerId = " + pndTran.getCustomerId() + " can be found!");
					continue;
				}
				Assert.notNull(mpa);
				Long cusomerId = mpa.getCustomerId();
				MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
				if (memberCashValue == null) {
					virtualAccTopupLog.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
				}
				Assert.notNull(memberCashValue);
				BigDecimal oldBalance = memberCashValue.getAvailableBalance();
				BigDecimal exchgFactor = memberCashValue.getExchgFactor();
				BigDecimal addBalance = pndTran.getAmount();
				if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
					addBalance = pndTran.getAmount().multiply(exchgFactor);
				}
				BigDecimal newBalance = oldBalance.add(addBalance);
				memberCashValue.setAvailableBalance(newBalance.setScale(2, BigDecimal.ROUND_HALF_UP));
				memberCashValueDao.updateMemberCashValue(memberCashValue);

				List<CustomerOrderTrans> orderTrans = customerOrderTransDao.getByCols(CustomerOrderTrans.class,
						new String[] { "paidAmount", "status", "paymentMethodCode" },
						new Serializable[] { addBalance, "PND", "VAC" }, null);

				virtualAccTopupLog.info("get customerOrderTrans data by paidAmount:"+addBalance+",Status:PND ,paymentMethodCode:VAC");
				virtualAccTopupLog.info(" the customerOrderTrans size is :"+(null!=orderTrans?orderTrans.size():0));
				
				if (null != orderTrans && orderTrans.size() > 0) {
					CustomerOrderTrans orderTran = orderTrans.get(0);
					orderTran.setStatus(Constant.Status.SUC.name());
					orderTran.setUpdateDate(new Timestamp(new Date().getTime()));
					// orderTran.setUpdateBy("[System job]");
					orderTran.setUpdateBy("[sysAdmin001]");

					CustomerOrderHd orderHd = orderTran.getCustomerOrderHd();
					orderHd.setOrderStatus(Constant.Status.CMP.toString());
					orderHd.setUpdateDate(new Date());
					// orderHd.setUpdateBy("[System job]");
					orderHd.setUpdateBy("[sysAdmin001]");
					customerOrderTransDao.update(orderTran);
					
					virtualAccTopupLog.info("update CustomerOrderTrans status is SUC  ,the transactionNo :"+orderTran.getTransactionNo());
					virtualAccTopupLog.info("update orderHd status is CMP  ,the orderNo :"+orderHd.getOrderNo());
					
					Member member=memberDao.getMemberByCustomerId(pndTran.getCustomerId());
					if(null!=member){
						virtualAccTopupLog.info("send topUp email success..........AcademyNo:"+member.getAcademyNo());
					}
					sendTopUpEmail(pndTran, memberCashValue);
				}

			}
		}

	}

	@Override
	public void processPndChTransactions(List<CashvalueTopupHistory> pndList) {
		for (CashvalueTopupHistory pndTran : pndList) {
			String accNo = pndTran.getAccNo();
			BigDecimal amount = pndTran.getAmount();

			MemberPaymentAcc mpa = memberPaymentAccDao.getMemberPaymentAccByAccountNo(accNo);
			if (mpa == null) {
				logger.error("No MemberPaymentAcc with accountNo = " + accNo + " can be found!");
				continue;
			}
			Assert.notNull(mpa);

			Long cusomerId = mpa.getCustomerId();
			MemberCashvalue memberCashValue = memberCashValueDao.getByCustomerId(cusomerId);
			if (memberCashValue == null) {
				logger.error("No memberCashValue with cusomerId = " + cusomerId + " can be found!");
			}
			Assert.notNull(memberCashValue);

			// {SAM 20161007 moved to bottom because need transaction_no 
//			pndTran.setCustomerId(cusomerId);
//			cashvalueTopupHistoryDao.addCashvalueTopupHistory(pndTran);
			//}

			BigDecimal oldBalance = memberCashValue.getAvailableBalance();
			BigDecimal exchgFactor = memberCashValue.getExchgFactor();
			BigDecimal addBalance = amount;
			if (exchgFactor.compareTo(BigDecimal.ZERO) == 1) {
				addBalance = amount.multiply(exchgFactor);
			}

			// CUSTOMER ORDER Header
			CustomerOrderHd customerOrderHd = new CustomerOrderHd();
			Date currentDate = new Date();
			customerOrderHd.setCustomerId(cusomerId);
			customerOrderHd.setOrderTotalAmount(addBalance);
			customerOrderHd.setOrderStatus(Constant.Status.OPN.toString());
			customerOrderHd.setOrderRemark("");
			customerOrderHd.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderHd.setUpdateDate(currentDate);

			// CUSTOMER ORDER Detail
			CustomerOrderDet customerOrderDet = new CustomerOrderDet();
			customerOrderDet.setCustomerOrderHd(customerOrderHd);
			customerOrderDet.setItemNo(Constant.TOPUP_ITEM_NO);
			customerOrderDet.setItemRemark("");
			customerOrderDet.setOrderQty(1L);
			customerOrderDet.setItemTotalAmout(addBalance);
			customerOrderDet.setCreateDate(new Timestamp(currentDate.getTime()));
			customerOrderDet.setUpdateDate(currentDate);

			// CUSTOMER ORDER TRANS
			CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
			/** job专用，默认值，以防报空指针 */
			initCustomerOrderTransForCronjob(customerOrderTrans);
			customerOrderTrans.setCustomerOrderHd(customerOrderHd);
			customerOrderTrans.setTransactionTimestamp(currentDate);
			customerOrderTrans.setPaidAmount(addBalance);
			customerOrderTrans.setPaymentMethodCode(PaymentMethod.VAC.name());
			customerOrderTrans.setStatus(Constant.Status.PND.name());

			customerOrderDetDao.saveOrderDet(customerOrderDet);
			customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
			
			//SAM 20161007 added: update transaction_no to corresponding Topup history record					
			Long transactionNo=customerOrderTrans.getTransactionNo();
			pndTran.setTransactionNo(transactionNo);
			
			// SAM 20161013 added: missing cashvalue_amt
			if (exchgFactor!=null && pndTran.getAmount()!=null)
				pndTran.setCashvalueAmt(pndTran.getAmount().multiply(exchgFactor) );			
			
			pndTran.setCustomerId(cusomerId);
			cashvalueTopupHistoryDao.addCashvalueTopupHistory(pndTran);			

			virtualAccTopupLog.info("pending cheque topup cash value. Transaction no is " + transactionNo);			
		}
	}

	/**
	 * replaced by getTwoDaysAgoWorkingDayFromDB
	 * 
	 * @param date
	 * @return
	 */
	@Deprecated
	private Date getTwoDaysAgoWorkingDayFromPropFile(Date date) {
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		HashSet holidaySet = new HashSet();
		String holidayListStr = appProps.getProperty("hk.holiday", null);
		StringTokenizer st = new StringTokenizer(holidayListStr, ",");
		while (st.hasMoreTokens()) {
			holidaySet.add(st.nextToken());
		}

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int workingDaysCount = 0;
		while (true) {
			if (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7
					|| holidaySet.contains(sdf_yyyymmdd.format(c.getTime()))) {
				c.add(Calendar.DATE, -1);
				continue;
			} else {
				workingDaysCount++;
			}

			if (workingDaysCount == 3) {
				return c.getTime();
			} else {
				c.add(Calendar.DATE, -1);
			}
		}

	}

	//private Date getTwoDaysAgoWorkingDayFromDB(Date date) {
	public Date getTwoDaysAgoWorkingDayFromDB(Date date) {		
		SimpleDateFormat sdf_yyyymmdd = new SimpleDateFormat("yyyyMMdd");
		List<Date> holidays = gtaPublicHolidayDao.getNearestHolidays(date); // SAM added comment: get current and next year holidays list 
 		                     // = new ArrayList();  for unit test
		List<String> holidayStrs = new ArrayList<String>();
		for (Date d : holidays) {   // SAM added comment: convert holidays from date type to string in yyyymmdd format
			holidayStrs.add(sdf_yyyymmdd.format(d));
		}

/*		
		Calendar c = Calendar.getInstance();
		c.setTime(date);  // today 

		int workingDaysCount = 0;
		while (true) {
			// SAM added comment: if c date is Sunday or Saturday or Holiday
			if (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7
					|| holidayStrs.contains(sdf_yyyymmdd.format(c.getTime()))) {
				c.add(Calendar.DATE, -1);  // SAM added comment: subtract 1 day from current date  
				continue;
			} else {
				workingDaysCount++;												
			}

			if (workingDaysCount == 3) {  // why ??? 
				return c.getTime();
			} else {
				c.add(Calendar.DATE, -1);
			}

		}						
*/
		// Sam revised the above code on 20161012
		/**
		 * start from today, lookup backward to get the topup by cheque date in 2 working days ago 
		 */
		Calendar topupDate = Calendar.getInstance();
		topupDate.setTime(date);  // start from today (passing from parameter)
		virtualAccTopupLog.info("backward lookup cheque topup date starting from " + (new SimpleDateFormat("yyyy-MM-dd")).format(topupDate.getTime()) );
		for (int workingDaysCount = 0; workingDaysCount <= 2;  ) {
			// if the date is not Sunday, Saturday and public holiday, count it as working day 
			if (topupDate.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY  && topupDate.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY  && !holidayStrs.contains(sdf_yyyymmdd.format(topupDate.getTime())) )  				
				workingDaysCount++;
			else
				virtualAccTopupLog.info((new SimpleDateFormat("yyyy-MM-dd")).format(topupDate.getTime()) + " is not a working day" );	
			
			topupDate.add(Calendar.DATE, -1);  // SAM added comment: subtract 1 day from current date			
		}
		
		virtualAccTopupLog.info("Approve/Reject pending cheque on or before " + (new SimpleDateFormat("yyyy-MM-dd")).format(topupDate.getTime()) );		
		
		return topupDate.getTime();		
	}

	/**
	 * This method is used to download Transaction extract from remote sftp
	 * server, and parse the file to update database.
	 */
	@Transactional
	public void downloadVirtualAccountTransactionExtract() {
		// file name regular
		String remoteFileNameReg = ddxProps.getProperty("vaTransactionReportFileNameReg");
		String requestUrl = "receivePath" + receivePath + ";receiveBackPath:" + receiveBackPath + ";downLoadBackPath:"
				+ downLoadBackPath + ";downLoadPath:" + downLoadPath;
		if (CommUtil.nvl(receivePath).length() == 0 || CommUtil.nvl(receiveBackPath).length() == 0
				|| CommUtil.nvl(downLoadPath).length() == 0 || CommUtil.nvl(downLoadBackPath).length() == 0) {

			logger.error("Schduler pause for error path configuration!");
			return;
		}
		virtualAccTopupLog.info(Log4jFormatUtil.logInfoMessage(requestUrl, null, null, null));
		File downloadDir = new File(downLoadPath);
		File downloadBackDir = new File(downLoadBackPath);
		if (!downloadDir.exists()) {
			downloadDir.mkdir();
		}
		if (!downloadBackDir.exists()) {
			downloadBackDir.mkdir();
		}
		try {
			requestUrl = ftpHost + ",ftpUserName:" + ftpUserName + ",port:" + port + ",downLoadPath:" + downLoadPath;
			virtualAccTopupLog.info(Log4jFormatUtil.logInfoMessage(requestUrl, null, null, null));

			SftpByJsch sftp = new SftpByJsch(ftpHost, ftpUserName, ftpPassword, Integer.valueOf(port).intValue(), null,
					null, 1000000, Integer.valueOf(connectTimeout).intValue());
			List<File> files = sftp.dowloadByFileNameRegex(receivePath, remoteFileNameReg, downLoadPath);
			List<CashvalueTopupHistory> succTopupList = new ArrayList<CashvalueTopupHistory>();
			List<CashvalueTopupHistory> rejChList = new ArrayList<CashvalueTopupHistory>();
			List<CashvalueTopupHistory> pndChList = new ArrayList<CashvalueTopupHistory>();
			File currentFile = null;
			try {
				if (null == files || files.size() == 0) {
					virtualAccTopupLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, null,
							"sftp.dowloadByFileNameRegex  files is null"));
					logger.error("virtual account topup failed! No VA file found");
				} else {
					for (File file : files) {
						logger.info("The File:" + file.getName());
						virtualAccTopupLog.info("The File:" + file.getName());
						currentFile = file;
						List<String> fileLines = new ArrayList<String>();
						LineNumberReader reader = new LineNumberReader(
								new InputStreamReader(new FileInputStream(file)));
						String currentLine = reader.readLine();
						logger.info("currentLine:" + currentLine);
						boolean firstLine = true;
						while (currentLine != null) {
							if (!firstLine) {
								fileLines.add(currentLine);
							}
							currentLine = reader.readLine();
							logger.info("currentLine:" + currentLine);
							virtualAccTopupLog
									.info(Log4jFormatUtil.logInfoMessage(requestUrl, null, null, currentLine));
							firstLine = false;
						}
						reader.close();
						String transactionRegex = "\\s*,\\s*";

						if (fileLines != null && fileLines.size() > 0) {
							for (String line : fileLines) {
								String[] strs = line.split(transactionRegex);
								String transDate = strs[4];
//								String valueDate = strs[2];
								//change accNo to VA number 
								String accNo = strs[7];
								String amount = strs[8];
								// Transaction Short Description
								String desc = strs[5];
								String transactionType = StringUtils.substring(desc.trim(), 0, 3);

								CashvalueTopupHistory topup = new CashvalueTopupHistory();
								if (CommUtil.nvl(transDate).length() != 0) {
									topup.setTopupDate(DateConvertUtil.parseString2Date(transDate, "dd/MM/yyyy"));
								}
								if(line.contains("\""))
								{
									topup.setAmount(handerAmount(line));
								}else{
									topup.setAmount(new BigDecimal(amount));	
								}
								
								topup.setBankFileReturn(file.getName());
								topup.setAccNo(accNo);
								topup.setCreateDate(new Date());
								if ("CHQ".equals(transactionType)) {
									topup.setTopupMethod("CHQ");
									topup.setStatus("PND");
									pndChList.add(topup);
								} else if ("RCQ".equals(transactionType)) {
									topup.setTopupMethod("RCQ");
									topup.setStatus("REJ");
									rejChList.add(topup);
								} else {
									topup.setTopupMethod("CSH");
									topup.setStatus("SUC");
									topup.setSuccessDate(DateConvertUtil.parseString2Date(transDate, "dd/MM/yyyy"));
									succTopupList.add(topup);
								}

							}
							// update database in batch
							topupCashValueByVirtualAccInBatch(succTopupList);
							processPndChTransactions(pndChList);
							processRejChTransactions(rejChList);
							virtualAccTopupLog
									.info(Log4jFormatUtil.logInfoMessage(null, null, null, succTopupList.toString()));
							virtualAccTopupLog
									.info(Log4jFormatUtil.logInfoMessage(null, null, null, pndChList.toString()));
							virtualAccTopupLog
									.info(Log4jFormatUtil.logInfoMessage(null, null, null, rejChList.toString()));
						}
						// backup sftp file to bak directory
//						sftp.move(file.getName(), receivePath, receiveBackPath);
						sftp.move(file, receivePath, receiveBackPath, ftpHost, ftpUserName, ftpPassword, Integer.valueOf(port).intValue(), null, null);
						File outFile = new File(downLoadBackPath + File.separator + file.getName());
						FileCopyUtils.copy(file, outFile);
					}
					 logger.info ("virtual account topup success" );
				}
				//hander history pending list
				handerHistoryPndTopUp(rejChList);
				
			} catch (Exception e) {
				logger.error(  "virtual account topup failed");
				virtualAccTopupLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
				logger.error(e.getMessage(), e);
				// ddxLogger.error("Happened Exception when process file:" +
				// currentFile.getName());
				alarmEmailService.sendAlarmEmailTask("VirtualAccTopupTask", e);
				throw new RuntimeException(e);
			}
		} catch (Exception e) {
			logger.error(  "virtual account topup failed");
			logger.error(e.getMessage(), e);
			virtualAccTopupLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));
			// ddxLogger.error("Happened Exception when download files.");
			alarmEmailService.sendAlarmEmailTask("VirtualAccTopupTask", e);
			throw new RuntimeException(e);
		}
	}
	/***
	 * hander amount format is "xx,xxx.00"
	 * @param line
	 * @return
	 * @throws Exception
	 */
	private BigDecimal handerAmount(String line)throws Exception
	{
		String am=line.substring(line.indexOf("\""),line.length()).replace("\"", "");
		DecimalFormat f = new DecimalFormat("0,000.00");
		BigDecimal big = new BigDecimal(f.parseObject(am).toString());
		big=big.setScale(2, BigDecimal.ROUND_HALF_UP);
		return big;
	}
}
