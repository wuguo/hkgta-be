package com.sinodynamic.hkgta.service.sys;

import com.sinodynamic.hkgta.dto.sys.CarparkInterfaceIoDto;
import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceIo;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface CarparkInterfaceIoService extends IServiceBase<CarparkInterfaceIo> {
	public ResponseResult createCarparkInterface(CarparkInterfaceIoDto carparkInterfaceIoDto);

	public ResponseResult checkCarparkInterface(String vehiclePlateNo);

	public ResponseResult updateCarparkInterface(String vehiclePlateNo);

	public boolean saveCarparkInterfaceIo(CarparkInterfaceIo carparkInterfaceIo) throws Exception;

}
