package com.sinodynamic.hkgta.service.rpos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.rpos.CustomerOrderPermitCardDao;
import com.sinodynamic.hkgta.dto.crm.DayPassCardDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.DayPassStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;
@Service
@Scope("prototype")
public class CustomerOrderPermitCardServiceImpl extends ServiceBase<CustomerOrderPermitCard> implements  CustomerOrderPermitCardService {

	@Autowired
	private CustomerOrderPermitCardDao customerOrderPermitCardDao;
	/**   
	* @author: Zero_Wang
	* @since: Sep 7, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional
	public void updateCustomerOrderPermitCardStatus() {
		List<CustomerOrderPermitCard> list = this.customerOrderPermitCardDao.getUnACTList(Constant.Status.ACT.toString());
		if(list !=null && list.size()>0){
			for(CustomerOrderPermitCard c : list){
				c.setStatus(Constant.Status.ACT.toString());
			}
		}
	}
	@Override
	@Transactional
	public ResponseResult getCustomerOrderPermitCardByQrCode(String qrCode,String cardNo) {
		StringBuffer hqlstr = new StringBuffer();
		hqlstr.append(
				"SELECT sp.plan_name AS planName,coc.effective_to AS effectiveTo,"
				+ " CONCAT(coc.qr_code,IF(CONCAT(sp.pass_nature_code,LPAD(coc.linked_card_no,8,'0'))IS NOT NULL,'-',''),IFNULL(CONCAT(sp.pass_nature_code,LPAD(coc.linked_card_no,8,'0')),'')) AS daypassId,"
				+ " coc.linked_card_no AS linkedCardNo,coc.effective_from AS effectiveFormat ,"
						+" pcm.status AS cardStatus,"
						+ " CASE "
						+ " WHEN coc.cardholder_customer_id IS NULL THEN  IFNULL(CONCAT(cc.given_name,' ',cc.surname),' ') "
						+ " WHEN coc.cardholder_customer_id IS NOT NULL THEN  IFNULL(CONCAT(ccp.given_name,' ',ccp.surname),' ') "
						+ " END AS user ,"
						+ " CASE  "
						+ " WHEN coh.staff_user_id IS NULL THEN  CONCAT(cp.given_name,' ',cp.surname) "
						+ " WHEN coh.customer_id IS NULL THEN  um.nickname END  AS  purchaser  "
						+ " FROM  customer_order_permit_card AS coc  "
						+ " LEFT JOIN candidate_customer AS cc  ON coc.candidate_customer_id=cc.candidate_id "
						+ " LEFT JOIN  permit_card_master AS pcm ON pcm.card_no=coc.linked_card_no "
						+ " LEFT JOIN customer_profile AS ccp  ON coc.cardholder_customer_id=ccp.customer_id "
						+ " LEFT JOIN  service_plan AS sp   ON coc.service_plan_no=sp.plan_no  "
						+ " LEFT JOIN customer_order_hd coh ON coc.customer_order_no=coh.order_no "
						+ " LEFT JOIN customer_profile AS cp ON cp.customer_id=coh.customer_id "
						+ " LEFT JOIN user_master AS um ON coh.staff_user_id=um.user_id  WHERE 1=1 ");
		List<String> param = new ArrayList<String>();
		if(StringUtils.isNotEmpty(qrCode)){
			hqlstr.append(" and coc.qr_code =? ");
			param.add(qrCode);	
		}else{
			hqlstr.append(" and coc.linked_card_no =? ");
			param.add(cardNo.substring(2, cardNo.length() - 1));
		}
		hqlstr.append(" order by coc.qr_code desc ");
		// set param
		
		List<DayPassCardDto> daypassDtoDtos = customerOrderPermitCardDao.getDtoBySql(hqlstr.toString(), param,
				DayPassCardDto.class);
		DayPassCardDto dayPassCardDto = null;
		if (null != daypassDtoDtos && daypassDtoDtos.size() > 0) {
			dayPassCardDto = daypassDtoDtos.get(0);
		}
		if (null == dayPassCardDto) {
			// This is an invalid day pass
			responseResult.initResult(GTAError.DayPassError.INVALID_DAY_PASS);
			return responseResult;
		} else {
			setDayPassStatus(dayPassCardDto);
			// The day pass has expired
			if (StringUtils.isNotEmpty(dayPassCardDto.getStatus())) {
				if (dayPassCardDto.getStatus().equals(DayPassStatus.EXP.getDesc())) {
					responseResult.initResult(GTAError.DayPassError.DAY_PASS_EXPIRED);
					return responseResult;
				}
				else if (dayPassCardDto.getStatus().equals(DayPassStatus.INVD.getDesc())){
					responseResult.initResult(GTAError.DayPassError.INVALID_DAY_PASS);
					return responseResult;
				}
			}
			responseResult.initResult(GTAError.Success.SUCCESS, "Success");
			responseResult.setData(dayPassCardDto);
			return responseResult;
		}

	}

	/****
	 * if buy dayPass and no bind card check effectiveFrom the current before
	 * effectiveFrom that status is Pending for Redeem The current date is
	 * within the range of valid date that set status is Ready for Redeem(Ready)
	 * else if bind card check effectiveFrom The current date is within the
	 * range of valid date that set status is Pending for Redeem The current
	 * date is after the end of the valid date that set status is Ready for
	 * Redeem(Ready)
	 * 
	 * @param daypassDto
	 */
	private void setDayPassStatus(DayPassCardDto daypassDto) {
		// no bind card
		Date current = new Date();
		String effectiveTo=DateConvertUtil.parseDate2String(daypassDto.getEffectiveTo(), "yyyy-MM-dd")+" 23:59:59";
		daypassDto.setEffectiveTo(DateConvertUtil.parseString2Date(effectiveTo,"yyyy-MM-dd hh:mm:ss"));
		
		if (current.after(daypassDto.getEffectiveTo())) {
			daypassDto.setStatus(DayPassStatus.EXP.getDesc());
			return;
		}
		if (null == daypassDto.getLinkedCardNo()) {
			if (current.before(daypassDto.getEffectiveFormat())) {
				daypassDto.setStatus(DayPassStatus.PND.getDesc());
			}
			if (current.after(daypassDto.getEffectiveFormat()) && current.before(daypassDto.getEffectiveTo())) {
				daypassDto.setStatus(DayPassStatus.Ready.getDesc());
			}
		} else {
			if (current.after(daypassDto.getEffectiveFormat()) && current.before(daypassDto.getEffectiveTo())) {
				if (null != daypassDto.getCardStatus()) {
					if (daypassDto.getCardStatus().equals("ISS")||daypassDto.getCardStatus().equals("OH")) {
						daypassDto.setStatus(DayPassStatus.ACT.getDesc());
					}else{
						daypassDto.setStatus(DayPassStatus.INVD.getDesc());
					}
				}

			}
		}
		daypassDto.setEffectiveFrom(DateConvertUtil.getYMDDate(daypassDto.getEffectiveTo()));
	}

	
}
