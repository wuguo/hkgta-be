package com.sinodynamic.hkgta.service.pms;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.adm.GlobalParameterDao;
import com.sinodynamic.hkgta.dao.adm.StaffMasterInfoDtoDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.ibeacon.IbeaconDao;
import com.sinodynamic.hkgta.dao.pms.HousekeepTaskFileDao;
import com.sinodynamic.hkgta.dao.pms.RoomCrewDao;
import com.sinodynamic.hkgta.dao.pms.RoomDao;
import com.sinodynamic.hkgta.dao.pms.RoomHousekeepStatusLogDao;
import com.sinodynamic.hkgta.dao.pms.RoomHousekeepTaskAssigneeDao;
import com.sinodynamic.hkgta.dao.pms.RoomHousekeepTaskDao;
import com.sinodynamic.hkgta.dto.crm.BeaconRoomDto;
import com.sinodynamic.hkgta.dto.pms.AssignAdHocTaskDto;
import com.sinodynamic.hkgta.dto.pms.AssignMaintenanceTaskDto;
import com.sinodynamic.hkgta.dto.pms.GuestRequestDto;
import com.sinodynamic.hkgta.dto.pms.HousekeepTaskFileDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskQueryDto;
import com.sinodynamic.hkgta.dto.pms.RoomMemberDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomTaskDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomCrew;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepStatusLog;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTaskAssignee;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTaskAssigneePK;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RoomCrewRoleType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskStatus;
import com.sinodynamic.hkgta.util.constant.RoomStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RoomHousekeepTaskServiceImpl extends
		ServiceBase<RoomHousekeepTask> implements RoomHousekeepTaskService {
	@Autowired
	private RoomHousekeepTaskDao roomHousekeepTaskDao;
	
	@Autowired
	private RoomDao roomDao;
	
	@Autowired
	private GlobalParameterDao globalParameterDao;
	
	@Autowired
	private RoomHousekeepStatusLogDao roomHousekeepStatusLogDao;
	
	@Autowired
	private HousekeepTaskFileDao housekeepTaskFileDao;
	
	@Autowired
	private RoomCrewDao roomCrewDao;
	
	@Autowired
	private RoomHousekeepTaskAssigneeDao roomHousekeepTaskAssigneeDao;
	
	@Autowired
	private StaffMasterInfoDtoDao staffMasterDao;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	@Autowired
	private IbeaconDao ibeaconDao;
	
	@Autowired
	private MessageTemplateDao messageTemplateDao;
	
	@Autowired
	private MemberDao memberDao;
	
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = GTACommonException.class)
	public void assignRoomHousekeepTask(RoomHousekeepTaskDto taskDto) throws Exception {
		taskDto.setJobType(taskDto.getJobType());
		setDatetimeForTask(taskDto);
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, "HK-MAXRESP-RA");
		for(int i=0;i<taskDto.getRoomIds().length; i++){
			Room room = roomDao.get(Room.class, taskDto.getRoomIds()[i]);
			if(room != null){
				RoomHousekeepTask roomHousekeepTask = new RoomHousekeepTask();
				roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.OPN.toString());
				roomHousekeepTask.setRequesterUserId(taskDto.getUpdateBy());
				roomHousekeepTask.setScheduleDatetime(taskDto.getBeginDate());
				roomHousekeepTask.setJobType(taskDto.getJobType());
				if(taskDto.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString()))
					roomHousekeepTask.setTargetCompleteDatetime(taskDto.getEndDate());
				roomHousekeepTask.setExpResponseTime(null == globalParameter?null:Long.parseLong(globalParameter.getParamValue()));
				roomHousekeepTask.setRoom(room);
				roomHousekeepTask.setTaskDescription(taskDto.getTaskDescription());
				roomHousekeepTask.setCreateBy(taskDto.getUpdateBy());
				roomHousekeepTask.setCreateDate(new Timestamp(new Date().getTime()));
				roomHousekeepTask.setStatusDate(new Date());
				roomHousekeepTask.setStatusChgBy(taskDto.getUpdateBy());
				roomHousekeepTaskDao.save(roomHousekeepTask);
				if(taskDto.getJobType().equals(RoomHousekeepTaskJobType.MTN.toString())){
					autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_ASSIGNED,RoomCrewRoleType.NGR.name(),room,roomHousekeepTask);
				}else
					autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_ASSIGNED,RoomCrewRoleType.RA.name(),room,roomHousekeepTask);
			}
		}
	}
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private void setDatetimeForTask(RoomHousekeepTaskDto taskDto) throws ParseException {
		Date now = new Date();
		if (taskDto.getBeginDate() != null) {
			if (isSameDay(taskDto.getBeginDate(), now)) {
				taskDto.setBeginDate(now);
			} else if (taskDto.getBeginDate().compareTo(now) > 0) { // not same day and after 'now'
				taskDto.setBeginDate(datetimeFormat.parse(dateFormat.format(taskDto.getBeginDate()) + " 00:00:00"));
			}
		} else {
			taskDto.setBeginDate(now);
		}
		if (RoomHousekeepTaskJobType.SCH.equalsWith(taskDto.getJobType()) && taskDto.getEndDate() != null) {
			taskDto.setEndDate(datetimeFormat.parse(dateFormat.format(taskDto.getEndDate()) + " 23:59:59"));
		}
	}

	private boolean isSameDay(Date date0, Date date1) {
		return dateFormat.format(date0).equals(dateFormat.format(date1));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Long cancelRoomHousekeepTask(Long taskId, String userId)  throws Exception{
		RoomHousekeepTask task = roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskId);
		if(null != task && task.getStartTimestamp() == null){
			if(!RoomHousekeepTaskStatus.CAN.toString().equals(task.getStatus())){
				RoomHousekeepStatusLog roomHousekeepStatusLog = buildRoomStatusLog(task.getTaskId(), task.getStatus(), 
						RoomHousekeepTaskStatus.CAN.name(), userId);
				roomHousekeepStatusLogDao.save(roomHousekeepStatusLog);
			}
			
			List<HousekeepTaskFile> taskFiles = task.getHousekeepTaskFiles();
			for(HousekeepTaskFile file : taskFiles){
				file.setStatus("DEL");
				file.setUpdateBy(userId);
				file.setUpdateDate(new Date());
				housekeepTaskFileDao.update(file);
			}
			
			task.setStatus(RoomHousekeepTaskStatus.CAN.toString());
			task.setStatusDate(new Date());
			task.setStatusChgBy(userId);
			task.setUpdateBy(userId);
			task.setUpdateDate(new Date());
			roomHousekeepTaskDao.update(task);
			autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_CANCELED,RoomCrewRoleType.RA.name(),task.getRoom(),task);
			return task.getTaskId();
		}
		else throw new GTACommonException(GTAError.HouseKeepError.TASK_NOT_FOUND);
	}
	@Override
	@Transactional
	public void autoRemindRoomAssistant(String functionId,String roleType,Room room,RoomHousekeepTask task)
	{
		try{
			MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(functionId);
			if(null != template && !StringUtils.isEmpty(template.getContent())){
				String subject = template.getMessageSubject();
				String message = template.getContent().replace("{taskType}",  null == task?"":RoomHousekeepTaskJobType.getDesc(task.getJobType()))
						.replace("{roomNo}", room.getRoomNo())
						.replace("{taskId}", null == task?"":task.getTaskId()+"");
				if(RoomCrewRoleType.NGR.name().equals(roleType)){
					List<StaffMaster> staffMasterInfos = staffMasterDao.getStaffMasterByStaffType(roleType);
					if(null != staffMasterInfos && staffMasterInfos.size() > 0){
						for(StaffMaster staffMaster : staffMasterInfos){
							devicePushService.pushMessage(new String[]{staffMaster.getUserId()}, subject, message, "housekeeper");
						}
						logger.info("pushMessage success!("+roleType+")" + message);
					}
				}else{
					List<RoomCrew> roomCrews = roomCrewDao.getRoomCrewListByCrewRoleId(room.getRoomId(), roleType);
					if(null != roomCrews && roomCrews.size() > 0){
						for(RoomCrew roomCrew : roomCrews){
							devicePushService.pushMessage(new String[]{roomCrew.getStaffProfile().getUserId()}, subject, message, "housekeeper");
						}
						logger.info("pushMessage success!("+roleType+")" + message);
					}
				}
			}
		}catch(Exception e){
			logger.error(RoomHousekeepTaskServiceImpl.class.getName() + " pushMessage Failed!", e);
		}
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Long reassignRoomHousekeepTask(RoomHousekeepTaskDto taskDto)  throws Exception{
		RoomHousekeepTask oldTask = roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskDto.getTaskId());
		if(null != oldTask){			
			GlobalParameter assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
			Integer raResponseTime = assistant != null ? Integer.parseInt(assistant.getParamValue()) : 0;
			taskDto.setJobType(oldTask.getJobType());
			setDatetimeForTask(taskDto);
			if(!RoomHousekeepTaskStatus.IRJ.toString().equals(oldTask.getStatus())){
				RoomHousekeepStatusLog roomHousekeepStatusLog = buildRoomStatusLog(oldTask.getTaskId(), oldTask.getStatus(), RoomHousekeepTaskStatus.IRJ.name(), taskDto.getUpdateBy());
				roomHousekeepStatusLogDao.save(roomHousekeepStatusLog);
			}

			RoomHousekeepTask newTask = buildRoomTask(taskDto, oldTask);
			newTask.setExpResponseTime(new Long(raResponseTime));
			roomHousekeepTaskDao.save(newTask);
			
			saveHousekeepTaskFiles(oldTask, newTask);

			oldTask.setStatus(RoomHousekeepTaskStatus.IRJ.name());
			oldTask.setStatusDate(new Date());
			oldTask.setStatusChgBy(taskDto.getUpdateBy());
			oldTask.setUpdateBy(taskDto.getUpdateBy());
			oldTask.setInspectorUserId(taskDto.getUpdateBy());
			oldTask.setUpdateDate(new Date());
			roomHousekeepTaskDao.update(oldTask);
			if(RoomHousekeepTaskJobType.MTN.name().equals(oldTask.getJobType()))
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_REASSIGNED,RoomCrewRoleType.NGR.name(),newTask.getRoom(),newTask);
			else
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_REASSIGNED,RoomCrewRoleType.RA.name(),newTask.getRoom(),newTask);
			
			//added by Kaster 20160315 将客房的状态改为“D”
			Room room = oldTask.getRoom();
			room.setStatus("D");
			roomDao.update(room);
			return newTask.getTaskId();
		}
		else throw new GTACommonException(GTAError.HouseKeepError.TASK_NOT_FOUND);
	}

	private void saveHousekeepTaskFiles(RoomHousekeepTask oldTask, RoomHousekeepTask newTask)
	{
		List<HousekeepTaskFile> housekeepTaskFiles = oldTask.getHousekeepTaskFiles();
		if(null != housekeepTaskFiles && housekeepTaskFiles.size() > 0){
			for(HousekeepTaskFile file : housekeepTaskFiles){
				HousekeepTaskFile newFile = new HousekeepTaskFile();
				newFile.setServerFile(file.getServerFile());
				newFile.setStatus("ACT");
				newFile.setTaskId(newTask.getTaskId());
				newFile.setCreateBy(newTask.getCreateBy());
				newFile.setCreateDate(newTask.getCreateDate());
				housekeepTaskFileDao.save(newFile);
			}
		}
	}

	private RoomHousekeepTask buildRoomTask(RoomHousekeepTaskDto taskDto, RoomHousekeepTask oldTask) {
		RoomHousekeepTask newTask = new RoomHousekeepTask();
		newTask.setFromTaskId(oldTask.getTaskId());
		newTask.setStatus(RoomHousekeepTaskStatus.OPN.name());
		newTask.setRequesterUserId(taskDto.getUpdateBy());
		if (RoomHousekeepTaskJobType.SCH.equalsWith(oldTask.getJobType())) {
			newTask.setScheduleDatetime(taskDto.getBeginDate() == null ? oldTask.getScheduleDatetime() : taskDto.getBeginDate());
			newTask.setTargetCompleteDatetime(taskDto.getEndDate() == null ? oldTask.getTargetCompleteDatetime() : taskDto.getEndDate());
		} else {
			newTask.setScheduleDatetime(taskDto.getBeginDate() == null ? new Date() : taskDto.getBeginDate());
		}
		newTask.setJobType(oldTask.getJobType());
		newTask.setRoom(oldTask.getRoom());
		newTask.setTaskDescription(taskDto.getTaskDescription());
		// 长潘说 reassignTask时将之前的remark更跟过来
		newTask.setRemark(oldTask.getRemark() == null ? "" : oldTask.getRemark());
		newTask.setCreateBy(taskDto.getUpdateBy());
		newTask.setCreateDate(new Timestamp(new Date().getTime()));
		newTask.setStatusDate(new Date());
		newTask.setStatusChgBy(taskDto.getUpdateBy());
		return newTask;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<RoomHousekeepTaskQueryDto> getRemindRoomHousekeepTaskList(Integer assistantResTime,Integer inspectorResTime,Integer supervisorResTime) {
		return roomHousekeepTaskDao.getRemindRoomHousekeepTaskList(assistantResTime,inspectorResTime,supervisorResTime);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Long updateRoomHousekeepTask(RoomHousekeepTaskDto taskDto)  throws Exception{
		RoomHousekeepTask roomHousekeepTask = roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskDto.getTaskId());
		if(null != roomHousekeepTask && roomHousekeepTask.getStartTimestamp() == null && !RoomHousekeepTaskStatus.CAN.name().equals(roomHousekeepTask.getStatus()) && !RoomHousekeepTaskStatus.CMP.name().equals(roomHousekeepTask.getStatus())){
			setDatetimeForTask(taskDto);
			if(roomHousekeepTask.getJobType().equals(RoomHousekeepTaskJobType.SCH.name())){
				roomHousekeepTask.setScheduleDatetime(taskDto.getBeginDate());
				roomHousekeepTask.setTargetCompleteDatetime(taskDto.getEndDate());
			}
			roomHousekeepTask.setTaskDescription(taskDto.getTaskDescription());
			roomHousekeepTask.setUpdateBy(taskDto.getUpdateBy());
			roomHousekeepTask.setUpdateDate(new Date());
			roomHousekeepTaskDao.update(roomHousekeepTask);
			if(taskDto.getJobType().equals(RoomHousekeepTaskJobType.MTN.toString())){
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_UPDATED,RoomCrewRoleType.NGR.name(),roomHousekeepTask.getRoom(),roomHousekeepTask);
			}else
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_UPDATED,RoomCrewRoleType.RA.name(),roomHousekeepTask.getRoom(),roomHousekeepTask);
			return roomHousekeepTask.getTaskId();
		}else
			throw new GTACommonException(GTAError.HouseKeepError.TASK_NOT_FOUND);
	}

	public RoomHousekeepTaskDto createRoomHousekeepTaskDto(RoomHousekeepTask task)
	{
		RoomHousekeepTaskDto taskDto = new RoomHousekeepTaskDto();
		taskDto.setTaskId(task.getTaskId());
		taskDto.setJobType(task.getJobType());
		taskDto.setStatus(task.getStatus());
		taskDto.setBeginDate(task.getScheduleDatetime());
		taskDto.setEndDate(task.getTargetCompleteDatetime());
		taskDto.setStartTimestamp(task.getStartTimestamp());
		taskDto.setFinishTimestamp(task.getFinishTimestamp());
		taskDto.setTaskDescription(task.getTaskDescription());
		taskDto.setRemark(task.getRemark());
		taskDto.setDndTimestamp(task.getStatusDate());
		GlobalParameter  assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
		Integer assistantResTime = assistant!=null?Integer.parseInt(assistant.getParamValue()):0;
		if(task.getJobType().equals(RoomHousekeepTaskJobType.ADH.toString()) && task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && null == task.getStartTimestamp()){
			Calendar cal = Calendar.getInstance();
			cal.setTime(task.getScheduleDatetime());
			cal.add(Calendar.MINUTE, assistantResTime);
			taskDto.setExpireTime(cal.getTime());
			if(taskDto.getExpireTime().before(new Date()))
				taskDto.setNoResponse(true);
		}else if(task.getJobType().equals(RoomHousekeepTaskJobType.SCH.toString()) && task.getStatus().equals(RoomHousekeepTaskStatus.OPN.toString()) && null == task.getStartTimestamp() && null != task.getTargetCompleteDatetime()){
			if(task.getTargetCompleteDatetime().before(new Date()))
				taskDto.setNoResponse(true);
		}else
			taskDto.setNoResponse(false);
		List<HousekeepTaskFile> taskFiles = task.getHousekeepTaskFiles();
		List<HousekeepTaskFileDto> taskFileList = null;
		if(null != taskFiles && taskFiles.size() > 0){
			taskFileList = new ArrayList<HousekeepTaskFileDto>();
			for(HousekeepTaskFile taskFile : taskFiles){
				if(!StringUtils.isEmpty(taskFile.getServerFile())){
					HousekeepTaskFileDto taskFileDto = new HousekeepTaskFileDto();
					taskFileDto.setFileId(taskFile.getFileId());
					if ("AAC".equalsIgnoreCase(getFileType(taskFile.getServerFile()))) {
						taskFileDto.setServerFile("/room/task/get_aacfile?fileId=" + taskFile.getFileId());
					}
					else if ("AMR".equalsIgnoreCase(getFileType(taskFile.getServerFile()))) {
						taskFileDto.setServerFile("/room/task/get_amrfile?fileId=" + taskFile.getFileId());
					}
					else {
						taskFileDto.setServerFile("/room/task/get_thumbnail?fileId=" + taskFile.getFileId());
					}
					taskFileList.add(taskFileDto);
				}
			}
			taskDto.setTaskFileList(taskFileList);
		}
		return taskDto;
	}
	
	private static String getFileType(String fileUri){
		String fileType = "";
		if(fileUri.lastIndexOf(".") > 0){
			fileType = fileUri.substring(fileUri.lastIndexOf(".")+1,fileUri.length()).toUpperCase();
		}
		 return fileType;
	}

	/**   
	* @author: Annie_Xiao
	* @since: Jul 30, 2015
	* 
	* @description
	* write the description here
	*/  
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveRoomHousekeepTask(RoomHousekeepTask roomHousekeepTask) throws Exception
	{
		roomHousekeepTaskDao.save(roomHousekeepTask);
	}

    @Override
	@Transactional
	public Long updateRoomHousekeepTask(UpdateRoomTaskDto taskDto, String userId) throws Exception
	{
		RoomHousekeepTask task = roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskDto.getTaskId());
		if(null != task){
			Room room = task.getRoom();
			String oldStatus = task.getStatus();
			StaffMaster staffMaster = staffMasterDao.getByUserId(userId);
			String roleType = staffMaster.getStaffType();
			switch (taskDto.getAction())
			{
			case "AR":
				if (task.getStartTimestamp() != null) {
					throw new GTACommonException(GTAError.HouseKeepError.ALREADY_ARRIVED);
				}
				handleTaskArrive(taskDto, userId, task, room, roleType);
				break;

			case "CMP":
				handleTaskComplete(userId, task, room, roleType);
				break;

			case "DND":
				if (task.getJobType()!=null && task.getJobType().equalsIgnoreCase(RoomHousekeepTaskJobType.RUT.toString())) {
					task.setStatus(RoomHousekeepTaskStatus.DND.name());
					autoRemindRoomAssistant(Constant.TEMPLATE_ID_DND_STATE,RoomCrewRoleType.ISP.name(),room,task);
				}
				else
				{
					throw new GTACommonException(GTAError.HouseKeepError.ONLY_ROUTINE_TASK_CAN_BE_DND);
				}
				break;
			}
			task.setUpdateBy(userId);
			task.setUpdateDate(new Date());
			task.setStatusChgBy(userId);
			task.setStatusDate(new Date());
			roomHousekeepTaskDao.update(task);
			//save task status log
			//when click arrive button in app, add a middle status name "ATN" to record the action, then when complete the task, the log should be ATN to RTI
			if(!task.getStatus().equals(oldStatus)){
				//when action is "CMP",   the log should be ATN to RIT
				if(RoomHousekeepTaskStatus.OPN.name().equals(oldStatus)&&"CMP".equals(taskDto.getAction())){
					RoomHousekeepStatusLog log = buildRoomStatusLog(task.getTaskId(), RoomHousekeepTaskStatus.ATN.name(), task.getStatus(), userId);
					roomHousekeepStatusLogDao.saveOrUpdate(log);
				}else{
					RoomHousekeepStatusLog log = buildRoomStatusLog(task.getTaskId(), oldStatus, task.getStatus(), userId);
					roomHousekeepStatusLogDao.saveOrUpdate(log);
				}				
			}else if(RoomHousekeepTaskStatus.OPN.name().equals(oldStatus)&&"AR".equals(taskDto.getAction())){ 
				//OPN to ATN
				RoomHousekeepStatusLog log = buildRoomStatusLog(task.getTaskId(), oldStatus, RoomHousekeepTaskStatus.ATN.name() , userId);
				roomHousekeepStatusLogDao.saveOrUpdate(log);
			}
		}else throw new GTACommonException(GTAError.HouseKeepError.TASK_NOT_FOUND);
		return task.getTaskId();
	}
    
	private RoomHousekeepStatusLog buildRoomStatusLog(Long taskId, String statusFrom, String statusTo, String changeBy) {
		RoomHousekeepStatusLog log = new RoomHousekeepStatusLog();
		log.setTaskId(taskId);
		log.setStatusFrom(statusFrom);
		log.setStatusTo(statusTo);
		log.setStatusChgBy(changeBy);
		log.setStatusDate(new Date());
		return log;
	}

	private void handleTaskComplete(String userId, RoomHousekeepTask task, Room room, String roleType) {
		if (null == task.getStartTimestamp())
			task.setStartTimestamp(new Timestamp(new Date().getTime()));
		if (null == task.getFinishTimestamp())
			task.setFinishTimestamp(new Timestamp(new Date().getTime()));
		if (roleType.equals(RoomCrewRoleType.RA.name()) || roleType.equals(RoomCrewRoleType.NGR.name())) {
			if(roleType.equals(RoomCrewRoleType.RA.name()) && RoomHousekeepTaskJobType.MTN.name().equals(task.getJobType())){
				task.setStatus(RoomHousekeepTaskStatus.CMP.name());
			}else{
				task.setStatus(RoomHousekeepTaskStatus.RTI.name());
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_COMPLETED,RoomCrewRoleType.ISP.name(),room,task);
			}
			if (roleType.equals(RoomCrewRoleType.NGR.name())) {
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_COMPLETED,RoomCrewRoleType.RA.name(),room,task);
			}
		} else {
			task.setStatus(RoomHousekeepTaskStatus.CMP.name());
			task.setInspectorUserId(userId);
			if (roleType.equals(RoomCrewRoleType.ISP.name())  && RoomHousekeepTaskJobType.ADH.name().equals(task.getJobType()))
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_COMPLETED,RoomCrewRoleType.SPV.name(),room,task);
			else if (roleType.equals(RoomCrewRoleType.SPV.name())  && RoomHousekeepTaskJobType.ADH.name().equals(task.getJobType()))
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_COMPLETED,RoomCrewRoleType.MGR.name(),room,task);
		}
		//Room change to R after RA complete, and change to VC after ISP complete
		/***
		 * SGG-2642   
		 * update  Maintenance Task CMP housekeepTask status ,no update room status 
		 * by christ 
		 * 2016-09-07
		 *
		 **/
		if (!RoomHousekeepTaskJobType.MTN.name().equals(task.getJobType())) {
			if (roleType.equals(RoomCrewRoleType.RA.name())) {
				room.setStatus(RoomStatus.R.name());
			} else {
				room.setStatus(RoomStatus.VC.name());
			}
			roomDao.update(room);
		}
	   
	}

	private void handleTaskArrive(UpdateRoomTaskDto taskDto, String userId, RoomHousekeepTask roomHousekeepTask, Room room, String roleType) {
		if (null != taskDto.getBeacons() && taskDto.getBeacons().length > 0) {
			List<BeaconRoomDto> beaconRooms = ibeaconDao.getRoomBeaconsList(room.getRoomId());
			boolean hasBeacon = false;
			for (int i = 0; i < taskDto.getBeacons().length; i++) {
				for (BeaconRoomDto beaconRoomDto : beaconRooms) {
					if (beaconRoomDto.getMajor().equals( taskDto.getBeacons()[i].getMajor()) && beaconRoomDto.getMinor().equals(taskDto.getBeacons()[i].getMinor())) {
						roomHousekeepTask.setMonitorDetected("Y");
						hasBeacon = true;
						break;
					}
				}
				if (hasBeacon)
					break;
			}
		}

		roomHousekeepTask.setStartTimestamp(new Timestamp(new Date().getTime()));
		if (!roomHousekeepTask.getStatus().equals(RoomHousekeepTaskStatus.OPN.name()))
			roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.OPN.name());
		if (roleType.equals(RoomCrewRoleType.NGR.name())) {
			saveRoomHousekeepTaskAssignee(userId, roomHousekeepTask, "MT");
		} else {
			List<RoomCrew> roomCrewList = roomCrewDao.getRoomCrewListByCrewRoleId(room.getRoomId(), roleType);
			for (RoomCrew roomCrew : roomCrewList) {
				RoomHousekeepTaskAssigneePK pk = new RoomHousekeepTaskAssigneePK();
				pk.setStaffUserId(roomCrew.getStaffProfile().getUserId());
				pk.setTaskId(roomHousekeepTask.getTaskId());
				RoomHousekeepTaskAssignee taskAssignee = roomHousekeepTaskAssigneeDao.get(RoomHousekeepTaskAssignee.class, pk);
				if (null == taskAssignee) {
					taskAssignee = new RoomHousekeepTaskAssignee();
					taskAssignee.setTaskRole("HK");
					taskAssignee.setId(pk);
					roomHousekeepTaskAssigneeDao.save(taskAssignee);
				}
			}
		}
	}

	private void saveRoomHousekeepTaskAssignee(String userId, RoomHousekeepTask roomHousekeepTask, String taskRole)
	{
		RoomHousekeepTaskAssigneePK pk = new RoomHousekeepTaskAssigneePK();
		pk.setStaffUserId(userId);
		pk.setTaskId(roomHousekeepTask.getTaskId());
		RoomHousekeepTaskAssignee taskAssignee = roomHousekeepTaskAssigneeDao.get(RoomHousekeepTaskAssignee.class, pk);
		if(null == taskAssignee){
			taskAssignee = new RoomHousekeepTaskAssignee();
			taskAssignee.setTaskRole(taskRole);
			taskAssignee.setId(pk);
			roomHousekeepTaskAssigneeDao.save(taskAssignee);
		}
	}

	@Override
	@Transactional
	public RoomHousekeepTaskDto getRoomHousekeepTask(Long taskId)
	{
		RoomHousekeepTaskDto roomHousekeepTaskDto = null;
		RoomHousekeepTask roomHousekeepTask = roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskId);
		if(roomHousekeepTask != null) roomHousekeepTaskDto = createRoomHousekeepTaskDto(roomHousekeepTask);
		return roomHousekeepTaskDto;
	}

	@Transactional
	@Override
	public GuestRequestDto getGuestRequestDto(Long taskId) {
		RoomHousekeepTask roomHousekeepTask = roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskId);
		GuestRequestDto guestRequestDto = new GuestRequestDto();
		guestRequestDto.setTaskId(roomHousekeepTask.getTaskId());
		guestRequestDto.setCreateDate(roomHousekeepTask.getCreateDate());
		guestRequestDto.setTaskDescription(roomHousekeepTask.getTaskDescription());
		guestRequestDto.setStatus(roomHousekeepTask.getStatus());
		guestRequestDto.setRoomNo(roomHousekeepTask.getRoom().getRoomNo());
		return guestRequestDto;
	}

	@Transactional
	@Override
	public boolean removeGuestRequestDto(Long taskId, String userId) {
		RoomHousekeepTask roomHousekeepTask = roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskId);
		boolean isSucess = false;
		
		if (RoomHousekeepTaskStatus.PND.toString().equals(roomHousekeepTask.getStatus())) {
			roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.CAN.toString());
			roomHousekeepTask.setUpdateBy(userId);
			roomHousekeepTask.setUpdateDate(new Date());
			roomHousekeepTask.setStatusChgBy(userId);
			roomHousekeepTask.setStatusDate(new Date());
			isSucess = roomHousekeepTaskDao.update(roomHousekeepTask);
		}
		
		return isSucess;
	}

	@Transactional
	@Override
	public boolean approveGuestRequestDto(Long taskId, String taskDescription, String userId) {
		RoomHousekeepTask roomHousekeepTask = roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskId);

		boolean isSucess = false;
		if (RoomHousekeepTaskStatus.PND.toString().equals(roomHousekeepTask.getStatus())) {
			roomHousekeepTask.setTaskDescription(taskDescription);
			roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.OPN.toString());
			roomHousekeepTask.setScheduleDatetime(new Date());
			roomHousekeepTask.setUpdateBy(userId);
			roomHousekeepTask.setUpdateDate(new Date());
			roomHousekeepTask.setStatusChgBy(userId);
			roomHousekeepTask.setStatusDate(new Date());
			isSucess = roomHousekeepTaskDao.update(roomHousekeepTask);
		}
		return isSucess;
	}

	@Override
	@Transactional
	public ResponseResult assignAdHocTask(AssignAdHocTaskDto dto, String status, String userId) {
		if(dto.getRoomId()==null){
			responseResult.initResult(GTAError.HouseKeepError.ROOM_ID_EMPTY);
			return responseResult;
		}
		if(org.apache.commons.lang.StringUtils.isBlank(dto.getTaskDescription())){
			responseResult.initResult(GTAError.HouseKeepError.TASK_DESCRI_EMPTY);
			return responseResult;
		}
		try {
			GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, "HK-MAXRESP-RA");
			Room room = roomDao.get(Room.class, dto.getRoomId());
			if(room != null){
				RoomHousekeepTask roomHousekeepTask = new RoomHousekeepTask();
				roomHousekeepTask.setStatus(status);
				roomHousekeepTask.setRequesterUserId(userId);
				roomHousekeepTask.setScheduleDatetime(new Date());
				roomHousekeepTask.setJobType(RoomHousekeepTaskJobType.ADH.toString());
				roomHousekeepTask.setExpResponseTime(null == globalParameter?null:Long.parseLong(globalParameter.getParamValue()));
				roomHousekeepTask.setRoom(room);
				roomHousekeepTask.setTaskDescription(dto.getTaskDescription());
				roomHousekeepTask.setCreateBy(userId);
				roomHousekeepTask.setCreateDate(new Timestamp(new Date().getTime()));
				roomHousekeepTask.setStatusDate(null);
				roomHousekeepTask.setStatusChgBy(null);
				roomHousekeepTaskDao.save(roomHousekeepTask);

				if (RoomHousekeepTaskStatus.OPN.toString().equals(status)) {
					autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_ASSIGNED,RoomCrewRoleType.RA.name(),room,roomHousekeepTask);
				}

				responseResult.initResult(GTAError.Success.SUCCESS);
			}else{
				responseResult.initResult(GTAError.HouseKeepError.ROOM_IS_NOT_EXIST);
			}
		} catch (NumberFormatException e) {
			logger.error(RoomHousekeepTaskServiceImpl.class.getName() + " assignAdHocTask Failed!", e);
			responseResult.initResult(GTAError.HouseKeepError.FAIL_ASSIGN_TASK);
		} catch (HibernateException e) {
			logger.error(RoomHousekeepTaskServiceImpl.class.getName() + " assignAdHocTask Failed!", e);
			responseResult.initResult(GTAError.HouseKeepError.FAIL_ASSIGN_TASK);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult assignAdHocTask(AssignAdHocTaskDto dto, String userId) {
		return assignAdHocTask(dto, RoomHousekeepTaskStatus.OPN.toString(), userId);
	}

	@Override
	@Transactional
	public ResponseResult assignMaintenanceTask(AssignMaintenanceTaskDto dto,String userId) {
		if(dto.getRoomId()==null){
			responseResult.initResult(GTAError.HouseKeepError.ROOM_ID_EMPTY);
			return responseResult;
		}
		try {
			GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, "HK-MAXRESP-RA");
			Room room = roomDao.get(Room.class, dto.getRoomId());
			if(room != null){
				RoomHousekeepTask roomHousekeepTask = new RoomHousekeepTask();
				roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.OPN.toString());
				roomHousekeepTask.setRequesterUserId(userId);
				roomHousekeepTask.setScheduleDatetime(new Date());
				roomHousekeepTask.setJobType(RoomHousekeepTaskJobType.MTN.toString());
				roomHousekeepTask.setExpResponseTime(null == globalParameter?null:Long.parseLong(globalParameter.getParamValue()));
				roomHousekeepTask.setRoom(room);
				roomHousekeepTask.setTaskDescription(dto.getTaskDescription());
				roomHousekeepTask.setCreateBy(userId);
				roomHousekeepTask.setCreateDate(new Timestamp(new Date().getTime()));
				roomHousekeepTask.setStatusDate(null);
				roomHousekeepTask.setStatusChgBy(null);
				Long taskId = (Long)roomHousekeepTaskDao.save(roomHousekeepTask);
				if(dto.getTaskFiles()!=null&&dto.getTaskFiles().length>0){
					String[] serverFiles = dto.getTaskFiles();
					for (int i = 0; i < serverFiles.length; i++) {
						if(!StringUtils.isEmpty(serverFiles[i])){
							HousekeepTaskFile file = new HousekeepTaskFile();
							file.setCreateBy(userId);
							Date date = new Date();
							file.setCreateDate(new Timestamp(date.getTime()));
							file.setServerFile(serverFiles[i]);
							file.setStatus("ACT");
							file.setTaskId(taskId);
							file.setUpdateBy(userId);
							file.setUpdateDate(date);
							housekeepTaskFileDao.saveOrUpdate(file);
						}
					}
				}
				autoRemindRoomAssistant(Constant.TEMPLATE_ID_TASK_ASSIGNED,RoomCrewRoleType.NGR.name(),room,roomHousekeepTask);
				responseResult.initResult(GTAError.Success.SUCCESS);
			}else{
				responseResult.initResult(GTAError.HouseKeepError.ROOM_IS_NOT_EXIST);
			}
		} catch (NumberFormatException e) {
			logger.error(RoomHousekeepTaskServiceImpl.class.getName() + " assignMaintenanceTask Failed!", e);
			responseResult.initResult(GTAError.HouseKeepError.FAIL_ASSIGN_TASK);
		} catch (HibernateException e) {
			logger.error(RoomHousekeepTaskServiceImpl.class.getName() + " assignMaintenanceTask Failed!", e);
			responseResult.initResult(GTAError.HouseKeepError.FAIL_ASSIGN_TASK);
		}
		
		return responseResult;
		
	}

	@Override
	@Transactional
	public ResponseResult getStaffType(String userId) {
		StaffMaster sm = staffMasterDao.getByUserId(userId);
		if(sm!=null){
			Map<String, String> data = new HashMap<String, String>();
			data.put("roleType", sm.getStaffType());
			responseResult.initResult(GTAError.Success.SUCCESS, data);
		}else{
			responseResult.initResult(GTAError.HouseKeepError.NOT_FOUND_STAFF_TYPE);
		}
		
		return responseResult;
	}

	@Override
	@Transactional
	public RoomHousekeepTask getRoomHousekeepTaskById(Long taskId)
	{
		return roomHousekeepTaskDao.get(RoomHousekeepTask.class, taskId);
	}

	@Override
	@Transactional
	public void update(RoomHousekeepTask roomHousekeepTask)
	{
		roomHousekeepTaskDao.update(roomHousekeepTask);
	}

	@Override
	@Transactional
	public List<RoomHousekeepTask> getRoomHousekeepTaskUnCmpList(String jobType)
	{
		return roomHousekeepTaskDao.getRoomHousekeepTaskUnCmpList(jobType);
	}

	@Override
	@Transactional
	public List<RoomMemberDto> getMyRoomListForApp(String userId, String roleType, String roomNo)
	{
		return roomHousekeepTaskDao.getMyRoomListForApp(userId, roleType, roomNo);
	}
	
	@Override
	@Transactional
	public List<RoomHousekeepTaskQueryDto> getMyRoomTaskListForApp(String userId, String roleType, String roomNo)
	{
		return roomHousekeepTaskDao.getMyRoomTaskListForApp(userId, roleType, roomNo);
	}
	
	@Override
	@Transactional
	public List<RoomMemberDto> getRoomListForApp(String userId, String roleType, String roomNo)
	{
		return roomHousekeepTaskDao.getRoomListForApp(userId, roleType, roomNo);
	}
	
	@Override
	@Transactional
	public List<RoomHousekeepTaskQueryDto> getRoomTaskListForApp(String userId, String roleType, String roomNo)
	{
		return roomHousekeepTaskDao.getRoomTaskListForApp(userId, roleType, roomNo);
	}
	
	/**
	 * 会员添加房间任务
	 * @param userId
	 * @param roomId
	 * @param serviceContent  服务详情内容
	 * @return
	 */
	@Override
	@Transactional
	public ResponseResult addRoomTaskByMember(String customerId, String roomId, String serviceContent)
	{
		GlobalParameter globalParameter = globalParameterDao.get(GlobalParameter.class, "HK-MAXRESP-RA");
		Room room = roomDao.get(Room.class, Long.parseLong(roomId));
		Member member = memberDao.get(Member.class, Long.parseLong(customerId));
		Long taskId = null;
		if(room != null && member != null){
			RoomHousekeepTask roomHousekeepTask = new RoomHousekeepTask();
			roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.PND.toString());
			roomHousekeepTask.setRequesterUserId(member.getUserId());
			roomHousekeepTask.setScheduleDatetime(new Date());
			roomHousekeepTask.setJobType(RoomHousekeepTaskJobType.ADH.toString());
			roomHousekeepTask.setExpResponseTime(null == globalParameter?null:Long.parseLong(globalParameter.getParamValue()));
			roomHousekeepTask.setRoom(room);
			roomHousekeepTask.setTaskDescription(serviceContent);
			roomHousekeepTask.setCreateBy(member.getUserId());
			roomHousekeepTask.setCreateDate(new Timestamp(new Date().getTime()));
			roomHousekeepTask.setStatusDate(null);
			roomHousekeepTask.setStatusChgBy(null);
			taskId = (Long)roomHousekeepTaskDao.save(roomHousekeepTask);
				responseResult.initResult(GTAError.Success.SUCCESS);
		} else {
			responseResult.initResult(GTAError.HouseKeepError.ROOM_IS_NOT_EXIST);
		}
		return responseResult;
	}
}

