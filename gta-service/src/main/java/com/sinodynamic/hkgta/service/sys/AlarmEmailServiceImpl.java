package com.sinodynamic.hkgta.service.sys;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;

@Repository
public class AlarmEmailServiceImpl implements  AlarmEmailService{
	Logger				logger	= Logger.getLogger(AlarmEmailServiceImpl.class);
	@Resource(name = "appProperties")
	private Properties	appProps;
	private static ExecutorService pool = Executors.newCachedThreadPool();
	
	/**
	 * 发送警报邮件
	 * @param taskName
	 * @param extContent
	 */
	public void sendAlarmEmail(String taskName, Exception ex) {

		try {
			String toAddress = appProps.getProperty(Constant.CHECK_SERVER_TO_EMAIL);
			logger.debug("start sending email:" + toAddress);
			if (!StringUtils.isBlank(toAddress)) {
				String subject = "【"+ taskName + "】定时任务执行错误警告";
				String content = getStackMsg(ex);
				boolean isOK = MailSender.sendEmail(toAddress, null, null, subject, content, null);
				logger.debug("email has been sent:" + isOK);
			}	
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 线程发送警报邮件
	 */
	@Override
	public void sendAlarmEmailTask(final String taskName, final Exception e) {
		 Runnable smsTask = new Runnable() {
		        public void run() {
			        try {
			        	sendAlarmEmail(taskName, e);
				    } catch (Exception e) {
				    	logger.error(e.getMessage(), e);
				    }
		        }
		    };
		    pool.execute(smsTask);
	}

	/**
	 * 将堆栈信息转换成字符串
	 * @param e
	 * @return
	 */
	private static String getStackMsg(Exception e) {  
		  
        StringBuffer sb = new StringBuffer();  
        StackTraceElement[] stackArray = e.getStackTrace();  
        for (int i = 0; i < stackArray.length; i++) {  
            StackTraceElement element = stackArray[i];  
            sb.append(element.toString() + "\n");  
        }  
        return sb.toString();  
    }  
}
