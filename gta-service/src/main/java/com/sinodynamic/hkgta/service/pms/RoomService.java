package com.sinodynamic.hkgta.service.pms;

import java.io.File;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.pms.RoomDetailDto;
import com.sinodynamic.hkgta.dto.pms.RoomDto;
import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.dto.push.RegisterDto;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.MessageResult;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface RoomService extends IServiceBase<Room> {

	String DEFAULT_PATH_PREFIX_KEY = "image.server.material";
	
	@Deprecated
	public List<RoomDto> getRoomList(String updateBy) throws Exception;

	public List<RoomDto> getRoomListWithTaskCounts(String updateBy) throws Exception;

	public RoomDetailDto getRoomDetail(Long roomId) throws Exception;
	
	public ResponseResult changeRoomStatus(Long roomId,String status,String userId) throws Exception;
	
	public List<Room> getRoomListByStatus(String[] status) throws Exception;
	
	public List<Room> getRoomListOutOfStatus(String[] status) throws Exception;
	
	public void updateRoom(Room room);
	
	public List<RoomDto> getRoomCrewList(String crewRoleId);
	
	public ResponseResult getRoomTaskList(String userId,Boolean isMyRoom,String roomNo);
	
	public MessageResult updateRoomStatus(UpdateRoomStatusDto dto) throws Exception;

	public ResponseResult pushRegister(RegisterDto dto,String userId);
	
	public List<Room> getRoomList();
	
	public void staffPushRegister(String staffId, String deviceArn, String token, String platform, String version, String updatedBy);
	
	public List<Room> getLimitedInfoRoomList();
	
	public List<Room> getCheckInRoomList();
	
	/***
	 * getRoomList by serviceStatus
	 * @param serviceStatus
	 * @return
	 */
	public List<Room>getRoomListServiceStatus(String serviceStatus);

	/**
	 * 是否可以checking
	 * @param customerId
	 * @param resvId
	 * @return
	 */
	public ResponseResult isCanCheckIn(Long customerId, Long resvId);
	
	/**
	 * member checkIn
	 * @param customerId
	 * @param resvId
	 * @return
	 */
	public ResponseResult checkInProgress(Long customerId, Long resvId);

	/**
	 * 获取当前今天CheckInProgress状态的总数
	 * @return
	 */
	public int getSilentCheckInProgressTotalNum();
	
	public void autoPushMsg4SilentCheckin() ;

	public Room getRoomByRoomNo(Long roomNo);
	
	public  ResponseResult importAllotment(String customerId,File csvFile) throws Exception;
	
	public boolean  showDueInByRoomNo(String roomNo);

	/**
	 * 对CheckInProgress状态的数据进行决策 ，accept批准/reject拒绝
	 * @param decision
	 * @param customerId
	 * @param resvId
	 * @return
	 * @throws Exception 
	 */
	public ResponseResult acceptOrRejectSilentCheckInProgress(String decision, Long customerId, Long resvId) throws Exception;
	
}
