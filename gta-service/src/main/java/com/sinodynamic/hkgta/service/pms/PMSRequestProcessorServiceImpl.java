package com.sinodynamic.hkgta.service.pms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.sinodynamic.hkgta.dao.adm.PermitCardMasterDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MemberLimitRuleDao;
import com.sinodynamic.hkgta.dao.fms.FacilityMasterDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotDao;
import com.sinodynamic.hkgta.dao.fms.FacilityTypeDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityBookAdditionAttrDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.fms.MemberReservedFacilityDao;
import com.sinodynamic.hkgta.dao.pms.PMSRequestProcessorDao;
import com.sinodynamic.hkgta.dao.pms.RoomDao;
import com.sinodynamic.hkgta.dao.pms.RoomHousekeepTaskDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationChangeLogDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderDetDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dto.crm.MemberCashValuePaymentDto;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationInfoDto;
import com.sinodynamic.hkgta.dto.pms.xml.input.changeroom.HTNGHotelRoomMoveNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.checkin.HTNGHotelCheckInNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.checkout.HTNGHotelCheckOutNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.epayment.Account;
import com.sinodynamic.hkgta.dto.pms.xml.input.epayment.HTNGChargePostingRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.epayment.RevenueDetail;
import com.sinodynamic.hkgta.dto.pms.xml.input.epayment.RevenueDetails;
import com.sinodynamic.hkgta.dto.pms.xml.input.facility.OTAGolfCourseAvailRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.membership.HTNGReadRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.membership.RequestedComponent;
import com.sinodynamic.hkgta.dto.pms.xml.input.membership.RequestedComponents;
import com.sinodynamic.hkgta.dto.pms.xml.output.changroom.HTNGHotelRoomMoveNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.checkin.HTNGHotelCheckInNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.checkout.HTNGHotelCheckOutNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.epayment.HTNGChargePostingRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.facility.Amenity;
import com.sinodynamic.hkgta.dto.pms.xml.output.facility.FacilityInfo;
import com.sinodynamic.hkgta.dto.pms.xml.output.facility.OTAGolfCourseAvailRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.facility.TeeTimes;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.Address;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.CountryName;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.CustLoyalty;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.Customer;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.Email;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.HTNGProfileReadRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.PersonName;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.Profile;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.ProfileInfo;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.Profiles;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.SubAccountBalance;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.Success;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.Telephone;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttrPK;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.fms.MemberReservedFacility;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.entity.pms.RoomReservationChangeLog;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.membercash.MemberCashvalueService;
import com.sinodynamic.hkgta.service.fms.FacilityTypeQuotaService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.service.rpos.RefundService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.PermitCardMasterEnumStatus;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;
import com.sinodynamic.hkgta.util.constant.RoomStatus;
import com.sinodynamic.hkgta.util.constant.limitUnit;

@Service
public class PMSRequestProcessorServiceImpl extends ServiceBase implements PMSRequestProcessorService {

	@Autowired
	BookingFacilityService						bookingFacilityService;

	@Autowired
	private RoomReservationRecDao				roomReservationRecDao;

	@Autowired
	private RoomReservationChangeLogDao			roomReservationChangeLogDao;

	@Autowired
	private MemberDao							memberDao;

	@Autowired
	private CustomerProfileDao					customerProfileDao;

	@Autowired
	private PermitCardMasterDao					permitCardMasterDao;


	@Autowired
	private MemberFacilityTypeBookingDao		memberFacilityTypeBookingDao;

	@Autowired
	private MemberReservedFacilityDao			memberReservedFacilityDao;


	@Autowired
	private CustomerOrderHdDao					customerOrderHdDao;

	@Autowired
	private CustomerOrderDetDao					customerOrderDetDao;

	@Autowired
	private CustomerOrderTransDao				customerOrderTransDao;

	@Autowired
	private MemberFacilityBookAdditionAttrDao	memberFacilityBookAdditionAttrDao;

	@Autowired
	private MemberCashvalueService				memberCashValueService;
	
	@Autowired
	private MemberLimitRuleDao                  memberLimitRuleDao;

	@Autowired
	private RoomHousekeepTaskDao				roomHousekeepTaskDao;

	@Autowired
	private RoomDao								roomDao;

	@Value(value = "#{apiProperties['officalCheckInHour']}")
	private Integer								officalCheckInHour;																	// 下午3点--当天预订bundle最早时间点，如果checkIn的时间晚于这个点，往后延迟一个小时开始预订

	@Value(value = "#{apiProperties['latestTimeHour']}")
	private Integer								latestTimeHour;																		// 下午10点--当天预订bundle的最晚时间点

	@Value(value = "#{apiProperties['earlestTimeHour']}")
	private Integer								earlestTimeHour;																	// 上午7点--如果当天没有找到时间，往后一天去找，从这个时间点开始找，一直找到officalCheckInHour时间点为止

	@Value(value = "#{apiProperties['zk.connectionString']}")
	private String								zookeeperConnectionString;

	private static Logger						logger			= Logger.getLogger(PMSRequestProcessorServiceImpl.class);
	private Logger						        pmsLog	= Logger.getLogger(LoggerType.PMS.getName());

	private String								facilityType	= "GOLF";
	private String								attributeId		= "GOLFBAYTYPE-RH";
	private String								status			= "ACT";
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	@Autowired
	private RefundService refundService;
	
	@Autowired
	private RoomReservationRecService roomReservationRecService;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	private PermitCardMasterService permitCardMasterService;
	
	@Override
	@Transactional
	public HTNGHotelCheckInNotifRS checkIn(HTNGHotelCheckInNotifRQ request) {
		HTNGHotelCheckInNotifRS response = new HTNGHotelCheckInNotifRS();
		Assert.notNull(request);
		Assert.notNull(request.getAffectedGuests());
		Assert.notNull(request.getAffectedGuests().getUniqueID());
		Assert.notNull(request.getAffectedGuests().getUniqueID().getID());
		RoomReservationRec roomReservationRec;
		String comfirmId = request.getAffectedGuests().getUniqueID().getID();
		String roomNo = request.getRoom().getRoomID();

		roomReservationRec = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", comfirmId);
		pmsLog.info("OASIS Call hkgta  CheckIn  start ......comfirmId:"+comfirmId+",roomNo:"+roomNo);
		
		if (roomReservationRec != null) {
			// check whether the reservation has checked already
			Date checkInDate = roomReservationRec.getCheckinTimestamp();
			if (null != checkInDate) {
				com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
				error.setErrorCode("001");
				error.setErrorMsg("The reservation has check in already,you can't check in again.");
				response.setError(error);
				logger.debug("Check in failed for reservation:" + comfirmId);
				
				pmsLog.info("Check in failed for reservation:" + comfirmId);
			} else {
				roomReservationRec.setCheckinTimestamp(new Date());
				Room room = roomDao.getByRoomNo(roomNo);
				if (null != room)
					roomReservationRec.setRoomId(room.getRoomId());
					roomReservationRec.setStatus(RoomReservationStatus.CKI.name());
					boolean flag = roomReservationRecDao.update(roomReservationRec);
				if (flag&&room!=null) {
					updateCheckInRoomStatusAndFrontdeskStatus(roomReservationRec.getRoomId(), Constant.RoomFrontdeskStatus.O.name());
					roomHousekeepTaskDao.cancelRoomHousekeepTask(roomReservationRec.getRoomId(), RoomHousekeepTaskJobType.ADH.name());

					pushNotificationForMemberAppAndStaffApp(roomReservationRec.getCustomerId(), roomReservationRec.getRoomId());
					
					com.sinodynamic.hkgta.dto.pms.xml.output.checkin.Success succ = new com.sinodynamic.hkgta.dto.pms.xml.output.checkin.Success();
					response.setSuccess(succ);
					pmsLog.info("Check in success for reservation:" + comfirmId);
				} else {
					com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
					error.setErrorCode("002");
					error.setErrorMsg("GTA save check in data failed.");
					response.setError(error);
					pmsLog.info("Check in failed for reservation:" + request.getAffectedGuests().getUniqueID().getID());
				}
			}
		} else {
			Room room = roomDao.getByRoomNo(roomNo);
			if (null != room)
				updateCheckInRoomStatusAndFrontdeskStatus(room.getRoomId(), Constant.RoomFrontdeskStatus.O.name());
			com.sinodynamic.hkgta.dto.pms.xml.output.checkin.Success succ = new com.sinodynamic.hkgta.dto.pms.xml.output.checkin.Success();
			response.setSuccess(succ);
			pmsLog.info("Check in room " + roomNo + " success for non-parton reservation");
//			com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
//			error.setErrorCode("003");
//			error.setErrorMsg("The reservationId is not existed.");
//			response.setError(error);
//			logger.debug("Check in failed for reservation:" + comfirmId + ",the reservation is not existed.");
		}
		pmsLog.info("OASIS Call hkgta  CheckIn  end.");
		return response;
	}

	@Override
	@Transactional
	public HTNGHotelRoomMoveNotifRS changeRoom(HTNGHotelRoomMoveNotifRQ request) {
		HTNGHotelRoomMoveNotifRS response = new HTNGHotelRoomMoveNotifRS();
		Assert.notNull(request);
		Assert.notNull(request.getAffectedGuests());
		Assert.notNull(request.getAffectedGuests().getUniqueID());
		Assert.notNull(request.getAffectedGuests().getUniqueID().getID());
		Assert.notNull(request.getSourceRoomInformation());
		Assert.notNull(request.getSourceRoomInformation().getRoom());
		Assert.notNull(request.getSourceRoomInformation().getRoom().getRoomID());
		Assert.notNull(request.getDestinationRoomInformation());
		Assert.notNull(request.getDestinationRoomInformation().getRoom());
		Assert.notNull(request.getDestinationRoomInformation().getRoom().getRoomID());
		String reservationId = request.getAffectedGuests().getUniqueID().getID();
		String sourceRoomNo = request.getSourceRoomInformation().getRoom().getRoomID();
		String destinationRoomNo = request.getDestinationRoomInformation().getRoom().getRoomID();

		Room sourceRoom = roomDao.getByRoomNo(sourceRoomNo);
		Room destinationRoom = roomDao.getByRoomNo(destinationRoomNo);
		Long sourceRoomId = sourceRoom.getRoomId();
		Long destinationRoomId = destinationRoom.getRoomId();
		
		//added by Kaster 20160506 打印日志以便后台跟踪执行过程。
		logger.info("running in changeRoom method of service......");
		logger.info("sourceRoomId,destinationRoomId: " + sourceRoomId + "," + destinationRoomId);

		// check the reservation id
		RoomReservationRec roomReservationRec = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", reservationId);
		if (roomReservationRec == null) {
			updateSourceRoomAndDestTaskRoom(sourceRoomNo, destinationRoomNo);
			if(sourceRoom != null){
				if(!checkExistRoutineTask(sourceRoom.getRoomId(), RoomHousekeepTaskJobType.RUT.name())){
	            	createRoutineTask(sourceRoom.getRoomId(), RoomHousekeepTaskJobType.RUT.name());
	            }
			}
			com.sinodynamic.hkgta.dto.pms.xml.output.changroom.Success succ = new com.sinodynamic.hkgta.dto.pms.xml.output.changroom.Success();
			response.setSuccess(succ);
			logger.debug("Change room from " + sourceRoomNo + " to " + destinationRoomNo + " success for non-parton reservation");
//			com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
//			error.setErrorCode("010");
//			error.setErrorMsg("Invalid request, the reservation is not existed.reservationId =" + reservationId);
//			response.setError(error);
//			logger.debug("Change room failed for reservation:" + reservationId);
			return response;
		}

		// check the current roomId
		Long currentRoomId = roomReservationRec.getRoomId();
		if (sourceRoomId.longValue() != currentRoomId.longValue()) {
			com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
			error.setErrorCode("011");
			error.setErrorMsg("Invalid request, the source room Id is not right");
			response.setError(error);
			logger.debug("Change room failed for reservation:" + reservationId);
			return response;
		}

		String hql = "from RoomReservationChangeLog where roomIdFrom = ? and roomIdTo = ?";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(sourceRoomId);
		params.add(destinationRoomId);
		List<RoomReservationChangeLog> result = roomReservationChangeLogDao.getByHql(hql, params);
		if (!(result!=null&&result.size()>0)) {
			RoomReservationChangeLog roomReservationChangeLog = new RoomReservationChangeLog();
			roomReservationChangeLog.setResvId(roomReservationRec.getResvId() + "");
			roomReservationChangeLog.setRoomIdFrom(sourceRoomId);
			roomReservationChangeLog.setRoomIdTo(destinationRoomId);
			roomReservationChangeLog.setChangeTimestamp(new Date());
			roomReservationChangeLog.setCreateDate(new Date());
			Serializable flag1 = roomReservationChangeLogDao.save(roomReservationChangeLog);

			roomReservationRec.setRoomId(destinationRoomId);
			boolean flag2 = roomReservationRecDao.update(roomReservationRec);

			if (flag1 != null && flag2) {

				//added by Kaster 20160506 打印日志以便后台跟踪执行过程。
				logger.info("running before updateSourceRoomAndDestTaskRoom method of service......");
				updateSourceRoomAndDestTaskRoom(sourceRoomNo, destinationRoomNo);
				com.sinodynamic.hkgta.dto.pms.xml.output.changroom.Success succ = new com.sinodynamic.hkgta.dto.pms.xml.output.changroom.Success();
				response.setSuccess(succ);
				logger.debug("Change room success for reservation:" + reservationId);
			} else {
				com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
				error.setErrorCode("008");
				error.setErrorMsg("GTA save change room data failed.");
				response.setError(error);
				logger.debug("Change room failed for reservation:" + reservationId);
			}
		} else {
			com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
			error.setErrorCode("009");
			error.setErrorMsg("Duplicated change room operation");
			response.setError(error);
			logger.debug("Change room failed for reservation:" + reservationId);
		}
		return response;

	}

	private void updateSourceRoomAndDestTaskRoom(String sourceRoomNo, String destinationRoomNo) {
		//added by Kaster 20160506 打印日志以便后台跟踪执行过程。
		logger.info("running in updateSourceRoomAndDestTaskRoom method of service......");
		logger.info("sourceRoomNo,destinationRoomNo:"+sourceRoomNo+","+destinationRoomNo);
		Room sourceRoom = roomDao.getByRoomNo(sourceRoomNo);
		Room destinationRoom = roomDao.getByRoomNo(destinationRoomNo);
		if(sourceRoom != null){
			//added by Kaster 20160506 打印日志以便后台跟踪执行过程。
			logger.info("before change source room status......");
			//Source room status: FD status = Vacant (V), HK status = Dirty (D)
			sourceRoom.setStatus(RoomStatus.D.name());
			sourceRoom.setFrontdeskStatus(Constant.RoomFrontdeskStatus.V.name());
			roomDao.update(sourceRoom);
			//should cancel the ADH for source room
			roomHousekeepTaskDao.cancelRoomHousekeepTask(sourceRoom.getRoomId(), RoomHousekeepTaskJobType.ADH.name());
			logger.info("after change source room status......");
		}
		if(destinationRoom != null){
			logger.info("before change destination room status......");
			//Destination room status: FD status = Occupied (O), HK status = "OC/D"
			if(RoomStatus.VC.name().equals(destinationRoom.getStatus()) 
					|| RoomStatus.R.name().equals(destinationRoom.getStatus())){
				destinationRoom.setStatus(RoomStatus.OC.name());
			}
			destinationRoom.setFrontdeskStatus(Constant.RoomFrontdeskStatus.O.name());
			roomDao.update(destinationRoom);
			logger.info("after change destination room status......");
		}
		
		/*if (null != room) {
			List<RoomHousekeepTask> roomHousekeepTasks = roomHousekeepTaskDao.getRoomHousekeepTaskUnCmpList(
					sourceRoomNo,
					RoomHousekeepTaskJobType.ADH.name());
			for (RoomHousekeepTask task : roomHousekeepTasks) {
				task.setRoom(room);
				task.setUpdateDate(new Date());
				roomHousekeepTaskDao.update(task);
			}
		}*/
	}

	@Override
	@Transactional
	public HTNGHotelCheckOutNotifRS checkOut(HTNGHotelCheckOutNotifRQ request) {
		HTNGHotelCheckOutNotifRS response = new HTNGHotelCheckOutNotifRS();
		Assert.notNull(request);
		Assert.notNull(request.getAffectedGuests());
		Assert.notNull(request.getAffectedGuests().getUniqueID());
		Assert.notNull(request.getAffectedGuests().getUniqueID().getID());
		String confirmId = request.getAffectedGuests().getUniqueID().getID();
		String roomNo = request.getRoom().getRoomID();
		RoomReservationRec roomReservationRec;
		roomReservationRec = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId);
		if (roomReservationRec != null) {
			// check whether the reservation has checked already
			Date checkInDate = roomReservationRec.getCheckinTimestamp();
			Date checkOutDate = roomReservationRec.getCheckoutTimestamp();
			boolean canCheckout = true;
			if (null == checkInDate) {
				com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
				error.setErrorCode("004");
				error.setErrorMsg("The reservationId has not check in yet, you must check in first.");
				response.setError(error);
				canCheckout = false;
				logger.debug("Check out failed for resercation:" + confirmId);
			} else if (null != checkOutDate) {
				com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
				error.setErrorCode("005");
				error.setErrorMsg("The reservation has check out already,you can't check out again.");
				response.setError(error);
				canCheckout = false;
				logger.debug("Check out failed for reservation:" + confirmId);
			}
			if (canCheckout) {
				roomReservationRec.setCheckoutTimestamp(new Date());
				roomReservationRec.setStatus(RoomReservationStatus.CKO.name());
				boolean flag = roomReservationRecDao.update(roomReservationRec);
				if (flag) {
					updateCheckOutRoomStatusAndFrontdeskStatus(roomReservationRec.getRoomId(), Constant.RoomFrontdeskStatus.V.name());
					//cancel roomHouseKeepTask  jobType is ADH  and status not in CMP/CAN
					roomHousekeepTaskDao.cancelRoomHousekeepTask(roomReservationRec.getRoomId(), RoomHousekeepTaskJobType.ADH.name());
                   
					com.sinodynamic.hkgta.dto.pms.xml.output.checkout.Success succ = new com.sinodynamic.hkgta.dto.pms.xml.output.checkout.Success();
					response.setSuccess(succ);
					logger.debug("Change out success for reservation:" + request.getAffectedGuests().getUniqueID().getID());
				} else {
					com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
					error.setErrorCode("006");
					error.setErrorMsg("GTA save check out data failed.");
					response.setError(error);
					logger.debug("check out failed for reservation:" + request.getAffectedGuests().getUniqueID().getID());
				}
			}

		} else {
			Room room = roomDao.getByRoomNo(roomNo);
			if (null != room){
				updateCheckOutRoomStatusAndFrontdeskStatus(room.getRoomId(), Constant.RoomFrontdeskStatus.V.name());
				if(!checkExistRoutineTask(room.getRoomId(), RoomHousekeepTaskJobType.RUT.name())){
	            	createRoutineTask(room.getRoomId(), RoomHousekeepTaskJobType.RUT.name());
	            }
			}
			com.sinodynamic.hkgta.dto.pms.xml.output.checkout.Success succ = new com.sinodynamic.hkgta.dto.pms.xml.output.checkout.Success();
			response.setSuccess(succ);
			logger.debug("Check out " + roomNo + " success for non-parton reservation");
//			com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
//			error.setErrorCode("007");
//			error.setErrorMsg("The reservationId is not existed.");
//			response.setError(error);
//			logger.debug("Check out failed for reservation:" + confirmId + ",the reservation is not existed.");
		}
		return response;

	}

	private void updateCheckInRoomStatusAndFrontdeskStatus(Long roomId, String frontdeskStatus) {
		Room room = roomDao.get(Room.class, roomId);
		if (null != room) {
			/*
			  If HK status was VC, it should be changed to OC after Checked In.
			  If HK status was D, it should be kept D after Checked In.
			  if HK status was "R", it should be changed to OC after Checked In
			*/
			if(RoomStatus.VC.name().equals(room.getStatus()) 
					|| RoomStatus.R.name().equals(room.getStatus())){
				room.setStatus(RoomStatus.OC.name());
			}
			room.setUpdateDate(new Date());
			room.setFrontdeskStatus(frontdeskStatus);
			roomDao.update(room);
		}
	}
	
	private void updateCheckOutRoomStatusAndFrontdeskStatus(Long roomId, String frontdeskStatus){
		Room room = roomDao.get(Room.class, roomId);
		if (null != room){
			room.setStatus(RoomStatus.D.name());
			room.setUpdateDate(new Date());
			room.setFrontdeskStatus(frontdeskStatus);
			roomDao.update(room);
		}
	}

	@Override
	@Transactional
	public HTNGProfileReadRS membership(HTNGReadRQ request) {
		Assert.notNull(request);
		Assert.notNull(request.getReadRequests());
		Assert.notNull(request.getReadRequests().getProfileReadRequest());
		Assert.notNull(request.getReadRequests().getProfileReadRequest().getCustomer());
		Assert.notNull(request.getReadRequests().getProfileReadRequest().getCustomer().getCustLoyalty());
		Assert.notNull(request.getReadRequests().getProfileReadRequest().getCustomer().getCustLoyalty().getMembershipID());
		String membershipID = request.getReadRequests().getProfileReadRequest().getCustomer().getCustLoyalty().getMembershipID();
		String programId = request.getReadRequests().getProfileReadRequest().getCustomer().getCustLoyalty().getProgramID();

		RequestedComponents requestedComponents = request.getRequestedComponents();
		List<RequestedComponent> requestedComponent = requestedComponents.getRequestedComponent();

		HTNGProfileReadRS HTNGProfileReadRS = new HTNGProfileReadRS();
		List<com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error> error = new ArrayList<com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error>();

		String academyNo = null;
		Member member = null;

		if (membershipID.trim().length() == 7) {
			// membershipID length = 7 : academic no.
			academyNo = membershipID;
			member = memberDao.getUniqueByCol(Member.class, "academyNo", academyNo);
		} else if(membershipID.trim().length() == 9){
			// membershipID length = 9 : card no.
			membershipID = membershipID.trim();
			String cardNoStr = membershipID.substring(0, membershipID.length() - 1);// reduce check digit
			try {
				// int cardNo = Integer.parseInt(cardNoStr);
				PermitCardMaster permitCardMaster = permitCardMasterDao.get(PermitCardMaster.class, cardNoStr);
				if (null != permitCardMaster) {
					Long customerId = permitCardMaster.getMappingCustomerId();
					if (null == customerId) {
						com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error error1 = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error();
						error1.setCode(BigInteger.valueOf(425));
						error1.setType(BigInteger.valueOf(3));
						error1.setContent("No matched customer data for this card.");
						error.add(error1);
						logger.debug("query member failed,the Card No haven't matched customer.");
					} else {
						member = memberDao.get(Member.class, customerId);
					}

				} else {
					com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error error1 = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error();
					error1.setCode(BigInteger.valueOf(425));
					error1.setType(BigInteger.valueOf(3));
					error1.setContent("No match found");
					error.add(error1);
					logger.debug("query member failed,the Card No is not existed.");
				}
			} catch (Exception e) {
				logger.error("cardNo parseInt error ; cardNo : " + cardNoStr);
				member = null;
			}
		}else{
			com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error error1 = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error();
			error1.setCode(BigInteger.valueOf(425));
			error1.setType(BigInteger.valueOf(3));
			error1.setContent("error: Please input 7 digits Patron ID / 9 digits Academy Card Number");
			error.add(error1);
			logger.debug("error: Please input 7 digits Patron ID / 9 digits Academy Card Number");
		}

		if (!error.isEmpty()) {
			com.sinodynamic.hkgta.dto.pms.xml.output.membership.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Errors();
			errors.setError(error);
			HTNGProfileReadRS.setErrors(errors);
			return HTNGProfileReadRS;
		}

		if (null == member) {
			com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error error1 = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error();
			error1.setCode(BigInteger.valueOf(425));
			error1.setType(BigInteger.valueOf(3));
			error1.setContent("No match found");
			error.add(error1);
			logger.debug("query member failed,the member is not existed.");

		} else {
			Date expiredDate = memberDao.getExpiredDate(member.getCustomerId().toString());
			if (expiredDate != null && expiredDate.before(new Date())) {
				com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error error2 = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error();
				error2.setCode(BigInteger.valueOf(277));
				error2.setType(BigInteger.valueOf(3));
				error2.setContent("Expired club membership");
				error.add(error2);
			}

			String status = member.getStatus();
			String act = Constant.Member_Status_ACT;
			if (!act.equalsIgnoreCase(status)) {
				com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error error3 = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error();
				error3.setCode(BigInteger.valueOf(278));
				error3.setType(BigInteger.valueOf(3));
				error3.setContent("Cancelled club membership");
				error.add(error3);
			}

		}

		if (!error.isEmpty()) {
			com.sinodynamic.hkgta.dto.pms.xml.output.membership.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Errors();
			errors.setError(error);
			HTNGProfileReadRS.setErrors(errors);
			return HTNGProfileReadRS;
		}

		// UserMaster user = userMasterDao.get(UserMaster.class, member.getUserId());

		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, member.getCustomerId());

		Success success = new Success();
		Profiles profiles = new Profiles();
		ProfileInfo profileInfo = new ProfileInfo();
		Profile profile = new Profile();
		profile.setProfileType(BigInteger.valueOf(1L));
		Customer customer = new Customer();
		PersonName personName = new PersonName();
		personName.setGivenName(customerProfile.getGivenName());
		personName.setSurname(customerProfile.getSurname());
		personName.setNamePrefix(customerProfile.getSalutation());
		customer.setPersonName(personName);
		CustLoyalty custLoyalty = new CustLoyalty();
		custLoyalty.setMembershipID(membershipID);
		custLoyalty.setProgramID(programId);
		SubAccountBalance subAccountBalance = new SubAccountBalance();
		Long superiorMemberId = member.getSuperiorMemberId();
		
		//primary member
		if (null == superiorMemberId) {
			MemberCashvalue cashValue = member.getMemberCashvalue();
			BigDecimal creditLimit = memberLimitRuleDao.getCreditLimitOfMember(member.getCustomerId());
			if(null == creditLimit){
				creditLimit = new BigDecimal(0);
			}
			if (null != cashValue) {
				BigDecimal availableBalance = cashValue.getAvailableBalance();
				if (null == availableBalance) {
					availableBalance = new BigDecimal(0);
				}
				availableBalance = availableBalance.add(creditLimit);
				subAccountBalance.setBalance(availableBalance);
			} else {
				subAccountBalance.setBalance(creditLimit);
			}
		//dependent member
		} else {
			Long customerId = superiorMemberId;
			Member parentMember = memberDao.get(Member.class, customerId);
			MemberCashvalue cashValue = parentMember.getMemberCashvalue();
			BigDecimal creditLimit = memberLimitRuleDao.getCreditLimitOfMember(customerId);
			if(null == creditLimit){
				creditLimit = new BigDecimal(0);
			}
			BigDecimal spendingLimit = memberLimitRuleDao.getTransactionLimitOfMember(member.getCustomerId());
			if(null == spendingLimit){
				spendingLimit = new BigDecimal(0);
			}
			if (null != cashValue) {
				BigDecimal availableBalance = cashValue.getAvailableBalance();
				if (null == availableBalance) {
					availableBalance = new BigDecimal(0);
				}
				availableBalance = availableBalance.add(creditLimit);
				if(spendingLimit.compareTo(availableBalance) > 0){
					subAccountBalance.setBalance(availableBalance);
				}else{
					subAccountBalance.setBalance(spendingLimit);
				}
			} else {
				if(spendingLimit.compareTo(creditLimit) > 0){
					subAccountBalance.setBalance(creditLimit);
				}else{
					subAccountBalance.setBalance(spendingLimit);
				}
			}
		}

		custLoyalty.setSubAccountBalance(subAccountBalance);

		customer.setCustLoyalty(custLoyalty);

		/*
		 * boolean needEmail = false; boolean needAddress = false; boolean needTel = false;
		 */

		for (RequestedComponent temp : requestedComponent) {
			String name = temp.getName();
			if ("CustomerAddress".equalsIgnoreCase(name)) {
				// needAddress = true;
				Address homeAddress = new Address();
				homeAddress.setType(BigInteger.valueOf(1));
				homeAddress.setFormattedInd(true);
				List<String> addressLine = new ArrayList<String>();

				if (StringUtils.isNotBlank(customerProfile.getPostalAddress1())) {
					addressLine.add(customerProfile.getPostalAddress1());
				}
				if (StringUtils.isNotBlank(customerProfile.getPostalAddress2())) {
					addressLine.add(customerProfile.getPostalAddress2());
				}
				if (!addressLine.isEmpty()) {
					homeAddress.setAddressLine(addressLine);
				}

				if (StringUtils.isNotBlank(customerProfile.getPostalDistrict())) {
					homeAddress.setCityName(customerProfile.getPostalDistrict());
				}

				CountryName countryName = new CountryName();
				countryName.setCode("HK");// 固定使用HK
				homeAddress.setCountryName(countryName);

				customer.setAddress(homeAddress);
			} else if ("CustomerEmail".equalsIgnoreCase(name)) {
				// needEmail = true;
				Email mail = new Email();
				mail.setContent(customerProfile.getContactEmail());
				mail.setEmailType(BigInteger.valueOf(1L));
				customer.setEmail(mail);
			} else if ("CustomerTelephone".equalsIgnoreCase(name)) {
				// needTel = true;
				List<Telephone> telephone = new ArrayList<Telephone>();

				boolean defaultFlag = false;
				Telephone mobile = new Telephone();
				mobile.setPhoneLocationType(BigInteger.valueOf(5));
				mobile.setPhoneNumber(customerProfile.getPhoneMobile());
				if (StringUtils.isNotBlank(customerProfile.getPhoneMobile())) {
					mobile.setDefaultInd(true);
					defaultFlag = true;// 已经设置了默认联系方式
					telephone.add(mobile);
				}

				Telephone homeTelephone = new Telephone();
				homeTelephone.setPhoneLocationType(BigInteger.valueOf(6));
				homeTelephone.setPhoneNumber(customerProfile.getPhoneHome());
				if (StringUtils.isNotBlank(customerProfile.getPhoneHome())) {
					if (!defaultFlag) {
						homeTelephone.setDefaultInd(true);
						defaultFlag = true;// 已经设置了默认联系方式
					} else {
						homeTelephone.setDefaultInd(false);
					}
					telephone.add(homeTelephone);
				}

				Telephone bussinessPhone = new Telephone();
				bussinessPhone.setPhoneLocationType(BigInteger.valueOf(7));
				bussinessPhone.setPhoneNumber(customerProfile.getPhoneBusiness());
				if (StringUtils.isNotBlank(customerProfile.getPhoneBusiness())) {
					if (!defaultFlag) {
						bussinessPhone.setDefaultInd(true);
					}
					telephone.add(bussinessPhone);
				}
				customer.setTelephone(telephone);
			}
		}

		profile.setCustomer(customer);
		profileInfo.setProfile(profile);
		profiles.setProfileInfo(profileInfo);
		HTNGProfileReadRS.setSuccess(success);
		HTNGProfileReadRS.setProfiles(profiles);
		HTNGProfileReadRS.setEchoToken(request.getEchoToken());
		return HTNGProfileReadRS;
	}

	@Deprecated
	public Map<Date, Long> giftBundleByPeriod(Long customerId, Date arriveDate, Date departDate, String facilityType, String attributeId,
			String status) {
		Map<Date, Long> startDateTimeslotMap = new HashMap<Date, Long>();
		Date bookingDate = arriveDate;// which day to booking
		long nights = departDate.getTime() / (24 * 60 * 60 * 1000) - arriveDate.getTime() / (24 * 60 * 60 * 1000);
		boolean oneDayNotFound = false;// whether there is one day haven't found available timeslot
		int dayIndex = 0;// booking day index
		while (startDateTimeslotMap.size() < nights && bookingDate.compareTo(departDate) <= 0) {
			// System.out.println("count0=" + count++);
			Date bookingMaxDateTime = DateUtils.setHours(bookingDate, (latestTimeHour + 1));
			Date bookingMinDateTime = DateUtils.setHours(bookingDate, earlestTimeHour);
			Date bookingStartTime = null;
			Date bookingEndTime = null;
			// If no day haven't found time-slot,then search like 16:00->23:00,16:00->7:00
			if (!oneDayNotFound) {
				logger.debug("book bundle for the " + (dayIndex + 1) + " day start:");
				bookingStartTime = DateUtils.setHours(bookingDate, (officalCheckInHour + 1));
				if (bookingDate.after(departDate)) {
					logger.debug("The time over the guest depart time,stop booking golf-bay for him.");
					return startDateTimeslotMap;
				} else {
					bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
					Long timeslotId = null;

					try {
						logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
						timeslotId = bookingFacilityService.bookTimeslot(
								customerId,
								bookingStartTime,
								bookingEndTime,
								facilityType,
								attributeId,
								status);
						if (timeslotId != null) {
							startDateTimeslotMap.put(bookingDate, timeslotId);
							bookingDate = DateUtils.addDays(bookingDate, 1);
							continue;
						}
						logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
					} catch (NoTimeslotException noTimeException) {
						logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
					}
					// search golf-bay from startDateTime to currentLatestDateTime, eg: from 16:00 - 23:00
					if (timeslotId == null) {
						bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
						bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
					}
					while (null == timeslotId && (!bookingEndTime.after(bookingMaxDateTime)) || bookingEndTime.equals(bookingMaxDateTime)) {
						// System.out.println("count1=" + count++);
						System.out.println("debug1");
						try {
							logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
							timeslotId = bookingFacilityService.bookTimeslot(
									customerId,
									bookingStartTime,
									bookingEndTime,
									facilityType,
									attributeId,
									status);
							logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
							if (timeslotId != null) {
								startDateTimeslotMap.put(bookingDate, timeslotId);
								bookingDate = DateUtils.addDays(bookingDate, 1);
								break;
							}
						} catch (NoTimeslotException noTimeException) {
							logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
						}
						if (timeslotId == null) {
							bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
							bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
						}
					}
					if (null == timeslotId) {
						// if search golf-bay from startDateTime to currentLatestDateTime failed, begin to search from startDateTime to
						// currentEarlestDateTime
						// eg: from 16:00 -> 7:00
						bookingStartTime = DateUtils.setHours(bookingDate, officalCheckInHour);
						bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
						try {
							logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
							timeslotId = bookingFacilityService.bookTimeslot(
									customerId,
									bookingStartTime,
									bookingEndTime,
									facilityType,
									attributeId,
									status);
							if (timeslotId != null) {
								startDateTimeslotMap.put(bookingDate, timeslotId);
								bookingDate = DateUtils.addDays(bookingDate, 1);
								continue;
							}
							logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
						} catch (NoTimeslotException noTimeException) {
							logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
						}
						if (timeslotId == null) {
							bookingStartTime = DateUtils.addHours(bookingStartTime, -1);
							bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
						}
						while (null == timeslotId && !bookingStartTime.before(bookingMinDateTime)) {
							// System.out.println("count2=" + count++);
							System.out.println("debug2");
							try {
								logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
								timeslotId = bookingFacilityService.bookTimeslot(
										customerId,
										bookingStartTime,
										bookingEndTime,
										facilityType,
										attributeId,
										status);
								logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
								if (timeslotId != null) {
									startDateTimeslotMap.put(bookingDate, timeslotId);
									bookingDate = DateUtils.addDays(bookingDate, 1);
									break;
								}
							} catch (NoTimeslotException noTimeException) {
								timeslotId = null;
								logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
							}
							if (timeslotId == null) {
								bookingStartTime = DateUtils.addHours(bookingStartTime, -1);
								bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
							}
						}
					}

					// add the reservId into List
					if (null == timeslotId) {
						oneDayNotFound = true;
						bookingDate = DateUtils.addDays(bookingDate, 1);
						logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");
					}
				}
			} else {
				// search from the next day 7:00 -> 23:00
				// bookingStartDate = DateUtils.addDays(bookingStartDate, 1);
				bookingStartTime = DateUtils.setHours(bookingDate, earlestTimeHour);
				bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
				Long timeslotId = null;
				try {
					timeslotId = bookingFacilityService.bookTimeslot(customerId, bookingStartTime, bookingEndTime, facilityType, attributeId, status);
					if (timeslotId != null) {
						startDateTimeslotMap.put(bookingDate, timeslotId);
						bookingDate = DateUtils.addDays(bookingDate, 1);
						continue;
					}
				} catch (NoTimeslotException noTimeException) {
					// reservId = null;
					logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
				}
				if (timeslotId == null) {
					bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
					bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
				}
				while (null == timeslotId && (!bookingEndTime.after(bookingMaxDateTime))) {
					// System.out.println("count3=" + count++);
					System.out.println("debug3");
					try {
						logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
						System.out.println("start booking " + bookingStartTime + " to " + bookingEndTime + ",bookingMaxDateTime="
								+ bookingMaxDateTime);
						timeslotId = bookingFacilityService.bookTimeslot(
								customerId,
								bookingStartTime,
								bookingEndTime,
								facilityType,
								attributeId,
								status);
						logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
						if (timeslotId != null) {
							logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");
							startDateTimeslotMap.put(bookingDate, timeslotId);
							bookingDate = DateUtils.addDays(bookingDate, 1);
							break;
						}
					} catch (NoTimeslotException noTimeException) {
						timeslotId = null;
						logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
					}
					if (timeslotId == null) {
						bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
						bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
					}

					if (bookingEndTime.after(bookingMaxDateTime)) {
						System.out.println("over");
					}

				}
				if (null == timeslotId) {
					oneDayNotFound = true;
					bookingDate = DateUtils.addDays(bookingDate, 1);
				}
				logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");
			}

		}

		return startDateTimeslotMap;

	}

	@Override
	@Transactional
	public OTAGolfCourseAvailRS queryBundle(OTAGolfCourseAvailRQ request) throws Exception {
		OTAGolfCourseAvailRS response = new OTAGolfCourseAvailRS();
		Assert.notNull(request);
		Assert.notNull(request.getGolfFacility());
		Assert.notNull(request.getGolfFacility().getID());
		Assert.notNull(request.getGolfFacility().getTeeTime());

		String comfirmId = request.getGolfFacility().getID();
		String arriveDateStr = request.getGolfFacility().getTeeTime().getStart();
		String departDateStr = request.getGolfFacility().getTeeTime().getEnd();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date arriveDate = sdf.parse(arriveDateStr);
		Date departDate = sdf.parse(departDateStr);

		com.sinodynamic.hkgta.dto.pms.xml.output.facility.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.Errors();

		if (arriveDate.after(departDate)) {
			com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error();
			error.setCode(BigInteger.valueOf(404));
			error.setType(BigInteger.valueOf(3));
			error.setContent("Invalid start/end date combination");
			List<com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error> errorList = new ArrayList();
			errorList.add(error);
			errors.setError(errorList);
			response.setErrors(errors);
			return response;
		}

		RoomReservationRec roomReservationRec;
		roomReservationRec = roomReservationRecDao.getUniqueByCol(RoomReservationRec.class, "confirmId", comfirmId);

		Long facilityTypeResvId = 0l;// roomReservationRec.getFacilityTypeResvId();//default value , this method would not be called
		if (null == facilityTypeResvId) {
			com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error();
			error.setCode(BigInteger.valueOf(425));
			error.setType(BigInteger.valueOf(3));
			error.setContent("No match found");
			List<com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error> errorList = new ArrayList();
			errorList.add(error);
			errors.setError(errorList);
			response.setErrors(errors);
			return response;
		}

		MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao.get(MemberFacilityTypeBooking.class, facilityTypeResvId);
		if (null == memberFacilityTypeBooking) {
			com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error();
			error.setCode(BigInteger.valueOf(425));
			error.setType(BigInteger.valueOf(3));
			error.setContent("No match found");
			List<com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error> errorList = new ArrayList();
			errorList.add(error);
			errors.setError(errorList);
			response.setErrors(errors);
			return response;
		}

		List<FacilityTimeslot> timeslots = memberFacilityTypeBooking.getFacilityTimeslots();
		if (timeslots.isEmpty()) {
			com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error();
			error.setCode(BigInteger.valueOf(425));
			error.setType(BigInteger.valueOf(3));
			error.setContent("No match found");
			List<com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error> errorList = new ArrayList();
			errorList.add(error);
			errors.setError(errorList);
			response.setErrors(errors);
			return response;
		}

		com.sinodynamic.hkgta.dto.pms.xml.output.facility.GolfFacility golfFacility = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.GolfFacility();
		FacilityInfo facilityInfo = new FacilityInfo();
		com.sinodynamic.hkgta.dto.pms.xml.output.facility.UniqueID uniqueId = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.UniqueID();
		uniqueId.setType(BigInteger.valueOf(7));
		uniqueId.setID(BigInteger.valueOf(Integer.valueOf(comfirmId)));
		uniqueId.setAssociatedFacility("HKGTA");
		facilityInfo.setUniqueID(uniqueId);
		TeeTimes teeTimes = new TeeTimes();
		for (FacilityTimeslot timeslot : timeslots) {
			com.sinodynamic.hkgta.dto.pms.xml.output.facility.TeeTime teeTime = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.TeeTime();
			teeTime.setStart(sdf2.format(timeslot.getBeginDatetime()));
			Amenity amenity = new Amenity();
			amenity.setAmenityCode("GOLF driving bay");
			amenity.setDescription("GOLF,begin time:" + sdf2.format(timeslot.getBeginDatetime()));
			amenity.setID(timeslot.getFacilityMaster().getFacilityNo() + "");
			teeTime.setAmenity(amenity);
			List<com.sinodynamic.hkgta.dto.pms.xml.output.facility.TeeTime> teeTimeList = new ArrayList();
			teeTimeList.add(teeTime);
			teeTimes.setTeeTime(teeTimeList);
		}
		golfFacility.setFacilityInfo(facilityInfo);
		golfFacility.setTeeTimes(teeTimes);
		response.setGolfFacility(golfFacility);
		com.sinodynamic.hkgta.dto.pms.xml.output.facility.Success succ = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.Success();
		response.setSuccess(succ);
		return response;
	}

	@Override
	@Transactional
	public synchronized Map<String, Long> giftBundleForReservations(Long customerId, List<RoomReservationInfoDto> reservations) {
		// DistributedLock lock = null;
		try {
			// lock = new DistributedLock(zookeeperConnectionString, "giftBundleForReservations");
			// lock.lock();
			Map<String, Long> golfReservationMap = new HashMap<String, Long>();
			for (RoomReservationInfoDto reservation : reservations) {
				Long resvId = giftBundleForOneRoom(customerId, reservation);
				golfReservationMap.put(reservation.getReservationId(), resvId);
			}
			return golfReservationMap;
		} finally {
			// lock.unlock();
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public synchronized Long giftBundleForOneRoom(Long customerId, RoomReservationInfoDto reservation) {
		// DistributedLock lock = null;
		try {
			// lock = new DistributedLock(zookeeperConnectionString, "GOLFBAYTYPE-RH");
			// lock.lock();
			Date arriveDate = reservation.getArriveDate();
			Date departDate = reservation.getDepartDate();
			Date bookingDate = arriveDate;// which day to booking
			long nights = departDate.getTime() / (24 * 60 * 60 * 1000) - arriveDate.getTime() / (24 * 60 * 60 * 1000);
			boolean oneDayNotFound = false;// whether there is one day haven't found available timeslot
			int dayIndex = 0;// booking day index
			List<Long> timeslotIds = new ArrayList<Long>();
			while (timeslotIds.size() < nights && bookingDate.compareTo(departDate) <= 0) {
				// System.out.println("count0=" + count++);
				Date bookingMaxDateTime = DateUtils.setHours(bookingDate, (latestTimeHour + 1));
				Date bookingMinDateTime = DateUtils.setHours(bookingDate, earlestTimeHour);
				Date bookingStartTime = null;
				Date bookingEndTime = null;
				// If no day haven't found time-slot,then search like 16:00->23:00,16:00->7:00
				if (!oneDayNotFound) {
					logger.debug("book bundle for the " + (dayIndex + 1) + " day start:");
					bookingStartTime = DateUtils.setHours(bookingDate, (officalCheckInHour + 1));
					if (bookingDate.after(departDate)) {
						logger.debug("The time over the guest depart time,stop booking golf-bay for him.");
						break;
					} else {
						bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
						Long timeslotId = null;

						try {
							logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
							timeslotId = bookingFacilityService.bookTimeslot(
									customerId,
									bookingStartTime,
									bookingEndTime,
									facilityType,
									attributeId,
									status);
							if (timeslotId != null) {
								timeslotIds.add(timeslotId);
								bookingDate = DateUtils.addDays(bookingDate, 1);
								continue;
							}
							logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
						} catch (NoTimeslotException noTimeException) {
							logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
						}
						// search golf-bay from startDateTime to currentLatestDateTime, eg: from 16:00 - 23:00
						if (timeslotId == null) {
							bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
							bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
						}
						while (null == timeslotId && (!bookingEndTime.after(bookingMaxDateTime)) || bookingEndTime.equals(bookingMaxDateTime)) {
							// System.out.println("count1=" + count++);
							System.out.println("debug1");
							try {
								logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
								timeslotId = bookingFacilityService.bookTimeslot(
										customerId,
										bookingStartTime,
										bookingEndTime,
										facilityType,
										attributeId,
										status);
								logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
								if (timeslotId != null) {
									timeslotIds.add(timeslotId);
									bookingDate = DateUtils.addDays(bookingDate, 1);
									break;
								}
							} catch (NoTimeslotException noTimeException) {
								logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
							}
							if (timeslotId == null) {
								bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
								bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
							}
						}
						if (null == timeslotId) {
							// if search golf-bay from startDateTime to currentLatestDateTime failed, begin to search from startDateTime to
							// currentEarlestDateTime
							// eg: from 16:00 -> 7:00
							bookingStartTime = DateUtils.setHours(bookingDate, officalCheckInHour);
							bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
							try {
								logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
								timeslotId = bookingFacilityService.bookTimeslot(
										customerId,
										bookingStartTime,
										bookingEndTime,
										facilityType,
										attributeId,
										status);
								if (timeslotId != null) {
									timeslotIds.add(timeslotId);
									bookingDate = DateUtils.addDays(bookingDate, 1);
									continue;
								}
								logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
							} catch (NoTimeslotException noTimeException) {
								logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
							}
							if (timeslotId == null) {
								bookingStartTime = DateUtils.addHours(bookingStartTime, -1);
								bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
							}
							while (null == timeslotId && !bookingStartTime.before(bookingMinDateTime)) {
								// System.out.println("count2=" + count++);
								System.out.println("debug2");
								try {
									logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
									timeslotId = bookingFacilityService.bookTimeslot(
											customerId,
											bookingStartTime,
											bookingEndTime,
											facilityType,
											attributeId,
											status);
									logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
									if (timeslotId != null) {
										timeslotIds.add(timeslotId);
										bookingDate = DateUtils.addDays(bookingDate, 1);
										break;
									}
								} catch (NoTimeslotException noTimeException) {
									timeslotId = null;
									logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
								}
								if (timeslotId == null) {
									bookingStartTime = DateUtils.addHours(bookingStartTime, -1);
									bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
								}
							}
						}

						// add the reservId into List
						if (null == timeslotId) {
							oneDayNotFound = true;
							bookingDate = DateUtils.addDays(bookingDate, 1);
							logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");
						}
					}
				} else {
					// search from the next day 7:00 -> 23:00
					// bookingStartDate = DateUtils.addDays(bookingStartDate, 1);
					bookingStartTime = DateUtils.setHours(bookingDate, earlestTimeHour);
					bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
					Long timeslotId = null;
					try {
						timeslotId = bookingFacilityService.bookTimeslot(
								customerId,
								bookingStartTime,
								bookingEndTime,
								facilityType,
								attributeId,
								status);
						if (timeslotId != null) {
							timeslotIds.add(timeslotId);
							bookingDate = DateUtils.addDays(bookingDate, 1);
							continue;
						}
					} catch (NoTimeslotException noTimeException) {
						// reservId = null;
						logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
					}
					if (timeslotId == null) {
						bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
						bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
					}
					while (null == timeslotId && (!bookingEndTime.after(bookingMaxDateTime))) {
						// System.out.println("count3=" + count++);
						System.out.println("debug3");
						try {
							logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
							System.out.println("start booking " + bookingStartTime + " to " + bookingEndTime + ",bookingMaxDateTime="
									+ bookingMaxDateTime);
							timeslotId = bookingFacilityService.bookTimeslot(
									customerId,
									bookingStartTime,
									bookingEndTime,
									facilityType,
									attributeId,
									status);
							logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
							if (timeslotId != null) {
								logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");
								timeslotIds.add(timeslotId);
								bookingDate = DateUtils.addDays(bookingDate, 1);
								break;
							}
						} catch (NoTimeslotException noTimeException) {
							timeslotId = null;
							logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
						}
						if (timeslotId == null) {
							bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
							bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
						}

						if (bookingEndTime.after(bookingMaxDateTime)) {
							System.out.println("over");
						}

					}
					if (null == timeslotId) {
						oneDayNotFound = true;
						bookingDate = DateUtils.addDays(bookingDate, 1);
					}
					logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");
				}

			}
			if (!timeslotIds.isEmpty()) {
				MemberFacilityTypeBooking memberFacilityTypeBooking = new MemberFacilityTypeBooking();
				// memberFacilityTypeBooking.setBeginDatetimeBook(new
				// Timestamp(startTime.getTime()));
				// memberFacilityTypeBooking.setEndDatetimeBook(new
				// Timestamp(endTime.getTime()));
				memberFacilityTypeBooking.setCustomerId(customerId);
				memberFacilityTypeBooking.setResvFacilityType("GOLF");
				memberFacilityTypeBooking.setFacilityTypeQty(1L);
				memberFacilityTypeBooking.setStatus("RSV");
				memberFacilityTypeBooking.setReserveVia("PMS");
				memberFacilityTypeBooking.setCreateDate(new Timestamp(new Date().getTime()));
				memberFacilityTypeBooking.setUpdateDate(new Timestamp(new Date().getTime()));

				Long resvId = (Long) memberFacilityTypeBookingDao.save(memberFacilityTypeBooking);

				for (Long timesslotId : timeslotIds) {
					MemberReservedFacility memberReservedFacility = new MemberReservedFacility();
					memberReservedFacility.setFacilityTimeslotId(timesslotId);
					memberReservedFacility.setResvId(resvId);
					memberReservedFacilityDao.save(memberReservedFacility);

				}

				MemberFacilityBookAdditionAttr memberFacilityBookAdditionAttr = new MemberFacilityBookAdditionAttr();
				MemberFacilityBookAdditionAttrPK pk = new MemberFacilityBookAdditionAttrPK();
				pk.setResvId(resvId);
				pk.setAttributeId(attributeId);
				memberFacilityBookAdditionAttr.setAttrValue(attributeId);
				memberFacilityBookAdditionAttr.setFacilityType(facilityType);
				memberFacilityBookAdditionAttr.setId(pk);
				memberFacilityBookAdditionAttrDao.save(memberFacilityBookAdditionAttr);

				CustomerOrderHd customerOrderHd = new CustomerOrderHd();
				customerOrderHd.setOrderDate(new Date());
				customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
				// customerOrderHd.setStaffUserId(staffUserId);
				customerOrderHd.setCustomerId(customerId);
				customerOrderHd.setOrderTotalAmount(new BigDecimal(0));
				customerOrderHd.setOrderRemark("");
				customerOrderHd.setCreateDate(new Timestamp(new Date().getTime()));
				// customerOrderHd.setCreateBy(staffUserId);
				// customerOrderHd.setUpdateBy(staffUserId);
				customerOrderHd.setUpdateDate(new Date());
				Long orderNo = (Long) customerOrderHdDao.addCustomreOrderHd(customerOrderHd);

				// CUSTOMER ORDER Detail
				CustomerOrderDet customerOrderDet = new CustomerOrderDet();
				customerOrderDet.setCustomerOrderHd(customerOrderHd);
				customerOrderDet.setItemNo("FMS00000004");
				customerOrderDet.setItemRemark("");
				customerOrderDet.setOrderQty(1L);
				customerOrderDet.setItemTotalAmout(new BigDecimal(0));
				customerOrderDet.setCreateDate(new Timestamp(new Date().getTime()));
				// customerOrderDet.setCreateBy(staffUserId);
				customerOrderDet.setUpdateDate(new Date());
				// customerOrderDet.setUpdateBy(staffUserId);
				customerOrderDetDao.saveOrderDet(customerOrderDet);

				// CUSTOMER ORDER TRANS
				CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
				initCustomerOrderTrans(customerOrderTrans);
				
				customerOrderTrans.setCustomerOrderHd(customerOrderHd);
				customerOrderTrans.setTransactionTimestamp(new Date());
				customerOrderTrans.setPaidAmount(new BigDecimal(0));// it's ok ,
																	// because
																	// only has
																	// one txn
				customerOrderTrans.setStatus(Constant.Status.PND.name());
				// customerOrderTrans.setPaymentRecvBy(staffUserId);
				// customerOrderTrans.setAgentTransactionNo(agentTransactionNo);
				// customerOrderTrans.setTerminalId(terminalId);
				customerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
				customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
				customerOrderTransDao.saveCustomerOrderTrans(customerOrderTrans);
				return resvId;
			} else {
				return null;
			}

		} finally {
			// lock.unlock();
		}
	}

	@Override
	public synchronized List<Long> giftBundleForOneRoomOneNightOneBooking(Long customerId, RoomReservationInfoDto reservation) {
		return giftBundleForOneRoomOneNightOneBooking(Constant.PaymentMethodCode.CASHVALUE.toString(), customerId, reservation);

	}

	@Override
	public synchronized List<Long> giftBundleForOneRoomOneNightOneBooking(String paymentMethod, Long customerId, RoomReservationInfoDto reservation) {
		// DistributedLock lock = null;
		List<Long> reservationIDs = new ArrayList<Long>();
		try {
			// lock = new DistributedLock(zookeeperConnectionString,"GOLFBAYTYPE-RH");
			// lock.lock();
			Date arriveDate = reservation.getArriveDate();
			Date departDate = reservation.getDepartDate();
			Date bookingDate = arriveDate;
			long nights = departDate.getTime() / (24 * 60 * 60 * 1000) - arriveDate.getTime() / (24 * 60 * 60 * 1000);
			boolean oneDayNotFound = false;// whether there is one day haven't
											// found available timeslot
			int dayIndex = 0;// booking day index
			// if booking date and arrival date is same the bookingStartTime should be the next day
			boolean arriveDateSameASBookingDate = false;
			Calendar calendar = Calendar.getInstance();
			Date today = new Date();
			if (DateUtils.isSameDay(today, arriveDate)||StringUtils.isNotEmpty(reservation.getEarlestBay())) {
				arriveDateSameASBookingDate = true;
			}
			while (reservationIDs.size() < nights && bookingDate.compareTo(departDate) <= 0) {
				// System.out.println("count0=" + count++);
				Date bookingMaxDateTime = DateUtils.setHours(bookingDate, (latestTimeHour + 1));
				Date bookingMinDateTime = DateUtils.setHours(bookingDate, earlestTimeHour);
				Date bookingStartTime = null;
				Date bookingEndTime = null;
				// If no day haven't found time-slot,then search like
				// 16:00->23:00,16:00->7:00
				if (!oneDayNotFound && !arriveDateSameASBookingDate) {
					logger.debug("book bundle for the " + (dayIndex + 1) + " day start:");
					bookingStartTime = DateUtils.setHours(bookingDate, (officalCheckInHour + 1));
					if (bookingDate.after(departDate)) {
						logger.debug("The time over the guest depart time,stop booking golf-bay for him.");
						break;
					} else {
						Long resvId = null;

						try {
							logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
							calendar.setTime(bookingStartTime);
							calendar.set(Calendar.MINUTE, 0);
							calendar.set(Calendar.SECOND, 0);
							bookingEndTime = DateUtils.addHours(calendar.getTime(), 1);
							resvId = bookingFacilityService.bookFacility(
									paymentMethod,
									customerId,
									calendar.getTime(),
									DateUtils.addSeconds(bookingEndTime, -1),
									facilityType,
									attributeId,
									status);
							if (resvId != null) {
								reservationIDs.add(resvId);
								bookingDate = DateUtils.addDays(bookingDate, 1);
								continue;
							}
							logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
						} catch (NoTimeslotException noTimeException) {
							logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
						}
						// search golf-bay from startDateTime to
						// currentLatestDateTime, eg: from 16:00 - 23:00
						if (resvId == null) {
							bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
							bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
						}
						while (null == resvId && (!bookingEndTime.after(bookingMaxDateTime)) || bookingEndTime.equals(bookingMaxDateTime)) {
							// System.out.println("count1=" + count++);
							System.out.println("debug1");
							try {
								logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
								calendar.setTime(bookingStartTime);
								calendar.set(Calendar.MINUTE, 0);
								calendar.set(Calendar.SECOND, 0);
								bookingEndTime = DateUtils.addHours(calendar.getTime(), 1);
								resvId = bookingFacilityService.bookFacility(
										paymentMethod,
										customerId,
										calendar.getTime(),
										DateUtils.addSeconds(bookingEndTime, -1),
										facilityType,
										attributeId,
										status);
								logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
								if (resvId != null) {
									reservationIDs.add(resvId);
									bookingDate = DateUtils.addDays(bookingDate, 1);
									break;
								}
							} catch (NoTimeslotException noTimeException) {
								logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
							}
							if (resvId == null) {
								bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
								bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
							}
						}
						if (null == resvId) {
							// if search golf-bay from startDateTime to
							// currentLatestDateTime failed, begin to search
							// from startDateTime to currentEarlestDateTime
							// eg: from 16:00 -> 7:00
							bookingStartTime = DateUtils.setHours(bookingDate, officalCheckInHour);
							try {
								logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
								calendar.setTime(bookingStartTime);
								calendar.set(Calendar.MINUTE, 0);
								calendar.set(Calendar.SECOND, 0);
								bookingEndTime = DateUtils.addHours(calendar.getTime(), 1);
								resvId = bookingFacilityService.bookFacility(
										paymentMethod,
										customerId,
										calendar.getTime(),
										DateUtils.addSeconds(bookingEndTime, -1),
										facilityType,
										attributeId,
										status);
								if (resvId != null) {
									reservationIDs.add(resvId);
									bookingDate = DateUtils.addDays(bookingDate, 1);
									continue;
								}
								logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
							} catch (NoTimeslotException noTimeException) {
								logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
							}
							if (resvId == null) {
								bookingStartTime = DateUtils.addHours(bookingStartTime, -1);
								bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
							}
							while (null == resvId && !bookingStartTime.before(bookingMinDateTime)) {
								// System.out.println("count2=" + count++);
								System.out.println("debug2");
								try {
									logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
									calendar.setTime(bookingStartTime);
									calendar.set(Calendar.MINUTE, 0);
									calendar.set(Calendar.SECOND, 0);
									bookingEndTime = DateUtils.addHours(calendar.getTime(), 1);
									resvId = bookingFacilityService.bookFacility(
											paymentMethod,
											customerId,
											calendar.getTime(),
											DateUtils.addSeconds(bookingEndTime, -1),
											facilityType,
											attributeId,
											status);
									logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
									if (resvId != null) {
										reservationIDs.add(resvId);
										bookingDate = DateUtils.addDays(bookingDate, 1);
										break;
									}
								} catch (NoTimeslotException noTimeException) {
									resvId = null;
									logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
								}
								if (resvId == null) {
									bookingStartTime = DateUtils.addHours(bookingStartTime, -1);
									bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
								}
							}
						}

						// add the reservId into List
						if (null == resvId) {
							oneDayNotFound = true;
							bookingDate = DateUtils.addDays(bookingDate, 1);
							logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");
						}
					}
				} else {
					// search from the next day 7:00 -> 23:00
					// bookingStartDate = DateUtils.addDays(bookingStartDate,
					// 1);
					if (arriveDateSameASBookingDate && bookingDate.equals(arriveDate)) {
						bookingDate = DateUtils.addDays(arriveDate, 1);
						bookingMaxDateTime=DateUtils.setHours(bookingDate, (latestTimeHour + 1));
					}
					bookingStartTime = DateUtils.setHours(bookingDate, earlestTimeHour);
					Long resvId = null;
					try {
						calendar.setTime(bookingStartTime);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						bookingEndTime = DateUtils.addHours(calendar.getTime(), 1);
						resvId = bookingFacilityService.bookFacility(
								paymentMethod,
								customerId,
								calendar.getTime(),
								DateUtils.addSeconds(bookingEndTime, -1),
								facilityType,
								attributeId,
								status);
						if (resvId != null) {
							reservationIDs.add(resvId);
							bookingDate = DateUtils.addDays(bookingDate, 1);
							continue;
						}
					} catch (NoTimeslotException noTimeException) {
						// reservId = null;
						logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
					}
					if (resvId == null) {
						bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
						bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
					}
					while (null == resvId && (!bookingEndTime.after(bookingMaxDateTime))) {
						// System.out.println("count3=" + count++);
						System.out.println("debug3");
						try {
							logger.debug("start booking " + bookingStartTime + " to " + bookingEndTime);
							System.out.println("start booking " + bookingStartTime + " to " + bookingEndTime + ",bookingMaxDateTime="
									+ bookingMaxDateTime);
							calendar.setTime(bookingStartTime);
							calendar.set(Calendar.MINUTE, 0);
							calendar.set(Calendar.SECOND, 0);
							bookingEndTime = DateUtils.addHours(calendar.getTime(), 1);
							resvId = bookingFacilityService.bookFacility(
									paymentMethod,
									customerId,
									calendar.getTime(),
									DateUtils.addSeconds(bookingEndTime, -1),
									facilityType,
									attributeId,
									status);
							logger.debug("end booking " + bookingStartTime + " to " + bookingEndTime);
							if (resvId != null) {
								logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");
								reservationIDs.add(resvId);
								bookingDate = DateUtils.addDays(bookingDate, 1);
								break;
							}
						} catch (NoTimeslotException noTimeException) {
							resvId = null;
							logger.error("No timeslot during:" + bookingStartTime + "-" + bookingEndTime);
						}
						if (resvId == null) {
							bookingStartTime = DateUtils.addHours(bookingStartTime, 1);
							bookingEndTime = DateUtils.addHours(bookingStartTime, 1);
						}

						if (bookingEndTime.after(bookingMaxDateTime)) {
							System.out.println("over");
						}

					}
					if (null == resvId) {
						oneDayNotFound = true;
						bookingDate = DateUtils.addDays(bookingDate, 1);
					}
					logger.debug("book bundle for the" + (dayIndex + 1) + "day end.");

				}
			}

			if (reservationIDs.isEmpty()) {
				return null;
			} else {
				return reservationIDs;
			}

		} finally {
			// lock.unlock();
		}

	}

	@Override
	public synchronized Map<String, List<Long>> giftBundleForReservationsOneNightOneBooking(Long customerId, List<RoomReservationInfoDto> reservations) {
		return giftBundleForReservationsOneNightOneBooking(Constant.PaymentMethodCode.CASHVALUE.toString(), customerId, reservations);
	}

	@Override
	public synchronized Map<String, List<Long>> giftBundleForReservationsOneNightOneBooking(String paymentMethod, Long customerId,
			List<RoomReservationInfoDto> reservations) {
		// DistributedLock lock = null;
		try {
			// lock = new DistributedLock(zookeeperConnectionString, "giftBundleForReservationsOneNightOneBooking");
			// lock.lock();
			Map<String, List<Long>> golfReservationMap = new HashMap<String, List<Long>>();
			for (RoomReservationInfoDto reservation : reservations) {
				List<Long> resvIds = giftBundleForOneRoomOneNightOneBooking(paymentMethod, customerId, reservation);
				golfReservationMap.put(reservation.getReservationId(), resvIds);
			}
			return golfReservationMap;
		} finally {
			// lock.unlock();
		}
	}

	@Override
	@Transactional
	public HTNGChargePostingRS epayment(HTNGChargePostingRQ request) throws Exception {

		HTNGChargePostingRS response = new HTNGChargePostingRS();

		Assert.notNull(request);
		// Assert.notNull(request.getUniqueID());
		Assert.notNull(request.getPosting());
		// Assert.notNull(request.getPosting().getRevenueCenter());
		// Assert.notNull(request.getPosting().getRevenueCenter().getID());
		Assert.notNull(request.getPosting().getRevenueCenter().getDescription());
		Assert.notNull(request.getPosting().getRevenueCenter().getTerminal());
		Assert.notNull(request.getPosting().getRevenueCenter().getTerminal().getID());

		Assert.notNull(request.getPosting().getTransaction());
		Assert.notNull(request.getPosting().getTransaction().getRevenueDetails());

		RoomReservationRec roomReservationRec;
		// String comfirmId = request.getUniqueID().getID();
		// BigInteger roomNumber = request.getPosting().getRevenueCenter().getID();
		String terminalID = request.getPosting().getRevenueCenter().getTerminal().getID();

		RevenueDetails revenueDetails = request.getPosting().getTransaction().getRevenueDetails();

		BigDecimal taxAmount;
		if (null != request.getPosting().getTransaction().getTaxItems()) {
			taxAmount = request.getPosting().getTransaction().getTaxItems().getAmount();
		}

		Assert.notNull(request.getPosting().getTransaction().getTenders());
		Assert.notNull(request.getPosting().getTransaction().getTenders().getRevenueDetail());

		Assert.notNull(request.getPosting().getTransaction().getTenders().getRevenueDetail().getAccount());

		RevenueDetail revenueDetail = request.getPosting().getTransaction().getTenders().getRevenueDetail();
		
		if(revenueDetail.getAmount() != null && revenueDetail.getAmount().compareTo(BigDecimal.ZERO) == -1){
		 /***
		  * Hkgta no handle revenueDetail.getAmount() less 0  ,return  success 
		  */
//			com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error();
//			error.setCode(BigInteger.valueOf(424));
//			error.setType(BigInteger.valueOf(2));
//			error.setContent("Do not allow negative amount");
//			com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors();
//			errors.setError(error);
//			response.setErrors(errors);
			String comfirmId = request.getUniqueID().getID();
			pmsLog.info("payment amount less 0 is : "+revenueDetail.getAmount()+",comfirmId:"+comfirmId);
			if(StringUtils.isNotEmpty(comfirmId))
			{
				RoomReservationRec rec=roomReservationRecService.getRoomReservationRecByConfirmId(comfirmId);
				if(null!=rec)
				{
					Member member=memberDao.getMemberByCustomerId(rec.getCustomerId());
					if(null!=member)
					{
						List<CustomerOrderTrans> trans=customerOrderTransService.getCustomerOrderTransListByOrderNo(Long.valueOf(comfirmId));
						if(null!=trans&&trans.size()>0){
							for (CustomerOrderTrans tran : trans) {
								BigDecimal balance =tran.getPaidAmount().add(revenueDetail.getAmount());
								if(balance.compareTo(BigDecimal.ZERO)==0)
								{
									pmsLog.info("refund cashValue to Member cashValue:amount="+tran.getPaidAmount()+",academyNo:"+member.getAcademyNo());
									//void 
									refundService.refundCashValueOasis(tran.getTransactionNo(), member.getAcademyNo());
								}
							}
						}
					}
				}
			}
			response.setSuccess(new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Success());
			return response;
		}

		Account account = request.getPosting().getTransaction().getTenders().getRevenueDetail().getAccount();

		//account ID is academyNo or cardId 
		String academyNo = account.getID();
		
		/***
		 * SGG-3686
		 * check academyNo is digits 7/cardNo digits is 9
		 */
		Member member =null;
		if(academyNo.length()==7)
		{
			member = memberDao.getUniqueByCol(Member.class, "academyNo", academyNo);
			
		}else if(academyNo.length()==9)
		{
			member=getMemberByCardNo(academyNo);
		}else{
			com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error();
			error.setCode(BigInteger.valueOf(425));
			error.setType(BigInteger.valueOf(3));
			error.setContent("error: Please input 7 digits Patron ID / 9 digits Academy Card Number");
			com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors();
			errors.setError(error);
			response.setErrors(errors);
			return response;
		}
		
		if (null == member) {
			//check cardId
			com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error();
			error.setCode(BigInteger.valueOf(425));
			error.setType(BigInteger.valueOf(3));
			error.setContent("No match found");
			com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors();
			errors.setError(error);
			response.setErrors(errors);
			return response;
		}
		/***
		 * SGG-3835
		 */
		if((Constant.memberType.IDM.toString().equalsIgnoreCase(member.getMemberType()) || Constant.memberType.CDM.toString().equalsIgnoreCase(member.getMemberType())) && 
				(null != member.getSuperiorMemberId() && member.getSuperiorMemberId() >0) )
		{
			MemberLimitRule memberLimitRuleTRN = memberLimitRuleDao.getEffectiveMemberLimitRule(member.getCustomerId(), LimitType.TRN.name());
			if(null != memberLimitRuleTRN){
				BigDecimal numValue = memberLimitRuleTRN.getNumValue();
				if(limitUnit.EACH.name().equals(memberLimitRuleTRN.getLimitUnit())){
					if(null!= numValue && revenueDetail.getAmount().compareTo(numValue) > 0)
					{
						com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error();
						error.setCode(BigInteger.valueOf(425));
						error.setType(BigInteger.valueOf(3));
						error.setContent("insufficient spending limit.");
						com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors();
						errors.setError(error);
						response.setErrors(errors);
						return response;
					}
				}
			}
		}
			
		MemberCashValuePaymentDto paymentDto = new MemberCashValuePaymentDto();
		paymentDto.setCustomerId(member.getCustomerId());
		Integer amount = 1;
		paymentDto.setOrderQty(amount);
		paymentDto.setPaymentMethod(Constant.CASH_Value);//
		paymentDto.setTotalAmount(revenueDetail.getAmount());
		paymentDto.setUserId(member.getUserId());
		paymentDto.setTerminalId(terminalID);
		Map<String,Integer> itemNoMap = new HashMap<String,Integer>();

		//add by Tony
		String paymentDesc=""; 
		String revenueCenterDesc=request.getPosting().getRevenueCenter().getDescription();
		if(Constant.BOOKING_DESC_ROOMCHARGE.equalsIgnoreCase(revenueCenterDesc)) {
			Assert.notNull(request.getPosting().getTransaction().getTenders().getRevenueDetail().getFolioIDs());
			Assert.notNull(request.getPosting().getTransaction().getTenders().getRevenueDetail().getFolioIDs().getFolioID());
			BigInteger folioId=(BigInteger)request.getPosting().getTransaction().getTenders().getRevenueDetail().getFolioIDs().getFolioID().get(0);
			String comfirmId = request.getUniqueID().getID();
			paymentDesc="PMS:"+revenueCenterDesc+" Folio:"+folioId + " comfirmId:"+comfirmId;
			paymentDto.setRemoteType(Constant.PMSTHIRD_ITEM_NO);
			paymentDto.setItemNoMap(itemNoMap);//
			
		}  // SAMHUI 20160918 change: if not PMS, treat it as POS
		else { //if(Constant.BOOKING_DESC_VARIASIA.equalsIgnoreCase(revenueCenterDesc)) { 
			/***
			 * SGG-3271
			 * "RestaurantName: #CheckNumber
			 */
			pmsLog.info("is POS? revenueCenterDesc = " + revenueCenterDesc);
			String ticketID=request.getPosting().getTransaction().getTicketID();
//			paymentDesc="POS:"+revenueCenterDesc+", Chk#:"+ticketID;
			if(revenueCenterDesc.trim().equalsIgnoreCase("Fitness Centre".trim())){
				paymentDesc="Main Counter"+": #"+ticketID;
			}else {
				paymentDesc=revenueCenterDesc+": #"+ticketID;
			}
			
			paymentDto.setRemoteType(Constant.POSTHIRD_ITEM_NO);
			paymentDto.setItemNoMap(itemNoMap);
		}
		logger.info("paymentDesc:"+paymentDesc);
		paymentDto.setDesc(paymentDesc);

		CustomerOrderTransDto order = memberCashValueService.paymentByMemberCashvalue(paymentDto);
		response.setSuccess(new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Success());
		response.setPostingGUID(order.getTransactionNo() + "");

		return response;
	}
	/***
	 * check cardNo payment ,if exist return true,else return false;
	 * @param cardId
	 * @return
	 */
	private boolean checkCardNoEpayment(String cardNo)
	{
		boolean cardBlg=false;
		if(StringUtils.isNotEmpty(cardNo))
		{
			cardNo=cardNo.substring(0,cardNo.length()-1);
			try {
				PermitCardMaster card=permitCardMasterService.getPermitCardMaster(cardNo);
				if(null!=card)
				{
					cardBlg=true;
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		return cardBlg;
	}
	private Member getMemberByCardNo(String cardNo){
		Member member=null;
		if(StringUtils.isNotEmpty(cardNo))
		{
			  cardNo=cardNo.substring(0,cardNo.length()-1);
			try {
				PermitCardMaster card=permitCardMasterService.getPermitCardMaster(cardNo);
				if(null!=card&&null!=card.getMappingCustomerId()&&PermitCardMasterEnumStatus.ISS.getCode().equals(card.getStatus()))
				{
					member= memberDao.getMemberByCustomerId(card.getMappingCustomerId());
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		return member;
		
	}
	
	/**
	 * 推送消息给member同staff
	 */
	private void pushNotificationForMemberAppAndStaffApp(final Long customerID, final Long roomId){
        try {
        	Member member = memberDao.getMemberByCustomerId(customerID);
    		if(member != null){
    			//CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(member.getCustomerId());
    			//String[] staffLocation = new String[]{profile.getGivenName() + " " + profile.getSurname()};
    			Room room = roomDao.getByRoomId(roomId);
    			String[] memberLocation = new String[]{room == null ? "null" : room.getRoomNo()};
    			devicePushService.pushMessage(new String[]{member.getUserId()}, Constant.TEMPLATE_ID_APP_GUEST_ROOM_READY_MEMBER_NOTIFICATION, memberLocation, Constant.MEMBERAPP_PUSH_APPLICATION);
    		}
	    } catch (Exception e) {
	    	logger.error(e.getMessage(), e);
	    }
	}
	private boolean createRoutineTask(Long roomId,String jobType)
	{
		Room room=roomDao.getByRoomId(roomId);
		if(null!=room){
			RoomHousekeepTask task=new RoomHousekeepTask();
			task.setRoom(room);
			task.setJobType(jobType);
			task.setScheduleDatetime(new Date());
			task.setStatus("OPN");
			task.setCreateDate(new Timestamp(new Date().getTime()));
			task.setStatusDate(new Date());
			roomHousekeepTaskDao.save(task);
			return true;
		}
		return false;
	}
	/***
	 * check room_housekeep_task  currentDate , status not in (CMP/CAN)
	 * @param roomId
	 * @param jobType
	 * @return
	 */
	private boolean checkExistRoutineTask(Long roomId,String jobType){
		String sql="SELECT task_id as taskId FROM room_housekeep_task WHERE  room_id=? AND job_type=?  AND DATE_FORMAT(schedule_datetime,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') AND status NOT IN('CMP','CAN')";
		List<Serializable>param=new ArrayList<>();
		param.add(roomId);
		param.add(jobType);
		 List<RoomHousekeepTaskDto>list=roomHousekeepTaskDao.getDtoBySql(sql, param, RoomHousekeepTaskDto.class);
		 if(null!=list&&list.size()>0){
			 return true;
		 }
		return false;
	}
}