package com.sinodynamic.hkgta.service.pos;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pos.RestaurantBookingQuotaDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantCustomerBookingDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantOpenHourDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantBookingQuota;
import com.sinodynamic.hkgta.entity.pos.RestaurantOpenHour;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class RestaurantOpenHourServiceImpl extends ServiceBase<RestaurantOpenHour>implements RestaurantOpenHourService {

	@Autowired
	private RestaurantOpenHourDao restaurantOpenHourDao;

	@Autowired
	private RestaurantBookingQuotaDao restaurantBookingQuotaDao;

	@Autowired
	private RestaurantCustomerBookingDao restaurantCustomerBookingDao;

	@Override
	@Transactional
	public boolean isAvailableRestaurant(String restaurantId, Integer partySize, Date bookTime, String bookVia)
			throws Exception {
		if (bookTime.before(new Date())) {
			return false;
		}
		boolean flag = validateBookingQuota(restaurantId, partySize, bookTime, bookVia);
		if (!flag) {
			responseResult.initResult(GTAError.RestaurantError.PARTYSIZE_EXCEED_QUOTA);
			return false;
		}

		RestaurantOpenHour openHour = restaurantOpenHourDao.getRestaurantOpenHour(restaurantId);
		if (null != openHour) {
			Long open = openHour.getAllowOrderTimeFrom();
			Long close = openHour.getAllowOrderTimeTo();
			int time = getHourAndMinuteOfDate(bookTime);
			if (null != open && null != close && open <= time && time <= close)
				return true;
		}
		return true;
	}

	public static int getWeekOfDate(Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	public static int getHourAndMinuteOfDate(Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		int h = cal.get(Calendar.HOUR_OF_DAY);
		int m = cal.get(Calendar.MINUTE);
		return Integer.parseInt(h + "" + (m == 0 ? "00" : m));
	}

	private boolean validateBookingQuota(String restaurantId, Integer partySize, Date bookTime, String bookVia)
			throws Exception {
		// if(Constant.RESTAURANT_BOOK_VIA_OL.equals(bookVia)){
		String bookingTimeStr = DateCalcUtil.formatDatetime(bookTime);
		Integer bookingTime = Integer.parseInt(bookingTimeStr.substring(11, 16).replace(":", ""));
		List<RestaurantBookingQuota> restaurantBookingQuotas = restaurantBookingQuotaDao
				.getRestaurantBookingQuotaList(restaurantId, bookingTime);
		if (null != restaurantBookingQuotas && restaurantBookingQuotas.size() > 0) {
			int totalQuota = 0;
			int totalPartySize = 0;
			for (RestaurantBookingQuota quota : restaurantBookingQuotas) {
				totalQuota += quota.getQuota().intValue();
				totalPartySize += restaurantCustomerBookingDao.getRestaurantCustomerBookingTotalPartySize(restaurantId,
						bookTime, quota.getStartTime(), quota.getEndTime());
			}
			if ((totalPartySize + partySize) > totalQuota) {
				return false;
			}
		// if the bookingTime is not in timeslot or specified timeslot is not available 	
		}else{
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	public ResponseResult checkBookingQuotaPeriod(String restaurantId, Integer partySize, Date bookTime, String bookVia)
			throws Exception {
		String bookingTimeStr = DateCalcUtil.formatDatetime(bookTime);
		Integer bookingTime = Integer.parseInt(bookingTimeStr.substring(11, 16).replace(":", ""));
		List<RestaurantBookingQuota> restaurantBookingQuotas = restaurantBookingQuotaDao
				.getRestaurantBookingQuotaList(restaurantId, bookingTime);
		if (null != restaurantBookingQuotas && restaurantBookingQuotas.size() > 0) {
			int totalQuota = 0;
			int totalPartySize = 0;
			for (RestaurantBookingQuota quota : restaurantBookingQuotas) {
				totalQuota += quota.getQuota().intValue();
				totalPartySize += restaurantCustomerBookingDao.getRestaurantCustomerBookingTotalPartySize(restaurantId,
						bookTime, quota.getStartTime(), quota.getEndTime());
			}
			if ((totalPartySize + partySize) > totalQuota) {
				responseResult.initResult(GTAError.Success.SUCCESS, (Object) responseResult
						.getI18nMessge(GTAError.RestaurantError.PARTYSIZE_EXCEED_QUOTA.getCode()));
			} else {
				responseResult.initResult(GTAError.Success.SUCCESS, true);
			}
		} else {
			responseResult.initResult(GTAError.Success.SUCCESS,
					(Object) responseResult.getI18nMessge(GTAError.RestaurantError.NO_ONLINE_BOOKQUOTA.getCode()));
		}
		return responseResult;
	}
}
