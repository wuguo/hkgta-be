package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import org.joda.time.LocalDate;

import com.sinodynamic.hkgta.dto.crm.ContractHelperDto;
import com.sinodynamic.hkgta.dto.crm.TempPassProfileDto;
import com.sinodynamic.hkgta.dto.crm.TempPassTypeDetailsDto;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.GateTerminal;
import com.sinodynamic.hkgta.entity.crm.HelperPassType;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface TempPassService extends IServiceBase<InternalPermitCard>
{
	String getTempPassProfileList(String status) throws Exception;
	
	String getTempPassProfileHistoryList(String status) throws Exception;

	ResponseResult createTempPassProfile(ContractHelperDto c) throws Exception;
	
	ResponseResult updateProfile(ContractHelperDto dto) throws Exception;	

	ResponseResult getAllPassType();
	
	ResponseResult listPassType();

	ResponseResult createTempPassType(TempPassTypeDetailsDto tempPassTypeDetail,String userId);

	ResponseResult updateTempPassType(TempPassTypeDetailsDto tempPassTypeDetail,String userId);

	ResponseResult changeTempPassStatus(TempPassTypeDetailsDto tempPassTypeDetail);
	
	ResponseResult getTempPassTypeById(Long passTypeId);
	
	ResponseResult deleteTempPassTypeById(Long passTypeId);

	ResponseResult getAllTerminalDropDown();
	
	List<GateTerminal> getAllTerminal();

	String getTempPassTypeList(String status);

	ResponseResult getProfileDetails(String contractorId);
	
	ResponseResult getProfileDetailsByHKID(String HKId);
	
	ResponseResult cloneTempPassTypeById(Long passTypeId,String userId);

	ResponseResult linkCard(TempPassProfileDto dto) throws Exception;
	
	ResponseResult replaceCard(TempPassProfileDto dto);

	List<ContractHelper> getTempPassProfilesExpiringToday(LocalDate date);

	void updateStatusForExpiredTempPass() throws Exception;
	
	ResponseResult returnInternalPermitCard(String cardId, String updateBy, String contractorId) throws Exception;
	
	ResponseResult disposalInternalPermitCard(String cardId, String updateBy) throws Exception;
	
	

}
