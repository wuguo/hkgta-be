package com.sinodynamic.hkgta.service.onlinepayment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JRException;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.crm.CashvalueTopupHistoryDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailAttachDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEmailContentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerEnrollmentDao;
import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceDao;
import com.sinodynamic.hkgta.dao.crm.CustomerServiceSubscribeDao;
import com.sinodynamic.hkgta.dao.crm.MemberCashValueDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dao.crm.MessageTemplateDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.fms.CourseDao;
import com.sinodynamic.hkgta.dao.fms.CourseEnrollmentDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dao.mms.SpaAppointmentRecDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayCmdDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayDao;
import com.sinodynamic.hkgta.dao.pms.RoomReservationRecDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderHdDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderPermitCardDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerOrderTransDao;
import com.sinodynamic.hkgta.dao.rpos.CustomerTransactionDao;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.crm.TopUpDto;
import com.sinodynamic.hkgta.dto.crm.TransactionEmailDto;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.pms.HotelPaymentDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationInfoDto;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmd;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmdPK;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.integration.spa.request.CCPaymentRequest;
import com.sinodynamic.hkgta.integration.spa.response.CCPaymentResponse;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.common.MailThreadService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.service.fms.CourseEnrollmentService;
import com.sinodynamic.hkgta.service.fms.FacilityEmailService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.mms.MMSService;
import com.sinodynamic.hkgta.service.pms.HotelReservationService;
import com.sinodynamic.hkgta.service.pms.HotelReservationServiceFacade;
import com.sinodynamic.hkgta.service.pms.PMSRequestProcessorService;
import com.sinodynamic.hkgta.service.pms.RoomReservationRecService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderDetService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.NoResultCallBack;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.response.MessageResult;
import com.sinodynamic.hkgta.util.constant.CustomerServiceStatus;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.EmailType;
import com.sinodynamic.hkgta.util.constant.EnrollCourseStatus;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MemberAcceptance;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.PaymentCmd;
import com.sinodynamic.hkgta.util.constant.PaymentCmdType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.SceneType;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.X509TrustManager;

/**
 * payment gateway business logic. Copy from firstdata's sample codes
 * 
 * @author Allen_Yu
 *
 */
@Service
public class PaymentGatewayServiceImpl extends ServiceBase<PaymentGateway> implements PaymentGatewayService {
	private Logger						pmsLog	= Logger.getLogger(LoggerType.PMS.getName());
	@Autowired
	private PaymentGatewayDao					paymentGatewayDao;

	@Autowired
	private PaymentGatewayCmdDao				paymentGatewayCmdDao;

	@Autowired
	private CustomerOrderTransDao				customerOrderTransDao;

	@Autowired
	private CustomerOrderHdDao					customerOrderHdDao;

	@Autowired
	private MemberCashValueDao					memberCashValueDao;

	@Autowired
	private MemberFacilityTypeBookingService	memberFacilityTypeBookingService;

	@Autowired
	private MemberDao							memberDao;
	@Autowired
	private MemberService memberService;
	@Autowired
	private UserPreferenceSettingService userPreferenceSettingService;

	@Autowired
	private CustomerOrderPermitCardDao			customerOrderPermitCardDao;

	@Autowired
	private MessageTemplateDao					messageTemplateDao;

	@Autowired
	private CustomerProfileDao					customerProfileDao;

	@Autowired
	private CourseEnrollmentDao					courseEnrollmentDao;

	@Autowired
	private CashvalueTopupHistoryDao			topupHistoryDao;

	@Autowired
	private CustomerOrderTransService			customerOrderTransService;

	@Autowired
	private MemberFacilityTypeBookingDao		memberFacilityTypeBookingDao;

	// @Autowired
	// private CourseService courseService;

	@Autowired
	private CourseEnrollmentService				courseEnrollmentService;

	@Autowired
	private ShortMessageService					smsService;

	@Autowired
	private CourseDao							courseDao;

	@Autowired
	private MailThreadService					mailThreadService;

	@Autowired
	private CustomerEmailContentDao				customerEmailContentDao;

	@Autowired
	private CustomerEmailAttachDao				customerEmailAttachDao;

	@Autowired
	private CustomerOrderDetService				customerOrderDetService;

	@Autowired
	private FacilityEmailService				facilityEmailService;

	@Autowired
	private ServicePlanDao						servicePlanDao;

	@Autowired
	private RoomReservationRecDao				roomReservationRecDao;
	
	@Autowired
	private RoomReservationRecService roomReservationRecService;

	@Autowired
	private PMSRequestProcessorService			pmsRequestProcessorService;

	@Autowired
	private HotelReservationServiceFacade		hotelReservationServiceFacade;

	@Autowired
	private HotelReservationService				hotelReservationService;

	@Autowired
	private SpaServcieManager					spaServcieManager;

	@Autowired
	private SpaAppointmentRecDao				spaAppointmentRecDao;

	@Autowired
	private CustomerServiceDao					customerServiceDao;

	@Autowired
	private CustomerServiceSubscribeDao			customerServiceSubscribeDao;

	@Autowired
	private MMSService							mmsService;
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;
	@Autowired
	private CustomerTransactionDao customerTransactionDao;
	@Autowired
	private CustomerEnrollmentDao customerEnrollmentDao;
	
	@Autowired
	private PMSApiService pmsApiService;
	
	// Define Constants
	// ****************
	// This is secret for encoding the MD5 hash
	// This secret will vary from merchant to merchant
	static String								SECURE_SECRET		= "A9EB88A22BBE3513BD36F57B3CB2E599";

	// This is an array for creating hex chars
	static final char[]							HEX_TABLE			= new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C',
			'D', 'E', 'F'											};
	public static X509TrustManager				s_x509TrustManager	= null;
	public static SSLSocketFactory				s_sslSocketFactory	= null;

	static {
		s_x509TrustManager = new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] {};
			}

			public boolean isClientTrusted(X509Certificate[] chain) {
				return true;
			}

			public boolean isServerTrusted(X509Certificate[] chain) {
				return true;
			}
		};

		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		try {
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { s_x509TrustManager }, null);
			s_sslSocketFactory = context.getSocketFactory();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------
	private String translateUrlParams(final Map<String, Object> urlParams) {
		final StringBuilder string = new StringBuilder();

		CollectionUtil.loop(urlParams.keySet(), new NoResultCallBack<String>() {
			@Override
			public void execute(String key, int index) {
				if (index == 0) {
					string.append(key + "=" + urlParams.get(key));
				} else {
					string.append("&" + key + "=" + urlParams.get(key));
				}
			}
		});

		return "?" + string.toString();
	}

	@Transactional
	public String payment(CustomerOrderTrans customerOrderTrans, Map<String, Object> urlParams) {
		logger.info("call  payment service");
		Long gatewayId = Long.parseLong(appProps.getProperty("hkgta.paymentgateway.gatewayId"));// The test environment is 1,the truth environment is
																								// 2
		String cmdId = PaymentCmd.PAY.getDesc();
		PaymentGatewayCmdPK pk = new PaymentGatewayCmdPK(gatewayId, cmdId);
		PaymentGateway paymentGateway = paymentGatewayDao.get(PaymentGateway.class, gatewayId);
		PaymentGatewayCmd paymentGatewayCmd = paymentGatewayCmdDao.get(PaymentGatewayCmd.class, pk);

		SECURE_SECRET = paymentGateway.getSecureSecret();

		String vpc_AccessCode = paymentGateway.getAccessCode();
		String vpc_Version = appProps.getProperty("hkgta.paymentgateway.vpc_Version");// "1"
		String vpc_Command = paymentGatewayCmd.getCommandType();
		String virtualPaymentClientURL = paymentGateway.getGatewayUrl() + "/" + paymentGatewayCmd.getUrlCmd();
		String vpc_Merchant = paymentGateway.getMerchantId();
		// TODO PaidAmount此数小于1时第三方系统会提示错误 所以jack添加容错处理2016年5月19日16:39:44长潘决定删除配置文件判断是否乘于100		
		String vpc_Amount = customerOrderTrans.getPaidAmount().multiply(new BigDecimal(100)).longValue()  + "";
		String vpc_ReturnURL = paymentGatewayCmd.getReturnToPage();
		String vpc_MerchTxnRef = customerOrderTrans.getTransactionNo().toString();
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("vpc_AccessCode", vpc_AccessCode);
		fields.put("vpc_Version", vpc_Version);
		fields.put("vpc_Command", vpc_Command);
		fields.put("virtualPaymentClientURL", virtualPaymentClientURL);
		fields.put("vpc_Merchant", vpc_Merchant);
		fields.put("vpc_Amount", vpc_Amount);
		if (urlParams == null || urlParams.isEmpty()) {
			fields.put("vpc_ReturnURL", vpc_ReturnURL);
		} else {
			fields.put("vpc_ReturnURL", vpc_ReturnURL + translateUrlParams(urlParams));
		}

		fields.put("vpc_MerchTxnRef", vpc_MerchTxnRef);
		String vpcURL = (String) fields.remove("virtualPaymentClientURL");
		// Create MD5 secure hash and insert it into the hash map if it was created. Remember if SECURE_SECRET = "" it will not be created
		if (SECURE_SECRET != null && SECURE_SECRET.length() > 0) {
			//String secureHash = hashAllFields(fields);  
			String secureHash = SHAhashAllFields(fields); // New SHA hash alogirthm
			fields.put("vpc_SecureHash", secureHash);
			fields.put("vpc_SecureHashType", "SHA256"); // New added for SHA hash alogirthm
		}

		// Create a redirection URL
		StringBuffer buf = new StringBuffer();
		buf.append(vpcURL).append('?');
		appendQueryFields(buf, fields);
		logger.info("redit card Redirection URL is :"+buf.toString());
		return buf.toString();
	}

	@Transactional
	public String payment(CustomerOrderTrans customerOrderTrans) {
		logger.info("create credit card Redirection URL start...");
		return payment(customerOrderTrans, null);
	}

	//////////////////begin  new HASH SHA256 ////////////////////////////////////////
	/**
	    * This method is for sorting the fields and creating a SHA256 secure hash.
	    *
	    * @param fields is a map of all the incoming hey-value pairs from the VPC
	    * @return is the hash being returned for comparison to the incoming hash
	*/
	
	String SHAhashAllFields(Map fields) {
		String hashedFields = new String();
		String hashKeys = "";
		String hashValues = "";

		// create a list and sort it
		List fieldNames = new ArrayList(fields.keySet());
		Collections.sort(fieldNames);

		// create a buffer for the SHA256 input
		StringBuffer buf = new StringBuffer();

		// iterate through the list and add the remaining field values
		Iterator itr = fieldNames.iterator();
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) fields.get(fieldName);
				hashKeys += fieldName + ", ";
			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				buf.append(fieldName + "=" + fieldValue);
				if (itr.hasNext()) {
					buf.append('&');
				}
			}
		}
		hashedFields = buf.toString();
		byte[] mac = null;
		try {
			byte []  b = fromHexString(SECURE_SECRET, 0, SECURE_SECRET.length());
			SecretKey key = new SecretKeySpec(b, "HmacSHA256");
			Mac m = Mac.getInstance("HmacSHA256");
			m.init(key);
			//String values = new String(buf.toString(), "UTF-8");
			m.update(buf.toString().getBytes("ISO-8859-1"));
			mac = m.doFinal();
		} catch(Exception e) {
			
		}
		String hashValue = hex(mac);
		return hashValue;

    } // end SHAhashAllFields()

	
	public static byte[] fromHexString(String s, int offset, int length)
	{
	    if ((length%2) != 0)
	      return null;
	    byte[] byteArray = new byte[length/2];
	    int j = 0;
	    int end = offset+length;
	    for (int i = offset; i < end; i += 2)
		{
		   int high_nibble = Character.digit(s.charAt(i), 16);
		   int low_nibble = Character.digit(s.charAt(i+1), 16);
		   if (high_nibble == -1 || low_nibble == -1)
		   {
			 // illegal format
			 return null;
		   }
		   byteArray[j++] = (byte)(((high_nibble << 4) & 0xf0) | (low_nibble & 0x0f));
		}
	    return byteArray;
	}
   //////////////////new HASH SHA256 end //////////////////////////////////////// 
	
	/**
	 * This method is for sorting the fields and creating an MD5 secure hash. (replaced by SHAhashAllFields)
	 *
	 * @param fields
	 *            is a map of all the incoming hey-value pairs from the VPC
	 * @param buf
	 *            is the hash being returned for comparison to the incoming hash
	 */
	@Deprecated 
	String hashAllFields(Map<String, String> fields) {

		// create a list and sort it
		List<String> fieldNames = new ArrayList<String>(fields.keySet());
		Collections.sort(fieldNames);

		// create a buffer for the md5 input and add the secure secret first
		StringBuffer buf = new StringBuffer();
		buf.append(SECURE_SECRET);

		// iterate through the list and add the remaining field values
		Iterator<String> itr = fieldNames.iterator();

		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) fields.get(fieldName);
			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				buf.append(fieldValue);
			}
		}

		MessageDigest md5 = null;
		byte[] ba = null;

		// create the md5 hash and UTF-8 encode it
		try {
			md5 = MessageDigest.getInstance("MD5");
			ba = md5.digest(buf.toString().getBytes("UTF-8"));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} // wont happen

		// return buf.toString();
		return hex(ba);

	} // end hashAllFields()

	// ----------------------------------------------------------------------------

	/**
	 * Returns Hex output of byte array
	 */
	static String hex(byte[] input) {
		// create a StringBuffer 2x the size of the hash array
		StringBuffer sb = new StringBuffer(input.length * 2);

		// retrieve the byte array data, convert it to hex
		// and add it to the StringBuffer
		for (int i = 0; i < input.length; i++) {
			sb.append(HEX_TABLE[(input[i] >> 4) & 0xf]);
			sb.append(HEX_TABLE[input[i] & 0xf]);
		}
		return sb.toString();
	}

	// ----------------------------------------------------------------------------

	/**
	 * This method is for creating a URL query string.
	 *
	 * @param buf
	 *            is the inital URL for appending the encoded fields to
	 * @param fields
	 *            is the input parameters from the order page
	 */
	// Method for creating a URL query string
	void appendQueryFields(StringBuffer buf, Map<String, String> fields) {

		// create a list
		List<String> fieldNames = new ArrayList<String>(fields.keySet());
		Iterator<String> itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) fields.get(fieldName);

			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				// append the URL parameters
				buf.append(URLEncoder.encode(fieldName));
				buf.append('=');
				buf.append(URLEncoder.encode(fieldValue));
			}

			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				buf.append('&');
			}
		}

	} // appendQueryFields()

	@SuppressWarnings({ "rawtypes", "unused" })
	@Override
	@Transactional
	public String processResult(HttpServletRequest request, String userId) {
		logger.info("============================Process reback result start========================================");
		Long gatewayId = Long.parseLong(appProps.getProperty("hkgta.paymentgateway.gatewayId"));// The test environment is 1,the truth environment is
																								// 2
		PaymentGateway paymentGateway = paymentGatewayDao.get(PaymentGateway.class, gatewayId);
		SECURE_SECRET = paymentGateway.getSecureSecret();

		// retrieve all the incoming parameters into a hash map
		Map<String, String> fields = new HashMap<String, String>();
		for (Enumeration enump = request.getParameterNames(); enump.hasMoreElements();) {
			String fieldName = (String) enump.nextElement();
			String fieldValue = request.getParameter(fieldName);
			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				fields.put(fieldName, fieldValue);
			}
		}
		logger.info("call back...."+Log4jFormatUtil.logInfoMessage(request.getRequestURI(), fields.toString(), request.getMethod(), null));
		/*
		 * If there has been a merchant secret set then sort and loop through all the data in the Virtual Payment Client response. while we have the
		 * data, we can append all the fields that contain values (except the secure hash) so that we can create a hash and validate it against the
		 * secure hash in the Virtual Payment Client response. NOTE: If the vpc_TxnResponseCode in not a single character then there was a Virtual
		 * Payment Client error and we cannot accurately validate the incoming data from the secure hash.
		 */

		// remove the vpc_TxnResponseCode code from the response fields as we do
		// not
		// want to include this field in the hash calculation
		String vpc_Txn_Secure_Hash = null2unknown((String) fields.remove("vpc_SecureHash"));
		String hashValidated = null;

		// defines if error message should be output
		boolean errorExists = false;

		if (SECURE_SECRET != null && SECURE_SECRET.length() > 0
				&& (fields.get("vpc_TxnResponseCode") != null || fields.get("vpc_TxnResponseCode") != "No Value Returned")) {

			// create secure hash and append it to the hash map if it was
			// created
			// remember if SECURE_SECRET = "" it wil not be created
			//String secureHash = hashAllFields(fields);
			String secureHash = SHAhashAllFields(fields); // New SHA hash alogirth

			// Validate the Secure Hash (remember MD5 hashes are not case
			// sensitive)
			if (vpc_Txn_Secure_Hash.equalsIgnoreCase(secureHash)) {
				// Secure Hash validation succeeded, add a data field to be
				// displayed later.
				hashValidated = "<font color='#00AA00'><strong>CORRECT</strong></font>";
			} else {
				// Secure Hash validation failed, add a data field to be
				// displayed later.
				errorExists = true;
				hashValidated = "<font color='#FF0066'><strong>INVALID HASH</strong></font>";
			}
		} else {
			// Secure Hash was not validated,
			hashValidated = "<font color='orange'><strong>Not Calculated - No 'SECURE_SECRET' present.</strong></font>";
		}

		// Extract the available receipt fields from the VPC Response
		// If not present then let the value be equal to 'Unknown'
		// Standard Receipt Data
		String title = null2unknown(fields.get("Title"));
		String againLink = null2unknown(fields.get("AgainLink"));
		String amount = null2unknown(fields.get("vpc_Amount"));
		String locale = null2unknown(fields.get("vpc_Locale"));
		String batchNo = null2unknown(fields.get("vpc_BatchNo"));
		String command = null2unknown(fields.get("vpc_Command"));
		String message = null2unknown(fields.get("vpc_Message"));
		String version = null2unknown(fields.get("vpc_Version"));
		String cardType = null2unknown(fields.get("vpc_Card"));
		String orderInfo = null2unknown(fields.get("vpc_OrderInfo"));
		String receiptNo = null2unknown(fields.get("vpc_ReceiptNo"));
		String merchantID = null2unknown(fields.get("vpc_Merchant"));
		String merchTxnRef = null2unknown(fields.get("vpc_MerchTxnRef"));
		String authorizeID = null2unknown(fields.get("vpc_AuthorizeId"));
		String transactionNo = null2unknown(fields.get("vpc_TransactionNo"));
		String acqResponseCode = null2unknown(fields.get("vpc_AcqResponseCode"));
		String txnResponseCode = null2unknown(fields.get("vpc_TxnResponseCode"));

		// CSC Receipt Data
		String vCSCResultCode = null2unknown(fields.get("vpc_CSCResultCode"));
		String vCSCRequestCode = null2unknown(fields.get("vpc_CSCRequestCode"));
		String vACQCSCRespCode = null2unknown(fields.get("vpc_AcqCSCRespCode"));

		// AVS Receipt Data
		String vAVS_City = null2unknown(fields.get("vpc_AVS_City"));
		String vAVS_Country = null2unknown(fields.get("vpc_AVS_Country"));
		String vAVS_Street01 = null2unknown(fields.get("vpc_AVS_Street01"));
		String vAVS_PostCode = null2unknown(fields.get("vpc_AVS_PostCode"));
		String vAVS_StateProv = null2unknown(fields.get("vpc_AVS_StateProv"));
		String vAVSResultCode = null2unknown(fields.get("vpc_AVSResultCode"));
		String vAVSRequestCode = null2unknown(fields.get("vpc_AVSRequestCode"));
		String vACQAVSRespCode = null2unknown(fields.get("vpc_AcqAVSRespCode"));

		// 3-D Secure Data
		String transType3DS = null2unknown(fields.get("vpc_VerType"));
		String verStatus3DS = null2unknown(fields.get("vpc_VerStatus"));
		String token3DS = null2unknown(fields.get("vpc_VerToken"));
		String secureLevel3DS = null2unknown(fields.get("vpc_VerSecurityLevel"));
		String enrolled3DS = null2unknown(fields.get("vpc_3DSenrolled"));
		String xid3DS = null2unknown(fields.get("vpc_3DSXID"));
		String eci3DS = null2unknown(fields.get("vpc_3DSECI"));
		String status3DS = null2unknown(fields.get("vpc_3DSstatus"));

		String error = "";
		// Show this page as an error page if error condition
		String htmlString = "<html><body onload='closeWindow();'>payment success"
				+ " <input type='button' value='ok' onclick='transactionSuccess()' />" + "<script>" + " function transactionSuccess() {"
				+ " window.close();" + " }" + " function closeWindow() {" + " window.close();" + " }" + "</script>" + "</body></html>";
		String paymentMethod = PaymentMethod.VISA.name();
		if ("MC".equalsIgnoreCase(cardType)) {
			paymentMethod = PaymentMethod.MASTER.name();
		} else if ("VC".equalsIgnoreCase(cardType)) {
			paymentMethod = PaymentMethod.VISA.name();
		}

		try {
			CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, Long.parseLong(merchTxnRef));
			customerOrderTrans.setPaymentMethodCode(paymentMethod);
			customerOrderTrans.setOpgResponseCode(txnResponseCode);
			customerOrderTrans.setAgentTransactionNo(transactionNo);
			customerOrderTrans.setUpdateBy("System");
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
			String status = customerOrderTrans.getStatus();
			if (txnResponseCode.equals("0")) {
				logger.info("texResponseCode is SUC .");
				if (CustomerTransationStatus.PND.getDesc().equals(status)) {
					customerOrderTrans.setStatus(CustomerTransationStatus.SUC.name());
					customerOrderTransDao.saveOrUpdate(customerOrderTrans);
					logger.info("update customerOrderTrans Status PND to SUC ..");
					
					CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
					if (null != customerOrderHd) {
						logger.info("find CustomerOrderHd  start ...");
						// update member type booking, from PND to RSV
						MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingDao
								.getMemberFacilityTypeBookingByOrderNo(customerOrderHd.getOrderNo());
						if (memberFacilityTypeBooking != null) {
							memberFacilityTypeBooking.setStatus("RSV");
							memberFacilityTypeBooking.setUpdateDate(new Date());
							// memberFacilityTypeBooking.setUpdateBy("System"); //used as a system update
							memberFacilityTypeBookingDao.update(memberFacilityTypeBooking);
							logger.info("update  MemberFacilityTypeBooking to  RSV is SUC..");
							try {
								logger.info(" send confrimation mail ...");
								facilityEmailService.sendReceiptConfirmationEmail(memberFacilityTypeBooking.getResvId(), userId);
							} catch (Exception e) {
								logger.error("Occurrd exception when send email for facility reservation!", e);
							}
						}

						List<CustomerOrderTrans> customerOrderTranses = customerOrderHd.getCustomerOrderTrans();
						BigDecimal totalAmount = BigDecimal.ZERO;
						for (CustomerOrderTrans tempTrans : customerOrderTranses) {
							if (CustomerTransationStatus.SUC.getDesc().equals(tempTrans.getStatus())) {
								totalAmount = totalAmount.add(tempTrans.getPaidAmount());
							}
						}
						if (customerOrderHd.getOrderTotalAmount().compareTo(totalAmount) == 0) {
							customerOrderHd.setOrderStatus(CustomerTransationStatus.CMP.getDesc());
							customerOrderHd.setUpdateDate(new Date());
							// customerOrderHd.setUpdateBy(userId);
							customerOrderHdDao.update(customerOrderHd);
							logger.info(" update customerOrderHd status to CMP..");
						}
						List<CustomerOrderPermitCard> cards = customerOrderHd.getCustomerOrderPermitCards();
						if (null != cards && cards.size() > 0) {
							processDaypassPurchase(cards, customerOrderHd, customerOrderTrans, userId);
						}
						List<CustomerOrderDet> customerOrderDets = customerOrderHd.getCustomerOrderDets();
						if (customerOrderDets.size() > 0 && Constant.TOPUP_ITEM_NO.equals(customerOrderDets.get(0).getItemNo())) {
							// Process the logic of top up.
							processTopUp(customerOrderTrans);
						} else if (customerOrderDets.size() > 0
								&& (customerOrderDets.get(0).getItemNo().startsWith(Constant.GOLF_COURSE_PRICE_PREFIX) || customerOrderDets.get(0)
										.getItemNo().startsWith(Constant.TENNIS_COURSE_PRICE_PREFIX))) {
							// Process the logic of payment for tennis.
							processCourseAndTennisPayment(customerOrderHd, userId);
						} else if (customerOrderDets.size() > 0 && customerOrderDets.get(0).getItemNo().startsWith(Constant.PMS_ROOMTYPE_ITEM_PREFIX)) 
						{
							/***
							 * if oasis payment is success then hander 
							 * when member/ISO payment VISA , after third success send oasis 
							 */
							logger.info("commit oasis payment ......");
							if(this.commitOasisPayment(customerOrderTrans, fields.get("items"))){
								// Callback from bank, bank successfully pay, do three things
								List<RoomReservationRec> bookings = roomReservationRecDao.getByHql(
										"from RoomReservationRec where orderNo = ?",
										Arrays.<Serializable> asList(customerOrderHd.getOrderNo()));

								// 1. Update booking status to SUC
								for (RoomReservationRec booking : bookings) {
									booking.setStatus(RoomReservationStatus.SUC.name());
								}

								// 2. Golfing bay bundle
								List<RoomReservationInfoDto> bundles = new ArrayList<RoomReservationInfoDto>();

								for (RoomReservationRec book : bookings) {
									RoomReservationInfoDto bundle = new RoomReservationInfoDto();
									bundle.setReservationId(book.getConfirmId());
									bundle.setArriveDate(book.getArrivalDate());
									bundle.setDepartDate(book.getDepartureDate());

									bundles.add(bundle);
								}
								Map<String, List<Long>> association = pmsRequestProcessorService.giftBundleForReservationsOneNightOneBooking(
										paymentMethod,
										customerOrderHd.getCustomerId(),
										bundles);
								// 3. Update room and facility booking association
								logger.info(" Update room and facility booking association ...");
								hotelReservationService.updateGuessroomAndFacilityAssociation(
										request.getParameter("userId"),
										association,
										new HashMap<String, Object>());
								// 4. Send Email
								Map<String, Object> params = new HashMap<String, Object>();
								params.put("userId", request.getParameter("userId"));
								params.put("userName", request.getParameter("userName"));
								logger.info("send guest room reservation mail ...");
								hotelReservationServiceFacade.sendGuestroomReservationEmail(customerOrderTrans.getTransactionNo(), params);
								
							}else{
								//update customerOrderTrans status is fail
								/***
								 * 1、voidpurchcase
								 * 2、refund
								 * 3、cancel customerOrderTrans
								 */
								logger.info("update customerOrderTrans status is fail");
								logger.info("to search CustomerOrderTrans from  gateway ...");
								CustomerOrderTrans trans=this.queryResult(customerOrderTrans);
								customerOrderTransDao.update(trans);
								logger.info("update trans status is FIAL...");
							}

						} else if (customerOrderDets.size() > 0 && customerOrderDets.get(0).getItemNo().startsWith("MMS")) {
							// added by vicky wang to implement MMS credit card payment
							// after success payment, call MMS API to finish the credit card payment.

							Member member = this.memberDao.getMemberByCustomerId(customerOrderHd.getCustomerId());

							CCPaymentRequest params = new CCPaymentRequest();
							params.setAmountPaid(customerOrderTrans.getPaidAmount());
							params.setCardType(getConfigPaymentCode("MMS", paymentMethod));	
							
							params.setGuestCode(member.getAcademyNo());
							params.setInvoiceNo(customerOrderHd.getVendorRefCode());
							params.setBankName("online");
							CCPaymentResponse response = spaServcieManager.creditCardPayment(params);

							logger.info("payment response from API:" + response);
							if (!response.getSuccess()) {
								logger.error(response.getMessage());
							}else{
								// for MMS payment, the order only canbe closed/completed in MMS.
								customerOrderHd.setOrderStatus(Constant.Status.OPN.name());
								customerOrderHd.setUpdateDate(new Date());
								customerOrderHdDao.update(customerOrderHd);
								// send payment receipt email and message.
								MMSPaymentDto dto = new MMSPaymentDto();
								dto.setInvoiceNo(customerOrderHd.getVendorRefCode());
								dto.setOrderNo(customerOrderHd.getOrderNo().toString());
								mmsService.sentPaymentReceipt(dto, false);

								List<SpaAppointmentRec> appointmentList = spaAppointmentRecDao.getByCol(
										SpaAppointmentRec.class,
										"extInvoiceNo",
										customerOrderHd.getVendorRefCode(),
										null);
								mmsService.sentMessage(appointmentList, SceneType.WELLNESS_PAYMENT_SETTLE);

							}
						} else if (customerOrderDets.size() > 0 && customerOrderDets.get(0).getItemNo().startsWith(Constant.RENEW_ITEM_NO_PREFIX)) {
							// if the payment is for renew, need to set effective date and expiry date for customer service account.
							logger.info("handleRenewPayment .....");
							handleRenewPayment(customerOrderHd);
							/**
							 * When any status change happens on payment record for membership  renewal, 
							 * an email notice should be sent to the Sales Person who is the Membership Officer of the member enrollment record .
							 */
						    sendMailToSalesPerson(Long.parseLong(merchTxnRef));
						    
						    
						}
					}
				} else if (CustomerTransationStatus.FAIL.getDesc().equals(status)) {
					// Process exception payment.
					processExceptionFailedPayment(customerOrderTrans);
				}

			}
		  else {

				CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
				if (null != customerOrderHd) {
					CustomerOrderDet customerOrderDet = customerOrderDetService.getCustomerOrderDet(customerOrderHd.getOrderNo());
					if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo())
							&& customerOrderDet.getItemNo().contains(Constant.FACILITY_PRICE_PREFIX)) {
						memberFacilityTypeBookingService.voidMemberFacilityTypeBooking(customerOrderTrans.getCustomerOrderHd().getOrderNo(), userId);
					} else if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo())
							&& customerOrderDet.getItemNo().contains(Constant.PMS_ROOMTYPE_ITEM_PREFIX)) {
						// Callback from bank , but bank fails to pay
						//payment before create reservation Guest room and golf/tennis payment after create reservation
						// do nothing
						
						//bank is fail include cancel
						//this is cancel
						/***
						 * SGG-3695
						 */
						if("C".equalsIgnoreCase(txnResponseCode))
						{
							//need update reservation status is RSV
							List<RoomReservationRec>roomRecs=roomReservationRecDao.getReservationByOrderNo(customerOrderHd.getOrderNo());
							if(null!=roomRecs){
								RoomReservationRec rec=roomRecs.get(0);
								rec.setStatus(Status.RSV.name());
								rec.setUpdateDate(new Date());
								roomReservationRecDao.update(rec);
							}
						}

					}
				}

				customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
				customerOrderTransDao.saveOrUpdate(customerOrderTrans);
				htmlString = "<html><body  onload='closeWindow();'>payment fail" + " <input type='button' value='ok' onclick='transactionFail()' />"
						+ "<script>" + " function transactionFail() {" + " app.failure();" + " }" + " function closeWindow() {" + " window.close();"
						+ " }" + "</script>" + "</body></html>";
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		customerOrderTransService.updateEnrollmentStatusOncePayementChange(Long.parseLong(merchTxnRef));
		logger.info("response code  to call back: "+htmlString);
		logger.info("============================Process reback result end========================================");
		
		return htmlString;

		// return "rest/getProfile"; // temporary only. Will change to the real url page later
	}
	private String getCardCodeByMethod(String paymentMethod){
		String cardCode="VI";
		if(paymentMethod.equalsIgnoreCase(PaymentMethod.AMEX.name()))
		{   cardCode=PaymentMethod.AMEX.getCardCode();
		}else if(paymentMethod.equalsIgnoreCase(PaymentMethod.JCB.name())){
			cardCode=PaymentMethod.JCB.getCardCode();
		}else if(paymentMethod.equalsIgnoreCase(PaymentMethod.CUP.name())){
			cardCode=PaymentMethod.CUP.getCardCode();
		}else if(paymentMethod.equalsIgnoreCase(PaymentMethod.VISA.name())){
			cardCode=PaymentMethod.VISA.getCardCode();
		}else if(paymentMethod.equalsIgnoreCase(PaymentMethod.MASTER.name())){
			cardCode=PaymentMethod.MASTER.getCardCode();
		}else if(paymentMethod.equalsIgnoreCase(PaymentMethod.DINERSCLUB.name())){
			cardCode=PaymentMethod.DINERSCLUB.getCardCode();
		}else if(paymentMethod.equalsIgnoreCase(PaymentMethod.CASH.name())){
			cardCode=PaymentMethod.CASH.getCardCode();
		}else if(paymentMethod.equalsIgnoreCase(PaymentMethod.CASHVALUE.name())){
			cardCode=PaymentMethod.CASHVALUE.getCardCode();
		}
		return cardCode;
	}
	
	public boolean commitOasisPayment(final CustomerOrderTrans trans, final String item) throws Exception {
			List<RoomReservationRec> bookings = roomReservationRecService
					.getReservationByOrderNo(trans.getCustomerOrderHd().getOrderNo());
			boolean oasisCommitBlean=Boolean.TRUE;
			
			if(null!=bookings){
				Map<String, String> map = mapItem(item,trans);
				pmsLog.info("set confrim map amount "+map.toString());
				logger.info("set confrim map amount "+map.toString());
				for (RoomReservationRec book : bookings) {
					HotelPaymentDto oasisPayment = new HotelPaymentDto();
					try {
						Member member = memberService.getMemberById(book.getCustomerId());
						oasisPayment.setResID(book.getConfirmId());
						oasisPayment.setAmount(new BigDecimal(map.get(book.getConfirmId())));
						oasisPayment.setCardHolderName(member.getAcademyNo());// membershipNo
						oasisPayment.setMaskedCardNumber("|" + trans.getTerminalId() + "|" + book.getConfirmId() + "|");
						oasisPayment.setCardCode(getConfigPaymentCode("PMS", trans.getPaymentMethodCode()));
						oasisPayment.setExpireDate(CommUtil.dateFormat(new Date(), "yyMM"));
						MessageResult msgResult = null;
						msgResult = pmsApiService.payment(oasisPayment);
						pmsLog.info("end pmsApiService.payment() for RoomReservationRec confirmId: " + book.getConfirmId()+",transcationNo:"+trans.getTransactionNo());
						logger.info("end pmsApiService.payment() for RoomReservationRec confirmId: " + book.getConfirmId()+",transcationNo:"+trans.getTransactionNo());
						String url="/guestroom/payment/posCallback ";
						
						if (!msgResult.isSuccess()) {
							// Oasis fail
							pmsLog.error(Log4jFormatUtil.logErrorMessage(url, null, null, null, msgResult.getMessage()));//
							logger.error(Log4jFormatUtil.logErrorMessage(url, null, null, null, msgResult.getMessage()));//
							oasisCommitBlean=Boolean.FALSE;
							break;
						} else {
							pmsLog.info(Log4jFormatUtil.logInfoMessage(url, null, null, msgResult.toString()));
							logger.info(Log4jFormatUtil.logInfoMessage(url, null, null, msgResult.toString()));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						oasisCommitBlean=Boolean.FALSE;
						pmsLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));//
						logger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, e.getMessage()));//
					}
				}
		}
			return oasisCommitBlean;
		
	}
	
	private Map<String, String>mapItem(String item ,CustomerOrderTrans trans)
	{
		Map<String, String>map=new HashMap<>();
		
		if(!StringUtils.isEmpty(item)){
			String items[]=item.split("\\|\\|");
			for (String ites : items) 
			{
				String []tes=ites.split("\\|");
				map.put(tes[0], tes[1]);
			}
		}else{
			List<CustomerOrderDet> orderDets=trans.getCustomerOrderHd().getCustomerOrderDets();
			for(CustomerOrderDet det:orderDets)
			{
				map.put(det.getExtRefNo(),det.getItemTotalAmout().toString());
			}
		}
		return map;
	}
	/***
	 * RenewPayment   send mail 
	 * @param transactionNo
	 */
	public void sendMailToSalesPerson(Long transactionNo){
		CustomerOrderTrans customerOrderTrans = customerOrderTransDao.get(CustomerOrderTrans.class, transactionNo);
		Long orderNo = null;
		if (customerOrderTrans == null) {
			return;
		} else {
			orderNo = customerOrderTrans.getCustomerOrderHd().getOrderNo();
		}
		CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
		BigDecimal validateBalanceDueAfterPayment = customerTransactionDao.countBalanceDueStatus(orderNo);
		CustomerEnrollment customerEnrollment = null;
		if (customerOrderHd != null) {
			customerEnrollment = customerEnrollmentDao
					.getCustomerEnrollmentByCustomerId(customerOrderHd.getCustomerId());
		}
		if (validateBalanceDueAfterPayment.compareTo(BigDecimal.ZERO) <= 0) {
			if (customerEnrollment != null) {
			   customerEnrollmentService.sendMailToSalesByUserId(customerEnrollment.getSalesFollowBy(), customerEnrollment.getCustomerId());
			}
		}
	}
	public void handleRenewPayment(CustomerOrderHd customerOrderHd) {
		CustomerServiceAcc csAcc = customerServiceDao.getLatestCustomerService(customerOrderHd.getCustomerId());
		/*
		 * setEffectiveDate1 if the status is ACT,then set effectiveDate is getExpiryDate+1,set ExpiryDate is effectiveDate + servicePlan's
		 * contract_length_month2 if the status is EXP,then set effectiveDate is the first of next month,set ExpiryDate is effectiveDate +
		 * servicePlan's contract_length_month
		 */
		if (null != csAcc) {
			CustomerServiceAcc newCustomerServiceAcc = customerServiceDao.getUniqueByCol(
					CustomerServiceAcc.class,
					"orderNo",
					customerOrderHd.getOrderNo());
			CustomerServiceSubscribe customerServiceSubscribe = customerServiceSubscribeDao.getUniqueByCol(
					CustomerServiceSubscribe.class,
					"id.accNo",
					newCustomerServiceAcc.getAccNo());
			ServicePlan renewalServicePlan = servicePlanDao.get(ServicePlan.class, customerServiceSubscribe.getId().getServicePlanNo());
			Calendar calendar = Calendar.getInstance();
			if (CustomerServiceStatus.ACT.getDesc().equals(csAcc.getStatus())) {
				calendar.setTime(csAcc.getExpiryDate());
				calendar.add(Calendar.DAY_OF_MONTH, 1);
				// setEffectiveDate
				newCustomerServiceAcc.setEffectiveDate(calendar.getTime());
				if (renewalServicePlan.getContractLengthMonth() != null) {
					calendar.add(Calendar.MONTH, renewalServicePlan.getContractLengthMonth().intValue());
				}
				// setEffectiveDate
				newCustomerServiceAcc.setExpiryDate(calendar.getTime());
			} else if (CustomerServiceStatus.EXP.getDesc().equals(csAcc.getStatus())) {
				calendar.setTime(new Date());
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.add(Calendar.MONTH, 1);
				// setEffectiveDate
				newCustomerServiceAcc.setEffectiveDate(calendar.getTime());
				if (renewalServicePlan.getContractLengthMonth() != null) {
					calendar.add(Calendar.MONTH, renewalServicePlan.getContractLengthMonth().intValue());
				}
				// setEffectiveDate
				newCustomerServiceAcc.setExpiryDate(calendar.getTime());
			}
		}
	}

	public void processDaypassPurchase(List<CustomerOrderPermitCard> cards, CustomerOrderHd customerOrderHd, CustomerOrderTrans customerOrderTrans,
			String userId) {
		for (CustomerOrderPermitCard card : cards) {// update status of daypass
			card.setStatus(Constant.Status.RA.name());
			customerOrderPermitCardDao.update(card);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			ServicePlan daypass = servicePlanDao.get(ServicePlan.class, card.getServicePlanNo());
			MessageTemplate mt = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_DAYPASS_PURCHASE_NOTIFY);
			CustomerProfile cp = customerProfileDao.get(CustomerProfile.class, customerOrderHd.getCustomerId());
			try {
				TransactionEmailDto emailDto = new TransactionEmailDto();
				emailDto.setEmailContent(mt
						.getContentHtml()
						.replaceAll(Constant.MessageTemplate.UserName.getName(), cp.getSalutation() + " " + cp.getGivenName() + " " + cp.getSurname())
						.replaceAll(Constant.MessageTemplate.Confirmation.getName(), customerOrderHd.getOrderNo() + "")
						.replaceAll(Constant.MessageTemplate.StartTime.getName(), sdf.format(card.getEffectiveFrom()))
						.replaceAll(Constant.MessageTemplate.EndTime.getName(), sdf.format(card.getEffectiveTo()))
						.replaceAll(Constant.MessageTemplate.OffPass.getName(), cards.size() + "")
						.replaceAll(Constant.MessageTemplate.Type.getName(), daypass.getServicePlanPoses().get(0).getAgeRange()));
				emailDto.setOrderNO(customerOrderHd.getOrderNo());
				emailDto.setEmailType(Constant.TEMPLATE_ID_DAYPASS_RECEIPT);
				emailDto.setSendTo(cp.getContactEmail());
				emailDto.setSubject(mt.getMessageSubject());
				emailDto.setTransactionNO(customerOrderTrans.getTransactionNo());
				LoginUserDto userDto = new LoginUserDto();
				userDto.setUserId(userId);
				userDto.setUserName("HKGTA");
				customerOrderTransService.sentTransactionEmail(emailDto, userDto);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Transactional
	public void processExceptionFailedPayment(CustomerOrderTrans customerOrderTrans) {
		Long gatewayId = Long.parseLong(appProps.getProperty("hkgta.paymentgateway.gatewayId"));// The test environment is 1,the truth environment is
																								// 2
		PaymentGateway paymentGateway = paymentGatewayDao.get(PaymentGateway.class, gatewayId);
		String cmdId = PaymentCmd.VOIDPURCHASE.getDesc();
		PaymentGatewayCmdPK pk = new PaymentGatewayCmdPK(gatewayId, cmdId);
		PaymentGatewayCmd paymentGatewayCmd = paymentGatewayCmdDao.get(PaymentGatewayCmd.class, pk);

		String vpc_AccessCode = paymentGateway.getAccessCode();
		String vpc_Version = appProps.getProperty("hkgta.paymentgateway.vpc_Version");// "1";
		String vpc_Command = paymentGatewayCmd.getCommandType();
		String vpc_Merchant = paymentGateway.getMerchantId();
		String vpc_MerchTxnRef = customerOrderTrans.getTransactionNo().toString();
		String vpc_User = paymentGateway.getOperatorId();
		String vpc_Password = paymentGateway.getOperatorPassword();
		String vpcURL = paymentGateway.getGatewayUrl() + "/" + paymentGatewayCmd.getUrlCmd();
		Map<String, String> requestFields = new HashMap<String, String>();
		requestFields.put("vpc_AccessCode", vpc_AccessCode);
		requestFields.put("vpc_Version", vpc_Version);
		requestFields.put("vpc_Command", vpc_Command);
		requestFields.put("vpc_Merchant", vpc_Merchant);
		requestFields.put("vpc_Password", vpc_Password);
		requestFields.put("vpc_MerchTxnRef", vpc_MerchTxnRef);
		requestFields.put("vpc_User", vpc_User);
		requestFields.put("vpc_Command", PaymentCmdType.VOIDPURCHASE.getDesc());
		requestFields.put("vpc_TransNo", customerOrderTrans.getAgentTransactionNo());
		requestFields.put("vpcURL", vpcURL);
		customerOrderTrans = voidpurchcase(requestFields, customerOrderTrans);
	}

	@Transactional
	public void processCourseAndTennisPayment(CustomerOrderHd customerOrderHd, String userId) {
		Date currentDate = new Date();
		CourseEnrollment courseEnrollment = courseEnrollmentDao.getCourseEnrollmentByOrderNo(customerOrderHd.getOrderNo());
		courseEnrollment.setStatus(EnrollCourseStatus.REG.getDesc());
		courseEnrollment.setUpdateBy(userId);
		courseEnrollment.setUpdateDate(currentDate);
		courseEnrollment.setStatusUpdateDate(currentDate);
		courseEnrollment.setStatusUpdateBy(userId);
		courseEnrollmentDao.updateCourseEnrollment(courseEnrollment);
		try {
			CustomerEmailContent customerEmailContent = courseEnrollmentService.getCourseEnrollEmailContent(customerOrderHd.getOrderNo());
			if (customerEmailContent != null) {
				List<CustomerOrderTrans> customerOrderTranses = customerOrderTransDao.getPaymentDetailsByOrderNo(customerOrderHd.getOrderNo());
				CustomerOrderTrans customerOrderTrans = customerOrderTranses.get(0);// Course Payment Order and transaction is one to one.
				try {
					byte[] attachment = customerOrderTransDao.getInvoiceReceipt(null, customerOrderTrans.getTransactionNo().toString(), "course");
					List<byte[]> attachmentList = Arrays.asList(attachment);
					List<String> mineTypeList = Arrays.asList("application/pdf");
					List<String> fileNameList = Arrays.asList("CourseEnrollmentReceipt-" + customerOrderTrans.getTransactionNo().toString() + ".pdf");
					mailThreadService.sendWithResponse(customerEmailContent, attachmentList, mineTypeList, fileNameList);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		// send sms confirm
		sendCourseEnrollConfirmBySms(courseEnrollment);
	}

	@SuppressWarnings("unchecked")
	private void sendCourseEnrollConfirmBySms(CourseEnrollment courseEnrollment) {

		// send sms info
		try {
			Map<String, Object> smsInfo = null;
			CourseMaster course = courseDao.getCourseMasterById(courseEnrollment.getCourseId());
			if (course == null)
				return;
			if (MemberAcceptance.AUTO.getType().equals(course.getMemberAcceptance())) {
				smsInfo = courseEnrollmentService.notifyEnrollMember(SceneType.COURSE_ENROLL_AUTO_CONFIRM, courseEnrollment, course);
			} else if (MemberAcceptance.MANUAL.getType().equals(course.getMemberAcceptance())) {
				smsInfo = courseEnrollmentService.notifyEnrollMember(SceneType.COURSE_ENROLL_MANUAL_CONFIRM, courseEnrollment, course);
			}
			if (smsInfo != null) {
				List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
				boolean sendSMS=userPreferenceSettingService.sendSMSByCustomerId((Long)smsInfo.get("customerId"));
				if(sendSMS){
					String message = (String) smsInfo.get("message");
					smsService.sendSMS(phoneNumbers, message, new Date());
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Transactional
	public void processTopUp(CustomerOrderTrans customerOrderTrans) {
		CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
		Long customerId = customerOrderHd.getCustomerId();
		Member member = memberDao.get(Member.class, customerId);
		Long superiorMemberId = member.getSuperiorMemberId();
		MemberCashvalue memberCashvalue = null;
		if (superiorMemberId != null) {
			memberCashvalue = memberCashValueDao.getByCustomerId(superiorMemberId);
		} else {
			memberCashvalue = memberCashValueDao.getByCustomerId(customerId);
		}
		// update Member's cash value.
		BigDecimal topUpAmount = new BigDecimal(customerOrderTrans.getPaidAmount().doubleValue());
		Date currentDate = new Date();
		if (memberCashvalue == null) {
			memberCashvalue = new MemberCashvalue();
			memberCashvalue.setInitialDate(currentDate);
			memberCashvalue.setInitialValue(topUpAmount);
			memberCashvalue.setExchgFactor(new BigDecimal(1));
			memberCashvalue.setInternalRemark("");
			if (superiorMemberId != null) {
				// if current member is a dependent member,should insert superior member's customer id to cashvalue_topup_history.
				memberCashvalue.setCustomerId(superiorMemberId);
			} else {
				memberCashvalue.setCustomerId(customerId);
			}
			memberCashvalue.setAvailableBalance(topUpAmount);
			memberCashValueDao.saveMemberCashValue(memberCashvalue);
		} else {
			BigDecimal updatedAmount = memberCashvalue.getAvailableBalance().add(topUpAmount);
			memberCashvalue.setAvailableBalance(updatedAmount);
			memberCashValueDao.updateMemberCashValue(memberCashvalue);
		}
		// Save CashvalueTopupHistory record.
		CashvalueTopupHistory topupHistory = new CashvalueTopupHistory();
		topupHistory.setAmount(topUpAmount);
		if (null != memberCashvalue && null != memberCashvalue.getExchgFactor()) {
			topupHistory.setCashvalueAmt(topUpAmount.multiply(memberCashvalue.getExchgFactor()));
		} else {
			topupHistory.setCashvalueAmt(topUpAmount);
		}
		topupHistory.setCustomerId(memberCashvalue.getCustomerId());// if current member is a dependent member,should insert superior member's
																	// customer id to cashvalue_topup_history.
		topupHistory.setSuccessDate(currentDate);
		topupHistory.setTopupDate(currentDate);
		topupHistory.setCreateDate(currentDate);
		topupHistory.setTopupMethod(customerOrderTrans.getPaymentMethodCode());
		// SAM 20161013 add: should set topup status and transaction no. as well
		topupHistory.setStatus(Constant.Status.SUC.toString() );
		topupHistory.setTransactionNo(customerOrderTrans.getTransactionNo());
				
		topupHistoryDao.save(topupHistory);

		// Send receipt email to customer.
		TopUpDto topUpDto = new TopUpDto();
		topUpDto.setCustomerId(customerOrderHd.getCustomerId());
		topUpDto.setTransactionNo(customerOrderTrans.getTransactionNo());
		topUpDto.setPaymentMethod(customerOrderTrans.getPaymentMethodCode());
		sentTopupEmail(topUpDto);
	}

	@Transactional
	public void sentTopupEmail(TopUpDto topUpDto) {
		MessageTemplate template = messageTemplateDao.getTemplateByFunctionId(Constant.TEMPLATE_ID_TOPUP);
		CustomerProfile customerProfile = customerProfileDao.get(CustomerProfile.class, topUpDto.getCustomerId());
		String to = customerProfile.getContactEmail();
		String copyto = null;
		String subject = template.getMessageSubject();
		String username = customerProfile.getSalutation() + " " + customerProfile.getGivenName() + " " + customerProfile.getSurname();
		String content = template.getContentHtml();
		content = content.replaceAll(MessageTemplateParams.UserName.getParam(), username);
		content = content.replaceAll(MessageTemplateParams.TopupMethod.getParam(), topUpDto.getPaymentMethod());
		Member member = memberDao.get(Member.class, topUpDto.getCustomerId());
		MemberCashvalue memberCashvalue = null;
		// if current member is a dependent member,should get superior member's cashvalue balance.
		if (null == member.getSuperiorMemberId()) {
			memberCashvalue = memberCashValueDao.getByCustomerId(topUpDto.getCustomerId());
		} else {
			memberCashvalue = memberCashValueDao.getByCustomerId(member.getSuperiorMemberId());
			CustomerProfile superiorCustomer = customerProfileDao.get(CustomerProfile.class, member.getSuperiorMemberId());
			copyto = superiorCustomer.getContactEmail();
		}
		String currentBalance=null;
		/**
		 * SGG-3320
		 * if getAvailableBalance less 0
		   the minus sign should be placed before currency sign. i.e. "-HK$99.99" not "HK$-99.99 
		 */
		if(memberCashvalue.getAvailableBalance().compareTo(BigDecimal.ZERO)<0)
		{
			currentBalance =  "-HK\\$ "+new DecimalFormat("#,###.00").format(memberCashvalue.getAvailableBalance().abs());
		}else{
			 currentBalance =  "HK\\$ "+new DecimalFormat("#,###.00").format(memberCashvalue.getAvailableBalance());
		}
		content = content.replaceAll(MessageTemplateParams.CurrentBalance.getParam(), currentBalance);
		byte[] receiptAttach = null;
		try {
			receiptAttach = customerOrderTransDao.getInvoiceReceipt(null, topUpDto.getTransactionNo().toString(), "topup");
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<byte[]> attachList = new ArrayList<byte[]>();
		attachList.add(receiptAttach);
		List<String> fileNameList = new ArrayList<String>();
		fileNameList.add("HKGTA_Receipt-" + topUpDto.getTransactionNo().toString() + ".pdf");
		List<String> mineTypeList = new ArrayList<String>();
		mineTypeList.add("application/pdf");
		try {
			// MailSender.sendEmailWithBytesMineAttach(to, copyto,null,subject,content, mineTypeList,attachList,fileNameList);
			CustomerEmailContent customerEmailContent = new CustomerEmailContent();

			customerEmailContent.setContent(content);
			customerEmailContent.setRecipientCustomerId(member.getCustomerId().toString());
			customerEmailContent.setRecipientEmail(to);
			customerEmailContent.setSendDate(new Date());
			customerEmailContent.setSenderUserId(topUpDto.getStaffUserId());
			customerEmailContent.setSubject(subject);
			customerEmailContent.setCopyto(copyto);
			customerEmailContent.setNoticeType(Constant.NOTICE_TYPE_ACCOUNT);
			customerEmailContentDao.save(customerEmailContent);

			CustomerEmailAttach customerEmailAttach = new CustomerEmailAttach();
			customerEmailAttach.setEmailSendId(customerEmailContent.getSendId());
			customerEmailAttach.setAttachmentName(fileNameList.get(0));
			customerEmailAttachDao.save(customerEmailAttach);
			mailThreadService.sendWithResponse(customerEmailContent, attachList, mineTypeList, fileNameList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string "No Value Returned", else returns
	 * input
	 * @param in String containing the data String
	 * @return String containing the output String
	 */
	private String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return "No Value Returned";
		} else {
			return in;
		}
	} // null2unknown()

	@SuppressWarnings({ "rawtypes", "unused" })
	@Override
	@Transactional
	public CustomerOrderTrans queryResult(CustomerOrderTrans customerOrderTrans) {
		logger.info("============================Query pending transaction result start================TransactionNo="
				+ customerOrderTrans.getTransactionNo() + "========================");
		// If using a proxy server you must set the following variables
		// If NOT using a proxy server then set the 'useProxy' to false
		Long gatewayId = Long.parseLong(appProps.getProperty("hkgta.paymentgateway.gatewayId"));// The test environment is 1,the truth environment is
																								// 2
		String cmdId = PaymentCmd.QUERYDR.getDesc();
		PaymentGatewayCmdPK pk = new PaymentGatewayCmdPK(gatewayId, cmdId);
		PaymentGateway paymentGateway = paymentGatewayDao.get(PaymentGateway.class, gatewayId);
		PaymentGatewayCmd paymentGatewayCmd = paymentGatewayCmdDao.get(PaymentGatewayCmd.class, pk);

		String vpc_AccessCode = paymentGateway.getAccessCode();
		String vpc_Version = appProps.getProperty("hkgta.paymentgateway.vpc_Version");// "1";
		String vpc_Command = paymentGatewayCmd.getCommandType();
		String vpc_Merchant = paymentGateway.getMerchantId();
		String vpc_MerchTxnRef = customerOrderTrans.getTransactionNo().toString();
		String vpc_User = paymentGateway.getOperatorId();
		String vpc_Password = paymentGateway.getOperatorPassword();
		boolean useProxy = false;

		// You would need to add you own proxy server URL and port here
		String proxyHost = appProps.getProperty("hkgta.paymentgateway.proxyHost");
		int proxyPort = Integer.parseInt(appProps.getProperty("hkgta.paymentgateway.proxyPort"));

		// retrieve all the parameters into a hash map
		Map<String, String> requestFields = new HashMap<String, String>();
		requestFields.put("vpc_AccessCode", vpc_AccessCode);
		requestFields.put("vpc_Version", vpc_Version);
		requestFields.put("vpc_Command", vpc_Command);
		requestFields.put("vpc_Merchant", vpc_Merchant);
		requestFields.put("vpc_Password", vpc_Password);
		requestFields.put("vpc_MerchTxnRef", vpc_MerchTxnRef);
		requestFields.put("vpc_User", vpc_User);
		// no need to send the vpcURL, Title and Submit Button to the vpc
		String vpcURL = paymentGateway.getGatewayUrl() + "/" + paymentGatewayCmd.getUrlCmd();

		// create the post data string to send
		String postData = createPostDataFromMap(requestFields);

		String resQS = "";
		String message = "";
		logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST", null));
		 
		try {
			// create a URL connection to the Virtual Payment Client
			resQS = doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);

		} catch (Exception ex) {
			// The response is an error message so generate an Error Page
			message = ex.toString();
			logger.error(Log4jFormatUtil.logErrorMessage(vpcURL, postData, "POST", message, ex.getMessage()));
		} // try-catch

		// create a hash map for the response data
		Map<String, String> responseFields = createMapFromResponse(resQS);

		logger.info(Log4jFormatUtil.logInfoMessage(vpcURL, postData, "POST", responseFields.toString()));
		// Extract the available receipt fields from the VPC Response
		// If not present then let the value be equal to 'No Value returned'
		// Not all data fields will return values for all transactions.

		// don't overwrite message if any error messages detected
		if (message.length() == 0) {
			message = null2unknown("vpc_Message", responseFields);
		}

		// Standard Receipt Data
		// merchTxnRef not always returned in response if no receipt so get
		// input
		// String merchTxnRef = request.getParameter("vpc_MerchTxnRef");
		String merchTxnRef = customerOrderTrans.getTransactionNo().toString();

		String amount = null2unknown("vpc_Amount", responseFields);
		String locale = null2unknown("vpc_Locale", responseFields);
		String batchNo = null2unknown("vpc_BatchNo", responseFields);
		String command = null2unknown("vpc_Command", responseFields);
		String version = null2unknown("vpc_Version", responseFields);
		String cardType = null2unknown("vpc_Card", responseFields);
		String orderInfo = null2unknown("vpc_OrderInfo", responseFields);
		String receiptNo = null2unknown("vpc_ReceiptNo", responseFields);
		String merchantID = null2unknown("vpc_Merchant", responseFields);
		String authorizeID = null2unknown("vpc_AuthorizeId", responseFields);
		String transactionNo = null2unknown("vpc_TransactionNo", responseFields);
		String acqResponseCode = null2unknown("vpc_AcqResponseCode", responseFields);
		String txnResponseCode = null2unknown("vpc_TxnResponseCode", responseFields);

		// CSC Receipt Data
		String cscResultCode = null2unknown("vpc_CSCResultCode", responseFields);
		String cscRequestCode = null2unknown("vpc_CSCRequestCode", responseFields);
		String cscACQRespCode = null2unknown("vpc_AcqCSCRespCode", responseFields);

		// AVS Receipt Data
		String avs_City = null2unknown("vpc_AVS_City", responseFields);
		String avs_Country = null2unknown("vpc_AVS_Country", responseFields);
		String avs_Street01 = null2unknown("vpc_AVS_Street01", responseFields);
		String avs_PostCode = null2unknown("vpc_AVS_PostCode", responseFields);
		String avs_StateProv = null2unknown("vpc_AVS_StateProv", responseFields);
		String avsResultCode = null2unknown("vpc_AVSResultCode", responseFields);
		String avsRequestCode = null2unknown("vpc_AVSRequestCode", responseFields);
		String avsACQRespCode = null2unknown("vpc_AcqAVSRespCode", responseFields);

		// 3-D Secure Data
		String transType3DS = null2unknown("vpc_VerType", responseFields);
		String verStatus3DS = null2unknown("vpc_VerStatus", responseFields);
		String token3DS = null2unknown("vpc_VerToken", responseFields);
		String secureLevel3DS = null2unknown("vpc_VerSecurityLevel", responseFields);
		String enrolled3DS = null2unknown("vpc_3DSenrolled", responseFields);
		String xid3DS = null2unknown("vpc_3DSXID", responseFields);
		String eci3DS = null2unknown("vpc_3DSECI", responseFields);
		String status3DS = null2unknown("vpc_3DSstatus", responseFields);

		// Financial Transaction Data
		String shopTransNo = null2unknown("vpc_ShopTransactionNo", responseFields);
		String authorisedAmount = null2unknown("vpc_AuthorisedAmount", responseFields);
		String capturedAmount = null2unknown("vpc_CapturedAmount", responseFields);
		String refundedAmount = null2unknown("vpc_RefundedAmount", responseFields);
		String ticketNumber = null2unknown("vpc_TicketNo", responseFields);

		// Specific QueryDR Data
		String drExists = null2unknown("vpc_DRExists", responseFields);
		String multipleDRs = null2unknown("vpc_FoundMultipleDRs", responseFields);

		String error = "";
		// Show this page as an error page if error condition
		/*
		 * if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) { error = "Error "; logger.error("Query failed!");
		 * customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc()); return customerOrderTrans; }
		 */
		if (!"0".equals(txnResponseCode)) {
			customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
			customerOrderTrans.setOpgResponseCode(txnResponseCode);
			customerOrderTrans.setAgentTransactionNo(transactionNo);
			customerOrderTrans.setUpdateBy("System");
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
			return customerOrderTrans;

		}
		// Date transactionDate = customerOrderTrans.getTransactionTimestamp();
		// Calendar calendar = Calendar.getInstance();
		// calendar.setTime(transactionDate);
		// calendar.add(Calendar.MINUTE, paymentGateway.getTimeoutMin().intValue());
		// Date timeout = calendar.getTime();
		// String hm = DateConvertUtil.date2String(timeout, "HH:mm");
		// if (hm.replaceAll(":", "").compareTo(paymentGateway.getSettlementCutoffTime().toString())>=0) {
		// requestFields.put("vpc_Command", PaymentCmdType.REFUND.getDesc());
		// requestFields.put("vpc_Amount", customerOrderTrans.getPaidAmount().longValue()*100+"");
		// requestFields.put("vpc_TransNo", transactionNo);
		// requestFields.put("vpcURL", vpcURL);
		// customerOrderTrans = refund(requestFields,customerOrderTrans);
		// }else {
		requestFields.put("vpc_Command", PaymentCmdType.VOIDPURCHASE.getDesc());
		requestFields.put("vpc_TransNo", transactionNo);
		requestFields.put("vpcURL", vpcURL);
		customerOrderTrans = voidpurchcase(requestFields, customerOrderTrans);
		// }
		logger.info("============================Query pending transaction result end===============TransactionNo="
				+ customerOrderTrans.getTransactionNo() + "=========================");
		return customerOrderTrans;
	}

	/**
	 * The method is used to refund when bank callback timeout after 10:00pm
	 */
	@SuppressWarnings("unused")
	@Transactional
	public CustomerOrderTrans refund(Map<String, String> requestFields, CustomerOrderTrans customerOrderTrans) {
		logger.info("============================Refund pending transaction result start===============TransactionNo="
				+ customerOrderTrans.getTransactionNo() + "=========================");
		// Define Variables
		// If using a proxy server you must set the following variables
		// If NOT using a proxy server then set the 'useProxy' to false
		boolean useProxy = !true;
		String proxyHost = appProps.getProperty("hkgta.paymentgateway.proxyHost");
		int proxyPort = Integer.parseInt(appProps.getProperty("hkgta.paymentgateway.proxyPort"));
		String vpcURL = requestFields.get("vpcURL");

		// create the post data string to send
		String postData = createPostDataFromMap(requestFields);

		String resQS = "";
		String message = "";

		try {
			// create a URL connection to the Virtual Payment Client
			resQS = doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);

		} catch (Exception ex) {
			// The response is an error message so generate an Error Page
			message = ex.toString();
		} // try-catch

		// create a hash map for the response data
		Map<String, String> responseFields = createMapFromResponse(resQS);

		// Extract the available receipt fields from the VPC Response
		// If not present then let the value be equal to 'No Value returned'
		// Not all data fields will return values for all transactions.

		// don't overwrite message if any error messages detected
		if (message.length() == 0) {
			message = null2unknown("vpc_Message", responseFields);
		}

		// Standard Receipt Data
		String amount = null2unknown("vpc_Amount", responseFields);
		String locale = null2unknown("vpc_Locale", responseFields);
		String batchNo = null2unknown("vpc_BatchNo", responseFields);
		String command = null2unknown("vpc_Command", responseFields);
		String version = null2unknown("vpc_Version", responseFields);
		String cardType = null2unknown("vpc_Card", responseFields);
		String orderInfo = null2unknown("vpc_OrderInfo", responseFields);
		String receiptNo = null2unknown("vpc_ReceiptNo", responseFields);
		String merchantID = null2unknown("vpc_Merchant", responseFields);
		// String merchTxnRef = null2unknown("vpc_MerchTxnRef", responseFields);
		String authorizeID = null2unknown("vpc_AuthorizeId", responseFields);
		String transactionNr = null2unknown("vpc_TransactionNo", responseFields);
		String acqResponseCode = null2unknown("vpc_AcqResponseCode", responseFields);
		String txnResponseCode = null2unknown("vpc_TxnResponseCode", responseFields);

		// Capture Data
		String shopTransNo = null2unknown("vpc_ShopTransactionNo", responseFields);
		String authorisedAmount = null2unknown("vpc_AuthorisedAmount", responseFields);
		String capturedAmount = null2unknown("vpc_CapturedAmount", responseFields);
		String refundedAmount = null2unknown("vpc_RefundedAmount", responseFields);
		String ticketNumber = null2unknown("vpc_TicketNo", responseFields);

		String error = "";
		// Show this page as an error page if error condition
		if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) {
			error = "Error ";
			logger.error("Occur error!");
		}
		if ("0".equals(txnResponseCode)) {
			CustomerOrderTrans newCustomerOrderTrans = new CustomerOrderTrans();
			initCustomerOrderTrans(customerOrderTrans);
			newCustomerOrderTrans.setCustomerOrderHd(customerOrderTrans.getCustomerOrderHd());
			Long gatewayId = Long.parseLong(appProps.getProperty("hkgta.paymentgateway.gatewayId"));// The test environment is 1,the truth environment
																									// is 2
			if (1l == gatewayId) {
				newCustomerOrderTrans.setPaidAmount(new BigDecimal(Long.parseLong(refundedAmount) / 100));
			} else {
				newCustomerOrderTrans.setPaidAmount(new BigDecimal(Long.parseLong(refundedAmount)));
			}
			newCustomerOrderTrans.setStatus(CustomerTransationStatus.REFUND.getDesc());
			newCustomerOrderTrans.setTransactionTimestamp(new Date());
			newCustomerOrderTrans.setAgentTransactionNo(transactionNr);
			newCustomerOrderTrans.setPaymentMethodCode(PaymentMethod.VISA.name());
			newCustomerOrderTrans.setOpgResponseCode(txnResponseCode);
			newCustomerOrderTrans.setFromTransactionNo(customerOrderTrans.getTransactionNo());
			newCustomerOrderTrans.setCreateBy("System");
			customerOrderTransDao.save(newCustomerOrderTrans);
			customerOrderTrans.setStatus(CustomerTransationStatus.PR.getDesc());
			customerOrderTrans.setInternalRemark(customerOrderTrans.getInternalRemark() + " Refund for payment gateway time out");
			customerOrderTrans.setUpdateBy("System");
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
		}
		logger.info("============================Refund pending transaction result end===============TransactionNo="
				+ customerOrderTrans.getTransactionNo() + "=========================");
		return customerOrderTrans;
	}

	/**
	 * The method is used to voidpurchcase when bank callback timeout before 10:00pm
	 */
	@SuppressWarnings("unused")
	public CustomerOrderTrans voidpurchcase(Map<String, String> requestFields, CustomerOrderTrans customerOrderTrans) {

		logger.info("============================Viod pending transaction  start================TransactionNo="
				+ customerOrderTrans.getTransactionNo() + "========================");
		// Define Variables
		// If using a proxy server you must set the following variables
		// If NOT using a proxy server then set the 'useProxy' to false
		boolean useProxy = false;
		String proxyHost = appProps.getProperty("hkgta.paymentgateway.proxyHost");
		int proxyPort = Integer.parseInt(appProps.getProperty("hkgta.paymentgateway.proxyPort"));
		String vpcURL = requestFields.get("vpcURL");

		// create the post data string to send
		String postData = createPostDataFromMap(requestFields);

		String resQS = "";
		String message = "";
	
		try {
			// create a URL connection to the Virtual Payment Client
			resQS = doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);

		} catch (Exception ex) {
			// The response is an error message so generate an Error Page
			message = ex.toString();
		} // try-catch

		// create a hash map for the response data
		Map<String, String> responseFields = createMapFromResponse(resQS);

		// Extract the available receipt fields from the VPC Response
		// If not present then let the value be equal to 'No Value returned'
		// Not all data fields will return values for all transactions.

		// don't overwrite message if any error messages detected
		if (message.length() == 0) {
			message = null2unknown("vpc_Message", responseFields);
		}

		// Standard Receipt Data
		String amount = null2unknown("vpc_Amount", responseFields);
		String locale = null2unknown("vpc_Locale", responseFields);
		String batchNo = null2unknown("vpc_BatchNo", responseFields);
		String command = null2unknown("vpc_Command", responseFields);
		String version = null2unknown("vpc_Version", responseFields);
		String cardType = null2unknown("vpc_Card", responseFields);
		String orderInfo = null2unknown("vpc_OrderInfo", responseFields);
		String receiptNo = null2unknown("vpc_ReceiptNo", responseFields);
		String merchantID = null2unknown("vpc_Merchant", responseFields);
		// String merchTxnRef = null2unknown("vpc_MerchTxnRef", responseFields);
		String authorizeID = null2unknown("vpc_AuthorizeId", responseFields);
		String transactionNr = null2unknown("vpc_TransactionNo", responseFields);
		String acqResponseCode = null2unknown("vpc_AcqResponseCode", responseFields);
		String txnResponseCode = null2unknown("vpc_TxnResponseCode", responseFields);

		// Void Purchase Data
		String shopTransNo = null2unknown("vpc_ShopTransactionNo", responseFields);
		String authorisedAmount = null2unknown("vpc_AuthorisedAmount", responseFields);
		String capturedAmount = null2unknown("vpc_CapturedAmount", responseFields);
		String refundedAmount = null2unknown("vpc_RefundedAmount", responseFields);
		String ticketNumber = null2unknown("vpc_TicketNo", responseFields);

		String error = "";
		// Show this page as an error page if error condition
		if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) {
			error = "Error ";
		}
		if ("0".equals(txnResponseCode)) {
			customerOrderTrans.setStatus(CustomerTransationStatus.VOID.getDesc());
			customerOrderTrans.setOpgResponseCode(txnResponseCode);
			customerOrderTrans.setAgentTransactionNo(transactionNr);
			customerOrderTrans.setUpdateBy("System");
			customerOrderTrans.setUpdateDate(new Timestamp(new Date().getTime()));
		} else {
			requestFields.put("vpc_Command", PaymentCmdType.REFUND.getDesc());
			Long gatewayId = Long.parseLong(appProps.getProperty("hkgta.paymentgateway.gatewayId"));// The test environment is 1,the truth environment
																									// is 2
			if (1l == gatewayId) {
				requestFields.put("vpc_Amount", customerOrderTrans.getPaidAmount().longValue() * 100 + "");
			} else {
				requestFields.put("vpc_Amount", customerOrderTrans.getPaidAmount().longValue() + "");
			}
			customerOrderTrans = refund(requestFields, customerOrderTrans);
		}
		logger.info("============================Void pending transaction end===============TransactionNo=" + customerOrderTrans.getTransactionNo()
				+ "=========================");
		return customerOrderTrans;
	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string "No Value Returned", else returns
	 * input
	 * @param in String containing the data String
	 * @return String containing the output String
	 */
	private static String null2unknown(String in, Map<String, String> responseFields) {
		if (in == null || in.length() == 0 || (String) responseFields.get(in) == null) {
			return null;
		} else {
			return (String) responseFields.get(in);
		}
	} // null2unknown()

	/**
	 * This method is for creating a URL POST data string.
	 *
	 * @param fields
	 *            is the input parameters from the order page
	 * @return is the output String containing POST data key value pairs
	 */
	private String createPostDataFromMap(Map<String, String> fields) {
		StringBuffer buf = new StringBuffer();

		String ampersand = "";

		// append all fields in a data string
		for (Iterator<String> i = fields.keySet().iterator(); i.hasNext();) {

			String key = (String) i.next();
			String value = (String) fields.get(key);

			if ((value != null) && (value.length() > 0)) {
				// append the parameters
				buf.append(ampersand);
				buf.append(URLEncoder.encode(key));
				buf.append('=');
				buf.append(URLEncoder.encode(value));
			}
			ampersand = "&";
		}

		// return string
		return buf.toString();
	}

	/**
	 * This method is for creating a URL POST data string.
	 *
	 * @param queryString
	 *            is the input String from POST data response
	 * @return is a Hashmap of Post data response inputs
	 */
	private Map<String, String> createMapFromResponse(String queryString) {
		Map<String, String> map = new HashMap<String, String>();
		StringTokenizer st = new StringTokenizer(queryString, "&");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			int i = token.indexOf('=');
			if (i > 0) {
				try {
					String key = token.substring(0, i);
					String value = URLDecoder.decode(token.substring(i + 1, token.length()));
					map.put(key, value);
				} catch (Exception ex) {
				}
			}
		}
		return map;
	}

	/**
	 * This method is for performing a Form POST operation from input data parameters.
	 *
	 * @param vpc_Host
	 *            - is a String containing the vpc URL
	 * @param data
	 *            - is a String containing the post data key value pairs
	 * @param useProxy
	 *            - is a boolean indicating if a Proxy Server is involved in the transfer
	 * @param proxyHost
	 *            - is a String containing the IP address of the Proxy to send the data to
	 * @param proxyPort
	 *            - is an integer containing the port number of the Proxy socket listener
	 * @return - is body data of the POST data response
	 */
	public static String doPost(String vpc_Host, String data, boolean useProxy, String proxyHost, int proxyPort) throws IOException {
		Socket s=null;
		SSLSocket ssl =null;
		InputStream is=null;
		OutputStream os=null;
		int vpc_Port = 443;
		String fileName = "";
		boolean useSSL = false;

		// determine if SSL encryption is being used
		if (vpc_Host.substring(0, 8).equalsIgnoreCase("HTTPS://")) {
			useSSL = true;
			// remove 'HTTPS://' from host URL
			vpc_Host = vpc_Host.substring(8);
			// get the filename from the last section of vpc_URL
			fileName = vpc_Host.substring(vpc_Host.lastIndexOf("/"));
			// get the IP address of the VPC machine
			vpc_Host = vpc_Host.substring(0, vpc_Host.lastIndexOf("/"));
		}

		// use the next block of code if using a proxy server
		if (useProxy) {
			s = new Socket(proxyHost, proxyPort);
			os = s.getOutputStream();
			is = s.getInputStream();
			// use next block of code if using SSL encryption
			if (useSSL) {
				String msg = "CONNECT " + vpc_Host + ":" + vpc_Port + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n\r\n";
				os.write(msg.getBytes());
				byte[] buf = new byte[4096];
				int len = is.read(buf);
				String res = new String(buf, 0, len);

				// check if a successful HTTP connection
				if (res.indexOf("200") < 0) {
					throw new IOException("Proxy would now allow connection - " + res);
				}

				// write output to VPC
				ssl = (SSLSocket) s_sslSocketFactory.createSocket(s, vpc_Host, vpc_Port, true);
				ssl.startHandshake();
				os = ssl.getOutputStream();
				// get response data from VPC
				is = ssl.getInputStream();
				// use the next block of code if NOT using SSL encryption
			} else {
				fileName = vpc_Host;
			}
			// use the next block of code if NOT using a proxy server
		} else {
			// use next block of code if using SSL encryption
			if (useSSL) {
				s = s_sslSocketFactory.createSocket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
				// use next block of code if NOT using SSL encryption
			} else {
				s = new Socket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
			}
		}

		String req = "POST " + fileName + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n" + "Content-Type: application/x-www-form-urlencoded\r\n"
				+ "Content-Length: " + data.length() + "\r\n\r\n" + data;

		os.write(req.getBytes());
		String res = new String(readAll(is));

		// check if a successful connection
		if (res.indexOf("200") < 0) {
			throw new IOException("Connection Refused - " + res);
		}

		if (res.indexOf("404 Not Found") > 0) {
			throw new IOException("File Not Found Error - " + res);
		}

		int resIndex = res.indexOf("\r\n\r\n");
		String body = res.substring(resIndex + 4, res.length());
		
		/***
		 * close resourse
		 */
		if(null!=ssl){
			ssl.close();
		}
		if(null!=s){
			s.close();
		}
		if(null!=os){
			os.close();
		}
		if(null!=is){
			is.close();
		}
		return body;
	}

	/**
	 * This method is for creating a byte array from input stream data.
	 *
	 * @param is
	 *            - the input stream containing the data
	 * @return is the byte array of the input stream data
	 */
	private static byte[] readAll(InputStream is) throws IOException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];

		while (true) {
			int len = is.read(buf);
			if (len < 0) {
				break;
			}
			baos.write(buf, 0, len);
		}
		return baos.toByteArray();
	}

	// ----------------------------------------------------------------------------

	/**
	 * This function uses the returned status code retrieved from the Digital Response and returns an appropriate description for the code
	 * 
	 * @param vResponseCode
	 *            String containing the vpc_TxnResponseCode
	 * 
	 * @return description String containing the appropriate description
	 */
	String getResponseDescription(String vResponseCode) {

		String result = "";

		// check if a single digit response code
		if (vResponseCode.length() != 1) {

			// Java cannot switch on a string so turn everything to a char
			char input = vResponseCode.charAt(0);

			switch (input) {
				case '0':
					result = "Transaction Successful";
					break;
				case '1':
					result = "Unknown Error";
					break;
				case '2':
					result = "Bank Declined Transaction";
					break;
				case '3':
					result = "No Reply from Bank";
					break;
				case '4':
					result = "Expired Card";
					break;
				case '5':
					result = "Insufficient Funds";
					break;
				case '6':
					result = "Error Communicating with Bank";
					break;
				case '7':
					result = "Payment Server System Error";
					break;
				case '8':
					result = "Transaction Type Not Supported";
					break;
				case '9':
					result = "Bank declined transaction (Do not contact Bank)";
					break;
				case 'A':
					result = "Transaction Aborted";
					break;
				case 'C':
					result = "Transaction Cancelled";
					break;
				case 'D':
					result = "Deferred transaction has been received and is awaiting processing";
					break;
				case 'F':
					result = "3D Secure Authentication failed";
					break;
				case 'I':
					result = "Card Security Code verification failed";
					break;
				case 'L':
					result = "Shopping Transaction Locked (Please try the transaction again later)";
					break;
				case 'N':
					result = "Cardholder is not enrolled in Authentication Scheme";
					break;
				case 'P':
					result = "Transaction has been received by the Payment Adaptor and is being processed";
					break;
				case 'R':
					result = "Transaction was not processed - Reached limit of retry attempts allowed";
					break;
				case 'S':
					result = "Duplicate SessionID (OrderInfo)";
					break;
				case 'T':
					result = "Address Verification Failed";
					break;
				case 'U':
					result = "Card Security Code Failed";
					break;
				case 'V':
					result = "Address Verification and Card Security Code Failed";
					break;
				case '?':
					result = "Transaction status is unknown";
					break;
				default:
					result = "Unable to be determined";
			}

			return result;
		} else {
			return "No Value Returned";
		}
	} // getResponseDescription()

	// ----------------------------------------------------------------------------

	/**
	 * This function uses the QSI AVS Result Code retrieved from the Digital Receipt and returns an appropriate description for this code.
	 *
	 * @param vAVSResultCode
	 *            String containing the vpc_AVSResultCode
	 * @return description String containing the appropriate description
	 */
	private String displayAVSResponse(String vAVSResultCode) {

		String result = "";
		if (vAVSResultCode != null || vAVSResultCode.length() == 0) {

			if (vAVSResultCode.equalsIgnoreCase("Unsupported") || vAVSResultCode.equalsIgnoreCase("No Value Returned")) {
				result = "AVS not supported or there was no AVS data provided";
			} else {
				// Java cannot switch on a string so turn everything to a char
				char input = vAVSResultCode.charAt(0);

				switch (input) {
					case 'X':
						result = "Exact match - address and 9 digit ZIP/postal code";
						break;
					case 'Y':
						result = "Exact match - address and 5 digit ZIP/postal code";
						break;
					case 'S':
						result = "Service not supported or address not verified (international transaction)";
						break;
					case 'G':
						result = "Issuer does not participate in AVS (international transaction)";
						break;
					case 'A':
						result = "Address match only";
						break;
					case 'W':
						result = "9 digit ZIP/postal code matched, Address not Matched";
						break;
					case 'Z':
						result = "5 digit ZIP/postal code matched, Address not Matched";
						break;
					case 'R':
						result = "Issuer system is unavailable";
						break;
					case 'U':
						result = "Address unavailable or not verified";
						break;
					case 'E':
						result = "Address and ZIP/postal code not provided";
						break;
					case 'N':
						result = "Address and ZIP/postal code not matched";
						break;
					case '0':
						result = "AVS not requested";
						break;
					default:
						result = "Unable to be determined";
				}
			}
		} else {
			result = "null response";
		}
		return result;
	}

	// ----------------------------------------------------------------------------

	/**
	 * This function uses the QSI CSC Result Code retrieved from the Digital Receipt and returns an appropriate description for this code.
	 *
	 * @param vCSCResultCode
	 *            String containing the vpc_CSCResultCode
	 * @return description String containing the appropriate description
	 */
	private String displayCSCResponse(String vCSCResultCode) {

		String result = "";
		if (vCSCResultCode != null || vCSCResultCode.length() == 0) {

			if (vCSCResultCode.equalsIgnoreCase("Unsupported") || vCSCResultCode.equalsIgnoreCase("No Value Returned")) {
				result = "CSC not supported or there was no CSC data provided";
			} else {
				// Java cannot switch on a string so turn everything to a char
				char input = vCSCResultCode.charAt(0);

				switch (input) {
					case 'M':
						result = "Exact code match";
						break;
					case 'S':
						result = "Merchant has indicated that CSC is not present on the card (MOTO situation)";
						break;
					case 'P':
						result = "Code not processed";
						break;
					case 'U':
						result = "Card issuer is not registered and/or certified";
						break;
					case 'N':
						result = "Code invalid or not matched";
						break;
					default:
						result = "Unable to be determined";
				}
			}

		} else {
			result = "null response";
		}
		return result;
	}

	// ----------------------------------------------------------------------------

	/**
	 * This method uses the 3DS verStatus retrieved from the Response and returns an appropriate description for this code.
	 *
	 * @param vpc_VerStatus
	 *            String containing the status code
	 * @return description String containing the appropriate description
	 */
	private String getStatusDescription(String vStatus) {

		String result = "";
		if (vStatus != null && !vStatus.equals("")) {

			if (vStatus.equalsIgnoreCase("Unsupported") || vStatus.equals("No Value Returned")) {
				result = "3DS not supported or there was no 3DS data provided";
			} else {

				// Java cannot switch on a string so turn everything to a
				// character
				char input = vStatus.charAt(0);

				switch (input) {
					case 'Y':
						result = "The cardholder was successfully authenticated.";
						break;
					case 'E':
						result = "The cardholder is not enrolled.";
						break;
					case 'N':
						result = "The cardholder was not verified.";
						break;
					case 'U':
						result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer.";
						break;
					case 'F':
						result = "There was an error in the format of the request from the merchant.";
						break;
					case 'A':
						result = "Authentication of your Merchant ID and Password to the ACS Directory Failed.";
						break;
					case 'D':
						result = "Error communicating with the Directory Server.";
						break;
					case 'C':
						result = "The card type is not supported for authentication.";
						break;
					case 'S':
						result = "The signature on the response received from the Issuer could not be validated.";
						break;
					case 'P':
						result = "Error parsing input from Issuer.";
						break;
					case 'I':
						result = "Internal Payment Server system error.";
						break;
					default:
						result = "Unable to be determined";
						break;
				}
			}
		} else {
			result = "null response";
		}
		return result;
	}
}
