package com.sinodynamic.hkgta.service.crm.backoffice.membership;

import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.ResponseMsg;

public interface EditMemberProfileService extends IServiceBase<CustomerProfile>{


	public ResponseMsg editMemberProfile(CustomerProfile dto,String userId);

	public ResponseMsg checkInputData(CustomerProfile dto);

}

