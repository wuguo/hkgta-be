package com.sinodynamic.hkgta.service.crm.sales.leads;

import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.IServiceBase;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public interface LeadCustomerService extends IServiceBase<CustomerProfile>{

	
	public ResponseResult saveOrUpdatePotentialCustomer(CustomerProfileDto customerProfile) throws Exception;
	
	public ResponseResult deleteLead(Long customerId,String salesLoginId);
	
	public ResponseResult enrollLead(Long customerId);
	
	public ResponseResult movePortraitToCustomerFolder(CustomerProfile customerProfile);
	
	public ResponseResult getCustomerProfileDetail(Long customerId);
}
