package com.sinodynamic.hkgta.service.sys;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.sys.CarparkInterfaceIoDao;
import com.sinodynamic.hkgta.dto.sys.CarparkInterfaceIoDto;
import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceIo;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.ServiceBase;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Service
public class CarparkInterfaceIoServiceImpl extends ServiceBase<CarparkInterfaceIo>
		implements CarparkInterfaceIoService {
	@Autowired
	private CarparkInterfaceIoDao carparkInterfaceIoDao;
	@Autowired
	private MemberService memberService;

	@Override
	@Transactional
	public ResponseResult createCarparkInterface(CarparkInterfaceIoDto carparkInterfaceIoDto) {
		Member member = memberService.getMemberByAcademyNo(carparkInterfaceIoDto.getAcademyNo());
		Long customerId = member.getCustomerId();
		List<CarparkInterfaceIo> carparkInterfaceIo = null;
		carparkInterfaceIo = carparkInterfaceIoDao.getByCustomerId(customerId);
		try {
			if (carparkInterfaceIo.size() > 0) {
				if (null != carparkInterfaceIo.get(0).getInTime() && null != carparkInterfaceIo.get(0).getOutTime()) {

					saveData(carparkInterfaceIoDto, customerId);
				}
			} else if (carparkInterfaceIo.size() == 0) {
				saveData(carparkInterfaceIoDto, customerId);
			}
		} catch (Exception e) {
			logger.error(CarparkInterfaceIoServiceImpl.class.getName() + " createCarparkInterface Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_CREATE_NEW_CARPARK_INTERFACE);
		}
		return responseResult;
	}

	private void saveData(CarparkInterfaceIoDto carparkInterfaceIoDto, Long customerId) {
		CarparkInterfaceIo carparkInterface = new CarparkInterfaceIo();
		carparkInterface.setCreateDate(new Date());
		carparkInterface.setVehiclePlateNo(carparkInterfaceIoDto.getVehiclePlateNo());
		carparkInterface.setInTime(new Date());
		carparkInterface.setCardType("02");
		carparkInterface.setCustomerId(customerId);
		carparkInterface.setImportCardId("02");
		carparkInterface.setStatus("YES");
		carparkInterfaceIoDao.save(carparkInterface);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		carparkInterfaceIoDto.setCheckStatus("out");
		carparkInterfaceIoDto.setInTime(dateFormat.format(carparkInterface.getInTime()));
		carparkInterfaceIoDto.setAcademyNo(null);
		responseResult.initResult(GTAError.Success.SUCCESS, carparkInterfaceIoDto);
	}

	@Override
	@Transactional
	public ResponseResult checkCarparkInterface(String academyNo) {
		Member member = memberService.getMemberByAcademyNo(academyNo);
		if (null == member) {
			responseResult.initResult(GTAError.MemberShipError.NOT_ARCHIVE_PARTON);
			return responseResult;
		}
		Long customerId = member.getCustomerId();
		List<CarparkInterfaceIo> carparkInterfaceIo = null;
		CarparkInterfaceIoDto carparkInterfaceIoDto = new CarparkInterfaceIoDto();
		carparkInterfaceIo = carparkInterfaceIoDao.getByCustomerId(customerId);
		try {
			if (carparkInterfaceIo.size() > 0) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				if (null != carparkInterfaceIo.get(0).getInTime()) {
					carparkInterfaceIoDto.setInTime(dateFormat.format(carparkInterfaceIo.get(0).getInTime()));
				}
				if (null != carparkInterfaceIo.get(0).getOutTime()) {
					carparkInterfaceIoDto.setOutTime(dateFormat.format(carparkInterfaceIo.get(0).getOutTime()));
				}
				if (null != carparkInterfaceIo.get(0).getInTime() && null != carparkInterfaceIo.get(0).getOutTime()) {
					carparkInterfaceIoDto.setCheckStatus("in");
					responseResult.initResult(GTAError.Success.SUCCESS, carparkInterfaceIoDto);
				} else if (carparkInterfaceIo.size() == 0) {
					carparkInterfaceIoDto.setCheckStatus("in");
					responseResult.initResult(GTAError.Success.SUCCESS, carparkInterfaceIoDto);
				} else if (null != carparkInterfaceIo.get(0).getInTime()
						&& null == carparkInterfaceIo.get(0).getOutTime()) {
					carparkInterfaceIoDto.setCheckStatus("out");
					responseResult.initResult(GTAError.Success.SUCCESS, carparkInterfaceIoDto);
				}
			} else {
				carparkInterfaceIoDto.setCheckStatus("in");
				responseResult.initResult(GTAError.Success.SUCCESS, carparkInterfaceIoDto);
			}
		} catch (Exception e) {
			logger.error(CarparkInterfaceIoServiceImpl.class.getName() + " checkCarparkInterface Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_CREATE_NEW_CARPARK_INTERFACE);
		}
		return responseResult;
	}

	@Override
	@Transactional
	public ResponseResult updateCarparkInterface(String academyNo) {
		Member member = memberService.getMemberByAcademyNo(academyNo);
		Long customerId = member.getCustomerId();
		List<CarparkInterfaceIo> carparkInterfaceIo = null;
		carparkInterfaceIo = carparkInterfaceIoDao.getByCustomerId(customerId);
		try {
			if (carparkInterfaceIo.size() > 0) {
				if (null != carparkInterfaceIo.get(0).getInTime() && null == carparkInterfaceIo.get(0).getOutTime()) {
					carparkInterfaceIo.get(0).setOutTime(new Date());
					carparkInterfaceIoDao.update(carparkInterfaceIo.get(0));
					responseResult.initResult(GTAError.Success.SUCCESS);
				}
			}
		} catch (Exception e) {
			logger.error(CarparkInterfaceIoServiceImpl.class.getName() + " updateCarparkInterface Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_CREATE_NEW_ROLE);
		}
		return responseResult;
	}

	@Transactional
	public boolean saveCarparkInterfaceIo(CarparkInterfaceIo carparkInterfaceIo) throws Exception {
		// TODO Auto-generated method stub
		carparkInterfaceIoDao.save(carparkInterfaceIo);
		return true;
	}

}
