package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.UserPreferenceSettingDto;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;
import com.sinodynamic.hkgta.service.IServiceBase;

public interface NotificationOnOffService extends IServiceBase<UserPreferenceSetting>{

	 public List<UserPreferenceSettingDto> getNotificationSettings(Long customerId) throws Exception;
	 
	 public List<UserPreferenceSettingDto> getRestaurantNotificationSettings(String userId) throws Exception;
	 
	 public void updateNotificationSetting(String userId, String paramId, String paramValue) throws Exception;
	 
	 public List<String> getPushNotificationStaffByRestaurantId(String restaurantId) throws Exception;
}
