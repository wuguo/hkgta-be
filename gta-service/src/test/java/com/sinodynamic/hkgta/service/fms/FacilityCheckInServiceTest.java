package com.sinodynamic.hkgta.service.fms;

import com.sinodynamic.hkgta.dao.fms.FacilityMasterDao;
import com.sinodynamic.hkgta.dao.fms.MemberFacilityTypeBookingDao;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.sinodynamic.hkgta.util.exception.GTACommonException;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mason
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-service-test.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class FacilityCheckInServiceTest {

	@InjectMocks
	private FacilityCheckInService facilityCheckInService = new FacilityCheckInServiceImpl();

	@Mock
	private MemberFacilityTypeBookingDao memberFacilityTypeBookingDao;

	@Autowired
	private FacilityMasterDao facilityMasterDao;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected=GTACommonException.class)
	public void testGetCheckAvailability() {
		
		long resvId = 0l;
		facilityCheckInService.getCheckAvailability(resvId, false);
	}

	@Test
	@Transactional
	public void testGetFacilityAllotment() {

		long resvId = 1l;
		MemberFacilityTypeBooking memberFacilityTypeBooking = new MemberFacilityTypeBooking();
		List<FacilityTimeslot> facilityTimeslots = new ArrayList<>();
		FacilityTimeslot facilityTimeslot = new FacilityTimeslot();
		facilityTimeslot.setFacilityTimeslotId(100l);
		facilityTimeslot.setFacilityMaster(facilityMasterDao.getByFacilityNo(1l));
		facilityTimeslots.add(facilityTimeslot);
		memberFacilityTypeBooking.setFacilityTimeslots(facilityTimeslots);
		Mockito.when(memberFacilityTypeBookingDao.getMemberFacilityTypeBookingIncludeAllStatus(resvId)).thenReturn(memberFacilityTypeBooking);

		List<FacilityMasterDto> facilityMasterDtoList =  facilityCheckInService.getFacilityAllotment(resvId);
		Assert.assertNotNull(facilityMasterDtoList);
		Assert.assertFalse(facilityMasterDtoList.isEmpty());
	}
}
