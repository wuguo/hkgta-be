package com.sinodynamic.hkgta.service.statement;


import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.statement.SearchStatementsDto;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalueBalHistory;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.service.crm.statement.MemberCashvalueBalHistoryService;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public class MemberCashvalueBalHistoryServiceTest extends BaseServiceTester {

	@Autowired
	private MemberCashvalueBalHistoryService memberCashvalueBalHistoryService;
	
	@Test
	public void getStatements(){
		try {
			String sortBy = "customerId";
			Integer pageNumber = 10;
			Integer pageSize = 1;
			String isAscending = "true";
			String month = "08";
			String year = "2015";
			String memberType = "ALL";
			String deliveryStatus = "NO";
			SearchStatementsDto dto = new SearchStatementsDto(sortBy, pageNumber, pageSize, isAscending, month, year, memberType,deliveryStatus);
			ListPage<MemberCashvalueBalHistory> page = new ListPage<MemberCashvalueBalHistory>();
			page.addAscending(sortBy);
			page.setNumber(pageNumber);
			page.setSize(pageSize);
			assertTrue(memberCashvalueBalHistoryService.getStatements(page, dto) instanceof ResponseResult);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
