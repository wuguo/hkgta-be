package com.sinodynamic.hkgta.service.fms;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.sinodynamic.hkgta.util.constant.Constant;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-service-test.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class FacilityMasterServiceTest {

	@Autowired
	private FacilityMasterService facilityMasterService;
	
	@Autowired
	private FacilityAdditionAttributeService facilityAdditionAttributeService;
	
	@Test
	public void getFacilityMasterList() {
		
		try
		{
			facilityMasterService.getFacilityMasterList("GOLF");
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
	
	
	
	@Test
	public void countFacilityMasterByTypeAndAttribute() {
		
		try
		{
			
			int tennisfacilityCount = facilityMasterService.countFacilityMasterByTypeAndFacilityNos("TENNIS",null);
			System.out.println(tennisfacilityCount);
			Assert.assertTrue(tennisfacilityCount > 0);
			
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			int golffacilityCount = facilityMasterService.countFacilityMasterByTypeAndFacilityNos("GOLF",facilityNos);
			System.out.println(golffacilityCount);
			Assert.assertTrue(golffacilityCount > 0);
			
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
	
}
