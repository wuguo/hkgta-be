package com.sinodynamic.hkgta.service.pos;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pos.RestaurantItemMasterDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantMasterDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantMenuCatDao;
import com.sinodynamic.hkgta.dao.pos.RestaurantMenuItemDao;
import com.sinodynamic.hkgta.dto.pos.RestaurantMenuItemDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantItemMaster;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuCat;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;
import com.sinodynamic.hkgta.util.constant.RestaurantMenuItemStatus;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-service-test.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class RestaurantMenuItemServiceTest {

	@Autowired
	private RestaurantMenuItemDao restaurantMenuItemDao;

	@Autowired
	private RestaurantItemMasterDao restaurantItemMasterDao;
	
	@Autowired
	private RestaurantMasterDao restaurantMasterDao;

	@Autowired
	private RestaurantMenuCatDao restaurantMenuCatDao;
	
	@Autowired
	private RestaurantMenuItemService restaurantMenuItemService;

	@Test
	@Transactional
	public void testChangeStatus() {

		String restaurantId = "0001";
		String catId = "test-cat";
		String itemNo = "test-itemNo";
		
		RestaurantMenuItem restaurantMenuItem = createMenuItem(restaurantId, catId, itemNo);

		RestaurantMenuItemDto restaurantMenuItemDto = new RestaurantMenuItemDto();
		restaurantMenuItemDto.setItemNo(itemNo);
		restaurantMenuItemDto.setRestaurantId(restaurantId);
		restaurantMenuItemDto.setCatId(catId);
		
		restaurantMenuItemService.changeStatus(restaurantMenuItemDto, "H", "mason");

		restaurantMenuItem =  restaurantMenuItemDao.getRestaurantMenuItem(restaurantId, catId, itemNo);
		Assert.assertEquals("H", restaurantMenuItem.getStatus());

	}

	@Test
	@Transactional
	public void testChangeDisplayOrder() {
		
		String restaurantId = "0001";
		String catId = "test-cat";
		String itemNo = "test-itemNo";
		
		RestaurantMenuItem restaurantMenuItem = createMenuItem(restaurantId, catId, itemNo);
		RestaurantMenuItemDto restaurantMenuItemDto = new RestaurantMenuItemDto();
		restaurantMenuItemDto.setItemNo(itemNo);
		restaurantMenuItemDto.setRestaurantId(restaurantId);
		restaurantMenuItemDto.setCatId(catId);
		restaurantMenuItemDto.setDisplayOrder(10);
		restaurantMenuItemService.changeDisplayOrder(restaurantMenuItemDto, "mason");

		restaurantMenuItem =  restaurantMenuItemDao.getRestaurantMenuItem(restaurantId, catId, itemNo);
		Assert.assertTrue(restaurantMenuItem.getDisplayOrder() == 10);
	}

	private RestaurantMenuItem createMenuItem(String restaurantId, String catId, String itemNo) {


		RestaurantMaster restaurantMaster = restaurantMasterDao.getRestaurantMaster("0001");

		
		RestaurantMenuCat restaurantMenuCat = saveRestaurantMenuCat(restaurantMaster, restaurantId, catId);
		saveRestaurantMenuItem(restaurantId, catId, itemNo, restaurantMenuCat);
		
		RestaurantMenuItem restaurantMenuItem = restaurantMenuItemDao.getRestaurantMenuItem(restaurantId, catId, itemNo);
		
		return restaurantMenuItem;
	}
	
	

	private RestaurantMenuCat saveRestaurantMenuCat(RestaurantMaster restaurantMaster, String restaurantId,
			String catId) {
		RestaurantMenuCat restaurantMenuCat = new RestaurantMenuCat();
			restaurantMenuCat.setCatId(catId);
			restaurantMenuCat.setRestaurantMaster(restaurantMaster);
			restaurantMenuCat.setCategoryName(catId);
			restaurantMenuCat.setStatus("ACT");
			restaurantMenuCat.setCreateDate(new Date());
		

		restaurantMenuCatDao.saveOrUpdate(restaurantMenuCat);
		return restaurantMenuCat;
	}
	
	private void saveRestaurantMenuItem(String restaurantId, String catId, String itemNo,  RestaurantMenuCat restaurantMenuCat) {
		RestaurantMenuItem restaurantMenuItem  = new RestaurantMenuItem();
		
		BigDecimal onlinePrice = new BigDecimal("200");
		
		RestaurantItemMaster restaurantItemMaster = saveRestaurantItemMaster(itemNo, onlinePrice);
		
		restaurantMenuItem.setRestaurantItemMaster(restaurantItemMaster);
		restaurantMenuItem.setOnlinePrice(onlinePrice);
		
		if (restaurantMenuItem.getCreateDate() == null) {
			restaurantMenuItem.setCreateDate(new Date());
		}
		restaurantMenuItem.setStatus(RestaurantMenuItemStatus.S.toString());
		
		if (restaurantMenuItem.getDisplayOrder() == null) {
			Integer maxDisplayOrder = restaurantMenuItemDao.getMaxDisplayOrder();
			if (maxDisplayOrder == null) {
				maxDisplayOrder = 0;
			}
			restaurantMenuItem.setDisplayOrder(++maxDisplayOrder);
		}

		restaurantMenuItem.setRestaurantMenuCat(restaurantMenuCat);

		restaurantMenuItemDao.saveOrUpdate(restaurantMenuItem);
	}

	private RestaurantItemMaster saveRestaurantItemMaster(String itemNo, BigDecimal onlinePrice) {
		RestaurantItemMaster restaurantItemMaster = restaurantItemMasterDao.get(RestaurantItemMaster.class, itemNo);
		if (restaurantItemMaster == null) {
			restaurantItemMaster = new RestaurantItemMaster();
		}
		restaurantItemMaster.setItemNo(itemNo);
		restaurantItemMaster.setItemName("test item");
		
		restaurantItemMaster.setSyncDate(new Date());
		restaurantItemMaster.setDefaultPrice(onlinePrice);
		if (restaurantItemMaster.getCreateDate() == null) {
			restaurantItemMaster.setCreateDate(new Date());
		}
		
		restaurantItemMasterDao.saveOrUpdate(restaurantItemMaster);
		return restaurantItemMaster;
	}
}
