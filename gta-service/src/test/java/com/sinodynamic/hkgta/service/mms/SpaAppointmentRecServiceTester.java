package com.sinodynamic.hkgta.service.mms;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.mms.MmsOrderItemDto;
import com.sinodynamic.hkgta.service.BaseServiceTester;

public class SpaAppointmentRecServiceTester extends BaseServiceTester {
    
    @Autowired
    private SpaAppointmentRecService spaAppointmentRecService;
    
    @Test
    public void getMmsAdvanceSql() {
	
	String beginTime = "2015-08-10";
	String endTime = "2015-11-10";
	
	try {
	    String sql = spaAppointmentRecService.getMmsAdvanceSql(beginTime, endTime, null,null);
	    System.out.println(sql);
	} catch (Exception e) {
	    Assert.fail(e.getMessage());
	}
    }
    
    @Test
    public void getSpaAppointmentRecByExtinvoiceNo() {

	try {
	    List<MmsOrderItemDto> spaAppointmentRecs = spaAppointmentRecService.getSpaAppointmentRecByExtinvoiceNo("184");
	    System.out.println("result size: " + (spaAppointmentRecs == null ? 0 : spaAppointmentRecs.size()));
	} catch (Exception e) {
	    Assert.fail(e.getMessage());
	}
    }
}
