package com.sinodynamic.hkgta.service.pms;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskQueryDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;

public class RoomHousekeepTaskServiceTest extends BaseServiceTester
{

	@Autowired
	private RoomHousekeepTaskService roomHousekeepTaskService;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	@Test
	public void getRoomTaskList(){
		try
		{
			GlobalParameter  assistant = globalParameterService.getGlobalParameter("HK-MAXRESP-RA");
			GlobalParameter  inspector = globalParameterService.getGlobalParameter("HK-MAXRESP-INSPECTOR");
			GlobalParameter  supervisor = globalParameterService.getGlobalParameter("HK-MAXRESP-SUPERVISOR");
			Integer assistantResTime = Integer.parseInt(assistant.getParamValue());
			Integer inspectorResTime = Integer.parseInt(inspector.getParamValue());
			Integer supervisorResTime = Integer.parseInt(supervisor.getParamValue());
			List<RoomHousekeepTaskQueryDto> roomTaskList=roomHousekeepTaskService.getRemindRoomHousekeepTaskList(assistantResTime,inspectorResTime,supervisorResTime);
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
	
	
	@Test
	public void getMyRoomTaskListForApp(){
		try
		{
			List<RoomHousekeepTaskQueryDto> taskList1=roomHousekeepTaskService.getMyRoomTaskListForApp("[staff001]", "RA", null);
			System.out.println("-------------------------------"+taskList1.size());
			
			List<RoomHousekeepTaskQueryDto> taskList2=roomHousekeepTaskService.getMyRoomTaskListForApp("[staff001]", "ISP", null);
			System.out.println("-------------------------------"+taskList2.size());
			
			List<RoomHousekeepTaskQueryDto> taskList3=roomHousekeepTaskService.getMyRoomTaskListForApp("[staff001]", "NGR", null);
			System.out.println("-------------------------------"+taskList3.size());
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void getRoomTaskListForApp(){
		try
		{
			List<RoomHousekeepTaskQueryDto> taskList1=roomHousekeepTaskService.getRoomTaskListForApp("[staff001]", "RA", null);
			System.out.println("-------------------------------"+taskList1.size());
			
			List<RoomHousekeepTaskQueryDto> taskList2=roomHousekeepTaskService.getRoomTaskListForApp("[staff001]", "ISP", null);
			System.out.println("-------------------------------"+taskList2.size());
			
			List<RoomHousekeepTaskQueryDto> taskList3=roomHousekeepTaskService.getRoomTaskListForApp("[staff001]", "NGR", null);
			System.out.println("-------------------------------"+taskList3.size());
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
}
