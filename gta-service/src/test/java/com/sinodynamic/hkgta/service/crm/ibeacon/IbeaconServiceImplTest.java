package com.sinodynamic.hkgta.service.crm.ibeacon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.entity.ibeacon.IbeaconNearbyPerson;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public class IbeaconServiceImplTest extends BaseServiceTester{

	@Autowired
	private IbeaconService ibeaconService;
	
	@Before
	public void setUp(){
		
	}

	@Test
	public void testIbeaconGetTheNextAvailableMinorId(){
		assertTrue(ibeaconService.getTheNextAvailableMinorId(Long.valueOf(1)) instanceof ResponseResult);
	}
	
	@Test
	public void testGetRoomBeaconList(){
		assertEquals("0", ibeaconService.getRoomBeaconsList().getReturnCode());
	}
	
	@Test
	public void testIbeaconGetRecogBeaconListReturnList(){
		ListPage<IbeaconNearbyPerson> page = new ListPage<IbeaconNearbyPerson>();
		page.addAscending("createDate");
		page.setNumber(1);
		page.setSize(10);
		
		assertTrue(ibeaconService.getRecogBeaconList(page,null,"ALL","HKC") instanceof ResponseResult);
	}

}
