package com.sinodynamic.hkgta.service.pms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pms.PMSRequestProcessorDao;
import com.sinodynamic.hkgta.service.BaseServiceTester;

public class DaoTester extends BaseServiceTester{
	@Autowired
	private PMSRequestProcessorDao pmsRequestProcessorDao;
	
	@Test
	@Transactional
	public void test() throws ParseException{
		
		String dateStr1 = "2015-09-03 16:00:00";
		String dateStr2 = "2015-09-03 17:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date arriveDate = sdf.parse(dateStr1);
		Date departDate = sdf.parse(dateStr2);
		int reservedFacilitys = pmsRequestProcessorDao.countReservedSpecifiedFacility(arriveDate,departDate,"GOLF", "GOLFBAYTYPE-RH");
		System.out.println("reservedFacilitys=" +reservedFacilitys);
	}
	
	@Test
	@Transactional
	public void test2() throws Exception{
		String dateStr1 = "2015-09-03 07:00:00";
		String dateStr2 = "2015-09-03 08:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date startTime = sdf.parse(dateStr1);
		Date endTime = sdf.parse(dateStr2);
		int fromFacilityNo = 1;
		int endFacilityNo = 44;
		List<Integer> list = pmsRequestProcessorDao.queryAllAvailableSpecifiedFacilityByPeriod(startTime,endTime, "GOLF", "GOLFBAYTYPE-RH", "ACT",fromFacilityNo,endFacilityNo);
		System.out.println(list.size());
	}
	
	@Test
	public void test3() throws Exception{
		String dateStr1 = "2015-09-03 07:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date startTime = sdf.parse(dateStr1);
		
		Date date = DateUtils.addHours(startTime, 18);
		System.out.println(date);
		
	}
	
}
