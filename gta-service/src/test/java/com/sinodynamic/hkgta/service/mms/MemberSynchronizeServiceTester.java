package com.sinodynamic.hkgta.service.mms;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/config/applicationContext-service-test.xml" }) 
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class MemberSynchronizeServiceTester {
    
    @Autowired
    private MemberSynchronizeService memberSynchronizeService;
    
    @Test
    public void synchronizeGtaMember2Mms() {
	
	try {
	    memberSynchronizeService.synchronizeGtaMember2Mms("");
	} catch (Exception e) {
	    Assert.fail(e.getMessage());
	}
    }
}
