package com.sinodynamic.hkgta.service.pms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.pms.RoomReservationInfoDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/config/applicationContext-service-test.xml" }) 
public class ConcurrencyTestUtilTest {
     
	@Autowired
	private PMSRequestProcessorService PMSRequestProcessorService;
	@Autowired
	private BookingFacilityService bookingFacilityService;
	
//    @Test
    public void testAssertConcurrent() throws InterruptedException {
        List<Runnable> tasks = new ArrayList<Runnable> (40);
        for(int i = 0; i < 40; i++) {
            tasks.add(new Runnable() {
                 
                @Override
                public void run() {
                	
                    try {
                    	
                		
                        //Thread.sleep(20);
                        String dateStr1 = "2015-09-03";
            			String dateStr2 = "2015-09-10";
            			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            			
            			Date arriveDate = sdf.parse(dateStr1);
            			Date departDate = sdf.parse(dateStr2);
            			
            			List<RoomReservationInfoDto> roomreservations = new ArrayList<RoomReservationInfoDto>();
            			RoomReservationInfoDto  room = new RoomReservationInfoDto();
            			room.setReservationId("1");
            			room.setArriveDate(arriveDate);
            			room.setDepartDate(departDate);
            			roomreservations.add(room);
            			
            			RoomReservationInfoDto  room2 = new RoomReservationInfoDto();
            			room2.setReservationId("2");
            			room2.setArriveDate(arriveDate);
            			room2.setDepartDate(departDate);
            			roomreservations.add(room2);
            			
            			RoomReservationInfoDto  room3 = new RoomReservationInfoDto();
            			room3.setReservationId("3");
            			room3.setArriveDate(arriveDate);
            			room3.setDepartDate(departDate);
            			roomreservations.add(room3);
            			
            			RoomReservationInfoDto  room4 = new RoomReservationInfoDto();
            			room4.setReservationId("4");
            			room4.setArriveDate(arriveDate);
            			room4.setDepartDate(departDate);
            			roomreservations.add(room4);
            			
            			RoomReservationInfoDto  room5 = new RoomReservationInfoDto();
            			room5.setReservationId("5");
            			room5.setArriveDate(arriveDate);
            			room5.setDepartDate(departDate);
            			roomreservations.add(room5);
            			
            			PMSRequestProcessorService.giftBundleForReservations(2L, roomreservations);
            			
            			//Map<Date,Long> map = PMSRequestProcessorService.giftBundleByPeriod(2L, arriveDate, departDate,"GOLF","GOLFBAYTYPE-RH","ACT");
            			//Map<Date,Long> map2 = PMSRequestProcessorService.giftBundleByPeriod2(2L, arriveDate, departDate,"GOLF","GOLFBAYTYPE-RH","ACT");
            			//bookingFacilityService.bookSpecifiedByPeriod(2L, arriveDate, departDate, "GOLF", "GOLFBAYTYPE-RH", "ACT");
                    }
                    /*catch(InterruptedException e) {
                         e.printStackTrace();
                    }*/ catch (ParseException e) {
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                	
                }
                 
            });
        }
         
        ConcurrencyTestUtil.assertConcurrent("20 thread test", tasks, 300, 20);
    }
}