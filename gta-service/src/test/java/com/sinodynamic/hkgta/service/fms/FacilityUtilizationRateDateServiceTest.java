package com.sinodynamic.hkgta.service.fms;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateDateDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateTimeDto;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.util.constant.Constant;

public class FacilityUtilizationRateDateServiceTest extends BaseServiceTester
{

	@Autowired
	private FacilityUtilizationRateDateService facilityUtilizationRateDateService;

	@Test
	public void getSpecialRateDateListByDate()
	{
		try
		{
			facilityUtilizationRateDateService.getSpecialRateDateListByDate(Constant.FACILITY_TYPE_TENNIS, "2019-09-24", "TENNISCRT-ITFID");
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void saveSpecialRateDate()
	{
		try
		{
			String facilityType = Constant.FACILITY_TYPE_TENNIS;
			String specialDate = "2019-09-24";
			String subType = "Mid-Autumn Festival";
			FacilityUtilizationRateDateDto rateDate = facilityUtilizationRateDateService.getSpecialRateDateListByDate(facilityType, specialDate, subType);
			if (null == rateDate)
			{
				FacilityUtilizationRateDateDto rateDateDto = new FacilityUtilizationRateDateDto();
				rateDateDto.setFacilityType(facilityType);
				rateDateDto.setFacilitySubTypeId("TENNISCRT-ITFOD");
				rateDateDto.setSpecialDate(specialDate);
				rateDateDto.setDateDesc(subType);

				List<FacilityUtilizationRateTimeDto> rateTimeList = new ArrayList<FacilityUtilizationRateTimeDto>();
				FacilityUtilizationRateTimeDto timeDto = new FacilityUtilizationRateTimeDto();
				timeDto.setBeginTime(12);
				timeDto.setRateType(Constant.RateType.HI.name());
				rateTimeList.add(timeDto);
				FacilityUtilizationRateTimeDto timeDto02 = new FacilityUtilizationRateTimeDto();
				timeDto02.setBeginTime(13);
				timeDto02.setRateType(Constant.RateType.HI.name());
				rateTimeList.add(timeDto02);

				rateDateDto.setRateList(rateTimeList);
				facilityUtilizationRateDateService.saveSpecialRateDate(Constant.FACILITY_TYPE_TENNIS, rateDateDto);
			}
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
}
