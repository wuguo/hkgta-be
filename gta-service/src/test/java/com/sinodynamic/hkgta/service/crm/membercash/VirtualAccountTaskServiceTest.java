package com.sinodynamic.hkgta.service.crm.membercash;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = { "classpath*:config/applicationContext-service-test.xml" })
//@ContextConfiguration(locations = { "classpath*:config/applicationContext-*.xml" })
@ContextConfiguration(locations = { "classpath:*applicationContext-*.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
@ComponentScan("com.sinodynamic.hkgta.service.crm.membercash")
public class VirtualAccountTaskServiceTest {

	//@Autowired
	VirtualAccountTaskService virtualAccountTaskService;  
	
	@Before
	public void before() {
		/**
		 * Pre test condition
		 * 1. make the interface public. 
		 * 2. comment out the Dao part inside VirtualAccountTaskServiceImpl 
		 */
		virtualAccountTaskService=new VirtualAccountTaskServiceImpl();
	}
	
	@Test
	public void getTwoDaysAgoWorkingDayFromDBTest() throws ParseException {
		Date chqdate;
		Date testDate;
		/*chqdate=virtualAccountTaskService.getTwoDaysAgoWorkingDayFromDB(testDate=new Date());
		System.out.println("test="+testDate+" chq date=" + chqdate);
		
		chqdate=virtualAccountTaskService.getTwoDaysAgoWorkingDayFromDB(testDate=(new SimpleDateFormat("yyyyMMdd")).parse("20161003")  );
		System.out.println("test="+testDate+" chq date=" + chqdate);
		
		chqdate=virtualAccountTaskService.getTwoDaysAgoWorkingDayFromDB(testDate=(new SimpleDateFormat("yyyyMMdd")).parse("20161002")  );
		System.out.println("test="+testDate+" chq date=" + chqdate);				
		
		chqdate=virtualAccountTaskService.getTwoDaysAgoWorkingDayFromDB(testDate=(new SimpleDateFormat("yyyyMMdd")).parse("20161001")  );
		System.out.println("test="+testDate+" chq date=" + chqdate);*/				
		
	}
	
}
