package com.sinodynamic.hkgta.service.fms;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sinodynamic.hkgta.dto.golfmachine.BatInfo;
import com.sinodynamic.hkgta.integration.zoomtech.service.GolfMachineService;
import com.sinodynamic.hkgta.integration.zoomtech.service.GolfMachineServiceImpl;


public class GolfMachineServiceTest { // extends BaseServiceTester {
	//@Autowired(required=true) GolfMachineService golfMachineService; // = new GolfMachineServiceImpl();
	GolfMachineService golfMachineService = null; //new GolfMachineServiceImpl();

	@Before
	public void before() {
		golfMachineService = new GolfMachineServiceImpl();
		golfMachineService.setRemoteHost("http://localhost:8085");
	}
	
	@Test
	public void testGolfMachine() throws Exception {
		System.out.println(golfMachineService.testHost());
		
/*		Long inBatNo=30L;
		List<BatInfo> batInfos=golfMachineService.getBatInfo(inBatNo);
		
		System.out.print("get batInfo " + inBatNo + " = ");
		//System.out.println(batInfos);
		for (BatInfo bi: batInfos) {
			System.out.print("\nbatNo: " +  bi.getBatNo());
			System.out.print("  status: " +  bi.getBatStat());
			System.out.print("  updateDate: " +  bi.getUpdateDate());
		}		
		
		
		batInfos=golfMachineService.getBatInfo(null);
		System.out.println("\n------------\nAll batInfos=");
		//System.out.println(batInfos);
		for (BatInfo bi: batInfos) {
			System.out.print("\nbatNo: " +  bi.getBatNo());
			System.out.print("  status: " +  bi.getBatStat());
			System.out.print("  updateDate: " +  bi.getUpdateDate());
		}
*/		
		

/*		for (long i=1; i < 44; i++) {			
		   BatInfo batInfo=golfMachineService.getBatInfo(i).get(0);
			System.out.print("\nbatNo: " +  batInfo.getBatNo());
			System.out.print("  status: " +  batInfo.getBatStat());
			System.out.print("  updateDate: " +  batInfo.getUpdateDate());
		   
		   if (GolfMachineService.IS_ON.contains(batInfo.getBatStat().toString() ) ) 
			  System.out.println(" is ON");
		   else
			  System.out.println(" is OFF");
		}   
*/		
		Map<Long, BatInfo> batInfMap=golfMachineService.getAllBatInfosMap();
		for (long i=1; i < 44; i++) {
			System.out.print("\nbatNo: " +  batInfMap.get(i).getBatNo());
			System.out.print("  status: " +  batInfMap.get(i).getBatStat());
			System.out.print("  updateDate: " +  batInfMap.get(i).getUpdateDate());
		   
		   if ("4,5".contains(batInfMap.get(i).getBatStat().toString() ) ) 
			  System.out.println(" is ON");
		   else
			  System.out.println(" is OFF");				
		}
		
	}
	
	//@Test
	public void testTeeupMachine() throws Exception {
//		golfMachineService.changeTeeupMachineStatsbyBatNo(4l, "ON");
//		
//		Thread.sleep(5000);
//		golfMachineService.changeTeeupMachineStatsbyBatNo(4l, "OFF");
		
		Date startTime=new Date((new Date()).getTime() + 1000 * 1000);
		Date endTime=new Date((new Date()).getTime() + 6000 * 1000);
		
		System.out.println("start time: " + startTime );
		System.out.println("end  time: " + endTime );
		
		golfMachineService.setTeeupMachineStatsbyCheckinTime(7l, startTime, endTime);	
		
	}
	
	
}
