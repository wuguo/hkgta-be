package com.sinodynamic.hkgta.service.pms;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.pms.StaffRosterWeeklyPresetDto;
import com.sinodynamic.hkgta.dto.pms.StaffRosterWeeklyPresetRecordDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-service-test.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class StaffRosterWeeklyPresetServiceTest {

	@Autowired
	private StaffRosterWeeklyPresetService staffRosterWeeklyPresetService;
	
	@Transactional
	@Test
	public void testSave() {
		
		staffRosterWeeklyPresetService.deleteStaffRoster("presetNmae");
		StaffRosterWeeklyPresetDto staffRosterDto = new StaffRosterWeeklyPresetDto();
		staffRosterDto.setPresetName("presetNmae");
		List<StaffRosterWeeklyPresetRecordDto> list = new ArrayList<>();
		StaffRosterWeeklyPresetRecordDto recordDto = new StaffRosterWeeklyPresetRecordDto();
		recordDto.setWeekDay(1L);
		recordDto.setOffDuty(true);
		list.add(recordDto);
		staffRosterDto.setList(list);
		String userId = "mason";
		staffRosterWeeklyPresetService.save(staffRosterDto, userId);
	}
}
