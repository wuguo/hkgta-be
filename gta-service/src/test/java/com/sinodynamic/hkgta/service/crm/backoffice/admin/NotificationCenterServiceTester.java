//package com.sinodynamic.hkgta.service.crm.backoffice.admin;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.sinodynamic.hkgta.notification.SMSService;
//import com.sinodynamic.hkgta.service.BaseServiceTester;
//import com.sinodynamic.hkgta.util.MailSender;
//
//public class NotificationCenterServiceTester extends BaseServiceTester {
//	
//	@Autowired
//	private SMSService sMSService;
//	
//	@Test
//	public void testSendEmail(){
//		try {
//			MailSender.sendTextEmail("changpan_wu@epam.com", null, null, "Test email", "Test email!");
//		} catch (Exception e) {
//			Assert.fail(e.getMessage());
//		}
//	}
//	
//	@Test
//	public void testSendSMS(){
//		try {
//			List<String> phonenumbers = new ArrayList<String>();
//			phonenumbers.add("+8618612119592");
//			sMSService.sendSMS(phonenumbers, "test  sms", new Date());
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			Assert.fail(e.getMessage());
//		}
//	}
//}
