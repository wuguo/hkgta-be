package com.sinodynamic.hkgta.service.pms;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.sinodynamic.hkgta.entity.fms.TLock;
import com.sinodynamic.hkgta.service.pms.BookingFacilityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/config/applicationContext.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class NestTransactionTest {
	@Autowired
	private BookingFacilityService bookingFacilityService;
	
	@Test
	public void testNestTransaction() {
		TLock lock = new TLock(5L,"Lock3");
		bookingFacilityService.testNestTransaction(lock);
	}

}
