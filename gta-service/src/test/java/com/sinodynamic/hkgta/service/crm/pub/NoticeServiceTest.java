package com.sinodynamic.hkgta.service.crm.pub;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.sinodynamic.hkgta.dto.crm.pub.NoticeDto;
import com.sinodynamic.hkgta.util.constant.NoticeStatus;
import com.sinodynamic.hkgta.util.response.Data;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-service-test.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class NoticeServiceTest {

	@Autowired
	private NoticeService noticeService;
	
	private NoticeDto getNoticeDto() {
		NoticeDto noticeDto = new NoticeDto();
		noticeDto.setSubject("news subject");
		noticeDto.setContent("news content");
		noticeDto.setStatus(NoticeStatus.ACT.toString());
		noticeDto.setNoticeType("50");
		noticeDto.setNoticeBegin(new Date());
		return noticeDto;
	}
	
	@Test
	public void testSaveOrUpdate() {
		try {
			NoticeDto noticeDto = getNoticeDto();
			String createBy = "mason";
			long noticeId = noticeService.save(noticeDto, createBy);
			NoticeDto noticeDto2 = noticeService.getNotice(noticeId);
			Assert.assertNotNull(noticeDto2);
			noticeDto2.setSubject("news subject2");
			noticeService.update(noticeDto2, createBy);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void testUpdateStatus() {
		NoticeDto noticeDto = getNoticeDto();
		String createBy = "mason";
		long noticeId = noticeService.save(noticeDto, createBy);
		noticeService.updateStatus(noticeId, NoticeStatus.NACT.toString(), "mason");
		NoticeDto noticeDto2 = noticeService.getNotice(noticeId);
		Assert.assertEquals(NoticeStatus.NACT.toString(), noticeDto2.getStatus());
	}
	
	@Test
	public void testGetNoticeList() {
		NoticeDto noticeDto = getNoticeDto();
		String createBy = "mason";
		noticeService.save(noticeDto, createBy);
		Data data = noticeService.getNoticeList(1, 10);
		Assert.assertNotNull(data);
		Assert.assertTrue(!data.getList().isEmpty());
	}
}
