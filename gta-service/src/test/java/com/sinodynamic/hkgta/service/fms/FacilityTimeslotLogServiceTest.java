package com.sinodynamic.hkgta.service.fms;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotLogDao;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotLogDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-service-test.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class FacilityTimeslotLogServiceTest {

	@Autowired
	private FacilityTimeslotLogService facTimeslotLogSvc;

	@Autowired
	private FacilityTimeslotLogDao facTimeslotLogDao;

	@Test
	@Transactional
	public void testGetCheckAvailability() {
		try {
			Date now = new Date();
			FacilityTimeslotLog log = new FacilityTimeslotLog();
			log.setBeginDatetime(now);
			log.setEndDatetime(new Date(now.getTime() + 3600l * 1000l));
			log.setFacilityNo(1001l);
			log.setFacilityTimeslotId(1l); // no foreign key constraint here, but 'not-null' column.
			log.setFacilityType("TENNIS");
			log.setInternalRemark("For maven junit test");
			log.setStatus(FacilityStatus.MT.getDesc());
			log.setTransferFromTimeslotId(null);
			log.setCreateDate(now);
			log.setCreateBy("");
			log.setUpdateDate(now);
			log.setUpdateBy("");
			facTimeslotLogDao.save(log);

			List<FacilityTimeslotLogDto> logs = facTimeslotLogSvc.getLogsByFacilityNo("TENNIS", 1001l);
			Assert.assertNotNull(logs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		}
	}

}
