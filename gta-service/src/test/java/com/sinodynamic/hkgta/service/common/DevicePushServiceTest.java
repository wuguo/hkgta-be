package com.sinodynamic.hkgta.service.common;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.UserDeviceDao;
import com.sinodynamic.hkgta.entity.crm.UserDevice;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/applicationContext-service-test.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class DevicePushServiceTest {

	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	private UserDeviceDao userDeviceDao;
	
	@Transactional
	@Test
	public void testPushRegister() {
		
		try {
			String userId = "[mason]";
			String deviceArn = "test";
			String token = "test";
			String platform = "ios";
			String application = "housekeeper";
			String version = "7";
			
			userDeviceDao.deleteUserDevice(userId, application);
			devicePushService.pushRegister(userId, deviceArn, token, platform, application, version, userId);
			// then update 
			devicePushService.pushRegister(userId, "test2", token, platform, application, version, userId);
			
			UserDevice userDevice = userDeviceDao.getUserDeviceByApplication(userId, application);
			Assert.assertEquals("test2", userDevice.getArnEndpoint());
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
