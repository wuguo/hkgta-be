package com.sinodynamic.hkgta.service;

import org.apache.commons.lang.math.NumberUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.service.crm.membercash.VirtualAccountTaskService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerAdditionInfoService;

public class SampleServiceTester extends BaseServiceTester{

	@Autowired
	private CustomerAdditionInfoService customerAdditionInfoService;
	
	@Autowired
	private VirtualAccountTaskService virtualAccountTaskService;
	
	@Test
	public void test1() {
		System.out.println(customerAdditionInfoService);
		
	}
	
	
	@Test
	public void testNumber() {
		/*BigInteger bigInt = new BigInteger("10");
		Integer i = 10;
		System.out.println(NumberUtils.toLong(i.toString()));*/
		
		Long i = new Long(1) != null ? NumberUtils.toLong("1") : null;
		System.out.println(i);
	}
	
	@Test
	public void test() {
		virtualAccountTaskService.downloadVirtualAccountTransactionExtract();
	}
}
