package com.sinodynamic.hkgta.service.fms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateListDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateTimeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateTimeListDto;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.util.constant.Constant;

public class FacilityUtilizationRateTimeServiceTest extends BaseServiceTester
{

	@Autowired
	private FacilityUtilizationRateTimeService facilityUtilizationRateTimeService;

	@Test
	public void getFacilityUtilizationRateTimeList()
	{
		try
		{
			facilityUtilizationRateTimeService.getFacilityUtilizationRateTimeList(Constant.FACILITY_TYPE_GOLF);
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void getFacilityUtilizationRateTimeListBySubType()
	{
		try
		{
			facilityUtilizationRateTimeService.getFacilityUtilizationRateTimeListBySubType(Constant.FACILITY_TYPE_GOLF, "TENNISCRT-ITFID");
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void saveFacilityUtilizationRateTime()
	{
		try
		{
			FacilityUtilizationRateListDto rateDateListDto = new FacilityUtilizationRateListDto();
			rateDateListDto.setFacilityType(Constant.FACILITY_TYPE_TENNIS);
			rateDateListDto.setSubType("TENNISCRT-ITFOD");
			List<FacilityUtilizationRateTimeListDto> dayRateList = new ArrayList<FacilityUtilizationRateTimeListDto>();

			FacilityUtilizationRateTimeListDto timeDto = new FacilityUtilizationRateTimeListDto();
			timeDto.setWeekDay(Calendar.SUNDAY + "");
			List<FacilityUtilizationRateTimeDto> rateList = buildFacilityRateList();
			timeDto.setRateList(rateList);
			dayRateList.add(timeDto);

			FacilityUtilizationRateTimeListDto timeDto02 = new FacilityUtilizationRateTimeListDto();
			timeDto02.setWeekDay(Calendar.MONDAY + "");
			List<FacilityUtilizationRateTimeDto> rateList02 = buildFacilityRateList();
			timeDto02.setRateList(rateList02);
			dayRateList.add(timeDto02);

			rateDateListDto.setDayRateList(dayRateList);
			;
			facilityUtilizationRateTimeService.saveFacilityUtilizationRateTime(rateDateListDto, "");
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}

	private List<FacilityUtilizationRateTimeDto> buildFacilityRateList()
	{
		List<FacilityUtilizationRateTimeDto> rateList = new ArrayList<FacilityUtilizationRateTimeDto>();
		FacilityUtilizationRateTimeDto time01 = new FacilityUtilizationRateTimeDto();
		time01.setBeginTime(7);
		time01.setRateType(Constant.RateType.HI.name());
		rateList.add(time01);

		FacilityUtilizationRateTimeDto time02 = new FacilityUtilizationRateTimeDto();
		time02.setBeginTime(8);
		time02.setRateType(Constant.RateType.HI.name());
		rateList.add(time01);
		return rateList;
	}
}
