package com.sinodynamic.hkgta.service.pms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.sinodynamic.hkgta.service.pms.BookingFacilityService;
import com.sinodynamic.hkgta.service.pms.PMSRequestProcessorService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/config/applicationContext.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class PMSRequestProcessorServiceImplTest {

	@Autowired
	private PMSRequestProcessorService PMSRequestProcessorService;
	
	@Autowired
	private BookingFacilityService bookingFacilityService;
	
	/*@Test
	public void testQueryRightHandGolfFacilityCount () {
		int count = PMSRequestProcessorService.queryAvailableRightHandGolfFacilityCount();
		System.out.println(count);
	}
	
	@Test
	public void testcountRihtHandGolfTimeslot() throws ParseException {
		String dateStr1 = "2015-08-27 9:00:00";
		String dateStr2 = "2015-08-27 10:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date startTime = sdf.parse(dateStr1);
		Date endTime = sdf.parse(dateStr2);
		int count = PMSRequestProcessorService.countRihtHandGolfTimeslot(startTime, endTime);
		System.out.println("count=" + count);
		
	}
	
	@Test
	public void testcheckRightHandGolfFacilityStatus() throws Exception {
		String dateStr1 = "2015-08-27 9:00:00";
		String dateStr2 = "2015-08-27 10:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date startTime = sdf.parse(dateStr1);
		Date endTime = sdf.parse(dateStr2);
		boolean flag = PMSRequestProcessorService.checkRightHandGolfFacilityStatus("1", startTime, endTime);
		System.out.println("flag=" + flag);
	}
	
	@Test
	public void testqueryLatestAvailabeFacility() throws ParseException {
		String dateStr1 = "2015-08-27 9:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date startTime = sdf.parse(dateStr1);
		PMSRequestProcessorService.queryLatestAvailabeFacility(startTime);
	}
	
	@Test
	public void testfindAnyOneAvailableFacilityByTimeslot() throws ParseException {
		FacilityTimeslot timeslot = new FacilityTimeslot();
		String dateStr1 = "2015-08-27 9:00:00";
		String dateStr2 = "2015-08-27 10:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date startTime = sdf.parse(dateStr1);
		Date endTime = sdf.parse(dateStr2);
		
		timeslot.setBeginDatetime(startTime);
		timeslot.setEndDatetime(endTime);
		PMSRequestProcessorService.findAnyOneAvailableFacilityByTimeslot(timeslot);
		
	}*/
	
	@Test
	public void testbookRHGolfBayByPeriod() throws ParseException, InterruptedException {
		String dateStr1 = "2015-08-27 22:00:00";
		String dateStr2 = "2015-08-27 23:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date startTime = sdf.parse(dateStr1);
		Date endTime = sdf.parse(dateStr2);
		
		System.out.println(!startTime.after(endTime) || startTime.equals(endTime));
	}
	
	

	
	/*public long date2Date(String sdate1, String sdate2) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = sdf.parse(sdate1);
		Date date2 = sdf.parse(sdate2);

		return date2.getTime() / (24 * 60 * 60 * 1000) - date1.getTime()
				/ (24 * 60 * 60 * 1000);
	}

	@Test
	public void testDate2Date() throws Exception {
		long date = date2Date("2012-4-11", "2012-4-23");
		System.out.println("相差：" + date);

	}
	*/
	@Test
	public void testgiftBundleByPeriod() throws ParseException, InterruptedException {
		
		for (int i =0;i<20;i++) {
			String dateStr1 = "2015-09-03";
			String dateStr2 = "2015-09-07";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			Date arriveDate = sdf.parse(dateStr1);
			Date departDate = sdf.parse(dateStr2);
			Map<Date,Long> map = PMSRequestProcessorService.giftBundleByPeriod(2L, arriveDate, departDate,"GOLF","GOLFBAYTYPE-RH","ACT");
			for (Date date:map.keySet()) {
				System.out.println("Date:" + date + ",reservID =" + map.get(date));
			}
		}
		
	}
	
	
	
}
