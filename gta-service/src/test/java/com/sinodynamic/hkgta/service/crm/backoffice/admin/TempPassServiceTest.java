package com.sinodynamic.hkgta.service.crm.backoffice.admin;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.crm.ContractHelperDto;
import com.sinodynamic.hkgta.dto.crm.TempPassProfileDto;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.util.CommUtil;

public class TempPassServiceTest extends BaseServiceTester {
    
    private static final String TEST_CARD_ID = "000002059";
    private static final String TEST_CUSTOMER_ID = "1";
    private static final String LOGIN_USER = "Tester";
    private Logger logger = Logger.getLogger(TempPassServiceTest.class);

    @Autowired
    private TempPassService tempPassService;
    
    @Autowired
    private InternalPermitCardService internalPermitCardService;
    
    @Test
    public void returnInternalPermitCard() {
	
	logger.info("----------------------------returnInternalPermitCard----------------------------------");
	    
	try {
	    
	    prepareTestDomain(TEST_CARD_ID, TEST_CUSTOMER_ID, "ISS");
	    tempPassService.returnInternalPermitCard(TEST_CARD_ID, LOGIN_USER, null);
	    
	} catch (Exception e) {
	    
	    Assert.fail(e.getMessage());
	}
	

    }

    @Test
    public void linkCard() {
	
	logger.info("-----------------------------linkCard---------------------------------");
	
	try {
	    
	    prepareTestDomain(TEST_CARD_ID, null, null);
	    TempPassProfileDto linkDto = new TempPassProfileDto();
	    linkDto.setCardId(TEST_CARD_ID);
	    linkDto.setContractorId(Integer.parseInt(TEST_CUSTOMER_ID));
	    tempPassService.linkCard(linkDto);
	    
	    prepareTestDomain(TEST_CARD_ID, TEST_CUSTOMER_ID, "ISS");
	    prepareTestDomain("000002069", null, null);
	    TempPassProfileDto replaceDto = new TempPassProfileDto();
	    replaceDto.setCardId("000002069");
	    replaceDto.setContractorId(Integer.parseInt(TEST_CUSTOMER_ID));
	    tempPassService.linkCard(replaceDto);
	    
	} catch (Exception e) {
	    
	    Assert.fail(e.getMessage());
	}
    }

    @Test
    public void updateProfile() {
	
	logger.info("------------------------------updateProfile--------------------------------");
	
	try {
	    
	    ContractHelperDto dto = new ContractHelperDto();
	    dto.setPeriodFrom("2015-09-25");
	    dto.setPeriodTo("2015-10-25");
	    dto.setInternalRemark("test");
	    dto.setHelperPassType("1");
	    tempPassService.updateProfile(dto);
	    
	} catch (Exception e) {
	    
	    Assert.fail(e.getMessage());
	}

    }

    @Test
    public void createTempPassProfile() {
	
	logger.info("-----------------------------createTempPassProfile---------------------------------");
	
	try {
	    
	    ContractHelperDto dto = new ContractHelperDto();
	    dto.setCreateBy(LOGIN_USER);
	    dto.setUpdateBy(LOGIN_USER);
	    dto.setCreateDate(new Date());
	    dto.setUpdateDate(new Date());
	    dto.setPassportType("1");
	    dto.setPassportNo("M1216");
	    dto.setSurname("Jh");
	    dto.setGivenName("Mason");
	    dto.setGender("M");
	    dto.setPeriodFrom("2015-09-25");
	    dto.setPeriodTo("2015-10-25");
	    dto.setHelperPassType("1");
	    dto.setCompanyName("epam");
	    dto.setPhoneMobile("13653658212");
	    dto.setInternalRemark("test");
	    tempPassService.createTempPassProfile(dto);
	    
	} catch (Exception e) {
	    
	    Assert.fail(e.getMessage());
	}
    }

    @Test
    public void getTempPassProfileList() {
	
	logger.info("-----------------------------getTempPassProfileList---------------------------------");
	
	try {
	    
	    String[] statuses = {"ACT", "NACT", "EXP"};
	    for (String status : statuses) {
		tempPassService.getTempPassProfileList(status);
	    }
	    
	} catch (Exception e) {
	    
	    Assert.fail(e.getMessage());
	}
    }

    @Test
    public void getTempPassProfileHistoryList() {
	
	logger.info("---------------------------------getTempPassProfileHistoryList-----------------------------");
	
	try {
	    
	    String[] statuses = {"ACT", "NACT", "EXP"};
	    for (String status : statuses) {
		tempPassService.getTempPassProfileHistoryList(status);
	    }
	    
	} catch (Exception e) {
	    
	    Assert.fail(e.getMessage());
	}
    }
	
    
    private InternalPermitCard prepareTestDomain(String cardId, String contractorId, String status) throws Exception {
	
	InternalPermitCard card = internalPermitCardService.getCardById(cardId);
	if (card != null) {
	    
	    card.setStatus(status);
	    if (!StringUtils.isEmpty(contractorId)) {
		card.setContractorId(Long.parseLong(contractorId));
	    }
	    internalPermitCardService.updateCard(card);
	    
	} else {
	    
	    card = new InternalPermitCard();
	    card.setCardId(Long.parseLong(CommUtil.cardNoTransfer(cardId)));
	    card.setStatus(status);
	    if (!StringUtils.isEmpty(contractorId)) {
		card.setContractorId(Long.parseLong(contractorId));
	    }
	    internalPermitCardService.saveCard(card);
	}
	
	return card;
    }
}
