package com.sinodynamic.hkgta.service.crm.backoffice.advertise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.dto.crm.AdvertiseImageDto;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageUploadDto;
import com.sinodynamic.hkgta.entity.crm.AdvertiseImage;
import com.sinodynamic.hkgta.service.BaseServiceTester;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public class AdvertiseImageServiceTester extends BaseServiceTester {

	@Autowired
	private AdvertiseImageService advertiseImageService;

	@Before
	public void setUp() {
		AdvertiseImageDto dto = new AdvertiseImageDto();
		dto.setAppType("mapp");
		dto.setDispLoc("mscr");
		dto.setFilepath("test_me.jpg");
		dto.setDispOrder(1L);
		advertiseImageService.create(dto);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void getAdImg() {

		ResponseResult resp = advertiseImageService.getList("mapp", null);
		Assert.assertNotNull(resp);
		Assert.assertEquals("0", resp.getReturnCode());
		Assert.assertNotNull(resp.getData());

		Map<String, Object> data = (Map<String, Object>) resp.getData();
		if (data == null || data.values().size() == 0) {
			return; // in case
		}
		List<AdvertiseImageDto> list = (List<AdvertiseImageDto>) data.values().iterator().next();
		if (list == null || list.size() == 0) {
			return; // in case
		}

		AdvertiseImageDto expDto = list.get(0);
		AdvertiseImage actlEntity = advertiseImageService.get(expDto.getImgId());
		Assert.assertNotNull(actlEntity);
		Assert.assertTrue(expDto.getImgId() == actlEntity.getImgId());
		Assert.assertTrue(expDto.getAppType().equals(actlEntity.getApplicationType()));
		Assert.assertTrue(expDto.getDispLoc().equals(actlEntity.getDisplayLocation()));
		Assert.assertTrue(expDto.getFilepath().equals(actlEntity.getServerFilename()));

	}

	@Test
	public void createAndDeleteAdImg() {

		AdvertiseImageDto dto = new AdvertiseImageDto();
		dto.setAppType("mapp");
		dto.setDispLoc("mscr");
		dto.setFilepath("test_me.jpg");
		dto.setDispOrder(1L);

		ResponseResult resp = advertiseImageService.create(dto);
		Assert.assertNotNull(resp);
		Assert.assertEquals("0", resp.getReturnCode());
		Assert.assertNotNull(resp.getData());
		dto = (AdvertiseImageDto) resp.getData();

		AdvertiseImage entity = advertiseImageService.get(dto.getImgId());
		Assert.assertNotNull(entity);

		resp = advertiseImageService.delete(entity);
		Assert.assertNotNull(resp);
		Assert.assertEquals("0", resp.getReturnCode());

		entity = advertiseImageService.get(dto.getImgId());
		Assert.assertNull(entity);

	}
	
	@Test
	public void createAdvertiseImages() {
		AdvertiseImageUploadDto uploadDto = new AdvertiseImageUploadDto();
		List<AdvertiseImageDto> advertiseImages = new ArrayList<AdvertiseImageDto>();
		AdvertiseImageDto imageDto1 = createAdertiseImageDto("MAPP","MSCR",0L,1L,"aaa.jpg");
		AdvertiseImageDto imageDto2 = createAdertiseImageDto("MAPP","MSCR",1L,1L,"aaa.jpg");
		AdvertiseImageDto imageDto3 = createAdertiseImageDto("MAPP","MSCR",0L,2L,"aaa.jpg");
		AdvertiseImageDto imageDto4 = createAdertiseImageDto("MAPP","MSCR",1L,2L,"aaa.jpg");
		advertiseImages.add(imageDto1);
		advertiseImages.add(imageDto2);
		advertiseImages.add(imageDto3);
		advertiseImages.add(imageDto4);
		uploadDto.setAdvertiseImages(advertiseImages);
		advertiseImageService.createAdvertiseImages("MSCR",uploadDto, "system");
	}
	
	@Test
	public void getAdvertiseImageList() {
		advertiseImageService.getAdvertiseImageList("MAPP","MSCR");
	}

	private AdvertiseImageDto createAdertiseImageDto(String appType,String dispLoc,Long linkLevel,Long dispOrder,String filePath)
	{
		AdvertiseImageDto imageDto = new AdvertiseImageDto();
		imageDto.setAppType(appType);
		imageDto.setDispLoc(dispLoc);
		imageDto.setLinkLevel(linkLevel);
		imageDto.setDispOrder(dispOrder);
		imageDto.setFilepath(filePath);
		return imageDto;
	}

}
