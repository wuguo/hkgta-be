//package com.sinodynamic.hkgta.service.pms;
//
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//
//import org.apache.commons.lang.time.DateFormatUtils;
//import org.joda.time.DateTime;
//import org.junit.Assert;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.sinodynamic.hkgta.dto.pms.CheckHotelAvailableDto;
//import com.sinodynamic.hkgta.dto.pms.RoomResDto;
//import com.sinodynamic.hkgta.dto.pms.RoomReservationDto;
//import com.sinodynamic.hkgta.dto.pms.RoomStayDto;
//import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
//import com.sinodynamic.hkgta.service.BaseServiceTester;
//import com.sinodynamic.hkgta.util.response.MessageResult;
//
//
//@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
//public class HotelReservationServiceTest extends BaseServiceTester{
//
//	@Autowired
//	private HotelReservationServiceFacade hotelReservationServiceFacade;
//	
//	@Autowired
//	private HotelReservationService hotelReservationService;
//	
//	@Autowired
//	private PMSApiService pmsApiService;
//	
//	@Test
//	@Transactional
//	public void testBookingHotel() throws Exception{
//		DateTime dateTime = new DateTime(new Date());
//		Date start = dateTime.dayOfYear().addToCopy(2).toLocalDate().toDate();
//		Date end = dateTime.dayOfYear().addToCopy(3).toLocalDate().toDate();
//		
//		String startStr = DateFormatUtils.format(start, "yyyy-MM-dd");
//		String endStr = DateFormatUtils.format(end, "yyyy-MM-dd");
//		
//		CheckHotelAvailableDto dto = new CheckHotelAvailableDto(startStr, endStr, 2, 0);
//		
//		MessageResult msgResult = pmsApiService.getkHotelAvailable(dto);
//		
//		RoomStayDto availableRooms = null;
//		if(msgResult.isSuccess()){
//			availableRooms = (RoomStayDto) msgResult.getObject();
//		}else{
//			Assert.fail("Oasis system get room types fail!");
//		}
//		
//		
//		RoomResDto rsv = new RoomResDto();
//		
//		
//		Assert.assertTrue("Oasis system doesn't have available room types!", availableRooms.getRoomTypes().size() > 0);
//		
//		
//		rsv.setRoomTypeCode(availableRooms.getRoomTypes().get(0).getRoomTypeCode());
//		rsv.setRoomRateCode("ONLRACK");
//		rsv.setStartDate(startStr);
//		rsv.setEndDate(endStr);
//		rsv.setRoomBookingCount(2);
//		rsv.setCustomerId(4l);
//		
//		List<RoomResDto> reservations = Arrays.asList(rsv);
//		
//		List<RoomReservationDto> bookingResults = hotelReservationServiceFacade.bookHotel(reservations);
//		
//		Assert.assertTrue(bookingResults.size() == 2);
//		
//		for(RoomReservationDto booking : bookingResults){
//			MessageResult msg = pmsApiService.ignoreReservation(booking.getReservationId());
//			if(!msg.isSuccess()){
//				Assert.fail("Oasis system test fail!");
//			}
//		}
//	}
//	
//	@Test
//	public void testCashvaluePayment(){
//		
//	}
//}
