package com.sinodynamic.hkgta.mock;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public class Json2ResponseResultConverter {

	/**
	 * 根据文件名称获取数据
	 * @param fileName
	 * @return
	 */
	 private static String getJsonDataByString(String fileName) {
		 InputStream in = Json2ResponseResultConverter.class.getClassLoader().getResourceAsStream("json/" + fileName + ".json");
		 BufferedReader br = null;
		 StringBuilder sb = new StringBuilder();
		 String str = null;
	        try {
	        	 br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
	        	 while ((str = br.readLine()) != null) {
					sb.append(str);
				}
	        } catch (Exception e) {
	         e.printStackTrace();
	         return null;
	        }
	        return sb.toString();
	 }

	 
	 /**
	  * 根据文件名称获取数据 【需封装在返回给调用者，未做非空判断】
	  * @param fileName
	  * @return
	  */
	public static String convertDateByString(String fileName){
			return getJsonDataByString(fileName);
	}
		
	/**
	 * 根据文件名称获取数据【直接返回，无需再次封装解析】
	 * @param fileName
	 * @return
	 */
	public static ResponseResult convert(ResponseResult responseResult, String fileName){
		String jsonData = getJsonDataByString(fileName);
		if (StringUtils.isBlank(jsonData)) {
			responseResult.initResult(GTAError.TemplateError.MESSAGE_TEMPLATE_NO_FOUND, "json文件名错误或json文件内容缺失！");
		} else{
			try {
				Data data = new Data();
				ArrayList<JSONObject> list = new ArrayList<JSONObject>();
				list.add(JSONObject.parseObject(jsonData));
				//data.setList(list);
				//data.setLastPage(false);
				//data.setCurrentPage(1);
				//responseResult.setErrorMessageEN("Success");
				//responseResult.setReturnCode("0");
				responseResult.initResult(GTAError.Success.SUCCESS, JSONObject.parseObject(jsonData));
				//responseResult.setData(data);
			} catch (Exception e) {
				responseResult.initResult(GTAError.TemplateError.MESSAGE_TEMPLATE_NO_FOUND, "json文件内容错误，请联系API开发人员！");
			}
		}
		return responseResult;
	}
	
	

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getJsonDataByString("dayPass"));

	}
	

	
}
