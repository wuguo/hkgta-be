package com.sinodynamic.hkgta.scheduler.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/ImportRestaurantDataTaskService.xml", "classpath:/config/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class ImportRestaurantDataTaskServiceTest {

	@Autowired
	private ImportRestaurantDataTaskService	importRestaurantDataTaskService;

	@Test
	public void testImportRestaurant() {
		System.out.println("test import1");
		try {
			importRestaurantDataTaskService.importRestaurant();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
