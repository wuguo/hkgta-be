package com.sinodynamic.hkgta.scheduler.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath*:/config/applicationContext-test.xml" })
@ComponentScan("com.sinodynamic.hkgta.scheduler.service")
public class DownloadCarParkMemberTaskServiceTest {

	
	//@Autowired 
	DownloadCarParkMemberTaskService downloadCarParkMemberTaskService=new DownloadCarParkMemberTaskService();
	
	@Test
	public void testHandleCarLicensePlate() {		
		System.out.println("Abc123/DDD222,CC1233 -> " + downloadCarParkMemberTaskService.handlerLicencePlateNo("Abc123/DDD222,CC1233"));
		
		System.out.println("/DDD222,CC1233 -> " + downloadCarParkMemberTaskService.handlerLicencePlateNo("/DDD222,CC1233"));
		
		System.out.println("Abc  123/ -> " + downloadCarParkMemberTaskService.handlerLicencePlateNo("Abc123/"));
		
		System.out.println("{}" + downloadCarParkMemberTaskService.handlerLicencePlateNo(""));

		System.out.println("          ->" + downloadCarParkMemberTaskService.handlerLicencePlateNo("          "));
		
		System.out.println("     ?     ->" + downloadCarParkMemberTaskService.handlerLicencePlateNo("    ?      "));
		
		System.out.println("abcd=1234 ->" + downloadCarParkMemberTaskService.handlerLicencePlateNo("abcd=1234"));
		
		System.out.println("abcd,1234 ->" + downloadCarParkMemberTaskService.handlerLicencePlateNo("abcd,1234"));
		
		System.out.println("ACC CC/1234 ->" + downloadCarParkMemberTaskService.handlerLicencePlateNo("ACC CC/1234"));
	}
	
}
