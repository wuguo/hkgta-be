package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoPushMessage4SilentCheckinService;

/**
 * <p>
 * Auto push message to check in within toady
 * </p>
 * 
 *
 */
public class AutoPushMessage4SilentCheckinTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(AutoPushMessage4SilentCheckinTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		
		logger.info("AutoPushMessage4SilentCheckinTask run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		AutoPushMessage4SilentCheckinService service = applicationContext.getBean(AutoPushMessage4SilentCheckinService.class);

		service.autoPushMsg4SilentCheckin();
		logger.info("AutoPushMessage4SilentCheckinTask auto execute end ...");

	}
}
