package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.CarparkDataImportTaskService;

/**
 * download carpark data from integration server sftp SINO file name format
 * ACCESSyyyyMMddHHmmss.Send.txt
 * 
 * @author christ
 *
 */
public class CarparkDataImportTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger logger = LoggerFactory.getLogger(CarparkDataImportTask.class);

	WebApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("CarparkDataImportTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		CarparkDataImportTaskService service = applicationContext.getBean(CarparkDataImportTaskService.class);

		service.excute();

		logger.info("CarparkDataImportTask  auto execute end ...");
	}

}
