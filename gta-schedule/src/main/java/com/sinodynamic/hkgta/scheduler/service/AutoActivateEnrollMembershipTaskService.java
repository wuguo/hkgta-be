package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.crm.ActivateMemberDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Component
public class AutoActivateEnrollMembershipTaskService {

	private Logger				logger					= Logger.getLogger(AutoActivateEnrollMembershipTaskService.class);
	private static String		defaultLoginUserId		= "Auto";
	private static String		defaultLoginUserName	= "HKGTA System";

	@Autowired
	CustomerEnrollmentService	customerEnrollmentService;

	@Autowired
	MemberService				memberService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void AutoActivateEnrollMembership() {
		List<CustomerEnrollment> list = customerEnrollmentService.getEnrollmentMembersByStatus(EnrollStatus.TOA.name());

		try {
			for (CustomerEnrollment customerEnrollment : list) {
				Member member = memberService.getMemberById(customerEnrollment.getCustomerId());
				Long timeDiff = null;
				if (member.getEffectiveDate() == null) {
					logger.info("AutoActivateEnrollMembershipTaskService.AutoActivateEnrollMembership()-- Customer Id: " + member.getCustomerId()+" Academy Id:"+member.getAcademyNo()
							+ " Desired Activation Date Required");
				} else {
					timeDiff = CompareDates(new Date(), member.getEffectiveDate());
				}
				if (timeDiff == null || timeDiff >= 0) {
					ActivateMemberDto activeMemberDto = new ActivateMemberDto();
					activeMemberDto.setCustomerId(customerEnrollment.getCustomerId());
					try {
						customerEnrollmentService.encapsulateActivationMember(activeMemberDto, defaultLoginUserId, defaultLoginUserName);
						logger.info("AutoActivateEnrollMembershipTaskService.AutoActivateEnrollMembership()-- Customer Id: " + member.getCustomerId()+" Academy Id:"+member.getAcademyNo()
								+ " Activation Status: Successfull");
					} catch (GTACommonException EE) {
						logger.error("AutoActivateEnrollMembershipTaskService.AutoActivateEnrollMembership()-- Academy Id:"+member.getAcademyNo() + " Activation Status: fail:" + EE.getError());
						System.out.println("AutoActivateEnrollMembershipTaskService.AutoActivateEnrollMembership()-- Academy Id:"+member.getAcademyNo() + " Activation Status: fail:" + EE.getError());
						continue;
					} catch (Exception e) {
						logger.error("AutoActivateEnrollMembershipTaskService.AutoActivateEnrollMembership()-Academy Id:"+member.getAcademyNo()+ " Activation Status: fail:" +"-Unexpected Exception", e);
						continue;
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoActivateEnrollMembershipTask", e);
		}
	}

	private static Long CompareDates(Date date1, Date date2) {

		if (date1 == null || date2 == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Long dateDiff;
		try {
			dateDiff = df.parse(df.format(date1)).getTime() - df.parse(df.format(date2)).getTime();
			return dateDiff;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
}
