package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.adm.StaffMasterService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class StaffDisposeTaskService {
	private final Logger logger = Logger.getLogger(StaffDisposeTaskService.class);
	
	@Autowired
	StaffMasterService staffMasterService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void autoDisposeQuitStaff() {
		try {
			staffMasterService.autoDisposeQuitStaff();
		} catch (Exception e) {
			alarmEmailService.sendAlarmEmailTask("StaffDisposeTask", e);
		}
	} 
}
