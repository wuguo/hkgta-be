package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.sys.CarparkDataHandler;

@Component
public class CarparkDataImportTaskService {
	private Logger logger = Logger.getLogger(CarparkDataImportTaskService.class);
	@Autowired
	private CarparkDataHandler handler;

	public void excute() {
		logger.info("excute carpark data handler start...");
		handler.excute();
		logger.info("excute carpark data handler end...");
	}
}
