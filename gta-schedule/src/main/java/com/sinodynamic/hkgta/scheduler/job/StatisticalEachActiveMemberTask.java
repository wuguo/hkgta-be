package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoCloseCourseTaskService;
import com.sinodynamic.hkgta.scheduler.service.StatisticalEachActiveMemberTaskService;

/**
 * <p>
 * Auto close golf/tennis course when course's regist_due_date have reached
 * </p>
 * 
 * @author Mianping Wu
 *
 */
public class StatisticalEachActiveMemberTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(StatisticalEachActiveMemberTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("statisticalEachActiveMemberTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		StatisticalEachActiveMemberTaskService autoCloseCourseTaskService = applicationContext.getBean(StatisticalEachActiveMemberTaskService.class);

		autoCloseCourseTaskService.statisticalEachActiveMembers();

		logger.info("statisticalEachActiveMemberTask.autoSendingJob auto execute end ...");
	}

}
