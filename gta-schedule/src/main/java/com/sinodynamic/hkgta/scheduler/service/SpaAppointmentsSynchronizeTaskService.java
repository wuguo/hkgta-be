package com.sinodynamic.hkgta.scheduler.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.mms.SpaAppointmentSynDto;
import com.sinodynamic.hkgta.integration.spa.em.AppointmentStatus;
import com.sinodynamic.hkgta.integration.spa.request.GetAppointmentsRequest;
import com.sinodynamic.hkgta.integration.spa.request.GetPaymentsRequest;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDatum;
import com.sinodynamic.hkgta.integration.spa.response.GetAppointmentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.GetPaymentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.Payment;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.service.mms.SynchronizeSpaData;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.LoggerType;

@Component
public class SpaAppointmentsSynchronizeTaskService {

	private Logger logger = Logger.getLogger(SpaAppointmentsSynchronizeTaskService.class);

	private Logger mmsLog = Logger.getLogger(LoggerType.MMS.getName());

	private static final int APPOINTMENT_DAYS_AFTERWORD = 14;

	@Autowired
	private SynchronizeSpaData synchronizeSpaData;

	@Autowired
	private SpaServcieManager spaServcieManager;
	
	/***
	 * sysch MMS appointment
	 */
	public void excuteSynchAppointments() {
		Date now = new Date();
		Date fromDate = synchronizeSpaData.getAppointmentsLatestSynSuccessTime(now);
		Date toDate = DateCalcUtil.getNearDay(now, APPOINTMENT_DAYS_AFTERWORD);

		GetAppointmentsRequest request = new GetAppointmentsRequest();
		request.setFromDate(fromDate);
		request.setToDate(toDate);
		request.setStatus(AppointmentStatus.ALL);
		mmsLog.info("---appointmentSynchronize----request--->>>" + request);
		GetAppointmentsResponse appointmentsResponse = spaServcieManager.getAppointments(request);
		mmsLog.info(fromDate + "--fromDate--toDate-->>" + toDate + "--appointmentSynchronize--response---->>>"
				+ appointmentsResponse);
		List<SpaAppointmentSynDto> appointmentResult = formatAppointmentData(appointmentsResponse);
		if (appointmentResult == null || appointmentResult.size() == 0)
			return;
		Map<String, List<SpaAppointmentSynDto>> appointmentGroup = this.groupingAppointments(appointmentResult);
		if (appointmentGroup == null || appointmentGroup.isEmpty())
			return;
		// iterator for synchronize
		for (Map.Entry<String, List<SpaAppointmentSynDto>> entry : appointmentGroup.entrySet()) {
			mmsLog.info("start synchronizeAppointmentsNew.................");
			try {
				synchronizeSpaData.synchronizeAppointmentsNew(entry);

			} catch (Exception ex) {
				logger.error(ex.getMessage());
			}
			mmsLog.info("end synchronizeAppointmentsNew.................");
		}

	}
	/***
	 * synch MMS Payment
	 */
	public void excuteSynchPaymentments() {
		Date toDate = new Date();
		Date fromDate = synchronizeSpaData.getPaymentsLatestSynSuccessTime(toDate);
		
		GetPaymentsRequest request = new GetPaymentsRequest();
		request.setFromDate(fromDate);
		request.setToDate(toDate);
		mmsLog.info("---paymentSynchronize----request--->>>" + request);
		GetPaymentsResponse response = spaServcieManager.getPayments(request);
		mmsLog.info(fromDate + "--fromDate--toDate-->>" + toDate + "--paymentSynchronize--response---->>>" + response);
		if (response == null || response.getPayments() == null || response.getPayments().size() == 0)
			return;
		List<Payment> payments = response.getPayments();
		Map<String, List<Payment>> groupingResult = this.groupingPayments(payments);
		if (groupingResult == null || groupingResult.size() == 0)
			return;
		for (Map.Entry<String, List<Payment>> entry : groupingResult.entrySet()) {
			mmsLog.info("start synchronizeAppointmentsNew.................");
			try {
				synchronizeSpaData.synchronizePaymentsNew(entry);

			} catch (Exception ex) {
				mmsLog.error(ex.getMessage());
			}
			mmsLog.info("end synchronizeAppointmentsNew.................");
		}

	}
	private List<SpaAppointmentSynDto> formatAppointmentData(GetAppointmentsResponse appointmentsResponse) {

		if (appointmentsResponse == null || appointmentsResponse.getAppointmentData() == null
				|| appointmentsResponse.getAppointmentData().size() == 0)
			return null;

		List<AppointmentDatum> appointmentData = appointmentsResponse.getAppointmentData();
		List<SpaAppointmentSynDto> appointmentResult = new ArrayList<SpaAppointmentSynDto>();
		for (AppointmentDatum appointment : appointmentData) {

			SpaAppointmentSynDto dto = new SpaAppointmentSynDto();
			dto.setExtAppointmentId(appointment.getAppointmentId());
			dto.setExtServiceCode(appointment.getServiceCode());
			dto.setExtInvoiceNo(appointment.getInvoiceNo());
			dto.setGuestCode(appointment.getGuestCode());
			dto.setServicePrice(appointment.getFinalSalePrice());
			dto.setServiceInternalCost(appointment.getServiceInternalCost());
			dto.setTherapistNo(appointment.getEmployeeCode());
			dto.setServiceTime(appointment.getServiceTime());
			dto.setServiceDescription(null);
			dto.setStatus(appointment.getStatus());
			dto.setStartTime(appointment.getStartTime());
			dto.setEndTime(appointment.getEndTime());
			dto.setServiceName(appointment.getServiceName());
			dto.setActualTime(appointment.getActualTime());
			dto.setRebooked(appointment.getRebooked());
			dto.setCheckInTime(appointment.getCheckInTime());
			dto.setCreatedBy(appointment.getCreatedby());
			dto.setCreationDate(appointment.getCreationDate());
			dto.setExtNote(appointment.getNote());
			dto.setEmployeeFName(appointment.getEmployeeFName());
			dto.setEmployeeLName(appointment.getEmployeeLName());
			appointmentResult.add(dto);
		}

		return appointmentResult;
	}

	private Map<String, List<SpaAppointmentSynDto>> groupingAppointments(List<SpaAppointmentSynDto> result) {
		if (result == null || result.size() == 0)
			return null;

		Map<String, List<SpaAppointmentSynDto>> groupResult = new HashMap<String, List<SpaAppointmentSynDto>>();
		for (SpaAppointmentSynDto dto : result) {

			String key = dto.getExtInvoiceNo();
			if (StringUtils.isEmpty(key))
				continue;
			List<SpaAppointmentSynDto> value = groupResult.get(key);
			if (value == null) {
				value = new ArrayList<SpaAppointmentSynDto>();
				value.add(dto);
				groupResult.put(key, value);
			} else {
				value.add(dto);
			}
		}
		return groupResult;
	}

	private Map<String, List<Payment>> groupingPayments(List<Payment> result) {

		if (result == null || result.size() == 0)
			return null;
		Map<String, List<Payment>> groupResult = new HashMap<String, List<Payment>>();
		for (Payment p : result) {
			String key = p.getInvoiceNo();
			if (StringUtils.isEmpty(key))
				continue;
			List<Payment> value = groupResult.get(key);
			if (value == null) {
				value = new ArrayList<Payment>();
				value.add(p);
				groupResult.put(key, value);
			} else {
				value.add(p);
			}
		}
		return groupResult;
	}
}
