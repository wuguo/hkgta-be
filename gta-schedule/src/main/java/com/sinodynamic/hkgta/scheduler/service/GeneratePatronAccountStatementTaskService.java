package com.sinodynamic.hkgta.scheduler.service;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.DateCalcUtil;

@Component
public class GeneratePatronAccountStatementTaskService {
	private Logger						logger	= Logger.getLogger(GeneratePatronAccountStatementTaskService.class);
	@Autowired
	private CustomerOrderTransService	customerOrderTransService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void generatePatronAccountStatement(String startDate,String endDate) {
		try {
			String startDateString = DateCalcUtil.previousMonthFirstDay();
			String endDateString = DateCalcUtil.previousMonthLastDay();
			if(!StringUtils.isEmpty(startDate)&&!StringUtils.isEmpty(endDate)){
				 startDateString = startDate;
				 endDateString =endDate;
			}
			customerOrderTransService.generatePatronAccountStatement(startDateString,endDateString);
		} catch (Exception e) {
			logger.error("Generate patron account statement failed!" + e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("GeneratePatronAccountStatementTask", e);
		}
	}
}
