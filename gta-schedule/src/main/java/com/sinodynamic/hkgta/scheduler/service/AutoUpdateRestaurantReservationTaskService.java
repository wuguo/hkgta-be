package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.pos.RestaurantMasterDao;
import com.sinodynamic.hkgta.service.pos.RestaurantMasterService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class AutoUpdateRestaurantReservationTaskService {
	private Logger logger = Logger.getLogger(AutoUpdateRestaurantReservationTaskService.class);

    @Autowired
	private RestaurantMasterService restaurantMasterService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void autoUpdateReservationStatus() {
		try {
			updateStatus();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoUpdateRestaurantReservationTask", e);
		}
	}


	@Transactional
	public void updateStatus() {
    //private void updateStatus() {		
		/* Critical bug: @Transactional method cannot be private (SAMHUI20160331) 
		   http://192.168.1.235:9000/dashboard/index?id=com.hkgta.coreservices%3Agta-schedule%3Asrc%2Fmain%2Fjava%2Fcom%2Fsinodynamic%2Fhkgta%2Fscheduler%2Fservice%2FAutoUpdateRestaurantReservationTaskService.java  
		*/		
		// get all member facility reservation
		restaurantMasterService.updateRestaurantReservationStatus();
	}

}
