package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;

@Component
public class FlexPaymentReportTaskService {
	
	private Logger			logger	= Logger.getLogger(AutoCloseCourseTaskService.class);
	
	@Autowired
	private CustomerOrderTransService  customerOrderTransService;
	
	public void  createReportCSV(){
		try{
			customerOrderTransService.createFlexPaymentReport(new Date());
			
		}catch(Exception ex){
			logger.error(ex.getMessage(),ex);
		}
	}
}
