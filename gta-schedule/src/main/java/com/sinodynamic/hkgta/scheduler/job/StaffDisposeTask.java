package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.StaffDisposeTaskService;

/**
 * 
 * Auto scan the staff_master's quit_date and change the staff's status to disposal when the staff's quit date is current date.
 * 
 * @author Kaster 20160217
 *
 */
public class StaffDisposeTask extends QuartzJobBean implements ApplicationContextAware {
	
	private final Logger logger = LoggerFactory.getLogger(StaffDisposeTask.class);
	
	WebApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("StaffDisposeTask at:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		StaffDisposeTaskService staffDisposeTaskService = applicationContext.getBean(StaffDisposeTaskService.class);
		staffDisposeTaskService.autoDisposeQuitStaff();
		logger.info("StaffDisposeTask executed end!");
	}

}
