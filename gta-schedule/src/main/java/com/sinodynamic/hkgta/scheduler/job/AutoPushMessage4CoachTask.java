package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoPushMessage4CoachTaskService;

/**
 * <p>
 * Auto push message to coach in adavance 2 days
 * </p>
 * 
 * @author Zero Wang
 *
 */
public class AutoPushMessage4CoachTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(AutoPushMessage4CoachTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoPushMessage4CoachTask run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		AutoPushMessage4CoachTaskService service = applicationContext.getBean(AutoPushMessage4CoachTaskService.class);

		service.autoPushMsgInAdvance2Day();
		logger.info("AutoPushMessage4CoachTask auto execute end ...");

	}
}
