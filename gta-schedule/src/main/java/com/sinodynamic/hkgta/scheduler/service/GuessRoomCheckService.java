package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.crm.CustomerProfileDao;
import com.sinodynamic.hkgta.dto.pms.CancelReservationDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.pms.HotelReservationService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.response.MessageResult;

@Component
public class GuessRoomCheckService {
	private Logger						logger	= Logger.getLogger(GuessRoomCheckService.class);

	@Resource(name = "appProperties")
	private Properties					appProps;

	@Autowired
	private HotelReservationService		hotelReservationService;

	@Autowired
	private PMSApiService				pmsApiService;

	@Autowired
	private CustomerProfileDao			customerProfileDao;

	@Autowired
	private CustomerOrderTransService	customerOrderTransService;

	@Autowired
	private AlarmEmailService	alarmEmailService;

	@Autowired
	private MemberService memberService;
	@Transactional
	public void checkGuessRoomStatus() {
		logger.info("checkGuessRoomStatus run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		List<RoomReservationRec> bookings = hotelReservationService.getAllFailedReservations(customerOrderTransService.getOrderTimeoutMin()
				.intValue());
		logger.info("RoomReservationRec FailedReservations booking size:"+(null==bookings?0:bookings.size()));
		MessageResult msgRes = null;
		for (RoomReservationRec book : bookings) {
			try {
				List<Member>members=memberService.getMembersByCustomerId(book.getCustomerId().toString());
				String acadeNo=null;
				if(null!=members&&members.size()>0){
					acadeNo=members.get(0).getAcademyNo();
				}
				
				if (StringUtils.equalsIgnoreCase(book.getStatus(), RoomReservationStatus.PAY.name())) {
					CustomerProfile profile = customerProfileDao.getCustomerProfileByCustomerId(book.getCustomerId());
					CancelReservationDto cancel = new CancelReservationDto();
					cancel.setReservationTimeSpanEnd(DateFormatUtils.format(book.getArrivalDate(), "yyyy-MM-dd"));
					cancel.setReservationTimeSpanEnd(DateFormatUtils.format(book.getDepartureDate(), "yyyy-MM-dd"));
					cancel.setResId((String) book.getConfirmId());
					cancel.setMembershipID(profile.getAcademyNo());

					msgRes = pmsApiService.cancelReservation(cancel);
					logger.info("pmsApiService.cancelReservation() : " + book.getConfirmId()+" member acadeNo:"+acadeNo);
				} else if (StringUtils.equalsIgnoreCase(book.getStatus(), RoomReservationStatus.RSV.name())) {
					msgRes = pmsApiService.ignoreReservation(book.getConfirmId());
					logger.info("pmsApiService.ignoreReservation() : " + book.getConfirmId()+" member acadeNo:"+acadeNo);
				} else {
					continue;
				}

				// Directly update status to CAN, don't rollback even though exception happens
				hotelReservationService.updateRoomReservationStatus(book.getConfirmId(), RoomReservationStatus.CAN);
				logger.info("hotelReservationService.updateRoomReservationStatus(" + book.getConfirmId() + ", RoomReservationStatus.CAN)"+" member acadeNo:"+acadeNo);

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				alarmEmailService.sendAlarmEmailTask("OnlinePaymentCheckTask", e);
			}
		}

		logger.info("GuessRoomCheckService.checkGuessRoomStatus auto execute end ...");
	}
}
