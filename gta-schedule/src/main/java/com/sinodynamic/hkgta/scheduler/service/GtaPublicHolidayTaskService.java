package com.sinodynamic.hkgta.scheduler.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.entity.ical.PublicHoliday;
import com.sinodynamic.hkgta.service.ical.GtaPublicHolidayService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Component
public class GtaPublicHolidayTaskService {

	private Logger					logger	= Logger.getLogger(GtaPublicHolidayTaskService.class);

	@Resource(name = "appProperties")
	private Properties				appProps;

	@Autowired
	private GtaPublicHolidayService	gtaPublicHolidayService;
	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Transactional
	public boolean execute() {
		try {
			String resourceURL = appProps.getProperty(Constant.HK_PUBLIC_HOLODAYS_URL);
			// load the HK holiday calender into memory from HK URL
			String calanderStr = loadHolidayCalender(resourceURL);
			// parse the calender string
			List<PublicHoliday> holidays = new HKHolidayCalanderParser().parse(calanderStr);
			logger.info("publicHoliday file path:"+resourceURL+" file content:"+holidays.toString());
			// save holidays into database
			gtaPublicHolidayService.saveHoliday(holidays);

			return true;

		} catch (Exception e) {
			logger.error("GtaPublicHolidayTaskService process error:" + e.getMessage(), e);
			// e.printStackTrace();
			alarmEmailService.sendAlarmEmailTask("GtaPublicHolidayTask.execute()", e);
			return false;
		}
	}

	public boolean reTry() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		if (execute()) {
			return true;
		} else {
			gtaPublicHolidayService.sendPublicHolidayEmail();
			return false;
		}
	}

	private String loadHolidayCalender(String resourceURL) {

		try {
			StringBuffer calanderBuff = new StringBuffer();
			URL fileURL = new URL(resourceURL);
			HttpURLConnection conn = (HttpURLConnection) fileURL.openConnection();
			conn.connect();
			BufferedReader bfReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null;
			while ((line = bfReader.readLine()) != null) {
				calanderBuff.append(line);
			}

			return calanderBuff.toString();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new GTACommonException(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		}
	}

	public static interface CalanderParser {
		public List<PublicHoliday> parse(String calenderStr);
	}

	private static class HKHolidayCalanderParser implements CalanderParser {

		private Logger		logger				= Logger.getLogger(HKHolidayCalanderParser.class);

		//SAMHUI 20160504 changed: Please follow my ERD, holiday types are GNR, STY
		//static final String	HOLIDAY_TYPE		= "HK_GOV";
		static final String	GENERAL_HOLIDAY		= "GNR";
		static final String	STATUTORY_HOLIDAY   = "STY";

		static final String	CREATEBY			= "[System]";

		static final String	linesMatchExp		= "BEGIN:VEVENT([\\s\\S]*?)END:VEVENT";

		static final String	dateMatchExp		= "DTSTART;VALUE=DATE:([\\S]+?)DTEND;";

		static final String	descriptionMatchExp	= "SUMMARY:(.+?)END:VEVENT";

		public List<PublicHoliday> parse(String calenderStr) {

			List<PublicHoliday> holiday = new ArrayList<PublicHoliday>();
			Pattern pattern = Pattern.compile(linesMatchExp, Pattern.MULTILINE);
			Matcher matcher = pattern.matcher(calenderStr);
			while (matcher.find()) {
				String line = matcher.group();
				PublicHoliday publicHoliday = convertToEntity(line);
				holiday.add(publicHoliday);
			}
			return holiday;
		}

		private PublicHoliday convertToEntity(String holidayStr) {

			String date = null;
			String descriptionEn = null;
			Date today = new Date();
			Matcher dateMatcher = Pattern.compile(dateMatchExp, Pattern.MULTILINE).matcher(holidayStr);
			if (dateMatcher.find()) {
				date = dateMatcher.group(1);
			}

			Matcher descriptionMatcher = Pattern.compile(descriptionMatchExp, Pattern.MULTILINE).matcher(holidayStr);
			if (descriptionMatcher.find()) {
				// SAMHUI 20160504 change: the ics file may contain non ascii characters
				//descriptionEn = descriptionMatcher.group(1).getBytes(charset) ;
				try {
					descriptionEn = new String(descriptionMatcher.group(1).getBytes("UTF-8"));
				} catch (UnsupportedEncodingException e) {					
					e.printStackTrace();
				}
			}
			PublicHoliday publicHoliday = new PublicHoliday();
			publicHoliday.setDescriptionEn(descriptionEn);
			publicHoliday.setHolidayDate(DateConvertUtil.parseString2Date(date, "yyyyMMdd"));
			publicHoliday.setHolidayType(GENERAL_HOLIDAY);
			publicHoliday.setCreateDate(today);
			publicHoliday.setCreateBy(CREATEBY);
			publicHoliday.setUpdateBy(CREATEBY);
			publicHoliday.setUpdateDate(today);
			publicHoliday.setVerNo("0");
			logger.debug("Date:" + date + " descriptionEn:" + descriptionEn);
			return publicHoliday;
		}
	}
}
