package com.sinodynamic.hkgta.scheduler.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.qst.SurveyMemberService;

@Component
public class AutoCountSurveyTimesTaskService {
	private final Logger	logger	= Logger.getLogger("schedulerLog");
	@Autowired
	private SurveyMemberService surveyMemberService;
	
	@Autowired
	private MemberService memberService;
	
	public void excute(){
		//yyyy-MM-dd 13:01 /23:01
	/*	Integer hour=Integer.valueOf(DateConvertUtil.formatCurrentDate(new Date(), "HH"));
		String startTime=null;
		String endTime=null;
		if(hour<14){
			startTime=DateConvertUtil.formatCurrentDate(new Date(), "yyyy-MM-dd 00:00:00");
			endTime=DateConvertUtil.formatCurrentDate(new Date(), "yyyy-MM-dd HH:mm:00");
		}else{
			startTime=DateConvertUtil.formatCurrentDate(new Date(), "yyyy-MM-dd 13:02:00");
			endTime=DateConvertUtil.formatCurrentDate(new Date(), "yyyy-MM-dd HH:59:59");
		}
		surveyMemberService.autoCountSurveyMember();
		*/
		try {
			List<Member>lists=memberService.getAllMembers();
			for (Member member : lists)
			{
				//every customerId job 
				surveyMemberService.autoCountSurveyMemberByCustomerId(member.getCustomerId());
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		
	}
}
