package com.sinodynamic.hkgta.scheduler.job;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoImportBiOasisFlexFileTaskService;
import com.sinodynamic.hkgta.util.constant.LoggerType;

public class AutoImportBiOasisFlexFileTask extends QuartzJobBean implements ApplicationContextAware {
	
	private final Logger	logger	= LoggerFactory.getLogger(LoggerType.OASISFLEXFILE.getName());
	
	WebApplicationContext	applicationContext;

	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("job auto import oasis flex file start.........");
		AutoImportBiOasisFlexFileTaskService task = applicationContext.getBean(AutoImportBiOasisFlexFileTaskService.class);
		task.excute(new Date());
		logger.info("job auto import oasis flex file end.........");
		
	}

}
