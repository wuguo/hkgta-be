package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.DownloadCarParkMemberTaskService;

/**
 * <p>
 * Download valid member list from datatbase and save it as csv file for third party car park system to verify
 * </p>
 * 
 * @author Shuyuan Zhu
 *
 */
public class DownloadCarParkMemberTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger	logger	= LoggerFactory.getLogger(DownloadCarParkMemberTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		logger.info("DownloadCarParkMemberTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		DownloadCarParkMemberTaskService downloadCarParkMemberTaskService = applicationContext.getBean(DownloadCarParkMemberTaskService.class);

		downloadCarParkMemberTaskService.DownloadCarParkMemberList();

		logger.info("DownloadCarParkMemberTaskService.DownloadCarParkMemberList auto execute end ...");

	}
}
