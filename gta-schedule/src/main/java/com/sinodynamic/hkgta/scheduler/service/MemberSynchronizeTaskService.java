package com.sinodynamic.hkgta.scheduler.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.mms.GuestRequestDto;
import com.sinodynamic.hkgta.service.mms.MemberSynchronizeService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.constant.LoggerType;

@Component
public class MemberSynchronizeTaskService {

	private Logger logger	= Logger.getLogger(MemberSynchronizeTaskService.class);
	private Logger mmsLog = Logger.getLogger(LoggerType.MMS.getName());

	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Autowired
	private MemberSynchronizeService	memberSynchronizeService;

	public void memberSynchronize() {
		try {
//			memberSynchronizeService.synchronizeGtaMember2Mms(null);
			List<GuestRequestDto>list=memberSynchronizeService.getMemberInfoList2Synchronize(null);
			if(null!=list&&list.size()>0)
			{
				for (GuestRequestDto guestRequestDto : list) {
					try{
						memberSynchronizeService.excuteSynchGtaMember2Mms(guestRequestDto);	
					}catch(Exception e){
						mmsLog.error(e.getMessage(), e);
						e.printStackTrace();
					}
				}
			}else{
				mmsLog.info("synchronize GtaMember to Mms  member size is 0");
			}
			
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("MemberSynchronizeTask", e);
		}
	}
	
	public void memberSynchronize(String academyNos) {
		try {

			memberSynchronizeService.synchronizeGtaMember2Mms(academyNos);

		} catch (Exception e) {
			// e.printStackTrace();
			mmsLog.error(e.getMessage(), e);
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("MemberSynchronizeTask", e);
		}
	}
}
