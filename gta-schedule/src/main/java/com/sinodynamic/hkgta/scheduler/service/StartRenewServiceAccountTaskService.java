package com.sinodynamic.hkgta.scheduler.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class StartRenewServiceAccountTaskService {
	private Logger						logger	= Logger.getLogger(StartRenewServiceAccountTaskService.class);
	@Autowired
	private CustomerServiceAccService	customerServiceAccService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void startRenewServiceAccount() {
		try {
			customerServiceAccService.startRenewServiceAccount();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("StartRenewServiceAccountTask", e);
		}
	}

}
