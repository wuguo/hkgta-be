package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.SpaAppointmentsSynchronizeTaskService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.constant.LoggerType;

/**
 * <p>
 * Synchronize all the mms appointments to hkgta from current date to 14 days later.
 * </p>
 * 
 * @author Mianping Wu
 *
 */
public class SpaAppointmentsSynchronizeTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger	logger	= Logger.getLogger(SpaAppointmentsSynchronizeTask.class);
	private Logger			mmsLog	= Logger.getLogger(LoggerType.MMS.getName());

	WebApplicationContext	applicationContext;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("SpaAppointmentsSynchronizeTaskrun at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		mmsLog.info("SpaAppointmentsSynchronizeTaskrun at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		try {
//			SynchronizeSpaData synchronizeSpaData = applicationContext.getBean(SynchronizeSpaData.class);
//			synchronizeSpaData.synchronizeAppointments();
			SpaAppointmentsSynchronizeTaskService task=applicationContext.getBean(SpaAppointmentsSynchronizeTaskService.class);
			task.excuteSynchAppointments();
			logger.info("SpaAppointmentsSynchronizeTask execute end ...");
			mmsLog.info("SpaAppointmentsSynchronizeTask execute end ...");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			mmsLog.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("SpaAppointmentsSynchronizeTask", e);
		}

	}
}
