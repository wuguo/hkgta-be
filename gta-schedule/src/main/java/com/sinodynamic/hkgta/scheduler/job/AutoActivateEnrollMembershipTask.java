package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoActivateEnrollMembershipTaskService;

/**
 * <p>
 * Auto activate enrolled members whose scheduled avtivation date is arriving
 * </p>
 * 
 * @author Liky Pan
 *
 */
public class AutoActivateEnrollMembershipTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger	logger	= LoggerFactory.getLogger(AutoActivateEnrollMembershipTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoActivateEnrollMembershipTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		AutoActivateEnrollMembershipTaskService autoActivateEnrollMembershipTaskService = applicationContext
				.getBean(AutoActivateEnrollMembershipTaskService.class);

		autoActivateEnrollMembershipTaskService.AutoActivateEnrollMembership();

		logger.info("AutoActivateEnrollMembershipTaskService.AutoActivateEnrollMembership auto execute end ...");
	}
}
