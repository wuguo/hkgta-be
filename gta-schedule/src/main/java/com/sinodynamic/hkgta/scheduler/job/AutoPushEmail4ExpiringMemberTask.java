package com.sinodynamic.hkgta.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class AutoPushEmail4ExpiringMemberTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(AutoPushEmail4ExpiringMemberTask.class);

	WebApplicationContext	applicationContext;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoPushEmail4ExpiringMemberTask.pushEmail4ExpiringMember invocation start ...");

		try {
			MemberService memberService = applicationContext.getBean(MemberService.class);
			memberService.pushEmail4ExpiringMember();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoPushEmail4ExpiringMemberTask", e);
		}
		logger.info("AutoPushEmail4ExpiringMemberTask.pushEmail4ExpiringMember invocation end ...");
	}
}
