package com.sinodynamic.hkgta.scheduler.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.pms.UpdateRoomStatusDto;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepStatusLog;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.service.pms.RoomHousekeepTaskService;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskStatus;
import com.sinodynamic.hkgta.util.constant.RoomServiceStatus;
import com.sinodynamic.hkgta.util.constant.RoomStatus;
import com.sinodynamic.hkgta.util.response.MessageResult;

import net.sf.json.JSONObject;

@Component
public class AutoUpdateRoomStatusTaskService {
	private Logger							logger	= Logger.getLogger(AutoUpdateRoomStatusTaskService.class);
	@Autowired
	private RoomService						roomService;

	@Autowired
	private RoomHousekeepTaskService		roomHousekeepTaskService;
	

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void autoUpdateRoomStatus() {
		try {
			List<RoomHousekeepTask> cancelTasks = roomHousekeepTaskService.getRoomHousekeepTaskUnCmpList(RoomHousekeepTaskJobType.RUT.name());
			logger.info("autoUpdateRoomStatus  cancelRoomHouseKeep task size:"+(null!=cancelTasks?cancelTasks.size():0));
			for (RoomHousekeepTask task : cancelTasks) {
				String oldStatus = task.getStatus();
				if (!RoomHousekeepTaskStatus.CAN.name().equals(oldStatus)) {
					logger.info("cancel RoomHousekeepTask taskId:"+task.getTaskId());
					task.setStatus(RoomHousekeepTaskStatus.CAN.name());
					task.setUpdateDate(new Date());
					roomHousekeepTaskService.update(task);
				}
			}

			//List<Room> rooms = roomService.getRoomList();
			//Routine task shouldn't be created if the HK status is Vacant Clean (VC)
			List<Room> rooms = roomService.getRoomListOutOfStatus(new String[]{RoomStatus.VC.name()});
			logger.info("Room status is VC(VACANT_CLEAN) size :"+(null==rooms?0:rooms.size()));
			if (null != rooms && rooms.size() > 0) {
				for (Room room : rooms) {
					UpdateRoomStatusDto statusDto = new UpdateRoomStatusDto();
					statusDto.setRoomNo(room.getRoomNo());
					statusDto.setStatus(RoomStatus.D.toString());
					//Call API of OASIS to update room status
					logger.info("Call API of OASIS to update room status--- roomNo:"+statusDto.getRoomNo()+",status:"+statusDto.getStatus());
					MessageResult messageResult = roomService.updateRoomStatus(statusDto);
					if (null != messageResult && messageResult.isSuccess()) {
						room.setStatus(RoomStatus.D.toString());
						room.setUpdateDate(new Date());
						roomService.updateRoom(room);
						logger.info("Call API of OASIS to update room status is success.................");
					}
					/*
					RoomHousekeepTask roomHousekeepTask = new RoomHousekeepTask();
					roomHousekeepTask.setCreateDate(new Timestamp(new Date().getTime()));
					roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.OPN.toString());
					roomHousekeepTask.setJobType(RoomHousekeepTaskJobType.RUT.toString());
					roomHousekeepTask.setScheduleDatetime(new Date());
					roomHousekeepTask.setRoom(room);
					roomHousekeepTask.setStatusDate(new Date());
					roomHousekeepTaskService.saveRoomHousekeepTask(roomHousekeepTask);
					logger.info("save new roomHousekeepTask  params :"+JSONObject.fromObject(roomHousekeepTask));
					*/
				}
				addRoomHousekeepTask(rooms);
			}
			
			updateRoomServiceStatus();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoUpdateRoomStatusTask", e);
			return;
		}
	}
	private void addRoomHousekeepTask(List<Room>rooms)throws Exception
	{
		for (Room room : rooms) {
			RoomHousekeepTask roomHousekeepTask = new RoomHousekeepTask();
			roomHousekeepTask.setCreateDate(new Timestamp(new Date().getTime()));
			roomHousekeepTask.setStatus(RoomHousekeepTaskStatus.OPN.toString());
			roomHousekeepTask.setJobType(RoomHousekeepTaskJobType.RUT.toString());
			roomHousekeepTask.setScheduleDatetime(new Date());
			roomHousekeepTask.setRoom(room);
			roomHousekeepTask.setStatusDate(new Date());
			roomHousekeepTaskService.saveRoomHousekeepTask(roomHousekeepTask);
			logger.info("save new roomHousekeepTask  params :"+JSONObject.fromObject(roomHousekeepTask));
		}
	}

	/***
	 * 2016-03-17 15:30:00
	 * @author christ
	 * @desc the housekeeping status will remain no change. OOS will only able to last for 1 day, it will automatically release after day end 
	 * 
	 */
	private void updateRoomServiceStatus(){
		//get room service status is OOS list
		List<Room> roomList=roomService.getRoomListServiceStatus(RoomServiceStatus.OOS.name());
		logger.info("Room status is OOS("+RoomServiceStatus.OOS.getDesc()+")  size :"+(null==roomList?0:roomList.size()));
		if(null!=roomList){
			for (Room room : roomList) {
				if(null!=room.getServiceStatus()&&RoomServiceStatus.OOS.name().equals(room.getServiceStatus())){
					logger.info("update room status OOS to null..........room id: "+room.getRoomId()+",roomNo:"+room.getRoomNo());
					room.setServiceStatus(null);
					room.setUpdateDate(new Date());
					roomService.updateRoom(room);
				}
			}
		}
	}
	private RoomHousekeepStatusLog buildRoomStatusLog(Long taskId, String statusFrom, String statusTo, String changeBy) {
		RoomHousekeepStatusLog log = new RoomHousekeepStatusLog();
		log.setTaskId(taskId);
		log.setStatusFrom(statusFrom);
		log.setStatusTo(statusTo);
		log.setStatusChgBy(changeBy);
		log.setStatusDate(new Date());
		return log;
	}

	public static long getDiffMinutes(Date startDate, Date endDate) {
		long time1 = startDate.getTime();
		long time2 = endDate.getTime();
		long diff;
		if (time1 < time2) {
			diff = time2 - time1;
		} else {
			diff = time1 - time2;
		}
		long day = diff / (24 * 60 * 60 * 1000);
		long hour = (diff / (60 * 60 * 1000) - day * 24);
		long min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
		long sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		return day * 24 * 60 + hour * 60 + min + sec / 60;
	}
}
