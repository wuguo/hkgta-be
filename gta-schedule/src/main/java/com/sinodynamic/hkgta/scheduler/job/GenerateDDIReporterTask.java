package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.service.crm.account.DDITaskService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.constant.LoggerType;

/**
 * <p>
 * Generate the DDI transaction report(Every month run one time on the first day of month)
 * </p>
 * 
 * @author Nick_Xiong
 *
 */
public class GenerateDDIReporterTask extends QuartzJobBean implements ApplicationContextAware {
	private Logger			ddxLogger	= LoggerFactory.getLogger(LoggerType.DDX.getName());
	WebApplicationContext	applicationContext;
	@Autowired
	private AlarmEmailService alarmEmailService;
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		ddxLogger.info("GenerateDDIReporterTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		DDITaskService ddiTaskService = applicationContext.getBean(DDITaskService.class);

		try {
			ddiTaskService.generateDDIReport(new Date());
		} catch (Exception e) {
			alarmEmailService.sendAlarmEmailTask("Generate DDI File task had happend exception", e);
		}
		ddxLogger.info("GenerateDDIReporterTask.generateDDIReport auto execute end ...");
	}

}
