package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.fms.TrainerAppService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class AutoRemindMemberAndCaochInAdvance1HourTaskService {
	private Logger				logger	= Logger.getLogger(AutoRemindMemberAndCaochInAdvance1HourTaskService.class);

	@Autowired
	private TrainerAppService	trainerAppService;

	// @Scheduled(cron="0 0 0 * * *")
	// public void autoPushMsgInAdvance2Day() {
	//
	// logger.info("TrainerAppService.auto push message invocation start ...");
	// try {
	//
	// trainerAppService.autoPushMsgInAdvance2Day();
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// logger.info(e.toString());
	// }
	//
	// logger.info("TrainerAppService.auto push message invocation end ...");
	// }

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void autoRemindInAdvance1Hour() {
		try {
			trainerAppService.autoRemindInAdvance1Hour();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoRemindMemberAndCaochInAdvance1HourTask", e);
		}
	}

}
