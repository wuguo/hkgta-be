package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.pms.RoomStatusScheduleService;

@Component
public class AutoReleaseRoomStatusService {

	private Logger logger = Logger.getLogger(AutoReleaseRoomStatusService.class);

	@Autowired
	private RoomStatusScheduleService roomStatusScheduleService;

	public void autoReleaseRoomStatusTask() {
		try {
			roomStatusScheduleService.autoUpdateRoomStatus(new Date());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void autoSettingRoomStatusTask() {
		try {
			roomStatusScheduleService.autoSettingRoomStatus(new Date());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
