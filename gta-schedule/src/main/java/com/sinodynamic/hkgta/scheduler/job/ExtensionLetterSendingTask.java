package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.ExtensionLetterSendingTaskService;

/**
 * <p>
 * Auto notify hkgta member to renewal their service for the service account well be expired in 30 days
 * </p>
 * 
 * @author Mianping Wu
 *
 */
public class ExtensionLetterSendingTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(ExtensionLetterSendingTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("ExtensionLetterSendingTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		ExtensionLetterSendingTaskService extensionLetterSendingTaskService = applicationContext.getBean(ExtensionLetterSendingTaskService.class);

		extensionLetterSendingTaskService.autoSendingJob();

		logger.info("ExtensionLetterSendingController.autoSendingJob auto execute end ...");
	}

}
