package com.sinodynamic.hkgta.scheduler.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.Notice;
import com.sinodynamic.hkgta.service.crm.pub.NoticeService;

@Component
public class AutoPushNoticeToPatronTaskService {

	@Autowired
	private NoticeService noticeService;
	/***
	 * current
	 * @param time
	 */
	public void pushNotice(Date time)
	{
		 List<Notice> notices=noticeService.getPushNoticeSubjectByDate(time);
		 if(null!=notices&&notices.size()>0)
		 {
			 //pushnotice
			 for (Notice notice : notices) {
				 noticeService.pushNotice(notice,"system");
			}
		 }
		
	}
}
