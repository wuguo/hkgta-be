package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.CourseAbsentTaskService;

/**
 * 
 * @author Kaster 20160304
 * 将会员点了attending但是没有Roll Call的过期记录的状态改为ABS
 *
 */
public class CourseAbsentTask extends QuartzJobBean implements ApplicationContextAware {
	
	private final Logger logger = LoggerFactory.getLogger(CourseAbsentTask.class);
	
	WebApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("CourseAbsentTask at:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		CourseAbsentTaskService courseAbsentTaskService = applicationContext.getBean(CourseAbsentTaskService.class);
		courseAbsentTaskService.autoChangeCourseAttendanceToAbsent();
		logger.info("CourseAbsentTask executed end!");
	}

}
