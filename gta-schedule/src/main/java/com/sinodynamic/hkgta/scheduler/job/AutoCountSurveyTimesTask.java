package com.sinodynamic.hkgta.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoCountSurveyTimesTaskService;

public class AutoCountSurveyTimesTask extends QuartzJobBean implements ApplicationContextAware 
{
	private final Logger	logger	= LoggerFactory.getLogger(AutoCountSurveyTimesTask.class);
	
	WebApplicationContext	applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		// TODO Auto-generated method stub
		this.applicationContext=(WebApplicationContext)applicationContext;
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		logger.info("auto count survey times task start..........");
		AutoCountSurveyTimesTaskService task = applicationContext.getBean(AutoCountSurveyTimesTaskService.class);
		task.excute();
		logger.info("auto count survey times task end..........");

	}

}
