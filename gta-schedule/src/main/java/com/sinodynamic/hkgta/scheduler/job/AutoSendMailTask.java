package com.sinodynamic.hkgta.scheduler.job;


import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoSendMailTaskService;

public class AutoSendMailTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger logger = LoggerFactory.getLogger(AutoSendMailTask.class);

	WebApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		// TODO Auto-generated method stub
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("auto send mail start..................");
		AutoSendMailTaskService autoSendMailTaskService = applicationContext.getBean(AutoSendMailTaskService.class);
		autoSendMailTaskService.excuteSend();
		logger.debug("auto send mail end..................");
	}

}
