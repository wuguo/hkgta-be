package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.adm.StaffMasterService;

@Component
public class StaffMasterTaskService {
	private final Logger	logger	= Logger.getLogger(StaffMasterTaskService.class);

	@Autowired
	StaffMasterService		staffMasterService;

	public void autoUNACTQuitStaff() {
		try {
			staffMasterService.autoQuitExpireStaff();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

}
