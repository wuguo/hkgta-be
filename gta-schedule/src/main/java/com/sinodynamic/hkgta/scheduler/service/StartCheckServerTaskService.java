package com.sinodynamic.hkgta.scheduler.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class StartCheckServerTaskService {
	private Logger				logger	= Logger.getLogger(StartCheckServerTaskService.class);
	@Autowired
	private CheckServerService	checkServerService;
	@Autowired
	private AlarmEmailService	alarmEmailService;

	public void check() {
		try {
			checkServerService.check();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("StartCheckServerTask", e);
		}
	}

}
