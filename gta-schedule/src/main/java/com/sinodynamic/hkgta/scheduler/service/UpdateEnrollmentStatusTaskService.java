package com.sinodynamic.hkgta.scheduler.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Component
public class UpdateEnrollmentStatusTaskService {
	private Logger logger = Logger.getLogger(UpdateEnrollmentStatusTaskService.class);

	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;

	@Autowired
	private PermitCardMasterService permitCardMasterService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	
	/**
	 * using for job execute
	 */
	@SuppressWarnings("unchecked")
	public void updateEnrollmentStatusJob(){
		ListPage<CustomerEnrollment> page = new ListPage<CustomerEnrollment>();
		try {
			customerEnrollmentService.searchAllCustomerEnrollments(page);
			List<CustomerEnrollment> enrollList = (List<CustomerEnrollment>) page.getList();
			if (enrollList.size() > 0) {
				for (CustomerEnrollment ce : enrollList) {
					String cardFlag = permitCardMasterService.checkPermitCardStatus(ce.getEnrollId());
					if (!EnrollStatus.CMP.name().equals(ce.getStatus()) && ("0").equals(cardFlag)) {
						if (customerEnrollmentService.checkBalanceDueAsZero(ce.getEnrollId())) {
							logger.info("update  CustomerEnrollment enroollId:"+ce.getEnrollId()+" status:"+EnrollStatus.CMP.name());
							customerEnrollmentService.updateEnrollmentStatusJob(ce.getEnrollId(),
									EnrollStatus.CMP.name());
						}
					}
				}
			} 
		} catch (Exception e) {
			alarmEmailService.sendAlarmEmailTask("UpdateEnrollmentStatusTask", e);
			logger.error("updateEnrollmentStatusJob exception"+e.getMessage());
		}
	} 
}
