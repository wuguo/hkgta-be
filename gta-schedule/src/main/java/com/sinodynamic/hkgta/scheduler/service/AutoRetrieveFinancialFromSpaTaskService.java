package com.sinodynamic.hkgta.scheduler.service;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.entity.mms.SpaFlexDailySummary;
import com.sinodynamic.hkgta.entity.onlinepayment.ItemPaymentAccCode;
import com.sinodynamic.hkgta.integration.spa.request.GetPaymentsRequest;
import com.sinodynamic.hkgta.integration.spa.response.GetPaymentsResponse;
import com.sinodynamic.hkgta.integration.spa.response.Payment;
import com.sinodynamic.hkgta.integration.spa.response.SpaAccountFlex;
import com.sinodynamic.hkgta.integration.spa.response.SpaAccountFlexReponse;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.service.mms.SpaFlexDailySummaryService;
import com.sinodynamic.hkgta.service.onlinepayment.ItemPaymentAccCodeService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.SalesInformationType;
import com.sinodynamic.hkgta.util.report.JasperUtil;

@Component
public class AutoRetrieveFinancialFromSpaTaskService {

	private Logger logger = Logger.getLogger(AutoRetrieveFinancialFromSpaTaskService.class);

	@Resource(name = "appProperties")
	Properties appProps;

	@Autowired
	private SpaServcieManager spaServcieManager;

	@Autowired
	private AlarmEmailService alarmEmailService;

	@Autowired
	private SpaFlexDailySummaryService spaFlexDailySummaryService;

	@Autowired
	private ItemPaymentAccCodeService itemPaymentAccCodeService;

	public void excute(String fromDate, String toDate) {
		logger.info("get  getAccounting_DaySummary from  Spa ... fromDate=" + fromDate + ",toDate=" + toDate);
		try {
			// clear fromDate toDate
			spaFlexDailySummaryService.deleteByTransactionDate(fromDate);

			logger.info("clear exsit talbe spa_flex_daily_summary old data : date=" + fromDate);

			this.excuteAccounting_DaySummary(fromDate, toDate);

			this.excutePaymentCustom(fromDate, toDate);
			/***
			 * generate csv
			 */
			generateCSV(DateConvertUtil.parseString2Date(fromDate, "yyyy-MM-dd"));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoRetrieveFinancialFromSpaTaskService", e);
		}
	}

	private void excuteAccounting_DaySummary(String fromDate, String toDate) {
		logger.info("excute get Accounting_DaySummary ");
		try {
			SpaAccountFlexReponse response = spaServcieManager.getSpaAccountFlexList(fromDate, toDate);
			logger.info("response:" + response.toString());
			if (null != response && response.getSuccess()) {
				List<SpaAccountFlex> datas = this.filterPayments(response.getAccountFlexs());
				logger.info("data size:" + (null != datas && datas.size() > 0 ? datas.size() : 0));
				if (null != datas && datas.size() > 0) {
					// save new data
					logger.info("save data  start....");
					for (SpaAccountFlex data : datas) {
						try {
							this.saveData(data, fromDate);
						} catch (Exception e) {
							logger.error("save data happend Exception:" + e);
						}
					}
					logger.info("save data end....");
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoRetrieveFinancialFromSpaTaskService-excute Accounting_DaySummary",
					e);
		}
		logger.info("excute get Accounting_DaySummary end");

	}

	private void excutePaymentCustom(String fromDate, String toDate) {
		logger.info("excute Payment Custom  start..");
		try {
			GetPaymentsRequest request = new GetPaymentsRequest();
			request.setFromDate(DateConvertUtil.parseString2Date(fromDate, "yyyy-MM-dd"));
			request.setToDate(DateConvertUtil.parseString2Date(toDate, "yyyy-MM-dd"));
			GetPaymentsResponse response = spaServcieManager.getPayments(request);
			logger.info("response:" + response.toString());
			if (response == null || response.getPayments() == null || response.getPayments().size() == 0)
				return;
			List<SpaAccountFlex> datas = new ArrayList<>();
			this.filterCustomData(datas, response.getPayments());
			logger.info("custom Payment size:" + datas.size());

			logger.info("save custom Payment  data  start....");
			for (SpaAccountFlex data : datas) {
				try {
					this.saveData(data, fromDate);
				} catch (Exception e) {
					logger.error("save custom Payment data happend Exception:" + e);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoRetrieveFinancialFromSpaTaskService-excute payment custom", e);
		}
		logger.info("save custom Payment data end....");
	}
	
	private void filterCustomData(List<SpaAccountFlex> datas, List<Payment> payments) {
		for (Payment payment : payments) {
			if (CustomPaymentType.CUSTOM.getDesc().equalsIgnoreCase(payment.getPaymentType())) {
				if (CustomPaymentType.SCV.getDesc().equalsIgnoreCase(payment.getCustomName())) {
					datas.add(init(payment, CustomPaymentType.CUSTOM.desc, CustomPaymentType.SCV.getCode(),CustomPaymentType.CUSTOM.desc + "-" + CustomPaymentType.SCV.getDesc()));
				} else if (CustomPaymentType.SRC.getDesc().equalsIgnoreCase(payment.getCustomName())) {
					datas.add(init(payment, CustomPaymentType.CUSTOM.desc, CustomPaymentType.SRC.getCode(),CustomPaymentType.CUSTOM.desc + "-" + CustomPaymentType.SRC.getDesc()));
				} else if (CustomPaymentType.CASHVALUE1.getDesc().equalsIgnoreCase(payment.getCustomName())) {
					datas.add(init(payment, CustomPaymentType.CUSTOM.desc, PaymentMethod.CASHVALUE.name(),CustomPaymentType.CUSTOM.desc + "-" + CustomPaymentType.CASHVALUE1.desc));
				}else if (CustomPaymentType.CASHVALUE.getDesc().equalsIgnoreCase(payment.getCustomName())) {
					datas.add(init(payment, CustomPaymentType.CUSTOM.desc, PaymentMethod.CASHVALUE.name(),CustomPaymentType.CUSTOM.desc + "-" + CustomPaymentType.CASHVALUE.desc));
				}
			}
		}
	}

	private SpaAccountFlex init(Payment payment, String name, String accountCode, String desc) {
		SpaAccountFlex flex = new SpaAccountFlex();
		flex.setAccountCategory("Payments");
		flex.setAccountName(CustomPaymentType.CUSTOM.desc);
		flex.setDescription(desc);
		flex.setName(name);
		flex.setAccountCode(accountCode);
		flex.setCredit(new BigDecimal(0));
		flex.setDebit(new BigDecimal(payment.getPaymentAmount()));
		return flex;
	}

	private void generateCSV(Date transDate) {
		String csvPath = appProps.getProperty("flex.account.report");
		String tempPath = appProps.getProperty("flex.account.report.tmp");
		File tempFile = new File(tempPath);
		if (!tempFile.exists()) {
			tempFile.mkdirs();
		}
		// MMS{yymmddHHmmss}.csv
		String tempFilePath = tempPath + "/MMS" + DateConvertUtil.parseDate2String(transDate, "yyMMddhhmmss") + ".csv";
		File file = new File(tempFilePath);
		JasperUtil.writerContent(file, convertCSVContent(transDate).toString());
		tempFile = new File(tempFilePath);
		logger.info("spaFlexDailySummary filePath:" + csvPath + "/" + tempFile.getName());
		this.copyTempFileToCsvPath(tempFile, csvPath);

	}

	private StringBuilder convertCSVContent(Date dateTime) {
		StringBuilder sber = new StringBuilder();
		char r = '\r';
		List<PaymentFacilityDto> dtoList = spaFlexDailySummaryService
				.getSpaFlexDailySummaryByDate(DateConvertUtil.parseDate2String(dateTime, "yyyy-MM-dd"));
		// key:accountCode value=account
		Map<String, BigDecimal> mapItemCategory = this.mapItemCategory(dtoList);

		String row = "\"Date:\"" + "," + "\"" + DateConvertUtil.parseDate2String(dateTime, "MM/dd/yyyy") + "\"" + ","
				+ r;
		sber.append(row);
		row = "\"TRN_CODE\"" + "," + "\"Description\"" + "," + "\"G/L\"" + r;
		sber.append(row);
		// get status is act itemPaymentAccCode
		List<ItemPaymentAccCode> items = itemPaymentAccCodeService.getItemPaymentAccCodeByStatus("ACT", "SPA");
		
		/***
		 * SGG-3638
		 * Currently, the MMS Flex is CR. amount, please change it to DR. amount. (Remove "-")
		 */
		for (ItemPaymentAccCode item : items) {
			String key = item.getPaymentMethodCode();
			BigDecimal account = mapItemCategory.get(key);
			row = "\"" + item.getAccountCode() + "\"" + "," + "\""
					+ (item.getDescription() == null ? "" : item.getDescription()) + "\"" + "," + "\""
					+ (null != account ? changeAccountDisplay(account) : "0") + "\"" + r;
			sber.append(row);
		}
		return sber;
	}
	/***
	 * SGG-3638
	 * Currently, the MMS Flex is CR. amount, please change it to DR. amount. (Remove "-")
	 * @param account
	 * @return
	 */
	private String  changeAccountDisplay(BigDecimal account){
		if(account.compareTo(new BigDecimal(0))<0){
			return (account.multiply(new BigDecimal(-1))).toString();
		}else{
			return account.toString();
		}
		
	}

	private Map<String, BigDecimal> mapItemCategory(List<PaymentFacilityDto> dtoList) {
		Map<String, BigDecimal> map = new HashMap<>();
		for (PaymentFacilityDto dto : dtoList) {
			map.put(dto.getAccountCode(), dto.getAmount());
		}
		return map;

	}

	private void copyTempFileToCsvPath(File tempFile, String csvPath) {
		if (tempFile.exists()) {
			File csv = new File(csvPath);
			if (!csv.exists()) {
				csv.mkdirs();
			}
			csv = new File(csvPath + "/" + tempFile.getName());
			try {
				FileCopyUtils.copy(tempFile, csv);
				tempFile.delete();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage(), e);
			}
		}

	}

	/***
	 * Only get the json data with AccountCategory = "Payments" and AccountCode
	 * in Visa, Mastercard, JCB Card, Amex, Union Pay, Custom
	 */
	private List<SpaAccountFlex> filterPayments(List<SpaAccountFlex> datas) {
		List<SpaAccountFlex> validDatas = new ArrayList<>();
		for (SpaAccountFlex data : datas) {
			if ("Payments".equals(data.getAccountCategory())
					&& (PaymentMethod.VISA.name().equalsIgnoreCase(data.getAccountCode())
							|| PaymentMethod.MASTER.name().equalsIgnoreCase(data.getAccountCode())
							|| PaymentMethod.JCB.name().equalsIgnoreCase(data.getAccountCode())
							|| PaymentMethod.AMEX.name().equalsIgnoreCase(data.getAccountCode())
							|| PaymentMethod.CUP.name().equalsIgnoreCase(data.getAccountCode())
							|| PaymentMethod.CASH.name().equalsIgnoreCase(data.getAccountCode())
							// SGG-3262 filter Custom ,use call getPayment
							// Custom ( Cash Value Standalone Cash Value
							// Standalone Room Charge)
							// ||
							// "Custom".equalsIgnoreCase(data.getAccountCode())
							// SGG-3632
							|| "Mastercard".equalsIgnoreCase(data.getAccountCode()))
					) {
				validDatas.add(data);
			}else if("Sales".equalsIgnoreCase(data.getAccountCategory())||
					"Returns".equalsIgnoreCase(data.getAccountCategory()))
			{
				//SGG-3767
				validDatas.add(fileSalesInformation(data));
			}
		}
		return validDatas;
	}
	private SpaAccountFlex fileSalesInformation(SpaAccountFlex data)
	{
		if(SalesInformationType.BQ.getDesc().equalsIgnoreCase(data.getAccountCode()))
		{
			data.setAccountCode(SalesInformationType.BQ.name());
			data.setDescription(SalesInformationType.BQ.getDesc());
		}else if(SalesInformationType.DIG.getDesc().equalsIgnoreCase(data.getAccountCode())){
			data.setAccountCode(SalesInformationType.DIG.name());
			data.setDescription(SalesInformationType.DIG.getDesc());
		}else if(SalesInformationType.DIM.getDesc().equalsIgnoreCase(data.getAccountCode())){
			data.setAccountCode(SalesInformationType.DIM.name());
			data.setDescription(SalesInformationType.DIM.getDesc());
		}else if(SalesInformationType.SA.getDesc().equalsIgnoreCase(data.getAccountCode())){
			data.setAccountCode(SalesInformationType.SA.name());
			data.setDescription(SalesInformationType.SA.getDesc());
		}else if(SalesInformationType.TT.getDesc().equalsIgnoreCase(data.getAccountCode())){
			data.setAccountCode(SalesInformationType.TT.name());
			data.setDescription(SalesInformationType.TT.getDesc());
		}
		return data;
		
	}

	private void saveData(SpaAccountFlex data, String fromDate) {
		Date transDate = DateConvertUtil.parseString2Date(fromDate, "yyyy-MM-dd");
		Timestamp createDate = new Timestamp(new Date().getTime());
		SpaFlexDailySummary summary = new SpaFlexDailySummary();
		summary.setAccountCategory(data.getAccountCategory());
		summary.setAccountCode(data.getAccountCode().toUpperCase());
		summary.setAccountName(data.getAccountName());
		summary.setCredit(data.getCredit());
		summary.setDebit(data.getDebit());
		summary.setDescription(data.getDescription());
		summary.setName(data.getName());
		summary.setCreateDate(createDate);
		summary.setTransactionDate(transDate);
		/// service
		spaFlexDailySummaryService.save(summary);

	}

	public enum CustomPaymentType {
		SCV("SCV", "Standalone Cash Value"), SRC("SRC", "Standalone Guest Room"), CASHVALUE1("CASHVALUE",
				"CashValue1"), CASHVALUE("CASHVALUE",
						"CASHVALUE"),CUSTOM("CASHVALUE", "Custom");
		private String code;

		private String desc;

		private CustomPaymentType(String code, String desc) {
			this.code = code;
			this.desc = desc;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
	}

}
