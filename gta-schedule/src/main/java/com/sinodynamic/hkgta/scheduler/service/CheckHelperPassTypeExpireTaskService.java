package com.sinodynamic.hkgta.scheduler.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.backoffice.admin.HelperPassTypeService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class CheckHelperPassTypeExpireTaskService {

	private Logger					logger	= Logger.getLogger(CheckHelperPassTypeExpireTaskService.class);

	@Autowired
	private HelperPassTypeService	helperPassTypeService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void autoDailyRun() {
		try {

			helperPassTypeService.checkIfHelperPassTypeExpired();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("CheckHelperPassTypeExpireTask", e);
		}
	}

}
