package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.DownloadCarParkMemberTaskService;

/**
 * <p>
 * Verify the returned csv file processed by the third party car park system. If passed, no furture step will be executed. If not, then
 * DownloadCarParkMemberTask will be executed in the next scheduled time
 * </p>
 * 
 * @author Shuyuan Zhu
 *
 */
public class VerifyReturnCarParkMemberTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(VerifyReturnCarParkMemberTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		logger.info("VerifyReturnCarParkMemberTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		DownloadCarParkMemberTaskService downloadCarParkMemberTaskService = applicationContext.getBean(DownloadCarParkMemberTaskService.class);

		downloadCarParkMemberTaskService.VerifyReturnCarParkMemberList();

		logger.info("VerifyReturnCarParkMemberTaskService.VerifyReturnCarParkMemberList auto execute end ...");

	}
}
