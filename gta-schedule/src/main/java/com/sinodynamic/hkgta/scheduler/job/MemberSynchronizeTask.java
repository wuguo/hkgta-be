package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.MemberSynchronizeTaskService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.constant.LoggerType;

/**
 * <p>
 * Synchronize hkgta member info to mms such as academyNo, phoneNumber, email etc.
 * </p>
 * 
 * @author Mianping Wu
 *
 */
public class MemberSynchronizeTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= Logger.getLogger(MemberSynchronizeTask.class);
	private Logger			mmsLog	= Logger.getLogger(LoggerType.MMS.getName());

	WebApplicationContext	applicationContext;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("MemberSynchronizeTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		mmsLog.info("MemberSynchronizeTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		try{
			MemberSynchronizeTaskService memberSynchronizeTaskService = applicationContext.getBean(MemberSynchronizeTaskService.class);
			memberSynchronizeTaskService.memberSynchronize();
	
			logger.info("MemberSynchronizeTask execute end ...");
			mmsLog.info("MemberSynchronizeTask execute end ...");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			mmsLog.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("MemberSynchronizeTask", e);
		}
	}

}
