package com.sinodynamic.hkgta.scheduler.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.FacilityTimeslotDao;
import com.sinodynamic.hkgta.dto.golfmachine.BatInfo;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.integration.zoomtech.service.GolfMachineService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;

@Component
public class GolfBayCheckInTaskService {
	private Logger logger = Logger.getLogger(GolfBayCheckInTaskService.class);

    @Autowired
    private FacilityTimeslotDao facilityTimeslotDao;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	
    @Autowired
   	private GolfMachineService golfMachineService;
        
	@Value("#{appProperties['golfmachine.status_on']}") String golfBayStatusON;

	@Transactional
	public void openGolfMachine() {	
		try {
			Calendar currentDate = Calendar.getInstance();
			currentDate.setTime(new Date());
			if(currentDate.get(Calendar.MINUTE) == 59){
				// currentTime is 59 minute and turn on next hour subtract 1 seconds
				machineStatCheckInTimeNextHourPer59Minute();
			}else{
				machineStatCheckInAfterStartTime();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("GolfBayCheckInTask", e);
		}
	}
	/***
	 * turn on next hour bay when currentTime is 59/per minute
	 * @return
	 */
	private void  machineStatCheckInTimeNextHourPer59Minute()
	{
		List<FacilityTimeslot> facilityTimeslotList = facilityTimeslotDao.getFacilityTimeslotListByStatus(Constant.FACILITY_TYPE_GOLF,FacilityStatus.OP.name(), 
				nextHourStartEndTime(true)[0], nextHourStartEndTime(true)[1]);
		
		logger.info("currentTime is 59 minutes  and get next hours ,all FacilityTimeslot status is "+FacilityStatus.OP.name()+" facilityTimeslotList size : "+(null==facilityTimeslotList?0:facilityTimeslotList.size()));
		for(FacilityTimeslot facilityTimeslot:facilityTimeslotList){
			Long facilityNo=facilityTimeslot.getFacilityMaster().getFacilityNo();											
			try {				
				Calendar trunOnTime = Calendar.getInstance();
				trunOnTime.setTime(facilityTimeslot.getBeginDatetime());
				trunOnTime.add(Calendar.SECOND, -1);
				golfMachineService.setTeeupMachineStatsbyCheckinTime(facilityNo, trunOnTime.getTime(), facilityTimeslot.getEndDatetime());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	/**
	 * bay start time and end time
	 * @param nextHour
	 * @return
	 */
	private Date[] nextHourStartEndTime(boolean nextHour){
		Calendar beginDatetimeCalendar = Calendar.getInstance();
		beginDatetimeCalendar.setTime(new Date());
		beginDatetimeCalendar.set(Calendar.SECOND, 0);
		beginDatetimeCalendar.set(Calendar.MINUTE, 0);
		if(nextHour){
			beginDatetimeCalendar.add(Calendar.HOUR_OF_DAY, 1);
		}
		Calendar endDatetimeCalendar = Calendar.getInstance();
		endDatetimeCalendar.setTime(new Date());
		endDatetimeCalendar.set(Calendar.SECOND, 59);
		endDatetimeCalendar.set(Calendar.MINUTE, 59);
		if(nextHour){
			endDatetimeCalendar.add(Calendar.HOUR_OF_DAY, 1);
		}
		
		Date[] dates=new Date[]{beginDatetimeCalendar.getTime(),endDatetimeCalendar.getTime()};
		return dates;
	}
	
	/**
	 * turn on bay when current time between start time and end time
	 */
	private void machineStatCheckInAfterStartTime(){
		// get Facility Timeslot List by Status
		List<FacilityTimeslot> facilityTimeslotList = facilityTimeslotDao.getFacilityTimeslotListByStatus(Constant.FACILITY_TYPE_GOLF,FacilityStatus.OP.name(), 
				nextHourStartEndTime(false)[0], nextHourStartEndTime(false)[1]);
		logger.info("get all FacilityTimeslot status is "+FacilityStatus.OP.name()+" facilityTimeslotList size : "+(null==facilityTimeslotList?0:facilityTimeslotList.size()));
		Map<Long, BatInfo> batInfMap=null;
		if(facilityTimeslotList!=null&&facilityTimeslotList.size()>0){
			try {
			   //get Bat info from golf machine
			   batInfMap=golfMachineService.getAllBatInfosMap();
			   logger.info("get Bat info from golf machine :"+batInfMap.toString());
		    } catch (Exception e) {		    	
		    	logger.error(e.getMessage(), e);			  
		    }  	
		}
		
		for(FacilityTimeslot facilityTimeslot:facilityTimeslotList){
			Long facilityNo=facilityTimeslot.getFacilityMaster().getFacilityNo();											
			try {					
				if (batInfMap!=null && !batInfMap.isEmpty())  {
					// if batNo of golf machine is open, then continue, if not ,open it.
					if (!golfBayStatusON.contains(batInfMap.get(facilityNo).getBatStat().toString())){
						logger.info(" batInfMap facilityNo:"+facilityNo);
						golfMachineService.setTeeupMachineStatsbyCheckinTime(facilityNo, facilityTimeslot.getBeginDatetime(), facilityTimeslot.getEndDatetime());
					}						  
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
		}
	}
}
