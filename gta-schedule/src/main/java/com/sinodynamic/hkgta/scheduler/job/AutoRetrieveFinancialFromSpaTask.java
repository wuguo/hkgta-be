package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoRetrieveFinancialFromSpaTaskService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;

/**
 * <p>
 * Auto retrieve financial from spa
 * 
 * </p>
 * 
 * @author christ
 *
 */
public class AutoRetrieveFinancialFromSpaTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger logger = LoggerFactory.getLogger(AutoRetrieveFinancialFromSpaTask.class);

	WebApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	/***
	 * excute before currentDate 1 day
	 */
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoRetrieveFinancialFromSpaTask  run at:"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		
		AutoRetrieveFinancialFromSpaTaskService task = applicationContext.getBean(AutoRetrieveFinancialFromSpaTaskService.class);
		String fromDate =DateConvertUtil.parseDate2String(DateConvertUtil.parseString2Date(DateCalcUtil.getDateBefore(new Date(), 1),"yyyy-MM-dd"),"yyyy-MM-dd");
		task.excute(fromDate, fromDate);
		
		logger.info("AutoRetrieveFinancialFromSpaTask run  end ...");
	}
}
