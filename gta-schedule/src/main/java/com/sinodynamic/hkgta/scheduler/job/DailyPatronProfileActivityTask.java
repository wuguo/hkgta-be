package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.DailyPatronProfileActivityTaskService;

/**
 * <p>
 * Auto generate DailyPatronProfile report  per day 23:59 
 * send mail 
 * </p>
 * 
 * @author christ
 *
 */
public class DailyPatronProfileActivityTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger	logger	= LoggerFactory.getLogger(DailyPatronProfileActivityTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		logger.info("DailyPatronProfileActivityTaskService  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		DailyPatronProfileActivityTaskService taskService = applicationContext
				.getBean(DailyPatronProfileActivityTaskService.class);
		taskService.generateReport();

		logger.info("DailyPatronProfileActivityTaskService.generateReport auto execute end ...");
	}
}
