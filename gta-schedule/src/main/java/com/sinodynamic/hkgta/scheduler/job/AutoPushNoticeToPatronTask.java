package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoPushNoticeToPatronTaskService;

/**
 * <p>
 * System automatically send the push notification to patrons when there is new news
 * </p>
 * 
 * @author christ li
 *
 */
public class AutoPushNoticeToPatronTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger	logger	= LoggerFactory.getLogger(AutoPushNoticeToPatronTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoPushNoticeToPatronTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		AutoPushNoticeToPatronTaskService autoPushNoticeToPatronTaskService = applicationContext
				.getBean(AutoPushNoticeToPatronTaskService.class);

		autoPushNoticeToPatronTaskService.pushNotice(new Date());

		logger.info("AutoPushNoticeToPatronTask auto execute end ...");
	}
}
