package com.sinodynamic.hkgta.scheduler.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.sinodynamic.hkgta.dto.carpark.CarParkMemberCheckDto;
import com.sinodynamic.hkgta.service.carpark.CarParkMemberCheckService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.CarkParkMemberUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.CarParkCSVRowType;
import com.sinodynamic.hkgta.util.constant.CarParkUserType;

@Component
public class DownloadCarParkMemberTaskService {

	private Logger logger = Logger.getLogger(DownloadCarParkMemberTaskService.class);

	public static boolean Verification_SuccessOrNot = false;
	public static Date currentSendDateTime = null;
	public final static String Default_Error_Code = "999";

	private final static String carparkInPath = "carparkFile.in.path";
	private final static String carparkReturnPath = "carparkFile.return.path";
	private final static String carparkNECInPath = "NEC.integration.in.path";
	private final static String carparkNECReturnPath = "NEC.integration.return.path";

	@Autowired
	private CarParkMemberCheckService carParkMemberCheckService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Resource(name = "appProperties")
	private Properties	appProps;
	
	public void DownloadCarParkMemberList() {

		Calendar calendar = Calendar.getInstance();
		int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
		int currentMinute = calendar.get(calendar.MINUTE);
		if (currentHour == 1 && currentMinute == 0) {
			Verification_SuccessOrNot = false;
		}
		if (Verification_SuccessOrNot) {
			logger.debug(
					"DownloadCarParkMemberTaskService.DownloadCarParkMemberList() -- The verification has passed ...");
			return;
		}
		currentSendDateTime = new Date();
		String currentDateTimeStr = formatCurrentDate(currentSendDateTime);
		try {
			String csvFilePath = appProps.getProperty(carparkInPath) + File.separatorChar + "FULL" + currentDateTimeStr
					+ ".Send.txt";
			File file = new File(appProps.getProperty(carparkInPath) + File.separatorChar);
			if (!file.exists()) {
				file.mkdirs();
			}
			String necFilePath = appProps.getProperty(carparkNECInPath) + File.separatorChar;

			logger.info("CarPark path:" + csvFilePath);

			StringBuilder sbContent=new StringBuilder();
			char r='\r';
			char n='\n';
			String header="\"" + CarParkCSVRowType.HEADER.getRowType() + "\""+","+ "\"" + currentDateTimeStr + "\""+","+
					"\"" + "SEND" + "\"" +r+n;
			sbContent.append(header);

			List<CarParkMemberCheckDto> carParkMemberCheckDtoList = carParkMemberCheckService
					.getCarParkMemberCheckList();
			List<CarParkMemberCheckDto> carParkStaffCheckDtoList = carParkMemberCheckService.getCarParkStaffCheckList();
			List<CarParkMemberCheckDto> carParkMemberCheckDtos = new ArrayList<CarParkMemberCheckDto>();
			carParkMemberCheckDtos.addAll(carParkMemberCheckDtoList);
			carParkMemberCheckDtos.addAll(carParkStaffCheckDtoList);
			if (null != carParkMemberCheckDtos && carParkMemberCheckDtos.size() != 0) {
				for (CarParkMemberCheckDto carParkMemberCheckDto : carParkMemberCheckDtos) {
					String newCardNo = "";
					String fullCardNo = "";
					String expiryDate = "";
					if (carParkMemberCheckDto.getType().intValue() == CarParkUserType.MEMBER.getRole().intValue()) {
						newCardNo = CarkParkMemberUtil.formatNumber(8, carParkMemberCheckDto.getCardNo());
						fullCardNo = CarkParkMemberUtil.getCheckDigitByLuhnAlogrithm(newCardNo);
						expiryDate = getFullExpiryDate(carParkMemberCheckDto.getExpiryDate());
						if (expiryDate == null || expiryDate.trim().length() == 0) {
							expiryDate = "2049-12-31 23:59:59";
						}
					} else
						if (carParkMemberCheckDto.getType().intValue() == CarParkUserType.STAFF.getRole().intValue()) {
						newCardNo = CarkParkMemberUtil.formatNumber(6, carParkMemberCheckDto.getCardNo());
						fullCardNo = "S" + CarkParkMemberUtil.getCheckDigitByLuhnAlogrithm(newCardNo);
						expiryDate = "2049-12-31 23:59:59";
					}

					String  row = "\"" + CarParkCSVRowType.DETAIL.getRowType() + "\""+","+
							"\"" + String.valueOf(carParkMemberCheckDto.getAcademyNo()) + "\""+","+
							"\"" + fullCardNo + "\""+","+
							"\"" + this.reverseMifareSN(carParkMemberCheckDto.getCardSerialNo()) + "\""+","+
							"\"" + carParkMemberCheckDto.getPersonName() + "\""+","+
							"\"" + carParkMemberCheckDto.getType() + "\""+","+
							"\"" + this.handlerLicencePlateNo(carParkMemberCheckDto.getLicencePlateNo()) + "\""+","+
							"\"" + carParkMemberCheckDto.getPhoneMobile() + "\""+","+ "\"" + expiryDate + "\""+","+
							"\"" + Default_Error_Code + "\""+","+ "\"" + "" + "\""+r+n ;
					
					sbContent.append(row);
					logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, row));
				}
			}
			String footer = "\"" + CarParkCSVRowType.FOOTER.getRowType() + "\""+","+
					"\"" + String.valueOf(carParkMemberCheckDtos.size()) + "\"";
			sbContent.append(footer);
			
			/***
			 * write success ,after copy file to necFilePath
			 */
			File csvPath = new File(csvFilePath);
			this.writerContent(csvPath, sbContent.toString());
			
			if (csvPath.exists()) {
				copyFileToNEC(csvPath, necFilePath);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			alarmEmailService.sendAlarmEmailTask("DownloadCarParkMemberTask", e);
			logger.debug(
					"DownloadCarParkMemberTaskService.DownloadCarParkMemberList() Invocation --- Unexpected Error: "
							+ e.toString());
		}

	}
	public void VerifyReturnCarParkMemberList() {

		if (Verification_SuccessOrNot) {
			logger.debug(
					"DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() The verification has passed ...");
			return;
		}
		if (currentSendDateTime == null) {
			logger.debug(
					"DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() No previous send file was created ...");
			return;
		}
		String previousSendTimeStr = formatCurrentDate(currentSendDateTime);

		try {
			// returnCarPark filePath+'/'+RETURN
			File csvRoot = new File(appProps.getProperty(carparkReturnPath));
			
			String necReturnFilePath = appProps.getProperty(carparkNECReturnPath) + File.separatorChar;
			
			File[] csvFiles = csvRoot.listFiles();
			if(null==csvFiles||csvFiles.length==0){
				logger.debug(
						"DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList()  csvFiles is null ,no find any file in  ..."+appProps.getProperty(carparkReturnPath)+" path");
				return ;
			}
			for (File csvFile : csvFiles) {
				if (csvFile.getName().length() == 29
						&& csvFile.getName().substring(4, 16).equals(previousSendTimeStr.substring(0, 12))
						&& csvFile.getName().substring(19, 25).toUpperCase().equals("RETURN")) {
					logger.info("csvFile path" + csvFile.getAbsolutePath() + " file name:" + csvFile.getName());
					ArrayList<String[]> csvList = new ArrayList<String[]>();
					CsvReader csvReader = new CsvReader(csvFile.getAbsolutePath(), ',', Charset.forName("SJIS"));

					csvReader.readHeaders();

					while (csvReader.readRecord()) {
						csvList.add(csvReader.getValues());
					}
					csvReader.close();
					if (csvList.size() > 1) {
						for (int row = 0; row < csvList.size() - 1; row++) {
							if (!csvList.get(row)[9].toString().equals("0")) {
								Verification_SuccessOrNot = false;
								logger.debug(
										"DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() verification failed ...");
								return;
							}
						}
						Verification_SuccessOrNot = true;
						logger.info(
								"DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() verification passed ..."
										+ csvList.toString());

						/***
						 * copy csvFile to RETURN
						 */
						this.copyFileToNEC(csvFile, necReturnFilePath);
						// Delete the csv send file

						if (DeleteAllFullDumpSendFile(carparkReturnPath)) {
							logger.info(
									"DownloadCarParkMemberTaskService.VerifyReturnCarParkMemberList() All the created files have been deleted ...");
						}
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("VerifyReturnCarParkMemberTask", e);
		}

	}
	private void DownloadCarParkMemberList_BACK() {

		Calendar calendar = Calendar.getInstance();
		int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
		int currentMinute = calendar.get(calendar.MINUTE);
		if (currentHour == 1 && currentMinute == 0) {
			Verification_SuccessOrNot = false;
		}
		if (Verification_SuccessOrNot) {
			logger.debug(
					"DownloadCarParkMemberTaskService.DownloadCarParkMemberList() -- The verification has passed ...");
			return;
		}
		currentSendDateTime = new Date();
		String currentDateTimeStr = formatCurrentDate(currentSendDateTime);
		try {
			String csvFilePath = appProps.getProperty(carparkInPath) + File.separatorChar + "FULL" + currentDateTimeStr
					+ ".Send.txt";
			File file = new File(appProps.getProperty(carparkInPath) + File.separatorChar);
			if (!file.exists()) {
				file.mkdirs();
			}
			String necFilePath = appProps.getProperty(carparkNECInPath) + File.separatorChar;

			logger.info("CarPark path:" + csvFilePath);

			
			
			String[] header = { "\"" + CarParkCSVRowType.HEADER.getRowType() + "\"", "\"" + currentDateTimeStr + "\"",
					"\"" + "SEND" + "\"" };
			CsvWriter wr = new CsvWriter(csvFilePath, ',', Charset.forName("SJIS"));
			wr.setUseTextQualifier(false);
			wr.writeRecord(header);
			

			List<CarParkMemberCheckDto> carParkMemberCheckDtoList = carParkMemberCheckService
					.getCarParkMemberCheckList();
			List<CarParkMemberCheckDto> carParkStaffCheckDtoList = carParkMemberCheckService.getCarParkStaffCheckList();
			List<CarParkMemberCheckDto> carParkMemberCheckDtos = new ArrayList<CarParkMemberCheckDto>();
			carParkMemberCheckDtos.addAll(carParkMemberCheckDtoList);
			carParkMemberCheckDtos.addAll(carParkStaffCheckDtoList);
			if (null != carParkMemberCheckDtos && carParkMemberCheckDtos.size() != 0) {
				for (CarParkMemberCheckDto carParkMemberCheckDto : carParkMemberCheckDtos) {
					String newCardNo = "";
					String fullCardNo = "";
					String expiryDate = "";
					if (carParkMemberCheckDto.getType().intValue() == CarParkUserType.MEMBER.getRole().intValue()) {
						newCardNo = CarkParkMemberUtil.formatNumber(8, carParkMemberCheckDto.getCardNo());
						fullCardNo = CarkParkMemberUtil.getCheckDigitByLuhnAlogrithm(newCardNo);
						expiryDate = getFullExpiryDate(carParkMemberCheckDto.getExpiryDate());
						if (expiryDate == null || expiryDate.trim().length() == 0) {
							expiryDate = "2049-12-31 23:59:59";
						}
					} else
						if (carParkMemberCheckDto.getType().intValue() == CarParkUserType.STAFF.getRole().intValue()) {
						newCardNo = CarkParkMemberUtil.formatNumber(6, carParkMemberCheckDto.getCardNo());
						fullCardNo = "S" + CarkParkMemberUtil.getCheckDigitByLuhnAlogrithm(newCardNo);
						expiryDate = "2049-12-31 23:59:59";
					}

					String[] row = { "\"" + CarParkCSVRowType.DETAIL.getRowType() + "\"",
							"\"" + String.valueOf(carParkMemberCheckDto.getAcademyNo()) + "\"",
							"\"" + fullCardNo + "\"",
							"\"" + this.reverseMifareSN(carParkMemberCheckDto.getCardSerialNo()) + "\"",
							"\"" + carParkMemberCheckDto.getPersonName() + "\"",
							"\"" + carParkMemberCheckDto.getType() + "\"",
							"\"" + (null == carParkMemberCheckDto.getLicencePlateNo() ? ""
									: carParkMemberCheckDto.getLicencePlateNo()) + "\"",
							"\"" + carParkMemberCheckDto.getPhoneMobile() + "\"", "\"" + expiryDate + "\"",
							"\"" + Default_Error_Code + "\"", "\"" + "" + "\"" };
					logger.info(Log4jFormatUtil.logInfoMessage(null, null, null, Arrays.toString(row)));

					wr.writeRecord(row);
				}
			}
			String[] footer = { "\"" + CarParkCSVRowType.FOOTER.getRowType() + "\"",
					"\"" + String.valueOf(carParkMemberCheckDtos.size()) + "\"" };
			wr.writeRecord(footer);
			wr.close();

			/***
			 * write success ,after copy file to necFilePath
			 */
			File csvPath = new File(csvFilePath);
			if (csvPath.exists()) {
				copyFileToNEC(csvPath, necFilePath);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			alarmEmailService.sendAlarmEmailTask("DownloadCarParkMemberTask", e);
			logger.debug(
					"DownloadCarParkMemberTaskService.DownloadCarParkMemberList() Invocation --- Unexpected Error: "
							+ e.toString());
		}

	}
	
	private void writerContent(File file,String content){
		FileWriter wr=null;
		try {
			 wr=new FileWriter(file);
			 wr.write(content);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(null!=wr){
				try {
					wr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	public Boolean DeleteAllFullDumpSendFile(String appPath) throws IOException {

		// String sendFilePath = getFilePath() + "FULL" + fileName +
		// ".Send.txt";
		// File sendFile = new File(sendFilePath);
		// if(sendFile.delete()){
		// return true;
		// }
		// return false;

		String fileDir = appProps.getProperty(appPath);
		File csvFile = new File(fileDir);
		if (!csvFile.exists() || !csvFile.isDirectory()) {
			return false;
		}
		File[] csvFileList = csvFile.listFiles();
		for (File file : csvFileList) {
			if (file.isFile()) {
				file.delete();
			}
		}
		return true;

	}

	public static String formatCurrentDate(Date d) {
		DateFormat fileNameDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String currentDate = fileNameDateFormat.format(d);
		return currentDate;
	}

	public static String getFullExpiryDate(Date d) {

		// d.setHours(23);
		// d.setMinutes(59);
		// d.setSeconds(59);

		if (d == null) {
			return "";
		}
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
		String expiryDate = df.format(d);
		return expiryDate;
	}

//	private static String getFilePath(String appPath) throws IOException {
//
//		Resource resource = new ClassPathResource("placeholder/app.properties");
//		Properties appProps = PropertiesLoaderUtils.loadProperties(resource);
//		String path = appProps.getProperty(appPath);
//
//		return path;
//	}

	private void copyFileToNEC(File file, String path) {
		File nFile = new File(path);
		if (!nFile.exists()) {
			nFile.mkdirs();
		}
		nFile = new File(path + file.getName());
		try {
			FileCopyUtils.copy(file, nFile);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (null != nFile) {
				nFile = null;
			}
		}
	}
	/***
	 * per 2 char Reverse
	 * @param content
	 * @return
	 */
	private String reverseMifareSN(String content){
		if(StringUtils.isNotEmpty(content))
		{
			String mifareSN=content.replaceAll("(.{2})", "$1,");
			String []cons=mifareSN.split(","); 
			StringBuffer sb=new StringBuffer();
			 for(int i=0;i<cons.length;i++){
		        	sb.append(cons[cons.length-1-i]);
		     }
			 return sb.toString();
		}
		return null;
	}
	
	/**
	 * handle License Plate  
	 * @param licenceNo
	 * @return "EMPTY" if "" or no alphanumeric string. Split by "/" 
	 */
	public String handlerLicencePlateNo(String licenceNo)
	{
		final String EMPTY="EMPTY";
		
		if (licenceNo==null)
		   return EMPTY;
		
		String licence=licenceNo.replace(" ", "");
		licence=licence.split("/")[0];
		
		if(StringUtils.isEmpty(licence))
		   return EMPTY;						
		
		if(!licence.matches("^[a-zA-Z0-9]*$"))
			return EMPTY;	
			
		return licence;
		
	}
}
