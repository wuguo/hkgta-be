package com.sinodynamic.hkgta.scheduler.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;

@Service
@Transactional
public class ExtensionLetterSendingTaskService {
	private static final String			EXTENSION_LETTER	= Constant.TEMPLATE_ID_EXTENSION;
	private final Logger				logger				= LoggerFactory.getLogger(ExtensionLetterSendingTaskService.class);
	@Autowired
	private CustomerEmailContentService	customerEmailContentService;

	@Autowired
	private MessageTemplateService		messageTemplateService;

	@Autowired
	private CustomerProfileService		customerProfileService;

	@Autowired
	private CustomerServiceAccService	customerServiceAccService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Autowired
	private MemberService memberService;


	public void autoSendingJob() {
		try {

			List<CustomerServiceAcc> customers = customerServiceAccService.getExpiringCustomers();
			
			if (customers != null && customers.size() > 0) {
				logger.info("expiring customerServiceAcc size:"+customers.size());
				MessageTemplate template = getMessageTemplate();
				if (template == null) {
					logger.info("Failed to find template type extension");
					return;
				}
				for (CustomerServiceAcc customer : customers) {
					CustomerProfile cp = customerProfileService.getById(customer.getCustomerId());
					 List<Member> members=memberService.getMembersByCustomerId(cp.getCustomerId().toString());
					 if(null==members&&members.size()>0){
						 logger.info("send mail to customer,customerId:"+cp.getCustomerId()+",member acadeNo:"+members.get(0).getAcademyNo());
					 }
					sendExtensionEmail(cp, template);
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("ExtensionLetterSendingTask", e);
			return;
		}
	}

	private String getLetterContent(CustomerProfile customer, MessageTemplate mt) throws Exception {

		if (customer == null || mt == null)
			return null;
		String content = mt.getContentHtml();
		StringBuilder named = new StringBuilder();
		named.append(customer.getSalutation()).append(" ").append(customer.getGivenName()).append(" ").append(customer.getSurname());
		return content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, named.toString()).replace(
				Constant.PLACE_HOLDER_FROM_USER,
				Constant.HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER);
	}

	private boolean sendExtensionEmail(CustomerProfile customer, MessageTemplate mt) throws Exception {

		if (customer == null || mt == null)
			return false;
		String emailTitle = mt.getMessageSubject();
		String emailBody = getLetterContent(customer, mt);
		CustomerEmailContent cec = customerEmailContentService.sendExtensionLetter(customer.getCustomerId(), emailTitle, emailBody);
		boolean success = MailSender.sendEmail(customer.getContactEmail(), null, null, emailTitle, emailBody, null);
		if (success) {
			cec.setStatus(EmailStatus.SENT.getName());
		} else {
			cec.setStatus(EmailStatus.FAIL.getName());
		}

		customerEmailContentService.modifyCustomerEmailContent(cec);
		return true;
	}

	private MessageTemplate getMessageTemplate() throws Exception {

		return messageTemplateService.getTemplateByFuncId(EXTENSION_LETTER);
	}
}
