package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoRemindRoomAssistantTaskService;

/**
 * <p>
 * Push message to inspector when the time over the room's task response time and did not start the task
 * </p>
 * 
 * @author Annie Xiao
 *
 */
public class AutoRemindRoomAssistantTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger	logger	= LoggerFactory.getLogger(AutoRemindRoomAssistantTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		logger.debug("AutoRemindRoomAssistantTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		AutoRemindRoomAssistantTaskService autoRemindRoomAssistantTaskService = applicationContext.getBean(AutoRemindRoomAssistantTaskService.class);

		autoRemindRoomAssistantTaskService.autoRemindRoomAssistant();

		logger.debug("AutoRemindRoomAssistantTask end");

	}

}
