package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.DDAJobTaskService;
import com.sinodynamic.hkgta.service.crm.account.DDATaskService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;

/**
 * <p>
 * process DDA report file:success report and failed report file, and log the
 * key data into DB
 * </p>
 * 
 * @author Nick_Xiong
 *
 */
public class DDAReporterTask extends QuartzJobBean implements ApplicationContextAware {
	private Logger ddxLogger = LoggerFactory.getLogger("ddx");

	WebApplicationContext applicationContext;

	@Autowired
	private AlarmEmailService alarmEmailService;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		ddxLogger.info("DDAReporterTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		
		DDAJobTaskService ddaTaskService = applicationContext.getBean(DDAJobTaskService.class);
		try {
			ddaTaskService.excute();
		} catch (Exception e) {
			ddxLogger.error("DDAReporterTask.ddaTask had happend" + e.getMessage());
			alarmEmailService.sendAlarmEmailTask("DDAReporterTask", e);
		}

		ddxLogger.info("DDAReporterTask.ddaTask auto execute end ...");
	}

}
