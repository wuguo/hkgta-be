package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoReleaseRoomStatusService;
/**
 * <p>
 * Auto setting the room status  
 * when  currentTime  between    beginDate  and endDate
 * update room frontdesk_status to OOO OR service_status to OOS
 * </p>
 * 
 * @author christ
 *
 */
public class AutoSettingRoomStatusTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(AutoSettingRoomStatusTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoSettingRoomStatusTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		AutoReleaseRoomStatusService service = applicationContext
				.getBean(AutoReleaseRoomStatusService.class);
		service.autoSettingRoomStatusTask();
		logger.info("AutoSettingRoomStatusTask.autoSendingJob auto execute end ...");
	}

}
