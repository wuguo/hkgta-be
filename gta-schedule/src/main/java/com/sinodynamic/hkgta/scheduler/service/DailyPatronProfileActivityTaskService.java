package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.adm.UserRecordActionLogService;


@Component
public class DailyPatronProfileActivityTaskService {
	
	private Logger logger	= Logger.getLogger(DailyPatronProfileActivityTaskService.class);
	
	@Autowired
	private UserRecordActionLogService userRecordActionLogService ;
	

	public void generateReport()
	{
		try {
			String pdfPath=userRecordActionLogService.excute(new Date());
			if(StringUtils.isNotEmpty(pdfPath)){
				//send staff mail
				logger.info("generate Daily patron activity report success ,and send mail to staff ......");
				userRecordActionLogService.sendEmailActivityReport(pdfPath);
			}else{
				logger.info("generate Daily patron activity report error ......");
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		
	}
}
