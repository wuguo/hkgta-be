package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.FacilityBookingEndRemindService;

/**
 * <p>
 * Remind member in the remaining 10 minutes for golf or tennis
 * </p>
 * @author Annie Xiao
 *
 */
public class FacilityBookingEndRemindTask extends QuartzJobBean implements  ApplicationContextAware
{
	private final Logger logger = LoggerFactory.getLogger(FacilityBookingEndRemindTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException
	{
		SimpleDateFormat simepleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		logger.info("start to push end remind message to members " + simepleDateFormat.format(new Date()));
		FacilityBookingEndRemindService facilityBookingEndRemindService = applicationContext.getBean(FacilityBookingEndRemindService.class);
		facilityBookingEndRemindService.pushMessageNotification();
		logger.info("Finished push end remind message to members .");
	}

}

