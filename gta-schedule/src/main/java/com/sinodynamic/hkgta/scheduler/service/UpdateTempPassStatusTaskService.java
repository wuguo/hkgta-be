package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.backoffice.admin.TempPassService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class UpdateTempPassStatusTaskService {

	private Logger			logger	= Logger.getLogger(UpdateTempPassStatusTaskService.class);

	@Autowired
	private TempPassService	tempPassService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void closeCourseOnRegistDueDate() {
		try {
			tempPassService.updateStatusForExpiredTempPass();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("UpdateTempPassStatusTask", e);
			// e.printStackTrace();
			// logger.info(e.toString());
		}
	}
}
