package com.sinodynamic.hkgta.scheduler.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;

@Component
public class AutoSendMailTaskService {
	@Autowired
	private CustomerEmailContentService customerEmailContentService;

	public void excuteSend() {
		customerEmailContentService.autoSendMail();
	}

}
