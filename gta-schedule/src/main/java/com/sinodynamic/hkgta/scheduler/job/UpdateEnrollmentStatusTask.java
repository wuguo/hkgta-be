package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.UpdateEnrollmentStatusTaskService;

/**
 * <p>
 * Set member's Enrollment status to Complete(CMP) when the Member complete enrollment and get Permit Card.
 * </p>
 * 
 * @author Allen Yu
 *
 */
public class UpdateEnrollmentStatusTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(UpdateEnrollmentStatusTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		logger.info("UpdateEnrollmentStatusTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		UpdateEnrollmentStatusTaskService updateEnrollmentStatusTaskService = applicationContext.getBean(UpdateEnrollmentStatusTaskService.class);

		updateEnrollmentStatusTaskService.updateEnrollmentStatusJob();

		logger.info("UpdateEnrollmentStatusTask.updateEnrollmentStatusJob auto execute end ...");
	}

}
