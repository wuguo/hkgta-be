package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.entity.crm.BiMemberCnt;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.ChangeMemberStatusService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.BiMemberCntPeriod;

@Component
public class AutoStatisticsActivationInactiveMembersTaskService {

	private Logger logger = Logger.getLogger(AutoStatisticsActivationInactiveMembersTaskService.class);

	@Autowired
	private AlarmEmailService	alarmEmailService;

	@Autowired
	private ChangeMemberStatusService	changeMemberStatusService;

	@Transactional
	public void autoStatisticsActivationInactiveMembers() {
		try {
			int actNum = changeMemberStatusService.getStatisticsActivationOrInactiveMembersNum(true);
			int nactNum = changeMemberStatusService.getStatisticsActivationOrInactiveMembersNum(false);
			BiMemberCnt abm = changeMemberStatusService.getBiMemberCnt(DateCalcUtil.thisMonthFirstDay(), BiMemberCntPeriod.Monthly.getDesc(), true);
			boolean isUpdate = true;
			if (abm == null) {
				abm = new BiMemberCnt();
				abm.setCountDate(DateCalcUtil.thisMonthFirstDay());
				abm.setCreateDate(new Date());
				abm.setPeriod(BiMemberCntPeriod.Monthly.getDesc());
				abm.setMbrStatus("ACT");
				isUpdate = false;
			}
			abm.setUpdateDate(new Date());
			abm.setTotalMember(actNum);
			if (isUpdate) {
				changeMemberStatusService.updateBiMemberCnt(abm);
			} else {
				changeMemberStatusService.seveBiMemberCnt(abm);
			}
			BiMemberCnt nabm = changeMemberStatusService.getBiMemberCnt(DateCalcUtil.thisMonthFirstDay(), BiMemberCntPeriod.Monthly.getDesc(), false);
			isUpdate = true;
			if (nabm == null) {
				nabm = new BiMemberCnt();
				nabm.setCountDate(DateCalcUtil.thisMonthFirstDay());
				nabm.setCreateDate(new Date());
				nabm.setPeriod(BiMemberCntPeriod.Monthly.getDesc());
				nabm.setMbrStatus("NACT");
				isUpdate = false;
			}
			nabm.setUpdateDate(new Date());
			nabm.setTotalMember(nactNum);
			if (isUpdate) {
				changeMemberStatusService.updateBiMemberCnt(nabm);
			} else {
				changeMemberStatusService.seveBiMemberCnt(nabm);
			}
		} catch (Exception e) {
			logger.info(e.getMessage(), e);
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoStatisticsActivationInactiveMembersTask", e);
		}
	}
}
