package com.sinodynamic.hkgta.scheduler.service;

import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayCmdDao;
import com.sinodynamic.hkgta.dao.onlinepayment.PaymentGatewayDao;
import com.sinodynamic.hkgta.dto.Constant.Status;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.mms.MMSService;
import com.sinodynamic.hkgta.service.onlinepayment.AmexPaymentGatewayService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderDetService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;

@Component
public class OnlinePaymentCheckTaskService {
	private Logger logger = Logger.getLogger(OnlinePaymentCheckTaskService.class);

	@Resource(name = "appProperties")
	private Properties appProps;

	@Autowired
	private CustomerOrderTransService customerOrderTransService;

	@Autowired
	private PaymentGatewayService paymentGatewayService;

	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;

	@Autowired
	private CustomerOrderDetService customerOrderDetService;

	@Autowired
	private GuessRoomCheckService guessRoomCheckService;

	@Autowired
	private MMSService mmsService;

	@Autowired
	private AlarmEmailService alarmEmailService;

	@Autowired
	private AmexPaymentGatewayService amexPaymentGatewayService;

	/**
	 * This method is used to check whether the online payment is success.
	 */
	public void checkOnlinePaymentStatus() {

		List<CustomerOrderTrans> customerOrderTranses = customerOrderTransService.getTimeoutPayments();
		logger.info("OnlinePaymentCheckTask result list size from search size:"
				+ (null == customerOrderTranses ? 0 : customerOrderTranses.size()));

		for (CustomerOrderTrans customerOrderTrans : customerOrderTranses) {
			if (PaymentMethod.AMEX.getCardCode().equalsIgnoreCase(customerOrderTrans.getPaymentMethodCode())) {
				logger.info("onlinePaymentCheck:"+PaymentMethod.AMEX.getDesc()+" query DR from amexPaymentGateWay  check trans status .... ");
				customerOrderTrans = amexPaymentGatewayService.queryDR(customerOrderTrans);
			} else {
				logger.info("onlinePaymentCheck:  query DR from PaymentGateWay  check trans status .... ");
				customerOrderTrans = paymentGatewayService.queryResult(customerOrderTrans);
			}
			
			if(null==customerOrderTrans)continue;
			logger.info("update GTA CustomerOrderTrans status  is FAIL...the customerOrderTran No :"+customerOrderTrans.getTransactionNo());
			customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
			
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			if (null != customerOrderHd) {
				CustomerOrderDet customerOrderDet = customerOrderDetService
						.getCustomerOrderDet(customerOrderHd.getOrderNo());

				if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo())
						&& customerOrderDet.getItemNo().contains(Constant.FACILITY_PRICE_PREFIX)) {
					logger.info(
							"orderNo:" + customerOrderHd.getOrderNo() + " orderItemNo:" + customerOrderDet.getItemNo());
					/***
					 * SGG-3470
					 * when same time payment two times ,one suc  and other fail , don't update order /memberFacilityBooking
					 */
					if(customerOrderHd.getOrderStatus().equals(Status.CMP.name()))
					{
						logger.info("CustomerOrderHd status :" +customerOrderHd.getOrderStatus() + ",don't voidMemberFacilityTypeBooking.. "
								+ " orderItemNo:" + customerOrderDet.getItemNo()+""
										+ "customerOrderTrans no:"+customerOrderTrans.getTransactionNo());
						continue;
					}
					memberFacilityTypeBookingService.voidMemberFacilityTypeBooking(
							customerOrderTrans.getCustomerOrderHd().getOrderNo(), "system");
				}
				if (null != customerOrderDet && !StringUtils.isEmpty(customerOrderDet.getItemNo())
						&& customerOrderDet.getItemNo().startsWith("MMS")) {
					// if(logger.isDebugEnabled()){
					// }
					try {
						mmsService.canCelAppointment(customerOrderHd.getVendorRefCode(), "System");
						logger.info("cancel MMS appointment, invoiceNo=" + customerOrderHd.getVendorRefCode());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage(), e);
						alarmEmailService.sendAlarmEmailTask("OnlinePaymentCheckTask", e);
					}
				}
			}

			logger.info("updateCustomerOrderTrans param: transactionNo:" + customerOrderTrans.getTransactionNo()
					+ ",paidAmount:" + customerOrderTrans.getPaidAmount() + ",status:" + customerOrderTrans.getStatus()
					+ ",customerId:" + customerOrderTrans.getCustomerOrderHd().getCustomerId());

		}
		/** This is for guess room check **/
		guessRoomCheckService.checkGuessRoomStatus();
	}
}
