package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class AutoCloseCourseTaskService {

	private Logger logger = Logger.getLogger(AutoCloseCourseTaskService.class);

	@Autowired
	private CourseService courseService;

	@Autowired
	private AlarmEmailService	alarmEmailService;

	public void closeCourseOnRegistDueDate() {
		try {
			courseService.closeCourseOnDueDate();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoCloseCourseTask", e);
		}
	}
}
