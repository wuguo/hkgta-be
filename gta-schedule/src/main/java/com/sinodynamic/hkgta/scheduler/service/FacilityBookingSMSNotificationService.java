package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.constant.Constant;

@Component
public class FacilityBookingSMSNotificationService {
	private Logger						logger	= Logger.getLogger(FacilityBookingSMSNotificationService.class);

	@Autowired
	MemberFacilityTypeBookingService	memberFacilityTypeBookingService;

	@Autowired
	private MessageTemplateService		messageTemplateService;

	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService			devicePushService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void pushMessageNotification() {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			List<FacilityReservationDto> facilityReservationDtoList = memberFacilityTypeBookingService.getAllTodayMemberFacilityTypeBooking(true);
			if (null != facilityReservationDtoList && facilityReservationDtoList.size() > 0) {
				logger.info("Today MemberFacilityTypeBooking size:"+facilityReservationDtoList.size()+" and send SMS..............");
				
				for (FacilityReservationDto facilityReservationDto : facilityReservationDtoList) {
					long diff = format.parse(facilityReservationDto.getBookingDate() + " " + facilityReservationDto.getStartTime()).getTime()
							- new Date().getTime();
					long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
					if (diffInMinutes <= 60 && diffInMinutes > 30) {
						MessageTemplate messageTemplate;
						if (facilityReservationDto.getResvFacilityType().equalsIgnoreCase("GOLF")) {
							messageTemplate = messageTemplateService.getTemplateByFuncId(Constant.TEMPLATE_ID_GOLF_BOOK_REMIND);
						} else {
							messageTemplate = messageTemplateService.getTemplateByFuncId(Constant.TEMPLATE_ID_TENNIS_BOOK_REMIND);
						}
						String content = messageTemplate.getContent();
						String subject = messageTemplate.getMessageSubject();
						//String message = content.replaceFirst("\\{facilityType\\}", facilityReservationDto.getResvFacilityType().toLowerCase())
						String message = content.replace("{facilityType}", facilityReservationDto.getBayType())
								.replace("{timeLeft}", Long.toString(diffInMinutes))
								.replace("{bookingDate}", facilityReservationDto.getBookingDate())
								.replace("{startTime}", facilityReservationDto.getStartTime())
								//.replace("{facilityType}", facilityReservationDto.getResvFacilityType())
								.replace("{facilityType}", facilityReservationDto.getBayType())
								.replace("{facilityTypeQty}", Long.toString(facilityReservationDto.getFacilityTypeQty()));
						devicePushService.pushMessage(new String[] { facilityReservationDto.getUserId() }, subject, message, "member");
						
						logger.info("push message ,memberFacilityTypeBooking resvId:"+facilityReservationDto.getResvId()+" facilityType:"+facilityReservationDto.getResvFacilityType()+" acadeNo:"+facilityReservationDto.getAcademyNo()+"\n message:"+message);
						
						logger.info(facilityReservationDto.getResvFacilityType() + " reservation remind success");
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("FacilityBookingSMSNotificationTask", e);
		}
	}


}
