package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoRemindMemberAndCaochInAdvance1HourTaskService;

/**
 * <p>
 * Auto send sms message to member in advance 1 hour, and push message to coach before private coach start
 * </p>
 * private coach Auto send MSM remind member InAdvance1Hour private coach Auto push msg remind coach InAdvance1Hour
 * 
 * @author Zero_Wang
 *
 */
public class AutoRemindMemberAndCaochInAdvance1HourTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(AutoRemindMemberAndCaochInAdvance1HourTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoRemindMemberAndCaochInAdvance1HourTask run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		AutoRemindMemberAndCaochInAdvance1HourTaskService autoRemindCoachTaskService = applicationContext
				.getBean(AutoRemindMemberAndCaochInAdvance1HourTaskService.class);

		autoRemindCoachTaskService.autoRemindInAdvance1Hour();
		logger.info("AutoRemindMemberAndCaochInAdvance1HourTask auto execute end ...");

	}
}
