package com.sinodynamic.hkgta.scheduler.service;

public interface ImportRestaurantDataTaskService {

	public void importRestaurant() throws Exception;
}
