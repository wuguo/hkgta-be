package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.service.mms.SpaAppointmentRecService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

/**
 * <p>
 * Auto notify mms reserved member the appointment begin time is comming 1 hour in advance
 * </p>
 * 
 * @author Mianping Wu
 *
 */
public class SpaAppointmentBeginNotificationTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(SpaAppointmentBeginNotificationTask.class);

	WebApplicationContext	applicationContext;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		logger.info("SpaAppointmentBeginNotificationTask run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		try {
			SpaAppointmentRecService spaAppointmentRecService = applicationContext.getBean(SpaAppointmentRecService.class);
			spaAppointmentRecService.spaAppointmentBeginNotification();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString(), e);
			alarmEmailService.sendAlarmEmailTask("SpaAppointmentBeginNotificationTask", e);
		}

		logger.info("SpaAppointmentBeginNotificationTask execute end ...");
	}

}
