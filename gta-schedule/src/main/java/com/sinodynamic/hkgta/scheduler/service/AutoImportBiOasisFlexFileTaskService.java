package com.sinodynamic.hkgta.scheduler.service;

import java.io.File;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.bi.BiOasisFlexFileService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.LoggerType;

@Component
public class AutoImportBiOasisFlexFileTaskService 
{
	private final Logger logger = Logger.getLogger(LoggerType.OASISFLEXFILE.getName());
	@Autowired
	private BiOasisFlexFileService biOasisFlexFileService;
	
	@Value(value = "#{oasisProperties['oasis.flex.path']}")
	private String oasisFlexPath;

	@Value(value = "#{oasisProperties['oasis.retry.times']}")
	private int retryTimes;
	
	
	public void excute(Date time)
	{
		if (null == time)time = new Date();
		Date originalDate = DateConvertUtil.parseString2Date(DateCalcUtil.getDateBefore(time, 1), "yyyy-MM-dd");
		String filePath = oasisFlexPath + DateConvertUtil.formatCurrentDate(originalDate) + "F.txt";
		for (int i = 1; i <= retryTimes; i++) {
			if (checkFileExist(filePath)) {
				// read file save db
				logger.info("auto import try " + i + " time ,the file exsit :" + filePath);
				try {
					if (biOasisFlexFileService.excuteImportFile(filePath, time)) {
						break;
					} else {
						sleepMins();
					}
				} catch (Exception e) {
					logger.error("save db happen The exceptioin is:" + e.getMessage());
					sleepMins();
				}
			} else {
				sleepMins();
				logger.info("auto import file not find :" + filePath + " try " + i + " times");

			}
		}
	}
	private void sleepMins() {
		logger.info(" wait after 1 mins  excute ......");
		try {
			Thread.sleep(60000l);
		} catch (InterruptedException e) {
			logger.error("wait 1 mins happend exceptioin is:" + e.getMessage());
		}
	}

	private boolean checkFileExist(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}
}
