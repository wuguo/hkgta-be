package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.FlexPaymentReportTaskService;
@Component
public class FlexPaymentReportTask extends QuartzJobBean implements ApplicationContextAware {

	private final Logger	logger	= LoggerFactory.getLogger(FlexPaymentReportTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("FlexPaymentReportTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		FlexPaymentReportTaskService onlinePaymentCheckTaskService = applicationContext.getBean(FlexPaymentReportTaskService.class);
		onlinePaymentCheckTaskService.createReportCSV();

		logger.info("FlexPaymentReportTask.createReportCSV auto execute end ...");
	}

}
