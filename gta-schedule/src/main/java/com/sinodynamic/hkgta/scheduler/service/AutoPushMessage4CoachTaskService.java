package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.fms.TrainerAppService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class AutoPushMessage4CoachTaskService {
	private Logger				logger	= Logger.getLogger(AutoPushMessage4CoachTaskService.class);

	@Autowired
	private TrainerAppService	trainerAppService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void autoPushMsgInAdvance2Day() {
		try {
			trainerAppService.autoPushMsgInAdvance2Day();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoPushMessage4CoachTask", e);
		}

		logger.info("TrainerAppService.auto push message invocation end ...");
	}

}
