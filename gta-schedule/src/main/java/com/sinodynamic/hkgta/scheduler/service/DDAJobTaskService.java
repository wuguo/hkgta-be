package com.sinodynamic.hkgta.scheduler.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import com.sinodynamic.hkgta.service.crm.account.DDATaskService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.SftpByJsch;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Component
public class DDAJobTaskService {

	private Logger ddxLogger = LoggerFactory.getLogger(LoggerType.DDX.getName());
	@Resource(name = "ddxProperties")
	protected Properties ddxProps;
	private String ftpHost;
	private String port;
	private String ftpUserName;
	private String ftpPassword;
	private String timeout;

	private String receivePath;
	private String receiveBackPath;
	private String downLoadPath; // store all the remote files in local
	private String downLoadBackPath; // store the success processed files in   local
	
	@Autowired
	private AlarmEmailService alarmEmailService;
	
	@Autowired
	private DDATaskService ddaTaskService;
	
	@PostConstruct
	public void init() {
		ftpHost = ddxProps.getProperty(Constant.SFTP_HOST, null);
		port = ddxProps.getProperty(Constant.SFTP_PORT, null);
		ftpUserName = ddxProps.getProperty(Constant.SFTP_USERNAME, null);
		ftpPassword = ddxProps.getProperty(Constant.SFTP_PASSWORD, null);
		timeout = ddxProps.getProperty(Constant.SFTP_TIMEOUT, null);

		receivePath = ddxProps.getProperty(Constant.SFTP_RECEIVE_PATH, null);// remote  downloand dir
		receiveBackPath = ddxProps.getProperty(Constant.SFTP_RECEIVE_BACK_PATH, null);// remote back dir, after processed move to this dir
		downLoadPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_PATH, null);// store all  the remote files in local
		downLoadBackPath = ddxProps.getProperty(Constant.CSV_DOWNLOAD_BACK_PATH, null);// store  the  success processed files in local
	}
	private void setFileListSortByTime(List<File> files)
	{
		ddxLogger.info("DDA file sort by time fileName start......");
		Collections.sort(files, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				String fTime = o1.getName().split("\\.")[2];
				String eTime = o2.getName().split("\\.")[2];
				Long fNum = Long.valueOf(fTime.replaceAll("[^\\d]", ""));
				Long eNum = Long.valueOf(eTime.replaceAll("[^\\d]", ""));
				return fNum.compareTo(eNum);
			}
		});
		ddxLogger.info("DDA file sort by time fileName end.....");
	}
	public void excute() throws Exception{
		String ddaPaymentsRcmsE64fileReg = ddxProps.getProperty("dda.payments.rcmsE64fileReg");
		String requestUrl = "ftpHost:" + ftpHost + ",ftpUserName:" + ftpUserName + ",port:" + port;
		ddxLogger.info("DDA file dowload start..");
		ddxLogger.info(Log4jFormatUtil.logInfoMessage(requestUrl, null, "sftp", null));
		SftpByJsch sftp = new SftpByJsch(ftpHost, ftpUserName, ftpPassword, Integer.valueOf(port).intValue(), null,
				null, Integer.valueOf(timeout).intValue());
		List<File> files = sftp.dowloadByFileNameRegex(receivePath, ddaPaymentsRcmsE64fileReg, downLoadPath);
		if (null != files && files.size() > 0) {
			this.setFileListSortByTime(files);// ascending time
			for (File file : files) {
				ddxLogger.info("DDA file  is:" + file.getName());
				ddxLogger.info("handler dda workflow start.....");
				try {
					ddaTaskService.parseDDAConfirmReporter(file);
				} catch (Exception e) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					e.printStackTrace(new PrintStream(baos));
					ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null, baos.toString()));
					ddaTaskService.saveDDALog(file.getName(), e.getMessage());
					alarmEmailService.sendAlarmEmailTask("DDA Task excute happend exception", e);
				}
				this.backUpLocal_Remote_DDAFile(file);
				ddxLogger.info("handler dda workflow end.....");
			}
		} else {
			ddxLogger.info(
					"don't find any file DDA RCMS_E_64 in sftp server ......please check sftp server have exist file ");
		}
		ddxLogger.info("DDA file dowload end..");
		
	}
	
	/***
	 * backup local and remote dda file 
	 * @param file
	 */
	private void backUpLocal_Remote_DDAFile(File file)throws GTACommonException
	{
		ddxLogger.info("backup local and remote DDA file start..");
		try {
			// backup sftp file to bak directory
			SftpByJsch sftp = new SftpByJsch(ftpHost, ftpUserName, ftpPassword, Integer.valueOf(port).intValue(), null,
					null, Integer.valueOf(timeout).intValue());
//			sftp.move(file.getName(), receivePath, receiveBackPath);
			
			String currentTime=DateConvertUtil.parseDate2String(new Date(),"yyyyMMddHHmmss");
			sftp.move(file.getName(), receivePath, currentTime+"-"+file.getName(), receiveBackPath);

			File outFile = new File(downLoadBackPath + File.separator + file.getName());
			FileCopyUtils.copy(file, outFile);
			file.delete();
			ddxLogger.info(Log4jFormatUtil.logInfoMessage(null, null, null,
					downLoadBackPath + File.separator + file.getName()));
		} catch (Exception e) {
			e.printStackTrace();  
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	        e.printStackTrace(new PrintStream(baos));  
			ddxLogger.error(Log4jFormatUtil.logErrorMessage(null, null, null, null,"backUpLocal_Remote_DDAFile happend Exception:"+baos.toString()));
			throw new GTACommonException(e);
		}
		ddxLogger.info("backup local and remote DDA file end..");
	}
}
