package com.sinodynamic.hkgta.scheduler.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class CheckExpiredIndividualMemberTaskService {
	private Logger logger = Logger.getLogger(CheckExpiredIndividualMemberTaskService.class);
	@Autowired
	private CustomerServiceAccService customerServiceAccService;

	@Autowired
	private AlarmEmailService	alarmEmailService;

	public void updateExpiredIndividualMemberStatus() {
		try {
			customerServiceAccService.handleExpiredCustomerServiceAcc();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("CheckExpiredIndividualMemberTask", e);
		}
	}

}
