package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.FacilityBookingSMSNotificationService;

/**
 * <p>
 * Notification member in one hour before the golf or tennis booking time
 * </p>
 * @author Annie Xiao
 *
 */
public class FacilityBookingSMSNotificationTask extends QuartzJobBean implements  ApplicationContextAware
{
	private final Logger logger = LoggerFactory.getLogger(FacilityBookingSMSNotificationTask.class); 
	
	WebApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = (WebApplicationContext)applicationContext;
		
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException
	{
		SimpleDateFormat simepleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		logger.info("start to push remind message to members " + simepleDateFormat.format(new Date()));
		FacilityBookingSMSNotificationService facilityBookingSMSNotificationService = applicationContext.getBean(FacilityBookingSMSNotificationService.class);
		facilityBookingSMSNotificationService.pushMessageNotification();
		logger.info("Finished push remind message to members. ");
	}

}

