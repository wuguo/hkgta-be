package com.sinodynamic.hkgta.scheduler.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.fms.CourseEnrollmentService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.DateCalcUtil;

@Component
public class CourseSessionReminderTaskService {

	private Logger					logger					= Logger.getLogger(CourseSessionReminderTaskService.class);

	private static final String		APPLICATION_NAME_MEMBER	= "member";
	private static final String		APPLICATION_NAME_COACH	= "trainer";


	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService		devicePushService;

	@Autowired
	private CourseEnrollmentService	courseEnrollmentService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void sendAppReminderMsg() {
		try {

			Date beginTime = new Date();
			Date endTime = DateCalcUtil.GetNextHour(beginTime);

			logger.info("startTime:" + beginTime + " endTime:" + endTime);

			// send app reminder to member
			Map<String, List<UserDevice>> container = courseEnrollmentService.getAppMemberAndReminderMsg(beginTime, endTime, APPLICATION_NAME_MEMBER);
			if (container != null && container.size() > 0) {
				for (Map.Entry<String, List<UserDevice>> entry : container.entrySet()) {
					String message = entry.getKey();
					String[] userIds = getUserIds(entry.getValue());
					logger.info("message for member:" + message+" userIds:"+Arrays.toString(userIds));
					devicePushService.pushMessage(userIds, message, APPLICATION_NAME_MEMBER);
				}
			}

			// send app reminder to coach
			Map<String, Set<String>> coachContainer = courseEnrollmentService.getAppCoachAndReminderMsg(beginTime, endTime, APPLICATION_NAME_COACH);
			if (coachContainer != null && coachContainer.size() > 0) {
				for (Map.Entry<String, Set<String>> coachEntry : coachContainer.entrySet()) {
					String message = coachEntry.getKey();
					Set<String> coachSet = coachEntry.getValue();
					String[] coachUserIds = coachSet.toArray(new String[coachSet.size()]);
					devicePushService.pushMessage(coachUserIds, message, APPLICATION_NAME_COACH);
				}
			}

		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("CourseSessionReminderTask", e);
		}
	}

	private String[] getUserIds(List<UserDevice> users) {

		String[] userIds = new String[users.size()];
		int index = 0;
		if (logger.isDebugEnabled()) {
			logger.debug("userIds:");
		}
		for (UserDevice user : users) {
			logger.debug(user.getUserId());
			userIds[index] = user.getUserId();
			index++;
		}
		return userIds;
	}

}
