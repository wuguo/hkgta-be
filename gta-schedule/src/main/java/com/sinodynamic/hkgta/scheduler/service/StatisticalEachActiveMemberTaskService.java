package com.sinodynamic.hkgta.scheduler.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dto.rpos.ToDayBookingMemberDto;
import com.sinodynamic.hkgta.service.adm.BiMemberUsageRateService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.ChangeMemberStatusService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.DateCalcUtil;

@Component
public class StatisticalEachActiveMemberTaskService {

	private Logger logger = Logger.getLogger(StatisticalEachActiveMemberTaskService.class);

	@Autowired
	private AlarmEmailService	alarmEmailService;

	@Autowired
	private ChangeMemberStatusService	changeMemberStatusService;
	
	@Autowired
	private BiMemberUsageRateService	biMemberUsageRateService;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private AutoStatisticsActivationInactiveMembersTaskService autoCloseCourseTaskService;

	@Transactional
	public void statisticalEachActiveMembers() {
		try {
			autoCloseCourseTaskService.autoStatisticsActivationInactiveMembers();
			String thisMonthFirstDayStr = DateCalcUtil.thisMonthFirstDayStr();
//			Date   thisMonthFirstDay = DateCalcUtil.thisMonthFirstDay();
			//如果今天是一号的话进行激活用户批量插入操作
//			if (thisMonthFirstDayStr.equals(DateCalcUtil.formatDate(new Date()))) {
				//判断是否已经执行过激活用户的批量操作
				if (!biMemberUsageRateService.isRunInsert(thisMonthFirstDayStr)) {
					biMemberUsageRateService.bulkInsertByActiveMember(thisMonthFirstDayStr);
				} else {
					//批量插入最新激活用户操作 避免当前有新用户未被统计
					biMemberUsageRateService.bulkInsertByNewsActiveMember(thisMonthFirstDayStr);
				}
//			}
			//获取今天有book的用户
			List<ToDayBookingMemberDto> list = memberFacilityTypeBookingService.getToDayBookingMember();
			StringBuilder ids = new StringBuilder();
			for (ToDayBookingMemberDto toDayBookingMemberDto : list) {
				if (ids.length() > 0) {
					ids.append(", ");
				}
				ids.append(toDayBookingMemberDto.getCustomerId());
			}
			//更新激活用户book数
			if(StringUtils.isNotEmpty(ids.toString()))
			{
				biMemberUsageRateService.updateActiveMemberBookNum(thisMonthFirstDayStr, ids.toString());	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage(), e);
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("StatisticalEachActiveMemberTaskService", e);
		}
	}
}
