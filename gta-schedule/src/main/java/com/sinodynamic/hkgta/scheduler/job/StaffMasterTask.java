package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.StaffMasterTaskService;

/**
 * <p>
 * auto change the staff's status to inactive if the staff's quit data is the last day.
 * </p>
 * 
 * @author Zero Wang
 *
 */
public class StaffMasterTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(StaffMasterTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("StaffMasterTask at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		StaffMasterTaskService staffMasterTaskService = applicationContext.getBean(StaffMasterTaskService.class);

		staffMasterTaskService.autoUNACTQuitStaff();
		logger.info("StaffMasterTask execute end ...");
	}
}
