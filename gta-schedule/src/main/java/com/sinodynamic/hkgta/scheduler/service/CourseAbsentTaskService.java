package com.sinodynamic.hkgta.scheduler.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.fms.CourseEnrollmentDao;
import com.sinodynamic.hkgta.dao.fms.CourseSessionDao;
import com.sinodynamic.hkgta.dao.fms.StudentCourseAttendanceDao;
import com.sinodynamic.hkgta.dto.fms.CourseSessionDto;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.fms.StudentCourseAttendance;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

import net.sf.json.JSONObject;

/**
 * 
 * @author Kaster 20160304
 *
 */
@Component
public class CourseAbsentTaskService {
	private final Logger logger = Logger.getLogger(CourseAbsentTaskService.class);
	
	@Autowired
	StudentCourseAttendanceDao studentCourseAttendanceDao;
	
	@Autowired
	CourseSessionDao courseSessionDao;
	
	@Autowired
	CourseEnrollmentDao courseEnrollmentDao;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	@Transactional
	public void autoChangeCourseAttendanceToAbsent() {
		try {
			//查询出过期的course session。为了确保性能，只查当天的，之前的记录已经被检查过了。
			String sqlCourseSession = "select sys_id as sysId,course_id as courseId from course_session where CURRENT_DATE=DATE_FORMAT(end_datetime,'%Y-%m-%d') and"
					+ " CURRENT_TIMESTAMP > DATE_ADD(end_datetime,INTERVAL 30 MINUTE)";
			List<CourseSessionDto> csList = courseSessionDao.getDtoBySql(sqlCourseSession, null,
					CourseSessionDto.class);
			
			logger.info(" get course session  expird  30 minute size :"+(null==csList?0:csList.size()));
			
			if (csList != null && csList.size() > 0) {
				for (CourseSessionDto dto : csList) {
					//根据course id找出Course_Enrollment的记录
					List<CourseEnrollment> ceList = courseEnrollmentDao
							.getCourseEnrollmentByCourseId(Long.valueOf(dto.getCourseId()));
					logger.info(" expird 30 minute course session , courseEnrollment size:"+(null==ceList?0:ceList.size())+",courseId:"+dto.getCourseId());
					
					for (CourseEnrollment ce : ceList) {
						//根据enroll_id和course_session_id在student_course_attendance中查找记录。
						String attendId = studentCourseAttendanceDao
								.getAttendIdBySessionIdAndEnrollid(dto.getSysId().toString(), ce.getEnrollId());
						if (attendId != null && !attendId.equals("")) {
							logger.info(" StudentCourseAttendance attendId : "+attendId);
							//如果能找到，且status为 NATD说明没有Roll Call，则修改记录的状态。
							StudentCourseAttendance sca = studentCourseAttendanceDao.get(StudentCourseAttendance.class,
									Long.valueOf(attendId));
							if (sca.getStatus().equalsIgnoreCase("NATD")) {
								logger.info("update status "+sca.getStatus()+" to ABS....and StudentCourseAttendance attendId:"+sca.getAttendId());
								sca.setStatus("ABS");
								sca.setUpdateDate(new Date());
								sca.setUpdateBy("system");
								studentCourseAttendanceDao.update(sca);
							}
						} else {
							//找不到记录，说明enroll了，但是完全没有Roll Call，则增加一条缺席的记录。
							StudentCourseAttendance attendance = new StudentCourseAttendance();
							attendance.setCourseSessionId(dto.getSysId());
							attendance.setCreateBy("system");
							attendance.setCreateDate(new Timestamp(new Date().getTime()));
							attendance.setEnrollId(ce.getEnrollId());
							attendance.setStatus("ABS");
							attendance.setUpdateBy("system");
							attendance.setUpdateDate(new Date());
							attendance.setVerNo(0L);
							studentCourseAttendanceDao.save(attendance);
							logger.info(" add new StudentCourseAttendance record ....params:"+JSONObject.fromObject(attendance).toString());
						}
					}
				}
			} 
		} catch (Exception e) {
			alarmEmailService.sendAlarmEmailTask("CourseAbsentTask", e);
		}
	}
}
