package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sinodynamic.hkgta.util.constant.LoggerType;

/**
 * <p>
 * auto logger per day
 * </p>
 * 
 * @author christ
 *
 */
public class AutoLoggerTask implements Job {

	private final Logger logger = LoggerFactory.getLogger(AutoLoggerTask.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoLoggerTask start.............................");
		for (LoggerType log : LoggerType.values()) {
			if (StringUtils.isNotEmpty(log.getName())) {
				Logger logger = LoggerFactory.getLogger(log.getName());
				logger.info("");
			}
		}
		logger.info("AutoLoggerTask end.............................");
	}
}
