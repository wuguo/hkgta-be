package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.AutoReleaseRoomStatusService;

/**
 * <p>
 * Auto release the room status is  OOO/OOS when expired 
 * when status is OOO  update status D
 * when status is OOS  update status null 
 * </p>
 * 
 * @author christ
 *
 */
public class AutoReleaseRoomStatusTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(AutoReleaseRoomStatusTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AutoReleaseRoomStatusTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
		AutoReleaseRoomStatusService service = applicationContext
				.getBean(AutoReleaseRoomStatusService.class);
		service.autoReleaseRoomStatusTask();
		logger.info("AutoReleaseRoomStatusTask.autoSendingJob auto execute end ...");
	}

}
