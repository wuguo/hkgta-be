package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderPermitCardService;

@Component
public class DaypassCardTaskService {
	private final Logger					logger	= LoggerFactory.getLogger(DaypassCardTaskService.class);
	@Autowired
	private CustomerOrderPermitCardService	customerOrderPermitCardService;

	@Autowired
	private ServicePlanService				servicePlanService;

	public void autoACTDaypassPermitCard() {
		customerOrderPermitCardService.updateCustomerOrderPermitCardStatus();
	}

	public void autoDisposeDaypassCard() {
		servicePlanService.autoDisposeDaypassCard();
	}
}
