package com.sinodynamic.hkgta.scheduler.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class ServicePlanTaskService {
	private Logger		logger	= Logger.getLogger(ServicePlanTaskService.class);
	@Autowired
	ServicePlanService	servicePlanService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void autoDeactivateServicePlanAndDP() {
		try {
			servicePlanService.deactivateServicePlans();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("ServicePlanTask", e);
		}
	}
}
