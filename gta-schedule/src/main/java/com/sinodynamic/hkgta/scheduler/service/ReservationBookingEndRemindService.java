package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.pos.RestaurantCustomerBooking;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.pos.RestaurantCustomerBookingService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;

@Component
public class ReservationBookingEndRemindService {
	private Logger						logger	= Logger.getLogger(ReservationBookingEndRemindService.class);
	@Autowired
	RestaurantCustomerBookingService	restaurantCustomerBookingService;

	@Autowired
	private MessageTemplateService		messageTemplateService;

	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService			devicePushService;

	@Autowired
	private MemberService				memberService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void pushMessageNotification() {
		try {
			List<RestaurantCustomerBooking> bookings = restaurantCustomerBookingService.getRestaurantCustomerBookingConfirmedList();
			logger.info("RestaurantCustomerBooking getRestaurantCustomerBookingConfirmedList size:"+(null==bookings?0:bookings.size()));
			
			if (null != bookings && bookings.size() > 0) {
				for (RestaurantCustomerBooking booking : bookings) {
					long diff = booking.getBookTime().getTime() - new Date().getTime();
					long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
					int minutes = 60;
					if (diffInMinutes >= 59 && diffInMinutes <= 61) {
						if (null != booking.getCustomerProfile()) {
							Member member = memberService.getMemberById(booking.getCustomerProfile().getCustomerId());
							if (null != member) {
								MessageTemplate messageTemplate = messageTemplateService.getTemplateByFuncId(Constant.TEMPLATE_ID_RESTAURANT_BOOK_REMINDER);
								if (null != messageTemplate) {
									String message = messageTemplate.getContent().replace("{timeLeft}", minutes + "")
											.replace("{bookingTime}", DateCalcUtil.formatDatetime(booking.getBookTime()))
											.replace("{restaurantName}", booking.getRestaurantMaster().getRestaurantName())
											.replace("{partySize}", booking.getPartySize().toString());
									String subject = messageTemplate.getMessageSubject();
									logger.info("push message customerId"+member.getUserId());
									devicePushService.pushMessage(new String[] { member.getUserId() }, subject, message, "member");
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("ReservationBookingEndRemindTask", e);
		}
	}

}
