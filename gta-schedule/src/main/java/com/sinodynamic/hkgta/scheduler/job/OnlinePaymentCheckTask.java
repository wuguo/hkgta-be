package com.sinodynamic.hkgta.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.scheduler.service.OnlinePaymentCheckTaskService;

/**
 * <p>
 * Auto cancel those reservation which not paid successful in 15 mins.
 * </p>
 * 
 * @author Vicky Wang
 *
 */
public class OnlinePaymentCheckTask extends QuartzJobBean implements ApplicationContextAware {
	private final Logger	logger	= LoggerFactory.getLogger(OnlinePaymentCheckTask.class);

	WebApplicationContext	applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = (WebApplicationContext) applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		logger.info("OnlinePaymentCheckTask  run at:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));

		OnlinePaymentCheckTaskService onlinePaymentCheckTaskService = applicationContext.getBean(OnlinePaymentCheckTaskService.class);

		onlinePaymentCheckTaskService.checkOnlinePaymentStatus();

		logger.info("onlinePaymentCheckTaskService.checkOnlinePaymentStatus auto execute end ...");
	}

}
