package com.sinodynamic.hkgta.scheduler.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.crm.pub.NoticeService;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;

@Component
public class AutoInactiveExpiredNewsTaskService {

	private Logger			logger	= Logger.getLogger(AutoInactiveExpiredNewsTaskService.class);

	@Autowired
	private NoticeService	noticeService;

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void updateExpiredNewsToInactive() {
		logger.info("AutoInactiveExpiredNewsTaskService.updateExpiredNewsToInactive invocation start ...");
		try {
			noticeService.updateExpiredNewsToInactive();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("AutoInactiveExpiredNewsTask", e);
		}

		logger.info("AutoInactiveExpiredNewsTaskService.updateExpiredNewsToInactive invocation end ...");
	}
}
