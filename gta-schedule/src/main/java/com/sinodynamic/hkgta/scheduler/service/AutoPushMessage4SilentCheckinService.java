package com.sinodynamic.hkgta.scheduler.service;

import java.util.Date;
import java.util.List;

import org.springframework.util.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.service.sys.CheckServerService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;

@Component
public class AutoPushMessage4SilentCheckinService {
	private Logger				logger	= Logger.getLogger(AutoPushMessage4SilentCheckinService.class);

	@Autowired
	private GlobalParameterService	globalParameterService;

	
	@Autowired
	private RoomService roomService;
	

	@Autowired
	private AlarmEmailService	alarmEmailService;
	public void autoPushMsg4SilentCheckin() {
		try {	
			//get setting from global parameter
			List<GlobalParameter> list=globalParameterService.getGlobalParameterListByParamCat("SILENTCHKIN");
			if(list!=null&&list.size()>0){
				String pushMsg="";
				String pushTime="";
				for(GlobalParameter globalParameter : list){
					if(globalParameter.getParamId().endsWith("SILENTCHKIN-PUSHMSG")){
						pushMsg=globalParameter.getParamValue().trim();
					}else if(globalParameter.getParamId().endsWith("SILENTCHKIN-PUSHTIME")){
						pushTime=globalParameter.getParamValue().trim();
					}					
				}
				
				//if pushMsg or pushTime is null ,ignore this process
				if(!StringUtils.isEmpty(pushMsg)&&!StringUtils.isEmpty(pushTime)&&Constant.YES.endsWith(pushMsg)){
					Date date = new Date();
					String currentTime= DateConvertUtil.date2String(date, "HH:mm");
					int currentTimeA = Integer.parseInt(currentTime.replace(":", "").toString());
					int pushTimeByInt = Integer.parseInt(pushTime.replace(":", "").toString());
					int currentTimeB = Integer.parseInt(DateConvertUtil.date2String(DateConvertUtil.addDateMinut(date, 5), "HH:mm").replace(":", "").toString());
					//if pushMsg is  yes, and pushTime equals current time ,then process
					if(Constant.YES.equalsIgnoreCase(pushMsg) && currentTimeA <= pushTimeByInt && currentTimeB >= pushTimeByInt){
						roomService.autoPushMsg4SilentCheckin();
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("autoPushMsg4SilentCheckin", e);
		}
	}

}
