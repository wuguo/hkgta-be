package com.sinodynamic.hkgta.dao.fms;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilityType;

@Repository
public class FacilityTypeDaoImpl extends GenericDao<FacilityType>  implements FacilityTypeDao{

}
