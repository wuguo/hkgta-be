package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.mms.SpaRetreat;

public interface SpaRetreatDao extends IBaseDao<SpaRetreat>{

	public Serializable addSpaRetreat(SpaRetreat spaRetreat);
	public SpaRetreat getByRetId(Long retId);
	public List<SpaRetreat> getAllSpaRetreats();
	public Long getMaxDisplayOrder(String categoryId);
}
