package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;

public interface ServicePlanAdditionRuleDao extends  IBaseDao<ServicePlanAdditionRule>{
	
	public List getServicePlanAdditionRuleByPlanNo(Long planNo);
	
	public List<ServicePlanAdditionRule> getServicePlanAdditionRuleByCustomerId(Long customerId);
	
	public ServicePlanAdditionRule getByPlanNoAndRightCode(Long planNo, String rightCode);
	
}
