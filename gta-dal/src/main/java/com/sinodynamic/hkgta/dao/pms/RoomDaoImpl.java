package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.pms.RoomDto;
import com.sinodynamic.hkgta.entity.pms.Room;
@Repository
public class RoomDaoImpl extends GenericDao<Room> implements RoomDao {

	@Override
	public List<Room> getRoomList() {
		StringBuffer hql = new StringBuffer(
				"from Room order by roomNo");
		return super.getByHql(hql.toString(), null);
	}
	
	public List<Room> getLimitedInfoRoomList(){
		String hql = " select r.roomId as roomId, r.roomNo as roomNo from Room r order by r.roomNo ";
		return getDtoByHql(hql, null, Room.class);
	}

	//added Kaster 20160407
	@Override
	public RoomDto getRoomWithTaskCountByRoomId(Long roomId) {
		String hqlCount = new StringBuffer("SELECT a.room.roomId as roomId, COUNT(CASE WHEN a.jobType = 'ADH' THEN 1 ELSE NULL END) AS adHocCount, ")
				.append("  COUNT(CASE WHEN a.jobType = 'SCH' THEN 1 ELSE NULL END) AS scheduleCount, ")
				.append("  COUNT(CASE WHEN a.jobType = 'RUT' THEN 1 ELSE NULL END) AS routineCount, ")
				.append("  COUNT(CASE WHEN a.jobType = 'MTN' THEN 1 ELSE NULL END) AS maintenanceCount ")
				.append("  FROM RoomHousekeepTask a WHERE a.room.roomId = "+ roomId +" AND a.status != 'CAN' AND a.status != 'CMP' AND a.status != 'PND'  AND a.status != 'IRJ' ").toString();
				List<RoomDto> roomCountDtos = super.getDtoByHql(hqlCount, null, RoomDto.class);
				if(roomCountDtos!=null && roomCountDtos.size()>0){
					return roomCountDtos.get(0);
				}
		return null;
	}
	
	@Override
	public List<RoomDto> getRoomListWithTaskCounts() {
		//modified by Kaster 20160318 查多一个字段serviceStatus。
//		String hql = "SELECT r.roomId AS roomId, r.roomNo AS roomNo, r.status AS status, r.frontdeskStatus as frontdeskStatus, r.serviceStatus as serviceStatus FROM Room r ORDER BY r.roomNo";
		/***
		 * update hql to buidSql 
		 * by christ add roomStatus display DUE in 
		 * 2016-08-11 
		    The Due in must meet the following conditions:
			1. Staff pre-assign a room to member (roomId is not null)
			2. Member have not checked-in the room  (status <> CKI)
			3. The room start date is Today (arrival_date=currendDate)
		 */
		StringBuilder buidSql=new StringBuilder();
		buidSql.append(" SELECT r.room_id AS roomId ,r.room_no AS roomNo, ");
		/***
		 * delete status in Due in
		 */
//		buidSql.append(" CASE ");
//		buidSql.append(" WHEN r.room_id is not null and  rec.status<>'CKI' and ( r.status='VC' OR r.status='R' OR r.status='D') THEN 'Due In' ");
//		buidSql.append(" ELSE r.status ");
//		buidSql.append(" END  AS status,");
		buidSql.append(" r.status AS status,");
		buidSql.append(" r.frontdesk_status AS frontdeskStatus ,r.service_status AS serviceStatus "); 
		buidSql.append(" FROM room AS r ");
		buidSql.append(" LEFT JOIN room_reservation_rec AS rec ON r.room_id=rec.room_id AND rec.arrival_date=DATE_FORMAT(NOW(),'%Y-%m-%d') ");
		buidSql.append(" ORDER BY r.room_no ");
		
		List<RoomDto> roomDtos=super.getDtoBySql(buidSql.toString(), null, RoomDto.class);
//		List<RoomDto> roomDtos = super.getDtoByHql(hql, null, RoomDto.class);
		if(null != roomDtos && roomDtos.size() >0){
			String hqlCount = new StringBuffer("SELECT a.room.roomId as roomId, COUNT(CASE WHEN a.jobType = 'ADH' THEN 1 ELSE NULL END) AS adHocCount, ")
			.append("  COUNT(CASE WHEN a.jobType = 'SCH' THEN 1 ELSE NULL END) AS scheduleCount, ")
			.append("  COUNT(CASE WHEN a.jobType = 'RUT' THEN 1 ELSE NULL END) AS routineCount, ")
			.append("  COUNT(CASE WHEN a.jobType = 'MTN' THEN 1 ELSE NULL END) AS maintenanceCount ")
			.append("  FROM RoomHousekeepTask a WHERE a.status != 'CAN' AND a.status != 'CMP' AND a.status != 'PND'  AND a.status != 'IRJ' ")
			.append("  GROUP BY a.room.roomId) t ").toString();
			List<RoomDto> roomCountDtos = super.getDtoByHql(hqlCount, null, RoomDto.class);
			if(null != roomCountDtos && roomCountDtos.size() >0){
				for(RoomDto roomDto : roomDtos){
					for(RoomDto roomCountDto : roomCountDtos){
						if(roomDto.getRoomId().equals(roomCountDto.getRoomId())){
							roomDto.setAdHocCount(roomCountDto.getAdHocCount());
							roomDto.setScheduleCount(roomCountDto.getScheduleCount());
							roomDto.setRoutineCount(roomCountDto.getRoutineCount());
							roomDto.setMaintenanceCount(roomCountDto.getMaintenanceCount());
							break;
						}
					}
				}
			}
		}
		return roomDtos;
		/*String sql = new StringBuffer("SELECT r.room_id AS roomId, r.room_no AS roomNo, r.status AS status,r.frontdesk_status as frontdeskStatus, t.adHocCount, t.scheduleCount, t.routineCount, t.maintenanceCount ")
		.append(" FROM room r LEFT JOIN ")
		.append(" (SELECT a.room_id, COUNT(CASE WHEN a.job_type = 'ADH' THEN 1 ELSE NULL END) AS adHocCount, ")
		.append("   COUNT(CASE WHEN a.job_type = 'SCH' THEN 1 ELSE NULL END) AS scheduleCount, ")
		.append("   COUNT(CASE WHEN a.job_type = 'RUT' THEN 1 ELSE NULL END) AS routineCount, ")
		.append("   COUNT(CASE WHEN a.job_type = 'MTN' THEN 1 ELSE NULL END) AS maintenanceCount ")
		.append("   FROM room_housekeep_task a WHERE a.status != 'CAN' AND a.status != 'CMP' AND a.status != 'PND' ")
		.append("   GROUP BY a.room_id) t ")
		.append(" ON r.room_id = t.room_id ORDER BY r.room_no").toString();
		return super.getDtoBySql(sql.toString(), null, RoomDto.class);*/
	}

	@Override
	public List<Long> getNoResponseRoomIdx(int raRespTime, Date reqDate) {
		/*String sql = new StringBuffer("SELECT DISTINCT t.room_id AS roomId FROM room_housekeep_task t ")
				.append(" WHERE t.start_timestamp IS NULL AND t.status = 'OPN' AND ( ")
				.append("  (DATE_ADD(t.schedule_datetime, INTERVAL ? MINUTE) < ? AND t.job_type = 'ADH') ")
				.append("  OR (TO_DAYS(t.target_complete_datetime) < TO_DAYS(?) AND t.job_type = 'SCH') )").toString();*/
		
		String hql = new StringBuffer("SELECT DISTINCT t.room.roomId AS roomId FROM RoomHousekeepTask t ")
		.append(" WHERE t.startTimestamp IS NULL AND t.status = 'OPN' AND ( ")
		.append("  (t.scheduleDatetime < ? AND t.jobType = 'ADH') ")
		.append("  OR (DATE(t.targetCompleteDatetime) < DATE(?) AND t.jobType = 'SCH') )").toString();
		
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(getCalendar(reqDate,raRespTime));
		paramList.add(reqDate);
		List<RoomDto> noRespRooms = super.getDtoByHql(hql.toString(), paramList, RoomDto.class);
		List<Long> retList = new ArrayList<Long>(noRespRooms.size());
		for (RoomDto r : noRespRooms) {
			retList.add(r.getRoomId());
		}
		return retList;
	}
	
	private Date getCalendar(Date date,int subtractMinute){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, -subtractMinute);
		return cal.getTime();
	}

	@Override
	public List<Room> getRoomListByStatus(String[] status) throws Exception {
		StringBuffer hqlStr = new StringBuffer(
				"from Room where status in ( ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		for(int i=0; i<status.length ; i++){
			hqlStr.append("?,");
			paramList.add(status[i]);
		}
		String hql = hqlStr.substring(0, hqlStr.length()-1)+")";
		return super.getByHql(hql, paramList);
	}
	
	@Override
	public List<Room> getRoomListOutOfStatus(String[] status) throws Exception {
		StringBuffer hqlStr = new StringBuffer(
				"from Room where status not in ( ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		for(int i=0; i<status.length ; i++){
			hqlStr.append("?,");
			paramList.add(status[i]);
		}
		String hql = hqlStr.substring(0, hqlStr.length()-1)+")";
		return super.getByHql(hql, paramList);
	}

	@Override
	public List<RoomDto> getRoomCrewList(String crewRoleId)
	{
		StringBuffer hql = new StringBuffer();
		/*sql.append(" select r.room_id as roomId, r.room_no as roomNo,r.status, ")
			.append(" (select count(rc.crew_id) roomCrewCount from room_crew rc where r.room_id =rc.room_id and rc.crew_role_id = ? ) roomCrewCount ")
			.append(" from room r ");*/
		
		hql.append(" select r.roomId as roomId, r.roomNo as roomNo,r.status as status from Room r ");
		List<RoomDto> roomDtos = super.getDtoByHql(hql.toString(), null,RoomDto.class);
		
		String hqlCount = "select rc.room.roomId as roomId, count(rc.crewId) as roomCrewCount from RoomCrew rc where rc.crewRoleId = ? group by rc.room.roomId";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(crewRoleId);
		List<RoomDto> roomCountDtos = super.getDtoByHql(hqlCount, paramList, RoomDto.class);
		if(null != roomCountDtos && roomCountDtos.size() >0){
			for(RoomDto roomDto : roomDtos){
				for(RoomDto roomCountDto : roomCountDtos){
					if(roomDto.getRoomId().equals(roomCountDto.getRoomId())){
						roomDto.setRoomCrewCount(roomCountDto.getRoomCrewCount());
						break;
					}
				}
			}
		}
		return roomDtos;
	}

	@Override
	public List<Room> getMaintenanceRoomList() {
		StringBuffer sb = new StringBuffer();
		sb.append("from Room where roomId in(select room.roomId from RoomHousekeepTask where jobType='MTN' and status in('OPN') GROUP BY room.roomId)");
		return super.getByHql(sb.toString());
	}

	@Override
	public List<Room> getMaintenanceMyRoomList()
	{
		String hql = "from Room where roomId in(select room.roomId from RoomHousekeepTask where status in('OPN','RTI') GROUP BY room.roomId)";
		return super.getByHql(hql);
	}
	
	public Room getByRoomId(Long roomId){
		String hql = "from Room r where r.roomId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(roomId);
		return (Room) getUniqueByHql(hql, param);
	}

	@Override
	public Room getByRoomNo(String roomNo)
	{
		String hql = "from Room r where r.roomNo = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(roomNo);
		return (Room) getUniqueByHql(hql, param);
	}

	@Override
	public List<Room> getCheckInRoomList() {
		StringBuffer hql = new StringBuffer(
				"from Room m where m.roomId in (select rec.roomId from RoomReservationRec rec where rec.checkinTimestamp <= ? and rec.checkoutTimestamp is null)");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(new Date());
		return super.getByHql(hql.toString(), param);
	}

}
