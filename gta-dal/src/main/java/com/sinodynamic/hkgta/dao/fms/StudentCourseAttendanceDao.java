package com.sinodynamic.hkgta.dao.fms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.StudentCourseAttendance;

/**
 * @author Vineela_Jyothi
 *
 */
public interface StudentCourseAttendanceDao extends
		IBaseDao<StudentCourseAttendance> {
	public String getAttendIdBySessionIdAndEnrollid(String sessionId, String enrollId) ;
}
