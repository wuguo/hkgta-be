package com.sinodynamic.hkgta.dao.onlinepayment;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmd;

@Repository
public class PaymentGatewayCmdDaoImpl extends GenericDao<PaymentGatewayCmd> implements
		PaymentGatewayCmdDao {


}
