package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;


@Repository
public class LeadCustomerDaoImpl extends GenericDao<CustomerProfile> implements LeadCustomerDao {
	private Logger logger = Logger.getLogger(LeadCustomerDaoImpl.class);

	public CustomerProfile getByCustomerId(Long customerId) {

		return (CustomerProfile) this.get(CustomerProfile.class, customerId);
	}
	
	/**
	 * Save PotentialCustomerProfile
	 */
	public Serializable savePotentialCustomerProfile(CustomerProfile customerProfile) {
		return save(customerProfile);
	}
	
	/**
	 * Save CustomerPreEnrollStage
	 */
	public void saveCustomerPreEnrollStage(CustomerPreEnrollStage customerPreEnrollStage) {
		//TODO  fix
		//save(customerPreEnrollStage);
	}
	/**
	 * Save Potential Customer Enrollment
	 */
	public void savePotentialCustomerEnrollment(
			CustomerEnrollment customerEnrollment) {
		//TODO fix
		//save(customerEnrollment);
		
	}
	/**
	 * Update PotentialCustomerProfile
	 */
	public void updatePotentialCustomerProfile(CustomerProfile customerProfile){
		this.update(customerProfile);
	}
	
	public void deleteLeadCustomerProfile(CustomerProfile customerProfile){
		String sql=" update CustomerProfile t set t.isDeleted = ? where t.customerId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add("Y");
		param.add(customerProfile.getCustomerId());
		int results=hqlUpdate(sql, param);
//	    delete(customerProfile);
	}


}