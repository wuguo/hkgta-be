package com.sinodynamic.hkgta.dao.mms;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.mms.GuestRequestDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.mms.SpaMemberSync;

public interface SpaMemberSyncDao extends IBaseDao<SpaMemberSync> {
    
    public List<SpaMemberSync> getSpecificSpaMemberSync(Long customerId, String status);
    
    public List<GuestRequestDto> getMemberInfoList2Synchronize(String status, String academyNos);
    
    public void addSpaMemberSyncWhenInsert(Member member);
    
    public void addSpaMemberSyncWhenUpdate(CustomerProfile oldThis, CustomerProfile newThis);
    
    public void addSpaMemberSyncWhenLinkCard(String customerId);

    /**
     * 批量插入执行 sql
     * @param sql
     * @return
     * @throws HibernateException
     */
    public int bulkInsertBySql(String sql) throws HibernateException;

    /**
     * 根据id删除同步记录
     * @param ids
     * @return
     */
	public int deleteMmsSync(String ids);

	public List<SpaMemberSync>getAllSpaMemberSynchThenGuestIdIsNotNull();
}
