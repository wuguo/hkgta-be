package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.CommonDataQueryBySQLDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DailyEnrollmentDto;
import com.sinodynamic.hkgta.dto.crm.DailyLeadsDto;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository("customerEnrollment")
public class CustomerEnrollmentDaoImpl  extends GenericDao<CustomerEnrollment> implements CustomerEnrollmentDao, AdvanceQueryConditionDao{
	
	
	 private Logger logger = Logger.getLogger(CustomerEnrollmentDaoImpl.class);
	 @Autowired
	 private CommonDataQueryBySQLDao	commonBySQLDao;
	   
	 /**
	  * Fetch all Customer Enrollments
	  * @return
	  * @throws HiberateException
	  */
	public ListPage<CustomerEnrollment> selectAllCustomerEnrollments(ListPage<CustomerEnrollment> page) throws HibernateException {
		
		logger.info("CustomerEnrollmentDaoImpl.getAllCustomerEnrollments invocation start ...");
		String sqlState = "from CustomerEnrollment";
		String sqlCount = "select count(*) from CustomerEnrollment";
		
		return listByHql(page, sqlCount, sqlState, null);
	}
	
	
	/**
	 * This function is used for selecting customer enrollment by staff name
	 * @return
	 * @throws HibernateException
	 */
	public ListPage<CustomerEnrollment> selectCustomerEnrollmentInfoByStaffName(ListPage<CustomerEnrollment> page, String name) throws HibernateException {
		
		logger.info("CustomerEnrollmentDaoImpl.selectCustomerEnrollmentInfoByStaffName invocation start ...");
		String sqlState = "from CustomerEnrollment where salesFollowBy = ?";
		String sqlCount = "select count(*) from CustomerEnrollment where salesFollowBy = ?";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(name);
		return listByHql(page, sqlCount, sqlState, param);
	}
	
	
	/**
	 * This function is used for selecting customer enrollment by customer name
	 * @return
	 * @throws HibernateException
	 */
	public ListPage<CustomerEnrollment> selectCustomerEnrollmentInfoByCustomerName(ListPage<CustomerEnrollment> page, String name) throws HibernateException {
		
		logger.info("CustomerEnrollmentDaoImpl.selectCustomerEnrollmentInfoByCustomerName invocation start ...");
		String sqlState = "from CustomerEnrollment t where t.customerProfile.givenName = ?";
		String sqlCount = "select count(*) from CustomerEnrollment t where t.customerProfile.givenName = ?";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(name);
		return listByHql(page, sqlCount, sqlState, param);
	}
	
	
	/**
	 * This function is used for checking the selected lucky number for academy Id is available or not and set it.
	 * The range of lucky number is 8000000-9999999.
	 * @return true or false
	 * @throws HibernateException
	 */
	@Deprecated
	@SuppressWarnings("unchecked")
	public boolean checkAvailableAcademyID(String academyNo) throws HibernateException{
		logger.info("CustomerEnrollmentDao.checkAvailableAcademyID invocation start ...");
		String sql=" from Member where academyNo= ?" ;

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(academyNo);
		//TODO: fix move to CustomerEnrollmentDao
		//List<Member> members = (List<Member>);
		if (getByHql(sql, param) != null && getByHql(sql, param).size() >0 ){
			return false;
		}
		
		return true;
	
	}

	@SuppressWarnings("unchecked")
	public boolean setAcademyID(String academyNo,Long customerId) throws HibernateException{
		logger.info("CustomerEnrollmentDao.setAcademyID invocation start ...");
		String sql=" update Member set academyNo = ? where customerId=?" ;

		List<Serializable> param = new ArrayList<Serializable>();
		param.add(academyNo);
		param.add(customerId);
		int results=hqlUpdate(sql, param);
		if (results > 0) {
			return true;
		}else {
			return false;
		}
	
	}

	
	/**
	 * This function is used for checking the enrollment status, which is new or not.
	 * @return "0" represents success, "1" represents status is not NEW,"2" represents no enrollment
	 * @throws HibernateException
	 *
	 */

	@SuppressWarnings("unchecked")
	public String  checkEnrollmentStatus(Long enrollId)throws HibernateException {

		logger.info("CustomerEnrollmentDao.checkEnrollmentStatus invocation start ...");
		String sql=" from CustomerEnrollment c where c.enrollId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(enrollId);
		List<CustomerEnrollment> enrollments=(List<CustomerEnrollment>)getByHql(sql, param);
		if (enrollments != null && enrollments.size() >0 ){
			String status = enrollments.get(0).getStatus();
			if(status.equals("NEW")){
				return "0";
			} else return "1";
		}
		return "2";
	}
	
	
	
	/**
	 * This function is used for only deleting enrollments which status is "new".
	 * @return true or false
	 * @throws HibernateException
	 */
	public boolean deleteEnrollmentsWithoutPayment(Long enrollId)throws HibernateException{
		logger.info("CustomerEnrollmentDao.deleteEnrollmentsWithoutPayment invocation start ...");
		
		String sql=" update CustomerEnrollment t set t.status = ? where enrollId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(EnrollStatus.OPN.name());
		param.add(enrollId);
		int results=hqlUpdate(sql, param);
		if (results>0) {
			return true;
		}else {
			return false;
		}
	}
	

	
	/**
	 * This function is used for viewing all enrollment records with status and all activation history of member (include all salesman).
	 * @return true or false
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public ListPage<CustomerEnrollment> viewEnrollmentActivationOfMember(ListPage<CustomerEnrollment> page,Serializable[] statusList)throws HibernateException {
		logger.info("CustomerEnrollmentDao.viewActivatedMember invocation start ...");
		String sql=" from CustomerEnrollment where " ;
		String countHql = " select count(*) from CustomerEnrollment where ";
		List<Serializable> param = new ArrayList<Serializable>();
		
		for (int i = 0; i < statusList.length; i++) {
			sql = sql + " status = ? or";
			countHql = countHql + " status = ? or";
			param.add(statusList[i]);
		}
		
		String sqlStr = sql.substring(0, sql.length() - 3);
		countHql = countHql.substring(0, sql.length() - 3);
		return  (ListPage<CustomerEnrollment>)listByHql(page,countHql,sqlStr, null);
	}
	
	
	/**
	 * This function is used for selecting the customer enrollment by enrollId
	 * 
	 * @param enrolId
	 * @return Customer Enrollment Object
	 */
	public CustomerEnrollment selectCustomerEnrollmentByCustomerId(
			ListPage<CustomerEnrollment> page, String customerId) {

		logger.info("CustomerEnrollmentDao.selectCustomerEnrollmentByCustomerIdFromCustomerEnrollment invocation start ...");
		String countHql = " select count(*) from CustomerEnrollment ";
		String hql = "from CustomerEnrollment t where t.customerProfile.customerId = "
				+ customerId;
		listByHql(page, countHql, hql, null);
		CustomerEnrollment customerEnrollment = page.getList().get(0);
		return customerEnrollment;

	}

	
	/**
	 * This function is used from updating the customer enrollment
	 * 
	 * @param customerEnrollment
	 * @return
	 */
	public Serializable updateCustomerEnrollment(
			CustomerEnrollment customerEnrollment) {
		logger.info("CustomerEnrollmentDao.updateCustomerEnrollment invocation start ...");
		if (customerEnrollment == null)
			throw new HibernateException("Customer enrollment is null!");

		return saveOrUpdate(customerEnrollment);

	}


	public void addCustomerEnrollment(CustomerEnrollment ce) {
		this.save(ce);
		
	}


	/**
	 * This function is used for selecting the customer enrollment by enrollId
	 * 
	 * @param enrolId
	 * @return Customer Enrollment Object
	 */
	public CustomerEnrollment getCustomerEnrollmentByCustomerId(Long customerId) {

		logger.info("CustomerEnrollmentDao.selectCustomerEnrollmentByCustomerIdFromCustomerEnrollment invocation start ...");
	
		String hql = "from CustomerEnrollment t where t.customerId = "
				+ customerId;
		
		List<CustomerEnrollment> customerEnrollmentList = super.excuteByHql(CustomerEnrollment.class, hql);
		
		if(null != customerEnrollmentList && customerEnrollmentList.size() > 0){
			return customerEnrollmentList.get(0);
		}
		return null;

	}
	
	public CustomerEnrollment getCustomerEnrollmentByEnrollId(Long enrollId) {

		logger.info("CustomerEnrollmentDao.selectCustomerEnrollmentByCustomerIdFromCustomerEnrollment invocation start ...");
	
		String hql = "from CustomerEnrollment t where t.enrollId = "
				+ enrollId;
		return (CustomerEnrollment) getUniqueByHql(hql);

	}
	
	/**
	 * Delete the lead customer enrollment!
	 * @param customerEnrollment
	 */
	public void deleteLeadCustomerEnrollment(CustomerEnrollment customerEnrollment){
		delete(customerEnrollment);
	}
	
	/*
	 * Update enrollment status to be complete
	 * @param enrollId
	*/
	public int updateEnrollmentStatusToComplete(Long enrollId, String status,String userId){
		List<Serializable> param = new ArrayList<Serializable>();
		String sql = "update CustomerEnrollment set status = ? , updateBy = ?,updateDate = ? where enrollId = ?" ;
		param.add(status);
		param.add(userId);
		param.add(new Date());
		param.add(enrollId);
		int i = this.hqlUpdate(sql, param);
		return i;
	}



	/**
	 * @author Li_Chen
	 * Date : 2015-5-7
	 * description: search 20 latest enrollment activities
	 */
	@Override
	public List getLatestEnrollmentActivaties(int total) {
		String sql = "SELECT coalesce(ce.update_date, ce.create_date) update_date,ce.`status` STATUS,CONCAT_WS(' ',cp.salutation,cp.given_name,cp.surname) username"
				+ " FROM  customer_profile cp INNER JOIN customer_enrollment ce"
				+ " ON cp.customer_id = ce.customer_id AND ce.`status` in ('NEW','APV','ANC','CMP','OPN','PYA','TOA') ORDER BY update_date DESC LIMIT 0,"+total;
		return getCurrentSession().createSQLQuery(sql).list();
	}
	
	public boolean updateStatus(String status, Long customerId){
		String hqlstr=" update CustomerEnrollment ce set ce.status = ? where ce.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		param.add(customerId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}
	
	public boolean updateStatus(String status, Long customerId, String loginUserId){
		String hqlstr=" update CustomerEnrollment ce set ce.status = ?, ce.updateBy = ?, ce.updateDate = ? where ce.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		param.add(loginUserId);
		param.add(new Date());
		param.add(customerId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}
	
	public boolean updateServicePlanNo(Long planNo, Long contactLength, Long customerId, String loginUserId){
		String hqlstr=" update CustomerEnrollment ce set ce.subscribePlanNo = ?, ce.subscribeContractLength = ?, ce.updateBy = ?, ce.updateDate = ? where ce.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planNo);
		param.add(contactLength);
		param.add(loginUserId);
		param.add(new Date());
		param.add(customerId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}
	
	/**
	 * @author Shuyuan Zhu
	 * Date : 2015-8-25
	 * description: Get the enrollment member list by the status
	 */
	public List<CustomerEnrollment> getEnrollmentMembersByStatus(String status){
		String sql = "SELECT ce.customer_id as customerId from customer_enrollment ce where ce.status =?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		return getDtoBySql(sql, param, CustomerEnrollment.class);
	}


	@Override
	public ListPage<CustomerEnrollment> getDailyEnrollment(ListPage page, String enrollDate) {
		String sql = "SELECT\n" +
				"	enroll.*, sc1.code_display AS businessNature\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			DATE_FORMAT(ce.enroll_date, '%Y/%m/%d') AS enrollDate,\n" +
				"			sp.plan_name AS planName,\n" +
				"			m.academy_no AS academyId,\n" +
				"			ce.enroll_type AS enrollType,\n" +
				"			CONCAT(\n" +
				"				cp.given_name,\n" +
				"				' ',\n" +
				"				cp.surname\n" +
				"			) AS patronName,\n" +
				"			sc2.code_display AS nationallity,\n" +
				"			cp.business_nature AS businessNatureDB,\n" +
				"			cp.company_name AS company,\n" +
				"			cp.position_title AS jobTitle,\n" +
				"			CONCAT(\n" +
				"				sp2.given_name,\n" +
				"				' ',\n" +
				"				sp2.surname\n" +
				"			) AS accountManager,\n" +
				"			psip.item_price AS servicePlanFee,\n" +
				"			1 AS qty,\n" +
				"			CONCAT(\n" +
				"				sp1.given_name,\n" +
				"				' ',\n" +
				"				sp1.surname\n" +
				"			) AS createBy,\n" +
				"			DATE_FORMAT(\n" +
				"				ce.create_date,\n" +
				"				'%Y/%m/%d %H:%i:%s'\n" +
				"			) createDate,\n" +
				"			ce.`status` AS enrollStatus\n" +
				"		FROM\n" +
				"			customer_enrollment ce,\n" +
				"			member m,\n" +
				"			customer_profile cp,\n" +
				"			service_plan sp,\n" +
				"			service_plan_pos spp,\n" +
				"			pos_service_item_price psip,\n" +
				"			staff_profile sp1,\n" +
				"			staff_profile sp2,\n" +
				"			sys_code sc2\n" +
				"		WHERE\n" +
				"			ce.customer_id = m.customer_id\n" +
				"		AND m.customer_id = cp.customer_id\n" +
				"		AND ce.subscribe_plan_no = sp.plan_no\n" +
				"		AND sp.plan_no = spp.plan_no\n" +
				"		AND spp.pos_item_no = psip.item_no\n" +
				"		AND ce.create_by = sp1.user_id\n" +
				"		AND ce.sales_follow_by = sp2.user_id\n" +
				"		AND sc2.code_value = cp.nationality\n" +
				"		AND sc2.category = 'nationality'\n" +
				"		AND ce.enroll_date = ? \n" +
				"	) enroll\n" +
				"LEFT JOIN sys_code sc1 ON sc1.code_value = enroll.businessNatureDB\n" +
				"AND sc1.category = 'businessNature'";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(enrollDate);
		String countSql = " SELECT count(1) from ( " +sql+") x";
		return listBySqlDto(page, countSql, sql, param, new DailyEnrollmentDto());
	}


	@Override
	public ListPage<CustomerEnrollment> getDailyLeads(ListPage page, String selectedDate) {
		String sql = "SELECT\n" +
				"	CONCAT_WS(\n" +
				"		' ',\n" +
				"		cp.given_name,\n" +
				"		cp.surname\n" +
				"	) AS patronName,\n" +
				"	(\n" +
				"		SELECT\n" +
				"			CONCAT_WS(\n" +
				"				' ',\n" +
				"				staffProfileAccount.given_name,\n" +
				"				staffProfileAccount.surname\n" +
				"			)\n" +
				"		FROM\n" +
				"			staff_profile staffProfileAccount\n" +
				"		WHERE\n" +
				"			staffProfileAccount.user_id = enroll.sales_follow_by\n" +
				"	) AS accountManager,\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sysClass.code_display\n" +
				"		FROM\n" +
				"			sys_code sysClass\n" +
				"		WHERE\n" +
				"			sysClass.code_value = cp.contact_class_code\n" +
				"		AND sysClass.category = 'contactClassCode'\n" +
				"	) AS classification,\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sysNation.code_display\n" +
				"		FROM\n" +
				"			sys_code sysNation\n" +
				"		WHERE\n" +
				"			sysNation.code_value = cp.nationality\n" +
				"		AND sysNation.category = 'nationality'\n" +
				"	) AS nationality,\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sysBusiness.code_display\n" +
				"		FROM\n" +
				"			sys_code sysBusiness\n" +
				"		WHERE\n" +
				"			sysBusiness.code_value = cp.business_nature\n" +
				"		AND sysBusiness.category = 'businessNature'\n" +
				"	) AS businessNature,\n" +
				"	(\n" +
				"		SELECT\n" +
				"			info.customer_input\n" +
				"		FROM\n" +
				"			customer_addition_info info\n" +
				"		WHERE\n" +
				"			info.customer_id = cp.customer_id\n" +
				"		AND info.caption_id = '14'\n" +
				"	) AS company,\n" +
				"	(\n" +
				"		SELECT\n" +
				"			info.customer_input\n" +
				"		FROM\n" +
				"			customer_addition_info info\n" +
				"		WHERE\n" +
				"			info.customer_id = cp.customer_id\n" +
				"		AND info.caption_id = '1'\n" +
				"	) AS jobTitle,\n" +
				"	1 AS qty,\n" +
				"	(\n" +
				"		SELECT\n" +
				"			CONCAT_WS(\n" +
				"				' ',\n" +
				"				staffProfileCreateBy.given_name,\n" +
				"				staffProfileCreateBy.surname\n" +
				"			)\n" +
				"		FROM\n" +
				"			staff_profile staffProfileCreateBy\n" +
				"		WHERE\n" +
				"			staffProfileCreateBy.user_id = cp.create_by\n" +
				"	) AS createBy,\n" +
				"	DATE_FORMAT(\n" +
				"		cp.create_date,\n" +
				"		'%Y/%m/%d %H:%i:%s'\n" +
				"	) AS createDate,\n" +
				"	'Open' AS enrollStatus\n" +
				"FROM\n" +
				"	customer_enrollment enroll,\n" +
				"	customer_profile cp\n" +
				"LEFT JOIN member m ON m.customer_id = cp.customer_id\n" +
				"WHERE\n" +
				"	cp.customer_id = enroll.customer_id\n" +
				"AND enroll. STATUS = 'OPN'\n" +
				"AND m.member_type IS NULL\n" +
				"AND date_format( ? , '%Y-%m-%d') = date_format(cp.create_date, '%Y-%m-%d')";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(selectedDate);
		String countSql = " SELECT count(1) from ( " +sql+") x";
		return listBySqlDto(page, countSql, sql, param, new DailyLeadsDto());
	}
	
	/**
	 * 获取CustomerTransfor列表语句
	 * @return
	 */
	@Override
	public String getCustomerTransforList() {
		String sql = "SELECT  "
		+ " custe.enroll_date enrollDateDB,"
		+ " custe.create_date enrollCreateDate,"
		+ " custe.enroll_id as enrollId,"
		+ " concat( custp.salutation, ' ', custp.given_name, ' ', custp.surname ) AS memberName, "
		+ " custe.`status`,"
		+ " concat( stapr.given_name, ' ', stapr.surname ) AS salesFollowBy, "
		+ " custe.customer_id customerId,"
		+ " custp.contact_email contactEmail,"
		+ " mb.member_type AS memberType,"
		+ " mb.academy_no AS academyNo"
		+ " FROM customer_enrollment custe "
		+ " LEFT JOIN staff_profile stapr ON custe.sales_follow_by = stapr.user_id "
		+ " LEFT JOIN customer_profile custp ON custe.customer_id = custp.customer_id "
		+ " LEFT JOIN member mb ON custp.customer_id = mb.customer_id WHERE mb.member_type NOT IN ('HG', 'MG')";   
		String joinSql = "SELECT * FROM (" + sql + " ) sub WHERE 1=1";
		return joinSql;
	}
	
	/**
	 * 批量更新销售人员
	 * @param enrollIds
	 * @param salepersonId
	 * @param userId
	 * @return
	 */
	@Override
	public int updateEnrollmentStatusToComplete(String enrollIds, String salepersonId, String userId, AdvanceQueryDto advanceQueryDto){
		List<Serializable> param = new ArrayList<Serializable>();
		String sql = "update customer_enrollment set sales_follow_by = ? , update_by = ?, update_date = ?  " ;
		if (!enrollIds.isEmpty()) {
			sql = sql + " where enroll_id IN ( " + enrollIds + " )" ;
		} else if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null
				&& advanceQueryDto.getGroupOp().length() > 0) {
			String sqls = "SELECT  "
					+ " custe.enroll_date enrollDateDB,"
					+ " custe.create_date enrollCreateDate,"
					+ " custe.enroll_id as enrollId,"
					+ " concat( custp.salutation, ' ', custp.given_name, ' ', custp.surname ) AS memberName, "
					+ " custe.`status`,"
					+ " concat( stapr.given_name, ' ', stapr.surname ) AS salesFollowBy, "
					+ " custe.customer_id customerId,"
					+ " custp.contact_email contactEmail,"
					+ " mb.member_type AS memberType,"
					+ " mb.academy_no AS academyNo"
					+ " FROM customer_enrollment custe "
					+ " LEFT JOIN staff_profile stapr ON custe.sales_follow_by = stapr.user_id "
					+ " LEFT JOIN customer_profile custp ON custe.customer_id = custp.customer_id "
					+ " LEFT JOIN member mb ON custp.customer_id = mb.customer_id WHERE mb.member_type NOT IN ('HG', 'MG')";   
					String joinSql = "SELECT enrollId FROM (" + sqls + " ) sub WHERE 1=1 AND ";
					String searchCondition = commonBySQLDao.getSearchCondition(advanceQueryDto, joinSql);
					sql = sql + " where enroll_id IN ( " + searchCondition + " )" ;
		}
		System.out.println(sql);
		param.add(salepersonId);
		param.add(userId);
		param.add(new Date());
		int i = this.sqlUpdate(sql, param);
		return i;
	}
	
	/**
	 * 高级查询条件
	 */
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
			final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Acdemy ID", "academyNo", "java.lang.String", "", 1);
			final AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Customer", "memberName", "java.lang.String", "", 2);
			final List<SysCode> memberType=new ArrayList<>();
			SysCode s1=new SysCode();
			s1.setCategory("permitCardmemberType");
			s1.setCodeDisplay("Corporate Dependent Member");
			s1.setCodeValue("CDM");
			memberType.add(s1);

			SysCode s2=new SysCode();
			s2.setCategory("permitCardmemberType");
			s2.setCodeDisplay("Corporate Primary Member");
			s2.setCodeValue("CPM");
			memberType.add(s2);
			
			SysCode s3=new SysCode();
			s3.setCategory("permitCardmemberType");
			s3.setCodeDisplay("Individual Primary Member");
			s3.setCodeValue("IPM");
			memberType.add(s3);
			
			SysCode s4=new SysCode();
			s4.setCategory("permitCardmemberType");
			s4.setCodeDisplay("Individual Dependent Member");
			s4.setCodeValue("IDM");
			memberType.add(s4);
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Patron Type","memberType","java.lang.String",new ArrayList<SysCode>(memberType),3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Sales Person", "salesFollowBy", "java.lang.String", "", 4);
			return Arrays.asList(condition1, condition2, condition3, condition4);
	}


	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		// TODO Auto-generated method stub
		return null;
	}

}