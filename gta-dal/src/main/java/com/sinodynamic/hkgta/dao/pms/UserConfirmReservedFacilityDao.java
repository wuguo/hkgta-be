package com.sinodynamic.hkgta.dao.pms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.UserConfirmReservedFacility;

public interface UserConfirmReservedFacilityDao extends IBaseDao<UserConfirmReservedFacility> {

}
