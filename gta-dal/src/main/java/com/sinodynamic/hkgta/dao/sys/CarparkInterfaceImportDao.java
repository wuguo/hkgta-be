package com.sinodynamic.hkgta.dao.sys;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceImport;

public interface CarparkInterfaceImportDao extends IBaseDao<CarparkInterfaceImport> 
{

}
