package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanPos;

public interface ServicePlanPosDao extends IBaseDao<ServicePlanPos> {
	public List<ServicePlanPos> getByPlanNo(Integer planNo);
}
