package com.sinodynamic.hkgta.dao.fms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;


@Repository("spaRetreatItemConditions")
public class SpaRetreatItemConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
	    	//String displayName, String columnName, String columnType, String selectName,Integer displayOrder
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Item Name", "itemName", "java.lang.String", "", 1);
		
//		final List<SysCode> status=new ArrayList<>();
//		SysCode s1=new SysCode();
//		s1.setCategory("Status");
//		s1.setCodeDisplay("Show");
//		s1.setCodeValue("Show");
//		status.add(s1);
//		
//		SysCode s2=new SysCode();
//		s2.setCategory("Status");
//		s2.setCodeDisplay("Hidden");
//		s2.setCodeValue("Hidden");
//		status.add(s2);
//		
//		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Status", "status", "java.lang.String", status, 2);
		
		return Arrays.asList(condition1);
	}

	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
	    
	    return null;
	}
	
}
