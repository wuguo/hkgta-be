package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
@Repository
public class AgeRangeDaoImpl extends GenericDao<AgeRange> implements AgeRangeDao {

}
