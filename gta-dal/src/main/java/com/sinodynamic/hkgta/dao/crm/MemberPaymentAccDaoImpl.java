package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
@Repository
public class MemberPaymentAccDaoImpl extends GenericDao<MemberPaymentAcc> implements MemberPaymentAccDao {

	public Serializable addMemberPaymentAcc(MemberPaymentAcc memberPaymentAcc) {
		return save(memberPaymentAcc);
	}

	@Override
	public MemberPaymentAcc getMemberPaymentAccByAccountNo(String accNo) {
		
		String hql = "from MemberPaymentAcc where accountNo = '" + accNo + "'";
		List<MemberPaymentAcc> list = getByHql(hql);
		if (list != null && list.size() > 0) return list.get(0); 
		return null;
	}

	@Override
	public MemberPaymentAcc getMemberPaymentAccByCustomerId(Long customerId) {

		String hql = "from MemberPaymentAcc where customerId = " + customerId;
		List<MemberPaymentAcc> list = getByHql(hql);
		if (list != null && list.size() > 0) return list.get(0); 
		return null;
	}

	@Override
	public MemberPaymentAcc getMemberPaymentAccByCustomerId(Long customerId, String accType) {
		String hql = "from MemberPaymentAcc where customerId = " + customerId +" and accType='"+accType+"'";
		List<MemberPaymentAcc> list = getByHql(hql);
		if (list != null && list.size() > 0) return list.get(0); 
		return null;
	}
	
}
