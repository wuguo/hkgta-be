package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceSubscribe;

public interface CustomerServiceSubscribeDao extends IBaseDao<CustomerServiceSubscribe> {

	public void saveServiceSubscribe(CustomerServiceSubscribe customerServiceSubscribe);

	public List<CustomerServiceSubscribe> getCustomerServiceSubscribeByAccNo(
			Long accNo);
}
