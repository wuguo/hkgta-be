package com.sinodynamic.hkgta.dao.pms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.pms.RoomDto;
import com.sinodynamic.hkgta.entity.pms.Room;

public interface RoomDao extends IBaseDao<Room>{

	public List<Room> getRoomList();
	
	public List<RoomDto> getRoomListWithTaskCounts();

	public List<Long> getNoResponseRoomIdx(int raRespTime, Date reqDate);

	public List<Room> getRoomListByStatus(String[] status) throws Exception;
	
	public List<Room> getRoomListOutOfStatus(String[] status) throws Exception;
	
	public List<RoomDto> getRoomCrewList(String crewRoleId);

	public List<Room> getMaintenanceRoomList();
	
	public List<Room> getMaintenanceMyRoomList();
	
	public Room getByRoomId(Long roomId);
	
	public List<Room> getLimitedInfoRoomList();
	
	public Room getByRoomNo(String roomNo);
	
	public List<Room> getCheckInRoomList();

	//added by Kaster 20160407
	public RoomDto getRoomWithTaskCountByRoomId(Long roomId);

}
