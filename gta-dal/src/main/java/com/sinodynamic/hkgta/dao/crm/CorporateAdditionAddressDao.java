package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CorporateAdditionAddress;

public interface CorporateAdditionAddressDao extends IBaseDao<CorporateAdditionAddress>{

	public Serializable saveCorpAdditionAddress(CorporateAdditionAddress corpAdditionAddress);
	
	public boolean updateCorpAdditionAddress(CorporateAdditionAddress corpAdditionAddress);
}
