package com.sinodynamic.hkgta.dao.pms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;

public interface HousekeepTaskFileDao extends IBaseDao<HousekeepTaskFile>{

	byte[] getHouseKeepTaskAttach(String jobType, String startTime, String endTime, String fileType, String roomNumber, String sortBy,
			String isAscending)  throws Exception;

	byte[] getRoomWeeklyAttendantAttach(String userId, String userName, String startTime, String dateSql, String endTime, String fileType,
			String sortBy, String isAscending) throws Exception;
	
}
