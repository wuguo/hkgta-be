
package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;


/**
 * Email operator interface
 * @author Mianping_Wu
 *
 */
public interface CustomerEmailContentDao extends IBaseDao<CustomerEmailContent>{
	
	public Serializable addCustomerEmail(CustomerEmailContent ca) throws HibernateException;
	
	public boolean updateCustomerEmail(CustomerEmailContent ca) throws HibernateException;
	/***
	 * select the PND status and FAIL status with resend_count < 3 email
	   records order by create_date.
	 * @param resendCount
	 * @param pageSize
	 * @return
	 */
	public List<CustomerEmailContent> getBatchCustomerEmailContentSendMail(int resendCount, int pageSize);
}