package com.sinodynamic.hkgta.dao.ibeacon;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.ibeacon.SitePoi;

public interface SitePoiDao extends IBaseDao<SitePoi> {
	 public List<SitePoi> listSitePois();
}
