package com.sinodynamic.hkgta.dao.crm;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.HealthCheckingDto;
import com.sinodynamic.hkgta.entity.crm.LoginSession;
import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * @author Junfeng_Yan
 * @since May 9 2015
 */
public interface LoginSessionDao extends IBaseDao<LoginSession>{

	public LoginSession getByToken(String authToken) throws HibernateException;
	
	public LoginSession getByUserId(String userId) throws HibernateException;
	
	public void saveSessionToken(LoginSession sessionToken) throws HibernateException;
	
	public LoginSession getByUserIdAndDevice(String userId, String device) throws HibernateException;
	
	public boolean deleteSessionToken(String userId, String device) throws HibernateException;

	public LoginSession getByUserIdAndAppTypeCode(String userId, String appTypeCode) throws HibernateException;

	/**
	 * 获取心跳检查列表
	 * @param page
	 * @param appTypeCode
	 * @return
	 */
	public ListPage<LoginSession> getHealthCheckingList(ListPage<LoginSession> page, String appTypeCode);
}
