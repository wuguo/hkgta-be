package com.sinodynamic.hkgta.dao.pos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;

@Repository
public class RestaurantMenuItemDaoImpl extends GenericDao<RestaurantMenuItem> implements RestaurantMenuItemDao{

	@Override
	public List<RestaurantMenuItem> getRestaurantMenuItembyDisplayOrder(Integer displayOrder) {
		StringBuilder hql = new StringBuilder();
		hql.append("from RestaurantMenuItem i where i.displayOrder = ? ");
		
		List<Serializable> param = new ArrayList<>();
		param.add(displayOrder);
		return super.getByHql(hql.toString(), param);
	}

	@Override
	public Integer getMaxDisplayOrder() {
		StringBuilder hql = new StringBuilder();
		hql.append("select max(displayOrder) as displayOrder from RestaurantMenuItem i ");
		return (Integer) super.getCurrentSession().createQuery(hql.toString()).uniqueResult();
	}

	@Override
	public Integer getMaxDisplayOrder(String restaurantId, String catId) {
		StringBuilder hql = new StringBuilder();
		hql.append("select max(displayOrder) as displayOrder from RestaurantMenuItem i ");
		hql.append(" where restaurantMenuCat.catId = ? and  i.restaurantMenuCat.restaurantMaster.restaurantId = ? ");

		Query query = super.getCurrentSession().createQuery(hql.toString());
		query.setParameter(0, catId);
		query.setParameter(1, restaurantId);
		return (Integer) query.uniqueResult();
	}
	
	@Override
	public Integer getMinDisplayOrder(String restaurantId, String catId) {
		StringBuilder hql = new StringBuilder();
		hql.append("select min(displayOrder) as displayOrder from RestaurantMenuItem i ");
		hql.append(" where i.restaurantMenuCat.catId = ? and  i.restaurantMenuCat.restaurantMaster.restaurantId = ? ");
		Query query = super.getCurrentSession().createQuery(hql.toString());
		query.setParameter(0, catId);
		query.setParameter(1, restaurantId);
		return (Integer) query.uniqueResult();
	}

	@Override
	public RestaurantMenuItem getRestaurantMenuItem(String restaurantId, String catId, String itemNo) {
		String hql = "from RestaurantMenuItem i where i.restaurantMenuCat.restaurantMaster.restaurantId = ? and i.restaurantMenuCat.catId = ?  and i.restaurantItemMaster.itemNo = ? ";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(restaurantId);
		params.add(catId);
		params.add(itemNo);
		return (RestaurantMenuItem) super.getUniqueByHql(hql, params);
	}

}
