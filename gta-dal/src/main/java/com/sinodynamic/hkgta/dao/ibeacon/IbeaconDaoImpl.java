package com.sinodynamic.hkgta.dao.ibeacon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.BeaconManageDto;
import com.sinodynamic.hkgta.dto.crm.BeaconMemberDto;
import com.sinodynamic.hkgta.dto.crm.BeaconRoomDto;
import com.sinodynamic.hkgta.entity.ibeacon.Ibeacon;
import com.sinodynamic.hkgta.entity.ibeacon.IbeaconNearbyPerson;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class IbeaconDaoImpl extends GenericDao<Ibeacon> implements IbeaconDao {

	public Ibeacon getByMinorAndMajor(Long major, Long minor) {
		String hql = " from Ibeacon i where i.major = ? and i.minor = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(major);
		param.add(minor);
		return (Ibeacon) getUniqueByHql(hql, param);
	}

	public IbeaconNearbyPerson getNearbyMemberById(String userId, Long ibeaconId) {
		String hql = " from IbeaconNearbyPerson i where i.id.userId = ? and i.id.ibeaconId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		param.add(ibeaconId);
		return (IbeaconNearbyPerson) getUniqueByHql(hql, param);
	}

	public boolean updateIbeaconStatus(String status, Long ibeaconIdDB, String loginUserId) {
		String hqlstr = " update Ibeacon m set m.status = ?, m.updateBy = ?, m.updateDate = ? where m.ibeaconId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		param.add(loginUserId);
		param.add(new Date());
		param.add(ibeaconIdDB);
		if (hqlUpdate(hqlstr, param) > 0)
			return true;
		return false;
	}

	public boolean updateMemberBeaconDetail(String description, String venueCode, Long poiId, String loginUserId) {
		String hqlstr = " update SitePoi m set m.description = ?,m.venueCode = ?, m.updateBy = ?, m.updateDate = ? where m.poiId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(description);
		param.add(venueCode);
		param.add(loginUserId);
		param.add(new Date());
		param.add(poiId);
		if (hqlUpdate(hqlstr, param) > 0)
			return true;
		return false;
	}
	
	public boolean updateHousekeepBeaconDetail(String description, Long pastRoomId, Long presentRoomId, Long pastPoiId,Long presentPoiId, String loginUserId) {
		Date currentDate = new Date();
		
		if (pastPoiId.longValue() != presentPoiId.longValue()) {
			// used to remove the origin room's beacon
			int k = 0;
			int i = 0;
			List<Ibeacon> pastBeaconList = getIbeaconBySitePoi(pastPoiId);
			if(pastBeaconList!=null&&pastBeaconList.size()==0){
				String oldHqlRoom = " update Room r set r.sitePoiId = ?, r.updateBy = ?, r.updateDate = ? where r.roomId = ? ";
				List<Serializable> paramOrigin = new ArrayList<Serializable>();
				paramOrigin.add(null);
				paramOrigin.add(loginUserId);
				paramOrigin.add(currentDate);
				paramOrigin.add(pastRoomId);
				k = hqlUpdate(oldHqlRoom, paramOrigin);
				
				Object[] param = new Object[] { pastPoiId };
				i = deleteByHql("DELETE m from site_poi m where m.poi_id = ? ", param);
			}
			
			// used to update the room's beacon
			String hqlRoom = " update Room r set r.sitePoiId = ?, r.updateBy = ?, r.updateDate = ? where r.roomId = ? ";
			List<Serializable> paramRoom = new ArrayList<Serializable>();
			paramRoom.add(presentPoiId);
			paramRoom.add(loginUserId);
			paramRoom.add(currentDate);
			paramRoom.add(presentRoomId);
			int j = hqlUpdate(hqlRoom, paramRoom);
			if (i>0&&j > 0 && k > 0)
				return true;
			return false;
		}

		return true;
	}

	public Ibeacon getIbeaconByPK(Long ibeaconIdDB) {
		String hql = " from Ibeacon i where i.ibeaconId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(ibeaconIdDB);
		return (Ibeacon) getUniqueByHql(hql, param);
	}

	public boolean checkAvailableMinor(Long major, Long minor, Long ibeaconIdDB) {
		String sqlstr = " select c.minor as minor from ibeacon c where c.major =? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(major);
		Ibeacon ibeacon = null;
		if (!StringUtils.isEmpty(ibeaconIdDB)) {
			ibeacon = (Ibeacon) get(Ibeacon.class, ibeaconIdDB);
		}
		List<BeaconManageDto> beaconlist = getDtoBySql(sqlstr, listParam, BeaconManageDto.class);
		if (beaconlist != null) {
			if (beaconlist.size() == 0) {
				return true;
			} else if (beaconlist.size() > 0) {
				for (BeaconManageDto dto : beaconlist) {
					if (ibeacon != null) {
						if (ibeacon.getMinor().equals(minor)) {
							return true;
						}
					}
					if (dto.getMinor().equals(minor)) {
						return false;
					}

				}
				return true;
			}
		}
		return true;
	}

	public List<BeaconManageDto> getListByStatus(String status) {
		String sql = " select vm.venue_code as venueCode, vm.venue_name as venueName from venue_master vm where vm.status = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		return getDtoBySql(sql, param, BeaconManageDto.class);
	}

	public int deleteIbeaconById(Long ibeaconIdDB) {
		Object[] param = new Object[] { ibeaconIdDB };
		return this.deleteByHql("DELETE m from ibeacon m where m.ibeacon_id = ? ", param);
	}

	public int deleteIbeaconNearbyPersonByIbeaconId(Long ibeaconIdDB) {
		Object[] param = new Object[] { ibeaconIdDB };
		return this.deleteByHql("DELETE m from ibeacon_nearby_person m where m.ibeacon_id = ? ", param);
	}
	
	public int deleteSitePoi(Long site_poi_id) {
		Object[] param = new Object[] { site_poi_id };
		return this.deleteByHql("DELETE m from site_poi m where m.poi_id = ? ", param);
	}
	
	public int removeSitPoiInRoom(String loginUserId, Long sitePoiId) {
		String hqlRoom = " update Room r set r.sitePoiId = ?, r.updateBy = ?, r.updateDate = ? where r.sitePoiId = ? ";
		List<Serializable> paramRoom = new ArrayList<Serializable>();
		paramRoom.add(null);
		paramRoom.add(loginUserId);
		paramRoom.add(new Date());
		paramRoom.add(sitePoiId);
		return hqlUpdate(hqlRoom, paramRoom);
	}

	public Map<String, Object> getMemberByVenueCode(Long[] poiIdArray, String venueCode, Integer beaconMemberCount) {
		StringBuilder sql = new StringBuilder(
				"SELECT\n" +
						"	m.academy_no AS academyNo,\n" +
						"	m.customer_id AS customerId,\n" +
						//"	IFNULL(CONCAT(cp.portrait_photo,'_small'),'') AS portraitPhoto,\n" +//modified by Kaster 20160310
						//update christ 2016-09-26  display _small.jpg
						"   CASE  \n"+ 
						"   WHEN cp.portrait_photo<>'' THEN CONCAT(SUBSTRING(cp.portrait_photo,1,(LOCATE('.',cp.portrait_photo)-1)),'_small',SUBSTRING(cp.portrait_photo,LOCATE('.',cp.portrait_photo),LENGTH(cp.portrait_photo))) \n"+
						"   ELSE '' \n"+
						"   END AS portraitPhoto ,"+
						
						"	m.vip AS vip,\n" +	//added by Kaster 20160516
						"	concat(\n" +
						"		cp.salutation,\n" +
						"		' ',\n" +
						"		cp.given_name,\n" +
						"		' ',\n" +
						"		cp.surname\n" +
						"	) AS fullName,concat(TIMEDIFF(CURRENT_TIMESTAMP,inb.last_timestamp),'') AS timeDifference\n" +//modified by Kaster 20160310
						"FROM\n" +
						"	customer_profile cp,\n" +
						"	member m,\n" +
						"	site_poi sp,\n" +
						"	ibeacon i,\n" +
						"	ibeacon_nearby_person inb\n" +
						"WHERE\n" +
						"	m.user_id = inb.user_id\n" +
						"AND m.customer_id = cp.customer_id\n" +
						"AND sp.poi_id = i.site_poi_id\n" +
						"AND inb.ibeacon_id = i.ibeacon_id\n" +
						"AND inb.last_timestamp = (\n" +
						"	SELECT\n" +
						"		max(person.last_timestamp)\n" +
						"	FROM\n" +
						"		ibeacon_nearby_person person,\n" +
						"		site_poi poi,\n" +
						"		ibeacon ibeaconInside\n" +
						"	WHERE\n" +
						"		ibeaconInside.ibeacon_id = person.ibeacon_id\n" +
						"	AND person.user_id = inb.user_id\n" +
						"	AND ibeaconInside.site_poi_id = poi.poi_id\n" +
						"	AND poi.venue_code = sp.venue_code\n" +
						"   <poiIdCondition>\n"+
						")\n");

		StringBuilder sqlForBeacon = new StringBuilder("select i. status AS status FROM site_poi sp, ibeacon i WHERE sp.poi_id = i.site_poi_id ");

		StringBuilder sqlTotalCount = new StringBuilder("select i. status AS status FROM site_poi sp, ibeacon i WHERE sp.poi_id = i.site_poi_id ");
        
		StringBuilder poiIdCondition = new StringBuilder("");
		
		List<Serializable> param = new ArrayList<Serializable>();
		List<Serializable> paramSqlForBeacon = new ArrayList<Serializable>();
		List<Serializable> paramTotal = new ArrayList<Serializable>();
		if (poiIdArray != null) {
			if (poiIdArray.length > 0) {
				sql.append(" and ( ");
				sqlForBeacon.append(" and ( ");
				poiIdCondition.append(" and (");
				
			}
			for (int i = 0; i < poiIdArray.length; i++) {
				if (i == poiIdArray.length - 1) {
					sql.append(" sp.poi_id = ? ) ");
					sqlForBeacon.append(" sp.poi_id = ? ) ");
					poiIdCondition.append(" poi.poi_id = ? ) ");
				} else {
					sql.append(" sp.poi_id = ? or ");
					sqlForBeacon.append(" sp.poi_id = ? or ");
					poiIdCondition.append(" poi.poi_id = ? or ");
				}
				paramSqlForBeacon.add(poiIdArray[i]);
				param.add(poiIdArray[i]);
			}
			
			if (poiIdArray.length > 0) {
				param.addAll(param);
			}
			
		}
		if (venueCode != null) {
			sql.append(" and sp.venue_code = ? ");
			sqlForBeacon.append(" and sp.venue_code = ? ");
			sqlTotalCount.append(" and sp.venue_code = ? ");
			paramTotal.add(venueCode);
			param.add(venueCode);
			paramSqlForBeacon.add(venueCode);
		}
		sql.append(" ORDER BY inb.last_timestamp DESC LIMIT 0,");
		sql.append(beaconMemberCount);
		String sqlMember = sql.toString();
		if(poiIdArray != null && poiIdArray.length > 0){
			sqlMember = sqlMember.replace("<poiIdCondition>",poiIdCondition);
		}else{
			sqlMember = sqlMember.replace("<poiIdCondition>","");
		}
		
		List<BeaconMemberDto> member = new ArrayList<BeaconMemberDto>();
		List<BeaconMemberDto> statusCount = getDtoBySql(sqlForBeacon.toString(), paramSqlForBeacon, BeaconMemberDto.class);
		List<BeaconMemberDto> statusTotalCount = new ArrayList<BeaconMemberDto>();
//		if (poiIdArray != null && poiIdArray.length > 0) {
			member = getDtoBySql(sqlMember, param, BeaconMemberDto.class);
			statusTotalCount = getDtoBySql(sqlTotalCount.toString(), paramTotal, BeaconMemberDto.class);
//		} else {
//			
//			statusTotalCount = statusCount;
//		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("member", member);
		map.put("statusCount", statusCount);
		map.put("statusTotalCount", statusTotalCount);
		return map;
	}

	public List<BeaconManageDto> getBeaconByVenueCode(String venueCode) {
		String sql = " select sp.description as beaconName, sp.venue_code as venueCode, i.status as status,sp.poi_id as poiId, i.guid as guid  "
				+ "from site_poi sp,ibeacon i where i.site_poi_id = sp.poi_id and sp.venue_code = ? ORDER BY beaconName";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(venueCode);
		return getDtoBySql(sql, param, BeaconManageDto.class);
	}

	public boolean changeMajor(Long major,String targetType) {
		String hql = " from Ibeacon i, SitePoi sp where sp.poiId = i.sitePoiId and sp.targetTypeCode = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(targetType);
		List<Ibeacon> ibeaconList = getByHql(hql,param);
		for (Object object : ibeaconList) {
			Object[] tempObject = (Object[]) object;
			Ibeacon tempBeacon = (Ibeacon) tempObject[0];
			tempBeacon.setMajor(major);
			boolean status = update(tempBeacon);
			if (!status) {
				return false;
			}
		}
		return true;
	}

	@Override
	public List<BeaconRoomDto> getRoomBeaconsList() {
		String sql = "SELECT r.roomId as roomId, r.roomNo as roomNo, i.guid as guid, i.major as major, i.minor as minor, i.status as status FROM Room r, Ibeacon i "
				+ "WHERE i.sitePoiId = r.sitePoiId AND i.status != 'NACT' ORDER BY r.roomId, i.ibeaconId";
		return getDtoByHql(sql, null, BeaconRoomDto.class);
	}

	@Override
	public List<BeaconRoomDto> getRoomBeaconsList(Long roomId)
	{
		String sql = "SELECT r.roomId as roomId, r.roomNo as roomNo, i.guid as guid, i.major as major, i.minor as minor, i.status as status FROM Room r, Ibeacon i "
				+ "WHERE i.sitePoiId = r.sitePoiId AND i.status != 'NACT' and r.roomId = ? ORDER BY r.roomId, i.ibeaconId";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(roomId);
		return getDtoByHql(sql, param, BeaconRoomDto.class);
	}
	
	public Long getTheNextAvailableMinorId(Long major){
		String hqlstr = " SELECT max(m.minor)+1 FROM Ibeacon m where m.major = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(major);
		List<Long> ret = excuteByHql(Long.class, hqlstr, param);
		if (ret != null && ret.size() > 0) {
			if (ret.get(0) == null) {
				return Long.valueOf("1");
			} else {
				return ret.get(0).longValue();
			}

		} else {
			return Long.valueOf("1");
		}
		
	}
	
	
	public boolean checkMajorAvailability(Long major,String targetType){
		String hql = " select DISTINCT i.major as major from Ibeacon i, SitePoi sp where sp.poiId = i.sitePoiId and sp.targetTypeCode != ? and i.major = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(targetType);
		param.add(major);
		List<Ibeacon> ibeaconList = getByHql(hql,param);
		if(ibeaconList!=null&&ibeaconList.size()>0){
			return false;
		}
		return true;
		
	}
	
	public List<Ibeacon> getIbeaconBySitePoi(Long sitePoiId) {
		String hql = " from Ibeacon i where i.sitePoiId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(sitePoiId);
		return getByHql(hql, param);
	}
	
	public ListPage<Ibeacon> getMemberByVenueCodeForTablet(ListPage<Ibeacon> page, String venueCode) {
		//modified by Kaster 20160511 查询多一个字段：member的vip:IFNULL(m.vip,'N') AS vip
		StringBuilder sql = new StringBuilder(
				"SELECT\n" +
						"	IFNULL(m.academy_no, '') AS academyNo,\n" +
						"	m.customer_id AS customerId,\n" +
						"	IFNULL(m.vip,'N') AS vip,\n" +
						"	cp.portrait_photo AS portraitPhoto,\n" +
						"	concat(\n" +
						"		cp.salutation,\n" +
						"		' ',\n" +
						"		cp.given_name,\n" +
						"		' ',\n" +
						"		cp.surname\n" +
						"	) AS fullName,\n" +
						"	IFNULL(cp.phone_mobile, '') AS phone,\n" +
						"	IFNULL(cp.greeting_info, '') AS greeting,\n" +
						"	inb.last_timestamp AS lastTimestamp,\n" +
						"   IFNULL(cp.surname_nls, '') as surnameNls,\n"+
						"   IFNULL(cp.given_name_nls, '') as givenNameNls\n"+
						"FROM\n" +
						"	customer_profile cp,\n" +
						"	member m,\n" +
						"	site_poi sp,\n" +
						"	ibeacon i,\n" +
						"	ibeacon_nearby_person inb\n" +
						"WHERE\n" +
						"	m.user_id = inb.user_id\n" +
						"AND m.customer_id = cp.customer_id\n" +
						"AND sp.poi_id = i.site_poi_id\n" +
						"AND inb.ibeacon_id = i.ibeacon_id\n" +
						"AND inb.last_timestamp = (\n" +
						"	SELECT\n" +
						"		max(person.last_timestamp)\n" +
						"	FROM\n" +
						"		ibeacon_nearby_person person,\n" +
						"		site_poi poi,\n" +
						"		ibeacon ibeaconInside\n" +
						"	WHERE\n" +
						"		ibeaconInside.ibeacon_id = person.ibeacon_id\n" +
						"	AND ibeaconInside.site_poi_id = poi.poi_id\n" +
						"	AND person.user_id = inb.user_id\n" +
						"	AND poi.venue_code = sp.venue_code\n" +
						")\n");

		List<Serializable> param = new ArrayList<Serializable>();
		if (venueCode != null) {
			sql.append(" and sp.venue_code = ? ");
			param.add(venueCode);
		}
		String countSql = " SELECT count(1) FROM ( " +sql.toString() + " ) x";
		return listBySqlDto(page, countSql, sql.toString(), param, new BeaconMemberDto());
	}

}
