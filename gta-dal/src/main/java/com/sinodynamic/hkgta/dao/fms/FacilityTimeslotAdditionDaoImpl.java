package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotAddition;

@Repository
public class FacilityTimeslotAdditionDaoImpl extends GenericDao<FacilityTimeslotAddition>  implements FacilityTimeslotAdditionDao{

	@Override
	public FacilityTimeslotAddition getFacilityTimeslotAddition(long facilityTimeslotId) {

		String hql = " SELECT f FROM FacilityTimeslotAddition f where f.facilityTimeslotId=?";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityTimeslotId);
		List<FacilityTimeslotAddition> facilityTimeslotAdditionList = getByHql(hql, paramList);
		if (facilityTimeslotAdditionList.size() > 0) {
			return facilityTimeslotAdditionList.get(0);
		}
		return null;
	}

	@Override
	public int deleteFacilityTimeslotAdditionByFacilityTimeslotId(
			Long facilityTimeslotId) throws HibernateException {
		// TODO Auto-generated method stub
		String sql = " delete from facility_timeslot_addition where facility_timeslot_id = ? ";
		return super.deleteByHql(sql, facilityTimeslotId);
	}
}
