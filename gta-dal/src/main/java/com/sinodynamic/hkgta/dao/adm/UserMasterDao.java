package com.sinodynamic.hkgta.dao.adm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface UserMasterDao extends IBaseDao<UserMaster>{
	public void addUserMaster(UserMaster m) ;

	public void getUserMasterList(ListPage<UserMaster> page,String countHql,String hql,List<?> param) throws Exception ;
	
	public ListPage<UserMaster> getUserMasterList(Long roleId,String orderColumn, String order, int pageSize, int currentPage,
			String filters,String userType);

	public void updateUserMaster(UserMaster m) throws Exception ;

	public void deleteUserMaster(UserMaster m) throws Exception ;
	
	public UserMaster getUserByLoginId(String id) throws Exception;
	
	public UserMaster getUserByUserId(String userId);

	public String saveUserMaster(UserMaster m) throws HibernateException;
	
	public StaffDto getStaffDto(String userId);
	
	public boolean checkAvailableLoginId(String loginId, String userId);
}
