package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;

/**
 * 
 * @author Mianping_Wu
 * @since 5/4/2015
 */
@Repository
public class StaffProfileDaoImpl extends GenericDao<StaffProfile> implements StaffProfileDao {

	@Override
	public List<StaffProfile> selectAllStaffProfile(Long enrollId) throws Exception {
		
//		String hql = "select new StaffProfile(userId, givenName, surname) from StaffProfile order by userId";
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT sp.user_id userId, sp.surname, sp.given_name givenName FROM staff_profile sp ");
		sql.append("LEFT JOIN staff_master sm ON sp.user_id = sm.user_id ");
		sql.append("LEFT JOIN position_title pt ON sm.position_title_code = pt.position_code ");
		sql.append("where sm.`status` = 'ACT' and pt.depart_id = ? ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(2);
		if (null != enrollId) {
			sql.append(" and (sm.user_id != (SELECT  ce.sales_follow_by from customer_enrollment ce where ce.enroll_id = ? ) or sm.user_id is NULL)");
			param.add(enrollId);
		}
		return getDtoBySql(sql.toString(), param, StaffProfile.class);

	}
	
	public StaffProfile getByUserId(String staffUserId){
		return get(StaffProfile.class, staffUserId);
	}

	@Override
	public void addStaffProfile(StaffProfile sp) {
		// TODO Auto-generated method stub
		super.save(sp);
	}

	@Override
	public void updateStaffProfile(StaffProfile sp) throws HibernateException {
		// TODO Auto-generated method stub
		super.update(sp);
	}

	@Override
	public StaffProfile getStaffProfileByUserId(String userId)
			throws HibernateException {
		// TODO Auto-generated method stub
		return super.get(StaffProfile.class, userId);
	}

	@Override
	public boolean isEmailUsed(String email) throws HibernateException {
		// TODO Auto-generated method stub
		List<StaffProfile> pList = super.getByCol(StaffProfile.class, "contactEmail", email, null);
		return null != pList && pList.size() > 0;
	}

	/**
	 * added by Kaster 20160509
	 */
	@Override
	public List<StaffProfile> selectAllStaffIdsForAssignTo(String salesStaffId) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT sp.user_id userId, sp.surname, sp.given_name givenName FROM staff_profile sp ");
		sql.append("LEFT JOIN staff_master sm ON sp.user_id = sm.user_id ");
		sql.append("LEFT JOIN position_title pt ON sm.position_title_code = pt.position_code ");
		sql.append("where sm.`status` = 'ACT' and pt.depart_id = ? ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(2);
		if (null != salesStaffId) {
			sql.append(" and (sm.user_id != ?)");
			param.add(salesStaffId);
		}
		return getDtoBySql(sql.toString(), param, StaffProfile.class);

	}

}
