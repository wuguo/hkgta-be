package com.sinodynamic.hkgta.dao.adm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * search for staffmaster
 * 
 * @author Junfeng_Yan
 * 
 * @since May 28 2015
 *
 */
public interface StaffMasterInfoDtoDao extends IBaseDao<StaffMasterInfoDto>
{

	public ListPage<StaffMasterInfoDto> getStaffMasterList(ListPage<StaffMasterInfoDto> page, String countHql, String hql) throws HibernateException;

	public StaffMaster getByUserId(String userId);

	public List<StaffMasterInfoDto> getStaffMasterByPositionTitle(String positionTitle);

	public List<StaffMaster> getStaffMasterByStaffType(String staffType);

	public List<StaffMaster> getExpireStaff();
}
