package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.BigDecimalType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.Constant.Status;
import com.sinodynamic.hkgta.dto.account.DDIDto;
import com.sinodynamic.hkgta.entity.crm.DdiTransactionLog;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
@Repository
public class DDITransactionLogDaoImpl extends GenericDao<DdiTransactionLog> implements DDITransactionLogDao {

	@Override
	public List<DDIDto> queryDDITransactionList(int year, int month) {
		String sql = "SELECT"
				+ " sum(cot.paid_amount) as amount,"
				+ " cot.`status` as status,"
				+ " cot.transaction_no as transactionNo,"
				+ "ohd.customer_id as customerId,"
				+ " mpa.account_no as accountNumber,"
				+ " mpa.bank_account_name as accountName,"
				+ " mpa.bank_id as bankId,"
				+ " mpa.originator_bank_code as originatorBankCode"
			    + " FROM"
				+ " customer_order_trans cot,"
				+ " customer_order_hd ohd,"
				+ " member_payment_acc mpa"
			    + " WHERE"
				+ " cot.order_no = ohd.order_no"
			    + " AND mpa.customer_id = ohd.customer_id"
			    //+ " AND cot.payment_method_code = 'DDI'"
			    + " AND cot.`status` = 'SUC'"//??????
			    + " AND year(transaction_timestamp)=? and month(transaction_timestamp)=?"
			    + " AND NOT EXISTS ("
				+ " SELECT 1  from "
				+ "	ddi_transaction_log dtl"
				+ " WHERE"
				+ "	dtl.transaction_no = cot.transaction_no)"
				+ " group by ohd.customer_id,mpa.bank_id";
	
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("amount", BigDecimalType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("transactionNo", LongType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("accountNumber", StringType.INSTANCE);
		typeMap.put("accountName", StringType.INSTANCE);
		typeMap.put("bankId", StringType.INSTANCE);
		typeMap.put("originatorBankCode", StringType.INSTANCE);
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(year);
		param.add(month);
		
		List<DDIDto> ddiList = getDtoBySql(sql.toString(), param, DDIDto.class, typeMap);
		
		return ddiList;
	}
	@Override
	public List<DDIDto> queryDDIData(String date) {
		StringBuilder hql=new StringBuilder();
		hql.append("SELECT ma.customerId AS customerId,\n");
		hql.append("mc.recalBalance*(-1) AS amount,\n");
		hql.append("ma.drawerBankCode AS debtorBankCode,\n");
		hql.append("ma.accountNo AS debtorAccountNumber,\n");
		hql.append("ma.bankAccountName AS debtorAccountName, \n");
		hql.append("ma.bankCustomerId AS reference \n");
		hql.append(" FROM  MemberCashvalueBalHistory mc,MemberPaymentAcc ma \n"); 
		hql.append("WHERE mc.customerId=ma.customerId  \n");
		hql.append("AND ma.accType='DD' \n"); 
		hql.append("AND ma.status='ACT' \n"); 
		hql.append("AND mc.recalBalance<0 \n");
		hql.append("AND DATE_FORMAT(mc.cutoffDate, '%Y-%m-%d %H:%i:%s')='"+date+"' \n");
		return this.getDtoByHql(hql.toString(), null, DDIDto.class);
	}

	
}
