package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.util.constant.Constant;
@Repository
public class UserDeviceDaoImpl extends GenericDao<UserDevice> implements UserDeviceDao {

	@Override
	public int deleteUserDevice(String userId,String arnApplication)
	{
		String hql = "update UserDevice set status = ? where userId = ? and arnApplication =? ";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(Constant.Status.CAN.name());
		params.add(userId);
		params.add(arnApplication);
		return super.hqlUpdate(hql, params);
	}

	@Override
	public UserDevice getUserDeviceByApplication(String userId, String arnApplication) {
		StringBuilder hql = new StringBuilder();
		
		hql.append("from UserDevice where userId = ? ");
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(userId);
		if (arnApplication != null) {
			hql.append(" and arnApplication = ? ");
			params.add(arnApplication);
		}
		return (UserDevice) super.getUniqueByHql(hql.toString(), params);
	}

}
