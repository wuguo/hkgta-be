package com.sinodynamic.hkgta.dao.fms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilityType;

public interface FacilityTypeDao extends IBaseDao<FacilityType>{

}
