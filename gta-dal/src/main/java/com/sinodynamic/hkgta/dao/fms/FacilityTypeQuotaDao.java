package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.FacilityTypeQuotaDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTypeQuota;

public interface FacilityTypeQuotaDao extends IBaseDao<FacilityTypeQuota> {
	
	public List<FacilityTypeQuotaDto> getFacilityTypeQuotaList(String hql, List<Serializable> params);

	public FacilityTypeQuota getFacilityTypeQuota(String facilityType, Date specificDate,String subtypeId);
}