package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageDto;
import com.sinodynamic.hkgta.entity.crm.AdvertiseImage;

public interface AdvertiseImageDao extends IBaseDao<AdvertiseImage> {

	/**
	 * @param appType
	 *            should be valid value of column 'application_type' in hkgta.advertise_image, in upper case.
	 * @param dispLoc
	 *            should be valid value of column 'display_location' in hkgta.advertise_image, in upper case.
	 */
	public List<AdvertiseImageDto> getByAppTypeDispLoc(String appType, String dispLoc);

	public Long getMaxDispOrder(String appType, String dispLoc);

	public void updateDispOrder(long imgId, int newDispOrder);

	public AdvertiseImage getAdvertiseImage(String appType, String dispLoc,Long linkLevel,Long dispOrder);
	
	public int deleteAdvertiseImages(String dispLoc);
}
