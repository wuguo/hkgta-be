package com.sinodynamic.hkgta.dao.pms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTaskAssignee;

public interface RoomHousekeepTaskAssigneeDao extends IBaseDao<RoomHousekeepTaskAssignee>{
	
}
