package com.sinodynamic.hkgta.dao.crm.setting;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;

@Repository
public class UserPreferenceSettingDaoImpl extends GenericDao<UserPreferenceSetting>
		implements UserPreferenceSettingDao {


	@Override
	public UserPreferenceSetting checkUserSettingValue(String userId, String paramId) {
		String hql="from UserPreferenceSetting as s where s.id.userId=? and s.globalParameter.paramId=?";
		List<Serializable>params=new ArrayList<>();
		params.add(userId);
		params.add(paramId);
		return (UserPreferenceSetting)super.getUniqueByHql(hql, params);
		
	}

}
