package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.AgeRange;

public interface AgeRangeDao extends IBaseDao<AgeRange>{

}
