package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;

public interface ServerFolderMappingDao extends IBaseDao<ServerFolderMapping>{

	public ServerFolderMapping findById(Long folderId);
	public ServerFolderMapping findByName(String folderName);
	public int findFileNumber(Long folderId);
}
