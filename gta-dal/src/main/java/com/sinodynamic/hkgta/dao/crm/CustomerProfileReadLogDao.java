package com.sinodynamic.hkgta.dao.crm;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerProfileReadLog;

public interface CustomerProfileReadLogDao extends IBaseDao<CustomerProfileReadLog>
{

	public boolean saveCustomerProfileReadLog(CustomerProfileReadLog log);
	
	/***
	 * get CustomerProfileReadLog by readTime
	 * @param readTime
	 * @return
	 */
	public List<CustomerProfileReadLog> getListByReadTime(Date readTime);
}
