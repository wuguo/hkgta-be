package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;

@Repository
public class ServerFolderMappingDaoImpl extends GenericDao<ServerFolderMapping> implements ServerFolderMappingDao {

	@Override
	public ServerFolderMapping findById(Long folderId) {
		return this.get(ServerFolderMapping.class, folderId);
	}

	@Override
	public ServerFolderMapping findByName(String folderName) {
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(folderName.toUpperCase());
		ServerFolderMapping exsitedFolder = (ServerFolderMapping)getUniqueByHql("from ServerFolderMapping where upper(displayName)=?", param);
		return exsitedFolder;
	}

	@Override
	public int findFileNumber(Long folderId) {
		return this.excuteByHql(PresentMaterial.class, "from PresentMaterial where status='ACT' and folderId="+folderId).size();
	}
}

