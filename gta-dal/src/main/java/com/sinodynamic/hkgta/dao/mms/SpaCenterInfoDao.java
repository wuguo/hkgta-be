package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.mms.SpaCenterInfo;

public interface SpaCenterInfoDao extends IBaseDao<SpaCenterInfo>{

	 public Serializable saveSpaCenterInfo(SpaCenterInfo sapAppointmenRec);	
		
	    public SpaCenterInfo getByCenterId(Long centerId);

        public void updateSpaCenterInfo(SpaCenterInfo sapAppointmenRec);
        
        public SpaCenterInfo getSpaCenterInfo();
}
