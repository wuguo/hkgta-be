package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.DepartmentBranch;
import com.sinodynamic.hkgta.entity.crm.PositionTitle;

@Repository
public class PositionTitleImpl extends GenericDao<PositionTitle> implements PositionTitleDao{

	@Override
	public List<PositionTitle> getAllPositionTitle(Long departId) throws HibernateException
	{
		String HQL = "FROM PositionTitle p where p.departId = ? or p.departId is null";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(departId);
		return getByHql(HQL, param);
	}


	
}
