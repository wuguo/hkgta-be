package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;

public interface MemberCashValueDao extends IBaseDao<MemberCashvalue>{

	public MemberCashvalue getByCustomerId(Long customerId);
	
	public Serializable saveMemberCashValue(MemberCashvalue memberCashvalue);
	
	public boolean updateMemberCashValue(MemberCashvalue memberCashvalue);
	
	public MemberCashvalue getByVirtualAccNo(String virtualAccNo);
	
	public MemberCashvalue getMemberCashvalueById(Long customerId);
	

	public MemberCashvalue getMemberCashvalueByIdForUpdate(Long customerId);
}
