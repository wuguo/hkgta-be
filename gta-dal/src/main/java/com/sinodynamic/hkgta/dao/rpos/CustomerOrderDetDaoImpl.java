package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
@Repository
public class CustomerOrderDetDaoImpl extends GenericDao<CustomerOrderDet> implements CustomerOrderDetDao {

	public Serializable saveOrderDet(CustomerOrderDet cOrderDet){
		return save(cOrderDet);
	}

	@Override
	public CustomerOrderDet getCustomerOrderDet(Long orderNo)
	{
		StringBuffer hql = new StringBuffer(
				"from CustomerOrderDet as co where co.customerOrderHd.orderNo = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(orderNo);
		List<CustomerOrderDet> customerOrderDets = super.getByHql(hql.toString(), paramList);
		if(null != customerOrderDets && customerOrderDets.size()>0) return customerOrderDets.get(0);
		return null;
	}
	
	public CustomerOrderDet getByItemNo(String itemNo){
		String hqlstr = " from CustomerOrderDet c where c.itemNo = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(itemNo);
		List<CustomerOrderDet> list = getByHql(hqlstr, param);
		if(list!=null&&list.size()>0) return list.get(0);
		return null;
	}

	@Override
	public int deleteByOrderNo(Long orderNo) {
	    
		String hql = "delete from customer_order_det where order_no = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(orderNo);
		return deleteByHql(hql, param);
	}

	@Override
	public List<CustomerOrderDet> getCustomerOrderDetsByOrderNo(Long orderNo) {
	    
	    String hql = "from CustomerOrderDet as co where co.customerOrderHd.orderNo = ?";
	    List<Serializable> paramList = new ArrayList<Serializable>();
	    paramList.add(orderNo);
	    List<CustomerOrderDet> customerOrderDets = super.getByHql(hql, paramList);
	    return customerOrderDets;
	}

	@Override
	public List<CustomerOrderDet> getCustomerOrderDetsByInvoiceNo(String invoiceNo) {
	    
	    String hql = "from CustomerOrderDet where extRefNo = ?";
	    List<Serializable> paramList = new ArrayList<Serializable>();
	    paramList.add(invoiceNo);
	    List<CustomerOrderDet> customerOrderDets = super.getByHql(hql, paramList);
	    return customerOrderDets;
	}
	
}
