package com.sinodynamic.hkgta.dao.adm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.adm.AppsVersion;
import com.sinodynamic.hkgta.entity.crm.DepartmentBranch;

/**
 * search for DepartmentStaffDao 
 * @author Vian Tang
 * 
 * @since June 29 2015
 *
 */
public interface AppsVersionDao extends IBaseDao<AppsVersion> {
	
	List<AppsVersion> getAllAppsVersion() throws HibernateException;
}
