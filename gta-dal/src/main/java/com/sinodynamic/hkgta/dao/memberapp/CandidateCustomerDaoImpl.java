package com.sinodynamic.hkgta.dao.memberapp;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.rpos.CandidateCustomer;

@Repository
public class CandidateCustomerDaoImpl extends GenericDao<CandidateCustomer> implements CandidateCustomerDao {

}
