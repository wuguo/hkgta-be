package com.sinodynamic.hkgta.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface CommonDataQueryDao  {

	public static final String UPPER_AND = "AND";
	public static final String UPPER_OR = "OR";
	public static final String ORDER_BY = " ORDER BY ";
	public static final String DESCENDING = " DESC ";
	public static final String ASCENDING = " ASC ";
	

	public static final String LEFT_BRACKET = "(";
	public static final String RIGHT_BRACKET = ")";
	public static final String SINGLE_QUOTES = "'";
	public static final String SPACE = " ";
	public static final String COLON = ":";
	public static final String COMMA = ",";
	public static final String LIKE_SYMBOL = "%";

	public static final String EQUAL = "eq";
	public static final String NOT_EQUAL = "ne";
	public static final String LESS_THAN = "lt";
	public static final String LESS_EQUAL = "le";
	public static final String GREATER_THAN = "gt";
	public static final String GREATER_EQUAL = "ge";
	public static final String IS_NULL = "nu"; // is null
	public static final String IS_NOT_NULL = "nn";//is not null
	public static final String IN = "in";   // sql in 
	public static final String NOT_IN = "ni";//sql not in 
	public static final String BEGIN_WITH = "bw";// sql begin with
	public static final String NOT_BEGIN_WITH = "bn"; //sql not begin 
	public static final String END_WITH = "ew"; //sql endwith 
	public static final String NOT_END_WITH = "en";//sql not endwith
	public static final String CONTAINS = "cn";//sql contains


	public static final String STRING = String.class.getName();
	public static final String DOUBLE = Double.class.getName();
	public static final String INTEGER = Integer.class.getName();
	public static final String DATE = Date.class.getName();
	public static final String LONG = Long.class.getName();
	public static final String LIST = List.class.getName();

	public static final String DATE_PATTERN_YYYY_MM_DD = "yyyy-MM-dd";
	
	public static final String DATE_PATTERN_COMPLEX = "yyyy-MM-dd HH:mm:ss";



}
