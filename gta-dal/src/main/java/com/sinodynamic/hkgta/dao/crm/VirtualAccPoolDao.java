package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.VirtualAccPool;

public interface VirtualAccPoolDao extends IBaseDao<VirtualAccPool> {
	
	public Serializable saveVirtualAccPool(VirtualAccPool vap);
	
	public List<VirtualAccPool> getVirtualAccList();
	
	public List<VirtualAccPool> getAvailableVirtualAccList(String[] exceptAccs);
	
	public boolean updateStatus(String status, String vaAccNo);
	
	public VirtualAccPool queryOneAvailableVirtualAccount();
}
