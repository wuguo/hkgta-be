package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;

public interface PresentationUploadDao extends IBaseDao<PresentMaterial> {

}
