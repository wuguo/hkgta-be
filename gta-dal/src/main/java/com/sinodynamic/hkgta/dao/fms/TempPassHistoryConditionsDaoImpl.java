package com.sinodynamic.hkgta.dao.fms;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;

@Repository("tempPassHistoryConditions")
public class TempPassHistoryConditionsDaoImpl  extends GenericDao implements AdvanceQueryConditionDao{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
    	
	AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Temp User", "memberName", "java.lang.String", "", 1);
	AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Temp Pass Type", "passType", "java.lang.String", "", 2);
	AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Card ID", "cardId", "java.lang.String", "", 3);
	

	return Arrays.asList(condition1, condition2, condition3);
}

	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		return null;
	}
}
