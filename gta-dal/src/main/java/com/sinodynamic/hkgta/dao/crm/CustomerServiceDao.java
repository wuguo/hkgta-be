package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.CustomerAccountInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerServiceAccDto;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.EnrollmentHistoryDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;

public interface CustomerServiceDao extends IBaseDao<CustomerServiceAcc> {
	public Serializable saveCustomerServiceAcc(CustomerServiceAcc customerServiceAcc);
	
	public CustomerServiceAcc getAccountNoByCustomerId(Long customerId);
	
	public ServicePlan getPlanByPlanName(String planName);
	
	CustomerServiceAcc getLatestCustomerService(Long customerId);
	
	public int updateCustomerServiceAccStatus(BigInteger customerId) ;
	
	public int getAccNo(BigInteger customerId) ;
	
	public List<CustomerServiceAcc> selectExpiringMember(Integer expiringDays) throws Exception;

	public CustomerServiceAcc getCurrentActiveAcc(Long customerID);
	
	public CustomerServiceAccDto getLatestServiceAccByCustomerId(Long customerID) throws HibernateException;

	public List<EnrollmentHistoryDto> getEnrollmentHistoryList(Long customerID);

	public List<CustomerServiceAcc> getExpiredCustomerServiceAccs() throws Exception;

	public List<CustomerServiceAcc> getEffctiveRenewCustomerServiceAccs();
	
	public CustomerAccountInfoDto getNearestServiceAccountByCustomerId(Long customerId);
}
