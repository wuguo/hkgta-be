
package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterialSeq;


@Repository
public class PresentMaterialSeqDaoImpl extends GenericDao<PresentMaterialSeq> implements PresentMaterialSeqDao{

	@Override
	public boolean saveOrUpdatePMS(PresentMaterialSeq pms) {
		return this.saveOrUpdate(pms);
	}

	@Override
	public int getPresentSeq(Long materialId, Long presentId) {
		// TODO Auto-generated method stub
		
		String hql = " select p.presentSeq from PresentMaterialSeq p where p.materialId = " + materialId+"and p.presentId ="+presentId;	
		int presentSeq = (int) super.getUniqueByHql(hql);	
		return presentSeq;
	}

}

