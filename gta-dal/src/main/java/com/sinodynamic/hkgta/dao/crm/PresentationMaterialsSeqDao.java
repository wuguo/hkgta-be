
package com.sinodynamic.hkgta.dao.crm;



import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterialSeq;


public interface PresentationMaterialsSeqDao extends IBaseDao<PresentMaterialSeq> {
	
	public void savePresentationMaterialsSeq(PresentMaterialSeq pms) throws HibernateException;
	
	public int getPresentSeq(Long materialId,Long presentId);
	
	public List<PresentMaterialSeq> getListByPresentId(Long presentId);

}

