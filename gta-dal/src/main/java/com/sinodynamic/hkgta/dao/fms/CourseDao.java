package com.sinodynamic.hkgta.dao.fms;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.CourseListDto;
import com.sinodynamic.hkgta.dto.fms.CourseSimpleInfoDto;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CourseDao extends IBaseDao<CourseMaster> {

	public ListPage<CourseMaster> getCourseList(ListPage<CourseMaster> page, CourseListDto dto);
	
	public CourseMaster getCourseMasterById(Long courseId);

	public CourseSimpleInfoDto getCourseInfo(String courseId);
	
	public void updateCourseMaster(CourseMaster course);
	/**
	 * get the courses will be started in tomorrow
	 * @return
	 * @author Miranda_Zhang
	 * @date Sep 30, 2015
	 */
	public List<CourseMaster> getTomorrowCourses(Date begin, Date end);


	/**
	 * 获取courseSession数量
	 * @param courseId
	 * @return
	 */
	public BigInteger getCoursesSessionNum(int courseId);
}
