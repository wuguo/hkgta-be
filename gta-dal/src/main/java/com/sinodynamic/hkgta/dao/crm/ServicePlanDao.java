package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;
import com.sinodynamic.hkgta.entity.fms.FacilityType;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface ServicePlanDao extends IBaseDao<ServicePlan>{

	public abstract Serializable saveServicePlan(ServicePlan servicePlan) throws HibernateException;

	public abstract List<FacilityType> getFacilityTypes() throws HibernateException;

	public abstract List<ServicePlan> getAllServicePlan() throws HibernateException;
	
	public abstract List<ServicePlan> getAllServicePlan(String subscriberType) throws HibernateException;

	public abstract ServicePlan getServicePlanById(Long planNo) throws HibernateException;

	public abstract ServicePlanAdditionRule getServicePlanAdditionRuleByFK(long planNo,String rightCode) throws HibernateException;
	
	public abstract ServicePlanFacility getServicePlanFacilityByFK(long planNo,String facilityType) throws HibernateException;
	
	public abstract List<ServicePlanRightMaster> getServicePlanRights() throws HibernateException;
	
	public abstract void deleteServicePlan(ServicePlan sp) throws HibernateException;

	public abstract List<ServicePlan> getValidServicPlan();
	
	public abstract List<ServicePlan> getValidCorporateServicPlan();
	
	public abstract List<ServicePlan> getExpiredServicePlans(String status);

	public abstract List<PosServiceItemPrice> getDaypassPriceItemNo(Long planNo, String type);
	
    public abstract List<ServicePlan> getServiceplanByDate(DaypassPurchaseDto dto);
    
    public abstract String getServiceplanQuotaById(Long planNo);

	public abstract List<ServicePlan> getValidDaypass();
	
	public ServicePlan getByplanName(String planName);
	
	ListPage getDailyDayPassUsageList(String date,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL);
}
