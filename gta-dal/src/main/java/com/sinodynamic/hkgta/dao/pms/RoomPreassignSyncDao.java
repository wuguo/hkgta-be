package com.sinodynamic.hkgta.dao.pms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.RoomPreassignSync;

public interface RoomPreassignSyncDao extends IBaseDao<RoomPreassignSync> {
	public boolean removeAll();

	public boolean saveOrUpdate(RoomPreassignSync sync);

}
