package com.sinodynamic.hkgta.dao.crm;


import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAccLog;
@Repository
public class MemberPaymentAccDaoLogImpl extends GenericDao<MemberPaymentAccLog> implements MemberPaymentAccLogDao {
	
}
