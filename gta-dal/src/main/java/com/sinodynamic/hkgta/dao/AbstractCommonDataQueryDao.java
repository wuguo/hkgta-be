package com.sinodynamic.hkgta.dao;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import java.util.List;

public abstract  class AbstractCommonDataQueryDao extends  GenericDao implements CommonDataQueryDao{
	
	public  abstract  void generateSearchStr(SearchRuleDto searchRule, StringBuilder sb) ;

	
	public  String getValueOfInConditions(String searchString) {
		String[] arr = searchString.split(COMMA);
		StringBuilder searchStr=new StringBuilder();
		for(int i=0;i<arr.length;i++){
			searchStr.append(SINGLE_QUOTES).append(arr[i]).append(SINGLE_QUOTES);
			if(i!=arr.length-1){
				searchStr.append(COMMA);
			}
		}
		return searchStr.toString();
	}

	public void parseHybridQuery(StringBuilder sb, AdvanceQueryDto queryDto) {

		List<SearchRuleDto> rules = queryDto.getRules();

		List<AdvanceQueryDto> groups = queryDto.getGroups();
		if (groups == null || groups.size() == 0) {

			if (rules != null && !rules.isEmpty()) {
				sb.append(LEFT_BRACKET);
				String op = (String) queryDto.getGroupOp();
				StringBuilder subBuilder = new StringBuilder();
				sb.append(parsePlainQuery(subBuilder, queryDto, op));
				sb.append(RIGHT_BRACKET);
			}

		} else {

			sb.append(LEFT_BRACKET);
			String groupOp = queryDto.getGroupOp();

			for (SearchRuleDto rule : rules) {
				generateSearchStr(rule, sb);
				sb.append(SPACE).append(groupOp).append(SPACE);
			}
			AdvanceQueryDto advanceQueryDto = queryDto.getGroups().get(0);
			parseHybridQuery(sb, advanceQueryDto);

			sb.append(RIGHT_BRACKET);
		}
	}

	public  String parsePlainQuery(StringBuilder sb, AdvanceQueryDto queryDto, String globleOp) {
		 List<SearchRuleDto> rules = queryDto.getRules();
		 if(rules !=null && rules.size()>0){
			for (SearchRuleDto searchRule : rules) {
				generateSearchStr(searchRule, sb);
				sb.append(SPACE).append(globleOp).append(SPACE);
			}
		 }
		String trimConditions = sb.toString().trim();
		int lastIndex = deleteUnusefulOp(globleOp, trimConditions);
		return trimConditions.substring(0, lastIndex);
	}
	


	private  int deleteUnusefulOp(String globleOp, String trimConditions) {
		int lastIndex = trimConditions.length();
		if (trimConditions.endsWith(UPPER_OR) || trimConditions.endsWith(UPPER_AND)) {
			if (StringUtils.endsWith(UPPER_OR, globleOp)) {
				lastIndex = trimConditions.lastIndexOf(UPPER_OR);
			} else {
				lastIndex = trimConditions.lastIndexOf(UPPER_AND);
			}
		}
		return lastIndex;
	}
	

	public  String getSearchCondition(AdvanceQueryDto queryDto,String joinSQL) {
		StringBuilder sb = new StringBuilder();
		String globleOp =queryDto.getGroupOp();
		// 如果包括groups，证明可能是混合，如果没有肯定不是混合
		boolean containsGroups=queryDto.getGroups()!=null&&queryDto.getGroups().size()>0;
		if (!containsGroups) {
			String cond = parsePlainQuery(sb, queryDto, globleOp);
			String queryString = null;
			if(StringUtils.isBlank(cond)){
				queryString = joinSQL+cond;
			}else{
				queryString = joinSQL+LEFT_BRACKET+cond+RIGHT_BRACKET;
			}
			
			return queryString;
		} else {
			parseHybridQuery(sb, queryDto);
			return joinSQL+LEFT_BRACKET+sb.toString()+RIGHT_BRACKET;
		}
	}
	public void addOrderByStr(StringBuilder sb, AdvanceQueryDto queryDto) {
		String orderBy = queryDto.getSortBy();
		String ascending = queryDto.getAscending();
		// TODO:just now only support one column sort as the front api only
		// support one
		if (StringUtils.isNotEmpty(orderBy)) {
			sb.append(" order by ");
			if (orderBy.indexOf(COMMA) == -1 || ascending.split(COMMA).length == 1) {
				sb.append(orderBy).append(SPACE).append(Boolean.valueOf(queryDto.ascending.trim()) ? ASCENDING : DESCENDING);
			} else {
				String[] orderArr = orderBy.split(COMMA);
				String[] ascendingArr = ascending.split(COMMA);
				if (orderArr.length == ascendingArr.length) {
					for (int i = 0; i < orderArr.length; i++) {
						sb.append(orderArr[i]).append(SPACE).append(Boolean.valueOf(ascendingArr[i].trim()) ? ASCENDING : DESCENDING);
						if (i != orderArr.length - 1) {
							sb.append(COMMA);
						}
					}
				}
			}
		}
	}
	
}
