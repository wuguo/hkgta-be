package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.CorporateMemberDto;
import com.sinodynamic.hkgta.dto.membership.SearchSpendingSummaryDto;
import com.sinodynamic.hkgta.entity.crm.CorporateMember;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CorporateMemberDao extends IBaseDao<CorporateMember>{

	public Serializable addCorporateMember(CorporateMember corporateMember);
	
	public List<CorporateMember> getAllCorporateMember(Long corporateId);
	
	public List<CorporateMember> getAllCorporateMemberByStatus(Long corporateId,String status);
	
	public boolean updateCorporateMember(CorporateMember corporateMember);
	
	public boolean updateStatus(String status, Long customerId);
	
	public boolean updateStatus(String status, Long customerId, String loginUserId);

	public ListPage<CorporateMember> getSpendingSummary(ListPage<CorporateMember> page,
			SearchSpendingSummaryDto dto);
	
	public List<CorporateMemberDto> getMemberDropList(Long corporateId);
	
	public List<CorporateMemberDto> checkCreditLimit(Long corporateId);
	
	public CorporateMember getCorporateMemberById(Long customerId);

	public BigDecimal getTotalSpending(ListPage<CorporateMember> page,SearchSpendingSummaryDto dto);
	
	public BigDecimal getRemainAvailableCreditLimit(Long corporateId, Long customerId);
	
	public BigDecimal getAllocatedCreditAmountByCorporateId(Long corporateId);
}
