package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;

@Repository
public class ServicePlanFacilityDaoImpl extends GenericDao<ServicePlanFacility>
		implements ServicePlanFacilityDao {

	@Override
	public ServicePlanFacility getServicePlanFacilityByCompositeKey(int planNo,
			String typeCode) {
		Query query = getCurrentSession().createQuery("from ServicePlanFacility s where s.id.servicePlanId=? and s.id.facilityTypeCode=? ");
		query.setInteger(0, planNo);
		query.setString(1, typeCode);
		return (ServicePlanFacility) query.list().get(0);
	}
	
	
	public List<ServicePlanFacility> getListByServicePlanNo(Long planNo){
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planNo);
		return getByHql(" from ServicePlanFacility m where m.id.servicePlanId = ? ",param);
	}

}
