package com.sinodynamic.hkgta.dao.pos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuCat;

@Repository
public class RestaurantMenuCatDaoImpl extends GenericDao<RestaurantMenuCat> implements RestaurantMenuCatDao
{

	@Override
	public List<RestaurantMenuCat> getRestaurantMenuCatListByRestaurantId(String restaurantId)
	{
		return this.getByCol(RestaurantMenuCat.class, "restaurantMaster.restaurantId", restaurantId, null);
	}

	@Override
	public String getRestaurantListByRestaurantId()
	{
		String sql = "select * from (select a.cat_id, a.category_name, (select count(1) from restaurant_menu_item b where a.cat_id = b.cat_id and a.restaurant_id = b.restaurant_id) as cnt"
				+ "  from restaurant_menu_cat a where "
				+ "  a.restaurant_id = ? and a.status = 'ACT' ) a where 1=1 ";
		return sql;
	}

	@Override
	public RestaurantMenuCat getRestaurantMenuCat(String restaurantId, String catId) {
		String hql = "from RestaurantMenuCat where restaurantMaster.restaurantId = ? and catId = ? ";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(restaurantId);
		params.add(catId);
		return (RestaurantMenuCat) super.getUniqueByHql(hql, params);
	}

}
