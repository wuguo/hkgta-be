package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.HelperPassType;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface HelperPassTypeDao extends IBaseDao<HelperPassType> {
	
	HelperPassType getById(Long id) throws HibernateException;
	
	HelperPassType getByName(String tempPassName) throws HibernateException;
	
	List<HelperPassType> getAllPassType() throws HibernateException;
	
	ListPage<HelperPassType> getPassTypeList(ListPage<HelperPassType> page, String hql, String counthql, List<Serializable> param) throws HibernateException;
	
	boolean deleteById(Long id) throws HibernateException;
	
	public List<HelperPassType> getExpiryPassTypeByCurdate(String expireDate);
	
	public boolean updatePassType(HelperPassType passType);

}
