package com.sinodynamic.hkgta.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * 
 * @author Angus_Zhu
 *
 */
@Repository
public interface CommonDataQueryBySQLDao extends  CommonDataQueryDao, IBaseDao {
	
	  String getSearchCondition(AdvanceQueryDto queryDto,String joinSQL);

		void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page,Class  clazz);

		void getAdvanceQuerySpecificResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page,Class  clazz);

		void getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto);

		void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page,Class  clazz, List<Object> paramList);

		void getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String sql, ListPage page, Class dto, List<Object> paramList);

		void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, Map<String, Type> typeMap);

		void getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class dto, Map<String, Type> typeMap);
}
