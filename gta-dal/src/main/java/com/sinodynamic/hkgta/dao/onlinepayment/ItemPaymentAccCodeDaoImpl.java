package com.sinodynamic.hkgta.dao.onlinepayment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.onlinepayment.ItemPaymentAccCode;

@Repository
public class ItemPaymentAccCodeDaoImpl extends GenericDao<ItemPaymentAccCode>implements ItemPaymentAccCodeDao {
	
	@Override
	public List<ItemPaymentAccCode> getItemPaymentAccCodeByStatus(String status,String categoryCode) 
	{
		// TODO Auto-generated method stub
		String hqlstr=" from ItemPaymentAccCode where 1=1 ";
		List<Serializable> params=new ArrayList<>();
		if(StringUtils.isNotEmpty(status))
		{
			hqlstr+=" and status =?";
			params.add(status);
		}
		if(StringUtils.isNotEmpty(categoryCode))
		{
			hqlstr+=" and categoryCode =?";
			params.add(categoryCode);
		}else{
			hqlstr+=" and categoryCode <> 'SPA'";
		}
		hqlstr+=" order by accountCode ";
		return getByHql(hqlstr, params);
	}

}
