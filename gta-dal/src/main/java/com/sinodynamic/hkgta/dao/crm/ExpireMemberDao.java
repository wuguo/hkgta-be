package com.sinodynamic.hkgta.dao.crm;

import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.ExpiredMemberDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * @author Junfeng_Yan
 * @since 2015/4/29
 *
 */
public interface ExpireMemberDao extends IBaseDao<ExpiredMemberDto>{

	public ListPage<ExpiredMemberDto> getExpiredMember(ListPage<ExpiredMemberDto> page, String countSql,
			String sql, List<?> param, ExpiredMemberDto dto,Map<String, org.hibernate.type.Type> map) throws HibernateException;
}
