package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailAttach;

@Repository
public class CustomerEmailAttachDaoImpl extends GenericDao<CustomerEmailAttach> implements CustomerEmailAttachDao {

	@Override
	public void saveCustomerEmailAttach(CustomerEmailAttach cea) {
		
		save(cea);
	}

}
