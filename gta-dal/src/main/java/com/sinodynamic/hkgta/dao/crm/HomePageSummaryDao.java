package com.sinodynamic.hkgta.dao.crm;

import java.math.BigDecimal;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;

public interface HomePageSummaryDao extends IBaseDao<String> {
	
	public int countCustomerProfile() throws HibernateException;

	public int countEnrollmentsWithActiveMembers(String staffName) throws HibernateException;
	
	public int countPresentation() throws HibernateException;
	
	public int countRenewal() throws HibernateException;
	
	public int countPendingPayment() throws HibernateException;
	
	public BigDecimal countBalanceDue(Long orderNo) throws HibernateException;
	
	public Boolean countEnrollmentRemark(String usedId);
	
	public Boolean countLeadRemark(String usedId);
}
