package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdditionalInfoDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAccountInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerCheckExsitDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentSearchDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberInfoDto;
import com.sinodynamic.hkgta.dto.crm.EnrollmentListResponseDto;
import com.sinodynamic.hkgta.dto.crm.LeadListResponseDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberInfoDto;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.pagination.ListPage;


@Repository("customerProfile")
public class CustomerProfileDaoImpl extends GenericDao<CustomerProfile> implements CustomerProfileDao, AdvanceQueryConditionDao{
	public ListPage<CustomerProfile> getCustomerProfileList(ListPage<CustomerProfile> page, CustomerProfileDto dto){
		String sql = "SELECT t.lead,t.createDate,t.customerId,t.contactEmail,t.status,t.salesPerson,t.companyName,t.contactClassCode,m.effective_date as effectiveDate    FROM "
			  + " (SELECT concat(p.salutation, ' ',p.given_name, ' ', p.surname) as lead,"
			  + " p.create_date as createDate, p.customer_id as customerId, p.contact_email  as contactEmail, e.status as status, e.sales_follow_by as salesPerson,p.company_name as companyName,p.contact_class_code as contactClassCode "
			  + "FROM customer_profile p, customer_enrollment e "
			  + " WHERE p.customer_id = e.customer_id and (p.is_deleted != 'Y' OR p.is_deleted IS NULL)) t  "
			  + "LEFT JOIN member m ON m.customer_id = t.customerId WHERE 1=1 and ((t.status = 'OPN' and m.member_type is null) or m.member_type ='IPM' "
			  + ") <QueryConditin> ";
		
		List<Object> param = new ArrayList<Object>();
		StringBuilder conditions = new StringBuilder(" ");

		if(CommUtil.notEmpty(dto.getSearchText())){
			conditions.append(" and ( t.lead like ? or t.companyName like ? ) ");
			param.add("%"+dto.getSearchText()+"%");
			param.add("%"+dto.getSearchText()+"%");
		}
		if(CommUtil.notEmpty(dto.getStatus())){
			conditions.append(" and t.status = ? ");
			param.add(dto.getStatus());
		}
		if("true".equals(dto.getIsMyClient())){
			conditions.append(" and t.salesPerson = ? ");
			param.add(dto.getCreateBy());
		}
		if(!"ALL".equalsIgnoreCase(dto.getContactClassCode())){
			conditions.append(" and t.contactClassCode = ? ");
			param.add(dto.getContactClassCode());
		}
		if(CommUtil.notEmpty(dto.getSortBy())){
			String orderByFiled = dto.getSortBy().trim();
			if("true".equals(dto.getIsAscending())){
				page.addAscending(orderByFiled);
			}else if("false".equals(dto.getIsAscending())){
				page.addDescending(orderByFiled);
			}
		}
		
		sql = sql.replace("<QueryConditin>", conditions.toString());
		String countSql = "select count(1) from ( " + sql + ") tt";
		return listBySqlDto(page, countSql, sql, param, new LeadListResponseDto());
	}

	public Serializable addCustomerProfile(CustomerProfile cp) {
		return this.save(cp);
	}

	public void deleteCustomerProfile(CustomerProfile cp) {
		this.delete(cp);
		
	}

	public void updateCustomerProfile(CustomerProfile cp) {
        this.update(cp);	
	}

	public CustomerProfile getCustomerProfile(CustomerProfile t) {
		return this.get(t, t.getCustomerId());
	}
	
	public CustomerProfile getCustomerProfileByCustomerId(String customerId) {
        String hqlstr = "from CustomerProfile where customerId = "
				+ customerId;
		CustomerProfile customerProfile = (CustomerProfile) getUniqueByHql(hqlstr);
		return customerProfile;
	}

	public CustomerProfile getCustomerProfileByCustomerId(Long customerId) {
        String hqlstr = "from CustomerProfile where customerId = ?";
        List<Serializable> param = new ArrayList<Serializable>();
        param.add(customerId);
		return (CustomerProfile) getUniqueByHql(hqlstr,param);
	}
	
	public CustomerProfile getById(Long customerId) {
		CustomerProfile profile=super.get(CustomerProfile.class, customerId);
		if(null!=profile){
			CustomerProfileDto dto=this.getCustomerProfilePhotoAndSinature(customerId);
				if(StringUtils.isNotEmpty(dto.getPortraitPhoto()))
				{
					profile.setPortraitPhotoIdentiy(true);
				}
				if(StringUtils.isNotEmpty(dto.getSignature()))
				{
					profile.setSignatureIdentiy(true);
				}
		}
        return profile;
	}

	public boolean checkAvailablePassportNo(Long customerId,String passportType,
			String passportNo) {
		String hqlstr = "from CustomerProfile cp where cp.passportType = ? and cp.passportNo = ? and (cp.isDeleted is null or cp.isDeleted = 'N') ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(passportType);
		listParam.add(passportNo);
		List<CustomerProfile> customerProfileList = getByHql(hqlstr,listParam);
		
		if (customerProfileList != null && customerProfileList.size() > 0) {
			for (CustomerProfile temp : customerProfileList) {
				if (temp.getCustomerId().equals(customerId)) return true;
			}
			return false;
		}
		return true;
	}


	/**
	 * This method is used to get the customer profile and service account
	 * information
	 */
	@Override
	public MemberInfoDto getCustomerInfo(Long customerId) {

		String sql1 = "SELECT s.customer_id AS customerId, s.salutation, s.surname, s.given_name AS givenName, s.surname_nls AS surnameNls, "
				+ "s.given_name_nls AS givenNameNls, if(s.gender<>'M' and s.gender<>'F',null,s.gender) as gender, s.phone_mobile AS phoneMobile, s.phone_business AS phoneBusiness, "
				+ "s.phone_home AS phoneHome, s.contact_email AS contactEmail, s.postal_address1 AS postalAddress1, s.postal_address2 AS postalAddress2, s.passport_type AS passportType, "
				+ "s.passport_no AS passportNo, DATE_FORMAT(s.date_of_birth, '%Y/%m/%d') AS dateOfBirth, s.nationality, s.business_nature AS businessNature, "
				+ "b.acc_cat AS serviceAccCategory, d.plan_name AS servicePlan "
				+ "FROM (SELECT a.*  FROM customer_profile a WHERE a.customer_id = ?) s "
				+ "LEFT JOIN customer_service_acc b ON s.customer_id = b.customer_id and b.status='ACT' "
				+ "LEFT JOIN customer_service_subscribe c ON b.acc_no=c.acc_no "
				+ "LEFT JOIN service_plan d ON c.service_plan_no=d.plan_no order by b.expiry_date asc limit 1";

		Query query1 = getCurrentSession()
				.createSQLQuery(sql1)
				.setResultTransformer(
						Transformers.aliasToBean(new MemberInfoDto().getClass()));
		query1.setParameter(0, customerId);
		
		MemberInfoDto dto = (MemberInfoDto) query1.list().get(0);
		
		
	
//		Additional info
		String sql2 = "SELECT a.caption,  b.customer_input AS inputData "
				+ "FROM customer_addition_info b "
				+ "left join customer_addition_info_caption a on a.caption_id = b.caption_id where b.customer_id = ? "
				+ "order by a.caption_id";
		
		
		Query query2 = getCurrentSession()
				.createSQLQuery(sql2)
				.setResultTransformer(
						Transformers.aliasToBean(new AdditionalInfoDto().getClass()));
		query2.setParameter(0, customerId);
		
		List<AdditionalInfoDto> additionInfo = new ArrayList<AdditionalInfoDto>();
		
		additionInfo = query2.list();
		System.out.println("Size: "+additionInfo.size());
		
		dto.setAdditionalInfo(additionInfo);

		return dto;

	}

	@Override
	public CustomerCheckExsitDto checkAvailableByPassportTypeAndPassportNo(String passportType,String passportNO) {
		
		String sql = "select m.academy_no as academyID, c.is_deleted as isDeleted,"
				+ "m.superior_member_id as supMemberId,"
				+ "m.member_type as memberType,"
				+ "m.status,"
				+ "c.customer_id as customerID,"
				+ "c.surname,"
				+ "c.given_name as givenName,"
				+ "c.salutation, "
				+ " case when ce.status is not null then ce.status else ce1.status end as enrollStatus, "
		        + " case when ceLog.status_from is not null then ceLog.status_from else ceLog1.status_from end as preEnrollStatus, "
		        + " c.passport_no as passportNo, "
				+ "concat(sp.given_name,' ',sp.surname) as followPerson "
				+ "from customer_profile c "
		        + "left join  member m on m.customer_id = c.customer_id  "
				+ "left join customer_enrollment ce on ce.customer_id = c.customer_id "
				+ "left join customer_enrollment ce1 on ce1.customer_id = m.superior_member_id "
				+ "left join customer_enrollment_log ceLog on ceLog.enroll_id = ce.enroll_id and ceLog.status_update_date = (select max(log.status_update_date) from customer_enrollment_log log where log.enroll_id = ce.enroll_id) "
				+ "left join customer_enrollment_log ceLog1 on ceLog1.enroll_id = ce1.enroll_id and ceLog1.status_update_date = (select max(log1.status_update_date) from customer_enrollment_log log1 where log1.enroll_id = ce1.enroll_id) "
				+ "left join staff_profile sp on sp.user_id = ce.sales_follow_by "
				+ "where (c.passport_no = ? or c.passport_no like '" + passportNO + "-%')"
				+ "and c.passport_type = ? "
				+ "order by c.passport_no ";
				
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(passportNO);
		param.add(passportType);
		
		List<CustomerCheckExsitDto> result = getDtoBySql(sql, param, CustomerCheckExsitDto.class);	
		
		//sql是根据passport_no升序排列，也就说如果存在not archive的member，一定是The first record,如果第一条是archive的，那就说明所以记录都是archive的，取最后一条即可（最后一条肯定是最后archive的）
		if(result != null && result.size() != 0){
			CustomerCheckExsitDto dto = result.get(0);
			if(!dto.getPassportNo().contains("-")){
				dto.setIsArchiveMember(false);
				return dto;
			}else{
				dto = result.get(result.size()-1);
				dto.setIsArchiveMember(true);
				return dto;
			}
		}
		
		return null;
	}


	@Override
	public CustomerProfile getCustomerProfileByPassportTypeAndPassportNo(String passportType, String passportNO) {
		return getUniqueByCols(CustomerProfile.class, new String[]{"passportNo","passportType"}, new String[]{passportNO,passportType});
	}

	@Override
	public CustomerProfile getUniqueCustomerProfileByPassportTypeAndPassportNo(String passportType, String passportNO) {
		List<CustomerProfile> list =getByCols(CustomerProfile.class, new String[]{"passportNo","passportType"}, new String[]{passportNO,passportType},null);
		return list!=null&&list.size()>0?list.get(0):null;
	}

	@Override
	public ListPage<CustomerProfile> getCustomerEnrollments(ListPage<CustomerProfile> page,
			CustomerEnrollmentSearchDto dto) {
		String sql = "SELECT t.customerId,t.enrollName,t.planNo,t.`status`,t.enrollDate,t.enrollId,t.contactEmail,t.salesPerson,t.servicePlan,t.companyName,m.effective_date as effectiveDate   FROM ( "
				+"SELECT p.customer_id as customerId,concat(p.salutation, ' ',p.given_name, ' ', p.surname) as enrollName,"
				+ "e.status as status,e.enroll_date as enrollDate,e.enroll_id as enrollId,p.contact_email as contactEmail,e.sales_follow_by as salesPerson," 
				+ "(select s.plan_name from service_plan s where s.plan_no = e.subscribe_plan_no) as servicePlan,e.subscribe_plan_no as planNo,p.company_name as companyName "
				+ " from customer_profile p , customer_enrollment e where e.customer_id = p.customer_id and (p.is_deleted != 'Y' OR p.is_deleted IS NULL) and e.status != 'OPN') t "
				+ "LEFT JOIN member m ON m.customer_id = t.customerId "
				+ "WHERE 1=1 and (m.member_type = 'IPM' ) <QueryConditin> <OrderBy>";
		
		List<Object> param = new ArrayList<Object>();
		StringBuilder conditions = new StringBuilder(" ");
		StringBuilder orderBy = new StringBuilder("");
		
		String searchText = dto.getSearchText().trim();
		if(CommUtil.notEmpty(dto.getSearchText())){
			conditions.append(" and ( t.enrollName like ? or t.companyName like ? ) ");
			param.add("%"+searchText+"%");
			param.add("%"+searchText+"%");
		}
		String customerStatus = dto.getCustomerStatus();
		String enrollStatus = dto.getEnrollStatus();
		if(CommUtil.notEmpty(customerStatus)){
			if("ALL".equals(customerStatus)){
				if (!CommUtil.notEmpty(enrollStatus) || "ALL".equals(enrollStatus)) {
					//it means search all status,so does not need this condition.
				}else {
					conditions.append(" and t.status = ? ");
					param.add(enrollStatus);
				}
			}else if("ENR".equals(customerStatus)){
				if (!CommUtil.notEmpty(enrollStatus) || "ALL".equals(enrollStatus)) {
					conditions.append(" and (t.status = 'NEW' or  t.status = 'ANC' or t.status = 'PYF')");
				}else {
					conditions.append(" and t.status = ? ");
					param.add(enrollStatus);
				}
			}else if("CMP".equals(customerStatus)){
				if (!CommUtil.notEmpty(enrollStatus) || "ALL".equals(enrollStatus)) {
					conditions.append(" and t.status = 'CMP' ");
				}else {
					conditions.append(" and t.status = ? ");
					param.add(enrollStatus);
				}
			}else if("NACT".equals(customerStatus)){
				if (!CommUtil.notEmpty(enrollStatus) || "ALL".equals(enrollStatus)) {
					conditions.append(" and (t.status = 'REJ' or  t.status = 'CAN')");
				}else {
					conditions.append(" and t.status = ? ");
					param.add(enrollStatus);
				}
			}else {
				conditions.append(" and 1=2 ");//if the customerStatus is invalid,there will can not get data.
			}
		}else {
			if (!CommUtil.notEmpty(enrollStatus) || "ALL".equals(enrollStatus)) {
				//it means search all status,so does not need this condition.
			}else {
				conditions.append(" and t.status = ? ");
				param.add(enrollStatus);
			}
		}

		if(CommUtil.notEmpty(dto.getIsMyClient())&&Boolean.parseBoolean(dto.getIsMyClient())){
			conditions.append(" and t.salesPerson = ? ");
			param.add(dto.getUserId());
		}
		if(CommUtil.notEmpty(dto.getSortBy())){
			String orderByFiled = dto.getSortBy().trim();
			orderBy.append(" order by ");
			if("true".equals(dto.getIsAscending())){
				orderBy.append(orderByFiled + " asc ");
			}else{
				orderBy.append(orderByFiled + " desc ");
			}
		}
		sql = sql.replace("<QueryConditin>", conditions.toString()).replace("<OrderBy>", orderBy.toString());
		String countSql = "SELECT COUNT(1) from ( " + sql + " ) tt";
		return listBySqlDto(page, countSql, sql, param, new EnrollmentListResponseDto());
	}
	
	@Override
	public List<CustomerProfile> quickSearchCustomerIdByAcademyIdOrPassportNo(String number){
		String sql="SELECT cc.customer_id as customerId, cc.surname as surname, cc.given_name as givenName, cc.salutation as salutation, cc.member_type as memberType,cc.academy_no as academyNo FROM"
				+ " (SELECT c.customer_id,c.surname,c.given_name,c.salutation,c.passport_no,m.academy_no, m.member_type FROM customer_profile c"
				+ " LEFT JOIN member m ON c.customer_id = m.customer_id UNION"
				+ " SELECT c.customer_id,c.surname,c.given_name,c.salutation,c.passport_no,m.academy_no, m.member_type FROM customer_profile c"
				+ " RIGHT JOIN member m ON c.customer_id = m.customer_id) cc where cc.passport_no = ? or cc.academy_no = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(number);
		param.add(number);
		List<CustomerProfile> customer =  this.getDtoBySql(sql, param, CustomerProfile.class);
		return customer;
		}
	

	public List<CustomerProfile> getCustomerIdByUserId(String userId){
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		String hqlstr = " select c.customerId as customerId, c.surname as surname, c.givenName as givenName from Member m, CustomerProfile c where m.userId = ? "+" and c.customerId  = m.customerId ";
		List<CustomerProfile> member = this.getDtoByHql(hqlstr, param, CustomerProfile.class);
		return member;
	}



	/**
	 * This method is used to get the dependent member profile
	 */

	@Override
    public DependentMemberInfoDto getDependentMemberInfoByCustomerId(Long customerId){
//    	String hql="select c.customerId as customerId,concat(c.salutation,' ',c.givenName,' ', c.surname ) as memberName, c.nationality as nationality, "
//    			+ "c.phoneMobile as phoneMobile, c.dateOfBirth as dateOfBirth, c.portraitPhoto as portraitPhoto, c.contactEmail as contactEmail, m.firstJoinDate as firstJoinDate, "
//    			+ "m.academyNo as academyNo from Member m, CustomerProfile c where m.customerId = ? and c.customerId  = m.customerId ";
	
		String sql="SELECT c.customer_id AS customerId,CONCAT(c.salutation,' ',c.given_name,' ', c.surname ) AS memberName, c.nationality AS nationality, "
				+ "c.phone_mobile AS phoneMobile, c.date_of_birth AS dateOfBirth, c.portrait_photo AS portraitPhoto,"
    			+ "c.contact_email AS contactEmail, m.first_join_date AS firstJoinDate, "
    			+ "m.academy_no AS academyNo "
    			+ "FROM member m, customer_profile c WHERE m.customer_id = ? AND c.customer_id  = m.customer_id";
    	List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		List<DependentMemberInfoDto> member  = getDtoBySql(sql, param, DependentMemberInfoDto.class);
		if(member==null) return null;
		return member.get(0);		
    }
	
	public List<DependentMemberInfoDto> getDependentMemberInfoListBySuperiorId(Long superiorCustomerId){
//    	String hql="select c.customerId as customerId,concat(c.salutation,' ',c.givenName,' ', c.surname ) as memberName, c.nationality as nationality, "
//    			+ "c.phoneMobile as phoneMobile, c.dateOfBirth as dateOfBirth, c.portraitPhoto as portraitPhoto, c.contactEmail as contactEmail, m.firstJoinDate as firstJoinDate, "
//    			+ "m.academyNo as academyNo from Member m, CustomerProfile c where m.superiorMemberId = ? and c.customerId  = m.customerId and c.isDeleted != ? ";
    	String sql="SELECT c.customer_id AS customerId,CONCAT(c.salutation,' ',c.given_name,' ', c.surname ) AS memberName, c.nationality AS nationality,"
    			+ "c.phone_mobile AS phoneMobile, c.date_of_birth AS dateOfBirth, c.portrait_photo AS portraitPhoto, c.contact_email AS contactEmail, m.first_join_date AS firstJoinDate,"
    			+ "m.academy_no AS academyNo "
    			+ "FROM member m, customer_profile c WHERE m.superior_member_id = ? AND c.customer_id  = m.customer_id AND c.is_deleted != ?";
    	List<Serializable> param = new ArrayList<Serializable>();
		param.add(superiorCustomerId);
		param.add("Y");
		return getDtoBySql(sql, param, DependentMemberInfoDto.class);
	}
	
	public CustomerProfile getCorporateMemberDetailByCustomerId(Long customerId){
		String hqlstr = " select m.superiorMemberId as superiorMemberId, m.memberType as memberType, cm.corporateProfile.corporateId as corporateId, mlr.numValue as totalCreditLimit "
				+ "from Member m, CorporateMember cm, MemberLimitRule mlr where mlr.customerId = m.customerId and cm.customerId = m.customerId and m.customerId = ? "
				+ " and date_format( ?,'%Y-%m-%d')>=mlr.effectiveDate and date_format( ?,'%Y-%m-%d') <= mlr.expiryDate";
    	List<Serializable> param = new ArrayList<Serializable>();
    	Date  currentDate = new Date();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		List<CustomerProfile> customerProfile  = getDtoByHql(hqlstr, param, CustomerProfile.class);
		if(customerProfile==null) return null;
		return (CustomerProfile) customerProfile.get(0);	
	}
	
	/**
	 * Method to check if the typed contact email exists or not for update
	 * (The current customer id is ignored)
	 * @param contactEmail
	 * @param customerId
	 * @return false if exists, true if not exists
	 */
	public boolean checkAvailableContactEmail(String contactEmail, Long customerId){
		String hqlstr = " from CustomerProfile c where c.contactEmail = ? and (c.isDeleted is null or c.isDeleted = 'N') ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(contactEmail);
		List<CustomerProfile> customerProfileList = getByHql(hqlstr,listParam);
		
		if(customerProfileList!=null&&customerProfileList.size()>0){
			for (CustomerProfile temp : customerProfileList) {
				if (temp.getCustomerId().equals(customerId)) return true;
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Method to return the academy no's reservation status
	 * @param customerId
	 * @return "FALSE" if REMOVE, TRUE if not RESERVE
	 */
	public MemberDto getAcademyNoReservationStatus(Long customerId){
		String hqlstr = " select m from Member m, CustomerProfile cp, CustomerEnrollment ce "
				+ "where ce.status = ? and ce.customerId = cp.customerId and cp.customerId = m.customerId and cp.customerId = ? and m.academyNo != null ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add("NEW");
		param.add(customerId);
		Member member = (Member) getUniqueByHql(hqlstr, param);
		MemberDto dto = new MemberDto();
		if(member==null){
			dto.setAcademyNoReserveStatus("TRUE");
		}else{
			dto.setAcademyNo(member.getAcademyNo());
			dto.setAcademyNoReserveStatus("FALSE");
		}
		dto.setCustomerId(customerId);
		return dto;
	}

	@Override
	public String getNameByUserId(String userId) {
		String userType = null;
		String userName = null;
		String hqlstr = "SELECT userType from UserMaster u where u.userId = '"+userId+"'";
		userType = (String)getUniqueByHql(hqlstr);
		if ("CUSTOMER".equalsIgnoreCase(userType)) {
			hqlstr = "SELECT  CONCAT(c.givenName,' ',c.surname) from CustomerProfile c,Member m where m.customerId = c.customerId and m.userId =  '"+userId+"'";
			userName = (String)getUniqueByHql(hqlstr);
		}else if ("STAFF".equalsIgnoreCase(userType)) {
			hqlstr = "SELECT  CONCAT(s.givenName,' ',s.surname) from StaffProfile s where s.userId =  '"+userId+"'";
			userName = (String)getUniqueByHql(hqlstr);
		}
		return userName;
	}
	
	@Override
	public Integer updateProfileAndSignatureFile(
			CustomerProfile customerProfile, Long customerProfileVersion)
			throws Exception {

		StringBuilder sql = new StringBuilder(
				" update customer_profile cp set ");

		List<Serializable> params = new ArrayList<Serializable>();
		if (!StringUtils.isEmpty(customerProfile.getPortraitPhoto())) {
			sql.append("cp.portrait_photo = ?,");
			params.add(customerProfile.getPortraitPhoto());
		}

		if (!StringUtils.isEmpty(customerProfile.getSignature())) {
			sql.append("cp.signature = ?,");
			params.add(customerProfile.getSignature());
		}

		if (customerProfileVersion != null
				&& customerProfile.getCustomerId() != null) {
			sql.append("cp.ver_no = cp.ver_no + 1 where cp.customer_id = ? and cp.ver_no = ? ");
			params.add(customerProfile.getCustomerId());
			params.add(customerProfileVersion);
		}

		return updateBySQL(sql.toString(), params);

	}

	@Override
	public Integer getVersionById(Long customerId) {

		StringBuilder sql = new StringBuilder();
		List<Serializable> params = new ArrayList<Serializable>();
		sql.append("select cp.ver_no version from customer_profile cp where cp.customer_id = ?");
		params.add(customerId);

		Integer version = (Integer) getUniqueBySQL(sql.toString(), params);
		// List<CustomerProfile> customerProfiles = getDtoBySql(sql.toString(),
		// params, CustomerProfile.class);

		return version;

	}
	
	@Override
	public AgeRange getServicePlanAgeRange(Long planNo) throws Exception {
		
		StringBuilder sql = new StringBuilder();
		List<Serializable> params = new ArrayList<Serializable>();
		
		sql.append("select ar.age_range_code ageRangeCode, ar.age_from ageFrom, ar.age_to ageTo, ar.description description from age_range ar ");
		sql.append("left join service_plan_pos spp on spp.age_range_code = ar.age_range_code where spp.plan_no = ? ");
		params.add(planNo);
		
		List<AgeRange> ageRanges = getDtoBySql(sql.toString(), params, AgeRange.class);
		
		if (ageRanges == null) return null;
		return ageRanges.get(0);
		
	}
	
	public CustomerProfile getLimitedCustomerInfoForTablet(Long customerId){
		String sql = "SELECT\n" +
				"  cp.salutation as salutation,\n" +
				"  cp.surname as surname,\n" +
				"  cp.given_name as givenName,\n" +
				"  CASE cp.gender\n" +
				"    when 'M' then 'Male'\n" +
				"    when 'F' then 'Female'\n" +
				"    else ''\n" +
				"  end as gender,\n" +
				"	IFNULL(cp.greeting_info,'') AS greetingInfo,\n" +
				"	IFNULL(cp.phone_mobile,'') AS phoneMobile,\n" +
				"  IFNULL(cp.internal_remark,'') as internalRemark,\n" +
				"  IF(cp.passport_type = 'VISA','Passport',cp.passport_type) as passportType,\n" +
				"  IFNULL(cp.passport_no,'') as passportNo,\n" +
				"  cp.date_of_birth as dateOfBirth,\n" +
				"  cp.portrait_photo as portraitPhoto,\n" +
				"  cp.customer_id as customerId\n"+
				"FROM\n" +
				"	customer_profile cp\n" +
				"WHERE\n" +
				"	cp.customer_id = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		List<CustomerProfile> list = getDtoBySql(sql, param, CustomerProfile.class);
		if(list!=null&&list.size()>0) {
			return list.get(0);
		}else{
			return new CustomerProfile();
		}
	}
	
	public CustomerProfile getPrimaryDetailForAutoFillingDependent(Long customerId){
		String sql = "SELECT\n" +
				"	cp.phone_mobile AS phoneMobile,\n" +
				"	cp.phone_business AS phoneBusiness,\n" +
				"	cp.phone_home AS phoneHome,\n" +
				"	cp.contact_email AS contactEmail,\n" +
				"	cp.contact_email2 AS contactEmail2,\n" +
				"	cp.postal_address1 AS postalAddress1,\n" +
				"	cp.postal_address2 AS postalAddress2,\n" +
				"	cp.postal_district AS postalDistrict\n" +
				"FROM\n" +
				"	customer_profile cp\n" +
				"WHERE\n" +
				"	cp.customer_id = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		List<CustomerProfile> list = getDtoBySql(sql, param, CustomerProfile.class);
		if(list!=null&&list.size()>0) {
			return list.get(0);
		}else{
			return new CustomerProfile();
		}
	}
	
	public CustomerAccountInfoDto getPreviewCardInfoByNearestServiceAccount(Long customerId){
		String sql = "SELECT\n" +
				"	inter.expiry_date AS expiryDate,\n" +
				"	CONCAT_WS(\n" +
				"		' ',\n" +
				"		cp.given_name,\n" +
				"		cp.surname\n" +
				"	) AS memberName,\n" +
				"	m.academy_no AS academyNo\n" +
				"FROM\n" +
				"	customer_service_acc inter,\n" +
				"	customer_service_subscribe sub,\n" +
				"	member m,\n" +
				"	customer_profile cp\n" +
				"WHERE\n" +
				"	sub.acc_no = inter.acc_no\n" +
				"AND m.customer_id = inter.customer_id\n" +
				"AND cp.customer_id = m.customer_id\n" +
				"AND (\n" +
				"	m.customer_id = ?\n" +
				"	OR m.customer_id = (\n" +
				"		SELECT\n" +
				"			supm.superior_member_id\n" +
				"		FROM\n" +
				"			member supm\n" +
				"		WHERE\n" +
				"			supm.customer_id = ?\n" +
				"	)\n" +
				")\n" +
				"AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"ORDER BY\n" +
				"	inter.expiry_date DESC\n" +
				"LIMIT 0,\n" +
				" 1";
		
		List<Serializable> param  = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(customerId);
		param.add(new Date());
		
		List<CustomerAccountInfoDto> accountInfoList = getDtoBySql(sql, param, CustomerAccountInfoDto.class);
		if(accountInfoList!=null&&accountInfoList.size()>0){
			return accountInfoList.get(0);
		}else{
			return null;
		}
	}
	
	/**
	 * 根据类型获取查询Overview列表语句
	 * @param type
	 * @return
	 */
	@Override
	public String getOverviewList(String type) {
		String sql = "SELECT  "
		+ " cp.customer_id as customerId, "                                                                      
		+ " m.academy_no AS academyNo, "                                                                         
		+ "	CONCAT_WS(" 
		+ "		' ',cp.salutation," 
		+ "		cp.given_name," 
		+ "		cp.surname" 
		+ "	) AS memberName,"                                                                      
		+ " CASE cp.gender "                                                                                     
		+ " when 'M' then 'Male' "                                                                               
		+ " when 'F' then 'Female' "                                                                             
		+ " else '' "                                                                                            
		+ " end as gender, "                                                                                     
		+ " IFNULL(cp.greeting_info,'') AS greetingInfo, "                                                       
		+ " IFNULL(cp.phone_mobile,'') AS phoneMobile, "                                                         
		+ " IFNULL(cp.internal_remark,'') as internalRemark, "                                                   
		+ " IF(cp.passport_type = 'VISA','Passport',cp.passport_type) as passportType, "                         
		+ " IFNULL(cp.passport_no,'') as passportNo, "                                                           
		+ " cp.date_of_birth as dateOfBirth, "                                                                   
		+ " cp.portrait_photo as portraitPhoto, "                                                                
		+ " cp.contact_email AS email, "                                                                         
		+ " IFNULL(gtam.customer_input,'Yes') AS academyMarketing, "                                              
		+ " IFNULL(nwdm.customer_input,'Yes') AS otherMarketing, "                                                
		+ " m.update_date AS updateDate, " 
		+ " m.create_date AS createDate " 
		+ " FROM member m "                                                                                      
		+ " LEFT JOIN customer_profile cp ON cp.customer_id = m.customer_id "
		+ " LEFT JOIN customer_addition_info_caption caic ON caic.category IN ('HKGTA Marketing', 'NWD Marketing') "
		+ " LEFT JOIN customer_addition_info gtam ON gtam.customer_id = m.customer_id AND gtam.caption_id =  " + Constant.OVERVIEW_HKGTA_MARKETING
		+ " LEFT JOIN customer_addition_info nwdm ON nwdm.customer_id = m.customer_id AND nwdm.caption_id =  " + Constant.OVERVIEW_WMD_MARKETING
		+ " WHERE ";
		if (type.equals("NWD")) {
			sql = sql + " m.member_type IN( 'MG', 'HG' )  "    ;
		} else {
			sql = sql + " m.member_type NOT IN( 'MG', 'HG' )  "    ;
		}
		sql = sql + " AND (NOT EXISTS ( "                                                                                   
		+ " SELECT 1 FROM customer_enrollment ce  "                                                              
		+ " LEFT JOIN customer_enrollment_log cel ON cel.enroll_id = ce.enroll_id  "                             
		+ " WHERE ce.customer_id = m.customer_id AND (ce.`status` = 'REJ' OR ce.`status` = 'CAN') "
		+ " AND (cel.status_from <> 'APV' AND cel.status_to <> 'APV' )) "       
		+ " OR EXISTS ( SELECT 1 FROM customer_enrollment ce  "                                                              
		+ " LEFT JOIN customer_enrollment_log cel ON cel.enroll_id = ce.enroll_id  "                             
		+ " WHERE ce.customer_id = m.customer_id AND (ce.`status` = 'REJ' OR ce.`status` = 'CAN') "
		+ " AND (cel.status_from = 'APV' OR cel.status_to = 'APV' )))  "   ;   
		String joinSql = "SELECT * FROM (" + sql + " ) sub WHERE 1=1";
		return joinSql;
	}
	
	/**
	 * 根据type 创建对应的高级查询条件
	 */
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		if (type.equals("Marketing")) {//创建CashValue变更日志对应高级查询条件
			final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID", "academyNo", "java.lang.Long", "", 1);
			final List<SysCode> status=new ArrayList<>();
			//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled, DPS-Disposal
			SysCode s3=new SysCode();
			s3.setCategory("academyMarketing");
			s3.setCodeDisplay("Yes");
			s3.setCodeValue("Yes");
			status.add(s3);

			SysCode s4=new SysCode();
			s4.setCategory("academyMarketing");
			s4.setCodeDisplay("No");
			s4.setCodeValue("No");
			status.add(s4);
			final AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("HKGTA Marketing", "academyMarketing", "java.lang.String", status, 2);
			final List<SysCode> status2=new ArrayList<>();
			//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled, DPS-Disposal
			SysCode s1=new SysCode();
			s3.setCategory("otherMarketing");
			s3.setCodeDisplay("Yes");
			s3.setCodeValue("Yes");
			status.add(s1);

			SysCode s2=new SysCode();
			s4.setCategory("otherMarketing");
			s4.setCodeDisplay("No");
			s4.setCodeValue("No");
			status.add(s2);
			final AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("NWD Marketing", "otherMarketing", "java.lang.String", status2, 3);
//			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Adjustment Value","transAmount","java.lang.String","",3);
			AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Email", "email", "java.lang.String", "", 4);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Mobile #", "phoneMobile", "java.lang.String", "", 5);
			AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Patron Name", "memberName", "java.lang.String", "", 6);
			AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Last update Time", "updateDate", "java.util.Date", "", 7);
			return Arrays.asList(condition1, condition2, condition3,  condition5, condition6, condition7, condition8);
		}
		return null;
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		// TODO Auto-generated method stub
		return null;
	}
	public CustomerProfileDto getCustomerProfilePhotoAndSinature(Long customerId)
	{
		String sql="SELECT c.customer_id as customerId,c.portrait_photo AS portraitPhoto,c.signature AS signature ,c.ver_no as version FROM customer_profile c WHERE c.customer_id=? ";
		List<Serializable>param=new ArrayList<>();
		param.add(customerId);
		List<CustomerProfileDto>dtos=this.getDtoBySql(sql, param, CustomerProfileDto.class);
		if(null!=dtos&&dtos.size()>0){
			return dtos.get(0);
		}
		else{
			return null;
		}
	}

	
}