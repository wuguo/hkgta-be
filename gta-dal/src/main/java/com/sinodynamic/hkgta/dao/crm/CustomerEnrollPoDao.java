package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollPo;

public interface CustomerEnrollPoDao  extends IBaseDao<CustomerEnrollPo>{

	
	public List<CustomerEnrollPo> getOrderNoListByEnrollId(Long enrollId);
	
	public Serializable saveCustomerEnrollPo(CustomerEnrollPo cEnrollPo);
	
	public List<CustomerEnrollPo> getListByEnrollId(Long EnrollId);
	
	public CustomerEnrollPo getEnrollPoByOrderNo(Long orderNo );
	
	public int deleteCustomerEnrollPoByEnrollId(Long enrollId);
}
