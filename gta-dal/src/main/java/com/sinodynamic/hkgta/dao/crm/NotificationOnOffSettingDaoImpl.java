package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.UserPreferenceSettingDto;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSettingPK;
import com.sinodynamic.hkgta.util.constant.RestaurantSwitchEnum;

@Repository
public class NotificationOnOffSettingDaoImpl extends GenericDao<UserPreferenceSetting> implements NotificationOnOffSettingDao{

	public List<UserPreferenceSettingDto> getNotificationOnOffSetting(Long customerId) throws Exception{
		String sql = "SELECT a.user_id AS userId, a.param_id AS paramId, " +
	       " ups.param_value AS paramValue, ups.update_date AS updateDate " +
           " FROM (select m.user_id,gp.param_id  " +
           " from member m, global_parameter gp " +
           " where gp.param_cat = 'NOTIFICATION' AND m.customer_id = ? ) a " +
           " Left join user_preference_setting ups " +
           " on a.user_id = ups.user_id  and a.param_id = ups.param_id";
		List param = new ArrayList();
		param.add(customerId);
		List<UserPreferenceSettingDto> settings = this.getDtoBySql(sql, param, UserPreferenceSettingDto.class);
		
		return settings;
	}
	
	public List<UserPreferenceSettingDto> getRestaurantNotificationOnOffSetting(String userId) throws Exception{
		String sql = "SELECT a.user_id AS userId, a.param_id AS paramId, " +
	       " ups.param_value AS paramValue, ups.update_date AS updateDate " +
           " FROM (select m.user_id,gp.param_id  " +
           " from staff_master m, global_parameter gp " +
           " where gp.param_cat = 'RESTAURANT_ALERT_SWITCH' AND m.user_id = ? ) a " +
           " Left join user_preference_setting ups " +
           " on a.user_id = ups.user_id  and a.param_id = ups.param_id";
		List param = new ArrayList();
		param.add(userId);
		List<UserPreferenceSettingDto> settings = this.getDtoBySql(sql, param, UserPreferenceSettingDto.class);
		
		return settings;
	}

	@Override
	public List<String> getPushNotificationStaffByRestaurantId(String restaurantId) {
		String sql = "select ups.user_id as userId from user_preference_setting ups where ups.param_id=? and ups.param_value='On'";
		List<Serializable> params = new ArrayList<Serializable>();
		for(RestaurantSwitchEnum switchEnum :RestaurantSwitchEnum.values()){
			if(switchEnum.getDesc().toString().equals(restaurantId)){
				params.add(switchEnum.toString());
				break;
			}
		}
		if(params == null || params.size() == 0){
			return null;
		}
		List<UserPreferenceSettingDto> settings = this.getDtoBySql(sql, params, UserPreferenceSettingDto.class);
		List<String> userIds = new ArrayList<String>();
		if(settings != null && settings.size() != 0){
			for(UserPreferenceSettingDto setting : settings){
				userIds.add(setting.getUserId());
			}
		}
		return userIds;
	}
}
