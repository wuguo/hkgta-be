package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.MemberCardPrintDto;


@Repository
public class PrintMemberWithCardDaoImpl extends GenericDao<MemberCardPrintDto> implements PrintMemberWithCardDao{

	@Override
	public MemberCardPrintDto getMemberCardPrint(String sql, List paramList)
			throws HibernateException {
		// TODO Auto-generated method stub
		List<MemberCardPrintDto> result = super.getDtoBySql(sql, paramList, MemberCardPrintDto.class);
		
		if(null == result || result.size() == 0)
			return null;
		
		return result.get(0);
	}

}
