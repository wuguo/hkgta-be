package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;

@Repository
public class FacilityTimeslotLogDaoImpl extends GenericDao<FacilityTimeslotLog> implements FacilityTimeslotLogDao {

	@Override
	public List<FacilityTimeslotLog> getByFacilityNo(String facilityType, Long facilityNo) {
		if (facilityNo == null || StringUtils.isEmpty(facilityType)) {
			return null;
		}
		String hql = "from FacilityTimeslotLog where facilityType = ? and facilityNo = ? order by createDate desc";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(facilityNo);
		return super.getByHql(hql, paramList);
	}

}
