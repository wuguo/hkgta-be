package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.UserConfirmReservedFacility;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class UserConfirmReservedFacilityDaoImpl extends GenericDao<UserConfirmReservedFacility>implements UserConfirmReservedFacilityDao {


}
