package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dao.crm.MemberDao;
import com.sinodynamic.hkgta.dto.fms.CourseSessionSmsParamDto;
import com.sinodynamic.hkgta.dto.fms.CourseTransactionDto;
import com.sinodynamic.hkgta.dto.fms.MemberAttendanceDto;
import com.sinodynamic.hkgta.dto.fms.MemberInfoDto;
import com.sinodynamic.hkgta.dto.fms.StudentCourseAttendanceDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.util.constant.EnrollCourseStatus;
import com.sinodynamic.hkgta.util.constant.ServicePlanRight;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class CourseEnrollmentDaoImpl extends GenericDao<CourseEnrollment>
		implements CourseEnrollmentDao {
    	
	private final Logger logger = LoggerFactory.getLogger(CourseEnrollmentDaoImpl.class); 
	
    	@Autowired
    	private MemberDao memberDao;
    
    	private static final Map<String, ServicePlanRight> COURSE_RIGHT =  new HashMap<String, ServicePlanRight>() {
    	    {
    		put("GSS", ServicePlanRight.GOLF_COURSE);
    		put("TSS", ServicePlanRight.TENNIS_COURSE);
    	    }
    	};

	/**
	 * This method is used to get list of enrolled members info for a course
	 * 
	 * @param courseId
	 * @param status
	 * @return List<MemberInfoDto>
	 * @author Vineela_Jyothi
	 */
	@Override
	public List<MemberInfoDto> getMemberInfo(String attendance,
			String courseId, String status, String memberAcceptance) {
		List<Serializable> listParam = new ArrayList<Serializable>();

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT a.enroll_id AS enrollId, a.enroll_date AS enrollDate, a.status, a.customer_id AS customerId, a.create_date AS createDate, "
				+ "cm.course_id as courseId, cm.course_name courseName, cm.capacity, "
				+ "(SELECT COUNT(*) FROM course_enrollment ce WHERE ce.course_id = a.course_id "
				+ "AND (ce.status ='REG' or ce.status ='ACT')) AS enrollmentQuota, "
				+ "(SELECT m.academy_no FROM member m WHERE m.customer_id = a.customer_id) AS memberId, "
				+ "(SELECT cp.contact_email FROM customer_profile cp "
				+ "WHERE cp.customer_id = a.customer_id) AS emailAddress, (SELECT CONCAT(c.salutation, ' ', c.given_name, ' ', c.surname) "
				+ "FROM customer_profile c WHERE c.customer_id = a.customer_id) AS memberName FROM course_enrollment a, course_master cm "
				+ "WHERE a.course_id = cm.course_id AND ");

		if (StringUtils.isNotEmpty(attendance)
				&& attendance.equalsIgnoreCase("Y")) {
			sql.append("a.status='REG' AND ");

		}

		if (null != courseId) {
			sql.append("a.course_id = ? ");
			listParam.add(courseId);
		}

		if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase("ALL")) {
			sql.append("AND a.status = ? ");
			listParam.add(status);
		}
		
		return getDtoBySql(sql.toString(), listParam, MemberInfoDto.class);
	}
	
	
	/**
	 * This method is used to get list of enrolled members info for a course
	 * 
	 * @param courseId
	 * @param status
	 * @return List<MemberInfoDto>
	 * @author Vineela_Jyothi
	 */
	@Override
	public List<MemberInfoDto> getMemberInfoList(ListPage<CourseEnrollment> page, String attendance,
			String courseId, String status, String memberAcceptance) {
		List<Serializable> listParam = new ArrayList<Serializable>();

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM (SELECT a.enroll_id AS enrollId, a.enroll_date AS enrollDate, a.status, a.customer_id AS customerId, a.create_date AS createDate, "
				+ "cm.course_id as courseId, cm.course_name courseName, cm.capacity, "
				+ "(CASE WHEN cm.member_acceptance='A' then (SELECT COUNT(*) FROM course_enrollment ce "
				+ "WHERE ce.course_id = a.course_id AND ce.status ='REG') when cm.member_acceptance='M' then "
				+ "(SELECT COUNT(*) FROM course_enrollment ce WHERE ce.course_id = a.course_id "
				+ "AND (ce.status ='REG' or ce.status ='ACT')) else 0 end) AS enrollmentQuota, "
				+ "(SELECT m.academy_no FROM member m WHERE m.customer_id = a.customer_id) AS memberId, "
				/***
				 * add memberVip
				 */
				+ "(SELECT m.vip FROM member m WHERE m.customer_id = a.customer_id) AS memberIsVIP, "
				
				+ "(SELECT cp.contact_email FROM customer_profile cp WHERE cp.customer_id = a.customer_id) AS emailAddress, "
				+ "(SELECT cp.portrait_photo FROM customer_profile cp WHERE cp.customer_id = a.customer_id) AS portraitPhoto, "
				+ "(SELECT CONCAT(c.salutation, ' ', c.given_name, ' ', c.surname) FROM customer_profile c WHERE c.customer_id = a.customer_id) AS memberName "
				+ "FROM course_enrollment a, course_master cm "
				+ "WHERE a.course_id = cm.course_id ");

		if (StringUtils.isNotEmpty(attendance)
				&& attendance.equalsIgnoreCase("Y")) {
			sql.append("AND a.status='REG' ");

		}

		if (null != courseId) {
			sql.append("AND a.course_id = ? ");
			listParam.add(courseId);
		}

		if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase("ALL")) {
			sql.append("AND a.status = ? ");
			listParam.add(status);
		}
		
		sql.append(") t ");
		
		if(!StringUtils.isBlank(page.getCondition())){
			String condition = " WHERE "+page.getCondition();
			sql.append(condition);
		}

		return getDtoBySql(sql.toString(), listParam, MemberInfoDto.class);
	}
	
	public List<MemberInfoDto> getMemberInfoListBySessionId(ListPage<CourseEnrollment> page, String attendance,
			String courseId, String status, String memberAcceptance) {
		List<Serializable> listParam = new ArrayList<Serializable>();

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM (SELECT a.enroll_id AS enrollId, a.enroll_date AS enrollDate, a.status, a.customer_id AS customerId, a.create_date AS createDate, "
				+ "cm.course_id as courseId, cm.course_name courseName, cm.capacity, "
				+ "(CASE WHEN cm.member_acceptance='A' then (SELECT COUNT(*) FROM course_enrollment ce "
				+ "WHERE ce.course_id = a.course_id AND ce.status ='REG') when cm.member_acceptance='M' then "
				+ "(SELECT COUNT(*) FROM course_enrollment ce WHERE ce.course_id = a.course_id "
				+ "AND (ce.status ='REG' or ce.status ='ACT')) else 0 end) AS enrollmentQuota, "
				+ "(SELECT m.academy_no FROM member m WHERE m.customer_id = a.customer_id) AS memberId, "
				+ "(SELECT cp.contact_email FROM customer_profile cp WHERE cp.customer_id = a.customer_id) AS emailAddress, "
				+ "(SELECT cp.portrait_photo FROM customer_profile cp WHERE cp.customer_id = a.customer_id) AS portraitPhoto, "
				+ "(SELECT CONCAT(c.salutation, ' ', c.given_name, ' ', c.surname) FROM customer_profile c WHERE c.customer_id = a.customer_id) AS memberName "
				+ "FROM course_enrollment a, course_master cm "
				+ "WHERE a.course_id = cm.course_id ");

		if (StringUtils.isNotEmpty(attendance)
				&& attendance.equalsIgnoreCase("Y")) {
			sql.append("AND a.status='REG' ");

		}

		if (null != courseId) {
			sql.append("AND a.course_id = ? ");
			listParam.add(courseId);
		}

		if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase("ALL")) {
			sql.append("AND a.status = ? ");
			listParam.add(status);
		}
		
		sql.append(") t ");
		
		if(!StringUtils.isBlank(page.getCondition())){
			String condition = " WHERE "+page.getCondition();
			sql.append(condition);
		}

		return getDtoBySql(sql.toString(), listParam, MemberInfoDto.class);
	}

	/**
	 * @author Mianping_Wu
	 * @date 7/13/2015
	 * @return CourseEnrollment
	 */
	@Override
	public CourseEnrollment getCourseEnrollmentById(String enrollId) {
	    
	   return get(CourseEnrollment.class, enrollId);
	}

	/**
	 * @author Mianping_Wu
	 * @date 7/13/2015
	 * @return
	 */
	@Override
	public void updateCourseEnrollment(CourseEnrollment ce) {
	     update(ce);
	}
	
	
	/**
	 * This method is used to get list of enrolled members info for a course
	 * 
	 * @param courseId
	 * @param status
	 * @return List<MemberInfoDto>
	 * @author Vineela_Jyothi
	 */
	@Override
	public List<MemberAttendanceDto> getMemberAttendance(String courseId, Long enrollId,String sessionId) {
		List<Serializable> listParam = new ArrayList<Serializable>();

		StringBuilder sql = new StringBuilder();

		if(StringUtils.isEmpty(sessionId)){
			sql.append("SELECT "
					+ "s.sys_id AS sysId, e.enroll_id AS enrollId, s.session_no AS sessionNo, s.status as sessionStatus, t.attend_id AS attendId, t.course_session_id AS courseSessionId, "
					+ "ifnull(t.status, 'PND') AS attendance FROM course_master c, course_enrollment e, course_session s "
					+ "LEFT JOIN (SELECT a.enroll_id, a.course_session_id, a.attend_id, a.status "
					+ "FROM course_session s INNER JOIN student_course_attendance a ON (s.sys_id = a.course_session_id) "
					+ "WHERE s.course_id = ? and a.enroll_id =?) t "
					+ "ON (t.course_session_id = s.sys_id) "
					+ "WHERE e.course_id = c.course_id AND c.course_id = s.course_id "
					//SGG-1679 delete AND s.status='ACT' 
					//[Admin Portal] Add 'Not attending' option to indicate that member is not attending a course session
					//+ "AND s.status = 'ACT'"
					+ " AND c.course_id = ? AND e.enroll_id  =? order by s.sys_id");
			
			
			if (null != courseId) {
				listParam.add(courseId);
			}
			
			if (null != enrollId){
				listParam.add(enrollId);
			}
			listParam.add(courseId);
			listParam.add(enrollId);
		}else {
			sql.append("select s.sys_id sysId,sca.enroll_id enrollId,s.session_no sessionNo, s.status as sessionStatus, sca.attend_id attendId,sca.course_session_id courseSessionId, sca.status attendance, sca.update_date updateDate "
					+ " from student_course_attendance sca,course_session s where sca.course_session_id= s.sys_id "
					+ "   and sca.enroll_id =? and s.session_no= ? ");
			listParam.add(enrollId);
			listParam.add(sessionId);
			
		}
		
		List<MemberAttendanceDto>  rs= getDtoBySql(sql.toString(), listParam, MemberAttendanceDto.class);
		if(!StringUtils.isEmpty(sessionId)&&(rs==null||rs.size()==0)){
			sql = new StringBuilder();
			sql.append("SELECT s.sys_id sysId,ce.enroll_id enrollId,s.session_no sessionNo, s.status as sessionStatus, s.sys_id courseSessionId,'PND' attendance "
					+ " FROM course_enrollment ce,course_session s,course_master cm "
					+ " WHERE cm.course_id = s.course_id and ce.course_id = cm.course_id "
					+ " and cm.course_id = ? and ce.enroll_id=? and s.session_no=? ");
			listParam = new ArrayList<Serializable>();
			listParam.add(courseId);
			listParam.add(enrollId);
			listParam.add(sessionId);
			rs= getDtoBySql(sql.toString(), listParam, MemberAttendanceDto.class);		
		}
		return rs;
	}

	@Override
	public int countByCourseId(Long courseId) {
	    
	    	String status = "('" + EnrollCourseStatus.REG.getDesc() + "', '" + EnrollCourseStatus.ACT.getDesc() + "')";
	    
		String hql = "select count(*) from CourseEnrollment ce where ce.courseId = ? and ce.status in " + status;
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(courseId);
		Object count = getUniqueByHql(hql,param);
		int countExp = ((Number)count).intValue();
		return countExp;
	}

	@Override
	public CourseEnrollment getCourseEnrollmentByCourseIdAndCustomerId(Long courseId,
			Long customerId) {
		String hql = " from CourseEnrollment ce where ce.status in ('PND', 'ACT', 'REG') and ce.courseId = ? and ce.customerId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(courseId);
		param.add(customerId);
		List<CourseEnrollment> enrolls = getByHql(hql,param);
		
		return enrolls == null || enrolls.size() == 0 ? null : enrolls.get(0);
		
		
	}

	@Override
	public CourseEnrollment getCourseEnrollmentByOrderNo(Long orderNo) {
	    
		String hql = " from CourseEnrollment ce where ce.custOrderDetId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(orderNo);
		List<CourseEnrollment> enrolls = getByHql(hql, param);
		
		return (enrolls == null || enrolls.size() == 0) ? null : enrolls.get(0);
	}

	@Override
	public String checkCustomerServicePlanRight(Long customerId, String courseType) {
	    
		if (customerId == null || StringUtils.isEmpty(courseType)) return "";

		String customerIdStr = String.valueOf(customerId);
		ServicePlanRight right = COURSE_RIGHT.get(courseType);
		String inputValue = (right != null ? right.getValue() : null);

		String sql = "select distinct "
				+ "text_value as inputValue from member_limit_rule "
				+ "where customer_id = " + customerIdStr
				+ " and limit_type = '" + inputValue + "'";

		List<ServicePlanAdditionRule> result = (List<ServicePlanAdditionRule>) getDtoBySql(
				sql, null, ServicePlanAdditionRule.class);
		return (result != null && result.size() > 0) ? result.get(0)
				.getInputValue() : null;
	}
	
	
	@Override
	public Object getUserIdByEnrollId(Long enrollId) {
		List<Serializable> param = new ArrayList<Serializable>();
		String sql = "SELECT a.user_id FROM user_master a, member b, course_enrollment c "
				+ "WHERE a.user_id=b.user_id AND b.customer_id=c.customer_id AND c.enroll_id=?";
		param.add(enrollId);
	
		return getUniqueBySQL(sql, param);
		
		
	}

	@Override
	public List<CourseEnrollment> getCourseEnrollmentByCourseId(Long courseId)
			throws HibernateException {
		String hql = " from CourseEnrollment ce where ce.courseId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(courseId);
		List<CourseEnrollment> enrolls = getByHql(hql,param);
		return enrolls;
	}

	
	@Override
	public String getCommonQueryForMembersList(String courseId,
			String attendance, String status) {
		
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM (SELECT a.enroll_id AS enrollId, a.enroll_date AS enrollDate, a.status, a.customer_id AS customerId, a.create_date AS createDate, "
				+ "cm.course_id as courseId, cm.course_name courseName, cm.capacity, "
				+ "(SELECT COUNT(*) FROM course_enrollment ce WHERE ce.course_id = a.course_id "
				+ "AND (ce.status ='REG' or ce.status ='ACT')) AS enrollmentQuota, "
				+ "(SELECT m.academy_no FROM member m WHERE m.customer_id = a.customer_id) AS memberId, "
				+ "(SELECT cp.contact_email FROM customer_profile cp "
				+ "WHERE cp.customer_id = a.customer_id) AS emailAddress, (SELECT CONCAT(c.salutation, ' ', c.given_name, ' ', c.surname) "
				+ "FROM customer_profile c WHERE c.customer_id = a.customer_id) AS memberName FROM course_enrollment a, course_master cm "
				+ "WHERE a.course_id = cm.course_id AND a.course_id = "+courseId);

		if (StringUtils.isNotEmpty(attendance)
				&& attendance.equalsIgnoreCase("Y")) {
			/***
			 * christ lh
			 * SGG-2788
			 * @time 2016-10-17
			 * Cancel the enrollment of golf / tennis course, the attendance records should not be disappear.
			 */
		//	sql.append(" AND a.status='REG' ");

		}

		if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase("ALL")) {
			sql.append(" AND a.status = '"+status+"'");
			
		}
		
		sql.append(")t ");

		return sql.toString();
	}
	
	
	@Override
	public String getMaxAndMinEnrollDate(Long courseId) {
	    
	    	if (courseId == null) return null;
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(courseId);
		String sql = "select concat(max(enroll_date), '#', min(enroll_date)) from course_enrollment where status in ('ACT', 'REG') and course_id = ?";
		Object object = getUniqueBySQL(sql, param);
		if (object == null) return null;
	
		return (String) object;
	}


	@Override
	public String getEnrollIdBySessionIdAndCard(String sessionId, String cardNO) {
	    
	    	if (StringUtils.isEmpty(sessionId) || StringUtils.isEmpty(cardNO)) return null;
		String sql = "select ce.enroll_id "
			+ "from permit_card_master cpm "
			+ "left join course_enrollment ce on cpm.mapping_customer_id = ce.customer_id "
			+ "left join course_session cs on cs.course_id = ce.course_id "
			+ "where cpm.status = 'ISS' "
			+ "and ce.status = 'REG' "
			+ "and cs.sys_id = " + sessionId
			+ " and cpm.card_no = " + cardNO;
		
		Object object = getUniqueBySQL(sql, null);
		if (object == null) return null;
	
		return String.valueOf(object);
	}


	@Override
	public CourseTransactionDto getCustomerOrderTransByEnrollId(String enrollId) {
	    
	    if (StringUtils.isEmpty(enrollId)) return null;
	    
	    String sql = "select "
	    	+ "cot.transaction_no as transactionNo, "
	    	+ "cot.payment_media as media, "
	    	+ "trim(cot.payment_method_code) as paymentMethod, "
	    	+ "cot.paid_amount as paidAmount, "
	    	+ "cot.payment_location_code as location, "
	    	+ "cot.transaction_timestamp as transactionTimestamp "
	    	+ "from course_enrollment ce "
	    	+ "left join customer_order_hd coh on ce.cust_order_no = coh.order_no "
	    	+ "left join customer_order_trans cot on coh.order_no = cot.order_no "
	    	+ "where ce.enroll_id = " + enrollId
	    	+"  ORDER BY cot.transaction_timestamp DESC  ";
	    
	    List<CourseTransactionDto> transactionInfos = getDtoBySql(sql, null, CourseTransactionDto.class);
	    
	    return ((transactionInfos != null && transactionInfos.size() > 0) ? transactionInfos.get(0) : null);
	}


	@Override
	public String getEnrollIdBySessionIdAndMember(String sessionId, String academyNo) {
	    
	    	if (StringUtils.isEmpty(sessionId) || StringUtils.isEmpty(academyNo)) return null;
	    	
		String sql = "select ce.enroll_id "
			+ "from member m "
			+ "left join course_enrollment ce on m.customer_id = ce.customer_id "
			+ "left join course_session cs on cs.course_id = ce.course_id "
			+ "where m.status = 'ACT' "
			+ "and ce.status = 'REG' "
			+ "and cs.sys_id = " + sessionId
			+ " and m.academy_no = " + academyNo;
		
		Object object = getUniqueBySQL(sql, null);
		if (object == null) return null;
	
		return String.valueOf(object);
	}


	@Override
	public StudentCourseAttendanceDto getMemberInfoByEnrollId(String enrollId) {
	    
	    if (StringUtils.isEmpty(enrollId)) return null;
	    
	    String sql = "select "
	    	+ "cast(cp.customer_id as char) as customerId, "
	    	+ "concat(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as name, "
	    	+ "m.academy_no as academyNo, "
	    	+ "cp.portrait_photo as portrait "
	    	+ "from course_enrollment ce "
	    	+ "left join customer_profile cp on cp.customer_id = ce.customer_id "
	    	+ "left join member m on m.customer_id = cp.customer_id where ce.enroll_id = " + enrollId;
	    
	    List<StudentCourseAttendanceDto> dtos = getDtoBySql(sql, null, StudentCourseAttendanceDto.class);
	    return ((dtos != null && dtos.size() > 0) ? dtos.get(0) : null);
	}


	@Override
	public List<UserDevice> getAppUser2Remind(Long sysId, String application) {
	    
	    if (sysId == null || StringUtils.isEmpty(application)) return null;
	    
	    String sql = "select distinct m.user_id as userId "
	    	+ "from course_session cs "
	    	+ "left join course_enrollment ce on cs.course_id = ce.course_id "
	    	+ "left join member m on ce.customer_id = m.customer_id "
	    	//+ "left join user_device ud on ud.user_id = m.user_id "
	    	+ "where ce.status = 'REG' "
	    	//+ "and ud.arn_application = ? "
	    	+ "and cs.sys_id = ?";
	    
	    if(logger.isDebugEnabled())
	    {
	    	logger.debug("sql:" + sql);
	    	logger.debug("sysId:" + sysId);
	    }
	    List<Serializable> params = new ArrayList<Serializable>();
//	    params.add(application);
	    params.add(sysId);
	    
	    List<UserDevice> result = getDtoBySql(sql, params, UserDevice.class);
	    return result;
	}


	@Override
	public List<CourseSessionSmsParamDto> getSessionDuringPeriod(Date start, Date end) {
	    
	    if (start == null || end == null) return null;
	    
	    String sql = "select cs.sys_id as sysId, "
	    	+ "cm.course_type as courseType, "
	    	+ "um.nickname as coach, "
	    	+ "cs.coach_user_id as coachUserId,"
	    	+ "cm.course_name as courseName, "
	    	+ "cs.begin_datetime as sessionDate, "
	    	+ "CONCAT(IF(cm.course_type = 'GSS', 'Bay# ', 'Court# '), "
	    	+ "GROUP_CONCAT(DISTINCT fm.facility_name ORDER BY CAST(fm.facility_no AS SIGNED) ASC SEPARATOR ',')) as facility "
	    	+ "from course_session cs  "
	    	+ "left join course_session_facility csf on cs.sys_id = course_session_id "
	    	+ "left join facility_master fm on csf.facility_no = fm.facility_no "
	    	+ "left join user_master um on um.user_id = cs.coach_user_id "
	    	+ "left join course_master cm on cm.course_id = cs.course_id "
	    	+ "where cs.status = 'ACT' "
	    	// + " and cs.begin_datetime between ? and ? "
	    	/***
	    	 * @author christ
	    	 *  auto one time  5min per
	    	 */
	    	+" and TIMESTAMPDIFF(MINUTE,cs.begin_datetime,DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 1 HOUR)) BETWEEN  0 AND 4 "
	    	+ "group by cs.sys_id";
	    
//	    if(logger.isDebugEnabled())
//	    {
	    	logger.info("sql:" + sql);
//	    }
	    List<Serializable> params = new ArrayList<Serializable>();
//	    params.add(start);
//	    params.add(end);
	    
	    List<CourseSessionSmsParamDto> result = getDtoBySql(sql, null, CourseSessionSmsParamDto.class);
//	    if(logger.isDebugEnabled())
//	    {
	    	logger.info("session size:" + result.size());
//	    }
	    return result;
	}
	
}
