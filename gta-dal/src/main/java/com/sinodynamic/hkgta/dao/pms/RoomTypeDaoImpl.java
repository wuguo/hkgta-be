package com.sinodynamic.hkgta.dao.pms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.pms.RoomPicPathDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.entity.pms.RoomType;
@Repository
public class RoomTypeDaoImpl extends GenericDao<RoomType> implements RoomTypeDao {

	@Override
	public List<RoomTypeDto> getRoomListByTypes(String types)
	{
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT type_code as roomTypeCode, ")
				.append("type_name as roomTypeName, ")
				.append("description as roomTypeDescription, amenity, status, ") 
				.append("num_of_guest_txt as numOfGuest ")
				.append("FROM room_type ");
				if (!StringUtils.isEmpty(types))
				{
					sql.append("where type_code in ( ").append(getValueOfInConditions(types)).append(" )");
				}
			
		List<RoomTypeDto> list = super.getDtoBySql(sql.toString(),null, RoomTypeDto.class);
		
		return list;
	}
	
	@Override
	public List<RoomPicPathDto> getRoomPics(String types)
	{
		StringBuffer sql = new StringBuffer();
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("id", LongType.INSTANCE);
		typeMap.put("roomTypeCode", StringType.INSTANCE);
		typeMap.put("serverFilename", StringType.INSTANCE);
		typeMap.put("displayOrder", LongType.INSTANCE);
		
		sql.append("SELECT pic_id as id, ")
				.append("room_type_code as roomTypeCode, ")
				.append("server_filename as serverFilename, ") 
				.append("display_order as displayOrder ")
				.append("FROM room_pic_path ");
				if (!StringUtils.isEmpty(types))
				{
					sql.append("where room_type_code ='").append(types).append("' ");
				}
				sql.append("ORDER BY display_order ASC");
			
		List<RoomPicPathDto> list =  getDtoBySql(sql.toString(), null, RoomPicPathDto.class, typeMap);
		
		return list;
	}

	@Override
	public RoomType getRoomTypeByCode(String roomTypeCode)
	{
		return super.getUniqueByCol(RoomType.class, "typeCode", roomTypeCode.toUpperCase());
	}
	
	@Override
	public String getRoomTypeList() {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM ( SELECT type_code as roomTypeCode, ")
				.append("type_name as roomTypeName, ")
				.append("description as roomTypeDescription, amenity, ") 
				.append("num_of_guest_txt as numOfGuest ")
				.append("FROM room_type WHERE status='ACT' ORDER BY create_date desc) sub WHERE 1=1 ");
		
		return sql.toString();
	}
	
	public  String getValueOfInConditions(String searchString) {
		String[] arr = searchString.split(",");
		StringBuilder searchStr=new StringBuilder();
		for(int i=0;i<arr.length;i++){
			searchStr.append("'").append(arr[i]).append("'");
			if(i!=arr.length-1){
				searchStr.append(",");
			}
		}
		return searchStr.toString();
	}
}
