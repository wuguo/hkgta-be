package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;

@Repository
public class FacilityAttributeCaptionDaoImpl extends GenericDao<FacilityAttributeCaption>  implements FacilityAttributeCaptionDao{

	@Override
	public List<FacilityAttributeCaption> getFacilityAttributeCaptionByFuzzy(String attributeId)
	{
		StringBuffer hql = new StringBuffer("from FacilityAttributeCaption where attributeId  like ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(attributeId+"%");
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public FacilityAttributeCaption getFacilityAttributeCaptionByAttributeId(String attributeId)
	{
		StringBuffer hql = new StringBuffer("from FacilityAttributeCaption where attributeId  = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(attributeId);
		return (FacilityAttributeCaption) super.getUniqueByHql(hql.toString(), paramList);
	}

}
