package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ServerFolderMapping;

/**
 * 
 * @author Vineela_Jyothi
 *
 */
@Repository
public class PresentationFolderDaoImpl extends GenericDao<ServerFolderMapping> implements PresentationFolderDao {
	
	/**
	 * This method is used to retrieve list of presentation folders
	 * @return List of presentation folders
	 * @throws Exception
	 * @author Vineela_Jyothi
	 */
	@Override
	public List<ServerFolderMapping> getTemplateFolderList() throws HibernateException {
		String hql = "from ServerFolderMapping ";
		return super.getByHql(hql, null);
	}

}
