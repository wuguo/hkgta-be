package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.mms.GuestRequestDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.mms.SpaMemberSync;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;

@Repository
public class SpaMemberSyncDaoImpl extends GenericDao<SpaMemberSync> implements SpaMemberSyncDao {
	 private Logger logger = Logger.getLogger(SpaMemberSyncDaoImpl.class);
    
	 @Override
    public List<GuestRequestDto> getMemberInfoList2Synchronize(String status, String academyNos) {
	
	String sql = "select "
		+ "sms.sys_id as sysId, "
		+ "(select max(sms1.spa_guest_id) from spa_member_sync sms1 where sms1.customer_id = sms.customer_id and sms1.action = 'INSERT' and sms1.status = 'CMP') as guestId, "
		+ "sms.action as action, "
		+ "sms.customer_id as customerId, "
		+ "pcm.card_no as cardNo, "
		+ "me.academy_no as guestCode, "
		+ "cp.given_name as firstName, "
		+ "cp.surname as lastName, "
		+ "cp.phone_mobile as mobilePhone, "
		+ "cp.contact_email as email, "
		+ "case cp.gender when '' then 'M' else cp.gender end as gender, "
		+ "cp.postal_address1 as address1, "
		+ "cp.postal_address2 as address2 "
		+ "from spa_member_sync sms "
		+ "inner join member me on me.customer_id = sms.customer_id "
		+ "inner join customer_profile cp on sms.customer_id = cp.customer_id "
		+ "left join permit_card_master pcm on sms.customer_id = pcm.mapping_customer_id "
		+ "where sms.status = 'OPN' "
		+  " and me.status='ACT' ";
		if (StringUtils.isEmpty(academyNos)) {
			sql = sql + "and me.academy_no is not null  order by sms.action limit 20 ";
		} else {
			sql = sql + "and me.academy_no IN(" + academyNos + " ) ";
		}
//		+ "and sms.retry_count<100 "
		
	
	return getDtoBySql(sql, null, GuestRequestDto.class);
    }
    
    @Override
    public List<SpaMemberSync> getSpecificSpaMemberSync(Long customerId, String status) {
	
	String hql = "from SpaMemberSync where customerId = ? and status = ?";
	List<Serializable> params = new ArrayList<Serializable>();
	params.add(customerId);
	params.add(status);
	return getByHql(hql, params);
    }

    @Override
    public void addSpaMemberSyncWhenInsert(Member member) {
	SpaMemberSync entity = new SpaMemberSync();
	entity.setCustomerId(member.getCustomerId());
	entity.setAction(Constant.MEMBER_SYNCHRONIZE_INSERT);
	entity.setCreateDate(new Date());
	entity.setRetryCount(0);
	entity.setStatus(Constant.STATUS_WAITING_TO_SYNCHRONIZE);
	save(entity);
    }

    @Override
    public void addSpaMemberSyncWhenUpdate(CustomerProfile oldThis, CustomerProfile newThis) {
	if (oldThis == null || newThis == null) return;
	String surName = CommUtil.nvl(newThis.getSurname());
	String givenName = CommUtil.nvl(newThis.getGivenName());
	String contactEmail = CommUtil.nvl(newThis.getContactEmail());
	String phoneMobile = CommUtil.nvl(newThis.getPhoneMobile());
	String gender = CommUtil.nvl(newThis.getGender());
	
	if (surName.equals(oldThis.getSurname()) && givenName.equals(oldThis.getGivenName())
		&& contactEmail.equals(oldThis.getContactEmail())
		&& phoneMobile.equals(oldThis.getPhoneMobile())
		&& gender.equals(oldThis.getGender())) return;
	
	SpaMemberSync entity = new SpaMemberSync();
	entity.setCustomerId(oldThis.getCustomerId());
	entity.setAction(Constant.MEMBER_SYNCHRONIZE_UPDATE);
	entity.setCreateDate(new Date());
	entity.setRetryCount(0);
	entity.setStatus(Constant.STATUS_WAITING_TO_SYNCHRONIZE);
	save(entity);
    }
    
    @Override
    public void addSpaMemberSyncWhenLinkCard(String customerId) {
	
	SpaMemberSync entity = new SpaMemberSync();
	entity.setCustomerId(Long.parseLong(customerId));
	entity.setAction(Constant.MEMBER_SYNCHRONIZE_LINKCARD);
	entity.setCreateDate(new Date());
	entity.setRetryCount(0);
	entity.setStatus(Constant.STATUS_WAITING_TO_SYNCHRONIZE);
	save(entity);
    }
    
    /**
     * 批量插入执行 sql
     * @param sql
     * @return
     * @throws HibernateException
     */
    @Override
    public int bulkInsertBySql(String sql){
    	try {
			return getsessionFactory().getCurrentSession().createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			 logger.error("[SpaMemberSyncDaoImpl-->>bulkInsertBySql-->>]" + e.getMessage(), e);
			 logger.info("[SpaMemberSyncDaoImpl-->>bulkInsertBySql-->>]" + e.getMessage(), e);
		}
		return 0;
    }
    
    /**
     * 根据id删除同步记录
     * @param ids
     * @return
     */
    @Override
	public int deleteMmsSync(String ids)
	{
    	
    	String deleteSql = " DELETE FROM spa_member_sync WHERE customer_id IN ("+ids+") ";
		return super.deleteByHql(deleteSql, null);
	}

	@Override
	public List<SpaMemberSync> getAllSpaMemberSynchThenGuestIdIsNotNull() {
		List<SpaMemberSync> synchs=this.getByHql("from SpaMemberSync where spaGuestId is not null and status='CMP'");
		return synchs;
	}
    
}
