package com.sinodynamic.hkgta.dao.pos;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuItem;

public interface RestaurantMenuItemDao  extends IBaseDao<RestaurantMenuItem> {

	List<RestaurantMenuItem> getRestaurantMenuItembyDisplayOrder(Integer displayOrder);
	
	Integer getMaxDisplayOrder();
	
	Integer getMaxDisplayOrder(String restaurantId, String catId);
	
	Integer getMinDisplayOrder(String restaurantId, String catId);
	
	RestaurantMenuItem getRestaurantMenuItem(String restaurantId, String catId, String itemNo);
}
