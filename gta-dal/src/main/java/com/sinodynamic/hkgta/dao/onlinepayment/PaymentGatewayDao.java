package com.sinodynamic.hkgta.dao.onlinepayment;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGateway;

public interface PaymentGatewayDao extends IBaseDao<PaymentGateway> {

}
