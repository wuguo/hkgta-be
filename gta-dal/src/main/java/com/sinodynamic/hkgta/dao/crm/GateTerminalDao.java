package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.GateTerminal;

public interface GateTerminalDao extends IBaseDao<GateTerminal>
{
	List<GateTerminal> getAllTerminal() throws HibernateException;
}
