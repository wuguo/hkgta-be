package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateTime;

public interface FacilityUtilizationRateTimeDao extends IBaseDao<FacilityUtilizationRateTime>{

	List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeList(String facilityType);
	
	List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeList(String facilityType,String weekDay, String subType);
	
	int removeFacilityUtilizationRateTime(String facilityType,String subType,String weekDay);
	
	int removeFacilityUtilizationRateTime(String facilityType,String weekDay);
	
	FacilityUtilizationRateTime getFacilityUtilizationRateTime(String facilityType,String weekDay,int beginTime,String subType);
	
	List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeListBySubType(String facilityType,String facilitySubtypeId);
}
