package com.sinodynamic.hkgta.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * 
 * @author Angus_Zhu
 *
 */
@Repository("commonByHQLDao")
public class CommonDataQueryByHQLDaoImpl extends AbstractCommonDataQueryDao implements CommonDataQueryByHQLDao {

	private Logger logger = Logger.getLogger(CommonDataQueryByHQLDaoImpl.class);

	private ConcurrentHashMap<String, Object> paramsValue;
	private ConcurrentHashMap<String, String> paramsType;
	
	public void generateSearchStr(SearchRuleDto searchRule, StringBuilder sb) {
		String searchField = searchRule.getField();
		String searchOper = searchRule.getOp();
		String searchString = searchRule.getData();
		String fieldType = searchRule.getType();
		if (StringUtils.isNotEmpty(searchField) && StringUtils.isNotEmpty(searchOper)) {
			String processedFiled = escapeProcessedField(searchField);
			if(!IS_NULL.equals(searchOper) && !IS_NOT_NULL.equals(searchOper)){
				paramsValue.put(processedFiled, searchString);
				paramsType.put(processedFiled, fieldType);
			}
			if (EQUAL.equals(searchOper)) {
				sb.append(LEFT_BRACKET).append(searchField).append(" = ").append(COLON).append(processedFiled).append(SPACE).append(RIGHT_BRACKET);
			} else if (NOT_EQUAL.equals(searchOper)) {
				sb.append(LEFT_BRACKET).append(searchField).append(" != ").append(COLON).append(processedFiled).append(SPACE).append(RIGHT_BRACKET);
			} else if (LESS_THAN.equals(searchOper)) {
				sb.append(LEFT_BRACKET).append(searchField).append("< ").append(COLON).append(processedFiled).append(SPACE).append(RIGHT_BRACKET);
			} else if (LESS_EQUAL.equals(searchOper)) {
				sb.append(LEFT_BRACKET).append(searchField).append("<= ").append(COLON).append(processedFiled).append(SPACE).append(RIGHT_BRACKET);
			} else if (GREATER_THAN.equals(searchOper)) {
				sb.append(LEFT_BRACKET).append(searchField).append(" > ").append(COLON).append(processedFiled).append(SPACE).append(RIGHT_BRACKET);
			} else if (GREATER_EQUAL.equals(searchOper)) {
				sb.append(LEFT_BRACKET).append(searchField).append(" >= ").append(COLON).append(processedFiled).append(SPACE).append(RIGHT_BRACKET);
			} else if (IS_NULL.equals(searchOper)) {// is null
				sb.append(LEFT_BRACKET).append(searchField).append(" is null").append(SPACE).append(RIGHT_BRACKET);
			} else if (IS_NOT_NULL.equals(searchOper)) {// is not null
				sb.append(LEFT_BRACKET).append(searchField).append("  is not null ").append(SPACE).append(RIGHT_BRACKET);
			} else if (IN.equals(searchOper)) {// is in
				paramsType.put(processedFiled, LIST);
				paramsValue.put(processedFiled, Arrays.asList(searchString.split(COMMA)));
				sb.append(LEFT_BRACKET).append(searchField).append(" in  (").append(COLON).append(processedFiled).append(")  )");
			} else if (NOT_IN.equals(searchOper)) {// is not in
				paramsType.put(processedFiled, LIST);
				paramsValue.put(processedFiled, Arrays.asList(searchString.split(COMMA)));
				sb.append(LEFT_BRACKET).append(searchField).append(" not in (").append(COLON).append(processedFiled).append(")  )");
			} else if (BEGIN_WITH.equals(searchOper)) {// begins with
				paramsValue.put(processedFiled, searchString + LIKE_SYMBOL);
				sb.append(LEFT_BRACKET).append(searchField).append("  like ").append(COLON).append(processedFiled).append(" )");
			} else if (NOT_BEGIN_WITH.equals(searchOper)) {// does not beginwith
				paramsValue.put(processedFiled, searchString + LIKE_SYMBOL);
				sb.append(LEFT_BRACKET).append(searchField).append(" not like  ").append(COLON).append(processedFiled).append("  )");
			} else if (END_WITH.equals(searchOper)) {// end with
				paramsValue.put(processedFiled, LIKE_SYMBOL + searchString);
				sb.append(LEFT_BRACKET).append(searchField).append(" like  ").append(COLON).append(processedFiled).append(" )");
			} else if (NOT_END_WITH.equals(searchOper)) {// does not end with
				paramsValue.put(processedFiled, LIKE_SYMBOL + searchString);
				sb.append(LEFT_BRACKET).append(searchField).append(" not like ").append(COLON).append(processedFiled).append(" )");
			} else if (CONTAINS.equals(searchOper)) {// contains
				paramsValue.put(processedFiled, LIKE_SYMBOL + searchString + LIKE_SYMBOL);
				sb.append(LEFT_BRACKET).append(searchField).append(" like  ").append(COLON).append(processedFiled).append("  )");
			}
		}
	}

	private String escapeProcessedField(String searchField) {
		String processedFiled = searchField.replaceAll("\\.", "_").replaceAll("\\(", "_").replaceAll("\\)", "_");
		return processedFiled;
	}

	private void assembleQueryParam(Query query) {
		logger.debug("the paramValue is:" + paramsValue);
		logger.debug("the paramType is:" + paramsType);
		for (Entry<String, Object> entry : paramsValue.entrySet()) {
			String key = entry.getKey();
			Object val = entry.getValue();
			String type = paramsType.get(key);
			if (StringUtils.equals(LIST, type)) {
				query.setParameterList(key, (Collection<?>) val);
				continue;
			}
			String value = (String) val;
			if (StringUtils.equals(STRING, type) || (StringUtils.isNotEmpty(value) && (value.indexOf(LIKE_SYMBOL) == 0 || value.lastIndexOf(LIKE_SYMBOL) == value.length() - 1))) {
					query.setString(key, value);
			} else if (StringUtils.equals(DOUBLE, type)) {
				query.setDouble(key, StringUtils.isEmpty(value)?0d:Double.valueOf(value));
			} else if (StringUtils.equals(INTEGER, type)) {
				query.setInteger(key, StringUtils.isEmpty(value)?0:Integer.valueOf(value));
			} else if (StringUtils.equals(LONG, type)) {
				query.setLong(key, StringUtils.isEmpty(value)?0l:Long.valueOf(value));
			} else if (StringUtils.equals(DATE, type)) {
				String dateStr = value.trim();
				try {
					if (dateStr.length() > 12) {
						query.setTimestamp(key, new SimpleDateFormat(DATE_PATTERN_COMPLEX).parse(dateStr));
					} else {
						query.setDate(key, new SimpleDateFormat(DATE_PATTERN_YYYY_MM_DD).parse(dateStr));
					}
				} catch (ParseException e) {
					// TODO need throw out?
					query.setParameter(key, null);
					logger.error("parse Date error ,check the value passed by front,date condition not used.", e);
				}
			} else {
				query.setParameter(key, type);
			}
		}
	}

	public Query assembleAdvacneQuery(AdvanceQueryDto queryDto, String joinSQL) {
		StringBuilder sb = new StringBuilder();
		String globleOp = queryDto.getGroupOp();
		// if contains groups，certified query would be hybrid,or the plain
		paramsValue = new ConcurrentHashMap<>();
		paramsType = new ConcurrentHashMap<>();
/*		boolean containsGroups = queryDto.getGroups() != null && queryDto.getGroups().size() > 0;
		if (!containsGroups) {
			String queryString = joinSQL + parsePlainQuery(sb, queryDto, globleOp);
			StringBuilder orderByStr = new StringBuilder();
			orderByStr.append(queryString);
			addOrderByStr(orderByStr, queryDto);
			Query query = getCurrentSession().createQuery(orderByStr.toString());
			assembleQueryParam(query);
			return query;
		}

*/
		parseHybridQuery(sb, queryDto);
		addOrderByStr(sb, queryDto);
		Query query = getCurrentSession().createQuery(joinSQL + sb.toString());
		assembleQueryParam(query);
		return query;
	}


	@Override
	public void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page) {
		Query query = assembleAdvacneQuery(queryDto, joinSQL);
		page.setAllSize(query.list().size());
		this.initFirstPage(page);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}

	@Override
	public void getInitialQueryResultByHQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page) {
		StringBuilder sb = new StringBuilder();
		sb.append(joinSQL);
		addOrderByStr(sb, queryDto);
		Query query = getCurrentSession().createQuery(sb.toString());
		page.setAllSize(query.list().size());
		this.initFirstPage(page);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}
	
	@Override
	public void getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page) {
		StringBuilder sb = new StringBuilder();
		sb.append(joinSQL);
		addOrderByStr(sb, queryDto);
		Query query = getCurrentSession().createSQLQuery(sb.toString());
		page.setAllSize(query.list().size());
		this.initFirstPage(page);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}
	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category) {
		final	String hql="select   "
		+" new com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto(aqc.displayName as displayName,    "
		+" aqc.columnName as  columnName,        "
		+" aqc.columnType as  columnType,            "
		+" aqc.selectName as  selectName)            "
		+" from AdvanceQueryCondition aqc         "
		+" where aqc.category=:category   and aqc.status=1  "
		+" group by aqc.conditionId                "
		+" order by displayOrder     ";
		  return (List<AdvanceQueryConditionDto>) getCurrentSession().createQuery(hql).setString("category", category).list();
	}

	@Override
	public void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class clazz) {
		Query query = assembleAdvacneQuery(queryDto, joinSQL);
		query.setResultTransformer(Transformers.aliasToBean(clazz));
		page.setAllSize(query.list().size());
		this.initFirstPage(page);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.setList(query.list());
	}
	
}
