package com.sinodynamic.hkgta.dao.ibeacon;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.BeaconManageDto;
import com.sinodynamic.hkgta.entity.ibeacon.IbeaconNearbyPerson;
import com.sinodynamic.hkgta.util.constant.TargetTypeCode;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class IbeaconNearbyPersonDaoImpl extends GenericDao<IbeaconNearbyPerson> implements IbeaconNearbyPersonDao {
	
	
	public ListPage<IbeaconNearbyPerson> getBeaconList(ListPage<IbeaconNearbyPerson> page, BeaconManageDto dto,String byMajor,String status,String targetType){
		List<Serializable> listParam = new ArrayList<Serializable>();
		StringBuilder sql = new StringBuilder("");
		
		if(TargetTypeCode.MBR.name().equalsIgnoreCase(targetType)){
			sql.append(" select a.venue_code as venueCode,a.poi_id as poiId,b.ibeacon_id as ibeaconIdDB,b.minor as minor, b.major as major, "
					+ " b.guid as guid, a.description as beaconName, b.minor as ibeaconId, c.venue_name as locationType, b.status as status,b.create_date as createDate "
				+ " from site_poi a, ibeacon b, venue_master c where a.poi_id=b.site_poi_id and a.venue_code=c.venue_code and a.target_type_code=? ");
		}else if(TargetTypeCode.HKC.name().equalsIgnoreCase(targetType)){
			sql.append(" select b.location as location, r.room_id as pastRoomId, r.room_no as roomNo,a.poi_id as poiId,b.ibeacon_id as ibeaconIdDB,b.minor as minor, b.major as major, "
					+ " b.guid as guid, a.description as beaconName, b.minor as ibeaconId, b.status as status,b.create_date as createDate "
					+ " from site_poi a,ibeacon b, room r where r.site_poi_id = a.poi_id and a.poi_id=b.site_poi_id and a.target_type_code=?  ");
		}
		listParam.add(targetType);
		if(!StringUtils.isEmpty(byMajor)){
			sql.append("and b.major = ? ");
			listParam.add(Long.valueOf(byMajor));
		}
		
		if(!status.equalsIgnoreCase("ALL")){
			sql.append("and b.status = ?");
			listParam.add(status);
		}
		
		if(!StringUtils.isBlank(page.getCondition())){
			String condition = " and "+page.getCondition();
			sql.append(condition);
		}
		
		String countHql = null;
		
		countHql = " SELECT count(a.description) " +sql.substring(sql.indexOf("from"));
		
		return listBySqlDto(page, countHql, sql.toString(), listParam, dto);
	}
	
	

}
