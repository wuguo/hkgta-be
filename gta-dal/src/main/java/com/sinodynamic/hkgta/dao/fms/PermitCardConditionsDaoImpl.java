package com.sinodynamic.hkgta.dao.fms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;

@Repository("permitCardConditions")
public class PermitCardConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao {

	@Autowired
	private ServicePlanDao servicePlanDao;
    @Override
    @Transactional
    public List<AdvanceQueryConditionDto> assembleQueryConditions() {
	
    	//String displayName, String columnName, String columnType, String selectName,Integer displayOrder
	AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID", "academyNo", "java.lang.String", "", 1);
	AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron", "memberName", "java.lang.String", "", 2);
	
	final List<SysCode> memberType=new ArrayList<>();
	SysCode s1=new SysCode();
	s1.setCategory("permitCardmemberType");
	s1.setCodeDisplay("Corporate Dependent Patron");
	s1.setCodeValue("Corporate Dependent Patron");
	memberType.add(s1);

	SysCode s2=new SysCode();
	s2.setCategory("permitCardmemberType");
	s2.setCodeDisplay("Corporate Primary Patron");
	s2.setCodeValue("Corporate Primary Patron");
	memberType.add(s2);
	
	SysCode s3=new SysCode();
	s3.setCategory("permitCardmemberType");
	s3.setCodeDisplay("Individual Primary Patron");
	s3.setCodeValue("Individual Primary Patron");
	memberType.add(s3);
	
	SysCode s4=new SysCode();
	s4.setCategory("permitCardmemberType");
	s4.setCodeDisplay("Individual Dependent Patron");
	s4.setCodeValue("Individual Dependent Patron");
	memberType.add(s4);


	List<ServicePlan> planNameList = servicePlanDao.getByHql("FROM  ServicePlan where passNatureCode='LT' ");
	
	Collection<SysCode> planName = CollectionUtil.map(planNameList, new CallBack<ServicePlan, SysCode> (){

		@Override
		public SysCode execute(ServicePlan r, int index) {
			SysCode sysCode = new SysCode();
			sysCode.setCodeValue(r.getPlanName());
			sysCode.setCodeDisplay(r.getPlanName());
			return sysCode;
		}
		
	});
	
	AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Patron Type", "memberTypeName", "java.lang.String", memberType, 3);
	AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Card No", "cardNo", "java.lang.String", "", 4);
	AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Issue Date", "cardIssueDate", "java.util.Date", "", 6);
	AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Service Plan","planName", String.class.getName(),new ArrayList<SysCode>(planName),7);
	return Arrays.asList(condition1, condition2, condition3, condition4, condition5, condition6);
    }

    @Override
    public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
	
    	//String displayName, String columnName, String columnType, String selectName,Integer displayOrder
	AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID", "academyNo", "java.lang.String", "", 1);
	AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron", "memberName", "java.lang.String", "", 2);
	
	final List<SysCode> memberType=new ArrayList<>();
	SysCode s1=new SysCode();
	s1.setCategory("permitCardmemberType");
	s1.setCodeDisplay("Corporate Dependent Patron");
	s1.setCodeValue("Corporate Dependent Patron");
	memberType.add(s1);

	SysCode s2=new SysCode();
	s2.setCategory("permitCardmemberType");
	s2.setCodeDisplay("Corporate Primary Patron");
	s2.setCodeValue("Corporate Primary Patron");
	memberType.add(s2);
	
	SysCode s3=new SysCode();
	s3.setCategory("permitCardmemberType");
	s3.setCodeDisplay("Individual Primary Patron");
	s3.setCodeValue("Individual Primary Patron");
	memberType.add(s3);
	
	SysCode s4=new SysCode();
	s4.setCategory("permitCardmemberType");
	s4.setCodeDisplay("Individual Dependent Patron");
	s4.setCodeValue("Individual Dependent Patron");
	memberType.add(s4);

	List<ServicePlan> planNameList = servicePlanDao.getByHql("FROM  ServicePlan where passNatureCode='LT' ");
	
	Collection<SysCode> planName = CollectionUtil.map(planNameList, new CallBack<ServicePlan, SysCode> (){

		@Override
		public SysCode execute(ServicePlan r, int index) {
			SysCode sysCode = new SysCode();
			sysCode.setCodeValue(r.getPlanName());
			sysCode.setCodeDisplay(r.getPlanName());
			return sysCode;
		}
		
	});

	
	AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Patron Type", "memberTypeName", "java.lang.String", memberType, 3);
	AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Card No", "cardNo", "java.lang.String", "", 4);
	AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Issue Date", "cardIssueDate", "java.util.Date", "", 6);
	AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Service Plan","planName", String.class.getName(),new ArrayList<SysCode>(planName),7);
	return Arrays.asList(condition1, condition2, condition3, condition4, condition5, condition6);
    }

    
}
