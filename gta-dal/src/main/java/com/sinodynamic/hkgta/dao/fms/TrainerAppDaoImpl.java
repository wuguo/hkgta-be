package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.staff.CoachReservationDto;
import com.sinodynamic.hkgta.dto.staff.CoachSessionListDto;
import com.sinodynamic.hkgta.dto.staff.MembersPresentCurriculumListDto;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class TrainerAppDaoImpl extends GenericDao implements TrainerAppDao {
	String getCountSql(String sql){
		return "SELECT COUNT(*) FROM (" + sql +") sub";
	}
	
	public ListPage<CoachReservationDto> reservationList(String coachId, String orderColumn, String order, int pageSize,
			int currentPage, String filterSQL){
		ListPage<CoachReservationDto> pageParam = new ListPage<CoachReservationDto>(pageSize, currentPage);
		
		if(order == null) {
			order = "asc";
		}
		if(order.equals("asc")){
			pageParam.addAscending(orderColumn);
		}
		if(order.equals("desc")){
			pageParam.addDescending(orderColumn);
		}
		
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("id", LongType.INSTANCE);
		typeMap.put("sessionId", StringType.INSTANCE);
		typeMap.put("beginTime", TimestampType.INSTANCE);
		typeMap.put("endTime", TimestampType.INSTANCE);
		typeMap.put("facilityName", StringType.INSTANCE);
		typeMap.put("courseName", StringType.INSTANCE);
		typeMap.put("bayType", StringType.INSTANCE);
		typeMap.put("facilityType", StringType.INSTANCE);
		typeMap.put("category", StringType.INSTANCE);
		typeMap.put("customerName", StringType.INSTANCE);
		typeMap.put("academyNo", StringType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("capacity", LongType.INSTANCE);
		typeMap.put("attendance", LongType.INSTANCE);
		typeMap.put("portraitPhoto", StringType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("timeGap", DoubleType.INSTANCE);
		
		
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT \n");
		sql.append("    rsvId id,\n");
		sql.append("    sessionId,\n");
		sql.append("    courseName,\n");
		sql.append("    staffBegin beginTime,\n");
		sql.append("    staffEnd endTime,\n");
		sql.append("    timeGap,\n");
		sql.append("    facilityName,\n");
		sql.append("    bayType,\n");
		sql.append("    facilityType,\n");
		sql.append("    category,\n");
		sql.append("    customerName,\n");
		sql.append("    academyNo,\n");
		sql.append("    customerId,\n");
		sql.append("    capacity,\n");
		sql.append("    attendance,\n");
		sql.append("    portraitPhoto,\n");
		sql.append("    status\n"); 
		sql.append("FROM\n");
		sql.append("    (SELECT \n");
		sql.append("            book.resv_id rsvId,\n");//resvId
		sql.append("            NULL sessionId,\n");
		sql.append("            NULL courseName,\n");
		sql.append("            min(slot.begin_datetime) staffBegin,\n");
		sql.append("            max(date_add(slot.end_datetime, interval 1 second)) staffEnd,\n");
		sql.append("            ROUND(TIMESTAMPDIFF(SECOND, NOW(), slot.begin_datetime)/3600, 2) timeGap,\n");
		sql.append("            facility.facility_name facilityName,\n");
		sql.append("            CASE facility.facility_type\n");
		sql.append("                WHEN 'GOLF' THEN 'Golfing Bay'\n");
		sql.append("                WHEN 'TENNIS' THEN 'Tennis Bay'\n");
		sql.append("            END facilityType,\n");
		/*  Mars ,update for SGG-450   :[Coach app] Display " Off" in Bay type
		sql.append("            (CASE facility_attr.input_value is null \n");
		sql.append("            WHEN true \n");
		sql.append("                THEN subtype.name \n");
		sql.append("                ELSE facility_attr.input_value \n");
		sql.append("            END) bayType,\n"); 
		 */
	    sql.append("			CASE                             \n");
	    sql.append(" 			WHEN book.resv_facility_type = 'TENNIS'    \n");
	    sql.append("            THEN subtype.name                          \n");
	    sql.append("            ELSE fa.caption                            \n");
	    sql.append("            END AS BayType,                             \n");
	
		sql.append("            'Coaching' category,\n");
		sql.append("            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) customerName,\n");
		sql.append("            m.academy_no academyNo,\n");
		sql.append("            m.customer_id customerId,\n");
		sql.append("            NULL capacity,\n");
		sql.append("            NULL attendance,\n");
		sql.append("            pro.portrait_photo portraitPhoto,\n");
		sql.append("            book.status status\n");
		sql.append("    FROM\n");
		sql.append("        staff_timeslot slot\n");
		sql.append("    LEFT JOIN member_reserved_staff book_slot ON book_slot.staff_timeslot_id = slot.staff_timeslot_id\n");
		sql.append("    LEFT JOIN member_facility_type_booking book ON book.resv_id = book_slot.resv_id\n");
		sql.append("    LEFT JOIN member_reserved_facility book_facility ON book_facility.resv_id = book.resv_id\n");
		sql.append("    LEFT JOIN facility_timeslot facility_slot ON facility_slot.facility_timeslot_id = book_facility.facility_timeslot_id\n");
		sql.append("    LEFT JOIN facility_master facility ON facility.facility_no = facility_slot.facility_no\n");
		sql.append("    LEFT JOIN facility_sub_type subtype on subtype.subtype_id = facility.facility_subtype_id\n");
		sql.append("    LEFT JOIN facility_addition_attribute facility_attr ON facility_attr.facility_no = facility.facility_no\n");
		sql.append("    LEFT JOIN customer_profile pro ON pro.customer_id = book.customer_id\n");
		sql.append("    LEFT JOIN member m ON m.customer_id = book.customer_id\n");
		
		/*  Mars ,update for SGG-450   :[Coach app] Display " Off" in Bay type*/
	    sql.append("    LEFT JOIN member_facility_book_addition_attr a  \n");
	    sql.append("      ON book.resv_id = a.resv_id                   \n");
	    sql.append("    LEFT JOIN facility_attribute_caption fa         \n");
	    sql.append("      ON a.attribute_id = fa.attribute_id           \n");
	    /*  Mars ,update for SGG-450   :[Coach app] Display " Off" in Bay type*/
	    
		sql.append("    WHERE\n");
		sql.append("        slot.duty_category IN ('PVTCOACH' , 'PVGCOACH')\n");
		sql.append("            AND slot.begin_datetime = facility_slot.begin_datetime\n");
		sql.append("            AND slot.end_datetime = facility_slot.end_datetime\n");
//		sql.append("            AND slot.end_datetime > NOW()\n");
		sql.append("            AND slot.staff_user_id = ?\n");
		sql.append("    GROUP BY book.resv_id UNION ALL SELECT \n");
		sql.append("            sub.course_id id,\n");
		sql.append("            sessionId,\n");
		sql.append("            courseName,\n");
		sql.append("            staffBegin,\n");
		sql.append("            staffEnd,\n");
		sql.append("            timeGap,\n");
		sql.append("            facilityName,\n");
		sql.append("            bayType,\n");
		sql.append("            facilityType,\n");
		sql.append("            category,\n");
		sql.append("            customerName,\n");
		sql.append("            academyNo,\n");
		sql.append("            customerId,\n");
		sql.append("            capacity,\n");
		sql.append("            IFNULL(attendance, 0) attendance,\n");
		sql.append("            portraitPhoto,\n");
		sql.append("            status\n");
		sql.append("    FROM\n");
		sql.append("        (SELECT \n");
		sql.append("            course.course_id,\n");
		sql.append("            sess.sys_id sessionId,\n");
		sql.append("            course.course_name courseName,\n");
		sql.append("            sess.begin_datetime staffBegin,\n");
		sql.append("            sess.end_datetime  staffEnd,\n");
		sql.append("            ROUND(TIMESTAMPDIFF(SECOND, NOW(), sess.begin_datetime)/3600, 2) timeGap,\n");
		sql.append("            GROUP_CONCAT(DISTINCT facility.facility_name) facilityName,\n");
		sql.append("            (CASE facility_attr.input_value is null \n");
		sql.append("            WHEN true \n");
		sql.append("                THEN subtype.name \n");
		sql.append("                ELSE facility_attr.input_value \n");
		sql.append("            END) bayType,\n");
		sql.append("            CASE facility.facility_type\n");
		sql.append("                WHEN 'GOLF' THEN 'Golfing Bay'\n");
		sql.append("                WHEN 'TENNIS' THEN 'Tennis Bay'\n");
		sql.append("            END facilityType,\n");
		sql.append("            'Course' category,\n");
		sql.append("            NULL customerName,\n");
		sql.append("            NULL academyNo,\n");
		sql.append("            NULL customerId,\n");
		sql.append("            course.capacity,\n");
		sql.append("            NULL portraitPhoto,\n");
		sql.append("            sess.status status\n");
		sql.append("    FROM\n");
		sql.append("        course_master course\n");
		sql.append("    LEFT JOIN course_session sess ON sess.course_id = course.course_id\n");
//		sql.append("    LEFT JOIN course_master coursemaster ON coursemaster.course_id = course.course_id\n");
		sql.append("    LEFT JOIN staff_timeslot slot ON slot.staff_timeslot_id = sess.coach_timeslot_id\n");
		sql.append("    LEFT JOIN course_session_facility sess_facility ON sess_facility.course_session_id = sess.sys_id\n");
		sql.append("    LEFT JOIN facility_timeslot facility_slot ON facility_slot.facility_timeslot_id = sess_facility.facility_timeslot_id\n");
		sql.append("    LEFT JOIN facility_master facility ON facility.facility_no = facility_slot.facility_no\n");
		sql.append("    LEFT JOIN facility_sub_type subtype on subtype.subtype_id = facility.facility_subtype_id\n");
		sql.append("    LEFT JOIN facility_addition_attribute facility_attr ON facility_attr.facility_no = facility.facility_no\n");
		sql.append("    WHERE\n");
		sql.append("        slot.duty_category IN ('TSS' , 'GSS', 'TRAINER')\n");
//		sql.append("            AND slot.end_datetime > NOW()\n");
		sql.append("            AND slot.staff_user_id = ?\n");
		sql.append("    GROUP BY slot.begin_datetime , slot.end_datetime )sub \n");
		sql.append("    LEFT JOIN (SELECT \n");
		sql.append("        course_id, COUNT(customer_id) attendance\n");
		sql.append("    FROM\n");
		sql.append("        course_enrollment\n");
		sql.append("    WHERE\n");
		sql.append("        status IN ('ACT' , 'REG')\n");
		sql.append("    GROUP BY course_id) AS attend ON attend.course_id = sub.course_id\n ");
		//********************************添加显示取消的课程  TODO*************************************************/
		sql.append("     UNION ALL SELECT \n");
		sql.append("            course.course_id id,\n");
		sql.append("            sess.sys_id sessionId,\n");
		sql.append("            course.course_name courseName,\n");
		sql.append("            sess.begin_datetime staffBegin,\n");
		sql.append("            sess.end_datetime  staffEnd,\n");
		sql.append("            ROUND(TIMESTAMPDIFF(SECOND, NOW(), sess.begin_datetime)/3600, 2) timeGap,\n");
		sql.append("            NULL facilityName,\n");
		sql.append("            NULL bayType,\n");
		sql.append("            NULL facilityType,\n");
		sql.append("            'Course' category,\n");
		sql.append("            NULL customerName,\n");
		sql.append("            NULL academyNo,\n");
		sql.append("            NULL customerId,\n");
		sql.append("            course.capacity,\n");
		sql.append("            NULL attendance,\n");
		sql.append("            NULL portraitPhoto,\n");
		sql.append("            sess.status status\n");
		sql.append("    FROM\n");
		sql.append("        course_master course\n");
		sql.append("    LEFT JOIN course_session sess ON sess.course_id = course.course_id\n");
		sql.append("    WHERE\n");
		sql.append("         sess.coach_timeslot_id IS NULL AND sess.status = 'CAN' AND sess.coach_user_id = ? \n");
		sql.append("     ) rsv_list\n");


		if (filterSQL!=null)sql.append(filterSQL);
		return this.listBySqlDto(
				pageParam,
				getCountSql(sql.toString()),
				sql.toString(),
				Collections.nCopies(3, coachId),
				new CoachReservationDto(),
				typeMap);
	}

	<T> T getValue(Class<T> clazz,Map<String, Object> params, String key){
		return (T)params.get(key);
	}
	
	@Override
	public Object calculateCoachAchievement(
			AchievementClassification classification, Map<String, Object> params) {
		String sql = getCoachAchievementSql(classification);
		SQLQuery query = this.getCurrentSession().createSQLQuery(sql);
		switch (classification){
			case HOUR_OF_COURSES:
			case HOUR_OF_CLASSES:
				Assert.notNull(params.get("coachId"), "Missing coach id");
				Assert.notNull(params.get("start"), "Missing request Month");
				query.setString(0, getValue(String.class, params, "coachId"));
				query.setDate(1, getValue(Date.class, params, "start"));
				query.setDate(2, getValue(Date.class, params, "end"));
				break;
				
			case TOTAL_OF_NEW_STUDENTS:
				Assert.notNull(params.get("coachId"), "Missing coach id");
				Assert.notNull(params.get("requestMonth"), "Missing request Month");
				
				query.setString(0, getValue(String.class, params, "coachId"));
				query.setString(1, getValue(String.class, params, "requestMonth"));
				query.setString(2, getValue(String.class, params, "coachId"));
				query.setString(3, getValue(String.class, params, "requestMonth"));
				break;
				
			case TOTAL_OF_STUDENTS:
				Assert.notNull(params.get("coachId"), "Missing coach id");
				Assert.notNull(params.get("requestMonth"), "Missing request Month");
				query.setString(0, getValue(String.class, params, "coachId"));
				query.setString(1, getValue(String.class, params, "requestMonth"));
				break;
			default:
				break;
		}
		return query.uniqueResult();
	}

	
	String getCoachAchievementSql(
			AchievementClassification classification) {
		String coachingCount = "SELECT \n" +
				"    SUM(CEIL(TIMESTAMPDIFF(SECOND,slot.begin_datetime,slot.end_datetime) / 3600)) achievement\n" +
				"FROM\n" +
				"    staff_timeslot slot\n" +
				"WHERE slot.staff_user_id = ?\n" +
				"    AND slot.duty_category IN ('PVTCOACH' , 'PVGCOACH')\n" +
				"    AND slot.begin_datetime BETWEEN ? AND ?\n";
		String courseCount = 
				"SELECT \n" +
				"    SUM(CEIL(TIMESTAMPDIFF(SECOND, slot.begin_datetime, slot.end_datetime) / 3600)) achievement\n" +
				"FROM\n" +
				"    staff_timeslot slot\n" +
				"        JOIN\n" +
				"    course_session sess ON sess.coach_timeslot_id = slot.staff_timeslot_id\n" +
				"WHERE sess.status = 'ACT'\n" +
				"    AND slot.staff_user_id = ?\n" +
				"    AND slot.duty_category IN ('GSS' , 'TSS', 'TRAINER')\n" +
				"    AND slot.begin_datetime BETWEEN ? AND ?\n";
		
		String studentCount = 
				"SELECT COUNT(DISTINCT customer_id) achievement \n" + 
				"FROM member_facility_type_booking \n" + 
				"WHERE STATUS = 'ATN' \n" + 
					"AND exp_coach_user_id = ? \n" + 
					"AND DATE_FORMAT(begin_datetime_book, '%Y-%m') = ? \n";
					
				
//				"SELECT \n" +
//				"    COUNT(DISTINCT book.customer_id) achievement\n" +
//				"FROM\n" +
//				"    member_facility_type_booking book\n" +
//				"        JOIN\n" +
//				"    member_reserved_staff rsv ON rsv.resv_id = book.resv_id\n" +
//				"        JOIN\n" +
//				"    staff_timeslot slot ON slot.staff_timeslot_id = rsv.staff_timeslot_id\n" +
//				"WHERE\n" +
//				"    slot.staff_user_id = ?\n" +
//				"        AND slot.duty_category IN ('PVTCOACH' , 'PVGCOACH')\n";
		if(classification == AchievementClassification.HOUR_OF_COURSES){
			return courseCount;
		}
		if(classification == AchievementClassification.HOUR_OF_CLASSES){
			return coachingCount;
		}
		if(classification == AchievementClassification.TOTAL_OF_STUDENTS){
			return studentCount;
		}
		if(classification == AchievementClassification.TOTAL_OF_NEW_STUDENTS){
			studentCount = studentCount + "AND customer_id NOT IN ( \n" + 
												"SELECT customer_id \n" + 
												"FROM member_facility_type_booking \n" + 
												"WHERE STATUS = 'ATN' \n" + 
													"AND exp_coach_user_id = ? \n" + 
													"AND DATE_FORMAT(begin_datetime_book, '%Y-%m') < ? )\n";
			return studentCount;
		}
		//modified by Kaster 20160510 将抛出异常统一规范化
		throw new GTACommonException(GTAError.CoachMgrError.UNKNOWN_COACH_ACHIEVEMENT_CLASSIFICATION);
	}

	@Override
	public List<CustomerProfileDto> getPrivateCoachTrainedMemberList(
			String staffNo, String isAsending) {
		List<CustomerProfileDto> list = this.getPrivateCoachTrainedMemberListByName(staffNo, null, isAsending);
		return list;
	}

	@Override
	public List<CustomerProfileDto> getPrivateCoachTrainedMemberListByName(
		String staffNo, String name, String isAsending) {
		StringBuilder sql = new StringBuilder(" SELECT m.customer_id as customerId2, ")
			.append(" m.academy_no as academyID, ") 
			.append(" cp.signature as signature, ")
			.append(" cp.salutation as salutation, ")
			.append("  cp.given_name as givenName, ")
			.append("  cp.portrait_photo as portraitPhoto, ")
			.append("  m.vip as memberIsVIP, ")
			.append(" cp.surname as surname ")
			.append(" from member_facility_attendance atte ")
			.append(" LEFT JOIN member_reserved_facility res on res.facility_timeslot_id = atte.facility_timeslot_id ")
			.append(" LEFT JOIN member_facility_type_booking booking on res.resv_id = booking.resv_id ")
			.append(" LEFT JOIN member m on m.customer_id = booking.customer_id ")
			.append(" LEFT JOIN customer_profile cp on m.customer_id = cp.customer_id")
			.append(" WHERE booking.exp_coach_user_id = ? ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(staffNo);
		if(!StringUtils.isEmpty(name)){
			sql.append(" and  concat( cp.salutation , ' ',cp.surname , ' ', cp.given_name) like ?");
			param.add("%"+name+"%");
		}
		sql.append("GROUP BY m.customer_id ");
		
		if ("true".equalsIgnoreCase(isAsending))
		{
			sql.append(" order by cp.given_name asc, atte.attend_time desc ");
		}
		else if ("false".equalsIgnoreCase(isAsending))
		{
			sql.append(" order by cp.given_name desc, atte.attend_time desc ");
		}
		else
		{
			sql.append(" order by atte.attend_time desc ");
		}
		List<CustomerProfileDto> list = this.getDtoBySql(sql.toString(), param, CustomerProfileDto.class);
		return list;
	}
	
	
	/**
	 * 根据条件参数查询课程列表
	 * @param coachId    staffId
	 * @param showType	0：查询私人教练课程同大学堂课程    1：查询私人教练课程   2：大学堂课程 
	 * @return
	 */
	@Override
	public List<CoachSessionListDto> reservationListBySessionDate(String coachId, int showType){
		List<Serializable> param = new ArrayList<Serializable>();
		Date time = new  Date();
		StringBuilder sql = new StringBuilder("")
				.append(" SELECT                                                                                 ") 
				.append("         rsvId id,                                                                      ") 
				.append("         sessionId,                                                                     ") 
				.append("         sessionNo,                                                                 ")
				.append("         courseName,                                                                    ")
				.append("         beginTime,                                                                     ")
				.append("         endTime,                                                                       ")
				.append("         timeGap,                                                                       ")
				.append("         category,                                                                      ")
				.append("         customerName,                                                                  ")
				.append("         academyNo,                                                                     ")
				.append("         customerId,                                                                    ")
				.append("         portraitPhoto ,                                                                ")
				.append("         status                                                                         ")
				.append("     FROM                                                                               ")
				.append("         (                                                                              ");
				if (showType == 0 || showType == 1) {
					sql.append(" SELECT                                                                              ")
					.append("             book.resv_id rsvId,                                                        ")
					.append("             NULL sessionId,                                                            ")
					.append("             NULL sessionNo,                                                            ")
					.append("             NULL courseName,                                                           ")
					.append("             min(slot.begin_datetime) beginTime,                                        ")
					.append("             max(date_add(slot.end_datetime,	                                         ")
					.append("             interval 1 second)) endTime,	                                             ")
					.append("             ROUND(TIMESTAMPDIFF(SECOND,	                                         ")
					.append("             NOW(),				                                         ")
					.append("             slot.begin_datetime)/3600,                                                 ")
					.append("             2) timeGap,		                                                 ")
					.append("             'Coaching' category,	                                                 ")
					.append("             CONCAT(pro.salutation,	                                                 ")
					.append("             ' ',			                                                 ")
					.append("             pro.given_name,                                                            ")
					.append("             ' ',			                                                 ")
					.append("             pro.surname) customerName,                                                 ")
					.append("             m.academy_no academyNo,	                                                 ")
					.append("             m.customer_id customerId,	                                                 ")
					.append("             pro.portrait_photo portraitPhoto,                                          ")
					.append("             book.status status     	                                                 ")
					.append("         FROM				                                                 ")
					.append("             staff_timeslot slot     	                                                 ")
					.append("         LEFT JOIN			                                                 ")
					.append("             member_reserved_staff book_slot                                            ")
					.append("                 ON book_slot.staff_timeslot_id = slot.staff_timeslot_id                ")
					.append("         LEFT JOIN							                                             ")
					.append("             member_facility_type_booking book 			                             ")
					.append("                 ON book.resv_id = book_slot.resv_id     		                         ")
					.append("          								                                                 ")
					.append("         LEFT JOIN                                                                      ")
					.append("             customer_profile pro                                                       ")
					.append("                 ON pro.customer_id = book.customer_id                                  ")
					.append("         LEFT JOIN					                                                     ")
					.append("             member m 					                                                 ")
					.append("                 ON m.customer_id = book.customer_id                                    ")
					.append(" LEFT JOIN member_facility_attendance mfa ON mfa.customer_id = m.customer_id            ")
					.append("                   					                                                 ")
					.append("         WHERE                                                                          ")
					.append("             slot.duty_category IN (                                                    ")
					.append("                 'PVTCOACH' , 'PVGCOACH'                                                ")
					.append("             )                                                                          ")
					.append("                 AND book.status = 'ATN'	AND mfa.attend_time IS NULL                  ")
					.append(" 	    AND date_sub(slot.begin_datetime, interval '30' minute)  <=  ?               ")  
					.append("             AND date_sub(slot.end_datetime, interval '-30' minute) >= ?             ")      
					.append("             AND slot.staff_user_id = ?                                                 ")
					.append("         GROUP BY                                                                       ")
					.append("             book.resv_id                                                               ");
					param.add(time);
					param.add(time);
					param.add(coachId);
				}
				if (showType == 0 ) {
					sql.append("         UNION    ALL                                                            ");
				}
				if (showType == 0 || showType == 2) {
					sql.append("          SELECT                                                                     ")
					.append("             sub.course_id id,                                                          ")
					.append("             sessionId,                                                                 ")
					.append("             sessionNo,                                                                 ")
					.append("             courseName,                                                                ")		          
					.append("             beginTime,                                                                 ")
					.append("             endTime,	                                                                 ")
					.append("             timeGap,                                                                   ")
					.append("             category,                                                                  ")
					.append("             customerName,                                                              ")
					.append("             academyNo,                                                                 ")
					.append("             customerId,                                                                ")
					.append("             portraitPhoto,                                                             ")
					.append("             status                                                                     ")
					.append("         FROM                                                                           ")
					.append("             (SELECT                                                                    ")
					.append("                 sess.course_id,                                                        ")
					.append("                 sess.sys_id sessionId,                                                 ")
					.append("                 sess.session_no sessionNo,                                             ")
					.append("                 coursemaster.course_name courseName,                                   ")
					.append("                 sess.begin_datetime beginTime,                                         ")
					.append("                 date_add(sess.end_datetime,	                                         ")
					.append("                 interval 1 second) endTime,	                                         ")
					.append("                 ROUND(TIMESTAMPDIFF(SECOND,	                                         ")
					.append("                 NOW(),                                                                 ")
					.append("                 slot.begin_datetime)/3600,                                             ")
					.append("                 2) timeGap,		                                                     ")
					.append("                 'Course' category,	                                                 ")
					.append("                 NULL customerName,	                                                 ")
					.append("                 NULL academyNo,	                                                     ")
					.append("                 NULL customerId,                                                       ")
					.append("                 NULL portraitPhoto,                                                    ")
					.append("                 sess.status status                                                     ")    
					.append("             FROM		                                                                 ")
					.append("                 course_session sess                                                    ") 
					.append("             LEFT JOIN                                                                  ")
					.append("                 course_master coursemaster 				                             ")
					.append("                     ON coursemaster.course_id = sess.course_id     	                 ")
					.append("             LEFT JOIN							                                         ")
					.append("                 staff_timeslot slot 					                                 ")
					.append("                     ON slot.staff_timeslot_id = sess.coach_timeslot_id                 ")
					.append("             WHERE							                                             ")
					.append("                 slot.duty_category IN (				                                 ")
					.append("                     'TSS' , 'GSS', 'TRAINER'				                             ")
					.append("                 )                                                                      ")
					.append("                 AND slot.staff_user_id = ?                                             ")
					.append("                 AND date_sub(sess.begin_datetime, interval '30' minute)  <=  ?     ")             
					.append("                 AND date_sub(sess.end_datetime, interval '-30' minute) >= ?         ")   
					.append("             GROUP BY					                                                 ")				
					.append("                 slot.begin_datetime ,			                                         ")				
					.append("                 slot.end_datetime) sub     		                                     ");
					param.add(coachId);
					param.add(time);
					param.add(time);
				}
				sql.append("             ) rsv_list  				                                                 ");
			List<CoachSessionListDto> list = this.getDtoBySql(sql.toString(), param, CoachSessionListDto.class);
			return list;
		}
	
	
	/**
	 * 根据条件参数查询课程列表
	 * @param QRCode    academyNo/cardNo
	 * @param QRCodeType	1：academyNo    2：cardNo    
	 * @return
	 */
	@Override
	public List<MembersPresentCurriculumListDto> getMembersPresentCurriculumList(String QRCode, int QRCodeType){
		List<Serializable> param = new ArrayList<Serializable>();
		Date time = new  Date();
		StringBuilder sql = new StringBuilder("");
		List<MembersPresentCurriculumListDto> list = new  ArrayList<MembersPresentCurriculumListDto>();
				switch (QRCodeType) {
				case 1:
					sql.append(" SELECT                                                                                   ")   
					.append(" id,                   						                   ")   
					.append(" sessionId,                       					                   ")
					.append(" sessionNo,                       						               ")
					.append(" courseName,               						                   ")   
					.append(" beginTime,                						                   ")   
					.append(" endTime,	                       					                   ")   
					.append(" category,                 						                   ")   
					.append(" academyNo,                      					                   ")   
					.append(" status             							                   ")   
					.append(" FROM             							                   ")   
					.append(" (SELECT 								                   ")   
					.append(" cs.course_id id,  							                   ")   
					.append(" cs.sys_id sessionId,   						                   ") 
					.append(" cs.session_no sessionNo,   							             ")
					.append(" cm.course_name courseName ,   						                   ")   
					.append(" cs.begin_datetime beginTime,      					                   ")   
					.append(" date_add(cs.end_datetime,	     					                   ")   
					.append(" interval 1 second) endTime,	    		           		                   ")   
					.append(" 'Course' category,	        	      				                   ")   
					.append(" m.academy_no academyNo,	            				                   ")   
					.append(" cs.status status     							                   ")   
					.append(" FROM member m 							                   ")   
					.append(" LEFT JOIN course_enrollment ce ON m.customer_id = ce.customer_id 	                   ")   
					.append(" LEFT JOIN course_session cs ON cs.course_id = ce.course_id 		                   ")   
					.append(" LEFT JOIN  course_master cm ON cm.course_id = cs.course_id 		                   ")  
					.append(" LEFT JOIN  student_course_attendance sca ON sca.enroll_id = ce.enroll_id 		                   ") 
					.append(" WHERE m.status = 'ACT' AND ce.status = 'REG' AND (sca.status = 'NATD' OR sca.status is NULL) AND m.academy_no = ?	                   ")   
					.append(" AND date_sub(cs.begin_datetime, interval '30' minute)  <=  ?    	                   ")   
					.append(" AND date_sub(cs.end_datetime, interval '-30' minute) >= ?                             ")   
					.append(" UNION    ALL  							                   ")   
					.append(" SELECT 								                   ")   
					.append(" book.resv_id id, 							                   ")   
					.append(" NULL sessionId,  							                   ")
					.append(" NULL sessionNo,  								               ")
					.append(" '' courseName, 							                   ")   
					.append(" min(slot.begin_datetime) beginTime,  					                   ")   
					.append(" max(date_add(slot.end_datetime,					                   ")   
					.append(" interval 1 second)) endTime,						                   ")   
					.append(" 'Coaching' category,							                   ")   
					.append(" m.academy_no academyNo,						                   ")   
					.append(" book.status status     	 					                   ")   
					.append(" FROM member m 							                   ")   
					.append(" LEFT JOIN member_facility_type_booking book 				                   ")   
					.append("  ON book.customer_id  =   m.customer_id 				                   ")   
					.append(" LEFT JOIN member_reserved_staff book_slot     			                   ")   
					.append("  ON book_slot.resv_id = book.resv_id  				                   ")   
					.append(" LEFT JOIN staff_timeslot slot 					                   ")   
					.append("  ON book_slot.staff_timeslot_id = slot.staff_timeslot_id   		                   ") 
					.append(" LEFT JOIN member_facility_attendance mfa ON mfa.customer_id = m.customer_id       ")
					.append(" WHERE slot.duty_category IN ('PVTCOACH' , 'PVGCOACH')    		                   ")   
					.append(" AND m.academy_no = ? 	AND book.status = 'ATN'	AND mfa.attend_time IS NULL            ")   
					.append(" AND date_sub(slot.begin_datetime, interval '30' minute)  <=  ?   	                   ")   
					.append(" AND date_sub(slot.end_datetime, interval '-30' minute) >= ?     	                   ")   
					.append(" GROUP BY  book.resv_id  						                   ")   
					.append(" ) rsv_list								                   ");   
					break;
				case 2:
					sql.append(" SELECT                                                                                     ")  
					.append(" id,                   							             ")
					.append(" sessionId,                       						             ")
					.append(" sessionNo,                       						             ")
					.append(" courseName,               							             ")
					.append(" beginTime,                							             ")
					.append(" endTime,	                       						             ")
					.append(" category,                 							             ")
					.append(" academyNo,                      						             ")
					.append(" status             								             ")
					.append(" FROM             								             ")
					.append(" (SELECT 									             ")
					.append(" cs.course_id id,  								             ")
					.append(" cs.sys_id sessionId,   							             ")
					.append(" cs.session_no sessionNo,   							             ")
					.append(" cm.course_name courseName,   							             ")
					.append(" cs.begin_datetime beginTime,      						             ")
					.append(" date_add(cs.end_datetime,	     						             ")
					.append(" interval 1 second) endTime,	    		           			             ")
					.append(" 'Course' category,	        	      					             ")
					.append(" m.academy_no academyNo,	            					             ")
					.append(" cs.status status    								             ")
					.append(" FROM permit_card_master cpm 							             ")
					.append(" LEFT JOIN course_enrollment ce ON cpm.mapping_customer_id = ce.customer_id 	             ")
					.append(" LEFT JOIN course_session cs ON cs.course_id = ce.course_id 			             ")
					.append(" LEFT JOIN course_master cm ON cm.course_id = cs.course_id 			             ")
					.append(" LEFT JOIN  student_course_attendance sca ON sca.enroll_id = ce.enroll_id 		                   ") 
					.append(" LEFT JOIN member m ON m.customer_id = ce.customer_id  			             ")
					.append(" WHERE cpm.status = 'ISS' AND ce.status = 'REG' AND (sca.status = 'NATD' OR sca.status is NULL) AND ce.status = 'REG'  AND cpm.card_no = ?		             ")
					.append(" AND date_sub(cs.begin_datetime, interval '30' minute)  <=  ?    		             ")
					.append(" AND date_sub(cs.end_datetime, interval '-30' minute) >= ?     		             ")
					.append(" UNION    ALL  								             ")
					.append(" SELECT 									             ")
					.append(" book.resv_id id, 								             ")
					.append(" NULL sessionId,  								             ")
					.append(" NULL sessionNo,  								             ")
					.append(" '' courseName, 								             ")
					.append(" min(slot.begin_datetime) beginTime,  						             ")
					.append(" max(date_add(slot.end_datetime,						             ")
					.append(" interval 1 second)) endTime,							             ")
					.append(" 'Coaching' category,								             ")
					.append(" m.academy_no academyNo,							             ")
					.append(" book.status status     	 						             ")
					.append(" FROM permit_card_master cpm 							             ")
					.append(" LEFT JOIN member m ON m.customer_id = cpm.mapping_customer_id  		             ")
					.append(" LEFT JOIN member_facility_attendance mfa ON mfa.customer_id = m.customer_id       ")
					.append(" LEFT JOIN member_facility_type_booking book 					             ")
					.append("  ON book.customer_id  = cpm.mapping_customer_id 				             ")
					.append(" LEFT JOIN member_reserved_staff book_slot     				             ")
					.append("  ON book_slot.resv_id = book.resv_id  					             ")
					.append(" LEFT JOIN staff_timeslot slot							             ")
					.append("  ON book_slot.staff_timeslot_id = slot.staff_timeslot_id   			             ")
					.append(" WHERE slot.duty_category IN ('PVTCOACH' , 'PVGCOACH') 			             ")
					.append(" AND cpm.card_no = ? AND book.status = 'ATN'	AND mfa.attend_time IS NULL     ")
					.append(" AND date_sub(slot.begin_datetime, interval '30' minute) <= ?   		             ")
					.append(" AND date_sub(slot.end_datetime, interval '-30' minute) >= ?     		             ")
					.append(" GROUP BY book.resv_id 							             ")
					.append(" ) rsv_list							             ");
					break;

				default:
					return list;
				}
			param.add(QRCode);
			param.add(time);
			param.add(time);
			param.add(QRCode);
			param.add(time);
			param.add(time);
			list = this.getDtoBySql(sql.toString(), param, MembersPresentCurriculumListDto.class);
			return list;
		}
}
