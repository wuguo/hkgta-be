package com.sinodynamic.hkgta.dao.pms;


import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.RoomPreassignSync;
@Repository
public class RoomPreassignSyncDaoImpl extends GenericDao<RoomPreassignSync> implements RoomPreassignSyncDao {

	@Override
	public boolean removeAll() {
		String hql="delete from RoomPreassignSync";
		int executeUpdate= super.hqlUpdate(hql);
		
		if(executeUpdate>0){
			return true;
		}else{
			return false;
		}
	}
	@Override
	public boolean saveOrUpdate(RoomPreassignSync sync) {
	   return super.saveOrUpdate(sync);
	}


}
