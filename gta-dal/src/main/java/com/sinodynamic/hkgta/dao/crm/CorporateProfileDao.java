package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.CorporateProfileDto;
import com.sinodynamic.hkgta.entity.crm.CorporateProfile;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CorporateProfileDao extends IBaseDao<CorporateProfile>{

	public Serializable saveCorporateProf(CorporateProfile corporateProfile);
	
	public boolean updateCorporateProf(CorporateProfile corporateProfile);
	
	public CorporateProfile getByCorporateId(Long corporateId);
	
	public boolean checkAvailableBRNo(String brNo);
	
	public ListPage<CorporateProfile> getCorpoateProfileListOld(ListPage<CorporateProfile> page, CorporateProfileDto dto,String status);
	
	public boolean checkAvailableBrNo(String brNo, Long corporateId);
	
	public ListPage<CorporateProfile> getMemberList(ListPage<CorporateProfile> page,String byAccount,String status,Long filterByCustomerId);
	
	public List<CorporateProfile> getCorporateAccountDropList();
	
	public boolean checkAvailableCompanyName(String companyName);
	
	public boolean checkAvailableCompanyName(String companyName, Long corporateId);
	
	public ListPage<CorporateProfile> getCorpoateProfileList(ListPage<CorporateProfile> page);
	
	public  CorporateProfileDto getCorporateContactByCorporateIdAndContactType(Long corporateId,String contactType);

	public ListPage<CorporateProfile> getMemberList(ListPage<CorporateProfile> page,String sortBy,String isAscending,String byAccount,String status,Long filterByCustomerId);
	
}
