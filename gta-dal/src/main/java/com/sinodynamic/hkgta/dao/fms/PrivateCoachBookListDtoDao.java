package com.sinodynamic.hkgta.dao.fms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;


public interface PrivateCoachBookListDtoDao extends IBaseDao<PrivateCoachBookListDto> {

	String getPrivateCoachBookList(String status, Integer show,String facilityType, String customerId);
}
