package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRuleLog;

public interface MemberLimitRuleLogDao extends IBaseDao<MemberLimitRuleLog> {


}
