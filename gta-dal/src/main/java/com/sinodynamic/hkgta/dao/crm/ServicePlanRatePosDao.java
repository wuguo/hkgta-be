package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRatePos;

public interface ServicePlanRatePosDao extends IBaseDao<ServicePlanRatePos>{

	public int deleteServicePlanRatePosByServPosId(Long servPosId) ;
	
	
}
