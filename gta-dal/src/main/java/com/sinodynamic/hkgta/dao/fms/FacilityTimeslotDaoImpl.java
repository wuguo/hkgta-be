package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateFormatUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.AvailableTimelotDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemStatusDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotQueryDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotResDto;
import com.sinodynamic.hkgta.dto.fms.IntervalParam;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;

@Repository
public class FacilityTimeslotDaoImpl extends GenericDao<FacilityTimeslot> implements FacilityTimeslotDao {

	@Override
	public List<FacilityTimeslot> getFacilityTimeslotList(String typeCode, Date beginDatetime, Integer venueFloor) {

		String hql = venueFloor == null ? " SELECT f FROM FacilityTimeslot f where upper(f.facilityMaster.facilityTypeBean.typeCode) = ? and date(f.beginDatetime) = ? "
				: " SELECT f FROM FacilityTimeslot f where upper(f.facilityMaster.facilityTypeBean.typeCode) = ? and date(f.beginDatetime) = ? and f.facilityMaster.venueFloor=?";
		hql += " and f.status <> ? ";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(typeCode.toUpperCase());
		paramList.add(beginDatetime);
		if (venueFloor != null) {
			paramList.add(venueFloor.longValue());
		}
		paramList.add(FacilityStatus.CAN.name());
		return super.getByHql(hql, paramList);
	}

	@Override
	public FacilityItemStatusDto getFacilityStatus(FacilityItemStatusDto facilityItemStatusDto) {
		String hql = "SELECT f FROM FacilityTimeslot f where f.facilityMaster.facilityNo = ? and f.beginDatetime = ? and f.endDatetime = ?";
		hql += " and f.status <> ? ";
		List<Serializable> paramList = new ArrayList<>();
		paramList.add(facilityItemStatusDto.getFacilityNo());
		paramList.add(facilityItemStatusDto.getBeginDatetime());
		paramList.add(facilityItemStatusDto.getEndDatetime());
		paramList.add(FacilityStatus.CAN.name());
		List<FacilityTimeslot> facilityTimeslotList = getByHql(hql, paramList);
		FacilityItemStatusDto facilityItemStatusDtoReturn = null;
		if (facilityTimeslotList.size() > 0) {
			facilityItemStatusDtoReturn = new FacilityItemStatusDto();
			facilityItemStatusDtoReturn.setFacilityTimeslotId(facilityTimeslotList.get(0).getFacilityTimeslotId());
			facilityItemStatusDtoReturn.setFacilityNo(facilityTimeslotList.get(0).getFacilityMaster().getFacilityNo());
			facilityItemStatusDtoReturn.setStatus(facilityTimeslotList.get(0).getStatus());
			facilityItemStatusDtoReturn.setRemark(facilityTimeslotList.get(0).getInternalRemark());
			facilityItemStatusDtoReturn.setBeginDatetime(facilityTimeslotList.get(0).getBeginDatetime());
			facilityItemStatusDtoReturn.setEndDatetime(facilityTimeslotList.get(0).getEndDatetime());
			facilityItemStatusDtoReturn.setStatus(facilityTimeslotList.get(0).getStatus());
			facilityItemStatusDtoReturn.setFacilityTimeslot(facilityTimeslotList.get(0));
			facilityItemStatusDtoReturn.setCreateDate(facilityTimeslotList.get(0).getCreateDate());
			facilityItemStatusDtoReturn.setReserveType(facilityTimeslotList.get(0).getReserveType());
		}
		return facilityItemStatusDtoReturn;
	}

	public boolean updateFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException {
		return super.update(facilityTimeslot);
	}

	@Override
	public Serializable addFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException {

		return super.save(facilityTimeslot);
	}

	@Override
	public FacilityTimeslot getFacilityTimeslot(Long facility_no,Date beginDatetime,
			Date endDatetime) {

		List<Serializable> paramList = new ArrayList<Serializable>();
		StringBuffer hql = new StringBuffer("select f FROM FacilityTimeslot f where f.beginDatetime = ?  and f.facilityMaster.facilityNo =? ");
		hql.append(" and f.status <> ? ");
		if(null != endDatetime){
			hql.append(" and f.endDatetime = ? ");
		}
		paramList.add(beginDatetime);
		paramList.add(facility_no);
		paramList.add(FacilityStatus.CAN.name());
		if(null != endDatetime){
			paramList.add(endDatetime);
		}
		List<FacilityTimeslot> timeslot = super.getByHql(hql.toString(), paramList);
		if (null != timeslot && timeslot.size() > 0)
			return timeslot.get(0);
		return null;
	}

	@Override
	public FacilityTimeslot getFacilityTimeslotById(long facilityTimeslotId) {
		String hql = " SELECT f FROM FacilityTimeslot f where f.facilityTimeslotId = ? ";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityTimeslotId);
		List<FacilityTimeslot> timeslot = super.getByHql(hql, paramList);
		if (timeslot.size()>0){
			return timeslot.get(0); 
		}
		return null;
	}

	@Override
	public boolean deleteFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException {
		return super.delete(facilityTimeslot);
	}

	@Override
	public List<FacilityTimeslot> getFacilityTimeslotListByRange(String typeCode, Date beginDatetime, Date endDatetime, Integer venueFloor)
	{

		String hql = venueFloor == null ? " SELECT f FROM FacilityTimeslot f where upper(f.facilityMaster.facilityTypeBean.typeCode) = ? and f.beginDatetime >= ? and f.endDatetime <= ?"
				: " SELECT f FROM FacilityTimeslot f where upper(f.facilityMaster.facilityTypeBean.typeCode) = ? and f.beginDatetime >= ? and f.endDatetime <= ? and f.facilityMaster.venueFloor=?";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(typeCode.toUpperCase());
		paramList.add(beginDatetime);
		paramList.add(endDatetime);
		if (venueFloor != null) {
			paramList.add(venueFloor);
		}
		return super.getByHql(hql, paramList);
	}

	@Override
	public List<FacilityTimeslotQueryDto> getFacilityTimeslotGroupTimeCountFacilityAttribute(String facilityType, Date[] times, String facilityAttribute) throws HibernateException
	{
		String hqlStaffReserved = "";
		String hqlMemberReserved = "";
		List<Serializable> paramListStaff = new ArrayList<Serializable>();
		List<Serializable> paramListMember = new ArrayList<Serializable>();
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			hqlStaffReserved = buildStaffGolfReservedSQL(facilityType, times, facilityAttribute, paramListStaff,null,null);
			hqlMemberReserved = buildMemberGolfReservedSQL(facilityType, times, facilityAttribute, paramListMember);
		}else{
			hqlStaffReserved = buildStaffTennisReservedSQL(facilityType, times, facilityAttribute, paramListStaff);
			hqlMemberReserved = buildMemberTennisReservedSQL(facilityType, times, facilityAttribute, paramListMember);
		}
		List<FacilityTimeslotQueryDto> dtoStaff = super.getDtoBySql(hqlStaffReserved, paramListStaff, FacilityTimeslotQueryDto.class);
		List<FacilityTimeslotQueryDto> dtoMember = super.getDtoBySql(hqlMemberReserved, paramListMember, FacilityTimeslotQueryDto.class);
		List<FacilityTimeslotQueryDto> dtoReturn = setFacilityTimeslotQueryDto(times, dtoStaff, dtoMember);
		return dtoReturn;
	}

	private String buildMemberGolfReservedSQL(String facilityType, Date[] times, String facilityAttribute, List<Serializable> paramListMember)
	{
		StringBuffer hqlMemberReserved = new StringBuffer();
		hqlMemberReserved.append("select count(*) as facilityCount,a.begin_datetime as beginDatetime ");
		hqlMemberReserved.append(" from facility_timeslot a, member_reserved_facility b, member_facility_type_booking c, member_facility_book_addition_attr d ");
		hqlMemberReserved.append(" where a.facility_timeslot_id = b.facility_timeslot_id ");
		hqlMemberReserved.append(" and a.facility_no is null and b.resv_id = c.resv_id ");
		hqlMemberReserved.append(" and c.resv_id = d.resv_id ");
		hqlMemberReserved.append(" and c.resv_facility_type = d.facility_type ");
		hqlMemberReserved.append(" and upper(c.status) <> 'CAN' and upper(c.status) <> 'NAT' ");
		hqlMemberReserved.append(" and a.status <> ? ");
		paramListMember.add(FacilityStatus.CAN.name());
		hqlMemberReserved.append(" and a.begin_datetime in ( ");
		for (int i = 0; i < times.length; i++)
		{
			hqlMemberReserved.append("?,");
			paramListMember.add(times[i]);
		}
		String sql = hqlMemberReserved.substring(0, hqlMemberReserved.length() - 1);
		sql += " ) and upper(c.resv_facility_type) = ? ";
		sql += " and d.attribute_id = ? ";
		sql += " group by a.begin_datetime ";
		paramListMember.add(facilityType.toUpperCase());
		paramListMember.add(facilityAttribute);
		return sql;
	}
	
	private String buildMemberTennisReservedSQL(String facilityType, Date[] times, String facilityAttribute, List<Serializable> paramListMember)
	{
		StringBuffer hqlMemberReserved = new StringBuffer();
		hqlMemberReserved.append("select count(*) as facilityCount,a.begin_datetime as beginDatetime ");
		hqlMemberReserved.append(" from facility_timeslot a, member_reserved_facility b, member_facility_type_booking c ");
		hqlMemberReserved.append(" where a.facility_timeslot_id = b.facility_timeslot_id ");
		hqlMemberReserved.append(" and a.facility_no is null and b.resv_id = c.resv_id ");
		hqlMemberReserved.append(" and upper(c.status) <> 'CAN' and upper(c.status) <> 'NAT' ");
		hqlMemberReserved.append(" and a.status <> ? ");
		paramListMember.add(FacilityStatus.CAN.name());
		hqlMemberReserved.append(" and a.begin_datetime in ( ");
		for (int i = 0; i < times.length; i++)
		{
			hqlMemberReserved.append("?,");
			paramListMember.add(times[i]);
		}
		String sql = hqlMemberReserved.substring(0, hqlMemberReserved.length() - 1);
		sql += " ) and upper(c.resv_facility_type) = ? ";
		sql += " and c.facility_subtype_id = ? ";
		sql += " group by a.begin_datetime ";
		paramListMember.add(facilityType.toUpperCase());
		paramListMember.add(facilityAttribute);
		return sql;
	}

	private String buildStaffTennisReservedSQL(String facilityType, Date[] times, String facilityAttribute, List<Serializable> paramListStaff)
	{
		StringBuffer hqlStaffReserved = new StringBuffer();
		hqlStaffReserved.append("select count(*) as facilityCount,a.begin_datetime as beginDatetime ");
		hqlStaffReserved.append(" from facility_timeslot a, facility_master b");
		hqlStaffReserved.append(" where a.facility_no = b.facility_no ");
		hqlStaffReserved.append(" and a.status <> ? ");
		paramListStaff.add(FacilityStatus.CAN.name());
		hqlStaffReserved.append(" and a.begin_datetime in ( ");
		for (int i = 0; i < times.length; i++)
		{
			hqlStaffReserved.append("?,");
			paramListStaff.add(times[i]);
		}
		String sql = hqlStaffReserved.substring(0, hqlStaffReserved.length() - 1);
		sql += " ) and upper(b.facility_type) = ? ";
		sql += " and b.facility_subtype_id = ? ";
		paramListStaff.add(facilityType.toUpperCase());
		paramListStaff.add(facilityAttribute);
		
		sql += " group by a.begin_datetime ";
		return sql;
	}

	private String buildStaffGolfReservedSQL(String facilityType, Date[] times, String facilityAttribute, List<Serializable> paramListStaff, List<Long> floors,List<Long> facilityNos)
	{
		StringBuffer hqlStaffReserved = new StringBuffer();
		hqlStaffReserved.append("select count(*) as facilityCount,a.begin_datetime as beginDatetime ");
		hqlStaffReserved.append(" from facility_timeslot a, facility_master b, facility_addition_attribute c, facility_attribute_caption d ");
		hqlStaffReserved.append(" where a.facility_no = b.facility_no and b.facility_no = c.facility_no ");
		hqlStaffReserved.append(" and a.status <> ? ");
		hqlStaffReserved.append(" and c.attribute_id = d.attribute_id " + " and a.begin_datetime in ( ");
		paramListStaff.add(FacilityStatus.CAN.name());
		for (int i = 0; i < times.length; i++)
		{
			hqlStaffReserved.append("?,");
			paramListStaff.add(times[i]);
		}
		String sql = hqlStaffReserved.substring(0, hqlStaffReserved.length() - 1);
		sql += " ) and upper(b.facility_type) = ? ";
		sql += " and c.attribute_id = ? ";
		paramListStaff.add(facilityType.toUpperCase());
		paramListStaff.add(facilityAttribute);
		
		if(null != floors && floors.size() > 0){
			sql += " and b.venue_floor in ( ";
			for(Long floor : floors){
				sql += " ?,";
				paramListStaff.add(floor.longValue());
			}
			sql = sql.substring(0,sql.length()-1);
			sql += " )";
		}
		if(null != facilityNos && facilityNos.size() > 0){
			sql += " and a.facility_no in ( ";
			for (int i = 0; i < facilityNos.size(); i++)
			{
				sql += "?,";
				paramListStaff.add(facilityNos.get(i).intValue());
			}
			sql = sql.substring(0,sql.length()-1);
			sql += " )";
		}
		sql += " group by a.begin_datetime ";
		
		return sql;
	}

	@Override
	public boolean isFacilityAvailable(String facilityNo, Date beginDatetime, Date endDatetime) {
	    
		String beginTime = DateConvertUtil.date2String(beginDatetime, "yyyy-MM-dd HH:mm:ss");
		String endTime = DateConvertUtil.date2String(endDatetime, "yyyy-MM-dd HH:mm:ss");
		StringBuilder sb = new StringBuilder("From FacilityTimeslot where beginDatetime >= '");
		sb.append(beginTime)
		.append("' and endDatetime <= '")
		.append(endTime)
		.append("' and facilityMaster.facilityNo = ")
		.append(facilityNo)
		.append(" and status <> 'CAN' ");
		String hql = sb.toString();
		List<FacilityTimeslot> list = getByHql(hql);
		
		return list == null || list.size() == 0 ? true : false;
	}

	@Override
	public boolean deleteFacilityTimeslotById(Long facilityTimeslotId) {
	    
	    return deleteById(FacilityTimeslot.class, facilityTimeslotId);
	}

	@Override
	public Boolean deleteFacilityTimeslot(Long facilityTimeslotId)
			throws HibernateException {
		String hql = "delete from facility_timeslot where facility_timeslot_id = ?";
		return deleteByHql(hql, facilityTimeslotId) >= 0 ? true : false;
	}

	@Override
	public List<FacilityTimeslot> getFacilityTimeslotByBeginDate(Long facilityNo, Date beginDatetime,
			Date endDatetime) {
		StringBuffer hql = new StringBuffer("select f FROM FacilityTimeslot f where f.beginDatetime >= ? and f.facilityMaster.facilityNo =? ");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(beginDatetime);
		paramList.add(facilityNo);
		
		if (null != endDatetime) {
			hql.append(" and f.beginDatetime <= ? ");
			paramList.add(endDatetime);
		}
		hql.append(" and f.status <> ? ");
		paramList.add(FacilityStatus.CAN.name());
		return super.getByHql(hql.toString(), paramList);
	}
	
	@Override
	public List<AvailableTimelotDto> getFacilityTimeslotDtoList(String facilityType,Date date, String bayType) {
		String sql = "SELECT\n" +
				"    concat(s.facility_timeslot_id,'') AS timeslotId,\n" +
				"    DATE_FORMAT(s.begin_datetime, '%H') AS timeslot\n" +
				"FROM\n" +
				"    facility_timeslot s,\n" +
				"    facility_master f\n" +
				"        INNER JOIN\n" +
				"    facility_addition_attribute a ON (f.facility_no = a.facility_no)\n" +
				"WHERE\n" +
				"    s.facility_no = f.facility_no\n" +
				"        AND a.attribute_id = ?\n" +
				"        AND f.facility_type = ?\n" +
				"        AND DATE_FORMAT(s.begin_datetime, '%Y-%m-%d') = ?\n"+
				" 	and s.status <> ? ";
		List<AvailableTimelotDto> list = this.getDtoBySql(sql, Arrays.asList(bayType,facilityType,DateFormatUtils.format(date, "yyyy-MM-dd"),FacilityStatus.CAN.name()), AvailableTimelotDto.class);
		return list;
	}

	@Override
	public List<AvailableTimelotDto> getFacilityTimeslotDtoList(String coachId,
			String facilityType, Date date, String bayType) {
		String sql = "SELECT \n" +
				"    concat(s.staff_timeslot_id,'') AS timeslotId,\n" +
				"    DATE_FORMAT(s.begin_datetime, '%H') AS timeslot\n" +
				"FROM\n" +
				"    staff_timeslot s\n" +
				"WHERE\n" +
				"    s.staff_user_id = ?\n" +
				"        AND DATE_FORMAT(s.begin_datetime, '%Y-%m-%d') = ? \n" +
				"UNION SELECT \n" +
				"    s.facility_timeslot_id AS timeslot_id,\n" +
				"    DATE_FORMAT(s.begin_datetime, '%H') AS timeslot\n" +
				"FROM\n" +
				"    facility_timeslot s,\n" +
				"    facility_master f\n" +
				"        INNER JOIN\n" +
				"    facility_addition_attribute a ON (f.facility_no = a.facility_no)\n" +
				"WHERE\n" +
				"    s.facility_no = f.facility_no\n" +
				"        AND a.attribute_id = ?\n" +
				"        AND f.facility_type = ?\n" +
				"        AND DATE_FORMAT(s.begin_datetime, '%Y-%m-%d') = ?\n"+
				" 	and s.status <> ? ";
		return this.getDtoBySql(sql, Arrays.asList(coachId,date,bayType,facilityType,DateFormatUtils.format(date, "yyyy-MM-dd"),FacilityStatus.CAN.name()), AvailableTimelotDto.class);
	}

	@Override
	public List<FacilityTimeslotResDto> getFacilityTimeslotByResvIds(List<Long> resvIds)
	{
		StringBuilder sqlStr = new StringBuilder("select mr.resv_id as resvId,f.facility_timeslot_id as facilityTimeslotId,f.begin_datetime as beginDatetime,f.end_datetime as endDatetime from facility_timeslot f ");
		sqlStr.append(" join member_reserved_facility mr on f.facility_timeslot_id = mr.facility_timeslot_id where mr.resv_id in (  ");
		for (int i=0; i<resvIds.size() ; i++)
		{
			sqlStr.append(" ?,");
		}
		String sql = sqlStr.substring(0, sqlStr.length()-1)+" )";
		sql += " order by mr.resv_id ";
		return this.getDtoBySql(sql, resvIds, FacilityTimeslotResDto.class);
	}

	@Override
	public List<FacilityTimeslotQueryDto> getFacilityTimeslotGroupTimeCountFacilityAttribute(String facilityType, Date[] times, String facilityAttribute,List<Long> floors,List<Long> facilityNos) throws HibernateException
	{
		String hqlStaffReserved = "";
		String hqlMemberReserved = "";
		List<Serializable> paramListStaff = new ArrayList<Serializable>();
		List<Serializable> paramListMember = new ArrayList<Serializable>();
		if(Constant.FACILITY_TYPE_GOLF.equals(facilityType.toUpperCase())){
			hqlStaffReserved = buildStaffGolfReservedSQL(facilityType, times, facilityAttribute, paramListStaff,floors,facilityNos);
			hqlMemberReserved = buildMemberGolfReservedSQL(facilityType, times, facilityAttribute, paramListMember);
		}else{
			hqlStaffReserved = buildStaffTennisReservedSQL(facilityType, times, facilityAttribute, paramListStaff);
			hqlMemberReserved = buildMemberTennisReservedSQL(facilityType, times, facilityAttribute, paramListMember);
		}
		List<FacilityTimeslotQueryDto> dtoStaff = super.getDtoBySql(hqlStaffReserved, paramListStaff, FacilityTimeslotQueryDto.class);
		List<FacilityTimeslotQueryDto> dtoMember = super.getDtoBySql(hqlMemberReserved, paramListMember, FacilityTimeslotQueryDto.class);
		List<FacilityTimeslotQueryDto> dtoReturn = setFacilityTimeslotQueryDto(times, dtoStaff, dtoMember);
		return dtoReturn;
	}

	private List<FacilityTimeslotQueryDto> setFacilityTimeslotQueryDto(Date[] times, List<FacilityTimeslotQueryDto> dtoStaff, List<FacilityTimeslotQueryDto> dtoMember)
	{
		List<FacilityTimeslotQueryDto> dtoReturn = new ArrayList<FacilityTimeslotQueryDto>();
		for(Date time: times){
			FacilityTimeslotQueryDto facilityTimeslotQueryDto = new FacilityTimeslotQueryDto();
			facilityTimeslotQueryDto.setBeginDatetime(time);
			facilityTimeslotQueryDto.setFacilityCount(0L);
			for(FacilityTimeslotQueryDto facilityTimeslotQueryDtoStaff : dtoStaff){
				if (facilityTimeslotQueryDtoStaff.getBeginDatetime().compareTo(time) == 0){
					facilityTimeslotQueryDto.setFacilityCount(facilityTimeslotQueryDtoStaff.getFacilityCount());
					break;
				}
			}
			for(FacilityTimeslotQueryDto facilityTimeslotQueryDtoMember : dtoMember){
				if (facilityTimeslotQueryDtoMember.getBeginDatetime().compareTo(time) == 0){
					Long count = facilityTimeslotQueryDtoMember.getFacilityCount();
					Long fcount = facilityTimeslotQueryDto.getFacilityCount();
					facilityTimeslotQueryDto.setFacilityCount(count + fcount);
					break;
				}
			}
			dtoReturn.add(facilityTimeslotQueryDto);
		}
		return dtoReturn;
	}
	
	@Override
	public List<FacilityTimeslot> getFacilityTimeslotListByStatus(String typeCode,String status, Date beginDatetime, Date endDatetime)
	{

		String hql =  " SELECT f FROM FacilityTimeslot f where upper(f.facilityMaster.facilityTypeBean.typeCode) = ? and upper(f.status) = ? and DATE_FORMAT(f.beginDatetime,'%Y-%m-%d %H:%i:%s') >= ? and DATE_FORMAT(f.endDatetime,'%Y-%m-%d %H:%i:%s')<= ?";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(typeCode);
		paramList.add(status);
		paramList.add(DateConvertUtil.parseDate2String(beginDatetime,"yyyy-MM-dd HH:mm:ss"));
		paramList.add(DateConvertUtil.parseDate2String(endDatetime,"yyyy-MM-dd HH:mm:ss"));

		return super.getByHql(hql, paramList);
	}
	
}
