package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateTime;

@Repository
public class FacilityUtilizationRateTimeDaoImpl extends GenericDao<FacilityUtilizationRateTime>  implements FacilityUtilizationRateTimeDao{

	@Override
	public List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeList(String facilityType)
	{
		StringBuffer hql = new StringBuffer(
				"from FacilityUtilizationRateTime t where t.weekDay is not null and  upper(t.facilityType) = ? order by t.weekDay asc ");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		
		return super.getByHql(hql.toString(),paramList);
	}

	@Override
	public int removeFacilityUtilizationRateTime(String facilityType,String subType,String weekDay)
	{
		StringBuffer hql = new StringBuffer(
				"delete from facility_utilization_rate_time where upper(facility_type) = ? and facility_date_id is null ");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		if(!StringUtils.isEmpty(subType)){
			hql.append(" and facility_subtype_id=? ");
			paramList.add(subType);
		}
		hql.append(" and week_day = ? ");
		paramList.add(weekDay);
		return super.deleteByHql(hql.toString(), paramList.toArray());
	}

	@Override
	public List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeList(String facilityType, String weekDay, String subType)
	{
		StringBuffer hql = new StringBuffer(
				"from FacilityUtilizationRateTime t where  upper(t.facilityType) = ? and t.weekDay =? ");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(weekDay);
		if(!StringUtils.isEmpty(subType)){
			hql.append(" and t.facilitySubtypeId=? ");
			paramList.add(subType);
		}
		return super.getByHql(hql.toString(),paramList);
	}

	@Override
	public FacilityUtilizationRateTime getFacilityUtilizationRateTime(String facilityType, String weekDay, int beginTime,String subType)
	{
		StringBuffer hql = new StringBuffer("from FacilityUtilizationRateTime t where  upper(t.facilityType) = ? and t.weekDay =? and t.beginTime = ? ");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(weekDay);
		paramList.add(beginTime);
		if(!StringUtils.isEmpty(subType)){
			hql.append(" and facilitySubtypeId=? ");
			paramList.add(subType);
		}
		List<FacilityUtilizationRateTime> rateTimes = super.getByHql(hql.toString(), paramList);
		return (null != rateTimes && rateTimes.size() > 0) ? rateTimes.get(0) : null;
	}

	@Override
	public List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimeListBySubType(String facilityType, String subType)
	{
		StringBuffer hql = new StringBuffer(
				"from FacilityUtilizationRateTime t where t.weekDay is not null and  upper(t.facilityType) = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		
		if(!StringUtils.isEmpty(subType)){
			hql.append(" and t.facilitySubtypeId=? ");
			paramList.add(subType);
		}
		hql.append(" order by t.weekDay asc ");
		return super.getByHql(hql.toString(),paramList);
	}

	@Override
	public int removeFacilityUtilizationRateTime(String facilityType, String weekDay)
	{
		StringBuffer hql = new StringBuffer(
				"delete from facility_utilization_rate_time where upper(facility_type) = ? and week_day = ? and facility_date_id is null ");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(weekDay);
		return super.deleteByHql(hql.toString(), paramList.toArray());
	}

}
