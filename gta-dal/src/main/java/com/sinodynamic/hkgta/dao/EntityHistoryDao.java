package com.sinodynamic.hkgta.dao;

import com.sinodynamic.hkgta.dto.EntityHistoryDto;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;

public interface EntityHistoryDao extends IBaseDao {

    EntityHistoryDto getEntityHistory(String entityName, String[] names, String[] values);
}
