package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollmentLog;
@Repository
public class CustomerEnrollmentLogDaoImpl extends GenericDao<CustomerEnrollmentLog> implements CustomerEnrollmentLogDao {

	public CustomerEnrollmentLog getLatestEnrollLogByEnrollId(Long enrollId){
		
		String sql = "SELECT ceLog.status_from as statusFrom, ceLog.status_to as statusTo, ceLog.enroll_id as enrollId"+
				" FROM customer_enrollment_log ceLog" +
				"WHERE " +
				"ceLog.enroll_id = ? " +
				"AND ceLog.status_update_date = ( " +
				"	SELECT" +
				"		max(log.status_update_date) " +
				"	FROM " +
				"		customer_enrollment_log log " +
				"	WHERE " +
				"		log.enroll_id = ?) ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(enrollId);
		param.add(enrollId);
		List<CustomerEnrollmentLog> result = getDtoBySql(sql, param, CustomerEnrollmentLog.class);		
		return result.size()>0?result.get(0):null;
	}
	
}
