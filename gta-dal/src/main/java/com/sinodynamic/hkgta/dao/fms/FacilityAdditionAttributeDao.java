package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilityAdditionAttribute;

public interface FacilityAdditionAttributeDao extends IBaseDao<FacilityAdditionAttribute>{
	
	FacilityAdditionAttribute getFacilityAdditionAttribute(Long facilityNo,String attributeId);
	
	FacilityAdditionAttribute getFacilityAdditionAttribute(Long facilityNo);
	
	public List<FacilityAdditionAttribute> getFacilityAdditionAttributeList(String attributeId);
}
