package com.sinodynamic.hkgta.dao.qst;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.qst.Survey;

public interface SurveyDao extends IBaseDao<Survey> {


	/**
	 * 获取问卷设置
	 * @return
	 */
	public List<Survey> getQuestionnaireSetting();

	
	public List<Survey>getAll();

}
