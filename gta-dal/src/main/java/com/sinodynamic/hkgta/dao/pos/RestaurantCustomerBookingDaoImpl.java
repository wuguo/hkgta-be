package com.sinodynamic.hkgta.dao.pos;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantCustomerBooking;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.RestaurantCustomerBookingStatus;

@Repository
public class RestaurantCustomerBookingDaoImpl extends GenericDao<RestaurantCustomerBooking> implements RestaurantCustomerBookingDao
{

	@Override
	public List<RestaurantCustomerBooking> getRestaurantCustomerBookingListByRestaurantIdAndCustomerId(String restaurantId, Long customerId)
	{
		String hql = null;
		List<Serializable> params = new ArrayList<Serializable>(); 
		if (!StringUtils.isEmpty(restaurantId)) {
			if (customerId != null && customerId > 0) {
				hql = "select rcb from RestaurantCustomerBooking rcb where rcb.restaurantMaster.restaurantId = ? and rcb.customerProfile.customerId = ?";
				params.add(restaurantId);
				params.add(customerId);
			}
			else
			{
				hql = "select rcb from RestaurantCustomerBooking rcb where rcb.restaurantMaster.restaurantId = ?";
				params.add(restaurantId);
			}
			
		}else{
			hql = "from RestaurantCustomerBooking rcb";
		}
		return super.getByHql(hql,params);
	}

	@Override
	public int updateRestaurantCustomerBookingExpired() throws Exception
	{
		String hql = "update RestaurantCustomerBooking set status = '"+RestaurantCustomerBookingStatus.EXP.name()+"',updateDate = ? where bookTime < ? and status = '"+RestaurantCustomerBookingStatus.CFM.name()+"'";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(new Date());
		params.add(new Date());
		return super.hqlUpdate(hql, params);
	}

	@Override
	public List<RestaurantCustomerBooking> getRestaurantCustomerBookingConfirmedList()
	{
		String hql = " from RestaurantCustomerBooking where status = ? ";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(Constant.Status.CFM.toString());
		return super.getByHql(hql, params);
	}

	@Override
	public int getRestaurantCustomerBookingTotalPartySize(String restaurantId,Date bookingDate,Integer startTime,Integer endTime) throws Exception
	{
		List<Serializable> params = new ArrayList<Serializable>();
		String sql = " select IFNULL(SUM(party_size),0) from restaurant_customer_booking where restaurant_id = ? and status = ? and date(book_time)= date(?)  ";
		sql +=" and ( ( concat(substring(book_time,11,3) ,substring(book_time,15,2)) >= ? and concat(substring(book_time,11,3) ,substring(book_time,15,2)) < ? ) " ;
		params.add(restaurantId);
		params.add(Constant.Status.CFM.toString());
		params.add(DateCalcUtil.formatDate(bookingDate));
		params.add(startTime);
		params.add((startTime>endTime)?(endTime+2400):endTime);
		if(startTime > endTime){
			sql +=" or ( concat(substring(book_time,11,3) ,substring(book_time,15,2)) >= ? and concat(substring(book_time,11,3) ,substring(book_time,15,2)) < ?  )" ;
			params.add((endTime-startTime));
			params.add(endTime);
		}
		sql +=" )" ;
		
		
		return ((Number)super.getUniqueBySQL(sql, params)).intValue();
	}
}
