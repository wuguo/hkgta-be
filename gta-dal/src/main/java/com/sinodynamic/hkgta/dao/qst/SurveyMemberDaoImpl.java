package com.sinodynamic.hkgta.dao.qst;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.qst.SurveyQuestionnaireStatisticsDataDto;
import com.sinodynamic.hkgta.entity.qst.SurveyMember;

@Repository
public class SurveyMemberDaoImpl extends GenericDao<SurveyMember>implements SurveyMemberDao {

	/**
	 * 获取符合条件的问卷调查统计数据
	 * @param customerId
	 * @return
	 */
	@Override
	public  List<SurveyQuestionnaireStatisticsDataDto>  getMeetConditionsSurveyQuestionnaireStatisticsData(Long customerId){
		String sql = " SELECT sm.sys_id AS sysId, sm.survey_id AS surveyId,  s.forward_url AS url, s.buffer_day AS bufferDay, " +
					" ( SELECT smy.survey_sent_date FROM survey_member AS smy WHERE smy.customer_id = sm.customer_id " + 
					" AND smy.survey_id = sm.survey_id AND smy.survey_sent_date IS NOT NULL ORDER BY smy.survey_sent_date DESC LIMIT 1) AS sendTime" +
					" FROM survey_member AS sm " +
					" LEFT JOIN survey AS s ON sm.survey_id = s.survey_id " +
					" WHERE s.`status` = 'ACT' AND sm.survey_sent_date IS NULL AND sm.customer_id = ? " +
					" AND sm.utilize_count >= s.utilize_rate_hit ";
		List<Serializable>param=new ArrayList<>();
		param.add(customerId);
		return this.getDtoBySql(sql, param, SurveyQuestionnaireStatisticsDataDto.class);
		
	}
	
	public List<SurveyMember> getPageDatalist(int pageNum,int pageSize, String queryListHQL, List<Serializable>  parameters) {
		return super.getPageDatalist(pageNum, pageSize, queryListHQL, parameters);
	}
}
