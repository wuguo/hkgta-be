package com.sinodynamic.hkgta.dao.adm;

/*
 * @Author Becky
 * @Date 4-29
*/
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;

public interface PermitCardMasterDao extends IBaseDao<PermitCardMaster>{

	
	public List<PermitCardMaster> getPermitCardMasterByCustomerId(Long customerId);
	
	
	/**
	 * @author Junfeng_Yan
	 * @param cardNo
	 * @return
	 * @throws HibernateException
	 */
	public PermitCardMaster getPermitCardMasterByCardNo(String cardNo) throws HibernateException;
	
	
	
	/**
	 * @author Junfeng_Yan
	 * @param permitCardMaster
	 * @return
	 * @throws HibernateException
	 */
	public boolean updatePermitCardMaster(PermitCardMaster permitCardMaster) throws HibernateException;
	
	
	
	/**
	 * @param cardNo
	 * @param customerId
	 * @return
	 * @throws HibernateException
	 */
	public boolean linkCardForMember(PermitCardMaster permitCardMaster) throws HibernateException;
	


	/**
	 * @author Vineela_Jyothi
	 * @param page
	 * @param dto
	 * @return
	 */
	public String getCards(String status);
	
	/**
	 * @Author Mianping_Wu
	 * @Date Sep 4, 2015
	 * @Param @param customerId
	 * @Param @return
	 * @return List<PermitCardMaster>
	 */
	public List<PermitCardMaster> getIssuedCardByCustomerId(Long customerId);


	public PermitCardMaster getLatestCustomerOrderPermitCardByCustermerId(
			Long customerId);
}
