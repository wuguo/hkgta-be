package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;

@Repository
public class MemberFacilityBookAdditionAttrDaoImpl extends GenericDao<MemberFacilityBookAdditionAttr>  implements MemberFacilityBookAdditionAttrDao{

	@Override
	public List<MemberFacilityBookAdditionAttr> getMemberFacilityBookAdditionAttrList(long resvId)
	{
		StringBuffer hql = new StringBuffer(
				"from MemberFacilityBookAdditionAttr as fm where fm.id.resvId= ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(resvId);
		return super.getByHql(hql.toString(), paramList);
	}
	
	@Override
	public int deleteByReservationId(long resvId) {
		String hql = "delete from MemberFacilityBookAdditionAttr where id.resvId= ?";
		Session session = getCurrentSession();
		Query query = session.createQuery(hql);
		query.setParameter(0, resvId);
		return query.executeUpdate();
	}

	@Override
	public MemberFacilityBookAdditionAttr getMemberFacilityBookAdditionAttr(Long resvId, String facilityType)
	{
		StringBuffer hql = new StringBuffer(
				"from MemberFacilityBookAdditionAttr as fm where fm.id.resvId= ?  and fm.facilityType=? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(resvId);
		paramList.add(facilityType);
		return (MemberFacilityBookAdditionAttr) super.getUniqueByHql(hql.toString(), paramList);
	}

}
