package com.sinodynamic.hkgta.dao.adm;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.DepartmentStaff;

/**
 * search for DepartmentStaffDao 
 * @author Vian Tang
 * 
 * @since June 29 2015
 *
 */
public interface DepartmentStaffDao extends IBaseDao<DepartmentStaff> {
	
	void saveDepartment(DepartmentStaff department) throws HibernateException;
	
	void updateDepartment(DepartmentStaff department) throws HibernateException;
	
	DepartmentStaff getDepart(String userId) throws HibernateException;
	
}
