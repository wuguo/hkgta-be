package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.RoomFacilityTypeBooking;

@Repository
public class RoomFacilityTypeBookingDaoImpl extends GenericDao<RoomFacilityTypeBooking> implements RoomFacilityTypeBookingDao {

	@Override
	public List<RoomFacilityTypeBooking> getBookingByFacilityTypeBookingId(Long facilityTypeBookingId) {
		String hql = "from RoomFacilityTypeBooking where facilityTypeBookingId = ? ";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(facilityTypeBookingId);
		return super.getByHql(hql, params);
	}

}
