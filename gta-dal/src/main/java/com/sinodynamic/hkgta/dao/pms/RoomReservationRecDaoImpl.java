package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.fms.AdvancedQueryConditionsDto;
import com.sinodynamic.hkgta.dto.fms.BayTypeDto;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.pms.RoomType;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;
import com.sinodynamic.hkgta.util.constant.Constant;

@Repository("guestroomAdvanceSearch")
public class RoomReservationRecDaoImpl extends GenericDao<RoomReservationRec> implements RoomReservationRecDao , AdvanceQueryConditionDao{
	@Autowired
	private RoomTypeDao roomTypeDao;
	@Override
	public List<RoomReservationRec> getAllFailedReservations(int timeout) {
		Date now = new Date();
		List<RoomReservationRec> bookings = new ArrayList<RoomReservationRec>();
		String hql1 = 
				
				"SELECT rec FROM RoomReservationRec AS rec, CustomerOrderTrans AS txn " +
				"WHERE rec.orderNo = txn.customerOrderHd.orderNo AND " + 
				"txn.status IN ('FAIL' , 'VOID') AND " + 
				"rec.status IN ('RSV' , 'PAY') AND " + 
				"TIMESTAMPDIFF(MINUTE, rec.requestDate, ?) >= ?";
		String hql2 = 
				
				"FROM  RoomReservationRec rec " +
				"WHERE " +
				"rec.status IN ('RSV' , 'PAY') AND rec.orderNo IS NULL AND TIMESTAMPDIFF(MINUTE, rec.requestDate, ?) >= ? ";
		
		bookings.addAll(getByHql(hql1, Arrays.<Serializable>asList(now, timeout)));
		bookings.addAll(getByHql(hql2, Arrays.<Serializable>asList(now, timeout)));
		return bookings;
	}

	@Override
	public List<RoomReservationRec> getReservationByCustomerID(Long customerid) {
		Date currentDate = new Date();
		List<RoomReservationRec> bookings = new ArrayList<RoomReservationRec>();
		String hql = 		
				" SELECT rec FROM  RoomReservationRec rec " +
				" WHERE " +
				" rec.customerId = ? "+
				" AND rec.status = 'CKI' "+
				" AND date_format(?, '%Y-%m-%d') >= rec.arrivalDate "+
				" AND date_format(?, '%Y-%m-%d') <= rec.departureDate ";
		
		bookings.addAll(getByHql(hql, Arrays.<Serializable>asList(customerid,currentDate, currentDate)));
		return bookings;
	}
	
	@Override
	public RoomReservationRec getReservationByConfirmId(String confirmId) {
		RoomReservationRec book = getUniqueByCol(RoomReservationRec.class, "confirmId", confirmId);
		return book;
	}
	
	public String getGuestroomAdvanceSearch(Map<String, org.hibernate.type.Type> typeMap) {
		typeMap.put("reservationId", StringType.INSTANCE);
		typeMap.put("confirmId", StringType.INSTANCE);
		typeMap.put("academyID", StringType.INSTANCE);
		typeMap.put("patronName", StringType.INSTANCE);
		typeMap.put("arrivalDate", StringType.INSTANCE);
		typeMap.put("departureDate", StringType.INSTANCE);
		typeMap.put("roomTypeCode", StringType.INSTANCE);
		typeMap.put("roomId", StringType.INSTANCE);
		typeMap.put("rateCode", StringType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("totalCharge", LongType.INSTANCE);
		typeMap.put("roomTypeName", StringType.INSTANCE);
		typeMap.put("paymentMethod", StringType.INSTANCE);
		typeMap.put("roomNo", StringType.INSTANCE);
		
		StringBuilder sql = new StringBuilder("select *  from (select distinct ").
				append(" r.resv_id reservationId,  ")
				.append(" r.confirm_id  confirmId ,")
				.append("  m.academy_no  academyID, ")
				.append(" CONCAT( c.salutation, ' ', c.given_name, ' ', c.surname ) patronName ,")
				//.append("  date_format(r.arrival_date, '%Y-%m-%d')  arrivalDate , r.arrival_date  arrival_date,  ")
				.append("  date_format(r.arrival_date, '%Y-%m-%d')  arrivalDate , ")
				//.append("  date_format(r.departure_date, '%Y-%m-%d')  departureDate, r.departure_date  departure_date , ")
				.append("  date_format(r.departure_date, '%Y-%m-%d')  departureDate,  ")
				.append(" r.room_type_code roomTypeCode,")
				.append(" r.room_id roomId, ")
				.append("  r.rate_code rateCode, ")
				.append(" trans.payment_method_code  paymentMethod , ")
				.append("  (det.item_total_amout * det.order_qty) totalCharge, ")
				.append(" rt.type_name  roomTypeName, ")
				.append("  CASE WHEN  r.checkout_timestamp is not null  THEN 'CKO' WHEN (  r.checkout_timestamp is  null  and   r.checkin_timestamp is not null ) THEN 'CKI' ELSE  r.status END as status,  ").
				append(" m.customer_id customerId ,").
				append(" rm.room_no roomNo").
				append(" from room_reservation_rec r inner join member m on r.customer_id=m.customer_id ")
				.append(" inner join customer_profile c on m.customer_id=c.customer_id ")
				.append(" left join customer_order_trans trans on 	trans.order_no=r.order_no	")
				.append("  Left join customer_order_det det on r.confirm_id = det.ext_ref_no  ")
				.append("  Left join room rm  on r.room_id = rm.room_id  ")
				.append(" left join room_type rt on rt.type_code=r.room_type_code  ")
				.append("  where  r.status in ('SUC','RFU', 'CKI', 'CKO', 'CKP', 'CAN') ) t "); 
		return sql.toString();
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		AdvanceQueryConditionDto condition0 = new AdvanceQueryConditionDto("Reservation ID","reservationId","java.lang.Long","",1);
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Confirm.#","confirmId","java.lang.Long","",2);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Academeny ID","academyID","java.lang.String","",3);
		
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Partron Name","patronName","java.lang.String","",4);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Arrival Date","arrivalDate","java.util.Date","",5);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Departure Date","departureDate","java.util.Date","",6);
		List<RoomType> roomType = roomTypeDao.getByHql("FROM  RoomType ");
				
		Collection<SysCode> roomTypeCode = CollectionUtil.map(roomType, new CallBack<RoomType, SysCode> (){

			@Override
			public SysCode execute(RoomType r, int index) {
				SysCode sysCode = new SysCode();
				sysCode.setCodeValue(r.getTypeCode());
				sysCode.setCodeDisplay(r.getTypeName());
				return sysCode;
			}
			
		});
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Room Type","roomTypeCode","java.lang.String",new ArrayList<SysCode>(roomTypeCode),7);
		
		
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Room #","roomId","java.lang.Long","",8);
		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Rate Code","rateCode","java.lang.String","",9);
		final List<SysCode> status=new ArrayList<>();
		//SUS-success; IGN-ignore; CAN-cancelled (need refund)
		SysCode s1=new SysCode();
		s1.setCategory("status");
		s1.setCodeDisplay("Ready to Check-in");
		s1.setCodeValue("SUC");
		status.add(s1);
		
		SysCode s2=new SysCode();
		s2.setCategory("status");
		s2.setCodeDisplay("Checked-in");
		s2.setCodeValue("CKI");
		status.add(s2);
		
		SysCode s3=new SysCode();
		s3.setCategory("status");
		s3.setCodeDisplay("Cancelled");
		s3.setCodeValue("CAN");
		status.add(s3);
		
		SysCode s4=new SysCode();
		s4.setCategory("status");
		s4.setCodeDisplay("Checked-out");
		s4.setCodeValue("CKO");
		status.add(s4);
		
		SysCode s5=new SysCode();
		s5.setCategory("status");
		s5.setCodeDisplay("Refund");
		s5.setCodeValue("RFU");
		status.add(s5);
		
		SysCode s6 = new SysCode();
		s6.setCategory("status");
		s6.setCodeDisplay("Check - In in progress");
		s6.setCodeValue("CKP");
		status.add(s6);
		
		AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Status","status","java.lang.String",status,10);
		AdvanceQueryConditionDto condition10 = new AdvanceQueryConditionDto("Total Charge","totalCharge","java.lang.Long","",11);
		AdvanceQueryConditionDto condition11 = new AdvanceQueryConditionDto("Room Name","roomTypeName","java.lang.String","",12);
		final List<SysCode> statuss=new ArrayList<>();
		//SUS-success; IGN-ignore; CAN-cancelled (need refund)
		SysCode s11=new SysCode();
		s11.setCategory("checkinMethod");
		s11.setCodeDisplay("Silent Check In Accept");
		s11.setCodeValue("SA");
		statuss.add(s11);
		
		SysCode s12=new SysCode();
		s12.setCategory("checkinMethod");
		s12.setCodeDisplay("Silent Check In Reject");
		s12.setCodeValue("SR");
		statuss.add(s12);
		AdvanceQueryConditionDto condition12 = new AdvanceQueryConditionDto("Checkin Method","checkinMethod","java.lang.String",statuss,13);
		return  Arrays.asList(condition0,condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8,condition9,condition10,condition11, condition12);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		return null;
	}

	/**
	 * 根据resvId查询预定好的房间信息
	 * @param resvId
	 * @return RoomReservationRec
	 */
	@Override
	public RoomReservationRec getReservationByResvId(String resvId) {
		return getUniqueByCols(RoomReservationRec.class, new String[]{"resvId"}, new Serializable[]{Long.parseLong(resvId)});
	}
	
	/**
	 * 根据resvId查询预定好的房间信息
	 * @param resvId
	 * @return RoomReservationRec
	 */
	@Override
	public RoomReservationRec getReservationByResvId(long resvId) {
		return getUniqueByCols(RoomReservationRec.class, new String[]{"resvId"}, new Serializable[]{resvId});
	}
	
	/**
	 * 根据roomId查询今天是否预订了该房间
	 * @param customerId
	 * @param resvId
	 * @return
	 */
	@Override
	@Transactional
	public RoomReservationRec todayToSeeIfThereBookingRoom(Long customerId, Long resvId){
		Date now = new Date();
		String hql = " FROM RoomReservationRec rc WHERE date_format( ? ,'%Y-%m-%d') = date_format(rc.arrivalDate,'%Y-%m-%d') AND rc.status = 'SUC' AND rc.resvId = ? AND rc.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(now);
		param.add(resvId);
		param.add(customerId);
		List<RoomReservationRec> list = getByHql(hql, param);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
	
	@Override
	@Transactional
	public List<RoomReservationRec> getSilentCheckInProgress(){
		Date now = new Date();
		String hql = " FROM RoomReservationRec rc WHERE date_format( ? ,'%Y-%m-%d') = date_format(rc.arrivalDate,'%Y-%m-%d') AND rc.status = 'SUC' ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(now);
		List<RoomReservationRec> list = getByHql(hql, param);
		
		return list;
	}
	
	/**
	 * 获取当前今天CheckInProgress状态的总数
	 * @return
	 */
	@Override
	@Transactional
	public int getSilentCheckInProgressTotalNum(){
		Date now = new Date();
		String sql ="SELECT count(1) FROM room_reservation_rec rc WHERE date_format( :arrivalDate ,'%Y-%m-%d') = date_format(rc.arrival_date,'%Y-%m-%d') AND rc.status IN( '"
				+ Constant.RoomReservationStatus.CKP.name() +"', '"
				+ Constant.RoomReservationStatus.CKI.name() + "') AND rc.slient_checkin_status IS NULL  ";
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("arrivalDate", now);
//		query.setParameter("status", "'" +  "', '" + "'");
		int availableRHGolfFy = ((Number)query.uniqueResult()).intValue();
		return availableRHGolfFy;
	}
	
	public List<RoomReservationRec> getReservationByOrderNo(Long orderNo){
		return this.getByHql("from RoomReservationRec where orderNo = ?", Arrays.<Serializable>asList(orderNo));
	}
	/**
	 * 重载方法 【添加aqcdto可控制是否显示canelled】
	 * @param typeMap
	 * @param dto
	 * @return
	 */
	@Override
	public String getGuestroomAdvanceSearch(Map<String, org.hibernate.type.Type> typeMap, AdvancedQueryConditionsDto dto) {
		typeMap.put("reservationId", StringType.INSTANCE);
		typeMap.put("confirmId", StringType.INSTANCE);
		typeMap.put("academyID", StringType.INSTANCE);
		typeMap.put("patronName", StringType.INSTANCE);
		typeMap.put("arrivalDate", StringType.INSTANCE);
		typeMap.put("departureDate", StringType.INSTANCE);
		typeMap.put("roomTypeCode", StringType.INSTANCE);
		typeMap.put("roomId", StringType.INSTANCE);
		typeMap.put("rateCode", StringType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("totalCharge", LongType.INSTANCE);
		typeMap.put("roomTypeName", StringType.INSTANCE);
		typeMap.put("paymentMethod", StringType.INSTANCE);
		typeMap.put("roomNo", StringType.INSTANCE);
		typeMap.put("checkinMethod", StringType.INSTANCE);
		typeMap.put("transactionNo", LongType.INSTANCE);
		String isShowCancelledSql = "";
		if (dto.getIsShowCancelled()) {
			isShowCancelledSql = ", 'CAN'";
		}
		StringBuilder sql = new StringBuilder("select *  from (select distinct ").
				append(" r.resv_id reservationId,  ")
				.append(" r.confirm_id  confirmId ,")
				.append("  m.academy_no  academyID, ")
				.append(" CONCAT( c.salutation, ' ', c.given_name, ' ', c.surname ) patronName ,")
				//.append("  date_format(r.arrival_date, '%Y-%m-%d')  arrivalDate , r.arrival_date  arrival_date,  ")
				.append("  date_format(r.arrival_date, '%Y-%m-%d')  arrivalDate , ")
				//.append("  date_format(r.departure_date, '%Y-%m-%d')  departureDate, r.departure_date  departure_date , ")
				.append("  date_format(r.departure_date, '%Y-%m-%d')  departureDate,  ")
				.append(" r.room_type_code roomTypeCode,")
				.append(" r.room_id roomId, ")
				.append("  r.rate_code rateCode, ")
				.append(" trans.payment_method_code  paymentMethod , ")
				.append(" trans.transaction_no  transactionNo , ")
				.append("  (det.item_total_amout * det.order_qty) totalCharge, ")
				.append(" rt.type_name  roomTypeName, ")
				.append("  CASE WHEN  r.checkout_timestamp is not null  THEN 'CKO' WHEN (  r.checkout_timestamp is  null  and   r.checkin_timestamp is not null ) THEN 'CKI' ELSE  r.status END as status,  ")
				.append(" m.customer_id customerId ,")
				.append(" r.slient_checkin_status checkinMethod ,")
				.append(" rm.room_no roomNo")
				.append(" from room_reservation_rec r inner join member m on r.customer_id=m.customer_id ")
				.append(" inner join customer_profile c on m.customer_id=c.customer_id ")
				.append(" left join customer_order_trans trans on 	trans.order_no=r.order_no	")
				.append("  Left join customer_order_det det on r.confirm_id = det.ext_ref_no  ")
				//add  refund record no show reservation guest room
				.append("  LEFT JOIN pos_service_item_price AS item ON item.item_no =det.item_no  ")
				.append("  Left join room rm  on r.room_id = rm.room_id  ")
				.append(" left join room_type rt on rt.type_code=r.room_type_code  ")
				.append("  where  item.item_no not in ('MMS2BRS','"+Constant.MMSTHIRD_ITEM_NO+"') and  item.item_catagory<>'REFUND' and r.status in ('SUC','RFU', 'CKI', 'CKO', 'CKP'") 
				.append(isShowCancelledSql) 
				.append(") ) t "); 
		return sql.toString();
	}

	@Override
	public boolean checkOccupyByDateAndRoomId(Date date, Long roomId) {
		String hql = " SELECT rec FROM  RoomReservationRec rec " +
				" WHERE " +
				" rec.roomId = ? "+
				" AND date_format(?, '%Y-%m-%d') >= rec.arrivalDate "+
				" AND date_format(?, '%Y-%m-%d') < rec.departureDate ";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(roomId);
		param.add(date);
		param.add(date);
		RoomReservationRec rec = (RoomReservationRec)getUniqueByHql(hql, param);
		if(rec != null){
			return true;
		}
		return false;
	}
	
	/**
	 * 根据roomId查询今天是否预订了该房间
	 * @param customerId
	 * @param resvId
	 * @return
	 */
	@Override
	@Transactional
	public RoomReservationRec todayToSeeIfThereBookingRoomByCheckinInProgress(Long customerId, Long resvId){
		Date now = new Date();
		String hql = " FROM RoomReservationRec rc WHERE date_format( ? ,'%Y-%m-%d') = date_format(rc.arrivalDate,'%Y-%m-%d') AND rc.status = 'CKP' AND rc.resvId = ? AND rc.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(now);
		param.add(resvId);
		param.add(customerId);
		List<RoomReservationRec> list = getByHql(hql, param);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

}
