package com.sinodynamic.hkgta.dao.bi;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.bi.BiOasisFlexFile;

public interface BiOasisFlexFileDao extends IBaseDao<BiOasisFlexFile> {
	
	/***
	 * delete BiOasisFlexFile by createDate  yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public boolean deleteByDate(Date date);

	/**+
	 * 获取BiOasisFlexFile每天列表
	 * @return
	 */
	public List<BiOasisFlexFile> getBioasisFlexFile();
	

}
