package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.CorporateMemberDto;
import com.sinodynamic.hkgta.dto.crm.CorporateProfileDto;
import com.sinodynamic.hkgta.entity.crm.CorporateProfile;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.pagination.ListPage.OrderType;

@Repository
public class CorporateProfileDaoImpl extends GenericDao<CorporateProfile>
		implements CorporateProfileDao {

	private Logger logger = Logger.getLogger(CorporateProfileDaoImpl.class);
	
	public Serializable saveCorporateProf(CorporateProfile corporateProfile) {
		return save(corporateProfile);
	}

	public boolean updateCorporateProf(CorporateProfile corporateProfile) throws HibernateException{
		return update(corporateProfile);
	}

	public CorporateProfile getByCorporateId(Long corporateId) {
		String hqlstr = "from CorporateProfile where corporateId = ?";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(corporateId);
		return (CorporateProfile) getUniqueByHql(hqlstr, listParam);
	}

	/**
	 * Method to check if the typed br_No exists or not for register
	 * @param brNo The business register number
	 * @return false if exists, true if not exists
	 */
	public boolean checkAvailableBRNo(String brNo) {
		String hqlstr = " from CorporateProfile where brNo = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(brNo);
		CorporateProfile corpProf = (CorporateProfile) getUniqueByHql(hqlstr,listParam);
		if (corpProf != null)
			return false;
		return true;
	}
	
	/**
	 * Method to check if the typed company name exists or not for register
	 * @param companyName: The company name
	 * @return false if exists, true if not exists
	 */
	public boolean checkAvailableCompanyName(String companyName) {
		String hqlstr = " from CorporateProfile c where c.companyName = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(companyName);
		CorporateProfile corpProf = (CorporateProfile) getUniqueByHql(hqlstr,listParam);
		if (corpProf != null)
			return false;
		return true;
	}
	
	/**
	 * Method to check if the typed company name exists or not for update
	 * (The current corporateId is ignored)
	 * @param comanyName
	 * @param corporateId
	 * @return false if exists, true if not exists
	 */
	public boolean checkAvailableCompanyName(String companyName, Long corporateId){
		String hqlstr = " from CorporateProfile c where c.companyName = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(companyName);
		CorporateProfile corpProf = (CorporateProfile) getUniqueByHql(hqlstr,listParam);
		if(corpProf!=null){//This brNo exists
			if(corpProf.getCorporateId().equals(corporateId)){
				return true;
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check if the typed br_No exists or not for update
	 * (The current corporateId is ignored)
	 * @param brNo
	 * @param corporateId
	 * @return false if exists, true if not exists
	 */
	public boolean checkAvailableBrNo(String brNo, Long corporateId){
		String hqlstr = " from CorporateProfile where brNo = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(brNo);
		CorporateProfile corpProf = (CorporateProfile) getUniqueByHql(hqlstr,listParam);
		if(corpProf!=null){//This brNo exists
			if(corpProf.getCorporateId().equals(corporateId)){
				return true;
			}
			return false;
		}
		return true;
	}
	
	public ListPage<CorporateProfile> getCorpoateProfileListOld(
			ListPage<CorporateProfile> page, CorporateProfileDto dto,String status) {
		
		StringBuilder hql = new StringBuilder(" select\n" +
				"        corporate_profile.corporate_id as corporateId,\n" +
				"        corporate_profile.company_name as companyName,\n" +
				"        service_plan.plan_name as planName,\n" +
				"        service_plan.plan_no as servicePlanNo,\n" +
				"        corporate_profile.create_date as createDate,\n" +
				"        um.nickname as followByStaff,\n" +
				"        corporate_acc.status as status,\n" +
				"        corporate_acc.contract_ref_no as contractRefNo\n," +
				"        corporate_acc.acc_no as accNo\n" +				
				"    from\n" +
				"        corporate_profile corporate_profile,\n" +
				"        service_plan service_plan ,\n" +
				"        corporate_service_subscribe corporates2_,\n" +
				"        (select csa.corporate_id as corporate_id,max(csa.acc_no) as acc_no from corporate_service_acc csa GROUP BY csa.corporate_id) corporate_alias, \n" +
				"        corporate_service_acc corporate_acc\n" +
				"    left join user_master um on um.user_id = corporate_acc.follow_by_staff \n"+
				"    where\n" +
				"        corporate_profile.corporate_id=corporate_acc.corporate_id \n" +
				"        and corporate_acc.acc_no=corporates2_.acc_no \n" +
				"        and service_plan.plan_no=corporates2_.service_plan_no\n" +
				"        and corporate_acc.acc_no = corporate_alias.acc_no ");
		String countHql=null;
		List<Serializable> param = new ArrayList<Serializable>();
		if(Constant.General_Status_ACT.equalsIgnoreCase(status)||Constant.General_Status_NACT.equalsIgnoreCase(status)||Constant.General_Status_EXP.equalsIgnoreCase(status)){
			hql.append("and corporate_acc.status = ? ");
			param.add(status);
			
			if(!StringUtils.isBlank(page.getCondition())){
				String condition = " and "+page.getCondition();
				hql.append(condition);
			}
			
			countHql = " SELECT count(corporate_profile.corporate_id) " +hql.substring(hql.indexOf("from"));
			return listBySqlDto(page, countHql, hql.toString(), param, dto);
		}
		
		if(!StringUtils.isBlank(page.getCondition())){
			String condition = " and "+page.getCondition();
			hql.append(condition);
		}
		
		countHql = " SELECT count(corporate_profile.corporate_id) " +hql.substring(hql.indexOf("from"));
		return listBySqlDto(page, countHql, hql.toString(), null, dto);

	}
	
	public ListPage<CorporateProfile> getCorpoateProfileList(ListPage<CorporateProfile> page) {
		
		String sql = new String("SELECT\n" +
				"	corporateProfile.corporate_id AS corporateId,\n" +
				"	corporateProfile.company_name AS companyName,\n" +
				"	CONCAT_WS(' ',corporateProfile.contact_person_firstname,contact_person_lastname) AS followByStaff,\n" +
				"	acc.follow_by_staff AS salesStaffId,\n" +
				"	concat( p.given_name,' ', p.surname) salesStaffName,\n" +
				"	(\n" +
				"		SELECT\n" +
				"			count(1)\n" +
				"		FROM\n" +
				"			corporate_member corporateMember,\n" +
				"			customer_service_acc customerServiceAcc\n" +
				"		WHERE\n" +
				"			corporateMember.corporate_id = corporateProfile.corporate_id\n" +
				"		AND corporateMember.customer_id = customerServiceAcc.customer_id\n" +
				"		AND date_format(now(), '%Y-%m-%d') BETWEEN customerServiceAcc.effective_date\n" +
				"		AND customerServiceAcc.expiry_date\n" +
				"		AND customerServiceAcc.status = 'ACT'\n" +
				"		AND corporateMember.status = 'ACT'\n" +
				"		AND customerServiceAcc.acc_cat = 'COP'\n" +
				"	) AS noOfActiveMember,\n" +
				"	acc.total_credit_limit AS creditLimit\n" +
				"FROM\n" +
				"	corporate_profile corporateProfile,\n" +
				"	corporate_service_acc acc\n" +
				"	INNER JOIN  staff_profile AS p ON p.user_id = acc.follow_by_staff " +
				"WHERE\n" +
				"	acc.corporate_id = corporateProfile.corporate_id\n" +
				"AND acc.acc_no = (\n" +
				"	SELECT\n" +
				"		max(accInternal.acc_no)\n" +
				"	FROM\n" +
				"		corporate_service_acc accInternal\n" +
				"	WHERE\n" +
				"		accInternal.corporate_id = corporateProfile.corporate_id\n" +
				")");
		if(!StringUtils.isBlank(page.getCondition())){
			sql = "SELECT * FROM ( "+sql + ") result\n";
			String condition = " WHERE "+page.getCondition();
			sql = sql + (condition);
		}
		
		String countHql = " SELECT count(1) FROM ( " +sql + " ) countSql ";
		return listBySqlDto(page, countHql, sql, null, new CorporateProfileDto());

	}
	
	public ListPage<CorporateProfile> getMemberList(
			ListPage<CorporateProfile> page,String byAccount,String status,Long filterByCustomerId) {
		List<Serializable> listParam = new ArrayList<Serializable>();
		String sql = new String("SELECT\n" +
				"	corp.*, \n" +
				"  ccp.corporate_id AS corporateId,\n" +
				"  mlr.num_value AS creditLimit,\n" +
				"  ccp.company_name AS companyName,\n" +
				"  CASE corp.memberType\n" +
				"  WHEN 'CPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = corp.customerId\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = corp.superiorMemberId\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"  ELSE NULL\n" +
				"	END AS planName,\n" +
				"	CASE corp.memberType\n" +
				"  WHEN 'CPM' THEN\n" +
				"		(\n" +
				"     SELECT CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) FROM staff_profile staffProfileCreateBy WHERE staffProfileCreateBy.user_id = ce.sales_follow_by\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN\n" +
				"		(\n" +
				"     SELECT CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) FROM staff_profile staffProfileCreateBy WHERE staffProfileCreateBy.user_id = ce1.sales_follow_by\n" +
				"		)\n" +
				"  ELSE NULL\n" +
				"	END AS officer,\n" +
				"  \n" +
				"	CASE corp.memberType\n" +
				"  WHEN 'CPM' THEN\n" +
				"		(\n" +
				"     SELECT\n" +
				"			inter.effective_date as effectiveDate\n" +
				"		 FROM\n" +
				"			customer_service_acc inter\n" +
				"		 WHERE\n" +
				"			inter.customer_id = corp.customerId\n" +
				"     AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"     ORDER BY\n" +
				"	    inter.expiry_date DESC\n" +
				"     limit 0,1\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN \n" +
				"    (\n" +
				"     SELECT\n" +
				"			inter.effective_date as effectiveDate\n" +
				"		 FROM\n" +
				"			customer_service_acc inter\n" +
				"		 WHERE\n" +
				"			inter.customer_id = corp.superiorMemberId\n" +
				"     AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"     ORDER BY\n" +
				"	    inter.expiry_date DESC\n" +
				"     limit 0,1\n" +
				"		)\n" +
				"  ELSE NULL\n" +
				"	END AS activationDate\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			m.academy_no AS academyNo,\n" +
				"			m.member_type AS memberType,\n" +
				"			m.superior_member_id AS superiorMemberId,\n" +
				"			CONCAT(\n" +
				"				cp.salutation,\n" +
				"				' ',\n" +
				"				cp.given_name,\n" +
				"				' ',\n" +
				"				cp.surname\n" +
				"			) AS memberName,\n" +
				"			m.status AS status ,"+
				"			m.create_by as userId,\n" +
				"           (case m.status when 'ACT' then 'Active' when 'NACT' then 'Inactive' end) as statusValue," +
				"			cp.customer_id AS customerId ,\n" +
				
				" 			cp.given_name as firstName , "+
			    " 			cp.surname as lastName ,"+
			    " 			cp.nationality ,"+
			    "  			cp.passport_no as passportNo,"+
				"  			cp.gender AS gender," +
				"  			cp.contact_email AS contactEmail," +
				"			date_format(cp.date_of_birth, '%m-%d') AS birthday,"+
				"			date_format(m.effective_date, '%m-%d') AS effectiveDate,"+
				"			cp.phone_mobile AS mobilePhone,"+
				"  			cai.customer_input AS licensePlate ," +
				"			m.vip as vip"+
				"		FROM\n" +
				"			member m,\n" +
				"			customer_profile cp "
							+ " LEFT JOIN customer_addition_info cai ON cp.customer_id = cai.customer_id   AND cai.caption_id = 5"
							+ "\n" +
				"		WHERE\n" +
				"			cp.customer_id = m.customer_id\n" +
				"	) corp\n" +
				"LEFT JOIN member_limit_rule mlr ON mlr.customer_id = corp.customerId AND mlr.limit_type = 'CR' AND date_format( ? , '%Y-%m-%d') BETWEEN mlr.effective_date AND mlr.expiry_date\n" +
				"LEFT JOIN customer_enrollment ce ON ce.customer_id = corp.customerId \n" +
				"LEFT JOIN customer_enrollment ce1 ON ce1.customer_id = corp.superiorMemberId \n" +
				"LEFT JOIN corporate_member cm ON corp.customerId = cm.customer_id OR corp.superiorMemberId = cm.customer_id\n" +
				"LEFT JOIN corporate_profile ccp ON cm.corporate_id = ccp.corporate_id\n" +
				//add Consolidate the advance search elements
				"WHERE\n" +
				"((corp.memberType = 'CPM'\n" +
				"  AND \n" +
				" (ce.status = 'ANC' OR ce.status = 'CMP'))\n" +
				"OR (corp.memberType = 'CDM'))\n");
		Date currentDate = new Date();
		listParam.add(currentDate);
		listParam.add(currentDate);
		listParam.add(currentDate);
		listParam.add(currentDate);
		listParam.add(currentDate);
		if (!byAccount.equalsIgnoreCase("ALL")) {
			sql = sql + " and ccp.corporate_id = ? ";
			listParam.add(Long.parseLong(byAccount));
		}
		
		if(filterByCustomerId!=null){
			sql = sql + " and corp.customerId = ? ";
			listParam.add(filterByCustomerId);
		}
		String countSql = null;
		if(Constant.General_Status_ACT.equalsIgnoreCase(status)||Constant.General_Status_NACT.equalsIgnoreCase(status)){
			sql = sql + " and corp.`status` = ? ";
			listParam.add(status);
		}
		
		if(!StringUtils.isBlank(page.getCondition())){
			sql = "SELECT * FROM ( "+sql + ") resultAdvanceSearch\n";
			String condition = " WHERE "+page.getCondition();
			sql = sql + (condition);
		}
		
		List<ListPage<CorporateProfile>.OrderBy> orderCondition = page.getOrderByList();
		StringBuilder orderBy = new StringBuilder("");
		for(ListPage<CorporateProfile>.OrderBy sortByTemp:orderCondition ){
			if(!StringUtils.isEmpty(sortByTemp.getProperty())){
				if(!StringUtils.isEmpty(orderBy.toString())){
					orderBy.append(",");
				}else{
					orderBy.append(" ORDER BY ");
				}
				if(OrderType.ASCENDING.equals(sortByTemp.getType())){
					orderBy.append(sortByTemp.getProperty()).append(" asc ");
				}else{
					orderBy.append(sortByTemp.getProperty()).append(" desc ");
				}
			}
		}
		sql = sql + orderBy;
		countSql = " SELECT count(1) from (" +sql+ ") countSql";
		
		if(logger.isDebugEnabled())
		{
			logger.debug("sql : " + sql);
		}
		return listBySqlDto(page, countSql, sql, listParam, new CorporateMemberDto());

	}
	
	
	@Override
	public List<CorporateProfile> getCorporateAccountDropList() {
		String hql = " select cp.companyName as companyName, cp.corporateId as corporateId, cp.status as status from CorporateProfile cp order by cp.companyName asc";
//		return getByHql(hql);
		return getDtoByHql(hql, null, CorporateProfile.class);
	}
	
	public  CorporateProfileDto getCorporateContactByCorporateIdAndContactType(Long corporateId,String contactType){
		String sql = null;
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		//Contact Type:INCHARGE, ADMINISTRATION, FINANCE
		if("ADMINISTRATION".equalsIgnoreCase(contactType)||"FINANCE".equalsIgnoreCase(contactType)){
			sql = "SELECT\n" +
					"	corpAdd.contact_email AS contactEmail,\n" +
					"	CONCAT_WS(\n" +
					"		' ',\n" +
					"		corpAdd.contact_person_firstname,\n" +
					"		corpAdd.contact_person_lastname\n" +
					"	) AS contactPersonFullName\n" +
					"FROM\n" +
					"	corporate_addition_contact corpAdd\n" +
					"WHERE\n" +
					"	corpAdd.corporate_id = ?\n" +
					"AND corpAdd.contact_type = ?";
			param.add(contactType);
		}else{
			sql = "SELECT\n" +
					"	corp.contact_email AS contactEmail,\n" +
					"	CONCAT_WS(\n" +
					"		' ',\n" +
					"		corp.contact_person_firstname,\n" +
					"		corp.contact_person_lastname\n" +
					"	) AS contactPersonFullName\n" +
					"FROM\n" +
					"	corporate_profile corp\n" +
					"WHERE\n" +
					"	corp.corporate_id = ?";
		}
		
		List<CorporateProfileDto> result = getDtoBySql(sql, param, CorporateProfileDto.class);
		if(result!=null&&result.size()>0){
			return result.get(0);
		}else{
			return null;
		}
	}

	@Override
	public ListPage<CorporateProfile> getMemberList(ListPage<CorporateProfile> page, String sortBy, String isAscending,
			String byAccount, String status, Long filterByCustomerId) {
		// TODO Auto-generated method stub
		List<Serializable> listParam = new ArrayList<Serializable>();
		String sql = new String("SELECT\n" +
				"	corp.*, \n" +
				"  ccp.corporate_id AS corporateId,\n" +
				"	mlr.num_value AS creditLimit,\n" +
				"	ccp.company_name AS companyName,\n" +
				"	CASE corp.memberType\n" +
				"  WHEN 'CPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = corp.customerId\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = corp.superiorMemberId\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"  ELSE NULL\n" +
				"	END AS planName,\n" +
				"	CASE corp.memberType\n" +
				"  WHEN 'CPM' THEN\n" +
				"		(\n" +
				"     SELECT CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) FROM staff_profile staffProfileCreateBy WHERE staffProfileCreateBy.user_id = ce.sales_follow_by\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN 'N/A'\n" +
				"  ELSE NULL\n" +
				"	END AS officer,\n" +
				"  \n" +
				"	CASE corp.memberType\n" +
				"  WHEN 'CPM' THEN\n" +
				"		(\n" +
				"     SELECT\n" +
				"			inter.effective_date as effectiveDate\n" +
				"		 FROM\n" +
				"			customer_service_acc inter\n" +
				"		 WHERE\n" +
				"			inter.customer_id = corp.customerId\n" +
				"     AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"     ORDER BY\n" +
				"	    inter.expiry_date DESC\n" +
				"     limit 0,1\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN \n" +
				"    (\n" +
				"     SELECT\n" +
				"			inter.effective_date as effectiveDate\n" +
				"		 FROM\n" +
				"			customer_service_acc inter\n" +
				"		 WHERE\n" +
				"			inter.customer_id = corp.superiorMemberId\n" +
				"     AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"     ORDER BY\n" +
				"	    inter.expiry_date DESC\n" +
				"     limit 0,1\n" +
				"		)\n" +
				"  ELSE NULL\n" +
				"	END AS activationDate\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			m.academy_no AS academyNo,\n" +
				"			m.member_type AS memberType,\n" +
				"			m.superior_member_id AS superiorMemberId,\n" +
				"			CONCAT(\n" +
				"				cp.salutation,\n" +
				"				' ',\n" +
				"				cp.given_name,\n" +
				"				' ',\n" +
				"				cp.surname\n" +
				"			) AS memberName,\n" +
				"			m.status AS status,\n" +
				"           (case m.status when 'ACT' then 'Active' when 'NACT' then 'Inactive' end) as statusValue," +
				"			cp.customer_id AS customerId\n" +
				"		FROM\n" +
				"			member m,\n" +
				"			customer_profile cp\n" +
				"		WHERE\n" +
				"			cp.customer_id = m.customer_id\n" +
				"	) corp\n" +
				"LEFT JOIN member_limit_rule mlr ON mlr.customer_id = corp.customerId AND mlr.limit_type = 'CR' AND date_format( ? , '%Y-%m-%d') BETWEEN mlr.effective_date AND mlr.expiry_date\n" +
				"LEFT JOIN customer_enrollment ce ON ce.customer_id = corp.customerId \n" +
				"LEFT JOIN corporate_member cm ON corp.customerId = cm.customer_id OR corp.superiorMemberId = cm.customer_id\n" +
				"LEFT JOIN corporate_profile ccp ON cm.corporate_id = ccp.corporate_id\n" +
				"WHERE\n" +
				"((corp.memberType = 'CPM'\n" +
				"  AND \n" +
				" (ce.status = 'ANC' OR ce.status = 'CMP'))\n" +
				"OR (corp.memberType = 'CDM'))\n");
		Date currentDate = new Date();
		listParam.add(currentDate);
		listParam.add(currentDate);
		listParam.add(currentDate);
		listParam.add(currentDate);
		listParam.add(currentDate);
		if (!byAccount.equalsIgnoreCase("ALL")) {
			sql = sql + " and ccp.corporate_id = ? ";
			listParam.add(Long.parseLong(byAccount));
		}
		
		if(filterByCustomerId!=null){
			sql = sql + " and corp.customerId = ? ";
			listParam.add(filterByCustomerId);
		}
		String countSql = null;
		if(Constant.General_Status_ACT.equalsIgnoreCase(status)||Constant.General_Status_NACT.equalsIgnoreCase(status)){
			sql = sql + " and corp.`status` = ? ";
			listParam.add(status);
		}
		
		if(!StringUtils.isBlank(page.getCondition())){
			sql = "SELECT * FROM ( "+sql + ") t\n";
			String condition = " WHERE "+page.getCondition();
			sql = sql + (condition);
		}
		
//		StringBuilder orderBy = new StringBuilder("");
//		List<ListPage<CorporateProfile>.OrderBy> orderCondition = page.getOrderByList();
//		for(ListPage<CorporateProfile>.OrderBy sortByTemp:orderCondition ){
//			if(!StringUtils.isEmpty(sortByTemp.getProperty())){
//				if(!StringUtils.isEmpty(orderBy.toString())){
//					orderBy.append(",");
//				}else{
//					orderBy.append(" ORDER BY ");
//				}
//				if(OrderType.ASCENDING.equals(sortByTemp.getType())){
//					orderBy.append(sortByTemp.getProperty()).append(" asc ");
//				}else{
//					orderBy.append(sortByTemp.getProperty()).append(" desc ");
//				}
//			}
//		}
		StringBuilder orderBy = new StringBuilder();
		if(!StringUtils.isEmpty(sortBy)){
			String orderByFiled = sortBy.trim();
			orderBy.append(" order by ");
			if("true".equals(isAscending)){
				orderBy.append(orderByFiled).append(" asc ");
			}else{
				orderBy.append(orderByFiled).append(" desc ");
			}
		}
		sql = sql + orderBy.toString();
		countSql = " SELECT count(1) from (" +sql+ ") countSql";
		
		if(logger.isDebugEnabled())
		{
			logger.debug("sql : " + sql);
		}
		return listBySqlDto(page, countSql, sql, listParam, new CorporateMemberDto());
	}
}
