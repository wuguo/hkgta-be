package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.mms.SpaCategory;

@Repository
public class SpaCategoryDaoImpl extends GenericDao<SpaCategory> implements SpaCategoryDao{

	@Override
	public Serializable addSpaCategory(SpaCategory spaCategory)
	{
		return this.save(spaCategory);
	}
	
	@Override
	public SpaCategory getByCategoryId(Long categoryId)
	{
		return this.get(SpaCategory.class, categoryId);
	}
	
	
	public List<SpaCategory> getAllSpaCategorys()
	{
		String hqlstr  = " from SpaCategory m order by m.displayOrder";
		List<SpaCategory> categoryList = getByHql(hqlstr);
		return categoryList;
	}
	
	public List<SpaCategory> getRootSpaCategorys()
	{
		String hqlstr  = " from SpaCategory m where m.parentCatId is null order by m.displayOrder";
		List<SpaCategory> categoryList = getByHql(hqlstr);
		return categoryList;
	}
	
	public List<SpaCategory> getSubtSpaCategorys(String parentCategoryId)
	{
		String hqlstr  = " from SpaCategory m where m.parentCatId=? order by m.displayOrder";
		List param = new ArrayList();
		param.add(parentCategoryId);
		List<SpaCategory> categoryList = this.getByHql(hqlstr, param);
		return categoryList;
	}
}
