package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;

@Repository
public class StaffMasterDaoImpl extends GenericDao<StaffMaster> implements StaffMasterDao
{

	@Override
	public List<StaffMaster> getExpireStaff() {
		String hql = " from StaffMaster  where quitDate < ? and status<> ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(new Date());
		param.add("NACT");
		List<StaffMaster> list = this.getByHql(hql, param);
		return list;
	}
	
	/**
	 * 根据staffId查询此staff是否有效
	 * @param staffId
	 * @return
	 */
	@Override
	public boolean isStaffActive(String staffId) {
	    
	    String hql = "from StaffMaster m where m.status = 'ACT' and m.userId = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(staffId);
	    StaffMaster staffMaster = (StaffMaster) getUniqueByHql(hql, params);
	    return (staffMaster == null ? false : true);
	}
}
