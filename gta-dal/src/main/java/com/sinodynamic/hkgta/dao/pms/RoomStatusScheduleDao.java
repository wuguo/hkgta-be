package com.sinodynamic.hkgta.dao.pms;


import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.pms.RoomStatusScheduleDto;
import com.sinodynamic.hkgta.entity.pms.RoomStatusSchedule;

public interface RoomStatusScheduleDao extends IBaseDao<RoomStatusSchedule>{
	/***
	 * get expired RoomStatusSchedule
	 * @param  date>endDate
	 * @return
	 */
	public List<RoomStatusSchedule> getExpiredRoomStatusSchedule(Date date);
	/***
	 * get RoomStatusSchedule currentTime  between  begin_date and end_date  
	 * @param date
	 * @return
	 */
	public List<RoomStatusSchedule> getRoomStatusScheduleByDate(Date date);

	public List<RoomStatusSchedule> getRoomStatusSchedule(long roomId);
	
	public int deleteAll(String type);
	
	public List<RoomStatusScheduleDto> getRoomStatusScheduleDtoList(long roomId);
}
