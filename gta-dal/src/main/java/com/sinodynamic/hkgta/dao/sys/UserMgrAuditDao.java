package com.sinodynamic.hkgta.dao.sys;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.adm.UserRecordActionLog;

public interface UserMgrAuditDao extends IBaseDao<UserRecordActionLog> {

}
