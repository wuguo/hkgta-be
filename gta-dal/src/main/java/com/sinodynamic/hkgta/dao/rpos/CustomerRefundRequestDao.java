package com.sinodynamic.hkgta.dao.rpos;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.DaypassRefundDto;
import com.sinodynamic.hkgta.dto.crm.SpaRefundDto;
import com.sinodynamic.hkgta.dto.crm.WellnessRefundDto;
import com.sinodynamic.hkgta.dto.fms.CustomerRefundRequestDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;
import com.sinodynamic.hkgta.dto.rpos.PaymentInfoDto;
import com.sinodynamic.hkgta.dto.rpos.RefundInfoDto;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CustomerRefundRequestDao extends
		IBaseDao<CustomerRefundRequest> ,AdvanceQueryConditionDao{

	/**search data, within how many days
	 * @param base 
	 * @param days 0 = all days , 
	 *             1 = today ,
	 *             2 = 2 days
	 *             3 = 3 days 
	 * @return
	 */
	ListPage<CustomerRefundRequest> getPendingRefundList(Date base, int days,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL);
	
	ListPage<CustomerRefundRequest> getServiceRefundList(String status,Long customerId,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL);
	
	ListPage<CustomerRefundRequest> getCashValueRefundList(Date base, Integer days,String status, Long customerId,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL);
	
	PaymentInfoDto getPaymentInfo(Long txnId);
	
	RefundInfoDto getRefundInfo(Long txnId);
	
	PrivateCoachBookListDto getPrivateCoachBookingDetail(Long refundId, String bookingType);
	
	FacilityReservationDto getFacilityBookingDetail(Long refundId);
	
	CourseEnrollment getEnrollment(Long refundId);

	DaypassRefundDto getDaypassBookingDetail(Long refundId);

	CustomerRefundRequest getById(Long refundRequestId);

	WellnessRefundDto getWellnessRefundDetail(Long refundId);

	List<SpaRefundDto> matchRequestId(List<String> paymentIds);
	
	List<SpaRefundDto> matchCustomerName(List<String> academyNos);

	RefundInfoDto getCancelInfo(String resvId);

	/**
	 * 根据时间获取serviceRefund列表
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<CustomerRefundRequestDto> getServiceRefundList(String startTime, String endTime);
	
}
