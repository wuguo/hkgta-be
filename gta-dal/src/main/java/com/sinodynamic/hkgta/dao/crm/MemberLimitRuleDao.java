package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.MemberLimitRuleDto;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;

public interface MemberLimitRuleDao extends IBaseDao<MemberLimitRule>{
	public Serializable saveMemberLimitRuleImpl(MemberLimitRule mLimitRule);
	
	public void saveMemberLimitRule(List<MemberLimitRule> list);
	
	public MemberLimitRule getEffectiveByCustomerId(Long customerId);
	
	public List<MemberLimitRule>  getEffectiveListByCustomerId(Long customerId);
	
	public BigDecimal getTransactionLimitOfMember(Long customerId);

	public boolean getDayPassPurchaseNoOfMember(Long customerId);

	boolean getMemberTrainingRight(Long customerId);
	
	boolean getMemberEventsRight(Long customerId);

	boolean updateMmberTransactionLimit(Long customerId, BigDecimal tran,String userId);

	boolean updateMemberFacilitiesRight(Long customerId, String facilityRight);
	
	public BigDecimal getCreditLimitOfMember(Long customerId);

	boolean updateMemberTrainingRight(Long customerId, String trainingRight,String userId);

	boolean updateMemberEventsRight(Long customerId, String eventRight,String userId);

	boolean updateMemberDayPassPurchasing(Long customerId, String dayPass,String userId);
	
	public int deleteMemberLimitRule(Long customerId,String excludedLimitType);
	
	public List<MemberLimitRuleDto> getEffectiveMemberLimitRuleDtoByCustomerId(Long customerId);
	
	public MemberLimitRule getEffectiveMemberLimitRule(Long customerId,String limitType);
	
	public MemberLimitRule getLastEffectiveMemberLimitRule(Long customerId,String limitType);
	/***
	 * Member Multi-Generation is true or false 
	 * @param customerId
	 * @return
	 */
	public boolean getMemberMultiGenerationRight(Long customerId);
	
	public List<MemberLimitRule> getLastEffectiveListByCustomerId(Long customerId);

	/**
	 * 根据customerId获取该用户的信用值
	 * @param customerId
	 * @return
	 */
	public BigDecimal getMemberCreditValue(Long customerId);
	public MemberLimitRule getEffectiveRightByCustomerIdAndFacilityType(
			Long customerId, String facilityType,Date effectiveDate,String startTime,String endTime);

	public MemberLimitRule getLimitRuleByKey(Long customerId, String limitType, Date effectiveDate);
	
	/***
	 * 
	 * @param customerId
	 * @param limitType
	 * @return
	 */
	public MemberLimitRule getLimitRuleByCustomerIdAndLimitType(Long customerId, String limitType) ;
	
}
