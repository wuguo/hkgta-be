package com.sinodynamic.hkgta.dao.pos;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantOpenHour;

@Repository
public class RestaurantOpenHourDaoImpl extends GenericDao<RestaurantOpenHour> implements RestaurantOpenHourDao
{
	@Override
	public RestaurantOpenHour getRestaurantOpenHour(String restaurantId)
	{
		String hql = "from RestaurantOpenHour r where r.restaurantMaster.restaurantId = ? ";
		List<Serializable> params = new ArrayList<>();
		params.add(restaurantId);
		return (RestaurantOpenHour) super.getUniqueByHql(hql,params);
		
	}
}
