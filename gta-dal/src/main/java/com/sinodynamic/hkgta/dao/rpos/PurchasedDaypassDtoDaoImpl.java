package com.sinodynamic.hkgta.dao.rpos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.rpos.PurchasedDaypassDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;

@Repository("PurchasedDaypassList")
public class PurchasedDaypassDtoDaoImpl extends GenericDao<PurchasedDaypassDto>  implements PurchasedDaypassDtoDao , AdvanceQueryConditionDao{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Payment Order #","orderNo","java.math.BigInteger","",1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("# of Pass","orderTotalAmount","java.math.BigInteger","",2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Referral by Name","memberName","java.lang.String","",3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Purchased Date","orderDate","java.util.Date","",4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Activation Period begin","effectiveStartDate","java.util.Date","",5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Activation Period end","effectiveEndDate","java.util.Date","",6);
		final List<SysCode> status=new ArrayList<>();
		//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled
		SysCode s1=new SysCode();
		s1.setCategory("status");
		s1.setCodeDisplay("Open");
		s1.setCodeValue("OPN");
		status.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("status");
		s2.setCodeDisplay("Completed");
		s2.setCodeValue("CMP");
		status.add(s2);
		
		SysCode s3=new SysCode();
		s3.setCategory("status");
		s3.setCodeDisplay("Cancelled");
		s3.setCodeValue("CAN");
		status.add(s3);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Order Status","orderStatus","java.lang.String",status,7);
		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Referral by Academy ID","academyNo","java.lang.String","",8);
		AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Guest Name","guestName","java.lang.String","",9);
		AdvanceQueryConditionDto condition10 = new AdvanceQueryConditionDto("Guest Phone Number","phoneMobile","java.lang.String","",10);
		AdvanceQueryConditionDto condition11 = new AdvanceQueryConditionDto("Guest Email Address","guestEmail","java.lang.String","",11);
		AdvanceQueryConditionDto condition12 = new AdvanceQueryConditionDto("Guest HKID / Passport #","passportNo","java.lang.String","",12);
		return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8,condition9,condition10,condition11,condition12);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		// TODO Auto-generated method stub
		return null;
	}

}
