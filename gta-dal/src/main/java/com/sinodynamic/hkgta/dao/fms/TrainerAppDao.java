package com.sinodynamic.hkgta.dao.fms;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.staff.CoachReservationDto;
import com.sinodynamic.hkgta.dto.staff.CoachSessionListDto;
import com.sinodynamic.hkgta.dto.staff.MembersPresentCurriculumListDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface TrainerAppDao extends IBaseDao {
	enum AchievementClassification{
		HOUR_OF_COURSES,
		HOUR_OF_CLASSES,
		TOTAL_OF_STUDENTS,
		TOTAL_OF_NEW_STUDENTS
	}
	ListPage<CoachReservationDto> reservationList(String coachId, String orderColumn, String order, int pageSize,
			int currentPage, String filterSQL);
	Object calculateCoachAchievement(AchievementClassification classification,Map<String, Object> params);
	List<CustomerProfileDto> getPrivateCoachTrainedMemberList(String staffNo, String isAscending);
	List<CustomerProfileDto> getPrivateCoachTrainedMemberListByName(String staffNo, String name, String isAscending);
	
	/**
	 * 根据条件参数查询课程列表
	 * @param coachId    staffId
	 * @param showType	0：查询私人教练课程同大学堂课程    1：查询私人教练课程   2：大学堂课程 
	 * @return
	 */
	List<CoachSessionListDto> reservationListBySessionDate(String coachId, int showType);
	
	/**
	 * 根据条件参数查询课程列表
	 * @param QRCode    academyNo/cardNo
	 * @param QRCodeType	1：academyNo    2：cardNo    
	 * @return
	 */
	List<MembersPresentCurriculumListDto> getMembersPresentCurriculumList(String QRCode, int QRCodeType);

}
