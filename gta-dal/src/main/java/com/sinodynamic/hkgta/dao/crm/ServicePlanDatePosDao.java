package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanDatePos;

public interface ServicePlanDatePosDao extends IBaseDao<ServicePlanDatePos>{

}
