package com.sinodynamic.hkgta.dao.sys;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceIo;

@Repository
public class CarparkInterfaceIoDaoImpl extends GenericDao<CarparkInterfaceIo> implements CarparkInterfaceIoDao {

	@Override
	public List<CarparkInterfaceIo> getByCustomerId(Long customerId) {
		String hqlstr = " from CarparkInterfaceIo c where c.customerId =? Order By c.createDate Desc";
		 Query query=	this.getsessionFactory().getCurrentSession().createQuery(hqlstr);
		 query.setParameter(0, customerId);
		 query.setFirstResult(0);
		 query.setMaxResults(1);
		return  query.list();
		
//		return (List<CarparkInterfaceIo>) getCurrentSession().createQuery(hqlstr).setLong("customerId", customerId).list();
	}
}
