package com.sinodynamic.hkgta.dao.sys;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CarparkInterfaceIo;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;

public interface CarparkInterfaceIoDao extends IBaseDao<CarparkInterfaceIo> 
{
	public List<CarparkInterfaceIo> getByCustomerId(Long customerId);

}
