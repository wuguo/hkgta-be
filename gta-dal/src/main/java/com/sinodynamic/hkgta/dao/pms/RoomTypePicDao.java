package com.sinodynamic.hkgta.dao.pms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.pms.RoomPicPathDto;
import com.sinodynamic.hkgta.entity.pms.RoomPicPath;

public interface RoomTypePicDao extends IBaseDao<RoomPicPath>{

	public void saveRoomPic(List<RoomPicPathDto> pics);
	
	public void updateRoomPic(List<RoomPicPathDto> pics);
	
}
