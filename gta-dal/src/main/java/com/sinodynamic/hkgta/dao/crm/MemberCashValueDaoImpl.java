package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;

@Repository
public class MemberCashValueDaoImpl extends GenericDao<MemberCashvalue> implements MemberCashValueDao {

	public MemberCashvalue getByCustomerId(Long customerId) {
		return get(MemberCashvalue.class, customerId);
	}

	public Serializable saveMemberCashValue(MemberCashvalue memberCashvalue) {
		return save(memberCashvalue);
	}

	public boolean updateMemberCashValue(MemberCashvalue memberCashvalue) {
		return update(memberCashvalue);
	}
	

	public MemberCashvalue getByVirtualAccNo(String virtualAccNo) {
		
		String hql = "from MemberCashvalue where virtualAccNo = '" + virtualAccNo + "'";
		List<MemberCashvalue> result = getByHql(hql);
		if (result != null && result.size() > 0) return result.get(0);
		return null;
	}

	
	@Override
	public MemberCashvalue getMemberCashvalueById(Long customerId) {
		return this.get(MemberCashvalue.class, customerId);
	}

	@Override
	public MemberCashvalue getMemberCashvalueByIdForUpdate(Long customerId)
	{
		String hql = "select m.customer_id,m.initial_value,m.initial_date,m.available_balance,m.exchg_factor,m.expiry_date,m.internal_remark from member_cashvalue m  where customer_id = ? for update";
		List<Serializable> params = new ArrayList<Serializable>(); 
		params.add(customerId);
		List<MemberCashvalue> result = getByHql(hql,params);
		if (result != null && result.size() > 0) return result.get(0);
		return null;
	}

	
}
