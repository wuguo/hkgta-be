package com.sinodynamic.hkgta.dao.qst;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.qst.SurveyQuestionnaireStatisticsDataDto;
import com.sinodynamic.hkgta.entity.qst.SurveyMember;

public interface SurveyMemberDao extends IBaseDao<SurveyMember> {

	/**
	 * 获取符合条件的问卷调查统计数据
	 * @param customerId
	 * @return
	 */
	public List<SurveyQuestionnaireStatisticsDataDto> getMeetConditionsSurveyQuestionnaireStatisticsData(Long customerId);
	
	public List<SurveyMember> getPageDatalist(int pageNum,int pageSize, String queryListHQL, List<Serializable>  parameters);

}
