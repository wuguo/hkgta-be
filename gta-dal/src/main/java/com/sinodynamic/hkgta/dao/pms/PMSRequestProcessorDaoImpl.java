package com.sinodynamic.hkgta.dao.pms;

import java.util.Date;
import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.TLock;
import com.sinodynamic.hkgta.util.exception.LockFailedException;

@Repository
public class PMSRequestProcessorDaoImpl extends GenericDao implements PMSRequestProcessorDao {
	@Override
	public List<Integer> queryAllAvailableSpecifiedFacilityByPeriod(
			Date startTime, Date endTime, String facilityType,
			String attributeId, String status ) {
		String sql = "select fm.facility_no from facility_master fm,facility_addition_attribute b, facility_attribute_caption c  where not EXISTS (select 1 from facility_timeslot  ft where fm.facility_no = ft.facility_no and ("  
				+ "(ft.begin_datetime < :startTime and ft.end_datetime>:startTime )"
				+ "or  (ft.begin_datetime < :endTime and ft.end_datetime >:startTime )"
				+ "or  (ft.begin_datetime <:endTime and ft.end_datetime>=:endTime)"
				+ ")) and  fm.facility_no = b.facility_no AND b.attribute_id = c.attribute_id AND UPPER(fm.facility_type) = :facilityType AND b.attribute_id = :attributeId AND fm. STATUS = :status ";
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("startTime", startTime);
		query.setParameter("endTime", endTime);
		query.setParameter("facilityType", facilityType);
		query.setParameter("attributeId", attributeId);
		query.setParameter("status", status);
		List<Integer> facilityNos = query.list();
		return facilityNos;
	}
	@Override
	public List<Integer> queryAllAvailableSpecifiedFacilityByPeriod(Date startTime,Date endTime,String facilityType,String attributeId,String status,int fromFacilityNo,int endFacilityNo) {
		
		String sql = "select fm.facility_no from facility_master fm,facility_addition_attribute b, facility_attribute_caption c  where not EXISTS (select 1 from facility_timeslot  ft where fm.facility_no = ft.facility_no and ("  
				+ "(ft.begin_datetime < :startTime and ft.end_datetime>:startTime )"
				+ "or  (ft.begin_datetime < :endTime and ft.end_datetime >:startTime )"
				+ "or  (ft.begin_datetime <:endTime and ft.end_datetime>=:endTime)"
				+ ")) and  fm.facility_no = b.facility_no AND b.attribute_id = c.attribute_id AND UPPER(fm.facility_type) = :facilityType AND b.attribute_id = :attributeId AND fm. STATUS = :status"
				+ "   and  fm.facility_no>=:fromFacilityNo and  fm.facility_no<=:endFacilityNo ";		
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("startTime", startTime);
		query.setParameter("endTime", endTime);
		query.setParameter("facilityType", facilityType);
		query.setParameter("attributeId", attributeId);
		query.setParameter("status", status);
		query.setParameter("fromFacilityNo", fromFacilityNo);
		query.setParameter("endFacilityNo", endFacilityNo);
		List<Integer> facilityNos = query.list();
		return facilityNos;
	}
	@Override
	public int countSpecifiedFacility(String facilityType, String attributeId, String status) {
		String sql ="SELECT count(1) FROM facility_master a, facility_addition_attribute b, facility_attribute_caption c WHERE a.facility_no = b.facility_no AND b.attribute_id = c.attribute_id AND UPPER(a.facility_type) = :facilityType AND b.attribute_id = :attributeId AND a. STATUS = :status ";
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("facilityType", facilityType);
		query.setParameter("attributeId", attributeId);
		query.setParameter("status", status);
		int availableRHGolfFy = ((Number)query.uniqueResult()).intValue();
		return availableRHGolfFy;
	}
	@Override
	public int countSpecifiedFacility(String facilityType,String attributeId,String status,int fromFacilityNo,int endFacilityNo) {
		String sql ="SELECT count(1) FROM facility_master a, facility_addition_attribute b, facility_attribute_caption c WHERE a.facility_no = b.facility_no AND b.attribute_id = c.attribute_id AND UPPER(a.facility_type) = :facilityType AND b.attribute_id = :attributeId AND a. STATUS = :status"
				+ "  and  a.facility_no>=:fromFacilityNo and  a.facility_no<=:endFacilityNo ";
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("facilityType", facilityType);
		query.setParameter("attributeId", attributeId);
		query.setParameter("status", status);
		query.setParameter("fromFacilityNo", fromFacilityNo);
		query.setParameter("endFacilityNo", endFacilityNo);
		int availableRHGolfFy = ((Number)query.uniqueResult()).intValue();
		return availableRHGolfFy;
	}

	@Override
	public int countReservedSpecifiedFacility(Date startTime, Date endTime,String facilityType,String attributeId) {
		String hql = "  select count(1) from facility_timeslot ft,member_reserved_facility mrf ,member_facility_type_booking mftb,member_facility_book_addition_attr mfbaa where" 
				   + " ft.facility_timeslot_id = mrf.facility_timeslot_id and mrf.resv_id = mftb.resv_id and mfbaa.resv_id = mftb.resv_id" 
				   + " and mfbaa.attribute_id=:attributeId and mfbaa.facility_type=:facilityType"
			       + " and ((ft.begin_datetime<:startTime and ft.end_datetime>:startTime)"
			       + " or  (ft.begin_datetime <:endTime and ft.end_datetime >:startTime)"
			       + " or  (ft.begin_datetime <:endTime and ft.end_datetime>=:endTime))";
		Query query = getCurrentSession().createSQLQuery(hql);
		query.setParameter("startTime", startTime);
		query.setParameter("endTime", endTime);
		query.setParameter("facilityType", facilityType);
		query.setParameter("attributeId", attributeId);
		int reservedFacility = ((Number)query.uniqueResult()).intValue();
		return reservedFacility;
		
	}

	@Override
	public int isoccupied(Integer facilityNo, Date startTime, Date endTime) {
		String sql = "select count(1) from facility_timeslot ft where ft.facility_no= :facilityNo and ("  
				+ "(ft.begin_datetime < :startTime and ft.end_datetime>:startTime )"
				+ "or  (ft.begin_datetime < :endTime and ft.end_datetime >:startTime )"
				+ "or  (ft.begin_datetime <:endTime and ft.end_datetime>=:endTime))";
		
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("facilityNo", facilityNo);
		query.setParameter("startTime", startTime);
		query.setParameter("endTime", endTime);
		int count = ((Number) query.uniqueResult()).intValue();
		
		return count;
	}

	@Override
	public void lockFacilityMaster() {
		Query query = getCurrentSession().createSQLQuery("select * from facility_timeslot for update");
		query.list();
		
	}
	
	
    public void getLock() {
    	 Session session = getCurrentSession();

         Query query = session.createQuery("from TLock where lockName= :lockName");
         query.setParameter("lockName", "facilitylock");
         query.setLockOptions(LockOptions.UPGRADE);
         Object record = query.uniqueResult();
         if(null == record) {
        	 throw new LockFailedException("Lock record: lockName=facilitylock failed");
         }
         
    	
    }
    
    @Override
	public void saveOrUpdateLockRecord(Long id, String lockName) {
		TLock lock = new TLock(id,lockName);
		saveOrUpdate(lock);
		
	}

	@Override
	public List<TLock> getAllLocks() {
		List<TLock> list = getByHql("from TLock");
		return list;
		
	}

}
