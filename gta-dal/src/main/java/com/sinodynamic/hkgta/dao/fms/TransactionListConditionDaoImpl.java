package com.sinodynamic.hkgta.dao.fms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sinodynamic.hkgta.util.constant.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.fms.FacilityAttributeCaption;

@Repository("transactionlistcondition")
public class TransactionListConditionDaoImpl extends GenericDao<FacilityAttributeCaption> implements AdvanceQueryConditionDao {

    @Autowired
    private FacilityAttributeCaptionDao facilityAttributeCaptionDao;

    @Override
    public List<AdvanceQueryConditionDto> assembleQueryConditions() {
        StringBuffer stringBuffer = new StringBuffer();
        final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Patron ID", "c.academy_no", "java.lang.String", "", 1);
        AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron Name", "b.given_name", "java.lang.String", "", 2);
        AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Booking Date", "date(a.begin_datetime_book)", "java.util.Date", "", 3);
        getAttributeListByFacilityType(stringBuffer, "GOLF");
        getAttributeListByFacilityType(stringBuffer, "TENNIS");
        AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("FacilityAttribute", "i.caption", "java.lang.String", stringBuffer.toString(), 4);
        return Arrays.asList(condition1, condition2, condition3, condition4);
    }

    @Override
    public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
        StringBuffer stringBuffer = new StringBuffer();
        final AdvanceQueryConditionDto condition0 = new AdvanceQueryConditionDto("Reservation ID", "a.resv_id", "java.lang.Long", "", 1);
        final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Patron ID", "c.academy_no", "java.lang.String", "", 2);
        AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron Name", "b.memberName", "java.lang.String", "", 3);
        AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Booking Date", "date(a.begin_datetime_book)", "java.util.Date", "", 4);
        List<SysCode> options = getAttributeListByFacilityType(stringBuffer, type);

        AdvanceQueryConditionDto condition4;
        if (type.equalsIgnoreCase("GOLF") == true) {
            condition4 = new AdvanceQueryConditionDto("Bay Type", "i.caption", "java.lang.String", "", 5);
        } else {
            condition4 = new AdvanceQueryConditionDto("Court Type", "i.caption", "java.lang.String", "", 5);
        }
        condition4.setOptions(options);

        AdvanceQueryConditionDto statusDto = new AdvanceQueryConditionDto("Status", "a.statusDisplay", "java.lang.String", "", 6);

        String[] statusList = new String[]{"Checked-in", "Paying", "Pending for Payment", "Ready to check-in", "Overdue"};

        AdvanceQueryConditionDto facilityTypeQty = new AdvanceQueryConditionDto("# of Bay", "a.facility_type_qty", "java.lang.Integer", "", 7);
        //facilityTypeQty
        statusDto.setSelectName(statusDto.getDisplayName());
        return Arrays.asList(condition0, condition1, condition2, condition3, condition4, statusDto, facilityTypeQty);
    }

    private List<SysCode> getAttributeListByFacilityType(StringBuffer stringBuffer, String type) {
        List<FacilityAttributeCaption> facilityAttributeCaptionList = null;
        if (Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(type)) {
            facilityAttributeCaptionList = facilityAttributeCaptionDao.getFacilityAttributeCaptionByFuzzy(Constant.GOLFBAYTYPE);
        } else {
            facilityAttributeCaptionList = facilityAttributeCaptionDao.getFacilityAttributeCaptionByFuzzy(Constant.TENNISCRT);
        }

        List<SysCode> options = new ArrayList<>();
        for (FacilityAttributeCaption facilityAttributeCaption : facilityAttributeCaptionList) {
            SysCode sysCodeShow = new SysCode();
            sysCodeShow.setCodeDisplay(facilityAttributeCaption.getCaption());
            sysCodeShow.setCodeValue(facilityAttributeCaption.getCaption());
            options.add(sysCodeShow);

            if (stringBuffer.length() > 0) {
                stringBuffer.append(",");
            }
            stringBuffer.append(facilityAttributeCaption.getAttributeId());
        }

        return options;
    }

}
