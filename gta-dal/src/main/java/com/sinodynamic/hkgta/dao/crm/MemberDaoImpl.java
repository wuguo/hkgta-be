package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.ActivateMemberDto;
import com.sinodynamic.hkgta.dto.crm.AdultDependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.GoingToAdultDependentMemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.fms.MemberCashvalueDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.DepartmentStaff;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberType;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.constant.LoggerType;

@Repository
public class MemberDaoImpl extends GenericDao<Member> implements MemberDao {
	Logger logger = Logger.getLogger(MemberDaoImpl.class); 
	private Logger	debugLog = Logger.getLogger(LoggerType.SYSTEMDEBUG.getName()); //SAM 20161013 added
	
	@Autowired
	private ServicePlanDao servicePlanDao;
	@Autowired
	private SysCodeDao sysCodeDao;
	
	public void addMember(Member m){
		this.save(m);
		
	}
	
	public void updateMemberer(Member m){
		this.update(m);
		
	}

	public void deleteMember(Member m) throws Exception {
		this.delete(m);
		
	}

	public void getMemberList(ListPage<Member> page, String countHql,
			String hql, List<?> param) throws Exception {
		this.listByHql(page, countHql, hql, param);
		
	}

	public Member getMemberById(Serializable id){
		return (Member)this.get(Member.class, id);
	}

	public MemberType getMemberTypeByTypeName(String typeName) {
		String hqlstr = "from MemberType where typeName="+ typeName;
		return (MemberType) getUniqueByHql(hqlstr);
	}
	
	public String findLargestAcademyNo(){
		// Method to find the largest academy number by using the hql
		String hqlstr = " SELECT max(cast(m.academyNo as integer)) FROM Member m where cast(m.academyNo as integer) < 8000000  ";
		List<Integer> ret = excuteByHql(Integer.class, hqlstr);
		if (ret != null && ret.size() > 0)
			if(ret.get(0)==null) {
				return null;
			}else{
				return ret.get(0).toString();
			}
		// Method to find the largest academy number by using the sql, Work Alright
		// String sqlstr=
		// " SELECT MAX(CAST(t.academy_no AS SIGNED)) FROM member t where CAST(t.academy_no AS SIGNED)< 8000000 ";
		// BigInteger tempBigInteger = (BigInteger)
		// super.getCurrentSession().createSQLQuery(sqlstr).list().get(0);
		// return tempBigInteger.toString() ;
		return null;
	}
	
	public boolean validateAcademyID(String academyNo){
		String hqlstr = " from Member where academyNo= ?" ;
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(academyNo);
		List<Member> tempList = getByHql(hqlstr, param);
		if (tempList != null && tempList.size() >0 ) return false;
		return true;
	
	}
	
	public boolean updateStatus(String status, Long customerId){
		String hqlstr=" update Member m set m.status = ? where m.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		param.add(customerId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}
	
	public boolean updateStatus(String status, Long customerId,String loginUserId){
		String hqlstr=" update Member m set m.status = ?, m.updateBy = ?, m.updateDate = ? where m.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		param.add(loginUserId);
		param.add(new Date());
		param.add(customerId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}
	
	public boolean updateRelationship(String relationshipCode, Long customerId,String loginUserId){
		String hqlstr=" update Member m set m.relationshipCode = ?, m.updateBy = ?, m.updateDate = ? where m.customerId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(relationshipCode);
		param.add(loginUserId);
		param.add(new Date());
		param.add(customerId);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}

	@Override
	public ListPage<Member> getDependentMembers(ListPage<Member> page,
			Long customerID,String platform) {
		StringBuilder sb=new StringBuilder();
		sb.append(" select * from (");
		String sql = "SELECT\n" +
				"	m.customer_id AS customerId,\n" +
				"	m.academy_no AS academyNo,\n" +
				"	m.member_type AS memberType,\n" +
				"	m.status AS status,\n" +
				"	m.first_join_date AS firstJoinDate,\n" +
				"	CONCAT_WS(\n" +
				"		' ',\n" +
				"		c.salutation,\n" +
				"		c.given_name,\n" +
				"		c.surname\n" +
				"	) AS memberName,\n" +
				"	c.portrait_photo AS portraitPhoto,\n" +
				"  (SELECT code.code_display FROM sys_code code where code.code_value = m.relationship_code AND code.category = 'relationshipCode') as relationship\n" +
				"FROM\n" +
				"	member m\n" +
				"LEFT JOIN\n" +
				"	customer_profile c ON m.customer_id = c.customer_id\n" +
				"WHERE 1=1 \n" +
				" and c.is_deleted != 'Y'\n" +
				
				"AND m.superior_member_id = ?\n" +
				"OR m.customer_id = (\n" +
				"	SELECT\n" +
				"		superior_member_id\n" +
				"	FROM\n" +
				"		member\n" +
				"	WHERE\n" +
				"		customer_id = ?\n" +
				") ";
		sb.append(sql);	
		sb.append(" ) as m ");
		
		//SGG-3872  if platform is not null then  staff to search else APP/MEMBER PORTAL
		if(StringUtils.isEmpty(platform))
		{
			sb.append(" where m.status='"+Status.ACT.name()+"'");
		}
		
		
		String countSql = "SELECT count(1) FROM ( " +sb.toString() + " ) countSql ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerID.longValue());
		param.add(customerID.longValue());
		return listBySqlDto(page, countSql, sb.toString(), param, new DependentMemberDto());
	}
	

	public String getMemberTypeByAcademyNo(String academyNo){
		String hqlstr  = " from Member m where m.academyNo = ?";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(academyNo);
		Member member = (Member) getUniqueByHql(hqlstr, listParam);
		if(member==null) return null;
		return member.getMemberType();
	}

	public List<Member> getListMemberBySuperiorId(Long customerId){
		String hqlstr  = " select m.customer_id as customerId, m.academy_no as academyNo, m.create_by as createBy, "
				+ "m.create_date as createDate, m.effective_date as effectiveDate, m.first_join_date as firstJoinDate, "
				+ "m.internal_remark as internalRemark,m.relationship_code as relationshipCode, m.status as status, "
				+ "m.termination_date as terminationDate,m.update_by as updateBy,m.update_date as updateDate, "
				+ "m.superior_member_id as superiorMemberId, m.member_type as memberType, m.user_id as userId,m.ver_no as version  "
				+ "from member m where m.superior_member_id = ?";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(customerId);
		return getDtoBySql(hqlstr, listParam,Member.class);
	}


	@Override
	public Member getMemberByAcademyNo(String academyNo)
			throws HibernateException {
		// TODO Auto-generated method stub
		String hqlstr  = " from Member m where m.academyNo = ?";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(academyNo);
		Member member = (Member) getUniqueByHql(hqlstr, listParam);
		if(member==null) return null;
		return member;
	}

	@Override
	public MemberDto checkDMCreditLimit(Long customerId) {
		String sql = "SELECT a.customer_id AS customerId, s.spendSum as spendSum ,a.available_balance as availableBalance,c.creditLimit as creditLimit, a.available_balance+c.creditLimit-s.spendSum as remainSum " +
				"FROM (SELECT mc.customer_id,mc.available_balance FROM member_cashvalue mc WHERE mc.customer_id = ?) a " +
				"LEFT JOIN (SELECT SUM(r.num_value) AS spendSum,r.customer_id FROM member_limit_rule r,(SELECT customer_id from member where superior_member_id = ?) b " +
				"WHERE r.customer_id = b.customer_id and date_format( ?,'%Y-%m-%d')>=r.effective_date and date_format( ?,'%Y-%m-%d') <= r.expiry_date GROUP BY r.customer_id) s " +
				"ON s.customer_id = a.customer_id " +
				"LEFT JOIN (SELECT mlr.num_value as creditLimit,mlr.customer_id FROM member_limit_rule mlr where mlr.customer_id = ? and mlr.limit_type=? and date_format( ?,'%Y-%m-%d')>=mlr.effective_date and date_format( ?,'%Y-%m-%d') <= mlr.expiry_date) c " +
				"ON a.customer_id = c.customer_id";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		param.add(LimitType.CR.name());
		List<MemberDto> dtoList = this.getDtoBySql(sql, param, MemberDto.class);
		if(dtoList != null && dtoList.size() >0){
			return dtoList.get(0);
		}
		String sqlT = "SELECT mlr.num_value as creditLimit,mlr.customer_id as customerId FROM member_limit_rule mlr where mlr.customer_id = ? and mlr.limit_type=?  "
				+ " and date_format( ?,'%Y-%m-%d')>=mlr.effective_date and date_format( ?,'%Y-%m-%d') <= mlr.expiry_date";
		param.clear();
		param.add(customerId);
		param.add(LimitType.CR.name());
		param.add(currentDate);
		param.add(currentDate);
		List<MemberDto> dtoListT = this.getDtoBySql(sqlT, param, MemberDto.class);
		if(dtoListT != null && dtoListT.size() >0){
			return dtoListT.get(0);
		}
		return null;
	}
	public MemberDto getMemberCashvalue(Long customerId, String type) {
		//StringBuilder sql = new StringBuilder(" select mc.available_balance as availableBalance ,  mc.exchg_factor as exchgFactor , mlr.expiry_date as expiryDate,  mlr.num_value as creditLimit ,  mc.available_balance+mlr.num_value as remainSum ") 
		StringBuilder sql = new StringBuilder(" select mc.available_balance as availableBalance ,  mc.exchg_factor as exchgFactor ,   mlr.num_value as creditLimit ,  mc.available_balance+mlr.num_value as remainSum ") 
		.append(" from  member_limit_rule mlr  left join member_cashvalue mc  on mc.customer_id= mlr.customer_id ")
		.append(" where mlr.customer_id=? and mlr.limit_type=? and date_format( ?,'%Y-%m-%d')>=mlr.effective_date and date_format( ?,'%Y-%m-%d') <= mlr.expiry_date");
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		param.add(customerId);
		param.add(type);
		param.add(currentDate);
		param.add(currentDate);
		List<MemberDto> dtoList = getDtoBySql(sql.toString(), param, MemberDto.class);
		if(dtoList != null && dtoList.size() >0){
			return dtoList.get(0);
		}
		return null;
	}
	
	@Override
	public MemberDto getMemberPurchaseLimit(Long customerId){
		String sql = "select l.num_value as creditLimit, l.expiry_date as expiryDate, c.available_balance as availableBalance, c.available_balance + l.num_value as remainSum, c.exchg_factor as exchgFactor "
				+ "from member_limit_rule l, member_cashvalue c where l.customer_id = ? and limit_type = ? and l.customer_id = c.customer_id";

		Member member = this.getMemberById(customerId);
		List<Serializable> param = new ArrayList<Serializable>();	
		if(member.getMemberType().equals("IPM")||member.getMemberType().equals("CPM")){		
			param.add(customerId);
		}else{	
			param.add(member.getSuperiorMemberId());			
		}	param.add("CR");
		List<MemberDto> dtos = getDtoBySql(sql, param, MemberDto.class);
		if(dtos != null && dtos.size() >0){
			
				return dtos.get(0);
			
		}
		return null;
	}

	@Override
	public MemberDto getMemberTransactionLimit(Long customerId){
		String sql = "select num_value as creditLimit from member_limit_rule where customer_id = ? and limit_type = ? and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date";
		List<Serializable> param = new ArrayList<Serializable>();	
		Date currentDate = new Date();
		param.add(customerId);
		param.add("TRN");
		param.add(currentDate);
		param.add(currentDate);
		List<MemberDto> dtos = getDtoBySql(sql, param, MemberDto.class);
		if(dtos != null && dtos.size() >0){
			
			return dtos.get(0);
		
	}
		return null;
	}

	@Override
	public Member getMemberByCustomerId(Long customerId)
			throws HibernateException {
		// TODO Auto-generated method stub
		
		String hqlstr  = " from Member m where m.customerId = ?";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(customerId);
		List<Member> resultList = getByHql(hqlstr, listParam);
		if(null == resultList || resultList.size() ==0 )
			return null;
		return resultList.get(0);
	}
	
	@Override
	public Member getMemberByUserId(String userId) throws HibernateException
	{
		String hqlstr = " from Member m where m.userId = ?";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(userId);

		return (Member) getUniqueByHql(hqlstr, listParam);

	}
	
	@Override
	public MemberCashvalueDto getMemberCashvlueInfo(String customerId) {
	    
	    
	    String sql = "select mb.customer_id as customerId, "
	    	+ "concat(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as customerName, "
	    	+ "mb.academy_no as academyId, "
	    	+ "ifnull(mc.available_balance, 0.00) as cashValue "
	    	+ "from member mb left join customer_profile cp on mb.customer_id = cp.customer_id "
	    	+ "left join member_cashvalue mc on mb.customer_id = mc.customer_id "
	    	+ "where mb.customer_id = " + customerId;
		    
	    List<MemberCashvalueDto> result = (List<MemberCashvalueDto>)getDtoBySql(sql, null, MemberCashvalueDto.class);
	    return result.size() > 0 ? result.get(0) : null;
	}
	
	@Override
	public ListPage<Member> getMemberList(ListPage<Member> page, String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType){
		
		String sql = "SELECT\n" +
				"			m.customer_id AS customerId,\n" +
				"			m.academy_no AS academyNo,\n" +
				"			CONCAT_WS(\n" +
				"				' ',\n" +
				"				c.salutation,\n" +
				"				c.given_name,\n" +
				"				c.surname\n" +
				"			) AS memberName,\n" +
			 // update memberName to firstName or lastName() by christ 2016-05-09
				"			c.given_name as firstName , c.surname as lastName ,\n"+
			    "			cpp.company_name as companyName, "+	
				"			c.nationality ,c.passport_no as passportNo,"+
			    "			DATE_FORMAT(m.effective_date,'%m-%d') AS effectiveDate,"+
				"			mlr.num_value as creditLimit, "+
			    "			m.create_by as userId ,"+
				"			m.member_type AS memberType,\n" +
				"			m. STATUS AS status,\n" +
				"			ce. STATUS AS enrollStatus,\n" +
				"			m.first_join_date AS firstJoinDate,\n" +
				"			c.phone_mobile AS mobilePhone,\n" +
				"			DATE_FORMAT(c.date_of_birth,'%m-%d')AS birthday,\n" +
				"			c.gender AS gender,\n" +
				"			c.contact_email AS contactEmail,\n" +
				"			cai.customer_input AS licensePlate,\n" +
				"			m.vip as vip,"+
				"			CASE m.member_type\n" +
				"		WHEN 'IPM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					inter.expiry_date\n" +
				"				FROM\n" +
				"					customer_service_acc inter\n" +
				"				WHERE\n" +
				"					inter.customer_id = m.customer_id\n" +
				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"				ORDER BY\n" +
				"					inter.expiry_date DESC\n" +
				"				LIMIT 0,\n" +
				"				1\n" +
				"			)\n" +
				"		WHEN 'CPM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					inter.expiry_date\n" +
				"				FROM\n" +
				"					customer_service_acc inter\n" +
				"				WHERE\n" +
				"					inter.customer_id = m.customer_id\n" +
				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"				ORDER BY\n" +
				"					inter.expiry_date DESC\n" +
				"				LIMIT 0,\n" +
				"				1\n" +
				"			)\n" +
				"		WHEN 'IDM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					inter.expiry_date AS expiryDate\n" +
				"				FROM\n" +
				"					customer_service_acc inter\n" +
				"				WHERE\n" +
				"					inter.customer_id = m.superior_member_id\n" +
				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"				ORDER BY\n" +
				"					inter.expiry_date DESC\n" +
				"				LIMIT 0,\n" +
				"				1\n" +
				"			)\n" +
				"		WHEN 'CDM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					inter.expiry_date AS expiryDate\n" +
				"				FROM\n" +
				"					customer_service_acc inter\n" +
				"				WHERE\n" +
				"					inter.customer_id = m.superior_member_id\n" +
				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"				ORDER BY\n" +
				"					inter.expiry_date DESC\n" +
				"				LIMIT 0,\n" +
				"				1\n" +
				"			)\n" +
				"		ELSE\n" +
				"			NULL\n" +
				"		END AS expiryDate,\n" +
				"		CASE m.member_type\n" +
				"	WHEN 'IPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.customer_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'CPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.customer_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'IDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.superior_member_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.superior_member_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	ELSE\n" +
				"		NULL\n" +
				"	END AS planName,\n" +
				"	CASE m.member_type\n" +
				"WHEN 'IPM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.customer_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'CPM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.customer_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'IDM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.superior_member_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'CDM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.superior_member_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"ELSE\n" +
				"	NULL\n" +
				"END AS planNo,\n" +
				" (SELECT   s.input_value AS inputValue FROM service_plan_addition_rule s WHERE s.right_code = 'G1' AND s.plan_no = planNo) AS dependentCreation " +
				"FROM\n" +
				"	member m "+
				// add Company Name to search
				" LEFT JOIN  member_limit_rule AS mlr ON m.customer_id=mlr.customer_id AND mlr.limit_type='CR' "+
				" LEFT JOIN corporate_member AS cpm ON m.customer_id =cpm.customer_id \n"+
				" LEFT JOIN corporate_profile AS cpp ON cpm.corporate_id=cpp.corporate_id \n"+
				",\n" +
				"	customer_profile c\n" +
				"LEFT JOIN customer_enrollment ce ON ce.customer_id = c.customer_id\n" +
				"LEFT JOIN customer_addition_info cai ON c.customer_id = cai.customer_id\n" +
				"AND cai.caption_id = 5\n" +
				"WHERE\n" +
				"	m.customer_id = c.customer_id\n" +
				"AND (\n" +
				"	(\n" +
				"		(\n" +
				"			m.member_type = 'CPM' || m.member_type = 'IPM'\n" +
				"		)\n" +
				"		AND (\n" +
				"			ce. STATUS = 'ANC'\n" +
				"			OR ce. STATUS = 'CMP'\n" +
				"		)\n" +
				"	)\n" +
				"	OR (\n" +
				"		(m.member_type = 'IDM' || m.member_type = 'CDM')  AND (c.is_deleted <> 'Y' AND m.superior_member_id IS NOT NULL)\n" +
				"	)\n" +
				")\n";
		
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		for(int i=0;i<12;i++){
			param.add(currentDate);	
		}
		
		
		if (status.equalsIgnoreCase("ALL")||status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)||status.equalsIgnoreCase(Constant.General_Status_EXP)) {
			if (status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)||status.equalsIgnoreCase(Constant.General_Status_EXP)) {
				sql = sql+" and m.status = '"+status+"'";
			}
		}
		
		if(!customerId.equalsIgnoreCase("ALL")){
			sql = sql+" and c.customer_id = "+customerId ;
		}
		
		sql = "select distinct t.customerId, t.academyNo, t.memberName, t.memberType, t.passportNo, t.status, t.enrollStatus, t.planNo,"
				+ "t.firstJoinDate, t.expiryDate, t.planName , t.mobilePhone, t.contactEmail, t.dependentCreation from " + "(" + sql + ") t where 1=1 ";
		
		if(!expiry.equalsIgnoreCase("ALL")){
			if(!expiry.equalsIgnoreCase("Today")&&!expiry.equalsIgnoreCase("EXP")){
				sql = sql + " and DATEDIFF(t.expiryDate,NOW()) <= " + expiry.replace("d", "") + " and DATEDIFF(t.expiryDate,NOW()) > 0" ;
			}else{
				if(expiry.equalsIgnoreCase("Today")){
					sql = sql + " and DATEDIFF(t.expiryDate,NOW()) = 0 ";	
				}else{
					sql = sql + " and DATEDIFF(t.expiryDate,NOW()) < 0";//
				}
				
			}
		}
		if(!StringUtils.isEmpty(planName)){
			if(planName.indexOf("'")>0){
				planName = planName.replace("'", "\\'");
			}
			sql = sql + " and t.planName =  '"+planName+"'";
			
		}
		if(!StringUtils.isEmpty(memberType)){
			sql = sql + " and t.memberType =  '"+memberType+"'";	
		}
		if(!StringUtils.isEmpty(page.getCondition())){
			sql = sql + " and " + page.getCondition();
		}
		
		StringBuilder orderBy = new StringBuilder();
		if(!StringUtils.isEmpty(sortBy)){
			String orderByFiled = sortBy.trim();
			orderBy.append(" order by ");
			if("true".equals(isAscending)){
				orderBy.append(orderByFiled).append(" asc ");
			}else{
				orderBy.append(orderByFiled).append(" desc ");
			}
		}
		sql = sql + orderBy.toString();
		
		String sqlCount = "select count(*) from " + "(" + sql + ") count";
		
		return listBySqlDto(page, sqlCount, sql, param, new DependentMemberDto());
	}
	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(List<SysCode> genderSysCodes, List<SysCode> memberStatusCodes){
		//displayName,columnName、columnType, ,displayOrder
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID","t.academyNo", String.class.getName(),"",1);
		
		final List<SysCode> memberType=new ArrayList<>();
		SysCode s1=new SysCode();
		s1.setCategory("permitCardmemberType");
		s1.setCodeDisplay("Corporate Dependent Member");
		s1.setCodeValue("CDM");
		memberType.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("permitCardmemberType");
		s2.setCodeDisplay("Corporate Primary Member");
		s2.setCodeValue("CPM");
		memberType.add(s2);
		
		SysCode s3=new SysCode();
		s3.setCategory("permitCardmemberType");
		s3.setCodeDisplay("Individual Primary Member");
		s3.setCodeValue("IPM");
		memberType.add(s3);
		
		SysCode s4=new SysCode();
		s4.setCategory("permitCardmemberType");
		s4.setCodeDisplay("Individual Dependent Member");
		s4.setCodeValue("IDM");
		memberType.add(s4);
		
		List<ServicePlan> planNameList = servicePlanDao.getByHql("FROM  ServicePlan where passNatureCode='LT' ");
		
		Collection<SysCode> planName = CollectionUtil.map(planNameList, new CallBack<ServicePlan, SysCode> (){

			@Override
			public SysCode execute(ServicePlan r, int index) {
				SysCode sysCode = new SysCode();
				sysCode.setCodeValue(r.getPlanName());
				sysCode.setCodeDisplay(r.getPlanName());
				return sysCode;
			}
			
		});
		
		
		List<SysCode> nationalitys=sysCodeDao.getSysCodeByCategory("nationality");
		
		
		List<DepartmentStaff> offerLists = servicePlanDao.getDtoBySql(" SELECT  CONCAT(s.given_name,' ',s.surname)AS createBy, staff_user_id AS staffUserId FROM department_staff  ds "
																		+ " LEFT JOIN staff_profile AS s ON ds.staff_user_id =s.user_id "+
																	"   WHERE depart_id =(SELECT db.depart_id FROM department_branch AS db WHERE db.status='ACT' AND depart_id=2) ", null, DepartmentStaff.class);
		Collection<SysCode> offers = CollectionUtil.map(offerLists, new CallBack<DepartmentStaff, SysCode> (){
			@Override
			public SysCode execute(DepartmentStaff r, int index) {
				SysCode sysCode = new SysCode();
				sysCode.setCodeValue(r.getStaffUserId());
				sysCode.setCodeDisplay(r.getCreateBy());
				return sysCode;
			}
		});
		
		final List<SysCode> vips=new ArrayList<>();
		SysCode sys1=new SysCode();
		sys1.setCategory("Y");
		sys1.setCodeDisplay("Y");
		sys1.setCodeValue("Y");
		vips.add(sys1);

		SysCode sys2=new SysCode();
		sys2.setCategory("N");
		sys2.setCodeDisplay("N");
		sys2.setCodeValue("N");
		vips.add(sys2);
		
		
		
		AdvanceQueryConditionDto condition11 = new AdvanceQueryConditionDto("Last Name","t.lastName", String.class.getName(),"",11);
		AdvanceQueryConditionDto condition12 = new AdvanceQueryConditionDto("First Name","t.firstName", String.class.getName(),"",12);
		AdvanceQueryConditionDto condition13 = new AdvanceQueryConditionDto("Company Name","t.companyName", String.class.getName(),"",13);
		AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Gender","t.gender", String.class.getName(),genderSysCodes,9);
		
		AdvanceQueryConditionDto condition14 = new AdvanceQueryConditionDto("Nationality","t.nationality", String.class.getName(),nationalitys,14);
		
		AdvanceQueryConditionDto condition15 = new AdvanceQueryConditionDto("HKID / Passport #","t.passportNo", String.class.getName(),"",15);
		AdvanceQueryConditionDto condition10 = new AdvanceQueryConditionDto("Email","t.contactEmail", String.class.getName(),"",10);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Birthday","t.birthday", Date.class.getName(),"",7);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Service Plan","t.planName", String.class.getName(),new ArrayList<SysCode>(planName),3);
		//member type update Patron Type
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Patron Type","t.memberType", String.class.getName(),memberType,4);
		
		//AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Member Name","t.memberName", String.class.getName(),"",2);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Status","t.status", String.class.getName(),memberStatusCodes,5);
		
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Mobile Phone","t.mobilePhone", String.class.getName(),"",6);
		AdvanceQueryConditionDto condition16 = new AdvanceQueryConditionDto("Activation Date","t.effectiveDate", Date.class.getName(),"",16);
		AdvanceQueryConditionDto condition17 = new AdvanceQueryConditionDto("Patronage Officer","t.userId", String.class.getName(),new ArrayList<SysCode>(offers),17);
		
		AdvanceQueryConditionDto condition18 = new AdvanceQueryConditionDto("Credit Limit","t.creditLimit", BigDecimal.class.getName(),"",18);
		
		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("License Plate","t.licensePlate", String.class.getName(),"",8);
		
		AdvanceQueryConditionDto condition19 = new AdvanceQueryConditionDto("VIP","t.vip", String.class.getName() ,vips,19);
		
		

		return  Arrays.asList(condition1,condition3,condition4,condition5,condition6,condition7,condition8,condition9,condition10,
				condition11,condition12,condition13,condition14,condition15,condition16,condition17,condition18,condition19);
	}
	

	/**
	 * @author Mianping_Wu
	 * @Date 8/4/2015
	 * @param customerId
	 */
	@Override
	public boolean isMemberActive(Long customerId) {
	    
	    String hql = "from Member m where m.status = 'ACT' and m.customerId = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(customerId);

	    Member member = (Member) getUniqueByHql(hql, params);
	    return (member == null ? false : true);
	}

	@Override
	public ListPage<Member> getRecentServedMemberList(ListPage<Member> page, String sortBy, String isAscending, Long[] customerId) {
		
		String sql = "SELECT DISTINCT" +
					" m.customer_id AS customerId," +
					" m.academy_no AS academyNo," +
					" CONCAT(c.salutation,' ',c.given_name,' ',c.surname) AS memberName," +
					" m.member_type AS memberType," +
					" m. STATUS AS status," +
					" m.first_join_date AS firstJoinDate," +
					" CASE m.member_type" +
					" WHEN 'IPM' THEN (SELECT acc.expiry_date FROM customer_service_acc acc WHERE acc.customer_id = m.customer_id AND acc. STATUS = 'ACT' )" +
					" WHEN 'CPM' THEN (SELECT coracc.expiry_date FROM corporate_service_acc coracc LEFT JOIN corporate_profile cp ON coracc.corporate_id = cp.corporate_id LEFT JOIN corporate_member cm ON cp.corporate_id = cm.corporate_id WHERE cm.customer_id = m.customer_id AND coracc. STATUS = 'ACT' )" +
					" WHEN 'IDM' THEN (SELECT acc.expiry_date FROM customer_service_acc acc WHERE acc.customer_id = m.superior_member_id and acc.status = 'ACT' )" +
					" WHEN 'CDM' THEN (SELECT coracc.expiry_date FROM corporate_service_acc coracc LEFT JOIN corporate_profile cp ON coracc.corporate_id = cp.corporate_id LEFT JOIN corporate_member cm ON cp.corporate_id = cm.corporate_id WHERE cm.customer_id = m.superior_member_id  and coracc.status = 'ACT' )" +
					" ELSE NULL" +
					" END AS expiryDate," +
					" CASE m.member_type" +
					" WHEN 'IPM' THEN (SELECT sp.plan_name FROM service_plan sp LEFT JOIN customer_service_subscribe css ON sp.plan_no = css.service_plan_no LEFT JOIN customer_service_acc csa ON css.acc_no = csa.acc_no WHERE csa.customer_id = m.customer_id AND csa. STATUS = 'ACT')" +
					" WHEN 'CPM' THEN (SELECT sp.plan_name FROM service_plan sp LEFT JOIN corporate_service_subscribe corss ON sp.plan_no = corss.service_plan_no LEFT JOIN corporate_service_acc corsa ON corss.acc_no = corsa.acc_no AND corsa. STATUS = 'ACT' LEFT JOIN corporate_profile corp ON corsa.corporate_id = corp.corporate_id LEFT JOIN corporate_member corm ON corp.corporate_id = corm.corporate_id WHERE corm.customer_id = m.customer_id )" + 
					" WHEN 'IDM' THEN (SELECT sp.plan_name FROM service_plan sp LEFT JOIN customer_service_subscribe css ON sp.plan_no = css.service_plan_no LEFT JOIN customer_service_acc csa ON css.acc_no = csa.acc_no WHERE csa.customer_id = m.superior_member_id and csa.status = 'ACT' )" +
					" WHEN 'CDM' THEN (SELECT sp.plan_name FROM service_plan sp LEFT JOIN corporate_service_subscribe corss ON sp.plan_no = corss.service_plan_no LEFT JOIN corporate_service_acc corsa ON corss.acc_no = corsa.acc_no and corsa.status = 'ACT' LEFT JOIN corporate_profile corp ON corsa.corporate_id = corp.corporate_id LEFT JOIN corporate_member corm ON corp.corporate_id = corm.corporate_id WHERE corm.customer_id = m.superior_member_id )" +
					" ELSE NULL" +
					" END AS planName" +
					" FROM" +
					" member m," +
					" member_limit_rule mlr," +
					" customer_profile c" +
					" LEFT JOIN customer_enrollment ce ON ce.customer_id = c.customer_id" +
					" AND ce. STATUS != 'APV'" +
					" AND ce. STATUS != 'OPN'" +
					" AND ce. STATUS != 'NEW'" +
					" LEFT JOIN (SELECT acc.acc_no, acc.customer_id, acc.expiry_date expiryDate FROM customer_service_acc acc WHERE acc.`status` = 'ACT' ) a ON c.customer_id = a.customer_id" +
					" LEFT JOIN corporate_member cm ON c.customer_id = cm.customer_id" +
					" LEFT JOIN ( SELECT cor.acc_no, cor.corporate_id, cor.expiry_date corExpiryDate FROM corporate_service_acc cor WHERE cor.`status` = 'ACT' ) cor ON cm.corporate_id = cor.corporate_id" +
					" LEFT JOIN corporate_profile ON cor.corporate_id = corporate_profile.corporate_id" +
					" LEFT JOIN corporate_service_acc csacc ON corporate_profile.corporate_id = csacc.corporate_id" +
					" LEFT JOIN corporate_service_subscribe cors ON csacc.acc_no = cors.acc_no" +
					" LEFT JOIN customer_service_subscribe css ON a.acc_no = css.acc_no" +
					" LEFT JOIN service_plan sp ON css.service_plan_no = sp.plan_no" +
					" OR cors.service_plan_no = sp.plan_no" +
					" WHERE" +
					" m.customer_id = c.customer_id" +
					" AND mlr.customer_id = c.customer_id ";
		
		sql = "select * from " + "(" + sql + ") t where 1=1 ";

		if (!StringUtils.isEmpty(page.getCondition())) {
			sql = sql + " and " + page.getCondition();
		}
		if (customerId != null) {
			sql += " and t.customerId in (" + org.apache.commons.lang.StringUtils.join(customerId, ",") + ")";
		}
		if (!StringUtils.isEmpty(sortBy)) {
			sql = sql + " ORDER BY t." + sortBy + ("true".equals(isAscending) ? " asc " : " desc ");
		}

		debugLog.info("getRecentServedMemberList sql:\n" + sql); // SAM 20161013 added: for error debug
		
		String sqlCount = "select count(*) from " + "(" + sql + ") count";
		List<Serializable> param = new ArrayList<Serializable>();
		return listBySqlDto(page, sqlCount, sql, param, new DependentMemberDto());
	}
	
	@Override
	public Date getExpiredDate(String customerId) {
		String act = Constant.Member_Status_ACT;
		String nact = Constant.Member_Status_NACT;
		String hql ="select max(csa.expiryDate)  from CustomerServiceAcc csa where csa.status in ('" +  act +  "','" + nact + "')";
		return (Date)getCurrentSession().createQuery(hql).setMaxResults(1).uniqueResult();
	}
	
	@Override
	public List<Member> getAllMembers() throws Exception {
		
		String sql = "select m.user_id userId, m.academy_no academyNo, m.customer_id customerId from member m, user_master u where u.user_id = m.user_id and u.user_type = 'CUSTOMER'";	
//		return (List<Member>)getByHql(hql);	
		return getDtoBySql(sql, null, Member.class);
	}

	@Override
	public Long getMemberCustomerId(String academyNO) {
	    
	    String hql = "select customerId from Member where academyNo = ?";
	    List<Serializable> param = new ArrayList<Serializable>();
	    param.add(academyNO);
	    Object result = getUniqueByHql(hql,param);
	    Long ret = null;
	    if (result != null) ret = ((Number) result).longValue();
	    return ret;
	}
	/**
	 * 根据customerId查询member
	 * @param ids
	 * @return
	 */
	@Override
	public List<Member> getMembersByCustomerId(String ids) throws Exception {
		
		String hql = "FROM Member  WHERE customerId IN(" + ids + ")";
		String sql = "SELECT user_id userId FROM member WHERE customer_id IN(" + ids + ")";
		return (List<Member>)getByHql(hql) ;	
//		return (List<String>)getDtoBySql(sql, null, new String().getClass());
	}

	@Override
	public List<GoingToAdultDependentMemberDto> getGoingToAdultMember(int preMonth) {
		// TODO Auto-generated method stub
		StringBuffer sb =new StringBuffer();
		sb.append(" select ");
		sb.append(" sp.user_id as userId, ");
		sb.append(" concat(sp.given_name, ' ', sp.surname) as salesName, ");
		sb.append(" concat(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as dependentName, ");
		sb.append(" concat(super.salutation, ' ', super.given_name, ' ', super.surname) as patronName, ");
		sb.append(" sp.contact_email salesPersonEmail, m.academy_no AS academyId,");
		sb.append(" date_add(cp.date_of_birth, interval 18 year) as inactiveDate ");
		sb.append(" from customer_profile cp,member m,customer_profile super,customer_enrollment ce,staff_profile sp ");
		sb.append(" where m.member_type in ('IDM','CDM') ");
		sb.append(" and m.relationship_code = 'CHD' ");
		sb.append(" and m.superior_member_id=super.customer_id ");
		sb.append(" and cp.customer_id=m.customer_id ");
		sb.append(" and super.customer_id = ce.customer_id ");
		sb.append(" and ce.sales_follow_by = sp.user_id ");
		sb.append(" and DATE_FORMAT(SYSDATE(),'%Y/%m/%d') = date_add(cp.date_of_birth, interval '<interval>' year_month) ");
		
		List<GoingToAdultDependentMemberDto> goingToAdultDependentMemberdtos = new ArrayList<GoingToAdultDependentMemberDto>(); 
		if(preMonth == 3){
			goingToAdultDependentMemberdtos = getDtoBySql(sb.toString().replaceAll("<interval>", "17.9"), null, GoingToAdultDependentMemberDto.class);
		}else if(preMonth == 1){
			goingToAdultDependentMemberdtos = getDtoBySql(sb.toString().replaceAll("<interval>", "17.11"), null, GoingToAdultDependentMemberDto.class);
		}else if(preMonth == 0){
			goingToAdultDependentMemberdtos = getDtoBySql(sb.toString().replaceAll("<interval>", "18.0"), null, GoingToAdultDependentMemberDto.class);
		}
		
		return goingToAdultDependentMemberdtos;
	}
	
	@Override
	public List<GoingToAdultDependentMemberDto> getExpiringMember(int preMonth) {
		// TODO Auto-generated method stub
		StringBuffer sb =new StringBuffer();
		sb.append(" select ");
		sb.append(" sp.user_id as userId, ");
		sb.append(" concat(sp.given_name, ' ', sp.surname) as saleName, ");
		sb.append(" concat(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as patronName, ");
		sb.append(" sp.contact_email salesPersonEmail, ");
		sb.append(" m.academy_no as academyId, ");
		sb.append(" acc.expiry_date as membershipExpireDate ");
		sb.append(" from customer_profile cp,member m,customer_enrollment ce,staff_profile sp,customer_service_acc acc ");
		sb.append(" where m.member_type in ('IPM','CPM') ");
		sb.append(" and cp.customer_id=m.customer_id ");
		sb.append(" and cp.customer_id = ce.customer_id ");
		sb.append(" and ce.sales_follow_by = sp.user_id ");
		sb.append(" and cp.customer_id = acc.customer_id ");
		sb.append(" and acc.`status` = 'ACT' ");
		sb.append(" and DATE_FORMAT(SYSDATE(),'%Y/%m/%d') = date_add(date_sub(acc.expiry_date, interval <interval> month), interval 1 day) ");
		
		List<GoingToAdultDependentMemberDto> goingToAdultDependentMemberdtos = new ArrayList<GoingToAdultDependentMemberDto>(); 
		if(preMonth == 3){
			goingToAdultDependentMemberdtos = getDtoBySql(sb.toString().replaceAll("<interval>", "3"), null, GoingToAdultDependentMemberDto.class);
		}else if(preMonth == 1){
			goingToAdultDependentMemberdtos = getDtoBySql(sb.toString().replaceAll("<interval>", "1"), null, GoingToAdultDependentMemberDto.class);
		}else if(preMonth == 0){
			goingToAdultDependentMemberdtos = getDtoBySql(sb.toString().replaceAll("<interval>", "0"), null, GoingToAdultDependentMemberDto.class);
		}
		
		return goingToAdultDependentMemberdtos;
	}
	
	/**
	 * 获取当天或30天后成年的子会员
	 * @return
	 */
	@Override
	public List<AdultDependentMemberDto> getAdultDependent(){
		String sql = " SELECT " +
				"	m.academy_no AS academyNo,\n" +
				"	(SELECT c.contact_email AS email FROM customer_profile c WHERE c.customer_id = m.superior_member_id) AS superiorMemberEmail,\n" +
				"	CONCAT_WS(\n" +
				"		' ',\n" +
				"		cp.salutation,\n" +
				"		cp.given_name,\n" +
				"		cp.surname\n" +
				"	) AS memberName\n" +
				" FROM customer_profile cp " +
				" LEFT JOIN member m ON m.customer_id = cp.customer_id  " +
				" WHERE  m.relationship_code = 'CHD' AND  " +
				" (DATE_FORMAT(SYSDATE(),'%Y/%m/%d') =  date_add(cp.date_of_birth, interval 18 year)  " +
				" OR DATE_FORMAT(SYSDATE(),'%Y/%m/%d') =  date_add(date_add(cp.date_of_birth, interval 18 year), interval 30 DAY)) ";
		return getDtoBySql(sql, null, AdultDependentMemberDto.class);
	}
	
	
	/**
	 * 查询所有未同步的member列表
	 * @return
	 */
	@Override
	public List<AdultDependentMemberDto> getSyncFailureMemberList(String academyNos) throws Exception {
		
		String sql = " SELECT m.academy_no AS academyNo, m.customer_id AS customerId FROM member m left join spa_member_sync s ON s.customer_id = m.customer_id WHERE m.`status` = 'ACT' AND m.member_type NOT IN ('MG', 'HG') AND s.customer_id is NULL ";
		if (null != academyNos && !academyNos.isEmpty()) {
			sql = sql + " AND m.academy_no IN (" + academyNos.toString() + ")";
		}
		return (List<AdultDependentMemberDto>)getDtoBySql(sql, null, AdultDependentMemberDto.class);	
	}
	
	/**
	 * 获取需要更新同步的Member列表
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<AdultDependentMemberDto> getNeedUpdateSyncMember() throws Exception {
		
		String sql = "  SELECT m.academy_no AS academyNo, m.customer_id AS customerId FROM member m left join spa_member_sync s ON s.customer_id = m.customer_id WHERE m.`status` = 'ACT' AND m.member_type NOT IN ('MG', 'HG') AND s.customer_id IS NOT NULL GROUP BY s.customer_id";
		return (List<AdultDependentMemberDto>)getDtoBySql(sql, null, AdultDependentMemberDto.class);		
	}
	
	/**
	 * 获取激活/非激活的Member数
	 * @return
	 * @throws Exception
	 */
	@Override
	public Integer getStatisticsActivationOrInactiveMembersNum(boolean isActivation) throws Exception {
		
		String sql = "  SELECT count(m.customer_id) FROM member m  WHERE m.`status` = ? AND m.member_type NOT IN ('MG', 'HG')  ";
		 List<Serializable> param = new ArrayList<Serializable>();
		 String act  = "NACT";
		 if (isActivation) {
			 act  = "ACT";
		}
		param.add(act);
		BigInteger num = (BigInteger)getUniqueBySQL(sql, param);
		return Integer.valueOf(num +"");		
	}
	
	/**
	 * 根据academyNos获取对应的member列表
	 * @param academyNos   多个academyNo,号分割
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<AdultDependentMemberDto> getMemberListByacademyNos(String academyNos) throws Exception {
		String[] arr = academyNos.split(",");
		StringBuilder sb = new  StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append("'");
			sb.append(arr[i]);
			sb.append("'");
		}
		
		String sql = "SELECT m.academy_no AS academyNo, m.customer_id AS customerId FROM member m  WHERE m.academy_no IN (" + sb.toString() + ") ";
		return getDtoBySql(sql, null, AdultDependentMemberDto.class);	

	}
	
	/**
	 * 获取当月生日会员列表
	 * @param page
	 * @param sortBy
	 * @param isAscending
	 * @param customerId
	 * @param status
	 * @param expiry
	 * @param planName
	 * @param memberType
	 * @param month
	 * @return
	 */
	@Override
	public ListPage<Member> getMemberList(ListPage<Member> page, String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType, Integer month){
		
		String sql = "SELECT\n" +
				"			m.customer_id AS customerId,\n" +
				"			m.academy_no AS academyNo,\n" +
				"			CONCAT_WS(\n" +
				"				' ',\n" +
				"				c.salutation,\n" +
				"				c.given_name,\n" +
				"				c.surname\n" +
				"			) AS memberName,\n" +
			 // update memberName to firstName or lastName() by christ 2016-05-09
				"			c.given_name as firstName , c.surname as lastName ,\n"+
//			    "			cpp.company_name as companyName, "+	
//				"			c.nationality ,c.passport_no as passportNo,"+
//			    "			DATE_FORMAT(m.effective_date,'%m-%d') AS effectiveDate,"+
//				"			mlr.num_value as creditLimit, "+
			    "			m.create_by as userId ,"+
				"			m.member_type AS memberType,\n" +
				"			m. STATUS AS status,\n" +
//				"			ce. STATUS AS enrollStatus,\n" +
//				"			m.first_join_date AS firstJoinDate,\n" +
				"			c.phone_mobile AS mobilePhone,\n" +
				"			DATE_FORMAT(c.date_of_birth,'%b-%d')AS birthday,\n" +
				"			c.gender AS gender,\n" +
//				"			c.contact_email AS contactEmail,\n" +
//				"			cai.customer_input AS licensePlate,\n" +
//				"			m.vip as vip,"+
//				"			CASE m.member_type\n" +
//				"		WHEN 'IPM' THEN\n" +
//				"			(\n" +
//				"				SELECT\n" +
//				"					inter.expiry_date\n" +
//				"				FROM\n" +
//				"					customer_service_acc inter\n" +
//				"				WHERE\n" +
//				"					inter.customer_id = m.customer_id\n" +
//				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
//				"				ORDER BY\n" +
//				"					inter.expiry_date DESC\n" +
//				"				LIMIT 0,\n" +
//				"				1\n" +
//				"			)\n" +
//				"		WHEN 'CPM' THEN\n" +
//				"			(\n" +
//				"				SELECT\n" +
//				"					inter.expiry_date\n" +
//				"				FROM\n" +
//				"					customer_service_acc inter\n" +
//				"				WHERE\n" +
//				"					inter.customer_id = m.customer_id\n" +
//				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
//				"				ORDER BY\n" +
//				"					inter.expiry_date DESC\n" +
//				"				LIMIT 0,\n" +
//				"				1\n" +
//				"			)\n" +
//				"		WHEN 'IDM' THEN\n" +
//				"			(\n" +
//				"				SELECT\n" +
//				"					inter.expiry_date AS expiryDate\n" +
//				"				FROM\n" +
//				"					customer_service_acc inter\n" +
//				"				WHERE\n" +
//				"					inter.customer_id = m.superior_member_id\n" +
//				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
//				"				ORDER BY\n" +
//				"					inter.expiry_date DESC\n" +
//				"				LIMIT 0,\n" +
//				"				1\n" +
//				"			)\n" +
//				"		WHEN 'CDM' THEN\n" +
//				"			(\n" +
//				"				SELECT\n" +
//				"					inter.expiry_date AS expiryDate\n" +
//				"				FROM\n" +
//				"					customer_service_acc inter\n" +
//				"				WHERE\n" +
//				"					inter.customer_id = m.superior_member_id\n" +
//				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
//				"				ORDER BY\n" +
//				"					inter.expiry_date DESC\n" +
//				"				LIMIT 0,\n" +
//				"				1\n" +
//				"			)\n" +
//				"		ELSE\n" +
//				"			NULL\n" +
//				"		END AS expiryDate,\n" +
				"		CASE m.member_type\n" +
				"	WHEN 'IPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.customer_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'CPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.customer_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'IDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.superior_member_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.superior_member_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	ELSE\n" +
				"		NULL\n" +
				"	END AS planName,\n" +
				"	CASE m.member_type\n" +
				"WHEN 'IPM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.customer_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'CPM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.customer_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'IDM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.superior_member_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'CDM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.superior_member_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"ELSE\n" +
				"	NULL\n" +
				"END AS planNo \n" +
//				" (SELECT   s.input_value AS inputValue FROM service_plan_addition_rule s WHERE s.right_code = 'G1' AND s.plan_no = planNo) AS dependentCreation " +
				"FROM\n" +
				"	member m "+
				// add Company Name to search
//				" LEFT JOIN  member_limit_rule AS mlr ON m.customer_id=mlr.customer_id AND mlr.limit_type='CR' "+
//				" LEFT JOIN corporate_member AS cpm ON m.customer_id =cpm.customer_id \n"+
//				" LEFT JOIN corporate_profile AS cpp ON cpm.corporate_id=cpp.corporate_id \n"+
				",\n" +
				"	customer_profile c\n" +
				"LEFT JOIN customer_enrollment ce ON ce.customer_id = c.customer_id\n" +
//				"LEFT JOIN customer_addition_info cai ON c.customer_id = cai.customer_id\n" +
//				"AND cai.caption_id = 5\n" +
				"WHERE\n" +
				"	m.customer_id = c.customer_id\n" +
				"AND (\n" +
				"	(\n" +
				"		(\n" +
				"			m.member_type = 'CPM' || m.member_type = 'IPM'\n" +
				"		)\n" +
				"		AND (\n" +
				"			ce. STATUS = 'ANC'\n" +
				"			OR ce. STATUS = 'CMP'\n" +
				"		)\n" +
				"	)\n" +
				"	OR (\n" +
				"		(m.member_type = 'IDM' || m.member_type = 'CDM')  AND (c.is_deleted <> 'Y' AND m.superior_member_id IS NOT NULL)\n" +
				"	)\n" +
				") AND DATE_FORMAT(c.date_of_birth,'%m') = ? \n";
		
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		for(int i=0;i<8;i++){
			param.add(currentDate);	
		}
		param.add(month);	
		
//		if (status.equalsIgnoreCase("ALL")||status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)||status.equalsIgnoreCase(Constant.General_Status_EXP)) {
//			if (status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)||status.equalsIgnoreCase(Constant.General_Status_EXP)) {
//				sql = sql+" and m.status = '"+status+"'";
//			}
//		}
		
//		if(!customerId.equalsIgnoreCase("ALL")){
//			sql = sql+" and c.customer_id = "+customerId ;
//		}
		
		sql = "select distinct t.customerId, t.academyNo, t.memberName, t.memberType, t.status, t.birthday, t.planNo,"
				+ " t.planName , t.mobilePhone   from " + "(" + sql + ") t where 1=1 ";
		
//		if(!expiry.equalsIgnoreCase("ALL")){
//			if(!expiry.equalsIgnoreCase("Today")&&!expiry.equalsIgnoreCase("EXP")){
//				sql = sql + " and DATEDIFF(t.expiryDate,NOW()) <= " + expiry.replace("d", "") + " and DATEDIFF(t.expiryDate,NOW()) > 0" ;
//			}else{
//				if(expiry.equalsIgnoreCase("Today")){
//					sql = sql + " and DATEDIFF(t.expiryDate,NOW()) = 0 ";	
//				}else{
//					sql = sql + " and DATEDIFF(t.expiryDate,NOW()) < 0";//
//				}
//				
//			}
//		}
		if(!StringUtils.isEmpty(planName)){
			if(planName.indexOf("'")>0){
				planName = planName.replace("'", "\\'");
			}
			sql = sql + " and t.planName =  '"+planName+"'";
			
		}
		if(!StringUtils.isEmpty(memberType)){
			sql = sql + " and t.memberType =  '"+memberType+"'";	
		}
		if(!StringUtils.isEmpty(page.getCondition())){
			sql = sql + " and " + page.getCondition();
		}
		
		StringBuilder orderBy = new StringBuilder();
		if(!StringUtils.isEmpty(sortBy)){
			String orderByFiled = sortBy.trim();
			orderBy.append(" order by ");
			if("true".equals(isAscending)){
				orderBy.append(orderByFiled).append(" asc ");
			}else{
				orderBy.append(orderByFiled).append(" desc ");
			}
		}
		sql = sql + orderBy.toString();
		
		String sqlCount = "select count(*) from " + "(" + sql + ") count";
		
		return listBySqlDto(page, sqlCount, sql, param, new DependentMemberDto());
	}
	
	
	/**
	 * 获取当月生日会员列表 【无分页】
	 * @param page
	 * @param sortBy
	 * @param isAscending
	 * @param customerId
	 * @param status
	 * @param expiry
	 * @param planName
	 * @param memberType
	 * @param month
	 * @return
	 */
	@Override
	public List<DependentMemberDto> getMemberList( String sortBy, String isAscending, String customerId, String status, String expiry,String planName,String memberType, Integer month){
		
		String sql = "SELECT\n" +
				"			m.customer_id AS customerId,\n" +
				"			m.academy_no AS academyNo,\n" +
				"			CONCAT_WS(\n" +
				"				' ',\n" +
				"				c.salutation,\n" +
				"				c.given_name,\n" +
				"				c.surname\n" +
				"			) AS memberName,\n" +
			 // update memberName to firstName or lastName() by christ 2016-05-09
				"			c.given_name as firstName , c.surname as lastName ,\n"+
//			    "			cpp.company_name as companyName, "+	
//				"			c.nationality ,c.passport_no as passportNo,"+
//			    "			DATE_FORMAT(m.effective_date,'%m-%d') AS effectiveDate,"+
//				"			mlr.num_value as creditLimit, "+
			    "			m.create_by as userId ,"+
				"			m.member_type AS memberType,\n" +
				"			m. STATUS AS status,\n" +
//				"			ce. STATUS AS enrollStatus,\n" +
//				"			m.first_join_date AS firstJoinDate,\n" +
				"			c.phone_mobile AS mobilePhone,\n" +
				"			DATE_FORMAT(c.date_of_birth,'%b-%d')AS birthday,\n" +
				"			c.gender AS gender,\n" +
//				"			c.contact_email AS contactEmail,\n" +
//				"			cai.customer_input AS licensePlate,\n" +
//				"			m.vip as vip,"+
//				"			CASE m.member_type\n" +
//				"		WHEN 'IPM' THEN\n" +
//				"			(\n" +
//				"				SELECT\n" +
//				"					inter.expiry_date\n" +
//				"				FROM\n" +
//				"					customer_service_acc inter\n" +
//				"				WHERE\n" +
//				"					inter.customer_id = m.customer_id\n" +
//				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
//				"				ORDER BY\n" +
//				"					inter.expiry_date DESC\n" +
//				"				LIMIT 0,\n" +
//				"				1\n" +
//				"			)\n" +
//				"		WHEN 'CPM' THEN\n" +
//				"			(\n" +
//				"				SELECT\n" +
//				"					inter.expiry_date\n" +
//				"				FROM\n" +
//				"					customer_service_acc inter\n" +
//				"				WHERE\n" +
//				"					inter.customer_id = m.customer_id\n" +
//				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
//				"				ORDER BY\n" +
//				"					inter.expiry_date DESC\n" +
//				"				LIMIT 0,\n" +
//				"				1\n" +
//				"			)\n" +
//				"		WHEN 'IDM' THEN\n" +
//				"			(\n" +
//				"				SELECT\n" +
//				"					inter.expiry_date AS expiryDate\n" +
//				"				FROM\n" +
//				"					customer_service_acc inter\n" +
//				"				WHERE\n" +
//				"					inter.customer_id = m.superior_member_id\n" +
//				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
//				"				ORDER BY\n" +
//				"					inter.expiry_date DESC\n" +
//				"				LIMIT 0,\n" +
//				"				1\n" +
//				"			)\n" +
//				"		WHEN 'CDM' THEN\n" +
//				"			(\n" +
//				"				SELECT\n" +
//				"					inter.expiry_date AS expiryDate\n" +
//				"				FROM\n" +
//				"					customer_service_acc inter\n" +
//				"				WHERE\n" +
//				"					inter.customer_id = m.superior_member_id\n" +
//				"				AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
//				"				ORDER BY\n" +
//				"					inter.expiry_date DESC\n" +
//				"				LIMIT 0,\n" +
//				"				1\n" +
//				"			)\n" +
//				"		ELSE\n" +
//				"			NULL\n" +
//				"		END AS expiryDate,\n" +
				"		CASE m.member_type\n" +
				"	WHEN 'IPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.customer_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'CPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.customer_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'IDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.superior_member_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				plan.plan_name\n" +
				"			FROM\n" +
				"				customer_service_acc inter,\n" +
				"				customer_service_subscribe sub,\n" +
				"				service_plan plan\n" +
				"			WHERE\n" +
				"				sub.acc_no = inter.acc_no\n" +
				"			AND plan.plan_no = sub.service_plan_no\n" +
				"			AND inter.customer_id = m.superior_member_id\n" +
				"			AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"			ORDER BY\n" +
				"				inter.expiry_date DESC\n" +
				"			LIMIT 0,\n" +
				"			1\n" +
				"		)\n" +
				"	ELSE\n" +
				"		NULL\n" +
				"	END AS planName,\n" +
				"	CASE m.member_type\n" +
				"WHEN 'IPM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.customer_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'CPM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.customer_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'IDM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.superior_member_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"WHEN 'CDM' THEN\n" +
				"	(\n" +
				"		SELECT\n" +
				"			sub.service_plan_no\n" +
				"		FROM\n" +
				"			customer_service_acc inter,\n" +
				"			customer_service_subscribe sub\n" +
				"		WHERE\n" +
				"			sub.acc_no = inter.acc_no\n" +
				"		AND inter.customer_id = m.superior_member_id\n" +
				"		AND date_format(?, '%Y-%m-%d') >= inter.effective_date\n" +
				"		ORDER BY\n" +
				"			inter.expiry_date DESC\n" +
				"		LIMIT 0,\n" +
				"		1\n" +
				"	)\n" +
				"ELSE\n" +
				"	NULL\n" +
				"END AS planNo \n" +
//				" (SELECT   s.input_value AS inputValue FROM service_plan_addition_rule s WHERE s.right_code = 'G1' AND s.plan_no = planNo) AS dependentCreation " +
				"FROM\n" +
				"	member m "+
				// add Company Name to search
//				" LEFT JOIN  member_limit_rule AS mlr ON m.customer_id=mlr.customer_id AND mlr.limit_type='CR' "+
//				" LEFT JOIN corporate_member AS cpm ON m.customer_id =cpm.customer_id \n"+
//				" LEFT JOIN corporate_profile AS cpp ON cpm.corporate_id=cpp.corporate_id \n"+
				",\n" +
				"	customer_profile c\n" +
				"LEFT JOIN customer_enrollment ce ON ce.customer_id = c.customer_id\n" +
//				"LEFT JOIN customer_addition_info cai ON c.customer_id = cai.customer_id\n" +
//				"AND cai.caption_id = 5\n" +
				"WHERE\n" +
				"	m.customer_id = c.customer_id\n" +
				"AND (\n" +
				"	(\n" +
				"		(\n" +
				"			m.member_type = 'CPM' || m.member_type = 'IPM'\n" +
				"		)\n" +
				"		AND (\n" +
				"			ce. STATUS = 'ANC'\n" +
				"			OR ce. STATUS = 'CMP'\n" +
				"		)\n" +
				"	)\n" +
				"	OR (\n" +
				"		(m.member_type = 'IDM' || m.member_type = 'CDM')  AND (c.is_deleted <> 'Y' AND m.superior_member_id IS NOT NULL)\n" +
				"	)\n" +
				") AND DATE_FORMAT(c.date_of_birth,'%m') = ? \n";
		
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		for(int i=0;i<8;i++){
			param.add(currentDate);	
		}
		param.add(month);	
		
//		if (status.equalsIgnoreCase("ALL")||status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)||status.equalsIgnoreCase(Constant.General_Status_EXP)) {
//			if (status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)||status.equalsIgnoreCase(Constant.General_Status_EXP)) {
//				sql = sql+" and m.status = '"+status+"'";
//			}
//		}
		
//		if(!customerId.equalsIgnoreCase("ALL")){
//			sql = sql+" and c.customer_id = "+customerId ;
//		}
		
		sql = "select distinct t.customerId, t.academyNo, t.memberName, t.memberType, t.status, t.birthday, t.planNo,"
				+ " t.planName , t.mobilePhone   from " + "(" + sql + ") t where 1=1 ";
		
//		if(!expiry.equalsIgnoreCase("ALL")){
//			if(!expiry.equalsIgnoreCase("Today")&&!expiry.equalsIgnoreCase("EXP")){
//				sql = sql + " and DATEDIFF(t.expiryDate,NOW()) <= " + expiry.replace("d", "") + " and DATEDIFF(t.expiryDate,NOW()) > 0" ;
//			}else{
//				if(expiry.equalsIgnoreCase("Today")){
//					sql = sql + " and DATEDIFF(t.expiryDate,NOW()) = 0 ";	
//				}else{
//					sql = sql + " and DATEDIFF(t.expiryDate,NOW()) < 0";//
//				}
//				
//			}
//		}
		if(!StringUtils.isEmpty(planName)){
			if(planName.indexOf("'")>0){
				planName = planName.replace("'", "\\'");
			}
			sql = sql + " and t.planName =  '"+planName+"'";
			
		}
		if(!StringUtils.isEmpty(memberType)){
			sql = sql + " and t.memberType =  '"+memberType+"'";	
		}
		
		StringBuilder orderBy = new StringBuilder();
		if(!StringUtils.isEmpty(sortBy)){
			String orderByFiled = sortBy.trim();
			orderBy.append(" order by ");
			if("true".equals(isAscending)){
				orderBy.append(orderByFiled).append(" asc ");
			}else{
				orderBy.append(orderByFiled).append(" desc ");
			}
		}
		sql = sql + orderBy.toString();
		
		
		return getDtoBySql( sql, param, DependentMemberDto.class);
	}
	
}	
