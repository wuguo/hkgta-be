package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilityAdditionAttribute;

@Repository
public class FacilityAdditionAttributeDaoImpl extends GenericDao<FacilityAdditionAttribute>  implements FacilityAdditionAttributeDao{

	@Override
	public FacilityAdditionAttribute getFacilityAdditionAttribute(Long facilityNo, String attributeId)
	{
		StringBuffer hql = new StringBuffer("from FacilityAdditionAttribute  faa where faa.id.facilityNo = ? and  faa.id.attributeId  like ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityNo);
		paramList.add(attributeId + "%");
		List<FacilityAdditionAttribute> facilityAdditionAttribute = super.getByHql(hql.toString(), paramList);
		if (null != facilityAdditionAttribute && facilityAdditionAttribute.size() > 0)
			return facilityAdditionAttribute.get(0);
		return null;
	}

	@Override
	public FacilityAdditionAttribute getFacilityAdditionAttribute(Long facilityNo)
	{
		StringBuffer hql = new StringBuffer("from FacilityAdditionAttribute faa where faa.id.facilityNo = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityNo);
		List<FacilityAdditionAttribute> facilityAdditionAttribute = super.getByHql(hql.toString(), paramList);
		if (null != facilityAdditionAttribute && facilityAdditionAttribute.size() > 0)
			return facilityAdditionAttribute.get(0);
		return null;
	}

	@Override
	public List<FacilityAdditionAttribute> getFacilityAdditionAttributeList(String attributeId)
	{
		StringBuffer hql = new StringBuffer("from FacilityAdditionAttribute faa where faa.id.attributeId = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(attributeId);
		return super.getByHql(hql.toString(), paramList);
	}

}
