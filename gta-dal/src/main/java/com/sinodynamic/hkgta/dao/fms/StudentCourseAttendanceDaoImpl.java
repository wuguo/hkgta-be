package com.sinodynamic.hkgta.dao.fms;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.StudentCourseAttendance;

@Repository
public class StudentCourseAttendanceDaoImpl extends GenericDao<StudentCourseAttendance> implements StudentCourseAttendanceDao
{

	@Override
	public String getAttendIdBySessionIdAndEnrollid(String sessionId, String enrollId) {
		    	if (StringUtils.isEmpty(sessionId) || StringUtils.isEmpty(enrollId)) 
			    		return null;
					String sql = "SELECT attend_id FROM student_course_attendance t WHERE t.course_session_id=? AND t.enroll_id=?";
				    List<Serializable> params = new ArrayList<Serializable>();
				    params.add(sessionId);
				    params.add(enrollId);
					Object object = getUniqueBySQL(sql, params);
					String objectStr =  String.valueOf(object).replace("null", "");
					String result = StringUtils.isBlank( objectStr)?null:objectStr;
					return result;
	}

	

}
