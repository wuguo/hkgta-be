package com.sinodynamic.hkgta.dao.adm;

import java.util.Date;
import java.util.List;


import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.adm.UserRecordActionLog;

/**
 * @author christ
 * 
 * @since OCT 10 2016
 *
 */
public interface UserRecordActionLogDao extends IBaseDao<UserRecordActionLog>
{
	public List<UserRecordActionLog> getUserRecordActionLogByDate(Date actionDate);
}
