package com.sinodynamic.hkgta.dao.carpark;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.carpark.CarParkMemberCheckDto;

public interface CarParkMemberDao extends IBaseDao{
	
	public List<CarParkMemberCheckDto> getCarParkMemberList();
	public List<CarParkMemberCheckDto> getCarParkStaffList();

}

