package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.FacilityTypeQuotaDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTypeQuota;

@Repository
public class FacilityTypeQuotaDaoImpl extends GenericDao<FacilityTypeQuota> implements FacilityTypeQuotaDao {
	
	@Override
	public List<FacilityTypeQuotaDto> getFacilityTypeQuotaList(String hql, List<Serializable> params) {
		return super.getDtoByHql(hql, params, FacilityTypeQuotaDto.class);
	}

	@Override
	public FacilityTypeQuota getFacilityTypeQuota(String facilityType, Date specificDate,String subtypeId)
	{
		String hql = "from FacilityTypeQuota where upper(facilityTypeCode) = ? and date(specificDate) = date(?)";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(specificDate);
		if(!StringUtils.isEmpty(subtypeId)){
			hql +=" and subtypeId =? ";
			paramList.add(subtypeId);
		}
		return (FacilityTypeQuota) super.getUniqueByHql(hql, paramList);
	}
}
