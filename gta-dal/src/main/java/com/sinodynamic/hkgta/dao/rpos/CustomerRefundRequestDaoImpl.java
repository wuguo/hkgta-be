package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.DaypassRefundDto;
import com.sinodynamic.hkgta.dto.crm.SpaRefundDto;
import com.sinodynamic.hkgta.dto.crm.WellnessRefundDto;
import com.sinodynamic.hkgta.dto.fms.CustomerRefundRequestDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;
import com.sinodynamic.hkgta.dto.rpos.PaymentInfoDto;
import com.sinodynamic.hkgta.dto.rpos.RefundInfoDto;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.rpos.CustomerRefundRequest;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;
import com.sinodynamic.hkgta.util.constant.FacilitySubType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.ReservationType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
@Repository
public class CustomerRefundRequestDaoImpl extends
		GenericDao<CustomerRefundRequest> implements CustomerRefundRequestDao {

	String getCountSql(String sql){
		return "SELECT COUNT(*) FROM (" + sql +") sub";
	}
	
	@Override
	public ListPage<CustomerRefundRequest> getCashValueRefundList(
			Date base, Integer days,String status, Long customerId, String orderColumn, String order, int pageSize,
			int currentPage, String filterSQL) {
		ListPage<CustomerRefundRequest> pageParam = new ListPage<CustomerRefundRequest>(pageSize, currentPage);
		
		if(order == null) {
			order = "asc";
		}
		if(order.equals("asc")){
			pageParam.addAscending(orderColumn);
		}
		if(order.equals("desc")){
			pageParam.addDescending(orderColumn);
		}
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("refundId", LongType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("requestDate", TimestampType.INSTANCE);
		typeMap.put("refundTransactionNo", LongType.INSTANCE);
		typeMap.put("refundFrom", StringType.INSTANCE);
		typeMap.put("memberName", StringType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("paidAmt", BigDecimalType.INSTANCE);
		typeMap.put("academyNo", StringType.INSTANCE);
		typeMap.put("requestAmt", BigDecimalType.INSTANCE);
		typeMap.put("approvedAmt", BigDecimalType.INSTANCE);
		typeMap.put("approvedBy", StringType.INSTANCE);
		typeMap.put("remark", StringType.INSTANCE);
		typeMap.put("refundMethod", StringType.INSTANCE);
		typeMap.put("refNo", StringType.INSTANCE);
		
		
		
		String sql = "SELECT \n" +
		        "    refundId,\n" +
				"    customerId,\n" +
				"    requestDate,\n" +
				"    refundTransactionNo,\n" +
				"    refundMethod,\n" +
				"    refundFrom,\n" +
				"    memberName,\n" +
				"    status,\n" +
				"    paidAmt,\n" +
				"    refNo,\n" +
				"    academyNo,\n"+
				"    requestAmt,\n" +
				"    approvedAmt,\n" +
				"    approvedBy,\n" +
				"    remark\n" +
				"FROM\n" +
				"    (SELECT \n" +
				"            req.refund_id refundId,\n" +
				"            hd.customer_id customerId,\n" +
				"            req.create_date requestDate,\n" +
				"            req.refund_transaction_no refundTransactionNo,\n" +
				"            req.refund_money_type refundMethod,\n" +
				"            req.refund_service_type refundFrom,\n" +
				"            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) memberName,\n" +
				"            req.status,\n" +
				"            tran.paid_amount paidAmt,\n" +
				"            tran.agent_transaction_no refNo,\n" +
				"            mem.academy_no academyNo,\n" +
				"            req.request_amt requestAmt,\n" +
				"            req.approved_amt approvedAmt,\n" +
				"            (select concat(staff.given_name, ' ', staff.surname) from staff_profile staff where staff.user_id = req.auditor_user_id) approvedBy,\n" +
				"            req.internal_remark remark\n" +
				"    FROM\n" +
				"        customer_refund_request req\n" +
				"    JOIN customer_order_trans tran ON tran.transaction_no = req.refund_transaction_no\n" +
				"    JOIN customer_order_hd hd ON hd.order_no = tran.order_no\n" +
				"    LEFT JOIN member mem ON mem.customer_id = hd.customer_id\n" +
				"    JOIN customer_profile pro ON pro.customer_id = mem.customer_id\n" +
				"    WHERE req.refund_service_type = 'CVL' <CUSTOMER_ID> <STATUS_CONDITION> <PERIOD_CONDITION>\n" +
				") sub\n";
		
		if(!StringUtils.isEmpty(filterSQL)){
			sql = sql + "WHERE " + filterSQL;
		}
		
		List<Serializable> params = new ArrayList<Serializable>(); 
		
		if(customerId != null){
			sql = sql.replaceFirst("<CUSTOMER_ID>", "AND hd.customer_id = ?");
			params.add(customerId);
		}else{
			sql = sql.replaceFirst("<CUSTOMER_ID>", "");
		}
		
		if(StringUtils.isNotEmpty(status) && !(StringUtils.equalsIgnoreCase("all", status) || StringUtils.equalsIgnoreCase("*", status))){
			sql = sql.replaceFirst("<STATUS_CONDITION>", "AND req.status = ?");
			params.add(status);
		}else{
			sql = sql.replaceFirst("<STATUS_CONDITION>", "AND req.status <> 'PND'");
		}
		
		if(days != null && days > 0 && base != null){
			DateTime beginOfDay = new DateTime(base);
			DateTime endOfDay = new DateTime(base);
			beginOfDay = beginOfDay.millisOfDay().withMinimumValue();
			endOfDay = endOfDay.millisOfDay().withMaximumValue();
			
			String where = "AND req.create_date BETWEEN ? - INTERVAL ? DAY AND ?";
			sql = sql.replaceFirst("<PERIOD_CONDITION>", where);
			params.addAll(Arrays.asList(beginOfDay.toLocalDateTime().toDate(), days-1, endOfDay.toLocalDateTime().toDate()));
			
			return this.listBySqlDto(
					pageParam,
					getCountSql(sql),
					sql,
					params,
					new CustomerRefundRequestDto(),
					typeMap);
		}else{
			
			sql = sql.replaceFirst("<PERIOD_CONDITION>", "");

			return this.listBySqlDto(
					pageParam,
					getCountSql(sql),
					sql,
					params,
					new CustomerRefundRequestDto(),
					typeMap);
		}
	}
	
	@Override
	public ListPage<CustomerRefundRequest> getServiceRefundList(String status,Long customerId,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL) {

		
		ListPage<CustomerRefundRequest> pageParam = new ListPage<CustomerRefundRequest>(pageSize, currentPage);
		
		if(order == null) {
			order = "asc";
		}
		if(order.equals("asc")){
			pageParam.addAscending(orderColumn);
		}
		if(order.equals("desc")){
			pageParam.addDescending(orderColumn);
		}
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("refundId", LongType.INSTANCE);
		typeMap.put("customerId", LongType.INSTANCE);
		typeMap.put("requestDate", TimestampType.INSTANCE);
		typeMap.put("refundTransactionNo", LongType.INSTANCE);
		typeMap.put("newTransactionNo", LongType.INSTANCE);
		typeMap.put("refundFrom", StringType.INSTANCE);
		typeMap.put("memberName", StringType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("refundType", StringType.INSTANCE);
		typeMap.put("paidAmt", BigDecimalType.INSTANCE);
		typeMap.put("requestAmt", BigDecimalType.INSTANCE);
		typeMap.put("approvedAmt", BigDecimalType.INSTANCE);
		typeMap.put("remark", StringType.INSTANCE);
		
		
		
		String sql = "SELECT \n" +
		        "    refundId,\n" +
		        "    customerId,\n" +
				"    requestDate,\n" +
				"    refundTransactionNo,\n" +
				"    newTransactionNo,\n" +
				"    refundFrom,\n" +
				"    memberName,\n" +
				"    status,\n" +
				"    paidAmt,\n" +
				"    requestAmt,\n" +
				"    approvedAmt,\n" +
				"	refundType,\n"+
				"    remark\n" +
				"FROM\n" +
				"    (SELECT \n" +
				"            req.refund_id refundId,\n" +
				"            hd.customer_id customerId,\n" +
				"            req.create_date requestDate,\n" +
				"            req.refund_transaction_no refundTransactionNo,\n" +
				"			 CASE req.refund_service_type \n"+
				"			WHEN '"+ReservationType.ROOM.getDesc().toUpperCase()+"' THEN \n"+
				" 			("+
				"    			 SELECT   trans.transaction_no \n"+
				" 				 FROM customer_order_trans AS  trans \n"+
				" 				 INNER JOIN customer_order_hd AS coh ON coh.order_no=trans.order_no \n"+
				" 	 			 INNER JOIN customer_order_det AS det ON det.order_no=coh.order_no \n"+
				" 	  			 WHERE  trans.from_transaction_no=req.refund_transaction_no \n"+
				" 	  			 AND det.ext_ref_no=cod.ext_ref_no"+              
				"     		 )"+
				"			ELSE "+
				"            (SELECT t_.transaction_no FROM customer_order_trans t_ WHERE t_.from_transaction_no=req.refund_transaction_no) \n " +
				"			END as 	newTransactionNo ,"+
				"			 CASE req.refund_service_type \n"+
				"			WHEN '"+ReservationType.WELLNESS.getDesc().toUpperCase()+"' THEN \n"+
//				"             tran.internal_remark "	+
				"  ("
				+ "			SELECT "
				+"				CASE WHEN t_.internal_remark IS NULL THEN CONCAT('MMS Invoice #',t_.ext_ref_no)	"
				+"				ELSE t_.internal_remark  end as internal_remark "
				+ "			 FROM customer_order_trans t_ WHERE t_.transaction_no = req.refund_transaction_no"
				+ "	 ) "+
				
				"			ELSE "+
				"             req.refund_service_type "+
				"			END as refundFrom ,"+
				"			req.refund_service_type as refundType ,"+
				"            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) memberName,\n" +
				"            req.status,\n" +
				"            tran.paid_amount paidAmt,\n" +
				"            req.request_amt requestAmt,\n" +
				"            req.approved_amt approvedAmt,\n" +
				"            req.internal_remark remark\n" +
				"    FROM\n" +
				"        customer_refund_request req\n" +
				"	 LEFT JOIN customer_order_det cod  ON req.order_det_id=cod.order_det_id \n"+
				"    JOIN customer_order_trans tran ON tran.transaction_no = req.refund_transaction_no\n" +
				"    JOIN customer_order_hd hd ON hd.order_no = tran.order_no\n" +
				"    LEFT JOIN customer_profile pro ON pro.customer_id = hd.customer_id\n" +
				"    WHERE req.refund_service_type <> 'CVL' <CUSTOMER_ID> <STATUS_CONDITION>\n" +
				") sub\n";
		
		
		if(!StringUtils.isEmpty(filterSQL)){
			sql = sql + "WHERE " + filterSQL;
		}
		
		List<Serializable> params = new ArrayList<Serializable>(); 
		
		if(customerId != null){
		//	sql = sql.replaceFirst("<CUSTOMER_ID>", "AND hd.customer_id = ?");
		//2016-02-22 update line 245 to 246 : The service refund record of dependent member should show on the primary member.
			sql=sql.replaceFirst("<CUSTOMER_ID>", "AND hd.customer_id in ("
					+ " SELECT customer_id FROM  member WHERE customer_id=? OR superior_member_id=?"
					+ ")");
			
			params.add(customerId);
			params.add(customerId);
		}else{
			sql = sql.replaceFirst("<CUSTOMER_ID>", "");
		}
		
		if(StringUtils.isNotEmpty(status) && !(StringUtils.equalsIgnoreCase("all", status) || StringUtils.equalsIgnoreCase("*", status))){
			if(!StringUtils.equalsIgnoreCase("APR", status)){
				sql = sql.replaceFirst("<STATUS_CONDITION>", "AND req.status = ?");
				params.add(status);
			}else{
				sql = sql.replaceFirst("<STATUS_CONDITION>", "AND req.status in (?,?)");
				params.add(status);
				params.add("REJ");
			}
			
			return this.listBySqlDto(
					pageParam,
					getCountSql(sql),
					sql,
					params,
					new CustomerRefundRequestDto(),
					typeMap);
		}else{
			sql = sql.replaceFirst("<STATUS_CONDITION>", "");

			return this.listBySqlDto(
					pageParam,
					getCountSql(sql),
					sql,
					params,
					new CustomerRefundRequestDto(),
					typeMap);
		}
	}
	
	@Override
	public ListPage<CustomerRefundRequest> getPendingRefundList(Date base, int days,
			String orderColumn, String order, int pageSize, int currentPage,
			String filterSQL) {
		
		ListPage<CustomerRefundRequest> pageParam = new ListPage<CustomerRefundRequest>(pageSize, currentPage);
		
		if(order == null) {
			order = "asc";
		}
		if(order.equals("asc")){
			pageParam.addAscending(orderColumn);
		}
		if(order.equals("desc")){
			pageParam.addDescending(orderColumn);
		}
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("refundId", LongType.INSTANCE);
		typeMap.put("requestDate", TimestampType.INSTANCE);
		typeMap.put("refundTransactionNo", LongType.INSTANCE);
		typeMap.put("refundFrom", StringType.INSTANCE);
		typeMap.put("memberName", StringType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("paidAmt", BigDecimalType.INSTANCE);
		typeMap.put("requestAmt", BigDecimalType.INSTANCE);
		typeMap.put("approvedAmt", BigDecimalType.INSTANCE);
		typeMap.put("remark", StringType.INSTANCE);
		
		
		
		String sql = "SELECT \n" +
		        "    refundId,\n" +
				"    requestDate,\n" +
				"    refundTransactionNo,\n" +
				"    refundFrom,\n" +
				"    memberName,\n" +
				"    status,\n" +
				"    paidAmt,\n" +
				"    requestAmt,\n" +
				"    approvedAmt,\n" +
				"    remark\n" +
				"FROM\n" +
				"    (SELECT \n" +
				"            req.refund_id refundId,\n" +
				"            req.create_date requestDate,\n" +
				"            req.refund_transaction_no refundTransactionNo,\n" +
				"            req.refund_service_type refundFrom,\n" +
				"            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) memberName,\n" +
				"            req.status,\n" +
				"            tran.paid_amount paidAmt,\n" +
				"            req.request_amt requestAmt,\n" +
				"            req.approved_amt approvedAmt,\n" +
				"            req.internal_remark remark\n" +
				"    FROM\n" +
				"        customer_refund_request req\n" +
				"    JOIN customer_order_trans tran ON tran.transaction_no = req.refund_transaction_no\n" +
				"    JOIN customer_order_hd hd ON hd.order_no = tran.order_no\n" +
				"    LEFT JOIN customer_profile pro ON pro.customer_id = hd.customer_id\n" +
				"    WHERE req.refund_service_type <> 'CVL' <WHERE_CONDITION>\n" +
				") sub\n";
		
		if(!StringUtils.isEmpty(filterSQL)){
			sql = sql + "WHERE " + filterSQL;
		}
		if(days > 0){
			DateTime beginOfDay = new DateTime(base);
			DateTime endOfDay = new DateTime(base);
			beginOfDay = beginOfDay.millisOfDay().withMinimumValue();
			endOfDay = endOfDay.millisOfDay().withMaximumValue();
			
			String where = "AND req.create_date BETWEEN ? - INTERVAL ? DAY AND ?";
			sql = sql.replaceFirst("<WHERE_CONDITION>", where);
			
			
			return this.listBySqlDto(
					pageParam,
					getCountSql(sql),
					sql,
					Arrays.asList(beginOfDay.toLocalDateTime().toDate(), days-1, endOfDay.toLocalDateTime().toDate()),
					new CustomerRefundRequestDto(),
					typeMap);
		}else{
			sql = sql.replaceFirst("<WHERE_CONDITION>", "");

			return this.listBySqlDto(
					pageParam,
					getCountSql(sql),
					sql,
					null,
					new CustomerRefundRequestDto(),
					typeMap);
		}
		
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Request Date & Time", "DATE(requestDate)", "java.util.Date", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Transaction ID", "refundTransactionNo", "java.lang.Long", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Refund From", "refundFrom", "java.lang.String", "", 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Patron", "memberName", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Paid Amount", "paidAmt", "java.math.BigDecimal", "", 5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Refund Amount", "approvedAmt", "java.math.BigDecimal", "", 6);
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5
				,condition6);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Refund ID #", "refundId", "java.lang.Long", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Request Date & Time", "DATE(requestDate)", "java.util.Date", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Academy ID", "academyNo", "java.lang.String", "", 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Patron", "memberName", "java.lang.String", "", 4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Request Amount", "requestAmt", "java.math.BigDecimal", "", 5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Refund Amount", "approvedAmt", "java.math.BigDecimal", "", 6);
		return Arrays.asList(condition1
				,condition2
				,condition3
				,condition4
				,condition5
				,condition6);
	}

	@Override
	public PaymentInfoDto getPaymentInfo(Long refundId) {
		ListPage<CustomerRefundRequest> listPage = new ListPage<CustomerRefundRequest>(1,1);
		
		String sql = "SELECT \n" +
				"    req.refund_transaction_no transactionID,\n" +
				"    txn.payment_method_code paymentMethod\n" +
				"FROM\n" +
				"    customer_refund_request req\n" +
				"        JOIN\n" +
				"    customer_order_trans txn ON req.refund_transaction_no = txn.transaction_no\n" +
				"WHERE\n" +
				"    refund_id = ?\n";
		
		
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("transactionID", LongType.INSTANCE);
		typeMap.put("paymentMethod", StringType.INSTANCE);
		
		ListPage<CustomerRefundRequest> list = listBySqlDto(listPage, getCountSql(sql), sql, Arrays.asList(refundId), new PaymentInfoDto(), typeMap);
		
		Assert.notEmpty(list.getDtoList(), "No such refund id");
		Assert.isTrue(list.getDtoList().size()==1, "More than one transaction have the same refund id");
		
		return (PaymentInfoDto)(list.getDtoList().get(0));
	}

	@Override
	public RefundInfoDto getRefundInfo(Long refundId) {
		ListPage<CustomerRefundRequest> listPage = new ListPage<CustomerRefundRequest>(1,1);
		String sql = "SELECT \n" +
				"    req.create_date requestDateTime,\n" +
				"    req.requester_type requestedBy,\n" +
				"    req.status,\n" +
				"    (select concat(staff.given_name, ' ', staff.surname) from staff_profile staff where staff.user_id = req.auditor_user_id) approvedBy,\n" +
				"    req.request_amt requestAmt,\n" +
				"    req.approved_amt refundAmt,\n" +
				"    txn.agent_transaction_no refNo,\n" +
				"    req.customer_reason reason,\n" +
				"    req.internal_remark remark,\n" +
				"    req.refund_money_type refundType\n" +
				"FROM\n" +
				"    customer_refund_request req\n" +
				"        JOIN\n" +
				"    customer_order_trans txn ON req.refund_transaction_no = txn.transaction_no\n" +
				"WHERE\n" +
				"    req.refund_id = ?\n";
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("requestDateTime", TimestampType.INSTANCE);
		typeMap.put("requestedBy", StringType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("approvedBy", StringType.INSTANCE);
		typeMap.put("requestAmt", BigDecimalType.INSTANCE);
		typeMap.put("refundAmt", BigDecimalType.INSTANCE);
		typeMap.put("refNo", StringType.INSTANCE);
		typeMap.put("reason", StringType.INSTANCE);
		typeMap.put("remark", StringType.INSTANCE);
		typeMap.put("refundType", StringType.INSTANCE);
		
		ListPage<CustomerRefundRequest> list = listBySqlDto(listPage, getCountSql(sql), sql, Arrays.asList(refundId), new RefundInfoDto(), typeMap);
		
		Assert.notEmpty(list.getDtoList(), "No such refund id");
		Assert.isTrue(list.getDtoList().size()==1, "More than one transaction have the same refund id");
		
		RefundInfoDto refundInfoDto = (RefundInfoDto)(list.getDtoList().get(0));
		if (!StringUtils.isEmpty(refundInfoDto.getRequestedBy()) && refundInfoDto.getRequestedBy().equalsIgnoreCase("CUSTOMER"))
			refundInfoDto.setRequestedBy("Customer");
		
		return refundInfoDto;
	}
	
	@Override
	public PrivateCoachBookListDto getPrivateCoachBookingDetail(Long refundId, String bookingType)
	{
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT m.academy_no AS memberID ")
					.append(",b.resv_id AS resvId ")
					.append(",b.exp_coach_user_id AS coachId ")
					.append(",b.STATUS AS status ")
			        .append(",trans.paid_amount AS totalAmount ")
			        .append(",CONCAT (cp.salutation, ' ', cp.given_name, ' ' ,cp.surname) AS memberName ")
					.append(",date_format(b.begin_datetime_book, '%Y/%m/%d') AS showDate2 ")
					.append(",date_format(b.begin_datetime_book, '%Y-%m-%d') AS showDate3 ")
					.append(",CONCAT (date_format(b.begin_datetime_book, '%H:%i'), '-', date_format(b.end_datetime_book, '%H:%i')) AS showTime ")
					.append(",b.resv_facility_type AS facilityType ")
					.append(",fa.caption AS BayType ")
					.append(",CONCAT (sp.given_name, ' ', sp.surname) AS coach ")
					.append(",b.customer_remark AS remark ")
				.append("FROM customer_refund_request ref ")
				.append("LEFT JOIN customer_order_trans trans on trans.transaction_no = ref.refund_transaction_no ")
				.append("LEFT JOIN member_facility_type_booking b on b.order_no = trans.order_no ")
				.append("LEFT JOIN member_facility_book_addition_attr a ON b.resv_id = a.resv_id ")
				.append("LEFT JOIN member m ON b.customer_id = m.customer_id ")
				.append("LEFT JOIN customer_profile cp ON b.customer_id = cp.customer_id ")
				.append("LEFT JOIN staff_profile sp ON b.exp_coach_user_id = sp.user_id ")
				.append("LEFT JOIN facility_attribute_caption fa ON a.attribute_id = fa.attribute_id ")
				.append("WHERE b.resv_facility_type = '").append(bookingType).append("' ")
				.append("AND b.exp_coach_user_id IS NOT NULL ")
		        .append("AND ref.refund_id = ").append(refundId);
        
		List<PrivateCoachBookListDto> bookingDetail = getDtoBySql(sql.toString(), null, PrivateCoachBookListDto.class);
		
		if(null != bookingDetail && bookingDetail.size()>0)
		{
			return bookingDetail.get(0);
		}
       
		return null;
	}

	@Override
	public FacilityReservationDto getFacilityBookingDetail(Long refundId)
	{
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT  m.academy_no AS academyNo, ")
		        	.append("b.facility_type_qty AS facilityTypeQty, ")
					.append("b.resv_id AS resvId, ")
					.append("trans.paid_amount AS totalAmount, ")
					.append("CONCAT (cp.salutation, ' ', cp.given_name, ' ', cp.surname) AS memberName, ")
				    .append("CONCAT (CAST(hour(b.begin_datetime_book) AS CHAR), ':00') AS startTime, ")
					.append("CONCAT (CAST((hour(b.end_datetime_book) + 1) AS CHAR), ':00') AS endTime, ")
					.append("date_format(DATE (b.begin_datetime_book), '%Y-%b-%d') AS bookingDate, ")
					.append("b.resv_facility_type AS resvFacilityType, ")
					.append("fa.caption AS BayType, ")
					.append("sum(CASE b.resv_facility_type ")
					.append("WHEN 'TENNIS' THEN (CASE tennis_pos.rate_type WHEN 'HI' THEN od.item_total_amout END) ")
					.append("ELSE (CASE pos.rate_type WHEN 'HI' THEN od.item_total_amout END) END) AS highPrice, ")
					.append("sum(CASE b.resv_facility_type ")
					.append("WHEN 'TENNIS' THEN (CASE tennis_pos.rate_type WHEN 'LO' THEN od.item_total_amout END) ")
					.append("ELSE (CASE pos.rate_type WHEN 'LO' THEN od.item_total_amout END) END) AS lowPrice, ")
					.append("min(CASE b.resv_facility_type ")
					.append("WHEN 'TENNIS' THEN (CASE tennis_pos.rate_type WHEN 'HI' THEN od.order_qty END) ")
					.append("ELSE (CASE pos.rate_type WHEN 'HI' THEN od.order_qty END) END) AS odQtyhighPrice, ")
					.append("min(CASE b.resv_facility_type ")
					.append("WHEN 'TENNIS' THEN (CASE tennis_pos.rate_type WHEN 'LO' THEN od.order_qty END) ")
					.append("ELSE (CASE pos.rate_type WHEN 'LO' THEN od.order_qty END) END) AS odQtylowPrice ")
			.append("FROM customer_refund_request ref ")
			.append("LEFT JOIN customer_order_trans trans on trans.transaction_no = ref.refund_transaction_no ")
			.append("LEFT JOIN customer_order_det od on trans.order_no = od.order_no ")
			.append("LEFT JOIN facility_utilization_pos pos on pos.pos_item_no = od.item_no ")
			.append("LEFT JOIN facility_sub_type_pos tennis_pos on tennis_pos.pos_item_no = od.item_no ")
			.append("LEFT JOIN member_facility_type_booking b on b.order_no = trans.order_no ")
			.append("LEFT JOIN member_facility_book_addition_attr a ON b.resv_id = a.resv_id ")
			.append("LEFT JOIN member m ON b.customer_id = m.customer_id ")
			.append("LEFT JOIN customer_profile cp ON b.customer_id = cp.customer_id ")
			.append("LEFT JOIN facility_attribute_caption fa ON a.attribute_id = fa.attribute_id ")
			.append("WHERE ref.refund_id = ").append(refundId);
		
		List<FacilityReservationDto> bookingDetail = getDtoBySql(sql.toString(), null, FacilityReservationDto.class);
		
		if(null != bookingDetail && bookingDetail.size()>0)
		{
			return bookingDetail.get(0);
		}
       
		return null;
	}

	@Override
	public CourseEnrollment getEnrollment(Long refundId)
	{
		StringBuilder sql = new StringBuilder();
		
		sql.append("")
		.append("SELECT c.customer_id AS customerIdValue ")
				.append(",c.course_id AS courseIdValue ")
		.append("FROM customer_refund_request ref ")
		.append("LEFT JOIN customer_order_trans trans ON trans.transaction_no = ref.refund_transaction_no ")
		.append("LEFT JOIN course_enrollment c ON c.cust_order_no = trans.order_no ")
		.append("WHERE trans.order_no IS NOT NULL ")
		.append("AND ref.refund_id = ").append(refundId);

		
		List<CourseEnrollment> courseEnrollment = getDtoBySql(sql.toString(), null, CourseEnrollment.class);
		
		if (courseEnrollment.size() > 0)
		{
			return courseEnrollment.get(0);
		}
		
		return null;
	}

	@Override
	public DaypassRefundDto getDaypassBookingDetail(Long refundId) {
		StringBuilder sql = new StringBuilder();
		sql.append("  select  distinct cohd.customer_id as customerId2, ")
			.append(" cr.refund_id as refundId, ")
			.append(" cohd.order_no as orderNo2,  ")
			.append("  concat( cp.salutation , ' ', cp.given_name, ' ', cp.surname) as memberName, ")
			.append("  m.academy_no as acdemyID ,  ")
			.append("  date_format(copc.effective_from, '%Y-%m-%d')  as activationDate,  ")
			.append("  date_format( copc.effective_to, '%Y-%m-%d') as deactivationDate, ")
			.append("  (select count(1) from customer_order_permit_card copc inner join customer_order_hd cohd on  copc.customer_order_no = cohd.order_no  inner join customer_order_trans cots on cots.order_no = cohd.order_no inner join  customer_refund_request cr on cr.refund_transaction_no = cots.transaction_no where cr.refund_id=? ) as ofPass2 ,  ")
			.append("  cohd.order_total_amount as totalPrice,  ")
			.append("  cots.payment_method_code as paymentMethodCode, ")
			.append("  cots.transaction_no as transactionID2, ")
			.append("  date_format( cr.create_date, '%Y-%m-%d %H:%i:%S') as requestDate, ")
			.append("  cr.requester_type as requestBy, ")
			.append("  cr.refund_money_type as refundMethod, ")
			.append("  cr.status as status , ")
			.append("  cr.request_amt as refundAmout, ")
			.append("  cr.customer_reason as refundRemark , ")
			.append("  cr.internal_remark as approvalRemark  ")
			.append("  from customer_refund_request cr ")
			.append("  left join customer_order_trans cots on cr.refund_transaction_no=cots.transaction_no ")
			.append("  left join customer_order_hd cohd on cohd.order_no=cots.order_no ")
			.append("  left join customer_order_permit_card copc on copc.customer_order_no = cohd.order_no ")
			.append("  left join customer_profile cp  on cohd.customer_id = cp.customer_id  ")
			.append("  LEFT JOIN member m on m.customer_id = cohd.customer_id  ")
			.append("  where cr.refund_id=?  ") ;
			
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(refundId);
		param.add(refundId);
		List<DaypassRefundDto> list = getDtoBySql(sql.toString(), param, DaypassRefundDto.class);
		if(list !=null && list.size()>0){
			return list.get(0);
		}else {
			//modified by Kaster 20160510 将抛出异常统一规划化
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{"get daypass refund dto error", refundId+""});
			throw new GTACommonException(GTAError.RefundError.NO_FIND_REFUND);
		}
	}
	
	public CustomerRefundRequest getById(Long refundRequestId) {
        return super.get(CustomerRefundRequest.class, refundRequestId);
	}

	@Override
	public WellnessRefundDto getWellnessRefundDetail(Long refundId) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("  select  distinct cohd.customer_id as customerId2, ")
			.append(" cr.refund_id as refundId, ")
			.append(" cr.refund_service_type as service, ")
			.append("  concat( cp.salutation , ' ', cp.surname, ' ', cp.given_name) as memberName, ")
			.append("  m.academy_no as acdemyID ,  ")
			.append("  cohd.order_total_amount as totalPrice,  ")
			.append("  cots.payment_method_code as paymentMethodCode, ")
			.append("  cots.transaction_no as transactionID2, ")
			.append("  date_format( cr.create_date, '%Y-%m-%d %H:%i:%S') as requestDate, ")
			.append("  cr.requester_type as requestBy, ")
			.append("  cr.refund_money_type as refundMethod, ")	
			.append("  cr.status as status , ")
			.append("  cr.customer_reason as refundRemark , ")
			.append("  cr.internal_remark as approvalRemark , ")
			.append("  cr.request_amt as refundAmout ")
			.append("  from customer_refund_request cr ")
			.append("  left join customer_order_trans cots on cr.refund_transaction_no=cots.transaction_no ")
			.append("  left join customer_order_hd cohd on cohd.order_no=cots.order_no ")
			.append("  left join customer_profile cp  on cohd.customer_id = cp.customer_id  ")
			.append("  LEFT JOIN member m on m.customer_id = cohd.customer_id  ")
			.append("  where cr.refund_id=?  ") ;
			
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(refundId);
		List<WellnessRefundDto> list = getDtoBySql(sql.toString(), param, WellnessRefundDto.class);
		if(list !=null && list.size()>0){
			return list.get(0);
		}else {
			//modified by Kaster 20160510 将抛出异常统一规划化
//			throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{"get wellness refund dto error", refundId+""});
			throw new GTACommonException(GTAError.RefundError.NO_FIND_REFUND);
		}
	}

	@Override
	public List<SpaRefundDto> matchRequestId(List<String> paymentIds) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("select t.agent_transaction_no as paymentId,r.refund_id as refundId from customer_order_trans t,customer_refund_request r where t.transaction_no=r.refund_transaction_no and t.agent_transaction_no in (");
		for (int i = 0; i < paymentIds.size(); i++) {
			hql.append("?");
			if (i < paymentIds.size() - 1) {
				hql.append(",");
			}
		}
		hql.append(") ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.addAll(paymentIds);
		return getDtoBySql(hql.toString(), param, SpaRefundDto.class);
	}
	
	@Override
	public List<SpaRefundDto> matchCustomerName(List<String> academyNos) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("select m.academy_no as academyNo,concat(cp.surname, ' ', cp.given_name) as customerName from member m,customer_profile cp where m.customer_id=cp.customer_id and m.academy_no in (");
		for (int i = 0; i < academyNos.size(); i++) {
			hql.append("?");
			if (i < academyNos.size() - 1) {
				hql.append(",");
			}
		}
		hql.append(") ");
		List<Serializable> param = new ArrayList<Serializable>();
		param.addAll(academyNos);
		return getDtoBySql(hql.toString(), param, SpaRefundDto.class);
	}
	
	@Override
	public RefundInfoDto getCancelInfo(String resvId) {
		ListPage<CustomerRefundRequest> listPage = new ListPage<CustomerRefundRequest>(1,1);
		String [] resvIds = resvId.split("-");
		String resvType = resvIds[0];
		String resvIdValue = resvIds[1];
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT a.requestDateTime as requestDateTime,case a.requestedBy when 'CUSTOMER' then 'Member' else a.requestedBy end as requestedBy,a.refundId as refundId  from ( \n");
		if("GBF".equals(resvType) || "TCF".equals(resvType) || "GPC".equals(resvType) || "TPC".equals(resvType)){ 	
			sb.append("SELECT \n");
			sb.append("    case when req.refund_id is not null then req.create_date else m.update_date end as requestDateTime, \n");
			sb.append("    case when req.refund_id is not null then req.requester_type else 'CUSTOMER' end as requestedBy, \n");
			sb.append("    req.refund_id refundId \n");
			sb.append("FROM\n ");
			sb.append("    member_facility_type_booking m\n");
			sb.append("    left join customer_order_hd hd on m.order_no = hd.order_no\n");
			sb.append("    left join customer_order_trans trans on hd.order_no = trans.order_no\n");
			sb.append("    left join customer_refund_request req on req.refund_transaction_no = trans.transaction_no\n");
			sb.append("WHERE\n");
			sb.append("    m.resv_id = ?\n");
		}else if("GCE".equals(resvType) || "TCE".equals(resvType)){
			sb.append("SELECT \n");
			sb.append("    case when req.refund_id is not null then req.create_date else m.update_date end as requestDateTime, \n");
			sb.append("    case when req.refund_id is not null then req.requester_type else 'CUSTOMER' end as requestedBy, \n");
			sb.append("    req.refund_id refundId \n");
			sb.append("FROM\n ");
			sb.append("    course_enrollment m\n");
			sb.append("    left join customer_order_hd hd on m.cust_order_no = hd.order_no\n");
			sb.append("    left join customer_order_trans trans on hd.order_no = trans.order_no\n");
			sb.append("    left join customer_refund_request req on req.refund_transaction_no = trans.transaction_no\n");
			sb.append("WHERE\n");
			sb.append("    m.enroll_id = ?\n");
		}else if("RES".equals(resvType) || "BAR".equals(resvType) || "CAF".equals(resvType) || "SKD".equals(resvType) || "RTL".equals(resvType)){
			sb.append("SELECT \n");
			sb.append("    m.update_date as requestDateTime, \n");
			sb.append("    'CUSTOMER' as requestedBy, \n");
			sb.append("    null as refundId \n");
			sb.append("FROM\n ");
			sb.append("    restaurant_customer_booking m\n");
			sb.append("WHERE\n");
			sb.append("    m.resv_id = ?\n");
		}else if("GRM".equals(resvType)){
			sb.append("SELECT \n");
			sb.append("    case when req.refund_id is not null then req.create_date else m.update_date end as requestDateTime, \n");
			sb.append("    case when req.refund_id is not null then req.requester_type else 'CUSTOMER' end as requestedBy, \n");
			sb.append("    req.refund_id refundId \n");
			sb.append("FROM\n ");
			sb.append("    room_reservation_rec m\n");
			sb.append("    left join customer_order_hd hd on m.order_no = hd.order_no\n");
			sb.append("    left join customer_order_trans trans on hd.order_no = trans.order_no\n");
			sb.append("    left join customer_refund_request req on req.refund_transaction_no = trans.transaction_no\n");
			sb.append("WHERE\n");
			sb.append("    m.resv_id = ?\n");
		}else if("SPA".equals(resvType)){
			sb.append("SELECT \n");
			sb.append("    case when req.refund_id is not null then req.create_date else m.update_date end as requestDateTime, \n");
			sb.append("    case when req.refund_id is not null then req.requester_type else 'CUSTOMER' end as requestedBy, \n");
			sb.append("    req.refund_id refundId \n");
			sb.append("FROM\n ");
			sb.append("    spa_appointment_rec m\n");
			sb.append("    left join customer_order_det det on m.order_det_id = det.order_det_id\n");
			sb.append("    left join customer_order_hd hd on det.order_no = hd.order_no\n");
			sb.append("    left join customer_order_trans trans on hd.order_no = trans.order_no\n");
			sb.append("    left join customer_refund_request req on req.refund_transaction_no = trans.transaction_no\n");
			sb.append("WHERE\n");
			sb.append("    right(m.ext_appointment_id,6) = ?\n");
		}
		sb.append(") a \n");
		
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("requestDateTime", TimestampType.INSTANCE);
		typeMap.put("requestedBy", StringType.INSTANCE);
		typeMap.put("refundId", LongType.INSTANCE);
		
		ListPage<CustomerRefundRequest> list = listBySqlDto(listPage, getCountSql(sb.toString()), sb.toString(), Arrays.asList(resvIdValue), new RefundInfoDto(), typeMap);
		
		RefundInfoDto refundInfoDto = (RefundInfoDto)(list.getDtoList().get(0));
		
		return refundInfoDto;
	}
	/**
	 * 根据时间获取serviceRefund列表
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	@Override
	public List<CustomerRefundRequestDto> getServiceRefundList(String startTime, String endTime) {

		String sql = "SELECT \n" +
		        "    refundId,\n" +
		        "    customerId,\n" +
				"    requestDate,\n" +
				"    refundTransactionNo,\n" +
				"    newTransactionNo,\n" +
				"    refundFrom,\n" +
				"    memberName,\n" +
				"    status,\n" +
				"    paidAmt,\n" +
				"    requestAmt,\n" +
				"    approvedAmt,\n" +
				"    remark\n" +
				"FROM\n" +
				"    (SELECT \n" +
				"            req.refund_id refundId,\n" +
				"            hd.customer_id customerId,\n" +
				"            req.create_date requestDate,\n" +
				"            req.refund_transaction_no refundTransactionNo,\n" +
				"			 CASE req.refund_service_type \n"+
				"			WHEN '"+ReservationType.ROOM.getDesc().toUpperCase()+"' THEN \n"+
				" 			("+
				"    			 SELECT   trans.transaction_no \n"+
				" 				 FROM customer_order_trans AS  trans \n"+
				" 				 INNER JOIN customer_order_hd AS coh ON coh.order_no=trans.order_no \n"+
				" 	 			 INNER JOIN customer_order_det AS det ON det.order_no=coh.order_no \n"+
				" 	  			 WHERE  trans.from_transaction_no=req.refund_transaction_no \n"+
				" 	  			 AND det.ext_ref_no=cod.ext_ref_no"+              
				"     		 )"+
				"			ELSE "+
				"            (SELECT t_.transaction_no FROM customer_order_trans t_ WHERE t_.from_transaction_no=req.refund_transaction_no) \n " +
				"			END as 	newTransactionNo ,"+
				"            req.refund_service_type refundFrom,\n" +
				"            CONCAT(pro.salutation, ' ', pro.given_name, ' ', pro.surname) memberName,\n" +
				"            req.status,\n" +
				"            tran.paid_amount paidAmt,\n" +
				"            req.request_amt requestAmt,\n" +
				"            req.approved_amt approvedAmt,\n" +
				"            req.internal_remark remark\n" +
				"    FROM\n" +
				"        customer_refund_request req\n" +
				"	 LEFT JOIN customer_order_det cod  ON req.order_det_id=cod.order_det_id \n"+
				"    JOIN customer_order_trans tran ON tran.transaction_no = req.refund_transaction_no\n" +
				"    JOIN customer_order_hd hd ON hd.order_no = tran.order_no\n" +
				"    LEFT JOIN customer_profile pro ON pro.customer_id = hd.customer_id\n" +
				"    WHERE req.refund_service_type <> 'CVL' AND date_format( req.create_date, '%Y-%m-%d') >= ? AND  date_format( req.create_date, '%Y-%m-%d') <= ? \n" +
				") sub\n";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(startTime); 
		param.add(endTime);

		return this.getDtoBySql(sql, param, CustomerRefundRequestDto.class);
	}
}
