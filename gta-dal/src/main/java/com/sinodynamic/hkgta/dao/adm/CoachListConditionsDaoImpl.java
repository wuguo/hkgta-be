package com.sinodynamic.hkgta.dao.adm;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;

@Repository("coachListConditions")
public class CoachListConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao
{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Coach Name", "staffName", "java.lang.String", "", 1);
		final AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Coach ID", "userId", "java.lang.String", "", 2);
		final AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Employ Date", "employDate", "java.lang.String", "", 3);
		final AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Position Title", "positionTitleName", "java.lang.String", "", 4);
		final AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Quit Date", "quitDate", "java.lang.String", "", 5);
		return Arrays.asList(condition1, condition2, condition3, condition4, condition5);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type)
	{
		return null;
	}

}
