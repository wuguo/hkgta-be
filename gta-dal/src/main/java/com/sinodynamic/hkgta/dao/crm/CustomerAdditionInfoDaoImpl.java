package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdditionalInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoCaptionDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAnalysisReportDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;

@Repository
public class CustomerAdditionInfoDaoImpl extends GenericDao<CustomerAdditionInfo> implements CustomerAdditionInfoDao {

	public CustomerAdditionInfo getCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		return this.get(t, t.getId());
	}

	public void saveCustomerAdditionInfo(CustomerAdditionInfo t)
			{
		this.save(t);

	}

	public void updateCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		this.update(t);

	}

	public void deleteCustomerAdditionInfo(CustomerAdditionInfo t)
			throws Exception {
		this.delete(t);

	}

	@Override
	public List<CustomerAdditionInfo> getCustomerAdditionInfoListByCustomerID(Long customerID) {
		return getByCol(CustomerAdditionInfo.class, "id.customerId", customerID, "createDate desc");
	}
	/**
	 * 根据customerId获取没有MRK信息的客户添加信息列表
	 * @param customerId
	 * @return
	 * @throws HibernateException
	 */
	@Override
	public List<CustomerAdditionInfo> getCustomerAdditionInfoNotMRKList(Long customerId) throws HibernateException {
		String sql =  "SELECT cai FROM CustomerAdditionInfo AS cai ," 
					+ " CustomerAdditionInfoCaption p WHERE  p.captionId = cai.id.captionId " 
					+ " AND cai.id.customerId = ? AND p.category <> 'MRK-OPT'" ;
		List paramList = new ArrayList<String>();
		paramList.add(customerId);
		List<CustomerAdditionInfo> addinfo = super.getByHql(sql.toString(), paramList);
		return addinfo;
	}
	
	public CustomerAdditionInfo getByCustomerIdAndCaptionId(Long customerId,Long captionId){
		String hqlstr = " from CustomerAdditionInfo c where c.id.customerId = ?  and c.id.captionId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(captionId);
		return (CustomerAdditionInfo) getUniqueByHql(hqlstr, param);
	}
	@Override
	public List<AdditionalInfoDto> getByCustomerIdAndCategory(Long customerId,
			String category) throws HibernateException {
		// TODO Auto-generated method stub

		StringBuilder sql = new StringBuilder();
		
		sql.append("	 SELECT c.customer_id AS customerId, p.caption_id AS captionId, p.category AS category, p.caption AS caption, " )
		.append("		c.customer_input AS inputData FROM customer_addition_info_caption p LEFT JOIN ( SELECT " )
		.append("				customer_addition_info.customer_id AS customer_id, customer_addition_info.customer_input AS customer_input, ")
		.append(" customer_addition_info.caption_id AS caption_id FROM customer_addition_info ")
		.append("			WHERE customer_addition_info.customer_id = ? ) c ON c.caption_id = p.caption_id ") 
		.append("		WHERE p.category = ? ");
	
		List paramList = new ArrayList<String>();
		paramList.add(customerId);
		paramList.add(category);
		
		List<AdditionalInfoDto> addinfo = super.getDtoBySql(sql.toString(), paramList, AdditionalInfoDto.class);
		return addinfo;
	}
	
	public List<AdditionalInfoDto> getByCustomerIdCaptinOrGory(Long customerId,
			String category,String caption) throws HibernateException {
		// TODO Auto-generated method stub

		StringBuilder sql = new StringBuilder();
		
		sql.append("	 SELECT c.customer_id AS customerId, p.caption_id AS captionId, p.category AS category, p.caption AS caption, " )
		.append("		c.customer_input AS inputData FROM customer_addition_info_caption p LEFT JOIN ( SELECT " )
		.append("				customer_addition_info.customer_id AS customer_id, customer_addition_info.customer_input AS customer_input, ")
		.append(" customer_addition_info.caption_id AS caption_id FROM customer_addition_info ")
		.append("			WHERE customer_addition_info.customer_id = ? ) c ON c.caption_id = p.caption_id ") 
		.append("		WHERE p.category = ?  and p.caption=?");
	
		List paramList = new ArrayList<String>();
		paramList.add(customerId);
		paramList.add(category);
		paramList.add(caption);
		List<AdditionalInfoDto> addinfo = super.getDtoBySql(sql.toString(), paramList, AdditionalInfoDto.class);
		return addinfo;
	}

	@Override
	public int deleteByCustomerId(Long customerId) throws HibernateException {
		// TODO Auto-generated method stub
		String hql = " delete from customer_addition_info  where customer_id = ? ";
		return super.deleteByHql(hql, customerId);
	}

	
	public List<CustomerAdditionInfoDto> getFavorByCustomerId(Long customerId){
		String sql = " select c.caption_id as captionId, c.customer_input as customerInput from customer_addition_info c where c.customer_id = ?  and c.caption_id in ('8','9','10','11','12') ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		return getDtoBySql(sql, param, CustomerAdditionInfoDto.class);
	}
	
	public List<CustomerAdditionInfoDto> getByCustomerIdAndCategoryForTablet(Long customerId,String category,String device){
		String sql = "";
		if("PC".equalsIgnoreCase(device)){
			sql = "SELECT\n" +
					"	caption.caption AS caption,\n" +
					"	caption.caption_id AS captionId,\n" +
					"	info.customer_input AS customerInput\n" +
					"FROM\n" +
					"	customer_addition_info_caption caption left join\n" +
					"	(select * from customer_addition_info i where i.customer_id = ?) info\n" +
					"	on info.caption_id = caption.caption_id\n" +
					"WHERE\n" +
					"	caption.category = ?\n" +
					"	ORDER BY\n" +
					"caption.display_order ASC";
		}else{
			sql = "SELECT\n" +
				"	caption.caption AS caption,\n" +
				"	caption.caption_id AS captionId,\n" +
				"	info.customer_input AS customerInput\n"+
				"FROM\n" +
				"	customer_addition_info info,\n" +
				"	customer_addition_info_caption caption\n" +
				"WHERE\n" +
				"	info.caption_id = caption.caption_id\n" +
				"AND info.customer_id = ?\n" +
				"AND caption.category = ?\n" +
				"ORDER BY\n" +
				"	caption.display_order ASC";
		}
	   List<Serializable> param = new ArrayList<Serializable>();
	   param.add(customerId);
	   param.add(category);
	   
	   return getDtoBySql(sql, param, CustomerAdditionInfoDto.class);
	}
	
	@Override
	public List<CustomerAdditionInfoDto> getAnalysisInfo(Long customerId) {
		String sql = "select s.caption_id as captionId, s.caption, a.customer_input as customerInput, s.category, s.display_order as displayOrder from (select c.caption_id, c.caption, c.category, c.display_order from customer_addition_info_caption c WHERE c.category in ('CNS-GH','CNS-KID','CNS-FB','CNS-SPA','CNS-GF','CNS-TNS') GROUP BY c.category,c.caption_id,c.category, c.display_order) s LEFT JOIN customer_addition_info a ON s.caption_id = a.caption_id and customer_id = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		return getDtoBySql(sql, param, CustomerAdditionInfoDto.class);
	}
	
	/**
	 * 获取Marketing id
	 * @return
	 */
	@Override
//	@Transactional
	public List<CustomerAdditionInfoCaptionDto> getCustomerAdditionInfoCaptionByMarketing() {
		String hql = "SELECT c.captionId AS captionId, c.caption AS caption FROM CustomerAdditionInfoCaption c  WHERE c.caption IN ('HKGTA Marketing','NWD Marketing') ";
		List<Serializable> param = new ArrayList<Serializable>();
		return getDtoByHql(hql, param, CustomerAdditionInfoCaptionDto.class);
	}
	
	/**
	 * 根据类型获取用户分析报告
	 * @param categoryType
	 * @return
	 */
	@Override
	public List<CustomerAnalysisReportDto> getCustomerAnalysisReport(String categoryType) {
		String sql = "SELECT a.caption_id AS captionId, a.caption, b.customer_input AS customerInput,"
				+ " a.category, COUNT(b.customer_input) AS countNum FROM customer_addition_info_caption a,"
				+ " customer_addition_info b, member m WHERE a.caption_id = b.caption_id and m.customer_id = b.customer_id AND a.category = ? "
				+ " AND a.caption != 'Spare' AND m.status='ACT' GROUP BY a.caption, b.customer_input ORDER BY countNum DESC ";//LIMIT 10
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(categoryType);
		return getDtoBySql(sql, param, CustomerAnalysisReportDto.class);
	}
	
}
