package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.VirtualAccPool;
import com.sinodynamic.hkgta.util.CommUtil;

@Repository
public class VirtualAccPoolDaoImpl extends GenericDao<VirtualAccPool> implements
		VirtualAccPoolDao {
	@Deprecated
	private static List<VirtualAccPool> availableAccounts = null;

	@Override
	public Serializable saveVirtualAccPool(VirtualAccPool vap) {
		return save(vap);
	}

	@Override
	public List<VirtualAccPool> getVirtualAccList() {
		return getByHql("from VirtualAccPool");
	}

	@Override
	@Deprecated
	public List<VirtualAccPool> getAvailableVirtualAccList(String[] exceptAccs) {
		String params = getExceptParamString(exceptAccs);
		String hql = null;
		if (params == null) {
			hql = "from VirtualAccPool where status = 'AV' order by vaAccNo";
		} else {
			hql = "from VirtualAccPool where status = 'AV'" + params + " order by vaAccNo";
		}
		return getByHql(hql);
	}

	@Deprecated
	public synchronized VirtualAccPool autoVirtualAccAssign(String[] exceptAccs) {

		if (availableAccounts == null || availableAccounts.size() == 0) {
			availableAccounts = getAvailableVirtualAccList(exceptAccs);
		}
		
		int size = availableAccounts.size();
		if (size == 0) return null;

		int index = size - 1;
		VirtualAccPool acc = availableAccounts.get(index);
		availableAccounts.remove(index);

		return acc;
	}
	@Deprecated
	private String getExceptParamString(String[] exceptAccs) {
		
		if (exceptAccs == null || exceptAccs.length == 0 ) return null;
		StringBuilder sb = new StringBuilder("");
		for (String param : exceptAccs) {
			if (CommUtil.nvl(param).length() == 0) continue;
			sb.append("'").append(param).append("'").append(",");
		}
		
		String paramStr = sb.toString();
		return " and vaAccNo not in (" + paramStr.substring(0, paramStr.length() - 1) + ")";
	}
	
	@Deprecated
	public boolean updateStatus(String status, String vaAccNo){
		String hqlstr=" update VirtualAccPool vap set vap.status = ? where vap.vaAccNo = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(status);
		param.add(vaAccNo);
		if(hqlUpdate(hqlstr,param) > 0) return true;
		return false;
	}

	@Override
	public VirtualAccPool queryOneAvailableVirtualAccount() {
		Query q = getCurrentSession().createQuery("from VirtualAccPool where status=? order by vaAccNo ");
		q.setString(0, "AV");
		q.setMaxResults(1);
		VirtualAccPool virtualAccount = (VirtualAccPool)q.uniqueResult();
		return virtualAccount;
	}

}
