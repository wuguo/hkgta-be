package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;

public interface CustomerPreEnrollStageDao extends IBaseDao<CustomerPreEnrollStage>{

	public List<CustomerPreEnrollStage> getByCustomerId(Long customerId);
	
	public void deleteCustomerPreEnrollStage(List<CustomerPreEnrollStage> customerPreEnrollStage);
	
	public List getLatestCommentsActivities (int totalSize);
}
