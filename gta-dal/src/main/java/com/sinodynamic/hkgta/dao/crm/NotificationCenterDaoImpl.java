package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.staff.NotificationDto;
import com.sinodynamic.hkgta.dto.staff.NotificationMemberDto;

@SuppressWarnings("rawtypes")
@Repository
public class NotificationCenterDaoImpl extends GenericDao implements NotificationCenterDao {
	
	Logger logger = Logger.getLogger(ServicePlanDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override

	public List<NotificationMemberDto> getNotificationMemberList(NotificationDto notificationDto) {
		String sql = "<StaffUnionSql> SELECT\n" +
				"	*\n" +
				"FROM\n" +
				"	(\n" +
				"		SELECT\n" +
				"			m.customer_id customerId,\n" +
				"			CONCAT(\n" +
				"				cp.given_name,\n" +
				"				' ',\n" +
				"				cp.surname\n" +
				"			) customerName,\n" +
				"			IF(LENGTH(cp.gender)>0,cp.gender,'U') gender,\n" +
				"			TIMESTAMPDIFF(\n" +
				"				YEAR,\n" +
				"				cp.date_of_birth,\n" +
				"				NOW()\n" +
				"			) age,\n" +
				"			cp.contact_email email,\n" +
				"			cp.phone_mobile phoneMobile,\n" +
				"			CASE m.member_type\n" +
				"		WHEN 'IPM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					sp.plan_no\n" +
				"				FROM\n" +
				"					service_plan sp\n" +
				"				LEFT JOIN customer_service_subscribe css ON sp.plan_no = css.service_plan_no\n" +
				"				LEFT JOIN customer_service_acc csa ON css.acc_no = csa.acc_no\n" +
				"				WHERE\n" +
				"					csa.customer_id = m.customer_id\n" +
				"				AND csa. STATUS = 'ACT'\n" +
				"				AND NOW() BETWEEN csa.effective_date\n" +
				"				AND csa.expiry_date\n" +
				"			)\n" +
				"		WHEN 'CPM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					sp.plan_no\n" +
				"				FROM\n" +
				"					service_plan sp\n" +
				"				LEFT JOIN corporate_service_subscribe corss ON sp.plan_no = corss.service_plan_no\n" +
				"				LEFT JOIN corporate_service_acc corsa ON corss.acc_no = corsa.acc_no\n" +
				"				AND corsa. STATUS = 'ACT'\n" +
				"				AND now() BETWEEN corsa.effective_date\n" +
				"				AND corsa.expiry_date\n" +
				"				LEFT JOIN corporate_profile corp ON corsa.corporate_id = corp.corporate_id\n" +
				"				LEFT JOIN corporate_member corm ON corp.corporate_id = corm.corporate_id\n" +
				"				WHERE\n" +
				"					corm.customer_id = m.customer_id\n" +
				"			)\n" +
				"		WHEN 'IDM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					sp.plan_no\n" +
				"				FROM\n" +
				"					service_plan sp\n" +
				"				LEFT JOIN customer_service_subscribe css ON sp.plan_no = css.service_plan_no\n" +
				"				LEFT JOIN customer_service_acc csa ON css.acc_no = csa.acc_no\n" +
				"				WHERE\n" +
				"					csa.customer_id = m.superior_member_id\n" +
				"				AND csa. STATUS = 'ACT'\n" +
				"				AND NOW() BETWEEN csa.effective_date\n" +
				"				AND csa.expiry_date\n" +
				"			)\n" +
				"		WHEN 'CDM' THEN\n" +
				"			(\n" +
				"				SELECT\n" +
				"					sp.plan_no\n" +
				"				FROM\n" +
				"					service_plan sp\n" +
				"				LEFT JOIN corporate_service_subscribe corss ON sp.plan_no = corss.service_plan_no\n" +
				"				LEFT JOIN corporate_service_acc corsa ON corss.acc_no = corsa.acc_no\n" +
				"				AND corsa. STATUS = 'ACT'\n" +
				"				AND NOW() BETWEEN corsa.effective_date\n" +
				"				AND corsa.expiry_date\n" +
				"				LEFT JOIN corporate_profile corp ON corsa.corporate_id = corp.corporate_id\n" +
				"				LEFT JOIN corporate_member corm ON corp.corporate_id = corm.corporate_id\n" +
				"				WHERE\n" +
				"					corm.customer_id = m.superior_member_id\n" +
				"			)\n" +
				"       WHEN m.member_type = 'MG' and m.member_type = 'HG' THEN \n" +
				"                   (SELECT sp.plan_no from service_plan sp  LEFT JOIN customer_order_permit_card copc on sp.plan_no = copc.service_plan_no \n" +
				"                     WHERE copc.cardholder_customer_id = m.customer_id\n" +
				"                     AND copc. STATUS = 'ACT'    \n" +
				"                     AND NOW() BETWEEN copc.effective_from  AND copc.effective_to ) \n" +
				"		ELSE\n" +
				"			NULL\n" +
				"		END AS planNo,\n" +
				"		m.member_type memberType,\n" +
				"		m.`status`,\n" +
				"		CASE m.member_type\n" +
				"	WHEN 'IPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				acc.expiry_date\n" +
				"			FROM\n" +
				"				customer_service_acc acc\n" +
				"			WHERE\n" +
				"				acc.customer_id = m.customer_id\n" +
				"			AND acc. STATUS = 'ACT'\n" +
				"			AND NOW() BETWEEN acc.effective_date\n" +
				"			AND acc.expiry_date\n" +
				"		)\n" +
				"	WHEN 'CPM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				coracc.expiry_date\n" +
				"			FROM\n" +
				"				corporate_service_acc coracc\n" +
				"			LEFT JOIN corporate_profile cp ON coracc.corporate_id = cp.corporate_id\n" +
				"			LEFT JOIN corporate_member cm ON cp.corporate_id = cm.corporate_id\n" +
				"			WHERE\n" +
				"				cm.customer_id = m.customer_id\n" +
				"			AND coracc. STATUS = 'ACT'\n" +
				"			AND now() BETWEEN coracc.effective_date\n" +
				"			AND coracc.expiry_date\n" +
				"		)\n" +
				"	WHEN 'IDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				acc.expiry_date\n" +
				"			FROM\n" +
				"				customer_service_acc acc\n" +
				"			WHERE\n" +
				"				acc.customer_id = m.superior_member_id\n" +
				"			AND acc. STATUS = 'ACT'\n" +
				"			AND NOW() BETWEEN acc.effective_date\n" +
				"			AND acc.expiry_date\n" +
				"		)\n" +
				"	WHEN 'CDM' THEN\n" +
				"		(\n" +
				"			SELECT\n" +
				"				coracc.expiry_date\n" +
				"			FROM\n" +
				"				corporate_service_acc coracc\n" +
				"			LEFT JOIN corporate_profile cp ON coracc.corporate_id = cp.corporate_id\n" +
				"			LEFT JOIN corporate_member cm ON cp.corporate_id = cm.corporate_id\n" +
				"			WHERE\n" +
				"				cm.customer_id = m.superior_member_id\n" +
				"			AND coracc. STATUS = 'ACT'\n" +
				"			AND NOW() BETWEEN coracc.effective_date\n" +
				"			AND coracc.expiry_date\n" +
				"		)\n" +
				"   WHEN m.member_type ='MG' and m.member_type = 'HG' THEN \n" +
				"       (SELECT copc.effective_to from customer_order_permit_card copc WHERE copc.cardholder_customer_id = m.customer_id\n" +
				"           AND copc. STATUS = 'ACT'    \n" +
				"           AND NOW() BETWEEN copc.effective_from AND copc.effective_to ) \n" +
				"	ELSE\n" +
				"		NULL\n" +
				"	END AS expiryDate\n" +
				"	FROM\n" +
				"		member m\n" +
				"	LEFT JOIN customer_profile cp ON cp.customer_id = m.customer_id\n" +
				"	) t\n" +
				" WHERE\n" +
				"	1=1 <CheckedDayPass> ";
		
		
		String staffSql = "SELECT * FROM (SELECT\n" +
				"	CONCAT('T', ch.contractor_id) customerId,\n" +
				"	CONCAT(\n" +
				"		ch.given_name,\n" +
				"		' ',\n" +
				"		ch.surname\n" +
				"	) customerName,\n" +
				"	ch.gender,\n" +
				"	NULL AS age,\n" +
				"	ch.contact_email email,\n" +
				"	ch.phone_mobile phoneMobile,\n" +
				"	NULL AS planNo,\n" +
				"	'S' AS memberType,\n" +
				"	ch.`status`,\n" +
				"	ch.period_to AS expiryDate\n" +
				"FROM\n" +
				"	contract_helper ch ) t WHERE 1=1 \n" +
				"UNION";
		
		List<Serializable> param = new ArrayList<Serializable>();
		Boolean withConditon = false;
		if (!notificationDto.getSendToall()) {
			if (null != notificationDto.getMemberType() && notificationDto.getMemberType().length > 0) {
				withConditon = true;
				String tempStr = "";
				for (String memberType : notificationDto.getMemberType()) {
					switch (memberType) {
					case "COR":
						String subCor = "";
						if(notificationDto.getMemberCorType() != null && notificationDto.getMemberCorType().length >0){
							subCor = subCor + " ( ";
							for (String memberCorType : notificationDto.getMemberCorType()) {
								subCor = subCor + " t.memberType = ? or"; 
								param.add(memberCorType);
							}
							subCor = getSqlStr(subCor,"or");
							subCor = subCor + " ) and";
						}
						if(notificationDto.getMemberCorStatus()!=null && notificationDto.getMemberCorStatus().length >0){
							subCor = subCor + "  ( ";
							for (String memberCorStatus : notificationDto.getMemberCorStatus()) {
								subCor = subCor + " t.`status` = ? or";
								param.add(memberCorStatus);
							}
							subCor = getSqlStr(subCor,"or");
							subCor = subCor + " ) and";
						}
						if(subCor.trim().length()>0){
							subCor = getSqlStr(subCor,"and");
							tempStr = tempStr + " ("+subCor+") or ";
						}
						break;
					case "IND":
						String subInd = "";
						if(notificationDto.getMemberIndType()!=null && notificationDto.getMemberIndType().length>0){
							subInd = subInd + " ( ";
							for (String memberIndType : notificationDto.getMemberIndType()) {
								subInd = subInd + " t.memberType = ? or"; 
								param.add(memberIndType);
							}
							subInd = getSqlStr(subInd,"or");
							subInd = subInd + " ) and";
						}
						if(notificationDto.getMemberIndStatus()!=null && notificationDto.getMemberIndStatus().length>0){
							subInd = subInd + "  ( ";
							for (String memberIndStatus : notificationDto.getMemberIndStatus()) {
								subInd = subInd + " t.`status` = ? or";
								param.add(memberIndStatus);
							}
							subInd = getSqlStr(subInd,"or");
							subInd = subInd + " ) and";
						}
						if(subInd.trim().length()>0){
							subInd = getSqlStr(subInd,"and");
							tempStr = tempStr + " ("+subInd+") or ";
						}
						break;
					default:
						break;
					} 
				}
				if(tempStr.trim().length()>0){
					tempStr = getSqlStr(tempStr,"or");
					sql = sql + " and ( " + tempStr +" ) ";
				}
			}
			
			if (null != notificationDto.getServicePlan() && notificationDto.getServicePlan().length > 0) {
				withConditon = true;
				sql = sql + " and ( ";
				for (String servicePlan : notificationDto.getServicePlan()) {
					sql = sql + " t.planNo = ? or";
					param.add(servicePlan);
				}
				sql = getSqlStr(sql,"or");
				sql = sql + " ) ";
			}
		
			if (notificationDto.getDayPass()!=null && notificationDto.getDayPass()) {
				if(!withConditon){
					sql = sql.replace("<CheckedDayPass>"," and (t.memberType = 'MG' or t.memberType = 'HG') ");
					withConditon=true;
				}else{
					sql = sql.replace("<CheckedDayPass>"," ");
				}
			}else{
				sql = sql.replace("<CheckedDayPass>"," and t.memberType != 'MG' and t.memberType != 'HG' ");
			}
			
			if (notificationDto.getTempPass()!=null && notificationDto.getTempPass()) {
				if(!withConditon){
					sql = staffSql.replace("UNION", " ");
				}else{
					sql = sql.replace("<StaffUnionSql>", staffSql);
					sql = "select * from ( "+sql+" ) t where 1=1 ";
				}
				withConditon = true;
			}
			
			if (null != notificationDto.getGender() && notificationDto.getGender().length > 0) {
				withConditon = true;
				sql = sql + " and ( ";
				for (String gender : notificationDto.getGender()) {
					if(StringUtils.isEmpty(gender)){
						sql = sql + " t.gender = 'U' or";
					}else{
						sql = sql + " t.gender = ? or";
						param.add(gender);
					}
				}
				sql = getSqlStr(sql,"or");
				sql = sql + " ) ";
			}
			
			if (null != notificationDto.getAge() && notificationDto.getAge().length == 2 
					&& notificationDto.getAge()[0]!=null && notificationDto.getAge()[1]!=null) {
				withConditon = true;
				sql = sql + " and ( t.age >= ? and t.age <= ? )";
				param.add(notificationDto.getAge()[0]);
				param.add(notificationDto.getAge()[1]);
			}
			
			if (null != notificationDto.getExpireMonth()) {
				withConditon = true;
				if(notificationDto.getExpireMonth() == 0){
					sql = sql + " and ( TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) = ?)";
				}else{
					sql = sql + " and ( TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) < ? and TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) >0 )";
				}
				param.add(notificationDto.getExpireMonth());
			}
		}
		
		List<NotificationMemberDto> dtoList = null;
		if(!notificationDto.getSendToall() && !withConditon){
			return dtoList;
		}
		
		sql = sql.replace("<CheckedDayPass>", "");
		sql = sql.replace("<StaffUnionSql>", "");
		dtoList = this.getDtoBySql(sql, param, NotificationMemberDto.class);
		return dtoList;
	}
	
	private String getSqlStr(String sql,String regx){
		if(sql.trim().endsWith(regx)){
			int corIndex = sql.lastIndexOf(regx);
			sql = sql.substring(0, corIndex);
		}
		return sql;
	}

	@Override
	public void saveSMSRecord(NotificationDto notificationDto) throws Exception {
		// TODO Auto-generated method stub
		
	}

	//added by Kaster 20160407
	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationMemberDto> getNotificationMembers(NotificationDto notificationDto) {
		String sql = "<StaffUnionSql> SELECT\n" +
				"	*\n" +
				"FROM\n" +
				"	(\n" +
				"  SELECT cp.customer_id AS customerId,"
				+ " CONCAT(cp.given_name,' ',cp.surname) AS customerName,"
				+ " cp.gender AS gender,"
				+ " TIMESTAMPDIFF(YEAR,cp.date_of_birth,NOW()) AS age,"
				+ " cp.contact_email AS email,"
				+ " cp.phone_mobile AS phoneMobile,"
				+ " sp.plan_no AS planNo,"
				+ " m.member_type AS memberType,"
				+ " m.status,"
				+ " csa.expiry_date AS expiryDate "
				+ " FROM member m,customer_profile cp,customer_service_acc csa,customer_service_subscribe css,service_plan sp"
				+ " WHERE m.customer_id=cp.customer_id"
				+ " AND cp.customer_id=csa.customer_id"
				+ " AND csa.acc_no=css.acc_no"
				+ " AND css.service_plan_no=sp.plan_no"
				+ " AND NOW() BETWEEN csa.effective_date AND csa.expiry_date" +
				"	) t\n" +
				" WHERE\n" +
				"	1=1 <CheckedDayPass> ";
		
		
		String staffSql = "SELECT * FROM (SELECT\n" +
				"	CONCAT('T', ch.contractor_id) customerId,\n" +
				"	CONCAT(\n" +
				"		ch.given_name,\n" +
				"		' ',\n" +
				"		ch.surname\n" +
				"	) customerName,\n" +
				"	ch.gender,\n" +
				"	NULL AS age,\n" +
				"	ch.contact_email email,\n" +
				"	ch.phone_mobile phoneMobile,\n" +
				"	NULL AS planNo,\n" +
				"	'S' AS memberType,\n" +
				"	ch.`status`,\n" +
				"	ch.period_to AS expiryDate\n" +
				"FROM\n" +
				"	contract_helper ch ) t WHERE 1=1 \n" +
				"UNION";
		
		List<Serializable> param = new ArrayList<Serializable>();
		Boolean withConditon = false;
		if (!notificationDto.getSendToall()) {
			if (null != notificationDto.getMemberType() && notificationDto.getMemberType().length > 0) {
				withConditon = true;
				String tempStr = "";
				for (String memberType : notificationDto.getMemberType()) {
					switch (memberType) {
					case "COR":
						String subCor = "";
						if(notificationDto.getMemberCorType() != null && notificationDto.getMemberCorType().length >0){
							subCor = subCor + " ( ";
							for (String memberCorType : notificationDto.getMemberCorType()) {
								subCor = subCor + " t.memberType = ? or"; 
								param.add(memberCorType);
							}
							subCor = getSqlStr(subCor,"or");
							subCor = subCor + " ) and";
						}
						if(notificationDto.getMemberCorStatus()!=null && notificationDto.getMemberCorStatus().length >0){
							subCor = subCor + "  ( ";
							for (String memberCorStatus : notificationDto.getMemberCorStatus()) {
								subCor = subCor + " t.`status` = ? or";
								param.add(memberCorStatus);
							}
							subCor = getSqlStr(subCor,"or");
							subCor = subCor + " ) and";
						}
						if(subCor.trim().length()>0){
							subCor = getSqlStr(subCor,"and");
							tempStr = tempStr + " ("+subCor+") or ";
						}
						break;
					case "IND":
						String subInd = "";
						if(notificationDto.getMemberIndType()!=null && notificationDto.getMemberIndType().length>0){
							subInd = subInd + " ( ";
							for (String memberIndType : notificationDto.getMemberIndType()) {
								subInd = subInd + " t.memberType = ? or"; 
								param.add(memberIndType);
							}
							subInd = getSqlStr(subInd,"or");
							subInd = subInd + " ) and";
						}
						if(notificationDto.getMemberIndStatus()!=null && notificationDto.getMemberIndStatus().length>0){
							subInd = subInd + "  ( ";
							for (String memberIndStatus : notificationDto.getMemberIndStatus()) {
								subInd = subInd + " t.`status` = ? or";
								param.add(memberIndStatus);
							}
							subInd = getSqlStr(subInd,"or");
							subInd = subInd + " ) and";
						}
						if(subInd.trim().length()>0){
							subInd = getSqlStr(subInd,"and");
							tempStr = tempStr + " ("+subInd+") or ";
						}
						break;
					default:
						break;
					} 
				}
				if(tempStr.trim().length()>0){
					tempStr = getSqlStr(tempStr,"or");
					sql = sql + " and ( " + tempStr +" ) ";
				}
			}
			
			if (null != notificationDto.getServicePlan() && notificationDto.getServicePlan().length > 0) {
				withConditon = true;
				sql = sql + " and ( ";
				for (String servicePlan : notificationDto.getServicePlan()) {
					sql = sql + " t.planNo = ? or";
					param.add(servicePlan);
				}
				sql = getSqlStr(sql,"or");
				sql = sql + " ) ";
			}
		
			if (notificationDto.getDayPass()!=null && notificationDto.getDayPass()) {
				if(!withConditon){
					sql = sql.replace("<CheckedDayPass>"," and (t.memberType = 'MG' or t.memberType = 'HG') ");
					withConditon=true;
				}else{
					sql = sql.replace("<CheckedDayPass>"," ");
				}
			}else{
				sql = sql.replace("<CheckedDayPass>"," and t.memberType != 'MG' and t.memberType != 'HG' ");
			}
			
			if (notificationDto.getTempPass()!=null && notificationDto.getTempPass()) {
				if(!withConditon){
					sql = staffSql.replace("UNION", " ");
				}else{
					sql = sql.replace("<StaffUnionSql>", staffSql);
					sql = "select * from ( "+sql+" ) t where 1=1 ";
				}
				withConditon = true;
			}
			
			if (null != notificationDto.getGender() && notificationDto.getGender().length > 0) {
				withConditon = true;
				sql = sql + " and ( ";
				for (String gender : notificationDto.getGender()) {
					if(StringUtils.isEmpty(gender)){
						sql = sql + " t.gender = 'U' or";
					}else{
						sql = sql + " t.gender = ? or";
						param.add(gender);
					}
				}
				sql = getSqlStr(sql,"or");
				sql = sql + " ) ";
			}
			
			if (null != notificationDto.getAge() && notificationDto.getAge().length == 2 
					&& notificationDto.getAge()[0]!=null && notificationDto.getAge()[1]!=null) {
				withConditon = true;
				sql = sql + " and ( t.age >= ? and t.age <= ? )";
				param.add(notificationDto.getAge()[0]);
				param.add(notificationDto.getAge()[1]);
			}
			
			if (null != notificationDto.getExpireMonth()) {
				withConditon = true;
				if(notificationDto.getExpireMonth() == 0){
					sql = sql + " and ( TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) = ?)";
				}else{
					sql = sql + " and ( TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) < ? and TIMESTAMPDIFF(MONTH,NOW(),IFNULL(t.expiryDate,NOW())) >0 )";
				}
				param.add(notificationDto.getExpireMonth());
			}
		}
		
		List<NotificationMemberDto> dtoList = null;
		if(!notificationDto.getSendToall() && !withConditon){
			return dtoList;
		}
		
		sql = sql.replace("<CheckedDayPass>", "");
		sql = sql.replace("<StaffUnionSql>", "");
		dtoList = this.getDtoBySql(sql, param, NotificationMemberDto.class);
		return dtoList;
	}
	
	
	public String getPlanNo(Integer[] servicePlanNos){
		String strPlanNos = "";
		if(servicePlanNos!=null){
			StringBuffer planNos = new StringBuffer("(");
			for(int i=0;i<servicePlanNos.length;i++){
				planNos.append(servicePlanNos[i]).append(",");
			}
			//最后一个逗号的位置
			int indexOfLastComma = planNos.lastIndexOf(",");
			
			strPlanNos = planNos.substring(0, indexOfLastComma);
			strPlanNos += ")";
		}
		return strPlanNos;
	}
	/**
	 * added by Kaster 20160513
	 * @param notificationDto
	 */
	@Override
	public List<NotificationMemberDto> getRecipients(NotificationDto notificationDto){
		//Corporate Service Plan
		Integer[] corporatePlanNos = notificationDto.getCorporatePlanNos();
		
		//Individual Service Plan
		Integer[] individualPlanNos = notificationDto.getIndividualPlanNos();
		
		//Marketing/General，默认为Marketing
		String promotionType = notificationDto.getPromotionType();
		
		//Patron/DayPassGuest
		String recipient = notificationDto.getRecipient();
		
		//Primary/Dependent/Both
		String corporateIsPrimaryOrDependent = notificationDto.getCorporateIsPrimaryOrDependent();
		
		//Active/Inactive/Both
		String corporateIsActiveOrNot = notificationDto.getCorporateIsActiveOrNot();
		
		//Primary/Dependent/Both
		String individualIsPrimaryOrDependent = notificationDto.getIndividualIsPrimaryOrDependent();
		
		//Active/Inactive/Both
		String individualIsActiveOrNot = notificationDto.getIndividualIsActiveOrNot();
		
		//Active/Inactive/Both
//		String dayPassGuestIsActiveOrNot = notificationDto.getDayPassGuestIsActiveOrNot();
		
		//Expiring Period
		Integer expiringPeriod = notificationDto.getExpiringPeriod();
		
		//Gender:M/F/Both
		String gender = notificationDto.getGenders();
		
		//Age
		Integer[] age = notificationDto.getAges(); 
		
		Boolean sendToall = notificationDto.getSendToall();
		
		String finalSqlForCorporate = "";
		String finalSqlForIndividual = "";
		String finalSqlForDayPassGuest = "";
		String finalSqlForSendToAll = "";
		
		List<NotificationMemberDto> corporateMemberList = new ArrayList<NotificationMemberDto>();
		List<NotificationMemberDto> individualMemberList = new ArrayList<NotificationMemberDto>();
		List<NotificationMemberDto> allMemberList = new ArrayList<NotificationMemberDto>();
		List<NotificationMemberDto> dayPassMemberList = new ArrayList<NotificationMemberDto>();
		
		String sqlForHKGTAMarketing = "";
		if("Marketing".equals(promotionType)){
			sqlForHKGTAMarketing = " AND not exists(select 1 from customer_addition_info info, customer_addition_info_caption cap "
					+ " where cap.caption_id=info.caption_id AND cap.caption='HKGTA Marketing' AND info.customer_input = 'No' "
					+ " and tt.customerId=info.customer_id)";
		}
		
		if(sendToall){
			//这种情况，前端传递过来的service plan是全部，corporate和individual是一样的。
			StringBuffer planNos = new StringBuffer("(");
			for(int i=0;i<corporatePlanNos.length;i++){
				planNos.append(corporatePlanNos[i]).append(",");
			}
			
			for(int i=0;i<individualPlanNos.length;i++){
				planNos.append(individualPlanNos[i]).append(",");
			}
			//最后一个逗号的位置
			int indexOfLastComma = planNos.lastIndexOf(",");
			String strPlanNos = planNos.substring(0, indexOfLastComma);
			strPlanNos += ")";
			
			String sqlForPrimary = " SELECT DISTINCT(m.customer_id) "
					+" FROM member m, customer_service_acc acc " 
					+" WHERE m.customer_id=acc.customer_id "
					+" 	AND acc.acc_no IN (SELECT acc_no FROM customer_service_subscribe WHERE service_plan_no IN "+strPlanNos+") ";
			
			String sqlForDependent = " SELECT customer_id " 
					+" FROM member "
					+" WHERE superior_member_id IN ( "
					+" 	SELECT DISTINCT(m.customer_id) " 
					+" 	FROM member m, customer_service_acc acc " 
					+" 	WHERE m.customer_id=acc.customer_id "
					+" 		AND acc.acc_no IN (SELECT acc_no FROM customer_service_subscribe WHERE service_plan_no IN "+strPlanNos+") "
					+" 	) ";
			
			String sqlForMGHG = " SELECT customer_id FROM member WHERE member_type IN ('MG','HG') ";
			
			//三种类型的用户资料用union连接起来。
			finalSqlForSendToAll = " SELECT m.customer_id AS customerId, "
					+" CONCAT(cp.given_name,' ',cp.surname) AS customerName, "
					+" cp.gender AS gender, "
					+" TIMESTAMPDIFF(YEAR,cp.date_of_birth,NOW()) AS age, "
					+" cp.contact_email AS email, "
					+" cp.phone_mobile AS phoneMobile, "
					+" m.member_type AS memberType, "
					+" m.status, "
					+" -1 AS planNo, "
					+" now() AS expiryDate "
				+" FROM member m,customer_profile cp "
				+" WHERE m.customer_id=cp.customer_id "
					+" AND m.customer_id IN ("+sqlForMGHG+") "
				+" UNION "
				+" SELECT m.customer_id AS customerId, "
						+" CONCAT(cp.given_name,' ',cp.surname) AS customerName, "
						+" cp.gender AS gender, "
						+" TIMESTAMPDIFF(YEAR,cp.date_of_birth,NOW()) AS age, "
						+" cp.contact_email AS email, "
						+" cp.phone_mobile AS phoneMobile, "
						+" m.member_type AS memberType, "
						+" m.status, "
						+" sub.service_plan_no AS planNo, "
						+" acc.expiry_date AS expiryDate "
					+" FROM member m,customer_profile cp,customer_service_acc acc,customer_service_subscribe sub "
					+" WHERE m.customer_id=cp.customer_id "
						+" AND acc.customer_id=m.customer_id "
						+" AND acc.acc_no=sub.acc_no "
						+" AND m.customer_id IN ("+sqlForPrimary+") "
					+" GROUP BY m.customer_id "
					+" HAVING acc.expiry_date=MAX(acc.expiry_date) "
				+" UNION "
				+" SELECT m.customer_id AS customerId, "
					+" CONCAT(cp.given_name,' ',cp.surname) AS customerName, "
					+" cp.gender AS gender, "
					+" TIMESTAMPDIFF(YEAR,cp.date_of_birth,NOW()) AS age, "
					+" cp.contact_email AS email, "
					+" cp.phone_mobile AS phoneMobile, "
					+" m.member_type AS memberType, "
					+" m.status, "
					+" sub.service_plan_no AS planNo, "
					+" acc.expiry_date AS expiryDate "
				+" FROM member m,customer_profile cp,customer_service_acc acc,customer_service_subscribe sub "
				+" WHERE m.customer_id=cp.customer_id "
					+" AND acc.customer_id=m.superior_member_id "
					+" AND acc.acc_no=sub.acc_no "
					+" AND m.customer_id IN ("+sqlForDependent+") "
				+" GROUP BY m.customer_id "
				+" HAVING acc.expiry_date=MAX(acc.expiry_date) ";
			
			
			finalSqlForSendToAll =  "SELECT tt.* FROM (" + finalSqlForSendToAll + ") tt WHERE " + (sqlForHKGTAMarketing.equals("")?" 1=1 ":sqlForHKGTAMarketing.substring(4));
			
			allMemberList = this.getDtoBySql(finalSqlForSendToAll, null, NotificationMemberDto.class);
			return allMemberList;
		}
		
		//Recipients为Patron，即member的member_type为IPM/IDM/CPM/CDM
		if(recipient.equals("Patron")){
			//查询Corporate member
			if(corporatePlanNos!=null && corporatePlanNos.length!=0 && corporateIsActiveOrNot!=null && !corporateIsActiveOrNot.equals("") && corporateIsPrimaryOrDependent!=null && !corporateIsPrimaryOrDependent.equals("")){
				finalSqlForCorporate = buildSql(corporatePlanNos, "Patron", "Corporate", expiringPeriod, corporateIsActiveOrNot, promotionType, corporateIsPrimaryOrDependent);
				if(finalSqlForCorporate != null){
					//加上年龄条件
					finalSqlForCorporate = " SELECT tt.* FROM(" + finalSqlForCorporate + ") tt WHERE tt.age BETWEEN "+age[0]+" AND "+age[1]+" ";
					//加上性别条件
					if(gender.equals("M")){
						finalSqlForCorporate = finalSqlForCorporate + " AND tt.gender='M' ";
					}else if(gender.equals("F")){
						finalSqlForCorporate = finalSqlForCorporate + " AND tt.gender='F' ";
					}
					//加上active条件
					if(corporateIsActiveOrNot.equals("Active")){
						finalSqlForCorporate = finalSqlForCorporate + " AND tt.status='ACT' ";
					}else if(corporateIsActiveOrNot.equals("Inactive")){
						finalSqlForCorporate = finalSqlForCorporate + " AND tt.status='NACT' ";
					}

					finalSqlForCorporate = finalSqlForCorporate + " AND tt.planNo in "+getPlanNo(corporatePlanNos);
					
					finalSqlForCorporate = finalSqlForCorporate + sqlForHKGTAMarketing;
					corporateMemberList = this.getDtoBySql(finalSqlForCorporate, null, NotificationMemberDto.class);
				}
			}
			
			//查询Individual member
			if(individualPlanNos!=null && individualPlanNos.length!=0 && individualIsActiveOrNot!=null && !individualIsActiveOrNot.equals("") && individualIsPrimaryOrDependent!=null && !individualIsPrimaryOrDependent.equals("")){
				finalSqlForIndividual = buildSql(individualPlanNos, "Patron", "Individual", expiringPeriod, individualIsActiveOrNot, promotionType, individualIsPrimaryOrDependent);
				if(finalSqlForIndividual != null){
					//加上年龄条件
					finalSqlForIndividual = " SELECT tt.* FROM(" + finalSqlForIndividual + ") tt WHERE tt.age BETWEEN "+age[0]+" AND "+age[1]+" ";
					//加上性别条件
					if(gender.equals("M")){
						finalSqlForIndividual = finalSqlForIndividual + " AND tt.gender='M' ";
					}else if(gender.equals("F")){
						finalSqlForIndividual = finalSqlForIndividual + " AND tt.gender='F' ";
					}
					//加上active条件
					if(individualIsActiveOrNot.equals("Active")){
						finalSqlForIndividual = finalSqlForIndividual + " AND tt.status='ACT' ";
					}else if(individualIsActiveOrNot.equals("Inactive")){
						finalSqlForIndividual = finalSqlForIndividual + " AND tt.status='NACT' ";
					}
					finalSqlForIndividual = finalSqlForIndividual + " AND tt.planNo in "+getPlanNo(individualPlanNos);
					
					finalSqlForIndividual = finalSqlForIndividual + sqlForHKGTAMarketing;
					individualMemberList = this.getDtoBySql(finalSqlForIndividual, null, NotificationMemberDto.class);
				}
			}
			
			allMemberList.addAll(corporateMemberList);
			allMemberList.addAll(individualMemberList);
			return allMemberList;
		}else if(recipient.equalsIgnoreCase("DayPassGuest")){
			//Recipients为Day Pass Guest，即member的member_type为MG/HG
			finalSqlForDayPassGuest = buildSql(null, "DayPassGuest", "DayPassGuest", null, null, promotionType, "DayPassGuest");
			if(finalSqlForDayPassGuest != null){
				//加上年龄条件
				finalSqlForDayPassGuest = " SELECT tt.* FROM(" + finalSqlForDayPassGuest + ") tt WHERE tt.age BETWEEN "+age[0]+" AND "+age[1]+" ";
				//加上性别条件
				if(gender.equals("M")){
					finalSqlForDayPassGuest = finalSqlForDayPassGuest + " AND tt.gender='M' ";
				}else if(gender.equals("F")){
					finalSqlForDayPassGuest = finalSqlForDayPassGuest + " AND tt.gender='F' ";
				}
				
				finalSqlForDayPassGuest = finalSqlForDayPassGuest + sqlForHKGTAMarketing;
				dayPassMemberList = this.getDtoBySql(finalSqlForDayPassGuest, null, NotificationMemberDto.class);
			}

			return dayPassMemberList;
		}else{
		//Both，Patron和DayPassGuest都有。
			//查询Corporate member
			if(corporatePlanNos!=null && corporatePlanNos.length!=0 && corporateIsActiveOrNot!=null && !corporateIsActiveOrNot.equals("") && corporateIsPrimaryOrDependent!=null && !corporateIsPrimaryOrDependent.equals("")){
				finalSqlForCorporate = buildSql(corporatePlanNos, "Patron", "Corporate", expiringPeriod, corporateIsActiveOrNot, promotionType, corporateIsPrimaryOrDependent);
				if(finalSqlForCorporate != null){
					//加上年龄条件
					finalSqlForCorporate = " SELECT tt.* FROM(" + finalSqlForCorporate + ") tt WHERE tt.age BETWEEN "+age[0]+" AND "+age[1]+" ";
					//加上性别条件
					if(gender.equals("M")){
						finalSqlForCorporate = finalSqlForCorporate + " AND tt.gender='M' ";
					}else if(gender.equals("F")){
						finalSqlForCorporate = finalSqlForCorporate + " AND tt.gender='F' ";
					}
					//加上active条件
					if(corporateIsActiveOrNot.equals("Active")){
						finalSqlForCorporate = finalSqlForCorporate + " AND tt.status='ACT' ";
					}else if(corporateIsActiveOrNot.equals("Inactive")){
						finalSqlForCorporate = finalSqlForCorporate + " AND tt.status='NACT' ";
					}
					finalSqlForCorporate = finalSqlForCorporate + " AND tt.planNo in "+getPlanNo(corporatePlanNos);
					
					finalSqlForCorporate = finalSqlForCorporate + sqlForHKGTAMarketing;
					corporateMemberList = this.getDtoBySql(finalSqlForCorporate, null, NotificationMemberDto.class);
				}
			}
			
			//查询Individual member
			if(individualPlanNos!=null && individualPlanNos.length!=0 && individualIsActiveOrNot!=null && !individualIsActiveOrNot.equals("") && individualIsPrimaryOrDependent!=null && !individualIsPrimaryOrDependent.equals("")){
				finalSqlForIndividual = buildSql(individualPlanNos, "Patron", "Individual", expiringPeriod, individualIsActiveOrNot, promotionType, individualIsPrimaryOrDependent);
				if(finalSqlForIndividual != null){
					//加上年龄条件
					finalSqlForIndividual = " SELECT tt.* FROM(" + finalSqlForIndividual + ") tt WHERE tt.age BETWEEN "+age[0]+" AND "+age[1]+" ";
					//加上性别条件
					if(gender.equals("M")){
						finalSqlForIndividual = finalSqlForIndividual + " AND tt.gender='M' ";
					}else if(gender.equals("F")){
						finalSqlForIndividual = finalSqlForIndividual + " AND tt.gender='F' ";
					}
					//加上active条件
					if(individualIsActiveOrNot.equals("Active")){
						finalSqlForIndividual = finalSqlForIndividual + " AND tt.status='ACT' ";
					}else if(individualIsActiveOrNot.equals("Inactive")){
						finalSqlForIndividual = finalSqlForIndividual + " AND tt.status='NACT' ";
					}
					finalSqlForIndividual = finalSqlForIndividual + " AND tt.planNo in "+getPlanNo(individualPlanNos);
					
					finalSqlForIndividual = finalSqlForIndividual + sqlForHKGTAMarketing;
					individualMemberList = this.getDtoBySql(finalSqlForIndividual, null, NotificationMemberDto.class);
				}
			}
			
			allMemberList.addAll(corporateMemberList);
			allMemberList.addAll(individualMemberList);
			
			//查Day Pass Guest的用户
			finalSqlForDayPassGuest = buildSql(null, "DayPassGuest", "DayPassGuest", null, null, promotionType, "DayPassGuest");
			if(finalSqlForDayPassGuest != null){
				//加上年龄条件
				finalSqlForDayPassGuest = " SELECT tt.* FROM(" + finalSqlForDayPassGuest + ") tt WHERE tt.age BETWEEN "+age[0]+" AND "+age[1]+" ";
				//加上性别条件
				if(gender.equals("M")){
					finalSqlForDayPassGuest = finalSqlForDayPassGuest + " AND tt.gender='M' ";
				}else if(gender.equals("F")){
					finalSqlForDayPassGuest = finalSqlForDayPassGuest + " AND tt.gender='F' ";
				}
				
				finalSqlForDayPassGuest = finalSqlForDayPassGuest + sqlForHKGTAMarketing;
				dayPassMemberList = this.getDtoBySql(finalSqlForDayPassGuest, null, NotificationMemberDto.class);
			}
			
			allMemberList.addAll(dayPassMemberList);			

			return allMemberList;			
		}
	}
	
	/**
	 * added by Kaster 20160513
	 * @param servicePlanNos
	 * @param recipient
	 * @param corporateOrIndividual
	 * @param expiringPeriod
	 * @param activeOrNot
	 * @param promotionType
	 * @param primaryOrDependent
	 * @return
	 */
	private String buildSql(Integer[] servicePlanNos, String recipient, String corporateOrIndividual, Integer expiringPeriod, String activeOrNot, String promotionType, String primaryOrDependent){
		//没有选择任何service plan且不是DayPassGuest。需求：不选service plan，不查任何数据。
		if((servicePlanNos==null || servicePlanNos.length==0) && !recipient.equals("DayPassGuest")){
			return null;
		}
		
		String finalSql = "";
		String strPlanNos = "";
		if(servicePlanNos!=null){
			StringBuffer planNos = new StringBuffer("(");
			for(int i=0;i<servicePlanNos.length;i++){
				planNos.append(servicePlanNos[i]).append(",");
			}
			//最后一个逗号的位置
			int indexOfLastComma = planNos.lastIndexOf(",");
			
			strPlanNos = planNos.substring(0, indexOfLastComma);
			strPlanNos += ")";
		}
		
		//查询购买了指定service plan的primary member的customer id的SQL
		String sqlPrimaryMember = "";
		
		//查询上述primary member对应的dependent member的customer id的SQL
		String sqlDependentMember = "";
		
		//查询primary和dependent member的customer id的SQL
		String sqlBothPrimaryAndDependentMember = "";
		
		//查询Day Pass Guest对应的member的customer id的SQL
		String sqlDayPassGuest = "";
		
		if(recipient.equals("Patron")){
			//Patron——member_type:CPM/CDM/IPM/IDM
			if(corporateOrIndividual.equals("Corporate")){
				sqlPrimaryMember = " SELECT DISTINCT(m.customer_id) FROM member m, customer_service_acc acc WHERE m.customer_id=acc.customer_id AND m.member_type IN ('CPM','CDM') AND acc.acc_no IN (SELECT acc_no FROM customer_service_subscribe WHERE service_plan_no IN "+strPlanNos+") ";
			}else if(corporateOrIndividual.equals("Individual")){
				sqlPrimaryMember = " SELECT DISTINCT(m.customer_id) FROM member m, customer_service_acc acc WHERE m.customer_id=acc.customer_id AND m.member_type IN ('IPM','IDM') AND acc.acc_no IN (SELECT acc_no FROM customer_service_subscribe WHERE service_plan_no IN "+strPlanNos+") ";
			}
			
			sqlDependentMember = " SELECT customer_id FROM member WHERE superior_member_id IN ("+sqlPrimaryMember+") ";
			
			sqlBothPrimaryAndDependentMember = sqlPrimaryMember + " UNION " + sqlDependentMember;
		}else if(recipient.equals("DayPassGuest")){
			//DayPassGuest——member_type:MG和HG
			sqlDayPassGuest = " SELECT customer_id FROM member m WHERE member_type IN ('MG','HG') ";
		}
		
		
		//Expiring Period
		if(expiringPeriod!=null && expiringPeriod>0){
			sqlPrimaryMember += " AND DATE_FORMAT(NOW(),'%Y-%m-%d')>=DATE_ADD(acc.expiry_date,INTERVAL -"+expiringPeriod+" MONTH) AND NOW()<acc.expiry_date ";
			sqlDependentMember = " SELECT customer_id FROM member WHERE superior_member_id IN ("+sqlPrimaryMember+") ";
			sqlBothPrimaryAndDependentMember = sqlPrimaryMember + " UNION " + sqlDependentMember;
		}
		
//		if(activeOrNot!=null){
//			if(activeOrNot.equals("Active")){
//				//查Active的member的customer id的SQL
//				sqlPrimaryMember += " AND m.status='ACT' ";
//				sqlDependentMember = " SELECT customer_id FROM member WHERE superior_member_id IN ("+sqlPrimaryMember+") ";
//				sqlBothPrimaryAndDependentMember = sqlPrimaryMember + " UNION " + sqlDependentMember;
//				
////				sqlDayPassGuest += " AND m.status='ACT' ";
//			}else if(activeOrNot.equals("Inactive")){
//				//查Inactive的member的customer id的SQL
//				sqlPrimaryMember += " AND m.status='NACT' ";
//				sqlDependentMember = " SELECT customer_id FROM member WHERE superior_member_id IN ("+sqlPrimaryMember+") ";
//				sqlBothPrimaryAndDependentMember = sqlPrimaryMember + " UNION " + sqlDependentMember;
//				
////				sqlDayPassGuest += " AND m.status='NACT' ";
//			}else{
//				//Active和Inactive的member都查，则不需增加过滤条件
//			}
//		}
		
		//promotionType默认是Marketing
		if(promotionType==null){
			promotionType = "Marketing";
		}
		if(promotionType.equals("Marketing")){
			sqlPrimaryMember = " SELECT t.customer_id FROM ("+sqlPrimaryMember+") t ";
			sqlDependentMember = " SELECT customer_id FROM member m WHERE superior_member_id IN ("+sqlPrimaryMember+")";
			sqlBothPrimaryAndDependentMember = sqlPrimaryMember + " UNION " + sqlDependentMember;
			
			sqlDayPassGuest = " SELECT t.customer_id FROM ("+sqlDayPassGuest+") t ";
		}
		
		if(primaryOrDependent.equals("Primary")){
			finalSql = " SELECT m.customer_id AS customerId, "
								+" CONCAT(cp.given_name,' ',cp.surname) AS customerName, "
								+" cp.gender AS gender, "
								+" TIMESTAMPDIFF(YEAR,cp.date_of_birth,NOW()) AS age, "
								+" cp.contact_email AS email, "
								+" cp.phone_mobile AS phoneMobile, "
								+" m.member_type AS memberType, "
								+" m.status, "
								+" sub.service_plan_no AS planNo, "
								+" acc.expiry_date AS expiryDate "
							+" FROM member m,customer_profile cp,customer_service_acc acc,customer_service_subscribe sub "
							+" WHERE m.customer_id=cp.customer_id "
								+" AND acc.customer_id=m.customer_id "
								+" AND acc.acc_no=sub.acc_no "
								+" AND m.customer_id IN ("+sqlPrimaryMember+") "
							+" GROUP BY m.customer_id "
							+" HAVING acc.expiry_date=MAX(acc.expiry_date) ";
		}else if(primaryOrDependent.equals("Dependent")){
			//Dependent member在customer_service_acc中没有记录，所以查询对应的Primary member的数据。
			//其中一个过滤条件变成了 acc.customer_id=m.superior_member_id 
			finalSql = " SELECT m.customer_id AS customerId, "
					+" CONCAT(cp.given_name,' ',cp.surname) AS customerName, "
					+" cp.gender AS gender, "
					+" TIMESTAMPDIFF(YEAR,cp.date_of_birth,NOW()) AS age, "
					+" cp.contact_email AS email, "
					+" cp.phone_mobile AS phoneMobile, "
					+" m.member_type AS memberType, "
					+" m.status, "
					+" sub.service_plan_no AS planNo, "
					+" acc.expiry_date AS expiryDate "
				+" FROM member m,customer_profile cp,customer_service_acc acc,customer_service_subscribe sub "
				+" WHERE m.customer_id=cp.customer_id "
					+" AND acc.customer_id=m.superior_member_id "
					+" AND acc.acc_no=sub.acc_no "
					+" AND m.customer_id IN ("+sqlDependentMember+") "
				+" GROUP BY m.customer_id "
				+" HAVING acc.expiry_date=MAX(acc.expiry_date) ";
		}else if(primaryOrDependent.equals("Both")){
			//Both:其中一个过滤条件变成了(acc.customer_id=m.superior_member_id OR acc.customer_id=m.customer_id)
			finalSql = " SELECT m.customer_id AS customerId, "
					+" CONCAT(cp.given_name,' ',cp.surname) AS customerName, "
					+" cp.gender AS gender, "
					+" TIMESTAMPDIFF(YEAR,cp.date_of_birth,NOW()) AS age, "
					+" cp.contact_email AS email, "
					+" cp.phone_mobile AS phoneMobile, "
					+" m.member_type AS memberType, "
					+" m.status, "
					+" sub.service_plan_no AS planNo, "
					+" acc.expiry_date AS expiryDate "
				+" FROM member m,customer_profile cp,customer_service_acc acc,customer_service_subscribe sub "
				+" WHERE m.customer_id=cp.customer_id "
					+" AND (acc.customer_id=m.superior_member_id OR acc.customer_id=m.customer_id) "
					+" AND acc.acc_no=sub.acc_no "
					+" AND sub.service_plan_no IN  " + strPlanNos
					
					+" AND m.customer_id IN ("+sqlBothPrimaryAndDependentMember+") "
				+" GROUP BY m.customer_id "
				+" HAVING acc.expiry_date=MAX(acc.expiry_date) ";
		}else{
			//DayPassGuest
			finalSql = " SELECT m.customer_id AS customerId, "
					+" CONCAT(cp.given_name,' ',cp.surname) AS customerName, "
					+" cp.gender AS gender, "
					+" TIMESTAMPDIFF(YEAR,cp.date_of_birth,NOW()) AS age, "
					+" cp.contact_email AS email, "
					+" cp.phone_mobile AS phoneMobile, "
					+" m.member_type AS memberType, "
					+" m.status, "
					+" -1 AS planNo, "
					+" now() AS expiryDate "
				+" FROM member m,customer_profile cp "
				+" WHERE m.customer_id=cp.customer_id "
					+" AND m.customer_id IN ("+sqlDayPassGuest+") ";
		}
		
		return finalSql;
	}
}
