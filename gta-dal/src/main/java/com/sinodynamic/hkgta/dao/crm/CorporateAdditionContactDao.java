package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CorporateAdditionContact;

public interface CorporateAdditionContactDao extends IBaseDao<CorporateAdditionContact>{

	public List<CorporateAdditionContact> getCorporateAdditionContactByCorporateId(Long corporateId);
	
	public CorporateAdditionContact getByContactId(Long contactId);

}
