package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilitySubType;

@Repository
public class FacilitySubTypeDaoImpl extends GenericDao<FacilitySubType> implements FacilitySubTypeDao {

	@Override
	public List<FacilitySubType> getFacilitySubType(String facilityType,String subType)
	{
		StringBuffer hql = new StringBuffer(" from FacilitySubType where facilityTypeCode = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType);
		if(!StringUtils.isEmpty(subType)){
			hql.append(" and subTypeId = ? ");
			paramList.add(subType);
		}
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public List<FacilitySubType> getFacilitySubType(String facilityType)
	{
		StringBuffer hql = new StringBuffer(" from FacilitySubType where facilityTypeCode = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType);
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public FacilitySubType getById(String facilitySubtypeId) {
		return this.get(FacilitySubType.class, facilitySubtypeId);
	}
}
