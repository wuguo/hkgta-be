package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.MemberType;

@Repository
public class HomePageSummaryDaoImpl extends GenericDao<String> implements HomePageSummaryDao {
	
	
	/* This function is used for counting total number of customers */
	public int countCustomerProfile() throws HibernateException {
		//String sql = "select count(*) from customer_profile c, customer_enrollment ce left join member m on m.customer_id = ce.customer_id and m.member_type != ? and m.member_type != ? and m.member_type != ? where c.customer_id =  ce.customer_id";
		
		String sql=" SELECT count(*) FROM customer_profile p INNER JOIN customer_enrollment e  ON  p.customer_id = e.customer_id AND (p.is_deleted != 'Y' OR p.is_deleted IS NULL)"+ 
				  "  LEFT JOIN member m ON m.customer_id = p.customer_id WHERE  ((m.member_type IS NULL) OR m.member_type ='IPM' ) AND e.status = 'OPN'";
//		List<Serializable> param = new ArrayList<Serializable>();
//		param.add(MemberType.CPM.name());
//		param.add(MemberType.MG.name());
//		param.add(MemberType.HG.name());
		Object customer = getUniqueBySQL(sql, null);
		int countCus = ((Number)customer).intValue();

		return countCus;

	}	 

	
	/* This function is used for counting current sales man's clients whose member status is active*/
	public int countEnrollmentsWithActiveMembers(String staffId) throws HibernateException {
		
		String hql = "select count(*) from CustomerEnrollment ce, Member m where m.customerId = ce.customerId and m.memberType = ? and (ce.status = 'ANC' or ce.status = 'CMP') and ce.salesFollowBy = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(MemberType.IPM.name());
		param.add(staffId);
		Object customer = getUniqueByHql(hql,param);
		int countEnroll = ((Number)customer).intValue();

		return countEnroll;		 

	}

	
	/* This function is used for counting number of presentations*/
	public int countPresentation() throws HibernateException {
		String hql = "select count(*) from PresentationBatch";

		Object presentations = getUniqueByHql(hql);
		int countPre = ((Number)presentations).intValue();

		return countPre;

	}


	/* This function is used for counting expiring members within 30 days */
	public int countRenewal() throws HibernateException {

		Calendar cal = Calendar.getInstance();
		Date date = new Date();
		cal.setTime(date);
		cal.add(Calendar.DATE, 30);
		java.sql.Date fromDate = new java.sql.Date(date.getTime());
		java.sql.Date toDate = new java.sql.Date(cal.getTime().getTime());
		String sql = "select count(*) from CustomerServiceAcc where status = 'ACT' and accCat = 'IDV' and expiryDate between '" + fromDate + "' and '" + toDate + "'";

		Object presentations = getUniqueByHql(sql);
		int countExp = ((Number)presentations).intValue();

		return countExp;

	}

	
	/* This function is used for counting pending payment */
	public BigDecimal countBalanceDue(Long orderNo) throws HibernateException {
		String sql1 = "select orderTotalAmount from CustomerOrderHd c where c.orderNo = "+ orderNo;
		//c.status = 'SUC' or c.status = null or c.status = ''
		String sql2 = "select sum(paidAmount) from CustomerOrderTrans c where c.customerOrderHd.orderNo = " + orderNo + " and (c.status = 'SUC' ) group by c.customerOrderHd.orderNo";
		BigDecimal orderTotalAmount = (BigDecimal) getUniqueByHql(sql1);
		BigDecimal orderTotalPaid = (BigDecimal) getUniqueByHql(sql2);
		if(CommUtil.num(orderTotalPaid) == 0 ){
			orderTotalPaid = BigDecimal.valueOf(0);
		}
		if (orderTotalAmount != null && orderTotalPaid != null) {
			return orderTotalAmount.subtract(orderTotalPaid);
		}
		else if (orderTotalAmount != null && orderTotalPaid == null) {
			return orderTotalAmount;
		}
		return BigDecimal.ZERO;
	}
	
	
	public int countPendingPayment() throws HibernateException {
		String hql = "select orderNo from CustomerOrderHd";
		List<Long> orderList = excuteByHql(Long.class, hql);
		int sum = 0;
		for (Long orderNo : orderList) {
			if(countBalanceDue(orderNo).signum() >0){
				sum++;
			}
		}
		return sum;
		
	}
	
	/* This function is used for count the unread remark for enrollment */
	public Boolean countEnrollmentRemark(String usedId){
		
//		String sql =    "SELECT DISTINCT\n" +
//				"	count(remark.stage_id)\n" +
//				"FROM\n" +
//				"  customer_profile cp,\n" +
//				"	customer_enrollment ce,\n" +
//				"	member m,\n" +
//				"  customer_pre_enroll_stage remark,\n" +
//				"  staff_master sm\n"+
//				"WHERE\n" +
//				"	m.customer_id = ce.customer_id\n" +
//				"AND m.member_type = ?\n" +
//				"AND ce.sales_follow_by = ?\n" +
//				"AND cp.customer_id = ce.customer_id\n" +
//				"AND (cp.is_deleted != 'Y' OR cp.is_deleted IS NULL)\n" +
//				"AND ce.status != 'OPN'\n" +
//				"AND remark.customer_id = cp.customer_id\n" +
//				"AND remark.comment_status = ?\n"+
//				"AND remark.comment_by !=?\n"+
//				"AND sm.user_id = remark.comment_by\n"+
//				"AND sm.position_title_code = ? ";
		String sql = "SELECT DISTINCT\n" +
				"	count(remark.stage_id)\n" +
				"FROM\n" +
				"	customer_profile cp,\n" +
				"	customer_enrollment ce,\n" +
				"	member m,\n" +
				"	customer_pre_enroll_stage remark\n" +
				"WHERE\n" +
				"	m.customer_id = ce.customer_id\n" +
				"AND m.member_type = ?\n" +
				"AND cp.customer_id = ce.customer_id\n" +
				"AND (\n" +
				"	cp.is_deleted != 'Y'\n" +
				"	OR cp.is_deleted IS NULL\n" +
				")\n" +
				"AND ce. STATUS != 'OPN'\n" +
				"AND remark.customer_id = cp.customer_id\n" +
				"AND remark.comment_status = ?\n" +
				"AND remark.comment_by_device = 'PC'\n" +
				"AND remark.sales_user_id = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(MemberType.IPM.name());
		param.add("NR");
		param.add(usedId);
		Object remark = getUniqueBySQL(sql,param);
		int countEnrollRemark = ((Number)remark).intValue();

		if(countEnrollRemark>0){
			return true;
		}else{
			return false;
		}
	}
	
	/* This function is used for count the unread remark for leads */
	public Boolean countLeadRemark(String usedId){
		
		String sql =    "SELECT\n" +
				"	COUNT(*)\n" +
				"FROM\n" +
				"  customer_pre_enroll_stage remark,\n" +
				"	customer_profile cp,\n" +
				"	customer_enrollment ce\n" +
				"LEFT JOIN member m ON m.customer_id = ce.customer_id\n" +
				"WHERE cp.customer_id = ce.customer_id\n" +
				"AND ((ce. STATUS = 'OPN' AND m.member_type IS NULL) OR m.member_type = 'IPM')\n" +
				"AND ce.sales_follow_by = ?\n" +
				"AND (cp.is_deleted != 'Y' OR cp.is_deleted IS NULL)\n" +
				"AND remark.customer_id = cp.customer_id\n" +
				"AND remark.comment_status = ?";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(usedId);
		param.add("NR");
		Object remark = getUniqueBySQL(sql,param);
		int countLeadRemark = ((Number)remark).intValue();

		if(countLeadRemark>0){
			return true;
		}else{
			return false;
		}
	}
	
	/* This function is used for count the unread remark for renewal(Pending)*/
	public Boolean countRenewalRemark(String usedId){
		
		String sql =    "SELECT\n" +
				"	COUNT(*)\n" +
				"FROM\n" +
				"  customer_pre_enroll_stage remark,\n" +
				"	customer_profile cp,\n" +
				"	customer_enrollment ce\n" +
				"LEFT JOIN member m ON m.customer_id = ce.customer_id\n" +
				"WHERE cp.customer_id = ce.customer_id\n" +
				"AND ((ce. STATUS = 'OPN' AND m.member_type IS NULL) OR m.member_type = 'IPM')\n" +
				"AND ce.sales_follow_by = ?\n" +
				"AND (cp.is_deleted != 'Y' OR cp.is_deleted IS NULL)\n" +
				"AND remark.customer_id = cp.customer_id\n" +
				"AND remark.comment_status = ?";
		
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(usedId);
		param.add("NR");
		Object remark = getUniqueBySQL(sql,param);
		int countLeadRemark = ((Number)remark).intValue();

		if(countLeadRemark>0){
			return true;
		}else{
			return false;
		}
	}	
}
