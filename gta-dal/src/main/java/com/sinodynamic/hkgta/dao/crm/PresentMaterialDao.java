package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;

public interface PresentMaterialDao extends IBaseDao<PresentMaterial> {

	public PresentMaterial getById(Long materialId);

	
	
}
