package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;

@Repository
public class CustomerEmailDaoImpl extends GenericDao<CustomerEmailContent> implements CustomerEmailContentDao {


	public Serializable addCustomerEmail(CustomerEmailContent ca) throws HibernateException {
		return save(ca);
	}

	public boolean updateCustomerEmail(CustomerEmailContent ca) throws HibernateException {
		return saveOrUpdate(ca);
	}
	@Override
	public List<CustomerEmailContent> getBatchCustomerEmailContentSendMail(int resendCount, int pageSize) {
		String hql="from CustomerEmailContent c where c.status='"+EmailStatus.PND.name()+"' or (c.status='"+EmailStatus.FAIL.name()+"' and c.resendCount<3) and c. noticeType='"+Constant.NOTICE_TYPE_BATCH+"' order by createDate";
		Query query=getCurrentSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(pageSize);
		return query.list();
	}

}
