package com.sinodynamic.hkgta.dao.adm;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.adm.AppsVersion;

@Repository
public class AppsVersionDaoImpl extends GenericDao<AppsVersion> implements AppsVersionDao{

	@Override
	public List<AppsVersion> getAllAppsVersion() throws HibernateException
	{
		String HQL = "FROM AppsVersion a ";
		return getByHql(HQL);
	}


	
}
