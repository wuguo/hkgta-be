package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.pms.RoomStatusScheduleDto;
import com.sinodynamic.hkgta.entity.pms.RoomStatusSchedule;
@Repository
public class RoomStatusScheduleDaoImpl extends GenericDao<RoomStatusSchedule> implements RoomStatusScheduleDao {

	@Override
	public List<RoomStatusSchedule> getExpiredRoomStatusSchedule(Date date) {
		// TODO Auto-generated method stub
		String hql="from RoomStatusSchedule where  ? >endDate";
		List<Serializable>param=new ArrayList<>();
		param.add(date);
		List<RoomStatusSchedule> list=super.getByHql(hql, param);
		return list;
	}
	
	@Override
	public int deleteAll(String type) {
		String hql = "delete from room_status_schedule  where schedule_oo_status = ? ";
		return super.deleteByHql(hql, type);

	}
	
	/**
	 * 获取schedule list
	 */
	@Override
	public List<RoomStatusSchedule> getRoomStatusSchedule(long roomId) {
		StringBuffer hql = new StringBuffer(
				"select a from RoomStatusSchedule a  where a.roomId =? ORDER BY a.scheduleId ASC");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		return super.getByHql(hql.toString(), paramList);
	}
	@Override
	public List<RoomStatusSchedule> getRoomStatusScheduleByDate(Date date) {
		String hql="from RoomStatusSchedule where  ?  BETWEEN beginDate and endDate";
		List<Serializable>param=new ArrayList<>();
		param.add(date);
		List<RoomStatusSchedule> list=super.getByHql(hql, param);
		return list;
	}
	
	/**
	 * 获取schedule list
	 */
	@Override
	public List<RoomStatusScheduleDto> getRoomStatusScheduleDtoList(long roomId) {
		StringBuffer hql = new StringBuffer(
				" SELECT a.schedule_id AS scheduleId, "
				+ " a.room_id AS roomId, "
				+ " a.schedule_oo_status AS stascheduleOoStatus, "
				+ " a.begin_date AS beginDate, "
				+ " a.end_date AS endDate, "
				+ " CASE WHEN b.description IS NULL THEN a.reason_code ELSE b.description END AS reason, "
				+ " a.reason_code AS reasonCode, "
				+ " a.remark AS remark, "
				+ " a.create_date AS createDate, "
				+ " a.create_by AS createBy "
				+ " FROM room_status_schedule a "
				+ " LEFT JOIN housekeep_preset_parameter b ON a.reason_code = b.param_id"
				+ " WHERE a.room_id =? ORDER BY a.schedule_id ASC ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		return super.getDtoBySql(hql.toString(), paramList, RoomStatusScheduleDto.class);
	}
}
