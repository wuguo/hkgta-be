package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.mms.SpaFlexDailySummary;

public interface SpaFlexDailySummaryDao extends IBaseDao<SpaFlexDailySummary> {
	
	public Serializable save(SpaFlexDailySummary spaFlexDailySummary);

	public boolean deleteByTransactionDate(String transDate);
}
