package com.sinodynamic.hkgta.dao.crm;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;

@Repository
public class PresentMaterialDaoImpl  extends GenericDao<PresentMaterial> implements PresentMaterialDao {

	@Override
	public PresentMaterial getById(Long materialId) {
		return super.get(PresentMaterial.class, materialId);
	}

}
