package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;

public interface CustomerOrderDetDao extends IBaseDao<CustomerOrderDet>{

	public Serializable saveOrderDet(CustomerOrderDet cOrderDet);
	
	public CustomerOrderDet getCustomerOrderDet(Long orderNo);
	
	public CustomerOrderDet getByItemNo(String itemNo);
	
	public int deleteByOrderNo(Long orderNo);
	
	public List<CustomerOrderDet> getCustomerOrderDetsByOrderNo(Long orderNo);
	
	public List<CustomerOrderDet> getCustomerOrderDetsByInvoiceNo(String invoiceNo);
}
