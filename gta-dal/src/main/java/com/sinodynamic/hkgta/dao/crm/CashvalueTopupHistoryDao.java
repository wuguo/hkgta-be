package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;

public interface CashvalueTopupHistoryDao extends IBaseDao<CashvalueTopupHistory> {
	
	public void addCashvalueTopupHistory(CashvalueTopupHistory cth);

}
