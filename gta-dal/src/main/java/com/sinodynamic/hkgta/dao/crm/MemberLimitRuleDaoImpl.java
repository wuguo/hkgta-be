package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.MemberLimitRuleDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanAdditionRuleDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanRightDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTypeDto;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRule;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.LimitType;
import com.sinodynamic.hkgta.util.constant.PlanRight;

@Repository
public class MemberLimitRuleDaoImpl extends GenericDao<MemberLimitRule> implements MemberLimitRuleDao {
	
	public Serializable saveMemberLimitRuleImpl(MemberLimitRule mLimitRule){
		return save(mLimitRule);
	}
	
	public void saveMemberLimitRule(List<MemberLimitRule> list) {
		if (list != null && list.size() > 0) {
			for (MemberLimitRule memberLimitRule : list) {
				save(memberLimitRule);
			}
		}
	}
	
	public MemberLimitRule getEffectiveByCustomerId(Long customerId){
		String hqlstr = " from MemberLimitRule m where m.customerId = ? "
				+" and date_format( ?,'%Y-%m-%d')>=m.effectiveDate and date_format( ?,'%Y-%m-%d') <= m.expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> ruleList = super.getByHql(hqlstr, param);
		if(null != ruleList && ruleList.size() > 0)
			return ruleList.get(0);
		return null;
	}
	
	public List<MemberLimitRule> getEffectiveListByCustomerId(Long customerId){
		String hqlstr = " from MemberLimitRule m where m.customerId = ? "
				+" and date_format( ?,'%Y-%m-%d')>=m.effectiveDate and date_format( ?,'%Y-%m-%d') <= m.expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		return getByHql(hqlstr, param);
	}
	
	
	public List<MemberLimitRule> getLastEffectiveListByCustomerId(Long customerId){
		/*String sqlstr = "select\n" +
				"        m.limit_id as limitId,\n" +
				"        m.applied_scope as appliedScope,\n" +
				"        m.create_by as createBy,\n" +
				"        m.create_date as createDate,\n" +
				"        m.customer_id as customerId,\n" +
				"        m.description as description,\n" +
				"        m.effective_date as effectiveDate,\n" +
				"        m.expiry_date as expiryDate,\n" +
				"        m.limit_type as limitType,\n" +
				"        m.limit_unit as limitUnit,\n" +
				"        m.num_value as numValue,\n" +
				"        m.text_value as textValue,\n" +
				"        m.update_by as updateBy,\n" +
				"        m.update_date as updateDate \n" +
				"    from\n" +
				"        member_limit_rule m \n" +
				"    where\n" +
				"        m.customer_id=? \n" +
				"        and date_format(?, '%Y-%m-%d')>=m.effective_date \n" +
				"        and date_format(?, '%Y-%m-%d')<=m.expiry_date";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		return this.getDtoBySql(sqlstr, param, MemberLimitRule.class);*/
		String hqlstr = " from MemberLimitRule m where m.customerId = ? "
				+" and date_format( ?,'%Y-%m-%d')>=m.effectiveDate and date_format( ?,'%Y-%m-%d') <= m.expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		return getByHql(hqlstr, param);
	}
	
	@Override
	public BigDecimal getCreditLimitOfMember(Long customerId){
		String hql="select numValue from MemberLimitRule where customerId = ? and limitType =? "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		param.add(customerId);
		param.add(LimitType.CR.name());
		param.add(currentDate);
		param.add(currentDate);
		BigDecimal cr = (BigDecimal) getUniqueByHql(hql,param);
		if(cr == null){
			return null;
		}
		return cr.setScale(2);
	}
	
	@Override
	public BigDecimal getTransactionLimitOfMember(Long customerId){
		String hql="select numValue from MemberLimitRule where customerId = ? and limitType =? "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		param.add(customerId);
		param.add(LimitType.TRN.name());
		param.add(currentDate);
		param.add(currentDate);
		BigDecimal tran = (BigDecimal) getUniqueByHql(hql,param);
		if(tran == null){
			return null;
		}
		return tran.setScale(2);
	}
	
	@Override
	public boolean getDayPassPurchaseNoOfMember(Long customerId){
		String hql="from MemberLimitRule where customerId = ? and limitType =? "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		param.add(customerId);
		param.add(PlanRight.D1.name());
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> daypassPermission = getByHql(hql,param);
		for(MemberLimitRule item : daypassPermission){
				if (item != null){
				if (item.getNumValue().equals(new BigDecimal("0.00"))){
					return false;
				} 
			}
		}
		return true;
	}
    

	@Override
	public boolean getMemberTrainingRight(Long customerId) {
		String hql ="from MemberLimitRule where customerId = ? and limitType like 'TRAIN%' "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> permissionList = getByHql(hql,param);
		for(MemberLimitRule item : permissionList){
			if (item.getTextValue().equals("true")){
				return true;
			} 
		}
		return false;
	}

	@Override
	public boolean getMemberEventsRight(Long customerId) {
		String hql ="from MemberLimitRule where customerId = ? and limitType like 'EVENT%' "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> permissionList = getByHql(hql,param);
		for(MemberLimitRule item : permissionList){
			if (item.getTextValue().equals("true") ){
				return true;
			} 
		}
		return false;
	}
	public boolean getMemberMultiGenerationRight(Long customerId) {
		String hql ="from MemberLimitRule where customerId = ? and limitType like 'MGD%' "
				+" and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate";
		Date currentDate = new Date();
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(currentDate);
		param.add(currentDate);
		List<MemberLimitRule> permissionList = getByHql(hql,param);
		for(MemberLimitRule item : permissionList){
			if (item.getTextValue().equals("true") ){
				return true;
			} 
		}
		return false;
	}
	
	@Override
	public boolean updateMmberTransactionLimit(Long customerId, BigDecimal tran,String updateBy){
		String sql="update member_limit_rule set num_value = ? ,update_by=? ,update_date=NOW() where customer_id = ? and limit_type =? "
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(tran);
		param1.add(updateBy);
		param1.add(customerId);
		param1.add(LimitType.TRN.name());
		param1.add(currentDate);
		param1.add(currentDate);
		int i = sqlUpdate(sql, param1);
		if (i >0) return true;
		return false;
	}
	
	@Override
	public boolean updateMemberDayPassPurchasing(Long customerId, String dayPass,String updateBy){
		String sql="update member_limit_rule set num_value = '0',update_by=? ,update_date=NOW()  where customer_id = ? and limit_type =? "
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql2="update member_limit_rule set num_value = ? ,update_by=? ,update_date=NOW()  where customer_id = ? and limit_type =?"
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql3="select num_value as numValue from member_limit_rule where customer_id = (select superior_member_id from member where customer_id = ?) and limit_type =?"
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(updateBy);
		param1.add(customerId);
		param1.add(PlanRight.D1.name());
		param1.add(currentDate);
		param1.add(currentDate);
		int i = 0;
		if(dayPass.equals("1")){
			param1.remove(0);
			List<ServicePlanRightDto> dayPassValueList = getDtoBySql(sql3, param1, ServicePlanRightDto.class);
			if(dayPassValueList.size()>0){
				for(ServicePlanRightDto item: dayPassValueList){
					List<Serializable> param2 = new ArrayList<Serializable>();
					param2.add(item.getNumValue());
					param2.add(updateBy);
					param2.add(customerId);
					param2.add(PlanRight.D1.name());
					param2.add(currentDate);
					param2.add(currentDate);
					i += sqlUpdate(sql2, param2);
					}
			}
		}else if(dayPass.equals("0")){
			i = sqlUpdate(sql, param1);
			
		}
		if (i >0) return true;
		return false;
		
	}
	
	@Override
	public boolean updateMemberFacilitiesRight(Long customerId,String facilityRight){
		String sql="update member_plan_facility_right m set m.permission = 'NO'  where m.customer_id = ? "
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effective_date and m.expiry_date";
		String sql2="update member_plan_facility_right m set m.permission = ?  where m.customer_id = ? and m.facility_type_code = ? "
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effective_date and m.expiry_date";
		String sql3="select facility_type_code as typeCode,permission from member_plan_facility_right where customer_id = (select superior_member_id from member where customer_id = ?)"
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN effective_date and expiry_date";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(customerId);	
		param1.add(currentDate);
		int i = 0;	
		if(facilityRight.equals("1")){
			List<FacilityTypeDto> permissionMappingList = getDtoBySql(sql3, param1, FacilityTypeDto.class);
			if(permissionMappingList.size()>0){
				for (FacilityTypeDto item : permissionMappingList) {
					List<Serializable> param2 = new ArrayList<Serializable>();
					param2.add(item.getPermission());
					param2.add(customerId);	
					param2.add(item.getTypeCode());
					param2.add(currentDate);
					i+=sqlUpdate(sql2, param2);
				}
			}
		}
		else if(facilityRight.equals("0")){
			i = sqlUpdate(sql, param1);
		}
		
		if (i >0) return true;
		return false;
	}
	
	@Override
	public boolean updateMemberTrainingRight(Long customerId,String trainRight,String updateBy){
		String sql="update member_limit_rule set text_value = 'false',update_by=? ,update_date=NOW() where customer_id = ? and limit_type like 'TRAIN%' "
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql2="update member_limit_rule set text_value = 'true',update_by=? ,update_date=NOW() where customer_id = ? and limit_type = ?"
				+ "  and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
//		String sql3="select right_code as rightCode from service_plan_right_master where right_type = ?";
		String sql3 = "SELECT  s.plan_no AS planNo, s.right_code AS rightCode, s.input_value AS inputValue FROM service_plan_addition_rule s WHERE s.right_code LIKE 'TRAIN%' AND s.input_value = 'true' AND s.plan_no = (SELECT service_plan FROM member_plan_facility_right WHERE customer_id = ? LIMIT 1)";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(updateBy);
		param1.add(customerId);	
		param1.add(currentDate);
		param1.add(currentDate);
		List<Serializable> param3 = new ArrayList<Serializable>();
		param3.add(customerId);
		int i = 0;
		
		if(trainRight.equals("1")){
			List<ServicePlanAdditionRuleDto> trainingRightCodeList = getDtoBySql(sql3, param3, ServicePlanAdditionRuleDto.class);
			if(trainingRightCodeList.size()>0){
				for(ServicePlanAdditionRuleDto item: trainingRightCodeList){
					List<Serializable> param2 = new ArrayList<Serializable>();
					param2.add(updateBy);	
					param2.add(customerId);	
					param2.add(item.getRightCode());
					param2.add(currentDate);
					param2.add(currentDate);
					i += sqlUpdate(sql2, param2);
					}
			}
		}else if(trainRight.equals("0")){
			i = sqlUpdate(sql, param1);
			
		}
		if (i >0) return true;
		return false;
		
		
	}
	
	@Override
	public boolean updateMemberEventsRight(Long customerId,String eventRight,String updateBy){
		String sql="update member_limit_rule set text_value = 'false' ,update_by=? ,update_date=NOW() where customer_id = ? and limit_type like 'EVENT%'  "
				+ " and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date ";
		String sql2="update member_limit_rule set text_value = 'true',update_by=? ,update_date=NOW() where customer_id = ? and limit_type = ?  "
				+ " and date_format( ?,'%Y-%m-%d')>=effective_date and date_format( ?,'%Y-%m-%d') <= expiry_date";
		String sql3 = "SELECT  s.plan_no AS planNo, s.right_code AS rightCode, s.input_value AS inputValue FROM service_plan_addition_rule s WHERE s.right_code LIKE 'EVENT%' AND s.input_value = 'true' AND s.plan_no = (SELECT service_plan FROM member_plan_facility_right WHERE customer_id = ? LIMIT 1)";
		Date currentDate = new Date();
		List<Serializable> param1 = new ArrayList<Serializable>();
		param1.add(updateBy);
		param1.add(customerId);
		param1.add(currentDate);
		param1.add(currentDate);
		
		List<Serializable> param3 = new ArrayList<Serializable>();
		param3.add(customerId);
		int i = 0;
		
		if(eventRight.equals("1")){
			List<ServicePlanAdditionRuleDto> eventsRightCodeList = getDtoBySql(sql3, param3, ServicePlanAdditionRuleDto.class);
			if(eventsRightCodeList.size()>0){
				for(ServicePlanAdditionRuleDto item: eventsRightCodeList){
					List<Serializable> param2 = new ArrayList<Serializable>();
					param2.add(updateBy);
					param2.add(customerId);	
					param2.add(item.getRightCode());
					param2.add(currentDate);
					param2.add(currentDate);
					i += sqlUpdate(sql2, param2);
					}
			}
		}else if(eventRight.equals("0")){
			i = sqlUpdate(sql, param1);
			
		}
		if (i >0) return true;
		return false;
	}
	
	public int deleteMemberLimitRule(Long customerId,String excludedLimitType) {
		if(StringUtils.isEmpty(excludedLimitType)){
			Object[] param = new Object[]{customerId};
			return this.deleteByHql("DELETE m from member_limit_rule m where m.customer_id = ? ",param);
		}else{
			Object[] param = new Object[]{customerId,excludedLimitType};
			return this.deleteByHql("DELETE m from member_limit_rule m where m.customer_id = ? and m.limit_type != ? ",param);
		}
		
	}
	
	
	public List<MemberLimitRuleDto> getEffectiveMemberLimitRuleDtoByCustomerId(Long customerId) {
		System.out.println("================Current Date is:"+ new Date() + "====================");
		String sql = " select mlr.limit_type as rightCode,\n"
				+ "CASE \n"
				+ "   when sprm.input_value_type = 'INT' THEN round(mlr.num_value)\n"
				+ "   when sprm.input_value_type = 'TXT' THEN mlr.text_value\n"
				+ "END as inputValue,\n"
				+ "sprm.right_type as rightType,\n"
				+ "mlr.description as description\n"
				+ "from member_limit_rule mlr, service_plan_right_master sprm where mlr.limit_type = sprm.right_code and mlr.customer_id = ? and date_format( ?,'%Y-%m-%d')>=mlr.effective_date and date_format( ?,'%Y-%m-%d') <= mlr.expiry_date and sprm.right_code <> 'MGD'";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(new Date());
		param.add(new Date());
		return getDtoBySql(sql, param, MemberLimitRuleDto.class);
	}

	@Override
	public MemberLimitRule getEffectiveMemberLimitRule(Long customerId, String limitType)
	{
		System.out.println("================Current Date is:"+ new Date() + "====================");
		String hql="from MemberLimitRule where customerId = ? and limitType =? and date_format( ?,'%Y-%m-%d')>=effectiveDate and date_format( ?,'%Y-%m-%d') <= expiryDate order by expiryDate DESC ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(limitType);
		param.add(new Date());
		param.add(new Date());
		
		List<MemberLimitRule> memberLimitRuleList = super.getByHql(hql, param);
		
		if(null != memberLimitRuleList && memberLimitRuleList.size() > 0){
			return memberLimitRuleList.get(0);
		}
		
		return null;
//		return (MemberLimitRule) getUniqueByHql(hql,param);
	}
	
	
	@Override
	public MemberLimitRule getLastEffectiveMemberLimitRule(Long customerId, String limitType)
	{		
		String hql="from MemberLimitRule where customerId = ? and limitType =? order by effectiveDate DESC ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(limitType);
				
		List<MemberLimitRule> memberLimitRuleList = super.getByHql(hql, param);
		
		if(null != memberLimitRuleList && memberLimitRuleList.size() > 0){
			return memberLimitRuleList.get(0);
		}
		
		return null;		
	}
	
	/**
	 * 根据customerId获取该用户的信用值
	 * @param customerId
	 * @return
	 */
	@Override
	public BigDecimal getMemberCreditValue(Long customerId){
		String hql="from MemberLimitRule where customerId = ? and (limitType = 'CR' OR limitType = 'TRN') and expiryDate>=? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(new Date());
		
		MemberLimitRule memberLimitRule = (MemberLimitRule) super.getUniqueByHql(hql, param);
		if(null != memberLimitRule){
			return memberLimitRule.getNumValue();
		}
		return null;
	}

	@Override
	public MemberLimitRule getEffectiveRightByCustomerIdAndFacilityType(Long customerId, String facilityType,
			Date effectiveDate,String startTime,String endTime) {
		String hql ="from MemberLimitRule m where m.customerId = ? and m.limitType = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(facilityType);
		if(StringUtils.isEmpty(startTime)&&StringUtils.isEmpty(endTime)){
			hql+=" and date_format( ?,'%Y-%m-%d') BETWEEN m.effectiveDate and m.expiryDate order by m.expiryDate DESC";
			if(effectiveDate==null){
				param.add(new Date());
			}else{
				param.add(effectiveDate);
			}
		}else{
			hql+=" and  ( m.expiryDate>=?  or m.expiryDate >=? )";
			param.add(DateConvertUtil.parseString2Date(startTime,"yyyy-MM-dd"));
			param.add(DateConvertUtil.parseString2Date(endTime,"yyyy-MM-dd"));
		}
		List<MemberLimitRule> permissionList = getByHql(hql,param);
		if(null != permissionList && permissionList.size()>0){ 
			return permissionList.get(0);
		}
		return null;
	}

	@Override
	public MemberLimitRule getLimitRuleByKey(Long customerId, String limitType, Date effectiveDate) {
		// TODO Auto-generated method stub
		String hqlstr = " from MemberLimitRule m where m.customerId = ? "
				+ " and m.limitType = ? and m.effectiveDate = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(limitType);
		param.add(effectiveDate);
		List<MemberLimitRule> permissionList = getByHql(hqlstr, param);
		if(null != permissionList && permissionList.size()>0){ 
			return permissionList.get(0);
		}
		return null;
	}
	
	public MemberLimitRule getLimitRuleByCustomerIdAndLimitType(Long customerId, String limitType) {
		// TODO Auto-generated method stub
		String hqlstr = " from MemberLimitRule m where m.customerId = ? "
				+ " and m.limitType = ?  and m.expiryDate is null  order by effectiveDate desc  ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(limitType);
		List<MemberLimitRule> permissionList = getByHql(hqlstr, param);
		if(null != permissionList && permissionList.size()>0){ 
			return permissionList.get(0);
		}
		return null;
	}
	
}
