package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.StaffTimeslot;

public interface StaffTimeslotDao extends IBaseDao<StaffTimeslot> {
    
    public Serializable addStaffTimeslot(StaffTimeslot slot);
    
    public Boolean isCoachVailable(String staffTimeslotId, Date beginDatetime, Date endDatetime);
    
    public Boolean deleteStaffTimeslotById(String staffTimeslotId);
    
    public StaffTimeslot getStaffTimeslotById(String staffTimeslotId);

    public List<Date> loadStaffOffdutyDates(String staffId, Date begin, Date end);
    
    public List<StaffTimeslot> loadStaffTimeslot(String staffId, Date begin, Date end);
}
