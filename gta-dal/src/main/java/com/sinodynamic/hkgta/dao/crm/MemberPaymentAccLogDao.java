package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAccLog;

public interface MemberPaymentAccLogDao extends IBaseDao<MemberPaymentAccLog>
{
}
