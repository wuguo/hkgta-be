package com.sinodynamic.hkgta.dao.crm;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;

@Repository("dayPassPayment")
public class DayPassPaymentOrderDaoImpl implements AdvanceQueryConditionDao {

	
	public  List<AdvanceQueryConditionDto> assembleQueryConditions() {
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Payment Item #","t.paymentNo", BigInteger.class.getName(),"",1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Guest","t.memberName",String.class.getName(),"",2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Day Pass Type","t.daypassType",String.class.getName(),"",3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Activation Date","t.activationDate", Date.class.getName(),"",4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Card #","pcm.cardNo",Integer.class.getName(),"",5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Card Status","cardStatus",String.class.getName(),"cardStatus",6);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Customer Id","customerId",BigInteger.class.getName(),"",7);
		 return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7);
	}

	public static void main(String[] args) {
		System.out.printf(BigInteger.class.getName());
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
