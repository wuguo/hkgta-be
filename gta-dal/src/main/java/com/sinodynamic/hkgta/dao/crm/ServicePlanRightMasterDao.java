package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.MemberLimitRuleDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;

public interface ServicePlanRightMasterDao extends IBaseDao<ServicePlanRightMaster>{

	List<MemberLimitRuleDto> getServicePlanRightMasterDtosByPlanNo(
			Long planNo);

}
