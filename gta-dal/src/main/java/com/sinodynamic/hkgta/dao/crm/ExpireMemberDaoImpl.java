package com.sinodynamic.hkgta.dao.crm;

import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.ExpiredMemberDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * @author Junfeng_Yan
 * @since 2015/4/29
 */
@Repository
public class ExpireMemberDaoImpl extends GenericDao<ExpiredMemberDto> implements ExpireMemberDao{

	@Override
	public ListPage<ExpiredMemberDto> getExpiredMember(
			ListPage<ExpiredMemberDto> page, String countSql, String sql,
			List<?> param, ExpiredMemberDto dto,Map<String, org.hibernate.type.Type> map) throws HibernateException {
		// TODO Auto-generated method stub
		return (ListPage<ExpiredMemberDto>) super.listBySqlDto(page, countSql, sql, param, dto,map);
		
	}
}
