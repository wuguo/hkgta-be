package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;

@Repository
public class InternalPermitCardDaoImpl extends GenericDao<InternalPermitCard>
		implements InternalPermitCardDao {

	@Override
        public String getTempPassProfileList(String status) throws Exception {
	    
	    	StringBuilder sql = new StringBuilder();
	    
	    	sql.append("SELECT * FROM (select ")
		.append("a.contractor_id as contractorId,")
		.append("a.create_date as createDate,")
		.append("a.surname as surname,")
		.append("a.given_name as givenName,")
		.append(" concat(a.given_name,' ',a.surname) as memberName, " )
		.append("a.gender as gender,")
		.append("a.passport_no as passportNo,")
		.append("a.passport_type as passportType,")
		.append("b.type_name as passType,")
		.append("cast(c.card_id as char) as cardId,")
		.append("a.period_from as activationDate,")
		.append("a.period_to as periodTo,")
		.append("(select max(concat(given_name,' ',surname)) from staff_profile sp where sp.user_id = a.create_by) as referral,")
		.append("a.status as status, ")
		.append("(case c.status when 'ISS' then 'Issued' when 'EXP' then 'Expired' when 'DPS' then 'Disposal' when 'RTN' then 'Returned' end) as statusValue,")
		.append("c.status as cardStatus ")
		.append("FROM ")
		.append("contract_helper a ")
		.append("left join internal_permit_card c on (a.contractor_id = c.contractor_id and c.status_update_date = (select max(tmp.status_update_date) from internal_permit_card tmp where tmp.contractor_id = a.contractor_id)) ")
		.append("left join helper_pass_type b on a.helper_pass_type = b.type_id) t ");
		
		if (!StringUtils.isEmpty(status)) {
		    sql.append("WHERE t.cardStatus = '").append(status).append("'");
		}
		
		return sql.toString();
        }
	
	

	@Override
	public String getTempPassProfileHistoryList(String status) throws Exception {
	    
	    	StringBuilder sql = new StringBuilder();
	    	
	    	sql.append("SELECT * FROM (")
	    	.append("select ") 
	    	.append("chh.contractor_id as contractorId, ")
	    	.append("a.surname as surname, ")
	    	.append("a.given_name as givenName, ")
	    	.append(" concat(a.given_name,' ',a.surname) as memberName, " )
	    	.append("b.type_name as passType, ")
	    	.append("cast(chh.permit_card_id as char) as cardId, ")
	    	.append("chh.period_from as activationDate, ")
	    	.append("chh.period_to as periodTo, ")
	    	.append("(select max(concat(sp.given_name, ' ', sp.surname)) from staff_profile sp where sp.user_id = a.create_by) as referral, ")
	    	.append("chh.create_date as createDate, ")
	    	.append("chh.remark as internalRemark " )
	    	.append("from ") 
	    	.append("contract_helper_history chh ")
	    	.append("left join contract_helper a on chh.contractor_id = a.contractor_id ")
	    	.append("left join helper_pass_type b on chh.helper_pass_type_id = b.type_id) t");
		
		return sql.toString();
	}



	@Override
	public List<InternalPermitCard> getTempCardContractorId(Long contractorId) {

		String hql = "from InternalPermitCard t where t.status = 'ISS' and t.contractorId = " + contractorId + " order by t.statusUpdateDate desc";
		List<InternalPermitCard> cards = getByHql(hql);
		return cards;
	}

	@Override
	public void updateCardInfo(InternalPermitCard card) {
		this.update(card);

	}

	@Override
	public InternalPermitCard getCardById(Long cardId)
	{
		return get(InternalPermitCard.class, Long.parseLong(cardId.toString()));
	}

	/**
	 * 根据staffId查询绑定数据列表
	 * @param staffUserId
	 * @return
	 */
	@Override
	public List<InternalPermitCard> getIssuedCardByStaffUserId(String staffUserId) {
	    String hql = "FROM InternalPermitCard p WHERE p.status = 'ISS' AND p.staffUserId = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(staffUserId);
	    return getByHql(hql, params);
	}

	//added by Kaster 20160217
	@Override
	public List<InternalPermitCard> getByStaffUserIds(String userIds) {
		String hql = "from InternalPermitCard where staffUserId in " + userIds;
		return super.getByHql(hql);
	}

	

}
