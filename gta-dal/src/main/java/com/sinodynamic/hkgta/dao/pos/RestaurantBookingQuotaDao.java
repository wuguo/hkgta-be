package com.sinodynamic.hkgta.dao.pos;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantBookingQuota;

public interface RestaurantBookingQuotaDao extends IBaseDao<RestaurantBookingQuota> {

	List<RestaurantBookingQuota> getRestaurantBookingQuotaList(String restaurantId, Integer bookingTime);

}
