package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.ContractHelperHistory;

public interface ContractHelperHistoryDao extends IBaseDao<ContractHelperHistory>{

    public void createContractHelperHistory(ContractHelper ch, Long cardId, String staffUser) throws Exception;
    
    public void updateContractHelperHistory(ContractHelper ch, Long cardId);

}
