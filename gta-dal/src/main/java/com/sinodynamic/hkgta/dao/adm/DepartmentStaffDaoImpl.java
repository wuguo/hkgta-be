package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.DepartmentStaff;

@Repository
public class DepartmentStaffDaoImpl extends GenericDao<DepartmentStaff> implements DepartmentStaffDao{

	@Override
	public void saveDepartment(DepartmentStaff department) throws HibernateException
	{
		super.save(department);
	}

	@Override
	public DepartmentStaff getDepart(String userId) throws HibernateException
	{
		String HQL = "FROM DepartmentStaff d where d.staffUserId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		return (DepartmentStaff)getUniqueByHql(HQL, param);
	}

	@Override
	public void updateDepartment(DepartmentStaff department) throws HibernateException
	{
		super.update(department);
	}

	

	
}
