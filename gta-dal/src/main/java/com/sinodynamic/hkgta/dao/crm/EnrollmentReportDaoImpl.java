package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.EnrollmentReportDto;
@Repository
public class EnrollmentReportDaoImpl extends GenericDao<EnrollmentReportDto> implements
		EnrollmentReportDao {

}
