/*
 * Copyright (C) HKGTA
 * File name:    MemberPlanFacilityRightDaoImpl.java
 * Author:       Li_Chen
 * Version:      1.0
 * Date:         2015-4-29
 * Description:  
 * Revision history: 
 * 2015-4-29  Li_Chen  The initial creation 
 * 
 */
package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.FacilityEntitlementDto;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.FacilityRight;
@Repository
public class MemberPlanFacilityRightDaoImpl extends GenericDao<MemberPlanFacilityRight> implements MemberPlanFacilityRightDao {

	public void savePlanFacilityRight(List<MemberPlanFacilityRight> list) {
		if (list != null && list.size() > 0) {
			for (MemberPlanFacilityRight facilityRightRight : list) {
				save(facilityRightRight);
			}
		}
	}
	
	public int deletePlanFacilityRight(Long customerId, Long planNo) {
		Object[] param = new Object[]{customerId,planNo};
		return this.deleteByHql("DELETE m from member_plan_facility_right m where m.customer_id = ? and m.service_plan = ? ",param);
	}
	
	public int deletePlanFacilityRight(Long customerId) {
		Object[] param = new Object[]{customerId};
		return this.deleteByHql("DELETE m from member_plan_facility_right m where m.customer_id = ? ",param);
	}
	
	public List<MemberPlanFacilityRight> getEffectiveFacilityRightByCustomerId(Long customerId) {
		String hqlstr = " from MemberPlanFacilityRight m where m.customerId = ? "
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effectiveDate and m.expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(new Date());
		return getByHql(hqlstr, param);
	}

	@Override
	public List<FacilityEntitlementDto> getEffectiveMemberPlanFacilityRightsByCustomerId(
			Long customerID) {
		String hqlstr = "select m.facilityTypeCode as facilityTypeCode,m.permission as permission from MemberPlanFacilityRight m where m.customerId = ? and m.permission != ? "
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effectiveDate and m.expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		Date currentDate = new Date();
		param.add(customerID);
		param.add(FacilityRight.NO.name());
		param.add(currentDate);
		List<FacilityEntitlementDto> facilityEntitlementDtos = getDtoByHql(hqlstr, param, FacilityEntitlementDto.class);
		return facilityEntitlementDtos;
	}
	
	@Override
	public boolean getEffectiveMemberFacilityRight(Long customerId){
		String hql ="from MemberPlanFacilityRight m where m.member.customerId = ?"
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effectiveDate and m.expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(new Date());
		List<MemberPlanFacilityRight> permissionList = getByHql(hql,param);
		for(MemberPlanFacilityRight item : permissionList){
			if (item.getPermission().equals("BA") || item.getPermission().equals("AO")){
				return true;
			} 
		}
		return false;
	}
	
	@Override
	public String getEffectiveMemberFacilityRightsInMyAccount(Long customerId){
		String hql ="from MemberPlanFacilityRight m where m.member.customerId = ?"
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effectiveDate and m.expiryDate";
		List<Serializable> param = new ArrayList<Serializable>();
		String right = "NO";
		param.add(customerId);
		param.add(new Date());
		List<MemberPlanFacilityRight> permissionList = getByHql(hql,param);
		for(MemberPlanFacilityRight item : permissionList){
			right = item.getPermission();
			if (right.equals("BA")){
				right = "BA";
				break;
			} else if(right.equals("AO")){
				right = "AO";
				}
			}
		return right;
		}

	@Override
	public MemberPlanFacilityRight getEffectiveRightByCustomerIdAndFacilityType(
			Long customerId, String facilityType,Date effectiveDate) {
		String hql ="from MemberPlanFacilityRight m where m.customerId = ? and m.facilityTypeCode = ? "
				+ " and date_format( ?,'%Y-%m-%d') BETWEEN m.effectiveDate and m.expiryDate order by m.expiryDate DESC";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(facilityType);
		if(effectiveDate==null){
			param.add(new Date());
		}else{
			param.add(effectiveDate);
		}
		List<MemberPlanFacilityRight> permissionList = getByHql(hql,param);
		if(null != permissionList && permissionList.size()>0){ 
			return permissionList.get(0);
		}
		return null;
	}
	@Override
	public MemberPlanFacilityRight getEffectiveRightByCustomerIdAndFacilityType(
			Long customerId, String facilityType,String startTime,String endTime) {
		String hql ="from MemberPlanFacilityRight m where m.customerId = ? and m.facilityTypeCode = ? "
				+ " and (m.expiryDate>=date_format( ?,'%Y-%m-%d') or m.expiryDate>=date_format( ?,'%Y-%m-%d'))";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(customerId);
		param.add(facilityType);
		
		if(StringUtils.isNotEmpty(startTime)&&StringUtils.isNotEmpty(endTime)){
			param.add(DateConvertUtil.parseString2Date(startTime,"yyyy-MM-dd"));
			param.add(DateConvertUtil.parseString2Date(endTime,"yyyy-MM-dd"));
		}else{
			param.add(new Date());
			param.add(new Date());
		}
		List<MemberPlanFacilityRight> permissionList = getByHql(hql,param);
		if(null != permissionList && permissionList.size()>0){ 
			return permissionList.get(0);
		}
		return null;
	}
	
}
