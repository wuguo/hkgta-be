package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.RoomCrew;
@Repository
public class RoomCrewDaoImpl extends GenericDao<RoomCrew> implements RoomCrewDao {

	@Override
	public List<RoomCrew> getRoomCrewList(Long roomId) {

		StringBuffer hql = new StringBuffer(
				"from RoomCrew a where a.room.roomId = ?");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public RoomCrew getRoomCrew(Long roomId, String staffUserId, String crewRoleId){
		StringBuffer hql = new StringBuffer(
				"from RoomCrew a where a.room.roomId = ? and a.staffProfile.userId= ? and a.crewRoleId = ?");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		paramList.add(staffUserId);
		paramList.add(crewRoleId);
		List<RoomCrew> roomCrews = super.getByHql(hql.toString(), paramList);
		if(roomCrews != null && roomCrews.size() > 0)return roomCrews.get(0);
		return null;
	}

	@Override
	public List<RoomCrew> getRoomCrewList(String staffUserId)
	{
		StringBuffer hql = new StringBuffer(
				"from RoomCrew a where a.staffProfile.userId = ?");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(staffUserId);
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public List<RoomCrew> getRoomCrewListByCrewRoleId(Long roomId,String crewRoleId)
	{
		StringBuffer hql = new StringBuffer(
				"from RoomCrew a where a.room.roomId = ? and a.crewRoleId = ?");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		paramList.add(crewRoleId);
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public RoomCrew getRoomCrew(Long roomId, String staffUserId)
	{
		StringBuffer hql = new StringBuffer(
				"from RoomCrew a where a.room.roomId = ? and a.staffProfile.userId = ?");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		paramList.add(staffUserId);
		List<RoomCrew> roomCrews = super.getByHql(hql.toString(), paramList);
		return (null != roomCrews && roomCrews.size() >0)?roomCrews.get(0): null;
	}
}
