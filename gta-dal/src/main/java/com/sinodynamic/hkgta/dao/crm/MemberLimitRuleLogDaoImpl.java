package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.MemberLimitRuleLog;

@Repository
public class MemberLimitRuleLogDaoImpl extends GenericDao<MemberLimitRuleLog> implements MemberLimitRuleLogDao {


}
