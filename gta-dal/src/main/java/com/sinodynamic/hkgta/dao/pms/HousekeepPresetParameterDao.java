package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.HousekeepPresetParameter;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface HousekeepPresetParameterDao extends IBaseDao<HousekeepPresetParameter>{

	public List<HousekeepPresetParameter> getHousekeepPresetParameterList();
	
	public List<HousekeepPresetParameter> getHousekeepPresetParameterCatList();
	
	public ListPage<HousekeepPresetParameter> getHousekeepPresetParameterList(ListPage<HousekeepPresetParameter> pListPage, String sqlState, String sqlCount, List<Serializable> param) throws HibernateException;
	
}
