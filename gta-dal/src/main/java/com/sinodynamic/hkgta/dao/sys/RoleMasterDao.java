package com.sinodynamic.hkgta.dao.sys;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;

/**
 * @author Junfeng_Yan
 * @since May 9 2015
 */
public interface RoleMasterDao extends IBaseDao<RoleMaster>{

	public List<RoleMaster> getRoleListByUserId(String userId) throws HibernateException;

	public boolean chkRoleNameExist(String roleName);
	
	public String getRoleListSQL();
}
