package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerAddress;

public interface CustomerAddressDao extends IBaseDao<CustomerAddress> {

	public List<CustomerAddress> getCustomerAddressesByCustomerID(Long customerID);
	
	public CustomerAddress getByCustomerIDAddressType(Long customerId, String addressType);
	
	public int deleteByCustomerId(Long customerId) throws HibernateException;
	
}
