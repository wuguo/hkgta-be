package com.sinodynamic.hkgta.dao.fms;
import org.springframework.stereotype.Repository;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.MemberReservedStaff;

@Repository
public class MemberReservedStaffDaoImpl extends GenericDao<MemberReservedStaff> implements MemberReservedStaffDao{

}
