package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityBookAdditionAttr;

public interface MemberFacilityBookAdditionAttrDao extends IBaseDao<MemberFacilityBookAdditionAttr>{
	
	List<MemberFacilityBookAdditionAttr> getMemberFacilityBookAdditionAttrList(long resvId);
	
	MemberFacilityBookAdditionAttr getMemberFacilityBookAdditionAttr(Long resvId,String facilityType);
	
	public int deleteByReservationId(long resvId);
}
