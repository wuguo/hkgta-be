package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;

/**
 * 
 * @author Becky Wu
 *
 */

public interface StaffDao extends IBaseDao<StaffMaster>{
	
	public String statffInfoValidate(String staffNo, String psd);
	
	public String getStaffNoByUserId(String userId);
	
	public void addStaffMaster(StaffMaster sm) throws HibernateException; 
	
	public void updateStaffMaster(StaffMaster sm) throws HibernateException; 
	
	public StaffMaster getStaffMasterByUserId(String userId) throws HibernateException;
	
	public StaffMaster getStaffMasterByStaffNo(String StaffNo) throws HibernateException;
	
	public String saveStaffMaster(StaffMaster sm) throws HibernateException; 
	
	
	public StaffMasterInfoDto getStaffMasterDtoByUserId(String userId) throws HibernateException;
	
	
	
	public boolean isStaffNoUsed(String staffNo) throws HibernateException;
	
	
	public List<StaffMaster> getEexpiredActStaffMaster() throws HibernateException;

	List<StaffMaster> getStaffMasterByPositionTitle(String positionTitle);	

	List<StaffMaster> getStaffMasterByRoomCrewRoleCode(String crewRoleId);	
	
}
