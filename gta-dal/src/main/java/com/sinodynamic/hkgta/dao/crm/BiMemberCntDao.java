package com.sinodynamic.hkgta.dao.crm;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageDto;
import com.sinodynamic.hkgta.entity.crm.AdvertiseImage;
import com.sinodynamic.hkgta.entity.crm.BiMemberCnt;

public interface BiMemberCntDao extends IBaseDao<BiMemberCnt> {

	/**
	 * @param appType
	 *            should be valid value of column 'application_type' in hkgta.advertise_image, in upper case.
	 * @param dispLoc
	 *            should be valid value of column 'display_location' in hkgta.advertise_image, in upper case.
	 */
	public List<AdvertiseImageDto> getByAppTypeDispLoc(String appType, String dispLoc);

	public Long getMaxDispOrder(String appType, String dispLoc);

	public void updateDispOrder(long imgId, int newDispOrder);

	
	public int deleteAdvertiseImages(String dispLoc);

	/**
	 * 获取BiMemberCnt
	 * @param countDate
	 * @param period
	 * @param isActivation
	 * @return
	 */
	public BiMemberCnt getBiMemberCnt(Date countDate, String period, boolean isActivation);
}
