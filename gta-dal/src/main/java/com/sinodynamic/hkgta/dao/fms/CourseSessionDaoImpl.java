package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.CourseSessionDetailsDto;
import com.sinodynamic.hkgta.dto.fms.CourseSessionDto;
import com.sinodynamic.hkgta.dto.fms.CourseSessionSmsParamDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.dto.fms.MemberCourseAttendanceDto;
import com.sinodynamic.hkgta.dto.fms.SessionMemberDto;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.fms.CourseSession;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;

@Repository
public class CourseSessionDaoImpl extends GenericDao<CourseSession> implements CourseSessionDao {

    @Override
    public Serializable addCourseSession(CourseSession session) {
	return save(session);
    }

    @Override
    public boolean updateCourseSession(CourseSession session) {
	
	return update(session);
    }

    @Override
    public boolean deleteCourseSession(CourseSession session) {
	return delete(session);
    }

    @Override
    public List<CourseSession> selectCourseSessionByCourseNo(String courseNo) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public List<StaffProfile> selectAvailableCoach(String type, String beginDatetime, String endDatetime, String coachTimeslotId) throws Exception {
	
	if (StringUtils.isEmpty(type) || StringUtils.isEmpty(beginDatetime) || StringUtils.isEmpty(endDatetime)) return null;
	
	String startDate = "'" + beginDatetime.substring(0, 10) + "'";
	String endDate = "'" + endDatetime.substring(0, 10) + "'";
	
	Date tempEndDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(endDatetime);
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(tempEndDateTime);
	calendar.set(Calendar.SECOND, 0);
	calendar.add(Calendar.SECOND, -1);		
	endDatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
	
	String startTime = "'" + beginDatetime + "'";
	String endTime = "'" + endDatetime + "'";
	
	String staffType = null;
	if ("TENNIS".equals(type)) staffType = "('FTR','PTR')";
	if ("GOLF".equals(type)) staffType = "('FTG','PTG')";
	
	String sql = "SELECT sp.user_id as userId, sp.surname as surname, "
		+ "sp.given_name as givenName, sp.portrait_photo as portraitPhoto,"
		+ " sp.postal_area as postalArea FROM staff_profile sp "
		+ "LEFT JOIN staff_master sm ON sp.user_id = sm.user_id "
		+ "WHERE sm.user_id NOT IN "
		+ "(SELECT DISTINCT st.staff_user_id from staff_timeslot st "
		+ "WHERE ";
		if(!StringUtils.isEmpty(coachTimeslotId)){
			sql += " st.staff_timeslot_id <> " + coachTimeslotId + " and ( ";
		}
		sql += " (st.begin_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ " and st.status != 'CAN') or (st.end_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ " and st.status != 'CAN') or (st.begin_datetime <= " + startTime + " and st.end_datetime >= " + endTime + " and st.status != 'CAN'))";
		if(!StringUtils.isEmpty(coachTimeslotId)){
			sql += ")";
		}
		sql += " AND sm.staff_type IN "
		+ staffType
		+ " AND sm.status = 'ACT' "
		//+ " AND sm.status = 'ACT' "
		+ "AND ifnull(sm.employ_date, "
		+ startDate
		+ ") <= "
		+ startDate
		+ " AND ifnull(sm.quit_date, "
		+ endDate
		+ ") >= "
		+ endDate
		+ " AND exists(select * from staff_coach_rate_pos scrp where scrp.user_id = sp.user_id)";
	
	    return (List<StaffProfile>)getDtoBySql(sql, null, StaffProfile.class);
    }

    @Override
    public CourseSession getCourseSessionById(Long sysId) {
	return get(CourseSession.class, sysId);
    }

    @Override
	public List<CourseSessionDetailsDto> getCourseSessionDetails(
			String courseId) {
		List<Serializable> listParam = new ArrayList<Serializable>();

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ") 
			    .append("cs.sys_id AS sysId, cs.course_id AS courseId, cs.session_no AS sessionNo, ")
			    .append("cs.coach_timeslot_id AS coachTimeslotId, cs.begin_datetime AS beginDatetime, cs.end_datetime AS endDatetime, ")
			    .append("cs.create_date AS createDate, cs.create_by AS createBy, ")
			    .append("cs.update_date AS updateDate, cs.status AS status, ")
//			    .append("cs.other_train_location AS otherTrainLocation, ")
			    .append("CONCAT(sp.given_name, ' ', sp.surname) AS coach, ")
			    .append("(select ifnull(cs.other_train_location, concat(if(cm.course_type='GSS','Bay# ', 'Court# '), ")
			    .append("group_concat(distinct fm.facility_name order by cast(fm.facility_no as SIGNED) asc Separator ','))) ")
			    .append("from course_session_facility csf left join facility_master fm on csf.facility_no = fm.facility_no ")
			    .append("where csf.course_session_id = cs.sys_id) as gatherLocation ")
			    .append("FROM course_session cs left join staff_profile sp on cs.coach_user_id = sp.user_id ")
			    .append("left join course_master cm on cm.course_id = cs.course_id ")
			    .append("WHERE cs.status!='DEL' AND ");

		if (null != courseId) {
			sql.append("cs.course_id = ? ");
			listParam.add(courseId);
		}
		
		sql.append("order by cs.begin_datetime");

		return getDtoBySql(sql.toString(), listParam, CourseSessionDetailsDto.class);

	}
    
	public List<CourseSessionDetailsDto> getCourseSessionDetails(String courseId, String device) {
	    
	List<Serializable> listParam = new ArrayList<Serializable>();
	StringBuilder sql = new StringBuilder();
	sql.append("SELECT ") 
		    .append("cs.sys_id AS sysId, cs.course_id AS courseId, cs.session_no AS sessionNo, ")
		    .append("cs.coach_timeslot_id AS coachTimeslotId, cs.begin_datetime AS beginDatetime, cs.end_datetime AS endDatetime, ")
		    .append("cs.create_date AS createDate, cs.create_by AS createBy, ")
		    .append("cs.update_date AS updateDate, cs.status AS status, ")
		    .append("CONCAT(sp.given_name, ' ', sp.surname) AS coach, ")
		    .append("'' AS attendingStatus , ")
		    .append("(select ifnull(cs.other_train_location, concat(if(cm.course_type='GSS','Bay# ', 'Court# '), ")
		    .append("group_concat(distinct fm.facility_name order by cast(fm.facility_no as SIGNED) asc Separator ','))) ")
		    .append("from course_session_facility csf left join facility_master fm on csf.facility_no = fm.facility_no ")
		    .append("where csf.course_session_id = cs.sys_id) as gatherLocation ")
		    .append("FROM course_session cs left join staff_profile sp on cs.coach_user_id = sp.user_id ")
		    .append("left join course_master cm on cm.course_id = cs.course_id ");

	if (null != courseId) {
		sql.append("WHERE cs.course_id = ? AND cs.status != 'DEL' ");
		listParam.add(courseId);
	}
	
	if ("WP".equals(device) || "APP".equals(device)) {
	    sql.append("AND cs.status != 'CAN'");
	}
	
	sql.append("order by cs.begin_datetime");
	return getDtoBySql(sql.toString(), listParam, CourseSessionDetailsDto.class);

}
	/**
	 * 查询个人课程状态
	 * @param courseId
	 * @param device
	 * @param customerId
	 * @return
	 */
	public List<CourseSessionDetailsDto> getCourseSessionDetails(String courseId, String device, String customerId) {
	    
	List<Serializable> listParam = new ArrayList<Serializable>();
	StringBuilder sql = new StringBuilder();
	sql.append("SELECT ") 
	    .append("cs.sys_id AS sysId, cs.course_id AS courseId, cs.session_no AS sessionNo, ")
	    .append("cs.coach_timeslot_id AS coachTimeslotId, cs.begin_datetime AS beginDatetime, cs.end_datetime AS endDatetime, ")
	    .append("cs.create_date AS createDate, cs.create_by AS createBy, ")
	    .append("cs.update_date AS updateDate, cs.status AS status, ")
	    .append("CONCAT(sp.given_name, ' ', sp.surname) AS coach, ")
	    .append("sca.status AS attendingStatus , ")
	    .append("(select ifnull(cs.other_train_location, concat(if(cm.course_type='GSS','Bay# ', 'Court# '), ")
	    .append("group_concat(distinct fm.facility_name order by cast(fm.facility_no as SIGNED) asc Separator ','))) ")
	    .append("from course_session_facility csf left join facility_master fm on csf.facility_no = fm.facility_no ")
	    .append("where csf.course_session_id = cs.sys_id) as gatherLocation ")
	    .append("FROM course_session cs left join staff_profile sp on cs.coach_user_id = sp.user_id ")
	    .append("left join course_enrollment ce on ce.course_id = cs.course_id ")
	    .append("left join student_course_attendance sca on sca.course_session_id = cs.sys_id  AND ce.enroll_id = sca.enroll_id ")
	    .append("left join course_master cm on cm.course_id = cs.course_id ")
		.append("WHERE cs.course_id = ? AND cs.status != 'DEL' AND ce.status = 'REG' ")
		.append(" AND ce.customer_id = ? ");
	
	if ("WP".equals(device) || "APP".equals(device)) {
	    sql.append("AND cs.status != 'CAN'");
	}
	listParam.add(courseId);
	listParam.add(customerId);
	sql.append("order by cs.begin_datetime");
	return getDtoBySql(sql.toString(), listParam, CourseSessionDetailsDto.class);

}

    @Override
    public List<StaffProfile> selectAvailableCoachContainCurrent(String type, String beginDatetime, String endDatetime, String staffId) {
	
	if (StringUtils.isEmpty(type) || StringUtils.isEmpty(beginDatetime) || StringUtils.isEmpty(endDatetime)) return null;
	
	String startDate = "'" + beginDatetime.substring(0, 10) + "'";
	String endDate = "'" + endDatetime.substring(0, 10) + "'";
	
	String startTime = "'" + beginDatetime + "'";
	String endTime = "'" + endDatetime + "'";
	String staffUserId = "'" + staffId + "'";
	
	String staffType = null;
	if ("TENNIS".equals(type)) staffType = "('FTR','PTR')";
	if ("GOLF".equals(type)) staffType = "('FTG','PTG')";
	
	String sql = "SELECT sp.user_id as userId, sp.surname as surname, "
		+ "sp.given_name as givenName, sp.portrait_photo as portraitPhoto,"
		+ " sp.postal_area as postalArea FROM staff_profile sp "
		+ "LEFT JOIN staff_master sm ON sp.user_id = sm.user_id "
		+ "WHERE sm.user_id NOT IN "
		+ "(SELECT DISTINCT st.staff_user_id from staff_timeslot st "
		+ "WHERE ((st.begin_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ ") or (st.end_datetime between "
		+ startTime
		+ " and "
		+ endTime
		+ ")) and st.staff_user_id <> "
		+ staffUserId
		+ ") AND sm.staff_type IN "
		+ staffType
		+ " AND sm.status = 'ACT' "
		+ "AND ifnull(sm.employ_date, "
		+ startDate
		+ ") <= "
		+ startDate
		+ " AND ifnull(sm.quit_date, "
		+ endDate
		+ ") >= "
		+ endDate
		+ " AND exists(select * from staff_coach_rate_pos scrp where scrp.user_id = sp.user_id)";
	
	return (List<StaffProfile>)getDtoBySql(sql, null, StaffProfile.class);
    }
    
	@Override
	public List<CourseSession> selectCourseSessionByCourseId(Integer courseId) {
		if (courseId == null) return null;
		String hql = "from CourseSession where courseId = " + courseId;
		return getByHql(hql);
	}

	@Override
	public Long getNextSessionNo(Long courseId) {
	    
	    if (courseId == null) return null;
	    String sql = "select max(cs.session_no) from course_session cs where cs.status <> 'DEL' and cs.course_id = " + courseId;
	    Object object = getUniqueBySQL(sql, null);
	    
	    return (object == null ? 1L : new Long((1 + (Integer) object)));
	}

	@Override
	public void moveSessionNoForword(Long courseId, Long sessionNO) {
	    
	    String sql = "update course_session set session_no = session_no - 1 where course_id = ? and session_no > ?";
	    List<Serializable> param = new ArrayList<Serializable>();
	    param.add(courseId);
	    param.add(sessionNO);
	    sqlUpdate(sql, param);
	}

	@Override
	public List<CourseSessionDto> getCoachSessionList(String staffId, Date fromDate, Date endDate) {
	    
		String coachUserId = "'" + staffId + "'";
		String beginDatetime = "'" + DateConvertUtil.getStrFromDate(fromDate) + "'";
		String endDatetime = "'" + DateConvertUtil.getStrFromDate(endDate) + "'";
		
		String sql = "select "
				+ "cm.course_name as courseName, "
				+ "cs.sys_id as sysId, "
				+ "cast(cs.course_id as CHAR) as courseId,"
				+ "cast(cs.session_no as CHAR) as sessionNo,"
				+ "cs.status as status,"
				+ "cs.coach_user_id as coachUserId,"
//				+ "cs.begin_datetime as beginDatetime,"
//				+ "cs.end_datetime as endDatetime,"
				+ "concat(date_format(begin_datetime, '%Y/%m/%d'), ' ', substring(begin_datetime, 11, 6), '-', substring(end_datetime, 11, 6)) as sessionTime "
				+ "from course_session cs "
				+ "left join course_master cm on cs.course_id = cm.course_id "
				+ "where coach_user_id = "
				+ coachUserId
				+ " and cs.begin_datetime between "
				+ beginDatetime
				+ " and "
				+ endDatetime
				+ " order by cs.begin_datetime";
		

		
//		List<String> params = new ArrayList<String>();
//		params.add(coachUserId);
//		params.add(beginDatetime);
//		params.add(endDatetime);
		
		List<CourseSessionDto> result = getDtoBySql(sql, null, CourseSessionDto.class);
		return result;
	}

	@Override
	public List<MemberCourseAttendanceDto> getMemberAttendanceList(String sessionId) {
	    
	    String sql = "select "
	    	+ "cp.customer_id as customerId, "
	    	+ "concat(cp.given_name, ' ', cp.surname) as customerName, "
	    	+ "cp.portrait_photo as portrait, "
	    	+ "m.academy_no as academyNo, "
	    	+ "sca.status as status "
	    	+ "from student_course_attendance sca "
	    	+ "left join customer_enrollment ce on sca.enroll_id = ce.enroll_id "
	    	+ "left join customer_profile cp on ce.customer_id = cp.customer_id "
	    	+ "left join member m on cp.customer_id = m.customer_id "
	    	+ "where sca.course_session_id = " + sessionId;
	    
	    List<MemberCourseAttendanceDto> result = getDtoBySql(sql, null, MemberCourseAttendanceDto.class);
	    return result;

	}

	@Override
	public List<SessionMemberDto> getStudentAttendanceInfo(String courseId, String sessionNo) {
	    
	    if (StringUtils.isEmpty(courseId) || StringUtils.isEmpty(sessionNo)) return null;
	    
	    String sql = "select m.academy_no as memberId, "
	    	+ "CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) as memberName, "
	    	+ "ifnull(sca.status, 'PND') as attendance, "
	    	+ "sca.update_date as updateDate "
	    	+ "from course_session cs "
	    	+ "left join student_course_attendance sca on cs.sys_id = sca.course_session_id "
	    	+ "left join course_enrollment ce on ce.course_id = cs.course_id "
	    	+ "left join customer_profile cp on cp.customer_id = ce.customer_id "
	    	+ "left join member m on m.customer_id = cp.customer_id "
	    	+ "where cs.course_id = " + courseId
	    	+ " and cs.session_no = " + sessionNo;
	    
	    List<SessionMemberDto> result = getDtoBySql(sql, null, SessionMemberDto.class);
	    return result;
	}

	@Override
	public CourseSession getLatestCourseSessionByCourseId(String courseId) {
	    
	    String sql = "select sys_id as sysId, course_id as courseId, begin_datetime as beginDatetime from course_session "
	    	+ "where course_id = " + courseId
	    	+ " and status = 'ACT' order by begin_datetime asc limit 1";
	    
	    List<CourseSession> result = getDtoBySql(sql, null, CourseSession.class);
	    
	    return (result == null || result.size() == 0) ? null :  result.get(0);
	}

	@Override
	public ContractHelper getSessionPeriodOfCourse(String courseId) {
	    
	    String sql1 = "select min(begin_datetime) as periodFrom, max(end_datetime) as periodTo from course_session where status = 'ACT' and course_id = " + courseId;
	    List<ContractHelper> result1 = getDtoBySql(sql1, null, ContractHelper.class);
	    if (result1 == null || result1.size() == 0) return null; 
	    return result1.get(0);
	}

	@Override
	public CourseSessionSmsParamDto getCourseSessionReminderInfo(Long sysId) {
	    
	    String sql = "select "
	    	+ "cs.sys_id as sysId, "
	    	+ "cm.course_type as courseType, "
	    	+ "cm.course_name as courseName, "
	    	+ "cs.begin_datetime as sessionDate, "
	    	+ "CONCAT(IF(cm.course_type = 'GSS', 'Bay# ', 'Court# '), "
	    	+ "GROUP_CONCAT(DISTINCT fm.facility_name ORDER BY CAST(fm.facility_no AS SIGNED) ASC SEPARATOR ',')) as facility "
	    	+ "from course_session cs "
	    	+ "left join course_session_facility csf on cs.sys_id = course_session_id "
	    	+ "left join facility_master fm on csf.facility_no = fm.facility_no "
	    	+ "left join course_master cm on cm.course_id = cs.course_id "
	    	+ "where cs.sys_id = " + sysId;
	    
	    List<CourseSessionSmsParamDto> result = getDtoBySql(sql, null, CourseSessionSmsParamDto.class);
	    return (result == null || result.size() == 0) ? null : result.get(0); 
	}
	
	
   public List<FacilityMasterDto> getCourseFacilitiesByCourseSessionId(Integer courseSessionId) {
	    
	    String sql = 
	    		"SELECT\n" +
	            "   DISTINCT"+
	    		"	ifnull(facility_master.facility_name,session_facility.facility_no) AS facilityName\n" +
	    		"FROM\n" +
	    		"	course_session_facility session_facility\n" +
	    		"LEFT JOIN facility_master facility_master ON facility_master.facility_no = session_facility.facility_no\n"+
	    		"WHERE\n" +
	    		"	session_facility.course_session_id = ?\n"+
	    		"ORDER BY CAST(session_facility.facility_no as SIGNED INTEGER) asc";
	    List<Serializable> param = new ArrayList<Serializable>();
	    param.add(courseSessionId);
	    
	    return getDtoBySql(sql, param, FacilityMasterDto.class);
	}
   
   public List<MemberFacilityTypeBookingDto> getActiveSessionByCourseId(Long courseId){
	   String sql = "SELECT DISTINCT\n" +
			   "	cs.sys_id AS sessionId\n" +
			   "FROM\n" +
			   "	course_session cs\n" +
			   "WHERE\n" +
			   "	cs.course_id = ?\n" +
			   "AND cs. status = ?\n" +
			   "ORDER BY\n" +
			   "	cs.begin_datetime ASC";
	   List<Serializable> param = new ArrayList<Serializable>();
	   param.add(courseId);
	   param.add("ACT");
	   return getDtoBySql(sql, param, MemberFacilityTypeBookingDto.class);
   }
   
}
