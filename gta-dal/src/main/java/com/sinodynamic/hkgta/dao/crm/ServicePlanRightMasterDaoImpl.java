package com.sinodynamic.hkgta.dao.crm;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.MemberLimitRuleDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRightMaster;

@Repository
public class ServicePlanRightMasterDaoImpl  extends GenericDao<ServicePlanRightMaster>
implements ServicePlanRightMasterDao{

	@Override
	public List<MemberLimitRuleDto> getServicePlanRightMasterDtosByPlanNo(
			Long planNo) {
		String sql = "SELECT m.right_code as rightCode,s.input_value as inputValue,m.right_type as rightType,m.description as description "
				+ "from service_plan_addition_rule s,service_plan_right_master m where s.plan_no = ? and s.right_code = m.right_code";
		List<Long> param = new ArrayList<Long>();
		param.add(planNo);
		List<MemberLimitRuleDto> servicePlanRightMasterDtos = getDtoBySql(sql, param, MemberLimitRuleDto.class);
		return servicePlanRightMasterDtos;
	}

}
