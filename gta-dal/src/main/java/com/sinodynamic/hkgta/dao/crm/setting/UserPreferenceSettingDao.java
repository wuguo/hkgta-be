package com.sinodynamic.hkgta.dao.crm.setting;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;

public interface UserPreferenceSettingDao extends IBaseDao<UserPreferenceSetting> {
	
	/***
	 * check user setting value
	 * @param userId   userId
	 * @param paramId  SMS_NOTIFICATION、BarAndLoungeAlertSwitch and so on
	 * @return
	 */
	public UserPreferenceSetting checkUserSettingValue(String userId,String paramId);

}
