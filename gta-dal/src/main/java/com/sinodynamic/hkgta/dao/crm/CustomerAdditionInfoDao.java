package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.AdditionalInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoCaptionDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAnalysisReportDto;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerAdditionInfoCaption;

public interface CustomerAdditionInfoDao extends IBaseDao<CustomerAdditionInfo> {
	
	public CustomerAdditionInfo getCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public void saveCustomerAdditionInfo(CustomerAdditionInfo t);

	public void updateCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public void deleteCustomerAdditionInfo(CustomerAdditionInfo t) throws Exception;
	
	public List<CustomerAdditionInfo> getCustomerAdditionInfoListByCustomerID(Long customerID);
	
	public CustomerAdditionInfo getByCustomerIdAndCaptionId(Long customerId,Long captionId);
	
	public List<AdditionalInfoDto> getByCustomerIdAndCategory(Long customerId,String category) throws HibernateException;
	
	public int deleteByCustomerId(Long customerId) throws HibernateException;
	
	public List<CustomerAdditionInfoDto> getFavorByCustomerId(Long customerId);

	public List<CustomerAdditionInfoDto> getAnalysisInfo(Long customerId);

	public List<CustomerAdditionInfoDto> getByCustomerIdAndCategoryForTablet(Long customerId,String category,String device);

	/**
	 * 根据customerId获取没有MRK信息的客户添加信息列表
	 * @param customerId
	 * @return
	 * @throws HibernateException
	 */
	public List<CustomerAdditionInfo> getCustomerAdditionInfoNotMRKList(Long customerId) throws HibernateException;

	/**
	 * 获取Marketing id
	 * @return
	 */
	public List<CustomerAdditionInfoCaptionDto> getCustomerAdditionInfoCaptionByMarketing();

	/**
	 * 根据类型获取用户分析报告
	 * @param categoryType
	 * @return
	 */
	public List<CustomerAnalysisReportDto> getCustomerAnalysisReport(String categoryType);
	/***
	 * get AdditionalInfoDto
	 * @param customerId
	 * @param category
	 * @param caption
	 * @return
	 * @throws HibernateException
	 */
	public List<AdditionalInfoDto> getByCustomerIdCaptinOrGory(Long customerId,
			String category,String caption)throws HibernateException;

}
