package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuest;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuestPK;

@Repository
public class UserActivateQuestDaoImpl extends GenericDao<UserActivateQuest> implements UserActivateQuestDao {

	@Override
	public UserActivateQuest getById(UserActivateQuestPK id) throws Exception
	{
		String hql = " from UserActivateQuest q where q.id.userId = '" + id.getUserId() + "' and q.id.questionNo = " + id.getQuestionNo();
		
		return (UserActivateQuest) getUniqueByHql(hql);
	}

	

}
