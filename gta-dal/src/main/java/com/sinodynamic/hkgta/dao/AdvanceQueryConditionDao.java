package com.sinodynamic.hkgta.dao;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;

public interface AdvanceQueryConditionDao {
	public List<AdvanceQueryConditionDto> assembleQueryConditions() ;
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) ;
	
}
