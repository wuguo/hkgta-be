package com.sinodynamic.hkgta.dao.pos;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuCat;

public interface RestaurantMenuCatDao extends IBaseDao<RestaurantMenuCat>
{
	List<RestaurantMenuCat> getRestaurantMenuCatListByRestaurantId(String restaurantId);
	
	String getRestaurantListByRestaurantId();
	
	RestaurantMenuCat getRestaurantMenuCat(String restaurantId, String catId);
}
