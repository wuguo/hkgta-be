package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CashvalueTopupHistory;

@Repository
public class CashvalueTopupHistoryDaoImpl extends GenericDao<CashvalueTopupHistory> implements CashvalueTopupHistoryDao {

	@Override
	public void addCashvalueTopupHistory(CashvalueTopupHistory cth) {
		save(cth);
	}
}
