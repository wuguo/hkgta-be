package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.Constant.Status;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.ServicePlanAdditionRule;

@Repository
public class ServicePlanAdditionRuleDaoImpl extends GenericDao<ServicePlanAdditionRule> implements ServicePlanAdditionRuleDao{

	@Autowired
	private MemberDao memberDao;
	
	@Override
	public List getServicePlanAdditionRuleByPlanNo(Long planNo) {
		String sql = "SELECT sprm.right_code right_code,sprm.input_value_type input_value_type,spar.input_value input_value,sprm.description description "
				+ "  FROM service_plan_addition_rule spar INNER JOIN service_plan_right_master sprm ON spar.right_code = sprm.right_code AND spar.plan_no = "+planNo;
		return getCurrentSession().createSQLQuery(sql).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<ServicePlanAdditionRule> getServicePlanAdditionRuleByCustomerId(Long customerId) {
//		String sql = "SELECT spar.* FROM service_plan_addition_rule spar INNER JOIN service_plan sp  ON sp.plan_no=spar.plan_no INNER JOIN customer_enrollment ce ON ce.subscribe_plan_no = sp.plan_no "
//				+" WHERE ce.customer_id=? ";
		String sql =null;
		Member member=memberDao.getMemberByCustomerId(customerId);
		//if patron EXP and renew service plan have no active excute else sql 
		//else active ,use if sb to sql excute
		if(null!=member&&Status.ACT.name().equals(member.getStatus()))
		{
			StringBuilder sb=new StringBuilder();
			sb.append("SELECT spar.* FROM service_plan_addition_rule spar  \n");
			sb.append(" INNER JOIN service_plan sp  ON sp.plan_no=spar.plan_no \n");
			sb.append(" INNER JOIN (customer_service_acc c,customer_service_subscribe  csb) \n");
			sb.append(" ON c.acc_no=csb.acc_no  AND sp.plan_no=csb.service_plan_no AND c.status='ACT' \n");
			sb.append(" WHERE c.customer_id=? \n");
			sql=sb.toString();	
		}else{
			sql = "SELECT spar.* FROM service_plan_addition_rule spar INNER JOIN service_plan sp  ON sp.plan_no=spar.plan_no INNER JOIN customer_enrollment ce ON ce.subscribe_plan_no = sp.plan_no   WHERE ce.customer_id=? ";
		}
		return getCurrentSession().createSQLQuery(sql).addEntity("spar", ServicePlanAdditionRule.class).setParameter(0, customerId).list();
	}
	
	public ServicePlanAdditionRule getByPlanNoAndRightCode(Long planNo, String rightCode){
		String hqlstr = " from ServicePlanAdditionRule s where s.servicePlan.planNo = ? and s.rightCode = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planNo);
		param.add(rightCode);
		List<ServicePlanAdditionRule> list = getByHql(hqlstr, param);
		if(list!=null&&list.size()>0){
			return list.get(0);
		}
		return null;
	}

}
