package com.sinodynamic.hkgta.dao.sys;

import java.math.BigInteger;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.sys.ProgramDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramURLDto;
import com.sinodynamic.hkgta.entity.crm.RoleProgram;

public interface RoleProgramDao extends IBaseDao<RoleProgram> {

	public List<RoleProgramURLDto> getRoleProgramsWithUrlPath();

	List<ProgramDto> getRoleProgramsById(Long roleId);

}
