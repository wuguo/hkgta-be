package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.mms.SpaRetreatItem;


@Repository
public class SpaRetreatItemDaoImpl  extends GenericDao<SpaRetreatItem>  implements SpaRetreatItemDao {
	
	
	public Serializable addSpaRetreatItem(SpaRetreatItem spaRetreatItem)
	{
		return this.save(spaRetreatItem);
	}
	public SpaRetreatItem getByItemId(Long itemId)
	{
		return this.get(SpaRetreatItem.class, itemId);
	}
}
