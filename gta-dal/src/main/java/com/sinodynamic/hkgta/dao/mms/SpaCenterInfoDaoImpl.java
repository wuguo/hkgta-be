package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.mms.SpaCenterInfo;

@Repository
public class SpaCenterInfoDaoImpl extends GenericDao<SpaCenterInfo> implements SpaCenterInfoDao{

	public Serializable saveSpaCenterInfo(SpaCenterInfo spaCenterInfo)
	{
		return save(spaCenterInfo);		
	}
	
    public SpaCenterInfo getByCenterId(Long centerId)
    {
    	
		String hqlstr  = " from SpaCenterInfo m where m.centerId = ?";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(centerId);
		List<SpaCenterInfo> resultList = getByHql(hqlstr, listParam);
		if(null == resultList || resultList.size() ==0 )
			return null;
		return resultList.get(0);
    }
    
    public SpaCenterInfo getSpaCenterInfo()
    {
    	
		String hqlstr  = " from SpaCenterInfo m";
		List<SpaCenterInfo> resultList = getByHql(hqlstr);
		if(null == resultList || resultList.size() ==0 )
			return null;
		return resultList.get(0);
    }
    
    public void updateSpaCenterInfo(SpaCenterInfo sapAppointmenRec)
    {
    	this.update(sapAppointmenRec);
    }
}
