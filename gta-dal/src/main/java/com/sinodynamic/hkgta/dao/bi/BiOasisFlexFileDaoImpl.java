package com.sinodynamic.hkgta.dao.bi;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.bi.BiOasisFlexFile;
import com.sinodynamic.hkgta.util.DateConvertUtil;

@Repository
public class BiOasisFlexFileDaoImpl extends GenericDao<BiOasisFlexFile>implements BiOasisFlexFileDao {
	
	Logger logger = Logger.getLogger(BiOasisFlexFileDaoImpl.class);
	
	@Override
	public Serializable save(BiOasisFlexFile obj) throws HibernateException {
		// TODO Auto-generated method stub
		return super.save(obj);
	}

	@Override
	public boolean deleteByDate(Date date) {
		if(super.deleteByHql("delete from bi_oasis_flex_file where  DATE_FORMAT(create_date ,'%Y-%m-%d')=? ",DateConvertUtil.parseDate2String(date, "yyyy-MM-dd"))!=0){
			return true;
		}
		return false;
	}
	
	/**+
	 * 获取BiOasisFlexFile每天列表
	 * @return
	 */
	@Override
	public List<BiOasisFlexFile> getBioasisFlexFile(){
		String hqlstr = " from BiOasisFlexFile b GROUP BY  b.oasisTransDate ORDER BY b.oasisTransDate DESC";
		return  getByHql(hqlstr);
	}


}
