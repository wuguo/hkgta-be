package com.sinodynamic.hkgta.dao.mms;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.mms.SpaInvoice;

@Repository
public class SpaInvoiceDaoImpl extends GenericDao<SpaInvoice> implements SpaInvoiceDao {

    @Override
    public void saveSpaInvoice(SpaInvoice spaInvoice) {
	save(spaInvoice);
    }

    @Override
    public void truncateSpaInvoice() {
	
	String sql = "truncate table spa_invoice";
	deleteByHql(sql, (Object[]) null);
    }

    @Override
    public List<SpaInvoice> getSynPaymentInvoiceList() {
	return getByHql("from SpaInvoice");
    }
    
    
}
