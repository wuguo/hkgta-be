package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanPos;
@Repository
public class ServicePlanPosDaoImpl extends GenericDao<ServicePlanPos> implements ServicePlanPosDao{
	public List<ServicePlanPos> getByPlanNo(Integer planNo){
		String hqlstr = "from ServicePlanPos s where s.servicePlan.planNo = "+planNo;
		return getByHql(hqlstr);
	}
}
