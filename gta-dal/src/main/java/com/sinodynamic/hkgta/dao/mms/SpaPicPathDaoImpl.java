package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.mms.SpaCategory;
import com.sinodynamic.hkgta.entity.mms.SpaPicPath;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class SpaPicPathDaoImpl extends GenericDao<SpaPicPath> implements SpaPicPathDao {
	
	@Override
	public Serializable addSpaPicPath(SpaPicPath spaPicPath)
	{
		return this.save(spaPicPath);
	}
	
	@Override
	public SpaPicPath getByPicId(Long picId)
	{
		return this.get(SpaPicPath.class, picId);
	}
	
}
