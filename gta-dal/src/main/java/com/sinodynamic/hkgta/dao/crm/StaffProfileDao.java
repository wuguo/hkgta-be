package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;

/**
 * 
 * @author Mianping_Wu
 * @since 5/4/2015
 */
public interface StaffProfileDao extends IBaseDao<StaffProfile> {
	
	public List<StaffProfile> selectAllStaffProfile(Long enrollId) throws Exception;
	
	
	/**
	 * 
	 * @param sp
	 */
	public void addStaffProfile(StaffProfile sp) throws HibernateException;
	
	
	
	/**
	 * @param sp
	 * @throws HibernateException
	 */
	public void updateStaffProfile(StaffProfile sp) throws HibernateException;
	
	/**
	 * @param userId
	 * @return
	 * @throws HibernateException
	 */
	public StaffProfile getStaffProfileByUserId(String userId) throws HibernateException;

	public StaffProfile getByUserId(String staffUserId) throws HibernateException;
	
	public boolean isEmailUsed(String email) throws HibernateException;

	//added by Kaster 20160509
	public List<StaffProfile> selectAllStaffIdsForAssignTo(String salesStaffId);

}
