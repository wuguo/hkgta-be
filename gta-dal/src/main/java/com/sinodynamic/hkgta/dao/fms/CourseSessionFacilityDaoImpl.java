package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.CourseSessionFacilityDto;
import com.sinodynamic.hkgta.entity.fms.CourseSessionFacility;

@Repository
public class CourseSessionFacilityDaoImpl extends GenericDao<CourseSessionFacility>  implements CourseSessionFacilityDao{

	@Override
	public CourseSessionFacility getCourseSessionFacility(long facilityTimeslotId)
	{
		StringBuffer hql = new StringBuffer(
				"from CourseSessionFacility as csf where csf.facilityTimeslot.facilityTimeslotId = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityTimeslotId);
		List<CourseSessionFacility> courseSessionFacilitys = super.getByHql(hql.toString(), paramList);
		if(null != courseSessionFacilitys && courseSessionFacilitys.size() >0)return courseSessionFacilitys.get(0);
		return null;
	}





    @Override
    public Serializable addCourseSessionFacility(CourseSessionFacility courseSessionFacility) {
	// TODO Auto-generated method stub
	return save(courseSessionFacility);
    }

	@Override
	public List<CourseSessionFacility> getCourseSessionFacilityList(Long courseSessionId)
	{
		StringBuffer hql = new StringBuffer(
				"from CourseSessionFacility as csf where csf.courseSessionId = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(courseSessionId);
		return super.getByHql(hql.toString(), paramList);
	}


    @Override
    public Boolean deleteCourseSessionFacilityBySessionId(Integer courseSessionId) {
	
	String hql = "delete from course_session_facility where course_session_id = ?";
	return deleteByHql(hql, courseSessionId) >= 0 ? true : false;

    }

    
    @Override
    public List<CourseSessionFacility> selectCourseSessionFacilityBySessionId(Long courseSessionId) {
	
	if (courseSessionId == null) return null;
	String hql = "from CourseSessionFacility where courseSessionId = " + courseSessionId;
	return getByHql(hql);
    }


    @Override
    public boolean deleteCourseSessionFacility(CourseSessionFacility courseSessionFacility) {
	return delete(courseSessionFacility);
    }

	@Override
	public List<CourseSessionFacilityDto> getCourseSessionFacilityBySessionId(
			Integer courseSessionId) {
		// TODO Auto-generated method stub
	if (courseSessionId == null) return null;
		String sql = " SELECT f.sys_id AS sysId, f.facility_timeslot_id AS facilityTimeslotId,f.facility_no as facilityNo FROM course_session_facility f WHERE f.course_session_id = ? ";
		ArrayList<Integer> param = new ArrayList<Integer>();
		param.add(courseSessionId);
		return super.getDtoBySql(sql, param, CourseSessionFacilityDto.class);
	}
}
