package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanOfferPos;

public interface ServicePlanOfferPosDao extends IBaseDao<ServicePlanOfferPos> {
	ServicePlanOfferPos getServicePlanOfferPosByFK(Long servPosId, String posItemNo);
}
