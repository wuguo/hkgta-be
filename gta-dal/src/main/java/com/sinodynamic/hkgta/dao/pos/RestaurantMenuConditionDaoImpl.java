package com.sinodynamic.hkgta.dao.pos;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.pos.RestaurantMenuCat;

@Repository("restaurantMenuCondition")
public class RestaurantMenuConditionDaoImpl extends GenericDao<RestaurantMenuCat> implements AdvanceQueryConditionDao
{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
		return assembleQueryConditions(null);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type)
	{
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Category ID", "cat_id", "java.lang.Integer", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Category Name", "category_name", "java.lang.String", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("# of items", "cnt", "java.lang.Integer", "", 3);
		return Arrays.asList(condition1, condition2, condition3);
	}

}
