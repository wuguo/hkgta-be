package com.sinodynamic.hkgta.dao.ibeacon;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.BeaconManageDto;
import com.sinodynamic.hkgta.entity.ibeacon.IbeaconNearbyPerson;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface IbeaconNearbyPersonDao extends IBaseDao<IbeaconNearbyPerson> {

	public ListPage<IbeaconNearbyPerson> getBeaconList(ListPage<IbeaconNearbyPerson> page, BeaconManageDto dto, String byMajor, String status,String targetType);

}
