package com.sinodynamic.hkgta.dao.pms;


import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.StaffRoster;

public interface StaffRosterDao extends IBaseDao<StaffRoster> {

	public List<StaffRoster> getStaffRoster(String staffUserId, Date beginDate, Date endDate);
	
	public List<StaffRoster> getStaffRoster(String staffUserId, Date onDate);
	
	public List<StaffRoster> getStaffRoster(String staffUserId, Date onDate, Integer beginTime, Integer endTime);
}
