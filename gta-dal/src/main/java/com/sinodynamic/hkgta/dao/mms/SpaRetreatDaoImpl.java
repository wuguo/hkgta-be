package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.mms.SpaCategory;
import com.sinodynamic.hkgta.entity.mms.SpaRetreat;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class SpaRetreatDaoImpl extends GenericDao<SpaRetreat> implements SpaRetreatDao {
	
	@Override
	public Serializable addSpaRetreat(SpaRetreat spaRetreat)
	{
		return this.save(spaRetreat);
	}
	
	@Override
	public SpaRetreat getByRetId(Long retId)
	{
		return this.get(SpaRetreat.class, retId);
	}
	
	
	public List<SpaRetreat> getAllSpaRetreats()
	{
		String hqlstr  = " from SpaRetreat m";
		List<SpaRetreat> resultList = getByHql(hqlstr);
		return resultList;
	}
	
	public Long getMaxDisplayOrder(String categoryId)
	{
		String sql = "SELECT MAX(display_order)AS maxOrder FROM spa_retreat WHERE category_id = ? ";
		List param = new ArrayList();
		param.add(categoryId);
		Object result = getUniqueBySQL(sql, param);
	    return Long.valueOf(result!=null?result.toString():"0");
	}
	
}
