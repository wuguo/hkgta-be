package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCardLog;

@Repository
public class InternalPermitCardLogDaoImpl extends GenericDao<InternalPermitCardLog> implements InternalPermitCardLogDao {

    @Override
    public void createInternalPermitCardLog(InternalPermitCard newCard, String oldStatus) throws Exception {
	
	InternalPermitCardLog cardLog = new InternalPermitCardLog();
	cardLog.setCardId(newCard.getCardId());
	cardLog.setContractorId(newCard.getContractorId());
	cardLog.setEffectiveDate(newCard.getEffectiveDate());
	cardLog.setExpiryDate(newCard.getExpiryDate());
	cardLog.setStatusUpdateDate(newCard.getStatusUpdateDate());
	cardLog.setStatusFrom(oldStatus);
	cardLog.setStaffUserId(newCard.getStaffUserId());
	cardLog.setStatusTo(newCard.getStatus());
	cardLog.setUpdateBy(newCard.getUpdateBy());
	save(cardLog);
    }

}
