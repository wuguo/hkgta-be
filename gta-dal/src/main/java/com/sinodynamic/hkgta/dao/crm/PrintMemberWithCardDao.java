package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.MemberCardPrintDto;

/**
 * @author Junfeng_Yan
 * @since May 11 2015
 */
public interface PrintMemberWithCardDao extends IBaseDao<MemberCardPrintDto>{

	/**
	 * @param sql
	 * @param custormerId
	 * @return
	 * @throws HibernateException
	 */
	public MemberCardPrintDto getMemberCardPrint(String sql, List paramList) throws HibernateException;
}
