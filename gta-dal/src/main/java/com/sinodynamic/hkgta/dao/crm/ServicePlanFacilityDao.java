package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;

public interface ServicePlanFacilityDao extends IBaseDao<ServicePlanFacility>{

	public ServicePlanFacility getServicePlanFacilityByCompositeKey(int planNo,
			String typeCode);
	
	public List<ServicePlanFacility> getListByServicePlanNo(Long planNo);

}
