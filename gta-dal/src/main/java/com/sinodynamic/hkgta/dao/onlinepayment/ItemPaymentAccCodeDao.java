package com.sinodynamic.hkgta.dao.onlinepayment;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.onlinepayment.ItemPaymentAccCode;

public interface ItemPaymentAccCodeDao extends IBaseDao<ItemPaymentAccCode> {
	
	/***
	 * get ItemPaymentAccCode list 
	 * @param status 
	 * @param categoryCode  when null then get exclude categoryCode is SPA
	 * else get by categoryCode
	 * @return
	 */
	public List<ItemPaymentAccCode> getItemPaymentAccCodeByStatus(String status,String categoryCode);

}
