package com.sinodynamic.hkgta.dao.pms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepStatusLog;

public interface RoomHousekeepStatusLogDao extends IBaseDao<RoomHousekeepStatusLog>{

}
