package com.sinodynamic.hkgta.dao.pms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskQueryDto;
import com.sinodynamic.hkgta.dto.pms.RoomMemberDto;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;

public interface RoomHousekeepTaskDao extends IBaseDao<RoomHousekeepTask>{

	public List<RoomHousekeepTask> getRoomHousekeepTaskList(Long roomId);
	
	public List<RoomHousekeepTaskQueryDto> getRemindRoomHousekeepTaskList(Integer assistantResTime,Integer inspectorResTime,Integer supervisorResTime);
	
	public List<RoomHousekeepTask> getRoomTaskList(Long roomId,String roleType);

	public List<RoomHousekeepTask> getMaintenanceRoomTaskListByRoomId(Integer roomId);
	
	public List<RoomHousekeepTask> getMyRoomTaskList(Long roomId);
	
	public List<RoomHousekeepTask> getRoomHousekeepTaskUnCmpList(String jobType);
	
	public int cancelRoomHousekeepTask(Long roomId,String jobType);
	
	public List<RoomHousekeepTask> getRoomHousekeepTaskUnCmpList(String roomNo,String jobType);
	
	public List<RoomMemberDto> getMyRoomListForApp(String userId,String roleType,String roomNo);
	
	public List<RoomHousekeepTaskQueryDto> getMyRoomTaskListForApp(String userId,String roleType,String roomNo);
	
	public List<RoomMemberDto> getRoomListForApp(String userId,String roleType,String roomNo);
	
	public List<RoomHousekeepTaskQueryDto> getRoomTaskListForApp(String userId,String roleType,String roomNo);
}
