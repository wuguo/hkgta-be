package com.sinodynamic.hkgta.dao.adm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.entity.crm.StaffMaster;
import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * search for staffmaster
 * 
 * @author Junfeng_Yan
 * 
 * @since May 28 2015
 *
 */
public interface StaffMasterDao extends IBaseDao<StaffMaster>
{


	public List<StaffMaster> getExpireStaff();
	/**
	 * 根据staffId查询此staff是否有效
	 * @param staffId
	 * @return
	 */
	public boolean isStaffActive(String staffId);
}
