package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.mms.SpaFlexDailySummary;

@Repository
public class SpaFlexDailySummaryDaoImpl extends GenericDao<SpaFlexDailySummary>implements SpaFlexDailySummaryDao {

	@Override
	public Serializable save(SpaFlexDailySummary spaFlexDailySummary) {
		return super.save(spaFlexDailySummary);
	}

	@Override
	public boolean deleteByTransactionDate (String transDate) {
		String hql = "delete from spa_flex_daily_summary where DATE_FORMAT(transaction_date,'%Y-%m-%d')='"+transDate+"'";
		int exNum = deleteByHql(hql, null);
		if (exNum > 0) {
			return true;
		}
		return false;
	}

}
