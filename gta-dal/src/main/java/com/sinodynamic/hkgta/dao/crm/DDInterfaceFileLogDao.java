package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.DdInterfaceFileLog;

public interface DDInterfaceFileLogDao extends IBaseDao<DdInterfaceFileLog>{

}
