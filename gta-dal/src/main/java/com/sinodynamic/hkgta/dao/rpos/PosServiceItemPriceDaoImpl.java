package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;

@Repository
public class PosServiceItemPriceDaoImpl extends GenericDao<PosServiceItemPrice> implements
		PosServiceItemPriceDao {

	@Override
	public List getRenewItemPrice(Long planNo,String offerCode) {
		String sql = "SELECT psip.item_price, psip.item_no from service_plan_pos  spp INNER JOIN "
				+ " service_plan_offer_pos spop INNER JOIN pos_service_item_price psip "
				+ " ON spp.serv_pos_id = spop.serv_pos_id and psip.item_no = spop.pos_item_no AND spop.offer_code='"+offerCode
				+ "' AND spp.plan_no = "+planNo;
		 return getCurrentSession().createSQLQuery(sql).list();
	}

	public PosServiceItemPrice getByItemNo(String itemNo){
		return get(PosServiceItemPrice.class, itemNo);
	}
	
	public PosServiceItemPrice getByServicePlanNo(Long planNo){
		String hqlstr = " select pip from PosServiceItemPrice pip, ServicePlan sp, ServicePlanPos spp "
				+ "where sp.planNo = spp.servicePlan.planNo and spp.posItemNo = pip.itemNo and sp.planNo = ? and pip.itemNo like 'SRV%' ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(planNo);
		return (PosServiceItemPrice) getUniqueByHql(hqlstr, param);
	}
}
