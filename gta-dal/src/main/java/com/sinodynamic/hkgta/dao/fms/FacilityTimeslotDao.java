package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.AvailableTimelotDto;
import com.sinodynamic.hkgta.dto.fms.FacilityItemStatusDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotQueryDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotResDto;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;

public interface FacilityTimeslotDao extends IBaseDao<FacilityTimeslot> {

	public List<FacilityTimeslot> getFacilityTimeslotList(String typeCode, Date beginDatetime, Integer venueFloor);
	
	public List<AvailableTimelotDto> getFacilityTimeslotDtoList(String facilityType,Date date, String bayType);
	
	public List<AvailableTimelotDto> getFacilityTimeslotDtoList(String coachId, String facilityType,Date date, String bayType);

	public List<FacilityTimeslot> getFacilityTimeslotListByRange(String typeCode, Date beginDatetime, Date endDatetime, Integer venueFloor);

	public FacilityItemStatusDto getFacilityStatus(FacilityItemStatusDto facilityItemStatusDto);

	public boolean updateFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException;

	public Serializable addFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException;

	public FacilityTimeslot getFacilityTimeslot(Long facility_no,Date beginDatetime,Date endDatetime);

	public FacilityTimeslot getFacilityTimeslotById(long facilityTimeslotId);
	
	public boolean deleteFacilityTimeslot(FacilityTimeslot facilityTimeslot) throws HibernateException;
	
	public List<FacilityTimeslotQueryDto> getFacilityTimeslotGroupTimeCountFacilityAttribute(String facilityType,Date[] times, String facilityAttribute) throws HibernateException;
	
	public List<FacilityTimeslotQueryDto> getFacilityTimeslotGroupTimeCountFacilityAttribute(String facilityType,Date[] times, String facilityAttribute,List<Long> floors,List<Long> facilityNos) throws HibernateException;
	
	public boolean isFacilityAvailable(String facilityNo, Date beginDatetime, Date endDatetime);
	
	public boolean deleteFacilityTimeslotById(Long facilityTimeslotId);
	
    public Boolean deleteFacilityTimeslot(Long facilityTimeslotId) throws HibernateException;
    
	public List<FacilityTimeslot> getFacilityTimeslotByBeginDate(Long facilityNo, Date beginDatetime, Date endDatetime);
		
	public List<FacilityTimeslotResDto> getFacilityTimeslotByResvIds(List<Long> resvIds);
	
	public List<FacilityTimeslot> getFacilityTimeslotListByStatus(String typeCode,String status, Date beginDatetime, Date endDatetime);
}