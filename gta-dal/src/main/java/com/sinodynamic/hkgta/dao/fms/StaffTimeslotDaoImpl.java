package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.AvailableDateDto;
import com.sinodynamic.hkgta.entity.crm.StaffTimeslot;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@Repository
public class StaffTimeslotDaoImpl extends GenericDao<StaffTimeslot> implements StaffTimeslotDao {

    public Serializable addStaffTimeslot(StaffTimeslot slot) {
	
	return save(slot);
    }

    @Override
    public Boolean isCoachVailable(String staffUserId, Date beginDatetime, Date endDatetime) {
	
	String beginTime = DateConvertUtil.date2String(beginDatetime, "yyyy-MM-dd HH:mm:ss");
	String endTime = DateConvertUtil.date2String(endDatetime, "yyyy-MM-dd HH:mm:ss");
	StringBuilder sb = new StringBuilder("From StaffTimeslot where beginDatetime >= '");
	sb.append(beginTime)
	.append("' and endDatetime <= '")
	.append(endTime)
	.append("' and staffUserId = '")
	.append(staffUserId)
	.append("'");
	
	String hql = sb.toString();
	List<StaffTimeslot> list = getByHql(hql);
	
	return list == null || list.size() == 0 ? true : false;
    }

    @Override
    public Boolean deleteStaffTimeslotById(String staffTimeslotId) {
	
	return deleteById(StaffTimeslot.class, staffTimeslotId);
    }

    @Override
    public StaffTimeslot getStaffTimeslotById(String staffTimeslotId) {
	
	if (CommUtil.nvl(staffTimeslotId).length() == 0) return null;
	return get(StaffTimeslot.class, staffTimeslotId);
    }

	@Override
	public List<Date> loadStaffOffdutyDates(String staffId, Date begin, Date end) {
		String sql = "select \n" +
				"    on_date as date, staff_user_id as coachId\n" +
				"from\n" +
				"    (select \n" +
				"        count(*) cnt,\n" +
				"            date_format(begin_datetime, '%Y-%m-%d') as on_date,\n" +
				"            staff_user_id\n" +
				"    from\n" +
				"        staff_timeslot\n" +
				"    where\n" +
				"        staff_user_id = ?\n" +
				"            and begin_datetime between ? and ?\n" +
				"    group by on_date\n" +
				"    having cnt = 16) sub\n";
		List<AvailableDateDto> offduty = this.getDtoBySql(sql, Arrays.asList(staffId,begin,end), AvailableDateDto.class);
		return new ArrayList<Date>(
		CollectionUtil.map(offduty, new CallBack<AvailableDateDto,Date>(){
			@Override
			public Date execute(AvailableDateDto t, int index) {
				try{
					String[] patterns = new String[]{"yyyy-MM-dd"};
					return DateUtils.parseDate(t.getDate(), patterns);
				}catch (Exception e){
					throw new GTACommonException(GTAError.CoachMgrError.FAIL_PARSE_DATE);
				}
			}
		})
		);
	}

	@Override
	public List<StaffTimeslot> loadStaffTimeslot(String staffId, Date begin, Date end) {
		String sql = "from StaffTimeslot where staffUserId = ? and beginDatetime between ? and ? and status='OP' ";
		
		return this.getByHql(sql, Arrays.<Serializable>asList(staffId, begin, end));
	}
}
