package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CorporateAdditionContact;

@Repository
public class CorporateAdditionContactDaoImpl extends GenericDao<CorporateAdditionContact> implements CorporateAdditionContactDao {

	public List<CorporateAdditionContact> getCorporateAdditionContactByCorporateId(Long corporateId){
		String hql = " from CorporateAdditionContact contact where contact.corporateId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(corporateId);
		return getByHql(hql, param);
	}
	
	public CorporateAdditionContact getByContactId(Long contactId){
		String hql = " from CorporateAdditionContact contact where contact.contactId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(contactId);
		return  (CorporateAdditionContact) getUniqueByHql(hql, param);
	}

}
