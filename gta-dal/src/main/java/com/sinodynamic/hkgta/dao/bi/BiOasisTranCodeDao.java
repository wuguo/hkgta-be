package com.sinodynamic.hkgta.dao.bi;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.bi.BiOasisTranCode;

public interface BiOasisTranCodeDao extends IBaseDao<BiOasisTranCode> {

	public List<BiOasisTranCode> getAllBiOasisTranCodeList();
}
