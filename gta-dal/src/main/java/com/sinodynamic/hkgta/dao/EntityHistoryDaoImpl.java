package com.sinodynamic.hkgta.dao;

import com.sinodynamic.hkgta.dto.EntityHistoryDto;

import org.apache.commons.beanutils.ConvertUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class EntityHistoryDaoImpl extends GenericDao implements EntityHistoryDao {

    @Override
    public EntityHistoryDto getEntityHistory(String entityName, String[] names, String[] values) {

        Criteria criteria = getCurrentSession().createCriteria(entityName);

        ClassMetadata classMetadata = getCurrentSession().getSessionFactory().getClassMetadata(entityName);

        ProjectionList projectionList = Projections.projectionList();
        addProjections(classMetadata, projectionList, "createBy");
        addProjections(classMetadata, projectionList, "createDate");
        addProjections(classMetadata, projectionList, "updateBy");
        addProjections(classMetadata, projectionList, "updateDate");
        criteria.setProjection(projectionList);
        criteria.addOrder(Order.desc("updateDate"));

        for (int i = 0; i < names.length; i++) {
            Class nameType = classMetadata.getPropertyType(names[i]).getReturnedClass();

            if (java.util.Date.class.equals(nameType)) {
                nameType = java.sql.Date.class;
            }

            Object value = ConvertUtils.convert(values[i], nameType);
            criteria.add(Restrictions.eq(names[i], value));
        }

        criteria.setResultTransformer(Transformers.aliasToBean(EntityHistoryDto.class));

        EntityHistoryDto result = null;
        List list = criteria.list();
        if (!list.isEmpty()) {
            result = (EntityHistoryDto) criteria.list().get(0);
        }
        
        return result;
    }

    private void addProjections(ClassMetadata classMetadata, ProjectionList projectionList, String field) {
        if (Arrays.asList(classMetadata.getPropertyNames()).contains(field)) {
            projectionList.add(Projections.property(field), field);
        }
    }
}
