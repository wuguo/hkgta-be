package com.sinodynamic.hkgta.dao.fms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;

@Repository("courseListConditions")
public class CourseListConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao
{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Course ID", "courseId", "java.lang.Integer", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Course Name", "courseName", "java.lang.String", "", 2);
		
		final List<SysCode> acceptance=new ArrayList<>();
		SysCode s1=new SysCode();
		s1.setCategory("Acceptance");
		s1.setCodeDisplay("Auto");
		s1.setCodeValue("Auto");
		acceptance.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("Acceptance");
		s2.setCodeDisplay("Manual");
		s2.setCodeValue("Manual");
		acceptance.add(s2);
		

		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Acceptance", "memberAcceptanceValue", "java.lang.String", acceptance, 3);
		
		final List<SysCode> openEnroll=new ArrayList<>();
		SysCode s3=new SysCode();
		s3.setCategory("openEnroll");
		s3.setCodeDisplay("Yes");
		s3.setCodeValue("Y");
		openEnroll.add(s3);

		SysCode s4=new SysCode();
		s4.setCategory("openEnroll");
		s4.setCodeDisplay("No");
		s4.setCodeValue("N");
		openEnroll.add(s4);
		

		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Open Enrollment", "openEnroll", "java.lang.String", openEnroll, 4);
		return Arrays.asList(condition1, condition2, condition3, condition4);
	}

	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Course ID", "courseId", "java.lang.Integer", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Course Name", "courseName", "java.lang.String", "", 2);
		
		final List<SysCode> acceptance=new ArrayList<>();
		SysCode s1=new SysCode();
		s1.setCategory("Acceptance");
		s1.setCodeDisplay("Auto");
		s1.setCodeValue("Auto");
		acceptance.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("Acceptance");
		s2.setCodeDisplay("Manual");
		s2.setCodeValue("Manual");
		acceptance.add(s2);
		

		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Acceptance", "memberAcceptanceValue", "java.lang.String", acceptance, 3);

		final List<SysCode> openEnroll=new ArrayList<>();
		SysCode s3=new SysCode();
		s3.setCategory("openEnroll");
		s3.setCodeDisplay("Yes");
		s3.setCodeValue("Y");
		openEnroll.add(s3);

		SysCode s4=new SysCode();
		s4.setCategory("openEnroll");
		s4.setCodeDisplay("No");
		s4.setCodeValue("N");
		openEnroll.add(s4);
		

		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Open Enrollment", "openEnroll", "java.lang.String", openEnroll, 4);
		return Arrays.asList(condition1, condition2, condition3, condition4);
	}

	

}
