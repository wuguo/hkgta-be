package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.staff.StaffDto;
import com.sinodynamic.hkgta.dto.staff.UserMasterDto;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class UserMasterDaoImpl extends GenericDao<UserMaster> implements UserMasterDao{
	Logger logger = Logger.getLogger(UserMasterDao.class);
	@Resource(name = "appProperties")
	protected Properties appProps;
	
	public UserMaster getUserByLoginId(String loginId) {
		
		String hqlstr = "from UserMaster where loginId = ? ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(loginId);
		return (UserMaster)getUniqueByHql(hqlstr, param);
	}
	
	public UserMaster getUserByUserId(String userId){
		
		String hqlstr = "from UserMaster where userId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(userId);
		return (UserMaster)getUniqueByHql(hqlstr, param);
	}

	public void addUserMaster(UserMaster m){
		this.save(m);
		
	}

	public void getUserMasterList(ListPage<UserMaster> page, String countHql,
			String hql, List<?> param) throws Exception {
		this.listByHql(page, countHql, hql, param);
		
	}

	public void updateUserMaster(UserMaster m) throws Exception {
		this.update(m);
		
	}

	public void deleteUserMaster(UserMaster m) throws Exception {
		this.delete(m);
		
	}

	@Override
	public String saveUserMaster(UserMaster m) throws HibernateException {
		// TODO Auto-generated method stub
		return (String)super.save(m);
	}
	@Override
	public StaffDto getStaffDto(String userId){
		StringBuilder sql = new StringBuilder(" select um.user_id as userId, um.nickname as nickname, um.user_type as userType,  concat(sf.given_name, ' ', sf.surname) staffName , sm.staff_type as staffType ")
			.append(" from user_master um  left join staff_master sm on um.user_id=sm.user_id ")
			.append("  left join staff_profile sf on um.user_id=sf.user_id ")
			.append(" where um.user_id=? ");
		List<Serializable>	param = new ArrayList<Serializable>();
		param.add(userId);
		List<StaffDto> list = this.getDtoBySql(sql.toString(), param, StaffDto.class);
		if(list !=null && list.size()>0){
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * Method to check if the typed loginId exists or not for update
	 * (The current userId is ignored)
	 * @param loginId
	 * @param userId
	 * @return false if exists, true if not exists
	 */
	public boolean checkAvailableLoginId(String loginId, String userId){
		String sqlstr = " select c.user_id as userId from user_master c where c.login_id = ? ";
		List<Serializable> listParam = new ArrayList<Serializable>();
		listParam.add(loginId);
		List<UserMaster> userMasterList = getDtoBySql(sqlstr, listParam, UserMaster.class);
		if (userMasterList != null){
			if (userMasterList.size() == 0) {
				return true;
			} else if (userMasterList.size() > 0) {
				UserMaster userMaster = userMasterList.get(0);
				if (userMaster != null) {// This user exists
					if (userMaster.getUserId().equals(userId)) {
						return true;// update itself
					}
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public ListPage<UserMaster> getUserMasterList(Long roleId,String orderColumn, String order,
			int pageSize, int currentPage, String filters,String userType) {
		ListPage<UserMaster> pageParam = new ListPage<UserMaster>(pageSize, currentPage);
		
		if(order == null) {
			order = "asc";
		}
		if(order.equals("asc")){
			pageParam.addAscending(orderColumn);
		}
		if(order.equals("desc")){
			pageParam.addDescending(orderColumn);
		}
		
		pageParam.addDescending("updateDate");
		
		
		
		StringBuilder sbder=new StringBuilder();
		sbder.append(" SELECT * FROM ( ");
		sbder.append("	SELECT u.user_id AS userId ,");
		sbder.append(" case u.user_id");
		
		sbder.append(" when '"+appProps.getProperty("system.admin")+"' then 1");
		sbder.append(" else 0");
		sbder.append(" end as nickSuperAdmin,");
		sbder.append("  u.login_id AS loginId ,");
		sbder.append("  u.nickname AS nickname,u.status,u.create_by AS createBy,u.update_by AS updateBy,u.user_type AS userType,u.default_module AS defaultModule, u.create_date as createDate,u.update_date as updateDate,");
		
		if(Constant.UserType.CUSTOMER.name().equals(userType))
		{
			sbder.append("c_profile.surname as lastName ,c_profile.given_name as firstName, ");
		}else{
			sbder.append("s_profile.surname as lastName ,s_profile.given_name as firstName ,");
		}
		
		
		if(null==roleId)
		{
			sbder.append("  GROUP_CONCAT(m.role_name)AS roleName FROM user_master u ");
		}else{
			sbder.append("  m.role_name AS roleName FROM user_master u ");
		}

		//patron account to search
		if(Constant.UserType.CUSTOMER.name().equals(userType))
		{
			sbder.append(" LEFT JOIN member member  ON u.login_id=member.academy_no ");
			sbder.append(" LEFT JOIN customer_profile c_profile ON member.customer_id=c_profile.customer_id  ");
			 
		}else{
			sbder.append(" LEFT JOIN staff_profile s_profile  ON u.user_id=s_profile.user_id ");
		}
		
		sbder.append("	LEFT JOIN user_role r ON u.user_id=r.user_id ");
		sbder.append("	LEFT JOIN role_master m ON r.role_id=m.role_id ");
		if(null==roleId)
		{
			if(Constant.UserType.CUSTOMER.name().equals(userType))
			{
				sbder.append(" where  u.user_type='"+Constant.UserType.CUSTOMER.name()+"'  ");
			}else{
				sbder.append(" where  u.user_type <>'"+Constant.UserType.CUSTOMER.name()+"'  ");
			}
			sbder.append("	GROUP BY u.user_id  ");
		}else{
			sbder.append("	where m.role_id="+roleId+" ");
			if(Constant.UserType.CUSTOMER.name().equals(userType))
			{
				sbder.append("  and  u.user_type='"+Constant.UserType.CUSTOMER.name()+"'  ");
			}else{
				sbder.append("  and  u.user_type <>'"+Constant.UserType.CUSTOMER.name()+"'  ");
			}
			
		}
		
		sbder.append("	)AS t "); 
		
		//search all
		String hql=null;
		
		if(null!=roleId)
		{
			StringBuilder roles=new StringBuilder();
			roles.append(" select * from (");
			roles.append(" SELECT b.userId ,");
			roles.append("  b.nickSuperAdmin,");
			roles.append("  b. loginId ,");
			roles.append("  b.nickname,b.status,b.createBy,b.updateBy,b.userType,b.defaultModule, b.createDate,b.updateDate,b.firstName,b.lastName,");
			roles.append("  GROUP_CONCAT(m.role_name)AS roleName ");
			roles.append(" from (");
			roles.append(sbder.toString());
			roles.append(") as b");
			
			// more role mapping customerId
			roles.append(" LEFT JOIN user_role r ON b.userId=r.user_id ");
			roles.append(" LEFT JOIN role_master m ON r.role_id=m.role_id");
			roles.append(" GROUP BY b.userId ");
			roles.append(") as c");
			hql=roles.toString();
			
		}else{
			hql=sbder.toString();
		}
		
		if(!StringUtils.isEmpty(filters)){
			hql = hql + " where " + filters;
		}
		
		 String count = "select count(*) from (" +hql+") as t ";
		 return this.listBySqlDto(pageParam, count, hql, null,new UserMasterDto());//
		 
	}
	
}
