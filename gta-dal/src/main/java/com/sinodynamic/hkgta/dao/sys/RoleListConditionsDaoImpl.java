package com.sinodynamic.hkgta.dao.sys;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository("roleListConditions")
public class RoleListConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao
{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Role Name", "roleName", "java.lang.String", "", 1);
		final List<SysCode> userType=new ArrayList<>();
		 SysCode s1=new SysCode();
		s1.setCategory("userType");
		s1.setCodeDisplay("ADMIN");
		s1.setCodeValue("ADMIN");
		userType.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("userType");
		s2.setCodeDisplay("STAFF");
		s2.setCodeValue("STAFF");
		userType.add(s2);
		
		SysCode s3=new SysCode();
		s3.setCategory("userType");
		s3.setCodeDisplay("SA");
		s3.setCodeValue("SA");
		userType.add(s3);

		final AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("User Type", "forUserType", "java.lang.String", userType, 2);
//		final List<SysCode> status=new ArrayList<>();
//		SysCode status1=new SysCode();
//		status1.setCategory("status");
//		status1.setCodeDisplay("Active");
//		status1.setCodeValue("ACT");
//		status.add(status1);
//
//		SysCode status2=new SysCode();
//		status2.setCategory("status");
//		status2.setCodeDisplay("Inactive");
//		status2.setCodeValue("NACT");
//		status.add(status2);
//		final AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Status", "status", "java.lang.String", status, 3);
		return Arrays.asList(condition1, condition2);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type)
	{
		return null;
	}

}
