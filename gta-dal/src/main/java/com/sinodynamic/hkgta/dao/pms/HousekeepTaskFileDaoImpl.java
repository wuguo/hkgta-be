package com.sinodynamic.hkgta.dao.pms;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.pms.HousekeepTaskFile;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRSortField;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
@Repository
public class HousekeepTaskFileDaoImpl extends GenericDao<HousekeepTaskFile> implements HousekeepTaskFileDao {

	@Override
	public byte[] getHouseKeepTaskAttach(String jobType, String startTime, String endTime, String fileType, String roomNumber,
			String sortBy, String isAscending) throws Exception {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String reportFileName = "";
		if (RoomHousekeepTaskJobType.ADH.name().equals(jobType)) {
			reportFileName = "AdHocTask.jasper";
		} else if ((RoomHousekeepTaskJobType.RUT.name().equals(jobType))) {
			reportFileName = "RoutineTask.jasper";
		} else if ((RoomHousekeepTaskJobType.MTN.name().equals(jobType))) {
			reportFileName = "MaintenanceTask.jasper";
		} else if ((RoomHousekeepTaskJobType.SCH.name().equals(jobType))) {
			reportFileName = "ScheduleTask.jasper";
		} else if ((RoomHousekeepTaskJobType.RA.name().equals(jobType))) {
			reportFileName = "RoomAssignment.jasper";
		}
		String parentjasperPath = reportPath + reportFileName;
		File reFile = new File(parentjasperPath);
		Map<String, Object> parameters = new HashMap<String, Object>();
		DataSource ds = SessionFactoryUtils.getDataSource(this.getsessionFactory());
		Connection dbconn = DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("fromReportDate", startTime);
		parameters.put("toReportDate", endTime);
		parameters.put("fileType", fileType);
		parameters.put("roomNumber", roomNumber);
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if("true".equalsIgnoreCase(isAscending)){
			sortField.setOrder(SortOrderEnum.ASCENDING);
		}else{
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		//Ignore pagination for csv
		if("csv".equals(fileType)){
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType, jasperPrint, outPut);
		if (exporter == null) {
			return null;
		} else {
			exporter.exportReport();
			return outPut.toByteArray();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JRAbstractExporter exportedByFileType(String fileType, JasperPrint jasperPrint, ByteArrayOutputStream outPut) {
		JRAbstractExporter exporter = null;
		switch (fileType) {
		case "pdf":
			exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));
			break;
		case "csv":
			exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(outPut));
			break;
		default:
		}

		return exporter;
	}

	@Override
	public byte[] getRoomWeeklyAttendantAttach(String userId, String userName, String dateSql, String startTime, String endTime, String fileType,
			String sortBy, String isAscending) throws Exception {
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String reportFileName = "RoomAttendant7DaysRoster.jasper";
		String parentjasperPath = reportPath + reportFileName;
		File reFile = new File(parentjasperPath);
		Map<String, Object> parameters = new HashMap<String, Object>();
		DataSource ds = SessionFactoryUtils.getDataSource(this.getsessionFactory());
		Connection dbconn = DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("userId", userId);
		parameters.put("roomAttendantName", userName);
		parameters.put("dateSql", dateSql);
		parameters.put("reportWeek", new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(startTime)) + " - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(endTime)));
//		parameters.put("reportWeek", startTime + " - " + endTime);
		parameters.put("fromReportDate", startTime);
		parameters.put("toReportDate", endTime);
		parameters.put("fileType", fileType);
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if("true".equalsIgnoreCase(isAscending)){
			sortField.setOrder(SortOrderEnum.ASCENDING);
		}else{
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		//Ignore pagination for csv
		if("csv".equals(fileType)){
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType, jasperPrint, outPut);
		if (exporter == null) {
			return null;
		} else {
			exporter.exportReport();
			return outPut.toByteArray();
		}
	}
	
}
