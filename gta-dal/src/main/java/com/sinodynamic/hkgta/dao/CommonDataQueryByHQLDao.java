package com.sinodynamic.hkgta.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * 
 * @author Angus_Zhu
 *
 */
@Repository
public interface CommonDataQueryByHQLDao extends CommonDataQueryDao, IBaseDao {
	Query assembleAdvacneQuery(AdvanceQueryDto queryDto, String joinSQL);

	void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page);

	void getAdvanceQueryResult(AdvanceQueryDto queryDto, String joinSQL, ListPage page, Class clazz);
	
	void getInitialQueryResultByHQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page);
	
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String category);
	
	public void getInitialQueryResultBySQL(AdvanceQueryDto queryDto, String joinSQL, ListPage page);
}
