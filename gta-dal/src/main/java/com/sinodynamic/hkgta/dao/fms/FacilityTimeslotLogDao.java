package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslotLog;

public interface FacilityTimeslotLogDao extends IBaseDao<FacilityTimeslotLog> {

	public List<FacilityTimeslotLog> getByFacilityNo(String facilityType, Long facilityNo);

}
