package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateDate;

@Repository
public class FacilityUtilizationRateDateDaoImpl extends GenericDao<FacilityUtilizationRateDate>  implements FacilityUtilizationRateDateDao{

	@Override
	public FacilityUtilizationRateDate getFacilityUtilizationRateDate(String weekDay,Date specialDate)
	{
		StringBuffer hql = new StringBuffer("select d from FacilityUtilizationRateDate d  where 1=1 ");
		
		List<Serializable> paramList = new ArrayList<Serializable>();
		if(!StringUtils.isEmpty(weekDay)){
			hql.append(" and d.weekDay = ? ");
			paramList.add(weekDay);
		}
		if(null != specialDate){
			hql.append(" and d.specialDate = ? ");
			paramList.add(specialDate);
		}
		List<FacilityUtilizationRateDate> facilityUtilizationRateDates = super.getByHql(hql.toString(),paramList);
		return facilityUtilizationRateDates.size() > 0 ?facilityUtilizationRateDates.get(0): null;
	}

	@Override
	public List<FacilityUtilizationRateDate> getSpecialRateDateList(String facilityType, int year, String subType)
	{
		StringBuffer hql = new StringBuffer("select distinct d from FacilityUtilizationRateDate d where upper(d.facilityType) = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		if(!StringUtils.isEmpty(subType)){
			hql.append(" and d.facilitySubtypeId = ? ");
			paramList.add(subType);
		}
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public List<FacilityUtilizationRateDate> getSpecialRateDateListByDate(String facilityType, Date specialDate, String subType)
	{
		StringBuffer hql = new StringBuffer("select distinct d from FacilityUtilizationRateDate d where d.specialDate = ? and upper(d.facilityType) = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(specialDate);
		paramList.add(facilityType.toUpperCase());
		if(!StringUtils.isEmpty(subType)){
			hql.append(" and d.facilitySubtypeId = ?");
			paramList.add(subType);
		}
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public FacilityUtilizationRateDate getSpecialRateDate(String facilityType, Date specialDate, String facilitySubtypeId)
	{
		StringBuffer hql = new StringBuffer("select distinct d from FacilityUtilizationRateDate d where d.specialDate = ? and upper(d.facilityType) = ? ");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(specialDate);
		paramList.add(facilityType.toUpperCase());
		if(!StringUtils.isEmpty(facilitySubtypeId)){
			hql.append(" and facilitySubtypeId=? ");
			paramList.add(facilitySubtypeId);
		}
		return (FacilityUtilizationRateDate) super.getUniqueByHql(hql.toString(), paramList);
	}
}
