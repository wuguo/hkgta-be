package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;
import com.sinodynamic.hkgta.entity.crm.ContractHelperHistory;

@Repository
public class ContractHelperHistoryDaoImpl extends GenericDao<ContractHelperHistory> implements ContractHelperHistoryDao {
    
    /**
     * This method is used to log ContractHelperHistory info
     */
    @Override
    public void createContractHelperHistory(ContractHelper ch, Long cardId, String staffUser) throws Exception {
	
	ContractHelperHistory chh = new ContractHelperHistory();
	
	Date now = new Date();
	chh.setContractorId(ch.getContractorId());
	chh.setCreateDate(now);
	chh.setStatus(ch.getStatus());
	chh.setHelperPassType(ch.getHelperPassType());
	chh.setPassTypeAssignBy(staffUser);
	chh.setPassTypeAssignDate(now);
	chh.setPeriodFrom(ch.getPeriodFrom());
	chh.setPeriodTo(ch.getPeriodTo());
	chh.setPermitCardId(cardId);
	chh.setRemark(ch.getInternalRemark());
	save(chh);
	
    }

    @Override
    public void updateContractHelperHistory(ContractHelper ch, Long cardId) {
	
	if (ch == null || cardId == null) return;
	
	String hql = "from ContractHelperHistory where "
		+ "contractorId = ? "
		+ "and helperPassType = ? "
		+ "and periodFrom = ? "
		+ "and periodTo = ? order by createDate desc";
	
	List<Serializable> params = new ArrayList<Serializable>();
	params.add(ch.getContractorId());
	params.add(ch.getHelperPassType());
	params.add(ch.getPeriodFrom());
	params.add(ch.getPeriodTo());
	
	List<ContractHelperHistory> data = getByHql(hql, params);
	if (data == null || data.size() == 0) return;
	
	ContractHelperHistory chh = data.get(0);
	chh.setPermitCardId(cardId);
	update(chh);
	
    }
    
    
}