package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationPos;

@Repository
public class FacilityUtilizationPosDaoImpl extends GenericDao<FacilityUtilizationPos> implements FacilityUtilizationPosDao {

	@Override
	public List<FacilityUtilizationPos> getFacilityUtilizationPosList(String facilityType) {
		StringBuffer hql = new StringBuffer("from FacilityUtilizationPos p where upper(p.facilityType) =? ");
		List<Serializable> paramList = new ArrayList<Serializable>();

		if (!StringUtils.isEmpty(facilityType)) {
			paramList.add(facilityType.toUpperCase());
		}
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public int getMaxFacilityUtilizationPosId()
	{
		String hql ="select max(facilityPosId) from FacilityUtilizationPos p ";
		Object object = super.getUniqueByHql(hql);
		if(null == object){
			return 0;
		}
		return (int) object;
	}

	@Override
	public FacilityUtilizationPos getFacilityUtilizationPos(String facilityType,String ageRangeCode, String rateType)
	{
		StringBuffer hql = new StringBuffer("from FacilityUtilizationPos p where upper(p.facilityType) =? and upper(p.ageRangeCode) =? and upper(p.rateType) = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityType.toUpperCase());
		paramList.add(ageRangeCode.toUpperCase());
		paramList.add(rateType.toUpperCase());
		List<FacilityUtilizationPos>  facilityUtilizationPosList =  super.getByHql(hql.toString(), paramList);
		if(null != facilityUtilizationPosList && facilityUtilizationPosList.size()>0){
			return facilityUtilizationPosList.get(0);
		}
		return null;
	}

}
