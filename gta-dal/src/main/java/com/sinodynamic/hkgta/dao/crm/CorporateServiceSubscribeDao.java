package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CorporateServiceSubscribe;

public interface CorporateServiceSubscribeDao extends IBaseDao<CorporateServiceSubscribe>{

	public Serializable saveCorpSerSub(CorporateServiceSubscribe corpSerSub);
	
	public CorporateServiceSubscribe getByAccountNo(Long accountNo);
	
	public CorporateServiceSubscribe getByCorporateId(Long corporateId);
}
