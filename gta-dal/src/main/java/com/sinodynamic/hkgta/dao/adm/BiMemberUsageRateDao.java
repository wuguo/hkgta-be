package com.sinodynamic.hkgta.dao.adm;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.adm.BiMemberUsageRate;

public interface BiMemberUsageRateDao extends IBaseDao<BiMemberUsageRate> 
{
	/***
	 * get getBiMemberUsageRate by params
	 * @param customerId
	 * @param period
	 * @param rateType
	 * @param periodDate
	 * @return BiMemberUsageRate
	 */
   public BiMemberUsageRate getBiMemberUsageRate(Long customerId,String period,String rateType,Date periodDate);

   /**
    * 激活用户批量插入操作
    * @param date
    * @return
    * @throws HibernateException
    */
   public int bulkInsertByActiveMember(String date);

   /**
    * 更新激活用户book数
    * @param date
    * @return
    * @throws HibernateException
    */
   public int updateActiveMemberBookNum(String date, String ids);

   /**
    * 根据参数获取BiMemberUsageRate列表数据
    * @param period
    * @param rateType
    * @param periodDate
    * @return
    */
	public List<BiMemberUsageRate> getBiMemberUsageRateList(String period, String rateType, String periodDate);

	 /**
     * 批量插入最新激活用户操作
     * @param date
     * @return
     * @throws HibernateException
     */
    public int bulkInsertByNewsActiveMember(String date);
}
