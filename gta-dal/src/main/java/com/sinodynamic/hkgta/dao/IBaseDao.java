package com.sinodynamic.hkgta.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.Type;

import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * @author Junfeng_Yan
 * summary:generic dao interface
 * @param <T>
 * @param <PK>
 */
public interface IBaseDao<T extends Serializable>{
	public Session getCurrentSession();
	
	public SessionFactory getsessionFactory();
	
	public ListPage<T> listByHql(final ListPage<T> page, String countHql, String hql, List<?> param) throws HibernateException;

	public ListPage<T> listByHqlDto(final ListPage<T> page, String countHql, String hql, List<?> param,Object dto) throws HibernateException;
		
	public ListPage<T> listBySql(final ListPage<T> page,String countSql, final String sql,List<?> param) throws HibernateException ;
	
	public ListPage<T> listBySqlDto(final ListPage<T> page,String countSql, String sql,List<?> param, Object dto) throws HibernateException;
	
	public ListPage<T> listBySqlDto(ListPage<T> page, String countSql, String sql, List<?> param, Object dto,Map<String, org.hibernate.type.Type> map) throws HibernateException;
	
	public Serializable save(T obj) throws HibernateException;

	public boolean update(T obj) throws HibernateException ;
	
	public boolean delete(T obj) throws HibernateException ;

	public boolean saveOrUpdate(T obj) throws HibernateException ;

	public T get(T object, Serializable id) throws HibernateException ;

	public T get(Class<T> clazz, Serializable PK) throws HibernateException ;

	public T get(Class<T> clazz, Serializable PK, String initializeMethodName) throws Exception ;

	public T get(Class<T> clazz, Serializable PK, String[] initializeMethodNameList) throws Exception;
	
	public List<T> getByCol(T obj , String colName , Serializable colVal, String orderBy) throws HibernateException ;

	public List<T> getByCol(Class<T> clazz , String colName , Serializable colVal, String orderBy) throws HibernateException ;

	public List<T> getByCols(T obj, String[] colName, Serializable[] colVal, String[] orderBy) throws HibernateException;

	public List<T> getByCols(Class<T> clazz , String[] colName , Serializable[] colVal, String[] orderBy) throws HibernateException;

	public T getUniqueByCol(T obj , String colName , Serializable colVal) throws HibernateException ;
	
	public T getUniqueByCol(Class<T> clazz , String colName , Serializable colVal) throws HibernateException ;

	public T getUniqueByCols(T obj , String[] colName , Serializable[] colVal) throws HibernateException ;
  
	public T getUniqueByCols(Class<T> theClass , String[] colName , Serializable[] colVal) throws HibernateException ;

   	public Object lockRec(Class<T> clazz, String colName, Serializable colVal) throws HibernateException;

   	public Object lockRec(Class<T> clazz, String[] colName , Serializable[] colVal) throws HibernateException ;
	
	public List<T> getByHql(String hqlstr) throws HibernateException ;

	public Object getUniqueByHql(String hqlstr) throws HibernateException ;

	public List<T> getByHql(String hqlstr, List<Serializable> param) throws HibernateException;		
	
	public Object getUniqueByHql(String hqlstr, List<Serializable> param) throws HibernateException;    	

	public int hqlUpdate(String sql) throws HibernateException;

	public int hqlUpdate(String sql, List<Serializable> param) throws HibernateException;	
	
	public int sqlUpdate(String sql, List<Serializable> param) throws HibernateException;
	
	public <M> List<M> excuteByHql(Class<M> clazz,  String hqlstr) throws HibernateException ;
	
	public <M> List<M> getDtoByHql(final String hql,  List<?> param, Class<M> dtoClass) throws HibernateException;

	public void evict(T object) throws HibernateException;
	
	/**
	 * delete by ID
	 * @author Junfeng_Yan
	 * @param clazz
	 * @param id
	 * @return
	 */
	public boolean deleteById(Class<T> clazz, Serializable id);

	/**
	 * @author Junfeng_Yan
	 * @param hql
	 * @param obj
	 * @return
	 * @throws HibernateException
	 */
	public int deleteByHql(String hql,Object... obj) throws HibernateException;
	
	public <T> List<T> getDtoBySql(final String hql,  List<?> param, Class<T> dtoClass) throws HibernateException;
	public abstract Object getUniqueBySQL(String sql, List<Serializable> param);

	public <T> List<T> getDtoBySql(String sql, List<?> param, Class<T> dtoClass, Map<String, Type> map) throws HibernateException;
	
	public <M> List<M> excuteByHql(Class<M> clazz, String hqlstr,List<?> param) throws HibernateException;

	public int updateBySQL(String sql, List<?> params) throws HibernateException;
	
	/**
	 * get sql with orderBy by 
	 * @param page
	 * @param hsql
	 * @return
	 */
	public String addOrderBy(ListPage<T> page, String hsql);

}

