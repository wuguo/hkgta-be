package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.adm.BiMemberUsageRate;
@Repository
public class BiMemberUsageRateDaoImpl extends GenericDao<BiMemberUsageRate>implements BiMemberUsageRateDao {

	 private Logger logger = Logger.getLogger(BiMemberUsageRateDaoImpl.class);
	@Override
	public Serializable save(BiMemberUsageRate obj) throws HibernateException {
		// TODO Auto-generated method stub
		return super.save(obj);
	}

	@Override
	public boolean update(BiMemberUsageRate obj) throws HibernateException {
		// TODO Auto-generated method stub
		return super.update(obj);
	}

	@Override
	public boolean delete(BiMemberUsageRate obj) throws HibernateException {
		// TODO Auto-generated method stub
		return super.delete(obj);
	}

	@Override
	public boolean saveOrUpdate(BiMemberUsageRate obj) throws HibernateException {
		// TODO Auto-generated method stub
		return super.saveOrUpdate(obj);
	}

	@Override
	public BiMemberUsageRate getBiMemberUsageRate(Long customerId, String period, String rateType, Date periodDate) {
		 String hql="from BiMemberUsageRate b where b.customerId=? and b.period=? and b.rateType=? and b.periodDate=? ";
		 List<Serializable>param=new ArrayList<>();
		 param.add(customerId);
		 param.add(period);
		 param.add(rateType);
		 param.add(periodDate);
		 return (BiMemberUsageRate)super.getUniqueByHql(hql, param);
	}
	
	 /**
     * 激活用户批量插入操作
     * @param date
     * @return
     * @throws HibernateException
     */
    @Override
    public int bulkInsertByActiveMember(String date){
    		String sql = " INSERT INTO bi_member_usage_rate(`customer_id`, `period`, `period_date`, `rate_type`, `rate`, `create_date`, `update_date`) "
    				+ " SELECT m.customer_id, 'M', DATE_FORMAT(?,'%Y-%m-%d'), 'BOOK', 0, NOW(),NULL "
    				+ " FROM member m WHERE m.`status` = 'ACT' AND m.member_type NOT IN ('MG', 'HG') ";
			return getsessionFactory().getCurrentSession().createSQLQuery(sql).setParameter(0, date).executeUpdate();
    }
    
	 /**
     * 更新激活用户book数
     * @param date
     * @return
     * @throws HibernateException
     */
    @Override
    public int updateActiveMemberBookNum(String date, String ids){
    		String sql = " UPDATE bi_member_usage_rate SET rate = rate+1, update_date = NOW() WHERE rate_type='BOOK' AND period='M' AND period_date = DATE_FORMAT(?,'%Y-%m-%d') AND ( update_date IS NULL OR  DATE_FORMAT(update_date,'%Y-%m-%d') <> DATE_FORMAT(NOW(),'%Y-%m-%d'))  AND customer_id IN (" + ids + ") ";
			return getsessionFactory().getCurrentSession().createSQLQuery(sql).setParameter(0, date).executeUpdate();
    }
    /**
     * 根据参数获取BiMemberUsageRate列表数据
     * @param period
     * @param rateType
     * @param periodDate
     * @return
     */
    @Override
	public List<BiMemberUsageRate> getBiMemberUsageRateList(String period, String rateType, String periodDate) {
		 String hql="from BiMemberUsageRate b where  b.period = ? and b.rateType = ? and b.periodDate= DATE_FORMAT(?,'%Y-%m-%d') ";
		 List<Serializable>param=new ArrayList<>();
		 param.add(period);
		 param.add(rateType);
		 param.add(periodDate);
		 return super.getByHql(hql, param);
	}
    
	 /**
     * 批量插入最新激活用户操作
     * @param date
     * @return
     * @throws HibernateException
     */
    @Override
    public int bulkInsertByNewsActiveMember(String date){
    		String sql = " INSERT INTO bi_member_usage_rate(`customer_id`, `period`, `period_date`, `rate_type`, `rate`, `create_date`, `update_date`) "
    				+ " SELECT m.customer_id, 'M', DATE_FORMAT(?,'%Y-%m-%d'), 'BOOK', 0, NOW(),NULL "
    				+ " FROM member m WHERE m.`status` = 'ACT' AND m.member_type NOT IN ('MG', 'HG')"
    				+ " AND NOT  EXISTS  ( SELECT 1 FROM bi_member_usage_rate b  WHERE b.rate_type='BOOK' AND b.period='M' AND b.period_date = DATE_FORMAT(?,'%Y-%m-%d')  AND b.customer_id = m.customer_id) ";
			return getsessionFactory().getCurrentSession().createSQLQuery(sql).setParameter(0, date).setParameter(1, date).executeUpdate();
    }
}
