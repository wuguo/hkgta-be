package com.sinodynamic.hkgta.dao.fms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dao.crm.ServicePlanDao;
import com.sinodynamic.hkgta.dao.crm.SysCodeDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;

@Repository("corporateMemberItemConditionsDao")
public class CorporateMemberItemConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao {
	
	@Autowired
	private ServicePlanDao servicePlanDao;
    @Autowired
    private SysCodeDao sysCodeDao;

    @Override
    public List<AdvanceQueryConditionDto> assembleQueryConditions() {
	
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Academy ID","t.academyNo", String.class.getName(),"",1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron Name","t.memberName", String.class.getName(),"",2);
		final List<SysCode> memberType=new ArrayList<>();
		SysCode s1=new SysCode();
		s1.setCategory("permitCardmemberType");
		s1.setCodeDisplay("Corporate Dependent Patron");
		s1.setCodeValue("CDM");
		memberType.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("permitCardmemberType");
		s2.setCodeDisplay("Corporate Primary Patron");
		s2.setCodeValue("CPM");
		memberType.add(s2);
		
//		SysCode s3=new SysCode();
//		s3.setCategory("permitCardmemberType");
//		s3.setCodeDisplay("Individual Primary Member");
//		s3.setCodeValue("IPM");
//		memberType.add(s3);
//		
//		SysCode s4=new SysCode();
//		s4.setCategory("permitCardmemberType");
//		s4.setCodeDisplay("Individual Dependent Member");
//		s4.setCodeValue("IDM");
//		memberType.add(s4);
		
		List<ServicePlan> planNameList = servicePlanDao.getByHql("FROM  ServicePlan where passNatureCode='LT' ");
		
		Collection<SysCode> planName = CollectionUtil.map(planNameList, new CallBack<ServicePlan, SysCode> (){

			@Override
			public SysCode execute(ServicePlan r, int index) {
				SysCode sysCode = new SysCode();
				sysCode.setCodeValue(r.getPlanName());
				sysCode.setCodeDisplay(r.getPlanName());
				return sysCode;
			}
			
		});
		List<SysCode> genderCodes = sysCodeDao.selectSysCodeByCategory("gender");
		List<SysCode> memberStatusCodes = sysCodeDao.selectSysCodeByCategory("memberstatus");
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Service Plan","t.planName", String.class.getName(),new ArrayList<SysCode>(planName),3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Patron Type","t.memberType", String.class.getName(),memberType,4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("activationDate","t.activationDate", Date.class.getName(),"",5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("officer","t.officer", String.class.getName(),"",6);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("creditLimit","t.creditLimit", BigDecimal.class.getName(),"",7);
		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Status","t.status", String.class.getName(),memberStatusCodes,8);
//		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Join Date","t.firstJoinDate", Date.class.getName(),"",5);
//		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Mobile Phone","t.mobilePhone", String.class.getName(),"",6);
//		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("License Plate","t.licensePlate", String.class.getName(),"",8);
//		AdvanceQueryConditionDto condition9 = new AdvanceQueryConditionDto("Gender","t.gender", String.class.getName(),genderCodes,9);

		return  Arrays.asList(condition1,condition2,condition3,condition4,condition5,condition6,condition7,condition8);
    }

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		// TODO Auto-generated method stub
		return null;
	}

}
