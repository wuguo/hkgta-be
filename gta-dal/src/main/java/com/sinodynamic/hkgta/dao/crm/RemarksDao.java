package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface RemarksDao extends IBaseDao<CustomerPreEnrollStage> {

	public void addRemark(CustomerPreEnrollStage remarkInfo);
	
	public List<CustomerPreEnrollStage> getRemarkList(Long customerId);
	
	public ListPage<CustomerPreEnrollStage> getRemarkList(ListPage<CustomerPreEnrollStage> page,Long customerId);
	
	public int countUnreadRemark(Long customerId,String loginUserId);
	
	public Serializable saveOrUpdateRemark(CustomerPreEnrollStage cPreEnrollStage);
	
	public int countUnreadPayment(Long orderNo);
	
	public ListPage<CustomerPreEnrollStage> getRemarkListByDto(ListPage<CustomerPreEnrollStage> page,Long customerId);
	
	public Boolean readRemarkByDevice(Long customerId,String loginUserId,String loginDevice);
	
	public Integer countUnreadRemarkByDevice(Long customerId,String loginUserId,String loginDevice);
}
