package com.sinodynamic.hkgta.dao.ibeacon;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.ibeacon.SitePoi;

@Repository
public class SitePoiDaoImpl extends GenericDao<SitePoi> implements SitePoiDao {
	
	 public List<SitePoi> listSitePois(){
		 String hql = "from SitePoi ";
		 return getByHql(hql);
	 }
}
