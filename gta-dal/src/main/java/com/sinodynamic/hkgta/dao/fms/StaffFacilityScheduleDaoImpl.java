package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.StaffFacilitySchedule;

@Repository
public class StaffFacilityScheduleDaoImpl extends GenericDao<StaffFacilitySchedule>  implements StaffFacilityScheduleDao{

	@Override
	public List<StaffFacilitySchedule> getStaffFacilityScheduleList(
			String staffUserId) {
		StringBuffer hql = new StringBuffer(
				"from StaffFacilitySchedule as fs where fs.status != 'NAT' and fs.id.staffUserId = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(staffUserId);
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public StaffFacilitySchedule getStaffFacilityScheduleByFacilityTimeslotId(long facilityTimeslotId)
	{
		StringBuffer hql = new StringBuffer(
				"from StaffFacilitySchedule as fs where fs.id.facilityTimeslotId = ? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityTimeslotId);
		List<StaffFacilitySchedule> staffFacilitySchedules = super.getByHql(hql.toString(), paramList);
		if(null != staffFacilitySchedules && staffFacilitySchedules.size() >0)return staffFacilitySchedules.get(0);
		return null;
	}

	@Override
	public int deleteStaffFacilityScheduleByFacilityTimeslotId(
			Long facilityTimeslotId) throws HibernateException {
		String sql = " delete from staff_facility_schedule where facility_timeslot_id = ? ";
		return super.deleteByHql(sql, facilityTimeslotId);
	}

}
