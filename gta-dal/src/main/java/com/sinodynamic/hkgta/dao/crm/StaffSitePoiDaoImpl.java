package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.StaffSitePoiMonitor;

@Repository
public class StaffSitePoiDaoImpl extends GenericDao<StaffSitePoiMonitor> implements StaffSitePoiDao{

	public List<StaffSitePoiMonitor> listBySiteId(Long sitePoiId) {
	    String hqlstr = "from StaffSitePoiMonitor s where s.sitePoiId = ? ";
	    List<Serializable> param = new ArrayList<Serializable>();
	    param.add(sitePoiId);
		return getByHql(hqlstr, param);
	}
	
	public StaffSitePoiMonitor findByStaff(String satffUserId) {
	    String hqlstr = "from StaffSitePoiMonitor s where s.staffUserId = ? ";
	    List<Serializable> param = new ArrayList<Serializable>();
	    param.add(satffUserId);
	    
	    StaffSitePoiMonitor s = null;
		List<StaffSitePoiMonitor> list = getByHql(hqlstr, param);
		if (list.size() > 0){
			s = list.get(0);
		}
	    return s;
	}
}
