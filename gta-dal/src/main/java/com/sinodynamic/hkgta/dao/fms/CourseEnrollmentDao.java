package com.sinodynamic.hkgta.dao.fms;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.CourseSessionSmsParamDto;
import com.sinodynamic.hkgta.dto.fms.CourseTransactionDto;
import com.sinodynamic.hkgta.dto.fms.MemberAttendanceDto;
import com.sinodynamic.hkgta.dto.fms.MemberInfoDto;
import com.sinodynamic.hkgta.dto.fms.StudentCourseAttendanceDto;
import com.sinodynamic.hkgta.entity.crm.UserDevice;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CourseEnrollmentDao extends IBaseDao<CourseEnrollment> {

	public List<MemberInfoDto> getMemberInfo(String attendance, String courseId, String status, String memberAcceptance);
	
	public CourseEnrollment getCourseEnrollmentById(String enrollId);
	
	public CourseEnrollment getCourseEnrollmentByCourseIdAndCustomerId(Long courseId,Long customerId);
	
	public void updateCourseEnrollment(CourseEnrollment ce);

	List<MemberAttendanceDto> getMemberAttendance(String courseId, Long enrollId,String sessionId);
	
	/**
	 * 
	 * @param courseId
	 * @return
	 * @author Nick_Xiong
	 */
	public int countByCourseId(Long courseId);
	
	public List<CourseEnrollment> getCourseEnrollmentByCourseId(Long courseId) throws HibernateException;  

	/**
	* @param
	* @return Object
	* @author Vineela_Jyothi
	* Date: Jul 28, 2015
	*/
	Object getUserIdByEnrollId(Long enrollId);
	
	public CourseEnrollment getCourseEnrollmentByOrderNo(Long orderNo);
	
	public String checkCustomerServicePlanRight(Long customerId, String courseType);
	
	/**
	* @param
	* @return String
	* @author Vineela_Jyothi
	* Date: Aug 3, 2015
	*/
	public String getCommonQueryForMembersList(String courseId,
			String attendance, String status);
	
	public String getMaxAndMinEnrollDate(Long courseId);

	/**
	* @param
	* @return List<MemberInfoDto>
	* @author Vineela_Jyothi
	* Date: Aug 13, 2015
	*/
	List<MemberInfoDto> getMemberInfoList(ListPage<CourseEnrollment> page,
			String attendance, String courseId, String status,
			String memberAcceptance);
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Sep 1, 2015
	 * @Param @param sessionId
	 * @Param @param academyId
	 * @Param @return
	 * @return String
	 */
	public String getEnrollIdBySessionIdAndCard(String sessionId, String cardNo);
	
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Sep 7, 2015
	 * @Param @param enrollId
	 * @Param @return
	 * @return CustomerOrderTrans
	 */
	public CourseTransactionDto getCustomerOrderTransByEnrollId(String enrollId);
	
	/**
	 * @Author Mianping_Wu
	 * @Date Oct 17, 2015
	 * @Param @param sessionId
	 * @Param @param academyNo
	 * @Param @return
	 * @return String
	 */
	public String getEnrollIdBySessionIdAndMember(String sessionId, String academyNo);
	
	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Oct 17, 2015
	 * @Param @param enrollId
	 * @Param @return
	 * @return StudentCourseAttendanceDto
	 */
	public StudentCourseAttendanceDto getMemberInfoByEnrollId(String enrollId);
	

	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Oct 27, 2015
	 * @Param @param start
	 * @Param @param end
	 * @Param @param application
	 * @Param @return
	 * @return List<UserDevice>
	 */
	public List<UserDevice> getAppUser2Remind(Long sysId, String application);
	
	/**
	 * @Author Mianping_Wu
	 * @Date Oct 27, 2015
	 * @Param @param start
	 * @Param @param end
	 * @Param @return
	 * @return List<CourseSessionSmsParamDto>
	 */
	public List<CourseSessionSmsParamDto> getSessionDuringPeriod(Date start, Date end);
}
