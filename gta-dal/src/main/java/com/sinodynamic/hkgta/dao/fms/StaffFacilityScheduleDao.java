package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.StaffFacilitySchedule;

public interface StaffFacilityScheduleDao extends IBaseDao<StaffFacilitySchedule>{

	public List<StaffFacilitySchedule> getStaffFacilityScheduleList(String staffUserId);
	
	public StaffFacilitySchedule getStaffFacilityScheduleByFacilityTimeslotId(long facilityTimeslotId);
	
	public int deleteStaffFacilityScheduleByFacilityTimeslotId(Long facilityTimeslotId) throws HibernateException;
}
