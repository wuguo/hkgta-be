package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.MemberType;

public interface MemberTypeDao extends IBaseDao<MemberType>{
	public MemberType getByMemberTypeName(String memberTypeName);
	public MemberType getByMemberTypeCode(String typeCode);
}
