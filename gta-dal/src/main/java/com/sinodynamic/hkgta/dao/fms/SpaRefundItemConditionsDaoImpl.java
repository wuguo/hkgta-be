package com.sinodynamic.hkgta.dao.fms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;

@Repository("spaRefundItemConditions")
public class SpaRefundItemConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao {

    @Override
    public List<AdvanceQueryConditionDto> assembleQueryConditions() {
	
    	//String displayName, String columnName, String columnType, String selectName,Integer displayOrder
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Patron Id", "patronId", "java.lang.String", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron Name", "patronName", "java.lang.String", "", 2);
		
		final List<SysCode> paymentType=new ArrayList<>();
		SysCode s1=new SysCode();
		s1.setCategory("paymentType");
		s1.setCodeDisplay("Visa");
		s1.setCodeValue("Visa");
		paymentType.add(s1);
	
		SysCode s2=new SysCode();
		s2.setCategory("paymentType");
		s2.setCodeDisplay("Cash");
		s2.setCodeValue("Cash");
		paymentType.add(s2);
		
		SysCode s3=new SysCode();
		s3.setCategory("paymentType");
		s3.setCodeDisplay("Custom");
		s3.setCodeValue("Custom");
		paymentType.add(s3);
		
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Patron Type", "memberTypeName", "java.lang.String", paymentType, 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("DateTime","dateTime","java.util.Date","",4);
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Amount", "amount", BigDecimal.class.getName(), "", 5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Ref Invoice Id", "refInvoiceId", "java.lang.String", "", 6);
		
		final List<SysCode> status=new ArrayList<>();
		SysCode status1=new SysCode();
		status1.setCategory("status");
		status1.setCodeDisplay("ALL");
		status1.setCodeValue("All");
		status.add(status1);
	
		SysCode status2=new SysCode();
		status2.setCategory("status");
		status2.setCodeDisplay("REF");
		status2.setCodeValue("Refunded");
		status.add(status2);
		
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Status", "Status", "java.lang.String", status, 7);
		return Arrays.asList(condition1, condition2, condition3, condition4, condition5, condition6, condition7);
    }

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		// TODO Auto-generated method stub
		return null;
	}

}
