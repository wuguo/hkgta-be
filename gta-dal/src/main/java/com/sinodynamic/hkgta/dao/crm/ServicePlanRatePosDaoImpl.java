package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRatePos;
@Repository
public class ServicePlanRatePosDaoImpl extends GenericDao<ServicePlanRatePos> implements ServicePlanRatePosDao {

	@Override
	public int deleteServicePlanRatePosByServPosId(Long servPosId) {
		// TODO Auto-generated method stub
		String hql = "Delete FROM ServicePlanRatePos s where s.servPosId = ?";
		return super.deleteByHql(hql, servPosId); 
		
	}

           

}
