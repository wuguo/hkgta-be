package com.sinodynamic.hkgta.dao.sys;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dao.adm.UserMasterDao;
import com.sinodynamic.hkgta.dto.crm.MemberFacilityAttendanceDto;
import com.sinodynamic.hkgta.dto.sys.ProgramDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramURLDto;
import com.sinodynamic.hkgta.entity.crm.RoleProgram;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityAttendance;

@Repository
public class RoleProgramDaoImpl extends GenericDao<RoleProgram> implements RoleProgramDao {

	private final Logger logger = Logger.getLogger(UserMasterDao.class);

	@Override
	public List<RoleProgramURLDto> getRoleProgramsWithUrlPath() {
		logger.debug("querying for role_programs with urlPath.");
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT rm.role_name as roleName, uri.uri_path as uriPath, rp.access_right as accessRight ")
		.append("FROM hkgta.role_program rp ")
		.append("left join role_master rm on rm.role_id = rp.role_id ")
		.append("left join program_sub_link pl on rp.program_id = pl.program_id ")
		.append("left join uri_master uri on pl.uri_id = uri.uri_id ")
		.append("where uri.status = 'ACT'");
		
		
		List<RoleProgramURLDto> rpList = getDtoBySql(sql.toString(), null, RoleProgramURLDto.class);
		
		return rpList;
	}
	
	@Override
	public List<ProgramDto> getRoleProgramsById(Long roleId) {
		logger.debug("querying for role_programs by role id.");
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		typeMap.put("programId", StringType.INSTANCE);
		typeMap.put("accessRight", StringType.INSTANCE);
		
		String sql = "SELECT rp.program_id as programId, rp.access_right as accessRight FROM role_program rp where rp.role_id = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(roleId);
		List<ProgramDto> rpList = getDtoBySql(sql, param, ProgramDto.class, typeMap);
		
		return rpList;
	}

}
