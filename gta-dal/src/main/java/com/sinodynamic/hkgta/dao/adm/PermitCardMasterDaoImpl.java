package com.sinodynamic.hkgta.dao.adm;

import java.util.Date;
/*
 * @Author Becky
 * @Date 4-29
*/
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
@Repository
public class PermitCardMasterDaoImpl  extends GenericDao<PermitCardMaster> implements PermitCardMasterDao {
	
	public List<PermitCardMaster> getPermitCardMasterByCustomerId(Long customerId){
		
		String hql = "from PermitCardMaster p where p.mappingCustomerId = " + customerId;
		
		return getByHql(hql);
		
	}

	@Override
	public PermitCardMaster getPermitCardMasterByCardNo(String cardNo)
			throws HibernateException {
		return super.get(PermitCardMaster.class, cardNo);
	}

	@Override
	public boolean updatePermitCardMaster(PermitCardMaster permitCardMaster)
			throws HibernateException {
		// TODO Auto-generated method stub
		return super.update(permitCardMaster);
	}

	@Override
	public boolean linkCardForMember(PermitCardMaster permitCardMaster) throws HibernateException {
		return super.update(permitCardMaster);
	}

	/**
	 * @author Vineela_Jyothi
	 */
	@Override
	public String getCards(String status) {
		
		StringBuilder sql = new StringBuilder(""); 
		String date = "";
		try {
			date = DateCalcUtil.formatDate(new Date());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sql.append("select b.academy_no as academyNo, concat(c.salutation, ' ', c.given_name, ' ', c.surname) as memberName,"
			+ " a.card_no as cardNo, a.card_type_name as cardTypeName, a.card_purpose as cardPurpose, b.customer_id as customerId,"
			+ " a.remark, a.status, a.card_issue_date as cardIssueDate, b.member_type as memberType, m.type_name as memberTypeName,"
			+ " b.superior_member_id as superiorMemberId,"
			+ " a.status_update_date as statusUpdateDate, "
			+ "		CASE b.member_type\n" 
			+ "	WHEN 'IPM' THEN\n" 
			+ "		(\n" 
			+ "			SELECT\n" 
			+ "				plan.plan_name\n" 
			+ "			FROM\n" 
			+ "				customer_service_acc inter,\n" 
			+ "				customer_service_subscribe sub,\n" 
			+ "				service_plan plan\n" 
			+ "			WHERE\n" 
			+ "				sub.acc_no = inter.acc_no\n" 
			+ "			AND plan.plan_no = sub.service_plan_no\n" 
			+ "			AND inter.customer_id = b.customer_id\n" 
			+ "			AND date_format('" + date + "', '%Y-%m-%d') >= inter.effective_date\n" 
			+ "			ORDER BY\n" 
			+ "				inter.expiry_date DESC\n" 
			+ "			LIMIT 0,\n" 
			+ "			1\n" 
			+ "		)\n" 
			+ "	WHEN 'CPM' THEN\n" 
			+ "		(\n" 
			+ "			SELECT\n" 
			+ "				plan.plan_name\n" 
			+ "			FROM\n" 
			+ "				customer_service_acc inter,\n" 
			+ "				customer_service_subscribe sub,\n" 
			+ "				service_plan plan\n" 
			+ "			WHERE\n" 
			+ "				sub.acc_no = inter.acc_no\n" 
			+ "			AND plan.plan_no = sub.service_plan_no\n" 
			+ "			AND inter.customer_id = b.customer_id\n" 
			+ "			AND date_format('" + date + "', '%Y-%m-%d') >= inter.effective_date\n" 
			+ "			ORDER BY\n" 
			+ "				inter.expiry_date DESC\n" 
			+ "			LIMIT 0,\n" 
			+ "			1\n" 
			+ "		)\n" 
			+ "	WHEN 'IDM' THEN\n" 
			+ "		(\n" 
			+ "			SELECT\n"
			+ "				plan.plan_name\n" 
			+ "			FROM\n" 
			+ "				customer_service_acc inter,\n" 
			+ "				customer_service_subscribe sub,\n" 
			+ "				service_plan plan\n" 
			+ "			WHERE\n" 
			+ "				sub.acc_no = inter.acc_no\n" 
			+ "			AND plan.plan_no = sub.service_plan_no\n" 
			+ "			AND inter.customer_id = b.superior_member_id\n" 
			+ "			AND date_format('" + date + "', '%Y-%m-%d') >= inter.effective_date\n" 
			+ "			ORDER BY\n" 
			+ "				inter.expiry_date DESC\n" 
			+ "			LIMIT 0,\n" 
			+ "			1\n" 
			+ "		)\n" 
			+ "	WHEN 'CDM' THEN\n" 
			+ "		(\n" 
			+ "			SELECT\n" 
			+ "				plan.plan_name\n" 
			+ "			FROM\n" 
			+ "				customer_service_acc inter,\n" 
			+ "				customer_service_subscribe sub,\n" 
			+ "				service_plan plan\n" 
			+ "			WHERE\n" 
			+ "				sub.acc_no = inter.acc_no\n" 
			+ "			AND plan.plan_no = sub.service_plan_no\n" 
			+ "			AND inter.customer_id = b.superior_member_id\n" 
			+ "			AND date_format('" + date + "', '%Y-%m-%d') >= inter.effective_date\n" 
			+ "			ORDER BY\n" 
			+ "				inter.expiry_date DESC\n" 
			+ "			LIMIT 0,\n" 
			+ "			1\n" 
			+ "		)\n" 
			+ "	ELSE\n" 
			+ "		NULL\n" 
			+ "	END AS planName,\n" 
			+ " a.update_by as updateBy"
			+ " from member b"
			+ " left join permit_card_master a ON a.mapping_customer_id = b.customer_id"
			+ ", customer_profile c, member_type m"
			+ " where b.customer_id = c.customer_id"
			+ " and b.status = 'ACT'"
			+ " and b.member_type not in ('HG', 'MG')"
			+ " and c.is_deleted <>'Y'"
			+ " and b.member_type = m.type_code"
			+ " and (a.card_no is null or a.status_update_date = (select max(status_update_date) from permit_card_master pcm"
			+ " where b.customer_id = pcm.mapping_customer_id ))");

		if ("NCD".equalsIgnoreCase(status)) {
			sql.append(" and (a.status is null or a.status <> 'ISS')");
			
		} else if ("ISS".equalsIgnoreCase(status)) {
		    	sql.append(" and (a.status = 'ISS')");
		}
		
		String joinSql = "SELECT * FROM (" + sql.toString() + ") t";
		return joinSql;
		
	}

	
	@Override
	public List<PermitCardMaster> getIssuedCardByCustomerId(Long customerId) {
	    
	    String hql = "from PermitCardMaster p where p.status = 'ISS' and p.mappingCustomerId = " + customerId;
	    return getByHql(hql);
	}

	@Override
	public PermitCardMaster getLatestCustomerOrderPermitCardByCustermerId(
			Long customerId) {
		String hql = "from PermitCardMaster p where p.mappingCustomerId = " + customerId + "order by statusUpdateDate desc";
		List<PermitCardMaster> list = getByHql(hql);
		if(null==list || list.size()<=0){
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[]{"PermitCardMaster mappingCustomerId not right:"+customerId});
		}
		return list.get(0);
	}
	
}
