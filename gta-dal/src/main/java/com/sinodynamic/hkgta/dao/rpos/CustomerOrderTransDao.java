package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.RefundedSpaPaymentDto;
import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.dto.rpos.CashValueLogDto;
import com.sinodynamic.hkgta.dto.rpos.CashValueSumDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.NetValue;

import net.sf.jasperreports.engine.JRException;

public interface CustomerOrderTransDao extends IBaseDao<CustomerOrderTrans> {

	public List<CustomerOrderTrans> getPaymentDetailsByOrderNo(Long orderNo);
	public BigDecimal  getFalueUnreadStatus(String type);
	public List<CustomerOrderTrans> getPaymentDetailsByTransactionNO(Long transactionNO);

	public List getLatestTransationActivaties(int totalSize);
	
	public Serializable saveCustomerOrderTrans(CustomerOrderTrans customerOrderTrans);
	
	public ListPage<CustomerOrderTrans> getTransactionList(ListPage<CustomerOrderTrans> page, Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending, String clientType, String filterSQL);

	public ListPage<CustomerOrderTrans> getTopupHistory(
			ListPage<CustomerOrderTrans> page, Long customerID,
			String pageNumber, String pageSize, String sortBy,
			String isAscending, String filterSQL);
	
	public List<CustomerOrderTrans> getTimeoutPayments(Long timeoutMin);
	
	List<CustomerOrderTrans> getTimeoutPayments(Long timeoutMin,String paymentMedia);
	
	public BigDecimal getApprovedPaymentAmount(Long orderNo);
	
	public BigDecimal getCustomerOrderTransAmoutByDay(long customerId,Date date);
	
	public BigDecimal getCustomerOrderTransAmoutByMonth(long customerId,int month);
	
	public byte[] getInvoiceReceipt(Long orderNo,String transactionNo,String receiptType) throws JRException;

	public ListPage<CustomerOrderTrans> getSattlementReport(ListPage<CustomerOrderTrans> page,
			SearchSettlementReportDto dto);

	public byte[]  getDailyRevenueAttachment(String startTime, String endTime, String location,String fileType, NetValue netValue) throws JRException;
	
	public  List<CustomerOrderTrans> getDailyRevenue(String startTime, String endTime, String location);
	
	
	public ListPage<CustomerOrderTrans> getMonthlyEnrollment(ListPage<CustomerOrderTrans> page,String year, String month);
	
 
	public byte[] getMonthlyEnrollmentAttachment(String year,String month,String fileType,String sortBy,String isAscending) throws JRException;

	public ListPage<CustomerOrderTrans> getCorporateMembershipSettlement(ListPage<CustomerOrderTrans> page, String status,String offerCode, Long filterByCustomerId);
	
	public ListPage<CustomerOrderTrans> getDailyMonthlyPrivateCoachingRevenue(String timePeriodType, ListPage<?> page,
			String selectedDate, String facilityType);

	public byte[] getDailyMonthlyPrivateCoachingRevenueAttachment(String timePeriodType, String selectedDate,
			String fileType, String sortBy, String isAscending, String facilityType) throws Exception;
	
	public Date getLastestSpaSyncTime();
	
	public byte[] getRenewalEnrollmentAttachment(String fileType,String sortBy,String isAscending) throws JRException;

	public ListPage getRefundedSpaPaymentList(ListPage<RefundedSpaPaymentDto> page);
	
	/**
	 * 根据【cuseromerId】获取【day】天内member的CashValue变更日志查询语句
	 * @param type
	 * @param customerId
	 * @param day
	 * @return
	 */
	public String getCashValueChangesLog(String type, Long customerId, int day);

	/**
	 * 获取加减总数
	 * @param customerId
	 * @return
	 */
	public CashValueSumDto getMemberDebitCashValueSum(long customerId);
	
	public byte[] getCustomerOrderTransAttachment(String startDate, String endDate, String fileType, String sortBy,
			String isAscending)throws JRException ;
	/**
	 * 获取所有member加减总数
	 * @param customerId
	 * @return
	 */
	public CashValueSumDto getAllDebitAndCreditCashValueSum();
	/**
	 * 根据条件获取cashvalue交易记录列表
	 * @param type
	 * @param customerId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<CashValueLogDto> getCashValueChangesLog(String type, Long customerId, String startTime, String endTime);

}
