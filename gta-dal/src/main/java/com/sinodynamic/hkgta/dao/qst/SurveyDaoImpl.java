package com.sinodynamic.hkgta.dao.qst;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.qst.Survey;

@Repository
public class SurveyDaoImpl extends GenericDao<Survey>implements SurveyDao {



	/**
	 * 获取问卷设置
	 * @return
	 */
	@Override
	public List<Survey> getQuestionnaireSetting(){
		return this.getByHql(" FROM Survey ");
	}
	
	
	

	@Override
	public List<Survey> getAll() {
		 String hql="from Survey";
		return super.getByHql(hql);
	}


}
