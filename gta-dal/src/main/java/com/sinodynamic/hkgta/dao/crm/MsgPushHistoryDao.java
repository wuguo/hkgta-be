package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.MsgPushHistory;

public interface MsgPushHistoryDao extends IBaseDao<MsgPushHistory> {
    
    public void addMsgPushHistory(MsgPushHistory msgPushHistory);
    
    public void modifyMsgPushHistory(MsgPushHistory msgPushHistory);
}
