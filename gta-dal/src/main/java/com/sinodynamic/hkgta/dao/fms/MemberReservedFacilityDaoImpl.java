package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.fms.MemberReservedFacility;

@Repository
public class MemberReservedFacilityDaoImpl extends GenericDao<MemberReservedFacility>  implements MemberReservedFacilityDao{

	@Override
	public List<MemberReservedFacility> getMemberReservedFacilityList(long resvId){
			
		String hql = " SELECT mrf FROM MemberReservedFacility as mrf where mrf.resvId=? ";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(resvId);
		return super.getByHql(hql, paramList);
	}

	@Override
	public boolean deleteMemberReservedFacility(
			MemberReservedFacility memberReservedFacility) {
		return super.delete(memberReservedFacility);
	}

	@Override
	public MemberReservedFacility getMemberReservedFacilityListByFacilityTimeslotId(
			long facilityTimeslotId) {
		String hql = " SELECT mrf FROM MemberReservedFacility as mrf where mrf.facilityTimeslotId=? ";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(facilityTimeslotId);
		return (MemberReservedFacility) super.getUniqueByHql(hql, paramList);
	}

	@Override
	public int deleteMemberReservedFacilityByFacilityTimeslotId(
			Long facilityTimeslotId) throws HibernateException {
		String sql = " delete from member_reserved_facility where facility_timeslot_id = ? ";
		return super.deleteByHql(sql, facilityTimeslotId);
	}

}
