package com.sinodynamic.hkgta.dao.pms;

import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.TLock;

public interface PMSRequestProcessorDao extends IBaseDao{

	public List<Integer>  queryAllAvailableSpecifiedFacilityByPeriod(Date startTime,Date endTime,String facilityType,String attributeId,String status,int fromFacilityNo,int endFacilityNo);
	
	public int countSpecifiedFacility(String facilityType,String attributeId,String status,int fromFacilityNo,int endFacilityNo);
	
	public int countReservedSpecifiedFacility(Date startTime,Date endTime,String facilityType,String attributeId);
	
	public int isoccupied(Integer facilityNo,Date startTime,Date endTime);
	
	public void lockFacilityMaster();
	
	public void getLock();
	
	public List<TLock>  getAllLocks();
	
	public void saveOrUpdateLockRecord(Long id,String lockName);

	public List<Integer> queryAllAvailableSpecifiedFacilityByPeriod(
			Date startTime, Date endTime, String facilityType,
			String attributeId, String status);

	public int countSpecifiedFacility(String facilityType, String attributeId,String status);
}
