package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;

public interface CustomerOrderHdDao extends IBaseDao<CustomerOrderHd>{

	public Serializable addCustomreOrderHd(CustomerOrderHd customerOrderHd);
	public CustomerOrderHd getOrderById(Long orderNo);
	public CustomerOrderHd getActiveOrderByEnrollId(Long enrollId);
	public BigDecimal getSpendingTotalDuringRange(Long customerId, Date beginDate, Date endDate);
	public CustomerOrderHd getOrderByExtInvoiceNo(String extInvoiceNo);
}
