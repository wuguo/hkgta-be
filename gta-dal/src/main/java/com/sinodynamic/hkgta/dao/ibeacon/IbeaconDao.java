package com.sinodynamic.hkgta.dao.ibeacon;

import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.BeaconManageDto;
import com.sinodynamic.hkgta.dto.crm.BeaconRoomDto;
import com.sinodynamic.hkgta.entity.ibeacon.Ibeacon;
import com.sinodynamic.hkgta.entity.ibeacon.IbeaconNearbyPerson;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface IbeaconDao extends IBaseDao<Ibeacon> {

	public Ibeacon getByMinorAndMajor(Long major, Long minor);

	public IbeaconNearbyPerson getNearbyMemberById(String userId, Long ibeaconId);

	public boolean updateIbeaconStatus(String status, Long ibeaconIdDB, String loginUserId);

	public boolean updateMemberBeaconDetail(String description, String venueCode, Long poiId, String loginUserId);

	public Ibeacon getIbeaconByPK(Long ibeaconIdDB);

	public boolean checkAvailableMinor(Long major, Long minor, Long ibeaconIdDB);

	public List<BeaconManageDto> getListByStatus(String status);

	public int deleteIbeaconById(Long ibeaconIdDB);

	public int deleteIbeaconNearbyPersonByIbeaconId(Long ibeaconIdDB);

	public Map<String, Object> getMemberByVenueCode(Long[] poiIdArray, String venueCode, Integer beaconMemberCount);

	public List<BeaconManageDto> getBeaconByVenueCode(String venueCode);

	public boolean changeMajor(Long major,String targetType);

	public List<BeaconRoomDto> getRoomBeaconsList();
	
	public List<BeaconRoomDto> getRoomBeaconsList(Long roomId);
	
	/**
	 * Used to update the housekeep's beacon detail. E.g. Change room's related beacon Id
	 * @author Liky_Pan
	 * @param description  The name of Beacon
	 * @param pastRoomId The origin beacon's room Id
	 * @param presentRoomId The updated beacon's room Id
	 * @param poiId The beacon's position id
	 * @param loginUserId The login user's Id
	 */
	public boolean updateHousekeepBeaconDetail(String description, Long pastRoomId, Long presentRoomId, Long pastPoiId,Long presentPoiId, String loginUserId);
	
	public Long getTheNextAvailableMinorId(Long major);
	
	/**
	 * Used to check if the selected major is available
	 * @author Liky_Pan
	 * @param major Major Id
	 * @param targetType MBR or HKC
	 * @return true if available, false if unavailable
	 */
	public boolean checkMajorAvailability(Long major,String targetType);
	
	public int deleteSitePoi(Long site_poi_id);
	
	public int removeSitPoiInRoom(String loginUserId, Long sitePoiId);
	
	public List<Ibeacon> getIbeaconBySitePoi(Long sitePoiId);
	
	public ListPage<Ibeacon> getMemberByVenueCodeForTablet(ListPage<Ibeacon> page, String venueCode);

}
