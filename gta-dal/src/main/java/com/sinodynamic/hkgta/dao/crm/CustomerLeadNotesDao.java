package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.CustomerLeadNote;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface CustomerLeadNotesDao extends IBaseDao<CustomerLeadNote> {
	
	public CustomerLeadNote getCustomerLeadNoteById(Long nodeId);

	public boolean saveCustomerLeadNote(CustomerLeadNote customerLeadNote) throws Exception;
	
	public boolean deleteById(Long nodeId) throws Exception;
	
	public ListPage<CustomerLeadNote> listByCustomerId(Long customerId, ListPage<CustomerLeadNote> page);
}
