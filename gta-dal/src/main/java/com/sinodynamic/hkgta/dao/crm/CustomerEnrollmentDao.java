package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.util.pagination.ListPage;


/**
 * 
 * @author Becky Wu
 *
 */
public interface CustomerEnrollmentDao extends IBaseDao<CustomerEnrollment>{

	public ListPage<CustomerEnrollment> selectAllCustomerEnrollments(ListPage<CustomerEnrollment> page) throws HibernateException;
	
	public ListPage<CustomerEnrollment> selectCustomerEnrollmentInfoByStaffName(ListPage<CustomerEnrollment> page, String name) throws HibernateException;
	
	public ListPage<CustomerEnrollment> selectCustomerEnrollmentInfoByCustomerName(ListPage<CustomerEnrollment> page, String name) throws HibernateException;
	
	public boolean checkAvailableAcademyID(String academyNo) throws HibernateException;
	
	public String checkEnrollmentStatus(Long customerId)throws HibernateException;
	
	public boolean deleteEnrollmentsWithoutPayment(Long customerId)throws HibernateException;
	
	public CustomerEnrollment getCustomerEnrollmentByCustomerId(Long customerId);
	
	public CustomerEnrollment getCustomerEnrollmentByEnrollId(Long enrollId);
	
	public Serializable updateCustomerEnrollment(CustomerEnrollment customerEnrollment);
	
	public void addCustomerEnrollment(CustomerEnrollment ce);

	public ListPage<CustomerEnrollment> viewEnrollmentActivationOfMember(
			ListPage<CustomerEnrollment> page, Serializable[] pengdingmemberlist);
	
	public void deleteLeadCustomerEnrollment(CustomerEnrollment customerEnrollment);
	
	public boolean setAcademyID(String academyNo,Long customerId) throws HibernateException;
	
	/*public int updateEnrollmentStatusToComplete(Long enrollId, String status, LoginUser user);*/
	public int updateEnrollmentStatusToComplete(Long enrollId, String status, String userId);
	
	public List getLatestEnrollmentActivaties(int total);
	
	public boolean updateStatus(String status, Long customerId);
	
	public boolean updateStatus(String status, Long customerId, String loginUserId);
	
	public boolean updateServicePlanNo(Long planNo, Long contactLength, Long customerId, String loginUserId);
	
	public List<CustomerEnrollment> getEnrollmentMembersByStatus(String status);

	public ListPage<CustomerEnrollment> getDailyEnrollment(ListPage page, String enrollDate);

	public ListPage<CustomerEnrollment> getDailyLeads(ListPage page, String selectedDate);

	/**
	 * 获取CustomerTransfor列表语句
	 * @return
	 */
	public String getCustomerTransforList();

	/**
	 * 批量更新销售人员
	 * @param enrollIds
	 * @param salepersonId
	 * @param userId
	 * @return
	 */
	
	public int updateEnrollmentStatusToComplete(String enrollIds, String salepersonId, String userId, AdvanceQueryDto advanceQueryDto);

	/**
	 * 高级查询条件
	 */
	public List<AdvanceQueryConditionDto> assembleQueryConditions();


}