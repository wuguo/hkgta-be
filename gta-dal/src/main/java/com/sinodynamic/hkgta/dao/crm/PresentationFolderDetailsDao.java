package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;

public interface PresentationFolderDetailsDao extends IBaseDao<PresentMaterial>{
	
	public List<PresentMaterial> getFolderDetails(Long folderId) throws HibernateException;

}
