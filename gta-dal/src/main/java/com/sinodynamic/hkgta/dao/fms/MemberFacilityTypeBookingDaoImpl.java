package com.sinodynamic.hkgta.dao.fms;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.sinodynamic.hkgta.util.constant.FacilityStatus;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRSortField;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignSortField;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SortFieldTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.DailyMonthlyCoachingDto;
import com.sinodynamic.hkgta.dto.crm.DailyMonthlyFacilityUsageDto;
import com.sinodynamic.hkgta.dto.crm.SourceBookingDto;
import com.sinodynamic.hkgta.dto.fms.AdvancedQueryConditionsDto;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.dto.rpos.ToDayBookingMemberDto;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.BiOasisTypeItem.Facility;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RoomReservationStatus;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.FacilityName;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.report.JasperUtil;

@Repository
public class MemberFacilityTypeBookingDaoImpl extends GenericDao<MemberFacilityTypeBooking>  implements MemberFacilityTypeBookingDao{

	@Override
	public MemberFacilityTypeBooking getMemberFacilityTypeBooking(long resvId) {
		String hql = " SELECT f FROM MemberFacilityTypeBooking f left join f.memberReservedFacilitys as mrf where f.resvId =? and status != 'CAN'";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(resvId);
		List<MemberFacilityTypeBooking> bookings =super.getByHql(hql, paramList);
		if(null != bookings && bookings.size()>0)return bookings.get(0);
		return null;
	}
	
	@Override
	public boolean updateMemberFacilityTypeBooking(
			MemberFacilityTypeBooking memberFacilityTypeBooking)
			throws HibernateException {
		return super.update(memberFacilityTypeBooking);
	}

	@Override
	public List<MemberFacilityTypeBooking> getMemberFacilityTypeBookingByCustomerId(long customerId) throws HibernateException
	{
		String hql = " SELECT f FROM MemberFacilityTypeBooking f where f.customerId =?  and status != 'CAN' order by f.beginDatetimeBook";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(customerId);
		return super.getByHql(hql, paramList);
	}

	@Override
	public MemberFacilityTypeBooking getMemberFacilityTypeBookingByOrderNo(Long orderNo) throws HibernateException
	{
		String hql = " SELECT f FROM MemberFacilityTypeBooking f where f.orderNo =? ";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(orderNo);
		List<MemberFacilityTypeBooking> bookings =super.getByHql(hql, paramList);
		if(null != bookings && bookings.size()>0)return bookings.get(0);
		return null;
	}
	@Override
	public String getAllMemberFacilityTypeBooking(String facilityType, String dateRange, String customerId,boolean isPushMessage) throws Exception
	{
		StringBuffer sb = new StringBuffer( 
"select "
+ "        a.resv_id as resvId, "
+ "         a.resv_facility_type as resvFacilityType, "
+ "         a.facilityTypeQty as facilityTypeQty, "
+ "         date_format(date(a.begin_datetime_book),'%Y-%c-%d') as bookingDate, "
+ "         a.statusDisplay as status,"
+ "         a.reserveType as reserveType,"
+ "         a.exp_coach_user_id as coachId, "
+ "         concat(concat(f.given_name,' '), f.surname) as coachName, "
+ "         c.academy_no as academyNo, "
+ "         b.memberName as memberName, "
+ "         concat(CAST(hour(a.begin_datetime_book) AS char),':00') as startTime, "
+ "         concat(CAST((hour(a.end_datetime_book) + 1) AS char),':00') as endTime, "
+ "         case when a.resv_facility_type='TENNIS' then a.facility_subtype_id else d.attribute_id end as facilityAttribute, "
+ "         case  "
+ "             when a.status='ATN' then '1'  "
+ "             else '0'  "
+ "         end as checkedIn, "
+ "         b.contact_email as email, "
+ "         a.order_no as orderNo, "
+ "			b.customer_id as customerId,"
+ "			h.transaction_no as transactionNo,"
+ "         a.create_date as createDate, "
+ "         c.user_id as userId, "
+ "			CAST(r.room_resv_id  as UNSIGNED INTEGER) as roomResvId, "
+ "         a.isBundle, CASE WHEN a.resv_facility_type='TENNIS' THEN  fst.name  ELSE i.caption  END AS bayType "
+ "     from "
+ " (  select distinct n.*, r.is_bundle as isBundle,"
+ "         case when n.status='ATN' "
+ "              then 'Checked-in' "
+ "		    WHEN n.STATUS = 'CAN' THEN 'Canceled' "
+ "         WHEN n.status = 'PND' AND ADDTIME(n.create_date, '00:15:00') >= ? "
+ "         THEN 'Paying' "
+ "         WHEN n.status = 'PND' AND ADDTIME(n.create_date, '00:15:00') < ? "
+ "         THEN 'Pending for Payment' "
+ "              else case when ? <= n.end_datetime_book "
+ "                        then 'Ready to check-in' "
+ "                                  else case when ? > n.end_datetime_book "
+ "                                            then 'Overdue'"
+ "                                            else 'Unknown' end end end as statusDisplay, "
+ "		case when n.exp_coach_user_id IS NOT NULL"
+ " then 'private coach training'"
+ " when r.is_bundle='Y'"
+ " then 'Guest Room Bundle'"
+ " when t.reserve_type ='MR' or n.order_no IS NOT NULL"
+ " then 'Member Booking'"
+ " when t.reserve_type ='MT' or n.order_no is NULL"
+ " then 'Maintenance Offer'"
+ " end as reserveType,"
+ " n.facility_type_qty AS facilityTypeQty "
//+ " count(distinct t.facility_no) as facilityTypeQty"
+" 			from member_facility_type_booking n"
+ " left join member_reserved_facility m on n.resv_id = m.resv_id"
+ "  left join facility_timeslot t on m.facility_timeslot_id = t.facility_timeslot_id"
+ " left join room_facility_type_booking r on r.facility_type_resv_id = n.resv_id"
		+ " group by n.resv_id"
		+ ") a"
+ "         inner join (select cp.*, concat(cp.salutation,' ',cp.given_name,' ',cp.surname) as memberName from customer_profile cp) b "
+ "         inner join member c "
+ "         left join member_facility_book_addition_attr d on a.resv_id = d.resv_id "
+ "         left  join staff_profile f on a.exp_coach_user_id=f.user_id"
+ "         left join customer_order_hd g on a.order_no = g.order_no "
+ "         left join customer_order_trans h on g.order_no = h.order_no   "
+ " 		AND (h.status = 'SUC' or h.status = 'PND' or h.status = 'FAIL') "
+ "         left join facility_attribute_caption i on d.attribute_id = i.attribute_id"
+ "			left join room_facility_type_booking r on r.facility_type_resv_id = a.resv_id"
+"          left Join facility_sub_type AS fst ON a.facility_subtype_id=fst.subtype_id "
+ "     where "
+ "         a.customer_id=b.customer_id  "
+ "         and b.customer_id = c.customer_id  "
+ "			and upper(a.status) <> 'CAN' "
+ "			and upper(a.status) <> 'NAT' "
+ "			and a.exp_coach_user_id is null "
);

		if(isPushMessage){
			sb.append(" and upper(a.status) <> 'PND' ");
			sb.append(" and h.status = 'SUC' ");
		}
		if (facilityType.toUpperCase().equals("GOLF") || facilityType.toUpperCase().equals("TENNIS")) {
			sb.append(" and a.resv_facility_type = '" + facilityType + "' ");
		}
		if (dateRange.toUpperCase().equals("TODAY")) {
			sb.append(" and date(a.begin_datetime_book) = '" + DateCalcUtil.formatDate(new Date()) + "' ");
		} else if (dateRange.toUpperCase().equals("30")) {
			sb.append(" and date(a.begin_datetime_book) >= '" + DateCalcUtil.formatDate(new Date()) + "' ");
			sb.append(" and date(a.begin_datetime_book) <= '" + DateCalcUtil.formatDate(DateCalcUtil.getNearDay(new Date(), 30)) + "' ");
		} else if (dateRange.toUpperCase().equals("90")) {
			sb.append(" and date(a.begin_datetime_book) >= '" + DateCalcUtil.formatDate(new Date()) + "' ");
			sb.append(" and date(a.begin_datetime_book) <= '" + DateCalcUtil.formatDate(DateCalcUtil.getNearDay(new Date(), 90)) + "' ");
		}
		if (customerId != null && customerId.length() > 0) {
			Integer.parseInt(customerId);
			sb.append(" and a.customer_id = " + customerId + " ");
		} 
		
					
		return sb.toString();
	}

	@Override
	public List<MemberFacilityTypeBooking> getMemberFacilityTypeBooking(long customerId, Date createDate,String facilityType) throws Exception
	{
		String hql = " SELECT f FROM MemberFacilityTypeBooking f ,MemberReservedFacility m where f.resvId = m.resvId and m.resvId is not null and f.expCoachUserId is null and f.customerId =? and f.resvFacilityType=? and status != 'CAN' and status != 'PND' and date(f.beginDatetimeBook)=? order by f.beginDatetimeBook";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(customerId);
		paramList.add(facilityType);
		paramList.add(createDate);
		return super.getByHql(hql, paramList);
	}

	@Override
	public MemberFacilityTypeBooking getMemberFacilityTypeBookingIncludeAllStatus(long resvId) throws HibernateException
	{
		String hql = " SELECT f FROM MemberFacilityTypeBooking f left join f.memberReservedFacilitys as mrf where f.resvId =?";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(resvId);
		List<MemberFacilityTypeBooking> bookings =super.getByHql(hql, paramList);
		if(null != bookings && bookings.size()>0)return bookings.get(0);
		return null;
	}

	
	public ListPage<MemberFacilityTypeBooking> getMemberBookingRecordsByCustomerId(ListPage<MemberFacilityTypeBooking> page,MemberFacilityTypeBookingDto dto,
			Long customerId,String appType,String year,String month,String day,String startDate,String endDate){
		
		String sql =  
		"SELECT\n" +
		"	NULL AS facilityName,\n" +
		"	NULL AS courseName,\n" +
		"	NULL AS posterFilename," +
		"	NULL AS courseId," +
		"   NULL as attendId,\n" +
		"	CONCAT(\n" +
		"		date_format(\n" +
		"			m.begin_datetime_book,\n" +
		"			'%Y-%b-%d %H:%i'\n" +
		"		),\n" +
		"		' - ',\n" +
		"		date_format(\n" +
		"			DATE_ADD(m.end_datetime_book,INTERVAL 1 MINUTE),\n" +
		"			'%H:%i'\n" +
		"		)\n" +
		"	) AS bookingDateFull,\n" +
		"	date_format(\n" +
		"		m.begin_datetime_book,\n" +
		"		'%Y-%b-%d'\n" +
		"	) AS bookingDate,\n" +
		"	date_format(\n" +
		"		m.begin_datetime_book,\n" +
		"		'%H:%i'\n" +
		"	) AS startTime,\n" +
		"	date_format(\n" +
		"		DATE_ADD(m.end_datetime_book,INTERVAL 1 MINUTE),\n" +
		"		'%H:%i'\n" +
		"	) AS endTime,\n" +
		"	m.begin_datetime_book AS beginDate,\n" +
		"	m.end_datetime_book AS endDate,\n" +
		"	m. STATUS AS status,\n" +
		"	NULL AS enrollStatus,\n" +
		"	m.facility_type_qty AS facilityTypeQty,\n" +
		"	NULL AS sessionNo,\n" +
		"	NULL AS venue,\n" +
		"	NULL AS enrollId,\n" +
		"	m.order_no AS orderNo,\n" +
		"	m.resv_id AS resvId,\n" +
		"	null as facilityTypeResvId,"+
		"	m.exp_coach_user_id AS coachName,\n" +
		"(CASE m.resv_facility_type\n" +
		"	WHEN 'GOLF' THEN \n" +
		"	cap.caption\n" +
		"  WHEN 'TENNIS' THEN\n" +
		"  ifnull(fst.name,cap.caption)\n" +
		"  else 'Other'\n" +
		"  end ) as facilityAttribute,\n "+
		"  (CASE m.resv_facility_type\n" +
		"	WHEN 'GOLF' THEN \n" +
		"	cap.attribute_id \n" +
		"  WHEN 'TENNIS' THEN\n" +
		"  IFNULL(fst.subtype_id,cap.attribute_id )\n" +
		"  else null\n" +
		"  end ) as venueCode, "+
		"	IFNULL(m.customer_remark, '') AS remark,null as sessionId,\n" +
		"   m.create_date as createDate,\n" +
		"   null as restaurantId,\n"+
		"   null as phone,\n "+
		"   null as confirmId,\n" +
		"   room_booking.is_bundle as isBundle,\n"+
		"   NULL AS therapist,\n" +
		"   NULL AS roomNo,\n" +
		" 	m.resv_facility_type as facilityType,\n" +
		"   CASE WHEN room_booking.room_resv_id IS NOT NULL THEN 'Guest Room Bundled'\n" +
		"       WHEN m.order_no IS NULL THEN 'Maintenance Offer'\n" +
		"	     ELSE 'Member Booking'\n" +
		"   END AS resvType,\n"+
		"   NULL as checkinTimestamp,\n" +
		"   NULL as checkoutTimestamp,\n" +
		"   NULL as picId,\n" +
		" 	NULL as roomId\n" +
		"FROM\n" +
		"	member_facility_type_booking m \n " +
		"  left join \n" +
		"	member_facility_book_addition_attr attr on m.resv_id = attr.resv_id\n" +
		"  left join\n" +
		"	facility_attribute_caption cap on cap.attribute_id = attr.attribute_id\n" +
		"  left join \n" +
		"  facility_sub_type fst on fst.subtype_id = m.facility_subtype_id \n"+
		"  left join room_facility_type_booking room_booking on room_booking.facility_type_resv_id = m.resv_id and room_booking.is_bundle = 'Y'\n"+
		"WHERE\n" +
		" m. STATUS != 'CAN'\n" +
		"AND m. STATUS != 'PND'\n"+
		"AND m.customer_id = ?\n" +
		"AND <condition_date_filter_facilities>\n" +
		
		"UNION ALL\n" +
		"	SELECT\n" +
		"       DISTINCT"+
		"		NULL AS facilityName,\n" +
		"		cm.course_name AS courseName,\n" +
		"		cm.poster_filename AS posterFilename,\n" +
		"		cm.course_id AS courseId,attendance.attend_id as attendId, \n" +
		"		CONCAT(\n" +
		"			date_format(\n" +
		"				cs.begin_datetime,\n" +
		"				'%Y-%b-%d %H:%i'\n" +
		"			),\n" +
		"			' - ',\n" +
		"			date_format(cs.end_datetime, '%H:%i')\n" +
		"		) AS bookingDateFull,\n" +
		"		date_format(\n" +
		"			cs.begin_datetime,\n" +
		"			'%Y-%b-%d'\n" +
		"		) AS bookingDate,\n" +
		"		date_format(cs.begin_datetime, '%H:%i') AS startTime,\n" +
		"		date_format(cs.end_datetime, '%H:%i') AS endTime,\n" +
		"		cs.begin_datetime AS beginDate,\n" +
		"		cs.end_datetime AS endDate,\n" +
		"		ifnull(attendance.status, NULL) AS status,\n" +
		"		ifnull(ce.status, NULL) AS enrollStatus,\n" +
		"		NULL AS facilityTypeQty,\n" +

		"		cs.session_no AS sessionNo,\n" +
		"       NULL AS venue,\n"+
		"		ce.enroll_id AS enrollId,\n" +
		"		NULL AS orderNo,\n" +
		"		ce.enroll_id AS resvId,\n" +
		"		null as facilityTypeResvId,"+
		"		NULL AS coachName,\n" +
		"		NULL AS facilityAttribute,\n" +
		"       null as venueCode,\n "+
		"		IFNULL(cm.internal_remark, ' ') AS remark,cs.sys_id as sessionId,\n" +
		"       ce.create_date as createDate,\n" +
		"       null as restaurantId,\n"+
		"       null as phone,\n "+
		"       null as confirmId,\n" +
		"       null as isBundle,\n"+
		"       NULL AS therapist,\n" +
		"       NULL AS roomNo,\n" +
		" 		cm.course_type as facilityType,\n" +
		"       NULL AS resvType,\n"+
		"   	NULL as checkinTimestamp,\n" +
		"   	NULL as checkoutTimestamp,\n" +
		"       NULL as picId,\n" +
		" 		NULL as roomId\n" +
		"	FROM\n" +
		"		course_enrollment ce,\n" +
		"		course_master cm\n"+
		"   LEFT JOIN course_session cs\n"+
		"        ON cs.course_id = cm.course_id and cs.status = 'ACT' " +
		"	LEFT JOIN student_course_attendance attendance ON attendance.course_session_id = cs.sys_id "+
		"             AND attendance.enroll_id in (select DISTINCT enroll_id from course_enrollment where customer_id = ? ) \n" + //fixed duplicate session by excluding those attendance record of other members.
		"	WHERE\n" +
		"		ce.customer_id = ?\n" +
		"	AND cm.course_id = ce.course_id\n" +
		"	AND ce.status != 'PND' and ce.status !='CAN' and cm.status != 'CAN'\n" +
		"   AND <condition_date_filter_course>\n" +
		"   AND ce.status != 'ACT'"+
		
		" UNION ALL\n "+
		"   SELECT\n " +
		"   rm.restaurant_name AS facilityName,\n" +
		"   NULL AS courseName,\n" +
		"   restImage.image_filename AS posterFilename,\n" +
		"   NULL AS courseId,\n" +
		"   NULL AS attendId,\n" +
		"   DATE_FORMAT(m.book_time, '%Y-%b-%d %H:%i') AS bookingDateFull,\n" +
		"   DATE_FORMAT(m.book_time, '%Y-%b-%d') AS bookingDate,\n" +
		"   DATE_FORMAT(m.book_time, '%H:%i') AS startTime,\n" +
		"   NULL AS endTime,\n" +
		"   m.book_time AS beginDate,\n" +
		"   NULL AS endDate,\n" +
		"   m.STATUS AS status,\n" +
		"   NULL AS enrollStatus,\n" +
		"   m.party_size AS facilityTypeQty,\n" +
		"   NULL AS sessionNo,\n" +
		"   NULL AS venue,\n" +
		"   NULL AS enrollId,\n" +
		"   NULL AS orderNo,\n" +
		"   m.resv_id AS resvId,\n" +
		"	null as facilityTypeResvId,\n"+
		"   NULL as coachName,\n" +
		"   NULL AS facilityAttribute,\n" +
		"   NULL AS venueCode,\n" +
		"   IFNULL(m.remark, '') AS remark,\n" +
		"   NULL AS sessionId,\n" +
		"   m.create_date AS createDate,\n" +
		"   m.restaurant_id as restaurantId,\n"+
		"   rm.phone as phone,\n"+
		"   null as confirmId,\n" +
		"   null as isBundle,\n"+
		"   NULL AS therapist,\n" +
		"   NULL AS roomNo,\n" +
		" 	'RESTAURANT' as facilityType,\n" +
		"   NULL AS resvType,\n"+
		"   NULL as checkinTimestamp,\n" +
		"   NULL as checkoutTimestamp,\n" +
		"   NULL as picId,\n" +
		" 	NULL as roomId\n" +
		"   FROM\n" +
		"   restaurant_customer_booking m\n" +
		"   left join restaurant_master rm on m.restaurant_id = rm.restaurant_id\n" +
		"   LEFT JOIN restaurant_image restImage ON restImage.restaurant_id = m.restaurant_id and restImage.image_feature_code = 'RR'\n"+
		"   WHERE\n" +
		"   <condition_date_filter_restaurant>\n" +
		"   AND m.STATUS != 'DEL'\n" +
		"   AND m.customer_id = ? \n"+
		
		"   UNION ALL\n" +
		"         SELECT\n" +
		"            'Guest Room' AS facilityName,\n" +
		"            NULL AS courseName,\n" +
		"            rp.server_filename AS posterFilename,\n" +
		"            NULL AS courseId,\n" +
		"            NULL AS attendId,\n" +
		"            CONCAT(DATE_FORMAT(rc.arrival_date, '%Y-%b-%d'),' - ',date_format(rc.departure_date, '%Y-%b-%d')) AS bookingDateFull,\n" +
		"            DATE_FORMAT(rc.arrival_date,\n" +
		"            '%Y-%b-%d') AS bookingDate,\n" +
		"            DATE_FORMAT(rc.arrival_date,\n" +
		"            '%b-%d') AS startTime,\n" +
		"            DATE_FORMAT(rc.departure_date, '%b-%d') AS endTime,\n" +
		"            rc.arrival_date AS beginDate,\n" +
		"            rc.departure_date AS endDate,\n" +
		"            rc.STATUS AS status,\n" +
		"            NULL AS enrollStatus,\n" +
		"            rc.night AS facilityTypeQty,\n" +
		"            NULL AS sessionNo,\n" +
		"            NULL AS venue,\n" +
		"            NULL AS enrollId,\n" +
		"            NULL AS orderNo,\n" +
		"            rc.resv_id AS resvId,\n" +
		"            null AS facilityTypeResvId,\n"+
		"            NULL as coachName,\n" +
		"            ifnull(r.type_name,rc.room_type_code) AS facilityAttribute,\n" +
		"            NULL AS venueCode,\n" +
		"            '' AS remark,\n" +
		"            NULL AS sessionId,\n" +
		"            rc.create_date AS createDate,\n" +
		"            NULL as restaurantId,\n" +
		"            NULL as phone,\n" +
		"            rc.confirm_id as confirmId,\n" +
		"   		 null as isBundle,\n"+
		"			 NULL AS therapist,\n" +
		"			 room.room_no AS roomNo,\n" +
		" 			 'GUESTROOM' as facilityType,\n" +
		"            NULL AS resvType,\n"+
		"            rc.checkin_timestamp as checkinTimestamp,\n" +
		"            rc.checkout_timestamp as checkoutTimestamp,\n"+
		"            rp.pic_id as picId,\n" +
		"            rc.room_id as roomId\n" +
		"  FROM room_reservation_rec rc\n " +
		"  LEFT JOIN room_type r ON r.type_code = rc.room_type_code\n"+
		"  LEFT JOIN room_pic_path rp ON rp.room_type_code = rc.room_type_code and rp.display_order = '1'\n"+
		"  LEFT JOIN room room ON room.room_id = rc.room_id\n"+
		"  where <condition_date_filter_guestroom>\n "+
		"  and rc.status IN('SUC', 'CKI', 'CKO', 'CKP')  and rc.customer_id =?\n "+
		
		"  UNION ALL\n" +
		"SELECT\n" +
		"	sar.service_name AS facilityName,\n" +
		"	NULL AS courseName,\n" +
		"	NULL AS posterFilename,\n" +
		"	NULL AS courseId,\n" +
		"	NULL AS attendId,\n" +
		"	CONCAT_ws(\n" +
		"		' - ',\n" +
		"		date_format(\n" +
		"			sar.start_datetime,\n" +
		"			'%Y-%b-%d %H:%i'\n" +
		"		),\n" +
		"		date_format(sar.end_datetime, '%H:%i')\n" +
		"	) AS bookingDateFull,\n" +
		"	date_format(\n" +
		"		sar.start_datetime,\n" +
		"		'%Y-%b-%d'\n" +
		"	) AS bookingDate,\n" +
		"	date_format(sar.start_datetime, '%H:%i') AS startTime,\n" +
		"	date_format(sar.end_datetime, '%H:%i') AS endTime,\n" +
		"	sar.start_datetime AS beginDate,\n" +
		"	sar.end_datetime AS endDate,\n" +
		"	sar.status AS status,\n" +
		"	NULL AS enrollStatus,\n" +
		"	NULL AS facilityTypeQty,\n" +
		"	NULL AS sessionNo,\n" +
		"	NULL AS venue,\n" +
		"	NULL AS enrollId,\n" +
		"	NULL AS orderNo,\n" +
		"	right(sar.ext_appointment_id,6) AS resvId,\n" +
		"	NULL AS facilityTypeResvId,\n" +
		"	NULL AS coachName,\n" +
		"	NULL AS facilityAttribute,\n" +
		"	NULL AS venueCode,\n" +
		"	NULL AS remark,\n" +
		"	NULL AS sessionId,\n" +
		"	sar.create_date AS createDate,\n" +
		"	NULL AS restaurantId,\n" +
		"	NULL AS phone,\n" +
		"	NULL AS confirmId,\n" +
		"	NULL AS isBundle,\n" +
		"	concat_ws(\n" +
		"		' ',\n" +
		"		sar.ext_employee_first_name,\n" +
		"		sar.ext_employee_last_name\n" +
		"	) AS therapist,\n" +
		"	ifnull(room.room_no, '') AS roomNo,\n" +
		"  'WELLNESS' as facilityType,\n" +
		"   NULL AS resvType,\n" +
		"   NULL as checkinTimestamp,\n" +
		"   NULL as checkoutTimestamp,\n" +
		"   NULL as picId,\n" +
		" 	NULL as roomId\n" +
		
		"FROM\n" +
		"	spa_appointment_rec sar\n" +
		"LEFT JOIN room room ON room.room_id = sar.room_id\n" +
		"WHERE\n" +
		"   sar.status is not null\n "+
		" AND sar.customer_id = ?\n"+
		" AND <condition_date_filter_wellness> ";

		List<Serializable> param = new ArrayList<Serializable>();
	    Date selectedFilteredDate = new Date();
	    
	    String dateFilteringFacilities = null;
	    String dateFilteringCourse = null;
	    String dateFilterRestaurant = null;
	    String dateFilterGuestRoom = null;
	    String dateFilterWellness = null;
	    if(!StringUtils.isEmpty(year)&&!StringUtils.isEmpty(month)&&!StringUtils.isEmpty(day)){
	    	selectedFilteredDate =CommUtil.formatTheGivenYearAndMonthAndDay(year,month,day);
		    dateFilteringFacilities = " date_format( ? ,'%Y-%m-%d') = date_format(m.begin_datetime_book,'%Y-%m-%d') ";
		    dateFilteringCourse     = " date_format( ? ,'%Y-%m-%d') = date_format(ifnull(cs.begin_datetime,?),'%Y-%m-%d') ";
		    dateFilterRestaurant    = " date_format( ? ,'%Y-%m-%d') = date_format(m.book_time,'%Y-%m-%d') ";
		    dateFilterGuestRoom     = " date_format( ? ,'%Y-%m-%d') = date_format(rc.arrival_date,'%Y-%m-%d') ";
		    dateFilterWellness      = " date_format( ? ,'%Y-%m-%d') = date_format(sar.start_datetime,'%Y-%m-%d') ";
	    }else if(!StringUtils.isEmpty(year)&&!StringUtils.isEmpty(month)){
	    	selectedFilteredDate =CommUtil.formatTheGivenYearAndMonth(year,month);
		    dateFilteringFacilities = " date_format( ? ,'%Y-%m') = date_format(m.begin_datetime_book,'%Y-%m') ";
		    dateFilteringCourse     = " date_format( ? ,'%Y-%m') = date_format(ifnull(cs.begin_datetime,?),'%Y-%m') ";
		    dateFilterRestaurant    = " date_format( ? ,'%Y-%m') = date_format(m.book_time,'%Y-%m') ";
		    dateFilterGuestRoom     = " date_format( ? ,'%Y-%m') = date_format(rc.arrival_date,'%Y-%m') ";
		    dateFilterWellness      = " date_format( ? ,'%Y-%m') = date_format(sar.start_datetime,'%Y-%m') ";
		    
	    }else if("WP".equalsIgnoreCase(appType)&& !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)){
		    dateFilteringFacilities = " date_format( m.begin_datetime_book,'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
		    dateFilteringCourse     = " date_format( ifnull(cs.begin_datetime,?),'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
		    dateFilterRestaurant    = " date_format( m.book_time,'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
		    dateFilterGuestRoom     = " date_format( rc.arrival_date,'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
		    dateFilterWellness      = " date_format( sar.start_datetime,'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
	    }else{
	    	dateFilteringFacilities = " date_format( ? ,'%Y-%m-%d') <= date_format(m.end_datetime_book,'%Y-%m-%d') ";
	    	//modified by Kaster 20160304 新需求：课程结束后半个小时内的记录也查出来。
	    	dateFilteringCourse     = " date_format( ? ,'%Y-%m-%d') <= date_format(DATE_ADD(ifnull(cs.end_datetime,?),INTERVAL 30 MINUTE),'%Y-%m-%d') ";
		    dateFilterRestaurant    = " m.STATUS != 'EXP' and date_format( ? ,'%Y-%m-%d') <= date_format(m.book_time,'%Y-%m-%d')";
		    dateFilterGuestRoom     = " date_format(?,'%Y-%m-%d')<=date_format(rc.arrival_date,'%Y-%m-%d') ";
		    dateFilterWellness      = " date_format(?,'%Y-%m-%d')<=date_format(sar.start_datetime,'%Y-%m-%d') ";
	    }
	    sql = sql.replace("<condition_date_filter_facilities>",dateFilteringFacilities)
	    		 .replace("<condition_date_filter_course>",dateFilteringCourse)
	    		 .replace("<condition_date_filter_restaurant>",dateFilterRestaurant)
	    		 .replace("<condition_date_filter_guestroom>",dateFilterGuestRoom)
	             .replace("<condition_date_filter_wellness>", dateFilterWellness);
	    
		/* Parameter for Facilities */
		param.add(customerId);
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(selectedFilteredDate);
		}

		/* Parameter for Course Enrollment */
		param.add(customerId);
		param.add(customerId);
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(selectedFilteredDate);
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(selectedFilteredDate);
			param.add(selectedFilteredDate);
		}

		/* Parameter for Restaurant Booking */
//		param.add(appType);
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(selectedFilteredDate);
		}
		param.add(customerId);

		/* Parameter for Guest Room */
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(selectedFilteredDate);
		}
		param.add(customerId);
		
		/* Parameter for Wellness */
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(customerId);
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(customerId);
			param.add(selectedFilteredDate);
		}
		
		sql = "select s.* from ("+sql+") s ";
		String countSql = " SELECT count(1) from ( " +sql+") x";
		
		/*Return Regular List without Pages for Member APP's My Booking for Particular Month*/
		if(!StringUtils.isEmpty(year)&&!StringUtils.isEmpty(month)&&StringUtils.isEmpty(day)){
			List<ListPage<MemberFacilityTypeBooking>.OrderBy> orderByList = page.getOrderByList();

			StringBuilder orderby = new StringBuilder("");
			if ((orderByList != null) && (orderByList.size()>0)){
				if(sql != null	&& sql.toUpperCase().indexOf("ORDER BY") < 0){
					for(ListPage<MemberFacilityTypeBooking>.OrderBy each: orderByList){
						if(each !=null){
							orderby.append(("".equals(orderby.toString())) ? " ORDER BY " : " ,");
							orderby.append(" "+each.getProperty() +  ((ListPage.OrderType.ASCENDING.equals(each.getType())) ? " ASC " : " DESC "));
						}
					}
				}
			}

			sql =  sql + orderby.toString();

			List<MemberFacilityTypeBookingDto> listOnly = getDtoBySql(sql, param, MemberFacilityTypeBookingDto.class);
			page.setDtoList(listOnly);

			return page;
		}
		/*Return Regular List with Pages for Member APP's Home Page, Web Portal's My Bookings and Portal's My Bookings*/
		else{
			return listBySqlDto(page, countSql, sql.toString(), param, dto);
		}
		
	}

	@Override
	public boolean deleteBySql(Long resvId) {
		String hql = "delete from member_facility_type_booking where resv_id = ?";
		return deleteByHql(hql, resvId) >= 0 ? true : false;
		
	}
	
	public List<MemberFacilityTypeBookingDto> getRoomReservationFacilities(Long roomResvId){
		String sql = 
				"SELECT\n" +
				"	date_format(\n" +
				"		member_booking.begin_datetime_book,\n" +
				"		'%Y-%b-%d'\n" +
				"	) AS bookingDate,\n" +
				"	date_format(\n" +
				"		member_booking.begin_datetime_book,\n" +
				"		'%H:%i'\n" +
				"	) AS startTime,\n" +
				"	date_format(\n" +
				"		DATE_ADD(member_booking.end_datetime_book,INTERVAL 1 MINUTE),\n" +
				"		'%H:%i'\n" +
				"	) AS endTime\n" +
				"FROM\n" +
				"	room_facility_type_booking room_booking\n" +
				"LEFT JOIN member_facility_type_booking member_booking ON room_booking.facility_type_resv_id = member_booking.resv_id\n" +
				"WHERE\n" +
				"	room_booking.room_resv_id = ?\n" +
				"   AND room_booking.is_bundle = 'Y'\n"+
				"   AND member_booking.status != 'CAN'\n"+
				"   AND member_booking.status != 'PND'\n"+
				"Order by member_booking.begin_datetime_book ASC ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(roomResvId);
		return getDtoBySql(sql, param, MemberFacilityTypeBookingDto.class);
	}
	
	//PC-private coach training; CS-course session; MR-member reserve; MT-maintenance.
	@Override
	public int getMemberFacilityBookedCountForQuota(String facilityType,Date bookingDateTime) throws Exception
	{
		String sql = " SELECT COUNT(*) FROM facility_timeslot t "
			        +" JOIN  member_reserved_facility f ON t.facility_timeslot_id = f.facility_timeslot_id "
			        +" JOIN member_facility_type_booking b ON b.resv_id = f.resv_id "
			        +" left join room_facility_type_booking r on r.facility_type_resv_id = b.resv_id "
			        +" WHERE b.resv_facility_type = ? "
			        +" AND b.status <> 'CAN' "
			        +" AND b.exp_coach_user_id IS NULL "
			        +" AND t.status <> 'CAN' "
			        +" AND t.begin_datetime = ? "
				    +" AND (t.reserve_type = ?)"
				    +" AND t.transfer_from_timeslot_id is null "
				    +" AND r.facility_type_resv_id is null "
		 			+" AND b.reserve_via in ('APS','WP') ";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(facilityType);
		params.add(DateCalcUtil.formatDatetime(bookingDateTime));
		params.add(Constant.RESERVE_TYPE_MR);
		Object count = super.getUniqueBySQL(sql, params);
		return ((Number)count).intValue();
	}
	
	public ListPage<MemberFacilityTypeBooking> getDailyMonthlyFacilityUsage(String timePeriodType, ListPage page, String selectedDate, String facilityType){
		String sql="SELECT\n" +
				"  DATE_FORMAT(booking.create_date, '%Y/%m/%d') as resvDate,\n" +
				"  booking.resv_id as resvId,\n" +
				"  CASE booking.resv_facility_type \n" +
				"			WHEN 'GOLF' THEN CONCAT_WS('-','GBF',booking.resv_id) \n" +
				"      WHEN 'TENNIS' THEN CONCAT_WS('-','TCF',booking.resv_id) \n" +
				"  END AS resvIdString,\n" +
				//modify by stephen 2016-04-15
//				"	CASE booking.resv_facility_type \n" +
//				"			WHEN 'GOLF' THEN 'Golf' \n" +
//				"      WHEN 'TENNIS' THEN 'Tennis'\n" +
//				"  END AS facilityType,\n" +
				"	CASE booking.resv_facility_type \n" +
				"			WHEN 'GOLF' THEN d.attribute_id \n" +
				"      WHEN 'TENNIS' THEN booking.facility_subtype_id\n" +
				"  END AS facilityType,\n" +
				//modify by stephen 2016-04-15
				"  IF(GROUP_CONCAT(distinct ft.facility_no),IF(GROUP_CONCAT(distinct ft.status) like '%TA%','Not Yet allotted',GROUP_CONCAT(distinct ft.facility_no)),'') as facilityNo,\n" +
				"  CASE WHEN roomFacility.room_resv_id IS NOT NULL THEN 'Guest Room Bundled'\n" +
				"       WHEN booking.order_no IS NULL THEN \n" +
				"						CONCAT('Maintenance Offer',(\n" +
				"										SELECT DISTINCT\n" +
				"											IF(fromResvFac.resv_id is not null,IF(booking.resv_facility_type='GOLF',CONCAT('(','GBF-',fromResvFac.resv_id,')'),CONCAT('(','TCF-',fromResvFac.resv_id,')') ),'')\n" +
				"										FROM\n" +
				"											member_facility_type_booking bookingInter\n" +
				"										LEFT JOIN member_reserved_facility resvFac ON bookingInter.resv_id = resvFac.resv_id\n" +
				"										LEFT JOIN facility_timeslot facSlot ON resvFac.facility_timeslot_id = facSlot.facility_timeslot_id\n" +
				"										LEFT JOIN member_reserved_facility fromResvFac ON fromResvFac.facility_timeslot_id = facSlot.transfer_from_timeslot_id\n" +
				"										WHERE\n" +
				"											bookingInter.resv_id = booking.resv_id\n" +
				"								)\n" +
				"            )\n" +
				"				ELSE 'Member Booking'\n" +
				"  END AS resvType,\n" +
				"  m.academy_no AS academyId,\n" +
				"  CONCAT_WS(\n" +
				"		' ',\n" +
				"		cp.given_name,\n" +
				"		cp.surname\n" +
				"	) AS patronName,\n" +
				"  m.member_type AS memberType,\n" +
				"  DATE_FORMAT(booking.begin_datetime_book, '%Y-%m-%d %H:%i:%S') as serviceDate,\n" +
				"  CASE trans.payment_method_code\n" +
				
				"			when '"+PaymentMethod.VISA.name()+"' then '"+PaymentMethod.VISA.getDesc()+"'\n" +
				"      when '"+PaymentMethod.MASTER.name()+"' then '"+PaymentMethod.MASTER.getDesc()+"'\n" +
				"      when '"+PaymentMethod.CASH.name()+"' then '"+PaymentMethod.CASH.getDesc()+"'\n" +
				"      when '"+PaymentMethod.CASHVALUE.name()+"' then '"+PaymentMethod.CASHVALUE.getDesc()+"'\n" +
				"      when '"+PaymentMethod.CHRGPOST.name()+"' then '"+PaymentMethod.CHRGPOST.getDesc()+"'\n" +
				"      when '"+PaymentMethod.BT.name()+"' then '"+PaymentMethod.BT.getDesc()+"'\n" +
				"      when '"+PaymentMethod.CHEQUE.name()+"' then '"+PaymentMethod.CHEQUE.getDesc()+"'\n" +
				"      when 'cheque' then '"+PaymentMethod.CHEQUE.getDesc()+"'\n" +
				"      when '"+PaymentMethod.VAC.name()+"' then '"+PaymentMethod.VAC.getDesc()+"'\n" +
				"      when '"+PaymentMethod.CUP.name()+"' then '"+PaymentMethod.CUP.getDesc()+"'\n" +
				"      when '"+PaymentMethod.AMEX.name()+"' then '"+PaymentMethod.AMEX.getDesc()+"'\n" +
				"      when 'OTH' then 'N/A'\n" +
				
				"      ELSE 'N/A'\n" +
				"  END as paymentMethodCode,\n" +
				"  CASE trans.payment_media\n" +
				"			WHEN 'OTH' THEN 'N/A'\n" +
				"      WHEN 'OP' THEN 'Online Payment'\n" +
				"      WHEN 'ECR' THEN 'ECR Terminal'\n" +
				"      WHEN 'FTF' THEN 'N/A'\n" +
				"      WHEN '' THEN 'N/A'\n" +
				"      ELSE 'N/A'\n" +
				"	END AS paymentMedia,\n" +
				"	booking.reserve_via AS location,\n" +
				"  booking.facility_type_qty AS noOfFacility,\n" +
				"  ifnull(trans.paid_amount,0) as paidAmount,\n" +
				"  CASE booking.status\n" +
				"			WHEN 'RSV' THEN 'Reserved'\n" +
				"      WHEN 'ATN' THEN 'Attended'\n" +
				"			WHEN 'NAT' THEN 'Not Attended' \n" +
				"	END AS status,\n" +
				"  (SELECT CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) FROM staff_profile staffProfileCreateBy WHERE staffProfileCreateBy.user_id = booking.create_by\n" +
				"   UNION ALL \n" +
				"   SELECT CONCAT_WS(' ',memberProfile.given_name,memberProfile.surname) FROM customer_profile memberProfile, user_master userMasterMember, member memberCreateBy \n" +
				"   WHERE userMasterMember.user_type = 'CUSTOMER' AND memberCreateBy.user_id = userMasterMember.user_id AND memberProfile.customer_id = memberCreateBy.customer_id\n" +
				"   AND booking.create_by = userMasterMember.user_id\n" +
				"  ) AS createBy\n" +
				"	\n" +
				"FROM\n" +
				"  customer_profile cp,\n" +
				"  member m,\n" +
				"  member_facility_type_booking booking\n" +
				//add by stephen 2016-04-15
				"  LEFT JOIN member_facility_book_addition_attr d ON booking.resv_id = d.resv_id\n" +
				"LEFT JOIN customer_order_hd hd ON hd.order_no = booking.order_no\n" +
				"LEFT JOIN customer_order_trans trans ON trans.order_no = hd.order_no AND trans.status = 'SUC'\n" +
				"LEFT JOIN room_facility_type_booking roomFacility ON roomFacility.facility_type_resv_id = booking.resv_id AND roomFacility.is_bundle = 'Y'\n" +
				//add by stephen 2016-04-15
				"LEFT JOIN member_reserved_facility mrf ON booking.resv_id = mrf.resv_id\n" +
				"LEFT JOIN facility_timeslot ft ON mrf.facility_timeslot_id = ft.facility_timeslot_id AND ft.status in('OP','TA') \n" +	
				"WHERE\n" +
				"	booking.customer_id = cp.customer_id\n" +
				"AND cp.customer_id = m.customer_id\n" +
				"AND booking.exp_coach_user_id is NULL\n" +
				"AND booking.status in ('RSV','ATN','NAT')\n" +
				"AND date_format(booking.create_date,<dateFormatCondition>) = date_format(?,<dateFormatCondition>)\n"+
				"AND <facilityCondition>" +
				"group by resvId ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(selectedDate);
		if("GOLF".equalsIgnoreCase(facilityType)||"TENNIS".equalsIgnoreCase(facilityType)){
			sql = sql.replace("<facilityCondition>", "booking.resv_facility_type = ?");
			param.add(facilityType);
		}else{
			sql = sql.replace("<facilityCondition>", " 1=1 ");
		}
		
		/*Daily Facility Usage Report*/
		if("DAILY".equalsIgnoreCase(timePeriodType)){
			sql = sql.replaceAll("<dateFormatCondition>", "'%Y-%m-%d'");
		}
		/*Monthly Facility Usage Report*/
		else{
			sql = sql.replaceAll("<dateFormatCondition>", "'%Y-%m'");
		}
		
		String countSql = " SELECT count(x.resvDate) from ( " +sql+") x";
		return listBySqlDto(page, countSql, sql, param, new DailyMonthlyFacilityUsageDto());
	}
	
	@SuppressWarnings("rawtypes")
	public byte[] getDailyMonthlyFacilityUsageAttachment(String timePeriodType, String selectedDate, String fileType, String sortBy, String isAscending, String facilityType) throws Exception{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String parentjasperPath = reportPath + "DailyMonthlyFacilityUsage.jasper";
		File reFile = new File(parentjasperPath);
		Map<String, Object> parameters = new HashMap<String, Object>();
		DataSource ds = SessionFactoryUtils.getDataSource(this.getsessionFactory());
		Connection dbconn = DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("resvDate", selectedDate);
		parameters.put("facilityType", facilityType);
		parameters.put("TimePeriodType", timePeriodType);
		parameters.put("fileType", fileType);
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if("true".equalsIgnoreCase(isAscending)){
			sortField.setOrder(SortOrderEnum.ASCENDING);
		}else{
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		//Ignore pagination for csv
		if("csv".equals(fileType)){
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType, jasperPrint, outPut);
		if (exporter == null) {
			return null;
		} else {
			exporter.exportReport();
			return outPut.toByteArray();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JRAbstractExporter exportedByFileType(String fileType, JasperPrint jasperPrint, ByteArrayOutputStream outPut) {
		JRAbstractExporter exporter = null;
		switch (fileType) {
		case "pdf":
			exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));
			break;
		case "csv":
			exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(outPut));
			break;
		default:
		}

		return exporter;
	}

	@Override
	public ListPage<MemberFacilityTypeBooking> getDailyMonthlyPrivateCoaching(String timePeriodType, ListPage page, String selectedDate, String facilityType)
	{
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT DATE_FORMAT(b.begin_datetime_book, '%Y/%m/%d') AS resvDate, ")
			.append("CASE  WHEN b.resv_facility_type = 'TENNIS' THEN CONCAT('TPC-',b.resv_id) ELSE CONCAT('GPC-',b.resv_id) END  AS resvId ,")
			.append(" b.resv_facility_type AS facilityType, ")
            .append(" CASE WHEN b.resv_facility_type = 'TENNIS' THEN fst.name ")
            .append(" ELSE fa.caption ")
            .append(" END AS bayType, ")
            .append(" m.academy_no AS academyId, ")
            .append(" CONCAT(cp.salutation, ' ', cp.given_name, ' ', cp.surname) AS patronName, ")
			.append(" m.member_type as memberType, ")
            .append(" b.exp_coach_user_id AS coachId, ")
            .append(" CONCAT(sp.given_name, ' ', sp.surname) AS coachName, ")
            .append(" b.facility_type_qty as qty, ")
            .append(" (DATE_FORMAT(b.end_datetime_book, '%H')+1-DATE_FORMAT(b.begin_datetime_book, '%H')) AS reservationHours, ")
            .append(" hd.order_total_amount as price, ")
            .append(" IF(IFNULL(sub.attendance_status, 0) = 0, 'Pending', 'Attended') AS attendanceStatus, ")
            .append(" b.status AS status, ")
            .append(" CONCAT(spu.given_name, ' ', spu.surname) AS updateBy ")
            .append(" FROM ")
            .append(" member_facility_type_booking b ")
            .append(" LEFT JOIN member_facility_book_addition_attr a ON b.resv_id = a.resv_id ")
		    .append(" LEFT JOIN member m ON b.customer_id = m.customer_id ")
		    .append(" LEFT JOIN customer_profile cp ON b.customer_id = cp.customer_id ")
		    .append(" LEFT JOIN staff_profile sp ON b.exp_coach_user_id = sp.user_id ")
		    .append(" LEFT JOIN staff_profile spu ON b.update_by = spu.user_id ")
		    .append(" LEFT JOIN facility_attribute_caption fa ON a.attribute_id = fa.attribute_id ")
		    .append(" LEFT JOIN facility_sub_type fst ON b.facility_subtype_id = fst.subtype_id ")
		    .append(" LEFT JOIN customer_order_hd hd on hd.order_no = b.order_no ")
		    .append(" LEFT JOIN (SELECT ")
		    .append(" booking.resv_id, ")
            .append(" SUM(IF(attend.attend_time IS NULL, 0, 1)) attendance_status ")
            .append(" FROM ")
            .append(" member_facility_type_booking booking ")
		    .append(" JOIN member_reserved_facility mem_facility ON mem_facility.resv_id = booking.resv_id ")
		    .append(" JOIN facility_timeslot slot ON slot.facility_timeslot_id = mem_facility.facility_timeslot_id ")
		    .append(" LEFT JOIN member_facility_attendance attend ON attend.facility_timeslot_id = slot.facility_timeslot_id ")
		    .append(" GROUP BY booking.resv_id) sub ON sub.resv_id = b.resv_id ")
		    .append(" WHERE b.exp_coach_user_id IS NOT NULL ")
		    .append(" AND b.status in ('RSV','ATN','NAT','CAN') ");

		List<Serializable> param = new ArrayList<Serializable>();
		if("GOLF".equalsIgnoreCase(facilityType)||"TENNIS".equalsIgnoreCase(facilityType)){
			sql.append(" AND b.resv_facility_type = ? ");
			param.add(facilityType);
		}
		/*Daily Facility Usage Report*/
		if("DAILY".equalsIgnoreCase(timePeriodType)){
			sql.append(" AND DATE_FORMAT(b.begin_datetime_book, '%Y-%m-%d') = date_format(?,'%Y-%m-%d') ");
			param.add(selectedDate);
		}
		else{
			/*Monthly Facility Usage Report*/
			sql.append(" AND DATE_FORMAT(b.begin_datetime_book, '%Y-%m') = date_format(?,'%Y-%m') ");
			param.add(selectedDate);
		}

		String countSql = " SELECT count(x.resvDate) from ( " +sql+") x";
		return listBySqlDto(page, countSql, sql.toString(), param, new DailyMonthlyCoachingDto());
	}

	@Override
	public byte[] getDailyMonthlyPrivateCoachingAttachment(String timePeriodType, String selectedDate, String fileType, String sortBy, String isAscending, String facilityType) throws Exception
	{
		String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		String parentjasperPath = reportPath + "DailyPrivateCoaching.jasper";
		File reFile = new File(parentjasperPath);
		Map<String, Object> parameters = new HashMap<String, Object>();
		DataSource ds = SessionFactoryUtils.getDataSource(this.getsessionFactory());
		Connection dbconn = DataSourceUtils.getConnection(ds);
		parameters.put("REPORT_CONNECTION", dbconn);
		parameters.put("resvDate", selectedDate);
		parameters.put("facilityType", facilityType);
		parameters.put("TimePeriodType", timePeriodType);
		parameters.put("fileType", fileType);
		//Sorting by desired field
		JRDesignSortField sortField = new JRDesignSortField();
		List<JRSortField> sortList = new ArrayList<JRSortField>();
		sortField.setName(sortBy);
		if("true".equalsIgnoreCase(isAscending)){
			sortField.setOrder(SortOrderEnum.ASCENDING);
		}else{
			sortField.setOrder(SortOrderEnum.DESCENDING);
		}
		sortField.setType(SortFieldTypeEnum.FIELD);
		sortList.add(sortField);
		parameters.put(JRParameter.SORT_FIELDS, sortList);
		//Ignore pagination for csv
		if("csv".equals(fileType)){
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(reFile.getPath(), parameters, dbconn);

		ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		JRAbstractExporter exporter = exportedByFileType(fileType, jasperPrint, outPut);
		if (exporter == null) {
			return null;
		} else {
			exporter.exportReport();
			return outPut.toByteArray();
		}
	}

	@Override
	public long getTotalFacilityTypeQty(long resvId) {
		StringBuilder hql = new StringBuilder();

		hql.append("SELECT count(distinct t.facilityMaster) FROM MemberFacilityTypeBooking f inner join f.facilityTimeslots t");
		hql.append(" where f.resvId = ? ");

		Query query = super.getCurrentSession().createQuery(hql.toString());
		query.setParameter(0, resvId);

		return (long) query.uniqueResult();

	}

	@Override
	public long getTotalCheckedInFacilityTypeQty(long resvId) {
		StringBuilder hql = new StringBuilder();

		hql.append("SELECT count(distinct t.facilityMaster) FROM MemberFacilityTypeBooking f inner join f.facilityTimeslots t");
		hql.append(" where f.resvId = ? ");

		hql.append(" and t.status in ('").append(FacilityStatus.OP).append("','").append(FacilityStatus.CAN).append("')");
		Query query = super.getCurrentSession().createQuery(hql.toString());
		query.setParameter(0, resvId);

		return (long) query.uniqueResult();
	}

	@Override
	public int getMemberFacilityTypeBookingBayCount(long customerId, Date createDate, String facilityType) throws Exception
	{
		String sql = " SELECT IFNULL(SUM(b.facility_type_qty), 0) AS facility_type_qty "
				+" FROM member_facility_type_booking b "
				+" WHERE	b.resv_facility_type = ? "
		        +" AND b.status <> 'CAN' "
		        +" AND b.status <> 'PND' "
		        +" AND b.exp_coach_user_id IS NULL "
		        +" AND b.reserve_via IN ('APS' , 'WP') "
		        +" AND b.customer_id = ? "
		        +" AND b.resv_id IN (SELECT f.resv_id "
		        +" FROM member_reserved_facility f "
		        +" JOIN facility_timeslot t ON t.facility_timeslot_id = f.facility_timeslot_id "
		        +" LEFT JOIN room_facility_type_booking r ON r.facility_type_resv_id = f.resv_id "
		        +" WHERE f.resv_id = b.resv_id "
                +" AND t.status <> 'CAN' "
                +" AND DATE(t.begin_datetime) = date(?) "
                +" AND (t.reserve_type = ?) "
                +" AND t.transfer_from_timeslot_id IS NULL "
                +" AND r.facility_type_resv_id IS NULL )";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(facilityType);
		params.add(customerId);
		params.add(DateCalcUtil.formatDatetime(createDate));
		params.add(Constant.RESERVE_TYPE_MR);
		Object count = super.getUniqueBySQL(sql, params);
		return ((Number)count).intValue();
	
	}
	
	/**
	 * 查询member预订记录【添加aqcdto可控制是否显示canelled】
	 * @param page
	 * @param dto
	 * @param customerId
	 * @param appType
	 * @param year
	 * @param month
	 * @param day
	 * @param startDate
	 * @param endDate
	 * @param aqcDto
	 * @return
	 */
	@Override
	public ListPage<MemberFacilityTypeBooking> getMemberBookingRecordsByCustomerId(ListPage<MemberFacilityTypeBooking> page,MemberFacilityTypeBookingDto dto,
			Long customerId,String appType,String year,String month,String day,String startDate,String endDate, AdvancedQueryConditionsDto aqcDto){
		String courtBookingShowStatusSql = "";
		String courseBookingShowStatusSql = "";
		String restaurantBookingShowStatusSql = "";
		String gustRoomBookingShowStatusSql = ", 'CAN' ";
		String wellnessBookingShowStatusSql = "";
		//  TODO 
		if (!aqcDto.getIsShowCancelled()) {
			courtBookingShowStatusSql = " AND m.STATUS != 'CAN'\n";
			courseBookingShowStatusSql = " and ce.status !='CAN' and cm.status != 'CAN'\n";
			restaurantBookingShowStatusSql = " AND m.STATUS != 'CAN' ";
			gustRoomBookingShowStatusSql = "";
			wellnessBookingShowStatusSql = " AND sar.status != 'CAN' ";
		}
		String sql =  
		"SELECT\n" +
		"	NULL AS facilityName,\n" +
		"	NULL AS courseName,\n" +
		"	NULL AS posterFilename," +
		"	NULL AS courseId," +
		"   NULL as attendId,\n" +
		"	CONCAT(\n" +
		"		date_format(\n" +
		"			m.begin_datetime_book,\n" +
		"			'%Y-%b-%d %H:%i'\n" +
		"		),\n" +
		"		' - ',\n" +
		"		date_format(\n" +
		"			DATE_ADD(m.end_datetime_book,INTERVAL 1 MINUTE),\n" +
		"			'%H:%i'\n" +
		"		)\n" +
		"	) AS bookingDateFull,\n" +
		"	date_format(\n" +
		"		m.begin_datetime_book,\n" +
		"		'%Y-%b-%d'\n" +
		"	) AS bookingDate,\n" +
		"	date_format(\n" +
		"		m.begin_datetime_book,\n" +
		"		'%H:%i'\n" +
		"	) AS startTime,\n" +
		"	date_format(\n" +
		"		DATE_ADD(m.end_datetime_book,INTERVAL 1 MINUTE),\n" +
		"		'%H:%i'\n" +
		"	) AS endTime,\n" +
		"	m.begin_datetime_book AS beginDate,\n" +
		"	m.end_datetime_book AS endDate,\n" +
		"	m. STATUS AS status,\n" +
		"	NULL AS enrollStatus,\n" +
		"	m.facility_type_qty AS facilityTypeQty,\n" +
		"	NULL AS sessionNo,\n" +
		"	NULL AS venue,\n" +
		"	NULL AS enrollId,\n" +
		"	m.order_no AS orderNo,\n" +
		"	m.resv_id AS resvId,\n" +
		"	null as facilityTypeResvId,"+
		"	m.exp_coach_user_id AS coachName,\n" +
		"(CASE m.resv_facility_type\n" +
		"	WHEN 'GOLF' THEN \n" +
		"	cap.caption\n" +
		"  WHEN 'TENNIS' THEN\n" +
		"  ifnull(fst.name,cap.caption)\n" +
		"  else 'Other'\n" +
		"  end ) as facilityAttribute,\n "+
		"  (CASE m.resv_facility_type\n" +
		"	WHEN 'GOLF' THEN \n" +
		"	cap.attribute_id \n" +
		"  WHEN 'TENNIS' THEN\n" +
		"  IFNULL(fst.subtype_id,cap.attribute_id )\n" +
		"  else null\n" +
		"  end ) as venueCode, "+
		"	IFNULL(m.customer_remark, '') AS remark,null as sessionId,\n" +
		"   m.create_date as createDate,\n" +
		"   null as restaurantId,\n"+
		"   null as phone,\n "+
		"   null as confirmId,\n" +
		"   room_booking.is_bundle as isBundle,\n"+
		"   NULL AS therapist,\n" +
		"   NULL AS roomNo,\n" +
		" 	m.resv_facility_type as facilityType,\n" +
		"   CASE WHEN room_booking.room_resv_id IS NOT NULL THEN 'Guest Room Bundled'\n" +
		"       WHEN m.order_no IS NULL THEN 'Maintenance Offer'\n" +
		"	     ELSE 'Member Booking'\n" +
		"   END AS resvType,\n"+
		"   NULL as checkinTimestamp,\n" +
		"   NULL as checkoutTimestamp,\n" +
		"   NULL as picId,\n" +
		" 	NULL as roomId ,\n" +
		//CR51
		"	CASE \n"+
		"	WHEN  m.exp_coach_user_id IS NOT NULL and m.status<>'"+Status.CAN.name()+"' THEN  (SELECT vendor_poi_id FROM venue_master v WHERE v.venue_code='PROSHOP') \n"+
		"	WHEN  m.exp_coach_user_id IS  NULL   and m.status='"+Status.ATN.name()+"' THEN  fm.vendor_poi_id \n"+
		"	ELSE NULL \n"+
		"	END  \n"+
		"	AS  vendorPoiId \n"+
		" FROM\n" +
		"	member_facility_type_booking m \n " +
		"  left join \n" +
		"	member_facility_book_addition_attr attr on m.resv_id = attr.resv_id\n" +
		"  left join\n" +
		"	facility_attribute_caption cap on cap.attribute_id = attr.attribute_id\n" +
		"  left join \n" +
		"  facility_sub_type fst on fst.subtype_id = m.facility_subtype_id \n"+
		"  left join room_facility_type_booking room_booking on room_booking.facility_type_resv_id = m.resv_id and room_booking.is_bundle = 'Y'\n"+
		//CR51
		"	INNER JOIN member_reserved_facility mf  ON m.resv_id=mf.resv_id  \n"+
		"	LEFT JOIN facility_timeslot ft ON mf.facility_timeslot_id=ft.facility_timeslot_id  \n"+
		"	LEFT JOIN facility_master fm ON ft.facility_no=fm.facility_no  \n"+
		
		"WHERE\n" +
		" m. STATUS != 'PND'\n" +
//		"AND m. STATUS != 'CAN'\n"+
		courtBookingShowStatusSql +
		"AND m.customer_id = ?\n" +
		"AND <condition_date_filter_facilities>\n" +
		
		"UNION ALL\n" +
		"	SELECT\n" +
		"       DISTINCT"+
		"		NULL AS facilityName,\n" +
		"		cm.course_name AS courseName,\n" +
		"		cm.poster_filename AS posterFilename,\n" +
		"		cm.course_id AS courseId,attendance.attend_id as attendId, \n" +
		"		CONCAT(\n" +
		"			date_format(\n" +
		"				cs.begin_datetime,\n" +
		"				'%Y-%b-%d %H:%i'\n" +
		"			),\n" +
		"			' - ',\n" +
		"			date_format(cs.end_datetime, '%H:%i')\n" +
		"		) AS bookingDateFull,\n" +
		"		date_format(\n" +
		"			cs.begin_datetime,\n" +
		"			'%Y-%b-%d'\n" +
		"		) AS bookingDate,\n" +
		"		date_format(cs.begin_datetime, '%H:%i') AS startTime,\n" +
		"		date_format(cs.end_datetime, '%H:%i') AS endTime,\n" +
		"		cs.begin_datetime AS beginDate,\n" +
		"		cs.end_datetime AS endDate,\n" +
		"		ifnull(attendance.status, NULL) AS status,\n" +
		"		ifnull(ce.status, NULL) AS enrollStatus,\n" +
		"		NULL AS facilityTypeQty,\n" +

		"		cs.session_no AS sessionNo,\n" +
		"       NULL AS venue,\n"+
		"		ce.enroll_id AS enrollId,\n" +
		"		NULL AS orderNo,\n" +
		"		ce.enroll_id AS resvId,\n" +
		"		null as facilityTypeResvId,"+
		"		NULL AS coachName,\n" +
		"		NULL AS facilityAttribute,\n" +
		"       null as venueCode,\n "+
		"		IFNULL(cm.internal_remark, ' ') AS remark,cs.sys_id as sessionId,\n" +
		"       ce.create_date as createDate,\n" +
		"       null as restaurantId,\n"+
		"       null as phone,\n "+
		"       null as confirmId,\n" +
		"       null as isBundle,\n"+
		"       NULL AS therapist,\n" +
		"       NULL AS roomNo,\n" +
		" 		cm.course_type as facilityType,\n" +
		"       NULL AS resvType,\n"+
		"   	NULL as checkinTimestamp,\n" +
		"   	NULL as checkoutTimestamp,\n" +
		"       NULL as picId,\n" +
		" 		NULL as roomId ,\n" +
		"	    NULL as vendorPoiId \n" +
		"	FROM\n" +
		"		course_enrollment ce,\n" +
		"		course_master cm\n"+
		"   LEFT JOIN course_session cs\n"+
		"        ON cs.course_id = cm.course_id and (cs.status = 'ACT' OR cs.status = 'CAN') " +
		"	LEFT JOIN student_course_attendance attendance ON attendance.course_session_id = cs.sys_id "+
		"             AND attendance.enroll_id in (select DISTINCT enroll_id from course_enrollment where customer_id = ? ) \n" + //fixed duplicate session by excluding those attendance record of other members.
		"	WHERE\n" +
		"		ce.customer_id = ?\n" +
		"	AND cm.course_id = ce.course_id\n" +
		"	AND ce.status != 'PND' " + courseBookingShowStatusSql +// and ce.status !='CAN' and cm.status != 'CAN'\n
		"   AND <condition_date_filter_course>\n" +
		"   AND ce.status != 'ACT'"+
		
		" UNION ALL\n "+
		"   SELECT\n " +
		"   rm.restaurant_name AS facilityName,\n" +
		"   NULL AS courseName,\n" +
		"   restImage.image_filename AS posterFilename,\n" +
		"   NULL AS courseId,\n" +
		"   NULL AS attendId,\n" +
		"   DATE_FORMAT(m.book_time, '%Y-%b-%d %H:%i') AS bookingDateFull,\n" +
		"   DATE_FORMAT(m.book_time, '%Y-%b-%d') AS bookingDate,\n" +
		"   DATE_FORMAT(m.book_time, '%H:%i') AS startTime,\n" +
		"   NULL AS endTime,\n" +
		"   m.book_time AS beginDate,\n" +
		"   NULL AS endDate,\n" +
		"   m.STATUS AS status,\n" +
		"   NULL AS enrollStatus,\n" +
		"   m.party_size AS facilityTypeQty,\n" +
		"   NULL AS sessionNo,\n" +
		"   NULL AS venue,\n" +
		"   NULL AS enrollId,\n" +
		"   NULL AS orderNo,\n" +
		"   m.resv_id AS resvId,\n" +
		"	null as facilityTypeResvId,\n"+
		"   NULL as coachName,\n" +
		"   NULL AS facilityAttribute,\n" +
		"   NULL AS venueCode,\n" +
		"   IFNULL(m.remark, '') AS remark,\n" +
		"   NULL AS sessionId,\n" +
		"   m.create_date AS createDate,\n" +
		"   m.restaurant_id as restaurantId,\n"+
		"   rm.phone as phone,\n"+
		"   null as confirmId,\n" +
		"   null as isBundle,\n"+
		"   NULL AS therapist,\n" +
		"   NULL AS roomNo,\n" +
		" 	'RESTAURANT' as facilityType,\n" +
		"   NULL AS resvType,\n"+
		"   NULL as checkinTimestamp,\n" +
		"   NULL as checkoutTimestamp,\n" +
		"   NULL as picId,\n" +
		" 	NULL as roomId ,\n" +
		"	CASE 	\n"+
		"	WHEN m.status='"+Status.CFM.name()+"' THEN vm.vendor_poi_id \n"+
		"	ELSE null \n"+
		"	END as vendorPoiId \n"+
		"   FROM\n" +
		"   restaurant_customer_booking m\n" +
		"   left join restaurant_master rm on m.restaurant_id = rm.restaurant_id\n" +
		"   LEFT JOIN restaurant_image restImage ON restImage.restaurant_id = m.restaurant_id and restImage.image_feature_code = 'RR'\n"+
		"	LEFT JOIN venue_master vm ON rm.venue_code=vm.venue_code \n"+
		"   WHERE\n" +
		"   <condition_date_filter_restaurant>\n" +
		"   AND m.STATUS != 'DEL'\n" + restaurantBookingShowStatusSql + 
		"   AND m.customer_id = ? \n"+
		
		"   UNION ALL\n" +
		"         SELECT\n" +
		"            'Accommodation' AS facilityName,\n" +
		"            NULL AS courseName,\n" +
		"            rp.server_filename AS posterFilename,\n" +
		"            NULL AS courseId,\n" +
		"            NULL AS attendId,\n" +
		"            CONCAT(DATE_FORMAT(rc.arrival_date, '%Y-%b-%d'),' - ',date_format(rc.departure_date, '%Y-%b-%d')) AS bookingDateFull,\n" +
		"            DATE_FORMAT(rc.arrival_date,\n" +
		"            '%Y-%b-%d') AS bookingDate,\n" +
		"            DATE_FORMAT(rc.arrival_date,\n" +
		"            '%b-%d') AS startTime,\n" +
		"            DATE_FORMAT(rc.departure_date, '%b-%d') AS endTime,\n" +
		"            rc.arrival_date AS beginDate,\n" +
		"            rc.departure_date AS endDate,\n" +
		//SGG-SGG-3472
		" 			IFNULL(CONCAT(rc.status,'-',rc.slient_checkin_status),rc.status) as status,"+
		"            NULL AS enrollStatus,\n" +
		"            rc.night AS facilityTypeQty,\n" +
		"            NULL AS sessionNo,\n" +
		"            NULL AS venue,\n" +
		"            NULL AS enrollId,\n" +
		"            NULL AS orderNo,\n" +
		"            rc.resv_id AS resvId,\n" +
		"            null AS facilityTypeResvId,\n"+
		"            NULL as coachName,\n" +
		"            ifnull(r.type_name,rc.room_type_code) AS facilityAttribute,\n" +
		"            NULL AS venueCode,\n" +
		"            '' AS remark,\n" +
		"            NULL AS sessionId,\n" +
		"            rc.create_date AS createDate,\n" +
		"            NULL as restaurantId,\n" +
		"            NULL as phone,\n" +
		"            rc.confirm_id as confirmId,\n" +
		"   		 null as isBundle,\n"+
		"			 NULL AS therapist,\n" +
		"			 room.room_no AS roomNo,\n" +
		" 			 'GUESTROOM' as facilityType,\n" +
		"            NULL AS resvType,\n"+
		"            rc.checkin_timestamp as checkinTimestamp,\n" +
		"            rc.checkout_timestamp as checkoutTimestamp,\n"+
		"            rp.pic_id as picId,\n" +
		"            rc.room_id as roomId ,\n" +
		"			CASE 	   \n"+
		"			WHEN  rc.status='"+RoomReservationStatus.CKI.name()+"' then room.vendor_poi_id \n"+
		"			ELSE null  \n"+
		"			END as vendorPoiId \n "+
		"  FROM room_reservation_rec rc\n " +
		"  LEFT JOIN room_type r ON r.type_code = rc.room_type_code\n"+
		"  LEFT JOIN room_pic_path rp ON rp.room_type_code = rc.room_type_code and rp.display_order = '1'\n"+
		"  LEFT JOIN room room ON room.room_id = rc.room_id\n"+
		"  where <condition_date_filter_guestroom>\n "+
		"  and rc.status IN('SUC', 'CKI', 'CKO', 'CKP'" + gustRoomBookingShowStatusSql + ")  and rc.customer_id =?\n "+ //TODO
		
		"  UNION ALL\n" +
		"SELECT\n" +
		"	sar.service_name AS facilityName,\n" +
		"	NULL AS courseName,\n" +
		"	NULL AS posterFilename,\n" +
		"	NULL AS courseId,\n" +
		"	NULL AS attendId,\n" +
		"	CONCAT_ws(\n" +
		"		' - ',\n" +
		"		date_format(\n" +
		"			sar.start_datetime,\n" +
		"			'%Y-%b-%d %H:%i'\n" +
		"		),\n" +
		"		date_format(sar.end_datetime, '%H:%i')\n" +
		"	) AS bookingDateFull,\n" +
		"	date_format(\n" +
		"		sar.start_datetime,\n" +
		"		'%Y-%b-%d'\n" +
		"	) AS bookingDate,\n" +
		"	date_format(sar.start_datetime, '%H:%i') AS startTime,\n" +
		"	date_format(sar.end_datetime, '%H:%i') AS endTime,\n" +
		"	sar.start_datetime AS beginDate,\n" +
		"	sar.end_datetime AS endDate,\n" +
		"	sar.status AS status,\n" +
		"	NULL AS enrollStatus,\n" +
		"	NULL AS facilityTypeQty,\n" +
		"	NULL AS sessionNo,\n" +
		"	NULL AS venue,\n" +
		"	NULL AS enrollId,\n" +
		"	NULL AS orderNo,\n" +
		"	CONCAT('SPA-', right(sar.ext_appointment_id,6)) AS resvId,\n" +
		"	NULL AS facilityTypeResvId,\n" +
		"	NULL AS coachName,\n" +
		"	NULL AS facilityAttribute,\n" +
		"	NULL AS venueCode,\n" +
		"	NULL AS remark,\n" +
		"	NULL AS sessionId,\n" +
		"	sar.create_date AS createDate,\n" +
		"	NULL AS restaurantId,\n" +
		"	NULL AS phone,\n" +
		"	NULL AS confirmId,\n" +
		"	NULL AS isBundle,\n" +
		"	concat_ws(\n" +
		"		' ',\n" +
		"		sar.ext_employee_first_name,\n" +
		"		sar.ext_employee_last_name\n" +
		"	) AS therapist,\n" +
		"	ifnull(room.room_no, '') AS roomNo,\n" +
		"  'WELLNESS' as facilityType,\n" +
		"   NULL AS resvType,\n" +
		"   NULL as checkinTimestamp,\n" +
		"   NULL as checkoutTimestamp,\n" +
		"   NULL as picId,\n" +
		" 	NULL as roomId,\n" +
		"	NULL AS vendorPoiId "+
		"FROM\n" +
		"	spa_appointment_rec sar\n" +
		"LEFT JOIN room room ON room.room_id = sar.room_id\n" +
		"WHERE\n" +
		"   sar.status is not null\n "+ wellnessBookingShowStatusSql + // TODO
		" AND sar.customer_id = ?\n"+
		" AND <condition_date_filter_wellness> ";

		List<Serializable> param = new ArrayList<Serializable>();
	    Date selectedFilteredDate = new Date();
	    
	    String dateFilteringFacilities = null;
	    String dateFilteringCourse = null;
	    String dateFilterRestaurant = null;
	    String dateFilterGuestRoom = null;
	    String dateFilterWellness = null;
	    if(!StringUtils.isEmpty(year)&&!StringUtils.isEmpty(month)&&!StringUtils.isEmpty(day)){
	    	selectedFilteredDate =CommUtil.formatTheGivenYearAndMonthAndDay(year,month,day);
		    dateFilteringFacilities = " date_format( ? ,'%Y-%m-%d') = date_format(m.begin_datetime_book,'%Y-%m-%d') ";
		    dateFilteringCourse     = " date_format( ? ,'%Y-%m-%d') = date_format(ifnull(cs.begin_datetime,?),'%Y-%m-%d') ";
		    dateFilterRestaurant    = " date_format( ? ,'%Y-%m-%d') = date_format(m.book_time,'%Y-%m-%d') ";
		    dateFilterGuestRoom     = " date_format( ? ,'%Y-%m-%d') = date_format(rc.arrival_date,'%Y-%m-%d') ";
		    dateFilterWellness      = " date_format( ? ,'%Y-%m-%d') = date_format(sar.start_datetime,'%Y-%m-%d') ";
	    }else if(!StringUtils.isEmpty(year)&&!StringUtils.isEmpty(month)){
	    	selectedFilteredDate =CommUtil.formatTheGivenYearAndMonth(year,month);
		    dateFilteringFacilities = " date_format( ? ,'%Y-%m') = date_format(m.begin_datetime_book,'%Y-%m') ";
		    dateFilteringCourse     = " date_format( ? ,'%Y-%m') = date_format(ifnull(cs.begin_datetime,?),'%Y-%m') ";
		    dateFilterRestaurant    = " date_format( ? ,'%Y-%m') = date_format(m.book_time,'%Y-%m') ";
		    dateFilterGuestRoom     = " date_format( ? ,'%Y-%m') = date_format(rc.arrival_date,'%Y-%m') ";
		    dateFilterWellness      = " date_format( ? ,'%Y-%m') = date_format(sar.start_datetime,'%Y-%m') ";
		    
	    }else if("WP".equalsIgnoreCase(appType)&& !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)){
		    dateFilteringFacilities = " date_format( m.begin_datetime_book,'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
		    dateFilteringCourse     = " date_format( ifnull(cs.begin_datetime,?),'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
		    dateFilterRestaurant    = " date_format( m.book_time,'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
		    dateFilterGuestRoom     = " date_format( rc.arrival_date,'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
		    dateFilterWellness      = " date_format( sar.start_datetime,'%Y-%m-%d') BETWEEN date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') ";
	    }else{
	    	dateFilteringFacilities = " date_format( ? ,'%Y-%m-%d') <= date_format(m.end_datetime_book,'%Y-%m-%d') ";
	    	//modified by Kaster 20160304 新需求：课程结束后半个小时内的记录也查出来。
	    	dateFilteringCourse     = " date_format( ? ,'%Y-%m-%d') <= date_format(DATE_ADD(ifnull(cs.end_datetime,?),INTERVAL 30 MINUTE),'%Y-%m-%d') ";
		    dateFilterRestaurant    = " m.STATUS != 'EXP' and date_format( ? ,'%Y-%m-%d') <= date_format(m.book_time,'%Y-%m-%d')";
		    dateFilterGuestRoom     = " date_format(?,'%Y-%m-%d')<=date_format(rc.arrival_date,'%Y-%m-%d') ";
		    dateFilterWellness      = " date_format(?,'%Y-%m-%d')<=date_format(sar.start_datetime,'%Y-%m-%d') ";
	    }
	    sql = sql.replace("<condition_date_filter_facilities>",dateFilteringFacilities)
	    		 .replace("<condition_date_filter_course>",dateFilteringCourse)
	    		 .replace("<condition_date_filter_restaurant>",dateFilterRestaurant)
	    		 .replace("<condition_date_filter_guestroom>",dateFilterGuestRoom)
	             .replace("<condition_date_filter_wellness>", dateFilterWellness);
	    
		/* Parameter for Facilities */
		param.add(customerId);
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(selectedFilteredDate);
		}

		/* Parameter for Course Enrollment */
		param.add(customerId);
		param.add(customerId);
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(selectedFilteredDate);
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(selectedFilteredDate);
			param.add(selectedFilteredDate);
		}

		/* Parameter for Restaurant Booking */
//		param.add(appType);
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(selectedFilteredDate);
		}
		param.add(customerId);

		/* Parameter for Guest Room */
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(selectedFilteredDate);
		}
		param.add(customerId);
		
		/* Parameter for Wellness */
		if ("WP".equalsIgnoreCase(appType) && !StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
			param.add(customerId);
			param.add(startDate);
			param.add(endDate);
		} else {
			param.add(customerId);
			param.add(selectedFilteredDate);
		}
		
		sql = "select s.* from ("+sql+") s ";
		String countSql = " SELECT count(1) from ( " +sql+") x";
		
		/*Return Regular List without Pages for Member APP's My Booking for Particular Month*/
		if(!StringUtils.isEmpty(year)&&!StringUtils.isEmpty(month)&&StringUtils.isEmpty(day)){
			List<ListPage<MemberFacilityTypeBooking>.OrderBy> orderByList = page.getOrderByList();

			StringBuilder orderby = new StringBuilder("");
			if ((orderByList != null) && (orderByList.size()>0)){
				if(sql != null	&& sql.toUpperCase().indexOf("ORDER BY") < 0){
					for(ListPage<MemberFacilityTypeBooking>.OrderBy each: orderByList){
						if(each !=null){
							orderby.append(("".equals(orderby.toString())) ? " ORDER BY " : " ,");
							orderby.append(" "+each.getProperty() +  ((ListPage.OrderType.ASCENDING.equals(each.getType())) ? " ASC " : " DESC "));
						}
					}
				}
			}

			sql =  sql + orderby.toString();

			List<MemberFacilityTypeBookingDto> listOnly = getDtoBySql(sql, param, MemberFacilityTypeBookingDto.class);
			page.setDtoList(listOnly);

			return page;
		}
		/*Return Regular List with Pages for Member APP's Home Page, Web Portal's My Bookings and Portal's My Bookings*/
		else{
			List<ListPage<MemberFacilityTypeBooking>.OrderBy> orderByList = page.getOrderByList();

			StringBuilder orderby = new StringBuilder("");
			if ((orderByList != null) && (orderByList.size()>0)){
				if(sql != null	&& sql.toUpperCase().indexOf("ORDER BY") < 0){
					for(ListPage<MemberFacilityTypeBooking>.OrderBy each: orderByList){
						if(each !=null){
							orderby.append(("".equals(orderby.toString())) ? " ORDER BY " : " ,");
							orderby.append(" "+each.getProperty() +  ((ListPage.OrderType.ASCENDING.equals(each.getType())) ? " ASC " : " DESC "));
						}
					}
				}
			}

			sql =  sql + orderby.toString();
			return listBySqlDto(page, countSql, sql.toString(), param, dto);
		}
		
	}
	
	/**
	 * 重载getRoomReservationFacilities【添加aqcdto可控制是否显示canelled】
	 * @param roomResvId
	 * @param dto
	 * @return
	 */
	@Override
	public List<MemberFacilityTypeBookingDto> getRoomReservationFacilities(Long roomResvId, AdvancedQueryConditionsDto dto){
		String isShowCancelledSql = "";
		if (!dto.getIsShowCancelled()) {
			isShowCancelledSql = "   AND member_booking.status != 'CAN'\n";
		}
		String sql = 
				"SELECT\n" +
				"	date_format(\n" +
				"		member_booking.begin_datetime_book,\n" +
				"		'%Y-%b-%d'\n" +
				"	) AS bookingDate,\n" +
				"	date_format(\n" +
				"		member_booking.begin_datetime_book,\n" +
				"		'%H:%i'\n" +
				"	) AS startTime,\n" +
				"	date_format(\n" +
				"		DATE_ADD(member_booking.end_datetime_book,INTERVAL 1 MINUTE),\n" +
				"		'%H:%i'\n" +
				"	) AS endTime\n" +
				"FROM\n" +
				"	room_facility_type_booking room_booking\n" +
				"LEFT JOIN member_facility_type_booking member_booking ON room_booking.facility_type_resv_id = member_booking.resv_id\n" +
				"WHERE\n" +
				"	room_booking.room_resv_id = ?\n" +
				"   AND room_booking.is_bundle = 'Y'\n"+
				isShowCancelledSql + 
				"   AND member_booking.status != 'PND'\n"+
				"Order by member_booking.begin_datetime_book ASC ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(roomResvId);
		return getDtoBySql(sql, param, MemberFacilityTypeBookingDto.class);
	}
	/**
	 * 重载方法【添加aqcdto可控制是否显示canelled】
	 * @param resvId
	 * @param dto
	 * @return
	 */
	@Override
	public MemberFacilityTypeBooking getMemberFacilityTypeBooking(long resvId, AdvancedQueryConditionsDto dto) {
		String isShowCancelledSql = "";
		if (!dto.getIsShowCancelled()) {
			isShowCancelledSql = " and status != 'CAN' ";
		}
		String hql = " SELECT f FROM MemberFacilityTypeBooking f left join f.memberReservedFacilitys as mrf where f.resvId =? " + isShowCancelledSql;
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(resvId);
		List<MemberFacilityTypeBooking> bookings =super.getByHql(hql, paramList);
		if(null != bookings && bookings.size()>0)return bookings.get(0);
		return null;
	}
	
	/**
	 * 重载方法【添加aqcdto可控制是否显示canelled】
	 * @param facilityType
	 * @param dateRange
	 * @param customerId
	 * @param isPushMessage
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	@Override
	public String getAllMemberFacilityTypeBooking(String facilityType, String dateRange, String customerId,boolean isPushMessage, AdvancedQueryConditionsDto dto) throws Exception
	{
		
		String isShowCancelledSql = "";
		if (!dto.getIsShowCancelled()) {
			isShowCancelledSql = " and upper(a.status) <> 'CAN' ";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("select \n");
		sb.append("a.resv_id as resvId, \n");
		sb.append("a.resv_facility_type as resvFacilityType, \n");
		sb.append("a.facilityTypeQty as facilityTypeQty, \n");
		sb.append("date_format(date(a.begin_datetime_book),'%Y-%b-%d') as bookingDate, \n");
		sb.append("a.statusDisplay as status,\n");
		sb.append("a.reserveType as reserveType,\n");
		sb.append("a.exp_coach_user_id as coachId, \n");
		sb.append("concat(concat(f.given_name,' '), f.surname) as coachName, \n");
		sb.append("c.academy_no as academyNo, \n");
		sb.append("b.memberName as memberName, \n");
		sb.append("concat(CAST(hour(a.begin_datetime_book) AS char),':00') as startTime, \n");
		sb.append("concat(CAST((hour(a.end_datetime_book) + 1) AS char),':00') as endTime, \n");
		sb.append(" case when a.resv_facility_type='TENNIS' then a.facility_subtype_id else d.attribute_id end as facilityAttribute, \n");
		sb.append(" case  \n");
		sb.append(" when a.status='ATN' then '1'  \n");
		sb.append(" else '0'  \n");
		sb.append(" end as checkedIn, \n");
		sb.append(" b.contact_email as email, \n");
		sb.append(" a.order_no as orderNo, \n");
		sb.append(" b.customer_id as customerId,\n");
		sb.append(" h.transaction_no as transactionNo,\n");
		sb.append(" h.payment_media AS paymentMedia,\n");
		sb.append(" h.status AS paymentStatus,\n");
		sb.append(" a.update_date AS updateDate, \n");
		sb.append(" a.create_date as createDate,  \n");
		sb.append(" c.user_id as userId, \n");
		sb.append(" CAST(r.room_resv_id  as UNSIGNED INTEGER) as roomResvId, \n");
		sb.append(" a.isBundle, CASE WHEN a.resv_facility_type='TENNIS' THEN  fst.name  ELSE i.caption  END AS bayType \n");
		sb.append(" from \n");
		sb.append(" (  select distinct n.*, r.is_bundle as isBundle,\n");
		sb.append("    case when n.status='ATN' THEN 'Checked-in' \n ");
		sb.append("		    WHEN n.STATUS = 'CAN' THEN 'Canceled'  \n");
		sb.append("         WHEN n.status = 'PND' AND ADDTIME(n.create_date, '00:15:00') >= ?  \n");
		sb.append( "          THEN 'Paying' ");
		sb.append( "         WHEN n.status = 'PND' AND ADDTIME(n.create_date, '00:15:00') < ?  \n");
		sb.append( "         THEN 'Pending for Payment' \n");
		sb.append( "         else case when ? <= n.end_datetime_book \n");
		sb.append( "         then 'Ready to check-in' \n");
		sb.append( "            else case when ? > n.end_datetime_book \n");
		sb.append( "               then 'Overdue' \n");
		sb.append( "         else 'Unknown' end end end as statusDisplay, \n");
		sb.append( " case when n.exp_coach_user_id IS NOT NULL \n");
		sb.append( " then 'private coach training' \n");
		sb.append( " when r.is_bundle='Y' \n");
		sb.append( " then 'Guest Room Bundle' \n");
		sb.append( " when t.reserve_type ='MR' or n.order_no IS NOT NULL \n");
		sb.append( " then 'Member Booking' ");
		sb.append( " when t.reserve_type ='MT' or n.order_no is NULL ");
		sb.append( " then 'Maintenance Offer' ");
		sb.append( " end as reserveType, ");
		sb.append( " n.facility_type_qty AS facilityTypeQty ");
		//sb.append( " count(distinct t.facility_no) as facilityTypeQty"
		sb.append(" from member_facility_type_booking n  \n");
		sb.append(" left join member_reserved_facility m on n.resv_id = m.resv_id  \n");
		sb.append("  left join facility_timeslot t on m.facility_timeslot_id = t.facility_timeslot_id  \n");
		sb.append(" left join room_facility_type_booking r on r.facility_type_resv_id = n.resv_id  \n");
		sb.append(" WHERE n.status <> 'PND' group by n.resv_id \n");
		sb.append(") a \n");
		sb.append("  inner join (select cp.*, concat(cp.salutation,' ',cp.given_name,' ',cp.surname) as memberName from customer_profile cp) b  \n");
		sb.append("  inner join member c  \n");
		sb.append("  left join member_facility_book_addition_attr d on a.resv_id = d.resv_id  \n");
		sb.append("  left  join staff_profile f on a.exp_coach_user_id=f.user_id \n");
		sb.append("  left join customer_order_hd g on a.order_no = g.order_no  \n");
		sb.append("  left join \n");
		sb.append(" (SELECT * FROM customer_order_trans h ,\n");
		sb.append("  ( SELECT CASE  \n");
		sb.append("	     WHEN EXISTS(SELECT 1 FROM customer_order_trans s  WHERE s.status='SUC'  AND s.order_no=th.order_no) \n");
		sb.append("	     THEN (SELECT MAX(s.transaction_no) FROM customer_order_trans s WHERE s.status='SUC' AND s.order_no=th.order_no) \n");
		sb.append("	     ELSE  MAX(th.transaction_no) END tid FROM customer_order_trans  th WHERE (th.status = 'SUC' or th.status = 'PND' or th.status = 'FAIL' ) group by th.order_no  \n");
		sb.append("    )ff  WHERE  ff.tid = h.transaction_no ");
		sb.append(" ) h  ON   h.order_no = g.order_no  \n");
		sb.append(" left join facility_attribute_caption i on d.attribute_id = i.attribute_id \n");
		sb.append(" left join room_facility_type_booking r on r.facility_type_resv_id = a.resv_id \n");
		sb.append(" left Join facility_sub_type AS fst ON a.facility_subtype_id=fst.subtype_id  \n");
		sb.append(" where  \n");
		sb.append(" a.customer_id=b.customer_id  \n");
		sb.append(" and b.customer_id = c.customer_id  \n");
		sb.append( isShowCancelledSql);
		sb.append(" and upper(a.status) <> 'NAT'  \n");
		sb.append(" and a.exp_coach_user_id is null  \n");

		if(isPushMessage){
			sb.append(" and upper(a.status) <> 'PND' \n");
			sb.append(" and h.status = 'SUC'  \n");
		}
		if (facilityType.toUpperCase().equals("GOLF") || facilityType.toUpperCase().equals("TENNIS")) {
			sb.append(" and a.resv_facility_type = '" + facilityType + "'  \n");
		}
		if (dateRange.toUpperCase().equals("TODAY")) {
			sb.append(" and date(a.begin_datetime_book) = '" + DateCalcUtil.formatDate(new Date()) + "' \n");
		} else if (dateRange.toUpperCase().equals("30")) {
			sb.append(" and date(a.begin_datetime_book) >= '" + DateCalcUtil.formatDate(new Date()) + "' \n");
			sb.append(" and date(a.begin_datetime_book) <= '" + DateCalcUtil.formatDate(DateCalcUtil.getNearDay(new Date(), 30)) + "' \n");
		} else if (dateRange.toUpperCase().equals("90")) {
			sb.append(" and date(a.begin_datetime_book) >= '" + DateCalcUtil.formatDate(new Date()) + "' ");
			sb.append(" and date(a.begin_datetime_book) <= '" + DateCalcUtil.formatDate(DateCalcUtil.getNearDay(new Date(), 90)) + "' \n");
		}
		if (customerId != null && customerId.length() > 0) {
			Integer.parseInt(customerId);
			sb.append(" and a.customer_id = " + customerId + "  \n");
		} 
		return sb.toString();
	}
	/**
	 * 获取最新预订记录id
	 * @param timeslotId
	 * @return
	 * @throws HibernateException
	 */
	@Override
	public BigInteger getNewBookingDataId(long timeslotId) throws HibernateException
	{
		String sql = " SELECT facility.resv_id resvId FROM facility_timeslot timeslot "
				+ " LEFT JOIN member_reserved_facility facility ON  timeslot.facility_timeslot_id = facility.facility_timeslot_id "
				+ " WHERE timeslot.transfer_from_timeslot_id = ? LIMIT 1";
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(timeslotId);
		BigInteger resvId = (BigInteger)super.getUniqueBySQL(sql, paramList);
		return resvId;
	}
	
	/**
	 * 获取今天又book的用户
	 * @param 
	 * @return List<ToDayBookingMemberDto>
	 * @throws HibernateException
	 */
	@Override
	public List<ToDayBookingMemberDto> getToDayBookingMember() throws HibernateException {
		String sql = " SELECT customerId, COUNT(booked) AS bookedNum  FROM "
				+ " ( 	SELECT customer_id AS customerId, m.resv_id AS booked  FROM member_facility_type_booking m "
				+ " 		WHERE status NOT IN ('CAN', 'NAT') AND DATE_FORMAT(m.end_datetime_book,'%Y-%m-%d') = CURDATE() "
				+ "   UNION ALL  "
				+ "   	SELECT r.customer_id AS customerId, r.resv_id AS booked  FROM room_reservation_rec r "
				+ "  		WHERE  status NOT IN ('CAN', 'IGN') AND DATE_FORMAT(r.arrival_date,'%Y-%m-%d') = CURDATE() "
				+ "   UNION ALL  "
				+ " 	SELECT a.customer_id AS customerId, a.enroll_id AS booked FROM course_enrollment a LEFT JOIN course_session b ON a.course_id = b.course_id "
				+ "    		WHERE a.status NOT IN ('CAN', 'REJ')  AND DATE_FORMAT(b.begin_datetime,'%Y-%m-%d') = CURDATE() "
				+ " ) sub  GROUP BY customerId ";
		List<ToDayBookingMemberDto> dto = super.getDtoBySql(sql, null, ToDayBookingMemberDto.class);
		return dto;
	}

}
