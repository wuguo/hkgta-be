package com.sinodynamic.hkgta.dao.pms;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;

@Repository("roomTypeListConditions")
public class RoomTypeListConditionsImpl extends GenericDao implements AdvanceQueryConditionDao
{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("RoomType Name", "roomTypeName", "java.lang.String", "", 1);
		final AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("RoomType Code", "roomTypeCode", "java.lang.String", "", 2);
		final AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Amenity", "amenity", "java.lang.String", "", 3);
		final AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Num Of Guest", "numOfGuest", "java.lang.String", "", 4);
		
		return Arrays.asList(condition1, condition2, condition3, condition4);
	}

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type)
	{
		return null;
	}

}
