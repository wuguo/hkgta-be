package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuest;
import com.sinodynamic.hkgta.entity.crm.UserActivateQuestPK;

public interface UserActivateQuestDao extends IBaseDao<UserActivateQuest> {
	
	public UserActivateQuest getById(UserActivateQuestPK id) throws Exception; 
	
}
