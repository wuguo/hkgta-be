package com.sinodynamic.hkgta.dao;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinodynamic.hkgta.util.pagination.ListPage;

/**
 * @author Junfeng_Yan
 *
 * @param <T> generic Type
 * @param <PK> primary key
 */
public abstract class GenericDao<T extends Serializable> implements IBaseDao<T>{
	
	private Logger logger = Logger.getLogger(GenericDao.class);
	@Autowired 
	private SessionFactory sessionFactory;
	
	protected Class<T> entityClass;

	public GenericDao(Class<T> t) {
		this.entityClass = t;
	}

	public SessionFactory getsessionFactory(){
		return sessionFactory;
	}

	@SuppressWarnings("unchecked")
	protected GenericDao() {
		Type type = null;
		if (getClass().getGenericSuperclass() instanceof ParameterizedType){
			type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
			}
		if ((type != null) && (type instanceof Class))
			this.entityClass = ((Class<T>) type);
	}

	public List getPageDatalist(int pageNum,int pageSize, String queryListHQL, List<Serializable>  parameters) {
		Query listQuery = this.sessionFactory.getCurrentSession().createQuery(queryListHQL);
		if (parameters != null && parameters.size() > 0) { 
			for (int i = 0; i < parameters.size(); i++) {
				listQuery.setParameter(i , parameters.get(i));
			}
		}
		listQuery.setFirstResult((pageNum - 1) * pageSize);
		listQuery.setMaxResults(pageSize);

		return listQuery.list();
		
	}
		
	
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public ListPage<T> listByHql(final ListPage<T> page, String countHql, String hql, List<?> param) throws HibernateException {
		Session session = sessionFactory.getCurrentSession();
		setPageSizeByHql(page, countHql,param,session);
		if(page.getAllPage()>0){
			hql = addOrderBy(page, hql);
			setPageByHql(page, hql, param,session);
		}
		return page;
	}
	
	public ListPage<T> listByHqlDto(final ListPage<T> page, String countHql, String hql, List<?> param,Object dto) throws HibernateException {
		Session session = sessionFactory.getCurrentSession();
		setPageSizeByHql(page, countHql,param,session);
		if(page.getAllPage()>0){
			hql = addOrderBy(page, hql);
			setPageByHqlDto(page, hql, param,session,dto);
		}
		return page;
	}
	
	@SuppressWarnings({ "unchecked"})
	private void setPageByHqlDto(final ListPage<T> page,final String hql,  List<?> param, Session session, Object dto) throws HibernateException{
		Query query = session.createQuery(hql).setResultTransformer(Transformers.aliasToBean(dto.getClass()));  
		addQueryParams(query,param);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.getDtoList().addAll(query.list());
	}
	
	public ListPage<T> listBySql(final ListPage<T> page,String countSql, String sql,List<?> param) throws HibernateException {
		Session session = sessionFactory.getCurrentSession();
		setPageSizeBySql(page, countSql,param,session);
		if(page.getAllPage()>0){
			sql = addOrderBy(page, sql);
			setPageBySql(page, sql, param,session);
		}
		return page;
	}
	
	@Override
	public ListPage<T> listBySqlDto(ListPage<T> page, String countSql,
			String sql, List<?> param, Object dto) throws HibernateException {
		Session session = sessionFactory.getCurrentSession();
		setPageSizeBySql(page, countSql,param,session);
		if(page.getAllPage()>0){
			sql = addOrderBy(page, sql);
			setPageBySqlDto(page, sql, param,session,dto);
		}
		return page;
	}
	
	
	public ListPage<T> listBySqlDto(ListPage<T> page, String countSql,
			String sql, List<?> param, Object dto,Map<String, org.hibernate.type.Type> map) throws HibernateException {
		Session session = sessionFactory.getCurrentSession();
		setPageSizeBySql(page, countSql,param,session);
		if(page.getAllPage()>0){
			sql = addOrderBy(page, sql);
			setPageBySqlDto(page, sql, param,session,dto,map);
		}
		return page;
	}
	
	
	@SuppressWarnings({ "unchecked"})
	private void setPageBySqlDto(final ListPage<T> page,final String sql,  List<?> param, Session session, Object dto,Map<String, org.hibernate.type.Type> map) throws HibernateException{
		
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		for(Map.Entry<String, org.hibernate.type.Type> entry : map.entrySet()){
			if(StringUtils.isNotEmpty(entry.getKey()) && null != entry.getValue()){
				sqlQuery.addScalar(entry.getKey(), entry.getValue());
			}
		}
		sqlQuery.setResultTransformer(Transformers.aliasToBean(dto.getClass()));
		addQueryParams(sqlQuery,param);
		sqlQuery.setMaxResults(page.getSize());
		sqlQuery.setFirstResult(page.getStart());
		page.getDtoList().addAll(sqlQuery.list());
	}
	
	@SuppressWarnings({ "unchecked"})
	private void setPageBySqlDto(final ListPage<T> page,final String sql,  List<?> param, Session session, Object dto) throws HibernateException{
		Query query = session.createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(dto.getClass()));  
		addQueryParams(query,param);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.getDtoList().addAll(query.list());
	}
	
	public String addOrderBy(ListPage<T> page, String hsql)
	  {
	    StringBuffer orderby = new StringBuffer("");
	    List<ListPage<T>.OrderBy> orderByList = page.getOrderByList();
	    if ((orderByList != null) && (orderByList.size()>0)){
	    	if(hsql != null	&& hsql.toUpperCase().indexOf("ORDER BY") < 0){
		    	for(ListPage<T>.OrderBy each: orderByList){
		    		if(each !=null){
		    			orderby.append(("".equals(orderby.toString())) ? " ORDER BY " : " ,");
		    			orderby.append(" "+each.getProperty() +  ((ListPage.OrderType.ASCENDING.equals(each.getType())) ? " ASC " : " DESC "));
		    		}
		    	}
	    	}else{
	    		hsql = "select * from("+hsql+") _GenericDaoAddOrderBy ";
	    		for(ListPage<T>.OrderBy each: orderByList){
		    		if(each !=null){
		    			orderby.append(("".equals(orderby.toString())) ? " ORDER BY " : " ,");
		    			orderby.append(" "+each.getProperty() +  ((ListPage.OrderType.ASCENDING.equals(each.getType())) ? " ASC " : " DESC "));
		    		}
		    	}
	    		
	    	}
	    }
	    return hsql + orderby.toString();
	  }
	
	private String addOrders(ListPage<T> page, String hsql)
	  {
	    StringBuffer orderby = new StringBuffer("");
	    List<ListPage<T>.OrderBy> orderByList = page.getOrderByList();
	    if ((orderByList != null) && (orderByList.size()>0)){
		    	for(ListPage<T>.OrderBy each: orderByList){
		    		if(each !=null){
		    			orderby.append(("".equals(orderby.toString())) ? " ORDER BY " : " ,");
		    			orderby.append(" "+each.getProperty() +  ((ListPage.OrderType.ASCENDING.equals(each.getType())) ? " ASC " : " DESC "));
		    		}
		    	}
	    }
	    return hsql + orderby.toString();
	  }
	
	private void setPageSizeByHql(final ListPage<T> page, String countHql, List<?> param,Session session) throws HibernateException{
		Query query = session.createQuery(countHql);
		addQueryParams(query,param);
		page.setAllSize(Integer.parseInt(String.valueOf(query.uniqueResult())));
		this.initFirstPage(page);
	}
	
	@SuppressWarnings("unchecked")
	private void setPageByHql(final ListPage<T> page,final String sql,  List<?> param,Session session) throws HibernateException{
		Query query = session.createQuery(sql);
		addQueryParams(query,param);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.getList().addAll(query.list());
	}
	
	private void setPageSizeBySql(final ListPage<T> page, String countSql, List<?> param,Session session) throws HibernateException{
		Query query = session.createSQLQuery(countSql);
		addQueryParams(query,param);
		page.setAllSize(Integer.parseInt(String.valueOf(query.uniqueResult())));
		this.initFirstPage(page);
	}
	
	@SuppressWarnings("unchecked")
	private void setPageBySql(final ListPage<T> page,final String sql,  List<?> param,Session session) throws HibernateException{
		Query query = session.createSQLQuery(sql).addEntity(entityClass);
		addQueryParams(query,param);
		query.setMaxResults(page.getSize());
		query.setFirstResult(page.getStart());
		page.getList().addAll(query.list());
	}
	
	protected Query addQueryParams(Query query, List<?> param){
		if (param != null && param.size()>0)
			for (int i = 0; i < param.size(); i++)
				query.setParameter(i, param.get(i));
	    return query;
	}

	@Override
	public Serializable save(T object) throws HibernateException {
		if (object == null) 
			throw new HibernateException("Object is null");
		 return getCurrentSession().save(object);
	}

	@Override
	public boolean update(T obj) throws HibernateException {
		if (obj == null) throw new HibernateException("Object is null");
		boolean isSucess = false;
		getCurrentSession().update(obj);
		isSucess = true;
		return isSucess;
	}

	@Override
	public boolean delete(T obj) throws HibernateException {
		if (obj == null) throw new HibernateException("Object is null");
		boolean isSucess = false;
		getCurrentSession().delete(obj);
		isSucess = true;
		return isSucess;
	}

	@Override
	public boolean saveOrUpdate(T obj) throws HibernateException {
		if (obj == null) throw new HibernateException("Object is null");
		boolean isSucess = false;
		getCurrentSession().saveOrUpdate(obj);
		isSucess = true;
		return isSucess;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(T object, Serializable PK) throws HibernateException {
		return (T) getCurrentSession().get(object.getClass(), PK);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(Class<T> clazz, Serializable PK) throws HibernateException {
		// TODO Auto-generated method stub
		return (T) getCurrentSession().get(clazz, PK);
	}

	@Override
	public T get(Class<T> clazz, Serializable PK, String initializeMethodName) throws Exception{
		return get(clazz, PK, new String[] {initializeMethodName});
	}

	@Override
	public T get(Class<T> clazz, Serializable PK,
			String[] initializeMethodNameList) throws Exception {
		Session session = null;
		T result	= null;
		try {
			session = sessionFactory.getCurrentSession();
			logger.debug("Getting " + clazz + " with id (" + PK + ")...");
			result = (T) session.get(clazz, PK);
			if (result != null) {
				try {
					for (String initializeMethodName : initializeMethodNameList) {
						Method mthd;
						mthd = clazz.getMethod(initializeMethodName);
						Hibernate.initialize(mthd.invoke(result));
					}
				} catch (Exception e) {
					throw e;
				}
			}
		} catch (HibernateException e) {
			logger.error("Unable to get " + clazz + "with id (" + PK + ")", e);
			throw e;
		} 
		return result;
	}

	@Override
	public List<T> getByCol(T obj, String colName, Serializable colVal,
			String orderBy) throws HibernateException {
		logger.debug("Getting " + obj.getClass().getSimpleName() + " by col ... ");

		Criteria criteria = getCurrentSession().createCriteria(obj.getClass());
		
		if (colName != null) 
		   criteria.add(Restrictions.eq(colName, colVal));
		
		if (orderBy!=null) {
		   for (String ob: orderBy.split(","))
			   if (ob.toLowerCase().endsWith(" desc") )
				   criteria.addOrder(Order.desc(ob.split(" desc")[0]) );
			   else
		         criteria.addOrder(Order.asc(ob));
		}	
		return criteria.list();
	}

	@Override
	public List<T> getByCol(Class<T> clazz, String colName,
			Serializable colVal, String orderBy) throws HibernateException {
		Session session = null;
		List <T> result = null;
		try {
			session = sessionFactory.getCurrentSession();
			logger.debug("Getting " + clazz + " by col ... ");

			Criteria criteria = session.createCriteria(clazz);
			
			if (colName != null) // if no colName specified, retrieve all records
			   criteria.add(Restrictions.eq(colName, colVal));
			
			if (orderBy!=null) {
			   for (String ob: orderBy.split(","))
				   if (ob.toLowerCase().endsWith(" desc") )
					   criteria.addOrder(Order.desc(ob.split(" desc")[0]) );
				   else
			         criteria.addOrder(Order.asc(ob));
			}	
			result = criteria.list();
		} catch (HibernateException e) {
			throw e;
		} 

		return result;
	}

	@Override
	public List<T> getByCols(T obj, String[] colName, Serializable[] colVal,
			String[] orderBy) throws HibernateException {
		
		logger.debug("Getting LookUp " + obj.getClass().getSimpleName()+ " by col ... ");
		Criteria criteria = getCurrentSession().createCriteria(obj.getClass());
		if (colName != null)
			for (int i = 0; i < colName.length; i++)
				criteria.add(Restrictions.eq(colName[i], colVal[i]));
		if (orderBy != null)
			for (String item : orderBy) {
				if (item.toLowerCase().endsWith(" desc"))
					criteria.addOrder(Order.desc(item.split(" ")[0]));
				else
					criteria.addOrder(Order.asc(item));
			}
		return criteria.list();
	}

	@Override
	public List<T> getByCols(Class<T> clazz, String[] colName,
			Serializable[] colVal, String[] orderBy) throws HibernateException {
		Session session = null;
		List <T> result = null;

		try {
			session = sessionFactory.getCurrentSession();
			logger.debug("Getting LookUp " + clazz + " by col ... ");

			Criteria criteria = session.createCriteria(clazz);
			
			if (colName != null) // if no colName specified, retrieve all records
			   for (int i=0; i < colName.length; i++)
			      criteria.add(Restrictions.eq(colName[i], colVal[i]));
			
			if (orderBy!=null)
				for (String item : orderBy) {
					if (item.toLowerCase().endsWith(" desc") )				
						criteria.addOrder(Order.desc(item.split(" ")[0]));
					else		
				       criteria.addOrder(Order.asc(item));
				}    
			
			result = criteria.list();

			logger.debug(clazz + " retreived successfully");

		} catch (HibernateException e) {
			throw e;
		} 
		return result;
	}

	@Override
	public T getUniqueByCol(T obj, String colName, Serializable colVal)
			throws HibernateException {
		return getUniqueByCols(obj , new String[] {colName} , new  Serializable[] {colVal});
	}

	@Override
	public T getUniqueByCol(Class<T> clazz, String colName, Serializable colVal)
			throws HibernateException {
		return getUniqueByCols(clazz , new String[] {colName} , new  Serializable[] {colVal});
	}

	@Override
	public T getUniqueByCols(T obj, String[] colName, Serializable[] colVal)
			throws HibernateException {
        Criteria criteria = getCurrentSession().createCriteria(obj.getClass());
        int i=0;
        for (String col: colName)
           criteria.add(Restrictions.eq(col, colVal[i++]));
	      return (T) criteria.uniqueResult();
	}

	@Override
	public T getUniqueByCols(Class<T> theClass, String[] colName,
			Serializable[] colVal) throws HibernateException {
	      Session session = null;
	      T result = null;
	      try {
	         session = sessionFactory.getCurrentSession();
	         logger.debug("Getting LookUp " + theClass + " by col ... ");

	         Criteria criteria = session.createCriteria(theClass);
	         int i=0;
	         for (String col: colName)  
	            criteria.add(Restrictions.eq(col, colVal[i++]));

	         result = (T) criteria.uniqueResult() ;

	         logger.debug(theClass + " retreived successfully");

	      } catch (HibernateException e) {
	         throw e;
	      } 
	      return result;
	}

	@Override
	public Object lockRec(Class<T> clazz, String colName, Serializable colVal)
			throws HibernateException {
		return lockRec(clazz , new String[]{colName}, new Serializable[]{colVal});
	}

	@Override
	public Object lockRec(Class<T> clazz, String[] colName,
			Serializable[] colVal) throws HibernateException {
	      Session session = null;
	      Object result = null;

	      try {
	         session = sessionFactory.getCurrentSession();
	         //session.beginTransaction();
	         logger.debug("Getting LookUp " + clazz + " by col ... ");

	         Criteria criteria = session.createCriteria(clazz);
	         int i=0;
	         for (String col: colName)  
	            criteria.add(Restrictions.eq(col, colVal[i++]));

	         criteria.setLockMode(LockMode.UPGRADE_NOWAIT);
	         result = criteria.uniqueResult() ;

	         //session.getTransaction().commit();
	         logger.debug(clazz + " retreived successfully");

	      } catch (HibernateException e) {
	         //if (session != null) session.getTransaction().rollback();
	         //logger.error("Unable to get " + theClass + "with id (" + id + ")", e);
	         throw e;
	      } finally {
	         //if (session != null) session.close();
	      }

	      return result;
	}

	@Override
	public List<T> getByHql(String hqlstr) throws HibernateException {
		Session session = null;
		List <T> result = null;
		try {
			session = sessionFactory.getCurrentSession();
			logger.debug("Hql: " + hqlstr);

			Query q=session.createQuery(hqlstr);			 			

			result = q.list();
			logger.debug(" hql retreived successfully");

		} catch (HibernateException e) {
			throw e;
		} 
		return result;
	}

	@Override
	public Object getUniqueByHql(String hqlstr) throws HibernateException {
		Session session = null;
		Object result = null;

		try {
			session = sessionFactory.getCurrentSession();
			logger.debug("Hql: " + hqlstr);
			Query q=session.createQuery(hqlstr);			 			

			result = q.uniqueResult();
			logger.debug(" hql retreived successfully");

		} catch (HibernateException e) {
			throw e;
		} 
		return result;
	}

	@Override
	public List<T> getByHql(String hqlstr, List<Serializable> param)
			throws HibernateException {
		Session session = null;
		List <T> result = null;
		try {
			session = sessionFactory.getCurrentSession();
			logger.debug("Hql: " + hqlstr);
			Query q=session.createQuery(hqlstr);
			if (param!=null)
			  for (int i=0; i < param.size(); i++ )
			      q.setParameter(i, param.get(i));

			result = q.list();
			logger.debug(" hql retreived successfully");
		} catch (HibernateException e) {
			   throw e;
		} 
		return result;
	}
	
	@Override
	public <T> List<T> getDtoBySql(final String sql,  List<?> param, Class<T> dtoClass,  Map<String, org.hibernate.type.Type> map) throws HibernateException{
		Query query = this.getCurrentSession().createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(dtoClass));
		for(Map.Entry<String, org.hibernate.type.Type> entry : map.entrySet()){
			if(StringUtils.isNotEmpty(entry.getKey()) && null != entry.getValue()){
				((SQLQuery) query).addScalar(entry.getKey(), entry.getValue());
			}
		}
		addQueryParams(query,param);
		return (List<T>)query.list();
	}
	
	
	

	@Override
	public Object getUniqueByHql(String hqlstr, List<Serializable> param)
			throws HibernateException {
	      Session session = null;
	      Object result = null;
	      try {
	         session = sessionFactory.getCurrentSession();
	         logger.debug("Hql: " + hqlstr);

	         Query q=session.createQuery(hqlstr);
	         if (param!=null)
	           for (int i=0; i < param.size(); i++ )
	               q.setParameter(i, param.get(i));
	         result = q.uniqueResult();
	         logger.debug(" hql retreived successfully");
	      } catch (HibernateException e) {
	            throw e;
	      } 
	      return result;
	}

	@Override
	public int hqlUpdate(String sql) throws HibernateException {
		Session session = null;
		int rowCount=0;
		logger.debug("hql: " +sql);
		try {
			session = sessionFactory.getCurrentSession();	
            Query query = session.createQuery(sql);
            rowCount = query.executeUpdate();
			logger.debug(" hql update successfully. rows affected:" + rowCount);

		} catch (HibernateException e) {
			throw e;
		} 
		return rowCount;
	}

	@Override
	public int hqlUpdate(String sql, List<Serializable> param)
			throws HibernateException {
		Session session = null;
		int rowCount=0;
		logger.debug("hql: " +sql);
		try {
			session = sessionFactory.getCurrentSession();	
            Query query = session.createQuery(sql);
			if (param!=null)
				  for (int i=0; i < param.size(); i++ )
					  query.setParameter(i, param.get(i));            
            rowCount = query.executeUpdate();
			logger.debug(" hql update successfully. rows affected:" + rowCount);
		} catch (HibernateException e) {
			throw e;
		} 
		return rowCount;
	}

	@Override
	public int sqlUpdate(String sql, List<Serializable> param)
			throws HibernateException {
		Session session = null;
		int rowCount=0;
		logger.debug("hql: " +sql);
		try {
			session = sessionFactory.getCurrentSession();	
            Query query = session.createSQLQuery(sql);
			if (param!=null)
				  for (int i=0; i < param.size(); i++ )
					  query.setParameter(i, param.get(i));            
          
          rowCount = query.executeUpdate();
	      logger.debug(" hql update successfully. rows affected:" + rowCount);

		} catch (HibernateException e) {
			   throw e;
		} 
		return rowCount;
	}
	
	@SuppressWarnings("unchecked")
	public <M> List<M> excuteByHql(Class<M> clazz,  String hqlstr) throws HibernateException {
		Session session = null;
		List <M> result = null;
		try {
			session = sessionFactory.getCurrentSession();
			logger.debug("Hql: " + hqlstr);

			Query q=session.createQuery(hqlstr);			 			
			result = (List<M>) q.list();
			logger.debug(" hql retreived successfully");

		} catch (HibernateException e) {
			throw e;
		} 
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <M> List<M> excuteByHql(Class<M> clazz, String hqlstr,List<?> param) throws HibernateException {
		Session session = null;
		List <M> result = null;
		try {
			session = sessionFactory.getCurrentSession();
			logger.debug("Hql: " + hqlstr);

			Query q=session.createQuery(hqlstr);
			addQueryParams(q,param);
			result = (List<M>) q.list();
			logger.debug(" hql retreived successfully");

		} catch (HibernateException e) {
			throw e;
		} 
		return result;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <M> List<M> getDtoByHql(final String hql,  List<?> param, Class<M> dtoClass) throws HibernateException{
		Query query = this.getCurrentSession().createQuery(hql).setResultTransformer(Transformers.aliasToBean(dtoClass));
		addQueryParams(query,param);
		return (List<M>)query.list();
	}

	
	@Override
	public void evict(T object) throws HibernateException {
		getCurrentSession().evict(object);
	}
	@SuppressWarnings("unchecked")
	public boolean deleteById(Class<T> clazz, Serializable id) throws HibernateException{
		Session session = sessionFactory.getCurrentSession();
		logger.debug("delete by ID:"+id);
		return this.delete((T)session.load(clazz, id));
	}
	
	public int deleteByHql(String hql,Object... obj) throws HibernateException{
		Session session = sessionFactory.getCurrentSession();
		logger.debug("Hql: " + hql);
		SQLQuery query = session.createSQLQuery(hql);
		if(null!=obj && obj.length>0){
			for(int i = 0;i<obj.length;i++){
				query.setParameter(i, obj[i]);
			}
		}
		return query.executeUpdate();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> List<T> getDtoBySql(final String sql,  List<?> param, Class<T> dtoClass) throws HibernateException{
		Query query = this.getCurrentSession().createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(dtoClass));
		addQueryParams(query,param);
		return (List<T>)query.list();
	}
	public  Object getUniqueBySQL(String sql, List<Serializable> param){
		Query query = this.getCurrentSession().createSQLQuery(sql);
		addQueryParams(query,param);
		return query.uniqueResult();
	}
	
	@Override
	public int updateBySQL(String sql, List<?> params) throws HibernateException{
		Session session = sessionFactory.getCurrentSession(); 
        Query query = session.createSQLQuery(sql);
        addQueryParams(query,params);
        int rowCount = query.executeUpdate();
        return rowCount;

	}
	
	//handle page number greater than 1 when conditions are changed
	protected void initFirstPage(ListPage<T> page){
		if(page.getNumber()>1 && page.getAllPage() < page.getNumber()){
			page.setNumber(1);
		}
	}
}