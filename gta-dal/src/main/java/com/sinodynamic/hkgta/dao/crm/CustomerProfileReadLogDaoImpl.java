package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerProfileReadLog;

@Repository
public class CustomerProfileReadLogDaoImpl extends GenericDao<CustomerProfileReadLog>implements CustomerProfileReadLogDao {

	@Override
	public boolean saveCustomerProfileReadLog(CustomerProfileReadLog log) {
		// TODO Auto-generated method stub
		return super.saveOrUpdate(log);
	}

	@Override
	public List<CustomerProfileReadLog> getListByReadTime(Date readTime) {
		// TODO Auto-generated method stub
		String sql=" SELECT lo.log_id AS logId,"+
		" lo.customer_id AS customerId,"+
		" lo.read_timestamp AS readTimestamp ,"+
		" lo.read_type AS readType,"+
		" lo.reader_user_id AS readerUserId,"+
		" m.academy_no AS academyId,"+
		" IFNULL(CONCAT(s.given_name,' ',s.surname),lo.reader_user_id) AS readUserName"+
		" FROM customer_profile_read_log AS lo"+
		" LEFT JOIN  member AS m ON lo.customer_id=m.customer_id "+
		" LEFT JOIN staff_profile AS s ON lo.reader_user_id=s.user_id"+
	    " where date_format(lo.read_timestamp,'%Y-%m-%d') =DATE_FORMAT(?,'%Y-%m-%d') "+
	    " ORDER BY lo.read_timestamp ASC ";
		
		List<Serializable>param=new ArrayList<>();
		param.add(readTime);
		return super.getDtoBySql(sql, param, CustomerProfileReadLog.class);
	}

}
