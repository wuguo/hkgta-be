package com.sinodynamic.hkgta.dao.memberapp;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.rpos.CandidateCustomer;

public interface CandidateCustomerDao extends IBaseDao<CandidateCustomer>{

}
