package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.UserDevice;

public interface UserDeviceDao extends IBaseDao<UserDevice>{

	int deleteUserDevice(String userId,String arnApplication);
	
	UserDevice getUserDeviceByApplication(String userId, String arnApplication);
}
