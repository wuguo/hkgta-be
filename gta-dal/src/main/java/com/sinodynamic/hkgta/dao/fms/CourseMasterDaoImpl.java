package com.sinodynamic.hkgta.dao.fms;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.CourseListDto;
import com.sinodynamic.hkgta.dto.fms.MonthlyCourseReportDto;
import com.sinodynamic.hkgta.dto.fms.MonthlyCourseRevenueReportDto;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class CourseMasterDaoImpl extends GenericDao<CourseMaster> implements
        CourseMasterDao {

    @Override
    public CourseListDto getCourseInfo(String courseId) {

        List<Serializable> listParam = new ArrayList<Serializable>();

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT cm.course_id AS courseId, cm.course_name AS courseName, cm.regist_begin_date AS registBeginDate, cm.poster_filename AS posterFilename, "
                + "cm.age_range_code AS ageRangeCode, cm.course_description AS courseDescription, cm.create_date AS createDate, "
                + "cm.regist_due_date AS registDueDate, cm.status, cm.capacity, cm.member_acceptance as memberAcceptance, "
                + "cm.update_by cancelBy, cm.open_enroll openEnroll, cm.no_of_session noOfSession,"
                + "cm.internal_remark cancelRemark,"
                + "(SELECT COUNT(*) FROM course_session cs WHERE cs.status <> 'DEL' and cs.course_id = cm.course_id) AS sessionCount, "
                + "(SELECT COUNT(*) FROM course_enrollment ce WHERE ce.course_id = cm.course_id AND (ce.status ='REG' or ce.status ='ACT')) AS enrollment, "
                + "(SELECT item_price FROM pos_service_item_price pos WHERE pos.item_no = cm.pos_item_no) AS price, "
                + "(SELECT MIN(cs.begin_datetime) FROM course_session cs WHERE cs.course_id = cm.course_id and cs.status = 'ACT' ) AS periodStart, "
                + "(SELECT MAX(cs.end_datetime) FROM course_session cs WHERE cs.course_id = cm.course_id and cs.status = 'ACT' ) AS periodEnd FROM course_master cm WHERE ");

        if (CommUtil.notEmpty(courseId)) {
            sql.append("cm.course_id = ? ");
            listParam.add(new Integer(courseId));
        }


        List<CourseListDto> list = getDtoBySql(sql.toString(), listParam, CourseListDto.class);

        return list.size() > 0 ? list.get(0) : null;

    }

    @Override
    public String getCommonQueryByCourseType(String courseType, String status,
                                             String expired, String customerId) {
        StringBuilder sql = new StringBuilder();
        String sqlPart = null;
        if (StringUtils.isEmpty(customerId)) {
            sqlPart = "\" \" as enrollStatus";
        } else {
            sqlPart = "(select en.status from course_enrollment en where en.course_id = cm.course_id and en.customer_id = " + customerId + ") as enrollStatus";
        }

        sql.append("SELECT * FROM (SELECT cm.course_id as courseId, cm.course_name as courseName, cm.regist_begin_date as registBeginDate, cm.course_description as courseDescription, "
                + "cm.regist_due_date as registDueDate, cm.status, cm.capacity, cm.create_date as createDate, cm.member_acceptance as memberAcceptance, cm.open_enroll AS openEnroll, cm.no_of_session AS noOfSession,"
                + " (case cm.member_acceptance when 'A' then 'Auto' when 'M' then 'Manual' end) as memberAcceptanceValue, cm.poster_filename as posterFilename, "
                + "(SELECT COUNT(*) FROM course_enrollment ce "
                + "WHERE ce.course_id = cm.course_id AND (ce.status ='REG' or ce.status ='ACT')) AS enrollment, "
                + "(select min(cs.begin_datetime) from course_session cs where cs.course_id=cm.course_id AND cs.status='ACT' ) as periodStart, "
                + "(select max(cs.end_datetime) from course_session cs where cs.course_id=cm.course_id AND cs.status='ACT' ) as periodEnd, "
                + sqlPart
                + " FROM course_master cm ");
       
        //Registration not yet opened
        if(CommUtil.notEmpty(courseType) && "RNO".equalsIgnoreCase(status)) {
        	sql.append("WHERE cm.course_type = '" + courseType + "' AND status = 'OPN' and regist_begin_date>CURDATE()) t ");
        }else if (CommUtil.notEmpty(courseType) && !"ALL".equalsIgnoreCase(status) && !"NC".equalsIgnoreCase(status)) {
        	if("OPN".equalsIgnoreCase(status)){
        		sql.append("WHERE cm.course_type = '" + courseType + "' AND status = '" + status + "' and regist_begin_date<=CURDATE()) t ");
        	}else
               sql.append("WHERE cm.course_type = '" + courseType + "' AND status = '" + status + "') t ");
        	
        } else if (CommUtil.notEmpty(courseType) && "ALL".equalsIgnoreCase(status)) {
            sql.append("WHERE cm.course_type = '" + courseType + "') t ");
        } else if (CommUtil.notEmpty(courseType) && "NC".equalsIgnoreCase(status)) {        	
        	sql.append("WHERE cm.course_type = '" + courseType + "' AND status in ('FUL','OPN')) t ");
        }

        if (StringUtils.isNotEmpty(expired)) {
            if (expired.equalsIgnoreCase("N"))
                sql.append("WHERE (ifnull(periodEnd,CURRENT_DATE()) >= current_date() AND status not like 'CAN') ");
            else if (expired.equalsIgnoreCase("Y"))
                sql.append("WHERE (ifnull(periodEnd,CURRENT_DATE()) < current_date() OR status like 'CAN') ");
        }

        return sql.toString();

    }


    @Override
    public String getCommonQueryByCourseTypeAndroid(String courseType, String status,
                                                    String expired, String customerId) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM (SELECT distinct cm.course_id as courseId, cm.course_name as courseName, cm.regist_begin_date as registBeginDate, cm.course_description as courseDescription, "
                + "cm.regist_due_date as registDueDate, cm.status as status, cm.capacity, cm.create_date as createDate, cm.member_acceptance as memberAcceptance, cm.poster_filename as posterFilename, "
                + "(select min(cs.begin_datetime) from course_session cs where cs.course_id=cm.course_id AND cs.status='ACT' ) as periodStart, "
                + "(select max(cs.end_datetime) from course_session cs where cs.course_id=cm.course_id AND cs.status='ACT' ) as periodEnd, psip.item_price as price, ce.status AS enrollStatus"
                + " FROM course_master cm"
                + " LEFT JOIN pos_service_item_price psip on (psip.item_no = cm.pos_item_no)"
                + " LEFT JOIN course_enrollment ce ON (cm.course_id = ce.course_id and ce.customer_id = " + customerId + ") AND "
                + "ce.enroll_date = (select max(subce.enroll_date) from course_enrollment subce where (cm.course_id = subce.course_id and subce.customer_id = " + customerId + "))"
                + " LEFT JOIN course_session cs on (cm.course_id = cs.course_id) ");

        if (CommUtil.notEmpty(courseType)) {

            if (("FUL".equalsIgnoreCase(status) || "OPN".equalsIgnoreCase(status))) {
                sql.append("WHERE cm.course_type = '" + courseType + "' AND cm.status = '" + status + "' and ce.status is null and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            } else if ("ALL".equalsIgnoreCase(status)) {
                sql.append("WHERE cm.course_type = '" + courseType + "' AND (ce.status is not null or cm.status in ('FUL','OPN')) and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            } else if ("REG".equalsIgnoreCase(status)) {//针对bug(SGG-3646)把cm.open_enroll <> 'Y'改成cm.open_enroll <> 'N'
            	 sql.append("WHERE cm.course_type = '" + courseType + "' AND ce.status='REG' and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
                //ACT: Active Course, Opposite to REG
            } else if ("ACT".equalsIgnoreCase(status)) {
                sql.append("WHERE cm.course_type = '" + courseType + "' AND cm.status in ('FUL','OPN') and ce.status is null and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            } else if ("PND".equalsIgnoreCase(status)) {
                sql.append("WHERE cm.course_type = '" + courseType + "' AND cm.status in ('FUL','OPN') and (ce.status = 'ACT' or ce.status ='PND') and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            } else {
                sql.append("WHERE cm.course_type = '" + courseType + "' AND ce.status = '" + status + "' and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            }

        } else {

            if (("FUL".equalsIgnoreCase(status) || "OPN".equalsIgnoreCase(status))) {
                sql.append("WHERE cm.status = '" + status + "' and ce.status is null and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            } else if ("ALL".equalsIgnoreCase(status)) {
                sql.append("WHERE (ce.status is not null or cm.status in ('FUL','OPN') ) and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            } else if ("REG".equalsIgnoreCase(status)) {
                sql.append("WHERE ce.status is not null and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
                //ACT: Active Course, Opposite to REG
            } else if ("ACT".equalsIgnoreCase(status)) {
                sql.append("WHERE cm.status in ('FUL','OPN') and ce.status is null and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            } else if ("PND".equalsIgnoreCase(status)) {
                sql.append("WHERE cm.status in ('FUL','OPN') and (ce.status = 'ACT' or ce.status ='PND') and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            } else {
                sql.append("WHERE ce.status = '" + status + "' and (cs.status <> 'DEL' or cs.status is null) AND cm.open_enroll <> 'N' ) t ");
            }
        }

        if (StringUtils.isNotEmpty(expired)) {
            if (expired.equalsIgnoreCase("N"))
                sql.append("WHERE (ifnull(t.periodEnd,current_date()) >= current_date() AND t.status <> 'CAN') ");
            else if (expired.equalsIgnoreCase("Y"))
                sql.append("WHERE (ifnull(t.periodEnd,current_date()) < current_date() OR t.status = 'CAN') ");
        }

        return sql.toString();

    }


    @Override
    public Date getCourseSessionStartDate(Long courseId) throws Exception {

        List<Serializable> param = new ArrayList<Serializable>();
        String sql = "select min(begin_datetime) from course_session where status = 'ACT' and course_id = ?";
        param.add(courseId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Object resultObject = getUniqueBySQL(sql, param);
        if (resultObject == null) {
            return null;
        }
        return format.parse(resultObject.toString());

    }


    @Override
    public Date getCourseSessionEndDate(Integer courseId) throws Exception {
        List<Serializable> param = new ArrayList<Serializable>();
        String sql = "select max(end_datetime) from course_session where course_id = ?";
        param.add(courseId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Object resultObject = getUniqueBySQL(sql, param);
        if (resultObject == null) {
            return null;
        }
        return format.parse(resultObject.toString());

    }

    @Override
    public CourseMaster getCourseMaseterByTypeAndName(String type, String name) {

        if (StringUtils.isEmpty(type) || StringUtils.isEmpty(name)) return null;

//	    String hql = "from CourseMaster where courseName = '" + name + "' and courseType = '" + type + "'";
//	    List<CourseMaster> data = getByHql(hql);

        String hql = "from CourseMaster where courseName = ? and courseType = ?";
        List<Serializable> params = new ArrayList<Serializable>();
        params.add(name);
        params.add(type);
        List<CourseMaster> data = getByHql(hql, params);

        return (data == null || data.size() == 0 ? null : data.get(0));
    }

    @Override
    public ListPage<CourseMaster> getMonthlyCourseCompletionReport(ListPage<CourseMaster> page, Integer closedYear, Integer closedMonth, String courseType) {
        StringBuilder sql = new StringBuilder();
        sql.append(" select * from (select cm.course_id as courseId, course_type as courseType, course_name as courseName, ");
        sql.append("min(cs.begin_datetime) as periodStart,");
        sql.append("max(cs.end_datetime) as periodEnd,");
        sql.append("(select count(*) from course_session cs  where cs.course_id=cm.course_id AND cs.status='ACT') as sessionCnt,");
        sql.append("(select count(distinct coach_user_id) from course_session cs where cs.course_id=cm.course_id AND cs.status='ACT') as tutorCnt,");
        sql.append("(SELECT item_price FROM pos_service_item_price pos WHERE pos.item_no = cm.pos_item_no) AS price,");
        sql.append(" COUNT(ce.enroll_id) AS studentCnt,");
        sql.append("round(IFNULL(SUM(cot.paid_amount), 0)/1000, 2) as totalRevenue,");
        sql.append("cm.member_acceptance as memberAcceptance, cm.update_date as closeDate,");
        sql.append(" if(p.given_name is null, cm.update_by, CONCAT_WS(' ',p.given_name,p.surname)) as closeBy");

        sql.append(" from course_master cm Inner join  course_session cs on cs.course_id=cm.course_id AND cs.status='ACT' ");
        sql.append(" LEFT JOIN staff_profile p on p.user_id = cm.update_by");
        sql.append(" LEFT JOIN course_enrollment ce on  ce.course_id = cm.course_id AND ce.status ='REG'");
        sql.append(" left join customer_order_hd coh  on ce.cust_order_no = coh.order_no ");
        sql.append(" left join customer_order_trans cot on coh.order_no = cot.order_no "); // " and cot.status = 'SUC' "
        sql.append(" where cm.status = 'CLD' ");

        List<Serializable> param = new ArrayList<>();
        if (StringUtils.isNotBlank(courseType)) {
            sql.append(" and cm.course_type = ? ");
            param.add(courseType);
        }

        sql.append(" GROUP BY cm.course_id ) t");

        if (closedMonth != null && closedYear != null) {
            sql.append(" where Year(periodEnd) = ?  and Month(periodEnd) = ? ");
            param.add(closedYear);
            param.add(closedMonth);
        }

        String countSql = " SELECT count(*) from ( " + sql + ") x";
        return listBySqlDto(page, countSql, sql.toString(), param, new MonthlyCourseReportDto());
    }


    @Override
    public ListPage<?> getMonthlyCourseRevenueReport(ListPage page, Integer year, Integer month, String facilityType) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT trans.transaction_timestamp AS transactionDate, ");
        sql.append("trans.transaction_no AS transactionId,");
        sql.append(" enrollment.enroll_id as reservationId,");
        sql.append("enrollment.enroll_date as reservationDate,");
        sql.append("m.academy_no AS academyId,");
        sql.append("course.course_id as courseId, course.course_name as courseName,");
        sql.append("course_type as courseType,");
        sql.append("CONCAT_WS(' ', cp.given_name, cp.surname) AS patronName,");
        sql.append("hd.order_total_amount as orderTotalAmount,");
        sql.append(" CASE trans.payment_method_code");
        sql.append(" when '"+PaymentMethod.VISA.name()+"' then '"+PaymentMethod.VISA.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.MASTER.name()+"' then '"+PaymentMethod.MASTER.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.CASH.name()+"' then '"+PaymentMethod.CASH.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.CASHVALUE.name()+"' then '"+PaymentMethod.CASHVALUE.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.CHRGPOST.name()+"' then '"+PaymentMethod.CHRGPOST.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.BT.name()+"' then '"+PaymentMethod.BT.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.CHEQUE.name()+"' then '"+PaymentMethod.CHEQUE.getDesc()+"'");
        sql.append(" when 'cheque' then '"+PaymentMethod.CHEQUE.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.VAC.name()+"' then '"+PaymentMethod.VAC.getDesc()+"' ");
        sql.append(" when '"+PaymentMethod.CUP.name()+"' then '"+PaymentMethod.CUP.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.AMEX.name()+"' then '"+PaymentMethod.AMEX.getDesc()+"'");
        sql.append(" when '"+PaymentMethod.OTH.name()+"' then 'N/A' ");
        sql.append(" END as paymentMethodCode, ");
        
        //add paymentMethod  CUP /AMEX 

        sql.append(" CASE trans.payment_media WHEN 'OTH' THEN 'N/A' WHEN 'OP' THEN 'Online Payment'");
        sql.append(" WHEN 'ECR' THEN 'ECR Terminal'  WHEN 'FTF' THEN 'N/A' WHEN '' THEN 'N/A' ELSE 'N/A' END as paymentMedia,");
        sql.append(" trans.payment_location_code as location, trans.status as status, 1 as qty, trans.paid_amount as transAmount");
        sql.append(" FROM customer_order_trans trans, customer_order_hd hd, member m, customer_profile cp, customer_order_det det,");
        sql.append(" pos_service_item_price pricePos, course_enrollment enrollment, course_master course");
        sql.append(" WHERE enrollment.course_id = course.course_id AND enrollment.status ='REG' AND enrollment.cust_order_no = hd.order_no");
        sql.append(" AND hd.order_no = trans.order_no AND enrollment.customer_id = m.customer_id AND m.customer_id = hd.customer_id");
        sql.append(" AND cp.customer_id = m.customer_id AND det.order_no = hd.order_no AND pricePos.item_no = det.item_no");
        sql.append(" AND trans.status = 'SUC' ");

        List<Serializable> param = new ArrayList<>();
        if (StringUtils.isNotBlank(facilityType)) {
            sql.append(" and course.course_type = ? ");
            param.add(facilityType);
        }

        if (year != null && month != null) {
            sql.append(" and Year(trans.transaction_timestamp) = ?  and Month(trans.transaction_timestamp) = ? ");
            param.add(year);
            param.add(month);
        }

        String countSql = " SELECT count(*) from ( " + sql + ") x";
        return listBySqlDto(page, countSql, sql.toString(), param, new MonthlyCourseRevenueReportDto());
    }
}
