package com.sinodynamic.hkgta.dao.pms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.StaffRosterWeeklyPreset;

public interface StaffRosterWeeklyPresetDao  extends IBaseDao<StaffRosterWeeklyPreset> {

	public List<StaffRosterWeeklyPreset> getStaffRosterWeeklyPreset(String presetName);
	
	public int delete(String presetName); 
	
	public List<StaffRosterWeeklyPreset> searchByPresetName(String presetName);
}
