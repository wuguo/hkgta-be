package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.staff.NotificationDto;
import com.sinodynamic.hkgta.dto.staff.NotificationMemberDto;

@SuppressWarnings("rawtypes")
public interface NotificationCenterDao extends IBaseDao{

	public List<NotificationMemberDto> getNotificationMemberList(NotificationDto notificationDto) throws Exception;
	
	public void saveSMSRecord(NotificationDto notificationDto) throws Exception;

	//added by Kaster 20160407
	public List<NotificationMemberDto> getNotificationMembers(NotificationDto notificationDto);
	
	//added by Kaster 20160513
	public List<NotificationMemberDto> getRecipients(NotificationDto notificationDto);

}
