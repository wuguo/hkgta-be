package com.sinodynamic.hkgta.dao.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.CourseSessionFacilityDto;
import com.sinodynamic.hkgta.entity.fms.CourseSessionFacility;
public interface CourseSessionFacilityDao extends IBaseDao<CourseSessionFacility>{

    public CourseSessionFacility getCourseSessionFacility(long facilityTimeslotId);
    
    public Serializable addCourseSessionFacility(CourseSessionFacility courseSessionFacility);
    
    public List<CourseSessionFacility> getCourseSessionFacilityList(Long courseSessionId);
    
    public Boolean deleteCourseSessionFacilityBySessionId(Integer courseSessionId);
    
    public List<CourseSessionFacility> selectCourseSessionFacilityBySessionId(Long courseSessionId);
    
    public boolean deleteCourseSessionFacility(CourseSessionFacility courseSessionFacility);
    
    public List<CourseSessionFacilityDto> getCourseSessionFacilityBySessionId(Integer courseSessionId);

}
