package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.PresentationBatch;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface PresentationDao extends IBaseDao<PresentationBatch> {
	
	public PresentationBatch getPresentation(PresentationBatch t) throws Exception;
	
	public Serializable savePresentation(PresentationBatch t) throws Exception;

	public void updatePresentation(PresentationBatch t) throws Exception;
		
	public PresentationBatch getById(Long id);
	
	public ListPage<PresentationBatch> getPresentationBatchList(ListPage<PresentationBatch> pListPage, String sqlState, String sqlCount, List<Serializable> param) throws HibernateException;
	

	/**
	 * @author Junfeng_Yan
	 * delete PresentationBatch by PresentationBatch ID;
	 * @param t
	 * @throws Exception
	 */
	public boolean deletePresentation(PresentationBatch t) throws HibernateException;
	
	/**
	 * @author Junfeng_Yan
	 * delete PresentMaterialSeq By presentationBatchId;
	 * @param hql
	 * @param presentationBatchId
	 * @return
	 */
	public int deletePresentMaterialSeq(Long presentationBatchId)  throws HibernateException;
	
	public boolean findPresentationByName(String presentationName)
			throws HibernateException;

}
