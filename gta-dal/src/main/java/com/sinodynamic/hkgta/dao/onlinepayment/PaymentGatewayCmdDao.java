package com.sinodynamic.hkgta.dao.onlinepayment;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.onlinepayment.PaymentGatewayCmd;

public interface PaymentGatewayCmdDao extends IBaseDao<PaymentGatewayCmd> {

}
