package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.HkArea;
import com.sinodynamic.hkgta.entity.crm.HkDistrict;

@SuppressWarnings("rawtypes")
@Repository
public class HKAreaDistrictDaoImpl extends GenericDao implements HKAreaDistrictDao {

	public List<HkArea> getHKArea(){
		return getByHql(" from HkArea h order by h.areaCode, h.areaName ");
	}
	
	public List<HkDistrict> getHKDistrict(){
		//update by vicky wang on 2015-10-29 to change the sort order.
		return getByHql(" from HkDistrict h order by h.district, h.hkArea.areaCode ");
//		return getByHql(" from HkDistrict h order by h.hkArea.areaCode, h.district ");
	}
}
