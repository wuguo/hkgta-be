package com.sinodynamic.hkgta.dao.fms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationPos;

public interface FacilityUtilizationPosDao extends IBaseDao<FacilityUtilizationPos>{

	List<FacilityUtilizationPos> getFacilityUtilizationPosList(String facilityType);
	
	int getMaxFacilityUtilizationPosId();
	
	FacilityUtilizationPos getFacilityUtilizationPos(String facilityType,String ageRangeCode,String rateType);
}
