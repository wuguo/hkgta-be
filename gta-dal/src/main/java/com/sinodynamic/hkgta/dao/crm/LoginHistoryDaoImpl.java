package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.LoginHistory;

@Repository
public class LoginHistoryDaoImpl extends GenericDao<LoginHistory> implements LoginHistoryDao{

	public LoginHistory getByUserId(String userId)
	{
		String hql = "FROM LoginHistory l where l.userId = ?";
		List<Serializable> param = new ArrayList<Serializable>();
		Object loginHistory = getUniqueByHql(hql, param);
		
		if (null !=loginHistory)
		{
			return (LoginHistory)loginHistory;
		}
		
		return null;
	} 

}
