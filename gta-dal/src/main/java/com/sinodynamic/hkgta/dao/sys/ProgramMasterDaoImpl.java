package com.sinodynamic.hkgta.dao.sys;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ProgramMaster;
@Repository
public class ProgramMasterDaoImpl extends GenericDao<ProgramMaster> implements ProgramMasterDao {

}
