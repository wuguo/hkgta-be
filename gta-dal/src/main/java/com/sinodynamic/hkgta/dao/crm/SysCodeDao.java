package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.SysCode;

public interface SysCodeDao extends IBaseDao<SysCode> {
	
	public List<SysCode> selectSysCodeByCategory(String category);
	
	public List<SysCode> getSysCodeByCategory(String category);
	
	public List<SysCode> getSysCodeByMutiCategory(String category);
	
	public List<SysCode> selectAllSysCodeCategory() throws Exception;
	
	public SysCode getByCategoryAndCodeValue(String category,String codeValue);
	
	public List<SysCode> selectSysCodeByCategoryWithoutEmptyCodeValue(String category);
	
}
