package com.sinodynamic.hkgta.dao.mms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.mms.MmsOrderItemDto;
import com.sinodynamic.hkgta.dto.mms.SpaCancelledNotificationInfoDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.util.constant.LoggerType;

@Repository
public class SpaAppointmentRecDaoImpl extends GenericDao<SpaAppointmentRec>
		implements SpaAppointmentRecDao {
	private Logger mmsLog = Logger.getLogger(LoggerType.MMS.getName());
	public Serializable saveSpaAppointmenRec(SpaAppointmentRec sapAppointmenRec){

		return save(sapAppointmenRec);
	}

	public SpaAppointmentRec getBySysId(Long sysId){
		
		return null;
	}

	@Override
	public List<SpaAppointmentRec> getSpaAppointmentRecByExtinvoiceNo(String extInvoiceNo) {
	    
	    String hql = "from SpaAppointmentRec where extInvoiceNo = ? order by sysId";
	    List<Serializable> params =  new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    return getByHql(hql, params);
	}

	@Override
	public int deleteByExtInvoiceNo(String extInvoiceNo) {
	    
	    String hql = "delete from spa_appointment_rec where ext_invoice_no = ?";
	    List<Serializable> params =  new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    return deleteByHql(hql, params);
	}

	@Override
	public void updateSpaAppointmentRec(SpaAppointmentRec spaAppointmentRec) {
	    update(spaAppointmentRec);
	}

	@Override
	public List<MmsOrderItemDto> getAppointmentItemsByExtinvoiceNo(String extInvoiceNo) {
	    
	    String sql = "select   right(sar.ext_appointment_id,6) as appointmentId,sar.ext_appointment_id as appointmentWholeId, sar.ext_employee_code as therapistCode,"
	    	+ "sar.ext_service_code as itemId, "
	    	+ "sar.service_name as service, "
	    	+ "concat(sar.ext_employee_first_name, ' ', sar.ext_employee_last_name) as therapist, "
	    	+ "cod.item_total_amout as rate, "
	    	+ "sar.status as status, "
	    	+ "sar.start_datetime as startTime, "
	    	+ "sar.end_datetime as endTime "
	    	+ "from "
	    	+ "spa_appointment_rec sar "
	    	+ "left join customer_order_det cod on sar.order_det_id = cod.order_det_id "
	    	+ "where sar.ext_invoice_no = ? ";
	    
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    
	    return getDtoBySql(sql, params, MmsOrderItemDto.class);
	    
	}

	@Override
	public Date getLastestSpaSyncTime() {
		mmsLog.info(" excute get Max spaSyncTime :select max(extSyncTimestamp) from SpaAppointmentRec where createBy='spa_sync'");
		String hql="select max(extSyncTimestamp) from SpaAppointmentRec where createBy='spa_sync'";
		Date date=null;
		Object ob=getUniqueByHql(hql);
		if(null!=ob){
			Timestamp re=(Timestamp)getUniqueByHql(hql);
			date=new Date(re.getTime());
		}
//		System.out.println(re.getTime());
	    String sql = "select max(ext_sync_timestamp) from spa_appointment_rec where create_by = 'spa_sync'";
	    Date date_Old = (Date) getUniqueBySQL(sql, null);
		mmsLog.info("the maxSpaSyncTime is :"+date);
		mmsLog.info("the maxSpaSyncTime is old code date: :"+date_Old);
	    return date;
	}

	@Override
	public List<SpaAppointmentRec> getAppointmentRecsByAppointmentId(String appointmentId) {
	    
	    String hql = "from SpaAppointmentRec where extAppointmentId = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(appointmentId);
	    return getByHql(hql, params);
	}

	@Override
	public List<SpaAppointmentRec> getAppointmentRecsByInvoiceNo(String extInvoiceNo) {
	    
	    String hql = "from SpaAppointmentRec where extInvoiceNo = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    return getByHql(hql, params);
	}
	@Override
	public List<SpaAppointmentRec> getAppointmentRecsByOrderDetId(Long orderDetId) {
	    
	    String hql = "from SpaAppointmentRec where orderDetId = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(orderDetId);
	    return getByHql(hql, params);
	}

	@Override
	public List<SpaCancelledNotificationInfoDto> getUserForSpaAppointmentBeginNotification() {
	    
	    String sql = "select "
	    	+ "mb.user_id as userId, "
	    	+ "sar.start_datetime as startDatetime, "
	    	+ "sar.end_datetime as endDatetime, "
	    	+ "sar.service_name as serviceName, "
	    	+ "sar.ext_employee_first_name as employFN, "
	    	+ "sar.ext_employee_last_name as employLN "
	    	+ "from spa_appointment_rec sar left join member mb on sar.customer_id = mb.customer_id "
	    	+ "where sar.status = 'RSV' "
	    	+ " and timestampdiff(minute, sar.start_datetime, date_add(current_timestamp(), interval 1 hour)) BETWEEN  0 AND 4 ";
	    
	    return getDtoBySql(sql, null, SpaCancelledNotificationInfoDto.class);
	}

        @Override
        public List<SpaCancelledNotificationInfoDto> getUserForSpaAppointmentEndNotification() {
    
        	String sql = "select "
        	    	+ "mb.user_id as userId, "
        	    	+ "sar.start_datetime as startDatetime, "
        	    	+ "sar.end_datetime as endDatetime, "
        	    	+ "sar.service_name as serviceName, "
        	    	+ "sar.ext_employee_first_name as employFN, "
        	    	+ "sar.ext_employee_last_name as employLN "
        		+ "from spa_appointment_rec sar left join member mb on sar.customer_id = mb.customer_id "
        		+ "where sar.status = 'RSV' and timestampdiff(minute, sar.end_datetime, date_add(current_timestamp(), interval 10 minute)) = 0";
        
        	return getDtoBySql(sql, null, SpaCancelledNotificationInfoDto.class);
        }

		@Override
		public SpaAppointmentRec getSpaAppointmentRecByInvoiceNoAndExAppointmentId(String invcoicNo,
				String extAppointmentId) {
			String hql="from SpaAppointmentRec where extInvoiceNo=? and extAppointmentId=?";
			List<Serializable>param=new ArrayList<>();
			param.add(invcoicNo);
			param.add(extAppointmentId);
			return (SpaAppointmentRec)this.getUniqueByHql(hql, param);
		}
	
}
