package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.CustomerPreEnrollStage;
@Repository
public class CustomerPreEnrollStageDaoImpl extends GenericDao<CustomerPreEnrollStage> implements CustomerPreEnrollStageDao {

	public List<CustomerPreEnrollStage> getByCustomerId(Long customerId){
		String hqlstr = "from CustomerPreEnrollStage where customerId = "+customerId;
		return getByHql(hqlstr);
	}
	
	public void deleteCustomerPreEnrollStage(List<CustomerPreEnrollStage> customerPreEnrollStageList){
		for(CustomerPreEnrollStage cpe : customerPreEnrollStageList){
			delete(cpe);
		}
	}

	@Override
	public List getLatestCommentsActivities(int totalSize) {
		String sql = "SELECT cpes.comment_date update_date,um.nickname nickname,CONCAT_WS(' ',cp.salutation,cp.given_name,cp.surname) username "
				+ "FROM customer_profile cp INNER JOIN customer_pre_enroll_stage cpes INNER JOIN user_master um "
				+ "ON cp.customer_id = cpes.customer_id AND cpes.comment_by = um.user_id ORDER BY cpes.comment_date DESC LIMIT 0,"+totalSize;
		
		return getCurrentSession().createSQLQuery(sql).list();
	}
}
