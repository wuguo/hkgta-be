package com.sinodynamic.hkgta.dao.pms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.pms.RoomPicPathDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.entity.pms.RoomType;

public interface RoomTypeDao extends IBaseDao<RoomType>{

	public List<RoomTypeDto> getRoomListByTypes(String types);

	List<RoomPicPathDto> getRoomPics(String types);
	
	public RoomType getRoomTypeByCode(String roomTypeCode);

	String getRoomTypeList();
	
}
