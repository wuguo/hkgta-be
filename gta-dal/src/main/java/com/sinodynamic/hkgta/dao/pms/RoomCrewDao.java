package com.sinodynamic.hkgta.dao.pms;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.RoomCrew;

public interface RoomCrewDao extends IBaseDao<RoomCrew> {

	public List<RoomCrew> getRoomCrewList(Long roomId);

	public RoomCrew getRoomCrew(Long roomId, String staffUserId, String crewRoleId);
	
	public List<RoomCrew> getRoomCrewList(String staffUserId);
	
	public List<RoomCrew> getRoomCrewListByCrewRoleId(Long roomId, String crewRoleId);
	
	public RoomCrew getRoomCrew(Long roomId,String staffUserId);

}
