package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.MessageTemplateDto;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;

public interface MessageTemplateDao extends IBaseDao<MessageTemplate> {
	
	MessageTemplate getTemplateByFunctionId(String colValue);
	
	// MessageTemplate getTemplateByFunctionId(String colValue);
	
	public boolean saveMessageTemplate(MessageTemplate mt) throws Exception;
	
	public MessageTemplate getMessageTemplateById(Long templateId) throws Exception;
	
	public boolean deleteMessageTemplateById(Long templateId) throws Exception;
	
	public List<MessageTemplate> selectAllMessageTemplate() throws Exception;
	
	public List<String> selectTemplateTypeList();
	
	public List<MessageTemplate> getMessageTemplatesByType(String templateType);
	
	public MessageTemplateDto getTemplateByOrderNoOrCustomerIdAndTemplateType(String templateType, Long orderNo, Long customerId );
	
}
