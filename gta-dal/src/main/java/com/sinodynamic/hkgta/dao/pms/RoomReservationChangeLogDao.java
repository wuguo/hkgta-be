package com.sinodynamic.hkgta.dao.pms;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.pms.RoomReservationChangeLog;

public interface RoomReservationChangeLogDao extends IBaseDao<RoomReservationChangeLog>{

}
