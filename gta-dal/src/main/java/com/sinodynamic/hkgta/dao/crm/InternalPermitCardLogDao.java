package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCard;
import com.sinodynamic.hkgta.entity.crm.InternalPermitCardLog;

public interface InternalPermitCardLogDao extends IBaseDao<InternalPermitCardLog> {

    public void createInternalPermitCardLog(InternalPermitCard newCard, String oldStatus) throws Exception;
		
}
