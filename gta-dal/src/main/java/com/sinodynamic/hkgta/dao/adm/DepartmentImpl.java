package com.sinodynamic.hkgta.dao.adm;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.DepartmentBranch;

@Repository
public class DepartmentImpl extends GenericDao<DepartmentBranch> implements DepartmentDao{

	@Override
	public List<DepartmentBranch> getAllDepartments() throws HibernateException
	{
		String HQL = "FROM DepartmentBranch d ";
		return getByHql(HQL);
	}


	
}
