package com.sinodynamic.hkgta.dao.crm;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ServicePlanDatePos;
@Repository
public class ServicePlanDatePosDaoImpl extends GenericDao<ServicePlanDatePos> implements ServicePlanDatePosDao{

}
