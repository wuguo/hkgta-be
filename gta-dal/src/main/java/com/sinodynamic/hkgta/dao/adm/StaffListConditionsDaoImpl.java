package com.sinodynamic.hkgta.dao.adm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.pms.RoomType;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;

@Repository("staffListConditions")
public class StaffListConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao
{

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Staff ID", "userId", "java.lang.String", "", 1);
		final AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Staff Name", "staffName", "java.lang.String", "", 2);
//		String sql = " select sc.code_value staffType, sc.code_display staffTypeDisplay  from sys_code sc where sc.category = 'HK-CREW-ROLE' OR sc.category = 'staffType'  ";
//		List<StaffMasterInfoDto> staffType = this.getDtoBySql(sql, null, StaffMasterInfoDto.class);
//		
//		Collection<SysCode> staffTypeList = CollectionUtil.map(staffType, new CallBack<StaffMasterInfoDto, SysCode> (){
//
//			@Override
//			public SysCode execute(StaffMasterInfoDto r, int index) {
//				SysCode sysCode = new SysCode();
//				sysCode.setCodeValue(r.getStaffType());
//				sysCode.setCodeDisplay(r.getStaffTypeDisplay());
//				return sysCode;
//			}
//			
//		});
//		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Job Nature","staffType","java.lang.String",new ArrayList<SysCode>(staffTypeList),3);
		String sql2 = " select pt.position_code  positionTitle , pt.position_name  positionTitleName  from position_title pt  ";
		List<StaffMasterInfoDto> positionType = this.getDtoBySql(sql2, null, StaffMasterInfoDto.class);
		
		Collection<SysCode> positionTypeList = CollectionUtil.map(positionType, new CallBack<StaffMasterInfoDto, SysCode> (){

			@Override
			public SysCode execute(StaffMasterInfoDto r, int index) {
				SysCode sysCode = new SysCode();
				sysCode.setCodeValue(r.getPositionTitle());
				sysCode.setCodeDisplay(r.getPositionTitleName());
				return sysCode;
			}
			
		});
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Position","positionTitle","java.lang.String",new ArrayList<SysCode>(positionTypeList),4);
		final AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Employ Date", "employDate", "java.util.Date", "", 5);
		final AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Quit Date", "quitDate", "java.util.Date", "", 6);
		final List<SysCode> status=new ArrayList<>();
		//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled
		SysCode s3=new SysCode();
		s3.setCategory("status");
		s3.setCodeDisplay("Active");
		s3.setCodeValue("ACT");
		status.add(s3);

		SysCode s4=new SysCode();
		s4.setCategory("status");
		s4.setCodeDisplay("Inactive");
		s4.setCodeValue("NACT");
		status.add(s4);
		final AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Status", "status", "java.lang.String", status, 7);
		//return Arrays.asList(condition1, condition2, condition3, condition4, condition5,condition6,condition7);
		return Arrays.asList(condition1, condition2,  condition4, condition5,condition6,condition7);
	}

	/**
	 * 根据type 创建对应的高级查询条件
	 */
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type)
	{
		if (type.equals("Card")) {//创建Staff Card对应高级查询条件
			final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Staff ID", "userId", "java.lang.String", "", 1);
			final AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Staff Name", "staffName", "java.lang.String", "", 2);
			String sql = " select sc.code_value staffType, sc.code_display staffTypeDisplay  from sys_code sc where sc.category = 'HK-CREW-ROLE' OR sc.category = 'staffType'  ";
			List<StaffMasterInfoDto> staffType = this.getDtoBySql(sql, null, StaffMasterInfoDto.class);
			Collection<SysCode> staffTypeList = CollectionUtil.map(staffType, new CallBack<StaffMasterInfoDto, SysCode> (){
				@Override
				public SysCode execute(StaffMasterInfoDto r, int index) {
					SysCode sysCode = new SysCode();
					sysCode.setCodeValue(r.getStaffType());
					sysCode.setCodeDisplay(r.getStaffTypeDisplay());
					return sysCode;
				}
			});
			AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Job Nature","staffType","java.lang.String",new ArrayList<SysCode>(staffTypeList),3);
			AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Card #", "cardNo", "java.lang.String", "", 4);
			final List<SysCode> status=new ArrayList<>();
			//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled, DPS-Disposal
			SysCode s3=new SysCode();
			s3.setCategory("status");
			s3.setCodeDisplay("issued");
			s3.setCodeValue("ISS");
			status.add(s3);

			SysCode s4=new SysCode();
			s4.setCategory("status");
			s4.setCodeDisplay("cancelled");
			s4.setCodeValue("CAN");
			status.add(s4);
			
			SysCode s5=new SysCode();
			s4.setCategory("status");
			s4.setCodeDisplay("onhand");
			s4.setCodeValue("OH");
			status.add(s5);
			
			SysCode s1=new SysCode();
			s4.setCategory("status");
			s4.setCodeDisplay("return");
			s4.setCodeValue("RTN");
			status.add(s1);
			
			SysCode s2=new SysCode();
			s4.setCategory("status");
			s4.setCodeDisplay("Disposal");
			s4.setCodeValue("DPS");
			status.add(s2);
			final AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Card Status", "status", "java.lang.String", status, 5);
			AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Issue Date", "cardIssueDate", "java.util.Date", "", 6);
			return Arrays.asList(condition1, condition2, condition3, condition4, condition5, condition6);
		}
		return null;
	}
	
	

}
