package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.EnrollmentReportDto;

public interface EnrollmentReportDao extends IBaseDao<EnrollmentReportDto> {

}
