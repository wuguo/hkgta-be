package com.sinodynamic.hkgta.dao.bi;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.bi.BiOasisTranCode;

@Repository
public class BiOasisTranCodeDaoImpl extends GenericDao<BiOasisTranCode>implements BiOasisTranCodeDao {

	@Override
	public List<BiOasisTranCode> getAllBiOasisTranCodeList() {
		 return super.getByHql("from BiOasisTranCode where status='ACT'");
	}
	

}
