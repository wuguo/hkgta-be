
package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.PresentationBatch;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class PresendationDaoImpl extends GenericDao<PresentationBatch> implements PresentationDao {

	public PresentationBatch getPresentation(PresentationBatch t)
			throws Exception {
		return this.get(t, t.getPresentId());
	}

	
	
	public Serializable savePresentation(PresentationBatch t) throws HibernateException {
		
		return super.save(t);
	}
	


	
	public void updatePresentation(PresentationBatch t)
			throws Exception {
		this.update(t);
	}

	@Override
	public boolean deletePresentation(PresentationBatch t) throws HibernateException {
		return super.deleteById(PresentationBatch.class, t.getPresentId());
	}

	@Override
	public int deletePresentMaterialSeq(Long presentationBatchId) {
		String hql = " delete from present_material_seq WHERE present_material_seq.present_id = ? ";
		return super.deleteByHql(hql, presentationBatchId);
	}

	@Override
	public PresentationBatch getById(Long id) {
		// TODO Auto-generated method stub
			return this.get(PresentationBatch.class, id);
	}

		
	public ListPage<PresentationBatch> getPresentationBatchList(ListPage<PresentationBatch> pListPage, String sqlState, String sqlCount, List<Serializable> param) throws HibernateException
	{
		return listByHql(pListPage, sqlCount, sqlState, param);
	}
	


	@Override
	public boolean findPresentationByName(String presentationName)
			throws HibernateException {
		// TODO Auto-generated method stub
		
		PresentationBatch p = super.getUniqueByCol(PresentationBatch.class, "presentName", presentationName);
		
		if(null != p)
			return true; 
		return false;
	}


}

