package com.sinodynamic.hkgta.dao.pms;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepStatusLog;
@Repository
public class RoomHousekeepStatusLogDaoImpl extends GenericDao<RoomHousekeepStatusLog> implements RoomHousekeepStatusLogDao {

}
