package com.sinodynamic.hkgta.dao.pms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.pms.RoomHousekeepTaskQueryDto;
import com.sinodynamic.hkgta.dto.pms.RoomMemberDto;
import com.sinodynamic.hkgta.entity.pms.RoomHousekeepTask;
import com.sinodynamic.hkgta.util.constant.RoomCrewRoleType;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskStatus;

@Repository
public class RoomHousekeepTaskDaoImpl extends GenericDao<RoomHousekeepTask> implements RoomHousekeepTaskDao {

	@Override
	public List<RoomHousekeepTask> getRoomHousekeepTaskList(Long roomId) {
		StringBuffer hql = new StringBuffer(
				"select a from RoomHousekeepTask a  where a.room.roomId =? and a.status != 'CAN'  and a.status != 'CMP' and a.status != 'PND'  and a.status != 'IRJ'   order by case when (startTimestamp is null and jobType in('ADH','SCH')) then startTimestamp else 1 end asc,jobType asc, scheduleDatetime asc");

		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public List<RoomHousekeepTaskQueryDto> getRemindRoomHousekeepTaskList(Integer assistantResTime, Integer inspectorResTime,
			Integer supervisorResTime) {
		StringBuffer hql = new StringBuffer();
		hql.append("select rc.room.roomId as roomId,rc.room.roomNo as roomNo,"
				+ " t.taskId as taskId,t.jobType as jobType,t.status as taskStatus,"
				+ " t.scheduleDatetime as beginDate,t.targetCompleteDatetime as endDate,"
				+ " t.startTimestamp as startTimestamp,t.finishTimestamp as finishTimestamp,t.taskDescription as taskDescription, "
				+ " rc.staffProfile.userId as staffUserId,rc.staffProfile.staffMaster.staffType as crewRoleId "
				+ " from RoomCrew rc,RoomHousekeepTask t"
				+ " where t.room.roomId = rc.room.roomId "
				+ " AND ( (t.status = 'OPN' AND t.jobType='ADH' AND rc.staffProfile.staffMaster.staffType = 'ISP' AND t.startTimestamp is null and t.scheduleDatetime < ? and (t.alertSent is null or t.alertSent = 0) )"
				+ " or (t.status = 'OPN' AND t.jobType='ADH' AND rc.staffProfile.staffMaster.staffType = 'SPV' AND t.startTimestamp is null and t.scheduleDatetime < ? and t.alertSent = 1 )"
				+ " or (t.status = 'OPN' AND t.jobType='ADH' AND rc.staffProfile.staffMaster.staffType = 'MGR' AND t.startTimestamp is null and t.scheduleDatetime < ? and t.alertSent = 2 ) "
				+ " or (t.status = 'OPN' AND t.jobType='SCH' AND rc.staffProfile.staffMaster.staffType = 'ISP' AND t.finishTimestamp is null and DATE(t.targetCompleteDatetime) < DATE(?) and (t.alertSent is null or t.alertSent = 0) )"
				+ " ) ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(getCalendar(new Date(), assistantResTime));
		paramList.add(getCalendar(new Date(), assistantResTime + inspectorResTime));
		paramList.add(getCalendar(new Date(), assistantResTime + inspectorResTime + supervisorResTime));
		paramList.add(new Date());
		return super.getDtoByHql(hql.toString(), paramList, RoomHousekeepTaskQueryDto.class);
	}

	private Date getCalendar(Date date, int subtractMinute) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, -subtractMinute);
		return cal.getTime();
	}

	@Override
	public List<RoomHousekeepTask> getRoomTaskList(Long roomId, String roleType) {
		StringBuffer hql = new StringBuffer();
		hql.append("select a from RoomHousekeepTask a  where a.room.roomId =? ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		if (RoomCrewRoleType.RA.name().equals(roleType)) {
			hql.append(" and ( (a.status ='RTI' AND a.jobType='MTN') ");
			hql.append(" or (a.status in ('OPN','DND') AND a.jobType!='MTN') )");
		} else {
			hql.append("and a.status in ('OPN','DND','RTI')");
		}
		hql.append("order by jobType asc,startTimestamp asc,scheduleDatetime asc");

		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public List<RoomHousekeepTask> getMaintenanceRoomTaskListByRoomId(Integer roomId) {
		StringBuffer hql = new StringBuffer(
				"select a from RoomHousekeepTask a  where a.room.roomId =? and status = 'OPN' and a.jobType='MTN' order by scheduleDatetime asc ");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public List<RoomHousekeepTask> getMyRoomTaskList(Long roomId) {
		StringBuffer hql = new StringBuffer(
				"select a from RoomHousekeepTask a  where a.room.roomId =? and status in ('OPN','DND','RTI') order by jobType asc,startTimestamp asc,scheduleDatetime asc");
		List<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(roomId);
		return super.getByHql(hql.toString(), paramList);
	}

	@Override
	public List<RoomHousekeepTask> getRoomHousekeepTaskUnCmpList(String jobType) {
		String hql = "from RoomHousekeepTask where jobType=? and status not in ( ?,?) ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(jobType);
		param.add(RoomHousekeepTaskStatus.CMP.name());
		param.add(RoomHousekeepTaskStatus.CAN.name());
		return super.getByHql(hql, param);
	}

	@Override
	public int cancelRoomHousekeepTask(Long roomId, String jobType) {
		String hql = "update RoomHousekeepTask a set a.status = ?,a.updateDate = ? where a.room.roomId = ? and a.jobType=? and a.status not in (?,?) ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(RoomHousekeepTaskStatus.CAN.name());
		param.add(new Date());
		param.add(roomId);
		param.add(jobType);
		param.add(RoomHousekeepTaskStatus.CMP.name());
		param.add(RoomHousekeepTaskStatus.CAN.name());
		return super.hqlUpdate(hql, param);
	}

	@Override
	public List<RoomHousekeepTask> getRoomHousekeepTaskUnCmpList(String roomNo, String jobType) {
		String hql = "from RoomHousekeepTask where room.roomNo = ? and jobType=? and status not in ( ?,?) ";
		List<Serializable> param = new ArrayList<Serializable>();
		param.add(roomNo);
		param.add(jobType);
		param.add(RoomHousekeepTaskStatus.CMP.name());
		param.add(RoomHousekeepTaskStatus.CAN.name());
		return super.getByHql(hql, param);
	}

	@Override
	public List<RoomMemberDto> getMyRoomListForApp(String userId, String roleType, String roomNo) {
		StringBuffer hqlRoom = new StringBuffer();
		List<Serializable> paramList = new ArrayList<Serializable>();
		hqlRoom.append("SELECT distinct r.roomId as roomId,r.roomNo as roomNo,r.status as status,r.frontdeskStatus as frontdeskStatus,r.serviceStatus as  serviceStatus   ");
		if (RoomCrewRoleType.NGR.name().equals(roleType)) {
			hqlRoom.append(" from Room r,RoomHousekeepTask a ");
			hqlRoom.append(" where r.roomId = a.room.roomId and a.status in ('OPN','RTI') ");
			hqlRoom.append(" and ((a.jobType ='SCH' and date(a.scheduleDatetime) <= date(?) AND date(?) <= date(a.targetCompleteDatetime)) ");
			hqlRoom.append(" OR (a.jobType ='SCH' and date(?) > date(a.targetCompleteDatetime))  ");
			hqlRoom.append(" OR a.jobType !='SCH') ");
			paramList.add(new Date());
			paramList.add(new Date());
			paramList.add(new Date());
		} else {
			hqlRoom.append(" from Room r ,RoomCrew rc ");
			hqlRoom.append(" where rc.room.roomId = r.roomId and rc.staffProfile.userId =? ");
			paramList.add(userId);
		}
		if (!StringUtils.isEmpty(roomNo)) {
			hqlRoom.append(" and r.roomNo like ? ");
			paramList.add("%" + roomNo + "%");
		}
		hqlRoom.append(" order by r.roomNo ");
		return super.getDtoByHql(hqlRoom.toString(), paramList, RoomMemberDto.class);
	}

	@Override
	public List<RoomHousekeepTaskQueryDto> getMyRoomTaskListForApp(String userId, String roleType, String roomNo) {
		StringBuffer hqlTask = new StringBuffer();
		List<Serializable> paramTaskList = new ArrayList<Serializable>();
		hqlTask.append(" SELECT r.roomNo as roomNo,r.roomId as roomId,a.taskId as taskId,a.status as taskStatus,a.jobType as jobType,a.scheduleDatetime as beginDate,a.targetCompleteDatetime as endDate, ");
		hqlTask.append(" a.startTimestamp as startTimestamp,a.finishTimestamp as finishTimestamp,a.taskDescription as taskDescription,a.remark as remark  ");
		if (RoomCrewRoleType.NGR.name().equals(roleType)) {
			hqlTask.append(" from RoomHousekeepTask a,Room r ");
			hqlTask.append(" where a.room.roomId = r.roomId and a.status in ('OPN','RTI') ");
		} else {
			hqlTask.append(" from RoomHousekeepTask a,Room r ,RoomCrew rc ");
			hqlTask.append(" where a.room.roomId = r.roomId and rc.room.roomId = r.roomId ");
			hqlTask.append(" and rc.staffProfile.userId =? ");
			hqlTask.append(" and a.status in ('OPN','DND','RTI') ");
			paramTaskList.add(userId);
		}
		hqlTask.append(" and ((a.jobType ='SCH' and date(a.scheduleDatetime) <= date(?) AND date(?) <= date(a.targetCompleteDatetime)) ");
		hqlTask.append(" OR (a.jobType ='SCH' and date(?) > date(a.targetCompleteDatetime))  ");
		hqlTask.append(" OR a.jobType !='SCH') ");
		paramTaskList.add(new Date());
		paramTaskList.add(new Date());
		paramTaskList.add(new Date());
		if (!StringUtils.isEmpty(roomNo)) {
			hqlTask.append(" and r.roomNo like ? ");
			paramTaskList.add("%" + roomNo + "%");
		}
		hqlTask.append(" order by r.roomNo,a.jobType asc,a.startTimestamp asc,a.scheduleDatetime asc");
		return super.getDtoByHql(hqlTask.toString(), paramTaskList, RoomHousekeepTaskQueryDto.class);
	}

	@Override
	public List<RoomMemberDto> getRoomListForApp(String userId, String roleType, String roomNo) {
		StringBuffer hqlRoom = new StringBuffer();
		List<Serializable> paramList = new ArrayList<Serializable>();
		hqlRoom.append("SELECT distinct r.roomId as roomId,r.roomNo as roomNo,r.status as status,r.frontdeskStatus as frontdeskStatus ,r.serviceStatus as  serviceStatus  ");
		if (RoomCrewRoleType.NGR.name().equals(roleType)) {
			hqlRoom.append(" from Room r,RoomHousekeepTask a ");
			hqlRoom.append(" where a.room.roomId = r.roomId and a.status = 'OPN' and a.jobType='MTN' ");
			hqlRoom.append(" and ((a.jobType ='SCH' and date(a.scheduleDatetime) <= date(?) AND date(?) <= date(a.targetCompleteDatetime)) ");
			hqlRoom.append(" OR (a.jobType ='SCH' and date(?) > date(a.targetCompleteDatetime))  ");
			hqlRoom.append(" OR a.jobType !='SCH') ");
			paramList.add(new Date());
			paramList.add(new Date());
			paramList.add(new Date());
		} else {
			hqlRoom.append(" from Room r ,RoomCrew rc ");
			hqlRoom.append(" where rc.room.roomId = r.roomId and rc.staffProfile.userId =? ");
			paramList.add(userId);
		}
		if (!StringUtils.isEmpty(roomNo)) {
			hqlRoom.append(" and r.roomNo like ? ");
			paramList.add("%" + roomNo + "%");
		}
		hqlRoom.append(" order by r.roomNo ");
		return super.getDtoByHql(hqlRoom.toString(), paramList, RoomMemberDto.class);
	}

	@Override
	public List<RoomHousekeepTaskQueryDto> getRoomTaskListForApp(String userId, String roleType, String roomNo) {
		StringBuffer hqlTask = new StringBuffer();
		List<Serializable> paramList = new ArrayList<Serializable>();
		hqlTask.append(" SELECT r.roomNo as roomNo,r.roomId as roomId,a.taskId as taskId,a.status as taskStatus,a.jobType as jobType,a.scheduleDatetime as beginDate,a.targetCompleteDatetime as endDate, ");
		hqlTask.append(" a.startTimestamp as startTimestamp,a.finishTimestamp as finishTimestamp,a.taskDescription as taskDescription,a.remark as remark  ");
		if (RoomCrewRoleType.NGR.name().equals(roleType)) {
			hqlTask.append(" from RoomHousekeepTask a,Room r ");
			hqlTask.append(" where a.room.roomId = r.roomId and a.status = 'OPN' and a.jobType='MTN' ");
		} else {
			hqlTask.append(" from RoomHousekeepTask a,Room r ,RoomCrew rc ");
			hqlTask.append(" where a.room.roomId = r.roomId and rc.room.roomId = r.roomId ");
			hqlTask.append(" and rc.staffProfile.userId =? ");
			if (RoomCrewRoleType.RA.name().equals(roleType)) {
				hqlTask.append(" and ( (a.status ='RTI' AND a.jobType='MTN') ");
				hqlTask.append(" or (a.status in ('OPN','DND') AND a.jobType!='MTN') )");
			} else {
				hqlTask.append(" and a.status in ('OPN','DND','RTI')");
			}
			paramList.add(userId);
		}
		hqlTask.append(" and ((a.jobType ='SCH' and date(a.scheduleDatetime) <= date(?) AND date(?) <= date(a.targetCompleteDatetime)) ");
		hqlTask.append(" OR (a.jobType ='SCH' and date(?) > date(a.targetCompleteDatetime))  ");
		hqlTask.append(" OR a.jobType !='SCH') ");
		paramList.add(new Date());
		paramList.add(new Date());
		paramList.add(new Date());
		if (!StringUtils.isEmpty(roomNo)) {
			hqlTask.append(" and r.roomNo like ? ");
			paramList.add("%" + roomNo + "%");
		}
		hqlTask.append(" order by r.roomNo,a.jobType asc,a.startTimestamp asc,a.scheduleDatetime asc");
		return super.getDtoByHql(hqlTask.toString(), paramList, RoomHousekeepTaskQueryDto.class);
	}
}
