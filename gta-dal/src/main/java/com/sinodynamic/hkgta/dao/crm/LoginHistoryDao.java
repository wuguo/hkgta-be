package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.LoginHistory;

/**
 * @author Vian Tang
 * @since May 9 2015
 */
public interface LoginHistoryDao extends IBaseDao<LoginHistory>{
	
	LoginHistory getByUserId(String userId);

	
}
