package com.sinodynamic.hkgta.dao.crm;

import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.account.DDIDto;
import com.sinodynamic.hkgta.entity.crm.DdiTransactionLog;

public interface DDITransactionLogDao extends IBaseDao<DdiTransactionLog>{
	
	public List<DDIDto> queryDDITransactionList(int year, int month);
	/**
	 * a.search have the DDA record in member_payment_acc
	 * b.member_cashvalue_bal_history.recal_balance < 0 
	 * c. where the year month of cutoff_date = :{year(month-1)} 
	 *  23:59:59
	 */
	public List<DDIDto> queryDDIData(String date);
	

}
