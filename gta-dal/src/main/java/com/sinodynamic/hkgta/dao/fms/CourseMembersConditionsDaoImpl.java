package com.sinodynamic.hkgta.dao.fms;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;

@Repository("courseMembersConditions")
public class CourseMembersConditionsDaoImpl extends GenericDao implements
		AdvanceQueryConditionDao {

	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions() {
		AdvanceQueryConditionDto condition0 = new AdvanceQueryConditionDto("Reservation ID", "enrollId", "java.lang.Long", "", 1);
		final AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Patron ID", "memberId", "java.lang.Integer", "", 2);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron Name", "memberName", "java.lang.String", "", 3);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Registration Date","enrollDate","java.util.Date","",4);
		return Arrays.asList(condition0, condition1, condition2, condition3);
	}

	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
