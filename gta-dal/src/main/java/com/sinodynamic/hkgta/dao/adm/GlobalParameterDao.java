package com.sinodynamic.hkgta.dao.adm;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.util.pagination.ListPage;

public interface GlobalParameterDao extends IBaseDao<GlobalParameter>{

	ListPage<GlobalParameter> getGlobalParameterList(ListPage<GlobalParameter> pListPage, String sqlState, String sqlCount, List<Serializable> param);

}
