package com.sinodynamic.hkgta.dao.adm;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.adm.UserRecordActionLog;

@Repository
public class UserRecordActionLogDaoImpl extends GenericDao<UserRecordActionLog>implements UserRecordActionLogDao {

	@Override
	public List<UserRecordActionLog> getUserRecordActionLogByDate(Date actionDate) {
		String hqlstr="from UserRecordActionLog where date_format(actionTimestamp,'%Y-%m-%d') =?";
		List<Serializable>param=new ArrayList<>();
		param.add(actionDate);
		return super.getByHql(hqlstr, param);
	}


}
