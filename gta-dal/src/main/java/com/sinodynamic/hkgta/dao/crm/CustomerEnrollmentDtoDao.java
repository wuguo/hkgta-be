package com.sinodynamic.hkgta.dao.crm;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentDto;

public interface CustomerEnrollmentDtoDao extends IBaseDao<CustomerEnrollmentDto>{

}
