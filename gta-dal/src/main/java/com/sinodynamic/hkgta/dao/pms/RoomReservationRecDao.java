package com.sinodynamic.hkgta.dao.pms;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.type.Type;
import org.springframework.transaction.annotation.Transactional;

import com.sinodynamic.hkgta.dao.IBaseDao;
import com.sinodynamic.hkgta.dto.fms.AdvancedQueryConditionsDto;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.pms.RoomType;

public interface RoomReservationRecDao  extends IBaseDao<RoomReservationRec> {
	List<RoomReservationRec> getAllFailedReservations(int timeout);
	
	public List<RoomReservationRec> getReservationByCustomerID(Long customerid) ;
	
	RoomReservationRec getReservationByConfirmId(String confirmId);
	
	String getGuestroomAdvanceSearch(Map<String, org.hibernate.type.Type> typeMap);
	/**
	 * 根据resvId查询预定好的房间信息
	 * @param resvId
	 * @return RoomReservationRec
	 */
	public RoomReservationRec getReservationByResvId(String resvId);

	/**
	 * 根据roomId查询今天是否预订了该房间
	 * @param customerId
	 * @param roomId
	 * @return
	 */
	public RoomReservationRec todayToSeeIfThereBookingRoom(Long customerId, Long resvId);

	/**
	 * 根据resvId查询预定好的房间信息
	 * @param resvId
	 * @return RoomReservationRec
	 */
	public RoomReservationRec getReservationByResvId(long resvId);

	public List<RoomReservationRec> getSilentCheckInProgress();
	
	/**
	 * 获取当前今天CheckInProgress状态的总数
	 * @return
	 */
	public int getSilentCheckInProgressTotalNum();

	public List<RoomReservationRec> getReservationByOrderNo(Long orderNo);

	/**
	 * 重载方法 【添加aqcdto可控制是否显示canelled】
	 * @param typeMap
	 * @param dto
	 * @return
	 */
	public String getGuestroomAdvanceSearch(Map<String, Type> typeMap, AdvancedQueryConditionsDto dto);

	public boolean checkOccupyByDateAndRoomId(Date date, Long roomId);

	/**
	 * 根据roomId查询今天是否预订了该房间
	 * @param customerId
	 * @param resvId
	 * @return
	 */
	public RoomReservationRec todayToSeeIfThereBookingRoomByCheckinInProgress(Long customerId, Long resvId);

}
