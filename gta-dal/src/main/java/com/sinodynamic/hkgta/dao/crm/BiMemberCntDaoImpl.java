package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageDto;
import com.sinodynamic.hkgta.entity.crm.BiMemberCnt;

@Repository
public class BiMemberCntDaoImpl extends GenericDao<BiMemberCnt> implements BiMemberCntDao {

	private final Logger logger = Logger.getLogger(BiMemberCntDao.class);

	@Override
	public List<AdvertiseImageDto> getByAppTypeDispLoc(String appType, String dispLoc) {
		StringBuffer hql = new StringBuffer("SELECT a.imgId as imgId, a.serverFilename as filepath, a.applicationType as appType, a.displayLocation as dispLoc, a.status as status,"
				+ "  a.displayOrder as dispOrder,a.linkLevel as linkLevel,a.description as description, a.createBy as createBy, a.createDate as createDate, a.updateBy as updateBy, a.updateDate as updateDate"
				+ " from AdvertiseImage a");
		List<Serializable> params = new ArrayList<Serializable>();
		hql.append(" WHERE a.displayLocation = ? AND a.status = 'ACT' ");
		params.add(dispLoc);
		if(!StringUtils.isEmpty(appType)){
			hql.append(" and a.applicationType = ? ");
			params.add(appType);
		}
		hql.append(" order by displayLocation,applicationType,displayOrder,linkLevel desc ");
		return getDtoByHql(hql.toString(), params, AdvertiseImageDto.class);
	}

	@Override
	public Long getMaxDispOrder(String appType, String dispLoc) {
		String hql = "SELECT MAX(a.displayOrder) FROM AdvertiseImage a WHERE a.applicationType = ?  AND a.displayLocation = ?";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(appType);
		params.add(dispLoc);
		Long maxDispOrder = (Long) getUniqueByHql(hql, params);
		if (maxDispOrder == null) {
			maxDispOrder = 0L;
		}
		return maxDispOrder;
	}

	@Override
	public void updateDispOrder(long imgId, int newDispOrder) {
		String hql = "UPDATE AdvertiseImage a SET a.displayOrder = ? WHERE a.imgId = ?";
		List<Serializable> params = new ArrayList<Serializable>();
		params.add(newDispOrder);
		params.add(imgId);
		int updCount = hqlUpdate(hql, params);
		logger.debug("Updating advertise image display order: " + updCount + " record updated.");
	}

	/**
	 * 获取BiMemberCnt
	 * @param countDate
	 * @param period
	 * @param isActivation
	 * @return
	 */
	@Override
	public BiMemberCnt getBiMemberCnt(Date countDate,String period, boolean isActivation)
	{
		try {
			String hql = " FROM BiMemberCnt a WHERE a.countDate = DATE_FORMAT(?,'%Y-%m-%d') AND a.period = ?  and a.mbrStatus = ? ";
			List<Serializable> params = new ArrayList<Serializable>();
			params.add(countDate);
			params.add(period);
			String act = "NACT";
			if (isActivation) {
				act = "ACT";
			}
			params.add(act);
			List<BiMemberCnt> bmc = super.getByHql(hql, params);
			if (null != bmc && bmc.size() > 0)
				return bmc.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int deleteAdvertiseImages(String dispLoc)
	{
		String hql = "delete from advertise_image WHERE (application_Type = 'MAPP' or application_Type = 'MPORT')  AND display_Location = ? and (link_Level =0 or link_Level =1) and status='ACT' ;";
		return super.deleteByHql(hql, dispLoc);
	}

}
