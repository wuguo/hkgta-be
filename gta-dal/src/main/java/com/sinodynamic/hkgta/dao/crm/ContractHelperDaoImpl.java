package com.sinodynamic.hkgta.dao.crm;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.entity.crm.ContractHelper;

@Repository
public class ContractHelperDaoImpl extends GenericDao<ContractHelper> implements
		ContractHelperDao {

	private Logger logger = Logger.getLogger(ContractHelperDaoImpl.class);
	
	@Override
	public Serializable createTempPassProfile(ContractHelper profile)
			throws Exception {
		return super.save(profile);

	}

	@Override
	public void updateProfile(ContractHelper profile) {
		this.update(profile);

	}

	@Override
	public ContractHelper getTempPassProfileDetails(Long contractorId)
			throws Exception {

		ContractHelper profileDetails = null;
		String hql = "from ContractHelper t where t.contractorId = "
				+ contractorId;

		profileDetails = (ContractHelper) getUniqueByHql(hql);
		return profileDetails;

	}

	@Override
	public int getCountByPassType(Long passTypeId) {
		String hql = "SELECT COUNT(*) FROM ContractHelper t WHERE t.helperPassType = "
				+ passTypeId + " and t.status = 'ACT'";
		Long cnt = (Long) getUniqueByHql(hql);
		return cnt.intValue();
	}

	@Override
	public ContractHelper getTempPassProfileDetailsByHKID(String HKId)
			throws Exception {

		ContractHelper profileDetails = null;
		String hql = "from ContractHelper t where t.passportNo = '" + HKId
				+ "'";

		profileDetails = (ContractHelper) getUniqueByHql(hql);
		return profileDetails;

	}

	@Override
	public List<ContractHelper> getTempPassProfilesExpiringToday(LocalDate date) {
		List<ContractHelper> expiringProfiles = null;
		String hql = "from ContractHelper t where t.status='ACT' and t.periodTo <= '" + date + "'";

		logger.info("sql:" + hql);
		expiringProfiles = getByHql(hql);
		logger.info("expired contractHelper size:" + expiringProfiles.size());
		return expiringProfiles;

	}
	

}