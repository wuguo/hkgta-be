package com.sinodynamic.hkgta.dao.fms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.entity.crm.SysCode;

@Repository("helpPassTypeListConditions")
public class HelpTypeConditionsDaoImpl extends GenericDao implements AdvanceQueryConditionDao
{
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions()
	{
	    	//String displayName, String columnName, String columnType, String selectName,Integer displayOrder
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Temporary Pass ID", "tempPassId", "java.lang.Integer", "", 1);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Temporary Pass Name", "tempPassName", "java.lang.String", "", 2);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Start Date", "startDate", "java.util.Date", "", 3);
		AdvanceQueryConditionDto condition4 = new AdvanceQueryConditionDto("Expiry Date", "expiryDate", "java.util.Date", "", 4);
		
		
		final List<SysCode> status=new ArrayList<>();
		SysCode s1=new SysCode();
		s1.setCategory("status");
		s1.setCodeDisplay("Active");
		s1.setCodeValue("Active");
		status.add(s1);

		SysCode s2=new SysCode();
		s2.setCategory("status");
		s2.setCodeDisplay("Inactive");
		s2.setCodeValue("Inactive");
		status.add(s2);
		
		SysCode s3=new SysCode();
		s3.setCategory("status");
		s3.setCodeDisplay("Expired");
		s3.setCodeValue("Expired");
		status.add(s3);		
		
		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto("Status", "statusValue", "java.lang.String", status, 5);
		return Arrays.asList(condition1, condition2, condition3, condition4,condition5);
	}

	
	@Override
	public List<AdvanceQueryConditionDto> assembleQueryConditions(String type) {
	    
	    return null;
	}

}
