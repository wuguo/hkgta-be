package com.sinodynamic.hkgta.dao.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sinodynamic.hkgta.dao.GenericDao;
import com.sinodynamic.hkgta.dto.fms.CustomerFacilityBookingTotal;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.pagination.ListPage;

@Repository
public class CustomerOrderHdDaoImpl  extends GenericDao<CustomerOrderHd> implements CustomerOrderHdDao {
	
	
	
	@Override
	public Serializable addCustomreOrderHd(CustomerOrderHd  customerOrderHd){
		
			return this.save(customerOrderHd);
		
	}
	
	@Override
	public CustomerOrderHd getOrderById(Long orderNo) {
	
		return get(CustomerOrderHd.class, orderNo);
			
	}
	
	@Override
	public CustomerOrderHd getActiveOrderByEnrollId(Long enrollId){
	
	String sql="select * from customer_enroll_po e left join customer_order_hd h on e.order_no = h.order_no where e.enroll_id = "+ enrollId +" and h.order_status = 'ACT' or 'CMP'";
	String sqlCount="select count(*) from customer_enroll_po e left join customer_order_hd h on e.order_no = h.order_no where e.enroll_id = "+ enrollId +" and h.order_status = 'ACT' or 'CMP'";
	ListPage<CustomerOrderHd> page = new ListPage<CustomerOrderHd>();
	page = listBySql(page, sqlCount.toString(), sql.toString(), null);
	 return page.getList().get(0);
	
	}

	@Override
	public BigDecimal getSpendingTotalDuringRange(Long customerId, Date beginDate, Date endDate)
	{
		String sql="select sum(order_total_amount) as total from customer_order_hd where order_status = 'CMP' and customer_id = ? and order_date >= ? and order_date <= ?";
		List<Object> param = new ArrayList<Object>();
		param.add(customerId);
		param.add(beginDate);
		param.add(endDate);
		List<CustomerFacilityBookingTotal> customerFacilityBookingTotalList = this.getDtoBySql(sql, param, CustomerFacilityBookingTotal.class);
		if (customerFacilityBookingTotalList != null && customerFacilityBookingTotalList.size() > 0){
			return customerFacilityBookingTotalList.get(0).getTotal();
		}
		return BigDecimal.ZERO;
	}

	@Override
	public CustomerOrderHd getOrderByExtInvoiceNo(String extInvoiceNo) {
	    
	    String hql = " from CustomerOrderHd where vendorRefCode = ?";
	    List<Serializable> params = new ArrayList<Serializable>();
	    params.add(extInvoiceNo);
	    
	    List<CustomerOrderHd> list = getByHql(hql, params);
	    return (list != null && list.size() > 0 ? list.get(0) : null);
	}
	
	
//	@Override
//	public CustomerOrderHd getOrderByExtInvoiceNo(String extInvoiceNo) {
//	    
//	    String sql = "SELECT * from customer_order_hd AS hd where hd.vendor_reference_code = ?";
//	    List<Serializable> params = new ArrayList<Serializable>();
//	    params.add(extInvoiceNo);
//	    
//	    List<CustomerOrderHd> list = getDtoBySql(sql, params, dtoClass);
//	    return (list != null && list.size() > 0 ? list.get(0) : null);
//	}
}
