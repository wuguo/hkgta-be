package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the customer_service_subscribe database table.
 * 
 */
@Embeddable
public class CustomerServiceSubscribePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="acc_no", insertable=false, updatable=false, unique=true, nullable=false)
	private Long accNo;

	@Column(name="service_plan_no", insertable=false, updatable=false, unique=true, nullable=false)
	private Long servicePlanNo;

	public CustomerServiceSubscribePK() {
	}
	public Long getAccNo() {
		return this.accNo;
	}
	public void setAccNo(Long accNo) {
		this.accNo = accNo;
	}
	public Long getServicePlanNo() {
		return this.servicePlanNo;
	}
	public void setServicePlanNo(Long servicePlanNo) {
		this.servicePlanNo = servicePlanNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CustomerServiceSubscribePK)) {
			return false;
		}
		CustomerServiceSubscribePK castOther = (CustomerServiceSubscribePK)other;
		return 
			(this.accNo.equals(castOther.accNo))
			&& (this.servicePlanNo.equals(castOther.servicePlanNo));
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = (int) (hash * prime + this.accNo);
		hash = (int) (hash * prime + this.servicePlanNo);
		
		return hash;
	}
}