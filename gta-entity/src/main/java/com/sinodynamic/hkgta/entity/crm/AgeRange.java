package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.math.NumberUtils;

/**
 * The persistent class for the age_range database table.
 * 
 */
@Entity
@Table(name="age_range")
@NamedQuery(name="AgeRange.findAll", query="SELECT a FROM AgeRange a")
public class AgeRange implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="age_range_code", unique=true, nullable=false, length=10)
	private String ageRangeCode;

	@Column(name="age_from")
	private Long ageFrom;

	@Column(name="age_to")
	private Long ageTo;

	@Column(length=50)
	private String description;

	public AgeRange() {
	}

	public String getAgeRangeCode() {
		return this.ageRangeCode;
	}

	public void setAgeRangeCode(String ageRangeCode) {
		this.ageRangeCode = ageRangeCode;
	}

	public Long getAgeFrom() {
		return ageFrom;
	}

	public void setAgeFrom(Object ageFrom) {
		/*if (ageFrom instanceof Integer) {
			this.ageFrom = ((Integer)ageFrom).longValue();
		}
		else if (ageFrom instanceof Long) {
			this.ageFrom = ((Long)ageFrom).longValue();
		}
		else if (ageFrom instanceof String) {
			this.ageFrom = Long.valueOf((String)ageFrom);
		}
		else {
			this.ageFrom = (Long)ageFrom;
		}*/
		
		this.ageFrom = (ageFrom!=null ? NumberUtils.toLong(ageFrom.toString()):null);
		
	}

	public Long getAgeTo() {
		return ageTo;
	}

	public void setAgeTo(Object ageTo) {
		/*if (ageTo instanceof Integer) {
			this.ageTo = ((Integer)ageTo).longValue();
		}
		else if (ageTo instanceof Long) {
			this.ageTo = ((Long)ageTo).longValue();
		}
		else if (ageTo instanceof String) {
			this.ageTo = Long.valueOf((String)ageTo);
		}
		else {
			this.ageTo = (Long)ageTo;
		}*/
		this.ageTo = (ageTo!=null ? NumberUtils.toLong(ageTo.toString()):null);
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}