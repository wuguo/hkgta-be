package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for the venue_master database table.
 * 
 */
@Entity
@Table(name = "venue_master")
@NamedQuery(name = "VenueMaster.findAll", query = "SELECT f FROM VenueMaster f")
public class VenueMaster implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "venue_code")
	private String venueCode;

	@Column(name = "venue_name")
	private String venueName;

	@Column(name = "venue_description")
	private String venueDescription;

	@Column(name = "status")
	private String status;
	
	@Column(name="create_by")
	private String createBy;

	@Column(name="vendor_poi_id",length=50)
	private String vendorPoiId;
	
	@Column(name="create_date")
	private Timestamp createDate;
	
	@Column(name="update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public String getVendorPoiId() {
		return vendorPoiId;
	}

	public void setVendorPoiId(String vendorPoiId) {
		this.vendorPoiId = vendorPoiId;
	}

	public VenueMaster() {
	}

	public String getVenueCode() {
		return venueCode;
	}

	public void setVenueCode(String venueCode) {
		this.venueCode = venueCode;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public String getVenueDescription() {
		return venueDescription;
	}

	public void setVenueDescription(String venueDescription) {
		this.venueDescription = venueDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
}