package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the member_facility_book_addition_attr database table.
 * 
 */
@Embeddable
public class MemberFacilityBookAdditionAttrPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="resv_id", nullable=false, length=40)
	private Long resvId;

	@Column(name="attribute_id", nullable=false, length=20)
	private String attributeId;

	public MemberFacilityBookAdditionAttrPK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MemberFacilityBookAdditionAttrPK)) {
			return false;
		}
		MemberFacilityBookAdditionAttrPK castOther = (MemberFacilityBookAdditionAttrPK)other;
		return 
			this.resvId.equals(castOther.resvId)
			&& this.attributeId.equals(castOther.attributeId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.resvId.intValue();
		hash = hash * prime + this.attributeId.hashCode();
		
		return hash;
	}

	public Long getResvId()
	{
		return resvId;
	}

	public void setResvId(Long resvId)
	{
		this.resvId = resvId;
	}

	public String getAttributeId()
	{
		return attributeId;
	}

	public void setAttributeId(String attributeId)
	{
		this.attributeId = attributeId;
	}


}