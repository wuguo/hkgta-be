package com.sinodynamic.hkgta.entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

@Deprecated
public class EntityHelper {
	/**
	 * return empty string if p1 is null
	 * 
	 * @param p1
	 * @return p1 or empty string
	 */
	public static String nvl(String p1) {
		return (p1 == null) ? "" : p1;
	}

	public static Date d = new Date();
	//public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static String defaultformat = "yyyy/MM/dd";

	public static String getYMDFormatDate(Date d) {
		DateFormat df  = new SimpleDateFormat("yyyy-MM-dd");
		if (d == null)
			return null;
		return df.format(d);
	}

	/**
	 * @author Junfeng_Yan
	 * @param d
	 * @param format
	 *            such as "yyyy-MM-dd or yyyy/MM/dd"
	 * @return
	 */
	public static String date2String(Date d, String format) {
		if (null == d)
			return "";
		if (StringUtils.isEmpty(format))
			format = defaultformat;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(d);
	}
}
