package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the batch_send_statement_hd database table.
 * 
 */
@Entity
@Table(name="batch_send_statement_hd")
@NamedQuery(name="BatchSendStatementHd.findAll", query="SELECT b FROM BatchSendStatementHd b")
public class BatchSendStatementHd implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="batch_id")
	private Long batchId;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_date")
	private Date createDate;

	@Column(name="error_count")
	private Long errorCount;

	@Temporal(TemporalType.DATE)
	@Column(name="statement_month")
	private Date statementMonth;

	public BatchSendStatementHd() {
	}

	public Long getBatchId() {
		return this.batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getErrorCount() {
		return this.errorCount;
	}

	public void setErrorCount(Long errorCount) {
		this.errorCount = errorCount;
	}

	public Date getStatementMonth() {
		return this.statementMonth;
	}

	public void setStatementMonth(Date statementMonth) {
		this.statementMonth = statementMonth;
	}


}