package com.sinodynamic.hkgta.entity.adm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the permit_card_master database table.
 * 
 */
@Entity
@Table(name="permit_card_master")
@NamedQuery(name="PermitCardMaster.findAll", query="SELECT p FROM PermitCardMaster p")
public class PermitCardMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="card_no", unique=true, nullable=false, length=50)
	private String cardNo;

	@Column(name="card_purpose", length=10)
	private String cardPurpose;

	@Column(name="card_type_name", length=100)
	private String cardTypeName;

	@Column(length=10)
	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="status_update_date")
	private Date statusUpdateDate;

	@Column(name="update_by", length=50)
	private String updateBy;

	
	@Column(name="mapping_customer_id")
	private Long mappingCustomerId;
	
	@Column(name="factory_serial_no")
	private String factorySerialNo;
	
	@Column(name="card_issue_date")
	private String cardIssueDate;
	
	@Column(name="remark")
	private String remark;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;
	
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public PermitCardMaster() {
	}

	public String getCardNo() {
		return this.cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardPurpose() {
		return this.cardPurpose;
	}

	public void setCardPurpose(String cardPurpose) {
		this.cardPurpose = cardPurpose;
	}

	public String getCardTypeName() {
		return this.cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStatusUpdateDate() {
		return this.statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getMappingCustomerId() {
		return mappingCustomerId;
	}

	public void setMappingCustomerId(Long mappingCustomerId) {
		this.mappingCustomerId = mappingCustomerId;
	}

	public String getFactorySerialNo() {
		return factorySerialNo;
	}

	public void setFactorySerialNo(String factorySerialNo) {
		this.factorySerialNo = factorySerialNo;
	}

	public String getCardIssueDate() {
		return cardIssueDate;
	}

	public void setCardIssueDate(String cardIssueDate) {
		this.cardIssueDate = cardIssueDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
}