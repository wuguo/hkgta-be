package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the room_housekeep_task_assignee database table.
 * 
 */
@Embeddable
public class RoomHousekeepTaskAssigneePK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "task_id", unique = true, nullable = false)
	private Long taskId;
	
	@Column(name="staff_user_id",nullable=false,length=50)
	private String staffUserId;
	
	public RoomHousekeepTaskAssigneePK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof RoomHousekeepTaskAssigneePK)) {
			return false;
		}
		RoomHousekeepTaskAssigneePK castOther = (RoomHousekeepTaskAssigneePK)other;
		return 
			this.taskId.equals(castOther.taskId)
			&& this.staffUserId.equals(castOther.staffUserId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.taskId.intValue();
		hash = hash * prime + this.staffUserId.hashCode();
		
		return hash;
	}

	public Long getTaskId()
	{
		return taskId;
	}

	public void setTaskId(Long taskId)
	{
		this.taskId = taskId;
	}

	public String getStaffUserId()
	{
		return staffUserId;
	}

	public void setStaffUserId(String staffUserId)
	{
		this.staffUserId = staffUserId;
	}


}