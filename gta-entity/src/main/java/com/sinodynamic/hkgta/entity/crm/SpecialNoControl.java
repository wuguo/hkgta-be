package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the special_no_control database table.
 * 
 */
@Entity
@Table(name="special_no_control")
@NamedQuery(name="SpecialNoControl.findAll", query="SELECT s FROM SpecialNoControl s")
public class SpecialNoControl implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SpecialNoControlPK id;

	@Column(nullable=false, length=10)
	private String category;

	private Long priority;

	@Column(name="sys_id", nullable=false)
	private Long sysId;

	public SpecialNoControl() {
	}

	public SpecialNoControlPK getId() {
		return this.id;
	}

	public void setId(SpecialNoControlPK id) {
		this.id = id;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getPriority() {
		return this.priority;
	}

	public void setPriority(Long priority) {
		this.priority = priority;
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Object sysId) {
		this.sysId = (sysId!=null ? NumberUtils.toLong(sysId.toString()):null);
	}

}