package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the user_preference_setting database table.
 * 
 */
@Embeddable
public class UserPreferenceSettingPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="user_id", insertable=false, updatable=false, unique=true, nullable=false, length=50)
	private String userId;

	@Column(name="param_id", insertable=false, updatable=false, unique=true, nullable=false, length=40)
	private String paramId;

	public UserPreferenceSettingPK() {
	}
	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getParamId() {
		return this.paramId;
	}
	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserPreferenceSettingPK)) {
			return false;
		}
		UserPreferenceSettingPK castOther = (UserPreferenceSettingPK)other;
		return 
			this.userId.equals(castOther.userId)
			&& this.paramId.equals(castOther.paramId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userId.hashCode();
		hash = hash * prime + this.paramId.hashCode();
		
		return hash;
	}
}