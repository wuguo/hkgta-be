package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the customer_service_subscribe database table.
 * 
 */
@Entity
@Table(name="customer_service_subscribe")
@NamedQuery(name="CustomerServiceSubscribe.findAll", query="SELECT c FROM CustomerServiceSubscribe c")
public class CustomerServiceSubscribe implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CustomerServiceSubscribePK id;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name="subscribe_date")
	private Date subscribeDate;

	@Column(name="sys_id", nullable=false)
	private Long sysId;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Column(name="order_det_id")
	private Long orderDetId;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long verNo;
	
	//bi-directional many-to-one association to CustomerServiceAcc
	@ManyToOne
	@JoinColumn(name="acc_no", nullable=false, insertable=false, updatable=false)
	private CustomerServiceAcc customerServiceAcc;

	public CustomerServiceSubscribe() {
	}

	
	
	public Long getVerNo() {
		return verNo;
	}



	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}



	public CustomerServiceSubscribePK getId() {
		return this.id;
	}

	
	public void setId(CustomerServiceSubscribePK id) {
		this.id = id;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getSubscribeDate() {
		return this.subscribeDate;
	}

	public void setSubscribeDate(Date subscribeDate) {
		this.subscribeDate = subscribeDate;
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public CustomerServiceAcc getCustomerServiceAcc() {
		return this.customerServiceAcc;
	}

	public void setCustomerServiceAcc(CustomerServiceAcc customerServiceAcc) {
		this.customerServiceAcc = customerServiceAcc;
	}

	public Long getOrderDetId() {
		return orderDetId;
	}

	public void setOrderDetId(Long orderDetId) {
		this.orderDetId = orderDetId;
	}
	

}