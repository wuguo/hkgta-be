package com.sinodynamic.hkgta.entity.rpos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the customer_order_permit_card database table.
 * 
 */
@Entity
@Table(name="customer_order_permit_card")
@NamedQuery(name="CustomerOrderPermitCard.findAll", query="SELECT c FROM CustomerOrderPermitCard c")
public class CustomerOrderPermitCard implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="qr_code", unique=true, nullable=false)
	private Long qrCode;

	//bi-directional many-to-one association to CustomerOrderHd
	@ManyToOne
	@JoinColumn(name="customer_order_no", nullable=false)
	@JsonIgnore
	private CustomerOrderHd customerOrderHd;
	
	@Column(name="cardholder_customer_id")
	private Long  cardholderCustomerId;
	
	@Column(name="status", length=10)
	private String status;
	
	@Column(name="service_plan_no")
	private Long servicePlanNo;
	
	@Column(name="effective_from")
	private Date effectiveFrom;
	
	@Column(name="effective_to")
	private Date effectiveTo;
	
	@Column(name="linked_card_no")
	private Long linkedCardNo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="linked_card_date")
	private Date linkedCardDate;
	
	@ManyToOne
	@JoinColumn(name="candidate_customer_id")
	private CandidateCustomer candidateCustomer;
	@Version
	@Column(name = "ver_no", nullable=false)
	private Long verNo;
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	public Long getServicePlanNo() {
		return servicePlanNo;
	}

	public void setServicePlanNo(Long servicePlanNo) {
		this.servicePlanNo = servicePlanNo;
	}

	public Long getLinkedCardNo() {
		return linkedCardNo;
	}

	public void setLinkedCardNo(Object linkedCardNo) {
		/*if(linkedCardNo instanceof Long){
			this.linkedCardNo = ((Long) linkedCardNo).longValue();
		}else if(linkedCardNo instanceof Integer){
			this.linkedCardNo = ((Integer) linkedCardNo).longValue();
		}else if(linkedCardNo instanceof String){
			this.linkedCardNo = Long.valueOf((String) linkedCardNo);
		}else{
			this.linkedCardNo = (Long) linkedCardNo;
		}*/
		this.linkedCardNo = (linkedCardNo!=null ? NumberUtils.toLong(linkedCardNo.toString()):null);
	}

	public Date getLinkedCardDate() {
		return linkedCardDate;
	}

	public void setLinkedCardDate(Date linkedCardDate) {
		this.linkedCardDate = linkedCardDate;
	}

	public CustomerOrderPermitCard() {
	}

	public CustomerOrderHd getCustomerOrderHd() {
		return this.customerOrderHd;
	}

	public void setCustomerOrderHd(CustomerOrderHd customerOrderHd) {
		this.customerOrderHd = customerOrderHd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}


	public Long getQrCode() {
		return qrCode;
	}

	public void setQrCode(Long qrCode) {
		this.qrCode = qrCode;
	}

	public Long getCardholderCustomerId() {
		return cardholderCustomerId;
	}

	public void setCardholderCustomerId(Long cardholderCustomerId) {
		this.cardholderCustomerId = cardholderCustomerId;
	}

	public CandidateCustomer getCandidateCustomer() {
		return candidateCustomer;
	}

	public void setCandidateCustomer(CandidateCustomer candidateCustomer) {
		this.candidateCustomer = candidateCustomer;
	}

	
}