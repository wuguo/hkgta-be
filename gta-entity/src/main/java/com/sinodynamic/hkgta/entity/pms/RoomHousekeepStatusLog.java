package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the room_housekeep_status_log	 database table.
 * 
 */
@Entity
@Table(name="room_housekeep_status_log")
@NamedQuery(name="RoomHousekeepStatusLog.findAll", query="SELECT p FROM RoomHousekeepStatusLog p")
public class RoomHousekeepStatusLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "log_id", unique = true, nullable = false, length = 20)
	private Long logId;
	
	@Column(name = "task_id")
	private Long taskId;
	
	@Column(name="status_from",length=10)
	private String statusFrom;
	
	@Column(name="status_to",length=10)
	private String statusTo;

	@Column(name = "status_chg_by", length = 50)
	private String statusChgBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_date")
	private Date statusDate;
	
	public RoomHousekeepStatusLog() {
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getStatusChgBy() {
		return statusChgBy;
	}

	public void setStatusChgBy(String statusChgBy) {
		this.statusChgBy = statusChgBy;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public String getStatusFrom()
	{
		return statusFrom;
	}

	public void setStatusFrom(String statusFrom)
	{
		this.statusFrom = statusFrom;
	}

	public String getStatusTo()
	{
		return statusTo;
	}

	public void setStatusTo(String statusTo)
	{
		this.statusTo = statusTo;
	}
	
	
}