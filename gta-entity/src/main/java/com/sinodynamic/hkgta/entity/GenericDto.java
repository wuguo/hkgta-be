package com.sinodynamic.hkgta.entity;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class GenericDto implements Serializable{ 

	private static final long serialVersionUID = 1L;

	
	private String GenericDtoInfo;


	public String getGenericDtoInfo() {
		return GenericDtoInfo;
	}


	public void setGenericDtoInfo(String genericDtoInfo) {
		GenericDtoInfo = genericDtoInfo;
	}
	
	public Object copyData(Object classzOrigin, Object classzTarget) {
		try{
		 Field[] fields = classzOrigin.getClass().getDeclaredFields();
    	 for(Field field :fields){
    		String methodType="";
			String beforeValue="";
			Method setMethod=null;
			Method[] methods = classzOrigin.getClass().getDeclaredMethods();
			for(Method method :methods){
	    		 if(method.getName().equalsIgnoreCase("get"+field.getName())){
	    			 String type =	 method.getGenericReturnType().toString();
	        		 if (type.equals("class java.lang.String")) {
	        			 beforeValue = (String) method.invoke(classzOrigin); //call getter method to get the value	                	
	        		 }else if (type.equalsIgnoreCase("class java.lang.Long")) {
//	        			 Long templong=(Long) method.invoke(classzOrigin);
	        			 beforeValue =String.valueOf((Long) method.invoke(classzOrigin)); // 调用getter方法获取属性值	 
	        			 methodType="java.lang.Long";
	        		 }
	    		 }else if(method.getName().equalsIgnoreCase("set"+field.getName())){
	    			 setMethod=method;	
	    		 }
			}
        	 
        	 if(beforeValue!=null&&!"null".equalsIgnoreCase(beforeValue.trim())&&setMethod!=null&&!"".equalsIgnoreCase(beforeValue.trim())){	
            	 Method[] tragetMethods = classzTarget.getClass().getDeclaredMethods();
//                	 classzTarget.getClass().getMethod(name, parameterTypes);
            	 for(Method tragetMethod :tragetMethods){
            		 if(tragetMethod.getName().equalsIgnoreCase(setMethod.getName())){
                		 if(methodType.equalsIgnoreCase("java.lang.Long")){
                			 tragetMethod.invoke(classzTarget,Long.valueOf(beforeValue)); //call setter method to set the value
                    		 methodType="";
                		 }else{
                			 tragetMethod.invoke(classzTarget,beforeValue); //call setter method to set the value		 
                		 }	
                		 break;
            		 }
            	 }  					                	
        	 }            
    	 }		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return classzTarget;		
	}
            	 
}
