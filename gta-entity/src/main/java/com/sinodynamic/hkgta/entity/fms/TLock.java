package com.sinodynamic.hkgta.entity.fms;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_lock"
    ,catalog="hkgta"
)
public class TLock  implements java.io.Serializable {


     private Long id;
     private String lockName;

    public TLock() {
    }

    public TLock(Long id, String lockName) {
       this.id = id;
       this.lockName = lockName;
    }
   
     @Id 
    
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
   

    @Column(name="lock_name", nullable=false, length=30)
	public String getLockName() {
		return lockName;
	}

	public void setLockName(String lockName) {
		this.lockName = lockName;
	}


    
    


}


