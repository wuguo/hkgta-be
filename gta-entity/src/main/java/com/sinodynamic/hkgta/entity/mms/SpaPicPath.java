package com.sinodynamic.hkgta.entity.mms;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name="spa_pic_path")
public class SpaPicPath implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pic_id", unique=true, nullable=false)
	private Long picId;
	
	@Column(name="pic_type")
	private String picType;
	
	@Column(name="server_filename")
	private String categoryFileName;
	
	@Column(name="create_date")
	private Timestamp createDate;
	
	@Column(name="create_by")
	private String createBy;
	
	@Column(name="update_date")
	private Timestamp updateDate;
	
	@Column(name="update_by")
	private String updateBy;
	
	@Version
	@Column(name = "ver_no", nullable=false)
	private Long verNo;
	
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="category_id", nullable=false)
	private SpaCategory spaCategory;
	
	public SpaPicPath()
	{
		
	}

	public Long getPicId() {
		return picId;
	}

	public void setPicId(Object picId) {
		this.picId = (picId!=null ? NumberUtils.toLong(picId.toString()):null);
	}

	public String getPicType() {
		return picType;
	}

	public void setPicType(String picType) {
		this.picType = picType;
	}

	public String getCategoryFileName() {
		return categoryFileName;
	}

	public void setCategoryFileName(String categoryFileName) {
		this.categoryFileName = categoryFileName;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public SpaCategory getSpaCategory() {
		return spaCategory;
	}

	public void setSpaCategory(SpaCategory spaCategory) {
		this.spaCategory = spaCategory;
	}
	
	
}
