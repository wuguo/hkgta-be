package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.math.NumberUtils;

@Entity
@Table(name = "room_status_schedule")
@NamedQuery(name = "RoomStatusSchedule.findAll", query = "SELECT r FROM RoomStatusSchedule r")
public class RoomStatusSchedule implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "schedule_id", unique = true, nullable = false, length = 20)

	private Long scheduleId;

	@Column(name = "room_id")
	private Long  roomId;

	@Column(name = "schedule_oo_status", length = 10)
	private String stascheduleOoStatus;

//	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "begin_date")
	private Date beginDate;
	
	@Transient
	private String lockDate;
	@Transient
	private String unlockDate;

//	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "reason_code", length = 20)
	private String reasonCode;

	@Column(name = "remark", length = 500)
	private String remark;

	@Column(name = "create_date")
	private Timestamp createDate;

	@Column(name = "create_by")
	private String createBy;

	public Long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId) {
		this.scheduleId = (scheduleId != null ? NumberUtils.toLong(scheduleId.toString()) : null);
	}
	
	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getStascheduleOoStatus() {
		return stascheduleOoStatus;
	}

	public void setStascheduleOoStatus(String stascheduleOoStatus) {
		this.stascheduleOoStatus = stascheduleOoStatus;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	private static final SimpleDateFormat SDF = new SimpleDateFormat("YYYY-MM-dd");
	@Transient
	public String getLockDate() {
		return SDF.format(beginDate);
	}
	@Transient
	public String getUnlockDate() {
		return SDF.format(endDate);
	}
	
	

}
