package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the customer_email_attach database table.
 * 
 */
@Embeddable
public class CustomerEmailAttachPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="email_send_id", insertable=false, updatable=false, unique=true, nullable=false)
	private String emailSendId;

	@Column(name="attachment_name", unique=true, nullable=false, length=250)
	private String attachmentName;

	public CustomerEmailAttachPK() {
	}
	public String getEmailSendId() {
		return this.emailSendId;
	}
	public void setEmailSendId(String emailSendId) {
		this.emailSendId = emailSendId;
	}
	public String getAttachmentName() {
		return this.attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CustomerEmailAttachPK)) {
			return false;
		}
		CustomerEmailAttachPK castOther = (CustomerEmailAttachPK)other;
		return 
			this.emailSendId.equals(castOther.emailSendId)
			&& this.attachmentName.equals(castOther.attachmentName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.emailSendId.hashCode();
		hash = hash * prime + this.attachmentName.hashCode();
		
		return hash;
	}
}