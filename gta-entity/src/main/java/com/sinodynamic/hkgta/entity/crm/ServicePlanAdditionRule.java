package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the service_plan_addition_rule database table.
 * 
 */
@Entity
@Table(name="service_plan_addition_rule")
@NamedQuery(name="ServicePlanAdditionRule.findAll", query="SELECT s FROM ServicePlanAdditionRule s")
public class ServicePlanAdditionRule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="rule_id", unique=true, nullable=false)
	private Long ruleId;

	@Column(name="input_value", length=100)
	private String inputValue;

	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="plan_no", nullable=false)
	private ServicePlan servicePlan;

	@Column(name="right_code")
	private String rightCode;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public ServicePlanAdditionRule() {
	}

	public String getRightCode() {
		return rightCode;
	}

	public void setRightCode(String rightCode) {
		this.rightCode = rightCode;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public String getInputValue() {
		return this.inputValue;
	}

	public void setInputValue(String inputValue) {
		this.inputValue = inputValue;
	}

	public ServicePlan getServicePlan() {
		return this.servicePlan;
	}

	public void setServicePlan(ServicePlan servicePlan) {
		this.servicePlan = servicePlan;
	}
}