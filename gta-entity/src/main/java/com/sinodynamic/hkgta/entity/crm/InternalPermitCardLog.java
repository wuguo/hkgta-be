package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "internal_permit_card_log")
public class InternalPermitCardLog implements Serializable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	private Long sysId;

	@Column(name = "card_id")
	private Long cardId;

	@Column(name = "staff_user_id", length = 50)
	private String staffUserId;

	@Column(name = "contractor_id")
	private Long contractorId;

	@Column(name = "effective_date")
	private Date effectiveDate;

	@Column(name = "expiry_date")
	private Date expiryDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_update_date")
	private Date statusUpdateDate;

	@Column(name = "update_by")
	private String updateBy;
	
	@Column(name = "status_from", length = 10)
	private String statusFrom;
	
	@Column(name = "status_to", length = 10)
	private String statusTo;

	public Long getSysId() {
	    return sysId;
	}

	public void setSysId(Long sysId) {
	    this.sysId = sysId;
	}

	public Long getCardId() {
	    return cardId;
	}

	public void setCardId(Long cardId) {
	    this.cardId = cardId;
	}

	public String getStaffUserId() {
	    return staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
	    this.staffUserId = staffUserId;
	}

	public Long getContractorId() {
	    return contractorId;
	}

	public void setContractorId(Long contractorId) {
	    this.contractorId = contractorId;
	}

	public Date getEffectiveDate() {
	    return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
	    this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
	    return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
	    this.expiryDate = expiryDate;
	}

	public Date getStatusUpdateDate() {
	    return statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate) {
	    this.statusUpdateDate = statusUpdateDate;
	}

	public String getUpdateBy() {
	    return updateBy;
	}

	public void setUpdateBy(String updateBy) {
	    this.updateBy = updateBy;
	}

	public String getStatusFrom() {
	    return statusFrom;
	}

	public void setStatusFrom(String statusFrom) {
	    this.statusFrom = statusFrom;
	}

	public String getStatusTo() {
	    return statusTo;
	}

	public void setStatusTo(String statusTo) {
	    this.statusTo = statusTo;
	}
	
}
