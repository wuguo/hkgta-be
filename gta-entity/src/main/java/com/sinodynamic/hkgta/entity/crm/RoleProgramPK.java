package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the role_program database table.
 * 
 */
@Embeddable
public class RoleProgramPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="role_id", insertable=false, updatable=false, unique=true, nullable=false)
	private String roleId;

	@Column(name="program_id", insertable=false, updatable=false, unique=true, nullable=false, length=40)
	private String programId;

	public RoleProgramPK() {
	}
	public String getRoleId() {
		return this.roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getProgramId() {
		return this.programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof RoleProgramPK)) {
			return false;
		}
		RoleProgramPK castOther = (RoleProgramPK)other;
		return 
			this.roleId.equals(castOther.roleId)
			&& this.programId.equals(castOther.programId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.roleId.hashCode();
		hash = hash * prime + this.programId.hashCode();
		
		return hash;
	}
}