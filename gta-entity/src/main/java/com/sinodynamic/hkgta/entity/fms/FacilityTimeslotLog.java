package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "facility_timeslot_log")
@NamedQuery(name = "FacilityTimeslotLog.findAll", query = "SELECT l FROM FacilityTimeslotLog l")
public class FacilityTimeslotLog implements Serializable {

	private static final long serialVersionUID = -9187766967092350876L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "log_id", unique = true, nullable = false)
	private Long logId;

	@Column(name = "facility_timeslot_id")
	private Long facilityTimeslotId;

	@Column(name = "facility_type")
	private String facilityType;

	@Column(name = "facility_no")
	private Long facilityNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "begin_datetime", nullable = false)
	private Date beginDatetime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_datetime")
	private Date endDatetime;

	@Column(name = "status")
	private String status; // 'MT-maintenance; OP-Occupied; RS-reserved; TA-temporary allocated'

	@Column(name = "reserve_type")
	private String reserveType; // 'PC-private coach training; CS-course session; MR-member reserve; MT-maintenance.'

	@Column(name = "transfer_from_timeslot_id")
	private Long transferFromTimeslotId; // 'indicate the timeslot is transferred the other facility timeslot. e.g. The
											// original facility is broken for maintenace and reassign another facility
											// to the user.'

	@Column(name = "internal_remark")
	private String internalRemark;

	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Column(name = "log_timestamp")
	private Date logTimestamp;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public Long getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}

	public Date getBeginDatetime() {
		return beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public Date getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReserveType() {
		return reserveType;
	}

	public void setReserveType(String reserveType) {
		this.reserveType = reserveType;
	}

	public Long getTransferFromTimeslotId() {
		return transferFromTimeslotId;
	}

	public void setTransferFromTimeslotId(Long transferFromTimeslotId) {
		this.transferFromTimeslotId = transferFromTimeslotId;
	}

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getLogTimestamp() {
		return logTimestamp;
	}

	public void setLogTimestamp(Date logTimestamp) {
		this.logTimestamp = logTimestamp;
	}

}
