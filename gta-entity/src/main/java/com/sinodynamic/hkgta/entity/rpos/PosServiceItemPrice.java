package com.sinodynamic.hkgta.entity.rpos;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * The persistent class for the pos_service_item_price database table.
 * 
 */
@Entity
@Table(name="pos_service_item_price")
@NamedQuery(name="PosServiceItemPrice.findAll", query="SELECT p FROM PosServiceItemPrice p")
public class PosServiceItemPrice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="item_no", unique=true, nullable=false, length=50)
	private String itemNo;

	@Column(length=100)
	private String description;

	@Column(name="item_catagory", length=10)
	private String itemCatagory;

	@Column(name="item_price", precision=10, scale=2)
	private BigDecimal itemPrice;

	@Column(length=10)
	private String status;

	@Column(name="supplier_no", length=20)
	private String supplierNo;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	public PosServiceItemPrice() {
	}

	public String getItemNo() {
		return this.itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getItemCatagory() {
		return this.itemCatagory;
	}

	public void setItemCatagory(String itemCatagory) {
		this.itemCatagory = itemCatagory;
	}

	public BigDecimal getItemPrice() {
		return this.itemPrice;
	}

	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSupplierNo() {
		return supplierNo;
	}

	public void setSupplierNo(String supplierNo) {
		this.supplierNo = supplierNo;
	}

}