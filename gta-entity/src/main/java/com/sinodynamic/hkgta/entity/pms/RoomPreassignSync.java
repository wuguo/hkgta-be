package com.sinodynamic.hkgta.entity.pms;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="room_preassign_sync")
public class RoomPreassignSync  implements java.io.Serializable {
     private static final long serialVersionUID = 1L;
     
     private String confirmId;
     private String roomNo;
     
     private Date arrivalDatetime;
     private Date departureDatetime;
     private Date syncTimestamp;

    public RoomPreassignSync() {
    }
    @Id
    @Column(name="confirm_id", unique=true, nullable=false)
    public String getConfirmId() {
		return confirmId;
	}

	public void setConfirmId(String confirmId) {
		this.confirmId = confirmId;
	}
	
    @Column(name="room_no", length=20)
	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	@Temporal(TemporalType.TIMESTAMP)
	 @Column(name="arrival_datetime", length=19)
	public Date getArrivalDatetime() {
		return arrivalDatetime;
	}

	public void setArrivalDatetime(Date arrivalDatetime) {
		this.arrivalDatetime = arrivalDatetime;
	}
	@Temporal(TemporalType.TIMESTAMP)
	 @Column(name="departure_datetime", length=19)
	public Date getDepartureDatetime() {
		return departureDatetime;
	}

	public void setDepartureDatetime(Date departureDatetime) {
		this.departureDatetime = departureDatetime;
	}
	 @Temporal(TemporalType.TIMESTAMP)
	 @Column(name="sync_timestamp", length=19)
	public Date getSyncTimestamp() {
		return syncTimestamp;
	}

	public void setSyncTimestamp(Date syncTimestamp) {
		this.syncTimestamp = syncTimestamp;
	}

}


