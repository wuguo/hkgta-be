package com.sinodynamic.hkgta.entity.pos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "restaurant_image")
public class RestaurantImage {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "image_id", unique = true, nullable = false)
	private Long imageId;
	
	@ManyToOne
	@JoinColumn(name = "restaurant_id")
	private RestaurantMaster restaurantMaster;
	
	@Column(name = "for_device_type")
	private String forDeviceType;
	
	@Column(name = "image_feature_code")
	private String imageFeatureCode;
	
	@Column(name = "image_filename")
	private String imageFileName;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	public Long getImageId() {
		return imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public RestaurantMaster getRestaurantMaster() {
		return restaurantMaster;
	}

	public void setRestaurantMaster(RestaurantMaster restaurantMaster) {
		this.restaurantMaster = restaurantMaster;
	}

	public String getForDeviceType() {
		return forDeviceType;
	}

	public void setForDeviceType(String forDeviceType) {
		this.forDeviceType = forDeviceType;
	}

	public String getImageFeatureCode() {
		return imageFeatureCode;
	}

	public void setImageFeatureCode(String imageFeatureCode) {
		this.imageFeatureCode = imageFeatureCode;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
