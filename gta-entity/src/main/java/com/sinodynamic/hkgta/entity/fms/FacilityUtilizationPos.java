package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the facility_utilization_pos database table.
 * 
 */
@Entity
@Table(name="facility_utilization_pos")
@NamedQuery(name="FacilityUtilizationPos.findAll", query="SELECT f FROM FacilityUtilizationPos f")
public class FacilityUtilizationPos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="facility_pos_id", unique=true, nullable=false)
	private Long facilityPosId;

	@Column(name="pos_item_no")
	private String posItemNo;
	
	@Column(name="facility_type", length=10)
	private String facilityType;
	
	@ManyToOne
	@JoinColumn(name="facility_no")
	private FacilityMaster facilityMaster;
	
	@Column(name="rate_type", length=10)
	private String rateType;
	
	@Column(name="age_range_code", length=10)
	private String ageRangeCode;

	@Column(length=250)
	private String description;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public FacilityUtilizationPos() {
	}

	public Long getFacilityPosId()
	{
		return facilityPosId;
	}

	public void setFacilityPosId(Long facilityPosId)
	{
		this.facilityPosId = facilityPosId;
	}

	public String getPosItemNo()
	{
		return posItemNo;
	}

	public void setPosItemNo(String posItemNo)
	{
		this.posItemNo = posItemNo;
	}

	public String getFacilityType()
	{
		return facilityType;
	}

	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}

	public FacilityMaster getFacilityMaster()
	{
		return facilityMaster;
	}

	public void setFacilityMaster(FacilityMaster facilityMaster)
	{
		this.facilityMaster = facilityMaster;
	}

	public String getRateType()
	{
		return rateType;
	}

	public void setRateType(String rateType)
	{
		this.rateType = rateType;
	}

	public String getAgeRangeCode()
	{
		return ageRangeCode;
	}

	public void setAgeRangeCode(String ageRangeCode)
	{
		this.ageRangeCode = ageRangeCode;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
}