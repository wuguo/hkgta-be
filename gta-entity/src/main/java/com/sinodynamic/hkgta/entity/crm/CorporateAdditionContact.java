package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;

/**
 * The persistent class for the corporate_addition_contact database table.
 * 
 */
@Entity
@Table(name = "corporate_addition_contact")
@NamedQuery(name = "CorporateAdditionContact.findAll", query = "SELECT c FROM CorporateAdditionContact c")
public class CorporateAdditionContact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "contact_id")
	private Long contactId;

	@Column(name = "contact_email")
	private String contactEmail;

	@Column(name = "contact_person_firstname")
	private String contactPersonFirstname;

	@Column(name = "contact_person_lastname")
	private String contactPersonLastname;

	@Column(name = "contact_phone")
	private String contactPhone;

	@Column(name = "contact_phone_mobile")
	private String contactPhoneMobile;

	@Column(name = "contact_type")
	private String contactType;

	@Column(name = "corporate_id")
	private Long corporateId;

	@Column(name = "create_by")
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "district")
	private String district;

	@Column(name = "postal_address1")
	private String postalAddress1;

	@Column(name = "postal_address2")
	private String postalAddress2;

	@Column(name = "update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;

	public CorporateAdditionContact() {
	}

	public Long getContactId() {
		return this.contactId;
	}

	public void setContactId(Object contactId) {
		this.contactId = (contactId != null ? NumberUtils.toLong(contactId.toString()) : null);
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactPersonFirstname() {
		return this.contactPersonFirstname;
	}

	public void setContactPersonFirstname(String contactPersonFirstname) {
		this.contactPersonFirstname = contactPersonFirstname;
	}

	public String getContactPersonLastname() {
		return this.contactPersonLastname;
	}

	public void setContactPersonLastname(String contactPersonLastname) {
		this.contactPersonLastname = contactPersonLastname;
	}

	public String getContactPhone() {
		return this.contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactPhoneMobile() {
		return this.contactPhoneMobile;
	}

	public void setContactPhoneMobile(String contactPhoneMobile) {
		this.contactPhoneMobile = contactPhoneMobile;
	}

	public String getContactType() {
		return this.contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public Long getCorporateId() {
		return this.corporateId;
	}

	public void setCorporateId(Object corporateId) {
		this.corporateId = (corporateId != null ? NumberUtils.toLong(corporateId.toString()) : null);
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPostalAddress1() {
		return this.postalAddress1;
	}

	public void setPostalAddress1(String postalAddress1) {
		this.postalAddress1 = postalAddress1;
	}

	public String getPostalAddress2() {
		return this.postalAddress2;
	}

	public void setPostalAddress2(String postalAddress2) {
		this.postalAddress2 = postalAddress2;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Object version) {
		this.version = (version != null ? NumberUtils.toLong(version.toString()) : null);
	}
	
	public Boolean equals(CorporateProfile corporateProfile){
		if(corporateProfile==null){
			return false;
		}
		
		if (!compare(this.contactPersonFirstname, corporateProfile.getContactPersonFirstname())) {
			return false;
		}
		
		if (!compare(this.contactPersonLastname, corporateProfile.getContactPersonLastname())) {
			return false;
		}
		
		if (!compare(this.contactPhone, corporateProfile.getContactPhone())) {
			return false;
		}
		
		if (!compare(this.contactPhoneMobile, corporateProfile.getContactPhoneMobile())) {
			return false;
		}
		
		if (!compare(this.contactEmail, corporateProfile.getContactEmail())) {
			return false;
		}
		
		if (!compare(this.postalAddress1, corporateProfile.getAddress1())) {
			return false;
		}
		
		if (!compare(this.postalAddress2, corporateProfile.getAddress2())) {
			return false;
		}
		
		if (!compare(this.district, corporateProfile.getDistrict())) {
			return false;
		}
		
		return true;
	}
	
	public boolean compare(Object source,Object target){
		if (null != source) {
			if(!source.equals(target)){
				return false;
			}
		}else {
			if (null != target){
				return false;
			}
		}
		return true;
	}

}