package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the corporate_service_subscribe database table.
 * 
 */
@Entity
@Table(name="corporate_service_subscribe")
@NamedQuery(name="CorporateServiceSubscribe.findAll", query="SELECT c FROM CorporateServiceSubscribe c")
public class CorporateServiceSubscribe implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CorporateServiceSubscribePK id;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_date")
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name="subscribe_date")
	private Date subscribeDate;

	@Column(name="sys_id")
	private Long sysId;

	@Column(name="update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@ManyToOne
	@JoinColumn(name="acc_no", insertable=false, updatable=false)
	private CorporateServiceAcc corporateServiceAcc;

	public CorporateServiceSubscribe() {
	}

	public CorporateServiceSubscribePK getId() {
		return this.id;
	}

	public void setId(CorporateServiceSubscribePK id) {
		this.id = id;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getSubscribeDate() {
		return this.subscribeDate;
	}

	public void setSubscribeDate(Date subscribeDate) {
		this.subscribeDate = subscribeDate;
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public CorporateServiceAcc getCorporateServiceAcc() {
		return this.corporateServiceAcc;
	}

	public void setCorporateServiceAcc(CorporateServiceAcc corporateServiceAcc) {
		this.corporateServiceAcc = corporateServiceAcc;
	}

}