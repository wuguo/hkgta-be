package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the room_housekeep_task database table.
 * 
 */
@Entity
@Table(name="room_housekeep_task")
@NamedQuery(name="RoomHousekeepTask.findAll", query="SELECT p FROM RoomHousekeepTask p")
public class RoomHousekeepTask implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "task_id", unique = true, nullable = false)
	private Long taskId;
	
	@Column(name="job_type" ,length=10)
	private String jobType;

	@Column(name="requester_user_id",length=50)
	private String requesterUserId;

	@Column(name="schedule_datetime")
	private Date scheduleDatetime;

	@Column(name = "exp_response_time")
	private Long expResponseTime;

	@Column(name = "target_complete_datetime")
	private Date targetCompleteDatetime;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "task_description",length=300)
	private String taskDescription;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_timestamp")
	private Date startTimestamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "finish_timestamp")
	private Date finishTimestamp;
	
	@Column(name = "inspector_user_id",length=50)
	private String inspectorUserId;
	
	@Column(name = "from_task_id")
	private Long fromTaskId;
	
	@Column(name = "remark",length=300)
	private String remark;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "status_chg_by", length = 50)
	private String statusChgBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_date")
	private Date statusDate;
	
	@Column(name = "alert_sent")
	private Long alertSent;
	
	@Column(name = "monitor_detected")
	private String monitorDetected;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_alert_timestamp")
	private Date lastAlertTimestamp;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "room_id")
	private Room room;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "task_id")
	private List<HousekeepTaskFile> housekeepTaskFiles;
	
	@OneToMany(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "room_id")
	private List<RoomCrew> roomCrews;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public RoomHousekeepTask() {
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getRequesterUserId() {
		return requesterUserId;
	}

	public void setRequesterUserId(String requesterUserId) {
		this.requesterUserId = requesterUserId;
	}

	public Date getScheduleDatetime() {
		return scheduleDatetime;
	}

	public void setScheduleDatetime(Date scheduleDatetime) {
		this.scheduleDatetime = scheduleDatetime;
	}

	public Long getExpResponseTime() {
		return expResponseTime;
	}

	public void setExpResponseTime(Long expResponseTime) {
		this.expResponseTime = expResponseTime;
	}

	public Date getTargetCompleteDatetime() {
		return targetCompleteDatetime;
	}

	public void setTargetCompleteDatetime(Date targetCompleteDatetime) {
		this.targetCompleteDatetime = targetCompleteDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public Date getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public Date getFinishTimestamp() {
		return finishTimestamp;
	}

	public void setFinishTimestamp(Timestamp finishTimestamp) {
		this.finishTimestamp = finishTimestamp;
	}

	public String getInspectorUserId() {
		return inspectorUserId;
	}

	public void setInspectorUserId(String inspectorUserId) {
		this.inspectorUserId = inspectorUserId;
	}

	public Long getFromTaskId() {
		return fromTaskId;
	}

	public void setFromTaskId(Long fromTaskId) {
		this.fromTaskId = fromTaskId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getStatusChgBy() {
		return statusChgBy;
	}

	public void setStatusChgBy(String statusChgBy) {
		this.statusChgBy = statusChgBy;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public List<HousekeepTaskFile> getHousekeepTaskFiles() {
		return housekeepTaskFiles;
	}

	public void setHousekeepTaskFiles(List<HousekeepTaskFile> housekeepTaskFiles) {
		this.housekeepTaskFiles = housekeepTaskFiles;
	}

	public List<RoomCrew> getRoomCrews()
	{
		return roomCrews;
	}

	public void setRoomCrews(List<RoomCrew> roomCrews)
	{
		this.roomCrews = roomCrews;
	}

	public Long getAlertSent()
	{
		return alertSent;
	}

	public void setAlertSent(Long alertSent)
	{
		this.alertSent = alertSent;
	}

	public Date getLastAlertTimestamp()
	{
		return lastAlertTimestamp;
	}

	public void setLastAlertTimestamp(Date lastAlertTimestamp)
	{
		this.lastAlertTimestamp = lastAlertTimestamp;
	}

	public Long getVerNo()
	{
		return verNo;
	}

	public void setVerNo(Long verNo)
	{
		this.verNo = verNo;
	}

	public String getMonitorDetected()
	{
		return monitorDetected;
	}

	public void setMonitorDetected(String monitorDetected)
	{
		this.monitorDetected = monitorDetected;
	}

	public void setStartTimestamp(Date startTimestamp)
	{
		this.startTimestamp = startTimestamp;
	}

	public void setFinishTimestamp(Date finishTimestamp)
	{
		this.finishTimestamp = finishTimestamp;
	}

}