package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "facility_sub_type")
public class FacilitySubType  implements Serializable {
	
	private static final long serialVersionUID = -7789362144746286383L;

	@Id
	@Column(name = "subtype_id" , unique=true, nullable=false, length=50)
	private String subTypeId;
	
	@Column(name = "facility_type_code")
	private String facilityTypeCode;
	
	@Column(name = "name")
	private String name;
	
	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="create_by")
	private String createBy;
	
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="update_by")
	private String updateBy;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	@OneToMany(mappedBy = "facilitySubType", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<FacilitySubTypePos> facilitySubTypePosList;

	public String getSubTypeId() {
		return subTypeId;
	}

	public void setSubTypeId(String subTypeId) {
		this.subTypeId = subTypeId;
	}

	public String getFacilityTypeCode() {
		return facilityTypeCode;
	}

	public void setFacilityTypeCode(String facilityTypeCode) {
		this.facilityTypeCode = facilityTypeCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<FacilitySubTypePos> getFacilitySubTypePosList()
	{
		return facilitySubTypePosList;
	}

	public void setFacilitySubTypePosList(List<FacilitySubTypePos> facilitySubTypePosList)
	{
		this.facilitySubTypePosList = facilitySubTypePosList;
	}
}
