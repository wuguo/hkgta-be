package com.sinodynamic.hkgta.entity.qst;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "survey")
public class Survey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "survey_id", unique = true, nullable = false)
	private Long surveyId;

	@Column(name = "survey_name", length = 200)
	private String surveyName;

	@Column(name = "forward_url", length = 500)
	private String forwardUrl;

	@Column(name = "utilize_rate_hit", length = 11)
	private Long utilizeRateHit;

	@Column(name = "buffer_day", length = 11)
	private Long bufferDay;

	@Column(name = "status", length = 10)
	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Version
	@Column(name = "ver_no", nullable = false)
	private Long verNo;

	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

	public String getSurveyName() {
		return surveyName;
	}

	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	public String getForwardUrl() {
		return forwardUrl;
	}

	public void setForwardUrl(String forwardUrl) {
		this.forwardUrl = forwardUrl;
	}

	public Long getUtilizeRateHit() {
		return utilizeRateHit;
	}

	public void setUtilizeRateHit(Long utilizeRateHit) {
		this.utilizeRateHit = utilizeRateHit;
	}

	public Long getBufferDay() {
		return bufferDay;
	}

	public void setBufferDay(Long bufferDay) {
		this.bufferDay = bufferDay;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
}
