package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author Zero_Wang
 * @date   May 13, 2015
 */
@Entity
@Table(name="service_plan_date_pos")
public class ServicePlanDatePos implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="date_id", unique=true, nullable=false)
	private Long dateId;
	@Column(name="special_date")
	private Date specialDate;
	@Column(name="annual_repeat")
	private String annualRepeat;
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="rate_id", nullable=false)
	private ServicePlanRatePos planRatePos;
	
	public ServicePlanRatePos getPlanRatePos() {
		return planRatePos;
	}
	public void setPlanRatePos(ServicePlanRatePos planRatePos) {
		this.planRatePos = planRatePos;
	}
	public Long getDateId() {
		return dateId;
	}
	public void setDateId(Long dateId) {
		this.dateId = dateId;
	}
	public Date getSpecialDate() {
		return specialDate;
	}
	public void setSpecialDate(Date specialDate) {
		this.specialDate = specialDate;
	}
	public String getAnnualRepeat() {
		return annualRepeat;
	}
	public void setAnnualRepeat(String annualRepeat) {
		this.annualRepeat = annualRepeat;
	}

	
	

}
