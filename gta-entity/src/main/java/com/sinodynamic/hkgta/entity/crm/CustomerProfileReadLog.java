package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

@Entity
@Table(name = "customer_profile_read_log")
public class CustomerProfileReadLog extends GenericDto implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "log_id", unique = true, nullable = true)
	private Long logId;
	
	@Column(name = "customer_id", nullable = false)
	@EncryptFieldInfo  
	private Long customerId;
	
	@Column(name = "reader_user_id", nullable = false,length=50)
	private String readerUserId;
	
	/***
	 * Base/Full and so on
	 */
	@Column(name = "read_type",length=5)
	private String readType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "read_timestamp",length=50)
	private Date readTimestamp;
	
	@Transient
	private String academyId;
	
	@Transient
	private String readUserName;
	
	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getReadUserName() {
		return readUserName;
	}

	public void setReadUserName(String readUserName) {
		this.readUserName = readUserName;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Object logId) {
		if(logId instanceof BigInteger){
			this.logId = ((BigInteger) logId).longValue();
		}else if(logId instanceof Integer){
			this.logId = ((Integer) logId).longValue();
		}else if(logId instanceof String){
			this.logId = Long.valueOf((String) logId);
		}else{
			this.logId = (Long) logId;
		}
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}
	}

	public String getReaderUserId() {
		return readerUserId;
	}

	public void setReaderUserId(String readerUserId) {
		this.readerUserId = readerUserId;
	}

	public String getReadType() {
		return readType;
	}

	public void setReadType(String readType) {
		this.readType = readType;
	}

	public Date getReadTimestamp() {
		return readTimestamp;
	}

	public void setReadTimestamp(Date readTimestamp) {
		this.readTimestamp = readTimestamp;
	}
	
}
