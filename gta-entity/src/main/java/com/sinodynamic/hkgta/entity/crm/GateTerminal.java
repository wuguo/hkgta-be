package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the global_parameter database table.
 * 
 */
@Entity
@Table(name="gate_terminal")
@NamedQuery(name="GateTerminal.findAll", query="SELECT t FROM GateTerminal t")
public class GateTerminal implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "generator")     
	@GenericGenerator(name = "generator", strategy = "assigned") 
	@Column(name="terminal_id", unique=true, nullable=false)
	private String terminalId;

	@Column(name="terminal_type", length=10)
	private String terminalType;

	@Column(name="description", length=255)
	private String description;

	@Column(name="facility_no", nullable=true)
	private Timestamp facilityNo;

	public String getTerminalId()
	{
		return terminalId;
	}

	public void setTerminalId(String terminalId)
	{
		this.terminalId = terminalId;
	}

	public String getTerminalType()
	{
		return terminalType;
	}

	public void setTerminalType(String terminalType)
	{
		this.terminalType = terminalType;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Timestamp getFacilityNo()
	{
		return facilityNo;
	}

	public void setFacilityNo(Timestamp facilityNo)
	{
		this.facilityNo = facilityNo;
	}

	
}
