package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the service_plan_right_master database table.
 * 
 */
@Entity
@Table(name="service_plan_right_master")
@NamedQuery(name="ServicePlanRightMaster.findAll", query="SELECT s FROM ServicePlanRightMaster s")
public class ServicePlanRightMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="right_code", unique=true, nullable=false, length=20)
	private String rightCode;

	@Column(length=250)
	private String description;

	@Column(name="input_value_type", length=20)
	private String inputValueType;

	@Column(name="right_type", nullable=false, length=50)
	private String rightType;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	
	public ServicePlanRightMaster() {
	}

	
	public Long getVerNo() {
		return verNo;
	}


	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}


	public String getRightCode() {
		return this.rightCode;
	}

	public void setRightCode(String rightCode) {
		this.rightCode = rightCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInputValueType() {
		return this.inputValueType;
	}

	public void setInputValueType(String inputValueType) {
		this.inputValueType = inputValueType;
	}

	public String getRightType() {
		return this.rightType;
	}

	public void setRightType(String rightType) {
		this.rightType = rightType;
	}
}