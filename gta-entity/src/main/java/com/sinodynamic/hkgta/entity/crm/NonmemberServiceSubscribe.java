package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the nonmember_service_subscribe database table.
 * 
 */
@Entity
@Table(name="nonmember_service_subscribe")
@NamedQuery(name="NonmemberServiceSubscribe.findAll", query="SELECT n FROM NonmemberServiceSubscribe n")
public class NonmemberServiceSubscribe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="service_id", unique=true, nullable=false)
	private String serviceId;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_from")
	private Date effectiveFrom;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_to")
	private Date effectiveTo;

	@Column(length=10)
	private String status;

	//bi-directional many-to-one association to Member
	@ManyToOne
	@JoinColumn(name="customer_id", nullable=false)
	private Member member;

	//bi-directional many-to-one association to ServicePlanPo
	@ManyToOne
	@JoinColumn(name="serv_pos_id", nullable=false)
	private ServicePlanPos servicePlanPo;
	
	public NonmemberServiceSubscribe() {
	}

	public String getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Date getEffectiveFrom() {
		return this.effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return this.effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public ServicePlanPos getServicePlanPo() {
		return this.servicePlanPo;
	}

	public void setServicePlanPo(ServicePlanPos servicePlanPo) {
		this.servicePlanPo = servicePlanPo;
	}

}