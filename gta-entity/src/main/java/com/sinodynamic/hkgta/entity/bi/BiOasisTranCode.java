package com.sinodynamic.hkgta.entity.bi;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "bi_oasis_tran_code")
public class BiOasisTranCode implements Serializable {

	private static final long serialVersionUID = -7866000693071195679L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "oasis_tran_code", length = 20)
	private String oasisTranCode;

	@Column(name = "gta_tag", length = 20)
	private String gtaTag;

	@Column(name = "party", length = 200)
	private String party;

	@Column(name = "description", length = 200)
	private String description;

	@Column(name = "tran_type", length = 50)
	private String tranType;

	@Column(name = "status", length = 10)
	private String status;

	@Column(name = "flex_acc_code", length = 20)
	private String flexAccCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	public String getOasisTranCode() {
		return oasisTranCode;
	}

	public void setOasisTranCode(String oasisTranCode) {
		this.oasisTranCode = oasisTranCode;
	}

	public String getGtaTag() {
		return gtaTag;
	}

	public void setGtaTag(String gtaTag) {
		this.gtaTag = gtaTag;
	}

	public String getParty() {
		return party;
	}

	public void setParty(String party) {
		this.party = party;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTranType() {
		return tranType;
	}

	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFlexAccCode() {
		return flexAccCode;
	}

	public void setFlexAccCode(String flexAccCode) {
		this.flexAccCode = flexAccCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	

}
