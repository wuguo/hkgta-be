package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the member_log database table.
 * 
 */
@Entity
@Table(name="member_log")
public class MemberLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="log_id")
	private String logId;

	@Column(name="academy_no")
	private String academyNo;

	@Column(name="customer_id")
	private BigInteger customerId;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_date")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="first_join_date")
	private Date firstJoinDate;

	@Column(name="internal_remark")
	private String internalRemark;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="log_timestamp")
	private Date logTimestamp;

	@Column(name="member_type")
	private String memberType;

	@Column(name="relationship_code")
	private String relationshipCode;

	private String status;

	@Column(name="superior_member_id")
	private BigInteger superiorMemberId;

	@Temporal(TemporalType.DATE)
	@Column(name="termination_date")
	private Date terminationDate;

	@Column(name="update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Column(name="user_id")
	private String userId;

	private String vip;

	public MemberLog() {
	}

	public String getLogId() {
		return this.logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public String getAcademyNo() {
		return this.academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public BigInteger getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getFirstJoinDate() {
		return this.firstJoinDate;
	}

	public void setFirstJoinDate(Date firstJoinDate) {
		this.firstJoinDate = firstJoinDate;
	}

	public String getInternalRemark() {
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public Date getLogTimestamp() {
		return this.logTimestamp;
	}

	public void setLogTimestamp(Date logTimestamp) {
		this.logTimestamp = logTimestamp;
	}

	public String getMemberType() {
		return this.memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getRelationshipCode() {
		return this.relationshipCode;
	}

	public void setRelationshipCode(String relationshipCode) {
		this.relationshipCode = relationshipCode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigInteger getSuperiorMemberId() {
		return this.superiorMemberId;
	}

	public void setSuperiorMemberId(BigInteger superiorMemberId) {
		this.superiorMemberId = superiorMemberId;
	}

	public Date getTerminationDate() {
		return this.terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getVip() {
		return this.vip;
	}

	public void setVip(String vip) {
		this.vip = vip;
	}

}