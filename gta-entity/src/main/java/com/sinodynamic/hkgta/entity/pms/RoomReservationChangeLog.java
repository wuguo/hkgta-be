package com.sinodynamic.hkgta.entity.pms;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="room_reservation_change_log")
public class RoomReservationChangeLog  implements java.io.Serializable {


     private Long logId;
     private String resvId;
     private Long roomIdFrom;
     private Long roomIdTo;
     private Date changeTimestamp;
     private String changeRemark;
     private Date createDate;

    public RoomReservationChangeLog() {
    }

    public RoomReservationChangeLog(String resvId, Long roomIdFrom, Long roomIdTo, Date changeTimestamp, String changeRemark, Date createDate) {
       this.resvId = resvId;
       this.roomIdFrom = roomIdFrom;
       this.roomIdTo = roomIdTo;
       this.changeTimestamp = changeTimestamp;
       this.changeRemark = changeRemark;
       this.createDate = createDate;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="log_id", unique=true, nullable=false)
    public Long getLogId() {
        return this.logId;
    }
    
    public void setLogId(Long logId) {
        this.logId = logId;
    }
    
    @Column(name="resv_id")
    public String getResvId() {
        return this.resvId;
    }
    
    public void setResvId(String resvId) {
        this.resvId = resvId;
    }
    
    @Column(name="room_id_from")
    public Long getRoomIdFrom() {
        return this.roomIdFrom;
    }
    
    public void setRoomIdFrom(Long roomIdFrom) {
        this.roomIdFrom = roomIdFrom;
    }
    
    @Column(name="room_id_to")
    public Long getRoomIdTo() {
        return this.roomIdTo;
    }
    
    public void setRoomIdTo(Long roomIdTo) {
        this.roomIdTo = roomIdTo;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="change_timestamp", length=19)
    public Date getChangeTimestamp() {
        return this.changeTimestamp;
    }
    
    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }
    
    @Column(name="change_remark", length=300)
    public String getChangeRemark() {
        return this.changeRemark;
    }
    
    public void setChangeRemark(String changeRemark) {
        this.changeRemark = changeRemark;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_date", length=19)
    public Date getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }




}


