package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.OrderBy;


/**
 * The persistent class for the permit_card_master database table.
 * 
 */
@Entity
@Table(name="helper_pass_type")
@NamedQuery(name="HelperPassType.findAll", query="SELECT p FROM HelperPassType p")
public class HelperPassType implements Serializable {
	private static final long serialVersionUID = 1L;

	
	//type_id, type_name, status, start_date, expiry_date, create_date, create_by, update_date, update_by
	@JsonIgnore
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="type_id", unique=true, nullable=false)
	private Long typeId;
	
	@Column(name="type_name", length=250)
	private String typeName;
	
	@Column(name="status", length=10)
	private String status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start_date")
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="expiry_date")
	private Date expiryDate;
	
	@Column(name="create_date")
	private Timestamp createDate;
	
	@Column(name="create_by", length=50)
	private String createBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="update_by", length=50)
	private String updateBy;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	@OneToMany(mappedBy="helperPassType" , cascade=CascadeType.ALL , orphanRemoval = true, fetch=FetchType.EAGER)
	@OrderBy(clause = "terminalId asc")
	private Set<HelperPassPermission> helperPassPermission;
	
	
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public Long getTypeId()
	{
		return typeId;
	}

	public void setTypeId(Object typeId)
	{
		this.typeId = (typeId!=null ? NumberUtils.toLong(typeId.toString()):null);
	}

	public String getTypeName()
	{
		return typeName;
	}

	public void setTypeName(String typeName)
	{
		this.typeName = typeName;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getExpiryDate()
	{
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate)
	{
		this.expiryDate = expiryDate;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	
	public Set<HelperPassPermission> getHelperPassPermission()
	{
		return helperPassPermission;
	}

	public void setHelperPassPermission(Set<HelperPassPermission> helperPassPermission)
	{
		this.helperPassPermission = helperPassPermission;
	}

	public HelperPassType() {
	}

}