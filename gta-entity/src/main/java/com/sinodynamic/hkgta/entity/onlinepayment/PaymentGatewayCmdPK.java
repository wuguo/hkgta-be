package com.sinodynamic.hkgta.entity.onlinepayment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the payment_gateway_cmd database table.
 * 
 */
@Embeddable
public class PaymentGatewayCmdPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="gateway_id", insertable=false, updatable=false)
	private Long gatewayId;

	@Column(name="cmd_id")
	private String cmdId;

	public PaymentGatewayCmdPK() {
	}
	public PaymentGatewayCmdPK(Long gatewayId, String cmdId) {
		this.gatewayId = gatewayId;
		this.cmdId = cmdId;
	}
	public Long getGatewayId() {
		return this.gatewayId;
	}
	public void setGatewayId(Long gatewayId) {
		this.gatewayId = gatewayId;
	}
	public String getCmdId() {
		return this.cmdId;
	}
	public void setCmdId(String cmdId) {
		this.cmdId = cmdId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PaymentGatewayCmdPK)) {
			return false;
		}
		PaymentGatewayCmdPK castOther = (PaymentGatewayCmdPK)other;
		return 
			(this.gatewayId.equals( castOther.gatewayId))
			&& this.cmdId.equals(castOther.cmdId);
	}

	public int hashCode() {
		final Long prime = 31l;
		Long hash = 17l;
		hash = hash * prime + this.gatewayId;
		hash = hash * prime + this.cmdId.hashCode();
		
		return hash.intValue();
	}
}