package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;


/**
 * The persistent class for the customer_service_acc database table.
 * 
 */
@Entity
@Table(name="customer_service_acc")
@NamedQuery(name="CustomerServiceAcc.findAll", query="SELECT c FROM CustomerServiceAcc c")
public class CustomerServiceAcc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="acc_no", unique=true, nullable=false)
	private Long accNo;

	@Column(name="acc_cat", length=10)
	private String accCat;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_date")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="expiry_date")
	private Date expiryDate;

	@Column(name="period_code", length=10)
	private String periodCode;

	@Column(length=2000)
	private String remark;

	@Column(name="settlement_method_code", length=10)
	private String settlementMethodCode;

	@Column(name="statement_delivery_by", length=10)
	private String statementDeliveryBy;

	@Column(length=10)
	private String status;

	@Column(name="order_no")
	private Long orderNo;
	
	//bi-directional many-to-one association to Member
	@Column(name="customer_id", nullable=false)
	private Long customerId;

	@Version
	@Column(name="ver_no", nullable = false)
	private Long verNo;
	
	//bi-directional many-to-one association to CustomerServiceSubscribe
	//@OneToMany(mappedBy="customerServiceAcc")
	@Transient
	private List<CustomerServiceSubscribe> customerServiceSubscribes;

	public CustomerServiceAcc() {
	}

	
	public Long getAccNo() {
		return this.accNo;
	}

	public void setAccNo(Long accNo) {
		this.accNo = accNo;
	}

	public String getAccCat() {
		return this.accCat;
	}

	public void setAccCat(String accCat) {
		this.accCat = accCat;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPeriodCode() {
		return this.periodCode;
	}

	public void setPeriodCode(String periodCode) {
		this.periodCode = periodCode;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSettlementMethodCode() {
		return this.settlementMethodCode;
	}

	public void setSettlementMethodCode(String settlementMethodCode) {
		this.settlementMethodCode = settlementMethodCode;
	}

	public String getStatementDeliveryBy() {
		return this.statementDeliveryBy;
	}

	public void setStatementDeliveryBy(String statementDeliveryBy) {
		this.statementDeliveryBy = statementDeliveryBy;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public List<CustomerServiceSubscribe> getCustomerServiceSubscribes() {
		return this.customerServiceSubscribes;
	}

	public void setCustomerServiceSubscribes(List<CustomerServiceSubscribe> customerServiceSubscribes) {
		this.customerServiceSubscribes = customerServiceSubscribes;
	}

	public CustomerServiceSubscribe addCustomerServiceSubscribe(CustomerServiceSubscribe customerServiceSubscribe) {
		getCustomerServiceSubscribes().add(customerServiceSubscribe);
		customerServiceSubscribe.setCustomerServiceAcc(this);

		return customerServiceSubscribe;
	}

	public CustomerServiceSubscribe removeCustomerServiceSubscribe(CustomerServiceSubscribe customerServiceSubscribe) {
		getCustomerServiceSubscribes().remove(customerServiceSubscribe);
		customerServiceSubscribe.setCustomerServiceAcc(null);

		return customerServiceSubscribe;
	}

}