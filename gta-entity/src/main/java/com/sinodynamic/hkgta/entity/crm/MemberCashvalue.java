package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;


/**
 * The persistent class for the member_cashvalue database table.
 * 
 */
@Entity
@Table(name="member_cashvalue")
@NamedQuery(name="MemberCashvalue.findAll", query="SELECT m FROM MemberCashvalue m")
public class MemberCashvalue extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="customer_id", unique=true, nullable=false)
	@EncryptFieldInfo  
	private Long customerId;

	@Column(name="available_balance", precision=10, scale=2)
	private BigDecimal availableBalance;

	@Column(name="exchg_factor", precision=10, scale=4)
	private BigDecimal exchgFactor;

	@Temporal(TemporalType.DATE)
	@Column(name="expiry_date")
	private Date expiryDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="initial_date")
	private Date initialDate;

	@Column(name="initial_value", precision=10, scale=2)
	private BigDecimal initialValue;

	@Column(name="internal_remark", length=300)
	private String internalRemark;
	
	@Column(name="update_by", length=50)
	private String updateBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

//	@Column(name="virtual_acc_no", length=100)
//	private String virtualAccNo;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long version;
	
	
	@OneToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="customer_id", nullable=false, insertable=false, updatable=false)
	@JsonIgnore
	private Member member;

	public MemberCashvalue() {
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Object version) {
		/*if(version instanceof BigInteger){
			this.version = ((BigInteger) version).longValue();
		}else if(version instanceof Integer){
			this.version = ((Integer) version).longValue();
		}else if(version instanceof String){
			this.version = Long.valueOf((String) version);
		}else{
			this.version = (Long) version;
		}*/
		this.version = (version!=null ? NumberUtils.toLong(version.toString()):null);
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public BigDecimal getAvailableBalance() {
		return this.availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getExchgFactor() {
		return this.exchgFactor;
	}

	public void setExchgFactor(BigDecimal exchgFactor) {
		this.exchgFactor = exchgFactor;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getInitialDate() {
		return this.initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public BigDecimal getInitialValue() {
		return this.initialValue;
	}

	public void setInitialValue(BigDecimal initialValue) {
		this.initialValue = initialValue;
	}

	public String getInternalRemark() {
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	

//	public String getVirtualAccNo() {
//		return virtualAccNo;
//	}
//
//	public void setVirtualAccNo(String virtualAccNo) {
//		this.virtualAccNo = virtualAccNo;
//	}

}