package com.sinodynamic.hkgta.entity.quartz;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "hkgtaext.QRTZ_TRIGGERS")
public class QrtzTriggers implements Serializable {
	@Id
	@Column(name="SCHED_NAME", insertable=false, updatable=false)
	private String schedName;

	@Id
	@Column(name="TRIGGER_NAME")
	private String triggerName;

	@Id	
	@Column(name="TRIGGER_GROUP")
	private String triggerGroup;
	
	@Column(name="CALENDAR_NAME")
	private String calendarName;

	private String description;

	@Column(name="END_TIME")
	private Long endTime;

	@javax.persistence.Lob
	@Column(name="JOB_DATA")
	private byte[] jobData;

	@Column(name="JOB_GROUP")
	private String jobGroup;

		@Column(name="JOB_NAME")
	private String jobName;

	@Column(name="MISFIRE_INSTR")
	private Integer misfireInstr;

	@Column(name="NEXT_FIRE_TIME")
	private Long nextFireTime;

	@Column(name="PREV_FIRE_TIME")
	private Long prevFireTime;

	private int priority;

	@Column(name="START_TIME")
	private Long startTime;

	@Column(name="TRIGGER_STATE")
	private String triggerState;

	@Column(name="TRIGGER_TYPE")
	private String triggerType;


	public String getSchedName() {
		return this.schedName;
	}
	
	public void setSchedName(String schedName) {
		this.schedName = schedName;
	}
	
	public String getTriggerName() {
		return this.triggerName;
	}
	
	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}
	
	public String getTriggerGroup() {
		return this.triggerGroup;
	}
	public void setTriggerGroup(String triggerGroup) {
		this.triggerGroup = triggerGroup;
	}
	

	public String getCalendarName() {
		return this.calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public byte[] getJobData() {
		return this.jobData;
	}

	public void setJobData(byte[] jobData) {
		this.jobData = jobData;
	}

	public String getJobGroup() {
		return this.jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Integer getMisfireInstr() {
		return this.misfireInstr;
	}

	public void setMisfireInstr(Integer misfireInstr) {
		this.misfireInstr = misfireInstr;
	}

	public Long getNextFireTime() {
		return this.nextFireTime;
	}

	public void setNextFireTime(Long nextFireTime) {
		this.nextFireTime = nextFireTime;
	}

	public Long getPrevFireTime() {
		return this.prevFireTime;
	}

	public void setPrevFireTime(Long prevFireTime) {
		this.prevFireTime = prevFireTime;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Long getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public String getTriggerState() {
		return this.triggerState;
	}

	public void setTriggerState(String triggerState) {
		this.triggerState = triggerState;
	}

	public String getTriggerType() {
		return this.triggerType;
	}

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

}
