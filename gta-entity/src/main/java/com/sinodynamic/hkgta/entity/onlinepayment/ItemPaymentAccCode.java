package com.sinodynamic.hkgta.entity.onlinepayment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="item_payment_acc_code")
public class ItemPaymentAccCode  implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="sys_id")
	private Long sysId;

	@Column(name="account_code")
	private String accountCode;

	@Column(name="category_code")
	private String categoryCode;

	@Column(name="payment_method_code")
	private String paymentMethodCode;

	@Column(name="description")
	private String description;
	
	@Column(name="status")
	private String status;

	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="create_by")
	private String createBy;

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
}
