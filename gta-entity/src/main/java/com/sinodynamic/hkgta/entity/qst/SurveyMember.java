package com.sinodynamic.hkgta.entity.qst;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "survey_member")
public class SurveyMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	private Long sysId;

	@Column(name = "customer_id")
	private Long customerId;

	@Column(name = "survey_id", length = 11)
	private Long surveyId;

	@Column(name = "utilize_count", length = 11)
	private Long utilizeCount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_hit_date")
	private Date startHitDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_hit_date")
	private Date lastHitDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "survey_sent_date")
	private Date surveySentDate;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;
	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Version
	@Column(name = "ver_no", nullable = false)
	private Long verNo;

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

	public Long getUtilizeCount() {
		return utilizeCount;
	}

	public void setUtilizeCount(Long utilizeCount) {
		this.utilizeCount = utilizeCount;
	}

	public Date getStartHitDate() {
		return startHitDate;
	}

	public void setStartHitDate(Date startHitDate) {
		this.startHitDate = startHitDate;
	}

	public Date getLastHitDate() {
		return lastHitDate;
	}

	public void setLastHitDate(Date lastHitDate) {
		this.lastHitDate = lastHitDate;
	}

	public Date getSurveySentDate() {
		return surveySentDate;
	}

	public void setSurveySentDate(Date surveySentDate) {
		this.surveySentDate = surveySentDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

}
