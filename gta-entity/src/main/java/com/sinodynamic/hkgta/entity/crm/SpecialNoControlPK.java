package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the special_no_control database table.
 * 
 */
@Embeddable
public class SpecialNoControlPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="code_type", unique=true, nullable=false, length=20)
	private String codeType;

	@Column(name="code_no", unique=true, nullable=false, length=50)
	private String codeNo;

	public SpecialNoControlPK() {
	}
	public String getCodeType() {
		return this.codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	public String getCodeNo() {
		return this.codeNo;
	}
	public void setCodeNo(String codeNo) {
		this.codeNo = codeNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SpecialNoControlPK)) {
			return false;
		}
		SpecialNoControlPK castOther = (SpecialNoControlPK)other;
		return 
			this.codeType.equals(castOther.codeType)
			&& this.codeNo.equals(castOther.codeNo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.codeType.hashCode();
		hash = hash * prime + this.codeNo.hashCode();
		
		return hash;
	}
}