package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the member_plan_facility_right database table.
 * 
 */
@Embeddable
public class MemberPlanFacilityRightPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="customer_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Long customerId;

	@Column(name="service_plan", insertable=false, updatable=false, unique=true, nullable=false)
	private Long servicePlan;

	@Column(name="facility_type_code", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String facilityTypeCode;

	public MemberPlanFacilityRightPK() {
	}
	public Long getCustomerId() {
		return this.customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
	public Long getServicePlan() {
		return this.servicePlan;
	}
	
	public void setServicePlan(Long servicePlan) {
		this.servicePlan = servicePlan;
	}
	public String getFacilityTypeCode() {
		return this.facilityTypeCode;
	}
	public void setFacilityTypeCode(String facilityTypeCode) {
		this.facilityTypeCode = facilityTypeCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MemberPlanFacilityRightPK)) {
			return false;
		}
		MemberPlanFacilityRightPK castOther = (MemberPlanFacilityRightPK)other;
		return 
			this.customerId.equals(castOther.customerId)
			&& (this.servicePlan.equals(castOther.servicePlan))
			&& this.facilityTypeCode.equals(castOther.facilityTypeCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.customerId.hashCode();
		hash = (int) (hash * prime + this.servicePlan);
		hash = hash * prime + this.facilityTypeCode.hashCode();
		
		return hash;
	}
}