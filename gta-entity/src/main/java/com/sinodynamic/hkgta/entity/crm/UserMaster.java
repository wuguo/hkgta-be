package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the user_master database table.
 * 
 */
@Entity
@Table(name="user_master")
@NamedQuery(name="UserMaster.findAll", query="SELECT u FROM UserMaster u")
public class UserMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id", unique=true, nullable=false, length=50)
	private String userId;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Date createDate;

	@Column(name="default_module", length=5)
	private String defaultModule;

	@Column(name="login_id", length=250)
	private String loginId;

	@Column(length=60)
	private String nickname;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="passwd_change_date")
	private Date passwdChangeDate;

	@Column(length=60)
	private String password;

	@Column(name="session_key", length=50)
	private String sessionKey;

	@Column(length=10)
	private String status;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Column(name="user_type", length=10)
	private String userType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_login_fail_time")
	private Date lastLoginFailTime;
	
	@Column(name="login_fail_count")
	private Long loginFailCount;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public Date getLastLoginFailTime()
	{
		return lastLoginFailTime;
	}

	public void setLastLoginFailTime(Date lastLoginFailTime)
	{
		this.lastLoginFailTime = lastLoginFailTime;
	}

	public Long getLoginFailCount()
	{
		return loginFailCount;
	}

	public void setLoginFailCount(Long loginFailCount)
	{
		this.loginFailCount = loginFailCount;
	}

	public UserMaster() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDefaultModule() {
		return this.defaultModule;
	}

	public void setDefaultModule(String defaultModule) {
		this.defaultModule = defaultModule;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getNickname() {
		return this.nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Date getPasswdChangeDate() {
		return this.passwdChangeDate;
	}

	public void setPasswdChangeDate(Date passwdChangeDate) {
		this.passwdChangeDate = passwdChangeDate;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSessionKey() {
		return this.sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserType() {
		return this.userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
}