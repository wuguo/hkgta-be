package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for the staff_card database table.
 * 
 */
@Entity
@Table(name = "internal_permit_card")
@NamedQuery(name = "InternalPermitCard.findAll", query = "SELECT s FROM InternalPermitCard s")
public class InternalPermitCard implements Serializable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "card_id", unique = true, nullable = false)
	private Long cardId;

	@Column(name = "factory_serial_no", length = 50)
	private String factorySerialNo;

	@Column(name = "status", length = 10)
	private String status;

	@Column(name = "card_purpose", length = 10)
	private String cardPurpose;

	@Column(name = "card_name", length = 100)
	private String cardName;

	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;
	
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	public String getCardName()
	{
		return cardName;
	}

	public void setCardName(String cardName)
	{
		this.cardName = cardName;
	}

	@Column(name = "staff_user_id", length = 50)
	private String staffUserId;

	@Column(name = "contractor_id")
	private Long contractorId;

	@Column(name = "effective_date")
	private Date effectiveDate;

	@Column(name = "expiry_date")
	private Date expiryDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_update_date")
	private Date statusUpdateDate;

	@Column(name = "update_by")
	private String updateBy;

	public Long getCardId()
	{
		return cardId;
	}

	public void setCardId(Long cardId)
	{
		this.cardId = cardId;
	}

	public String getFactorySerialNo()
	{
		return factorySerialNo;
	}

	public void setFactorySerialNo(String factorySerialNo)
	{
		this.factorySerialNo = factorySerialNo;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getCardPurpose()
	{
		return cardPurpose;
	}

	public void setCardPurpose(String cardPurpose)
	{
		this.cardPurpose = cardPurpose;
	}

	public String getStaffUserId()
	{
		return staffUserId;
	}

	public void setStaffUserId(String staffUserId)
	{
		this.staffUserId = staffUserId;
	}

	public Long getContractorId()
	{
		return contractorId;
	}

	public void setContractorId(Long contractorId)
	{
		this.contractorId = contractorId;
	}

	public Date getEffectiveDate()
	{
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate)
	{
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate()
	{
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate)
	{
		this.expiryDate = expiryDate;
	}

	public Date getStatusUpdateDate()
	{
		return statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate)
	{
		this.statusUpdateDate = statusUpdateDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

}
