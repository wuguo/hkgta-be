package com.sinodynamic.hkgta.entity.bi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "bi_oasis_flex_file")
public class BiOasisFlexFile implements Serializable {

	private static final long serialVersionUID = -7866000693071195679L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	private Long sysId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "oasis_trans_date")
	private Date oasisTransDate;

	@Column(name = "oasis_tran_code")
	private String oasisTranCode;

	@Column(name = "amount", precision = 10, scale = 2)
	private BigDecimal amount;

	@Column(name = "file_name")
	private String fileName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public Date getOasisTransDate() {
		return oasisTransDate;
	}

	public void setOasisTransDate(Date oasisTransDate) {
		this.oasisTransDate = oasisTransDate;
	}

	public String getOasisTranCode() {
		return oasisTranCode;
	}

	public void setOasisTranCode(String oasisTranCode) {
		this.oasisTranCode = oasisTranCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
