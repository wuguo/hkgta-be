package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the facility_utilization_rate_time database table.
 * 
 */
@Entity
@Table(name="facility_utilization_rate_time")
@NamedQuery(name="FacilityUtilizationRateTime.findAll", query="SELECT f FROM FacilityUtilizationRateTime f")
public class FacilityUtilizationRateTime implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="time_id", unique=true, nullable=false)
	private Long timeId;
	
	@Column(name="facility_type", length=10, nullable=false)
	private String facilityType;
	
	@Column(name="facility_subtype_id", length=10)
	private String facilitySubtypeId;
	
	@Column(name="facility_no", length=20)
	private Long facilityNo;
	
	@Column(name="facility_date_id",length=20)
	private Long facilityDateId;

	@Column(name="week_day", length=2, nullable=false)
	private String weekDay;
	
	@Column(name="rate_type", length=10)
	private String rateType;

	@Column(name="begin_time", nullable=false)
	private int beginTime;

	@Column(name="end_time")
	private int endTime;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	public FacilityUtilizationRateTime() {
	}

	public Long getTimeId()
	{
		return timeId;
	}

	public void setTimeId(Long timeId)
	{
		this.timeId = timeId;
	}

	public String getRateType()
	{
		return rateType;
	}

	public void setRateType(String rateType)
	{
		this.rateType = rateType;
	}

	public int getBeginTime()
	{
		return beginTime;
	}

	public void setBeginTime(int beginTime)
	{
		this.beginTime = beginTime;
	}

	public int getEndTime()
	{
		return endTime;
	}

	public void setEndTime(int endTime)
	{
		this.endTime = endTime;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}


	public String getFacilityType()
	{
		return facilityType;
	}

	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}


	public String getWeekDay()
	{
		return weekDay;
	}

	public void setWeekDay(String weekDay)
	{
		this.weekDay = weekDay;
	}

	public Long getFacilityNo()
	{
		return facilityNo;
	}

	public void setFacilityNo(Long facilityNo)
	{
		this.facilityNo = facilityNo;
	}

	public Long getFacilityDateId()
	{
		return facilityDateId;
	}

	public void setFacilityDateId(Long facilityDateId)
	{
		this.facilityDateId = facilityDateId;
	}

	public String getFacilitySubtypeId()
	{
		return facilitySubtypeId;
	}

	public void setFacilitySubtypeId(String facilitySubtypeId)
	{
		this.facilitySubtypeId = facilitySubtypeId;
	}

}