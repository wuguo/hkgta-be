package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the customer_enroll_po database table.
 * 
 */
@Embeddable
public class CustomerEnrollPoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="order_det_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Long orderDetId;

	@Column(name="enroll_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Long enrollId;

	public CustomerEnrollPoPK() {
	}
	public Long getOrderDetId() {
		return this.orderDetId;
	}
	public void setOrderDetId(Long orderDetId) {
		this.orderDetId = orderDetId;
	}
	public Long getEnrollId() {
		return this.enrollId;
	}
	public void setEnrollId(Long enrollId) {
		this.enrollId = enrollId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CustomerEnrollPoPK)) {
			return false;
		}
		CustomerEnrollPoPK castOther = (CustomerEnrollPoPK)other;
		return 
			this.orderDetId.equals(castOther.orderDetId)
			&& this.enrollId.equals(castOther.enrollId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.orderDetId.hashCode();
		hash = hash * prime + this.enrollId.hashCode();
		
		return hash;
	}
}