package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the present_material database table.
 * 
 */
@Entity
@Table(name = "server_folder_mapping")
@NamedQuery(name = "ServerFolderMapping.findAll", query = "SELECT p FROM ServerFolderMapping p")
public class ServerFolderMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "folder_id", unique = true, nullable = false)
	private Long folderId;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Column(name = "display_name", length = 250)
	private String displayName;

	@Column(name = "path_prefix_key", length = 50)
	private String pathPrefixKey;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 50)
	private Date createDate;

//	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	public Long getFolderId() {
		return folderId;
	}

	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPathPrefixKey() {
		return pathPrefixKey;
	}

	public void setPathPrefixKey(String pathPrefixKey) {
		this.pathPrefixKey = pathPrefixKey;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public ServerFolderMapping() {
	}

}