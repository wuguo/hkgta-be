package com.sinodynamic.hkgta.entity.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the customer_order_det database table.
 * 
 */
@Entity
@Table(name="customer_order_det")
@NamedQuery(name="CustomerOrderDet.findAll", query="SELECT c FROM CustomerOrderDet c")
public class CustomerOrderDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="order_det_id", unique=true, nullable=false)
	private Long orderDetId;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(name="item_remark", length=100)
	private String itemRemark;

	@Column(name="item_total_amout", precision=10, scale=2)
	private BigDecimal itemTotalAmout;

	@Column(name="order_qty")
	private Long orderQty;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="ext_ref_no")
	private String extRefNo;

	//bi-directional many-to-one association to CustomerOrderHd
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.ALL})
	@JoinColumn(name="order_no", nullable=false)
	private CustomerOrderHd customerOrderHd;
	
	//bi-directional many-to-one association to PosServiceItemPrice
/*	@ManyToOne
	@JoinColumn(name="item_no", nullable=false)
	private PosServiceItemPrice posServiceItemPrice;*/

/*	public PosServiceItemPrice getPosServiceItemPrice() {
		return posServiceItemPrice;
	}

	public void setPosServiceItemPrice(PosServiceItemPrice posServiceItemPrice) {
		this.posServiceItemPrice = posServiceItemPrice;
	}*/

	@Column(name="item_no")
	private String itemNo;
	@Version
	@Column(name = "ver_no", nullable=false)
	private Long verNo;
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Object verNo) {
		this.verNo = verNo != null ? NumberUtils.toLong(verNo.toString()) : null;
	}
	public CustomerOrderDet() {
	}

	public Long getOrderDetId() {
		return this.orderDetId;
	}

	public void setOrderDetId(Object orderDetId) {
		this.orderDetId = orderDetId != null ? NumberUtils.toLong(orderDetId.toString()) : null;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getItemRemark() {
		return this.itemRemark;
	}

	public void setItemRemark(String itemRemark) {
		this.itemRemark = itemRemark;
	}

	public BigDecimal getItemTotalAmout() {
		return this.itemTotalAmout;
	}

	public void setItemTotalAmout(BigDecimal itemTotalAmout) {
		this.itemTotalAmout = itemTotalAmout;
	}

	public Long getOrderQty() {
		return this.orderQty;
	}

	public void setOrderQty(Object orderQty) {
		this.orderQty = orderQty != null ? NumberUtils.toLong(orderQty.toString()) : null;
	}
	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getExtRefNo() {
		return extRefNo;
	}

	public void setExtRefNo(String extRefNo) {
		this.extRefNo = extRefNo;
	}

	public CustomerOrderHd getCustomerOrderHd() {
		return this.customerOrderHd;
	}

	public void setCustomerOrderHd(CustomerOrderHd customerOrderHd) {
		this.customerOrderHd = customerOrderHd;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	
	
}