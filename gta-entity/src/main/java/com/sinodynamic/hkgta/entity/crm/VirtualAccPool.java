package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "virtual_acc_pool")
public class VirtualAccPool implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "va_acc_no", unique = true, nullable = false)
	private String vaAccNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "bank_client_id")
	private Long bankClentId;

	@Column(name = "bank_id")
	private String bankId;

	@Column(name = "client_type")
	private String clientType;

	@Column(name = "status")
	private String status;
	
	@Version
    @Column(nullable = false,name="ver_no")
	private Long versionNo;

	public String getVaAccNo() {
		return vaAccNo;
	}

	public void setVaAccNo(String vaAccNo) {
		this.vaAccNo = vaAccNo;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getBankClentId() {
		return bankClentId;
	}

	public void setBankClentId(Long bankClentId) {
		this.bankClentId = bankClentId;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(Long versionNo) {
		this.versionNo = versionNo;
	}
	
	

}
