package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the customer_enrollment_log database table.
 * 
 */
@Entity
@Table(name="customer_enrollment_log")
@NamedQuery(name="CustomerEnrollmentLog.findAll", query="SELECT c FROM CustomerEnrollmentLog c")
public class CustomerEnrollmentLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id")
	private Long sysId;

	@Column(name="enroll_id")
	private Long enrollId;

	@Column(name="sales_follow_by")
	private String salesFollowBy;

	@Column(name="status_from")
	private String statusFrom;

	@Column(name="status_to")
	private String statusTo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="status_update_date")
	private Date statusUpdateDate;

	@Column(name="subscribe_contract_length")
	private Long subscribeContractLength;

	@Column(name="subscribe_plan_no")
	private Long subscribePlanNo;

	public CustomerEnrollmentLog() {
	}

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Object sysId) {
		/*if(sysId instanceof Long){
			this.sysId = ((Long) sysId).longValue();
		}else if(sysId instanceof Integer){
			this.sysId = ((Integer) sysId).longValue();
		}else if(sysId instanceof String){
			this.sysId = Long.valueOf((String) sysId);
		}else{
			this.sysId = (Long) sysId;
		}*/
		this.sysId = (sysId!=null ? NumberUtils.toLong(sysId.toString()):null);
	}

	public Long getEnrollId() {
		return enrollId;
	}

	public void setEnrollId(Object enrollId) {
		if(enrollId instanceof Long){
			this.enrollId = ((Long) enrollId).longValue();
		}else if(enrollId instanceof Integer){
			this.enrollId = ((Integer) enrollId).longValue();
		}else if(enrollId instanceof String){
			this.enrollId = Long.valueOf((String) enrollId);
		}else{
			this.enrollId = (Long) enrollId;
		}
	}

	public String getSalesFollowBy() {
		return salesFollowBy;
	}

	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}

	public String getStatusFrom() {
		return statusFrom;
	}

	public void setStatusFrom(String statusFrom) {
		this.statusFrom = statusFrom;
	}

	public String getStatusTo() {
		return statusTo;
	}

	public void setStatusTo(String statusTo) {
		this.statusTo = statusTo;
	}

	public Date getStatusUpdateDate() {
		return statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}

	public Long getSubscribeContractLength() {
		return subscribeContractLength;
	}

	public void setSubscribeContractLength(Object subscribeContractLength) {
		/*if(subscribeContractLength instanceof Long){
			this.subscribeContractLength = ((Long) subscribeContractLength).longValue();
		}else if(subscribeContractLength instanceof Integer){
			this.subscribeContractLength = ((Integer) subscribeContractLength).longValue();
		}else if(subscribeContractLength instanceof String){
			this.subscribeContractLength = Long.valueOf((String) subscribeContractLength);
		}else{
			this.subscribeContractLength = (Long) subscribeContractLength;
		}*/
		this.subscribeContractLength = (subscribeContractLength!=null ? NumberUtils.toLong(subscribeContractLength.toString()):null);
	}

	public Long getSubscribePlanNo() {
		return subscribePlanNo;
	}

	public void setSubscribePlanNo(Object subscribePlanNo) {
		/*if(subscribePlanNo instanceof Long){
			this.subscribePlanNo = ((Long) subscribePlanNo).longValue();
		}else if(subscribePlanNo instanceof Integer){
			this.subscribePlanNo = ((Integer) subscribePlanNo).longValue();
		}else if(subscribePlanNo instanceof String){
			this.subscribePlanNo = Long.valueOf((String) subscribePlanNo);
		}else{
			this.subscribePlanNo = (Long) subscribePlanNo;
		}*/
		this.subscribePlanNo = (subscribePlanNo!=null ? NumberUtils.toLong(subscribePlanNo.toString()):null);
	}


}