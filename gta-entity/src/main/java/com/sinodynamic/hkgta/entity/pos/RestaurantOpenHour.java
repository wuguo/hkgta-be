package com.sinodynamic.hkgta.entity.pos;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Type;


@Entity
@Table(name = "restaurant_open_hour")
public class RestaurantOpenHour implements Serializable
{
	private static final long serialVersionUID = 5911666142185479367L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	private Long sysId;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "restaurant_id")
	private RestaurantMaster restaurantMaster;
	
	private String weekday;
	
	@Column(name = "open_hour")
	private Long openHour;
	
	@Column(name = "close_hour")
	private Long closeHour;
	
	@Type(type="yes_no")
	@Column(name = "allow_online_order")
	private Boolean allowOnlineOrder;
	
	@Column(name = "allow_order_time_from")
	private Long allowOrderTimeFrom;
	
	@Column(name = "allow_order_time_to")
	private Long allowOrderTimeTo;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;

	public Long getSysId()
	{
		return sysId;
	}

	public void setSysId(Long sysId)
	{
		this.sysId = sysId;
	}

	public String getWeekday()
	{
		return weekday;
	}

	public void setWeekday(String weekday)
	{
		this.weekday = weekday;
	}

	public Long getOpenHour()
	{
		return openHour;
	}

	public void setOpenHour(Long openHour)
	{
		this.openHour = openHour;
	}

	public Long getCloseHour()
	{
		return closeHour;
	}

	public void setCloseHour(Long closeHour)
	{
		this.closeHour = closeHour;
	}

	public Long getAllowOrderTimeFrom()
	{
		return allowOrderTimeFrom;
	}

	public void setAllowOrderTimeFrom(Long allowOrderTimeFrom)
	{
		this.allowOrderTimeFrom = allowOrderTimeFrom;
	}

	public Long getAllowOrderTimeTo()
	{
		return allowOrderTimeTo;
	}

	public void setAllowOrderTimeTo(Long allowOrderTimeTo)
	{
		this.allowOrderTimeTo = allowOrderTimeTo;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public Long getVerNo()
	{
		return verNo;
	}

	public void setVerNo(Long verNo)
	{
		this.verNo = verNo;
	}

	public Boolean getAllowOnlineOrder() {
		return allowOnlineOrder;
	}

	public void setAllowOnlineOrder(Boolean allowOnlineOrder) {
		this.allowOnlineOrder = allowOnlineOrder;
	}

	public RestaurantMaster getRestaurantMaster()
	{
		return restaurantMaster;
	}

	public void setRestaurantMaster(RestaurantMaster restaurantMaster)
	{
		this.restaurantMaster = restaurantMaster;
	}

}
