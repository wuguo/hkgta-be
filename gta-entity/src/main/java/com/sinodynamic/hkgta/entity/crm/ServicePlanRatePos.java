package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
/**
 * 
 * @author Zero_Wang
 * @date   May 13, 2015
 */
@Entity
@Table(name="service_plan_rate_pos")
public class ServicePlanRatePos implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="rate_id", unique=true, nullable=false)
	private Long rateId;
	@Column(name="rate_type")
	private String rateType;
	@Column(name="description")
	private String description;
	@Column(name="weekly_repeat_bit")
	private String weeklyRepeatBit;
	@Column(name="serv_pos_id")
	private Long servPosId;
	@Column(name="pos_item_no")
	private String posItemNo;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	//bi-directional many-to-one association to ServicePlanPo
	@OneToMany(cascade=CascadeType.ALL, mappedBy="planRatePos")
	Set<ServicePlanDatePos> planDatePosSet = new HashSet<ServicePlanDatePos>();
	
	
	public Long getVerNo() {
		return verNo;
	}
	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	public Set<ServicePlanDatePos> getPlanDatePosSet() {
		return planDatePosSet;
	}
	public void setPlanDatePosSet(Set<ServicePlanDatePos> planDatePosSet) {
		this.planDatePosSet = planDatePosSet;
	}
	public Long getRateId() {
		return rateId;
	}
	public void setRateId(Long rateId) {
		this.rateId = rateId;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getWeeklyRepeatBit() {
		return weeklyRepeatBit;
	}
	public void setWeeklyRepeatBit(String weeklyRepeatBit) {
		this.weeklyRepeatBit = weeklyRepeatBit;
	}
	public Long getServPosId() {
		return servPosId;
	}
	public void setServPosId(Long servPosId) {
		this.servPosId = servPosId;
	}
	public String getPosItemNo() {
		return posItemNo;
	}
	public void setPosItemNo(String posItemNo) {
		this.posItemNo = posItemNo;
	}
	
	
	
	
}
