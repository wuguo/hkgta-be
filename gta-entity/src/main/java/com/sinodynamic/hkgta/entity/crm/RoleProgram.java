package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the role_program database table.
 * 
 */
@Entity
@Table(name="role_program")
@NamedQuery(name="RoleProgram.findAll", query="SELECT r FROM RoleProgram r")
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable
public class RoleProgram implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RoleProgramPK id;	

	@Column(name="access_right", length=1)
	private String accessRight;

	//bi-directional many-to-one association to ProgramMaster
	@ManyToOne
	@JoinColumn(name="program_id", nullable=false, insertable=false, updatable=false)
	private ProgramMaster programMaster;

	//bi-directional many-to-one association to RoleMaster
	@ManyToOne
	@JoinColumn(name="role_id", nullable=false, insertable=false, updatable=false)
	private RoleMaster roleMaster;

	public RoleProgram() {
	}

	public RoleProgramPK getId() {
		return this.id;
	}

	public void setId(RoleProgramPK id) {
		this.id = id;
	}

	public String getAccessRight() {
		return this.accessRight;
	}

	public void setAccessRight(String accessRight) {
		this.accessRight = accessRight;
	}

	public ProgramMaster getProgramMaster() {
		return this.programMaster;
	}

	public void setProgramMaster(ProgramMaster programMaster) {
		this.programMaster = programMaster;
	}

	public RoleMaster getRoleMaster() {
		return this.roleMaster;
	}

	public void setRoleMaster(RoleMaster roleMaster) {
		this.roleMaster = roleMaster;
	}

}