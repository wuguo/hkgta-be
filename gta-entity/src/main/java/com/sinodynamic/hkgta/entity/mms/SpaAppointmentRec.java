package com.sinodynamic.hkgta.entity.mms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="spa_appointment_rec")
public class SpaAppointmentRec implements Serializable{

	 private static final long serialVersionUID = 1L;
	 
	    @Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="sys_id", unique=true, nullable=false)
		private Long sysId;
	 
	    @Column(name="ext_appointment_id")
	    private String extAppointmentId;
	    
	    @Column(name="room_id")
	    private Long roomId;
	    
	    @Column(name="ext_service_code")
	    private String extServiceCode;
	    
	    @Column(name="ext_invoice_no")
	    private String extInvoiceNo;	    
		
//	    @Column(name="order_no")
//		private Long orderNo;
	    
	    @Column(name="order_det_id")
		private Long orderDetId;
		
	    @Column(name="start_datetime")
		private Timestamp startDatetime;
		
	    @Column(name="end_datetime")
		private Timestamp endDatetime;
		
	    @Column(name="status")
		private String status;
		
	    @Column(name="checkin_datetime")
		private Timestamp checkinDatetime;
		
	    @Column(name="rebooked")
		private Long rebooked;
		
		@Column(name="service_internal_cost", precision=10, scale=2)
		private BigDecimal serviceInternalCost;
		
		@Column(name="customer_id")
		private Long customerId;
		
		@Column(name="service_name")
		private String serviceName;
		
		@Column(name="service_description")
		private String serviceDescription;
		
		@Column(name="ext_note")
		private String extNote;
		
		@Column(name="service_time")
		private Long serviceTime;
		
		@Column(name="recovery_time")
		private Long recoveryTime;
		
		@Column(name="actual_time")
		private Long actualTime;
		
		@Column(name="create_date")
		private Timestamp createDate;
		
		@Column(name="create_by")
		private String createBy;
		
		@Column(name="update_date")
		private Timestamp updateDate;
		
		@Column(name="update_by")
		private String updateBy;
		
		@Column(name="ext_employee_code")
		private String therapistCode;
		
		@Column(name="ext_employee_first_name")
		private String therapistFirstname;
		
		@Column(name="ext_employee_last_name")
		private String therapistLastname;
		
		@Column(name="ext_sync_timestamp")
		private Timestamp extSyncTimestamp;
		
		@Version
		@Column(name = "ver_no", nullable = false)
		private Long verNo;
		
		public SpaAppointmentRec (){
			
		}

		
		public String getServiceName() {
			return serviceName;
		}


		public void setServiceName(String serviceName) {
			this.serviceName = serviceName;
		}


		public String getTherapistFirstname() {
			return therapistFirstname;
		}


		public void setTherapistFirstname(String therapistFirstname) {
			this.therapistFirstname = therapistFirstname;
		}


		public String getTherapistLastname() {
			return therapistLastname;
		}


		public void setTherapistLastname(String therapistLastname) {
			this.therapistLastname = therapistLastname;
		}


		public Long getOrderDetId() {
			return orderDetId;
		}


		public void setOrderDetId(Long orderDetId) {
			this.orderDetId = orderDetId;
		}


		public String getTherapistCode() {
			return therapistCode;
		}


		public void setTherapistCode(String therapistCode) {
			this.therapistCode = therapistCode;
		}


		public Long getSysId() {
			return sysId;
		}

		public void setSysId(Long sysId) {
			this.sysId = sysId;
		}

		public String getExtAppointmentId() {
			return extAppointmentId;
		}

		public void setExtAppointmentId(String extAppointmentId) {
			this.extAppointmentId = extAppointmentId;
		}

		public Long getRoomId() {
			return roomId;
		}

		public void setRoomId(Long roomId) {
			this.roomId = roomId;
		}

		public String getExtServiceCode() {
			return extServiceCode;
		}

		public void setExtServiceCode(String extServiceCode) {
			this.extServiceCode = extServiceCode;
		}

		public String getExtInvoiceNo() {
			return extInvoiceNo;
		}

		public void setExtInvoiceNo(String extInvoiceNo) {
			this.extInvoiceNo = extInvoiceNo;
		}

		
//		public Long getOrderNo() {
//			return orderNo;
//		}
//
//		public void setOrderNo(Long orderNo) {
//			this.orderNo = orderNo;
//		}

		public Timestamp getStartDatetime() {
			return startDatetime;
		}

		public void setStartDatetime(Timestamp startDatetime) {
			this.startDatetime = startDatetime;
		}

		public Timestamp getEndDatetime() {
			return endDatetime;
		}

		public void setEndDatetime(Timestamp endDatetime) {
			this.endDatetime = endDatetime;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Timestamp getCheckinDatetime() {
			return checkinDatetime;
		}

		public void setCheckinDatetime(Timestamp checkinDatetime) {
			this.checkinDatetime = checkinDatetime;
		}

		public Long getRebooked() {
			return rebooked;
		}

		public void setRebooked(Long rebooked) {
			this.rebooked = rebooked;
		}

		public BigDecimal getServiceInternalCost() {
			return serviceInternalCost;
		}

		public void setServiceInternalCost(BigDecimal serviceInternalCost) {
			this.serviceInternalCost = serviceInternalCost;
		}

		public Long getCustomerId() {
			return customerId;
		}

		public void setCustomerId(Long customerId) {
			this.customerId = customerId;
		}

		public String getServiceDescription() {
			return serviceDescription;
		}

		public void setServiceDescription(String serviceDescription) {
			this.serviceDescription = serviceDescription;
		}

		public String getExtNote() {
			return extNote;
		}

		public void setExtNote(String extNote) {
			this.extNote = extNote;
		}

		public Long getServiceTime() {
			return serviceTime;
		}

		public void setServiceTime(Long serviceTime) {
			this.serviceTime = serviceTime;
		}

		public Long getRecoveryTime() {
			/***
			 * SGG-3373
    		 * add christ 2017-03-06
    		 * current flow ,the MMS payment  don't pass recoverTime param
    		 *  when synchAppointment  happend null NullPointerException ,
    		 *  hander to set recoverTime default
    		 */
			if(null==recoveryTime){
				return 0L;
			}
			return recoveryTime;
		}

		public void setRecoveryTime(Long recoveryTime) 
		{
			/***
			 * SGG-3373
    		 * add christ 2017-03-06
    		 * current flow ,the MMS payment  don't pass recoverTime param
    		 *  when synchAppointment  happend null NullPointerException ,
    		 *  hander to set recoverTime default
    		 */
			this.recoveryTime = recoveryTime;
		}

		public Long getActualTime() {
			return actualTime;
		}

		public void setActualTime(Long actualTime) {
			this.actualTime = actualTime;
		}

		public Timestamp getCreateDate() {
			return createDate;
		}

		public void setCreateDate(Timestamp createDate) {
			this.createDate = createDate;
		}

		public String getCreateBy() {
			return createBy;
		}

		public void setCreateBy(String createBy) {
			this.createBy = createBy;
		}

		public Timestamp getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(Timestamp updateDate) {
			this.updateDate = updateDate;
		}

		public String getUpdateBy() {
			return updateBy;
		}

		public void setUpdateBy(String updateBy) {
			this.updateBy = updateBy;
		}
		

		public Timestamp getExtSyncTimestamp() {
		    return extSyncTimestamp;
		}


		public void setExtSyncTimestamp(Timestamp extSyncTimestamp) {
		    this.extSyncTimestamp = extSyncTimestamp;
		}


		public Long getVerNo() {
			return verNo;
		}

		public void setVerNo(Long verNo) {
			this.verNo = verNo;
		}
	 
		
}
