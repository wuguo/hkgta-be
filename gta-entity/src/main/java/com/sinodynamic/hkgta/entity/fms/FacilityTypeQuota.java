package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for the facility_type_quota database table.
 * 
 */
@Entity
@Table(name = "facility_type_quota")
@NamedQuery(name = "FacilityTypeQuota.findAll", query = "SELECT f FROM FacilityTypeQuota f")
public class FacilityTypeQuota implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false, length =15)
	private Long sysId;

	@Column(name = "facility_type_code" )
	private String facilityTypeCode;

	@Column(name = "subtype_id")
	private String subtypeId;

	@Column(name = "specific_date")
	private Date specificDate;

	@Column(name = "quota", length = 50)
	private Long quota;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public FacilityTypeQuota() {
	}

	public FacilityTypeQuota(Long venueFloor) {
		super();
	}

	public String getCreateBy() {
		return this.createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}


	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getSysId()
	{
		return sysId;
	}

	public void setSysId(Long sysId)
	{
		this.sysId = sysId;
	}

	public String getFacilityTypeCode()
	{
		return facilityTypeCode;
	}

	public void setFacilityTypeCode(String facilityTypeCode)
	{
		this.facilityTypeCode = facilityTypeCode;
	}

	public String getSubtypeId()
	{
		return subtypeId;
	}

	public void setSubtypeId(String subtypeId)
	{
		this.subtypeId = subtypeId;
	}

	public Date getSpecificDate()
	{
		return specificDate;
	}

	public void setSpecificDate(Date specificDate)
	{
		this.specificDate = specificDate;
	}

	public Long getQuota()
	{
		return quota;
	}

	public void setQuota(Long quota)
	{
		this.quota = quota;
	}

	public Long getVerNo()
	{
		return verNo;
	}

	public void setVerNo(Long verNo)
	{
		this.verNo = verNo;
	}

}