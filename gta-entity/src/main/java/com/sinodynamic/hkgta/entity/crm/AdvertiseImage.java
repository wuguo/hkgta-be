package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for table 'hkgta'.'advertise_image'.
 * 
 */
@Entity
@Table(name = "advertise_image")
@NamedQuery(name = "AdvertiseImage.findAll", query = "SELECT a FROM AdvertiseImage a order by a.applicationType, a.displayLocation, a.displayOrder, a.imgId")
public class AdvertiseImage implements Serializable {

	private static final long serialVersionUID = -7866000693071195679L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "img_id", unique = true, nullable = false)
	private Long imgId;

	@Column(name = "server_filename")
	private String serverFilename;

	@Column(name = "application_type")
	private String applicationType; // MAPP or MPORT

	@Column(name = "display_location")
	private String displayLocation; // MSCR or TOPUP

	@Column
	private String status;

	@Column(name = "link_level")
	private Long linkLevel;
	
	@Column
	private String description;
	
	@Column(name = "display_order")
	private Long displayOrder;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Version
	@Column(name = "ver_no", nullable = false)
	private Long verNo;

	public Long getImgId() {
		return imgId;
	}

	public void setImgId(Long imgId) {
		this.imgId = imgId;
	}

	public String getServerFilename() {
		return serverFilename;
	}

	public void setServerFilename(String serverFilename) {
		this.serverFilename = serverFilename;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getDisplayLocation() {
		return displayLocation;
	}

	public void setDisplayLocation(String displayLocation) {
		this.displayLocation = displayLocation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Long displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Long getLinkLevel()
	{
		return linkLevel;
	}

	public void setLinkLevel(Long linkLevel)
	{
		this.linkLevel = linkLevel;
	}

}
