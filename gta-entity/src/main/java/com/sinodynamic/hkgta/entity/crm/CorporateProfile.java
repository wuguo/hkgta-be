package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;


/**
 * The persistent class for the corporate_profile database table.
 * 
 */
@Entity
@Table(name="corporate_profile")
@NamedQuery(name="CorporateProfile.findAll", query="SELECT c FROM CorporateProfile c")
public class CorporateProfile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="corporate_id", unique=true, nullable=false)
	private Long corporateId;

	@Column(name="br_no", nullable=false, length=50)
	private String brNo;
	
	@Column(length=250)
	private String address1;
	
	@Column(length=250)
	private String address2;

	@Column(name="business_nature_code", length=50)
	private String businessNatureCode;

	@Column(name="company_name", length=100)
	private String companyName;

	@Column(name="company_name_nls", length=100)
	private String companyNameNls;

	@Column(name="contact_email")
	private String contactEmail;
	
	@Column(name="contact_person_firstname")
	private String contactPersonFirstname;

	@Column(name="contact_person_lastname")
	private String contactPersonLastname;
	
	@Column(name="contact_phone", length=50)
	private String contactPhone;
	
	@Column(name="contact_phone_mobile")
	private String contactPhoneMobile;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Date createDate;

	@Column(nullable=false, length=30)
	private String district;

	@Column(length=10)
	private String status;

	@Column(name="update_by", length=50)
	private String updateBy;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@OneToOne(mappedBy="corporateProfile")
	private CorporateAdditionAddress corporateAdditionAddress;

	@Transient
	private List<CorporateMember> corporateMembers;

	@Transient
	private List<CorporateServiceAcc> corporateServiceAccs;

	public CorporateProfile() {
	}

	public Long getCorporateId() {
		return this.corporateId;
	}

	public void setCorporateId(Long corporateId) {
		this.corporateId = corporateId;
	}

	public String getBrNo() {
		return this.brNo;
	}

	public void setBrNo(String brNo) {
		this.brNo = brNo;
	}

	public String getBusinessNatureCode() {
		return this.businessNatureCode;
	}

	public void setBusinessNatureCode(String businessNatureCode) {
		this.businessNatureCode = businessNatureCode;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyNameNls() {
		return this.companyNameNls;
	}

	public void setCompanyNameNls(String companyNameNls) {
		this.companyNameNls = companyNameNls;
	}

	public String getContactPhone() {
		return this.contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<CorporateMember> getCorporateMembers() {
		return this.corporateMembers;
	}

	public CorporateAdditionAddress getCorporateAdditionAddress() {
		return corporateAdditionAddress;
	}

	public void setCorporateAdditionAddress(
			CorporateAdditionAddress corporateAdditionAddress) {
		this.corporateAdditionAddress = corporateAdditionAddress;
	}

	public List<CorporateServiceAcc> getCorporateServiceAccs() {
		return corporateServiceAccs;
	}

	public void setCorporateServiceAccs(
			List<CorporateServiceAcc> corporateServiceAccs) {
		this.corporateServiceAccs = corporateServiceAccs;
	}

	public void setCorporateMembers(List<CorporateMember> corporateMembers) {
		this.corporateMembers = corporateMembers;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	
	public String getContactPersonFirstname() {
		return contactPersonFirstname;
	}

	public void setContactPersonFirstname(String contactPersonFirstname) {
		this.contactPersonFirstname = contactPersonFirstname;
	}

	public String getContactPersonLastname() {
		return contactPersonLastname;
	}

	public void setContactPersonLastname(String contactPersonLastname) {
		this.contactPersonLastname = contactPersonLastname;
	}

	public String getContactPhoneMobile() {
		return contactPhoneMobile;
	}

	public void setContactPhoneMobile(String contactPhoneMobile) {
		this.contactPhoneMobile = contactPhoneMobile;
	}

	public boolean equals(CorporateProfile c){
		if(null==c)return false;
		if (!compare(this.address1, c.getAddress1())) {
			return false;
		}
		if (!compare(this.address2, c.getAddress2())) {
			return false;
		}
		if (!compare(this.brNo, c.getBrNo())) {
			return false;
		}
		if (!compare(this.businessNatureCode, c.getBusinessNatureCode())) {
			return false;
		}
		if (!compare(this.companyName, c.getCompanyName())) {
			return false;
		}
		if (!compare(this.companyNameNls, c.getCompanyNameNls())) {
			return false;
		}
		if (!compare(this.contactEmail, c.getContactEmail())) {
			return false;
		}
		if (!compare(this.district, c.getDistrict())) {
			return false;
		}
		CorporateAdditionAddress address = c.getCorporateAdditionAddress();
		if (null != this.corporateAdditionAddress && null != address && this.corporateAdditionAddress.getAddressType().equals(address.getAddressType())) {
			if (!compare(this.corporateAdditionAddress.getAddress1(), address.getAddress1())) {
				return false;
			}
			if (!compare(this.corporateAdditionAddress.getAddress2(), address.getAddress2())) {
				return false;
			}
			if (!compare(this.corporateAdditionAddress.getHkDistrict(), address.getHkDistrict())) {
				return false;
			}
		}
		
		return true;
	}
	public boolean compare(Object source,Object target){
		if (null != source) {
			if(!source.equals(target)){
				return false;
			}
		}else {
			if (null != target){
				return false;
			}
		}
		return true;
	}
}