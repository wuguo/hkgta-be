package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "carpark_interface_import")
@NamedQuery(name = "CarparkInterfaceImport.findAll", query = "SELECT c FROM CarparkInterfaceImport c")
public class CarparkInterfaceImport implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "import_id", unique = true, nullable = false)
	private Long importId;

	@Column(name = "import_filename", length = 250)
	private String importFilename;

	@Column(name = "status", length = 10)
	private String status;

	@Column(name = "file_header_datetime")
	private Date fileHeaderDatetime;

	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "update_date")
	private Date updateDate;

	public Long getImportId() {
		return importId;
	}

	public void setImportId(Long importId) {
		this.importId = importId;
	}

	public String getImportFilename() {
		return importFilename;
	}

	public void setImportFilename(String importFilename) {
		this.importFilename = importFilename;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getFileHeaderDatetime() {
		return fileHeaderDatetime;
	}

	public void setFileHeaderDatetime(Date fileHeaderDatetime) {
		this.fileHeaderDatetime = fileHeaderDatetime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}