package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the customer_enroll_stage database table.
 * 
 */
@Entity
@Table(name="customer_enroll_stage")
@NamedQuery(name="CustomerEnrollStage.findAll", query="SELECT c FROM CustomerEnrollStage c")
public class CustomerEnrollStage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="stage_id", unique=true, nullable=false)
	private String stageId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="comment_date")
	private Date commentDate;

	@Column(length=300)
	private String comments;

	@Column(name="enroll_status", length=10)
	private String enrollStatus;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="enroll_id")
	private CustomerEnrollment customerEnrollment;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="comment_by")
	private UserMaster userMaster;

	public CustomerEnrollStage() {
	}

	public String getStageId() {
		return this.stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}

	public Date getCommentDate() {
		return this.commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getEnrollStatus() {
		return this.enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}

	public CustomerEnrollment getCustomerEnrollment() {
		return this.customerEnrollment;
	}

	public void setCustomerEnrollment(CustomerEnrollment customerEnrollment) {
		this.customerEnrollment = customerEnrollment;
	}

	public UserMaster getUserMaster() {
		return this.userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

}