package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the facility_item_attribute database table.
 * 
 */
@Entity
@Table(name="facility_item_attribute")
@NamedQuery(name="FacilityItemAttribute.findAll", query="SELECT f FROM FacilityItemAttribute f")
public class FacilityItemAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FacilityItemAttributePK id;

	@Column(name="attr_name", length=250)
	private String attrName;

	@Column(name="attr_value", length=250)
	private String attrValue;

	@Column(name="attr_value_type", length=10)
	private String attrValueType;

	//bi-directional many-to-one association to FacilityItem
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="facility_no", nullable=false, insertable=false, updatable=false)
	private FacilityMaster facilityMaster;

	public FacilityItemAttribute() {
	}

	public FacilityItemAttributePK getId() {
		return this.id;
	}

	public void setId(FacilityItemAttributePK id) {
		this.id = id;
	}

	public String getAttrName() {
		return this.attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public String getAttrValue() {
		return this.attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	public String getAttrValueType() {
		return this.attrValueType;
	}

	public void setAttrValueType(String attrValueType) {
		this.attrValueType = attrValueType;
	}

	public FacilityMaster getFacilityMaster() {
		return facilityMaster;
	}

	public void setFacilityMaster(FacilityMaster facilityMaster) {
		this.facilityMaster = facilityMaster;
	}


}