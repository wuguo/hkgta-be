package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the customer_email_content database table.
 * 
 */
@Entity
@Table(name = "customer_email_content")
@NamedQuery(name = "CustomerEmailContent.findAll", query = "SELECT c FROM CustomerEmailContent c")
public class CustomerEmailContent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "send_id", unique = true, nullable = false)
	private String sendId;

	@Column(name="content")
	private String content;

	@Column(name = "notice_type", length = 10)
	private String noticeType;

	@Column(name = "recipient_email", length = 250)
	private String recipientEmail;

	@Column(name = "recipient_customer_id", length = 50)
	private String recipientCustomerId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "send_date")
	private Date sendDate;

	@Column(name = "sender_user_id", length = 50)
	private String senderUserId;

	@Column(length = 10)
	private String status;
	

	@Column(length = 300)
	private String subject;

	@Column(name="server_return_code",length = 20)
	private String serverReturnCode;
	
	@Column(name="server_return_msg",length = 500)
	private String serverReturnMsg;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="server_resp_timestamp")
	private Date serverRespTimestamp;
	
	@Transient
	private String copyto;
	
	@Column(name="resend_count")
	private int resendCount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "prev_attempt_sent")
	private Date prevAttemptSent; 
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate; 
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate; 
	
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public int getResendCount() {
		return resendCount;
	}

	public void setResendCount(int resendCount) {
		this.resendCount = resendCount;
	}

	public Date getPrevAttemptSent() {
		return prevAttemptSent;
	}

	public void setPrevAttemptSent(Date prevAttemptSent) {
		this.prevAttemptSent = prevAttemptSent;
	}

	public String getCopyto() {
		return copyto;
	}

	public void setCopyto(String copyto) {
		this.copyto = copyto;
	}

	public String getBlindCopyTo() {
		return blindCopyTo;
	}

	public void setBlindCopyTo(String blindCopyTo) {
		this.blindCopyTo = blindCopyTo;
	}

	@Transient
	private String blindCopyTo;
	
	
	
	public CustomerEmailContent() {
	}

	public String getSendId() {
		return this.sendId;
	}

	public void setSendId(String sendId) {
		this.sendId = sendId;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNoticeType() {
		return this.noticeType;
	}

	public void setNoticeType(String noticeType) {
		this.noticeType = noticeType;
	}

	public String getRecipientEmail() {
		return this.recipientEmail;
	}

	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}

	public String getRecipientCustomerId() {
		return recipientCustomerId;
	}

	public void setRecipientCustomerId(String recipientCustomerId) {
		this.recipientCustomerId = recipientCustomerId;
	}

	public Date getSendDate() {
		return this.sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getSenderUserId() {
		return this.senderUserId;
	}

	public void setSenderUserId(String senderUserId) {
		this.senderUserId = senderUserId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getServerReturnCode() {
		return serverReturnCode;
	}

	public void setServerReturnCode(String serverReturnCode) {
		this.serverReturnCode = serverReturnCode;
	}

	public String getServerReturnMsg() {
		return serverReturnMsg;
	}

	public void setServerReturnMsg(String serverReturnMsg) {
		this.serverReturnMsg = serverReturnMsg;
	}

	public Date getServerRespTimestamp() {
		return serverRespTimestamp;
	}

	public void setServerRespTimestamp(Date serverRespTimestamp) {
		this.serverRespTimestamp = serverRespTimestamp;
	}

	
}