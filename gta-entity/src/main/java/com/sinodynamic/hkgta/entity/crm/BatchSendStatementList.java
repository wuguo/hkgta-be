package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.math.BigDecimal;


/**
 * The persistent class for the batch_send_statement_list database table.
 * 
 */
@Entity
@Table(name="batch_send_statement_list")
@NamedQuery(name="BatchSendStatementList.findAll", query="SELECT b FROM BatchSendStatementList b")
public class BatchSendStatementList implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="email_send_id")
	private Long emailSendId;

	@Column(name="cashvalue_balance")
	private BigDecimal cashvalueBalance;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_date")
	private Date createDate;

	@ManyToOne
	@JoinColumn(name="batch_id")
	private BatchSendStatementHd batchSendStatementHd;

	public BatchSendStatementList() {
	}

	public Long getEmailSendId() {
		return this.emailSendId;
	}

	public void setEmailSendId(Long emailSendId) {
		this.emailSendId = emailSendId;
	}

	public BigDecimal getCashvalueBalance() {
		return this.cashvalueBalance;
	}

	public void setCashvalueBalance(BigDecimal cashvalueBalance) {
		this.cashvalueBalance = cashvalueBalance;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BatchSendStatementHd getBatchSendStatementHd() {
		return this.batchSendStatementHd;
	}

	public void setBatchSendStatementHd(BatchSendStatementHd batchSendStatementHd) {
		this.batchSendStatementHd = batchSendStatementHd;
	}

}