package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the facility_utilization_rate_date database table.
 * 
 */
@Entity
@Table(name="facility_utilization_rate_date")
@NamedQuery(name="FacilityUtilizationRateDate.findAll", query="SELECT f FROM FacilityUtilizationRateDate f")
public class FacilityUtilizationRateDate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="date_id", unique=true, nullable=false)
	private Long dateId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="special_date", length=10, nullable=false)
	private Date specialDate;
	
	@Column(name="facility_type", length=10)
	private String facilityType;
	
	@Column(name="facility_subtype_id", length=10)
	private String facilitySubtypeId;
	
	@Column(name="date_desc", length=100)
	private String dateDesc;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER,orphanRemoval=true)
	@JoinColumn(name="facility_date_id" , nullable=true, insertable=false, updatable=false)
	private List<FacilityUtilizationRateTime> facilityUtilizationRateTimes;
	
	public FacilityUtilizationRateDate() {
	}

	public Long getDateId()
	{
		return dateId;
	}

	public void setDateId(Long dateId)
	{
		this.dateId = dateId;
	}

	public String getDateDesc()
	{
		return dateDesc;
	}

	public void setDateDesc(String dateDesc)
	{
		this.dateDesc = dateDesc;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public Date getSpecialDate()
	{
		return specialDate;
	}

	public void setSpecialDate(Date specialDate)
	{
		this.specialDate = specialDate;
	}

	public List<FacilityUtilizationRateTime> getFacilityUtilizationRateTimes()
	{
		return facilityUtilizationRateTimes;
	}

	public void setFacilityUtilizationRateTimes(List<FacilityUtilizationRateTime> facilityUtilizationRateTimes)
	{
		this.facilityUtilizationRateTimes = facilityUtilizationRateTimes;
	}

	public String getFacilityType()
	{
		return facilityType;
	}

	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}

	public String getFacilitySubtypeId()
	{
		return facilitySubtypeId;
	}

	public void setFacilitySubtypeId(String facilitySubtypeId)
	{
		this.facilitySubtypeId = facilitySubtypeId;
	}


}