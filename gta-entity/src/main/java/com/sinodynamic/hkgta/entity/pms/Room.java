package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the room database table.
 * 
 */
@Entity
@Table(name="room")
@NamedQuery(name="Room.findAll", query="SELECT p FROM Room p")
public class Room implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "room_id", unique = true, nullable = false, length = 20)
	private Long roomId;

	@Column(name="room_no" , nullable = false,length=20)
	private String roomNo;

	@Column(name="status",length=10)
	private String status;
	
	@Column(name="service_status",length=10)
	private String serviceStatus;
	
	@Column(name="frontdesk_status",length=10)
	private String frontdeskStatus;

	@Column(name="room_class",length=10)
	private String roomClass;

	@ManyToOne
	@JoinColumn(name = "room_type")
	private RoomType roomType;

	@Column(name="venue_code",length=10)
	private String venueCode;
	
	@Column(name="floor",length=2)
	private Long floor;
	
	@Column(name="capacity",length=2)
	private Long capacity;
	
	@Column(name="bed_qty",length=1)
	private Long bedQty;
	
	@Column(name="phone_no",length=50)
	private String phoneNo;
	
	@Column(name="academy_house_code",length=10)
	private String academyHouseCode;
	
	@Column(name="internal_remark",length=300)
	private String internalRemark;
	
	@Column(name="status_date")
	private Date status_date;
	
	@Column(name="status_chg_by",length=50)
	private String statusChgBy;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "site_poi_id")
	private Long sitePoiId;
	
	@Column(name="vendor_poi_id",length=50)
	private String vendorPoiId;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public Room() {
	}
	
	public Long getRoomId() {
		return roomId;
	}

	public String getVendorPoiId() {
		return vendorPoiId;
	}

	public void setVendorPoiId(String vendorPoiId) {
		this.vendorPoiId = vendorPoiId;
	}

	public void setRoomId(Object roomId) {
		this.roomId = (roomId!=null ? NumberUtils.toLong(roomId.toString()):null);
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRoomClass() {
		return roomClass;
	}

	public void setRoomClass(String roomClass) {
		this.roomClass = roomClass;
	}

	public String getVenueCode() {
		return venueCode;
	}

	public void setVenueCode(String venueCode) {
		this.venueCode = venueCode;
	}

	public Long getFloor() {
		return floor;
	}

	public void setFloor(Object floor) {
		this.floor = (floor!=null ? NumberUtils.toLong(floor.toString()):null);
	}

	public Long getCapacity() {
		return capacity;
	}

	public void setCapacity(Object capacity) {
		this.capacity = (capacity!=null ? NumberUtils.toLong(capacity.toString()):null);
	}

	public Long getBedQty() {
		return bedQty;
	}

	public void setBedQty(Object bedQty) {
		this.bedQty = (bedQty!=null ? NumberUtils.toLong(bedQty.toString()):null);
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getAcademyHouseCode() {
		return academyHouseCode;
	}

	public void setAcademyHouseCode(String academyHouseCode) {
		this.academyHouseCode = academyHouseCode;
	}

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public Date getStatus_date() {
		return status_date;
	}

	public void setStatus_date(Date status_date) {
		this.status_date = status_date;
	}

	public String getStatusChgBy() {
		return statusChgBy;
	}

	public void setStatusChgBy(String statusChgBy) {
		this.statusChgBy = statusChgBy;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public String getServiceStatus()
	{
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus)
	{
		this.serviceStatus = serviceStatus;
	}

	public Long getSitePoiId() {
		return sitePoiId;
	}

	public void setSitePoiId(Object sitePoiId) {
		this.sitePoiId = (sitePoiId!=null ? NumberUtils.toLong(sitePoiId.toString()):null);
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Object verNo) {
		this.verNo = (verNo!=null ? NumberUtils.toLong(verNo.toString()):null);
	}

	public String getFrontdeskStatus()
	{
		return frontdeskStatus;
	}

	public void setFrontdeskStatus(String frontdeskStatus)
	{
		this.frontdeskStatus = frontdeskStatus;
	}
}