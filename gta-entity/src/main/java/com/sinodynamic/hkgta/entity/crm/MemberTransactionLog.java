package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the member_transaction_log database table.
 * 
 */
@Entity
@Table(name="member_transaction_log")
@NamedQuery(name="MemberTransactionLog.findAll", query="SELECT m FROM MemberTransactionLog m")
public class MemberTransactionLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="transaction_no", unique=true, nullable=false)
	private Long transactionNo;
	
	
	@Column(name="audit_by")
	private String auditBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="audit_date")
	private Date auditDate;

	@Column(name="create_by")
	private String createBy;

	@Column(name="from_transaction_no")
	private Long fromTransactionNo;

	@Column(name="internal_remark")
	private String internalRemark;

	@Column(name="order_no")
	private Long orderNo;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	@Column(name="payment_location_code")
	private String paymentLocationCode;

	@Column(name="payment_media")
	private String paymentMedia;

	@Column(name="payment_method_code")
	private String paymentMethodCode;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transaction_timestamp")
	private Date transactionTimestamp;

	@Column(name="update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	//bi-directional many-to-one association to MemberCashvalueBalHistory
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="process_id")
	private MemberCashvalueBalHistory memberCashvalueBalHistory;

	public MemberTransactionLog() {
	}

	public Long getTransactionNo() {
		return this.transactionNo;
	}

	public void setTransactionNo(Long transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getAuditBy() {
		return this.auditBy;
	}

	public void setAuditBy(String auditBy) {
		this.auditBy = auditBy;
	}

	public Date getAuditDate() {
		return this.auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Long getFromTransactionNo() {
		return this.fromTransactionNo;
	}

	public void setFromTransactionNo(Object fromTransactionNo) {
		this.fromTransactionNo = (fromTransactionNo!=null ? NumberUtils.toLong(fromTransactionNo.toString()):null);
	}

	public String getInternalRemark() {
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public Long getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(Object orderNo) {
		this.orderNo = (orderNo!=null ? NumberUtils.toLong(orderNo.toString()):null);
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPaymentLocationCode() {
		return this.paymentLocationCode;
	}

	public void setPaymentLocationCode(String paymentLocationCode) {
		this.paymentLocationCode = paymentLocationCode;
	}

	public String getPaymentMedia() {
		return this.paymentMedia;
	}

	public void setPaymentMedia(String paymentMedia) {
		this.paymentMedia = paymentMedia;
	}

	public String getPaymentMethodCode() {
		return this.paymentMethodCode;
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTransactionTimestamp() {
		return this.transactionTimestamp;
	}

	public void setTransactionTimestamp(Date transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public MemberCashvalueBalHistory getMemberCashvalueBalHistory() {
		return this.memberCashvalueBalHistory;
	}

	public void setMemberCashvalueBalHistory(MemberCashvalueBalHistory memberCashvalueBalHistory) {
		this.memberCashvalueBalHistory = memberCashvalueBalHistory;
	}

}