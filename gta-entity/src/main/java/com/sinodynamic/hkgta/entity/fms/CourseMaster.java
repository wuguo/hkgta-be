package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;

/**
 * The persistent class for the course_master database table.
 * 
 */
@Entity
@Table(name = "course_master")
@NamedQuery(name = "CourseMaster.findAll", query = "SELECT c FROM CourseMaster c")
public class CourseMaster implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_id", unique = true, nullable = false)
	private Long courseId;

	@Column(name = "age_range_code", length = 10)
	private String ageRangeCode;

	@Column(name = "capacity", length = 3)
	private Long capacity;

	@Column(name = "course_description", length = 1000)
	private String courseDescription;

	@Column(name = "course_name", length = 200)
	private String courseName;

	@Column(name = "course_type", length = 10)
	private String courseType;

	@Column(name = "create_by", nullable = false, length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;

	@Column(name = "internal_remark", length = 300)
	private String internalRemark;

	@Column(name = "member_acceptance", length = 10)
	private String memberAcceptance;

	@Column(name = "pos_item_no", length = 50)
	private String posItemNo;

	@Temporal(TemporalType.DATE)
	@Column(name = "regist_begin_date")
	private Date registBeginDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "regist_due_date")
	private Date registDueDate;

	@Column(length = 45)
	private String status;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;
	

//	cancel_requester_type
	@Column(name = "cancel_requester_type", length = 10)
	private String cancelRequesterType;
	
	@Column(name = "poster_filename", length = 250)
	private String posterFilename;
	
	@Column(name = "open_enroll", length = 1)
	private String openEnroll;

	@Column(name = "no_of_session", length = 3)
	private Integer noOfSession;
	public CourseMaster()
	{
	}
	
	public Long getCourseId()
	{
		return this.courseId;
	}

	public void setCourseId(Object courseId)
	{
		this.courseId = (courseId!=null ? NumberUtils.toLong(courseId.toString()):null);
	}

	public String getAgeRangeCode()
	{
		return this.ageRangeCode;
	}

	public void setAgeRangeCode(String ageRangeCode)
	{
		this.ageRangeCode = ageRangeCode;
	}

	public Long getCapacity()
	{
		return this.capacity;
	}

	public void setCapacity(Object capacity)
	{
		this.capacity = (capacity!=null ? NumberUtils.toLong(capacity.toString()):null);
	}

	public String getCourseDescription()
	{
		return this.courseDescription;
	}

	public void setCourseDescription(String courseDescription)
	{
		this.courseDescription = courseDescription;
	}

	public String getCourseName()
	{
		return this.courseName;
	}

	public void setCourseName(String courseName)
	{
		this.courseName = courseName;
	}

	public String getCourseType()
	{
		return this.courseType;
	}

	public void setCourseType(String courseType)
	{
		this.courseType = courseType;
	}

	public String getCreateBy()
	{
		return this.createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public String getInternalRemark()
	{
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark)
	{
		this.internalRemark = internalRemark;
	}

	public String getMemberAcceptance()
	{
		return this.memberAcceptance;
	}

	public void setMemberAcceptance(String memberAcceptance)
	{
		this.memberAcceptance = memberAcceptance;
	}

	public String getPosItemNo()
	{
		return this.posItemNo;
	}

	public void setPosItemNo(String posItemNo)
	{
		this.posItemNo = posItemNo;
	}

	public Date getRegistBeginDate()
	{
		return this.registBeginDate;
	}

	public void setRegistBeginDate(Date registBeginDate)
	{
		this.registBeginDate = registBeginDate;
	}

	public Date getRegistDueDate()
	{
		return this.registDueDate;
	}

	public void setRegistDueDate(Date registDueDate)
	{
		this.registDueDate = registDueDate;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getUpdateBy()
	{
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}



	public String getCancelRequesterType() {
		return cancelRequesterType;
	}

	public void setCancelRequesterType(String cancelRequesterType) {
		this.cancelRequesterType = cancelRequesterType;
	}
	public String getPosterFilename() {
		return posterFilename;
	}

	public void setPosterFilename(String posterFilename) {
		this.posterFilename = posterFilename;
	}

	public String getOpenEnroll() {
		return openEnroll;
	}

	public void setOpenEnroll(String openEnroll) {
		this.openEnroll = openEnroll;
	}

	public Integer getNoOfSession() {
		return noOfSession;
	}

	public void setNoOfSession(Integer noOfSession) {
		this.noOfSession = noOfSession;
	}
	

}