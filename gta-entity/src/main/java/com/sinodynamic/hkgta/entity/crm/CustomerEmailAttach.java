package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the customer_email_attach database table.
 * 
 */
@Entity
@Table(name = "customer_email_attach")
public class CustomerEmailAttach implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "attach_id", unique = true, nullable = false)
	private String attachId;

	@Column(name = "email_send_id", nullable = false)
	private String emailSendId;

	@Column(name = "attachment_name", length = 250)
	private String attachmentName;

	@Column(name = "attachment_path", length = 250)
	private String attachmentPath;




	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getEmailSendId() {
		return emailSendId;
	}

	public void setEmailSendId(String emailSendId) {
		this.emailSendId = emailSendId;
	}

	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	public String getAttachmentPath() {
		return attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

}