package com.sinodynamic.hkgta.entity.ibeacon;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ibeacon_nearby_person database table.
 * 
 */
@Entity
@Table(name="ibeacon_nearby_person")
@NamedQuery(name="IbeaconNearbyPerson.findAll", query="SELECT i FROM IbeaconNearbyPerson i")
public class IbeaconNearbyPerson implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private IbeaconNearbyPersonPK id;

	@Column(name="distance")
	private Long distance;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_timestamp")
	private Date lastTimestamp;

	public IbeaconNearbyPerson() {
	}

	public IbeaconNearbyPersonPK getId() {
		return this.id;
	}

	public void setId(IbeaconNearbyPersonPK id) {
		this.id = id;
	}

	public Long getDistance() {
		return this.distance;
	}

	public void setDistance(Long distance) {
		this.distance = distance;
	}

	public Date getLastTimestamp() {
		return this.lastTimestamp;
	}

	public void setLastTimestamp(Date lastTimestamp) {
		this.lastTimestamp = lastTimestamp;
	}

}