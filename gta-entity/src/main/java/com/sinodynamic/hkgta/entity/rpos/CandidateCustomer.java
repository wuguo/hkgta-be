package com.sinodynamic.hkgta.entity.rpos;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the candidate_customer database table.
 * 
 */
@Entity
@Table(name="candidate_customer")
@NamedQuery(name="CandidateCustomer.findAll", query="SELECT c FROM CandidateCustomer c")
public class CandidateCustomer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="candidate_id")
	private Long candidateId;

	@Column(name="contact_email")
	private String contactEmail;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(name="customer_id")
	private Long customerId;

	@Column(name="customer_type")
	private String customerType;

	@Column(name="follow_by_user_id")
	private String followByUserId;

	private String gender;

	@Column(name="given_name")
	private String givenName;

	@Column(name="given_name_nls")
	private String givenNameNls;

	@Column(name="internal_remark")
	private String internalRemark;

	@Column(name="phone_mobile")
	private String phoneMobile;

	private String surname;

	@Column(name="surname_nls")
	private String surnameNls;

	@Column(name="update_by")
	private String updateBy;
	
	@Version
	@Column(name = "ver_no", nullable=false)
	private Long verNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	//bi-directional many-to-one association to CustomerOrderPermitCard
	@OneToMany(mappedBy="candidateCustomer")
	private List<CustomerOrderPermitCard> customerOrderPermitCards;

	public CandidateCustomer() {
	}

	
	public Long getVerNo() {
		return verNo;
	}


	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}


	public Long getCandidateId() {
		return this.candidateId;
	}

	public void setCandidateId(Long candidateId) {
		this.candidateId = candidateId;
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}

	public String getCustomerType() {
		return this.customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getFollowByUserId() {
		return this.followByUserId;
	}

	public void setFollowByUserId(String followByUserId) {
		this.followByUserId = followByUserId;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGivenName() {
		return this.givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getGivenNameNls() {
		return this.givenNameNls;
	}

	public void setGivenNameNls(String givenNameNls) {
		this.givenNameNls = givenNameNls;
	}

	public String getInternalRemark() {
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getPhoneMobile() {
		return this.phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurnameNls() {
		return this.surnameNls;
	}

	public void setSurnameNls(String surnameNls) {
		this.surnameNls = surnameNls;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<CustomerOrderPermitCard> getCustomerOrderPermitCards() {
		return this.customerOrderPermitCards;
	}

	public void setCustomerOrderPermitCards(List<CustomerOrderPermitCard> customerOrderPermitCards) {
		this.customerOrderPermitCards = customerOrderPermitCards;
	}

	public CustomerOrderPermitCard addCustomerOrderPermitCard(CustomerOrderPermitCard customerOrderPermitCard) {
		getCustomerOrderPermitCards().add(customerOrderPermitCard);
		customerOrderPermitCard.setCandidateCustomer(this);

		return customerOrderPermitCard;
	}

	public CustomerOrderPermitCard removeCustomerOrderPermitCard(CustomerOrderPermitCard customerOrderPermitCard) {
		getCustomerOrderPermitCards().remove(customerOrderPermitCard);
		customerOrderPermitCard.setCandidateCustomer(null);

		return customerOrderPermitCard;
	}

}