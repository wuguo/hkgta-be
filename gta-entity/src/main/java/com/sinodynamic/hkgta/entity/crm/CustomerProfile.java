package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.EntityHelper;
import com.sinodynamic.hkgta.entity.GenericDto;
/**
 * The persistent class for the customer_profile database table.
 * 
 */
@Entity
@Table(name = "customer_profile")
@NamedQuery(name = "CustomerProfile.findAll", query = "SELECT c FROM CustomerProfile c")
public class CustomerProfile extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_id", unique = true, nullable = false)
	@EncryptFieldInfo  
	private Long customerId;

	@Column(name = "business_nature", nullable = false, length = 50)
	private String businessNature;

	@Column(name = "contact_email", length = 50)
	private String contactEmail;
	
	@Column(name = "contact_email2", length = 50)
	private String contactEmail2;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth", nullable = false)
	private Date dateOfBirth;

	@Column(length = 1)
	private String gender;

	@Column(name = "given_name", length = 50)
	private String givenName;

	@Column(name = "given_name_nls", length = 50)
	private String givenNameNls;

	@Column(nullable = false, length = 50)
	private String nationality;

	@Column(name = "passport_no", nullable = false, length = 40)
	private String passportNo;

	@Column(name = "passport_type", nullable = false, length = 20)
	private String passportType;

	@Column(name = "phone_business", nullable = false, length = 50)
	private String phoneBusiness;

	@Column(name = "phone_home", nullable = false, length = 50)
	private String phoneHome;

	@Column(name = "phone_mobile", nullable = false, length = 50)
	private String phoneMobile;

	@Transient
	@Column(name = "portrait_photo", length = 300)
	private String portraitPhoto;

	@Column(name = "position_title", length = 100)
	private String positionTitle;

	@Column(name = "postal_address1", length = 250)
	private String postalAddress1;

	@Column(name = "postal_address2", length = 250)
	private String postalAddress2;

	@Column(name = "postal_district", nullable = false, length = 30)
	private String postalDistrict;

	@Column(length = 10)
	private String salutation;
	
	@Transient
	@Column(length = 300)
	private String signature;

	@Column(length = 50)
	private String surname;

	@Column(name = "surname_nls", length = 50)
	private String surnameNls;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "internal_remark")
	private String internalRemark;

	@Column(name = "is_deleted")
	private String isDeleted;

	@Column(name = "contact_class_code")
	private String contactClassCode;

	@Column(name = "greeting_info")
	private String greetingInfo;
	
	@Column(name = "marital_status")
	private String maritalStatus;
	
	@Transient
	private boolean portraitPhotoIdentiy;
	
	@Transient
	private boolean signatureIdentiy;
	
	@Transient
	private String message;
	
	@Transient
	private String code;
	
	@Transient
	private String button;

	public String getButton() {
		return button;
	}

	public void setButton(String button) {
		this.button = button;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isPortraitPhotoIdentiy() {
		return portraitPhotoIdentiy;
	}

	public void setPortraitPhotoIdentiy(boolean portraitPhotoIdentiy) {
		this.portraitPhotoIdentiy = portraitPhotoIdentiy;
	}

	public boolean isSignatureIdentiy() {
		return signatureIdentiy;
	}

	public void setSignatureIdentiy(boolean signatureIdentiy) {
		this.signatureIdentiy = signatureIdentiy;
	}

	public String getContactEmail2() {
		return EntityHelper.nvl(contactEmail2);
	}

	public void setContactEmail2(String contactEmail2) {
		this.contactEmail2=contactEmail2;
	}

	public String getMaritalStatus() {
		return EntityHelper.nvl(maritalStatus);
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus=maritalStatus;
	}

	@Version
	@Column(name = "ver_no", nullable = false)
	private Long version;

	@Transient
	private String fullBusinessNature;

	@Transient
	private String fullNationality;
	
	@Transient
	private String remoteType;

	@Transient
	@EncryptFieldInfo  
	private Member member;

	private @Transient MemberLimitRule memberLimitRule;

	@Transient
	@EncryptFieldInfo 
	private List<CustomerAdditionInfo> customerAdditionInfos;

	@Transient
	@EncryptFieldInfo 
	private List<CustomerAddress> customerAddresses;

	@Transient
	@EncryptFieldInfo 
	private List<CustomerEnrollment> customerEnrollments;

	@Transient
	private String checkBillingAddress;

	@Transient
	private String useExistProfile;

	@Transient
	private String billingAddress;

	@Transient
	private String salesFollowBy;

	@Transient
	private String memberType;

	@Transient
	private Long corporateId;

	@Transient
	private BigDecimal totalCreditLimit;

	//modified by Kaster 20160506 加解密
	@Transient
	@EncryptFieldInfo
	private Long superiorMemberId;

	@Transient
	private String importStatus;

	@Transient
	private String relationshipCode;

	@Transient
	private String vip;
	
	@Transient
	private String academyNo;

	@Transient
	private Boolean checkAge;

	@Transient
	private Long planNo;

	
	 /***
     * add daypass attribute  consent ,two checkbox
     * @return
     */
	@Transient
    private String hkgtaMarket;
	@Transient
    private String nweMarket;
	
	
    
	public String getHkgtaMarket() {
		return EntityHelper.nvl(hkgtaMarket);
	}

	public void setHkgtaMarket(String hkgtaMarket) {
		this.hkgtaMarket = hkgtaMarket;
	}

	public String getNweMarket() {
		return EntityHelper.nvl(nweMarket);
	}

	public void setNweMarket(String nweMarket) {
		this.nweMarket = nweMarket;
	}

	public CustomerProfile() {
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Object customerId) {
		/*
		 * if(customerId instanceof Long){ this.customerId = ((Long)
		 * customerId).longValue(); }else if(customerId instanceof Integer){
		 * this.customerId = ((Integer) customerId).longValue(); }else
		 * if(customerId instanceof String){ this.customerId =
		 * Long.valueOf((String) customerId); }else{ this.customerId = (Long)
		 * customerId; }
		 */
		this.customerId = (customerId != null ? NumberUtils.toLong(customerId.toString()) : null);
	}

	public String getBusinessNature() {
		return this.businessNature;
	}

	public String getCheckBillingAddress() {
		return checkBillingAddress;
	}

	public void setCheckBillingAddress(String checkBillingAddress) {
		this.checkBillingAddress = checkBillingAddress;
	}

	public void setBusinessNature(String businessNature) {
		this.businessNature = EntityHelper.nvl(businessNature);
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = EntityHelper.nvl(billingAddress);
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = EntityHelper.nvl(contactEmail);
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDateOfBirth() {
		return EntityHelper.getYMDFormatDate(dateOfBirth);
	}

//	public Date getOriDateOfBirth() {
//		return this.dateOfBirth;
//	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = EntityHelper.nvl(gender);
	}

	public String getGivenName() {
		return this.givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = EntityHelper.nvl(givenName);
	}

	public String getGivenNameNls() {
		return this.givenNameNls;
	}

	public void setGivenNameNls(String givenNameNls) {
		this.givenNameNls = EntityHelper.nvl(givenNameNls);
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = EntityHelper.nvl(nationality);
	}

	public String getPassportNo() {
		return this.passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportType() {
		return this.passportType;
	}

	public void setPassportType(String passportType) {
		this.passportType = passportType;
	}

	public String getPhoneBusiness() {
		return this.phoneBusiness;
	}

	public void setPhoneBusiness(String phoneBusiness) {
		this.phoneBusiness = EntityHelper.nvl(phoneBusiness);
	}

	public String getPhoneHome() {
		return this.phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = EntityHelper.nvl(phoneHome);
	}

	public String getPhoneMobile() {
		return this.phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = EntityHelper.nvl(phoneMobile);
	}

	public String getPortraitPhoto() {
		return this.portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = EntityHelper.nvl(portraitPhoto);
	}

	public String getPositionTitle() {
		return this.positionTitle;
	}

	public void setPositionTitle(String positionTitle) {
		this.positionTitle = EntityHelper.nvl(positionTitle);
	}

	public String getPostalAddress1() {
		return postalAddress1;
	}

	public void setPostalAddress1(String postalAddress1) {
		this.postalAddress1 = EntityHelper.nvl(postalAddress1);
	}

	public String getPostalAddress2() {
		return postalAddress2;
	}

	public void setPostalAddress2(String postalAddress2) {
		this.postalAddress2 = (postalAddress2 == null) ? "" : postalAddress2;
		;
	}

	public String getPostalDistrict() {
		return this.postalDistrict;
	}

	public void setPostalDistrict(String postalDistrict) {
		this.postalDistrict = EntityHelper.nvl(postalDistrict);
	}

	public String getSalutation() {
		return this.salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = EntityHelper.nvl(salutation);
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = EntityHelper.nvl(signature);
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = EntityHelper.nvl(surname);
	}

	public String getSurnameNls() {
		return this.surnameNls;
	}

	public void setSurnameNls(String surnameNls) {
		this.surnameNls = EntityHelper.nvl(surnameNls);
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public MemberLimitRule getMemberLimitRule() {
		return memberLimitRule;
	}

	public void setMemberLimitRule(MemberLimitRule memberLimitRule) {
		this.memberLimitRule = memberLimitRule;
	}

	public List<CustomerAdditionInfo> getCustomerAdditionInfos() {
		return this.customerAdditionInfos;
	}

	public void setCustomerAdditionInfos(List<CustomerAdditionInfo> customerAdditionInfos) {
		this.customerAdditionInfos = customerAdditionInfos;
	}

	public CustomerAdditionInfo addCustomerAdditionInfo(CustomerAdditionInfo customerAdditionInfo) {
		getCustomerAdditionInfos().add(customerAdditionInfo);
		// customerAdditionInfo.setCustomerProfile(this);

		return customerAdditionInfo;
	}

	public CustomerAdditionInfo removeCustomerAdditionInfo(CustomerAdditionInfo customerAdditionInfo) {
		getCustomerAdditionInfos().remove(customerAdditionInfo);
		// customerAdditionInfo.setCustomerProfile(null);

		return customerAdditionInfo;
	}

	public List<CustomerAddress> getCustomerAddresses() {
		return this.customerAddresses;
	}

	public void setCustomerAddresses(List<CustomerAddress> customerAddresses) {
		this.customerAddresses = customerAddresses;
	}

	public CustomerAddress addCustomerAddress(CustomerAddress customerAddress) {
		getCustomerAddresses().add(customerAddress);
		customerAddress.setCustomerProfile(this);

		return customerAddress;
	}

	public CustomerAddress removeCustomerAddress(CustomerAddress customerAddress) {
		getCustomerAddresses().remove(customerAddress);
		customerAddress.setCustomerProfile(null);

		return customerAddress;
	}

	public List<CustomerEnrollment> getCustomerEnrollments() {
		return customerEnrollments;
	}

	public void setCustomerEnrollments(List<CustomerEnrollment> customerEnrollments) {
		this.customerEnrollments = customerEnrollments;
	}

	public String getCompanyName() {
		return EntityHelper.nvl(companyName);
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = EntityHelper.nvl(internalRemark);
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = EntityHelper.nvl(isDeleted);
	}

	public String getUseExistProfile() {
		return useExistProfile;
	}

	public void setUseExistProfile(String useExistProfile) {
		this.useExistProfile = useExistProfile;
	}

	public String getSalesFollowBy() {
		return salesFollowBy;
	}

	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public Long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Long corporateId) {
		this.corporateId = corporateId;
	}

	public BigDecimal getTotalCreditLimit() {
		return totalCreditLimit;
	}

	public void setTotalCreditLimit(BigDecimal totalCreditLimit) {
		if (totalCreditLimit != null) {
			this.totalCreditLimit = totalCreditLimit;
		} else {
			this.totalCreditLimit = BigDecimal.ZERO;
		}
	}

	public Long getSuperiorMemberId() {
		return superiorMemberId;
	}

	public void setSuperiorMemberId(Long superiorMemberId) {
		this.superiorMemberId = superiorMemberId;
	}

	public String getContactClassCode() {
		return contactClassCode;
	}

	public void setContactClassCode(String contactClassCode) {
		this.contactClassCode = contactClassCode;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getFullBusinessNature() {
		return fullBusinessNature;
	}

	public void setFullBusinessNature(String fullBusinessNature) {
		this.fullBusinessNature = fullBusinessNature;
	}

	public String getFullNationality() {
		return fullNationality;
	}

	public void setFullNationality(String fullNationality) {
		this.fullNationality = fullNationality;
	}

	public String getImportStatus() {
		return importStatus;
	}

	public void setImportStatus(String importStatus) {
		this.importStatus = importStatus;
	}

	public String getRelationshipCode() {
		return relationshipCode;
	}

	public void setRelationshipCode(String relationshipCode) {
		this.relationshipCode = relationshipCode;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public Boolean getCheckAge() {
		return checkAge;
	}

	public void setCheckAge(Boolean checkAge) {
		this.checkAge = checkAge;
	}

	public boolean equals(CustomerProfile c) {
		if(null==c)return false;
		if (!compare(this.salutation, c.getSalutation())) {
			return false;
		}

		if (!compare(this.surname, c.getSurname())) {
			return false;
		}

		if (!compare(this.surnameNls, c.getSurnameNls())) {
			return false;
		}

		if (!compare(this.givenName, c.getGivenName())) {
			return false;
		}
		if (!compare(this.givenNameNls, c.getGivenNameNls())) {
			return false;
		}
		if (!compare(this.gender, c.getGender())) {
			return false;
		}
		if (!compare(this.phoneMobile, c.getPhoneMobile())) {
			return false;
		}
		if (!compare(this.phoneBusiness, c.getPhoneBusiness())) {
			return false;
		}
		if (!compare(this.phoneHome, c.getPhoneHome())) {
			return false;
		}
		if (!compare(this.surnameNls, c.getSurnameNls())) {
			return false;
		}
		if (!compare(this.passportType, c.getPassportType())) {
			return false;
		}
		if (!compare(this.passportNo, c.getPassportNo())) {
			return false;
		}
		if (!compare(EntityHelper.getYMDFormatDate(this.dateOfBirth), c.getDateOfBirth())) {
			return false;
		}
		if (!compare(this.nationality, c.getNationality())) {
			return false;
		}
		if (!compare(this.postalAddress1, c.getPostalAddress1())) {
			return false;
		}
		if (!compare(this.postalAddress2, c.getPostalAddress2())) {
			return false;
		}
		if (!compare(this.postalDistrict, c.getPostalDistrict())) {
			return false;
		}
		if (!compare(this.contactEmail, c.getContactEmail())) {
			return false;
		}
		if (!compare(this.businessNature, c.getBusinessNature())) {
			return false;
		}
		if (!compare(this.internalRemark, c.getInternalRemark())) {
			return false;
		}
		List<CustomerAddress> addresses = c.getCustomerAddresses();
		for (CustomerAddress source : this.customerAddresses) {

			for (CustomerAddress target : addresses) {
				if (source.getId().getAddressType().equals(target.getId().getAddressType())) {
					if (!compare(source.getAddress1(), target.getAddress1())) {
						return false;
					}
					if (!compare(source.getAddress2(), target.getAddress2())) {
						return false;
					}
					if (!compare(source.getHkDistrict(), target.getHkDistrict())) {
						return false;
					}
				}

			}
		}
		return true;
	}

	public boolean compare(Object source, Object target) {
		if (null != source) {
			if (!source.equals(target)) {
				return false;
			}
		} else {
			if (null != target) {
				return false;
			}
		}
		return true;
	}

	public String getGreetingInfo() {
		return greetingInfo;
	}

	public void setGreetingInfo(String greetingInfo) {
		this.greetingInfo = greetingInfo;
	}

	public Long getPlanNo() {
		return planNo;
	}

	public void setPlanNo(Long planNo) {
		this.planNo = planNo;
	}

	public String getRemoteType() {
		return remoteType;
	}

	public void setRemoteType(String remoteType) {
		this.remoteType = remoteType;
	}

	public String getVip() {
		return vip;
	}

	public void setVip(String vip) {
		this.vip = vip;
	}
	
}