package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.sinodynamic.hkgta.entity.crm.StaffTimeslot;


/**
 * The persistent class for the staff_facility_schedule database table.
 * 
 */
@Entity
@Table(name="staff_facility_schedule")
@NamedQuery(name="StaffFacilitySchedule.findAll", query="SELECT f FROM StaffFacilitySchedule f")
public class StaffFacilitySchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private StaffFacilitySchedulePK id;

	@Column(name="status", length=50)
	private String status;

	@Column(name="purpose", length=250)
	private String purpose;

	@Column(name="is_reserved_by", length=1)
	private String isReservedByMe;
	
	@Column(name="attend_in")
	private Timestamp attendIn;
	
	@Column(name="attend_out")
	private Timestamp attendOut;
	
	@Column(name="attend_machine_id", length=50)
	private String attendMachineId;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="facility_timeslot_id", nullable=false, insertable=false, updatable=false)
	private FacilityTimeslot facilityTimeslot;

	@OneToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="staff_timeslot_id", nullable=false, insertable=false, updatable=false)
	private StaffTimeslot staffTimeslot;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public StaffFacilitySchedule() {
	}

	public StaffFacilitySchedulePK getId() {
		return id;
	}

	public void setId(StaffFacilitySchedulePK id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getIsReservedByMe() {
		return isReservedByMe;
	}

	public void setIsReservedByMe(String isReservedByMe) {
		this.isReservedByMe = isReservedByMe;
	}

	public Timestamp getAttendIn() {
		return attendIn;
	}

	public void setAttendIn(Timestamp attendIn) {
		this.attendIn = attendIn;
	}

	public Timestamp getAttendOut() {
		return attendOut;
	}

	public void setAttendOut(Timestamp attendOut) {
		this.attendOut = attendOut;
	}

	public String getAttendMachineId() {
		return attendMachineId;
	}

	public void setAttendMachineId(String attendMachineId) {
		this.attendMachineId = attendMachineId;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public FacilityTimeslot getFacilityTimeslot() {
		return facilityTimeslot;
	}

	public void setFacilityTimeslot(FacilityTimeslot facilityTimeslot) {
		this.facilityTimeslot = facilityTimeslot;
	}

	public StaffTimeslot getStaffTimeslot() {
		return staffTimeslot;
	}

	public void setStaffTimeslot(StaffTimeslot staffTimeslot) {
		this.staffTimeslot = staffTimeslot;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
}