package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;

/**
 * The persistent class for the customer_addition_info_caption database table.
 * 
 */
@Entity
@Table(name = "customer_addition_info_caption")
@NamedQuery(name = "CustomerAdditionInfoCaption.findAll", query = "SELECT c FROM CustomerAdditionInfoCaption c")
public class CustomerAdditionInfoCaption implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "caption_id", unique = true, nullable = false)
	private Long captionId;

	@Column(name = "answer_type", length = 10)
	private String answerType;

	@Column(length = 100)
	private String caption;

	@Column(length = 10)
	private String category;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;

	@Column(length = 10)
	private String status;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "display_order")
	private Long displayOrder;

	@Version
	@Column(name = "ver_no", nullable = false)
	private Long verNo;

	public CustomerAdditionInfoCaption() {
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public Long getCaptionId() {
		return this.captionId;
	}

	public void setCaptionId(Long captionId) {
		this.captionId = captionId;
	}

	public String getAnswerType() {
		return this.answerType;
	}

	public void setAnswerType(String answerType) {
		this.answerType = answerType  == null ? "" : answerType;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption  == null ? "" : caption;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category  == null ? "" : category;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy  == null ? "" : createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Object createDate) {
		this.createDate = createDate == null ? new Timestamp(new Date().getTime()) : (Timestamp) createDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status  == null ? "" : status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy == null ? "" : updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Object updateDate) {
		this.updateDate = updateDate == null ? new Date() : (Date) updateDate;
	}

	public Long getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Object displayOrder) {
		/*if (displayOrder instanceof Long) {
			this.displayOrder = ((Long) displayOrder).longValue();
		} else if (displayOrder instanceof Integer) {
			this.displayOrder = ((Integer) displayOrder).longValue();
		} else if (displayOrder instanceof String) {
			this.displayOrder = Long.valueOf((String) displayOrder);
		} else {
			this.displayOrder = (Long) displayOrder;
		}*/
		
		this.displayOrder = (displayOrder!=null ? NumberUtils.toLong(displayOrder.toString()):null);
	}

}