package com.sinodynamic.hkgta.entity.crm;

import com.sinodynamic.hkgta.entity.EntityHelper;


public class FacilityEntitlementDto {
	private String facilityTypeCode;
	private String permission;
	public String getFacilityTypeCode() {
		return EntityHelper.nvl(facilityTypeCode);
	}
	public void setFacilityTypeCode(String facilityTypeCode) {
		this.facilityTypeCode = facilityTypeCode;
	}
	public String getPermission() {
		return EntityHelper.nvl(permission);
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public FacilityEntitlementDto(String facilityTypeCode, String permission) {
		super();
		this.facilityTypeCode = facilityTypeCode;
		this.permission = permission;
	}
	public FacilityEntitlementDto() {

	}
	
}
