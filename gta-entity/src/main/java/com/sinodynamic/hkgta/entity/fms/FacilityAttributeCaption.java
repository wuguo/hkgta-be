package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the facility_attribute_caption database table.
 * 
 */
@Entity
@Table(name="facility_attribute_caption")
@NamedQuery(name="FacilityAttributeCaption.findAll", query="SELECT f FROM FacilityAttributeCaption f")
public class FacilityAttributeCaption implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "attribute_id", unique = true, nullable = false, length = 20)
	private String attributeId;
	
	@Column(name="facility_type", length=10)
	private String facilityType;
	
	@Column(name="caption", length=250)
	private String caption;
	
	@Column(name="attr_value_type", length=10)
	private String attrValueType;
	
	@Column(name="default_value", length=250)
	private String defaultValue;
	
	@Column(name="description", length=250)
	private String description;
	
	@Column(name="on_book_screen", length=1)
	private String onBookScreen;
	
	@Column(name="status", length=10)
	private String status;
	
	public FacilityAttributeCaption() {
	}

	public String getFacilityType()
	{
		return facilityType;
	}

	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}

	public String getCaption()
	{
		return caption;
	}

	public void setCaption(String caption)
	{
		this.caption = caption;
	}

	public String getAttrValueType()
	{
		return attrValueType;
	}

	public void setAttrValueType(String attrValueType)
	{
		this.attrValueType = attrValueType;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}


	public String getAttributeId()
	{
		return attributeId;
	}


	public void setAttributeId(String attributeId)
	{
		this.attributeId = attributeId;
	}

	public String getDefaultValue()
	{
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}

	public String getOnBookScreen()
	{
		return onBookScreen;
	}

	public void setOnBookScreen(String onBookScreen)
	{
		this.onBookScreen = onBookScreen;
	}

	
}