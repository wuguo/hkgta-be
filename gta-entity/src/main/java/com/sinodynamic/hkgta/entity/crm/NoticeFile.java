package com.sinodynamic.hkgta.entity.crm;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="notice_file")
public class NoticeFile {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="file_id", unique=true, nullable=false)
	private Long fileId;

	@ManyToOne
	@JoinColumn(name="notice_id")
	private Notice notice;
	
	@Column(name="file_type")
	private String fileType;
	
	@Column(name="filename")
	private String fileName;
	
	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Date createDate;

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Notice getNotice() {
		return notice;
	}

	public void setNotice(Notice notice) {
		this.notice = notice;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
