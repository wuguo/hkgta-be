package com.sinodynamic.hkgta.entity.mms;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;

@Entity
@Table(name="spa_center_info")
public class SpaCenterInfo implements Serializable{
	
	 private static final long serialVersionUID = 1L;
	 
	    @Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="center_id", unique=true, nullable=false)
		private Long centerId;
	 
	    @Column(name="center_name")
	    private String centerName;

	    @Column(name="location")
	    private String location;
	    
	    @Column(name="open_hour")
	    private String openHour;
	    
	    @Column(name="phone")
	    private String phone;
	    
	    @Column(name="app_pic_filename")
	    private String appPicFilename;
	    
	    @Column(name="create_date")
	    private Timestamp createDate;
	    
	    @Column(name="create_by")
	    private String createBy;
	    
	    @Column(name="update_date")
	    private Timestamp updateDate;
	    
	    @Column(name="update_by")
	    private String updateBy;
	    
	    @Version
		@Column(name = "ver_no", nullable = false)
		private Long version;

		public Long getCenterId() {
			return centerId;
		}

		public void setCenterId(Object centerId) {
			this.centerId = NumberUtils.toLong(centerId.toString());
		}

		public String getCenterName() {
			return centerName;
		}

		public void setCenterName(String centerName) {
			this.centerName = centerName;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getOpenHour() {
			return openHour;
		}

		public void setOpenHour(String openHour) {
			this.openHour = openHour;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getAppPicFilename() {
			return appPicFilename;
		}

		public void setAppPicFilename(String appPicFilename) {
			this.appPicFilename = appPicFilename;
		}

		public Timestamp getCreateDate() {
			return createDate;
		}

		public void setCreateDate(Timestamp createDate) {
			this.createDate = createDate;
		}

		public String getCreateBy() {
			return createBy;
		}

		public void setCreateBy(String createBy) {
			this.createBy = createBy;
		}

		public Timestamp getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(Timestamp updateDate) {
			this.updateDate = updateDate;
		}

		public String getUpdateBy() {
			return updateBy;
		}

		public void setUpdateBy(String updateBy) {
			this.updateBy = updateBy;
		}

		public Long getVersion() {
			return version;
		}

		public void setVersion(Long version) {
			this.version = version;
		}
	    
	    
	    
}
