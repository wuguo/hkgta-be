package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for table 'hkgta'.'advertise_image'.
 * 
 */
@Entity
@Table(name = "bi_member_cnt")
@NamedQuery(name = "BiMemberCnt.findAll", query = "SELECT b FROM BiMemberCnt b ")
public class BiMemberCnt implements Serializable {

	private static final long serialVersionUID = -7866000693071195679L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	private Long sysId;

	@Column(name = "period")
	private String period;

	@Column(name = "count_date")
	private Date countDate; // MAPP or MPORT

	@Column(name = "mbr_status")
	private String mbrStatus; // MSCR or TOPUP

	@Column(name = "total_member")
	private int totalMember;

	@Column(name = "create_date")
	private Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;


	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Date getCountDate() {
		return countDate;
	}

	public void setCountDate(Date countDate) {
		this.countDate = countDate;
	}

	public String getMbrStatus() {
		return mbrStatus;
	}

	public void setMbrStatus(String mbrStatus) {
		this.mbrStatus = mbrStatus;
	}

	public int getTotalMember() {
		return totalMember;
	}

	public void setTotalMember(int totalMember) {
		this.totalMember = totalMember;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


}
