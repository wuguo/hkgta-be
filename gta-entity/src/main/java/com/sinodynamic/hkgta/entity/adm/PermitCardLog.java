package com.sinodynamic.hkgta.entity.adm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the permit_card_master database table.
 * 
 */
@Entity
@Table(name="permit_card_log")
@NamedQuery(name="PermitCardLog.findAll", query="SELECT l FROM PermitCardLog l")
public class PermitCardLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id", unique=true, nullable=false, length=50)
	private String sysId;

	@Column(name="card_no", length=11)
	private Long cardNo;

	@Column(name="customer_id", length=20)
	private String customerId;

	@Column(name="card_status_from", length=10)
	private String cardStatusFrom;
	
	@Column(name="card_status_to", length=10)
	private String cardStatusTo;
	
	@Column(name="status_update_date")
	private Date statusUpdateDate;
	
	@Column(name="update_by", length=50)
	private String updateBy;
	
	public PermitCardLog() {
	}
	
	public PermitCardLog(PermitCardMaster card, String statusTo, String updateBy) {
		this.cardNo = Long.parseLong(card.getCardNo());
		this.customerId = card.getMappingCustomerId() == null ? "" : card.getMappingCustomerId().toString();
		this.cardStatusFrom = card.getStatus();
		this.statusUpdateDate = new Date();
		this.cardStatusTo = statusTo;
		this.updateBy = updateBy;
	}

	public String getSysId()
	{
		return sysId;
	}

	public void setSysId(String sysId)
	{
		this.sysId = sysId;
	}

	public Long getCardNo()
	{
		return cardNo;
	}

	public void setCardNo(Long cardNo)
	{
		this.cardNo = cardNo;
	}

	public String getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(String customerId)
	{
		this.customerId = customerId;
	}

	public String getCardStatusFrom()
	{
		return cardStatusFrom;
	}

	public void setCardStatusFrom(String cardStatusFrom)
	{
		this.cardStatusFrom = cardStatusFrom;
	}

	public String getCardStatusTo()
	{
		return cardStatusTo;
	}

	public void setCardStatusTo(String cardStatusTo)
	{
		this.cardStatusTo = cardStatusTo;
	}

	public Date getStatusUpdateDate()
	{
		return statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate)
	{
		this.statusUpdateDate = statusUpdateDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}
	
}