package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for the contract_helper database table.
 * 
 * @author Vineela_Jyothi
 */
@Entity
@Table(name = "contract_helper")
@NamedQuery(name = "ContractHelper.findAll", query = "SELECT c FROM ContractHelper c")
public class ContractHelper implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contractor_id", unique = true, nullable = false)
	private Long contractorId;

	@Column(name = "helper_pass_type", length = 10)
	private Long helperPassType;

	@Column(name = "surname", length = 50)
	private String surname;

	@Column(name = "given_name", length = 50)
	private String givenName;

	@Column(name = "gender", length = 10)
	private String gender;

	@Column(name = "passport_no", length = 40)
	private String passportNo;

	@Column(name = "passport_type", length = 20)
	private String passportType;

	@Column(name = "company_name", length = 250)
	private String companyName;

	@Column(name = "user_id", length = 50)
	private String userId;

	@Column(name = "phone_mobile", length = 50)
	private String phoneMobile;

	@Column(name = "phone_home", length = 50)
	private String phoneHome;

	@Column(name = "contact_email", length = 50)
	private String contactEmail;

	@Column(name = "period_from")
	private Date periodFrom;

	@Column(name = "period_to")
	private Date periodTo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Column(name = "status", length = 10)
	private String status;

	@Column(name = "internal_remark", length = 300)
	private String internalRemark;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long verNo;
	
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Long getContractorId()
	{
		return contractorId;
	}

	public void setContractorId(Long contractorId)
	{
		this.contractorId = contractorId;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public String getGivenName()
	{
		return givenName;
	}

	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public String getPassportNo()
	{
		return passportNo;
	}

	public void setPassportNo(String passportNo)
	{
		this.passportNo = passportNo;
	}

	public String getPassportType()
	{
		return passportType;
	}

	public void setPassportType(String passportType)
	{
		this.passportType = passportType;
	}

	public String getCompanyName()
	{
		return companyName;
	}

	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getPhoneMobile()
	{
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile)
	{
		this.phoneMobile = phoneMobile;
	}

	public String getPhoneHome()
	{
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome)
	{
		this.phoneHome = phoneHome;
	}

	public String getContactEmail()
	{
		return contactEmail;
	}

	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = contactEmail;
	}

	public Date getPeriodFrom()
	{
		return periodFrom;
	}

	public void setPeriodFrom(Date periodFrom)
	{
		this.periodFrom = periodFrom;
	}

	public Date getPeriodTo()
	{
		return periodTo;
	}

	public void setPeriodTo(Date periodTo)
	{
		this.periodTo = periodTo;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Long getHelperPassType()
	{
		return helperPassType;
	}

	public void setHelperPassType(Long helperPassType)
	{
		this.helperPassType = helperPassType;
	}

	public ContractHelper()
	{
		super();

	}

	public ContractHelper(Long contractorId)
	{
		super();
		this.contractorId = contractorId;
	}

}
