package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the staff_facility_schedule database table.
 * 
 */
@Embeddable
public class StaffFacilitySchedulePK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="staff_user_id", unique=true, nullable=false, length=50)
	private String staffUserId;

	@Column(name="facility_timeslot_id", insertable=false, updatable=false, unique=true, nullable=false, length=20)
	private Long facilityTimeslotId;

	public StaffFacilitySchedulePK() {
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof StaffFacilitySchedulePK)) {
			return false;
		}
		StaffFacilitySchedulePK castOther = (StaffFacilitySchedulePK)other;
		return 
			this.staffUserId.equals(castOther.staffUserId)
			&& this.facilityTimeslotId.equals(castOther.facilityTimeslotId);
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (facilityTimeslotId ^ (facilityTimeslotId >>> 32));
		result = prime * result + ((staffUserId == null) ? 0 : staffUserId.hashCode());
		return result;
	}

	public String getStaffUserId() {
		return staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}

	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}
}