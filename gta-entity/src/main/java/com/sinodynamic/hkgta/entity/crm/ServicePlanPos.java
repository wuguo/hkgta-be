package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the service_plan_pos database table.
 * 
 */
@Entity
@Table(name="service_plan_pos")
@NamedQuery(name="ServicePlanPos.findAll", query="SELECT s FROM ServicePlanPos s")
public class ServicePlanPos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="serv_pos_id", unique=true, nullable=false)
	private Long servPosId;

	@Column(name="description",length=200)
	private String description;

	@Column(name="age_range_code", length=10)
	private String ageRange;

	@Column(name="pos_item_no", length=50)
	private String posItemNo;

	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="plan_no", nullable=false)
	private ServicePlan servicePlan;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	public ServicePlanPos() {
	}

	public Long getServPosId() {
		return this.servPosId;
	}

	public void setServPosId(Long servPosId) {
		this.servPosId = servPosId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ServicePlan getServicePlan() {
		return this.servicePlan;
	}

	public void setServicePlan(ServicePlan servicePlan) {
		this.servicePlan = servicePlan;
	}

	public String getPosItemNo() {
		return posItemNo;
	}

	public void setPosItemNo(String posItemNo) {
		this.posItemNo = posItemNo;
	}

	public String getAgeRange() {
		return ageRange;
	}

	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}
}