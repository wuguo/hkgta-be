package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the login_history database table.
 * 
 */
@Entity
@Table(name="login_history")
@NamedQuery(name="LoginHistory.findAll", query="SELECT l FROM LoginHistory l")
public class LoginHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id", unique=true, nullable=false)
	private Long sysId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="attempt_login_date")
	private Date attemptLoginDate;

	@Column(name="last_login_status", length=1)
	private String lastLoginStatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="prev2_login_date")
	private Date prev2LoginDate;

	@Column(name="prev2_remote_ip", length=30)
	private String prev2RemoteIp;

	@Column(name="remote_ip", length=30)
	private String remoteIp;

	//bi-directional many-to-one association to UserMaster
	@Column(name="user_id", nullable=false)
	private String userId;

	public LoginHistory() {
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public Date getAttemptLoginDate() {
		return this.attemptLoginDate;
	}

	public void setAttemptLoginDate(Date attemptLoginDate) {
		this.attemptLoginDate = attemptLoginDate;
	}

	public String getLastLoginStatus() {
		return this.lastLoginStatus;
	}

	public void setLastLoginStatus(String lastLoginStatus) {
		this.lastLoginStatus = lastLoginStatus;
	}

	public Date getPrev2LoginDate() {
		return this.prev2LoginDate;
	}

	public void setPrev2LoginDate(Date prev2LoginDate) {
		this.prev2LoginDate = prev2LoginDate;
	}

	public String getPrev2RemoteIp() {
		return this.prev2RemoteIp;
	}

	public void setPrev2RemoteIp(String prev2RemoteIp) {
		this.prev2RemoteIp = prev2RemoteIp;
	}

	public String getRemoteIp() {
		return this.remoteIp;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

	

}