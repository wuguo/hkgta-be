package com.sinodynamic.hkgta.entity.mms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spa_invoice")
public class SpaInvoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inv_id", unique = true, nullable = false)
    private Long invId;

    @Column(name = "ext_invoice_no")
    private String extInvoiceNo;

    @Column(name = "total_amount")
    private BigDecimal totalAmount;

    @Column(name = "status")
    private String status;

    @Column(name = "cust_order_no")
    private String custOrderNo;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "ext_sync_timestamp")
    private Date extSyncTimestamp;

    @Column(name = "ver_no")
    private Long verNo;

    public Long getInvId() {
	return invId;
    }

    public void setInvId(Long invId) {
	this.invId = invId;
    }

    public String getExtInvoiceNo() {
	return extInvoiceNo;
    }

    public void setExtInvoiceNo(String extInvoiceNo) {
	this.extInvoiceNo = extInvoiceNo;
    }

    public BigDecimal getTotalAmount() {
	return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
	this.totalAmount = totalAmount;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getCustOrderNo() {
	return custOrderNo;
    }

    public void setCustOrderNo(String custOrderNo) {
	this.custOrderNo = custOrderNo;
    }

    public Date getUpdateDate() {
	return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
	this.updateDate = updateDate;
    }

    public String getUpdateBy() {
	return updateBy;
    }

    public void setUpdateBy(String updateBy) {
	this.updateBy = updateBy;
    }

    public Date getExtSyncTimestamp() {
	return extSyncTimestamp;
    }

    public void setExtSyncTimestamp(Date extSyncTimestamp) {
	this.extSyncTimestamp = extSyncTimestamp;
    }

    public Long getVerNo() {
	return verNo;
    }

    public void setVerNo(Long verNo) {
	this.verNo = verNo;
    }

}
