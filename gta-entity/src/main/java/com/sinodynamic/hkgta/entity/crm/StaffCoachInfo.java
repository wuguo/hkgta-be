package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the staff_coach_info database table.
 * 
 */
@Entity
@Table(name="staff_coach_info")
@NamedQuery(name="StaffCoachInfo.findAll", query="SELECT s FROM StaffCoachInfo s")
public class StaffCoachInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "UserMasterGenerator")     
	@GenericGenerator(name = "UserMasterGenerator", strategy = "assigned")   
	@Column(name="user_id", unique=true, nullable=false, length=50)
	private String userId;

	@Column(name="create_by", length=50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	private Date createDate;

	@Column(name="update_by", length=50)
	private String updateBy;
	
	@Column(name="personal_info")
	private String personalInfo;
	
	@Column(name="speciality")
	private String speciality;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public StaffCoachInfo() {
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getPersonalInfo()
	{
		return personalInfo;
	}

	public void setPersonalInfo(String personalInfo)
	{
		this.personalInfo = personalInfo;
	}

	public String getSpeciality()
	{
		return speciality;
	}

	public void setSpeciality(String speciality)
	{
		this.speciality = speciality;
	}

}