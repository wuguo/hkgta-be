package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name="message_template")
public class MessageTemplate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8233751904568571717L;
	private static String PARAM_PREFIX = "{";
	private static String PARAM_SUFFIX = "}";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="template_id", unique=true, nullable=false)
	private Long templateId;
	
	@Column(name="category")
	private String category;
	
	@Column(name="function_id")
	private String functionId;
	
	@Column(name="template_name")
	private String templateName;
	
	@Column(name="message_subject")
	private String messageSubject;
	
	@Column(name="content")
	private String content;
	
	@Column(name="content_html")
	private String contentHtml;
	
	@Column(name="template_type")
	private String templateType;
	
	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Timestamp createDate;
	
	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Transient
	private String contactEmail;
	
	@Transient
	private Long customerId;
	
	@Column(name="macro_tag")
	private String macroTag;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	@Column(name="template_desc", length=500)
	private String templateDesc;
	
	public String getTemplateDesc() {
		return templateDesc;
	}

	public void setTemplateDesc(String templateDesc) {
		this.templateDesc = templateDesc;
	}

	public MessageTemplate(){
		
	}
	
	public MessageTemplate(Long templateId, String functionId, String templateName) {
	    
	    this.functionId = functionId;
	    this.templateName = templateName;
	    this.templateId = templateId;
	}


	public Long getTemplateId() {
		return templateId;
	}


	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}


	public String getFunctionId() {
		return functionId;
	}


	public void setFunctionId(String templateType) {
		this.functionId = templateType;
	}


	public String getTemplateName() {
		return templateName;
	}


	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getCreateBy() {
		return createBy;
	}


	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}


	public Timestamp getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}


	public String getUpdateBy() {
		return updateBy;
	}


	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getMessageSubject() {
		return messageSubject;
	}


	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}


	public String getContactEmail() {
		return contactEmail;
	}


	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}


	public Long getCustomerId() {
		return customerId;
	}


	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getVerNo() {
	    return verNo;
	}


	public void setVerNo(Long verNo) {
	    this.verNo = verNo;
	}


	public String getContentHtml() {
	    return contentHtml;
	}


	public void setContentHtml(String contentHtml) {
	    this.contentHtml = contentHtml;
	}


	public String getTemplateType() {
	    return templateType;
	}


	public void setTemplateType(String functionId) {
	    this.templateType = functionId;
	}

	public String getCategory() {
	    return category;
	}

	public void setCategory(String category) {
	    this.category = category;
	}

	public String getMacroTag() {
	    return macroTag;
	}

	public void setMacroTag(String macroTag) {
	    this.macroTag = macroTag;
	}
	
	public String getFullContent(String ... values) {
	    
	    String fullContent = new String(content);
	    if (values == null || values.length == 0 || StringUtils.isEmpty(macroTag)) return fullContent;
	    
	    String[] params = macroTag.split(",");
	    for (int i = 0; i < params.length; i++) {
		
		String param = params[i];
		if (StringUtils.isEmpty(param)) continue;
		
		String value = null;
		param = param.replace(PARAM_PREFIX, "\\" + PARAM_PREFIX).replace(PARAM_SUFFIX, "\\" + PARAM_SUFFIX);
		if (values.length >= (i + 1)) {
		    value = values[i];
		} else {
		    break;
		}
		
		fullContent = fullContent.replaceAll("(?i)" + param, value);
	    }
	    
	    return fullContent;
	}
	
	public String getFullContentHtml(String ... values) {
	    
	    String fullContent = new String(contentHtml);
	    if (values == null || values.length == 0 || StringUtils.isEmpty(macroTag)) return fullContent;
	    
	    String[] params = macroTag.split(",");
	    for (int i = 0; i < params.length; i++) {
		
		String param = params[i];
		if (StringUtils.isEmpty(param)) continue;
		
		String value = null;
		param = param.replace(PARAM_PREFIX, "\\" + PARAM_PREFIX).replace(PARAM_SUFFIX, "\\" + PARAM_SUFFIX);
		if (values.length >= (i + 1)) {
		    value = values[i];
		} else {
		    break;
		}
		
		fullContent = fullContent.replaceAll("(?i)" + param, value);
	    }
	    
	    return fullContent;
	}
	
}
