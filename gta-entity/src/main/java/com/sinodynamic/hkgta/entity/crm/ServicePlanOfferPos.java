package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "service_plan_offer_pos")
public class ServicePlanOfferPos implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long offerId;
	private Long servPosId;
	private String offerCode;
	private String posItemNo;
	private String description;
	
	
	private Long verNo;
	@Version
	@Column(name="ver_no", nullable=false)
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="offer_id")
	public Long getOfferId() {
		return offerId;
	}
	public void setOfferId(Long offerId) {
		this.offerId = offerId;
	}
	
	@Column(name="serv_pos_id")
	public Long getServPosId() {
		return servPosId;
	}
	public void setServPosId(Long servPosId) {
		this.servPosId = servPosId;
	}
	
	@Column(name="offer_code")
	public String getOfferCode() {
		return offerCode;
	}
	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}
	
	@Column(name="pos_item_no")
	public String getPosItemNo() {
		return posItemNo;
	}
	public void setPosItemNo(String posItemNo) {
		this.posItemNo = posItemNo;
	}
	
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
