package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

/**
 * The persistent class for the member_payment_acc database table.
 * 
 */
@Entity
@Table(name = "member_payment_acc_log")
public class MemberPaymentAccLog extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "log_id", unique = true, nullable = false)
	private Long logId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "acc_id", nullable = false)
	private MemberPaymentAcc memberPaymentAcc;
	
	@Column(name = "customer_id")
	private Long customerId;

	@Column(name = "acc_type", length = 10)
	private String accType;

	@Column(name = "bank_id", length = 20)
	private String bankId;

	@Column(name = "account_no", length = 50)
	private String accountNo;

	@Column(name = "status", length = 10)
	private String status;

	@Column(length = 500)
	private String remark;

	@Column(name = "originator_bank_code", length = 10)
	private String originatorBankCode;

	@Column(name = "drawer_bank_code", length = 10)
	private String drawerBankCode;

	@Column(name = "bank_customer_id", length = 50)
	private String bankCustomerId;

	@Column(name = "bank_account_name", length = 40)
	private String bankAccountName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "create_by")
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "log_timestamp")
	private Date logTimestamp;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public MemberPaymentAcc getMemberPaymentAcc() {
		return memberPaymentAcc;
	}

	public void setMemberPaymentAcc(MemberPaymentAcc memberPaymentAcc) {
		this.memberPaymentAcc = memberPaymentAcc;
	}
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOriginatorBankCode() {
		return originatorBankCode;
	}

	public void setOriginatorBankCode(String originatorBankCode) {
		this.originatorBankCode = originatorBankCode;
	}

	public String getDrawerBankCode() {
		return drawerBankCode;
	}

	public void setDrawerBankCode(String drawerBankCode) {
		this.drawerBankCode = drawerBankCode;
	}

	public String getBankCustomerId() {
		return bankCustomerId;
	}

	public void setBankCustomerId(String bankCustomerId) {
		this.bankCustomerId = bankCustomerId;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getLogTimestamp() {
		return logTimestamp;
	}

	public void setLogTimestamp(Date logTimestamp) {
		this.logTimestamp = logTimestamp;
	}
}