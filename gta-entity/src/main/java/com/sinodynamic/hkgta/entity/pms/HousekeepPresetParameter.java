package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the housekeep_preset_parameter database table.
 * 
 */
@Entity
@Table(name="housekeep_preset_parameter")
@NamedQuery(name="HousekeepPresetParameter.findAll", query="SELECT p FROM HousekeepPresetParameter p")
public class HousekeepPresetParameter implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "param_id", unique = true, nullable = false, length = 20)
	private Long paramId;

	@Column(name="param_cat" , nullable = false,length=50)
	private String paramCat;

	@Column(name="status",length=10)
	private String status;

	@Column(name="description",length=200)
	private String description;

	@Column(name="max_int",length=200)
	private Long maxInt;

	@Column(name="display_scope",length=10)
	private String displayScope;
	
	@Column(name="display_order")
	private Long displayOrder;
	
	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public HousekeepPresetParameter() {
	}

	public HousekeepPresetParameter(String paramCat) {
		this.paramCat = paramCat;
	}
	
	public Long getParamId() {
		return paramId;
	}


	public void setParamId(Long paramId) {
		this.paramId = paramId;
	}


	public String getParamCat() {
		return paramCat;
	}


	public void setParamCat(String paramCat) {
		this.paramCat = paramCat;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Long getMaxInt() {
		return maxInt;
	}


	public void setMaxInt(Long maxInt) {
		this.maxInt = maxInt;
	}


	public Long getDisplayOrder() {
		return displayOrder;
	}


	public void setDisplayOrder(Long displayOrder) {
		this.displayOrder = displayOrder;
	}


	public String getCreateBy() {
		return createBy;
	}


	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}


	public Timestamp getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}


	public String getUpdateBy() {
		return updateBy;
	}


	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getDisplayScope()
	{
		return displayScope;
	}


	public void setDisplayScope(String displayScope)
	{
		this.displayScope = displayScope;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
}