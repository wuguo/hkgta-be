package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * The persistent class for the hk_area database table.
 * 
 */
@Entity
@Table(name="hk_area")
@NamedQuery(name="HkArea.findAll", query="SELECT h FROM HkArea h")
public class HkArea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="area_code", unique=true, nullable=false, length=3)
	private String areaCode;

	@Column(name="area_name", length=16)
	private String areaName;

	@Column(name="area_name_tc", length=9)
	private String areaNameTc;
	
	@Transient
	private List<HkDistrict> hkDistricts;

	public HkArea() {
	}

	public String getAreaCode() {
		return this.areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public List<HkDistrict> getHkDistricts() {
		return this.hkDistricts;
	}

	public void setHkDistricts(List<HkDistrict> hkDistricts) {
		this.hkDistricts = hkDistricts;
	}

	public HkDistrict addHkDistrict(HkDistrict hkDistrict) {
		getHkDistricts().add(hkDistrict);
		hkDistrict.setHkArea(this);

		return hkDistrict;
	}

	public HkDistrict removeHkDistrict(HkDistrict hkDistrict) {
		getHkDistricts().remove(hkDistrict);
		hkDistrict.setHkArea(null);

		return hkDistrict;
	}

	public String getAreaNameTc() {
		return areaNameTc;
	}

	public void setAreaNameTc(String areaNameTc) {
		this.areaNameTc = areaNameTc;
	}
	
}