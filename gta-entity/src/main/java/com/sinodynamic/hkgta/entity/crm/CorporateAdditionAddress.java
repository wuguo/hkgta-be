package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the corporate_addition_address database table.
 * 
 */
@Entity
@Table(name="corporate_addition_address")
@NamedQuery(name="CorporateAdditionAddress.findAll", query="SELECT c FROM CorporateAdditionAddress c")
public class CorporateAdditionAddress implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="corporate_id")
	private Long corporateId;

	@Column(name="address_type",nullable=false)
	private String addressType;

	@Column(name="address1")
	private String address1;

	@Column(name="address2")
	private String address2;

	@Column(name="hk_district")
	private String hkDistrict;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;

	@OneToOne
	@JoinColumn(name="corporate_id",nullable=false, insertable=false, updatable=false)
	private CorporateProfile corporateProfile;

	public CorporateAdditionAddress() {
	}

	public Long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Long corporateId) {
		this.corporateId = corporateId;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getHkDistrict() {
		return hkDistrict;
	}

	public void setHkDistrict(String hkDistrict) {
		this.hkDistrict = hkDistrict;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public CorporateProfile getCorporateProfile() {
		return corporateProfile;
	}

	public void setCorporateProfile(CorporateProfile corporateProfile) {
		this.corporateProfile = corporateProfile;
	}

	

}