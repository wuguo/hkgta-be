package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;


/**
 * The persistent class for the corporate_member database table.
 * 
 */
@Entity
@Table(name="corporate_member")
@NamedQuery(name="CorporateMember.findAll", query="SELECT c FROM CorporateMember c")
public class CorporateMember  extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="customer_id", unique=true, nullable=false)
	@EncryptFieldInfo
	private Long customerId;

	@Column(name="authorized_position_title", length=50)
	private String authorizedPositionTitle;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Date createDate;

	@Column(length=10)
	private String status;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;

	@ManyToOne
	@JoinColumn(name="corporate_id", nullable=false)
	private CorporateProfile corporateProfile;
	
	public CorporateMember() {
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Object customerId) {
		/*if(customerId instanceof Long){
			this.customerId = ((Long) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}*/
		
		this.customerId = (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}

	public String getAuthorizedPositionTitle() {
		return this.authorizedPositionTitle;
	}

	public void setAuthorizedPositionTitle(String authorizedPositionTitle) {
		this.authorizedPositionTitle = authorizedPositionTitle;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public CorporateProfile getCorporateProfile() {
		return this.corporateProfile;
	}

	public void setCorporateProfile(CorporateProfile corporateProfile) {
		this.corporateProfile = corporateProfile;
	}
	
}