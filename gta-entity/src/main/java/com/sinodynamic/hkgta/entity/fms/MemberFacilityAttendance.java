package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the member_facility_attendance database table.
 * 
 */
@Entity
@Table(name="member_facility_attendance")
@NamedQuery(name="MemberFacilityAttendance.findAll", query="SELECT f FROM MemberFacilityAttendance f")
public class MemberFacilityAttendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MemberFacilityAttendancePK id;
	
	@Column(name="attend_time",nullable=false,length=20)
	private Timestamp attendTime;
	
	@Column(name="machine_id",length=50)
	private String machineId;
	
	@Column(name="trainer_comment",length=500)
	private String trainerComment;
	
	@Column(name="customer_comment",length=500)
	private String customerComment;
	
	@Column(name="assignment",length=500)
	private String assignment;
	
	@Column(name="score")
	private Double score;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public MemberFacilityAttendance() {
	}

	public MemberFacilityAttendancePK getId() {
		return id;
	}

	public void setId(MemberFacilityAttendancePK id) {
		this.id = id;
	}

	public Timestamp getAttendTime() {
		return attendTime;
	}

	public void setAttendTime(Timestamp attendTime) {
		this.attendTime = attendTime;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getTrainerComment()
	{
		return trainerComment;
	}

	public void setTrainerComment(String trainerComment)
	{
		this.trainerComment = trainerComment;
	}

	public String getCustomerComment()
	{
		return customerComment;
	}

	public void setCustomerComment(String customerComment)
	{
		this.customerComment = customerComment;
	}

	public String getAssignment()
	{
		return assignment;
	}

	public void setAssignment(String assignment)
	{
		this.assignment = assignment;
	}

	public Double getScore()
	{
		return score;
	}

	public void setScore(Double score)
	{
		this.score = score;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
}