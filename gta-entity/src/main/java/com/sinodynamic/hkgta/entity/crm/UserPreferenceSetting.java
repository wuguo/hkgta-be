package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the user_preference_setting database table.
 * 
 */
@Entity
@Table(name="user_preference_setting")
@NamedQuery(name="UserPreferenceSetting.findAll", query="SELECT u FROM UserPreferenceSetting u")
public class UserPreferenceSetting implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserPreferenceSettingPK id;

	@Column(name="param_value", length=50)
	private String paramValue;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	//bi-directional many-to-one association to GlobalParameter
	@ManyToOne
	@JoinColumn(name="param_id", nullable=false, insertable=false, updatable=false)
	private GlobalParameter globalParameter;

	public UserPreferenceSetting() {
	}

	public UserPreferenceSettingPK getId() {
		return this.id;
	}

	public void setId(UserPreferenceSettingPK id) {
		this.id = id;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public GlobalParameter getGlobalParameter() {
		return this.globalParameter;
	}

	public void setGlobalParameter(GlobalParameter globalParameter) {
		this.globalParameter = globalParameter;
	}
}