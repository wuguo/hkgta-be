package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.entity.GenericDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;


/**
 * The persistent class for the customer_enroll_po database table.
 * 
 */
@Entity
@Table(name="customer_enroll_po")
@NamedQuery(name="CustomerEnrollPo.findAll", query="SELECT c FROM CustomerEnrollPo c")
public class CustomerEnrollPo extends GenericDto  implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CustomerEnrollPoPK id;

	//bi-directional many-to-one association to CustomerEnrollment
	@ManyToOne
	@JoinColumn(name="enroll_id", nullable=false, insertable=false, updatable=false)
	@JsonIgnore
	private CustomerEnrollment customerEnrollment;

	//bi-directional many-to-one association to CustomerOrderDet
	@ManyToOne 
	@JoinColumn(name="order_det_id", nullable=false, insertable=false, updatable=false)
	private CustomerOrderDet customerOrderDet;

	//bi-directional many-to-one association to CustomerOrderHd
	
/*	@ManyToOne
	@JoinColumn(name="order_no", nullable=false)*/
	@Transient
	private CustomerOrderHd customerOrderHd;
	
	@Column(name="order_no", nullable=false)
	private Long orderNo;


	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Object orderNo) {
		/*if(orderNo instanceof Long){
			this.orderNo = ((Long) orderNo).longValue();
		}else if(orderNo instanceof Integer){
			this.orderNo = ((Integer) orderNo).longValue();
		}else if(orderNo instanceof String){
			this.orderNo = Long.valueOf((String) orderNo);
		}else{
			this.orderNo = (Long) orderNo;
		}*/
		this.orderNo = (orderNo!=null ? NumberUtils.toLong(orderNo.toString()):null);
	}

	public CustomerEnrollPo() {
	}

	public CustomerEnrollPoPK getId() {
		return this.id;
	}

	public void setId(CustomerEnrollPoPK id) {
		this.id = id;
	}

	public CustomerEnrollment getCustomerEnrollment() {
		return this.customerEnrollment;
	}

	public void setCustomerEnrollment(CustomerEnrollment customerEnrollment) {
		this.customerEnrollment = customerEnrollment;
	}

	public CustomerOrderDet getCustomerOrderDet() {
		return this.customerOrderDet;
	}

	public void setCustomerOrderDet(CustomerOrderDet customerOrderDet) {
		this.customerOrderDet = customerOrderDet;
	}

 	public CustomerOrderHd getCustomerOrderHd() {
 		return this.customerOrderHd;
 	}
//
 	public void setCustomerOrderHd(CustomerOrderHd customerOrderHd) {
		this.customerOrderHd = customerOrderHd;
 	}

}