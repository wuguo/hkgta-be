package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;


/**
 * The persistent class for the member database table.
 * 
 */
@Entity
@Table(name="member")
@NamedQuery(name="Member.findAll", query="SELECT m FROM Member m")
public class Member extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="customer_id", unique=true, nullable=false)
	@EncryptFieldInfo  
	private Long customerId;

	@Column(name="academy_no", length=20)
	private String academyNo;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date", nullable=false)
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_date")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="first_join_date")
	private Date firstJoinDate;

	@Column(name="internal_remark", length=300)
	private String internalRemark;

	@Column(name="relationship_code", length=10)
	private String relationshipCode;

	@Column(length=10)
	private String status;

	@Temporal(TemporalType.DATE)
	@Column(name="termination_date")
	private Date terminationDate;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	//cancel mapping by oneself
	@Column(name="superior_member_id")
	private Long superiorMemberId;

	@Column(name="member_type")
	private String memberType;

	@Column(name="user_id")
	private String userId;
	
	//added by Kaster 20160510
	@Column(name="vip")
	private String vip;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;

	//bi-directional one-to-one association to MemberCashvalue
	@OneToOne(mappedBy="member", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@EncryptFieldInfo 
	private MemberCashvalue memberCashvalue;


	//bi-directional many-to-one association to MemberPaymentAcc
	@OneToMany(mappedBy="member", cascade=CascadeType.ALL , orphanRemoval = true, fetch=FetchType.EAGER)
	@OrderBy("accId")
	@EncryptFieldInfo 
	private Set<MemberPaymentAcc> memberPaymentAccs;

	//bi-directional many-to-one association to MemberPlanFacilityRight
	@OneToMany(mappedBy="member", cascade=CascadeType.ALL , orphanRemoval = true, fetch=FetchType.EAGER)
	@OrderBy("id")
	@EncryptFieldInfo 
	private Set<MemberPlanFacilityRight> memberPlanFacilityRights;

	@Transient
	private String dependentCreation;
	

	public String getDependentCreation() {
		return dependentCreation;
	}

	public void setDependentCreation(String dependentCreation) {
		this.dependentCreation = dependentCreation;
	}

	public Member() {
	}

	public Member(Long superiorMemberId) {
		super();
		this.superiorMemberId = superiorMemberId;
	}

	public String getAcademyNo() {
		return this.academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getFirstJoinDate() {
		return this.firstJoinDate;
	}

	public void setFirstJoinDate(Date firstJoinDate) {
		this.firstJoinDate = firstJoinDate;
	}

	public String getInternalRemark() {
		return this.internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getRelationshipCode() {
		return this.relationshipCode;
	}

	public void setRelationshipCode(String relationshipCode) {
		this.relationshipCode = relationshipCode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTerminationDate() {
		return this.terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		/*if(customerId instanceof Long){
			this.customerId = ((Long) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}*/
		
		this.customerId = (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}

	public Long getSuperiorMemberId() {
		return superiorMemberId;
	}

	public void setSuperiorMemberId(Object superiorMemberId) {
		/*if(superiorMemberId instanceof Long){
			this.superiorMemberId = ((Long) superiorMemberId).longValue();
		}else if(superiorMemberId instanceof Integer){
			this.superiorMemberId = ((Integer) superiorMemberId).longValue();
		}else if(superiorMemberId instanceof String){
			this.superiorMemberId = Long.valueOf((String) superiorMemberId);
		}else{
			this.superiorMemberId = (Long) superiorMemberId;
		}*/
		this.superiorMemberId = (superiorMemberId!=null ? NumberUtils.toLong(superiorMemberId.toString()):null);
	}

	public MemberCashvalue getMemberCashvalue() {
		return this.memberCashvalue;
	}

	public void setMemberCashvalue(MemberCashvalue memberCashvalue) {
		this.memberCashvalue = memberCashvalue;
	}


	public Set<MemberPaymentAcc> getMemberPaymentAccs() {
		return this.memberPaymentAccs;
	}

	public void setMemberPaymentAccs(Set<MemberPaymentAcc> memberPaymentAccs) {
		this.memberPaymentAccs = memberPaymentAccs;
	}

	public MemberPaymentAcc addMemberPaymentAcc(MemberPaymentAcc memberPaymentAcc) {
		getMemberPaymentAccs().add(memberPaymentAcc);
		memberPaymentAcc.setMember(this);

		return memberPaymentAcc;
	}

	public MemberPaymentAcc removeMemberPaymentAcc(MemberPaymentAcc memberPaymentAcc) {
		getMemberPaymentAccs().remove(memberPaymentAcc);
		memberPaymentAcc.setMember(null);

		return memberPaymentAcc;
	}

	public Set<MemberPlanFacilityRight> getMemberPlanFacilityRights() {
		return this.memberPlanFacilityRights;
	}

	public void setMemberPlanFacilityRights(Set<MemberPlanFacilityRight> memberPlanFacilityRights) {
		this.memberPlanFacilityRights = memberPlanFacilityRights;
	}

	public MemberPlanFacilityRight addMemberPlanFacilityRight(MemberPlanFacilityRight memberPlanFacilityRight) {
		getMemberPlanFacilityRights().add(memberPlanFacilityRight);
		memberPlanFacilityRight.setMember(this);

		return memberPlanFacilityRight;
	}

	public MemberPlanFacilityRight removeMemberPlanFacilityRight(MemberPlanFacilityRight memberPlanFacilityRight) {
		getMemberPlanFacilityRights().remove(memberPlanFacilityRight);
		memberPlanFacilityRight.setMember(null);

		return memberPlanFacilityRight;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Object version) {
		this.version = (version != null ? NumberUtils.toLong(version.toString()) : null);
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getVip() {
		return vip==null ? "N" : vip;
	}

	public void setVip(String vip) {
		this.vip = vip;
	}
	
}