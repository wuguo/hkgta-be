package com.sinodynamic.hkgta.entity.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the customer_order_trans database table.
 * 
 */
@Entity
@Table(name="customer_order_trans")
@NamedQuery(name="CustomerOrderTrans.findAll", query="SELECT c FROM CustomerOrderTrans c")
public class CustomerOrderTrans implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="transaction_no", unique=true, nullable=false)
	private Long transactionNo;

	@Column(name="agent_transaction_no", length=50)
	private String agentTransactionNo;

	@Column(name="paid_amount", precision=10, scale=2)
	private BigDecimal paidAmount;

	@Column(name="payment_method_code", length=10)
	private String paymentMethodCode;

	@Column(name="payment_media", length=10)
	private String paymentMedia;

	@Column(length=10)
	private String status;

	@Column(name="terminal_id", length=20)
	private String terminalId;
	
	@Column(name="payment_card_no", length=20)
	private String paymentCardNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transaction_timestamp")
	private Date transactionTimestamp;
	
	@Column(name="from_transaction_no")
	private Long fromTransactionNo;
	
	@Column(name = "transcript_file_name", length=200)
	private String transcriptFileName;
	
	@Column(name = "payment_recv_by",length = 50)
	private String paymentRecvBy;
	
	@Column(name = "audit_by", length = 50)
	private String auditBy;
	
	@Column(name = "audit_date")
	private Timestamp auditDate;

	@Column(name="internal_remark", length=300)
	private String internalRemark;
	
	@Column(name = "opg_response_code", length = 10)
	private String opgResponseCode;
	
	@Column(name = "read_by", length = 50)
	private String readBy;
	
	@Column(name = "read_date")
	private Timestamp readDate;
	
	@Column(name = "payment_location_code", length = 10)
	private String paymentLocationCode;
	
	@Column(name = "create_by", length = 50)
	private String createBy;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;
	
	@Column(name = "ext_ref_no", length = 50)
	private String extRefNo;
	
	@Column(name = "update_date")
	private Timestamp updateDate;
	
	@Version
	@Column(name = "ver_no", nullable = false)
	private Long verNo;
	
	//bi-directional many-to-one association to CustomerOrderHd
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="order_no", nullable=false)
	@JsonIgnore
	private CustomerOrderHd customerOrderHd;

	public CustomerOrderTrans() {
	}

	
	public String getExtRefNo() {
		return extRefNo;
	}

	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public void setExtRefNo(String extRefNo) {
		this.extRefNo = extRefNo;
	}


	public Long getTransactionNo() {
		return this.transactionNo;
	}

	public void setTransactionNo(Long transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getAgentTransactionNo() {
		return this.agentTransactionNo;
	}

	public void setAgentTransactionNo(String agentTransactionNo) {
		this.agentTransactionNo = agentTransactionNo;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPaymentMethodCode() {
		return this.paymentMethodCode;
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode.trim();
	}

	public String getPaymentMedia() {
		return paymentMedia;
	}

	public void setPaymentMedia(String paymentMedia) {
		this.paymentMedia = paymentMedia;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public Date getTransactionTimestamp() {
		return this.transactionTimestamp;
	}

	public void setTransactionTimestamp(Date transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}

	public Long getFromTransactionNo() {
		return fromTransactionNo;
	}

	public void setFromTransactionNo(Long fromTransactionNo) {
		this.fromTransactionNo = fromTransactionNo;
	}

	public CustomerOrderHd getCustomerOrderHd() {
		return this.customerOrderHd;
	}

	public void setCustomerOrderHd(CustomerOrderHd customerOrderHd) {
		this.customerOrderHd = customerOrderHd;
	}

	public String getTranscriptFileName() {
		return transcriptFileName;
	}

	public void setTranscriptFileName(String transcriptFileName) {
		this.transcriptFileName = transcriptFileName;
	}

	public String getPaymentRecvBy() {
		return paymentRecvBy;
	}

	public void setPaymentRecvBy(String paymentRecvBy) {
		this.paymentRecvBy = paymentRecvBy;
	}

	public String getAuditBy() {
		return auditBy;
	}

	public void setAuditBy(String auditBy) {
		this.auditBy = auditBy;
	}

	public String getOpgResponseCode() {
		return opgResponseCode;
	}

	public void setOpgResponseCode(String opgResponseCode) {
		this.opgResponseCode = opgResponseCode;
	}

	public Timestamp getAuditDate()
	{
		return auditDate;
	}

	public void setAuditDate(Timestamp auditDate)
	{
		this.auditDate = auditDate;
	}

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getReadBy() {
		return readBy;
	}

	public void setReadBy(String readBy) {
		this.readBy = readBy;
	}

	public Timestamp getReadDate() {
		return readDate;
	}

	public void setReadDate(Timestamp readDate) {
		this.readDate = readDate;
	}

	public String getPaymentLocationCode() {
		return paymentLocationCode;
	}

	public void setPaymentLocationCode(String paymentLocationCode) {
		this.paymentLocationCode = paymentLocationCode;
	}
	
	public String getPaymentCardNo() {
		return paymentCardNo;
	}

	public void setPaymentCardNo(String paymentCardNo) {
		this.paymentCardNo = paymentCardNo;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	
}