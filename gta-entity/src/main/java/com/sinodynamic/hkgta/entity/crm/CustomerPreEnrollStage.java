package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the customer_pre_enroll_stage database table.
 * 
 */
@Entity
@Table(name="customer_pre_enroll_stage")
@NamedQuery(name="CustomerPreEnrollStage.findAll", query="SELECT c FROM CustomerPreEnrollStage c")
public class CustomerPreEnrollStage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="stage_id", unique=true, nullable=false)
	private Long stageId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="comment_date")
	private Date commentDate;

	@Column(length=300)
	private String comments;

	@Column(name="enroll_status", length=10)
	private String enrollStatus;

	@Column(name="sales_user_id", length=50)
	private String salesUserId;

	@Column(name="customer_id", nullable=false)
	private Long customerId;

	@Column(name="comment_status")
	private String commentStatus;

	@Column(name="comment_by")
	private String commentBy;
	
	@Column(name="comment_by_device")
	private String commentByDevice;
	
	public CustomerPreEnrollStage() {
	}

	public Long getStageId() {
		return this.stageId;
	}

	public void setStageId(Long stageId) {
		this.stageId = stageId;
	}

	public Date getCommentDate() {
		return this.commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getEnrollStatus() {
		return this.enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}

	public String getSalesUserId() {
		return this.salesUserId;
	}

	public void setSalesUserId(String salesUserId) {
		this.salesUserId = salesUserId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCommentStatus() {
		return commentStatus;
	}

	public void setCommentStatus(String commentStatus) {
		this.commentStatus = commentStatus;
	}
	
	public String getCommentBy() {
		return commentBy;
	}

	public void setCommentBy(String commentBy) {
		this.commentBy = commentBy;
	}

	public String getCommentByDevice() {
		return commentByDevice;
	}

	public void setCommentByDevice(String commentByDevice) {
		this.commentByDevice = commentByDevice;
	}

}