package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the notice_recipient database table.
 * 
 */
@Entity
@Table(name="notice_recipient")
@NamedQuery(name="NoticeRecipient.findAll", query="SELECT n FROM NoticeRecipient n")
public class NoticeRecipient implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private NoticeRecipientPK id;

	@Column(name="sys_id", nullable=false)
	private Long sysId;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	//bi-directional many-to-one association to Notice
	@ManyToOne
	@JoinColumn(name="notice_id", nullable=false, insertable=false, updatable=false)
	private Notice notice;

	public NoticeRecipient() {
	}

	
	public Long getVerNo() {
		return verNo;
	}


	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}


	public NoticeRecipientPK getId() {
		return this.id;
	}

	public void setId(NoticeRecipientPK id) {
		this.id = id;
	}

	public Long getSysId() {
		return this.sysId;
	}

	public void setSysId(Object sysId) {
		this.sysId = (sysId!=null ? NumberUtils.toLong(sysId.toString()):null);
	}

	public Notice getNotice() {
		return this.notice;
	}

	public void setNotice(Notice notice) {
		this.notice = notice;
	}

}