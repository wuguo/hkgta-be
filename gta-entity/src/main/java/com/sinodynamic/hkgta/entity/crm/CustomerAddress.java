package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;


/**
 * The persistent class for the customer_address database table.
 * 
 */
@Entity
@Table(name="customer_address")
@NamedQuery(name="CustomerAddress.findAll", query="SELECT c FROM CustomerAddress c")
public class CustomerAddress  extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@EncryptFieldInfo 
	private CustomerAddressPK id;

	@Column(length=250)
	private String address1;
	
	@Column(length=250)
	private String address2;

	@Column(name="hk_district", length=30)
	private String hkDistrict;

	/*@ManyToOne
	@JoinColumn(name="customer_id", nullable=false, insertable=false, updatable=false)
	@JsonIgnore*/
	@Transient
	private CustomerProfile customerProfile;

	@Transient
	private String addressType;
	
	public CustomerAddress() {
	}

	public CustomerAddressPK getId() {
		return this.id;
	}

	public void setId(CustomerAddressPK id) {
		this.id = id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getHkDistrict() {
		return this.hkDistrict;
	}

	public void setHkDistrict(String hkDistrict) {
		this.hkDistrict = hkDistrict;
	}

	public CustomerProfile getCustomerProfile() {
		return this.customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	
}