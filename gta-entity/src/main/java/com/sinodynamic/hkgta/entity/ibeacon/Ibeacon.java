package com.sinodynamic.hkgta.entity.ibeacon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the ibeacon database table.
 * 
 */
@Entity
@Table(name="ibeacon")
@NamedQuery(name="Ibeacon.findAll", query="SELECT i FROM Ibeacon i")
public class Ibeacon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ibeacon_id")
	private Long ibeaconId;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="guid")
	private String guid;
	
	@Column(name="major")
	private Long major;

	@Column(name="minor")
	private Long minor;

	@Column(name="status")
	private String status;

	@Column(name="update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="site_poi_id")
	private Long sitePoiId;
	
	@Column(name="location")
	private String location;
	
	public Ibeacon() {
	}

	public Long getIbeaconId() {
		return ibeaconId;
	}

	public void setIbeaconId(Long ibeaconId) {
		this.ibeaconId = ibeaconId;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getMajor() {
		return major;
	}

	public void setMajor(Long major) {
		this.major = major;
	}

	public Long getMinor() {
		return minor;
	}

	public void setMinor(Long minor) {
		this.minor = minor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getSitePoiId() {
		return sitePoiId;
	}

	public void setSitePoiId(Long sitePoiId) {
		this.sitePoiId = sitePoiId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
}