package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OrderBy;


/**
 * The persistent class for the presentation_batch database table.
 * 
 */
@Entity
@Table(name="presentation_batch")
@NamedQuery(name="PresentationBatch.findAll", query="SELECT p FROM PresentationBatch p")
public class PresentationBatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="present_id", unique=true, nullable=false)
	private Long presentId;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(length=250)
	private String description;

	@Column(name="present_name", length=150)
	private String presentName;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;


	@OneToMany(mappedBy="presentationBatch" , cascade=CascadeType.ALL , orphanRemoval = true, fetch=FetchType.EAGER)
	@OrderBy(clause = "presentSeq asc")
	private List<PresentMaterialSeq> presentMaterialSeqs;

	public PresentationBatch() {
	}

	public PresentationBatch(Long presentId) {
		super();
		this.presentId = presentId;
	}

	public Long getPresentId() {
		return presentId;
	}



	public void setPresentId(Long presentId) {
		this.presentId = presentId;
	}



	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPresentName() {
		return this.presentName;
	}

	public void setPresentName(String presentName) {
		this.presentName = presentName;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public List<PresentMaterialSeq> getPresentMaterialSeqs() {
		return presentMaterialSeqs;
	}

	public void setPresentMaterialSeqs(List<PresentMaterialSeq> presentMaterialSeqs) {
		this.presentMaterialSeqs = presentMaterialSeqs;
	}


	public PresentMaterialSeq addPresentMaterialSeq(PresentMaterialSeq presentMaterialSeq) {
		getPresentMaterialSeqs().add(presentMaterialSeq);
		presentMaterialSeq.setPresentationBatch(this);
		return presentMaterialSeq;
	}

	public PresentMaterialSeq removePresentMaterialSeq(PresentMaterialSeq presentMaterialSeq) {
		getPresentMaterialSeqs().remove(presentMaterialSeq);
		presentMaterialSeq.setPresentationBatch(null);
		return presentMaterialSeq;
	}

}