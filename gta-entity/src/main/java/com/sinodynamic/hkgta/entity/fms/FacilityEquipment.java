package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.sinodynamic.hkgta.entity.crm.Equipment;


/**
 * The persistent class for the facility_equipment database table.
 * 
 */
@Entity
@Table(name="facility_equipment")
@NamedQuery(name="FacilityEquipment.findAll", query="SELECT f FROM FacilityEquipment f")
public class FacilityEquipment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="facility_no", unique=true, nullable=false, length=20)
	private Long facilityNo;

	//bi-directional many-to-one association to Equipment
	@ManyToOne
	@JoinColumn(name="equipment", nullable=false)
	private Equipment equipmentBean;

	//bi-directional one-to-one association to FacilityMaster
	@OneToOne
	@JoinColumn(name="facility_no", nullable=false, insertable=false, updatable=false)
	private FacilityMaster facilityMaster;

	public FacilityEquipment() {
	}


	public Equipment getEquipmentBean() {
		return this.equipmentBean;
	}

	public void setEquipmentBean(Equipment equipmentBean) {
		this.equipmentBean = equipmentBean;
	}

	public FacilityMaster getFacilityMaster() {
		return this.facilityMaster;
	}

	public void setFacilityMaster(FacilityMaster facilityMaster) {
		this.facilityMaster = facilityMaster;
	}


	public Long getFacilityNo() {
		return facilityNo;
	}


	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}

}