package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the permit_card_master database table.
 * 
 */
@Entity
@Table(name="helper_pass_permission")
@NamedQuery(name="HelperPassPermission.findAll", query="SELECT p FROM HelperPassPermission p")
public class HelperPassPermission implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id", unique=true, nullable=false)
	private Long sysId;
	
	@Column(name="helper_pass_type_id", nullable=false)
	private Long helperPassTypeId;
	
	@Column(name="terminal_id", nullable=false)
	private Long terminalId;
	
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="helper_pass_type_id",nullable=false, insertable=false, updatable=false)
	private HelperPassType helperPassType;
	
	public HelperPassPermission() {
	}

	public Long getSysId()
	{
		return sysId;
	}

	public void setSysId(Long sysId)
	{
		this.sysId = sysId;
	}

	public Long getHelperPassTypeId()
	{
		return helperPassTypeId;
	}

	public void setHelperPassTypeId(Long helperPassTypeId)
	{
		this.helperPassTypeId = helperPassTypeId;
	}

	public Long getTerminalId()
	{
		return terminalId;
	}

	public void setTerminalId(Long terminalId)
	{
		this.terminalId = terminalId;
	}

	public HelperPassType getHelperPassType()
	{
		return helperPassType;
	}

	public void setHelperPassType(HelperPassType helperPassType)
	{
		this.helperPassType = helperPassType;
	}

	
}