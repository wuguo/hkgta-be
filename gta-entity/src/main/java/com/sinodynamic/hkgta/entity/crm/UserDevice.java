package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="user_device")
@NamedQuery(name="UserDevice.findAll", query="SELECT u FROM UserDevice u")
public class UserDevice implements Serializable{
	private static final long serialVersionUID = -1426852573704806780L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="sys_id", unique=true, nullable=false, length=11)
	private Long sysId;
	@Column(name="device_token",nullable=false, length=200)
	private String deviceToken;
	@Column(name="user_id",length=50)
	private String userId;
	@Column(name="platform",length=50)
	private String platform;
	@Column(name="device_language",length=50)
	private String deviceLanguage;
	@Column(name="system_version",length=100)
	private String systemVersion;
	@Column(name="arn_endpoint",length=100)
	private String arnEndpoint;
	@Column(name="arn_application",length=100)
	private String arnApplication;
	@Column(name="arn_topic",length=100)
	private String arnTopic;
	@Column(name="status",length=10)
	private String status;
	@Column(name="create_date")
	private Date createDate;
	@Column(name="create_by", length=50)
	private String createBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	@Column(name="update_by", length=50)
	private String updateBy;
	public Long getSysId() {
		return sysId;
	}
	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getDeviceLanguage() {
		return deviceLanguage;
	}
	public void setDeviceLanguage(String deviceLanguage) {
		this.deviceLanguage = deviceLanguage;
	}
	public String getSystemVersion() {
		return systemVersion;
	}
	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}
	public String getArnEndpoint() {
		return arnEndpoint;
	}
	public void setArnEndpoint(String arnEndpoint) {
		this.arnEndpoint = arnEndpoint;
	}
	public String getArnApplication() {
		return arnApplication;
	}
	public void setArnApplication(String arnApplication) {
		this.arnApplication = arnApplication;
	}
	public String getArnTopic() {
		return arnTopic;
	}
	public void setArnTopic(String arnTopic) {
		this.arnTopic = arnTopic;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
	

}
