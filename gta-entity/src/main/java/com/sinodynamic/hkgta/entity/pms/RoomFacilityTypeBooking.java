package com.sinodynamic.hkgta.entity.pms;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "room_facility_type_booking")
public class RoomFacilityTypeBooking implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "sys_id")
	private Long sysId;

	@Column(name = "room_resv_id")
	private Long roomBookingId;

	@Column(name = "facility_type_resv_id")
	private Long facilityTypeBookingId;

	@Column(name = "is_bundle")
	private String isBundle;

	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "create_by")
	private String createBy;

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public Long getRoomBookingId() {
		return roomBookingId;
	}

	public void setRoomBookingId(Long roomBookingId) {
		this.roomBookingId = roomBookingId;
	}

	public Long getFacilityTypeBookingId() {
		return facilityTypeBookingId;
	}

	public void setFacilityTypeBookingId(Long facilityTypeBookingId) {
		this.facilityTypeBookingId = facilityTypeBookingId;
	}

	public String getIsBundle() {
		return isBundle;
	}

	public void setIsBundle(String isBundle) {
		this.isBundle = isBundle;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

}
