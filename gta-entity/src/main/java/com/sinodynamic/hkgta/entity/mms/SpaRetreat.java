package com.sinodynamic.hkgta.entity.mms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="spa_retreat")
public class SpaRetreat implements Serializable{
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ret_id", unique=true, nullable=false)
	private Long retId;
	
	@Column(name="ret_name")
	private String retreatName;
	
	@Column(name="status")
	private String status;
	
	@Column(name="description")
	private String description;
	
	@Column(name="category_id")
	private String categoryId;
	
	@Column(name="pic_path")
	private String picPath;
	
	@Column(name="display_order")
	private Long displayOrder;
	
	@Column(name="create_date")
	private Timestamp createDate;
	
	@Column(name="create_by")
	private String createBy;
	
	@Column(name="update_date")
	private Timestamp updateDate;
	
	@Column(name="update_by")
	private String updateBy;
	
	@Version
	@Column(name = "ver_no", nullable = false)
	private Long verNo;

	@OneToMany(mappedBy="spaRetreat")
	private List<SpaRetreatItem> spaRetreatItems;
	
	public SpaRetreat(){
		
	}
	
	public Long getRetId() {
		return retId;
	}

	public void setRetId(Long retId) {
		this.retId = retId;
	}

	public String getRetreatName() {
		return retreatName;
	}

	public void setRetreatName(String retreatName) {
		this.retreatName = retreatName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public Long getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Long displayOrder) {
		this.displayOrder = displayOrder;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public List<SpaRetreatItem> getSpaRetreatItems() {
		return spaRetreatItems;
	}

	public void setSpaRetreatItems(List<SpaRetreatItem> spaRetreatItems) {
		this.spaRetreatItems = spaRetreatItems;
	}
	
	
	
}
