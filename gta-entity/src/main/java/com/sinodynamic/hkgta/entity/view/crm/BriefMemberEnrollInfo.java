package com.sinodynamic.hkgta.entity.view.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "v_member_enroll_brief_info")
public class BriefMemberEnrollInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String memberName;
	private String enrollSattus;
	private Date enrollDate;
	private Long subscribePlanNo;
	private String givenName;
	private String salesFollowBy;

	
	public BriefMemberEnrollInfo() {
	}
	
	@Id
	@Column(name="given_name")
	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	
	@Column(name="sales_follow_by")
	public String getSalesFollowBy() {
		return salesFollowBy;
	}

	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}
	
	@Column(name = "member_name")
	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	@Column(name = "enroll_status")
	public String getEnrollSattus() {
		return enrollSattus;
	}

	public void setEnrollSattus(String enrollSattus) {
		this.enrollSattus = enrollSattus;
	}
	
	@Column(name = "enroll_date")
	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}
	
	@Column(name = "subscribe_plan_no")
	public Long getSubscribePlanNo() {
		return subscribePlanNo;
	}

	public void setSubscribePlanNo(Long subscribePlanNo) {
		this.subscribePlanNo = subscribePlanNo;
	}
	

}
