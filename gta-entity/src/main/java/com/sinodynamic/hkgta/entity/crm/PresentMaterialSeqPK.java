package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the present_material_seq database table.
 * 
 */
@Embeddable
public class PresentMaterialSeqPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="present_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Long presentId;

	@Column(name="material_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Long materialId;

	public PresentMaterialSeqPK() {
	}
	public Long getPresentId() {
		return this.presentId;
	}
	public void setPresentId(Long presentId) {
		this.presentId = presentId;
	}
	public Long getMaterialId() {
		return this.materialId;
	}
	public void setMaterialId(Long materialId) {

		this.materialId = materialId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PresentMaterialSeqPK)) {
			return false;
		}
		PresentMaterialSeqPK castOther = (PresentMaterialSeqPK)other;
		return 
			(this.presentId.equals(castOther.presentId))
			&& (this.materialId.equals(castOther.materialId));
	}

	public int hashCode() {
		final Long prime = 31L;
		Long hash = 17L;
		hash = hash * prime + this.presentId;
		hash = hash * prime + this.materialId;
		
		return hash.intValue();
	}
}