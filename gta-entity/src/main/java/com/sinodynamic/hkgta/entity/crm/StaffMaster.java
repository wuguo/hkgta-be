package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the staff_master database table.
 * 
 */
@Entity
@Table(name="staff_master")
@NamedQuery(name="StaffMaster.findAll", query="SELECT s FROM StaffMaster s")
public class StaffMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")     
	@GenericGenerator(name = "paymentableGenerator", strategy = "assigned")   
	@Column(name="user_id", unique=true, nullable=false, length=50)
	private String userId;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Temporal(TemporalType.DATE)
	@Column(name="employ_date")
	private Date employDate;

	@Column(name="position_title_code", length=20)
	private String positionTitle;

	@Temporal(TemporalType.DATE)
	@Column(name="quit_date")
	private Date quitDate;

	@Column(name="staff_no", nullable=false, length=50)
	private String staffNo;

	@Column(name="staff_type", length=10)
	private String staffType;

	@Column(name="status", length=10)
	private String status;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	//bi-directional one-to-one association to StaffProfile
	@OneToOne(mappedBy="staffMaster", fetch=FetchType.LAZY)
	private StaffProfile staffProfile;

	public StaffMaster() {
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Date getEmployDate() {
		return this.employDate;
	}

	public void setEmployDate(Date employDate) {
		this.employDate = employDate;
	}

	public Date getQuitDate() {
		return this.quitDate;
	}

	public void setQuitDate(Date quitDate) {
		this.quitDate = quitDate;
	}

	public String getStaffNo() {
		return this.staffNo;
	}

	public void setStaffNo(String staffNo) {
		this.staffNo = staffNo;
	}

	public String getStaffType() {
		return this.staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getPositionTitle() {
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle) {
		this.positionTitle = positionTitle;
	}

	public StaffProfile getStaffProfile() {
		return this.staffProfile;
	}

	public void setStaffProfile(StaffProfile staffProfile) {
		this.staffProfile = staffProfile;
	}

}