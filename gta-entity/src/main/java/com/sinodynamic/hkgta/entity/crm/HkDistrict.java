package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the hk_district database table.
 * 
 */
@Entity
@Table(name="hk_district")
@NamedQuery(name="HkDistrict.findAll", query="SELECT h FROM HkDistrict h")
public class HkDistrict implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, length=30)
	private String district;

	@Column(name="district_tc",length = 30)
	private String districtTc;
	
	@ManyToOne
	@JoinColumn(name="area_code", nullable=false)
	private HkArea hkArea;

	public HkDistrict() {
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getDistrictTc() {
		return districtTc;
	}

	public void setDistrictTc(String districtTc) {
		this.districtTc = districtTc;
	}

	public HkArea getHkArea() {
		return hkArea;
	}

	public void setHkArea(HkArea hkArea) {
		this.hkArea = hkArea;
	}

}