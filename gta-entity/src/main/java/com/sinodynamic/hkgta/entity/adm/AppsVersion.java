package com.sinodynamic.hkgta.entity.adm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import javassist.tools.framedump;

@Entity
@Table(name = "apps_version")
public class AppsVersion implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="app_id", unique=true, nullable=false, length=20)
	private String appId;

	@Column(name = "app_name", length = 100, nullable = false)
	private String appName;

	@Column(name = "latest_version", length = 20)
	private String latestVersion;

	@Column(name = "version_info", length = 500)
	private String versionInfo;

	@Column(name = "download_link", length = 500)
	private String downloadLink;

	@Column(name = "create_date", length = 20)
	private Date createDate;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "update_date", length = 20)
	private Date updateDate;

	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Column(name = "ver_no", length = 20)
	private String verNo;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getLatestVersion() {
		return latestVersion;
	}

	public void setLatestVersion(String latestVersion) {
		this.latestVersion = latestVersion;
	}

	public String getVersionInfo() {
		return versionInfo;
	}

	public void setVersionInfo(String versionInfo) {
		this.versionInfo = versionInfo;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getVerNo() {
		return verNo;
	}

	public void setVerNo(String verNo) {
		this.verNo = verNo;
	}

	

}
