package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the service_plan_facility database table.
 * 
 */
@Embeddable
public class ServicePlanFacilityPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="service_plan_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Long servicePlanId;

	@Column(name="facility_type_code", insertable=false, updatable=false, unique=true, nullable=false, length=10)
	private String facilityTypeCode;

	public ServicePlanFacilityPK(Long servicePlanId, String facilityTypeCode) {
		this.servicePlanId = servicePlanId;
		this.facilityTypeCode = facilityTypeCode;
	}
	public ServicePlanFacilityPK() {
	}
	
	public Long getServicePlanId() {
		return servicePlanId;
	}
	public void setServicePlanId(Long servicePlanId) {
		this.servicePlanId = servicePlanId;
	}
	public String getFacilityTypeCode() {
		return this.facilityTypeCode;
	}
	public void setFacilityTypeCode(String facilityTypeCode) {
		this.facilityTypeCode = facilityTypeCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ServicePlanFacilityPK)) {
			return false;
		}
		ServicePlanFacilityPK castOther = (ServicePlanFacilityPK)other;
		return 
			(this.servicePlanId.equals(castOther.servicePlanId))
			&& this.facilityTypeCode.equals(castOther.facilityTypeCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = (int) (hash * prime + this.servicePlanId.hashCode());
		hash = hash * prime + this.facilityTypeCode.hashCode();
		
		return hash;
	}
}