package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the department_branch database table.
 * 
 */
@Entity
@Table(name="department_branch")
@NamedQuery(name="DepartmentBranch.findAll", query="SELECT d FROM DepartmentBranch d")
public class DepartmentBranch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="depart_id", unique=true, nullable=false, length=11)
	private Long id;

	@Column(name="name", length=200)
	private String name;
	
	@Column(name="name_nls", length=200)
	private String nameNls;
	
	@Column(name="branch_type", length=10)
	private String branchType;
	
	@Column(name="status", length=10)
	private String status;
	
	@Column(name="parent_depart_id", length=11)
	private Long parentDepartId;
	
	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="create_by", length=50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	private Date createDate;

	public DepartmentBranch() {
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getNameNls()
	{
		return nameNls;
	}

	public void setNameNls(String nameNls)
	{
		this.nameNls = nameNls;
	}

	public String getBranchType()
	{
		return branchType;
	}

	public void setBranchType(String branchType)
	{
		this.branchType = branchType;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Long getParentDepartId()
	{
		return parentDepartId;
	}

	public void setParentDepartId(Long parentDepartId)
	{
		this.parentDepartId = parentDepartId;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

}