package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the user_activate_quest database table.
 * 
 */
@Entity
@Table(name="user_activate_quest")
@NamedQuery(name="UserActivateQuest.findAll", query="SELECT u FROM UserActivateQuest u")
public class UserActivateQuest implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserActivateQuestPK id;

	@Column(length=200)
	private String answer;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(length=250)
	private String question;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	public UserActivateQuest() {
	}

	public UserActivateQuestPK getId() {
		return this.id;
	}

	public void setId(UserActivateQuestPK id) {
		this.id = id;
	}

	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public void initUserActivateQuest(String userId, Long questionNo, String question, String answer, boolean isNewQuestion)
	{
		if (isNewQuestion)
		{
			UserActivateQuestPK id = new UserActivateQuestPK();
			id.setUserId(userId);
			id.setQuestionNo(questionNo);
			
			this.setId(id);
			Date date = new Date();
			Timestamp ts = new Timestamp(date.getTime());
			this.setCreateDate(ts);
			this.setCreateBy(userId);
		}
		
		this.setQuestion(question);
		this.setAnswer(answer);
		this.setUpdateBy(userId);
		this.setUpdateDate(new Date());
	}
}