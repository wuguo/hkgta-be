package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.sinodynamic.hkgta.entity.crm.StaffProfile;


/**
 * The persistent class for the room_crew database table.
 * 
 */
@Entity
@Table(name="room_crew")
@NamedQuery(name="RoomCrew.findAll", query="SELECT p FROM RoomCrew p")
public class RoomCrew implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "crew_id", unique = true, nullable = false)
	private Long crewId;
	
	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "staff_user_id")
	private StaffProfile staffProfile;

	@Column(name = "crew_role_id")
	private String crewRoleId;

	@Column(name = "create_by", length = 50)
	private String createBy;

	@Column(name = "create_date")
	private Timestamp createDate;
	
	@Column(name = "update_by", length = 50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "room_id")
	private Room room;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public RoomCrew() {
	}


	public Long getCrewId() {
		return crewId;
	}

	public void setCrewId(Long crewId) {
		this.crewId = crewId;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}


	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public String getCrewRoleId() {
		return crewRoleId;
	}


	public void setCrewRoleId(String crewRoleId) {
		this.crewRoleId = crewRoleId;
	}


	public StaffProfile getStaffProfile() {
		return staffProfile;
	}


	public void setStaffProfile(StaffProfile staffProfile) {
		this.staffProfile = staffProfile;
	}


	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

}