package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user_confirm_reserved_facility")
@NamedQuery(name = "UserConfirmReservedFacility.findAll", query = "SELECT p FROM UserConfirmReservedFacility p")
public class UserConfirmReservedFacility implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "user_id")
	private String userId;

	@Column(name = "service_type")
	private String serviceType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "confirm_timestamp")
	private Date confirmTimestamp;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Date getConfirmTimestamp() {
		return confirmTimestamp;
	}

	public void setConfirmTimestamp(Date confirmTimestamp) {
		this.confirmTimestamp = confirmTimestamp;
	}

}