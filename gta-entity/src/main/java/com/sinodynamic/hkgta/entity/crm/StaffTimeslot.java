package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;


/**
 * The persistent class for the staff_timeslot database table.
 * 
 */
@Entity
@Table(name="staff_timeslot")
@NamedQuery(name="StaffTimeslot.findAll", query="SELECT s FROM StaffTimeslot s")
public class StaffTimeslot implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="staff_timeslot_id", unique=true, nullable=false)
	private String staffTimeslotId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="begin_datetime", nullable=false)
	private Date beginDatetime;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;
	
	@Column(name="duty_category")
	private String dutyCategory;

	@Column(name="duty_description", length=250)
	private String dutyDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_datetime")
	private Date endDatetime;

	@Column(name="staff_user_id", nullable=false, length=50)
	private String staffUserId;

	@Column(length=10)
	private String status;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinTable(
            name="member_reserved_staff",
            joinColumns = @JoinColumn( name="staff_timeslot_id"),
            inverseJoinColumns = @JoinColumn( name="resv_id")
     )
	private MemberFacilityTypeBooking memberFacilityTypeBooking;
	public MemberFacilityTypeBooking getMemberFacilityTypeBooking() {
		return memberFacilityTypeBooking;
	}
	public void setMemberFacilityTypeBooking(
			MemberFacilityTypeBooking memberFacilityTypeBooking) {
		this.memberFacilityTypeBooking = memberFacilityTypeBooking;
	}
	public StaffTimeslot() {
	}

	public Long getVerNo() {
		return verNo;
	}
	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}
	public String getStaffTimeslotId() {
		return this.staffTimeslotId;
	}

	public void setStaffTimeslotId(String staffTimeslotId) {
		this.staffTimeslotId = staffTimeslotId;
	}

	public Date getBeginDatetime() {
		return this.beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getDutyDescription() {
		return this.dutyDescription;
	}

	public void setDutyDescription(String dutyDescription) {
		this.dutyDescription = dutyDescription;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getStaffUserId() {
		return this.staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDutyCategory() {
	    return dutyCategory;
	}

	public void setDutyCategory(String dutyCategory) {
	    this.dutyCategory = dutyCategory;
	}
	
}