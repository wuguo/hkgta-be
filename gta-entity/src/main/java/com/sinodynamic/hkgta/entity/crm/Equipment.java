package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.sinodynamic.hkgta.entity.fms.FacilityEquipment;


/**
 * The persistent class for the equipment database table.
 * 
 */
@Entity
@Table(name="equipment")
@NamedQuery(name="Equipment.findAll", query="SELECT e FROM Equipment e")
public class Equipment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="equip_id", unique=true, nullable=false)
	private String equipId;

	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(name="equip_description", length=250)
	private String equipDescription;

	@Column(name="equip_name", length=100)
	private String equipName;

	@Column(name="serial_no", length=50)
	private String serialNo;

	@Column(length=10)
	private String status;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Version
	@Column(name="ver_no", nullable = false)
	private Long verNo;
	
	//bi-directional many-to-one association to FacilityEquipment
	@OneToMany(mappedBy="equipmentBean")
	private List<FacilityEquipment> facilityEquipments;

	public Equipment() {
	}

	
	public Long getVerNo() {
		return verNo;
	}


	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}


	public String getEquipId() {
		return this.equipId;
	}

	public void setEquipId(String equipId) {
		this.equipId = equipId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getEquipDescription() {
		return this.equipDescription;
	}

	public void setEquipDescription(String equipDescription) {
		this.equipDescription = equipDescription;
	}

	public String getEquipName() {
		return this.equipName;
	}

	public void setEquipName(String equipName) {
		this.equipName = equipName;
	}

	public String getSerialNo() {
		return this.serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<FacilityEquipment> getFacilityEquipments() {
		return this.facilityEquipments;
	}

	public void setFacilityEquipments(List<FacilityEquipment> facilityEquipments) {
		this.facilityEquipments = facilityEquipments;
	}

	public FacilityEquipment addFacilityEquipment(FacilityEquipment facilityEquipment) {
		getFacilityEquipments().add(facilityEquipment);
		facilityEquipment.setEquipmentBean(this);

		return facilityEquipment;
	}

	public FacilityEquipment removeFacilityEquipment(FacilityEquipment facilityEquipment) {
		getFacilityEquipments().remove(facilityEquipment);
		facilityEquipment.setEquipmentBean(null);

		return facilityEquipment;
	}

}