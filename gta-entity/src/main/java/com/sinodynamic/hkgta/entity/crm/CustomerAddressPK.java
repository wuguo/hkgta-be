package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

/**
 * The primary key class for the customer_address database table.
 * 
 */
@Embeddable
public class CustomerAddressPK  extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="customer_id", insertable=false, updatable=false, unique=true, nullable=false)
	@EncryptFieldInfo 
	private Long customerId;

	@Column(name="address_type", unique=true, nullable=false, length=10)
	private String addressType;

	public CustomerAddressPK() {
	}
	public Long getCustomerId() {
		return this.customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getAddressType() {
		return this.addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CustomerAddressPK)) {
			return false;
		}
		CustomerAddressPK castOther = (CustomerAddressPK)other;
		return 
			this.customerId.equals(castOther.customerId)
			&& this.addressType.equals(castOther.addressType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.customerId.hashCode();
		hash = hash * prime + this.addressType.hashCode();
		
		return hash;
	}

}