package com.sinodynamic.hkgta.entity.mms;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spa_member_sync")
public class SpaMemberSync implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sys_id", unique = true, nullable = false)
    private Long sysId;
    
    @Column(name = "customer_id")
    private Long customerId;
    
    @Column(name = "action")
    private String action;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "retry_count")
    private Integer retryCount;
    
    @Column(name = "last_response_msg")
    private String lastResponseMsg;
    
    @Column(name = "spa_guest_id")
    private String spaGuestId;
    
    @Column(name = "create_date")
    private Date createDate;
    
    @Column(name = "update_date")
    private Date updateDate;

    public Long getSysId() {
        return sysId;
    }

    public void setSysId(Long sysId) {
        this.sysId = sysId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public String getLastResponseMsg() {
        return lastResponseMsg;
    }

    public void setLastResponseMsg(String lastResponseMsg) {
        this.lastResponseMsg = lastResponseMsg;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getSpaGuestId() {
        return spaGuestId;
    }

    public void setSpaGuestId(String spaGuestId) {
        this.spaGuestId = spaGuestId;
    }
    
}
