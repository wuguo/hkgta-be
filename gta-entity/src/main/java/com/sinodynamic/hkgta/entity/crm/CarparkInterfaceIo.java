package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "carpark_interface_io")
@NamedQuery(name = "CarparkInterfaceIo.findAll", query = "SELECT c FROM CarparkInterfaceIo c")
public class CarparkInterfaceIo implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	private Long sysId;

	@Column(name = "import_id")
	private Long importId;

	@Column(name = "customer_id")
	private Long customerId;

	@Column(name = "card_type", nullable = false, length = 2)
	private String cardType;

	@Column(name = "import_card_id", length = 10)
	private String importCardId;

	@Column(name = "in_time")
	private Date inTime;

	@Column(name = "out_time")
	private Date outTime;

	@Column(name = "vehicle_plate_no", length = 50)
	private String vehiclePlateNo;

	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "status", length = 3)
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}

	public Long getImportId() {
		return importId;
	}

	public void setImportId(Long importId) {
		this.importId = importId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getImportCardId() {
		return importCardId;
	}

	public void setImportCardId(String importCardId) {
		this.importCardId = importCardId;
	}

	public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	public String getVehiclePlateNo() {
		return vehiclePlateNo;
	}

	public void setVehiclePlateNo(String vehiclePlateNo) {
		this.vehiclePlateNo = vehiclePlateNo;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}