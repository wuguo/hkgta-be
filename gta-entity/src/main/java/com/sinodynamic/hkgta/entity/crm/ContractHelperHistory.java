package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the contract_helper database table.
 * 
 * @author Vineela_Jyothi
 */
@Entity
@Table(name = "contract_helper_history")
@NamedQuery(name = "ContractHelperHistory.findAll", query = "SELECT c FROM ContractHelperHistory c")
public class ContractHelperHistory implements Serializable {
	private static final long serialVersionUID = 1L;
//	sys_id, contractor_id, helper_pass_type_id, permit_card_id, period_from, period_to, remark, create_date

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sys_id", unique = true, nullable = false)
	private Long sysId;
	
	@Column(name = "contractor_id", unique = true, nullable = false)
	private Long contractorId;

	@Column(name = "helper_pass_type_id")
	private Long helperPassType;
	
	@Column(name = "permit_card_id")
	private Long permitCardId;
	
	@Column(name = "period_from")
	private Date periodFrom;
	
	@Column(name = "period_to")
	private Date periodTo;
	
	@Column(name = "pass_type_assign_by")
	private String passTypeAssignBy;
	
	@Column(name = "pass_type_assign_date")
	private Date passTypeAssignDate;

	@Column(name = "remark")
	private String remark;
	
	@Column(name = "status")
	private String status;
	
	public Long getSysId()
	{
		return sysId;
	}

	public void setSysId(Long sysId)
	{
		this.sysId = sysId;
	}

	public Long getContractorId()
	{
		return contractorId;
	}

	public void setContractorId(Long contractorId)
	{
		this.contractorId = contractorId;
	}

	public Long getHelperPassType()
	{
		return helperPassType;
	}

	public void setHelperPassType(Long helperPassType)
	{
		this.helperPassType = helperPassType;
	}

	public Long getPermitCardId()
	{
		return permitCardId;
	}

	public void setPermitCardId(Long permitCardId)
	{
		this.permitCardId = permitCardId;
	}

	public Date getPeriodFrom()
	{
		return periodFrom;
	}

	public void setPeriodFrom(Date periodFrom)
	{
		this.periodFrom = periodFrom;
	}

	public Date getPeriodTo()
	{
		return periodTo;
	}

	public void setPeriodTo(Date periodTo)
	{
		this.periodTo = periodTo;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	@Column(name = "create_date")
	private Date createDate;

	public String getPassTypeAssignBy() {
	    return passTypeAssignBy;
	}

	public void setPassTypeAssignBy(String passTypeAssignBy) {
	    this.passTypeAssignBy = passTypeAssignBy;
	}

	public Date getPassTypeAssignDate() {
	    return passTypeAssignDate;
	}

	public void setPassTypeAssignDate(Date passTypeAssignDate) {
	    this.passTypeAssignDate = passTypeAssignDate;
	}

	public String getStatus() {
	    return status;
	}

	public void setStatus(String status) {
	    this.status = status;
	}
	
}
