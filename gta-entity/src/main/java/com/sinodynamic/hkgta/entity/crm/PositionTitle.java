package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * The persistent class for the position_title database table.
 * 
 */
@Entity
@Table(name="position_title")
@NamedQuery(name="PositionTitle.findAll", query="SELECT t FROM PositionTitle t")
public class PositionTitle implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="position_code", unique=true, nullable=false, length=20)
	private String positionCode;

	@Column(name="position_name", length=200)
	private String positionName;

	@Column(name="position_name_nls", length=200)
	private String positionNameNls;

	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="create_by", length=50)
	private String createBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="depart_id", length=15)
	private Long departId;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public PositionTitle() {
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public String getPositionCode()
	{
		return positionCode;
	}

	public void setPositionCode(String positionCode)
	{
		this.positionCode = positionCode;
	}

	public String getPositionName()
	{
		return positionName;
	}

	public void setPositionName(String positionName)
	{
		this.positionName = positionName;
	}

	public String getPositionNameNls()
	{
		return positionNameNls;
	}

	public void setPositionNameNls(String positionNameNls)
	{
		this.positionNameNls = positionNameNls;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public Long getDepartId()
	{
		return departId;
	}

	public void setDepartId(Long departId)
	{
		this.departId = departId;
	}

}