package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

/**
 * The persistent class for the site_poi database table.
 * 
 */
@Entity
@Table(name="staff_site_poi_monitor")
@NamedQuery(name="StaffSitePoiMonitor.findAll", query="SELECT s FROM StaffSitePoiMonitor s")
public class StaffSitePoiMonitor implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sys_id")
	private Long sysId;

	@Column(name="staff_user_id")
	@ForeignKey(name = "null")
	private String staffUserId;
	
	@Column(name="site_poi_id")
	private Long sitePoiId;

	@Column(name="create_date")
	private Date createDate;

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Object sysId) {
		if (sysId != null){
			this.sysId = Long.valueOf(sysId.toString());
		}
	}

	public String getStaffUserId() {
		return staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}

	public Long getSitePoiId() {
		return sitePoiId;
	}

	public void setSitePoiId(Long sitePoiId) {
		this.sitePoiId = sitePoiId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}