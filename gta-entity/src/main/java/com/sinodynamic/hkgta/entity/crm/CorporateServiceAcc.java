package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the corporate_service_acc database table.
 * 
 */
@Entity
@Table(name="corporate_service_acc")
@NamedQuery(name="CorporateServiceAcc.findAll", query="SELECT c FROM CorporateServiceAcc c")
public class CorporateServiceAcc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="acc_no")
	private Long accNo;

	@Column(name="acc_cat")
	private String accCat;

	@Column(name="contract_ref_no")
	private String contractRefNo;

	@Temporal(TemporalType.DATE)
	@Column(name="effective_date")
	private Date effectiveDate;

	@Temporal(TemporalType.DATE)
	@Column(name="expiry_date")
	private Date expiryDate;

	@Column(name="follow_by_staff")
	private String followByStaff;

	@Column(name="period_code")
	private String periodCode;

	@Column(name="remark")
	private String remark;

	@Column(name="reset_credit_period")
	private String resetCreditPeriod;

	@Column(name="settlement_method_code")
	private String settlementMethodCode;

	@Column(name="statement_delivery_by")
	private String statementDeliveryBy;

	@Column(name="status")
	private String status;

	@Column(name="total_credit_limit")
	private BigDecimal totalCreditLimit;
	
	@Column(name="corporate_id")
	private Long corporateId;
	
	@Version
	@Column(name="ver_no", nullable = false)
	private Long version;

	@Transient
	private CorporateProfile corporateProfile;

	@Transient
	private List<CorporateServiceSubscribe> corporateServiceSubscribes;

	public CorporateServiceAcc() {
	}

	public Long getAccNo() {
		return this.accNo;
	}

	public void setAccNo(Long accNo) {
		this.accNo = accNo;
	}

	public String getAccCat() {
		return this.accCat;
	}

	public void setAccCat(String accCat) {
		this.accCat = accCat;
	}

	public String getContractRefNo() {
		return this.contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getFollowByStaff() {
		return this.followByStaff;
	}

	public void setFollowByStaff(String followByStaff) {
		this.followByStaff = followByStaff;
	}

	public String getPeriodCode() {
		return this.periodCode;
	}

	public void setPeriodCode(String periodCode) {
		this.periodCode = periodCode;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getResetCreditPeriod() {
		return this.resetCreditPeriod;
	}

	public void setResetCreditPeriod(String resetCreditPeriod) {
		this.resetCreditPeriod = resetCreditPeriod;
	}

	public String getSettlementMethodCode() {
		return this.settlementMethodCode;
	}

	public void setSettlementMethodCode(String settlementMethodCode) {
		this.settlementMethodCode = settlementMethodCode;
	}

	public String getStatementDeliveryBy() {
		return this.statementDeliveryBy;
	}

	public void setStatementDeliveryBy(String statementDeliveryBy) {
		this.statementDeliveryBy = statementDeliveryBy;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getTotalCreditLimit() {
		return this.totalCreditLimit;
	}

	public void setTotalCreditLimit(BigDecimal totalCreditLimit) {
		this.totalCreditLimit = totalCreditLimit;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public CorporateProfile getCorporateProfile() {
		return this.corporateProfile;
	}

	public void setCorporateProfile(CorporateProfile corporateProfile) {
		this.corporateProfile = corporateProfile;
	}

	public List<CorporateServiceSubscribe> getCorporateServiceSubscribes() {
		return this.corporateServiceSubscribes;
	}

	public void setCorporateServiceSubscribes(List<CorporateServiceSubscribe> corporateServiceSubscribes) {
		this.corporateServiceSubscribes = corporateServiceSubscribes;
	}

	public Long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Object corporateId) {
		this.corporateId =(corporateId!=null ? NumberUtils.toLong(corporateId.toString()):null);
	}

}