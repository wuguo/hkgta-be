package com.sinodynamic.hkgta.entity.ibeacon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.math.NumberUtils;


/**
 * The persistent class for the site_poi database table.
 * 
 */
@Entity
@Table(name="site_poi")
@NamedQuery(name="SitePoi.findAll", query="SELECT s FROM SitePoi s")
public class SitePoi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="poi_id")
	private Long poiId;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_date")
	private Date createDate;

	@Column(name="description")
	private String description;

	@Column(name="update_by")
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Column(name="venue_code")
	private String venueCode;
	
	@Column(name="target_type_code")
	private String targetTypeCode;

	public SitePoi() {
	}

	public Long getPoiId() {
		return this.poiId;
	}

	public void setPoiId(Object poiId) {
		this.poiId = (poiId!=null ? NumberUtils.toLong(poiId.toString()):null);
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getVenueCode() {
		return this.venueCode;
	}

	public void setVenueCode(String venueCode) {
		this.venueCode = venueCode;
	}

	public String getTargetTypeCode() {
		return targetTypeCode;
	}

	public void setTargetTypeCode(String targetTypeCode) {
		this.targetTypeCode = targetTypeCode;
	}
	

}