package com.sinodynamic.hkgta.entity.onlinepayment;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the payment_gateway database table.
 * 
 */
@Entity
@Table(name="payment_gateway")
@NamedQuery(name="PaymentGateway.findAll", query="SELECT p FROM PaymentGateway p")
public class PaymentGateway implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="gateway_id")
	private Long gatewayId;

	@Column(name="access_code")
	private String accessCode;

	@Column(name="gateway_url")
	private String gatewayUrl;

	@Column(name="merchant_id")
	private String merchantId;

	private String provider;

	@Column(name="secure_secret")
	private String secureSecret;

	@Column(name="timeout_min")
	private Long timeoutMin;
	
	@Column(name="operator_id")
//	@Transient
	private String operatorId;
	
	@Column(name="operator_password")
//	@Transient
	private String operatorPassword;
	
	@Column(name="settlement_cutoff_time")
//	@Transient
	private Long settlementCutoffTime;

	//bi-directional many-to-one association to PaymentGatewayCmd
	@OneToMany(mappedBy="paymentGateway")
	private List<PaymentGatewayCmd> paymentGatewayCmds;

	public PaymentGateway() {
	}

	public Long getGatewayId() {
		return this.gatewayId;
	}

	public void setGatewayId(Long gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getAccessCode() {
		return this.accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getGatewayUrl() {
		return this.gatewayUrl;
	}

	public void setGatewayUrl(String gatewayUrl) {
		this.gatewayUrl = gatewayUrl;
	}

	public String getMerchantId() {
		return this.merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getProvider() {
		return this.provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getSecureSecret() {
		return this.secureSecret;
	}

	public void setSecureSecret(String secureSecret) {
		this.secureSecret = secureSecret;
	}

	public Long getTimeoutMin() {
		return this.timeoutMin;
	}

	public void setTimeoutMin(Long timeoutMin) {
		this.timeoutMin = timeoutMin;
	}

	public List<PaymentGatewayCmd> getPaymentGatewayCmds() {
		return this.paymentGatewayCmds;
	}

	public void setPaymentGatewayCmds(List<PaymentGatewayCmd> paymentGatewayCmds) {
		this.paymentGatewayCmds = paymentGatewayCmds;
	}

	public PaymentGatewayCmd addPaymentGatewayCmd(PaymentGatewayCmd paymentGatewayCmd) {
		getPaymentGatewayCmds().add(paymentGatewayCmd);
		paymentGatewayCmd.setPaymentGateway(this);

		return paymentGatewayCmd;
	}

	public PaymentGatewayCmd removePaymentGatewayCmd(PaymentGatewayCmd paymentGatewayCmd) {
		getPaymentGatewayCmds().remove(paymentGatewayCmd);
		paymentGatewayCmd.setPaymentGateway(null);

		return paymentGatewayCmd;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorPassword() {
		return operatorPassword;
	}

	public void setOperatorPassword(String operatorPassword) {
		this.operatorPassword = operatorPassword;
	}

	public Long getSettlementCutoffTime() {
		return settlementCutoffTime;
	}

	public void setSettlementCutoffTime(Long settlementCutoffTime) {
		this.settlementCutoffTime = settlementCutoffTime;
	}

}