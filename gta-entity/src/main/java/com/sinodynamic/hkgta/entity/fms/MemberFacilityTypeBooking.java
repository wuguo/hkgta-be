package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.sinodynamic.hkgta.entity.crm.StaffTimeslot;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;


/**
 * The persistent class for the member_facility_type_booking database table.
 * 
 */
@Entity
@Table(name="member_facility_type_booking")
@NamedQuery(name="MemberFacilityTypeBooking.findAll", query="SELECT f FROM MemberFacilityTypeBooking f")
public class MemberFacilityTypeBooking implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="resv_id", unique=true, nullable=false, length=20)
	private Long resvId;
	
	@Column(name="customer_id",nullable=false,length=20)
	private Long customerId;
	
	@Column(name="resv_facility_type",length=10)
	private String resvFacilityType;
	
	@Column(name="facility_subtype_id",length=20)
	private String facilitySubtypeId;
	
	@Column(name="facility_type_qty",length=20)
	private Long facilityTypeQty;
	@Column(name="same_facility")
	private String sameFacility;
	@Column(name="begin_datetime_book")
	private Timestamp beginDatetimeBook;
	
	@Column(name="end_datetime_book")
	private Timestamp endDatetimeBook;
	
	@Column(name="status",length=50)
	private String status;
	
	@Column(name="order_no")
	private Long orderNo;
	
	@Column(name="exp_coach_user_id",length=50)
	private String expCoachUserId;
	
	@Column(name="reserve_via",length=10)
	private String reserveVia;
	@Column(name="customer_remark",length=300)
	private String customerRemark;
	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Timestamp createDate;
	
	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;	
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER,orphanRemoval=true)
	@JoinColumn(name="resv_id" , nullable=true, insertable=false, updatable=false)
	private List<MemberReservedFacility> memberReservedFacilitys;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="order_no" , nullable=true, insertable=false, updatable=false)
	private CustomerOrderHd customerOrderHd;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="memberFacilityTypeBooking")
	private List<FacilityTimeslot> facilityTimeslots;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="memberFacilityTypeBooking")
	private List<StaffTimeslot> StaffTimeslots;
	
	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	public String getCustomerRemark() {
		return customerRemark;
	}

	public void setCustomerRemark(String customerRemark) {
		this.customerRemark = customerRemark;
	}
	
	public List<StaffTimeslot> getStaffTimeslots() {
		return StaffTimeslots;
	}

	public void setStaffTimeslots(List<StaffTimeslot> staffTimeslots) {
		StaffTimeslots = staffTimeslots;
	}

	public String getSameFacility() {
		return sameFacility;
	}

	public void setSameFacility(String sameFacility) {
		this.sameFacility = sameFacility;
	}

	public MemberFacilityTypeBooking() {
	}

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Long resvId) {
		this.resvId = resvId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getResvFacilityType() {
		return resvFacilityType;
	}

	public void setResvFacilityType(String resvFacilityType) {
		this.resvFacilityType = resvFacilityType;
	}

	public Long getFacilityTypeQty() {
		return facilityTypeQty;
	}

	public void setFacilityTypeQty(Long facilityTypeQty) {
		this.facilityTypeQty = facilityTypeQty;
	}

	public Timestamp getBeginDatetimeBook() {
		return beginDatetimeBook;
	}

	public void setBeginDatetimeBook(Timestamp beginDatetimeBook) {
		this.beginDatetimeBook = beginDatetimeBook;
	}

	public Timestamp getEndDatetimeBook() {
		return endDatetimeBook;
	}

	public void setEndDatetimeBook(Timestamp endDatetimeBook) {
		this.endDatetimeBook = endDatetimeBook;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExpCoachUserId() {
		return expCoachUserId;
	}

	public void setExpCoachUserId(String expCoachUserId) {
		this.expCoachUserId = expCoachUserId;
	}

	public String getReserveVia() {
		return reserveVia;
	}

	public void setReserveVia(String reserveVia) {
		this.reserveVia = reserveVia;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<MemberReservedFacility> getMemberReservedFacilitys() {
		return memberReservedFacilitys;
	}

	public void setMemberReservedFacilitys(
			List<MemberReservedFacility> memberReservedFacilitys) {
		this.memberReservedFacilitys = memberReservedFacilitys;
	}

	public List<FacilityTimeslot> getFacilityTimeslots() {
		return facilityTimeslots;
	}

	public void setFacilityTimeslots(List<FacilityTimeslot> facilityTimeslots) {
		this.facilityTimeslots = facilityTimeslots;
	}

	public Long getOrderNo()
	{
		return orderNo;
	}

	public void setOrderNo(Long orderNo)
	{
		this.orderNo = orderNo;
	}

	public CustomerOrderHd getCustomerOrderHd()
	{
		return customerOrderHd;
	}

	public void setCustomerOrderHd(CustomerOrderHd customerOrderHd)
	{
		this.customerOrderHd = customerOrderHd;
	}
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public String getFacilitySubtypeId()
	{
		return facilitySubtypeId;
	}

	public void setFacilitySubtypeId(String facilitySubtypeId)
	{
		this.facilitySubtypeId = facilitySubtypeId;
	}
}