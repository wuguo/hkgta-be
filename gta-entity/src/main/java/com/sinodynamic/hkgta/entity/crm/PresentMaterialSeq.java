package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the present_material_seq database table.
 * 
 */
@Entity
@Table(name="present_material_seq")
@NamedQuery(name="PresentMaterialSeq.findAll", query="SELECT p FROM PresentMaterialSeq p")
public class PresentMaterialSeq implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PresentMaterialSeqPK id;

	@Column(name="present_seq", nullable=false)
	private Long presentSeq;

	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="present_id", nullable=false, insertable=false, updatable=false)
	private PresentationBatch presentationBatch;
	
	

	public PresentMaterialSeq() {
	}

	public PresentMaterialSeqPK getId() {
		return this.id;
	}

	public void setId(PresentMaterialSeqPK id) {
		this.id = id;
	}

	public Long getPresentSeq() {
		return this.presentSeq;
	}

	public void setPresentSeq(Long presentSeq) {
		this.presentSeq = presentSeq;
	}

	public PresentationBatch getPresentationBatch() {
		return this.presentationBatch;
	}

	public void setPresentationBatch(PresentationBatch presentationBatch) {
		this.presentationBatch = presentationBatch;
	}


	
	

}