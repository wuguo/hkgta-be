package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The persistent class for the room_type database table.
 * 
 */
@Entity
@Table(name="room_type")
@NamedQuery(name="RoomType.findAll", query="SELECT p FROM RoomType p")
public class RoomType implements Serializable {
	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name = "type_code", unique = true, nullable = false, length = 10)
	private String typeCode;

	@Column(name="type_name", length=200)
	private String typeName;
	
	@Column(name="num_of_guest_txt", length=100)
	private String numOfGuest;
	
	@Column(name="amenity", length=1000)
	private String amenity;
	
	@Column(name="description", length=1000)
	private String description;
	
	@Column(name="adult_capacity", length=2)
	private Long adultCapacity;
	
	@Column(name="child_capacity", length=2)
	private Long childCapacity;
	
	@Column(name="single_bed_qty", length=1)
	private Long singleBedQty;
	
	@Column(name="double_bed_qty", length=1)
	private Long doubleBedQty;
	
	@Column(name="create_by", length=50)
	private String createBy;

	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="update_by", length=50)
	private String updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;
	
	@Column(name="status", length=10)
	private String status;

	@Version
	@Column(name="ver_no", nullable=false)
	private Long verNo;
	
	
	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		this.verNo = verNo;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTypeName()
	{
		return typeName;
	}

	public void setTypeName(String typeName)
	{
		this.typeName = typeName;
	}

	public String getNumOfGuest()
	{
		return numOfGuest;
	}

	public void setNumOfGuest(String numOfGuest)
	{
		this.numOfGuest = numOfGuest;
	}

	public String getAmenity()
	{
		return amenity;
	}

	public void setAmenity(String amenity)
	{
		this.amenity = amenity;
	}

	public Long getAdultCapacity()
	{
		return adultCapacity;
	}

	public void setAdultCapacity(Long adultCapacity)
	{
		this.adultCapacity = adultCapacity;
	}

	public Long getChildCapacity()
	{
		return childCapacity;
	}

	public void setChildCapacity(Long childCapacity)
	{
		this.childCapacity = childCapacity;
	}

	public Long getSingleBedQty()
	{
		return singleBedQty;
	}

	public void setSingleBedQty(Long singleBedQty)
	{
		this.singleBedQty = singleBedQty;
	}

	public Long getDoubleBedQty()
	{
		return doubleBedQty;
	}

	public void setDoubleBedQty(Long doubleBedQty)
	{
		this.doubleBedQty = doubleBedQty;
	}
	
	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public RoomType()
	{}
	
	
}