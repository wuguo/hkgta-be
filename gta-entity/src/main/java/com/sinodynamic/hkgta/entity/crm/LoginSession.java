package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the login_session database table.
 * 
 */
@Entity
@Table(name="login_session")
@NamedQuery(name="LoginSession.findAll", query="SELECT l FROM LoginSession l")
public class LoginSession implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private LoginSessionPK id;

	public LoginSessionPK getId() {
		return id;
	}

	public void setId(LoginSessionPK id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_access_time")
	private Date lastAccessTime;

	@Column(name="session_token", length=128)
	private String sessionToken;
	
	@Column(name="app_type_code", length=10)
	private String appTypeCode;

	public Date getLastAccessTime()
	{
		return lastAccessTime;
	}

	public void setLastAccessTime(Date lastAccessTime)
	{
		this.lastAccessTime = lastAccessTime;
	}

	public String getSessionToken()
	{
		return sessionToken;
	}

	public void setSessionToken(String sessionToken)
	{
		this.sessionToken = sessionToken;
	}

	public String getAppTypeCode() {
		return appTypeCode;
	}

	public void setAppTypeCode(String appTypeCode) {
		this.appTypeCode = appTypeCode;
	}

	
}