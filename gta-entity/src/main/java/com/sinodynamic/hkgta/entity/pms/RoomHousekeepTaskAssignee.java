package com.sinodynamic.hkgta.entity.pms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the room_housekeep_task_assignee database table.
 * 
 */
@Entity
@Table(name="room_housekeep_task_assignee")
@NamedQuery(name="RoomHousekeepTaskAssignee.findAll", query="SELECT p FROM RoomHousekeepTaskAssignee p")
public class RoomHousekeepTaskAssignee implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private RoomHousekeepTaskAssigneePK id;
	
	@Column(name="task_role",length=10)
	private String taskRole;

	public RoomHousekeepTaskAssignee() {
	}

	public String getTaskRole() {
		return taskRole;
	}

	public void setTaskRole(String taskRole) {
		this.taskRole = taskRole;
	}


	public RoomHousekeepTaskAssigneePK getId()
	{
		return id;
	}


	public void setId(RoomHousekeepTaskAssigneePK id)
	{
		this.id = id;
	}
	
	
}