package com.sinodynamic.hkgta.entity.crm;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name="v_customer_action_content")
public class CustomerActionContent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerActionContent() {
		// TODO Auto-generated constructor stub
	}
	
	@EmbeddedId
	private CustomerActionContentPK id;
	
	
	

	public CustomerActionContentPK getId() {
		return id;
	}

	public void setId(CustomerActionContentPK id) {
		this.id = id;
	}


	
	
	
	

}
