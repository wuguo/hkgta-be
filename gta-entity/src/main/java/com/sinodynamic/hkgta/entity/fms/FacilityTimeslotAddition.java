package com.sinodynamic.hkgta.entity.fms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the facility_timeslot_addition database table.
 * 
 */
@Entity
@Table(name="facility_timeslot_addition")
@NamedQuery(name="FacilityTimeslotAddition.findAll", query="SELECT f FROM FacilityTimeslotAddition f")
public class FacilityTimeslotAddition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "facility_timeslot_id", unique = true, nullable = false, length = 20)
	private Long facilityTimeslotId;
	
	@Column(name="ball_feeding_status", length=3)
	private String ballFeedingStatus;

	
	public FacilityTimeslotAddition() {
	}

	public String getBallFeedingStatus() {
		return ballFeedingStatus;
	}

	public void setBallFeedingStatus(String ballFeedingStatus) {
		this.ballFeedingStatus = ballFeedingStatus;
	}


	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}


	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}
}