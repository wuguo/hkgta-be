package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class PatronServicePlanDto implements Serializable {

	private String	customerID;

	private String	servicePlan;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getServicePlan() {
		return servicePlan;
	}

	public void setServicePlan(String servicePlan) {
		this.servicePlan = servicePlan;
	}

}
