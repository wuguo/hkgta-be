package com.sinodynamic.hkgta.dto.qst;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.entity.qst.Survey;

public class QuestionnaireSettingListDto implements Serializable {
	
	private List<Survey>  list;
	
	public QuestionnaireSettingListDto(){
		super();
	}

	public List<Survey> getList() {
		return list;
	}

	public void setList(List<Survey> list) {
		this.list = list;
	}
	
}
