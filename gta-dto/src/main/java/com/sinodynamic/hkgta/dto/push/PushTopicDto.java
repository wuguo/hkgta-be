package com.sinodynamic.hkgta.dto.push;

import java.io.Serializable;

public class PushTopicDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3042595851216544765L;
    private String topicArn;
    private String message;
	public String getTopicArn() {
		return topicArn;
	}
	public void setTopicArn(String topicArn) {
		this.topicArn = topicArn;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
    

}
