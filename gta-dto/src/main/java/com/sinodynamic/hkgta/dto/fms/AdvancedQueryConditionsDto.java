package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;


/**
 * @author Kevin_Liang
 *
 */
public class AdvancedQueryConditionsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7971315640737297629L;
	public AdvancedQueryConditionsDto(){}
	/**
	 * 解析不需要显示的状态【暂支持CAN状态，其它带扩展】
	 * @param notShowStatus
	 */
	public AdvancedQueryConditionsDto(String notShowStatus){
		if (!notShowStatus.equals("")) {
			String[] statusArr = notShowStatus.split(",");
			for (String status : statusArr) {
				switch (status) {
				case "CAN":
					this.isShowCancelled = false;
					break;

				default:
					break;
				}
			}
		} else {
			this.isShowCancelled = true;
		}
	}
	
	private boolean isShowCancelled = true;
	public boolean getIsShowCancelled() {
		return isShowCancelled;
	}
	public void setIsShowCancelled(boolean isShowCancelled) {
		this.isShowCancelled = isShowCancelled;
	}
	

}
