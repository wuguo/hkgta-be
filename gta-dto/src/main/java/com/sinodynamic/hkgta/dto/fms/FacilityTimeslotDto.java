package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class FacilityTimeslotDto implements Serializable {

	private static final long serialVersionUID = 430887760348794557L;

	private Long facilityNo;
	private String beginDatetime;
	private String status;

	public Long getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}

	public String getBeginDatetime() {
		return beginDatetime;
	}

	public void setBeginDatetime(String beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
