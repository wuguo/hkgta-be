package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;

public class CustomerOrderPermitCardDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long orderDetId;
	private Long customerOrderNo;
	private Long cardholderCustomerId;
	private String status;

	private String staffUserId;
	
	
	//for list query
	private Long pageNumber;
	private Long pageSize;
	
	
	
	public Long getOrderDetId() {
		return orderDetId;
	}
	public void setOrderDetId(Long orderDetId) {
		this.orderDetId = orderDetId;
	}
	public Long getCustomerOrderNo() {
		return customerOrderNo;
	}
	public void setCustomerOrderNo(Long customerOrderNo) {
		this.customerOrderNo = customerOrderNo;
	}
	public Long getCardholderCustomerId() {
		return cardholderCustomerId;
	}
	public void setCardholderCustomerId(Long cardholderCustomerId) {
		this.cardholderCustomerId = cardholderCustomerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
	
	public Long getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Long pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Long getPageSize() {
		return pageSize;
	}
	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}
	public String getStaffUserId() {
		return staffUserId;
	}
	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}
	public CustomerOrderPermitCardDto(Long orderDetId, Long customerOrderNo, Long cardholderCustomerId, String status, String staffUserId) {
		super();
		this.orderDetId = orderDetId;
		this.customerOrderNo = customerOrderNo;
		this.cardholderCustomerId = cardholderCustomerId;
		this.status = status;
		this.staffUserId = staffUserId;
	}
	public CustomerOrderPermitCardDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "CustomerOrderPermitCardDto [orderDetId=" + orderDetId + ", customerOrderNo=" + customerOrderNo + ", cardholderCustomerId=" + cardholderCustomerId + ", status=" + status + ", staffUserId=" + staffUserId + "]";
	}
	
}
