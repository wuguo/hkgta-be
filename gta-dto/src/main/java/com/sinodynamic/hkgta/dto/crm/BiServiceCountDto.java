package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class BiServiceCountDto implements Serializable {
	
	private String name;
	
	private String type;
	
	private BigDecimal primaryCount;
	
	private BigDecimal dependentCount;
	
	private BigDecimal total;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getPrimaryCount() {
		return primaryCount;
	}

	public void setPrimaryCount(Object primaryCount) {
		if(primaryCount instanceof BigInteger)
		{
			this.primaryCount = BigDecimal.valueOf(Double.valueOf(primaryCount.toString()));
		}else{
			this.primaryCount = (BigDecimal)primaryCount;
		}
	}

	public BigDecimal getDependentCount() {
		return dependentCount;
	}

	public void setDependentCount(Object dependentCount) {
		if(dependentCount instanceof BigInteger)
		{
			this.dependentCount = BigDecimal.valueOf(Double.valueOf(dependentCount.toString()));
		}else{
			this.dependentCount = (BigDecimal)dependentCount;
		}
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(Object total) {
		if(total instanceof BigInteger)
		{
			this.total = BigDecimal.valueOf(Double.valueOf(total.toString()));
		}else{
			this.total = (BigDecimal)total;
		}
	}
	
	
	

}
