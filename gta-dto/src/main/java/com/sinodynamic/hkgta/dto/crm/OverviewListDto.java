package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class OverviewListDto extends GenericDto  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public OverviewListDto() {
		super();
	}

	@EncryptFieldInfo
	private BigInteger customerId;
	
	private String academyNo;

	
	private String memberName;

	private String gender;
	
	private String greetingInfo;
	private String phoneMobile;

	private String internalRemark;
	
	private String passportType;
	private String passportNo;

	private Date dateOfBirth;
	
	private String portraitPhoto;
	private String email;

	private String academyMarketing;
	
	private String otherMarketing;
	private Date updateDate;
	private Date createDate;


	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigInteger getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = (BigInteger) (customerId !=null ? customerId : 0);
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGreetingInfo() {
		return greetingInfo;
	}

	public void setGreetingInfo(String greetingInfo) {
		this.greetingInfo = greetingInfo;
	}

	public String getPhoneMobile() {
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}

	public String getPassportType() {
		return passportType;
	}

	public void setPassportType(String passportType) {
		this.passportType = passportType;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPortraitPhoto() {
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAcademyMarketing() {
		return academyMarketing;
	}

	public void setAcademyMarketing(String academyMarketing) {
		this.academyMarketing = academyMarketing;
	}


	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getOtherMarketing() {
		return otherMarketing;
	}

	public void setOtherMarketing(String otherMarketing) {
		this.otherMarketing = otherMarketing;
	}

	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	
}