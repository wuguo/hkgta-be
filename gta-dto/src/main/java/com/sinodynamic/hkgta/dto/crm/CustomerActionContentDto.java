package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;



public class CustomerActionContentDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerActionContentDto() {
	
	}
	
		
	private String datetimeStringEN;

	private Date  dateTime;
	
	private String newsContentEN;
	

	public String getDatetimeStringEN() {
		return datetimeStringEN;
	}

	public void setDatetimeStringEN(String datetimeStringEN) {
		this.datetimeStringEN = datetimeStringEN;
	}

	public String getNewsContentEN() {
		return newsContentEN;
	}

	public void setNewsContentEN(String newsContentEN) {
		this.newsContentEN = newsContentEN;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	
	

	
	
	

	

}
