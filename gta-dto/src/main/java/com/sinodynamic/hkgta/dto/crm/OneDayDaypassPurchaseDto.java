package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.codehaus.jackson.annotate.JsonIgnore;

public class OneDayDaypassPurchaseDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String oneday;
	private Long memberQuota;
	private Long hkgtaQuota;
	private BigDecimal oneDayPrice;
	private String itemNo;
	private Long orderQty;
	private String memberFlag;
	private String hkgtaFlag;
	private String errorMsg;
	
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getMemberFlag() {
		return memberFlag;
	}
	public void setMemberFlag(String memberFlag) {
		this.memberFlag = memberFlag;
	}
	public String getHkgtaFlag() {
		return hkgtaFlag;
	}
	public void setHkgtaFlag(String hkgtaFlag) {
		this.hkgtaFlag = hkgtaFlag;
	}
	public Long getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(Long orderQty) {
		this.orderQty = orderQty;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public String getOneday() {
		return oneday;
	}
	public void setOneday(String oneday) {
		this.oneday = oneday;
	}
	public Long getMemberQuota() {
		return memberQuota;
	}
	@JsonIgnore
	public void setMemberQuota2(BigInteger memberQuota) {
		if(null !=memberQuota) {
			this.memberQuota = memberQuota.longValue();
		}
	}
	public void setMemberQuota(Long memberQuota) {
		this.memberQuota = memberQuota;
	}
	public Long getHkgtaQuota() {
		return hkgtaQuota;
	}
	@JsonIgnore
	public void setHkgtaQuota2(BigInteger hkgtaQuota) {
		if(null !=hkgtaQuota) {
			this.hkgtaQuota = hkgtaQuota.longValue();
		}
	}
	public void setHkgtaQuota(Long hkgtaQuota) {
		this.hkgtaQuota = hkgtaQuota;
	}
	public BigDecimal getOneDayPrice() {
		return oneDayPrice;
	}
	public void setOneDayPrice(BigDecimal oneDayPrice) {
		this.oneDayPrice = oneDayPrice;
	}
	
	
}
