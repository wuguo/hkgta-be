package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

public class MemberFacilityAttendanceDto implements Serializable {
	private Long facilityTimeslotId;
	
	private Long customerId;
	
	private String trainerComment;
	
	private String customerComment;
	
	private String assignment;
	
	private Date attendTime;
	
	private Double score;
	
	private String assignmentComments;

	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}

	public String getTrainerComment() {
		return trainerComment;
	}

	public void setTrainerComment(String trainerComment) {
		this.trainerComment = trainerComment;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getAssignmentComments() {
		return assignmentComments;
	}

	public void setAssignmentComments(String assignmentComments) {
		this.assignmentComments = assignmentComments;
	}

	public Long getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(Long customerId)
	{
		this.customerId = customerId;
	}

	public String getCustomerComment()
	{
		return customerComment;
	}

	public void setCustomerComment(String customerComment)
	{
		this.customerComment = customerComment;
	}

	public String getAssignment()
	{
		return assignment;
	}

	public void setAssignment(String assignment)
	{
		this.assignment = assignment;
	}

	public Date getAttendTime()
	{
		return attendTime;
	}

	public void setAttendTime(Date attendTime)
	{
		this.attendTime = attendTime;
	}
	
	
	

}
