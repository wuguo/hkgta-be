package com.sinodynamic.hkgta.dto.crm;


public class SearchSettlementReportDto {
	private String fileType;
	private String sortBy;
	private Integer pageNumber;
	private Integer pageSize;
	private String isAscending;
	private String transactionDate;
	private String startDate;
	private String endDate;
	private String orderStatus;
	private String transStatus;
	private String enrollType;
	private String location;
	private String salesman;
	private String createBy;
	private String auditBy;
	
	public SearchSettlementReportDto() {
		
	}
	public SearchSettlementReportDto(String fileType,String sortBy, Integer pageNumber, Integer pageSize, String isAscending,
			String transactionDate, String startDate, String endDate, String orderStatus, String transStatus,
			String enrollType, String location, String salesman, String createBy, String auditBy) {
		super();
		this.fileType = fileType;
		this.sortBy = sortBy;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.isAscending = isAscending;
		this.transactionDate = transactionDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.orderStatus = orderStatus;
		this.transStatus = transStatus;
		this.enrollType = enrollType;
		this.location = location;
		this.salesman = salesman;
		this.createBy = createBy;
		this.auditBy = auditBy;
	}
	public SearchSettlementReportDto(String sortBy, Integer pageNumber, Integer pageSize, String isAscending,
			String transactionDate, String startDate, String endDate, String orderStatus, String transStatus,
			String enrollType, String location, String salesman, String createBy, String auditBy) {
		super();
		this.sortBy = sortBy;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.isAscending = isAscending;
		this.transactionDate = transactionDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.orderStatus = orderStatus;
		this.transStatus = transStatus;
		this.enrollType = enrollType;
		this.location = location;
		this.salesman = salesman;
		this.createBy = createBy;
		this.auditBy = auditBy;
	}
	
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getIsAscending() {
		return isAscending;
	}
	public void setIsAscending(String isAscending) {
		this.isAscending = isAscending;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getTransStatus() {
		return transStatus;
	}
	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}
	public String getEnrollType() {
		return enrollType;
	}
	public void setEnrollType(String enrollType) {
		this.enrollType = enrollType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSalesman() {
		return salesman;
	}
	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getAuditBy() {
		return auditBy;
	}
	public void setAuditBy(String auditBy) {
		this.auditBy = auditBy;
	}
	
}
