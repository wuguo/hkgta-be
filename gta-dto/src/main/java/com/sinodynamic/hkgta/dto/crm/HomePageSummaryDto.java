package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

/**
 * @since 5/4/2015
 * @author Mianping_Wu
 *
 */
public class HomePageSummaryDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer prospectsNumber;

	private Integer presentationNumber;

	private Integer activeMemberNumber;

	private Integer paymentPendingNumber;

	private Integer willExpireNumber;

	private Boolean isShowRemarkIconLeads = Boolean.FALSE;

	private Boolean isShowRemarkIconEnrollment = Boolean.FALSE;
	
	private Boolean isShowRemarkIconRenewal = Boolean.FALSE;
	
	private Boolean isShowRemarkIconTransaction = Boolean.FALSE;

	public Boolean getIsShowRemarkIconLeads() {
		return isShowRemarkIconLeads;
	}

	public void setIsShowRemarkIconLeads(Boolean isShowRemarkIconLeads) {
		this.isShowRemarkIconLeads = isShowRemarkIconLeads;
	}

	public Boolean getIsShowRemarkIconEnrollment() {
		return isShowRemarkIconEnrollment;
	}

	public void setIsShowRemarkIconEnrollment(Boolean isShowRemarkIconEnrollment) {
		this.isShowRemarkIconEnrollment = isShowRemarkIconEnrollment;
	}

	public Boolean getIsShowRemarkIconRenewal() {
		return isShowRemarkIconRenewal;
	}

	public void setIsShowRemarkIconRenewal(Boolean isShowRemarkIconRenewal) {
		this.isShowRemarkIconRenewal = isShowRemarkIconRenewal;
	}

	public Boolean getIsShowRemarkIconTransaction() {
		return isShowRemarkIconTransaction;
	}

	public void setIsShowRemarkIconTransaction(
			Boolean isShowRemarkIconTransaction) {
		this.isShowRemarkIconTransaction = isShowRemarkIconTransaction;
	}

	public Integer getWillExpireNumber() {
		return willExpireNumber;
	}

	public void setWillExpireNumber(Integer willExpireNumber) {
		this.willExpireNumber = willExpireNumber;
	}

	public Integer getProspectsNumber() {
		return prospectsNumber;
	}

	public void setProspectsNumber(Integer prospectsNumber) {
		this.prospectsNumber = prospectsNumber;
	}

	public Integer getPresentationNumber() {
		return presentationNumber;
	}

	public void setPresentationNumber(Integer presentationNumber) {
		this.presentationNumber = presentationNumber;
	}

	public Integer getActiveMemberNumber() {
		return activeMemberNumber;
	}

	public void setActiveMemberNumber(Integer activeMemberNumber) {
		this.activeMemberNumber = activeMemberNumber;
	}

	public Integer getPaymentPendingNumber() {
		return paymentPendingNumber;
	}

	public void setPaymentPendingNumber(Integer paymentPendingNumber) {
		this.paymentPendingNumber = paymentPendingNumber;
	}

}
