package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

public class GuessRoomReservationDetailDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long nights;
	private BigDecimal itemAmount;
	private Date arrivalDate;
	private Date departureDate;
	private String status;
	private String roomType;
	private List<Date[]> golfingBayPeriod;
	
	public Long getNights() {
		return nights;
	}

	public void setNights(Long nights) {
		this.nights = nights;
	}

	public BigDecimal getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(BigDecimal itemAmount) {
		this.itemAmount = itemAmount;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public List<Date[]> getGolfingBayPeriod() {
		return golfingBayPeriod;
	}

	public void setGolfingBayPeriod(List<Date[]> golfingBayPeriod) {
		this.golfingBayPeriod = golfingBayPeriod;
	}

	public String getGolfingBayPeriodString() {
		List<String> dateStrings = new ArrayList<String>();
		for(Date[] begin_end : golfingBayPeriod){
			dateStrings.add(DateFormatUtils.format(begin_end[0], "yyyy-MM-dd HH:mm") + " - " + DateFormatUtils.format(begin_end[1].getTime() + 1000, "HH:mm"));
		}
		return StringUtils.join(dateStrings, "\n");
	}
}
