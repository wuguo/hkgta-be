package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;


/**
 * @author Kevin_Liang
 *
 */
public class CalendarViewDto implements Serializable {
	/*
	 * Weekday
	 * Weekend		
	 * Public Holiday
	 */
	private Boolean weekday;
	private Boolean Weekend;
	private Boolean holiday;
	
	private String[]publicHoliday;
	
	public Boolean getHoliday() {
		return holiday;
	}
	public void setHoliday(Boolean holiday) {
		this.holiday = holiday;
	}
	public Boolean getWeekday() {
		return weekday;
	}
	public void setWeekday(Boolean weekday) {
		this.weekday = weekday;
	}
	public Boolean getWeekend() {
		return Weekend;
	}
	public void setWeekend(Boolean weekend) {
		Weekend = weekend;
	}
	public String[] getPublicHoliday() {
		return publicHoliday;
	}
	public void setPublicHoliday(String[] publicHoliday) {
		this.publicHoliday = publicHoliday;
	}
	
	

}
