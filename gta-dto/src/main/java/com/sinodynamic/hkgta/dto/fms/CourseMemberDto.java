package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;


/**
 * @author Kevin_Liang
 *
 */
public class CourseMemberDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9074343341530604976L;
	private String courseId;
	private String enrollId;
	private String memberId;
	private String memberName;
	private String registeredDate;
	private String status;
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getEnrollId() {
		return enrollId;
	}
	public void setEnrollId(String enrollId) {
		this.enrollId = enrollId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getRegisteredDate() {
		return registeredDate;
	}
	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
