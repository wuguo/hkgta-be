package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.math.NumberUtils;

public class DailyMonthlyCoachingDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String resvDate;

	private String resvId;

	private String facilityType;
	
	private String bayType;

	private String academyId;

	private String patronName;

	private String memberType;
	
	private String coachId;
	
	private String coachName;

	private Long qty;
	
	private Integer reservationHours;

	private BigDecimal price;

	private String attendanceStatus;
	
	private String status;
	
	private String updateBy;
	
	public String getResvDate() {
		return resvDate;
	}

	public void setResvDate(String resvDate) {
		this.resvDate = resvDate;
	}

	public String getResvId() {
		return resvId;
	}

	public void setResvId(String resvId) {
		//this.resvId = (resvId != null ? NumberUtils.toLong(resvId.toString()) : null);
		this.resvId = resvId;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getCoachId()
	{
		return coachId;
	}

	public void setCoachId(String coachId)
	{
		this.coachId = coachId;
	}

	public String getCoachName()
	{
		return coachName;
	}

	public void setCoachName(String coachName)
	{
		this.coachName = coachName;
	}

	public Long getQty()
	{
		return qty;
	}

	public void setQty(Object qty)
	{
		this.qty = (qty != null ? NumberUtils.toLong(qty.toString()) : null);
	}

	public Integer getReservationHours()
	{
		return reservationHours;
	}

	public void setReservationHours(Object reservationHours)
	{
		this.reservationHours = (reservationHours != null ? NumberUtils.toInt(qty.toString()) : null);
	}

	public BigDecimal getPrice()
	{
		return price;
	}

	public void setPrice(BigDecimal price)
	{
		this.price = price;
	}

	public String getAttendanceStatus()
	{
		return attendanceStatus;
	}

	public void setAttendanceStatus(String attendanceStatus)
	{
		this.attendanceStatus = attendanceStatus;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public String getBayType()
	{
		return bayType;
	}

	public void setBayType(String bayType)
	{
		this.bayType = bayType;
	}


}
