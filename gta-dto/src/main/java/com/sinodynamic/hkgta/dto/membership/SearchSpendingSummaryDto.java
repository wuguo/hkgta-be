package com.sinodynamic.hkgta.dto.membership;


public class SearchSpendingSummaryDto {
	private String sortBy;
	private Integer pageNumber;
	private Integer pageSize;
	private String isAscending;
	private String dateField;
	private String dateVaule;
	private String corporateId;
	private String customerId;
	private String email;
	private String fileType;
	private String userId;
	private String emailTo;
	private String userName;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailTo() {
		return emailTo;
	}
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getIsAscending() {
		return isAscending;
	}
	public void setIsAscending(String isAscending) {
		this.isAscending = isAscending;
	}
	public String getDateField() {
		return dateField;
	}
	public void setDateField(String dateField) {
		this.dateField = dateField;
	}
	public String getDateVaule() {
		return dateVaule;
	}
	public void setDateVaule(String dateVaule) {
		this.dateVaule = dateVaule;
	}
	public String getCorporateId() {
		return corporateId;
	}
	public void setCorporateId(String corporateId) {
		this.corporateId = corporateId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public SearchSpendingSummaryDto(String sortBy, Integer pageNumber,
			Integer pageSize, String isAscending, String dateField,
			String dateVaule, String corporateId, String customerId) {
		super();
		this.sortBy = sortBy;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.isAscending = isAscending;
		this.dateField = dateField;
		this.dateVaule = dateVaule;
		this.corporateId = corporateId;
		this.customerId = customerId;
	}
	public SearchSpendingSummaryDto() {
		super();
	}
	public SearchSpendingSummaryDto(String sortBy, String isAscending, String dateField, String dateVaule,
			String corporateId, String customerId, String email,String fileType,String userId,String emailTo,String userName) {
		super();
		this.sortBy = sortBy;
		this.isAscending = isAscending;
		this.dateField = dateField;
		this.dateVaule = dateVaule;
		this.corporateId = corporateId;
		this.customerId = customerId;
		this.email = email;
		this.fileType = fileType;
		this.userId = userId;
		this.emailTo = emailTo;
		this.userName = userName;
	}
	
}
