package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

public class ContractHelperDto implements Serializable {
	
	private static final long serialVersionUID = -1371912768347486929L;
	
	private Long contractorId;
	private String surname;
	private String givenName;
	private String gender;
	private String passportNo;
	private String passportType;
	private String phoneMobile;
	private String phoneHome;
	private String periodFrom;
	private String periodTo;
	
	private String createBy;
	private String updateBy;
	private Date createDate;
	private Date updateDate;
	
	private String helperPassType;
	private String companyName;
	private String contactEmail;
	private String status;
    private String internalRemark;

	
	public String getInternalRemark() {
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}
	
	
	
	public Long getContractorId() {
		return contractorId;
	}


	public void setContractorId(Long contractorId) {
		this.contractorId = contractorId;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getHelperPassType() {
		return helperPassType;
	}


	public void setHelperPassType(String helperPassType) {
		this.helperPassType = helperPassType;
	}
	


	public String getCreateBy() {
		return createBy;
	}


	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}


	public String getUpdateBy() {
		return updateBy;
	}


	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	
	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getGivenName() {
		return givenName;
	}


	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getPassportNo() {
		return passportNo;
	}


	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}


	public String getPassportType() {
		return passportType;
	}


	public void setPassportType(String passportType) {
		this.passportType = passportType;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getPhoneMobile() {
		return phoneMobile;
	}


	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}


	public String getPhoneHome() {
		return phoneHome;
	}


	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}


	public String getContactEmail() {
		return contactEmail;
	}


	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}


	public String getPeriodFrom() {
		return periodFrom;
	}


	public void setPeriodFrom(String periodFrom) {
		this.periodFrom = periodFrom;
	}


	public String getPeriodTo() {
		return periodTo;
	}


	public void setPeriodTo(String periodTo) {
		this.periodTo = periodTo;
	}


	
	

}
