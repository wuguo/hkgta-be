package com.sinodynamic.hkgta.dto.pms;

import org.apache.commons.lang.builder.ToStringBuilder;

public class PassMemberServiceTaskDto {

	private Long taskId; 					//任务id
	private String status;					//任务需变更状态  RTI:通过  CAN:不通过 
	private String content;					//更改后的任务内容【无更改时传空字符串】
	private String userId;					//用户id
	

	

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
