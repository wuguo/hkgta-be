package com.sinodynamic.hkgta.dto.rpos;

import java.math.BigDecimal;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class TempDto extends GenericDto{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4012521179149592761L;
	@EncryptFieldInfo
	private Long      customerId;
	private Long    orderNo;
	private String    customerSurname;
	private String    customerGivenname;
	private String    customerSalutation;
	private BigDecimal    orderTotalAmount;
	private BigDecimal    balanceDue;
	private String    planName;
	private Object      list    ;
	 
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	 
	public Long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustomerSurname() {
		return customerSurname;
	}
	public void setCustomerSurname(String customerSurname) {
		this.customerSurname = customerSurname;
	}
	public String getCustomerGivenname() {
		return customerGivenname;
	}
	public void setCustomerGivenname(String customerGivenname) {
		this.customerGivenname = customerGivenname;
	}
	public String getCustomerSalutation() {
		return customerSalutation;
	}
	public void setCustomerSalutation(String customerSalutation) {
		this.customerSalutation = customerSalutation;
	}
	 
	public BigDecimal getOrderTotalAmount() {
		return orderTotalAmount;
	}
	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}
	public BigDecimal getBalanceDue() {
		return balanceDue;
	}
	public void setBalanceDue(BigDecimal balanceDue) {
		this.balanceDue = balanceDue;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Object getList() {
		return list;
	}
	public void setList(Object list) {
		this.list = list;
	}
 
}
