package com.sinodynamic.hkgta.dto.qst;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class CustomerAttendanceCountDto implements Serializable {
	
	private Long customerId;
	
	private Long total;
	
	private String surveyType;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else{
			this.customerId = (Long) customerId;
		}
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Object total)
	{	
		if(total instanceof BigInteger){
			this.total =Long.valueOf(total.toString());
		}else if(total instanceof BigDecimal){
			this.total =Long.valueOf(total.toString());
		}else{
			this.total = (Long) total;
		}
	}

	public String getSurveyType() {
		return surveyType;
	}

	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}
	
	

}
