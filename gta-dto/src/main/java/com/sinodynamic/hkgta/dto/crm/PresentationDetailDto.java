package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.entity.crm.PresentationBatch;


public class PresentationDetailDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1371912768347486929L;

	private Long presentId;
	
	private String presentName;
	
	private String description;
	
	private String createDate;
	
	private String updateDate;
	
	private String createBy;
	
	private String updateBy;

	public Long getPresentId() {
		return presentId;
	}

	public void setPresentId(Long presentId) {
		this.presentId = presentId;
	}

	public String getPresentName() {
		return presentName;
	}

	public void setPresentName(String presentName) {
		this.presentName = presentName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
//	@JsonIgnore
	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	
//	@JsonIgnore
	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public PresentationDetailDto(PresentationBatch present)
	{
		this.presentId = present.getPresentId();
		this.presentName = present.getPresentName();
		this.createDate =   DtoHelper.getObliqueYMDDateAndDateDiff(present.getCreateDate(), "yyyy/MM/dd HH:mm");
		this.description = present.getDescription();
		this.updateDate =   DtoHelper.getObliqueYMDDateAndDateDiff(present.getUpdateDate(), "yyyy/MM/dd HH:mm");
		this.createBy = present.getCreateBy();
		this.updateBy = present.getUpdateBy();
	}
	
	public PresentationDetailDto()
	{
		
	}
	
	
	

}
