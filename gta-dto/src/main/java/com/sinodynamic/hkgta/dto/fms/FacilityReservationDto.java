package com.sinodynamic.hkgta.dto.fms;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class FacilityReservationDto
{
	private Long resvId;
	private String resvFacilityType;
	private Long facilityTypeQty;
	private String bookingDate;
	private String status;
	private String coachId;
	private String coachName;
	private String academyNo;
	private String memberName;
	private String startTime;
	private String endTime;
	private String facilityAttribute;
	private String checkedIn;
	private String email;
	private Long orderNo;
	private Long customerId;
	private Long transactionNo;
	private Date createDate;
	private BigDecimal totalAmount;
	private String bayType;
	private Long odQtyhighPrice;
	private Long odQtylowPrice;
	private BigDecimal highPrice;
	private BigDecimal lowPrice;
	private String userId;
	private Long roomResvId;
	private String reserveType;
	private String isBundle;
	private String paymentMedia;
	private String paymentStatus;
	private Date updateDate;

	public BigDecimal getTotalAmount()
	{
		return totalAmount;
	}
	public String getBayType()
	{
		return bayType;
	}
	public void setBayType(String bayType)
	{
		this.bayType = bayType;
	}
	public void setTotalAmount(BigDecimal totalAmount)
	{
		this.totalAmount = totalAmount;
	}
	public Long getResvId()
	{
		return resvId;
	}
	public void setResvId(Object resvId)
	{
		this.resvId = (resvId!=null ? NumberUtils.toLong(resvId.toString()):null);
	}
	public String getResvFacilityType()
	{
		return resvFacilityType;
	}
	public void setResvFacilityType(String resvFacilityType)
	{
		this.resvFacilityType = resvFacilityType;
	}
	public Long getFacilityTypeQty()
	{
		return facilityTypeQty;
	}
	public void setFacilityTypeQty(Object facilityTypeQty)
	{
		this.facilityTypeQty = facilityTypeQty != null ? NumberUtils.toLong(facilityTypeQty.toString()):null;
	}
	public String getBookingDate()
	{
		return bookingDate;
	}
	public void setBookingDate(String bookingDate)
	{
		this.bookingDate = bookingDate;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getCoachId()
	{
		return coachId;
	}
	public void setCoachId(String coachId)
	{
		this.coachId = coachId;
	}
	public String getCoachName()
	{
		return coachName;
	}
	public void setCoachName(String coachName)
	{
		this.coachName = coachName;
	}
	public String getAcademyNo()
	{
		return academyNo;
	}
	public void setAcademyNo(String academyNo)
	{
		this.academyNo = academyNo;
	}
	public String getMemberName()
	{
		return memberName;
	}
	public void setMemberName(String memberName)
	{
		this.memberName = memberName;
	}
	public String getStartTime()
	{
		return startTime;
	}
	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}
	public String getEndTime()
	{
		return endTime;
	}
	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}
	public String getFacilityAttribute()
	{
		return facilityAttribute;
	}
	public void setFacilityAttribute(String facilityAttribute)
	{
		this.facilityAttribute = facilityAttribute;
	}
	public String getCheckedIn()
	{
		return checkedIn;
	}
	public void setCheckedIn(String checkedIn)
	{
		this.checkedIn = checkedIn;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public Long getOrderNo()
	{
		return orderNo;
	}
	public void setOrderNo(Object orderNo)
	{
		this.orderNo = (orderNo!=null ? NumberUtils.toLong(orderNo.toString()):null);
	}
	public Long getCustomerId()
	{
		return customerId;
	}
	public void setCustomerId(Object customerId)
	{
		this.customerId = (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}
	public Long getTransactionNo()
	{
		return transactionNo;
	}
	public void setTransactionNo(Object transactionNo)
	{
		this.transactionNo = (transactionNo!=null ? NumberUtils.toLong(transactionNo.toString()):null);
	}
	public Date getCreateDate()
	{
		return createDate;
	}
	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}
	
	public Long getOdQtyhighPrice()
	{
		return odQtyhighPrice;
	}
	public void setOdQtyhighPrice(Object odQtyhighPrice)
	{
		this.odQtyhighPrice = odQtyhighPrice != null ? NumberUtils.toLong(odQtyhighPrice.toString()) : null;
	}
	public Long getOdQtylowPrice()
	{
		return odQtylowPrice;
	}
	public void setOdQtylowPrice(Object odQtylowPrice)
	{
		this.odQtylowPrice = (odQtylowPrice!=null ? NumberUtils.toLong(odQtylowPrice.toString()):null);
	}
	public BigDecimal getHighPrice()
	{
		return highPrice;
	}
	public void setHighPrice(BigDecimal highPrice)
	{
		this.highPrice = highPrice;
	}
	public BigDecimal getLowPrice()
	{
		return lowPrice;
	}
	public void setLowPrice(BigDecimal lowPrice)
	{
		this.lowPrice = lowPrice;
	}
	public FacilityReservationDto()
	{
	}
	public FacilityReservationDto(Long resvId, String resvFacilityType, Long facilityTypeQty, String bookingDate, String status, String coachId, String coachName, String academyNo, String memberName, String startTime, String endTime, String facilityAttribute, String checkedIn, String email, Long orderNo)
	{
		super();
		this.resvId = resvId;
		this.resvFacilityType = resvFacilityType;
		this.facilityTypeQty = facilityTypeQty;
		this.bookingDate = bookingDate;
		this.status = status;
		this.coachId = coachId;
		this.coachName = coachName;
		this.academyNo = academyNo;
		this.memberName = memberName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.facilityAttribute = facilityAttribute;
		this.checkedIn = checkedIn;
		this.email = email;
		this.orderNo = orderNo;
	}
	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public Long getRoomResvId() {
		return roomResvId;
	}
	public void setRoomResvId(Object roomResvId) {
		this.roomResvId =  (roomResvId!=null ? NumberUtils.toLong(roomResvId.toString()):null);
	}

	public String getReserveType() {
		return reserveType;
	}

	public void setReserveType(String reserveType) {
		this.reserveType = reserveType;
	}
	public String getIsBundle() {
		return isBundle;
	}
	public void setIsBundle(String isBundle) {
		this.isBundle = isBundle;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getPaymentMedia() {
		return paymentMedia;
	}
	public void setPaymentMedia(String paymentMedia) {
		this.paymentMedia = paymentMedia;
	}
	
	
}
