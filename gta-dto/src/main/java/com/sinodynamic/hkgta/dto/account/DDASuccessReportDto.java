package com.sinodynamic.hkgta.dto.account;
import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DDASuccessReportDto implements Serializable{
	
	private String systemDate;
	private String transactionTime;
	private String originatingNo;
	private String customerName;
	private List<DDASuccessReportDetailDto> details;
	private int noOfRecord;
	private int totalTransactionAmount;
	
	
	public String getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}
	public String getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getOriginatingNo() {
		return originatingNo;
	}
	public void setOriginatingNo(String originatingNo) {
		this.originatingNo = originatingNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public List<DDASuccessReportDetailDto> getDetails() {
		return details;
	}
	public void setDetails(List<DDASuccessReportDetailDto> details) {
		this.details = details;
	}
	public int getNoOfRecord() {
		return noOfRecord;
	}
	public void setNoOfRecord(int noOfRecord) {
		this.noOfRecord = noOfRecord;
	}
	public int getTotalTransactionAmount() {
		return totalTransactionAmount;
	}
	public void setTotalTransactionAmount(int totalTransactionAmount) {
		this.totalTransactionAmount = totalTransactionAmount;
	}
	
	/*@Override
	public String toString() {
		return "DDAReport [systemDate=" + systemDate + ", transactionTime="
				+ transactionTime + ", originatingNo=" + originatingNo
				+ ", customerId=" + customerId + ", details=" + details
				+ ", noOfRecord=" + noOfRecord + "]";
	}*/
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
