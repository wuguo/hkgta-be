package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Kevin_Liang
 *
 */
public class RoomRateDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8236548381754793439L;
	private String roomTypeCode;
	private String ratePlanCode;
	private String amountAfterTax;
	private String roomRateDescription;

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public String getRatePlanCode() {
		return ratePlanCode;
	}

	public void setRatePlanCode(String ratePlanCode) {
		this.ratePlanCode = ratePlanCode;
	}

	public String getAmountAfterTax() {
		return amountAfterTax;
	}

	public void setAmountAfterTax(String amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	public String getRoomRateDescription() {
		return roomRateDescription;
	}

	public void setRoomRateDescription(String roomRateDescription) {
		this.roomRateDescription = roomRateDescription;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
