package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.dto.DtoHelper;

/**
 * @author Kevin_Liang
 *
 */
public class PrivateCoachBookListDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8631770628291409144L;
	private String resvId;
	private String memberID;
	private String memberName;
	private String showDate;
	private String showTime;
	private String attributeId;
	private String bayType;
	private String facilityAttribute;
	private String coach;
	private String remark;
	private String coachUserId;
	private String facilityTimeslotId;
	private String portraitPhoto;
	private String score;
	private String trainerComment;
	private String assignment;
	private Long qty;
	private String facilityType;
	private String sortBy;
	private Long isAscending;    
	private Long pageSize = 10L;
	private Long pageNumber = 1L;
	private String status;
	//0-today, 30-within 30 days
	private Long show;
	private String beginDate;
	private String endDate;
	private String coachId;
	private Long customerId;
	private BigDecimal totalAmount;
	private Date createDate;
	private String attendanceStatus;
	private String reserveType;
	
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Object customerId) {
		this.customerId = customerId != null ? NumberUtils.toLong(customerId.toString()) : null;
	}
	public String getCoachId() {
		return coachId;
	}
	public void setCoachId(String coachId) {
		this.coachId = coachId;
	}
	public Long getIsAscending() {
		return isAscending;
	}
	public void setIsAscending(Object isAscending) {
		this.isAscending = isAscending != null ? NumberUtils.toLong(isAscending.toString()) : null;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Long getShow() {
		return show;
	}
	public void setShow(Object show) {
		this.show = show != null ? NumberUtils.toLong(show.toString()) : null;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
	public Long getPageSize() {
		return pageSize;
	}
	public void setPageSize(Object pageSize) {
		this.pageSize = pageSize != null ? NumberUtils.toLong(pageSize.toString()) : null;
	}
	public Long getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Long pageNumber) {
		this.pageNumber = pageNumber != null ? NumberUtils.toLong(pageNumber.toString()) : null;
	}
	public String getResvId() {
		return resvId;
	}
	public void setResvId(Object  resvId) {
		this.resvId = (resvId!=null ? resvId.toString():null);
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	public String getShowDate() {
		return showDate;
	}
	public BigDecimal getTotalAmount()
	{
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount)
	{
		this.totalAmount = totalAmount;
	}
	public void setShowDate(String showDate) {
		this.showDate = showDate;
	}
	@JsonIgnore
	public void setShowDate3(String showDate) {
	}
	@JsonIgnore
	public void setShowDate2(String showDate) {
		if(null!=showDate){
			Date qdate = DtoHelper.parseString2Date(showDate, "yyyy/MM/dd");
			this.showDate = showDate+DtoHelper.getNoOfDays(qdate);
		}
	}
	
	public String getShowTime() {
		return showTime;
	}
	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}
	public String getBayType() {
		return bayType;
	}
	public void setBayType(String bayType) {
		this.bayType = bayType;
	}
	public String getFacilityAttribute() {
		return facilityAttribute;
	}
	public void setFacilityAttribute(String facilityAttribute) {
		this.facilityAttribute = facilityAttribute;
	}
	public String getCoach() {
		return coach;
	}
	public void setCoach(String coach) {
		this.coach = coach;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCoachUserId() {
		return coachUserId;
	}
	public void setCoachUserId(String coachUserId) {
		this.coachUserId = coachUserId;
	}
	public String getFacilityTimeslotId() {
		return facilityTimeslotId;
	}
	public void setFacilityTimeslotId(String facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}
	public String getAssignment() {
		return assignment;
	}
	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}
	public String getPortraitPhoto() {
		return portraitPhoto;
	}
	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getTrainerComment() {
		return trainerComment;
	}
	public void setTrainerComment(String trainerComment) {
		this.trainerComment = trainerComment;
	}
	public Long getQty() {
		return qty;
	}
	public void setQty(Object qty) {
		this.qty = qty != null ? NumberUtils.toLong(qty.toString()) : null;
	}
	public String getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}
	public String getFacilityType() {
		return facilityType;
	}
	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}
	public String getAttendanceStatus() {
		return attendanceStatus;
	}
	public void setAttendanceStatus(String attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}
	public String getReserveType() {
		return reserveType;
	}
	public void setReserveType(String reserveType) {
		this.reserveType = reserveType;
	}
	

}
