package com.sinodynamic.hkgta.dto;

import java.util.List;

/**
 * List page request parameters
 * @author Johnmax_Wang
 * @date   Apr 29, 2015
 */
public class ListPageDto {
	private int pageSize;
	private int currentPage;
	private Long  planNo;
	private String planName;
	private String passPeriodType;
	private String status;
	private Long contractLengthMonth;
	private String effectiveEndDate;
	private String effectiveStartDate;
	private List<Object> filterConditions;
	private List<Object> filterConditionsValue;
	private List<Object> orderConditions;
	private List<Object> orderConditionsValue;
	
	
	public Long getPlanNo() {
		return planNo;
	}
	public void setPlanNo(Long planNo) {
		this.planNo = planNo;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPassPeriodType() {
		return passPeriodType;
	}
	public void setPassPeriodType(String passPeriodType) {
		this.passPeriodType = passPeriodType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getContractLengthMonth() {
		return contractLengthMonth;
	}
	public void setContractLengthMonth(Long contractLengthMonth) {
		this.contractLengthMonth = contractLengthMonth;
	}
	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public List<Object> getFilterConditions() {
		return filterConditions;
	}
	public void setFilterConditions(List<Object> filterConditions) {
		this.filterConditions = filterConditions;
	}
	public List<Object> getFilterConditionsValue() {
		return filterConditionsValue;
	}
	public void setFilterConditionsValue(List<Object> filterConditionsValue) {
		this.filterConditionsValue = filterConditionsValue;
	}
	public List<Object> getOrderConditions() {
		return orderConditions;
	}
	public void setOrderConditions(List<Object> orderConditions) {
		this.orderConditions = orderConditions;
	}
	public List<Object> getOrderConditionsValue() {
		return orderConditionsValue;
	}
	public void setOrderConditionsValue(List<Object> orderConditionsValue) {
		this.orderConditionsValue = orderConditionsValue;
	}
	
	
}
