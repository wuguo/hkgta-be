package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.Date;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class HotelReservationChangeDto extends GenericDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date currentDate;
	private String userId;
	@EncryptFieldInfo
	private RoomResDto reservation;
	private HotelReservationCancelDto reservationCancel;

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		reservationCancel.setCurrentDate(currentDate);
		this.currentDate = currentDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		reservationCancel.setUserId(userId);
		this.userId = userId;
	}

	public RoomResDto getReservation() {
		return reservation;
	}

	public void setReservation(RoomResDto reservation) {
		this.reservation = reservation;
	}

	public HotelReservationCancelDto getReservationCancel() {
		return reservationCancel;
	}

	public void setReservationCancel(HotelReservationCancelDto reservationCancel) {
		this.reservationCancel = reservationCancel;
	}
}
