package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class LoginUserDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String userId;
	
	private String userNo;
	
	private String fullName;
	
	private String userName;
	
	private String userType;
	
	private String portraitPhoto;
	
	private String dateOfBirth;
	
	private String positionTitle;
	
	private String firstName;
	
	private String lastName;
	
	private String memberType;
	
	private String token;
	
	private String patronReservationWellnessSwitch;
	
	private String patronReservationAccommodationSwitch;
	
	private String loginId;
	
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPatronReservationAccommodationSwitch() {
		return patronReservationAccommodationSwitch;
	}

	public void setPatronReservationAccommodationSwitch(String patronReservationAccommodationSwitch) {
		this.patronReservationAccommodationSwitch = patronReservationAccommodationSwitch;
	}

	private String questionnaireURL;
	
	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	public String getMemberType()
	{
		return memberType;
	}

	public void setMemberType(String memberType)
	{
		this.memberType = memberType;
	}

	public String getFullName()
	{
		return fullName;
	}

	public void setFullName(String fullName)
	{
		this.fullName = fullName;
	}

	public String getUserNo()
	{
		return userNo;
	}

	public void setUserNo(String userNo)
	{
		this.userNo = userNo;
	}


	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getUserType()
	{
		return userType;
	}

	public void setUserType(String userType)
	{
		this.userType = userType;
	}
	
	public String getPortraitPhoto()
	{
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto)
	{
		this.portraitPhoto = portraitPhoto;
	}

	public String getDateOfBirth()
	{
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}

	public String getPositionTitle()
	{
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle)
	{
		this.positionTitle = positionTitle;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getPatronReservationWellnessSwitch() {
		return patronReservationWellnessSwitch;
	}

	public void setPatronReservationWellnessSwitch(String patronReservationWellnessSwitch) {
		this.patronReservationWellnessSwitch = patronReservationWellnessSwitch;
	}

	
	public String getQuestionnaireURL() {
		return questionnaireURL;
	}

	public void setQuestionnaireURL(String questionnaireURL) {
		this.questionnaireURL = questionnaireURL;
	}

	public LoginUserDto()
	{}
	
	public LoginUserDto(String userId, String userNo, String userName,
			String userType, String fullName, String portraitPhoto, String dateOfBirth, String positionTitle, String firstName, String lastName, String memberType)
	{
		this.userId = userId;
		this.userNo = userNo;
		this.userName = userName;
		this.userType = userType;
		this.fullName = fullName;
		this.portraitPhoto = portraitPhoto;
		this.dateOfBirth = dateOfBirth;
		this.positionTitle = positionTitle;
		this.firstName = firstName;
		this.lastName = lastName;
		this.memberType = memberType;
	}

	
	
	

}