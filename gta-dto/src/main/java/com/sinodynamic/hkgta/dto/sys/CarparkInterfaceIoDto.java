package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;
import java.util.Date;

public class CarparkInterfaceIoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5581946421935677912L;
	private String inTime;
	private String outTime;
	private String vehiclePlateNo;
	private String cardType;
	private String importCardId;
	private String checkStatus;
	private String  academyNo;
	
	public String getAcademyNo() {
		return academyNo;
	}
	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}
	public String getCheckStatus() {
		return checkStatus;
	}
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}
	public String getInTime() {
		return inTime;
	}
	public void setInTime(String inTime) {
		this.inTime = inTime;
	}
	public String getOutTime() {
		return outTime;
	}
	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}
	public String getVehiclePlateNo() {
		return vehiclePlateNo;
	}
	public void setVehiclePlateNo(String vehiclePlateNo) {
		this.vehiclePlateNo = vehiclePlateNo;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getImportCardId() {
		return importCardId;
	}
	public void setImportCardId(String importCardId) {
		this.importCardId = importCardId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}