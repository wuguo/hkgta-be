package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;

public class SettleMMSPaymentByGuestFoliorResponseDto implements Serializable {
	private String returnCode;
	private String message;
	private Data data;

	/**
	 * 
	 * @return The returnCode
	 */
	public String getReturnCode() {
		return returnCode;
	}

	/**
	 * 
	 * @param returnCode
	 *            The returnCode
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return The data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * 
	 * @param data
	 *            The data
	 */
	public void setData(Data data) {
		this.data = data;
	}

	public class Data {

		private String orderNo;
		private String transactionNo;
		private String settleTimestamp;

		/**
		 * 
		 * @return The orderNo
		 */
		public String getOrderNo() {
			return orderNo;
		}

		/**
		 * 
		 * @param orderNo
		 *            The orderNo
		 */
		public void setOrderNo(String orderNo) {
			this.orderNo = orderNo;
		}

		/**
		 * 
		 * @return The transactionNo
		 */
		public String getTransactionNo() {
			return transactionNo;
		}

		/**
		 * 
		 * @param transactionNo
		 *            The transactionNo
		 */
		public void setTransactionNo(String transactionNo) {
			this.transactionNo = transactionNo;
		}

		/**
		 * 
		 * @return The settleTimestamp
		 */
		public String getSettleTimestamp() {
			return settleTimestamp;
		}

		/**
		 * 
		 * @param settleTimestamp
		 *            The settleTimestamp
		 */
		public void setSettleTimestamp(String settleTimestamp) {
			this.settleTimestamp = settleTimestamp;
		}

	}
}
