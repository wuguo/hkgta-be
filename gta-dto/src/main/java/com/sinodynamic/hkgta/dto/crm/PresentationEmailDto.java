package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class PresentationEmailDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String presentID;
	private String customerID;
	private String emailTo;
	private String emailTitle;
	private String emailBody;
	private String messageSubject;

	public String getPresentID() {
		return presentID;
	}

	public void setPresentID(String presentID) {
		this.presentID = presentID;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getEmailTitle() {
		return emailTitle;
	}

	public void setEmailTitle(String emailTitle) {
		this.emailTitle = emailTitle;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getMessageSubject() {
		return messageSubject;
	}

	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}
	
	
}
