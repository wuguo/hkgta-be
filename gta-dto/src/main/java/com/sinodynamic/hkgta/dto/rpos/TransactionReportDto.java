package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TransactionReportDto  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8724980534326135257L;
	
	private String servciceType;
	private String reservationID;
	private String patronId;
	private String patronName;
	private Date reservationStartDate;
	private Date reservationEndDate;
	private String location;
	private String sourceType;
	private String status;
	private String createBy;
	private Date createDate;
	private String qty;
	public String getServciceType() {
		return servciceType;
	}
	public void setServciceType(String servciceType) {
		this.servciceType = servciceType;
	}
	public String getReservationID() {
		return reservationID;
	}
	public void setReservationID(String reservationID) {
		this.reservationID = reservationID;
	}
	public String getPatronId() {
		return patronId;
	}
	public void setPatronId(String patronId) {
		this.patronId = patronId;
	}
	public String getPatronName() {
		return patronName;
	}
	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}
	public Date getReservationStartDate() {
		return reservationStartDate;
	}
	public void setReservationStartDate(Date reservationStartDate) {
		this.reservationStartDate = reservationStartDate;
	}
	public Date getReservationEndDate() {
		return reservationEndDate;
	}
	public void setReservationEndDate(Date reservationEndDate) {
		this.reservationEndDate = reservationEndDate;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	 
 
	
	
	 
	
}
