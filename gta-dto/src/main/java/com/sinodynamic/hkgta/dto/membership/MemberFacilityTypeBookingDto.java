package com.sinodynamic.hkgta.dto.membership;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.math.NumberUtils;

public class MemberFacilityTypeBookingDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String facilityName;

	private String bookingDateFull;
	 
	private String startTime;
	
	private String endTime;
	
	private String bookingDate;

	private String status;
	
	private String enrollStatus;

	private Long facilityTypeQty;

	private String courseName;

	private String venue;

	private Long enrollId;

	private Long orderNo;

	private Long resvId;
	
	private String resvIdStr;

	private String facilityAttribute;

	private String coachName;

	private String remark;

	private Long sessionNo;

	private Date beginDate;

	private Date endDate;
	
	private String facilityType;
	
	private Long courseId;
	
	private Long attendId;
	
	private Long sessionId;
	
	private String posterFilename;
	
	private Date createDate;
	
	private String remainingTime;
	
	private String venueCode;
	
	private Long floorNo;
	
	private String restaurantId;

	private String phone;
	
	private Long facilityTypeResvId;
	
	private List<MemberFacilityTypeBookingDto> roomResFacilitys;
	
	private String beginDateStr;

	private String endDateStr;
	
	private String confirmId;
	
	private String isBundle;
	
	private Long totalSessionNo;
	
	private String therapist;
	
	private String roomNo;
	
	private String resvType;
	
	private Date checkinTimestamp;
	
	private Date checkoutTimestamp;
	
	private Long picId;
	
	private Integer roomId;
	
	/***
	 * CR51
	 * @return
	 */
	private String vendorPoiId;
	
	public String getVendorPoiId() {
		return vendorPoiId;
	}

	public void setVendorPoiId(String vendorPoiId) {
		this.vendorPoiId = vendorPoiId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Long getEnrollId() {
		return enrollId;
	}

	public void setEnrollId(Object enrollId) {
		if (enrollId instanceof BigInteger) {
			this.enrollId = ((BigInteger) enrollId).longValue();
		} else if (enrollId instanceof Integer) {
			this.enrollId = ((Integer) enrollId).longValue();
		} else if (enrollId instanceof String) {
			this.enrollId = Long.valueOf((String) enrollId);
		} else {
			this.enrollId = (Long) enrollId;
		}
	}

	public void setResvIdStr(String resvIdStr) {
		this.resvIdStr = resvIdStr;
	}

	public String getFacilityAttribute() {
		return facilityAttribute;
	}

	public void setFacilityAttribute(String facilityAttribute) {
		this.facilityAttribute = facilityAttribute;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Long getFacilityTypeQty() {
		return facilityTypeQty;
	}

	public void setFacilityTypeQty(Object facilityTypeQty) {
		if (facilityTypeQty instanceof BigInteger) {
			this.facilityTypeQty = ((BigInteger) facilityTypeQty).longValue();
		} else if (facilityTypeQty instanceof Integer) {
			this.facilityTypeQty = ((Integer) facilityTypeQty).longValue();
		} else if (facilityTypeQty instanceof String) {
			this.facilityTypeQty = Long.valueOf((String) facilityTypeQty);
		} else {
			this.facilityTypeQty = (Long) facilityTypeQty;
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Object orderNo) {
		if (orderNo instanceof BigInteger) {
			this.orderNo = ((BigInteger) orderNo).longValue();
		} else if (orderNo instanceof Integer) {
			this.orderNo = ((Integer) orderNo).longValue();
		} else if (orderNo instanceof String) {
			this.orderNo = Long.valueOf((String) orderNo);
		} else {
			this.orderNo = (Long) orderNo;
		}
	}

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Object resvId) {
		if (resvId instanceof BigInteger) {
			this.resvId = ((BigInteger) resvId).longValue();
		} else if (resvId instanceof Integer) {
			this.resvId = ((Integer) resvId).longValue();
		} else if (resvId instanceof String) {
			if(((String) resvId).startsWith("SPA-")){
				resvIdStr = (String)resvId;
			}else{
				this.resvId = Long.valueOf((String)resvId);
			}
		} else {
			this.resvId = (Long) resvId;
		}
	}

	public String getCoachName() {
		return coachName;
	}

	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}

	public Long getSessionNo() {
		return sessionNo;
	}

	public void setSessionNo(Object sessionNo) {
		if (sessionNo instanceof BigInteger) {
			this.sessionNo = ((BigInteger) sessionNo).longValue();
		} else if (sessionNo instanceof Integer) {
			this.sessionNo = ((Integer) sessionNo).longValue();
		} else if (sessionNo instanceof String) {
			this.sessionNo = Long.valueOf((String) sessionNo);
		} else {
			this.sessionNo = (Long) sessionNo;
		}
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBookingDateFull() {
		return bookingDateFull;
	}

	public void setBookingDateFull(String bookingDateFull) {
		this.bookingDateFull = bookingDateFull;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Object courseId) {
		if (courseId instanceof BigInteger) {
			this.courseId = ((BigInteger) courseId).longValue();
		} else if (courseId instanceof Integer) {
			this.courseId = ((Integer) courseId).longValue();
		} else if (courseId instanceof String) {
			this.courseId = Long.valueOf((String) courseId);
		} else {
			this.courseId = (Long) courseId;
		}
	}

	public Long getAttendId() {
		return attendId;
	}

	public void setAttendId(Object attendId) {
		if (attendId instanceof BigInteger) {
			this.attendId = ((BigInteger) attendId).longValue();
		} else if (attendId instanceof Integer) {
			this.attendId = ((Integer) attendId).longValue();
		} else if (attendId instanceof String) {
			this.attendId = Long.valueOf((String) attendId);
		} else {
			this.attendId = (Long) attendId;
		}
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Object sessionId) {
		if (sessionId instanceof BigInteger) {
			this.sessionId = ((BigInteger) sessionId).longValue();
		} else if (sessionId instanceof Integer) {
			this.sessionId = ((Integer) sessionId).longValue();
		} else if (sessionId instanceof String) {
			this.sessionId = Long.valueOf((String) sessionId);
		} else {
			this.sessionId = (Long) sessionId;
		}
	}

	public String getPosterFilename() {
		return posterFilename;
	}

	public void setPosterFilename(String posterFilename) {
		this.posterFilename = posterFilename;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getEnrollStatus() {
		return enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}

	public String getRemainingTime() {
		return remainingTime;
	}

	public void setRemainingTime(String remainingTime) {
		this.remainingTime = remainingTime;
	}

	public String getVenueCode() {
		return venueCode;
	}

	public void setVenueCode(String venueCode) {
		this.venueCode = venueCode;
	}

	public Long getFloorNo() {
		return floorNo;
	}

	public void setFloorNo(Long floorNo) {
		this.floorNo = floorNo;
	}

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<MemberFacilityTypeBookingDto> getRoomResFacilitys()
	{
		return roomResFacilitys;
	}

	public void setRoomResFacilitys(List<MemberFacilityTypeBookingDto> roomResFacilitys)
	{
		this.roomResFacilitys = roomResFacilitys;
	}

	public Long getFacilityTypeResvId()
	{
		return facilityTypeResvId;
	}

	public void setFacilityTypeResvId(Object facilityTypeResvId)
	{
		this.facilityTypeResvId = (facilityTypeResvId!=null ? NumberUtils.toLong(facilityTypeResvId.toString()):null);
	}

	public String getBeginDateStr() {
		return beginDateStr;
	}

	public void setBeginDateStr(String beginDateStr) {
		this.beginDateStr = beginDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getConfirmId() {
		return confirmId;
	}

	public void setConfirmId(String confirmId) {
		this.confirmId = confirmId;
	}

	public String getIsBundle() {
		return isBundle;
	}

	public void setIsBundle(String isBundle) {
		this.isBundle = isBundle;
	}

	public Long getTotalSessionNo() {
		return totalSessionNo;
	}

	public void setTotalSessionNo(Long totalSessionNo) {
		this.totalSessionNo = totalSessionNo;
	}

	public String getTherapist() {
		return therapist;
	}

	public void setTherapist(String therapist) {
		this.therapist = therapist;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getResvType() {
		return resvType;
	}

	public void setResvType(String resvType) {
		this.resvType = resvType;
	}

	public Date getCheckinTimestamp() {
		return checkinTimestamp;
	}

	public void setCheckinTimestamp(Date checkinTimestamp) {
		this.checkinTimestamp = checkinTimestamp;
	}

	public Date getCheckoutTimestamp() {
		return checkoutTimestamp;
	}

	public void setCheckoutTimestamp(Date checkoutTimestamp) {
		this.checkoutTimestamp = checkoutTimestamp;
	}

	public Long getPicId() {
		return picId;
	}

	public void setPicId(Object picId) {
		this.picId = (picId!=null ? NumberUtils.toLong(picId.toString()):null);
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId == null ? 0:roomId ;
	}
	
	public String getResvIdStr() {
		if("Accommodation".equals(this.facilityName)){
			resvIdStr = "GRM-" + resvId;
		}else if("Golfing Bay".equals(this.facilityName)){
			resvIdStr = "GBF-" + resvId;
		}else if ("Tennis Court".equals(this.facilityName)) {
			resvIdStr = "TCF-" + resvId;
		}else if ("Tennis Course".equals(this.facilityName)) {
			resvIdStr = "TCE-" + resvId;
//		}else if ("WELLNESS".equals(this.facilityType)) {
//			resvIdStr = "SPA-" + resvId;
		}else if ("Golf Private Coaching".equals(this.facilityName)) {
			resvIdStr = "GPC-" + resvId;
		}else if ("Tennis Private Coaching".equals(this.facilityName)) {
			resvIdStr = "TPC-" + resvId;
			/***
			 * remove restaurant name display resvIdStr ,because the name is change ,when the name is change ,the resvIdStr display null
			 */
//		}else if ("Bar & Lounge".equals(this.facilityName)) {
//			resvIdStr = "BAR-" + resvId;
//		}else if ("Chinese Restaurant".equals(this.facilityName)) {
//			resvIdStr = "RES-" + resvId;
//		}else if ("All Day Dining".equals(this.facilityName)) {
//			resvIdStr = "CAF-" + resvId;
//		}else if ("Sakti Dining".equals(this.facilityName)) {
//			resvIdStr = "SKD-" + resvId;
//		}else if ("Royal Tea Lounge".equals(this.facilityName)) {
//			resvIdStr = "RTL-" + resvId;
		}else if ("Golf Course".equals(this.facilityName)) {
			resvIdStr = "GCE-" + resvId;
		}
		return resvIdStr;
	}
}
