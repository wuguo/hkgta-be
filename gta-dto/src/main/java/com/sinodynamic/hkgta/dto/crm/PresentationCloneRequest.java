package com.sinodynamic.hkgta.dto.crm;

public class PresentationCloneRequest {
	
	private long presentId;
	private String presentName;
	
	public long getPresentId() {
		return presentId;
	}
	
	public void setPresentId(long presentId) {
		this.presentId = presentId;
	}
	
	public String getPresentName() {
		return presentName;
	}
	
	public void setPresentName(String presentName) {
		this.presentName = presentName;
	}
	
	

}
