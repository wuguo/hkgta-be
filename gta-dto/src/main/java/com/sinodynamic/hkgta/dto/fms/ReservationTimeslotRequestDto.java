package com.sinodynamic.hkgta.dto.fms;

public class ReservationTimeslotRequestDto {

	private Long facilityTimeslotId;
	
	private Long facilityNo;

	public long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}

	public Long getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}
}
