package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;


public class StaffProfileDto implements Serializable {
	
	private static final long serialVersionUID = 1L;

	
	private String userId;

	private String contactEmail;

	private String createBy;

	private String dateOfBirth;

	private String gender;

	private String givenName;

	private String nationality;

	private String passportNo;

	private String passportType;

	private String phoneHome;

	private String phoneMobile;
	
	private String phoneOffice;

	private String portraitPhoto;

	private String postalAddress1;
	
	private String postalAddress2;

	private String postalArea;

	private String postalDistrict;

	private String surname;
	
	private String surnameNls;
	
	private String givenNameNls;
	
	private String updateBy;

	public String getUserId()
	{
		return userId;
	}


	public void setUserId(String userId)
	{
		this.userId = userId;
	}


	public String getContactEmail()
	{
		return contactEmail;
	}


	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = contactEmail;
	}


	public String getCreateBy()
	{
		return createBy;
	}


	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}


	public String getDateOfBirth()
	{
		return dateOfBirth;
	}


	public void setDateOfBirth(String dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}


	public String getGender()
	{
		return gender;
	}


	public void setGender(String gender)
	{
		this.gender = gender;
	}


	public String getGivenName()
	{
		return givenName;
	}


	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}


	public String getNationality()
	{
		return nationality;
	}


	public void setNationality(String nationality)
	{
		this.nationality = nationality;
	}


	public String getPassportNo()
	{
		return passportNo;
	}


	public void setPassportNo(String passportNo)
	{
		this.passportNo = passportNo;
	}


	public String getPassportType()
	{
		return passportType;
	}


	public void setPassportType(String passportType)
	{
		this.passportType = passportType;
	}


	public String getPhoneHome()
	{
		return phoneHome;
	}


	public void setPhoneHome(String phoneHome)
	{
		this.phoneHome = phoneHome;
	}


	public String getPhoneMobile()
	{
		return phoneMobile;
	}


	public void setPhoneMobile(String phoneMobile)
	{
		this.phoneMobile = phoneMobile;
	}


	public String getPhoneOffice()
	{
		return phoneOffice;
	}


	public void setPhoneOffice(String phoneOffice)
	{
		this.phoneOffice = phoneOffice;
	}


	public String getPortraitPhoto()
	{
		return portraitPhoto;
	}


	public void setPortraitPhoto(String portraitPhoto)
	{
		this.portraitPhoto = portraitPhoto;
	}


	public String getPostalAddress1()
	{
		return postalAddress1;
	}


	public void setPostalAddress1(String postalAddress1)
	{
		this.postalAddress1 = postalAddress1;
	}


	public String getPostalAddress2()
	{
		return postalAddress2;
	}


	public void setPostalAddress2(String postalAddress2)
	{
		this.postalAddress2 = postalAddress2;
	}


	public String getPostalArea()
	{
		return postalArea;
	}


	public void setPostalArea(String postalArea)
	{
		this.postalArea = postalArea;
	}


	public String getPostalDistrict()
	{
		return postalDistrict;
	}


	public void setPostalDistrict(String postalDistrict)
	{
		this.postalDistrict = postalDistrict;
	}


	public String getSurname()
	{
		return surname;
	}


	public void setSurname(String surname)
	{
		this.surname = surname;
	}


	public String getSurnameNls()
	{
		return surnameNls;
	}


	public void setSurnameNls(String surnameNls)
	{
		this.surnameNls = surnameNls;
	}


	public String getGivenNameNls()
	{
		return givenNameNls;
	}


	public void setGivenNameNls(String givenNameNls)
	{
		this.givenNameNls = givenNameNls;
	}


	public String getUpdateBy()
	{
		return updateBy;
	}


	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}


	public StaffProfileDto() {
	}
	
}
