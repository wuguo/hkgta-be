package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class CorporateProfileDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String loginUserId;
	
	private String loginUserName;
	
	private Long servicePlanNo;
	
	private String planName;
	
	private Long corporateId;
	
	private String brNo;

	private String address1;

	private String address2;

	private String businessNatureCode;

	private String companyName;

	private String companyNameNls;

	private String contactEmail;

	private String contactPerson;

	private String contactPhone;

	private String district;

	private String checkBillingAddress;

	private String billingAddress1;

	private String billingAddress2;

	private String billingHKDistrict;
	
	private String contractRefNo;
	
	private String remark;
	
	private BigDecimal creditLimit;
	
	private Date createDate;
	
	private String followByStaff;
	
	private String status;
	
	private Long accNo;
	
	private Integer corporateProfileVersion;
	
	private Integer corporateAdditionAddressVersion;
	
	private Integer corporateServiceAccVersion;
	
	private Boolean primaryCreationRight;
	
	private Long noOfActiveMember;
	
	private String contactPersonFirstname;

	private String contactPersonLastname;
	
	private String contactPhoneMobile;
	
	private String contactPersonFullName;
	
	private List<CorporateAdditionContactDto> corporateAdditionContactDtos;
	
	private String salesStaffName;
	
	
	
	public String getSalesStaffName() {
		return salesStaffName;
	}

	public void setSalesStaffName(String salesStaffName) {
		this.salesStaffName = salesStaffName;
	}

	//added by Kaster 20160509
	private String salesStaffId;
	
	public String getSalesStaffId() {
		return salesStaffId;
	}

	public void setSalesStaffId(String salesStaffId) {
		this.salesStaffId = salesStaffId;
	}

	public Integer getCorporateServiceAccVersion() {
		return corporateServiceAccVersion;
	}

	public void setCorporateServiceAccVersion(Object corporateServiceAccVersion) {
		if (corporateServiceAccVersion instanceof Long) {
			this.corporateServiceAccVersion = ((Long) corporateServiceAccVersion).intValue();
		}
		else if (corporateServiceAccVersion instanceof BigInteger) {
			this.corporateServiceAccVersion = ((BigInteger) corporateServiceAccVersion).intValue();
		}
		else if (corporateServiceAccVersion instanceof String) {
			this. corporateServiceAccVersion = Integer.parseInt((String)corporateServiceAccVersion);
		}
		else {
			this.corporateServiceAccVersion = (Integer)corporateServiceAccVersion;
		}
	}

	public Integer getCorporateAdditionAddressVersion() {
		return corporateAdditionAddressVersion;
	}

	public void setCorporateAdditionAddressVersion(
			Object corporateAdditionAddressVersion) {
		if (corporateAdditionAddressVersion instanceof Long) {
			this.corporateAdditionAddressVersion = ((Long) corporateAdditionAddressVersion).intValue();
		}
		else if (corporateAdditionAddressVersion instanceof BigInteger) {
			this.corporateAdditionAddressVersion = ((BigInteger) corporateAdditionAddressVersion).intValue();
		}
		else if (corporateAdditionAddressVersion instanceof String) {
			this. corporateAdditionAddressVersion = Integer.parseInt((String)corporateAdditionAddressVersion);
		}
		else {
			this.corporateAdditionAddressVersion = (Integer)corporateAdditionAddressVersion;
		}
	}

	public Integer getCorporateProfileVersion() {
		return corporateProfileVersion;
	}

	public void setCorporateProfileVersion(Object corporateProfileVersion) {
		if (corporateProfileVersion instanceof Long) {
			this.corporateProfileVersion = ((Long) corporateProfileVersion).intValue();
		}
		else if (corporateProfileVersion instanceof BigInteger) {
			this.corporateProfileVersion = ((BigInteger) corporateProfileVersion).intValue();
		}
		else if (corporateProfileVersion instanceof String) {
			this. corporateProfileVersion = Integer.parseInt((String)corporateProfileVersion);
		}
		else {
			this.corporateProfileVersion = (Integer)corporateProfileVersion;
		}
	}

	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

	public String getLoginUserName() {
		return loginUserName;
	}

	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}
	
	public Long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Object corporateId) {
		if(corporateId instanceof BigInteger){
			this.corporateId = ((BigInteger) corporateId).longValue();
		}else if(corporateId instanceof Integer){
			this.corporateId = ((Integer) corporateId).longValue();
		}else if(corporateId instanceof String){
			this.corporateId = Long.valueOf((String) corporateId);
		}else{
			this.corporateId = (Long) corporateId;
		}
	}

	public Long getServicePlanNo() {
		return servicePlanNo;
	}

	public void setServicePlanNo(Object servicePlanNo) {
		if(servicePlanNo instanceof BigInteger){
			this.servicePlanNo = ((BigInteger) servicePlanNo).longValue();
		}else if(servicePlanNo instanceof Integer){
			this.servicePlanNo = ((Integer) servicePlanNo).longValue();
		}else if(servicePlanNo instanceof String){
			this.servicePlanNo = Long.valueOf((String) servicePlanNo);
		}else{
			this.servicePlanNo = (Long) servicePlanNo;
		}
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getBrNo() {
		return brNo;
	}

	public void setBrNo(String brNo) {
		this.brNo = DtoHelper.nvl(brNo);
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = DtoHelper.nvl(address1);
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = DtoHelper.nvl(address2);
	}

	public String getBusinessNatureCode() {
		return businessNatureCode;
	}

	public void setBusinessNatureCode(String businessNatureCode) {
		this.businessNatureCode = DtoHelper.nvl(businessNatureCode);
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = DtoHelper.nvl(companyName);
	}

	public String getCompanyNameNls() {
		return companyNameNls;
	}

	public void setCompanyNameNls(String companyNameNls) {
		this.companyNameNls = DtoHelper.nvl(companyNameNls);
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = DtoHelper.nvl(contactEmail);
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = DtoHelper.nvl(contactPerson);
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = DtoHelper.nvl(contactPhone);
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = DtoHelper.nvl(district);
	}

	public String getBillingAddress1() {
		return billingAddress1;
	}

	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = DtoHelper.nvl(billingAddress1);
	}

	public String getBillingAddress2() {
		return billingAddress2;
	}

	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = DtoHelper.nvl(billingAddress2);
	}

	public String getBillingHKDistrict() {
		return billingHKDistrict;
	}

	public void setBillingHKDistrict(String billingHKDistrict) {
		this.billingHKDistrict = DtoHelper.nvl(billingHKDistrict);
	}

	public String getCheckBillingAddress() {
		return checkBillingAddress;
	}

	public void setCheckBillingAddress(String checkBillingAddress) {
		this.checkBillingAddress = DtoHelper.nvl(checkBillingAddress);
	}

	public String getContractRefNo() {
		return contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = DtoHelper.nvl(contractRefNo);
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = DtoHelper.nvl(remark);
	}

	public String getFollowByStaff() {
		return followByStaff;
	}

	public void setFollowByStaff(String followByStaff) {
		this.followByStaff = followByStaff;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getAccNo() {
		return accNo;
	}

	public void setAccNo(Object accNo) {
		if(accNo instanceof BigInteger){
			this.accNo = ((BigInteger) accNo).longValue();
		}else if(accNo instanceof Integer){
			this.accNo = ((Integer) accNo).longValue();
		}else if(accNo instanceof String){
			this.accNo = Long.valueOf((String) accNo);
		}else{
			this.accNo = (Long) accNo;
		}
	}

	public Boolean isPrimaryCreationRight() {
		return primaryCreationRight;
	}

	public void setPrimaryCreationRight(Boolean primaryCreationRight) {
		this.primaryCreationRight = primaryCreationRight;
	}

	public Long getNoOfActiveMember() {
		return noOfActiveMember;
	}

	public void setNoOfActiveMember(Object noOfActiveMember) {
		this.noOfActiveMember =(noOfActiveMember!=null ? NumberUtils.toLong(noOfActiveMember.toString()):null);
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public List<CorporateAdditionContactDto> getCorporateAdditionContactDtos() {
		return corporateAdditionContactDtos;
	}

	public void setCorporateAdditionContactDtos(List<CorporateAdditionContactDto> corporateAdditionContactDtos) {
		this.corporateAdditionContactDtos = corporateAdditionContactDtos;
	}

	public String getContactPersonFirstname() {
		return contactPersonFirstname;
	}

	public void setContactPersonFirstname(String contactPersonFirstname) {
		this.contactPersonFirstname = contactPersonFirstname;
	}

	public String getContactPersonLastname() {
		return contactPersonLastname;
	}

	public void setContactPersonLastname(String contactPersonLastname) {
		this.contactPersonLastname = contactPersonLastname;
	}

	public String getContactPhoneMobile() {
		return contactPhoneMobile;
	}

	public void setContactPhoneMobile(String contactPhoneMobile) {
		this.contactPhoneMobile = contactPhoneMobile;
	}

	public String getContactPersonFullName() {
		return contactPersonFullName;
	}

	public void setContactPersonFullName(String contactPersonFullName) {
		this.contactPersonFullName = contactPersonFullName;
	}
	
	
}
