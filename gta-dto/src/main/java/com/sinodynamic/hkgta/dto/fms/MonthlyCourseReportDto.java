package com.sinodynamic.hkgta.dto.fms;

import com.sinodynamic.hkgta.dto.CustomDateSerializer;
import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Mason_Yang on 12/22/2015.
 */
public class MonthlyCourseReportDto {

    private Long courseId;
    private String courseType;
    private String courseName;
    private Date periodStart;
    private Date periodEnd;
    private String memberAcceptance;
    private Long sessionCnt;
    private Long tutorCnt;
    private BigDecimal price;
    private Long studentCnt;
    private BigDecimal totalRevenue;

    private Date closeDate;
    private String closeBy;
    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Object courseId) {
        this.courseId = (courseId != null ? NumberUtils.toLong(courseId.toString()) : null);
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(Date periodEnd) {
        this.periodEnd = periodEnd;
    }

    public String getMemberAcceptance() {
        return memberAcceptance;
    }

    public void setMemberAcceptance(String memberAcceptance) {
        this.memberAcceptance = memberAcceptance;
    }

    public Long getSessionCnt() {
        return sessionCnt;
    }

    public void setSessionCnt(Object sessionCnt) {
        this.sessionCnt = (sessionCnt != null ? NumberUtils.toLong(sessionCnt.toString()) : null);
    }

    public Long getTutorCnt() {
        return tutorCnt;
    }

    public void setTutorCnt(Object tutorCnt) {
        this.tutorCnt = (tutorCnt != null ? NumberUtils.toLong(tutorCnt.toString()) : null);
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getStudentCnt() {
        return studentCnt;
    }

    public void setStudentCnt(Object studentCnt) {
        this.studentCnt = (studentCnt != null ? NumberUtils.toLong(studentCnt.toString()) : null);
    }

    public BigDecimal getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(BigDecimal totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getCloseBy() {
        return closeBy;
    }

    public void setCloseBy(String closeBy) {
        this.closeBy = closeBy;
    }
}
