package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FolderInfoDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long folderId;
	private String folderName;
	private String folderParentPath;
	private String folderPrefixKey;
	private int folderFileNumber;
	private Date folderCreatedDate;
	private Date folderUpdatedDate;
	private List<PresentMaterialDto> presentMaterialDtos;
	
	public List<PresentMaterialDto> getPresentMaterialDtos() {
		return presentMaterialDtos;
	}

	public void setPresentMaterialDtos(List<PresentMaterialDto> presentMaterialDtos) {
		this.presentMaterialDtos = presentMaterialDtos;
	}

	public Long getFolderId() {
		return folderId;
	}

	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}
	
	public Date getFolderUpdatedDate() {
		return folderUpdatedDate;
	}

	public void setFolderUpdatedDate(Date folderUpdatedDate) {
		this.folderUpdatedDate = folderUpdatedDate;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getFolderParentPath() {
		return folderParentPath;
	}

	public void setFolderParentPath(String folderParentPath) {
		this.folderParentPath = folderParentPath;
	}

	public int getFolderFileNumber() {
		return folderFileNumber;
	}

	public void setFolderFileNumber(int folderFileNumber) {
		this.folderFileNumber = folderFileNumber;
	}

	public Date getFolderCreatedDate() {
		return folderCreatedDate;
	}

	public void setFolderCreatedDate(Date folderCreatedDate) {
		this.folderCreatedDate = folderCreatedDate;
	}

	public String getFolderPrefixKey() {
		return folderPrefixKey;
	}

	public void setFolderPrefixKey(String folderPrefixKey) {
		this.folderPrefixKey = folderPrefixKey;
	}
}
