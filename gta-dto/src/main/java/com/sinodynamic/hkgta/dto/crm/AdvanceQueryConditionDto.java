package com.sinodynamic.hkgta.dto.crm;

import java.util.List;

import com.sinodynamic.hkgta.entity.crm.SysCode;

public class AdvanceQueryConditionDto {
	private String displayName;
	private String columnName;
	private String columnType;
	private String selectName;
	private Integer displayOrder;

	private List<SysCode> options;


	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	
	public String getSelectName() {
		return selectName;
	}

	public void setSelectName(String selectName) {
		this.selectName = selectName;
	}

	public List<SysCode> getOptions() {
		return options;
	}

	public void setOptions(List<SysCode> options) {
		this.options = options;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public AdvanceQueryConditionDto(String displayName, String columnName, String columnType, String selectName,Integer displayOrder) {
		super();
		this.displayName = displayName;
		this.columnName = columnName;
		this.columnType = columnType;
		this.selectName = selectName;
		this.displayOrder = displayOrder;
	}
	public AdvanceQueryConditionDto(String displayName, String columnName, String columnType, List<SysCode> selectList,Integer displayOrder) {
		super();
		this.displayName = displayName;
		this.columnName = columnName;
		this.columnType = columnType;
		this.options = selectList;
		this.displayOrder = displayOrder;
	}

	public AdvanceQueryConditionDto() {
		super();
	}
	
}
