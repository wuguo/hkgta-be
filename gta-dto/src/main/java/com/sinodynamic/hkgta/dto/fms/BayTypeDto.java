package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;


/**
 * @author Kevin_Liang
 *
 */
public class BayTypeDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7971315640737297629L;
	private String id;
	private String value;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public BayTypeDto()
	{}
	
	public BayTypeDto(String id)
	{
		this.id = id;
	}
	
	@Override
	public int hashCode()
	{
		return id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (obj == null)
		{
			return false;
		}

		if (obj != null && obj instanceof BayTypeDto)
		{
			if (((BayTypeDto) obj).id.equals(this.id))
				return true;
		}

		return false;
	}

}
