package com.sinodynamic.hkgta.dto.crm;

import java.util.Date;

public class AdvertiseImageDto {

	private Long imgId;
	private String filepath;
	private String parentFilepath;
	private String appType; // MAPP or MPORT
	private String dispLoc; // MSCR or TOPUP
	private String status;
	private Long dispOrder;
	private Long linkLevel;
	private String description;
	private String createBy;
	private Date createDate;
	private String updateBy;
	private Date updateDate;
	
	public Long getImgId() {
		return imgId;
	}

	public void setImgId(Long imgId) {
		this.imgId = imgId;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getDispLoc() {
		return dispLoc;
	}

	public void setDispLoc(String dispLoc) {
		this.dispLoc = dispLoc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	

	public Long getDispOrder() {
		return dispOrder;
	}

	public void setDispOrder(Long dispOrder) {
		this.dispOrder = dispOrder;
	}

	public Long getLinkLevel() {
		return linkLevel;
	}

	public void setLinkLevel(Long linkLevel) {
		this.linkLevel = linkLevel;
	}

	public String getParentFilepath()
	{
		return parentFilepath;
	}

	public void setParentFilepath(String parentFilepath)
	{
		this.parentFilepath = parentFilepath;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

}
