package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class FieldChangeDto implements Serializable {
	private String fieldName;
	
	private String originalValue;
	
	private String newValue;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getOriginalValue() {
		if(StringUtils.isEmpty(originalValue)){
			originalValue="-";
		}
		return originalValue;
	}

	public void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}

	public String getNewValue() {
		if(StringUtils.isEmpty(newValue)){
			newValue="-";
		}
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	
}
