package com.sinodynamic.hkgta.dto.membership;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

/**
 * 
 * @author Kaster 20160328
 *
 */
public class MemberQuickSearchDto extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EncryptFieldInfo
	private Long customerId;
	
	private String salutation;
	
	private String givenName;
	
	private String surname;
	
	private String memberType;
	
	private String memberName;
	
	private String academyNo;
	
	private String status;
	
	/***
	 * CR46 staff Preferred Payment Method
	 */
	private List<PaymentFacilityDto>paymentMethods;
	
	
	public List<PaymentFacilityDto> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(List<PaymentFacilityDto> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
