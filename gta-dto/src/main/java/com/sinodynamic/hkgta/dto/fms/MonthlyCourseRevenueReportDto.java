package com.sinodynamic.hkgta.dto.fms;

import com.sinodynamic.hkgta.dto.CustomDateDeserialize;
import com.sinodynamic.hkgta.dto.CustomDateSerializer;
import com.sinodynamic.hkgta.dto.CustomDateTimeSerializer;
import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Mason_Yang on 12/25/2015.
 */
public class MonthlyCourseRevenueReportDto {

    @JsonSerialize(using = CustomDateSerializer.class)
    private Date transactionDate;

    private Long transactionId;

    private Long reservationId;

    private Date reservationDate;

    private String academyId;

    private Long courseId;

    private String courseName;

    private String courseType;

    private String patronName;

    private BigDecimal orderTotalAmount;

    private String paymentMethodCode;

    private String paymentMedia;

    private String location;

    private String status;

    private Long qty;

    private BigDecimal transAmount;

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Object transactionId) {
        this.transactionId = (transactionId != null ? NumberUtils.toLong(transactionId.toString()) : null);
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Object reservationId) {
        this.reservationId = (reservationId != null ? NumberUtils.toLong(reservationId.toString()) : null);
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getAcademyId() {
        return academyId;
    }

    public void setAcademyId(String academyId) {
        this.academyId = academyId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Object courseId) {
        this.courseId = (courseId != null ? NumberUtils.toLong(courseId.toString()) : null);
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getPatronName() {
        return patronName;
    }

    public void setPatronName(String patronName) {
        this.patronName = patronName;
    }

    public BigDecimal getOrderTotalAmount() {
        return orderTotalAmount;
    }

    public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
        this.orderTotalAmount = orderTotalAmount;
    }

    public String getPaymentMethodCode() {
        return paymentMethodCode;
    }

    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    public String getPaymentMedia() {
        return paymentMedia;
    }

    public void setPaymentMedia(String paymentMedia) {
        this.paymentMedia = paymentMedia;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Object qty) {
        this.qty = (qty != null ? NumberUtils.toLong(qty.toString()) : null);
    }

    public BigDecimal getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(BigDecimal transAmount) {
        this.transAmount = transAmount;
    }
}