package com.sinodynamic.hkgta.dto.statement;

import java.io.Serializable;

public class SearchStatementsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String sortBy;
	private Integer pageNumber;
	private Integer pageSize;
	private String isAscending;
	private String month;
	private String year;
	private String memberType;
	private String deliveryStatus;
	public SearchStatementsDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SearchStatementsDto(String sortBy, Integer pageNumber, Integer pageSize, String isAscending, String month,
			String year, String memberType,String deliveryStatus) {
		super();
		this.sortBy = sortBy;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.isAscending = isAscending;
		this.month = month;
		this.year = year;
		this.memberType = memberType;
		this.deliveryStatus = deliveryStatus;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
	
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getIsAscending() {
		return isAscending;
	}
	public void setIsAscending(String isAscending) {
		this.isAscending = isAscending;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	
	
}
