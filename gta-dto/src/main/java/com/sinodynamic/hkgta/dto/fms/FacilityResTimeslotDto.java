package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class FacilityResTimeslotDto implements Serializable {

	private static final long serialVersionUID = 430887760348794557L;

	private Integer facilityNo;
	private String status;
	// auto trun on or off ballFeedingMachine
	private String startTime;// 7:00 or 8:00

	private String endTime; //

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Integer facilityNo) {
		this.facilityNo = facilityNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
