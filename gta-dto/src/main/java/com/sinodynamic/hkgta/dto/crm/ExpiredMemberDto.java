package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;



/**
 * @author Junfeng_Yan
 * @since 2015/4/30
 *
 */
public class ExpiredMemberDto extends GenericDto implements Serializable{

	
	private String userId;
	
	@EncryptFieldInfo
	private BigInteger customerId;
	
//	private String surName;
//	
//	private String givenName;
//	
//	private String gender;
	
	private String customerName;
	
	private Boolean isRemarkIconShow;
	
	private String contactEmail;
	
	private String expiryDate;
	
	private String currentPlanName;
	
	private String newPlanName;
	
	private Long orderNo;
	

	public ExpiredMemberDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getUserId() {
		return userId;
	}




	public void setUserId(String userId) {
		this.userId = userId;
	}




	public BigInteger getCustomerId() {
		return customerId;
	}




	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public Boolean getIsRemarkIconShow() {
		return isRemarkIconShow;
	}


	public void setIsRemarkIconShow(Boolean isRemarkIconShow) {
		this.isRemarkIconShow = isRemarkIconShow;
	}


	public String getContactEmail() {
		return contactEmail;
	}


	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}


	public String getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}


	public String getCurrentPlanName() {
		return currentPlanName;
	}


	public void setCurrentPlanName(String currentPlanName) {
		this.currentPlanName = currentPlanName;
	}


	public String getNewPlanName() {
		return newPlanName;
	}


	public void setNewPlanName(String newPlanName) {
		this.newPlanName = newPlanName;
	}


	public Long getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(Object orderNo) {
		if(orderNo instanceof BigInteger){
			this.orderNo = ((BigInteger) orderNo).longValue();
		}else if(orderNo instanceof Integer){
			this.orderNo = ((Integer) orderNo).longValue();
		}else if(orderNo instanceof String){
			this.orderNo = Long.valueOf((String) orderNo);
		}else{
			this.orderNo = (Long) orderNo;
		}
	}


	@Override
	public String toString() {
		return "ExpiredMemberDto [userId=" + userId + ", customerId="
				+ customerId + ", customerName=" + customerName
				+ ", isRemarkIconShow=" + isRemarkIconShow + ", contactEmail="
				+ contactEmail + ", expiryDate=" + expiryDate
				+ ", currentPlanName=" + currentPlanName + ", newPlanName="
				+ newPlanName + ", orderNo="+orderNo+"]";
	}

}
