package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class StaffCardMasterDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	public StaffCardMasterDto() {
		super();
	}

	
	public StaffCardMasterDto(String fromDate, String toDate, String status, String staffId, int pageNumber, int pageSize) {
		super();
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.status = status;
		this.setPageNumber(pageNumber);
		this.setPageSize(pageSize);
		this.staffId = staffId;
		
	}

	private String staffId;
	private String status;
	private String updateBy;
	private String cardTypeName;
	private String cardNo;

	
	//for list query
	private String fromDate;
	private int pageNumber;
	private int pageSize;
	
	public String getFromDate() {
		return fromDate;
	}


	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	public String getToDate() {
		return toDate;
	}


	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	private String toDate;
	
	

	public String getStaffId() {
		return staffId;
	}


	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}


	public int getPageNumber() {
		return pageNumber;
	}


	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}


	public int getPageSize() {
		return pageSize;
	}


	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
