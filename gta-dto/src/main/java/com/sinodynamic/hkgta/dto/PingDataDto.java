package com.sinodynamic.hkgta.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PingDataDto implements Serializable {
	public class Drilldown{
		private String name;
		private String[]categories;
		private BigDecimal[]data;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String[] getCategories() {
			return categories;
		}
		public void setCategories(String[] categories) {
			this.categories = categories;
		}
		public BigDecimal[] getData() {
			return data;
		}
		public void setData(BigDecimal[] data) {
			this.data = data;
		}
	}

	private BigDecimal y;
	
	private Drilldown drilldown;

	public BigDecimal getY() {
		return y;
	}

	public void setY(BigDecimal y) {
		this.y = y;
	}

	public Drilldown getDrilldown() {
		return drilldown;
	}

	public void setDrilldown(Drilldown drilldown) {
		this.drilldown = drilldown;
	}
	
}
