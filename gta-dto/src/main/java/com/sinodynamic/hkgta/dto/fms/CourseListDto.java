package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class CourseListDto implements Serializable {

	private static final long serialVersionUID = -1371912768347486929L;

	public CourseListDto() {
		super();

	}

	public CourseListDto(String courseType, String expired, String status) {
		this.courseType = courseType;
		this.expired = expired;
		this.status = status;
	}

	private Long courseId;
	private String courseType;
	private String courseName;
	private Date registBeginDate;
	private Date registDueDate;
	private Date createDate;
	private String status;
	private String expired;
	private Long enrollment;
	private Long capacity;
	private Date periodStart;
	private Date periodEnd;
	private List<CourseSessionDetailsDto> sessions;
	private BigDecimal price;
	private String memberAcceptance;
	private String memberAcceptanceValue;
	private String updateBy;
	private Date updateDate;
	private String ageRangeCode;
	private String ageRangeDescription;
	private String courseDescription;
	private Long sessionCount;
	private String posterFilename;
	private String enrollStatus;
	private String cancelBy;
	private String cancelRemark;
	private String openEnroll;
	private Integer    noOfSession;
	
	
	public String getMemberAcceptanceValue() {
		return memberAcceptanceValue;
	}

	public void setMemberAcceptanceValue(String memberAcceptanceValue) {
		this.memberAcceptanceValue = memberAcceptanceValue;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Object courseId) {
		this.courseId = (courseId!=null ? NumberUtils.toLong(courseId.toString()):null);
	}

	public String getCourseType() {
		return courseType;
	}

	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getRegistBeginDate() {
		return DtoHelper.getYMDDate(registBeginDate);
	}

	public void setRegistBeginDate(Date registBeginDate) {
		this.registBeginDate = registBeginDate;
	}

	public String getRegistDueDate() {
		return DtoHelper.getYMDDate(registDueDate);
	}

	public void setRegistDueDate(Date registDueDate) {
		this.registDueDate = registDueDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExpired() {
		return expired;
	}

	public void setExpired(String expired) {
		this.expired = expired;
	}

	public Long getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Object enrollment) {
		this.enrollment = (enrollment!=null ? NumberUtils.toLong(enrollment.toString()):null);
	}

	public Long getCapacity() {
		return capacity;
	}

	public void setCapacity(Object capacity) {
		this.capacity = (capacity!=null ? NumberUtils.toLong(capacity.toString()):null);
	}

	public String getPeriodStart() {
		return DtoHelper.getYMDDate(periodStart);

	}

	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}

	public String getPeriodEnd() {
		return DtoHelper.getYMDDate(periodEnd);
	}

	public void setPeriodEnd(Date periodEnd) {
		this.periodEnd = periodEnd;
	}

	public List<CourseSessionDetailsDto> getSessions() {
		return sessions;
	}

	public void setSessions(List<CourseSessionDetailsDto> sessions) {
		List<CourseSessionDetailsDto> listDto = new ArrayList<CourseSessionDetailsDto>();
		if (sessions.size() > 0) {//将Roll Call记录状态转换为客户端显示状态    TODO 优化性能时，此操作应该放客户端操作
			for (CourseSessionDetailsDto dto : sessions) {
				listDto.add(dto.getAttendingStatusCourseSessionDetailsDto(dto));
			}
		}
		this.sessions = listDto;
	}
	/**
	 * 根据实际情况看是否需要转换显示状态
	 * @param sessions
	 * @param isFor
	 */
	public void setSessions(List<CourseSessionDetailsDto> sessions, boolean isFor) {
		
		if (!isFor) {
			this.sessions = sessions;
		} else {
			List<CourseSessionDetailsDto> listDto = new ArrayList<CourseSessionDetailsDto>();
			if (sessions.size() > 0) {//将Roll Call记录状态转换为客户端显示状态    TODO 优化性能时，此操作应该放客户端操作
				for (CourseSessionDetailsDto dto : sessions) {
					listDto.add(dto.getAttendingStatusCourseSessionDetailsDto(dto));
				}
			}
			this.sessions = listDto;
		}
		
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getCreateDate() {
		return DtoHelper.getYMDDateAndDateDiff(createDate);
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getMemberAcceptance() {
		return memberAcceptance;
	}

	public void setMemberAcceptance(String memberAcceptance) {
		this.memberAcceptance = memberAcceptance;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getAgeRangeCode() {
		return ageRangeCode;
	}

	public void setAgeRangeCode(String ageRangeCode) {
		this.ageRangeCode = ageRangeCode;
	}

	public String getCourseDescription() {
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

	public Long getSessionCount() {
		return sessionCount;
	}

	public void setSessionCount(Object sessionCount) {
		this.sessionCount = (sessionCount!=null ? NumberUtils.toLong(sessionCount.toString()):null);
	}

	public String getPosterFilename() {
		return posterFilename;
	}

	public void setPosterFilename(String posterFilename) {
		this.posterFilename = posterFilename;
	}

	public String getEnrollStatus() {
		return enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}
	
	public String getCancelBy() {
		return cancelBy;
	}

	public void setCancelBy(String cancelBy) {
		this.cancelBy = cancelBy;
	}

	public String getCancelRemark() {
		return cancelRemark;
	}

	public void setCancelRemark(String cancelRemark) {
		this.cancelRemark = cancelRemark;
	}

	public String getAgeRangeDescription() {
		return ageRangeDescription;
	}

	public void setAgeRangeDescription(String ageRangeDescription) {
		this.ageRangeDescription = ageRangeDescription;
	}

	public String getOpenEnroll() {
		return openEnroll;
	}

	public void setOpenEnroll(String openEnroll) {
		this.openEnroll = openEnroll == null ? "N" : openEnroll;
	}

	public Integer getNoOfSession() {
		return noOfSession;
	}

	public void setNoOfSession(Integer noOfSession) {
		this.noOfSession = noOfSession;
	}
	

}
