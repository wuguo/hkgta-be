package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class ActivateMemberDto extends GenericDto implements Serializable{

	private static final long serialVersionUID = 1L;
	@EncryptFieldInfo
	private Long customerId;
	
	private String loginId;
	
	private String academyNo;
	
	private Date activationDate;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = customerId == null ? 0l : Long.parseLong(customerId.toString());
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}
	
}
