package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class CashValueLogDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date transactionDate;
	private Long orderNo;
	private Date orderDate;
	private String academyId;
	private String memberName;
	private String servicePlan;
	private String salesman;
	private String enrollType;
	private String paymentMethod;
	private String paymentMedia;
	private String location;
	private String orderStatus;
	private String transStatus;
	private String createBy;
	private String updateBy;
	private Date updateDate;
	private String auditBy;
	private Date auditDate;
	private Long qty;
	private BigDecimal transAmount;
	private Long transactionId;
	private String adjustmentType;
	private String memberType;
	private Date planStartDate;
	private Date planEndDate;
	private Integer contractLength;
	private String remark;
	private BigDecimal creditLimit;
	private BigDecimal servicePlanPrice;
	private String agentTransactionNo;   				//第三方系统订单号
	
	private String transactionDateDes;
	private String transAmountStr;
	private String transactionDateStr;
	
	private String invoiceNo;
	
	
	
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
	public String getTransactionDateStr() {
		return transactionDateStr;
	}
	public void setTransactionDateStr(String transactionDateStr) {
		this.transactionDateStr = transactionDateStr;
	}
	public String getTransactionDateDes() {
		if(null!=transactionDate)
		{
			transactionDateDes=DtoHelper.getYMDDHHMMateAndDateDiff(transactionDate);
		}
		return transactionDateDes;
	}
	public String getTransAmountStr() {
		return transAmountStr;
	}
	public void setTransAmountStr(String transAmountStr) {
		this.transAmountStr = transAmountStr;
	}
	public void setTransactionDateDes(String transactionDateDes) {
		this.transactionDateDes = transactionDateDes;
	}
	public String getAgentTransactionNo() {
		return agentTransactionNo;
	}
	public void setAgentTransactionNo(String agentTransactionNo) {
		this.agentTransactionNo = agentTransactionNo;
	}
	public String getTransactionDate() {
		return DtoHelper.date2String(transactionDate,"yyyy-MM-dd HH:mm");
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Object transactionId) {
		if(transactionId instanceof BigInteger){
			this.transactionId = ((BigInteger) transactionId).longValue();
		}else{
			this.transactionId = (Long) transactionId;
		}
	}
	public Long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Object orderNo) {
		if(orderNo instanceof BigInteger){
			this.orderNo = ((BigInteger) orderNo).longValue();
		}else{
			this.orderNo = (Long) orderNo;
		}
	}
	public String getOrderDate() {
		return DtoHelper.date2String(orderDate,"yyyy-MM-dd");
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getAcademyId() {
		return academyId;
	}
	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getServicePlan() {
		return servicePlan;
	}
	public void setServicePlan(String servicePlan) {
		this.servicePlan = servicePlan;
	}
	public String getSalesman() {
		return salesman;
	}
	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}
	public String getEnrollType() {
		return enrollType;
	}
	public void setEnrollType(String enrollType) {
		this.enrollType = enrollType;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPaymentMedia() {
		return paymentMedia;
	}
	public void setPaymentMedia(String paymentMedia) {
		this.paymentMedia = paymentMedia;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getTransStatus() {
		return transStatus;
	}
	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getUpdateDate() {
		return DtoHelper.date2String(updateDate,"yyyy-MM-dd HH:mm:ss");
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getAuditBy() {
		return auditBy;
	}
	public void setAuditBy(String auditBy) {
		this.auditBy = auditBy;
	}
	public String getAuditDate() {
		return DtoHelper.date2String(auditDate,"yyyy-MM-dd HH:mm:ss");
	}
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	public Long getQty() {
		return qty;
	}
	public void setQty(Object qty) {
		if(qty instanceof BigInteger){
			this.qty = ((BigInteger) qty).longValue();
		}else if(qty instanceof Integer){
			this.qty = ((Integer) qty).longValue();
		}else{
			this.qty = (Long) qty;
		}
	}
	public String getTransAmount() {
		DecimalFormat   df   = new   DecimalFormat("#.00");  
		return df.format(transAmount);
	}
	public void setTransAmount(BigDecimal transAmount) {
		this.transAmount = transAmount;
	}
	
	
	
	
	public String getAdjustmentType() {
		return adjustmentType;
	}
	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public Date getPlanStartDate() {
		return planStartDate;
	}
	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}
	public Date getPlanEndDate() {
		return planEndDate;
	}
	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}
	public Integer getContractLength() {
		return contractLength;
	}
	public void setContractLength(Integer contractLength) {
		this.contractLength = contractLength;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}
	public BigDecimal getServicePlanPrice() {
		return servicePlanPrice;
	}
	public void setServicePlanPrice(BigDecimal servicePlanPrice) {
		this.servicePlanPrice = servicePlanPrice;
	}
	
	
}
