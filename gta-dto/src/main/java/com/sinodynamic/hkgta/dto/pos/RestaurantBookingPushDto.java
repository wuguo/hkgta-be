package com.sinodynamic.hkgta.dto.pos;

public class RestaurantBookingPushDto {
	
	private String pushType;

	private String defaultTitle;
	
	private String popUpTitle;
	
	private String memberName;
	
	private String bookingTime;
	
	private long partySize;

	public String getPushType() {
		return pushType;
	}

	public void setPushType(String pushType) {
		this.pushType = pushType;
	}

	public String getDefaultTitle() {
		return defaultTitle;
	}

	public void setDefaultTitle(String defaultTitle) {
		this.defaultTitle = defaultTitle;
	}

	public String getPopUpTitle() {
		return popUpTitle;
	}

	public void setPopUpTitle(String popUpTitle) {
		this.popUpTitle = popUpTitle;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getBookingTime() {
		return bookingTime;
	}

	public void setBookingTime(String bookingTime) {
		this.bookingTime = bookingTime;
	}

	public long getPartySize() {
		return partySize;
	}

	public void setPartySize(long partySize) {
		this.partySize = partySize;
	}
}
