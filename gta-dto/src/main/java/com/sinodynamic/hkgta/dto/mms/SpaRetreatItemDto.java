package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class SpaRetreatItemDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long itemId;
	private String itemName;
	private String retreatName;
	private Long retId;
	private String status;
	private String description;
	
	public SpaRetreatItemDto()
	{
		
	}

	public Long getRetId() {
		return retId;
	}

	public void setRetId(Object retId) {
		this.retId = (retId!=null ? NumberUtils.toLong(retId.toString()):null);
	}


	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Object itemId) {
		this.itemId = (itemId!=null ? NumberUtils.toLong(itemId.toString()):null);
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getRetreatName() {
		return retreatName;
	}

	public void setRetreatName(String retreatName) {
		this.retreatName = retreatName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
