package com.sinodynamic.hkgta.dto.push;

import java.io.Serializable;

public class RegisterDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8307850430543139875L;
	private String token;
	private String lang;
	private String systemVersion;
	private String platform;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getSystemVersion() {
		return systemVersion;
	}
	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	

}
