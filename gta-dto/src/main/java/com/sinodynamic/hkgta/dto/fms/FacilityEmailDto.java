package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class FacilityEmailDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long resvId;
	private String emailTitle;
	private String emailBody;

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Long resvId) {
		this.resvId = resvId;
	}

	public String getEmailTitle() {
		return emailTitle;
	}

	public void setEmailTitle(String emailTitle) {
		this.emailTitle = emailTitle;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

}
