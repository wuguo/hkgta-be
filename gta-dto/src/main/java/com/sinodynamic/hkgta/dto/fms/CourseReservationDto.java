package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

/**
 * @date 7/14/2015
 * @author Mianping_Wu
 */
public class CourseReservationDto implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String enrollId;
    
    public String getEnrollId() {
        return enrollId;
    }
    
    public void setEnrollId(String enrollId) {
        this.enrollId = enrollId;
    }
    
}
