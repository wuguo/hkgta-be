package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

public class StaffMasterDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String userId;
	
	private String employDate;

	private String positionTitle;

	private String quitDate;
	
	private String staffNo;

	private String staffType;

	private String status;
	
	private String createBy;
	
	private String updateBy;
	
	private Long departId;

	public String getUserId()
	{
		return userId;
	}


	public void setUserId(String userId)
	{
		this.userId = userId;
	}


	public String getEmployDate()
	{
		return employDate;
	}

	public void setEmployDate(String employDate)
	{
		this.employDate = employDate;
	}


	public String getPositionTitle()
	{
		return positionTitle;
	}


	public void setPositionTitle(String positionTitle)
	{
		this.positionTitle = positionTitle;
	}


	public String getQuitDate()
	{
		return quitDate;
	}


	public void setQuitDate(String quitDate)
	{
		this.quitDate = quitDate;
	}


	public String getStaffNo()
	{
		return staffNo;
	}


	public void setStaffNo(String staffNo)
	{
		this.staffNo = staffNo;
	}


	public String getStaffType()
	{
		return staffType;
	}


	public void setStaffType(String staffType)
	{
		this.staffType = staffType;
	}


	public String getStatus()
	{
		return status;
	}


	public void setStatus(String status)
	{
		this.status = status;
	}


	public String getCreateBy()
	{
		return createBy;
	}


	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}


	public String getUpdateBy()
	{
		return updateBy;
	}


	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}


	public StaffMasterDto() {
	}


	public Long getDepartId()
	{
		return departId;
	}


	public void setDepartId(Long departId)
	{
		this.departId = departId;
	}


}
