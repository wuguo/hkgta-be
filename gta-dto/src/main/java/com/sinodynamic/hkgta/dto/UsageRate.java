package com.sinodynamic.hkgta.dto;

import java.io.Serializable;

public class UsageRate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/***
	 * bi show Web/iSO AND SO NO
	 */
	private String name;
	
	/***
	 * from 1 to 12 moth count 
	 */
	private Object[]data;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object[] getData() {
		return data;
	}

	public void setData(Object[] data) {
		this.data = data;
	}
}
