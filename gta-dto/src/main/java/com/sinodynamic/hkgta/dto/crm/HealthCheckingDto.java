package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

public class HealthCheckingDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String userId;
	
	private Date  lastAccessTime;
	
	private String appTypeCode;
	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getLastAccessTime() {
		return lastAccessTime;
	}

	public void setLastAccessTime(Date lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}

	public String getAppTypeCode() {
		return appTypeCode;
	}

	public void setAppTypeCode(String appTypeCode) {
		this.appTypeCode = appTypeCode;
	}

	public HealthCheckingDto()
	{}
	
//	public HealthCheckingDto(String userId, String userNo, String userName,
//			String userType, String fullName, String portraitPhoto, String dateOfBirth, String positionTitle, String firstName, String lastName, String memberType)
//	{
//		this.userId = userId;
//		
//	}

	
	
	

}