package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class MemberInfoDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public MemberInfoDto() {
		super();
	}

	public MemberInfoDto(Long customerId, String salutation, String surname, String givenName, String surnameNls, String givenNameNls,
			String gender, String phoneMobile, String phoneBusiness, String phoneHome,
			String contactEmail, String postalAddress1, String postalAddress2, String passportType, String passportNo,
			Date dateOfBirth, String nationality, String businessNature, String serviceAccCategory, String servicePlan, List<AdditionalInfoDto> additionalInfo){
		
	}

	public MemberInfoDto(BigInteger customerId) {
		super();
		this.customerId = customerId;
	}


	private BigInteger customerId;
	
	private String salutation;

	private String surname;

	private String givenName;

	private String surnameNls;

	private String givenNameNls;

	private Character gender;

	private String passportNo;

	private String passportType;

	private String nationality;

	private String dateOfBirth;

	private String phoneMobile;
	
	private String phoneBusiness;
	
	private String phoneHome;
	
	private String contactEmail;
	
	private String postalAddress1;
	
	private String postalAddress2;
	
	private String postalDistrict;

	private String portraitPhoto;

	private String signature;
	
	private String businessNature;

	private String serviceAccCategory;
    
    private String servicePlan;
    
    private List<AdditionalInfoDto> additionalInfo;

	
	public List<AdditionalInfoDto> getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(List<AdditionalInfoDto> additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getServicePlan() {
		return servicePlan;
	}

	public void setServicePlan(String servicePlan) {
		this.servicePlan = servicePlan;
	}

	public String getServiceAccCategory() {
		return serviceAccCategory;
	}

	public void setServiceAccCategory(String serviceAccCategory) {
		this.serviceAccCategory = serviceAccCategory;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigInteger getCustomerId() {
		return customerId;
	}

	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurnameNls() {
		return surnameNls;
	}

	public void setSurnameNls(String surnameNls) {
		this.surnameNls = surnameNls;
	}

	public String getGivenNameNls() {
		return givenNameNls;
	}

	public void setGivenNameNls(String givenNameNls) {
		this.givenNameNls = givenNameNls;
	}

	public Character getGender() {
		return gender;
	}

	public void setGender(Object gender) {
		if(gender instanceof Character){
			this.gender = (Character) gender;
		}else if(gender instanceof String){
			if(!StringUtils.isBlank((String) gender)){
				this.gender = ((String)gender).charAt(0);
			}else{
				this.gender = null;
			}
		}
		
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportType() {
		return passportType;
	}

	public void setPassportType(String passportType) {
		this.passportType = passportType;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneMobile() {
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public String getPhoneBusiness() {
		return phoneBusiness;
	}

	public void setPhoneBusiness(String phoneBusiness) {
		this.phoneBusiness = phoneBusiness;
	}

	public String getPhoneHome() {
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	
	public String getPostalDistrict() {
		return postalDistrict;
	}

	public void setPostalDistrict(String postalDistrict) {
		this.postalDistrict = postalDistrict;
	}

	public String getPortraitPhoto() {
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getBusinessNature() {
		return businessNature;
	}

	public void setBusinessNature(String businessNature) {
		this.businessNature = businessNature;
	}

	public String getPostalAddress1() {
		return postalAddress1;
	}

	public void setPostalAddress1(String postalAddress1) {
		this.postalAddress1 = postalAddress1;
	}

	public String getPostalAddress2() {
		return postalAddress2;
	}

	public void setPostalAddress2(String postalAddress2) {
		this.postalAddress2 = postalAddress2;
	}


}
