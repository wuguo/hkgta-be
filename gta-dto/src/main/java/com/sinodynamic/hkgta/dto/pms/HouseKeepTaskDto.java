package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.math.BigInteger;

/***
 * houseKeep report dto
 * 
 * @author christ
 *
 */

public class HouseKeepTaskDto implements Serializable {
	/* ad hoc task attribute **/
	private Long taskId;//TaskID
	private String assignedDate; //assigned Date
	private String roomNumber;// Room Number
	private String taskDetails; //Task Detail 
	private String respondTime; // Respond Time
	private String handlingStaff;//Handling Staff
	private String sbeaconDesc;// S-beacon detection
	
	/* ad routine  task**/
	private String taskStatus; // Task Status
	private Long originalTaskID; //Original Task Id
	
	private String arrivalTime; //ArrivalTime
	private String  arrivalStaff;// ArrivalStaff
	private String completeTime; //Complete Time
	private String completeStaff; //CompleteStaff
	private String ispCompleteTime; //Inspection CompleteTime
	private String ispStaff;//Inspection Staff
 
	/**maintenance task*/
	private String assignee;
	
	/*ad schedule task**/
	private String beginDate;//Task Begin Date
	
	private String expireDate; // Task Expire Date

	/*room assignment**/
	
	private String taskType;
	
	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public Long getTaskId() {
		return taskId;
	}
	
	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public void setTaskId(Object taskId) {
		if(null!=taskId){
			if(taskId instanceof BigInteger){
				this.taskId = ((BigInteger) taskId).longValue();
			}else if(taskId instanceof Integer){
				this.taskId = ((Integer) taskId).longValue();
			}else if(taskId instanceof String){
				this.taskId = Long.valueOf((String) taskId);
			}else{
				this.taskId = (Long) taskId;
			}
			
		}
	}

	public String getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(String assignedDate) {
		this.assignedDate = assignedDate;
	}
	
	public String getIspStaff() {
		return ispStaff;
	}

	public void setIspStaff(String ispStaff) {
		this.ispStaff = ispStaff;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getTaskDetails() {
		return taskDetails;
	}

	public void setTaskDetails(String taskDetails) {
		this.taskDetails = taskDetails;
	}

	public String getRespondTime() {
		return respondTime;
	}

	public void setRespondTime(String respondTime) {
		this.respondTime = respondTime;
	}

	public String getHandlingStaff() {
		return handlingStaff;
	}

	public void setHandlingStaff(String handlingStaff) {
		this.handlingStaff = handlingStaff;
	}

	public String getSbeaconDesc() {
		return sbeaconDesc;
	}

	public void setSbeaconDesc(String sbeaconDesc) {
		this.sbeaconDesc = sbeaconDesc;
	}
	
	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public Long getOriginalTaskID() {
		return originalTaskID;
	}

	public void setOriginalTaskID(Object originalTaskID) {
		if(null!=originalTaskID){
			if(originalTaskID instanceof BigInteger){
				this.originalTaskID = ((BigInteger) originalTaskID).longValue();
			}else if(originalTaskID instanceof Integer){
				this.originalTaskID = ((Integer) originalTaskID).longValue();
			}else if(originalTaskID instanceof String){
				this.originalTaskID = Long.valueOf((String) originalTaskID);
			}else{
				this.originalTaskID = (Long) originalTaskID;
			}
		}
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getArrivalStaff() {
		return arrivalStaff;
	}

	public void setArrivalStaff(String arrivalStaff) {
		this.arrivalStaff = arrivalStaff;
	}

	public String getCompleteTime() {
		return completeTime;
	}

	public void setCompleteTime(String completeTime) {
		this.completeTime = completeTime;
	}

	public String getCompleteStaff() {
		return completeStaff;
	}

	public void setCompleteStaff(String completeStaff) {
		this.completeStaff = completeStaff;
	}

	public String getIspCompleteTime() {
		return ispCompleteTime;
	}

	public void setIspCompleteTime(String ispCompleteTime) {
		this.ispCompleteTime = ispCompleteTime;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	
}
