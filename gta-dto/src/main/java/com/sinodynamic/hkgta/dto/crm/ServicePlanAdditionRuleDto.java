package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.entity.GenericDto;

public class ServicePlanAdditionRuleDto extends GenericDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private String inputValue;

	private Long planNo;

	private String rightCode;
	
	private Integer available;
	

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Object available) {
		this.available = Integer.valueOf(available.toString());
	}

	public String getInputValue() {
		return inputValue;
	}

	public void setInputValue(String inputValue) {
		this.inputValue = inputValue;
	}

	public Long getPlanNo() {
		return planNo;
	}

	public void setPlanNo(Object planNo) {
		this.planNo = Long.parseLong(planNo.toString());
	}

	public String getRightCode() {
		return rightCode;
	}

	public void setRightCode(String rightCode) {
		this.rightCode = rightCode;
	}

	
	
}
