package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class CourseTransactionDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String paymentDate;

    private Long transactionNo;

    private String media;

    private String paymentMethod;

    private String expense;

    private BigDecimal paidAmount;

    private Date transactionTimestamp;

    private String location;

    public String getPaymentDate() {
	return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
	this.paymentDate = paymentDate;
    }

    public Long getTransactionNo() {
	return transactionNo;
    }

    public void setTransactionNo(Object transactionNo) {
	this.transactionNo = (transactionNo!=null ? NumberUtils.toLong(transactionNo.toString()):null);
    }

    public String getExpense() {
	return expense;
    }

    public void setExpense(String expense) {
	this.expense = expense;
    }

    public BigDecimal getPaidAmount() {
	return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
	this.paidAmount = paidAmount;
    }

    public Date getTransactionTimestamp() {
	return transactionTimestamp;
    }

    public void setTransactionTimestamp(Date transactionTimestamp) {
	this.transactionTimestamp = transactionTimestamp;
    }

    public String getMedia() {
	return media;
    }

    public void setMedia(String media) {
	this.media = media;
    }

    public String getPaymentMethod() {
	return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
	this.paymentMethod = paymentMethod;
    }

    public String getLocation() {
	return location;
    }

    public void setLocation(String location) {
	this.location = location;
    }

}
