package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class PatronAccountStatementDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date transactionDate;
	private Long invoiceNo;
	private String category;
	private String description;
	private String paymentType;
	private String memberName;
	private BigDecimal amount;
	private BigDecimal cashvalueBalance;
	private String itemNo;
	private String remark;
	private String status;

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(Object invoiceNo) {
		if (invoiceNo instanceof BigInteger) {
			this.invoiceNo = ((BigInteger) invoiceNo).longValue();
		} else if (invoiceNo instanceof Integer) {
			this.invoiceNo = ((Integer) invoiceNo).longValue();
		} else if (invoiceNo instanceof String) {
			this.invoiceNo = Long.valueOf((String) invoiceNo);
		} else {
			this.invoiceNo = (Long) invoiceNo;
		}
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCashvalueBalance() {
		return cashvalueBalance;
	}

	public void setCashvalueBalance(BigDecimal cashvalueBalance) {
		this.cashvalueBalance = cashvalueBalance;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
