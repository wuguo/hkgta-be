package com.sinodynamic.hkgta.dto;

import java.util.Date;

/**
 * Created by Mason_Yang on 1/5/2016.
 */
public class EntityHistoryDto {

    private String createBy;
    private Date createDate;
    private String updateBy;
    private Date updateDate;

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setThis(Object o) {

    }
}
