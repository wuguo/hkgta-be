package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.List;

public class FacilitySubtypesDto implements Serializable {

	private static final long serialVersionUID = -8266990731456498621L;

	private Long advancedResPeriod;
	private List<FacilitySubtypeDto> priceList;

	private String updateBy;
	
	public Long getAdvancedResPeriod() {
		return advancedResPeriod;
	}

	public void setAdvancedResPeriod(Long advancedResPeriod) {
		this.advancedResPeriod = advancedResPeriod;
	}

	public List<FacilitySubtypeDto> getPriceList() {
		return priceList;
	}

	public void setPriceList(List<FacilitySubtypeDto> priceList) {
		this.priceList = priceList;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

}
