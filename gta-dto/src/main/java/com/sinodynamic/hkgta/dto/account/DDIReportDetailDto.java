package com.sinodynamic.hkgta.dto.account;

public class DDIReportDetailDto {

	private String seqNo;

	private String ebswCorpId;
	private String batchNumber;
	private String transactionSeq;

	private String debtorBankCode;
	private String debtorBankBranchCode;
	private String debtorAccountNumber;

	private String debtorName;
	private String amount; // with 6 decimal point (HKD 1.00 >> 1000000)

	private String debtorReference; // customer ID
	private String valueDate;

	private String crAccountCode;
	private String particular;

	private String returnStatusCode;
	private String returnDate;
	private String returnReason;

	private String ebswBranchCode;

	private String patronId;

	private String patronName;

	private String gtaStatus;

	private String gtaRemark;

	public String getGtaStatus() {
		return gtaStatus;
	}

	public void setGtaStatus(String gtaStatus) {
		this.gtaStatus = gtaStatus;
	}

	public String getGtaRemark() {
		return gtaRemark;
	}

	public void setGtaRemark(String gtaRemark) {
		this.gtaRemark = gtaRemark;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getPatronId() {
		return patronId;
	}

	public void setPatronId(String patronId) {
		this.patronId = patronId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getReturnStatusCode() {
		return returnStatusCode;
	}

	public void setReturnStatusCode(String returnStatusCode) {
		this.returnStatusCode = returnStatusCode;
	}

	public String getEbswCorpId() {
		return ebswCorpId;
	}

	public void setEbswCorpId(String ebswCorpId) {
		this.ebswCorpId = ebswCorpId;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getTransactionSeq() {
		return transactionSeq;
	}

	public void setTransactionSeq(String transactionSeq) {
		this.transactionSeq = transactionSeq;
	}

	public String getDebtorBankCode() {
		return debtorBankCode;
	}

	public void setDebtorBankCode(String debtorBankCode) {
		this.debtorBankCode = debtorBankCode;
	}

	public String getDebtorBankBranchCode() {
		return debtorBankBranchCode;
	}

	public void setDebtorBankBranchCode(String debtorBankBranchCode) {
		this.debtorBankBranchCode = debtorBankBranchCode;
	}

	public String getDebtorAccountNumber() {
		return debtorAccountNumber;
	}

	public void setDebtorAccountNumber(String debtorAccountNumber) {
		this.debtorAccountNumber = debtorAccountNumber;
	}

	public String getDebtorName() {
		return debtorName;
	}

	public void setDebtorName(String debtorName) {
		this.debtorName = debtorName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDebtorReference() {
		return debtorReference;
	}

	public void setDebtorReference(String debtorReference) {
		this.debtorReference = debtorReference;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public String getCrAccountCode() {
		return crAccountCode;
	}

	public void setCrAccountCode(String crAccountCode) {
		this.crAccountCode = crAccountCode;
	}

	public String getParticular() {
		return particular;
	}

	public void setParticular(String particular) {
		this.particular = particular;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public String getEbswBranchCode() {
		return ebswBranchCode;
	}

	public void setEbswBranchCode(String ebswBranchCode) {
		this.ebswBranchCode = ebswBranchCode;
	}

}
