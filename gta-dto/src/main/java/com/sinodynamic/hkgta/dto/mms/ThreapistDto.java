package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;

public class ThreapistDto implements Serializable {
	private String therapistid;
	private String timeslot;
	private String therapistCode;
	private boolean isAvailable;    //是否可用

	public String getTherapistCode() {
		return therapistCode;
	}

	public void setTherapistCode(String therapistCode) {
		this.therapistCode = therapistCode;
	}

	public String getTherapistid() {
		return therapistid;
	}

	public void setTherapistid(String therapistid) {
		this.therapistid = therapistid;
	}

	public String getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}

	public boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
}
