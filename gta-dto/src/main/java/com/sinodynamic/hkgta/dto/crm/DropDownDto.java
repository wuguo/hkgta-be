package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

/**
 * 
 * @author Mianping_Wu
 * @since 5/4/2015
 */
public class DropDownDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codeValue;
	private String codeDisplay;
	private String codeDisplayTC;

	public String getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public String getCodeDisplay() {
		return codeDisplay;
	}

	public void setCodeDisplay(String codeDisplay) {
		this.codeDisplay = codeDisplay;
	}

	public String getCodeDisplayTC() {
		return codeDisplayTC;
	}

	public void setCodeDisplayTC(String codeDisplayTC) {
		this.codeDisplayTC = codeDisplayTC;
	}

}
