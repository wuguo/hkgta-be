package com.sinodynamic.hkgta.dto.pms;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class StaffRosterWeeklyPresetDto {

	private String presetName;

	private List<StaffRosterWeeklyPresetRecordDto> list;

	public String getPresetName() {
		return presetName;
	}

	public void setPresetName(String presetName) {
		this.presetName = presetName;
	}

	public List<StaffRosterWeeklyPresetRecordDto> getList() {
		return list;
	}

	public void setList(List<StaffRosterWeeklyPresetRecordDto> list) {
		this.list = list;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
