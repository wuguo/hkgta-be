package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

public class FoodItemDto implements Serializable{
	
	private String id;
	
	private String desc;
	
	private String price;
	
	

	public FoodItemDto() {
		super();
	}

	
	
	public FoodItemDto(String id, String desc, String price) {
		super();
		this.id = id;
		this.desc = desc;
		this.price = price;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}



	@Override
	public String toString() {
		return "FoodItemDto [id=" + id + ", desc=" + desc + ", price=" + price
				+ "]";
	}
	
	

}
