package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class CustomerServiceAccDto implements Serializable{
	
	private BigInteger customerId;  
	
	private Date effectiveDate;
	
	private String expiryDate;


	public BigInteger getCustomerId() {
		return customerId;
	}

	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "CustomerServiceAccDto [customerId=" + customerId
				+ ", effectiveDate=" + effectiveDate + ", expiryDate="
				+ expiryDate + "]";
	}
	
}
