package com.sinodynamic.hkgta.dto.pms;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class StaffRosterRecordDto {

	private String onDate;

	private boolean offDuty;

	private List<StaffRosterTimeDto> timeList;

	public String getOnDate() {
		return onDate;
	}

	public void setOnDate(String onDate) {
		this.onDate = onDate;
	}

	public boolean getOffDuty() {
		return offDuty;
	}

	public void setOffDuty(boolean offDuty) {
		this.offDuty = offDuty;
	}

	public List<StaffRosterTimeDto> getTimeList() {
		return timeList;
	}

	public void setTimeList(List<StaffRosterTimeDto> timeList) {
		this.timeList = timeList;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
