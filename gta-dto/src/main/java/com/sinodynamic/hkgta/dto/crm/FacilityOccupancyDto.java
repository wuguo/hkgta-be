package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

public class FacilityOccupancyDto implements Serializable {
	
	private String facilityType;
	
	private String occupancy;
	
	private String description;

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getOccupancy() {
		return occupancy;
	}

	public void setOccupancy(String occupancy) {
		this.occupancy = occupancy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
