package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class PushMessageDto implements Serializable {

	private static final long serialVersionUID = -7690999977773940514L;
	private Long taskId;

	

	public Long getTaskId() {
		return taskId;
	}



	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}



	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
