package com.sinodynamic.hkgta.dto.statement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class RefundItemDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long transactionNo;
	private BigDecimal paidAmount;
	private Date transactionTimestamp;
	public Long getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(Object transactionNo) {
		if(transactionNo instanceof BigInteger){
			this.transactionNo = ((BigInteger) transactionNo).longValue();
		}else if(transactionNo instanceof Integer){
			this.transactionNo = ((Integer) transactionNo).longValue();
		}else if(transactionNo instanceof String){
			this.transactionNo = Long.valueOf((String) transactionNo);
		}else{
			this.transactionNo = (Long) transactionNo;
		}
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Date getTransactionTimestamp() {
		return transactionTimestamp;
	}
	public void setTransactionTimestamp(Date transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}
	
	

}
