package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class CashValueSumDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private BigDecimal debitTotal;  //减数总和
	private BigDecimal creditTotal; //加数总和
	private BigDecimal total = new BigDecimal(0);		//加减总和
	
	public CashValueSumDto(){
		
	}
	public CashValueSumDto(CashValueSumDto dto){
		setDebitTotal(dto.getDebitTotalByNum());
		setCreditTotal(dto.getCreditTotalByNum());
		setTotal(total);
	}
	public String getDebitTotal() {
		return debitTotal.toString();
	}
	public BigDecimal getDebitTotalByNum() {
		return debitTotal;
	}
	public void setDebitTotal(BigDecimal debitTotal) {
		if (null == debitTotal) {
			this.debitTotal = new BigDecimal(0);
		} else {
			this.debitTotal = debitTotal;
		}
		
	}
	public String getCreditTotal() {
		return creditTotal.toString();
	}
	public BigDecimal getCreditTotalByNum() {
		return creditTotal;
	}
	public void setCreditTotal(BigDecimal creditTotal) {
		if (null == creditTotal) {
			this.creditTotal = new BigDecimal(0);
		} else {
			this.creditTotal = creditTotal;
		}
	}
	public String getTotal() {
		this.total = this.creditTotal.subtract(this.debitTotal).setScale(2, BigDecimal.ROUND_HALF_UP);
//		DecimalFormat   df   = new   DecimalFormat("#.00");  
		return total.toString();
	}
	public void setTotal(BigDecimal total) {
		if (null == total) {
			this.total = new BigDecimal(0);
		} else {
			this.total = this.creditTotal.subtract(this.debitTotal).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}
	
	
	
	
}
