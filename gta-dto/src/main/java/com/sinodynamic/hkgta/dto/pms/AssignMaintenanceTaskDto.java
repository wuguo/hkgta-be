package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class AssignMaintenanceTaskDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7690999977773940514L;
	private Long roomId;
	private String taskDescription;
	private String[] taskFiles;

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public String[] getTaskFiles() {
		return taskFiles;
	}

	public void setTaskFiles(String[] taskFiles) {
		this.taskFiles = taskFiles;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
