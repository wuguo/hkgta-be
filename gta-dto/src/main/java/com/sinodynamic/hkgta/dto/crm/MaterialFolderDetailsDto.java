package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class MaterialFolderDetailsDto implements Serializable{
               
	   private Long materialId;
	   
	   private String materialFilename;
	   
	   private String status;
	   
	   private String thumbnail;

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public String getMaterialFilename() {
		return materialFilename;
	}

	public void setMaterialFilename(String materialFilename) {
		this.materialFilename = materialFilename;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	
}
