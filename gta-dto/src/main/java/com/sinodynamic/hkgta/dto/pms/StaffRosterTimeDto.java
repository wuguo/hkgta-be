package com.sinodynamic.hkgta.dto.pms;

import org.apache.commons.lang.builder.ToStringBuilder;

public class StaffRosterTimeDto {

	private Long beginTime;

	private Long endTime;

	public Long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Long beginTime) {
		this.beginTime = beginTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
