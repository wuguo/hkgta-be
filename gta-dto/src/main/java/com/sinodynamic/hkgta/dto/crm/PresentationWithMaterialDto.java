package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.List;


public class PresentationWithMaterialDto extends PresentationDetailDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<PresentMaterialDto> presentMaterials;

	public List<PresentMaterialDto> getPresentMaterials() {
		return presentMaterials;
	}

	public void setPresentMaterials(List<PresentMaterialDto> presentMaterials) {
		this.presentMaterials = presentMaterials;
	}
	
	

}
