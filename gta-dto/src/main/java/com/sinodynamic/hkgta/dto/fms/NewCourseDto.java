package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.sinodynamic.hkgta.entity.fms.CourseSession;

public class NewCourseDto implements Serializable
{

	private static final long serialVersionUID = -1371912768347486929L;

	public NewCourseDto()
	{
		super();

	}

	public NewCourseDto(String courseType, String expired)
	{
		this.courseType = courseType;

	}

	private Long courseId;
	private String ageRangeCode;
	private Long capacity;
	private String courseDescription;
	private String courseName;
	private String courseType;
	private String createBy;
	private Timestamp createDate;
	private String internalRemark;
	private String memberAcceptance;
	private String posItemNo;
	private Date registBeginDate;
	private Date registDueDate;
	private String status;
	private String updateBy;
	private Date updateDate;
	private List<CourseSession> courseSessions;
	private BigDecimal price;
	private String posterFilename;
	private boolean sendSms;
	private String editType;
	private String openEnroll;
	private Integer    noOfSession;

	public String getEditType() {
		return editType;
	}

	public void setEditType(String editType) {
		this.editType = editType;
	}

	public Long getCourseId()
	{
		return courseId;
	}

	public void setCourseId(Long courseId)
	{
		this.courseId = courseId;
	}

	public String getAgeRangeCode()
	{
		return ageRangeCode;
	}

	public void setAgeRangeCode(String ageRangeCode)
	{
		this.ageRangeCode = ageRangeCode;
	}

	public Long getCapacity()
	{
		return capacity;
	}

	public void setCapacity(Long capacity)
	{
		this.capacity = capacity;
	}

	public String getCourseDescription()
	{
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription)
	{
		this.courseDescription = courseDescription;
	}

	public String getCourseName()
	{
		return courseName;
	}

	public void setCourseName(String courseName)
	{
		this.courseName = courseName;
	}

	public String getCourseType()
	{
		return courseType;
	}

	public void setCourseType(String courseType)
	{
		this.courseType = courseType;
	}

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public Timestamp getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Timestamp createDate)
	{
		this.createDate = createDate;
	}

	public String getInternalRemark()
	{
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark)
	{
		this.internalRemark = internalRemark;
	}

	public String getMemberAcceptance()
	{
		return memberAcceptance;
	}

	public void setMemberAcceptance(String memberAcceptance)
	{
		this.memberAcceptance = memberAcceptance;
	}

	public String getPosItemNo()
	{
		return posItemNo;
	}

	public void setPosItemNo(String posItemNo)
	{
		this.posItemNo = posItemNo;
	}

	public Date getRegistBeginDate()
	{
		return registBeginDate;
	}

	public void setRegistBeginDate(Date registBeginDate)
	{
		this.registBeginDate = registBeginDate;
	}

	public Date getRegistDueDate()
	{
		return registDueDate;
	}

	public void setRegistDueDate(Date registDueDate)
	{
		this.registDueDate = registDueDate;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public List<CourseSession> getCourseSessions()
	{
		return courseSessions;
	}

	public void setCourseSessions(List<CourseSession> courseSessions)
	{
		this.courseSessions = courseSessions;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getPosterFilename() {
		return posterFilename;
	}

	public void setPosterFilename(String posterFilename) {
		this.posterFilename = posterFilename;
	}

	public boolean isSendSms() {
	    return sendSms;
	}

	public void setSendSms(boolean sendSms) {
	    this.sendSms = sendSms;
	}

	public String getOpenEnroll() {
		return openEnroll;
	}

	public void setOpenEnroll(String openEnroll) {
		this.openEnroll = openEnroll;
	}
	
	public Integer getNoOfSession() {
		return noOfSession;
	}

	public void setNoOfSession(Integer noOfSession) {
		this.noOfSession = noOfSession;
	}
	
}
