package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

public class StaffCoachInfoDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String userId;

	private String personalInfo;
	
	private String speciality;

	public StaffCoachInfoDto() {
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getPersonalInfo()
	{
		return personalInfo;
	}

	public void setPersonalInfo(String personalInfo)
	{
		this.personalInfo = personalInfo;
	}

	public String getSpeciality()
	{
		return speciality;
	}

	public void setSpeciality(String speciality)
	{
		this.speciality = speciality;
	}

}