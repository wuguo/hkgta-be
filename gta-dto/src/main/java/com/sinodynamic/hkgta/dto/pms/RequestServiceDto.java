package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class RequestServiceDto  extends GenericDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String resvId;
	@EncryptFieldInfo
	private Long customerId;

	private String serviceContent;
	


	

	public String getResvId() {
		return resvId;
	}

	public void setResvId(String resvId) {
		this.resvId = resvId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getServiceContent() {
		return serviceContent;
	}

	public void setServiceContent(String serviceContent) {
		this.serviceContent = serviceContent;
	}


	
}
