package com.sinodynamic.hkgta.dto.pms.xml;

public enum ErrorCode {
	
	ExpiredMember("Expired club membership","277","3"),CancelMember("Cancelled club membership","277","3"),NoMatch("No match found","425","3");
	
	private ErrorCode(String errorMsg, String errorCode, String errorType) {
		this.errorMsg = errorMsg;
		this.errorCode = errorCode;
		this.errorType = errorType;
	}

	private String errorMsg;
	
	private String errorCode;
	
	private String errorType;

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	
	
	
	

}
