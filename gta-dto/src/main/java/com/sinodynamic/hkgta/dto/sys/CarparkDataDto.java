package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CarparkDataDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Header header;

	private List<DataDetail> detail=new ArrayList<>();

	private Footer footer;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}
	public List<DataDetail> getDetail() {
		return detail;
	}

	public void setDetail(List<DataDetail> detail) {
		this.detail = detail;
	}

	public Footer getFooter() {
		return footer;
	}

	public void setFooter(Footer footer) {
		this.footer = footer;
	}

	public class DataDetail {
		private String rowType; // "D" for detail row
		private String cardType;// 01 for Octopus 02 for patron card
		private String cardId;
		private String inTime;
		private String outTime;
		private String vehiclePlateNumber;

		private String eventCode;// YES /NO

		private String errorCode;

		private String responseDate;// yyyy-MM-dd HH:mm:ss

		public String getRowType() {
			return rowType;
		}

		public void setRowType(String rowType) {
			this.rowType = rowType;
		}

		public String getCardType() {
			return cardType;
		}

		public void setCardType(String cardType) {
			this.cardType = cardType;
		}

		public String getCardId() {
			return cardId;
		}

		public void setCardId(String cardId) {
			this.cardId = cardId;
		}

		public String getInTime() {
			return inTime;
		}

		public void setInTime(String inTime) {
			this.inTime = inTime;
		}

		public String getOutTime() {
			return outTime;
		}

		public void setOutTime(String outTime) {
			this.outTime = outTime;
		}

		public String getVehiclePlateNumber() {
			return vehiclePlateNumber;
		}

		public void setVehiclePlateNumber(String vehiclePlateNumber) {
			this.vehiclePlateNumber = vehiclePlateNumber;
		}

		public String getEventCode() {
			return eventCode;
		}

		public void setEventCode(String eventCode) {
			this.eventCode = eventCode;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		public String getResponseDate() {
			return responseDate;
		}

		public void setResponseDate(String responseDate) {
			this.responseDate = responseDate;
		}

	}

	public class Header {
		private String rowType;// H / D /F

		private String time;// yyyyMMddHHmmss

		private String status;// SEND

		public String getRowType() {
			return rowType;
		}

		public void setRowType(String rowType) {
			this.rowType = rowType;
		}

		public String getTime() {
			return time;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

	}

	public class Footer {
		private String rowType;// H / D /F

		private int totalLine;

		public String getRowType() {
			return rowType;
		}

		public void setRowType(String rowType) {
			this.rowType = rowType;
		}

		public int getTotalLine() {
			return totalLine;
		}

		public void setTotalLine(int totalLine) {
			this.totalLine = totalLine;
		}
	}

}
