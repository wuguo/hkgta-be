package com.sinodynamic.hkgta.dto.pms;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RosterTimelostDto {
	private String beginTime;
	private String endTime;

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
