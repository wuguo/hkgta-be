package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class PresentMaterialDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long materialId;
	
	private Long materialSeq;

	private String materialDesc;
	
	private Long folderId;
	
	private String materialFilename;
	
	private String thumbnail;
	
	private String materialURL;
	
	private String thumbnailURL;
	
	private String materialType;
	
	private String status;

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public String getMaterialDesc() {
		return materialDesc;
	}

	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}

	public Long getFolderId() {
		return folderId;
	}

	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}

	


	public Long getMaterialSeq() {
		return materialSeq;
	}

	public void setMaterialSeq(Long materialSeq) {
		this.materialSeq = materialSeq;
	}

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMaterialURL() {
		return materialURL;
	}

	public void setMaterialURL(String materialURL) {
		this.materialURL = materialURL;
	}

	public String getThumbnailURL() {
		return thumbnailURL;
	}

	public void setThumbnailURL(String thumbnailURL) {
		this.thumbnailURL = thumbnailURL;
	}

	public String getMaterialFilename() {
		return materialFilename;
	}

	public void setMaterialFilename(String materialFilename) {
		this.materialFilename = materialFilename;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

}
