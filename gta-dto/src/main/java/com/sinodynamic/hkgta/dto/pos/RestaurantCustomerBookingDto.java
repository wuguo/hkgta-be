package com.sinodynamic.hkgta.dto.pos;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class RestaurantCustomerBookingDto extends GenericDto implements Serializable{
	private Long resvId;
	
	private String resvIdStr;

	private String restaurantId;
	
	private String restaurantName;
	@EncryptFieldInfo  
	private Long customerId;

	private String bookTime;

	private Long partySize;

	private String status;

	private String bookVia;

	private String remark;

	private String memberName;

	private String academyNo;

	private String phoneMobile;

	private String phoneHome;

	private Boolean checkAvailable;

	private String createdBy ;
	
	private String prefix;
	
	/***
	 * Add 'Create Time' on restaurant booking
	 * @return
	 */
	private String createTime;

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getCheckAvailable() {
		if (null == this.checkAvailable) {
			this.checkAvailable = Boolean.FALSE;
		}
		return checkAvailable;
	}

	public void setCheckAvailable(Boolean checkAvailable) {
		this.checkAvailable = checkAvailable;
	}

	public String getPhoneMobile() {
		return DtoHelper.nvl(phoneMobile);
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public String getPhoneHome() {
		return DtoHelper.nvl(phoneHome);
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Object resvId) {
		this.resvId = (resvId != null ? NumberUtils.toLong(resvId.toString()) : null);
	}

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getRestaurantName() {
		return restaurantName;
	}

	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getBookTime() {
		return bookTime;
	}

	public void setBookTime(String bookTime) {
		this.bookTime = bookTime;
	}

	public Long getPartySize() {
		return partySize;
	}

	public void setPartySize(Object partySize) {
		this.partySize = (partySize != null ? NumberUtils.toLong(partySize.toString()) : null);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBookVia() {
		return bookVia;
	}

	public void setBookVia(String bookVia) {
		this.bookVia = bookVia;
	}

	public String getRemark() {
		return DtoHelper.nvl(remark);
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getResvIdStr() {
		return resvIdStr;
	}

	public void setResvIdStr(String resvIdStr) {
		this.resvIdStr = resvIdStr;
	}

}
