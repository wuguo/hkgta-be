package com.sinodynamic.hkgta.dto.qst;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import org.omg.CORBA.OBJ_ADAPTER;

public class SurveyQuestionnaireStatisticsDataDto implements Serializable {
	
	private Long sysId;
	
	private Long surveyId;
	
	private String url;
	
	private Date sendTime;
	
	private int bufferDay;

	
	public int getBufferDay() {
		return bufferDay;
	}

	public void setBufferDay(Object bufferDay) {
		this.bufferDay = Integer.parseInt(bufferDay.toString());
	}

	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Object sysId) {
		this.sysId = Long.parseLong(sysId.toString());
	}

	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Object surveyId) {
		this.surveyId = Long.parseLong(surveyId.toString());
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	
	

}
