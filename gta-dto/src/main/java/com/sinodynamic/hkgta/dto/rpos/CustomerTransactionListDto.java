package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
//import java.sql.Date;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.dto.Constant;
import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;
/**
 * @author Shuai_Liang
 * @since 4-29
 */
//modified by Kaster 20160323 增加extends GenericDto
public class CustomerTransactionListDto extends GenericDto implements Serializable {

	private static final long serialVersionUID = -7923248531853988914L;
	private Long orderNo;
	//added by Kaster 20160323
	@EncryptFieldInfo
	private Long customerId;
	private BigDecimal orderTotalAmount;
	private BigDecimal balanceDue;
	private String description;
	private String surname;
	private String givenName;
	private String companyName;
	private String salutation;
	private String customerName;
	private String contactEmail;
	private Date effectiveDateToConvert;
	private Date orderUpdateDateToConvert;
	private String effectiveDate;
	private String updateDate;
	private Date enrollDateToConvert;
	private Date enrollCreateDate;
	private String offerCode;
	private Boolean hasRemark;
	private String status;
	private Long newTransNumber;
	private String enrollStatus;
	private String noOfDays;
	private String servicePlan;
	private String salesFollowBy;
	private String orderStatus;
	private String academyNo;
	private Boolean hasRead;
	private String corporateName;
	
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getAcademyNo() {
		return academyNo;
	}
	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public BigDecimal getOrderTotalAmount() {
		return orderTotalAmount;
	}
	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}
	public BigDecimal getBalanceDue() {
		return balanceDue;
	}
	public void setBalanceDue(BigDecimal balanceDue) {
		this.balanceDue = balanceDue;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getEffectiveDate() {
		return DtoHelper.getYMDDateAndDateDiff(enrollDateToConvert);
	}
	
	public String getUpdateDate() {
		return DtoHelper.getYMDDateAndDateDiff(orderUpdateDateToConvert);
	}
	
	public String getOfferCode() {
		return offerCode;
	}
	public void setOfferCode(String offerCode) {
		if(DtoHelper.notEmpty(offerCode) && offerCode.equals(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)){
			this.offerCode = Constant.SERVICE_PLAN_TYPE_RENEWAL;
		}else{
			this.offerCode = Constant.SERVICE_PLAN_TYPE_ENROLLMENT;
		}
	}
	public Boolean getHasRemark() {
		return hasRemark;
	}
	public void setHasRemark(Boolean hasRemark) {
		this.hasRemark = hasRemark;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	public Long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Object orderNo) {
		this.orderNo = (orderNo!=null ? NumberUtils.toLong(orderNo.toString()):null);
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Object customerId) {
		this.customerId = (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}
	public Long getNewTransNumber() {
		return newTransNumber;
	}
	public void setNewTransNumber(Object newTransNumber) {
		this.newTransNumber = (newTransNumber!=null ? NumberUtils.toLong(newTransNumber.toString()):null);
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public void setServicePlan(String servicePlan) {
		this.servicePlan = servicePlan;
	}
	public String getEnrollStatus() {
		if(enrollStatus != null && enrollStatus.equalsIgnoreCase("TOA") && getNoOfDays() != null && Integer.parseInt(getNoOfDays().replace("(", "").replace(")", "")) > 0){
			enrollStatus = enrollStatus + getNoOfDays().replace(")", " Days)");
		}
		return enrollStatus;
	}
	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}
	public String getServicePlan() {
		return servicePlan;
	}
	public void setSevicePlan(String servicePlan) {
		this.servicePlan = servicePlan;
	}
	public String getSalesFollowBy() {
		return salesFollowBy;
	}
	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}
	public Date getOrderUpdateDateToConvert() {
		return orderUpdateDateToConvert;
	}
	public void setOrderUpdateDateToConvert(Date orderUpdateDateToConvert) {
		this.orderUpdateDateToConvert = orderUpdateDateToConvert;
	}
	public String getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}
	public Boolean getHasRead() {
		return hasRead;
	}
	public void setHasRead(Boolean hasRead) {
		this.hasRead = hasRead;
	}
	public Date getEffectiveDateToConvert() {
		return effectiveDateToConvert;
	}
	public void setEffectiveDateToConvert(Date effectiveDateToConvert) {
		this.effectiveDateToConvert = effectiveDateToConvert;
	}
	public Date getEnrollDateToConvert() {
		return enrollDateToConvert;
	}
	public void setEnrollDateToConvert(Date enrollDateToConvert) {
		this.enrollDateToConvert = enrollDateToConvert;
	}
	public Date getEnrollCreateDate() {
		return enrollCreateDate;
	}
	public void setEnrollCreateDate(Date enrollCreateDate) {
		this.enrollCreateDate = enrollCreateDate;
	}
	public String getCorporateName() {
		return corporateName;
	}
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}
	
}
