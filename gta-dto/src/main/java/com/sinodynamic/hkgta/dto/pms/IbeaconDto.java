package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class IbeaconDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long major;
	private Long minor;

	public Long getMajor() {
		return major;
	}

	public void setMajor(Long major) {
		this.major = major;
	}

	public Long getMinor() {
		return minor;
	}

	public void setMinor(Long minor) {
		this.minor = minor;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
