package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

public class verifyResultDto implements Serializable{

	private String isExists;

	public String getIsExists() {
		return isExists;
	}

	public void setIsExists(String isExists) {
		this.isExists = isExists;
	}

	@Override
	public String toString() {
		return "verifyResultDto [isExists=" + isExists + ", getIsExists()="
				+ getIsExists() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	
}
