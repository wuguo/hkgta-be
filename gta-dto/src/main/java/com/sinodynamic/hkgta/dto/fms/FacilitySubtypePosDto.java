package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;

public class FacilitySubtypePosDto implements Serializable {

	private static final long serialVersionUID = 7704464042374843389L;

	private Long subtypePosId;
	private BigDecimal price = BigDecimal.ZERO;

	public Long getSubtypePosId() {
		return subtypePosId;
	}

	public void setSubtypePosId(Long subtypePosId) {
		this.subtypePosId = subtypePosId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public FacilitySubtypePosDto() {
		super();
	}

	// TODO remove me.
	public FacilitySubtypePosDto(Long subtypePosId) {
		super();
		this.subtypePosId = subtypePosId;
	}

}
