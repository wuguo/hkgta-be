package com.sinodynamic.hkgta.dto.fms;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class PatronActivitiesDto {
	
	private String academyId;
	private String patronName;
	private String type;
	private String description;
	private String activities;
	private Date activitiesDateFrom;
	private Date activitiesDateTo;
	private Integer units;
	private BigDecimal amount;
	private Date activitiesDate;
	
	public String getAcademyId() {
		return academyId;
	}	
	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}
	public String getPatronName() {
		return patronName;
	}
	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getActivities() {
		return activities;
	}
	public void setActivities(String activities) {
		this.activities = activities;
	}
	public Date getActivitiesDateFrom() {
		return activitiesDateFrom;
	}
	public void setActivitiesDateFrom(Date activitiesDateFrom) {
		this.activitiesDateFrom = activitiesDateFrom;
	}
	public Date getActivitiesDateTo() {
		return activitiesDateTo;
	}
	public void setActivitiesDateTo(Date activitiesDateTo) {
		this.activitiesDateTo = activitiesDateTo;
	}
	public Integer getUnits() {
		return units;
	}
	public void setUnits(Object units) {
		if(units instanceof BigDecimal){
			this.units = ((BigDecimal) units).intValue();
		}else if(units instanceof BigInteger){
			this.units = ((BigInteger) units).intValue();
		}else{
			this.units = (Integer) units;
		}
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Date getActivitiesDate() {
		return activitiesDate;
	}
	public void setActivitiesDate(Date activitiesDate) {
		this.activitiesDate = activitiesDate;
	}
	
}
