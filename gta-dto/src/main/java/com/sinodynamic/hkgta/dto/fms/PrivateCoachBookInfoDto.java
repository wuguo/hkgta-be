package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

/**
 * @author kevin_Liang
 *
 */
public class PrivateCoachBookInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7050556633567144154L;
	private String resvId;
	private String date;
	private String bayType;
	private String priorTime;
	private String priorCoachId;
	private String priorCoachName;
	public String getResvId() {
		return resvId;
	}
	public void setResvId(String resvId) {
		this.resvId = resvId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getBayType() {
		return bayType;
	}
	public void setBayType(String bayType) {
		this.bayType = bayType;
	}
	public String getPriorTime() {
		return priorTime;
	}
	public void setPriorTime(String priorTime) {
		this.priorTime = priorTime;
	}
	public String getPriorCoachId() {
		return priorCoachId;
	}
	public void setPriorCoachId(String priorCoachId) {
		this.priorCoachId = priorCoachId;
	}
	public String getPriorCoachName() {
		return priorCoachName;
	}
	public void setPriorCoachName(String priorCoachName) {
		this.priorCoachName = priorCoachName;
	}
	

}
