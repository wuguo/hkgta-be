package com.sinodynamic.hkgta.dto.pms;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ManagementStaffDto {

	private String userId;

	private String fullName;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
