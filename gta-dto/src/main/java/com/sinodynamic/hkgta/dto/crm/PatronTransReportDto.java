package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class PatronTransReportDto implements Serializable {
	
	private String servicePlanName;
	
	private String academyNo;
	
	private String memberName;
	
	private String memberType;
	
	private String facilitiyType;
	
	private BigDecimal amount;
	
	private String paymentMethod;
	
	private String paymentCode;
	
	private Long number;
	
	public String getFacilitiyType() {
		return facilitiyType;
	}

	public void setFacilitiyType(String facilitiyType) {
		this.facilitiyType = facilitiyType;
	}

	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}



	public String getServicePlanName() {
		return servicePlanName;
	}

	public void setServicePlanName(String servicePlanName) {
		this.servicePlanName = servicePlanName;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(Object academyNo) {
		this.academyNo=academyNo.toString();
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(Object amount) {
		if (amount instanceof BigInteger) {
			this.amount = BigDecimal.valueOf(Double.valueOf(amount.toString()));
		} else {
			this.amount = (BigDecimal) amount;
		}
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Object number) {
		
		if(number instanceof BigInteger){
			this.number = ((BigInteger) number).longValue();
		}else if(number instanceof Integer){
			this.number = ((Integer) number).longValue();
		}else if(number instanceof String){
			this.number = Long.valueOf((String) number);
		}else if(number instanceof BigDecimal){
			this.number =Long.valueOf(number.toString());
		}
		else{
			this.number = (Long) number;
		}
	}

}
