package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.dto.DtoHelper;

public class MemberTopupDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date transactionTimestamp;
	private Long transactionNo;
	private String paymentMethodCode;
	private BigDecimal paidAmount;
	private String memberName;

	public String getTransactionTimestamp() {
		if (transactionTimestamp == null) {
			return "";
		}
		return DtoHelper.getYMDDateAndDateDiff(transactionTimestamp);
	}

	public void setTransactionTimestamp(Date transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}

	public Long getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(Long transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getPaymentMethodCode() {
		return DtoHelper.nvl(paymentMethodCode);
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public BigDecimal getPaidAmount() {
		if (this.paidAmount != null) {
			return paidAmount;
		} else {
			return new BigDecimal(0);
		}
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

}
