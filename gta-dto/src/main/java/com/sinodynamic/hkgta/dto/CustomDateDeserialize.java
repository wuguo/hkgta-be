package com.sinodynamic.hkgta.dto;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

/**
 * @author Mason_Yang
 *
 */
public class CustomDateDeserialize extends JsonDeserializer<Date> {    
    
    @Override    
    public Date deserialize(JsonParser jp, DeserializationContext ctxt)    
            throws IOException, JsonProcessingException {    
    
        Date date = null;    
        try {
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");    
            date = sdf.parse(jp.getText());    
        } catch (ParseException e) {    
            e.printStackTrace();    
        }    
        return date;    
    }    
}   