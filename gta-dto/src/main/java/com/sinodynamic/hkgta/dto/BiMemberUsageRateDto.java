package com.sinodynamic.hkgta.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class BiMemberUsageRateDto implements Serializable {
	
	private String periodDate;
	
	private String rateType;
	
	private int rate;
	
	private double quot;
	
	private long totalMember;
	
	private long totalNum;
	
	
	public String getPeriodDate() {
		return periodDate;
	}

	public void setPeriodDate(Object periodDate) {
		this.periodDate = periodDate.toString();
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType == null ? "" : rateType;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(Object rate) {
		if(rate instanceof BigDecimal){
			this.rate = ((BigDecimal) rate).intValue();
		}else{
			this.rate = (Integer.valueOf(rate.toString()));
		}
	}

	public double getQuot() {
		return quot;
	}

	public void setQuot(Object quot) {
		this.quot = quot == null ? 0.0 : Double.parseDouble(quot.toString());
	}

	public long getTotalMember() {
		return totalMember;
	}

	public void setTotalMember(Object totalMember) {
		this.totalMember = totalMember == null ? 0l : Long.parseLong(totalMember.toString());
	}

	public long getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(Object totalNum) {
		this.totalNum = totalNum == null ? 0l : Long.parseLong(totalNum.toString());
	}



}
