package com.sinodynamic.hkgta.dto.fms;

public class PosResponse {
	private String type;
	private String referenceNo;
	private String amount;
	private String tips;
	
	private String responseCode;
	private String responseText;
	private String transactionDate;
	
	private String cardType;
	private String cardNumber;
	
	//EDC CUP
	private String expirationDate;
	private String cardHolderName;
	private String terminalNumber;
	private String merchantNumber;
	private String traceNumber;
	private String batchNumber;
	private String approvalCode;
	private String hostReferenceNo;
	
	//CUP
	private String operatorId;	
	private String issuerReferenceNo;
	private String acquirerReferenceNo;
	private String posCenterReferenceNo;
	private String hostResponseMsg;
	private String originalApprovalCode;

	//EPS
	private String storeNumber;
	private String valueDay;
	private String debitAccountNumber;
	private String bankAdditionalResponseMsg;	
	//EPS cashback
	private String purchase;
	private String cashback;
	//EPS_CUP
	private String brandName;
	private String billingCurrencyCode;
	private String billingAmount;
	private String acIndicator;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTips() {
		return tips;
	}
	public void setTips(String tips) {
		this.tips = tips;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseText() {
		return responseText;
	}
	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
	public String getTerminalNumber() {
		return terminalNumber;
	}
	public void setTerminalNumber(String terminalNumber) {
		this.terminalNumber = terminalNumber;
	}
	public String getMerchantNumber() {
		return merchantNumber;
	}
	public void setMerchantNumber(String merchantNumber) {
		this.merchantNumber = merchantNumber;
	}
	public String getTraceNumber() {
		return traceNumber;
	}
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getHostReferenceNo() {
		return hostReferenceNo;
	}
	public void setHostReferenceNo(String hostReferenceNo) {
		this.hostReferenceNo = hostReferenceNo;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public String getIssuerReferenceNo() {
		return issuerReferenceNo;
	}
	public void setIssuerReferenceNo(String issuerReferenceNo) {
		this.issuerReferenceNo = issuerReferenceNo;
	}
	public String getAcquirerReferenceNo() {
		return acquirerReferenceNo;
	}
	public void setAcquirerReferenceNo(String acquirerReferenceNo) {
		this.acquirerReferenceNo = acquirerReferenceNo;
	}
	public String getPosCenterReferenceNo() {
		return posCenterReferenceNo;
	}
	public void setPosCenterReferenceNo(String posCenterReferenceNo) {
		this.posCenterReferenceNo = posCenterReferenceNo;
	}
	public String getHostResponseMsg() {
		return hostResponseMsg;
	}
	public void setHostResponseMsg(String hostResponseMsg) {
		this.hostResponseMsg = hostResponseMsg;
	}
	public String getOriginalApprovalCode() {
		return originalApprovalCode;
	}
	public void setOriginalApprovalCode(String originalApprovalCode) {
		this.originalApprovalCode = originalApprovalCode;
	}

	
	public String getStoreNumber() {
		return storeNumber;
	}
	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}
	public String getValueDay() {
		return valueDay;
	}
	public void setValueDay(String valueDay) {
		this.valueDay = valueDay;
	}
	public String getDebitAccountNumber() {
		return debitAccountNumber;
	}
	public void setDebitAccountNumber(String debitAccountNumber) {
		this.debitAccountNumber = debitAccountNumber;
	}
	public String getBankAdditionalResponseMsg() {
		return bankAdditionalResponseMsg;
	}
	public void setBankAdditionalResponseMsg(String bankAdditionalResponseMsg) {
		this.bankAdditionalResponseMsg = bankAdditionalResponseMsg;
	}
	public String getPurchase() {
		return purchase;
	}
	public void setPurchase(String purchase) {
		this.purchase = purchase;
	}
	public String getCashback() {
		return cashback;
	}
	public void setCashback(String cashback) {
		this.cashback = cashback;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getBillingCurrencyCode() {
		return billingCurrencyCode;
	}
	public void setBillingCurrencyCode(String billingCurrencyCode) {
		this.billingCurrencyCode = billingCurrencyCode;
	}
	public String getBillingAmount() {
		return billingAmount;
	}
	public void setBillingAmount(String billingAmount) {
		this.billingAmount = billingAmount;
	}
	public String getAcIndicator() {
		return acIndicator;
	}
	public void setAcIndicator(String acIndicator) {
		this.acIndicator = acIndicator;
	}
}