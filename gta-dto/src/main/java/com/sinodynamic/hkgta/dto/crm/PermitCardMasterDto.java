package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class PermitCardMasterDto  extends GenericDto  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	public PermitCardMasterDto() {
		super();
	}

	
	public PermitCardMasterDto(String fromDate, String toDate, String status, Long customerId, int pageNumber, int pageSize) {
		super();
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.status = status;
		this.setPageNumber(pageNumber);
		this.setPageSize(pageSize);
		this.customerId = customerId;
		
	}
	@EncryptFieldInfo
	private Long customerId;
	private String status;
	private String updateBy;
	private String cardTypeName;
	private String cardNo;

	
	//for list query
	private String fromDate;
	private int pageNumber;
	private int pageSize;
	
	public String getFromDate() {
		return fromDate;
	}


	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	public String getToDate() {
		return toDate;
	}


	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	private String toDate;
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}


	public int getPageNumber() {
		return pageNumber;
	}


	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}


	public int getPageSize() {
		return pageSize;
	}


	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
