package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class AllTransactionDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 327791100997292676L;
	
	
	private String transactionDate;
	private BigInteger transactionNo;
	private String transactionType;
	private String paymentMethodCode;
	private String description;
	private String memberName;
	private BigDecimal paidAmount;
	
	private BigInteger fromTransactionNo;
	
	private String transactionTimestampDesc;
	
    public String getTransactionTimestampDesc() {
    	if (transactionTimestamp == null) {
			return "";
		}
		return DtoHelper.getYMDDateAndDateDiff(transactionTimestamp);
	}
	public void setTransactionTimestampDesc(String transactionTimestampDesc) {
		this.transactionTimestampDesc = transactionTimestampDesc;
	}
	public BigInteger getFromTransactionNo() {
		return fromTransactionNo;
	}
	public void setFromTransactionNo(BigInteger fromTransactionNo) {
		this.fromTransactionNo = fromTransactionNo;
	}

	/***
	 * christ add 2016-06-01
	 */
	private String itemCatagory;
	
	private Date transactionTimestamp;
	
	public String getItemCatagory() {
		return itemCatagory;
	}
	public void setItemCatagory(String itemCatagory) {
		this.itemCatagory = itemCatagory;
	}
	public Date getTransactionTimestamp() {
		return transactionTimestamp;
	}
	public void setTransactionTimestamp(Date transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
 
	public BigInteger getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(BigInteger transactionNo) {
		this.transactionNo = transactionNo;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}
	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if (null == description || description.equals("")) {
			this.description = "";
		} else {
			String[] arr = description.split("@#;");
			if (null == arr || description.equals("")) {
				this.description = description;
			} else {
				this.description = arr[0];
			}
		}
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
 
  
	 
	 
}
