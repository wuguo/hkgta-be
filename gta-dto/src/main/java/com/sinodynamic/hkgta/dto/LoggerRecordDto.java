package com.sinodynamic.hkgta.dto;

import java.io.Serializable;

public class LoggerRecordDto implements Serializable {
	
	private String fileName;
	
	private String loggerType;

	private String rootPath;
	
	private String filePath;
	
	private String date;
	
	private String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	public String getLoggerType() {
		return loggerType;
	}

	public void setLoggerType(String loggerType) {
		this.loggerType = loggerType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDate() {
		return date;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setDate(String date) {
		this.date = date;
	}
	

}
