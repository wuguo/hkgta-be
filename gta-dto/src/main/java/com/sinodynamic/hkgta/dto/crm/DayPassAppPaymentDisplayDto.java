package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

public class DayPassAppPaymentDisplayDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long noOfPerson;
	private BigDecimal dayPrice;
	private Integer noOfDay;
	public Long getNoOfPerson() {
		return noOfPerson;
	}
	public void setNoOfPerson(Long noOfPerson) {
		this.noOfPerson = noOfPerson;
	}
	public BigDecimal getDayPrice() {
		return dayPrice;
	}
	public void setDayPrice(BigDecimal dayPrice) {
		this.dayPrice = dayPrice;
	}
	public Integer getNoOfDay() {
		return noOfDay;
	}
	public void setNoOfDay(Integer noOfDay) {
		this.noOfDay = noOfDay;
	}
	
}
