package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class UpadeCashValueDto  extends GenericDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String agentTransactionNo;   				//第三方系统订单号
	private BigDecimal adjustmentAmount;
	private String adjustmentType;
	private String remark;
	@EncryptFieldInfo
	private Long customerId;
	private String staffUserId;
	private String topUpLocation;
	
	
	public String getTopUpLocation() {
		return topUpLocation;
	}

	public void setTopUpLocation(String topUpLocation) {
		this.topUpLocation = topUpLocation;
	}
	
	
	
	
	public String getAdjustmentType() {
		return adjustmentType;
	}
	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getAgentTransactionNo() {
		return agentTransactionNo;
	}
	public void setAgentTransactionNo(String agentTransactionNo) {
		this.agentTransactionNo = agentTransactionNo;
	}
	public BigDecimal getAdjustmentAmount() {
		return adjustmentAmount;
	}
	public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getStaffUserId() {
		return staffUserId;
	}
	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}
	
	
	
}
