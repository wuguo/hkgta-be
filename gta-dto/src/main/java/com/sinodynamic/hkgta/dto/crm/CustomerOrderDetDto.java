package com.sinodynamic.hkgta.dto.crm;

import java.math.BigDecimal;

public class CustomerOrderDetDto {
	private Long orderQty;
	private BigDecimal itemTotalAmout;
	private Long planNo;
	private String showName;
	private String rateType;
	private int rateNo;
	
	
	public int getRateNo() {
		return rateNo;
	}
	public void setRateNo(int rateNo) {
		this.rateNo = rateNo;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getShowName() {
		return showName;
	}
	public void setShowName(String showName) {
		this.showName = showName;
	}
	public Long getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(Long orderQty) {
		this.orderQty = orderQty;
	}
	public BigDecimal getItemTotalAmout() {
		return itemTotalAmout;
	}
	public void setItemTotalAmout(BigDecimal itemTotalAmout) {
		this.itemTotalAmout = itemTotalAmout;
	}
	public Long getPlanNo() {
		return planNo;
	}
	public void setPlanNo(Long planNo) {
		this.planNo = planNo;
	}
}
