package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

//modified by Kaster 20160323 
public class RemarksDto extends GenericDto implements Serializable {

	private static final long serialVersionUID = 1L;

	//added by Kaster 20160323
	@EncryptFieldInfo
	private Long customerId;

	private String salesLoginId;

	private String contentRemarks;

	private Date commentDate;

	private String commentBy;

	private String commentStatus;

	private String commentByDevice;

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public String getCommentBy() {
		return commentBy;
	}

	public void setCommentBy(String commentBy) {
		this.commentBy = commentBy;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = (customerId != null ? NumberUtils.toLong(customerId.toString()) : null);
	}

	public String getSalesLoginId() {
		return salesLoginId;
	}

	public void setSalesLoginId(String salesLoginId) {
		this.salesLoginId = salesLoginId;
	}

	public String getContentRemarks() {
		return contentRemarks;
	}

	public void setContentRemarks(String contentRemarks) {
		this.contentRemarks = contentRemarks;
	}

	public String getCommentStatus() {
		return commentStatus;
	}

	public void setCommentStatus(String commentStatus) {
		this.commentStatus = commentStatus;
	}

	public String getCommentByDevice() {
		return commentByDevice;
	}

	public void setCommentByDevice(String commentByDevice) {
		this.commentByDevice = commentByDevice;
	}

}
