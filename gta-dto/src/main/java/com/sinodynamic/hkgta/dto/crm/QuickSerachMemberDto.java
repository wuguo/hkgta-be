package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;

public class QuickSerachMemberDto  implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	public QuickSerachMemberDto(){
		super();
	}
	
	public QuickSerachMemberDto(Long customerId, String salutation, String surname, String givenName){
	}
	public QuickSerachMemberDto(BigInteger customerId) {
		super();
		this.customerId = customerId;
	}
	
	private BigInteger customerId;
	
	private String salutation;

	private String surname;

	private String givenName;

	public BigInteger getCustomerId() {
		return customerId;
	}

	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	
	}

