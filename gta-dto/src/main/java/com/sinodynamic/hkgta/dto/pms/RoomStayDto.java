package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Kevin_Liang
 *
 */
public class RoomStayDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7305766393779752188L;
	private List<RoomTypeDto> roomTypes;
	private List<RatePlanDto> ratePlans;
	private List<RoomRateDto> roomRates;

	public List<RoomTypeDto> getRoomTypes() {
		return roomTypes;
	}

	public void setRoomTypes(List<RoomTypeDto> roomTypes) {
		this.roomTypes = roomTypes;
	}

	public List<RatePlanDto> getRatePlans() {
		return ratePlans;
	}

	public void setRatePlans(List<RatePlanDto> ratePlans) {
		this.ratePlans = ratePlans;
	}

	public List<RoomRateDto> getRoomRates() {
		return roomRates;
	}

	public void setRoomRates(List<RoomRateDto> roomRates) {
		this.roomRates = roomRates;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
