package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class InternalPermitCardDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public InternalPermitCardDto() {
		super();
	}
	
	public InternalPermitCardDto(String status, int pageNumber, int pageSize) {
		super();
		this.status = status;
		this.setPageNumber(pageNumber);
		this.setPageSize(pageSize);
		
	}

	private Long cardId;
	private String factorySerialNo;
	private String status;
	private String cardPurpose;
	private String staffUserId;
	private Long contractorId;
	private Date effectiveDate;
	private String expiryDate;
	private Timestamp statusUpdateDate;
	private String updateBy;

	// for list query
	private int pageNumber;
	private int pageSize;
	
	
	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	

	public Long getCardId() {
		return cardId;
	}

	public void setCardId(Long cardId) {
		this.cardId = cardId;
	}

	public String getFactorySerialNo() {
		return factorySerialNo;
	}

	public void setFactorySerialNo(String factorySerialNo) {
		this.factorySerialNo = factorySerialNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCardPurpose() {
		return cardPurpose;
	}

	public void setCardPurpose(String cardPurpose) {
		this.cardPurpose = cardPurpose;
	}

	public String getStaffUserId() {
		return staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}

	public Long getContractorId() {
		return contractorId;
	}

	public void setContractorId(Long contractorId) {
		this.contractorId = contractorId;
	}

	
	public String getEffectiveDate() {
		return DtoHelper.getYMDDateAndDateDiff(effectiveDate);
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Timestamp getStatusUpdateDate() {
		return statusUpdateDate;
	}

	public void setStatusUpdateDate(Timestamp statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

}
