/**
 * 
 */
package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

/**
 * @author Tony_Dong
 *
 */
public class CoachMonthyAchivementDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6646086576148624543L;
	
	private Long sumStudent;
	
	private Long sumNewStudent;
	
	private Long sumCourseHours;
	
	private Long sumClassesHours;
	
	

	public Long getSumStudent() {
		return sumStudent;
	}



	public void setSumStudent(Long sumStudent) {
		this.sumStudent = sumStudent;
	}



	public Long getSumNewStudent() {
		return sumNewStudent;
	}



	public void setSumNewStudent(Long sumNewStudent) {
		this.sumNewStudent = sumNewStudent;
	}



	public Long getSumCourseHours() {
		return sumCourseHours;
	}



	public void setSumCourseHours(Long sumCourseHours) {
		this.sumCourseHours = sumCourseHours;
	}



	public Long getSumClassesHours() {
		return sumClassesHours;
	}



	public void setSumClassesHours(Long sumClassesHours) {
		this.sumClassesHours = sumClassesHours;
	}



	/**
	 * 
	 */
	public CoachMonthyAchivementDto() {
		// TODO Auto-generated constructor stub
	}
	

}
