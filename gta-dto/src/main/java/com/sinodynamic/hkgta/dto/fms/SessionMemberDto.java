package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.Date;

public class SessionMemberDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String memberId;
    private String memberName;
    private String attendance;
    private Date updateDate;

    public String getMemberId() {
	return memberId;
    }

    public void setMemberId(String memberId) {
	this.memberId = memberId;
    }

    public String getMemberName() {
	return memberName;
    }

    public void setMemberName(String memberName) {
	this.memberName = memberName;
    }

    public String getAttendance() {
	return attendance;
    }

    public void setAttendance(String attendance) {
	this.attendance = attendance;
    }

    public Date getUpdateDate() {
	return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
	this.updateDate = updateDate;
    }

}
