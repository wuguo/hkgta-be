package com.sinodynamic.hkgta.dto.dashboard;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * 
 * @author Kaster 20160413
 *
 */
public class StaffSalesAmountDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String staffId;
	private BigDecimal salesAmount;
	private BigInteger numOfPatron;
	public String getStaffId() {
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	public BigDecimal getSalesAmount() {
		return salesAmount;
	}
	public void setSalesAmount(BigDecimal salesAmount) {
		this.salesAmount = salesAmount;
	}
	public BigInteger getNumOfPatron() {
		return numOfPatron;
	}
	public void setNumOfPatron(BigInteger numOfPatron) {
		this.numOfPatron = numOfPatron;
	}
}
