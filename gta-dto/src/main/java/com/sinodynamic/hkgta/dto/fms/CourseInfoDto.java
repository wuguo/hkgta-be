package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;


/**
 * @author Kevin_Liang
 *
 */
public class CourseInfoDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1246735309344559405L;
	private String courseId;
	private String courseName;
	private String registDate;
	private String startDate;
	private String coursePeriod;
	private String status;
	private String enrollment;
	private String acceptance;
	private Long sessionCount;
	private String ageRange;
	private String description;
	private BigDecimal price;
	private String internalRemark;
	private String cancelRequesterType;
	private boolean refundFlag;
	
	private String updateBy;
	
	private List<CourseScheduleDto> courseScheduleList;
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getRegistDate() {
		return registDate;
	}
	public void setRegistDate(String registDate) {
		this.registDate = registDate;
	}
	public String getCoursePeriod() {
		return coursePeriod;
	}
	public void setCoursePeriod(String coursePeriod) {
		this.coursePeriod = coursePeriod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEnrollment() {
		return enrollment;
	}
	public void setEnrollment(String enrollment) {
		this.enrollment = enrollment;
	}
	public Long getSessionCount() {
		return sessionCount;
	}
	public void setSessionCount(Long sessionCount) {
		this.sessionCount = sessionCount;
	}
	public String getAgeRange() {
		return ageRange;
	}
	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public List<CourseScheduleDto> getCourseScheduleList() {
		return courseScheduleList;
	}
	public void setCourseScheduleList(List<CourseScheduleDto> courseScheduleList) {
		this.courseScheduleList = courseScheduleList;
	}
	public String getAcceptance() {
		return acceptance;
	}
	public void setAcceptance(String acceptance) {
		this.acceptance = acceptance;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getInternalRemark() {
		return internalRemark;
	}
	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}
	public String getCancelRequesterType() {
		return cancelRequesterType;
	}
	public void setCancelRequesterType(String cancelRequesterType) {
		this.cancelRequesterType = cancelRequesterType;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public boolean isRefundFlag() {
	    return refundFlag;
	}
	public void setRefundFlag(boolean refundFlag) {
	    this.refundFlag = refundFlag;
	}
	
}
