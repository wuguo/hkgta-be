package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.List;


public class FacilityUtilizationRateDateDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long dateId;

	private String weekDay;
	
	private String specialDate;
	
	private String dateDesc;
	
	private List<FacilityUtilizationRateTimeDto> rateList;
	
	private String facilityType;
	
	private String facilitySubTypeId;
	
	private String updatedBy;
	
	private String updatedTime;
	
	public Long getDateId()
	{
		return dateId;
	}

	public void setDateId(Long dateId)
	{
		this.dateId = dateId;
	}

	public String getFacilityType()
	{
		return facilityType;
	}

	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}

	public String getFacilitySubTypeId()
	{
		return facilitySubTypeId;
	}

	public void setFacilitySubTypeId(String facilitySubTypeId)
	{
		this.facilitySubTypeId = facilitySubTypeId;
	}

	public String getWeekDay()
	{
		return weekDay;
	}

	public void setWeekDay(String weekDay)
	{
		this.weekDay = weekDay;
	}

	public String getSpecialDate()
	{
		return specialDate;
	}

	public void setSpecialDate(String specialDate)
	{
		this.specialDate = specialDate;
	}

	public String getDateDesc()
	{
		return dateDesc;
	}

	public void setDateDesc(String dateDesc)
	{
		this.dateDesc = dateDesc;
	}

	public String getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	public List<FacilityUtilizationRateTimeDto> getRateList()
	{
		return rateList;
	}

	public void setRateList(List<FacilityUtilizationRateTimeDto> rateList)
	{
		this.rateList = rateList;
	}

	public String getUpdatedTime()
	{
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime)
	{
		this.updatedTime = updatedTime;
	}

}