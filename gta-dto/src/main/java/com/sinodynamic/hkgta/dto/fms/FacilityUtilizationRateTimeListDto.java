package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.List;


public class FacilityUtilizationRateTimeListDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String weekDay;
	
	private List<FacilityUtilizationRateTimeDto> rateList;
	
	public String getWeekDay()
	{
		return weekDay;
	}

	public void setWeekDay(String weekDay)
	{
		this.weekDay = weekDay;
	}

	public List<FacilityUtilizationRateTimeDto> getRateList()
	{
		return rateList;
	}

	public void setRateList(List<FacilityUtilizationRateTimeDto> rateList)
	{
		this.rateList = rateList;
	}
}