package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;
import com.sinodynamic.hkgta.entity.crm.Member;

public class CustomerProfileDto extends GenericDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public CustomerProfileDto() {
		super();
	}

	public CustomerProfileDto(String pageNumber, String pageSize,
			String searchText, String sortBy, String isAscending,
			String status, String isMyClient) {
		super();
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.searchText = searchText;
		this.sortBy = sortBy;
		this.isAscending = isAscending;
		this.status = status;
		this.isMyClient = isMyClient;
	}
	
	private String loginUserId;
	@EncryptFieldInfo
	private Long customerId;
	
	private String salutation;

	private String surname;

	private String givenName;

	private String surnameNls;

	private String givenNameNls;

	private String gender;

	private String passportNo;

	private String passportType;

	private String nationality;

	private Date dateOfBirth;

	private String phoneMobile;
	
	private String phoneBusiness;
	
	private String phoneHome;
	
	private String contactEmail;
	
	private String postalDistrict;
	
	private String postalAddress1;
	
	private String postalAddress2;

	private String portraitPhoto;

	private String signature;
	
	private String businessNature;

	private String positionTitle;
	
	private String companyName;

	private String updateBy;

	private String createBy;

	private Date updateDate;

	private Date createDate;
	
	private String salesFollowBy;
	
	private String internalRemark;
	
	private String contactClassCode;
	
	//for list query
    private String pageNumber;
    
    private String pageSize;
    
    private String searchText;
    
    private String sortBy;
    
    private String isAscending;
    
    private String status;
    
    private String isMyClient;
  
    //for register
    private String checkBillAddress;
    
    private String memberType;
    
    private String academyID;
    
    
    private String contactEmail2;
    
    private String maritalStatus;
    private String memberIsVIP;
    
    private Long version;
    
    
	public Long getVersion() {
		return version;
	}

	public void setVersion(Object version) {
		if(version instanceof BigInteger){
			this.version = ((BigInteger) version).longValue();
		}else if(version instanceof Integer){
			this.version = ((Integer) version).longValue();
		}else if(version instanceof String){
			this.version = Long.valueOf((String) version);
		}else{
			this.version = (Long) version;
		}
	}

	public String getContactEmail2() {
		return contactEmail2;
	}

	public void setContactEmail2(String contactEmail2) {
		this.contactEmail2 = contactEmail2;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

	public Long getCustomerId() {
		return customerId;
	}
	@JsonIgnore
	public void setCustomerId2(BigInteger customerId) {
		if(null !=customerId) {
			this.customerId = customerId.longValue();
		}
	}
	public void setCustomerId(Object customerId)
	{
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}
	}
	
	public String getPostalDistrict() {
		return postalDistrict;
	}

	public void setPostalDistrict(String postalDistrict) {
		this.postalDistrict = postalDistrict;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurnameNls() {
		return surnameNls;
	}

	public void setSurnameNls(String surnameNls) {
		this.surnameNls = surnameNls;
	}

	public String getGivenNameNls() {
		return givenNameNls;
	}

	public void setGivenNameNls(String givenNameNls) {
		this.givenNameNls = givenNameNls;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportType() {
		return passportType;
	}

	public void setPassportType(String passportType) {
		this.passportType = passportType;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneMobile() {
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public String getPhoneBusiness() {
		return phoneBusiness;
	}

	public void setPhoneBusiness(String phoneBusiness) {
		this.phoneBusiness = phoneBusiness;
	}

	public String getPhoneHome() {
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getPostalAddress1() {
		return postalAddress1;
	}

	public void setPostalAddress1(String postalAddress1) {
		this.postalAddress1 = postalAddress1;
	}

	public String getPostalAddress2() {
		return postalAddress2;
	}

	public void setPostalAddress2(String postalAddress2) {
		this.postalAddress2 = postalAddress2;
	}

	public String getPortraitPhoto() {
		return portraitPhoto;
	}

	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getBusinessNature() {
		return businessNature;
	}

	public void setBusinessNature(String businessNature) {
		this.businessNature = businessNature;
	}

	public String getPositionTitle() {
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle) {
		this.positionTitle = positionTitle;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getSalesFollowBy() {
		return salesFollowBy;
	}

	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}

	public String getInternalRemark() {
		return nvl(internalRemark);
	}

	public void setInternalRemark(String internalRemark) {
		this.internalRemark = nvl(internalRemark);
	}

	public String getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getIsAscending() {
		return isAscending;
	}

	public void setIsAscending(String isAscending) {
		this.isAscending = isAscending;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsMyClient() {
		return isMyClient;
	}

	public void setIsMyClient(String isMyClient) {
		this.isMyClient = isMyClient;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getCheckBillAddress() {
		return checkBillAddress;
	}

	public void setCheckBillAddress(String checkBillAddress) {
		this.checkBillAddress = checkBillAddress;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getAcademyID() {
		return academyID;
	}

	public void setAcademyID(String academyID) {
		this.academyID = academyID;
	}

	public String getContactClassCode() {
		return contactClassCode;
	}

	public void setContactClassCode(String contactClassCode) {
		this.contactClassCode = contactClassCode;
	}
	
	  private  String nvl(String p1) {
		     return (p1==null)? "" : p1;
		  }

	public String getMemberIsVIP() {
		return memberIsVIP;
	}

	public void setMemberIsVIP(String memberIsVIP) {
		this.memberIsVIP = memberIsVIP;
	}
	  
	  
}
