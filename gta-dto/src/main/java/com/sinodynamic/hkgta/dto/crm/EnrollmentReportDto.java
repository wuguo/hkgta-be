package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class EnrollmentReportDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long accNo;
	private Long customerId;
	private Long planNo;
	private Date enrollDate;
	private String planName;
	private String passPeriodType;
	private Long contractLengthMonth;
	private String academyNo;
	private String salutation;
	private String givenName;
	private String surname;
	private String salesFollowBy;
	private BigDecimal orderTotalAmount;
	private String offerCode;

	public Long getAccNo() {
		return accNo;
	}

	public void setAccNo(Long accNo) {
		this.accNo = accNo;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getPlanNo() {
		return planNo;
	}

	public void setPlanNo(Long planNo) {
		this.planNo = planNo;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}
	
	public String getEnrollDateString() {
//		return DateConvertUtil.getYMDDateAndDateDiff(enrollDate);
		return DtoHelper.getYMDDate(enrollDate);
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPassPeriodType() {
		return passPeriodType;
	}

	public void setPassPeriodType(String passPeriodType) {
		this.passPeriodType = passPeriodType;
	}

	public Long getContractLengthMonth() {
		return contractLengthMonth;
	}

	public void setContractLengthMonth(Long contractLengthMonth) {
		this.contractLengthMonth = contractLengthMonth;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSalesFollowBy() {
		return salesFollowBy;
	}

	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}

	public BigDecimal getOrderTotalAmount() {
		return orderTotalAmount;
	}

	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}

	public String getOfferCode() {
		return offerCode;
	}

	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}
}
