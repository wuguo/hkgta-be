package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;

public class ProgramDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7552094569577155405L;
	private String programId;
	private String accessRight;
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getAccessRight() {
		return accessRight;
	}
	public void setAccessRight(String accessRight) {
		this.accessRight = accessRight;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((programId == null) ? 0 : programId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProgramDto other = (ProgramDto) obj;
		if (programId == null) {
			if (other.programId != null)
				return false;
		} else if (!programId.equals(other.programId))
			return false;
		
		return true;
	}
	
}
