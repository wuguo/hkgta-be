package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class RoleMasterDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1288132507831519218L;
	private boolean hasBeenUsed;
	private Long roleId;
	private String roleName;
	private String forUserType;
	private String status;
	
	
	
	public boolean isHasBeenUsed()
	{
		return hasBeenUsed;
	}
	public void setHasBeenUsed(Integer hasBeenUsed)
	{
		this.hasBeenUsed = hasBeenUsed == 1;
	}
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Object roleId) {
		this.roleId = (roleId!=null ? NumberUtils.toLong(roleId.toString()):null);
	}
	
	public String getRoleName()
	{
		return roleName;
	}
	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}
	public String getForUserType()
	{
		return forUserType;
	}
	public void setForUserType(String forUserType)
	{
		this.forUserType = forUserType;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}

}
