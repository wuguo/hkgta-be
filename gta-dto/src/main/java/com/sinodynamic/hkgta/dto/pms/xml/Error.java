package com.sinodynamic.hkgta.dto.pms.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Error")
public class Error {

	@XmlElement(name = "errorMsg", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	private String errorMsg;
	@XmlElement(name = "errorCode", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	private String errorCode;
	@XmlElement(name = "exceptionMsg", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlSchemaType(name = "NCName")
	private String exceptionMsg;

	public Error() {
		super();
	}

	public Error(String errorMsg) {
		super();
		this.errorMsg = errorMsg;
	}

	public Error(String errorMsg, String exceptionMsg) {
		super();
		this.errorMsg = errorMsg;
		this.exceptionMsg = exceptionMsg;
	}

	public Error(String errorMsg, String errorCode, String exceptionMsg) {
		super();
		this.errorMsg = errorMsg;
		this.errorCode = errorCode;
		this.exceptionMsg = exceptionMsg;
	}

	
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	
	public String getExceptionMsg() {
		return exceptionMsg;
	}

	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}

}
