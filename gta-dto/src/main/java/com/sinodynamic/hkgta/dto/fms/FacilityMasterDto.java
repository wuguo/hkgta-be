package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class FacilityMasterDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long facilityNo;
	
	private String facilityName;
	
	private String status;
	
	private String venueCode;
	
	private Long venueFloor;
	
	private String bookingDate;
	
	private Long resvId;
	
	private Long customerId;
	
	private String ballFeedingStatus;
	
	private String updateBy;
	
	private String venueName;
	
	private String facilityType;
	
	private Long startTime;
	
	private Long endTime;
	
	private String facilityAttribute;
	
	private Boolean isSelected;
	
	private String zone;
	
	private Long facilityTimeslotId;

	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Long facilityTimeslotId) {
		this.facilityTimeslotId = facilityTimeslotId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getVenueCode() {
		return venueCode;
	}

	public void setVenueCode(String venueCode) {
		this.venueCode = venueCode;
	}

	public Long getVenueFloor() {
		return venueFloor;
	}

	public void setVenueFloor(Long venueFloor) {
		this.venueFloor = venueFloor;
	}

	public Long getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Long facilityNo) {
		this.facilityNo = facilityNo;
	}


	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Long resvId) {
		this.resvId = resvId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getBallFeedingStatus() {
		return ballFeedingStatus;
	}

	public void setBallFeedingStatus(String ballFeedingStatus) {
		this.ballFeedingStatus = ballFeedingStatus;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}


	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getFacilityAttribute() {
		return facilityAttribute;
	}

	public void setFacilityAttribute(String facilityAttribute) {
		this.facilityAttribute = facilityAttribute;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}
}
