package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class MarkMessageDto implements Serializable {

	private String	customerID;

	private String	customerInput;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCustomerInput() {
		return customerInput;
	}

	public void setCustomerInput(String customerInput) {
		this.customerInput = customerInput;
	}

}
