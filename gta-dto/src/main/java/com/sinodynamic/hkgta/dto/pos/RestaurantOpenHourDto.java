package com.sinodynamic.hkgta.dto.pos;


public class RestaurantOpenHourDto
{
	private String restaurantId;
	
	private String weekday;
	
	private Long openHour;
	
	private Long closeHour;
	
	private Boolean allowOnlineOrder;
	
	private Long allowOrderTimeFrom;
	
	private Long allowOrderTimeTo;

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getWeekday() {
		return weekday;
	}

	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}

	public Long getOpenHour() {
		return openHour;
	}

	public void setOpenHour(Long openHour) {
		this.openHour = openHour;
	}

	public Long getCloseHour() {
		return closeHour;
	}

	public void setCloseHour(Long closeHour) {
		this.closeHour = closeHour;
	}

	public Boolean getAllowOnlineOrder() {
		return allowOnlineOrder;
	}

	public void setAllowOnlineOrder(Boolean allowOnlineOrder) {
		this.allowOnlineOrder = allowOnlineOrder;
	}

	public Long getAllowOrderTimeFrom() {
		return allowOrderTimeFrom;
	}

	public void setAllowOrderTimeFrom(Long allowOrderTimeFrom) {
		this.allowOrderTimeFrom = allowOrderTimeFrom;
	}

	public Long getAllowOrderTimeTo() {
		return allowOrderTimeTo;
	}

	public void setAllowOrderTimeTo(Long allowOrderTimeTo) {
		this.allowOrderTimeTo = allowOrderTimeTo;
	}

	
}
