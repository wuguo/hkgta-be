package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class UpdateRoomTaskDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5122352917019995633L;
	private Long taskId;
	/**
	 * AR-Arrive,CMP-Complete,DND
	 */
	private String action;

	private IbeaconDto[] beacons;

	

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public IbeaconDto[] getBeacons() {
		return beacons;
	}

	public void setBeacons(IbeaconDto[] beacons) {
		this.beacons = beacons;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
