package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class FacilityTimeslotResDto implements Serializable {

	private static final long serialVersionUID = 430887760348794557L;

	private Long resvId;
	private Long facilityTimeslotId;
	private Date beginDatetime;
	private Date endDatetime;

	public Date getBeginDatetime() {
		return beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public Date getEndDatetime()
	{
		return endDatetime;
	}

	public void setEndDatetime(Date endDatetime)
	{
		this.endDatetime = endDatetime;
	}

	public Long getFacilityTimeslotId()
	{
		return facilityTimeslotId;
	}

	public void setFacilityTimeslotId(Object facilityTimeslotId)
	{
		this.facilityTimeslotId = (facilityTimeslotId!=null ? NumberUtils.toLong(facilityTimeslotId.toString()):null);;
	}

	public Long getResvId()
	{
		return resvId;
	}

	public void setResvId(Object resvId)
	{
		this.resvId = (resvId!=null ? NumberUtils.toLong(resvId.toString()):null);
	}

}
