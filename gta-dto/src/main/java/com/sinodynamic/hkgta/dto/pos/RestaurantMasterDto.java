package com.sinodynamic.hkgta.dto.pos;

import com.sinodynamic.hkgta.entity.pos.RestaurantBookingQuota;

import java.util.List;


public class RestaurantMasterDto
{
	private String restaurantId;
	
	private String restaurantName;

	private String cuisineType;
	
	private String location;
	
	private String openHourText;
	
	private String phone;
	
	private Boolean allowOnlineOrder;
	
	private Boolean onlineReservation;
	
	private RestaurantOpenHourDto restaurantOpenHour;
	
	private String fileBasePath;
	
	private List<RestaurantImageDto> restaurantImages;

	private List<RestaurantBookingQuotaDto> restaurantBookingQuotas;

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getRestaurantName() {
		return restaurantName;
	}

	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}

	public String getCuisineType() {
		return cuisineType;
	}

	public void setCuisineType(String cuisineType) {
		this.cuisineType = cuisineType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOpenHourText() {
		return openHourText;
	}

	public void setOpenHourText(String openHourText) {
		this.openHourText = openHourText;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Boolean getAllowOnlineOrder() {
		return allowOnlineOrder;
	}

	public void setAllowOnlineOrder(Boolean allowOnlineOrder) {
		this.allowOnlineOrder = allowOnlineOrder;
	}

	public RestaurantOpenHourDto getRestaurantOpenHour() {
		return restaurantOpenHour;
	}

	public void setRestaurantOpenHour(RestaurantOpenHourDto restaurantOpenHour) {
		this.restaurantOpenHour = restaurantOpenHour;
	}

	public List<RestaurantImageDto> getRestaurantImages() {
		return restaurantImages;
	}

	public String getFileBasePath() {
		return fileBasePath;
	}

	public void setFileBasePath(String fileBasePath) {
		this.fileBasePath = fileBasePath;
	}

	public void setRestaurantImages(List<RestaurantImageDto> restaurantImages) {
		this.restaurantImages = restaurantImages;
	}

	public List<RestaurantBookingQuotaDto> getRestaurantBookingQuotas() {
		return restaurantBookingQuotas;
	}

	public void setRestaurantBookingQuotas(List<RestaurantBookingQuotaDto> restaurantBookingQuotas) {
		this.restaurantBookingQuotas = restaurantBookingQuotas;
	}

	public Boolean getOnlineReservation() {
		return onlineReservation;
	}

	public void setOnlineReservation(Boolean onlineReservation) {
		this.onlineReservation = onlineReservation;
	}

 
}
