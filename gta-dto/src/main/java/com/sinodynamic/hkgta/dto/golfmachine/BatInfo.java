package com.sinodynamic.hkgta.dto.golfmachine;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonSetter;

public class BatInfo implements Serializable {
	
	
	String updateDate;
	Long batNo;
	Long batStat;
	Long timeCount;
	Long ballCount;
	Long errorNo;
	Long useType;
	Long ballBoxStat;
	
	
	
	public BatInfo() {

	}

	public BatInfo(String updateDate, Long batNo, Long batStat, Long timeCount,
			Long ballCount, Long errorNo, Long useType, Long ballBoxStat) {
		super();
		this.updateDate = updateDate;
		this.batNo = batNo;
		this.batStat = batStat;
		this.timeCount = timeCount;
		this.ballCount = ballCount;
		this.errorNo = errorNo;
		this.useType = useType;
		this.ballBoxStat = ballBoxStat;
	}


	public String getUpdateDate() {
		return updateDate;
	}

	@JsonSetter("UpdateDate")	
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public Long getBatNo() {
		return batNo;
	}

	@JsonSetter("BatNo")	
	public void setBatNo(Long batNo) {
		this.batNo = batNo;
	}


	public Long getBatStat() {
		return batStat;
	}
	@JsonSetter("BatStat")
	public void setBatStat(Long batStat) {
		this.batStat = batStat;
	}


	public Long getTimeCount() {
		return timeCount;
	}
	@JsonSetter("TimeCount")
	public void setTimeCount(Long timeCount) {
		this.timeCount = timeCount;
	}


	public Long getBallCount() {
		return ballCount;
	}
	@JsonSetter("BallCount")
	public void setBallCount(Long ballCount) {
		this.ballCount = ballCount;
	}


	public Long getErrorNo() {
		return errorNo;
	}
	@JsonSetter("ErrorNo")
	public void setErrorNo(Long errorNo) {
		this.errorNo = errorNo;
	}


	public Long getUseType() {
		return useType;
	}
	@JsonSetter("UseType")
	public void setUseType(Long useType) {
		this.useType = useType;
	}


	public Long getBallBoxStat() {
		return ballBoxStat;
	}
	@JsonSetter("BallBoxStat")
	public void setBallBoxStat(Long ballBoxStat) {
		this.ballBoxStat = ballBoxStat;
	}	
	
}

