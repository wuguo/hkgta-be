package com.sinodynamic.hkgta.dto.fms;

import java.util.Date;

/**
 * @author Kevin_He
 *
 */
public interface IFacilityTimeslotLogDto {

	Date getBeginDatetime();

	Date getEndDatetime();

	Long getFacilityNo();

	String getRemark();

	String getUpdateBy();
	
	String getStatus();

}
