package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.entity.GenericDto;


public class AdultDependentMemberDto extends GenericDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String academyNo;
	private String memberName;
	private String superiorMemberEmail;
	private Long	customerId;



	public String getSuperiorMemberEmail() {
		return superiorMemberEmail;
	}

	public void setSuperiorMemberEmail(String superiorMemberEmail) {
		this.superiorMemberEmail = superiorMemberEmail;
	}

	public String getAcademyNo() {
		return DtoHelper.nvl(academyNo);
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getMemberName() {
		return DtoHelper.nvl(memberName);
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = Long.parseLong(customerId + "");
	}

	
}
