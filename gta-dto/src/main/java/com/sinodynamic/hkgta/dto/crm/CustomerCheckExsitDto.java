package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class CustomerCheckExsitDto extends GenericDto implements Serializable{

	private static final long serialVersionUID = -8085423820019982178L;

	@EncryptFieldInfo
	private BigInteger customerID;
	
	private String academyID;
	
	private String memberType;
	
	private String status;
	
	private String surname;
	
	private String givenName;
	
	private String salutation;
	
	private String isDeleted;
	
	private String enrollStatus;

	private String preEnrollStatus;
	
	private String followPerson;
	 
	private Long supMemberId;
	
	private String enrollType;
	
	private String button;
	
	private String message;
	
	private String code;
	
	private String passportNo;
	
	private boolean isArchiveMember;
		
	
	public Long getSupMemberId() {
		return supMemberId;
	}
	@JsonIgnore
	public void setSupMemberId(Object supMemberId) {
		this.supMemberId =(supMemberId!=null ? NumberUtils.toLong(supMemberId.toString()):null);
	}

	public BigInteger getCustomerID() {
		return customerID;
	}

	public void setCustomerID(BigInteger customerID) {
		this.customerID = customerID;
	}

	public String getAcademyID() {
		return academyID;
	}

	public void setAcademyID(String academyID) {
		this.academyID = academyID;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "CustomerCheckExsitDto [customerID=" + customerID
				+ ", academyID=" + academyID + ", memberType=" + memberType
				+ ", status=" + status + ", surname=" + surname
				+ ", givenName=" + givenName + ", salutation=" + salutation
				+ "]";
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getEnrollStatus() {
		return enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}

	public String getPreEnrollStatus() {
		return preEnrollStatus;
	}

	public void setPreEnrollStatus(String preEnrollStatus) {
		this.preEnrollStatus = preEnrollStatus;
	}

	public String getFollowPerson() {
		return followPerson;
	}

	public void setFollowPerson(String followPerson) {
		this.followPerson = followPerson;
	}
	public String getEnrollType() {
		return enrollType;
	}
	public void setEnrollType(String enrollType) {
		this.enrollType = enrollType;
	}
	public String getButton() {
		if("1".equals(this.code)){
			button = "";
		}else if("2".equals(this.code)){
			button = "";
		}else if("3".equals(this.code)){
//			if(this.enrollType.endsWith("PM")){
//				button = "Upgrade Membership";
//			}else if(this.enrollType.endsWith("DM")){
//				button = "Import Profile";
//			}
			button = "";
		}else if("4".equals(this.code)){
//			if(this.enrollType.endsWith("PM")){
//				button = "Upgrade Membership";
//			}else if(this.enrollType.endsWith("DM")){
//				button = "Import Profile";
//			}
			button = "";
		}else if("5".equals(this.code)){
			button = "Import Profile";
		}else if("6".equals(this.code)){
			button = "Import Profile";
		}else if("7".equals(this.code)){
			button = "";
		}else if("8".equals(this.code)){
			button = "Import Profile";
		}else if("9".equals(this.code)){
			button = "";
		}else if("10".equals(this.code)){
			if(this.enrollType.endsWith("PM")){
				button = "Upgrade Membership";
			}else if(this.enrollType.endsWith("DM")){
				button = "Import Profile";
			}
		}else if("11".equals(this.code)){
//			if(this.enrollType.endsWith("PM")){
//				if("IPM".equals(this.enrollType)){
//					button = "";
//				}else if("CPM".equals(this.enrollType)){
//					button = "Import Profile";
//				}
//			}else if(this.enrollType.endsWith("DM")){
//				button = "Import Profile";
//			}
			button = "";
		}else if("12".equals(this.code)){
//			button = "Import Profile";
			button = "";
		}else if("13".equals(this.code)){
			button = "";
		}else if("14".equals(this.code)){
//			if(this.enrollType.endsWith("PM")){
//				button = "Upgrade Membership";
//			}else if(this.enrollType.endsWith("DM")){
//				button = "Import Profile";
//			}
			button = "";
		}else if("15".equals(this.code)){
			button = "";
		}else if("16".equals(this.code)){
//			if(this.enrollType.endsWith("PM")){
//				button = "Upgrade Membership";
//			}else if(this.enrollType.endsWith("DM")){
//				button = "Import Profile";
//			}
			button = "";
		}else if("17".equals(this.code)){
			button = "";
		}else if("18".equals(this.code)){
			button = "";
		}else if("19".equals(this.code)){
//			button = "Import Profile";
			button = "";
		}else if("20".equals(this.code)){
//			button = "Import Profile";
			button = "";
		}else if("21".equals(this.code)){
			button = "Import Profile";
		}else if("22".equals(this.code)){
			button = "";
		}else if("23".equals(this.code)){
			button = "";
		}
		else if("24".equals(this.code)){
//			button = "Import Profile";
			button = "";
		}
		else if("25".equals(this.code)){
			button = "Import Profile";
		}
		else if("26".equals(this.code)){
			button = "Import Profile";
		}
		else if("27".equals(this.code)){
			button = "";
		}
		return button;
	}
	public void setButton(String button) {
		this.button = button;
	}
	public String getMessage() {
		message = "";
		if("1".equals(this.code)){
			message = "Active Individual Primary Member";
		}else if("2".equals(this.code)){
			message = "Inactive Individual Primary Member";
		}else if("3".equals(this.code)){
			message = "Active Individual Dependent Member";
		}else if("4".equals(this.code)){
			message = "Inactive Individual Dependent Member";
		}else if("5".equals(this.code)){
			message = "Cancelled/Rejected/Delete Individual Primary Member";
		}else if("6".equals(this.code)){
			message = "Cancelled/Rejected Individual Dependent Member";
		}else if("7".equals(this.code)){
			message = "Enrolling Individual Primary Member";
		}else if("8".equals(this.code)){
			message = "Enrolling Individual Dependent Member";
		}else if("9".equals(this.code)){
			message = "To-Active Individual Primary Member";
		}else if("10".equals(this.code)){
			message = "To-Active Individual Dependent Member";
		}else if("11".equals(this.code)){
			message = "Expired Individual Primary Member.\nThe membership is expired.\nYou may proceed to Renewal for membership renewal.";
		}else if("12".equals(this.code)){
			message = "Expired Individual Dependent Member";
		}else if("13".equals(this.code)){
			message = "Renewing Individual Primary Member";
		}else if("14".equals(this.code)){
			message = "Renewing Individual Dependent Member";
		}else if("15".equals(this.code)){
			message = "To-Active Individual Primary Member";
		}else if("16".equals(this.code)){
			message = "To-Active Individual Dependent Member";
		}else if("17".equals(this.code)){
			message = "Active Corporate Primary Member";
		}else if("18".equals(this.code)){
			message = "Inactive Corporate Primary Member";
		}else if("19".equals(this.code)){
			message = "Active Corporate Dependent Member";
		}else if("20".equals(this.code)){
			message = "Inactive Corporate Dependent Member";
		}else if("21".equals(this.code)){
			message = "Cancelled/Rejected Corporate Primary Member";
		}else if("22".equals(this.code)){
			message = "Enrolling Corporate Primary Member";
		}else if("23".equals(this.code)){
			message = "To-Active Corporate Primary Member";
		}
		else if("24".equals(this.code)){
			message = "The customer profile already exists";
		}
		else if("25".equals(this.code)){
			message = "The customer profile already exists";
		}
		else if("26".equals(this.code)){
			message = "The customer profile already exists";
		}
		else if("27".equals(this.code)){
			message = "The guest customer profile already exists";
		}
		if(!"5".equals(this.getCode()) && !"6".equals(this.getCode()) && !"8".equals(this.getCode()) && !"10".equals(this.getCode()) 
				&& !"21".equals(this.getCode()) && !"11".equals(this.getCode())
				&&!"25".equals(this.getCode())
				&&!"26".equals(this.getCode())){
			message += ".\nPlease close the patron account in admin portal if you want to\n re-enrol this patron.";
		}
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public boolean getIsArchiveMember() {
		return isArchiveMember;
	}
	public void setIsArchiveMember(boolean isArchiveMember) {
		this.isArchiveMember = isArchiveMember;
	}
	
}
