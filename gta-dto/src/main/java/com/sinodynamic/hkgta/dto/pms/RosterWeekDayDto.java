package com.sinodynamic.hkgta.dto.pms;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RosterWeekDayDto {
	private String onDate;
	private boolean offDuty;
	private List<RosterTimelostDto> timeList;

	public String getOnDate() {
		return onDate;
	}

	public void setOnDate(String onDate) {
		this.onDate = onDate;
	}

	public List<RosterTimelostDto> getTimeList() {
		return timeList;
	}

	public void setTimeList(List<RosterTimelostDto> timeList) {
		this.timeList = timeList;
	}

	public boolean isOffDuty() {
		return offDuty;
	}

	public void setOffDuty(boolean offDuty) {
		this.offDuty = offDuty;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
