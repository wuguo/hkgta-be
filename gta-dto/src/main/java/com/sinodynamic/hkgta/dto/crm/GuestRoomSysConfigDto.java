package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class GuestRoomSysConfigDto extends GenericDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String isCheckPush;
	
	private String pushTime;

	public String getIsCheckPush() {
		return isCheckPush;
	}

	public void setIsCheckPush(String isCheckPush) {
		this.isCheckPush = isCheckPush;
	}

	public String getPushTime() {
		return pushTime;
	}

	public void setPushTime(String pushTime) {
		this.pushTime = pushTime;
	}
	
	
}
