package com.sinodynamic.hkgta.dto.memberapp;

import java.util.List;

import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

//modified by Kaster 20160503
public class MemberAppCustomerAdditionInfoDto extends GenericDto{
	//modified by Kaster 20160503 增加@EncryptFieldInfo
	@EncryptFieldInfo
	private Long customerId;
	
	private String portraitPhoto;
	
	public String getPortraitPhoto() {
		return portraitPhoto;
	}
	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}
	private List<CustomerAdditionInfoDto> customerAdditionInfoDtos;
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public List<CustomerAdditionInfoDto> getCustomerAdditionInfoDtos() {
		return customerAdditionInfoDtos;
	}
	public void setCustomerAdditionInfoDtos(
			List<CustomerAdditionInfoDto> customerAdditionInfoDtos) {
		this.customerAdditionInfoDtos = customerAdditionInfoDtos;
	}

}
