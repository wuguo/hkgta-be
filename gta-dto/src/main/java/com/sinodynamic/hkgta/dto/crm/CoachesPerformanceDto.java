package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class CoachesPerformanceDto implements Serializable {

	private String facilityType;// facility type
	private String coachStaffNo;// coach staff no
	private String coachStaffName;// coach staff name
	private String gender;// gender
	private BigInteger studentNum; // no of student
	private BigInteger reservNum;// no of private coaching reservation

	private BigDecimal totalHourCoach;// total hour of private coaching

	private BigDecimal bookAmount;// booking revenue (HK$)

	private BigInteger courseSessionNum;// no of course session

	private BigDecimal totalHourCourse;// total hour of course
	
	
	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getCoachStaffNo() {
		return coachStaffNo;
	}

	public void setCoachStaffNo(String coachStaffNo) {
		this.coachStaffNo = coachStaffNo;
	}

	public String getCoachStaffName() {
		return coachStaffName;
	}

	public void setCoachStaffName(String coachStaffName) {
		this.coachStaffName = coachStaffName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public BigInteger getStudentNum() {
		return studentNum;
	}

	public void setStudentNum(BigInteger studentNum) {
		this.studentNum = studentNum;
	}

	public BigInteger getReservNum() {
		return reservNum;
	}

	public void setReservNum(BigInteger reservNum) {
		this.reservNum = reservNum;
	}

	public BigDecimal getTotalHourCoach() {
		return totalHourCoach;
	}

	public void setTotalHourCoach(BigDecimal totalHourCoach) {
		this.totalHourCoach = totalHourCoach;
	}

	public BigDecimal getBookAmount() {
		return bookAmount;
	}

	public void setBookAmount(BigDecimal bookAmount) {
		this.bookAmount = bookAmount;
	}

	public BigInteger getCourseSessionNum() {
		return courseSessionNum;
	}

	public void setCourseSessionNum(BigInteger courseSessionNum) {
		this.courseSessionNum = courseSessionNum;
	}

	public BigDecimal getTotalHourCourse() {
		return totalHourCourse;
	}

	public void setTotalHourCourse(BigDecimal totalHourCourse) {
		this.totalHourCourse = totalHourCourse;
	}


	

}
