package com.sinodynamic.hkgta.dto.dashboard;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * 
 * @author Kaster 20160414
 *
 */
public class ServicePlanSalesPercentageDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//单个service plan被购买的次数
	private BigInteger num;
	private int planNo;
	private String planName;
	private BigDecimal itemPrice;
	//单个service plan的营业额
	private BigDecimal revenue;
	//单个service plan被购买的次数占所有service plan被购买次数的百分比
	private BigDecimal pctOfNumber;
	//单个service plan的营业额占所有service plan营业总额的百分比
	private BigDecimal pctOfRevenue;
	public BigInteger getNum() {
		return num;
	}
	public void setNum(BigInteger num) {
		this.num = num;
	}
	public int getPlanNo() {
		return planNo;
	}
	public void setPlanNo(int planNo) {
		this.planNo = planNo;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public BigDecimal getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}
	public BigDecimal getRevenue() {
		return revenue;
	}
	public void setRevenue(BigDecimal revenue) {
		this.revenue = revenue;
	}
	public String getPctOfNumber() {
		return pctOfNumber.toString()+"%";
	}
	public void setPctOfNumber(BigDecimal pctOfNumber) {
		this.pctOfNumber = pctOfNumber;
	}
	public String getPctOfRevenue() {
		return pctOfRevenue.toString()+"%";
	}
	public void setPctOfRevenue(BigDecimal pctOfRevenue) {
		this.pctOfRevenue = pctOfRevenue;
	}
	
}
