package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class DeliveryRecordDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String deliveryDate;

	private Long jobId;

	private String monthOfStatement;

	private Long totalCount;

	private Long successCount;

	private Long failureCount;

	private String sendBy;
	
	private Date nativeDeliveryDate;
	
	private Date nativeMonthOfStatement;
	
	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Object jobId) {
		if (jobId instanceof BigInteger) {
			this.jobId = ((BigInteger) jobId).longValue();
		} else if (jobId instanceof Integer) {
			this.jobId = ((Integer) jobId).longValue();
		} else if (jobId instanceof String) {
			this.jobId = Long.valueOf((String) jobId);
		} else {
			this.jobId = (Long) jobId;
		}
	}

	public String getMonthOfStatement() {
		return monthOfStatement;
	}

	public void setMonthOfStatement(String monthOfStatement) {
		this.monthOfStatement = monthOfStatement;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Object totalCount) {
		if (totalCount instanceof BigInteger) {
			this.totalCount = ((BigInteger) totalCount).longValue();
		} else if (totalCount instanceof Integer) {
			this.totalCount = ((Integer) totalCount).longValue();
		} else if (totalCount instanceof String) {
			this.totalCount = Long.valueOf((String) totalCount);
		} else {
			this.totalCount = (Long) totalCount;
		}
	}

	public Long getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(Object successCount) {
		if (successCount instanceof BigInteger) {
			this.successCount = ((BigInteger) successCount).longValue();
		} else if (successCount instanceof Integer) {
			this.successCount = ((Integer) successCount).longValue();
		} else if (successCount instanceof String) {
			this.successCount = Long.valueOf((String) successCount);
		} else if (successCount instanceof BigDecimal) {
			this.successCount = ((BigDecimal) successCount).longValue();
		} else {
			this.successCount = (Long) successCount;
		}
	}

	public Long getFailureCount() {
		return failureCount;
	}

	public void setFailureCount(Object failureCount) {
		if (failureCount instanceof BigInteger) {
			this.failureCount = ((BigInteger) failureCount).longValue();
		} else if (failureCount instanceof Integer) {
			this.failureCount = ((Integer) failureCount).longValue();
		} else if (failureCount instanceof String) {
			this.failureCount = Long.valueOf((String) failureCount);
		} else if (failureCount instanceof BigDecimal) {
			this.failureCount = ((BigDecimal) failureCount).longValue();
		} else {
			this.failureCount = (Long) failureCount;
		}
	}

	public String getSendBy() {
		return sendBy;
	}

	public void setSendBy(String sendBy) {
		this.sendBy= sendBy;
	}

	public Date getNativeDeliveryDate() {
		return nativeDeliveryDate;
	}

	public void setNativeDeliveryDate(Date nativeDeliveryDate) {
		this.nativeDeliveryDate = nativeDeliveryDate;
	}

	public Date getNativeMonthOfStatement() {
		return nativeMonthOfStatement;
	}

	public void setNativeMonthOfStatement(Date nativeMonthOfStatement) {
		this.nativeMonthOfStatement = nativeMonthOfStatement;
	}

	
}
