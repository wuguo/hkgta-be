package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class CancelDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long resvId;
	
	private boolean flag = false;
	//for purchase daypass
	private String purchaser = "MEMBER";
	
	
	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}
	
	public CancelDto() {
	}

	public CancelDto(Long resvId, boolean flag) {
		this.resvId = resvId;
		this.flag = flag;
	}
	
	public CancelDto(Long resvId, boolean flag, String purchaser) {
		this.resvId = resvId;
		this.flag = flag;
		this.purchaser = purchaser;
	}

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Long resvId) {
		this.resvId = resvId;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	
}
