package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

/***
 * houseKeep report dto
 * 
 * @author stephen
 *
 */

public class RoomWeeklyAttendantDto implements Serializable {
	private String date;
	private String duties;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDuties() {
		return duties;
	}
	public void setDuties(String duties) {
		this.duties = duties;
	} 
}
