package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.entity.crm.GlobalParameter;

public class GlobalParameterListDto implements Serializable {
	
	private List<GlobalParameter>  list;
	
	public GlobalParameterListDto(){
		super();
	}

	public List<GlobalParameter> getList() {
		return list;
	}

	public void setList(List<GlobalParameter> list) {
		this.list = list;
	}
	
}
