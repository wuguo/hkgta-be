package com.sinodynamic.hkgta.dto.account;
import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DDAFailReportDto implements Serializable{
	
	private String systemDate;//DDMMYYY
	private String transactionTime;//HHMMSS
	private String bankNo;
	private String customerName;
	private List<DDAFailReportDetailDto> details;
	private int noOfRecord;
	private String totalTransctionAmount;
	
	
	public String getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}
	public String getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getBankNo() {
		return bankNo;
	}
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public List<DDAFailReportDetailDto> getDetails() {
		return details;
	}
	public void setDetails(List<DDAFailReportDetailDto> details) {
		this.details = details;
	}
	public int getNoOfRecord() {
		return noOfRecord;
	}
	public void setNoOfRecord(int noOfRecord) {
		this.noOfRecord = noOfRecord;
	}
	public String getTotalTransctionAmount() {
		return totalTransctionAmount;
	}
	public void setTotalTransctionAmount(String totalTransctionAmount) {
		this.totalTransctionAmount = totalTransctionAmount;
	}
	/*@Override
	public String toString() {
		return "DDAFailReport [systemDate=" + systemDate + ", transactionTime="
				+ transactionTime + ", bankNo=" + bankNo + ", customerName="
				+ customerName + ", details=" + details + ", noOfRecord="
				+ noOfRecord + ", totalTransctionAmount="
				+ totalTransctionAmount + "]";
	}*/
	

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
