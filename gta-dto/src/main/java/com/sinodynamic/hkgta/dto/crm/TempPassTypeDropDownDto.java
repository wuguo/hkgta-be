package com.sinodynamic.hkgta.dto.crm;

import java.util.Date;

public class TempPassTypeDropDownDto extends DropDownDto
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7957087913210204709L;
	
	private Date startDate;
	
	private Date expiryDate;

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getExpiryDate()
	{
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate)
	{
		this.expiryDate = expiryDate;
	}
}
