package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;
import java.util.List;

public class SpaCategoryResponseDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long latestUpdatedPicId;
	
	private List<SpaCategoryDto> spaCategory;

	public Long getLatestUpdatedPicId() {
		return latestUpdatedPicId;
	}

	public void setLatestUpdatedPicId(Long latestUpdatedPicId) {
		this.latestUpdatedPicId = latestUpdatedPicId;
	}

	public List<SpaCategoryDto> getSpaCategory() {
		return spaCategory;
	}

	public void setSpaCategory(List<SpaCategoryDto> spaCategory) {
		this.spaCategory = spaCategory;
	}
	
	
	
	
}
