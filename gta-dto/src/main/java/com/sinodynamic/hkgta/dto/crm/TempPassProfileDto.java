package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class TempPassProfileDto implements Serializable
{

	private static final long serialVersionUID = -1371912768347486929L;

	private Integer contractorId;
	private String surname;
	private String givenName;
	private String memberName;
	private Character gender;
	private String passportNo;
	private String passportType;
	private String phoneMobile;

	private String phoneHome;
	private Date activationDate;
	private Date periodTo;

	private String referral;
	private String updateBy;
	private Date updateDate;

	private String passType;
	private String companyName;
	private String contactEmail;
	private String cardId;
	private String status;
	private String statusValue;
	private String internalRemark;
	private Date createDate;
	private String cardStatus;
	

	public String getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}

	public Integer getContractorId()
	{
		return contractorId;
	}

	public void setContractorId(Integer contractorId)
	{
		this.contractorId = contractorId;
	}

	public String getInternalRemark()
	{
		return internalRemark;
	}

	public void setInternalRemark(String internalRemark)
	{
		this.internalRemark = internalRemark;
	}

	public String getReferral()
	{
		return referral;
	}

	public void setReferral(String referral)
	{
		this.referral = referral;
	}

	public String getActivationDate()
	{
		if (null == activationDate) {
			return "";
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
		return df.format(activationDate);
	}

	public void setActivationDate(Date activationDate)
	{
		this.activationDate = activationDate;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public String getGivenName()
	{
		return givenName;
	}

	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}

	public Character getGender()
	{
		return gender;
	}

	public void setGender(Character gender)
	{
		this.gender = gender;
	}

	public String getPassportNo()
	{
		return passportNo;
	}

	public void setPassportNo(String passportNo)
	{
		this.passportNo = passportNo;
	}

	public String getPassportType()
	{
		return passportType;
	}

	public void setPassportType(String passportType)
	{
		this.passportType = passportType;
	}

	public String getPhoneMobile()
	{
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile)
	{
		this.phoneMobile = phoneMobile;
	}

	public String getPhoneHome()
	{
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome)
	{
		this.phoneHome = phoneHome;
	}

	public String getPeriodTo()
	{
		if (null == activationDate) {
			return "";
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
		return df.format(periodTo);
	}

	public void setPeriodTo(Date periodTo)
	{
		this.periodTo = periodTo;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public String getPassType()
	{
		return passType;
	}

	public void setPassType(String passType)
	{
		this.passType = passType;
	}

	public String getCompanyName()
	{
		return companyName;
	}

	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	public String getContactEmail()
	{
		return contactEmail;
	}

	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = contactEmail;
	}

	public String getCardId()
	{
		return cardId;
	}

	public void setCardId(String cardId)
	{
		this.cardId = cardId;
	}

	public String getCreateDate() {
		return DtoHelper.getYMDDateTimeAndDateDiff(createDate);
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCardStatus() {
	    return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
	    this.cardStatus = cardStatus;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	
}
