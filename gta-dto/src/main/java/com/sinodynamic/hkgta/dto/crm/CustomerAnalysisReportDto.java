package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class CustomerAnalysisReportDto  implements Serializable {
	private static final long serialVersionUID = 1L;

	private String captionId;


	private String customerInput;


	private String category;

	private String caption;
	
	private BigInteger countNum;


	public String getCustomerInput() {
		return this.customerInput;
	}

	public void setCustomerInput(String customerInput) {
		this.customerInput = customerInput;
	}


	public String getCaptionId() {
		return captionId;
	}

	public void setCaptionId(Object captionId) {
		if (captionId instanceof Integer) {
			this.captionId = String.valueOf((Integer) captionId);
		} else {
			this.captionId = (String) captionId;
		}
	}


	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public BigInteger getCountNum() {
		return countNum;
	}

	public void setCountNum(BigInteger countNum) {
		this.countNum = countNum;
	}



	
}