package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class RenewalDto  extends GenericDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3677193570112734997L;
	
	 private Long servicePlanNo;
	 @EncryptFieldInfo
	 private Long customerId;
	 

	 
	 private Long orderNo;
	 

	 
	 private String createBy;



	 private Long accNo;
	 
	 private String userName;



	 public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public RenewalDto (){
		 
	 }
	 
	
	
	public Long getCustomerId() {
		return customerId;
	}


	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Long getServicePlanNo() {
		return servicePlanNo;
	}


	public void setServicePlanNo(Long servicePlanNo) {
		this.servicePlanNo = servicePlanNo;
	}


	public Long getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}


	public Long getAccNo() {
		return accNo;
	}


	public void setAccNo(Long accNo) {
		this.accNo = accNo;
	}



	public String getCreateBy() {
		return createBy;
	}


	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

}
