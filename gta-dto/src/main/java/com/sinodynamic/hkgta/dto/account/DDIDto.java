package com.sinodynamic.hkgta.dto.account;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DDIDto implements Serializable {
	
	private Long customerOrderTransNo;
	
	private Long customerId;
	
	private BigDecimal amount;

	private String debtorBankCode;

	private String debtorAccountNumber;

	private String debtorAccountName;
	
	private String reference;

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId)
	{
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}
	}
	public Long getCustomerOrderTransNo() {
		return customerOrderTransNo;
	}

	public void setCustomerOrderTransNo(Long customerOrderTransNo) {
		this.customerOrderTransNo = customerOrderTransNo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDebtorBankCode() {
		return debtorBankCode;
	}

	public void setDebtorBankCode(String debtorBankCode) {
		this.debtorBankCode = debtorBankCode;
	}

	public String getDebtorAccountNumber() {
		return debtorAccountNumber;
	}

	public void setDebtorAccountNumber(String debtorAccountNumber) {
		this.debtorAccountNumber = debtorAccountNumber;
	}

	public String getDebtorAccountName() {
		return debtorAccountName;
	}

	public void setDebtorAccountName(String debtorAccountName) {
		this.debtorAccountName = debtorAccountName;
	}

}
