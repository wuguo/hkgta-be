package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Kevin_Liang
 *
 */
public class CheckHotelAvailableDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7491640511763556609L;
	private String stayBeginDate;
	private String stayEndDate;
	private Integer adultCount;
	private Integer childCount;

	public String getStayBeginDate() {
		return stayBeginDate;
	}

	public void setStayBeginDate(String stayBeginDate) {
		this.stayBeginDate = stayBeginDate;
	}

	public String getStayEndDate() {
		return stayEndDate;
	}

	public void setStayEndDate(String stayEndDate) {
		this.stayEndDate = stayEndDate;
	}

	

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	public Integer getChildCount() {
		return childCount;
	}

	

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public CheckHotelAvailableDto(String stayBeginDate, String stayEndDate, Integer adultCount, Integer childCount)
	{
		this.stayBeginDate = stayBeginDate;
		this.stayEndDate = stayEndDate;
		this.adultCount = adultCount;
		this.childCount = childCount;
	}
	
	public CheckHotelAvailableDto()
	{		
	}
}
