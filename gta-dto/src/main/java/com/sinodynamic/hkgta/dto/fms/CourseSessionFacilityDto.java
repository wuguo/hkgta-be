package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigInteger;

import org.apache.commons.lang.math.NumberUtils;

public class CourseSessionFacilityDto implements Serializable{

	private Long sysId;
	private Long facilityTimeslotId;
	private String facilityNo;
	public Long getSysId() {
		return sysId;
	}
	public void setSysId(Object sysId) {
		this.sysId = (sysId!=null ? NumberUtils.toLong(sysId.toString()):null);
	}
	public Long getFacilityTimeslotId() {
		return facilityTimeslotId;
	}
	public void setFacilityTimeslotId(Object facilityTimeslotId) {
		this.facilityTimeslotId = (facilityTimeslotId!=null ? NumberUtils.toLong(facilityTimeslotId.toString()):null);
	}
	public String getFacilityNo() {
		return facilityNo;
	}
	public void setFacilityNo(String facilityNo) {
		this.facilityNo = facilityNo;
	}
	
}
