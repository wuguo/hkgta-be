package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class CustomerTransforListDto extends GenericDto  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public CustomerTransforListDto() {
		super();
	}

	@EncryptFieldInfo
	private BigInteger customerId;
	private String academyNo;
	private String memberName;
	private BigInteger   enrollId;
	private Date enrollDateDB;
	private String memberType;
	private String contactEmail;
	private String salesFollowBy;
	private String status;
	private Date enrollCreateDate;



	public BigInteger getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = (BigInteger) (customerId !=null ? customerId : 0);
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}




	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}


	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public BigInteger getEnrollId() {
		return enrollId;
	}

	public void setEnrollId(BigInteger enrollId) {
		this.enrollId = enrollId;
	}

	public Date getEnrollDateDB() {
		return enrollDateDB;
	}

	public void setEnrollDateDB(Date enrollDateDB) {
		this.enrollDateDB = enrollDateDB;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getSalesFollowBy() {
		return salesFollowBy;
	}

	public void setSalesFollowBy(String salesFollowBy) {
		this.salesFollowBy = salesFollowBy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getEnrollCreateDate() {
		return enrollCreateDate;
	}

	public void setEnrollCreateDate(Date enrollCreateDate) {
		this.enrollCreateDate = enrollCreateDate;
	}


	
}