package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class MemberLimitRuleDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String rightCode;
	private String inputValue;
	private String rightType;
	private String description;
	public String getRightCode() {
		return rightCode;
	}
	public void setRightCode(String rightCode) {
		this.rightCode = rightCode;
	}
	public String getInputValue() {
		return inputValue;
	}
	public void setInputValue(String inputValue) {
		this.inputValue = inputValue;
	}
	public String getRightType() {
		return rightType;
	}
	public void setRightType(String rightType) {
		this.rightType = rightType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
