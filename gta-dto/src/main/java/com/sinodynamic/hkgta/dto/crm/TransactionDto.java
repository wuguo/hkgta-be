package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class TransactionDto implements Serializable {
	/**
	 * add dto of transaction for commit
	 */
	private static final long serialVersionUID = 327791100997292676L;
	
	
	private String transactionDate;
	private String transactionNo;
	private String transactionType;
	/***
	 * FE display code
	 */
	private String paymentMethodCode;
	
	/***
	 * filter code 
	 */
	private String methodCode;
	
	private String patronId;
	private String createBy;
	private String description;
	private String memberName;
	private BigDecimal paidAmount;
	private String internalRemark;
	private String itemCatagory;
	private String orderNo;
	private String paidmentLocationCode;
	private Date transactionTimestamp;
	

	public String getMethodCode() {
		return methodCode;
	}
	public void setMethodCode(String methodCode) {
		this.methodCode = methodCode;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getPaidmentLocationCode() {
		return paidmentLocationCode;
	}
	public void setPaidmentLocationCode(String paidmentLocationCode) {
		this.paidmentLocationCode = paidmentLocationCode;
	}
	public String getInternalRemark() {
		return internalRemark;
	}
	public void setInternalRemark(String internalRemark) {
		this.internalRemark = internalRemark;
	}
	public String getPatronId() {
		return patronId;
	}
	public void setPatronId(String patronId) {
		this.patronId = patronId;
	}


	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getItemCatagory() {
		return itemCatagory;
	}
	public void setItemCatagory(String itemCatagory) {
		this.itemCatagory = itemCatagory;
	}
	public Date getTransactionTimestamp() {
		return transactionTimestamp;
	}
	public void setTransactionTimestamp(Date transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
 

	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}
	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Object paidAmount) {
		if (paidAmount instanceof BigInteger) {
			this.paidAmount = BigDecimal.valueOf(Double.valueOf(paidAmount.toString()));
		}else if(paidAmount instanceof String){
			this.paidAmount = BigDecimal.valueOf(Double.valueOf(paidAmount.toString()));
		}else {
			this.paidAmount = (BigDecimal) paidAmount;
		}
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
 
  
	 
	 
}
