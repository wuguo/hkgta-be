package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class PurchasedDaypassBackDto   extends GenericDto   implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2298867123165328981L;

	private Integer pageSize;
    
    private String show;
    
    private  String order;
	
	private  String propertyName;
	@EncryptFieldInfo
	private   java.math.BigInteger customerId;   
	
	private String contactEmail;
	private String memberBuy;
	private Integer pageNumber;

    private java.math.BigInteger orderNo;
    
//    private BigDecimal orderTotalAmount;

	private java.math.BigInteger orderTotalAmount;
	private String planName;
    
    private String orderDate;
    
    private String effectiveEndDate;
    private String effectiveStartDate;
	
	private String memberName;
	private String orderStatus;
	
	private String payed = "N";
	
	public String getPayed() {
		return payed;
	}

	public void setPayed(String payed) {
		this.payed = payed;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	

	public java.math.BigInteger getCustomerId() {
		return customerId;
	}

	public String getMemberBuy() {
		return memberBuy;
	}

	public void setMemberBuy(String memberBuy) {
		this.memberBuy = memberBuy;
	}

	public void setCustomerId(java.math.BigInteger customerId) {
		this.customerId = customerId;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
                
    public String getShow() {
		return show;
	}

	public void setShow(String show) {
		this.show = show;
	}

	


	

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public java.math.BigInteger getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(java.math.BigInteger orderNo) {
		this.orderNo = orderNo;
	}

	public java.math.BigInteger getOrderTotalAmount() {
		return orderTotalAmount;
	}

	public void setOrderTotalAmount(java.math.BigInteger orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}

	
	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}


	
    
    public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}


	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	
}
