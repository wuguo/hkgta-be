package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.util.List;

public class SecurityQuestionDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String userId;
	
	private List<QuestionAndAnswer> answers;
	
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public List<QuestionAndAnswer> getAnswers()
	{
		return answers;
	}

	public void setAnswers(List<QuestionAndAnswer> answers)
	{
		this.answers = answers;
	}
			
}