package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.math.NumberUtils;

public class MonthlyPrivateCoachingRevenueDto implements Serializable {

	private static final long serialVersionUID = 8473462147618601174L;

	private Long transId;
	private String transDate;
	private Long resvId;
	private String resvDate;
	private String academyId;
	private String patronName;
	private String coachId;
	private String coachName;
	private String facilityType;
	private String paymentMethod;
	private String paymentMedia;
	private String location;
	private String status;
	private Long qty;
	private BigDecimal transAmount;

	public Long getTransId() {
		return transId;
	}

	public void setTransId(Object transId) {
		this.transId = (transId != null ? NumberUtils.toLong(transId.toString()) : null);
	}

	public String getTransDate() {
		return transDate;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Object resvId) {
		this.resvId = (resvId != null ? NumberUtils.toLong(resvId.toString()) : null);
	}

	public String getResvDate() {
		return resvDate;
	}

	public void setResvDate(String resvDate) {
		this.resvDate = resvDate;
	}

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getCoachId() {
		return coachId;
	}

	public void setCoachId(String coachId) {
		this.coachId = coachId;
	}

	public String getCoachName() {
		return coachName;
	}

	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentMedia() {
		return paymentMedia;
	}

	public void setPaymentMedia(String paymentMedia) {
		this.paymentMedia = paymentMedia;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getQty() {
		return qty;
	}

	public void setQty(Object qty) {
		this.qty = (qty != null ? NumberUtils.toLong(qty.toString()) : null);
	}

	public BigDecimal getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(BigDecimal transAmount) {
		this.transAmount = transAmount;
	}

}
