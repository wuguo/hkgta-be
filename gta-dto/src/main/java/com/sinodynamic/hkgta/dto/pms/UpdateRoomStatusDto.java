package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Kevin_Liang
 *
 */
public class UpdateRoomStatusDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6178190141671328066L;
	private String userId;
	private String roleName;
	private String roomNo;
	/**
	 * <!-- HKStatus: 'NEEDS_INSPECTION', --> <!-- 'OCCUPIED_CLEAN',
	 * 'VACANT_CLEAN' --> <!-- 'OCCUPIED_DIRTY', 'VACANT_DIRTY' --> <!--
	 * 'OFF_MARKET' - OOO > would turn D after remove or add, --> <!--
	 * 'OUT_OF_ORDER' - OOS > would turn D after remove, -->
	 */
	private String status;

	private String reasonCode;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
