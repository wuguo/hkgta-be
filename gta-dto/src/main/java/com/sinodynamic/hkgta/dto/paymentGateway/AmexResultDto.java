package com.sinodynamic.hkgta.dto.paymentGateway;

public class AmexResultDto {
    String title;           
    String againLink;       
	
    String amount;          
    String locale;          
    String batchNo;         
    String command;         
    String message;         
    String version;         
    String cardType;        
    String orderInfo;       
    String receiptNo;       
    String merchantID;      
    String merchTxnRef;     
    String authorizeID;     
    String transactionNo;   
    String acqResponseCode; 
    String txnResponseCode; 

    // CSC Receipt Data
    String cscResultCode;
    String ACQCSCRespCode;
    
    // AVS Receipt Data
    String avsResultCode;
    String ACQAVSRespCode;
    
    String txnResponseCodeDesc = "";
    String cscResultCodeDesc = "";
    String avsResultCodeDesc = "";
	
    // Capture Data
    String shopTransNo;
    String authorisedAmount;
    String capturedAmount;
    String refundedAmount;
    String ticketNumber;
    
    // Specific QueryDR Data
    String drExists;
    String multipleDRs;
    
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAgainLink() {
		return againLink;
	}
	public void setAgainLink(String againLink) {
		this.againLink = againLink;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getOrderInfo() {
		return orderInfo;
	}
	public void setOrderInfo(String orderInfo) {
		this.orderInfo = orderInfo;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	public String getMerchTxnRef() {
		return merchTxnRef;
	}
	public void setMerchTxnRef(String merchTxnRef) {
		this.merchTxnRef = merchTxnRef;
	}
	public String getAuthorizeID() {
		return authorizeID;
	}
	public void setAuthorizeID(String authorizeID) {
		this.authorizeID = authorizeID;
	}
	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	public String getAcqResponseCode() {
		return acqResponseCode;
	}
	public void setAcqResponseCode(String acqResponseCode) {
		this.acqResponseCode = acqResponseCode;
	}
	public String getTxnResponseCode() {
		return txnResponseCode;
	}
	public void setTxnResponseCode(String txnResponseCode) {
		this.txnResponseCode = txnResponseCode;
	}
	public String getCscResultCode() {
		return cscResultCode;
	}
	public void setCscResultCode(String cscResultCode) {
		this.cscResultCode = cscResultCode;
	}
	public String getACQCSCRespCode() {
		return ACQCSCRespCode;
	}
	public void setACQCSCRespCode(String aCQCSCRespCode) {
		ACQCSCRespCode = aCQCSCRespCode;
	}
	public String getAvsResultCode() {
		return avsResultCode;
	}
	public void setAvsResultCode(String avsResultCode) {
		this.avsResultCode = avsResultCode;
	}
	public String getACQAVSRespCode() {
		return ACQAVSRespCode;
	}
	public void setACQAVSRespCode(String aCQAVSRespCode) {
		ACQAVSRespCode = aCQAVSRespCode;
	}
	public String getTxnResponseCodeDesc() {
		return txnResponseCodeDesc;
	}
	public void setTxnResponseCodeDesc(String txnResponseCodeDesc) {
		this.txnResponseCodeDesc = txnResponseCodeDesc;
	}
	public String getCscResultCodeDesc() {
		return cscResultCodeDesc;
	}
	public void setCscResultCodeDesc(String cscResultCodeDesc) {
		this.cscResultCodeDesc = cscResultCodeDesc;
	}
	public String getAvsResultCodeDesc() {
		return avsResultCodeDesc;
	}
	public void setAvsResultCodeDesc(String avsResultCodeDesc) {
		this.avsResultCodeDesc = avsResultCodeDesc;
	}
	public String getShopTransNo() {
		return shopTransNo;
	}
	public void setShopTransNo(String shopTransNo) {
		this.shopTransNo = shopTransNo;
	}
	public String getAuthorisedAmount() {
		return authorisedAmount;
	}
	public void setAuthorisedAmount(String authorisedAmount) {
		this.authorisedAmount = authorisedAmount;
	}
	public String getCapturedAmount() {
		return capturedAmount;
	}
	public void setCapturedAmount(String capturedAmount) {
		this.capturedAmount = capturedAmount;
	}
	public String getRefundedAmount() {
		return refundedAmount;
	}
	public void setRefundedAmount(String refundedAmount) {
		this.refundedAmount = refundedAmount;
	}
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getDrExists() {
		return drExists;
	}
	public void setDrExists(String drExists) {
		this.drExists = drExists;
	}
	public String getMultipleDRs() {
		return multipleDRs;
	}
	public void setMultipleDRs(String multipleDRs) {
		this.multipleDRs = multipleDRs;
	}	
    

}
