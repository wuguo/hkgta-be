package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class AreaAccessRight implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3510295717532570741L;
	
	private Long terminalId;
	
	private String terminalName;
	
	private String permission;
	
	public Long getTerminalId()
	{
		return terminalId;
	}

	public void setTerminalId(Long terminalId)
	{
		this.terminalId = terminalId;
	}

	public String getTerminalName()
	{
		return terminalName;
	}

	public void setTerminalName(String terminalName)
	{
		this.terminalName = terminalName;
	}

	public String getPermission()
	{
		return permission;
	}

	public void setPermission(String permission)
	{
		this.permission = permission;
	}

}
