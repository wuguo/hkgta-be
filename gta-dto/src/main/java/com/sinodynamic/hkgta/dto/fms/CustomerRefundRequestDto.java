package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class CustomerRefundRequestDto extends GenericDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1686367286687075044L;
	private Long refundId;
	@EncryptFieldInfo 
	private Long customerId;
	private String requesterUserId;
	private Long refundTransactionNo;
	private Long newTransactionNo;
	private Date requestDate;
	private String refundFrom;
	private String refundType;
	private String memberName;
	private String status;
	private BigDecimal paidAmt;
	private BigDecimal requestAmt;
	private BigDecimal approvedAmt;
	private String remark;
	private String bookingDate;
	private String time;
	private String academyNo;
	private String bayType;
	private String ageRange;
	private String ofBay;
	private String refundMoneyType;
	private String approvedBy;
	private String refNo;
	private String refundMethod;

	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Object refundId) {
		this.refundId = Long.parseLong(refundId.toString());
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = Long.parseLong(customerId.toString());
	}

	public String getRequesterUserId() {
		return requesterUserId;
	}

	public void setRequesterUserId(String requesterUserId) {
		this.requesterUserId = requesterUserId;
	}

	public Long getRefundTransactionNo() {
		return refundTransactionNo;
	}

	public void setRefundTransactionNo(Object refundTransactionNo) {
		this.refundTransactionNo = Long.parseLong(refundTransactionNo.toString());
	}

	public Long getNewTransactionNo() {
		return newTransactionNo;
	}

	public void setNewTransactionNo(Object newTransactionNo) {
		this.newTransactionNo = Long.parseLong(newTransactionNo == null ? "0": newTransactionNo.toString());
	}

	public String getRequestDate() {
		return DtoHelper.getObliqueYMDDateAndDateDiff(requestDate,
				"yyyy/MMM/dd HH:mm");
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getRefundFrom() {
		return refundFrom;
	}

	public void setRefundFrom(String refundFrom) {
		this.refundFrom = refundFrom;
	}

	public String getRefundTpye() {
		return refundType;
	}

	public void setRefundTpye(String refundType) {
		this.refundType = refundType;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getPaidAmt() {
		return paidAmt;
	}

	public void setPaidAmt(BigDecimal paidAmt) {
		this.paidAmt = paidAmt;
	}

	public BigDecimal getRequestAmt() {
		return requestAmt;
	}

	public void setRequestAmt(BigDecimal requestAmt) {
		this.requestAmt = requestAmt;
	}

	public BigDecimal getApprovedAmt() {
		return approvedAmt;
	}

	public void setApprovedAmt(BigDecimal approvedAmt) {
		this.approvedAmt = approvedAmt;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getBayType() {
		return bayType;
	}

	public void setBayType(String bayType) {
		this.bayType = bayType;
	}

	public String getAgeRange() {
		return ageRange;
	}

	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}

	public String getOfBay() {
		return ofBay;
	}

	public void setOfBay(String ofBay) {
		this.ofBay = ofBay;
	}

	public String getRefundMoneyType() {
		return refundMoneyType;
	}

	public void setRefundMoneyType(String refundMoneyType) {
		this.refundMoneyType = refundMoneyType;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getRefundMethod() {
		return refundMethod;
	}

	public void setRefundMethod(String refundMethod) {
		this.refundMethod = refundMethod;
	}

}
