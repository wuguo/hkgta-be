package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;

public class UpdateServiceAccountStatusDto implements Serializable {
       
	    int accNo;
        
        String status;

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public int getAccNo() {
			return accNo;
		}

		public void setAccNo(int accNo) {
			this.accNo = accNo;
		}
}
