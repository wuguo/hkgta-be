package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;

public class RoomHousekeepTaskQueryDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String roomNo;

	private Long taskId;

	private Long roomId;

	private String roomStatus;
	
	private String frontdeskStatus;

	private String jobType;

	private String taskStatus;

	private Date beginDate;

	private Date endDate;

	private Date startTimestamp;

	private Date finishTimestamp;

	private String taskDescription;

	private String staffUserId;

	private String crewRoleId;

	private String remark;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Object taskId)
	{
		this.taskId = (taskId!=null ? NumberUtils.toLong(taskId.toString()):null);
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public Date getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(Date startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public Date getFinishTimestamp() {
		return finishTimestamp;
	}

	public void setFinishTimestamp(Date finishTimestamp) {
		this.finishTimestamp = finishTimestamp;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getStaffUserId() {
		return staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}

	public String getCrewRoleId() {
		return crewRoleId;
	}

	public void setCrewRoleId(String crewRoleId) {
		this.crewRoleId = crewRoleId;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getRoomStatus() {
		return roomStatus;
	}

	public void setRoomStatus(String roomStatus) {
		this.roomStatus = roomStatus;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getFrontdeskStatus()
	{
		return frontdeskStatus;
	}

	public void setFrontdeskStatus(String frontdeskStatus)
	{
		this.frontdeskStatus = frontdeskStatus;
	}
}
