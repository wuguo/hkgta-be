package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;

public class AssignRoleDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8051823571237608435L;
	private String userId;
	private String roleIds;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}
	

}
