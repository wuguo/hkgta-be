/**
 * 
 */
package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * @author jack
 *
 */
public class MembersPresentCurriculumListDto implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4148238137887813820L;

	private Date beginTime;

	private Date endTime;

	private String category;

	private String courseName;
	

	private String academyNo;
	

	
	
	private String sessionId;
	
	private String sessionNo;
	
	



	
	private BigInteger id;
	
	private String status;
	
	
	private Integer getStatusOrder(String status)
	{

		if (status.equals("RSV"))
		{
			return 1;
		}
		
		if (status.equals("ATD"))
		{
			return 2;
		}
		
		if (status.equals("COMPLETE"))
		{
			return 3;
		}
		
		if (status.equals("CAN"))
		{
			return 4;
		}
		return 0;
	}
	
	private Integer getCategoryOrder(String category)
	{

		if (category.equals("Coaching"))
		{
			return 1;
		}
		
		if (category.equals("Course"))
		{
			return 2;
		}
		
		return 0;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
		if (this.endTime.before(new Date()))
		{
			this.status = "COMPLETE";
		}
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName =StringUtils.isNotEmpty(courseName) ? courseName :"" ;
	}
	
	public String getSessionNo() {
		return sessionNo;
	}

	public void setSessionNo(Object sessionNo) {
		this.sessionNo = sessionNo == null ? "" : sessionNo.toString();
	}


	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}


	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(Object sessionId)
	{
		this.sessionId = sessionId == null ? "" : sessionId.toString();
	}


	public String getAcademyNo()
	{
		return academyNo;
	}

	public void setAcademyNo(String academyNo)
	{
		this.academyNo = academyNo;
	}


	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		if(null == this.status || this.status.isEmpty())
		{
			this.status = status;
		}
	}


}
