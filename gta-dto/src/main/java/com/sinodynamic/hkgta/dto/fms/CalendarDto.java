package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/****
 * GOLF/tennis training calendar dto
 * @author christ
 * @time 2016-04-01
 *
 */
public class CalendarDto implements Serializable {

	/***
	 * time format yyyy-MM-dd
	 */
	private String time;
	
	/***
	 * course booking list
	 */
	private List<CourseCoachCalendarDto> courses;
	
	/***
	 * private coaching list
	 */
	private List<CourseCoachCalendarDto> coachings;

	private Integer courseSum;
	
	private Integer coachSum;
	
	private List<CourseCoachCalendarDto>list;
	
	public Integer getCourseSum() {
		return courseSum;
	}

	public void setCourseSum(Integer courseSum) {
		this.courseSum = courseSum;
	}

	public Integer getCoachSum() {
		return coachSum;
	}

	public void setCoachSum(Integer coachSum) {
		this.coachSum = coachSum;
	}

	public List<CourseCoachCalendarDto> getList() {
		return list;
	}

	public void setList(List<CourseCoachCalendarDto> list) {
		this.list = list;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<CourseCoachCalendarDto> getCourses() {
		return courses;
	}

	public void setCourses(List<CourseCoachCalendarDto> courses) {
		this.courses = courses;
	}

	public List<CourseCoachCalendarDto> getCoachings() {
		return coachings;
	}

	public void setCoachings(List<CourseCoachCalendarDto> coachings) {
		this.coachings = coachings;
	}
	

}
