package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

/**
 * @author Kevin_Liang
 *
 */
public class AvailableFacilityDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8823907542465962613L;
    private String facilityNo;
    private String facilityName;
    private String venueFloor;
    private String status;
	public String getFacilityNo() {
		return facilityNo;
	}
	public void setFacilityNo(String facilityNo) {
		this.facilityNo = facilityNo;
	}
	public String getFacilityName() {
		return facilityName;
	}
	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}
	public String getVenueFloor() {
		return venueFloor;
	}
	public void setVenueFloor(String venueFloor) {
		this.venueFloor = venueFloor;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    
  
}
