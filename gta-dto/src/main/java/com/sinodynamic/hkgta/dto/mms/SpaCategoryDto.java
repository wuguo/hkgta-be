package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;
import java.util.List;

public class SpaCategoryDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String categoryId;
	private String categoryName;
	private List<SpaPicPathDto> categoryPicList;
	
	public SpaCategoryDto()
	{
		
	}

	
	public String getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}


	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<SpaPicPathDto> getCategoryPicList() {
		return categoryPicList;
	}

	public void setCategoryPicList(List<SpaPicPathDto> categoryPicList) {
		this.categoryPicList = categoryPicList;
	}
	
	
}
