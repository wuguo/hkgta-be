package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class ServicePlanPosDto implements Serializable {
	private int servPosId;
	private String description;
	private String weeklyRepeatBit;
	private String posItemNo;
	private PosServiceItemPriceDto posServiceItemPriceDto;
	public int getServPosId() {
		return servPosId;
	}
	public void setServPosId(int servPosId) {
		this.servPosId = servPosId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getWeeklyRepeatBit() {
		return weeklyRepeatBit;
	}
	public void setWeeklyRepeatBit(String weeklyRepeatBit) {
		this.weeklyRepeatBit = weeklyRepeatBit;
	}
	public String getPosItemNo() {
		return posItemNo;
	}
	public void setPosItemNo(String posItemNo) {
		this.posItemNo = posItemNo;
	}
	public PosServiceItemPriceDto getPosServiceItemPriceDto() {
		return posServiceItemPriceDto;
	}
	public void setPosServiceItemPriceDto(
			PosServiceItemPriceDto posServiceItemPriceDto) {
		this.posServiceItemPriceDto = posServiceItemPriceDto;
	}
	
}
