package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;

public class RoomHousekeepTaskDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long taskId;

	private Long[] roomIds;

	private String jobType;

	private String status;

	private String taskDescription;

	private String remark;

	private Date beginDate;

	private Date endDate;

	private Date createDate;

	private String createDateStr;

	/* response */
	private Date startTimestamp;

	private Date finishTimestamp;

	private Boolean noResponse;

	private Date expireTime;

	private Date dndTimestamp;
	
	private List<HousekeepTaskFileDto> taskFileList;

	private String updateBy;

	

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Object taskId)
	{
		this.taskId = (taskId!=null ? NumberUtils.toLong(taskId.toString()):null);
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(Date startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public Date getFinishTimestamp() {
		return finishTimestamp;
	}

	public void setFinishTimestamp(Date finishTimestamp) {
		this.finishTimestamp = finishTimestamp;
	}

	public List<HousekeepTaskFileDto> getTaskFileList() {
		return taskFileList;
	}

	public void setTaskFileList(List<HousekeepTaskFileDto> taskFileList) {
		this.taskFileList = taskFileList;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long[] getRoomIds() {
		return roomIds;
	}

	public void setRoomIds(Long[] roomIds) {
		this.roomIds = roomIds;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public Boolean getNoResponse() {
		return noResponse;
	}

	public void setNoResponse(Boolean noResponse) {
		this.noResponse = noResponse;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getDndTimestamp()
	{
		return dndTimestamp;
	}

	public void setDndTimestamp(Date dndTimestamp)
	{
		this.dndTimestamp = dndTimestamp;
	}
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
