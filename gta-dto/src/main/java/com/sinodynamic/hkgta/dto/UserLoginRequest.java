package com.sinodynamic.hkgta.dto;

import java.io.Serializable;

public class UserLoginRequest implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -263680041047443L;
	
	
	private String username;
	private String password;
	private String validCode;
	private String location;
	
	public String getValidCode()
	{
		return validCode;
	}
	public void setValidCode(String validCode)
	{
		this.validCode = validCode;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getLocation()
	{
		return location;
	}
	public void setLocation(String location)
	{
		this.location = location;
	}
}
