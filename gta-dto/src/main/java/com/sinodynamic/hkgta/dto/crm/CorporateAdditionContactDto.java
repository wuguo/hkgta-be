package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class CorporateAdditionContactDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long contactId;

	private String contactEmail;

	private String contactPersonFirstname;

	private String contactPersonLastname;

	private String contactPhone;

	private String contactPhoneMobile;

	private String contactType;

	private Long corporateId;

	private String district;

	private String postalAddress1;

	private String postalAddress2;

	private Boolean isSameAsInCharge;

	public CorporateAdditionContactDto() {
	}

	public Long getContactId() {
		return this.contactId;
	}

	public void setContactId(Object contactId) {
		this.contactId = (contactId != null ? NumberUtils.toLong(contactId.toString()) : null);
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactPersonFirstname() {
		return this.contactPersonFirstname;
	}

	public void setContactPersonFirstname(String contactPersonFirstname) {
		this.contactPersonFirstname = contactPersonFirstname;
	}

	public String getContactPersonLastname() {
		return this.contactPersonLastname;
	}

	public void setContactPersonLastname(String contactPersonLastname) {
		this.contactPersonLastname = contactPersonLastname;
	}

	public String getContactPhone() {
		return this.contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactPhoneMobile() {
		return this.contactPhoneMobile;
	}

	public void setContactPhoneMobile(String contactPhoneMobile) {
		this.contactPhoneMobile = contactPhoneMobile;
	}

	public String getContactType() {
		return this.contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public Long getCorporateId() {
		return this.corporateId;
	}

	public void setCorporateId(Object corporateId) {
		this.corporateId = (corporateId != null ? NumberUtils.toLong(corporateId.toString()) : null);
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPostalAddress1() {
		return this.postalAddress1;
	}

	public void setPostalAddress1(String postalAddress1) {
		this.postalAddress1 = postalAddress1;
	}

	public String getPostalAddress2() {
		return this.postalAddress2;
	}

	public void setPostalAddress2(String postalAddress2) {
		this.postalAddress2 = postalAddress2;
	}

	public Boolean getIsSameAsInCharge() {
		return isSameAsInCharge;
	}

	public void setIsSameAsInCharge(Boolean isSameAsInCharge) {
		this.isSameAsInCharge = isSameAsInCharge;
	}

}