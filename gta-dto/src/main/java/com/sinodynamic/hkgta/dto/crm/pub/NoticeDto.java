package com.sinodynamic.hkgta.dto.crm.pub;

import java.io.Serializable;
import java.util.Date;

import com.sinodynamic.hkgta.dto.CustomDateTimeSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.sinodynamic.hkgta.dto.CustomDateDeserialize;
import com.sinodynamic.hkgta.dto.CustomDateSerializer;

/**
 * The persistent class for the notice database table.
 * 
 */

public class NoticeDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6983218873589572604L;

	private Long noticeId;

	private String subject;

	private String content;

	private String noticeType;

	private String recipientType;

	private Date noticeBegin;

	private Date noticeEnd;

	private String status;

	private String createBy;

	private String creator;
	
	private Date createDate;

	private String categoryName;
	
	private NoticeFileDto noticeFile;
	
	public Long getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(String noticeType) {
		this.noticeType = noticeType;
	}

	public String getRecipientType() {
		return recipientType;
	}

	public void setRecipientType(String recipientType) {
		this.recipientType = recipientType;
	}

	public Date getNoticeBegin() {
		return noticeBegin;
	}

	public void setNoticeBegin(Date noticeBegin) {
		this.noticeBegin = noticeBegin;
	}

	public Date getNoticeEnd() {
		return noticeEnd;
	}

	public void setNoticeEnd(Date noticeEnd) {
		this.noticeEnd = noticeEnd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	public Date getCreateDate() {
		return createDate;
	}

	@JsonDeserialize(using = CustomDateDeserialize.class) 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public NoticeFileDto getNoticeFile() {
		return noticeFile;
	}

	public void setNoticeFile(NoticeFileDto noticeFile) {
		this.noticeFile = noticeFile;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
}