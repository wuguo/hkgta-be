package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class MemberAccountRightsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7128814071201872582L;
	
	public MemberAccountRightsDto(){
		super();
	}
	
	public Long getCustomerId() {
		return customerId;
	}


	
	private Long customerId;
	private boolean daypassPurchase;
	private String facility;
	private boolean training;
	private boolean events;
	
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public boolean isDaypassPurchase() {
		return daypassPurchase;
	}

	public void setDaypassPurchase(boolean daypassPurchase) {
		this.daypassPurchase = daypassPurchase;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public boolean isTraining() {
		return training;
	}

	public void setTraining(boolean training) {
		this.training = training;
	}

	public boolean isEvents() {
		return events;
	}

	public void setEvents(boolean events) {
		this.events = events;
	}

	public MemberAccountRightsDto(Long customerId) {
		super();
		this.customerId = customerId;
	}
	
	
}
