package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

public class NotificationDto implements Serializable{
	private static final long serialVersionUID = 1L;
	public String deliveryType;
	public String promotionType;
	public Boolean sendToall;
	
	//added by Kaster 20160513 start
	//Corporate Service Plan
	public Integer[] corporatePlanNos;
			
	//Individual Service Plan
	public Integer[] individualPlanNos;
			
	//Patron/DayPassGuest
	public String recipient;
			
	//Primary/Dependent/Both
	public String corporateIsPrimaryOrDependent;
			
	//Active/Inactive/Both
	public String corporateIsActiveOrNot;
			
	//Primary/Dependent/Both
	public String individualIsPrimaryOrDependent;
			
	//Active/Inactive/Both
	public String individualIsActiveOrNot;
			
	//Active/Inactive/Both
	public String dayPassGuestIsActiveOrNot;
			
	//Expiring Period
	public Integer expiringPeriod;
			
	//Gender:M/F/Both
	public String genders;
			
	//Age
	public Integer[] ages; 
	
	public Integer[] getCorporatePlanNos() {
		return corporatePlanNos;
	}
	public void setCorporatePlanNos(Integer[] corporatePlanNos) {
		this.corporatePlanNos = corporatePlanNos;
	}
	public Integer[] getIndividualPlanNos() {
		return individualPlanNos;
	}
	public void setIndividualPlanNos(Integer[] individualPlanNos) {
		this.individualPlanNos = individualPlanNos;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getCorporateIsPrimaryOrDependent() {
		return corporateIsPrimaryOrDependent;
	}
	public void setCorporateIsPrimaryOrDependent(String corporateIsPrimaryOrDependent) {
		this.corporateIsPrimaryOrDependent = corporateIsPrimaryOrDependent;
	}
	public String getCorporateIsActiveOrNot() {
		return corporateIsActiveOrNot;
	}
	public void setCorporateIsActiveOrNot(String corporateIsActiveOrNot) {
		this.corporateIsActiveOrNot = corporateIsActiveOrNot;
	}
	public String getIndividualIsPrimaryOrDependent() {
		return individualIsPrimaryOrDependent;
	}
	public void setIndividualIsPrimaryOrDependent(String individualIsPrimaryOrDependent) {
		this.individualIsPrimaryOrDependent = individualIsPrimaryOrDependent;
	}
	public String getIndividualIsActiveOrNot() {
		return individualIsActiveOrNot;
	}
	public void setIndividualIsActiveOrNot(String individualIsActiveOrNot) {
		this.individualIsActiveOrNot = individualIsActiveOrNot;
	}
	public String getDayPassGuestIsActiveOrNot() {
		return dayPassGuestIsActiveOrNot;
	}
	public void setDayPassGuestIsActiveOrNot(String dayPassGuestIsActiveOrNot) {
		this.dayPassGuestIsActiveOrNot = dayPassGuestIsActiveOrNot;
	}
	public Integer getExpiringPeriod() {
		return expiringPeriod;
	}
	public void setExpiringPeriod(Integer expiringPeriod) {
		this.expiringPeriod = expiringPeriod;
	}
	public String getGenders() {
		return genders;
	}
	public void setGenders(String genders) {
		this.genders = genders;
	}
	public Integer[] getAges() {
		return ages;
	}
	public void setAges(Integer[] ages) {
		this.ages = ages;
	}
	//added by Kaster 20160513 end
	
	//corporate-COR,individual-IND
	public String[] memberType;
	
	//corporate primary member-CPM, corporate dependent member-CDM
	public String[] memberCorType;
	
	//individual primary member-IPM, individual dependent member-IDM
	public String[] memberIndType;
	
	//active-ACT,inactive-INAT
	public String[] memberCorStatus;
	
	//active-ACT,inactive-INAT
    public String[] memberIndStatus;
	
	//day pass-DP,temporary pass-TP
	public String[] passType;
	
	public Boolean dayPass;
	
	public Boolean tempPass;
	
	public String[] servicePlan;
	
	//male-M,female-F
	public String[] gender;
	
	//low to high
	public Integer[] age;
	
	public Integer expireMonth;
	
	//send email
	public String from;
	public String subject;
	public Boolean enableHtml;
	public String content;
	public String[] attachPaths;
	
	public String senderUserId;
	
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}
	public Boolean getSendToall() {
		return sendToall;
	}
	public void setSendToall(Boolean sendToall) {
		this.sendToall = sendToall;
	}
	public String[] getMemberType() {
		return memberType;
	}
	public void setMemberType(String[] memberType) {
		this.memberType = memberType;
	}
	public String[] getMemberCorStatus() {
		return memberCorStatus;
	}
	public void setMemberCorStatus(String[] memberCorStatus) {
		this.memberCorStatus = memberCorStatus;
	}
	public String[] getMemberIndStatus() {
		return memberIndStatus;
	}
	public void setMemberIndStatus(String[] memberIndStatus) {
		this.memberIndStatus = memberIndStatus;
	}
	public String[] getPassType() {
		return passType;
	}
	public void setPassType(String[] passType) {
		this.passType = passType;
	}
	public String[] getServicePlan() {
		return servicePlan;
	}
	public void setServicePlan(String[] servicePlan) {
		this.servicePlan = servicePlan;
	}
	public String[] getGender() {
		return gender;
	}
	public void setGender(String[] gender) {
		this.gender = gender;
	}
	public Integer[] getAge() {
		return age;
	}
	public void setAge(Integer[] age) {
		this.age = age;
	}
	public Integer getExpireMonth() {
		return expireMonth;
	}
	public void setExpireMonth(Integer expireMonth) {
		this.expireMonth = expireMonth;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Boolean getEnableHtml() {
		return enableHtml;
	}
	public void setEnableHtml(Boolean enableHtml) {
		this.enableHtml = enableHtml;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String[] getAttachPaths() {
		return attachPaths;
	}
	public void setAttachPaths(String[] attachPaths) {
		this.attachPaths = attachPaths;
	}
	public String[] getMemberCorType() {
		return memberCorType;
	}
	public void setMemberCorType(String[] memberCorType) {
		this.memberCorType = memberCorType;
	}
	public String[] getMemberIndType() {
		return memberIndType;
	}
	public void setMemberIndType(String[] memberIndType) {
		this.memberIndType = memberIndType;
	}

	public String getSenderUserId() {
		return senderUserId;
	}
	public void setSenderUserId(String senderUserId) {
		this.senderUserId = senderUserId;
	}
	public Boolean getDayPass() {
		return dayPass;
	}
	public void setDayPass(Boolean dayPass) {
		this.dayPass = dayPass;
	}
	public Boolean getTempPass() {
		return tempPass;
	}
	public void setTempPass(Boolean tempPass) {
		this.tempPass = tempPass;

	}
	
}
