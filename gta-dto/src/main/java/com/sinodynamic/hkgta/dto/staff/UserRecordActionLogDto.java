package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class UserRecordActionLogDto implements Serializable {
	
	private String academyId;

	private String tableName;

	private String action;

	private String primaryKeyValue;

	private String beforeImage;

	private Date actionTimestamp;

	private String actionUserId;
	
	private String readType;

	private List<FieldChangeDto> fields;

	public List<FieldChangeDto> getFields() {
		return fields;
	}

	public void setFields(List<FieldChangeDto> fields) {
		this.fields = fields;
	}

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPrimaryKeyValue() {
		return primaryKeyValue;
	}

	public void setPrimaryKeyValue(String primaryKeyValue) {
		this.primaryKeyValue = primaryKeyValue;
	}

	public String getBeforeImage() {
		return beforeImage;
	}

	public void setBeforeImage(String beforeImage) {
		this.beforeImage = beforeImage;
	}

	public Date getActionTimestamp() {
		return actionTimestamp;
	}

	public void setActionTimestamp(Date actionTimestamp) {
		this.actionTimestamp = actionTimestamp;
	}

	public String getActionUserId() {
		return actionUserId;
	}

	public void setActionUserId(String actionUserId) {
		this.actionUserId = actionUserId;
	}

	public String getReadType() {
		return readType;
	}

	public void setReadType(String readType) {
		this.readType = readType;
	}
	
}
