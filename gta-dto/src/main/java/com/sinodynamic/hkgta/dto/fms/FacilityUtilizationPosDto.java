package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


public class FacilityUtilizationPosDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String facilityType;
	
	private Long advancedResPeriod;
	
	private Long facilityPosId;

	private String ageRangeCode;
	
	private String rateType;

	private BigDecimal itemPrice;
	
	private List<FacilityUtilizationPosDto> priceList;
	
	private String updateBy;
	
	public BigDecimal getItemPrice()
	{
		return itemPrice;
	}

	public void setItemPrice(BigDecimal itemPrice)
	{
		this.itemPrice = itemPrice;
	}

	public Long getFacilityPosId()
	{
		return facilityPosId;
	}

	public void setFacilityPosId(Long facilityPosId)
	{
		this.facilityPosId = facilityPosId;
	}

	public Long getAdvancedResPeriod()
	{
		return advancedResPeriod;
	}

	public void setAdvancedResPeriod(Long advancedResPeriod)
	{
		this.advancedResPeriod = advancedResPeriod;
	}

	public String getAgeRangeCode()
	{
		return ageRangeCode;
	}

	public void setAgeRangeCode(String ageRangeCode)
	{
		this.ageRangeCode = ageRangeCode;
	}

	public String getFacilityType()
	{
		return facilityType;
	}

	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}

	public List<FacilityUtilizationPosDto> getPriceList()
	{
		return priceList;
	}

	public void setPriceList(List<FacilityUtilizationPosDto> priceList)
	{
		this.priceList = priceList;
	}

	public String getRateType()
	{
		return rateType;
	}

	public void setRateType(String rateType)
	{
		this.rateType = rateType;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

	
}