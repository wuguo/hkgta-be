package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

public class ServicePlanRightDto implements Serializable{
	private String rightCode;
	private String inputValue;
	private String inputValueType;
	private String rightType;
	private String servicePlanRightMasterDescription;
	private BigDecimal numValue;
	private String textValue;
	private BigDecimal availableBanlance;
	
	public BigDecimal getAvailableBanlance() {
		return availableBanlance;
	}

	public void setAvailableBanlance(BigDecimal availableBanlance) {
		this.availableBanlance = availableBanlance;
	}

	public String getServicePlanRightMasterDescription() {
		return servicePlanRightMasterDescription;
	}

	public void setServicePlanRightMasterDescription(
			String servicePlanRightMasterDescription) {
		this.servicePlanRightMasterDescription = servicePlanRightMasterDescription;
	}

	public String getRightType() {
		return rightType;
	}

	public void setRightType(String rightType) {
		this.rightType = rightType;
	}

	public String getRightCode() {
		return rightCode;
	}

	public void setRightCode(String rightCode) {
		this.rightCode = rightCode;
	}

	public String getInputValue() {
		return inputValue;
	}

	public void setInputValue(String inputValue) {
		this.inputValue = inputValue;
	}

	public String getInputValueType() {
		return inputValueType;
	}

	public void setInputValueType(String inputValueType) {
		this.inputValueType = inputValueType;
	}

	public BigDecimal getNumValue() {
		return numValue;
	}

	public void setNumValue(BigDecimal numValue) {
		this.numValue = numValue;
	}

	public String getTextValue() {
		return textValue;
	}

	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}
	
}
