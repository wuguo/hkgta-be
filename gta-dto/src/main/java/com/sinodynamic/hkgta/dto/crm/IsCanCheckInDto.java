package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class IsCanCheckInDto extends GenericDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private boolean isCanCheckIn;

	public boolean getIsCanCheckIn() {
		return isCanCheckIn;
	}

	public void setIsCanCheckIn(boolean isCanCheckIn) {
		this.isCanCheckIn = isCanCheckIn;
	}

	
	
	
}
