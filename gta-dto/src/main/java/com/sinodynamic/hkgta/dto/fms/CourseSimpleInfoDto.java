package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class CourseSimpleInfoDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long courseId;
    private String courseName;
    private BigDecimal price;
    private String beginDate;
    private String dueDate;
    private Long sessions;
    private Date time;
    private String courseType;
    private String ageRange;

    public Long getCourseId() {
	return courseId;
    }

    public void setCourseId(Object courseId) {
		this.courseId = courseId != null ? NumberUtils.toLong(courseId.toString()) : null;
    }

    public String getCourseName() {
	return courseName;
    }

    public void setCourseName(String courseName) {
	this.courseName = courseName;
    }

    public BigDecimal getPrice() {
	return price;
    }

    public void setPrice(BigDecimal price) {
	this.price = price;
    }

    public String getBeginDate() {
	return beginDate;
    }

    public void setBeginDate(String beginDate) {
	this.beginDate = beginDate;
    }

    public String getDueDate() {
	return dueDate;
    }

    public void setDueDate(String dueDate) {
	this.dueDate = dueDate;
    }

    public Long getSessions() {
	return sessions;
    }

    public void setSessions(Object sessions) {
	this.sessions = (sessions!=null ? NumberUtils.toLong(sessions.toString()):null);
    }

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getCourseType() {
		return courseType;
	}

	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}

	public String getAgeRange() {
		return ageRange;
	}

	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}

}
