package com.sinodynamic.hkgta.dto.membership;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QuestionAnswerOrBirthDateDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String hkid;
	private Date birthDate;
	private String secrectType;
	private String userId;
	private String passportType;
	
	public String getPassportType() {
		return passportType;
	}
	public void setPassportType(String passportType) {
		this.passportType = passportType;
	}
	private List<UserActivateQuestDto> userActivateQuestDtos = new ArrayList<UserActivateQuestDto>();
	
	public String getHkid() {
		return hkid;
	}
	public void setHkid(String hkid) {
		this.hkid = hkid;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getSecrectType() {
		return secrectType;
	}
	public void setSecrectType(String secrectType) {
		this.secrectType = secrectType;
	}
	public List<UserActivateQuestDto> getUserActivateQuestDtos() {
		return userActivateQuestDtos;
	}
	public void setUserActivateQuestDtos(List<UserActivateQuestDto> userActivateQuestDtos) {
		this.userActivateQuestDtos = userActivateQuestDtos;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
