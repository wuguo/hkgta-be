package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class DependentMemberRightsDto extends GenericDto implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8596567985951783310L;
	@EncryptFieldInfo
	private Long customerId;
	private String facilityRight;
	private String trainingRight;
	private String eventRight;
	private String dayPass;
	private BigDecimal tran;
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId= customerId;
	}
	public String getFacilityRight() {
		return facilityRight;
	}
	public void setFacilityRight(String facilityRight) {
		this.facilityRight = facilityRight;
	}
	public String getTrainingRight() {
		return trainingRight;
	}
	public void setTrainingRight(String trainingRight) {
		this.trainingRight = trainingRight;
	}
	public String getEventRight() {
		return eventRight;
	}
	public void setEventRight(String eventRight) {
		this.eventRight = eventRight;
	}
	public String getDayPass() {
		return dayPass;
	}
	public void setDayPass(String dayPass) {
		this.dayPass = dayPass;
	}
	public BigDecimal getTran() {
		return tran;
	}
	public void setTran(BigDecimal tran) {
		this.tran = tran;
	}
}
