package com.sinodynamic.hkgta.dto.carpark;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class CarParkMemberCheckDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String academyNo;
	private Long cardNo;
	private String cardSerialNo;
	private String personName;
	private Long type;
	private String licensePlateNo;
	private String phoneMobile;
	private Date expiryDate;
	
	
	public Long getCardNo() {
		return this.cardNo;
	}

	public void setCardNo(Object cardNo) {
		if (cardNo instanceof Integer) {
			this.cardNo = ((Integer)cardNo).longValue();
		}
		else if (cardNo instanceof BigInteger) {
			this.cardNo = ((BigInteger)cardNo).longValue();
		}
		else if (cardNo instanceof String) {
			this.cardNo = Long.valueOf((String)cardNo);
		}
		else {
		this.cardNo = (Long)cardNo;
		}
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getCardSerialNo() {
		return cardSerialNo;
	}

	public void setCardSerialNo(String cardSerialNo) {
		this.cardSerialNo = cardSerialNo;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Long getType() {
		return type;
	}

	public void setType(Object type) {
		if (type instanceof Long) {
			this.type = (Long)type;
		}
		else if (type instanceof BigInteger) {
			this.type = ((BigInteger)type).longValue();
		}
		else if (type instanceof String) {
			this.type = Long.valueOf((String)type);
		}
		else {
		this.type = (Long)type;
		}
	}
	
	public String getLicencePlateNo() {
		return licensePlateNo;
	}

	public void setLicencePlateNo(String licensePlateNo) {
		this.licensePlateNo = licensePlateNo;
	}

	public String getPhoneMobile() {
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}



}
