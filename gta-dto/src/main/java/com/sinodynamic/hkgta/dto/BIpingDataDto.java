package com.sinodynamic.hkgta.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class BIpingDataDto implements Serializable {

	private String name;

	private BigDecimal data;

	private String status;
	
	private String code;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public BIpingDataDto(){}
	public BIpingDataDto(String name,BigDecimal data)
	{
		this.name=name;
		this.data=data;
	}
	public String getName() {
		return name;
	}

	public void setName(Object name)
	{
		if(name instanceof String){
			this.name =(String)name;	
		}else if(name instanceof Character){
			this.name=String.valueOf(name);
		}
	}

	public BigDecimal getData() {
		return data;
	}

	public void setData(Object data) {
		if (data instanceof BigInteger) {
			this.data = BigDecimal.valueOf(Double.valueOf(data.toString()));
		} else {
			this.data = (BigDecimal) data;
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
