package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class CoachScheduleDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8448456399208991894L;
	
	private List<CoachTimeSlotDto>  coachTimeSlotSet = new ArrayList<CoachTimeSlotDto>();
	
	private String rateType;
	
	private int rosterBeginTime;
	
	private int rosterEndTime;
	
	private String offduty;
	
	private Date onDate;
	
	private String weekday;
	
	/**
	 * @return the onDate
	 */
	public Date getOnDate() {
		return onDate;
	}




	/**
	 * @param onDate the onDate to set
	 */
	public void setOnDate(Date onDate) {
		this.onDate = onDate;
	}




	/**
	 * @return the weekday
	 */
	public String getWeekday() {
		return weekday;
	}




	/**
	 * @param weekday the weekday to set
	 */
	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}
	
	/**
	 * @return the coachTimeSlotSet
	 */
	public List<CoachTimeSlotDto> getCoachTimeSlotSet() {
		return coachTimeSlotSet;
	}




	/**
	 * @param coachTimeSlotSet the coachTimeSlotSet to set
	 */
	public void setCoachTimeSlotSet(List<CoachTimeSlotDto> coachTimeSlotSet) {
		this.coachTimeSlotSet = coachTimeSlotSet;
	}

	

	/**
	 * @return the rateType
	 */
	public String getRateType() {
		return rateType;
	}

	/**
	 * @param rateType the rateType to set
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	/**
	 * @return the rosterBeginTime
	 */
	public int getRosterBeginTime() {
		return rosterBeginTime;
	}


	/**
	 * @param rosterBeginTime the rosterBeginTime to set
	 */
	public void setRosterBeginTime(int rosterBeginTime) {
		this.rosterBeginTime = rosterBeginTime;
	}

	/**
	 * @return the rosterEndTime
	 */
	public int getRosterEndTime() {
		return rosterEndTime;
	}

	/**
	 * @param rosterEndTime the rosterEndTime to set
	 */
	public void setRosterEndTime(int rosterEndTime) {
		this.rosterEndTime = rosterEndTime;
	}

	/**
	 * @return the offduty
	 */
	public String getOffduty() {
		return offduty;
	}


	/**
	 * @param offduty the offduty to set
	 */
	public void setOffduty(String offduty) {
		this.offduty = offduty;
	}

	/**
	 * 
	 */
	public CoachScheduleDto() {
		// TODO Auto-generated constructor stub
	}



}
