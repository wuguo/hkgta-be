package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

public class StaffCardDataPrintDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String staffId;
	private String staffName;
	private String staffPhoto;
	private String cardId;
	
	public String getStaffId() {
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffPhoto() {
		return staffPhoto;
	}
	public void setStaffPhoto(String staffPhoto) {
		this.staffPhoto = staffPhoto;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	
	

}
