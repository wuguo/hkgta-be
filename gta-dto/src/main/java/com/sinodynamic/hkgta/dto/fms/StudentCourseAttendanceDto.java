package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class StudentCourseAttendanceDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long attendId;

    private Long courseSessionId;

    private String createBy;

    private Timestamp createDate;

    private Long enrollId;

    private String status;

    private String updateBy;

    private Date updateDate;

    private Long verNo;

    private String portrait;

    private String name;

    private String customerId;

    private String academyNo;

    public Long getAttendId() {
	return attendId;
    }

    public void setAttendId(Long attendId) {
	this.attendId = attendId;
    }

    public Long getCourseSessionId() {
	return courseSessionId;
    }

    public void setCourseSessionId(Long courseSessionId) {
	this.courseSessionId = courseSessionId;
    }

    public String getCreateBy() {
	return createBy;
    }

    public void setCreateBy(String createBy) {
	this.createBy = createBy;
    }

    public Timestamp getCreateDate() {
	return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
	this.createDate = createDate;
    }

    public Long getEnrollId() {
	return enrollId;
    }

    public void setEnrollId(Object enrollId) {
	this.enrollId = (enrollId!=null ? NumberUtils.toLong(enrollId.toString()):null);
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getUpdateBy() {
	return updateBy;
    }

    public void setUpdateBy(String updateBy) {
	this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
	return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
	this.updateDate = updateDate;
    }

    public Long getVerNo() {
	return verNo;
    }

    public void setVerNo(Long verNo) {
	this.verNo = verNo;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAcademyNo() {
	return academyNo;
    }

    public void setAcademyNo(String academyNo) {
	this.academyNo = academyNo;
    }

}
