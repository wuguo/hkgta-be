package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class BookingRecordDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6472924038024945181L;
	private String resvId;
	private String customerId;
	private String bookTpye;
	private String facility;
	private String dateTime;
	private String bookingDetails;
	private String state;
	private String remark;
	public String getResvId() {
		return resvId;
	}
	public void setResvId(String resvId) {
		this.resvId = resvId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getBookTpye() {
		return bookTpye;
	}
	public void setBookTpye(String bookTpye) {
		this.bookTpye = bookTpye;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getBookingDetails() {
		return bookingDetails;
	}
	public void setBookingDetails(String bookingDetails) {
		this.bookingDetails = bookingDetails;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	

}
