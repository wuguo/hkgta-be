package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;
import java.math.BigInteger;

import org.apache.commons.lang.math.NumberUtils;

public class UserRoleDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1288132507831519218L;
	private String userId;
	private Long roleId;
	private String roleName;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Object roleId) {
		this.roleId =  (roleId!=null ? NumberUtils.toLong(roleId.toString()):null);
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	

}
