package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RoomStatusScheduleDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7936492387046859018L;
	private String roomId;              //多个房间，分割
	private String stascheduleOoStatus;
	private BigInteger scheduleId;

	private Date beginDate;

	private Date endDate;

	private String reasonCode;

	private String remark;
	

	private Timestamp createDate;
	private String reason;

	private String createBy;
	private String lockDate;
	private String unlockDate;
	public RoomStatusScheduleDto(){}
	public String getRoomId() {
		return roomId;
	}
	 DateFormat formart = new SimpleDateFormat("yyyy-MM-dd");
	public void setRoomId(Object roomId) {
		this.roomId = roomId.toString();
	}


	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getStascheduleOoStatus() {
		return stascheduleOoStatus;
	}

	public void setStascheduleOoStatus(String stascheduleOoStatus) {
		this.stascheduleOoStatus = stascheduleOoStatus;
	}
	
	public Date getBeginDate() throws ParseException {
		return formart.parse(formart.format(beginDate));
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() throws ParseException {
		return formart.parse(formart.format(endDate));
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public BigInteger getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(BigInteger scheduleId) {
		this.scheduleId = scheduleId;
	}
	public String getLockDate() {
		return formart.format(beginDate);
	}
	public String getUnlockDate() {
		return formart.format(endDate);
	}
	
	

}
