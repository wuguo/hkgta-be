package com.sinodynamic.hkgta.dto.ical;

import java.text.SimpleDateFormat;
import java.util.Date;


public class PublicHolidayDto {
	
    private Long holidayId;

	private String holidayDate;

	private String descriptionEn;

	private String descriptionTc;

	private String descriptionSc;

	private String holidayType;	

	private String createDate;

	private String createBy;

	private String updateDate;

	private String update_by;

	private Long verNo;

	public Long getHolidayId() {
		return holidayId;
	}

	public void setHolidayId(Object holidayId) {
		if (holidayId != null)
		  this.holidayId = Long.valueOf(holidayId.toString());
	}

	public String getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(Object holidayDate) {
		if (holidayDate != null){
		     if (holidayDate instanceof Date){
		    	 this.holidayDate = new SimpleDateFormat("yyyy-MM-dd").format(holidayDate);
		     }else{
		         this.holidayDate = holidayDate.toString();
		     }
		}
	}

	public String getDescriptionEn() {
		return descriptionEn;
	}

	public void setDescriptionEn(String descriptionEn) {
		this.descriptionEn = descriptionEn;
	}

	public String getDescriptionTc() {
		return descriptionTc;
	}

	public void setDescriptionTc(String descriptionTc) {
		this.descriptionTc = descriptionTc;
	}

	public String getDescriptionSc() {
		return descriptionSc;
	}

	public void setDescriptionSc(String descriptionSc) {
		this.descriptionSc = descriptionSc;
	}

	public String getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(String holidayType) {
		this.holidayType = holidayType;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Object createDate) {
		if (createDate != null){
			 if (createDate instanceof Date){
		    	 this.createDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(createDate);
		     }else{
		         this.createDate = createDate.toString();
		     }
		}
		     
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Object updateDate) {
		if (updateDate != null){
			 if (updateDate instanceof Date){
		    	 this.updateDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(updateDate);
		     }else{
		         this.updateDate = updateDate.toString();
		     }
		}    
	}

	public String getUpdate_by() {
		return update_by;
	}

	public void setUpdate_by(String update_by) {
		this.update_by = update_by;
	}

	public Long getVerNo() {
		return verNo;
	}

	public void setVerNo(Long verNo) {
		if (verNo != null)
		   this.verNo = Long.valueOf(verNo.toString());
	}   
}
