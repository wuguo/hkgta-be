/**
 * 
 */
package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

import javax.management.loading.PrivateClassLoader;

/**
 * @author Tony_Dong
 *
 */
public class CoachProfileInfoDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1760908356355111092L;
	
	private StaffMasterDto staffMasterObj;
	
	private StaffProfileDto staffProfileObj;
	
	private String qualification;
	
	private String specialty;

	/**
	 * @return the staffMasterObj
	 */
	public StaffMasterDto getStaffMasterObj() {
		return staffMasterObj;
	}

	/**
	 * @param staffMasterObj the staffMasterObj to set
	 */
	public void setStaffMasterObj(StaffMasterDto staffMasterObj) {
		this.staffMasterObj = staffMasterObj;
	}

	/**
	 * @return the staffProfileObj
	 */
	public StaffProfileDto getStaffProfileObj() {
		return staffProfileObj;
	}

	/**
	 * @param staffProfileObj the staffProfileObj to set
	 */
	public void setStaffProfileObj(StaffProfileDto staffProfileObj) {
		this.staffProfileObj = staffProfileObj;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}

	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the specialtyv
	 */
	public String getSpecialty() {
		return specialty;
	}

	/**
	 * @param specialtyv the specialtyv to set
	 */
	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}
	

}
