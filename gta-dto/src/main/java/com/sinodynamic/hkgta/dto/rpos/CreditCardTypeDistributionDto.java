package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

public class CreditCardTypeDistributionDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String payCode;
	private Long countNum;
	private double accountingNum;
	public String getPayCode() {
		return payCode;
	}
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}
	public Long getCountNum() {
		return countNum;
	}
	public void setCountNum(Object countNum) {
		this.countNum = countNum == null ? 0l : Long.parseLong(countNum.toString());
	}
	public double getAccountingNum() {
		return accountingNum;
	}
	public void setAccountingNum(Object accountingNum) {
		this.accountingNum = accountingNum == null ? 0.0 : Double.parseDouble(accountingNum.toString());
	}
	
	
	
	
}
