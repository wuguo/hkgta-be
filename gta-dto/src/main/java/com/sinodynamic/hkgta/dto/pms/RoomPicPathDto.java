package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Kevin_Liang
 *
 */
public class RoomPicPathDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3814652008026237530L;

	private Long id;

	private String roomTypeCode;
	
	private Long roomId;
	
	private String picType;
	
	private String serverFilename;
	
	private Long displayOrder;
	
	private String action;

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getRoomId()
	{
		return roomId;
	}

	public void setRoomId(Long roomId)
	{
		this.roomId = roomId;
	}

	public String getPicType()
	{
		return picType;
	}

	public void setPicType(String picType)
	{
		this.picType = picType;
	}

	public String getServerFilename()
	{
		return serverFilename;
	}

	public void setServerFilename(String serverFilename)
	{
		this.serverFilename = serverFilename;
	}

	public Long getDisplayOrder()
	{
		return displayOrder;
	}

	public void setDisplayOrder(Long displayOrder)
	{
		this.displayOrder = displayOrder;
	}

	public String getAction()
	{
		return action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}
	
}
