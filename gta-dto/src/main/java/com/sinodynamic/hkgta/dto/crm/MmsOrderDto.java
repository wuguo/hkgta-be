package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class MmsOrderDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private BigInteger resvId;
    private BigInteger orderNO;
    private String extInvoiceNo;
    private BigInteger itemCount;
    private String memberId;
    private String memberName;
    private String orderDate;
    private BigDecimal totalAmount;
    private BigDecimal totalPaid;
    private String status;
    private String statusValue;
    

    public BigInteger getResvId() {
		return resvId;
	}

	public void setResvId(BigInteger resvId) {
		this.resvId = resvId;
	}

	public String getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}

	public BigInteger getOrderNO() {
	return orderNO;
    }

    public void setOrderNO(BigInteger orderNO) {
	this.orderNO = orderNO;
    }

    public BigInteger getItemCount() {
	return itemCount;
    }

    public void setItemCount(BigInteger itemCount) {
	this.itemCount = itemCount;
    }

    public String getMemberId() {
	return memberId;
    }

    public void setMemberId(String memberId) {
	this.memberId = memberId;
    }

    public String getMemberName() {
	return memberName;
    }

    public void setMemberName(String memberName) {
	this.memberName = memberName;
    }

    public String getOrderDate() {
	return orderDate;
    }

    public void setOrderDate(String orderDate) {
	this.orderDate = orderDate;
    }

    public BigDecimal getTotalAmount() {
	return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
	this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalPaid() {
	return totalPaid;
    }

    public void setTotalPaid(BigDecimal totalPaid) {
	this.totalPaid = totalPaid;
    }

    public String getExtInvoiceNo() {
        return extInvoiceNo;
    }

    public void setExtInvoiceNo(String extInvoiceNo) {
        this.extInvoiceNo = extInvoiceNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
