package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.List;

public class FacilityUtilizationRateListDto implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	private String facilityType;
	
	private String subType;
	
	private List<FacilityUtilizationRateTimeListDto> dayRateList;
	
	public List<FacilityUtilizationRateTimeListDto> getDayRateList()
	{
		return dayRateList;
	}

	public void setDayRateList(List<FacilityUtilizationRateTimeListDto> dayRateList)
	{
		this.dayRateList = dayRateList;
	}

	public String getSubType()
	{
		return subType;
	}

	public void setSubType(String subType)
	{
		this.subType = subType;
	}

	public String getFacilityType()
	{
		return facilityType;
	}

	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}

}