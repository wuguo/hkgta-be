package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.entity.pms.RoomType;

/**
 * @author Kevin_Liang
 *
 */
public class RoomTypeListParam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3814652008026237530L;

	
	private List<RoomTypeDto> roomTypes;


	public List<RoomTypeDto> getRoomTypes()
	{
		return roomTypes;
	}


	public void setRoomTypes(List<RoomTypeDto> roomTypes)
	{
		this.roomTypes = roomTypes;
	}

		
}
