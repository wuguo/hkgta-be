package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;


public class CancelReservationResponseDto implements Serializable {
	private static final long serialVersionUID = 4579382900585524048L;
	private String returnCode;
	private Data data;

	/**
	 * 
	 * @return The returnCode
	 */
	public String getReturnCode() {
		return returnCode;
	}

	/**
	 * 
	 * @param returnCode
	 *            The returnCode
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * 
	 * @return The data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * 
	 * @param data
	 *            The data
	 */
	public void setData(Data data) {
		this.data = data;
	}

	public class Data implements Serializable{
		private String appointmentId;

		public String getAppointmentId() {
			return appointmentId;
		}

		public void setAppointmentId(String appointmentId) {
			this.appointmentId = appointmentId;
		}
		
		
	}
}
