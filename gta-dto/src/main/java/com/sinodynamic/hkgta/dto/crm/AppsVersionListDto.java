package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.List;

public class AppsVersionListDto implements Serializable {
	
	private List<AppsVersionInfoDto>  list;
	
	public AppsVersionListDto(){
		super();
	}

	public List<AppsVersionInfoDto> getList() {
		return list;
	}

	public void setList(List<AppsVersionInfoDto> list) {
		this.list = list;
	}
	
}
