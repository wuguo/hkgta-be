package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

public class CourseSessionSmsParamDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long sysId;
    private String courseName;
    private String courseType;
    private String coach;
    private String coachUserId;
    private Date sessionDate;
    private String facility;

    
    
    public String getCoachUserId() {
		return coachUserId;
	}

	public void setCoachUserId(String coachUserId) {
		this.coachUserId = coachUserId;
	}

	public Long getSysId() {
	return sysId;
    }

    public void setSysId(Object sysId) {
	this.sysId = (sysId!=null ? NumberUtils.toLong(sysId.toString()):null);
    }

    public String getCourseName() {
	return courseName;
    }

    public void setCourseName(String courseName) {
	this.courseName = courseName;
    }

    public String getCourseType() {
	return courseType;
    }

    public void setCourseType(String courseType) {
	this.courseType = courseType;
    }

    public String getCoach() {
	return coach;
    }

    public void setCoach(String coach) {
	this.coach = coach;
    }

    public Date getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(Date sessionDate) {
        this.sessionDate = sessionDate;
    }

    public String getFacility() {
	return facility;
    }

    public void setFacility(String facility) {
	this.facility = facility;
    }

}
