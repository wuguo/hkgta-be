/**
 * Project Name:gta-dto
 * File Name:TrainningItem.java
 * Package Name:com.sinodynamic.hkgta.dto.crm
 * Create Date:Jul 9, 2015 10:58:19 AM
 *
 */

package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

/**
 * ClassName:TrainningItem <br/>
 * Function: Dislpay the Trainner's month trainning items: coaching and course
 * sessions. <br/>
 * Create Date: Jul 9, 2015 10:58:19 AM <br/>
 * 
 * @author Nick_Xiong
 * @version
 */
public class TrainningItemDto implements Serializable
{

    private String reservId;// for coaching

    private String sessionId;// for course session

    private String trainningType; // course or private coaching

    // course and coaching common parts
    private Date trainningStartTime;

    private Date trainningEndTime;

    private boolean isGolf;

    private String coachingFacilityName;// for coaching

    private String gatherLocation;// for course gather location

    private boolean isTennis;

    // for coach
    private String memberName;

    public String getReservId() {
        return reservId;
    }

    public void setReservId(String reservId) {
        this.reservId = reservId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTrainningType() {
        return trainningType;
    }

    public void setTrainningType(String trainningType) {
        this.trainningType = trainningType;
    }

    public Date getTrainningStartTime() {
        return trainningStartTime;
    }

    public void setTrainningStartTime(Date trainningStartTime) {
        this.trainningStartTime = trainningStartTime;
    }

    public Date getTrainningEndTime() {
        return trainningEndTime;
    }

    public void setTrainningEndTime(Date trainningEndTime) {
        this.trainningEndTime = trainningEndTime;
    }

    public boolean isGolf() {
        return isGolf;
    }

    public void setGolf(boolean isGolf) {
        this.isGolf = isGolf;
    }

    public String getCoachingFacilityName() {
        return coachingFacilityName;
    }

    public void setCoachingFacilityName(String coachingFacilityName) {
        this.coachingFacilityName = coachingFacilityName;
    }

    public String getGatherLocation() {
        return gatherLocation;
    }

    public void setGatherLocation(String gatherLocation) {
        this.gatherLocation = gatherLocation;
    }

    public boolean isTennis() {
        return isTennis;
    }

    public void setTennis(boolean isTennis) {
        this.isTennis = isTennis;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

}
