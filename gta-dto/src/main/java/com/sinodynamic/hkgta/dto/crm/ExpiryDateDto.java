package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class ExpiryDateDto implements Serializable {

	private String	customerID;

	private String	expiryDate;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

}
