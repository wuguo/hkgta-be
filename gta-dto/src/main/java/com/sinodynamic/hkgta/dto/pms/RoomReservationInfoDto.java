package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.Date;

public class RoomReservationInfoDto implements Serializable {
	private String reservationId;
	
	private Date arriveDate;
	
	private Date departDate;
	
	private String earlestBay;
	

	public String getEarlestBay() {
		return earlestBay;
	}

	public void setEarlestBay(String earlestBay) {
		this.earlestBay = earlestBay;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	public Date getArriveDate() {
		return arriveDate;
	}

	public void setArriveDate(Date arriveDate) {
		this.arriveDate = arriveDate;
	}

	public Date getDepartDate() {
		return departDate;
	}

	public void setDepartDate(Date departDate) {
		this.departDate = departDate;
	}
	
	
}
