package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

/**
 * @author Kevin_Liang
 *
 */
public class CourseScheduleDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7731866039240584803L;
	private String sessionId;
	private String sessionName;
	private String date;
	private String venue;
	private String coach;
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getCoach() {
		return coach;
	}
	public void setCoach(String coach) {
		this.coach = coach;
	}
	public String getSessionName() {
		return sessionName;
	}
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	

}
