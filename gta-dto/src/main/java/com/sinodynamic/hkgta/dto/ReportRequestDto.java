package com.sinodynamic.hkgta.dto;

import java.util.Map;

/**
 * Created by Mason_Yang on 12/24/2015.
 */
public class ReportRequestDto {

    private String sortBy;

    private String isAscending;

    private String reportFile;

    private String fileType;

    private Map<String, Object> parameters;

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getIsAscending() {
        return isAscending;
    }

    public void setIsAscending(String isAscending) {
        this.isAscending = isAscending;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public String getReportFile() {
        return reportFile;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
