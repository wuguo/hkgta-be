package com.sinodynamic.hkgta.dto;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;

public class DtoHelper {
	public static Date d = new Date();
	//public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static String defaultformat = "yyyy/MM/dd";


	public static String getYMDFormatDate(Date d) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		if (d == null)
			return null;
		return df.format(d);
	}

	/**
	 * @author Junfeng_Yan
	 * @param d
	 * @param format
	 *            such as "yyyy-MM-dd or yyyy/MM/dd"
	 * @return
	 */
	public static String date2String(Date d, String format) {
		if (null == d)
			return "";
		if (StringUtils.isEmpty(format))
			format = defaultformat;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(d);
	}

	

	/**
	 * @author Vineela_Jyothi This method returns the no.of days/weeks/months
	 *         between the given date and today. If the given date is after
	 *         today and no.of days in between <30, returns --> 'xxd left' (xx -
	 *         no.of days) If the given date is after today and no.of days in
	 *         between >30 & <90, returns --> 'xxw left' (xx - no.of weeks) If
	 *         the given date is after today and no.of days in between >90,
	 *         returns --> 'xxm left' (xx - no.of months) If the given date is
	 *         before today and no.of days in between <30, returns --> 'xxd ago'
	 *         (xx - no.of days) If the given date is before today and no.of
	 *         days in between >30 & <90, returns --> 'xxw ago' (xx - no.of
	 *         weeks) If the given date is before today and no.of days in
	 *         between >90, returns --> 'xxm ago' (xx - no.of months) Else
	 *         returns (Today)
	 * @param givenDate
	 * @return no.of days/weeks/months
	 */
	public static String getNoOfDays(Date givenDate) {

		if (null == givenDate)
			return "";

		Date today = new Date();
		DateTime start = new DateTime(givenDate);
		DateTime end = new DateTime(today);

		int noOfDays = Days.daysBetween(start.withTimeAtStartOfDay(),
				end.withTimeAtStartOfDay()).getDays();
		if (noOfDays < 0 && noOfDays > -30) {
			return "(" + Math.abs(noOfDays) + "d left)";
		} else if (noOfDays > -90 && noOfDays <= -30) {
			return "(" + Math.abs(noOfDays) / 7 + "w left)";
		} else if (noOfDays <= -90) {
			return "(" + Math.abs(noOfDays) / 30 + "m left)";
		} else if (noOfDays == 0) {
			return "(Today)";
		} else if (noOfDays > 0 && noOfDays < 30) {
			return "(" + noOfDays + "d ago)";
		} else if (noOfDays >= 30 && noOfDays < 90) {
			return "(" + noOfDays / 7 + "w ago)";
		} else {
			return "(" + noOfDays / 30 + "m ago)";
		}
	}

	/**
	 * @author Allen_Yu
	 * @param givenDate
	 * @return ymdAndDateDiff String such as 2015/05/05(Today)
	 */
	public static String getYMDDateAndDateDiff(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		DateFormat df  = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
		return df.format(givenDate) + " " + getNoOfDays(givenDate);
	}
	
	public static String getYMDDHHMMateAndDateDiff(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		DateFormat df  = new SimpleDateFormat("yyyy/MM/dd HH:mm",Locale.ENGLISH);
		return df.format(givenDate) + " " + getNoOfDays(givenDate);
	}
	public static String getYMDDatePeriodAndDateDiff(String beginDateString, String endDateString) throws Exception{
		if (null == beginDateString || null == endDateString){
			return "";
		}
		DateFormat df  = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		Date beginDate = df.parse(beginDateString);
		
		return beginDateString + "-" + endDateString.substring(11) + " " + getNoOfDays(beginDate);
	}
	
	public static String getYMMDDateAndDateDiff(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		DateFormat df  = new SimpleDateFormat("yyyy/MM/dd",Locale.ENGLISH);
		return df.format(givenDate) + " " + getNoOfDays(givenDate);
	}

	public static String getYMDDate(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		DateFormat df  = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);
		return df.format(givenDate);
	}

	


	public static String getObliqueYMDDateAndDateDiff(Date givenDate,
			String format) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df = new SimpleDateFormat(format);
		return df.format(givenDate) + getNoOfDays(givenDate);
	}

	public static Date parseString2Date(String src, String format) {

		if (StringUtils.isEmpty(src))
			return null;

		if (StringUtils.isEmpty(format))
			format = defaultformat;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = simpleDateFormat.parse(src);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * @param givenDate
	 * @return ymdAndDateTimeDiff String such as 2015/05/05(Today)
	 * @author Vineela_Jyothi
	 */
	public static String getYMDDateTimeAndDateDiff(Date givenDate) {
		if (null == givenDate) {
			return "";
		}
		DateFormat df  = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		return df.format(givenDate) + " " + getNoOfDays(givenDate);
	}
	
	public static String nvl(String p1) {
		return (p1 == null) ? "" : p1;
	}

	public static boolean notEmpty(String obj) {
		if (obj != null && obj.trim().length() > 0) {
			return true;
		} else {
			return false;
		}
	}
	public static void main (String arg[]){
		System.out.println(System.getProperties());
	}
}
