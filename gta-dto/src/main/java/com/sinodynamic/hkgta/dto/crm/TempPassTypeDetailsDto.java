package com.sinodynamic.hkgta.dto.crm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.entity.crm.HelperPassPermission;
import com.sinodynamic.hkgta.entity.crm.HelperPassType;

public class TempPassTypeDetailsDto extends TempPassTypeDto
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5479965166998562342L;
	
	private List<AreaAccessRight> areaAccessRight;
	
	public TempPassTypeDetailsDto()
	{
	}

	public TempPassTypeDetailsDto(HelperPassType passType, Map<String, String> gateTerminal)
	{
		this.setTempPassId(passType.getTypeId());
		this.setStartDate(DtoHelper.getYMDDateAndDateDiff(passType.getStartDate()));
		this.setExpiryDate(DtoHelper.getYMDDateAndDateDiff(passType.getExpiryDate()));
		this.setTempPassName(passType.getTypeName());
		this.setStatus(passType.getStatus());
		
		List<AreaAccessRight> areaAccessRight = new ArrayList<AreaAccessRight>(); 
		areaAccessRight = initAccessRight(passType.getHelperPassPermission(), gateTerminal);
		this.setAreaAccessRight(areaAccessRight);
	}

	private List<AreaAccessRight> initAccessRight(Set<HelperPassPermission> helperPassPermission, Map<String, String> gateTerminal)
	{
		List<AreaAccessRight> accessRights = new ArrayList<AreaAccessRight>();
		
		for (HelperPassPermission right : helperPassPermission)
		{
			AreaAccessRight accessRight = new AreaAccessRight();
			accessRight.setTerminalId(right.getTerminalId());			
			accessRight.setTerminalName(gateTerminal.get(right.getTerminalId().toString()));			
			accessRights.add(accessRight);
		}
		return accessRights;
	}

	public List<AreaAccessRight> getAreaAccessRight()
	{
		return areaAccessRight;
	}

	public void setAreaAccessRight(List<AreaAccessRight> areaAccessRight)
	{
		this.areaAccessRight = areaAccessRight;
	}

}
