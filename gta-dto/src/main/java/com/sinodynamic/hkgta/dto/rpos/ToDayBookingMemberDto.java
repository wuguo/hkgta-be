package com.sinodynamic.hkgta.dto.rpos;

import java.io.Serializable;
import java.math.BigDecimal;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class ToDayBookingMemberDto  extends GenericDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long customerId;
	private Long bookedNum;
	
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Object customerId) {
		this.customerId = Long.parseLong(customerId.toString());
	}
	public Long getBookedNum() {
		return bookedNum;
	}
	public void setBookedNum(Object bookedNum) {
		this.bookedNum = Long.parseLong(bookedNum.toString());
	}
	
	
	
}
