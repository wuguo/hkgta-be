package com.sinodynamic.hkgta.dto.crm;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class RefundedSpaPaymentDto{

	private static final long serialVersionUID = 3075356519144070439L;
	private String patronId;
	private String patronName;
	private String paymentType;
	private Date dateTime;
	private String paymentDate;
	private BigDecimal amount;
	private String refInvoiceId;
	private String refPaymentId;
	private String creditCard;
	private String creditCardDate;
	private Integer refundRequest;
	private String remark;

	public String getPatronId() {
		return patronId;
	}

	public void setPatronId(String patronId) {
		this.patronId = patronId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getRefInvoiceId() {
		return refInvoiceId;
	}

	public void setRefInvoiceId(String refInvoiceId) {
		this.refInvoiceId = refInvoiceId;
	}

	public String getRefPaymentId() {
		return refPaymentId;
	}

	public void setRefPaymentId(String refPaymentId) {
		this.refPaymentId = refPaymentId;
	}

	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	public String getCreditCardDate() {
		return creditCardDate;
	}

	public void setCreditCardDate(String creditCardDate) {
		this.creditCardDate = creditCardDate;
	}

	public Integer getRefundRequest() {
		return refundRequest;
	}

	public void setRefundRequest(Integer refundRequest) {
		this.refundRequest = refundRequest;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}