package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.sinodynamic.hkgta.entity.pms.HousekeepPresetParameter;

public class HousekeepPresetParameterDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long paramId;

	private String paramCat;

	private String description;

	private Long maxInt;

	private String displayScope;

	private String updateBy;

	public HousekeepPresetParameterDto() {
	}

	public HousekeepPresetParameterDto(
			HousekeepPresetParameter housekeepPresetParameter) {
		this.paramId = housekeepPresetParameter.getParamId();
		this.paramCat = housekeepPresetParameter.getParamCat();
		this.description = housekeepPresetParameter.getDescription();
		this.maxInt = housekeepPresetParameter.getMaxInt();
		this.displayScope = housekeepPresetParameter.getDisplayScope();
	}

	public Long getParamId() {
		return paramId;
	}

	public void setParamId(Long paramId) {
		this.paramId = paramId;
	}

	public String getParamCat() {
		return paramCat;
	}

	public void setParamCat(String paramCat) {
		this.paramCat = paramCat;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getMaxInt() {
		return maxInt;
	}

	public void setMaxInt(Long maxInt) {
		this.maxInt = maxInt;
	}

	public String getDisplayScope() {
		return displayScope;
	}

	public void setDisplayScope(String displayScope) {
		this.displayScope = displayScope;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
