/**
 * 
 */
package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

/**
 * @author Tony_Dong
 *
 */
public class CoachNewMessageDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1537043352835127429L;
	
	private Long sumNewMsg;

	public Long getSumNewMsg() {
		return sumNewMsg;
	}

	public void setSumNewMsg(Long sumNewMsg) {
		this.sumNewMsg = sumNewMsg;
	}

	
	
	

}
