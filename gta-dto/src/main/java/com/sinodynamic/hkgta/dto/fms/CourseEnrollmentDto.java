package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;


public class CourseEnrollmentDto extends GenericDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long courseId;
	@EncryptFieldInfo
	private Long customerId;
	private String remark;
	private Boolean isPay;
	private String paymentMethod;
	private String device;
	private String paymentLocation;
	private String createdBy;
	
	private Long transactionId;
	
	private Long orderNo;
	
	private BigDecimal totalPrice;
	
	private String terminalType;
	
	/***
	 * FE pass identifi paymentType is ECR OR OP
	 * if ecr  set  payment_media is ECR else OP
	 * @return
	 */
	public String getTerminalType() {
		return terminalType;
	}

	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}
	
	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Long getCourseId() {
		return courseId;
	}
	
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Boolean getIsPay() {
		return isPay;
	}
	public void setIsPay(Boolean isPay) {
		this.isPay = isPay;
	}

	public String getDevice() {
	    return device;
	}

	public void setDevice(String device) {
	    this.device = device;
	}

	public String getPaymentLocation() {
	    return paymentLocation;
	}

	public void setPaymentLocation(String paymentLocation) {
	    this.paymentLocation = paymentLocation;
	}

}