package com.sinodynamic.hkgta.dto.pos;

import java.math.BigDecimal;

public class RestaurantMenuItemDto {

	private String itemNo;
	
	private String catId;
	
	private String restaurantId;
	
	private String itemName;
	
	private String status;
	
	private BigDecimal defaultPrice;
	
	private BigDecimal onlinePrice;
	
	private String imageFileName;
	
	private Integer displayOrder;

	private Boolean isFirst;
	
	private Boolean isLast;
	
	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getCatId() {
		return catId;
	}

	public void setCatId(String catId) {
		this.catId = catId;
	}

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getDefaultPrice() {
		return defaultPrice;
	}

	public void setDefaultPrice(BigDecimal defaultPrice) {
		this.defaultPrice = defaultPrice;
	}

	public BigDecimal getOnlinePrice() {
		return onlinePrice;
	}

	public void setOnlinePrice(BigDecimal onlinePrice) {
		this.onlinePrice = onlinePrice;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public Boolean getIsFirst() {
		return isFirst;
	}

	public void setIsFirst(Boolean isFirst) {
		this.isFirst = isFirst;
	}

	public Boolean getIsLast() {
		return isLast;
	}

	public void setIsLast(Boolean isLast) {
		this.isLast = isLast;
	}
}
