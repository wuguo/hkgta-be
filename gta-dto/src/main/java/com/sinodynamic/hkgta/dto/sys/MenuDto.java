package com.sinodynamic.hkgta.dto.sys;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.dto.staff.CoachReservationDto;

public class MenuDto implements Serializable, Comparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5581946421935677912L;
	private String name;
	private String icon;
	private String index;
	private String link;
	private String type;
	private String accessRight;
	private List<MenuDto> submenu;
	private Long seq;
	public Long getSeq()
	{
		return seq;
	}
	public void setSeq(Long seq)
	{
		this.seq = seq;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	public List<MenuDto> getSubmenu() {
		return submenu;
	}
	public void setSubmenu(List<MenuDto> submenu) {
		this.submenu = submenu;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccessRight() {
		return accessRight;
	}
	public void setAccessRight(String accessRight) {
		this.accessRight = accessRight;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MenuDto other = (MenuDto) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		
		return true;
	}
	
	@Override
	public int compareTo(Object o)
	{
		MenuDto other = (MenuDto)o;
		
		if(o == null)
			return 1;
		
		return this.getSeq().compareTo(other.getSeq());
		
//		if(this.status!=null && other.status!=null)
//		{
//			int compareStatus = getStatusOrder(this.status).compareTo(getStatusOrder(other.status));
//			if(compareStatus == 0)
//			{
//				int compareTimegap = other.timeGap.compareTo(this.timeGap);
//				if (compareTimegap == 0)
//				{
//					int compareCategory = getCategoryOrder(this.category).compareTo(getCategoryOrder(other.category));
//					return compareCategory;
//				}
//				else
//				{
//					return compareTimegap;
//				}
//			}
//			else
//			{
//				return compareStatus;
//			}
//		}
		
//		return 0;
		
	}
	

}