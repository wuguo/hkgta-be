package com.sinodynamic.hkgta.dto.fms;

public class FacilityCheckInRequestDto {

	private Long resvId;
	
	private Long[] facilityNo;
	
	//added by Kaster 20160329
	private String createdBy;
	private String updateBy;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public long getResvId() {
		return resvId;
	}

	public void setResvId(long resvId) {
		this.resvId = resvId;
	}

	public Long[] getFacilityNo() {
		return facilityNo;
	}

	public void setFacilityNo(Long[] facilityNo) {
		this.facilityNo = facilityNo;
	}

	
}
