package com.sinodynamic.hkgta.dto.statement;

import java.io.Serializable;
import java.math.BigDecimal;

public class CustomerStatementBaseInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String memberName;
	private String companyName;
	private String address1;
	private String address2;
	private String address3;
	private String email;
	private String academyId;
	private String typeDesc;
	private String virtualNo;
	private BigDecimal creditLimit;
	private String period;
	private String printDate;
	private String statementBalance;
	private BigDecimal cashvalue;
	
	public CustomerStatementBaseInfoDto() {
		
	}
	public CustomerStatementBaseInfoDto(String memberName, String companyName, String address1, String address2,
			String address3, String email, String academyId, String typeDesc, String virtualNo, BigDecimal creditLimit,
			String period, String printDate, String statementBalance,BigDecimal cashvalue) {
		super();
		this.memberName = memberName;
		this.companyName = companyName;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.email = email;
		this.academyId = academyId;
		this.typeDesc = typeDesc;
		this.virtualNo = virtualNo;
		this.creditLimit = creditLimit;
		this.period = period;
		this.printDate = printDate;
		this.statementBalance = statementBalance;
		this.cashvalue = cashvalue;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAcademyId() {
		return academyId;
	}
	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}
	public String getTypeDesc() {
		return typeDesc;
	}
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	public String getVirtualNo() {
		return virtualNo;
	}
	public void setVirtualNo(String virtualNo) {
		this.virtualNo = virtualNo;
	}
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	public String getStatementBalance() {
		return statementBalance;
	}
	public void setStatementBalance(String statementBalance) {
		this.statementBalance = statementBalance;
	}
	
	public BigDecimal getCashvalue() {
		return cashvalue;
	}
	public void setCashvalue(BigDecimal cashvalue) {
		this.cashvalue = cashvalue;
	}

}
