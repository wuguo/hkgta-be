package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;


public class FacilityUtilizationRateTimeDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long timeId;

	private String rateType;

	private Integer beginTime;
	
	public String getRateType()
	{
		return rateType;
	}

	public void setRateType(String rateType)
	{
		this.rateType = rateType;
	}

	

	public Integer getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Integer beginTime) {
		this.beginTime = beginTime;
	}

	public Long getTimeId()
	{
		return timeId;
	}

	public void setTimeId(Long timeId)
	{
		this.timeId = timeId;
	}

}