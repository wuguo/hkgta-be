package com.sinodynamic.hkgta.dto.pms;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class StaffRosterDto {

	private String staffUserId;

	private List<StaffRosterRecordDto> StaffRosterRecord;

	public String getStaffUserId() {
		return staffUserId;
	}

	public void setStaffUserId(String staffUserId) {
		this.staffUserId = staffUserId;
	}

	public List<StaffRosterRecordDto> getStaffRosterRecord() {
		return StaffRosterRecord;
	}

	public void setStaffRosterRecord(
			List<StaffRosterRecordDto> staffRosterRecord) {
		StaffRosterRecord = staffRosterRecord;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
