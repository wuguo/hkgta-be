package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Kevin_Liang
 *
 */
public class RoomTypeDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3814652008026237530L;

	private String roomTypeCode;
	private String roomTypeDescription;
	private String roomTypeName;
	private String amenity;
	private String numOfGuest;
	private Double roomPrice;
	
	private Long adultCapacity;
	private Long childCapacity;
	private Long singleBedQty;
	private Long doubleBedQty;
	
	private String status;
	
	private List<RoomPicPathDto> roomPics;
	private List<RoomRateDto> roomRates;
	
	private String oldRoomTypeCode;
	
	public String getRoomTypeCode() {
		return roomTypeCode.toUpperCase();
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public String getRoomTypeDescription() {
		return roomTypeDescription;
	}

	public void setRoomTypeDescription(String roomTypeDescription) {
		this.roomTypeDescription = roomTypeDescription;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getRoomTypeName()
	{
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName)
	{
		this.roomTypeName = roomTypeName;
	}

	public String getAmenity()
	{
		return amenity;
	}

	public void setAmenity(String amenity)
	{
		this.amenity = amenity;
	}

	public String getNumOfGuest()
	{
		return numOfGuest;
	}

	public void setNumOfGuest(String numOfGuest)
	{
		this.numOfGuest = numOfGuest;
	}

	

	public Long getAdultCapacity() {
		return adultCapacity;
	}

	public void setAdultCapacity(Long adultCapacity) {
		this.adultCapacity = adultCapacity;
	}

	public Long getChildCapacity() {
		return childCapacity;
	}

	public void setChildCapacity(Long childCapacity) {
		this.childCapacity = childCapacity;
	}

	public Long getSingleBedQty() {
		return singleBedQty;
	}

	public void setSingleBedQty(Long singleBedQty) {
		this.singleBedQty = singleBedQty;
	}

	public Long getDoubleBedQty() {
		return doubleBedQty;
	}

	public void setDoubleBedQty(Long doubleBedQty) {
		this.doubleBedQty = doubleBedQty;
	}

	public List<RoomPicPathDto> getRoomPics()
	{
		return roomPics;
	}

	public void setRoomPics(List<RoomPicPathDto> roomPics)
	{
		this.roomPics = roomPics;
	}

	public List<RoomRateDto> getRoomRates() {
		return roomRates;
	}

	public void setRoomRates(List<RoomRateDto> roomRates) {
		this.roomRates = roomRates;
	}

	public Double getRoomPrice()
	{
		return roomPrice;
	}

	public void setRoomPrice(Double roomPrice)
	{
		this.roomPrice = roomPrice;
	}
	
	public RoomTypeDto()
	{}
	
	public RoomTypeDto(String roomTypeCode)
	{
		this.roomTypeCode = roomTypeCode;
	}
	
	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getOldRoomTypeCode() {
		return oldRoomTypeCode;
	}

	public void setOldRoomTypeCode(String oldRoomTypeCode) {
		this.oldRoomTypeCode = oldRoomTypeCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((roomTypeCode == null) ? 0 : roomTypeCode.toUpperCase().hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoomTypeDto other = (RoomTypeDto) obj;
		if (roomTypeCode == null) {
			if (other.roomTypeCode != null)
				return false;
		} else if (!roomTypeCode.equalsIgnoreCase(other.roomTypeCode))
			return false;
		
		return true;
	}
	
}
