package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

public class UniqueId implements Serializable {

	private static final long serialVersionUID = 1L;

	private String type;					
	private String id;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}					

	
}
