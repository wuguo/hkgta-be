package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.sql.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;

/**
 * @author kevin_Liang
 *
 */
public class PrivateCoachInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4645521949280412129L;
	private String portraitPhoto;
	private String nickname;
	private String positionTitle;
	private String sex;
	private String dateOfBirth;
	private String nationality;
	private String language;
	private String qualification;
	private String speciality;
	public String getPortraitPhoto() {
		return portraitPhoto;
	}
	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPositionTitle() {
		return positionTitle;
	}
	public void setPositionTitle(String positionTitle) {
		this.positionTitle = positionTitle;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		
		this.dateOfBirth = DtoHelper.date2String(dateOfBirth, "yyyy-MM-dd");
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	

}
