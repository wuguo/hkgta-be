package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberCashvalueDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long customerId;
	
	private BigDecimal availableBalance;
	
	private BigDecimal amount;
	
	private String academyNo;
	
	private String patronName;
	
	private String patronType;
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getPatronType() {
		return patronType;
	}

	public void setPatronType(String patronType) {
		this.patronType = patronType;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getAvailableBalance()
	{
		return availableBalance.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
	}

	public void setAvailableBalance(BigDecimal availableBalance)
	{
		this.amount=availableBalance;
		this.availableBalance = availableBalance;
	}


}
