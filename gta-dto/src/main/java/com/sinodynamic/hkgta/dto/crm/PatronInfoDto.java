package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class PatronInfoDto implements Serializable {

	private String	customerID;

	private String	superCustomerID;

	private String	academyID;

	private String	lastName;

	private String	firstName;

	private String	dateOfBirth;

	private String	monthOfBirth;

	private String	email;

	private String	expiryDate;

	private String	servicePlan;

	private String	memberType;

	private String	servicePlanType;

	private String	gender;

	private String	hKGTADirectMarketing;

	private String	nWDDirectMarketing;



	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getSuperCustomerID() {
		return superCustomerID;
	}

	public void setSuperCustomerID(String superCustomerID) {
		this.superCustomerID = superCustomerID;
	}

	public String getAcademyID() {
		return academyID;
	}

	public void setAcademyID(String academyID) {
		this.academyID = academyID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getMonthOfBirth() {
		return monthOfBirth;
	}

	public void setMonthOfBirth(String monthOfBirth) {
		this.monthOfBirth = monthOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getServicePlan() {
		return servicePlan;
	}

	public void setServicePlan(String servicePlan) {
		this.servicePlan = servicePlan;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getServicePlanType() {
		return servicePlanType;
	}

	public void setServicePlanType(String servicePlanType) {
		this.servicePlanType = servicePlanType;
	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String gethKGTADirectMarketing() {
		return hKGTADirectMarketing;
	}

	public void sethKGTADirectMarketing(String hKGTADirectMarketing) {
		this.hKGTADirectMarketing = hKGTADirectMarketing;
	}

	public String getnWDDirectMarketing() {
		return nWDDirectMarketing;
	}

	public void setnWDDirectMarketing(String nWDDirectMarketing) {
		this.nWDDirectMarketing = nWDDirectMarketing;
	}

}
