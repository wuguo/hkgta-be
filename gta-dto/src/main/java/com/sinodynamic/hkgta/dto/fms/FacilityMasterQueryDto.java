package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class FacilityMasterQueryDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long facilityNo;

	private Long venueFloor;

	private String facilityName;

	private String status;

	private String facilityType;
	
	private String ballFeedingStatus;
	
	private String factorySerialNo;


	public FacilityMasterQueryDto() {
	}


	public Long getFacilityNo()
	{
		return facilityNo;
	}


	public void setFacilityNo(Object facilityNo)
	{
		this.facilityNo = facilityNo != null ? NumberUtils.toLong(facilityNo.toString()) : null;
	}


	public Long getVenueFloor()
	{
		return venueFloor;
	}


	public void setVenueFloor(Object venueFloor)
	{
		this.venueFloor = venueFloor != null ? NumberUtils.toLong(venueFloor.toString()) : null;
	}


	public String getFacilityName()
	{
		return facilityName;
	}


	public void setFacilityName(String facilityName)
	{
		this.facilityName = facilityName;
	}


	public String getStatus()
	{
		return status;
	}


	public void setStatus(String status)
	{
		this.status = status;
	}


	public String getFacilityType()
	{
		return facilityType;
	}


	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}


	public String getBallFeedingStatus()
	{
		return ballFeedingStatus;
	}


	public void setBallFeedingStatus(String ballFeedingStatus)
	{
		this.ballFeedingStatus = ballFeedingStatus;
	}


	public String getFactorySerialNo() {
		return factorySerialNo;
	}


	public void setFactorySerialNo(String factorySerialNo) {
		this.factorySerialNo = factorySerialNo;
	}

}