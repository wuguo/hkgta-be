package com.sinodynamic.hkgta.dto.pos;

import java.math.BigInteger;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestaurantMenuCatDto
{
	private String cat_id;
	private String category_name;
	private BigInteger cnt;
	public String getCat_id()
	{
		return cat_id;
	}
	public void setCat_id(String cat_id)
	{
		this.cat_id = cat_id;
	}
	public String getCategory_name()
	{
		return category_name;
	}
	public void setCategory_name(String category_name)
	{
		this.category_name = category_name;
	}
	public BigInteger getCnt() {
		return cnt;
	}
	public void setCnt(BigInteger cnt) {
		this.cnt = cnt;
	}

}
