package com.sinodynamic.hkgta.dto.pms;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class StaffRosterWeeklyPresetRecordDto {

	private Long weekDay;

	private boolean offDuty;

	private List<StaffRosterTimeDto> timeList;

	

	public Long getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(Long weekDay) {
		this.weekDay = weekDay;
	}

	public boolean isOffDuty() {
		return offDuty;
	}

	public void setOffDuty(boolean offDuty) {
		this.offDuty = offDuty;
	}

	public List<StaffRosterTimeDto> getTimeList() {
		return timeList;
	}

	public void setTimeList(List<StaffRosterTimeDto> timeList) {
		this.timeList = timeList;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
