package com.sinodynamic.hkgta.dto.crm;

public class DaypassTypeDto {
	
		private String planNo;
		private String planName;
		private String ageRange;
		
		
		
		public DaypassTypeDto() {
			super();
		}
		public DaypassTypeDto(String planNo, String planName, String ageRange) {
			super();
			this.planNo = planNo;
			this.planName = planName;
			this.ageRange = ageRange;
		}
		public String getPlanNo() {
			return planNo;
		}
		public void setPlanNo(String planNo) {
			this.planNo = planNo;
		}
		public String getPlanName() {
			return planName;
		}
		public void setPlanName(String planName) {
			this.planName = planName;
		}
		public String getAgeRange() {
			return ageRange;
		}
		public void setAgeRange(String ageRange) {
			this.ageRange = ageRange;
		}
		
		
	
}
