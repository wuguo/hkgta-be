/**
 * 
 */
package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;
import java.sql.Date;

/**
 * @author Tony_Dong
 *
 */
public class CoachMsgDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3270048453830496863L;
	
	private String content;
	
	private String noticetype;
	
	private String recipienttype;
	
	private Date noticebegin;
	
	private Date noticeend;
	
	private Date createdate;
	
	private String createby;
	
	private Date updatedate;
	
	private String updateby;
	
	private String recipientUserId;
	
	private String msgStatus;
	
	
	/**
	 * @return the noticetype
	 */
	public String getNoticetype() {
		return noticetype;
	}

	/**
	 * @param noticetype the noticetype to set
	 */
	public void setNoticetype(String noticetype) {
		this.noticetype = noticetype;
	}

	/**
	 * @return the recipienttype
	 */
	public String getRecipienttype() {
		return recipienttype;
	}

	/**
	 * @param recipienttype the recipienttype to set
	 */
	public void setRecipienttype(String recipienttype) {
		this.recipienttype = recipienttype;
	}

	/**
	 * @return the noticebegin
	 */
	public Date getNoticebegin() {
		return noticebegin;
	}

	/**
	 * @param noticebegin the noticebegin to set
	 */
	public void setNoticebegin(Date noticebegin) {
		this.noticebegin = noticebegin;
	}

	/**
	 * @return the noticeend
	 */
	public Date getNoticeend() {
		return noticeend;
	}

	/**
	 * @param noticeend the noticeend to set
	 */
	public void setNoticeend(Date noticeend) {
		this.noticeend = noticeend;
	}

	/**
	 * @return the createdate
	 */
	public Date getCreatedate() {
		return createdate;
	}

	/**
	 * @param createdate the createdate to set
	 */
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	/**
	 * @return the createby
	 */
	public String getCreateby() {
		return createby;
	}

	/**
	 * @param createby the createby to set
	 */
	public void setCreateby(String createby) {
		this.createby = createby;
	}

	/**
	 * @return the updatedate
	 */
	public Date getUpdatedate() {
		return updatedate;
	}

	/**
	 * @param updatedate the updatedate to set
	 */
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	/**
	 * @return the updateby
	 */
	public String getUpdateby() {
		return updateby;
	}

	/**
	 * @param updateby the updateby to set
	 */
	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}


	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}



	/**
	 * @return the recipientUserId
	 */
	public String getRecipientUserId() {
		return recipientUserId;
	}

	/**
	 * @param recipientUserId the recipientUserId to set
	 */
	public void setRecipientUserId(String recipientUserId) {
		this.recipientUserId = recipientUserId;
	}

	/**
	 * @return the msgStatus
	 */
	public String getMsgStatus() {
		return msgStatus;
	}

	/**
	 * @param msgStatus the msgStatus to set
	 */
	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}


}
