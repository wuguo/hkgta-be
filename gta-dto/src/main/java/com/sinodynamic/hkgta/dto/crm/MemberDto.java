package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

//added by Kaster 20160323 增加extends GenericDto
public class MemberDto extends GenericDto implements Serializable{
	
	//added by Kaster 20160323
	@EncryptFieldInfo
	private Long customerId;
	
	private String academyNo;
	
	private String customerName;
	
	private BigDecimal spendSum;
	
	private BigDecimal creditLimit;
	
	private BigDecimal availableBalance;
	
	private BigDecimal remainSum;
	
	private BigDecimal  exchgFactor;
	
	private Date expiryDate;

	private String academyNoStatus;
	
	private String academyNoReserveStatus;
	
	private String portraitPhoto;
	
	private String primaryMemberFullName;
	
	private Boolean dependentOrPrimary;
	
	private Boolean isCorporateMember;
	
	private boolean dependentCreationRight;
	
	@EncryptFieldInfo
	private Long superiorMemberId;
	
	private boolean isBackEndApi;
	
	private String phoneMobile;
	
	public String getPhoneMobile() {
		return phoneMobile;
	}
	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}
	
	public boolean isBackEndApi() {
		return isBackEndApi;
	}


	public void setBackEndApi(boolean isBackEndApi) {
		this.isBackEndApi = isBackEndApi;
	}


	public Long getCustomerId() {
		return customerId;
	}


	public void setCustomerId(Object customerId) {
		if(customerId instanceof BigInteger){
			this.customerId = ((BigInteger) customerId).longValue();
		}else if(customerId instanceof Integer){
			this.customerId = ((Integer) customerId).longValue();
		}else if(customerId instanceof String){
			this.customerId = Long.valueOf((String) customerId);
		}else{
			this.customerId = (Long) customerId;
		}
	}

	public String getAcademyNo() {
		return academyNo;
	}

	public void setAcademyNo(String academyNo) {
		this.academyNo = academyNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public BigDecimal getSpendSum() {
		return spendSum;
	}

	public void setSpendSum(BigDecimal spendSum) {
		this.spendSum = spendSum;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public BigDecimal getRemainSum() {
		return remainSum;
	}

	public void setRemainSum(BigDecimal remainSum) {
		this.remainSum = remainSum;
	}
	public BigDecimal getExchgFactor() {
		return exchgFactor;
	}

	public void setExchgFactor(BigDecimal exchgFactor) {
		this.exchgFactor = exchgFactor;
	}


	public Date getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public String getAcademyNoStatus() {
		return academyNoStatus;
	}

	
	public void setAcademyNoStatus(String academyNoStatus) {
		this.academyNoStatus = academyNoStatus;
	}


	public String getAcademyNoReserveStatus() {
		return academyNoReserveStatus;
	}


	public void setAcademyNoReserveStatus(String academyNoReserveStatus) {
		this.academyNoReserveStatus = academyNoReserveStatus;
	}


	public String getPortraitPhoto() {
		return portraitPhoto;
	}


	public void setPortraitPhoto(String portraitPhoto) {
		this.portraitPhoto = portraitPhoto;
	}


	public String getPrimaryMemberFullName() {
		return primaryMemberFullName;
	}

	public void setPrimaryMemberFullName(String primaryMemberFullName) {
		this.primaryMemberFullName = primaryMemberFullName;
	}


	public Boolean isDependentOrPrimary() {
		return dependentOrPrimary;
	}

	public void setDependentOrPrimary(Boolean dependentOrPrimary) {
		this.dependentOrPrimary = dependentOrPrimary;
	}


	public Long getSuperiorMemberId() {
		return superiorMemberId;
	}


	public void setSuperiorMemberId(Long superiorMemberId) {
		this.superiorMemberId = superiorMemberId;
	}


	public Boolean getIsCorporateMember() {
		return isCorporateMember;
	}


	public void setIsCorporateMember(Boolean isCorporateMember) {
		this.isCorporateMember = isCorporateMember;
	}
	
	public boolean isDependentCreationRight() {
		return dependentCreationRight;
	}
	
	public void setDependentCreationRight(boolean dependentCreationRight) {
		this.dependentCreationRight = dependentCreationRight;
	}

}
