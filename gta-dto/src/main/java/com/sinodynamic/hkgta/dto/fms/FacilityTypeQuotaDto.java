package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.util.Date;

public class FacilityTypeQuotaDto implements Serializable {

	private static final long serialVersionUID = 430887760348794557L;

	private Long sysId;
	private String facilityType;
	private Long defaultQuota;
	private Date specificDate;
	private Long quota;
	private String updateBy;
	private String createBy;
	private Date updateDate;
	private Date createDate;
	
	public String getFacilityType()
	{
		return facilityType;
	}
	public void setFacilityType(String facilityType)
	{
		this.facilityType = facilityType;
	}
	public Date getSpecificDate()
	{
		return specificDate;
	}
	public void setSpecificDate(Date specificDate)
	{
		this.specificDate = specificDate;
	}
	public Long getSysId()
	{
		return sysId;
	}
	public void setSysId(Long sysId)
	{
		this.sysId = sysId;
	}
	public Long getDefaultQuota()
	{
		return defaultQuota;
	}
	public void setDefaultQuota(Long defaultQuota)
	{
		this.defaultQuota = defaultQuota;
	}
	public Long getQuota()
	{
		return quota;
	}
	public void setQuota(Long quota)
	{
		this.quota = quota;
	}
	public String getUpdateBy()
	{
		return updateBy;
	}
	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}
	public String getCreateBy()
	{
		return createBy;
	}
	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}
	public Date getUpdateDate()
	{
		return updateDate;
	}
	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}
	public Date getCreateDate()
	{
		return createDate;
	}
	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}


}
