package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class IBeaconNearbyPersonDto  extends GenericDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EncryptFieldInfo  
	private Long customerId;
	
	private Long minor;
	
	private Long major;
	
	private String distance;
	
	private String uuid;
	
	private String targetType;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getMinor() {
		return minor;
	}

	public void setMinor(Long minor) {
		this.minor = minor;
	}

	public Long getMajor() {
		return major;
	}

	public void setMajor(Long major) {
		this.major = major;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
	
}
