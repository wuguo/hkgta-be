package com.sinodynamic.hkgta.dto.account;

import java.util.ArrayList;
import java.util.List;

public class DDIReportDto {
	
	private String creditAccountNumber;
	private String startDate;
	private String endDate;
	private List<DDIReportDetailDto> details;
	private int noOfRecord;
	private String errorMsg;
	
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public DDIReportDto(){
		super();
		details = new ArrayList<DDIReportDetailDto>();
	}
	
	public String getCreditAccountNumber() {
		return creditAccountNumber;
	}

	public void setCreditAccountNumber(String creditAccountNumber) {
		this.creditAccountNumber = creditAccountNumber;
	}


	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public List<DDIReportDetailDto> getDetails() {
		return details;
	}
	public void setDetails(List<DDIReportDetailDto> details) {
		this.details = details;
	}
	public int getNoOfRecord() {
		return noOfRecord;
	}
	public void setNoOfRecord(int noOfRecord) {
		this.noOfRecord = noOfRecord;
	}

	@Override
	public String toString() {
		return "DDIReport [startDate=" + startDate + ", endDate=" + endDate
				+ ", details=" + details + ", noOfRecord=" + noOfRecord + "]";
	}
	
	

}
