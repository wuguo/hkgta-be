package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class BIFacilitiesDto implements Serializable 
{
	private String facilityType;
	
	private BigDecimal totalAvailable;
	
	private String month;

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public BigDecimal getTotalAvailable() {
		return totalAvailable;
	}

	public void setTotalAvailable(Object totalAvailable) {
		if(totalAvailable instanceof BigDecimal){
			this.totalAvailable = (BigDecimal)totalAvailable;
		}else if(totalAvailable instanceof BigInteger)
		{
			this.totalAvailable = BigDecimal.valueOf(Double.valueOf(totalAvailable.toString()));
		}
		
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}
	
	
}
