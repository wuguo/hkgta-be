package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import com.sinodynamic.hkgta.entity.adm.AppsVersion;

public class AppsVersionInfoDto implements Serializable {
	private String appId;
	private String latestVersion;	//版本号用于显示
	private String versionInfo;
	private String downloadLink;
	private double version;     //双精度版本号，用于比较版本大小
	
	public AppsVersionInfoDto(){
		super();
	}
	public AppsVersionInfoDto(AppsVersion info, double version){
		this.appId = info.getAppId();
		this.latestVersion = info.getLatestVersion();
		this.versionInfo = info.getVersionInfo();
		this.downloadLink = info.getDownloadLink();
		this.version = version; 
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getLatestVersion() {
		return latestVersion;
	}
	public void setLatestVersion(String latestVersion) {
		this.latestVersion = latestVersion;
	}
	public String getVersionInfo() {
		return versionInfo;
	}
	public void setVersionInfo(String versionInfo) {
		this.versionInfo = versionInfo;
	}
	public String getDownloadLink() {
		return downloadLink;
	}
	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}
	public double getVersion() {
		return version;
	}
	public void setVersion(double version) {
		this.version = version;
	}
	
}
