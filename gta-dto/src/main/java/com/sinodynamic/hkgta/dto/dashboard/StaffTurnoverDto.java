package com.sinodynamic.hkgta.dto.dashboard;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * @author Kaster 20160413
 *
 */
public class StaffTurnoverDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String staffId;
	private BigDecimal turnover;
	public String getStaffId() {
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	public BigDecimal getTurnover() {
		return turnover;
	}
	public void setTurnover(BigDecimal turnover) {
		this.turnover = turnover;
	}
	
}
