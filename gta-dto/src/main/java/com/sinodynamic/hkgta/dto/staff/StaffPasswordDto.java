package com.sinodynamic.hkgta.dto.staff;

import java.io.Serializable;

public class StaffPasswordDto implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String userId;
	
	private String oldPsw;
	
	private String newPsw;
	
	private String repeatPsw;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getOldPsw()
	{
		return oldPsw;
	}

	public void setOldPsw(String oldPsw)
	{
		this.oldPsw = oldPsw;
	}

	public String getNewPsw()
	{
		return newPsw;
	}

	public void setNewPsw(String newPsw)
	{
		this.newPsw = newPsw;
	}

	public String getRepeatPsw()
	{
		return repeatPsw;
	}

	public void setRepeatPsw(String repeatPsw)
	{
		this.repeatPsw = repeatPsw;
	}
	
	

}
