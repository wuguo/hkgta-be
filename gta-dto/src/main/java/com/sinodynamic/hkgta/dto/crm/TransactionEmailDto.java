package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class TransactionEmailDto implements Serializable{

	private static final long serialVersionUID = -1084077594841939352L;
	
	private String subject;
	
	private String cc;
	
	private String emailContent;
	
	private String emailType;
	
	private String sendTo;
	
	private Long orderNO;
	
	private Long transactionNO;

	/** orderNO/transactionNo */
	private Long number;
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getEmailContent() {
		return emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}
	

	public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}
	
	public Long getOrderNO() {
		return orderNO;
	}

	public void setOrderNO(Long orderNO) {
		this.number = orderNO;
		this.orderNO = orderNO;
	}

	public Long getTransactionNO() {
		return transactionNO;
	}

	public void setTransactionNO(Long transactionNO) {
		this.number = transactionNO;
		this.transactionNO = transactionNO;
	}

	@Override
	public String toString() {
		return "TransactionEmailDto [subject=" + subject + ", cc=" + cc
				+ ", emailContent=" + emailContent + ", emailType=" + emailType
				+ ", sendTo=" + sendTo + ", orderNO=" + orderNO
				+ ", transactionNO=" + transactionNO + ", number=" + number
				+ "]";
	}
	
}
