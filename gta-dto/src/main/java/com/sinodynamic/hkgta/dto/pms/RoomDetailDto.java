package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RoomDetailDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long roomId;

	private String roomNo;

	private String status;
	
	private String frontdeskStatus;
	
	private String serviceStatus;
	
	private String calculatedStatus;

	private List<RoomHousekeepTaskDto> taskList;

	private List<RoomCrewDto> roomCrewList;
	
	//added by Kaster 20160407
	private Long adHocCount;
	private Long routineCount;
	private Long scheduleCount;
	private Long maintenanceCount;

	public Long getAdHocCount() {
		return adHocCount;
	}

	public void setAdHocCount(Long adHocCount) {
		this.adHocCount = adHocCount;
	}

	public Long getRoutineCount() {
		return routineCount;
	}

	public void setRoutineCount(Long routineCount) {
		this.routineCount = routineCount;
	}

	public Long getScheduleCount() {
		return scheduleCount;
	}

	public void setScheduleCount(Long scheduleCount) {
		this.scheduleCount = scheduleCount;
	}

	public Long getMaintenanceCount() {
		return maintenanceCount;
	}

	public void setMaintenanceCount(Long maintenanceCount) {
		this.maintenanceCount = maintenanceCount;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public String getCalculatedStatus() {
		return calculatedStatus;
	}

	public void setCalculatedStatus(String calculatedStatus) {
		this.calculatedStatus = calculatedStatus;
	}

	public List<RoomHousekeepTaskDto> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<RoomHousekeepTaskDto> taskList) {
		this.taskList = taskList;
	}

	public List<RoomCrewDto> getRoomCrewList() {
		return roomCrewList;
	}

	public void setRoomCrewList(List<RoomCrewDto> roomCrewList) {
		this.roomCrewList = roomCrewList;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getFrontdeskStatus()
	{
		return frontdeskStatus;
	}

	public void setFrontdeskStatus(String frontdeskStatus)
	{
		this.frontdeskStatus = frontdeskStatus;
	}
}
