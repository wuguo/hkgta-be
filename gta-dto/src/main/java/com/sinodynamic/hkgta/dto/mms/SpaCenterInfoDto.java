package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;

public class SpaCenterInfoDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long centerId;
	 
    private String centerName;

    private String location;

    private String openHour;
    
    private String phone;
    
    private String appPicFilename;

    private String advanceBookPeriod;
    
        
	public String getAdvanceBookPeriod() {
		return advanceBookPeriod;
	}

	public void setAdvanceBookPeriod(String advanceBookPeriod) {
		this.advanceBookPeriod = advanceBookPeriod;
	}

	public String getAppPicFilename() {
		return appPicFilename;
	}

	public void setAppPicFilename(String appPicFilename) {
		this.appPicFilename = appPicFilename;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getCenterId() {
		return centerId;
	}

	public void setCenterId(Long centerId) {
		this.centerId = centerId;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOpenHour() {
		return openHour;
	}

	public void setOpenHour(String openHour) {
		this.openHour = openHour;
	}
    
    
}
