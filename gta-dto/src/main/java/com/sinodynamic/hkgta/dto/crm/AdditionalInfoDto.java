package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class AdditionalInfoDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public AdditionalInfoDto() {
		super();
	}
	
	private Integer  captionId;
	
	private Long customerId;
	
	private String caption;

	private String inputData;
	
	private String category;

	public Integer getCaptionId() {
		return captionId;
	}

	public void setCaptionId(Integer captionId) {
		this.captionId = captionId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		this.customerId = (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getInputData() {
		return inputData;
	}

	public void setInputData(String inputData) {
		this.inputData = inputData;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
}