package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RoomTaskDto implements Serializable {

	private static final long serialVersionUID = 571038812637263057L;
	private Long taskId;
	private String jobType;
	private String status;
	private Boolean noResponse;
	private Date startTimestamp;
	private Date finishTimestamp;

	

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getNoResponse() {
		return noResponse;
	}

	public void setNoResponse(Boolean noResponse) {
		this.noResponse = noResponse;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public Date getStartTimestamp()
	{
		return startTimestamp;
	}

	public void setStartTimestamp(Date startTimestamp)
	{
		this.startTimestamp = startTimestamp;
	}

	public Date getFinishTimestamp()
	{
		return finishTimestamp;
	}

	public void setFinishTimestamp(Date finishTimestamp)
	{
		this.finishTimestamp = finishTimestamp;
	}
}
