package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class CustomerLeadNoteDto  extends GenericDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long sysId;

	private String subject;
	
	private String content;
	
	private String author;
	
	private String noteDate;
	
	private String backNoteDate;
	@EncryptFieldInfo
	private Long customerId;
	
	private String loginUserId;	
	
	private Date createDate;

	private String createBy;

	private Date updateDate;

	private String updateBy;

	private Long version;
	

	public String getBackNoteDate() {
		return backNoteDate;
	}

	public void setBackNoteDate(String backNoteDate) {
		this.backNoteDate = backNoteDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
   
	public Long getSysId() {
		return sysId;
	}

	public void setSysId(Object sysId) {
		if (sysId != null)
		   this.sysId = Long.valueOf(sysId.toString());
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(Object subject) {
		if (subject != null)
		   this.subject = subject.toString();
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getNoteDate() {
		return noteDate;
	}

	public void setNoteDate(Object noteDate) {
		if (noteDate != null){
		   if (noteDate instanceof Date)
		      this.noteDate = new SimpleDateFormat("yyyy-MM-dd").format(noteDate);
		   else 
		     this.noteDate = noteDate.toString();	
		}	
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Object customerId) {
		if (customerId != null)
		    this.customerId = Long.valueOf(customerId.toString());
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}
}
