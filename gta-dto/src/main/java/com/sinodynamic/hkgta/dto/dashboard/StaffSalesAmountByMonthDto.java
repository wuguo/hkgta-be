package com.sinodynamic.hkgta.dto.dashboard;

/**
 * 
 * @author Kaster 20160413
 *
 */
public class StaffSalesAmountByMonthDto extends StaffSalesAmountDto {

	private static final long serialVersionUID = 1L;
	
	private String yearMonth;
	
	//以下三个变量为别表示admission的数量、admission的营业额相对于上一个月的增减情况。
	//用1、0、-1分别表示增、平、减。
	private int upOrDownNum;
	private int upOrDownAdm;
	
	public int getUpOrDownNum() {
		return upOrDownNum;
	}

	public void setUpOrDownNum(int upOrDownNum) {
		this.upOrDownNum = upOrDownNum;
	}

	public int getUpOrDownAdm() {
		return upOrDownAdm;
	}

	public void setUpOrDownAdm(int upOrDownAdm) {
		this.upOrDownAdm = upOrDownAdm;
	}

	public String getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}
}
