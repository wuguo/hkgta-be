package com.sinodynamic.hkgta.dto.statement;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;


public class StatementParamsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String year;
	private String month;
	private boolean isAll;
	private Long[] customerIds;
	private boolean isSent;
	private String userId;
	
	/***
	 * Select one statement and click Send All 1 Statements will send more than one statement
	 * search condition pass to BE ,to send search condition to send statements
	 */
	private String sortBy;
	private Integer pageNumber;
	private Integer pageSize;
	private String isAscending;
	private String filters;
	private String memberType;
	private String deliveryStatus;
	
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		if(StringUtils.isEmpty(sortBy)){
			sortBy="customerId";//default
		}
		this.sortBy = sortBy;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		if(null==pageNumber){
			pageNumber=1;//default
		}
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getIsAscending() {
		if(StringUtils.isEmpty(isAscending)){
			isAscending="true";//default
		}
		return isAscending;
	}
	public void setIsAscending(String isAscending) {
		this.isAscending = isAscending;
	}
	public String getFilters() {
		return filters;
	}
	public void setFilters(String filters) {
		this.filters = filters;
	}
	public String getMemberType() {
		if(StringUtils.isEmpty(memberType)){
			memberType="all";//default
		}
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getDeliveryStatus() {
		if(StringUtils.isEmpty(deliveryStatus)){
			deliveryStatus="all";//default
		}
		return deliveryStatus;
	}
	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public boolean getIsAll() {
		return isAll;
	}
	public void setIsAll(boolean isAll) {
		this.isAll = isAll;
	}
	public Long[] getCustomerIds() {
		return customerIds;
	}
	public void setCustomerIds(Long[] customerIds) {
		this.customerIds = customerIds;
	}
	public boolean isSent() {
		return isSent;
	}
	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}
	public void setAll(boolean isAll) {
		this.isAll = isAll;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
