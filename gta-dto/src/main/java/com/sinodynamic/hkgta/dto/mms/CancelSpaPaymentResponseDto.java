package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;


public class CancelSpaPaymentResponseDto implements Serializable {
	private static final long serialVersionUID = -922986115802375103L;
	private String returnCode;
	private Data data;

	/**
	 * 
	 * @return The returnCode
	 */
	public String getReturnCode() {
		return returnCode;
	}

	/**
	 * 
	 * @param returnCode
	 *            The returnCode
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * 
	 * @return The data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * 
	 * @param data
	 *            The data
	 */
	public void setData(Data data) {
		this.data = data;
	}

	public class Data {
		private String uniqueIdentifier;

		public String getUniqueIdentifier() {
			return uniqueIdentifier;
		}

		public void setUniqueIdentifier(String uniqueIdentifier) {
			this.uniqueIdentifier = uniqueIdentifier;
		}
		
		
	}
}
