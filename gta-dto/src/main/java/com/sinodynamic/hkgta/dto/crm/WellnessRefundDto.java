package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.dto.memberapp.CandidateCustomerDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;

public class WellnessRefundDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long customerId;
	private Integer refundId;
	private String service;
	private String memberName;
	private String acdemyID;
	private Map<String, CustomerOrderDetDto> itemMap = new HashMap<String, CustomerOrderDetDto>();
	private BigDecimal totalPrice;
	private String paymentMethodCode;
	private Long transactionID;
	private String requestDate;
	private String requestBy;
	private String refundMethod;
	private String status;
	private BigDecimal refundAmout;
	private String refundRemark;
	private String approvalRemark;
	
	public Long getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
	public Integer getRefundId() {
		return refundId;
	}
	public void setRefundId(Integer refundId) {
		this.refundId = refundId;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getAcdemyID() {
		return acdemyID;
	}
	public void setAcdemyID(String acdemyID) {
		this.acdemyID = acdemyID;
	}
	public Map<String, CustomerOrderDetDto> getItemMap() {
		return itemMap;
	}
	public void setItemMap(Map<String, CustomerOrderDetDto> itemMap) {
		this.itemMap = itemMap;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}
	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}
	public Long getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public String getRequestBy() {
		return requestBy;
	}
	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}
	public String getRefundMethod() {
		return refundMethod;
	}
	public void setRefundMethod(String refundMethod) {
		this.refundMethod = refundMethod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getRefundAmout() {
		return refundAmout;
	}
	public void setRefundAmout(BigDecimal refundAmout) {
		this.refundAmout = refundAmout;
	}
	public String getRefundRemark() {
		return refundRemark;
	}
	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}
	public String getApprovalRemark() {
		return approvalRemark;
	}
	public void setApprovalRemark(String approvalRemark) {
		this.approvalRemark = approvalRemark;
	}
	@JsonIgnore
	public void setCustomerId2(BigInteger customerId) {
		if(null !=customerId) {
			this.customerId = customerId.longValue();
		}
	}

//	@JsonIgnore
//	public void setRefundId2(Long refundId) {
//		if(null !=refundId) {
//			this.refundId = refundId.longValue();
//		}
//	}
	@JsonIgnore
	public void setTransactionID2(BigInteger transactionID) {
		if(null !=transactionID) {
			this.transactionID = transactionID.longValue();
		}
	}
}
