package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
/***
 * sourceBooking report
 * @author christ
 *
 */
public class SourceBookingDto implements Serializable {
	
	private String serviceType;
	
	private String reservationId;
	
	private String patronId;
	
	private String patronName;
	
	private String startTime;
	
	private String endTime;
	
	private String location;
	
	private String  sourceType;
	
	private String  status;
	
	private String createBy;
	
	private String createDate;
	
	private String qty;

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	public String getPatronId() {
		return patronId;
	}

	public void setPatronId(String patronId) {
		this.patronId = patronId;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {		
		this.qty = qty;
	}
	

}
