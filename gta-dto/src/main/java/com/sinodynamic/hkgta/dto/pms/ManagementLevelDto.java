package com.sinodynamic.hkgta.dto.pms;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ManagementLevelDto {

	private String positionTitleName = null;

	private String positionTitleCode = null;

	private List<ManagementStaffDto> staffList = null;

	public String getPositionTitleName() {
		return positionTitleName;
	}

	public void setPositionTitleName(String positionTitleName) {
		this.positionTitleName = positionTitleName;
	}

	public String getPositionTitleCode() {
		return positionTitleCode;
	}

	public void setPositionTitleCode(String positionTitleCode) {
		this.positionTitleCode = positionTitleCode;
	}

	public List<ManagementStaffDto> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<ManagementStaffDto> staffList) {
		this.staffList = staffList;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
