package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

public class RoomDeskDto implements Serializable {
	private String room;
	
	private String roomtype;
	
	private String fdstatus;

	public String getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getFdstatus() {
		return fdstatus;
	}

	public void setFdstatus(String fdstatus) {
		this.fdstatus = fdstatus;
	}
}
