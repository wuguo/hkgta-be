package com.sinodynamic.hkgta.dto.account;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DDASuccessReportDetailDto implements Serializable{
	
	private String originatorId;
	private String originatorBankCode; //003 - SCB
	private String originatorAccountNumber;
	private String ddaType; // ? // S - SCB
	private String buyerCode; // ? //"" if empty
	private String buyerName; // ? //"" if empty
	
	private String debtorBankCode;//8
	
	private String debtorBankName;//9
	
	private String draweeBranchCode; // "" if empty
	
	private String debtorAccountNumber;//patron virtual account number
	
	private String debtorAccountName;
	
	private String startDate;//YYYYMMDD
	private String endDate;//YYYYMMDD
	private String mandateType; // ? //"" if empty or FIXED or VARIABLE 
	private String frequency; // ? //"" if empty
	private String amount; //24,6
	
	private String remarks; 
	
	private String valueDate; // ? //format:YYYYMMDD  21
	
	private String revisedReference; //40
	
	private String transactionId; // ? //"" if empty
	private String mandateRemarks; // "A"
	private String debtorId; // ? //"" if empty
	private String rcmsMandateManagementId; // ?
	private String reference; // our membership id -- VA account no.
	
	private String mandateStatus; //Accepted 30
	
	private String mandateStatusCode; //A  31
	
	private String currencyCode; //? - HKD ? 32
	
	private String debtorIdType2; // ?
	private String debtorIdNumber2; // ?
	
	private String patronId;
	
	private String patronName;
	
	private String rejectCode;//19
	
	private String rejectReason;//20
	
	private String debtorReferenceNumber ;//23
	
	private String gtaStatus;
	
	private String gtaRemark;
	
	private String actionIndicator;
	
	public String getActionIndicator() {
		return actionIndicator;
	}
	public void setActionIndicator(String actionIndicator) {
		this.actionIndicator = actionIndicator;
	}
	public String getGtaStatus() {
		return gtaStatus;
	}
	public void setGtaStatus(String gtaStatus) {
		this.gtaStatus = gtaStatus;
	}
	public String getGtaRemark() {
		return gtaRemark;
	}
	public void setGtaRemark(String gtaRemark) {
		this.gtaRemark = gtaRemark;
	}
	/***
	 * patron virtual account number [accountNo]
	 * @return
	 */
	public String getDebtorReferenceNumber() {
		return debtorReferenceNumber;
	}
	public void setDebtorReferenceNumber(String debtorReferenceNumber) {
		this.debtorReferenceNumber = debtorReferenceNumber;
	}
	public String getDebtorBankCode() {
		return debtorBankCode;
	}
	public void setDebtorBankCode(String debtorBankCode) {
		this.debtorBankCode = debtorBankCode;
	}
	public String getDebtorBankName() {
		return debtorBankName;
	}
	public void setDebtorBankName(String debtorBankName) {
		this.debtorBankName = debtorBankName;
	}
	public String getDebtorAccountNumber() {
		return debtorAccountNumber;
	}
	public void setDebtorAccountNumber(String debtorAccountNumber) {
		this.debtorAccountNumber = debtorAccountNumber;
	}
	public String getDebtorAccountName() {
		return debtorAccountName;
	}
	public void setDebtorAccountName(String debtorAccountName) {
		this.debtorAccountName = debtorAccountName;
	}
	public String getRevisedReference() {
		return revisedReference;
	}
	public void setRevisedReference(String revisedReference) {
		this.revisedReference = revisedReference;
	}
	public String getRejectCode() {
		return rejectCode;
	}
	public void setRejectCode(String rejectCode) {
		this.rejectCode = rejectCode;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public String getPatronId() {
		return patronId;
	}
	public void setPatronId(String patronId) {
		this.patronId = patronId;
	}
	public String getPatronName() {
		return patronName;
	}
	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}
	public String getOriginatorId() {
		return originatorId;
	}
	public void setOriginatorId(String originatorId) {
		this.originatorId = originatorId;
	}
	public String getOriginatorBankCode() {
		return originatorBankCode;
	}
	public void setOriginatorBankCode(String originatorBankCode) {
		this.originatorBankCode = originatorBankCode;
	}
	public String getOriginatorAccountNumber() {
		return originatorAccountNumber;
	}
	public void setOriginatorAccountNumber(String originatorAccountNumber) {
		this.originatorAccountNumber = originatorAccountNumber;
	}
	public String getDdaType() {
		return ddaType;
	}
	public void setDdaType(String ddaType) {
		this.ddaType = ddaType;
	}
	public String getBuyerCode() {
		return buyerCode;
	}
	public void setBuyerCode(String buyerCode) {
		this.buyerCode = buyerCode;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getDraweeBranchCode() {
		return draweeBranchCode;
	}
	public void setDraweeBranchCode(String draweeBranchCode) {
		this.draweeBranchCode = draweeBranchCode;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getMandateType() {
		return mandateType;
	}
	public void setMandateType(String mandateType) {
		this.mandateType = mandateType;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getMandateRemarks() {
		return mandateRemarks;
	}
	public void setMandateRemarks(String mandateRemarks) {
		this.mandateRemarks = mandateRemarks;
	}
	public String getDebtorId() {
		return debtorId;
	}
	public void setDebtorId(String debtorId) {
		this.debtorId = debtorId;
	}
	public String getRcmsMandateManagementId() {
		return rcmsMandateManagementId;
	}
	public void setRcmsMandateManagementId(String rcmsMandateManagementId) {
		this.rcmsMandateManagementId = rcmsMandateManagementId;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getMandateStatus() {
		return mandateStatus;
	}
	public void setMandateStatus(String mandateStatus) {
		this.mandateStatus = mandateStatus;
	}
	public String getMandateStatusCode() {
		return mandateStatusCode;
	}
	public void setMandateStatusCode(String mandateStatusCode) {
		this.mandateStatusCode = mandateStatusCode;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getDebtorIdType2() {
		return debtorIdType2;
	}
	public void setDebtorIdType2(String debtorIdType2) {
		this.debtorIdType2 = debtorIdType2;
	}
	public String getDebtorIdNumber2() {
		return debtorIdNumber2;
	}
	public void setDebtorIdNumber2(String debtorIdNumber2) {
		this.debtorIdNumber2 = debtorIdNumber2;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
