package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class GoingToAdultDependentMemberDto extends GenericDto implements Serializable{

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public String getAcademyId() {
		return academyId;
	}

	public void setAcademyId(String academyId) {
		this.academyId = academyId;
	}

	public Date getMembershipExpireDate() {
		return membershipExpireDate;
	}

	public void setMembershipExpireDate(Date membershipExpireDate) {
		this.membershipExpireDate = membershipExpireDate;
	}

	private static final long serialVersionUID = 1L;

	private String userId;
	
	private String saleName;
	
	private String academyId;
	
	private String salesName;
	
	private String dependentName;
	
	private String patronName;
	
	private String salesPersonEmail;
	
	private Date inactiveDate;
	
	private Date membershipExpireDate;
	

	public String getSalesName() {
		return salesName;
	}

	public void setSalesName(String salesName) {
		this.salesName = salesName;
	}

	public String getDependentName() {
		return dependentName;
	}

	public void setDependentName(String dependentName) {
		this.dependentName = dependentName;
	}

	public String getPatronName() {
		return patronName;
	}

	public void setPatronName(String patronName) {
		this.patronName = patronName;
	}

	public String getSalesPersonEmail() {
		return salesPersonEmail;
	}

	public void setSalesPersonEmail(String salesPersonEmail) {
		this.salesPersonEmail = salesPersonEmail;
	}

	public Date getInactiveDate() {
		return inactiveDate;
	}

	public void setInactiveDate(Date inactiveDate) {
		this.inactiveDate = inactiveDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}