package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class AllowMarketingDto extends GenericDto  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public AllowMarketingDto() {
		super();
	}

	@EncryptFieldInfo
	private BigInteger customerId;
	
	private String allowType;

	private boolean isAllow;

	public BigInteger getCustomerId() {
		return customerId;
	}

	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public String getAllowType() {
		return allowType;
	}

	public void setAllowType(String allowType) {
		this.allowType = allowType;
	}

	public boolean isAllow() {
		return isAllow;
	}

	public void setAllow(boolean isAllow) {
		this.isAllow = isAllow;
	}
	
	


	
}