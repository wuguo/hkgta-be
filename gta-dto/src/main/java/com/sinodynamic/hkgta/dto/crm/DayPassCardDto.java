package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

public class DayPassCardDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String user;//cardholder_customer_id
	private String daypassId;// (qr_code + “_” + linked_card_no)
	private String status;// (qr_code + “_” + linked_card_no)
	private String planName;
	private Date effectiveFormat;//
	private String effectiveFrom;
	private Date effectiveTo;//
	private String purchaser;
	private Integer linkedCardNo;
	private String cardStatus;
	
	public String getCardStatus() {
		return cardStatus;
	}
	@JsonIgnore
	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}
	public Date getEffectiveFormat() {
		return effectiveFormat;
	}
	@JsonIgnore
	public void setEffectiveFormat(Date effectiveFormat) {
		this.effectiveFormat = effectiveFormat;
	}
	public String getEffectiveFrom() {
		return effectiveFrom;
	}
	public void setEffectiveFrom(String effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}
	
	public Date getEffectiveTo() {
		return effectiveTo;
	}
	@JsonIgnore
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getDaypassId() {
		return daypassId;
	}
	public void setDaypassId(String daypassId) {
		this.daypassId = daypassId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPurchaser() {
		return purchaser;
	}
	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}
	public Integer getLinkedCardNo() {
		return linkedCardNo;
	}
	@JsonIgnore
	public void setLinkedCardNo(Integer linkedCardNo) {
		this.linkedCardNo = linkedCardNo;
	}
	
	
}
