package com.sinodynamic.hkgta.dto.membership;

public class FacilityTransactionInfo {

	private Long resvId;
	
	private String paymentDate;
	
	private Long transactionNo;
	
	private String location;
	
	private String media;
	
	private String paymentMethod;
	
	private String expense;

	public Long getResvId() {
		return resvId;
	}

	public void setResvId(Long resvId) {
		this.resvId = resvId;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Long getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(Long transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getExpense() {
		return expense;
	}

	public void setExpense(String expense) {
		this.expense = expense;
	}
	
}