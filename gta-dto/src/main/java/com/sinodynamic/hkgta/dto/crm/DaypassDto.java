package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.util.Date;

public class DaypassDto implements Serializable{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = -3248553933937973856L;

	private int pageSize;
	
	private int currentPage;
	
	private  String order;
	
	private  String propertyName;
	
	private Date startDate;
	private Date endDate;
	
	private Long  planNo;
	private String planName;
	private String status;
	private String effectiveEndDate;
	private String effectiveStartDate;
	
	private int daysLater;
	private int monthLater;
	private String subscriberType;
	
	public String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	public int getDaysLater() {
		return daysLater;
	}

	public Long getPlanNo() {
		return planNo;
	}

	public void setPlanNo(Long planNo) {
		this.planNo = planNo;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public void setDaysLater(int daysLater) {
		this.daysLater = daysLater;
	}

	public int getMonthLater() {
		return monthLater;
	}

	public void setMonthLater(int monthLater) {
		this.monthLater = monthLater;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	
	
	

	
	
}
