package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class PaymentOrderDto extends GenericDto implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigInteger orderNo;
    private BigInteger paymentNo;
    private String memberName;
    private Integer planNo;
    private String daypassType;
    private Date activationDate;
    private Date startDate;//used for member app
    private Date expiryDate;
    private Integer cardNo;
    private String itemNo;
    private String cardStatus;
    @EncryptFieldInfo
    private Long customerId;
    private String orderStatus;
    private String periodDate;
    private boolean today;
    private String ageRange;
    private String planName;
    private Long candidateId;

    public PaymentOrderDto(BigInteger paymentNo, String memberName, String daypassType, Date activationDate, Date expiryDate,Integer cardNo, String cardStatus, Long customerId) {
        super();
        this.paymentNo = paymentNo;
        this.memberName = memberName;
        this.daypassType = daypassType;
        this.activationDate = activationDate;
        this.expiryDate = expiryDate;
        this.cardNo = cardNo;
        this.cardStatus = cardStatus;
        this.customerId = customerId;
    }

    public PaymentOrderDto(BigInteger paymentNo, String itemNo, String daypassType, Date activationDate, Long customerId) {
        super();
        this.paymentNo = paymentNo;
        this.itemNo = itemNo;
        this.daypassType = daypassType;
        this.activationDate = activationDate;
        this.customerId = customerId;
    }

    public PaymentOrderDto() {
        super();
    }

    public BigInteger getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(BigInteger orderNo) {
        this.orderNo = orderNo;
    }

    public BigInteger getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(BigInteger paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getDaypassType() {
        return daypassType;
    }

    public void setDaypassType(String daypassType) {
        this.daypassType = daypassType;
    }

    public String getActivationDate() {
        if (activationDate == null) {
            return "";
        }
        return DtoHelper.getYMDDateAndDateDiff(activationDate);
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

	public String getStartDate() {
		if (activationDate == null) {
            return "";
        }
        return DtoHelper.getYMDDate(activationDate);
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getExpiryDate() {
		if (null == expiryDate) {
			return "";
		}
		return DtoHelper.getYMDDate(expiryDate);
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
    public Integer getCardNo() {
        return cardNo;
    }

    public void setCardNo(Integer cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Object customerId) {
    	if (customerId instanceof BigInteger) {
			this.customerId = ((BigInteger) customerId).longValue();
		}
		else if (customerId instanceof Integer) {
			this.customerId = ((Integer) customerId).longValue();
		}
		else if (customerId instanceof String) {
			this.customerId = Long.parseLong((String) customerId);
		}
		else {
			this.customerId = (Long) customerId;
		}
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getPlanNo() {
        return planNo;
    }

    public void setPlanNo(Integer planNo) {
        this.planNo = planNo;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

	public String getPeriodDate() {
		return periodDate;
	}

	public void setPeriodDate(String periodDate) {
		this.periodDate = periodDate;
	}

	public boolean isToday() {
		return today;
	}

	public void setToday(boolean today) {
		this.today = today;
	}


    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

	public Long getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(Object candidateId) {
		if(candidateId instanceof BigInteger){
			this.candidateId = ((BigInteger) candidateId).longValue();
		}else if(candidateId instanceof Integer){
			this.candidateId = ((Integer) candidateId).longValue();
		}else if(candidateId instanceof String){
			this.candidateId = Long.valueOf((String) candidateId);
		}else{
			this.candidateId = (Long) candidateId;
		}
	}
    
}

