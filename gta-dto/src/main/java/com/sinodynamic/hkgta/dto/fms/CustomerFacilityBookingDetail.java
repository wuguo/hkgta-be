package com.sinodynamic.hkgta.dto.fms;

import java.math.BigDecimal;

public class CustomerFacilityBookingDetail
{
	private String name;
	private BigDecimal number;
	private BigDecimal subTotalPrice;
	private BigDecimal unitPrice;
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public BigDecimal getNumber()
	{
		return number;
	}
	public void setNumber(BigDecimal number)
	{
		this.number = number;
	}
	public BigDecimal getSubTotalPrice()
	{
		return subTotalPrice;
	}
	public void setSubTotalPrice(BigDecimal subTotalPrice)
	{
		this.subTotalPrice = subTotalPrice;
	}
	public BigDecimal getUnitPrice()
	{
		return unitPrice;
	}
	public void setUnitPrice(BigDecimal unitPrice)
	{
		this.unitPrice = unitPrice;
	}
}
