package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class FacilitySubtypeDto implements Serializable {

	private static final long serialVersionUID = 188052723853399521L;

	private String subtypeId;
	private String courtName;
	private FacilitySubtypePosDto adultHigh;
	private FacilitySubtypePosDto adultLow;
	private FacilitySubtypePosDto childHigh;
	private FacilitySubtypePosDto childLow;

	public String getSubtypeId() {
		return subtypeId;
	}

	public void setSubtypeId(String subtypeId) {
		this.subtypeId = subtypeId;
	}

	public String getCourtName() {
		return courtName;
	}

	public void setCourtName(String courtName) {
		this.courtName = courtName;
	}

	public FacilitySubtypePosDto getAdultHigh() {
		return adultHigh;
	}

	public void setAdultHigh(FacilitySubtypePosDto adultHigh) {
		this.adultHigh = adultHigh;
	}

	public FacilitySubtypePosDto getAdultLow() {
		return adultLow;
	}

	public void setAdultLow(FacilitySubtypePosDto adultLow) {
		this.adultLow = adultLow;
	}

	public FacilitySubtypePosDto getChildHigh() {
		return childHigh;
	}

	public void setChildHigh(FacilitySubtypePosDto childHigh) {
		this.childHigh = childHigh;
	}

	public FacilitySubtypePosDto getChildLow() {
		return childLow;
	}

	public void setChildLow(FacilitySubtypePosDto childLow) {
		this.childLow = childLow;
	}

}
