package com.sinodynamic.hkgta.dto.dashboard;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * 
 * @author Kaster 20160414
 *
 */
public class AdmAndOpeAccRevenueDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String yearMonth;
	private BigInteger numOfAdmission;
	private BigDecimal accAdmRevenue;
	private BigDecimal accOpeRevenue;
	//以下三个变量为别表示admission的数量、admission的营业额、operational的营业额相对于上一个月的增减情况。
	//用1、0、-1分别表示增、平、减。
	private int upOrDownNum;
	private int upOrDownAdm;
	private int upOrDownOpe;
	
	public int getUpOrDownNum() {
		return upOrDownNum;
	}
	public void setUpOrDownNum(int upOrDownNum) {
		this.upOrDownNum = upOrDownNum;
	}
	public int getUpOrDownAdm() {
		return upOrDownAdm;
	}
	public void setUpOrDownAdm(int upOrDownAdm) {
		this.upOrDownAdm = upOrDownAdm;
	}
	public int getUpOrDownOpe() {
		return upOrDownOpe;
	}
	public void setUpOrDownOpe(int upOrDownOpe) {
		this.upOrDownOpe = upOrDownOpe;
	}
	public String getYearMonth() {
		return yearMonth;
	}
	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}
	public BigInteger getNumOfAdmission() {
		return numOfAdmission;
	}
	public void setNumOfAdmission(BigInteger numOfAdmission) {
		this.numOfAdmission = numOfAdmission;
	}
	public BigDecimal getAccAdmRevenue() {
		return accAdmRevenue;
	}
	public void setAccAdmRevenue(BigDecimal accAdmRevenue) {
		this.accAdmRevenue = accAdmRevenue;
	}
	public BigDecimal getAccOpeRevenue() {
		return accOpeRevenue;
	}
	public void setAccOpeRevenue(BigDecimal accOpeRevenue) {
		this.accOpeRevenue = accOpeRevenue;
	}
}
