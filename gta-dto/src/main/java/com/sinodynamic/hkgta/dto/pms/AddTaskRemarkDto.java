package com.sinodynamic.hkgta.dto.pms;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.sinodynamic.hkgta.entity.pms.HousekeepPresetParameter;

public class AddTaskRemarkDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long taskId; 					//任务id
	private String remark;					//备注说明
	private String userId;					//用户id

	

	public Long getTaskId() {
		return taskId;
	}



	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}



	public String getUserId() {
		return userId;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
