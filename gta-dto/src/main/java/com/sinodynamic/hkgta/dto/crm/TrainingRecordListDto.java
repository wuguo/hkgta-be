package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class TrainingRecordListDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Long customerId;
	public String coachId;
	public Integer pageSize = 10;
	public Integer pageNumber = 1;

	public Long getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(Long customerId)
	{
		this.customerId = customerId;
	}

	public String getCoachId()
	{
		return coachId;
	}

	public void setCoachId(String coachId)
	{
		this.coachId = coachId;
	}

	public Integer getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(Integer pageSize)
	{
		this.pageSize = pageSize;
	}

	public Integer getPageNumber()
	{
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber)
	{
		this.pageNumber = pageNumber;
	}

}
