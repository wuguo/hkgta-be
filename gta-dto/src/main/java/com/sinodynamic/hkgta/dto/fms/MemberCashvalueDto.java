package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.math.NumberUtils;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class MemberCashvalueDto extends GenericDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EncryptFieldInfo
    private Long customerId;
    private String customerName;
    private String academyId;
    private BigDecimal cashValue;
    private Long transactionId;
    private BigDecimal creditLimit;
    private BigDecimal spendingLimit;

    public Long getCustomerId() {
	return customerId;
    }

    public void setCustomerId(Object customerId) {
	this.customerId =  (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);
    }

    public String getCustomerName() {
	return customerName;
    }

    public void setCustomerName(String customerName) {
	this.customerName = customerName;
    }

    public String getAcademyId() {
	return academyId;
    }

    public void setAcademyId(String academyId) {
	this.academyId = academyId;
    }

    public String getCashValue() {
	return cashValue == null ? "0.00" : cashValue.toString();
    }

    public void setCashValue(BigDecimal cashValue) {
	this.cashValue = cashValue;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getCreditLimit() {
        return creditLimit == null ? "0.00" : creditLimit.toString();
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getSpendingLimit() {
        return spendingLimit == null ? "0.00" : spendingLimit.toString();
    }

    public void setSpendingLimit(BigDecimal spendingLimit) {
        this.spendingLimit = spendingLimit;
    }
    
}
