package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

public class MemberCourseAttendanceDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long customerId;
    private String customerName;
    private String portrait;
    private String academyNo;
    private String status;

    public Long getCustomerId() {
	return customerId;
    }

    public void setCustomerId(Object customerId) {
	this.customerId = (customerId!=null ? NumberUtils.toLong(customerId.toString()):null);;
    }

    public String getCustomerName() {
	return customerName;
    }

    public void setCustomerName(String customerName) {
	this.customerName = customerName;
    }

    public String getPortrait() {
	return portrait;
    }

    public void setPortrait(String portrait) {
	this.portrait = portrait;
    }

    public String getAcademyNo() {
	return academyNo;
    }

    public void setAcademyNo(String academyNo) {
	this.academyNo = academyNo;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

}
