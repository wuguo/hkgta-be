package com.sinodynamic.hkgta.dto.pos;

public class RestaurantImageDto {

	private Long imageId;
	
	private String forDeviceType;
	
	private String imageFeatureCode;
	
	private String imageFileName;

	public Long getImageId() {
		return imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public String getForDeviceType() {
		return forDeviceType;
	}

	public void setForDeviceType(String forDeviceType) {
		this.forDeviceType = forDeviceType;
	}

	public String getImageFeatureCode() {
		return imageFeatureCode;
	}

	public void setImageFeatureCode(String imageFeatureCode) {
		this.imageFeatureCode = imageFeatureCode;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
}
