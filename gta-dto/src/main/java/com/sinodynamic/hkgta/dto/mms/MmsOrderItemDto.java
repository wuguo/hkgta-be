package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class MmsOrderItemDto implements Serializable {
    private static final long serialVersionUID = 1L;
    

	private String itemId;
    private String service;
    private String date;
    private String time;
    private String therapist;
    private BigDecimal rate;
    private String status;
    private Date startTime;
    private Date endTime;
    private String appointmentId;
    private String appointmentWholeId;
    private String therapistCode;
    
        
    public String getTherapistCode() {
		return therapistCode;
	}

	public void setTherapistCode(String therapistCode) {
		this.therapistCode = therapistCode;
	}

	public String getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getItemId() {
	return itemId;
    }

    public void setItemId(String itemId) {
	this.itemId = itemId;
    }

    public String getService() {
	return service;
    }

    public void setService(String service) {
	this.service = service;
    }

    public String getDate() {
	return date;
    }

    public void setDate(String date) {
	this.date = date;
    }

    public String getTime() {
	return time;
    }

    public void setTime(String time) {
	this.time = time;
    }

    public String getTherapist() {
	return therapist;
    }

    public void setTherapist(String therapist) {
	this.therapist = therapist;
    }

    public BigDecimal getRate() {
	return rate;
    }

    public void setRate(BigDecimal rate) {
	this.rate = rate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

	public String getAppointmentWholeId() {
		return appointmentWholeId;
	}

	public void setAppointmentWholeId(String appointmentWholeId) {
		this.appointmentWholeId = appointmentWholeId;
	}
    
}
