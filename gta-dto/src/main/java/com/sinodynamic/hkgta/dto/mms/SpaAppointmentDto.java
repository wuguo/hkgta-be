package com.sinodynamic.hkgta.dto.mms;

import java.io.Serializable;
import java.util.List;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;

public class SpaAppointmentDto extends GenericDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@EncryptFieldInfo
	private Long customerId;
	private String memberName;
	private String accademyNo;
	private String invoiceNo;
	
	private String paymentMethod;
	private String remoteType;
	
	private List<SpaAppointmentDetailDto> serviceDetails;

	//added by Kaster 20160331
	private String createdBy;
	private String updateBy;
	
	private String appointmentInfo;
	
	public String getAppointmentInfo() {
		return appointmentInfo;
	}

	public void setAppointmentInfo(String appointmentInfo) {
		this.appointmentInfo = appointmentInfo;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getRemoteType() {
		return remoteType;
	}

	public void setRemoteType(String remoteType) {
		this.remoteType = remoteType;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getAccademyNo() {
		return accademyNo;
	}

	public void setAccademyNo(String accademyNo) {
		this.accademyNo = accademyNo;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public List<SpaAppointmentDetailDto> getServiceDetails() {
		return serviceDetails;
	}

	public void setServiceDetails(List<SpaAppointmentDetailDto> serviceDetails) {
		this.serviceDetails = serviceDetails;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	

}
