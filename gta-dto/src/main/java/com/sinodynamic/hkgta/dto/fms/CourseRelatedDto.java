package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;

public class CourseRelatedDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String coachNo;
    private String[] facilityNo;
    private String editType;
    private String repeatMode;
    private int repeatCount;
    private CourseSessionDto sessionDto;

    public String getEditType() {
		return editType;
	}

	public void setEditType(String editType) {
		this.editType = editType;
	}

	public String getRepeatMode() {
		return repeatMode;
	}

	public void setRepeatMode(String repeatMode) {
		this.repeatMode = repeatMode;
	}

	public int getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(int repeatCount) {
		this.repeatCount = repeatCount;
	}

	public String getCoachNo() {
	return coachNo;
    }

    public void setCoachNo(String coachNo) {
	this.coachNo = coachNo;
    }

    public String[] getFacilityNo() {
	return facilityNo;
    }

    public void setFacilityNo(String[] facilityNo) {
	this.facilityNo = facilityNo;
    }

    public CourseSessionDto getSessionDto() {
	return sessionDto;
    }

    public void setSessionDto(CourseSessionDto sessionDto) {
	this.sessionDto = sessionDto;
    }

}
