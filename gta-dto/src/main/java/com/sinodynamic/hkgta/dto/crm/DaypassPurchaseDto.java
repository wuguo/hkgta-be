package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.GenericDto;
import com.sinodynamic.hkgta.dto.memberapp.CandidateCustomerDto;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderDet;

public class DaypassPurchaseDto extends GenericDto implements Serializable{

	private static final long serialVersionUID = 1L;
	@EncryptFieldInfo
	private Long customerId;
	private String userId;
	/*
	 * used for check customer can buy daypass or not 
	 */
	private Long planNo;
	private String activationDate;
	private String deactivationDate;
	/*
	 * used for calculate taotal amount
	 */
	private List<OneDayDaypassPurchaseDto> days = new ArrayList<OneDayDaypassPurchaseDto>();
	private Long[] daypassNos;
	private Long[] buysNos;
	private BigDecimal totalPrice;
	private Long totalHkgtaQuota;
	private Long totalMemberQuota;
	//1:member  0:not member ,staff
	private int isMember;
	private String memberName;
	private Long orderNo;
	private BigDecimal currentCashvalue;
	private BigDecimal creditLimit;
	private Map<String, CustomerOrderDetDto> itemMap = new HashMap<String, CustomerOrderDetDto>();
	private Long ofPass;
	private String paymentMethodCode;
	//update old id to staffId ,specification attribute, to understand flow
	private String staffId;
	
	public String getStaffId() {
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	private String buyFlag;
	private List<String> errorMsg = new ArrayList<String>();
	private Long candidateId;
	private List<CandidateCustomerDto> candidateCustomerDtos = new ArrayList<CandidateCustomerDto>();
	private String acdemyID;
	private BigDecimal spendingLimit;
	private String cardNo;
	private Long transactionNo;

	private List<Map<String, Object>> paymentDetail = new ArrayList<Map<String,Object>>();
	private BigDecimal availableBalance;
	private Map<String, CustomerOrderDetDto> daypassItem = new HashMap<String, CustomerOrderDetDto>();
	/*
	 * to specify price of daypass
	 * 
	 */
	private List<List<CustomerOrderDetDto>> priceItems = new ArrayList<List<CustomerOrderDetDto>>();
	private String  purchaser; 
	// used for webportal to list member can buy daypass list
	private List<ServicePlanDto> canBuyList = new ArrayList<ServicePlanDto>();
	//used for webprotal.PC can get paymentLocation from session
	private String paymentLocation;
	//used for webprotal,to decide member can daypass at weekend or not
	private String passPeriodType;
	
	public List<List<CustomerOrderDetDto>> getPriceItems() {
		return priceItems;
	}
	public void setPriceItems(List<List<CustomerOrderDetDto>> priceItems) {
		this.priceItems = priceItems;
	}
	public String getPassPeriodType() {
		return passPeriodType;
	}
	public void setPassPeriodType(String passPeriodType) {
		this.passPeriodType = passPeriodType;
	}
	public String getPaymentLocation() {
		return paymentLocation;
	}
	public void setPaymentLocation(String paymentLocation) {
		this.paymentLocation = paymentLocation;
	}
	public List<ServicePlanDto> getCanBuyList() {
		return canBuyList;
	}
	public void setCanBuyList(List<ServicePlanDto> canBuyList) {
		this.canBuyList = canBuyList;
	}
	public String getPurchaser() {
		return purchaser;
	}
	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}
	public Map<String, CustomerOrderDetDto> getDaypassItem() {
		return daypassItem;
	}
	public void setDaypassItem(Map<String, CustomerOrderDetDto> daypassItem) {
		this.daypassItem = daypassItem;
	}
	public Long getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(Long transactionNo) {
		this.transactionNo = transactionNo;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getAcdemyID() {
		return acdemyID;
	}
	public void setAcdemyID(String acdemyID) {
		this.acdemyID = acdemyID;
	}
	public BigDecimal getSpendingLimit() {
		return spendingLimit;
	}
	public void setSpendingLimit(BigDecimal spendingLimit) {
		this.spendingLimit = spendingLimit;
	}
	public String getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}
	public String getDeactivationDate() {
		return deactivationDate;
	}
	public void setDeactivationDate(String deactivationDate) {
		this.deactivationDate = deactivationDate;
	}
	public List<String> getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(List<String> errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getBuyFlag() {
		return buyFlag;
	}
	public void setBuyFlag(String buyFlag) {
		this.buyFlag = buyFlag;
	}
	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}
	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}
	public Long getOfPass() {
		return ofPass;
	}
	public void setOfPass(Long ofPass) {
		this.ofPass = ofPass;
	}
	public BigDecimal getCurrentCashvalue() {
		return currentCashvalue;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}
	public void setCurrentCashvalue(BigDecimal currentCashvalue) {
		this.currentCashvalue = currentCashvalue;
	}
	
	public Long getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}


	public String getMemberName() {
		return memberName;
	}


	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}



	public Map<String, CustomerOrderDetDto> getItemMap() {
		return itemMap;
	}
	public void setItemMap(Map<String, CustomerOrderDetDto> itemMap) {
		this.itemMap = itemMap;
	}

	public Long getTotalHkgtaQuota() {
		return totalHkgtaQuota;
	}


	public void setTotalHkgtaQuota(Long totalHkgtaQuota) {
		this.totalHkgtaQuota = totalHkgtaQuota;
	}


	public Long getTotalMemberQuota() {
		return totalMemberQuota;
	}


	public void setTotalMemberQuota(Long totalMemberQuota) {
		this.totalMemberQuota = totalMemberQuota;
	}


	public DaypassPurchaseDto() {
	}
	
	
	public DaypassPurchaseDto(Long customerId, Long planNo, String activationDate, String deactivationDate) {
		super();
		this.customerId = customerId;
		this.planNo = planNo;
		this.activationDate = activationDate;
		this.deactivationDate = deactivationDate;
	}
		public DaypassPurchaseDto(Long customerId, String userId, String activationDate, String deactivationDate) {
		super();
		this.customerId = customerId;
		this.userId = userId;
		this.activationDate = activationDate;
		this.deactivationDate = deactivationDate;
	}
	
	public int getIsMember() {
			return isMember;
		}
	public void setIsMember(int isMember) {
		this.isMember = isMember;
	}
	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public List<OneDayDaypassPurchaseDto> getDays() {
		return days;
	}

	public void setDays(List<OneDayDaypassPurchaseDto> days) {
		this.days = days;
	}
	

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}


	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}


	public Long[] getDaypassNos() {
		return daypassNos;
	}


	public void setDaypassNos(Long[] daypassNos) {
		this.daypassNos = daypassNos;
	}


	public Long[] getBuysNos() {
		return buysNos;
	}


	public void setBuysNos(Long[] buysNos) {
		this.buysNos = buysNos;
	}
	
	public Long getPlanNo() {
		return planNo;
	}
	public void setPlanNo(Long planNo) {
		this.planNo = planNo;
	}
	
	public Long getCandidateId() {
		return candidateId;
	}
	public void setCandidateId(Long candidateId) {
		this.candidateId = candidateId;
	}
	public List<CandidateCustomerDto> getCandidateCustomerDtos() {
		return candidateCustomerDtos;
	}
	
	public void setCandidateCustomerDtos(List<CandidateCustomerDto> candidateCustomerDtos) {
		this.candidateCustomerDtos = candidateCustomerDtos;
	}
	public List<Map<String, Object>> getPaymentDetail() {
		return paymentDetail;
	}
	public void setPaymentDetail(List<Map<String, Object>> paymentDetail) {
		this.paymentDetail = paymentDetail;
	}
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	
}
