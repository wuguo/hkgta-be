package com.sinodynamic.hkgta.dto.fms;

import java.io.Serializable;



public class FacilityTypeDto implements Serializable {
	private String typeCode;
	private String description;
	private String permission;
	
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	
}
