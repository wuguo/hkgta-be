package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CoachRosterInfo implements Serializable {

	public static class CoachDayRateInfo implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private Integer weekDay;
		
		private Date date;
		
		private Integer dutyHours;
		
		private Boolean hasDuty;
		
		private List<TimeSliceRate> rateList = new ArrayList<TimeSliceRate>();

		public Integer getWeekDay() {
			return weekDay;
		}

		public void setWeekDay(Integer weekDay) {
			this.weekDay = weekDay;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(String strDate) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				this.date = sdf.parse(strDate);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
		}
		
		public Integer getDutyHours() {
			return dutyHours;
		}

		public void setDutyHours(Integer dutyHours) {
			this.dutyHours = dutyHours;
		}

		public Boolean getHasDuty() {
			return hasDuty;
		}

		public void setHasDuty(Boolean hasDuty) {
			this.hasDuty = hasDuty;
		}

		public List<TimeSliceRate> getRateList() {
			return rateList;
		}

		public void setRateList(List<TimeSliceRate> rateList) {
			this.rateList = rateList;
		}
	}
	
	public static class TimeSliceRate implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private Long beginTime;
		
		private String rateType;
		
		private String status;

		public Long getBeginTime() {
			return beginTime;
		}

		public void setBeginTime(Long beginTime) {
			this.beginTime = beginTime;
		}

		public String getRateType() {
			return rateType;
		}

		public void setRateType(String rateType) {
			this.rateType = rateType;
		}

		public String getStatus()
		{
			return status;
		}

		public void setStatus(String status)
		{
			this.status = status;
		}
		
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String coachId;
	
	private String coachName;
	
	private String highPriceItemNo;
	
	private String lowPriceItemNo;
	
	private BigDecimal lowRatePrice;
	
	private BigDecimal highRatePrice;
	
	private List<CoachDayRateInfo> dayRateList = new ArrayList<CoachDayRateInfo>();

	public String getCoachId() {
		return coachId;
	}

	public void setCoachId(String userId) {
		this.coachId = userId;
	}

	public String getCoachName() {
		return coachName;
	}

	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}

	public BigDecimal getLowRatePrice() {
		return lowRatePrice;
	}

	public void setLowRatePrice(BigDecimal lowRatePrice) {
		this.lowRatePrice = lowRatePrice;
	}

	public BigDecimal getHighRatePrice() {
		return highRatePrice;
	}

	public void setHighRatePrice(BigDecimal highRatePrice) {
		this.highRatePrice = highRatePrice;
	}

	public List<CoachDayRateInfo> getDayRateList() {
		return dayRateList;
	}

	public void setDayRateList(List<CoachDayRateInfo> dayRateList) {
		this.dayRateList = dayRateList;
	}

	public String getHighPriceItemNo()
	{
		return highPriceItemNo;
	}

	public void setHighPriceItemNo(String highPriceItemNo)
	{
		this.highPriceItemNo = highPriceItemNo;
	}

	public String getLowPriceItemNo()
	{
		return lowPriceItemNo;
	}

	public void setLowPriceItemNo(String lowPriceItemNo)
	{
		this.lowPriceItemNo = lowPriceItemNo;
	}
}
