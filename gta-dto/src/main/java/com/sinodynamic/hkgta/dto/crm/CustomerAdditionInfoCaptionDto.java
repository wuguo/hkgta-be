package com.sinodynamic.hkgta.dto.crm;

import java.io.Serializable;

public class CustomerAdditionInfoCaptionDto  implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long captionId;


	private String caption;
	


//	public String getCaptionId() {
//		return captionId;
//	}
//
//	public void setCaptionId(Object captionId) {
//		if (captionId instanceof Integer) {
//			this.captionId = String.valueOf((Integer) captionId);
//		} else {
//			this.captionId = (String) captionId;
//		}
//	}


	public String getCaption() {
		return caption;
	}

	public Long getCaptionId() {
		return captionId;
	}

	public void setCaptionId(Long captionId) {
		this.captionId = captionId;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	
}