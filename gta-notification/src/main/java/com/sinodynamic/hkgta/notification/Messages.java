package com.sinodynamic.hkgta.notification;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "messages")
public class Messages implements Serializable {
	private static final long serialVersionUID = 563925018375154887L;
	private List<Message> messages;

	@XmlElement(name = "message")	
	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	
	
	
}
