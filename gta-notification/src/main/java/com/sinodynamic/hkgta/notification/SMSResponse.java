package com.sinodynamic.hkgta.notification;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlRootElement(name = "sendresponse")
public class SMSResponse implements Serializable {
	private static final long serialVersionUID = -3937656843205232425L;

	private String correlationid;
	
	private String batchid;
	
	private String statuscode;
	
	private String reason;
	
	private Date submitdatetime;

	@XmlElement(name = "correlationid")		
	public String getCorrelationid() {
		return correlationid;
	}

	public void setCorrelationid(String correlationid) {
		this.correlationid = correlationid;
	}

	@XmlElement(name = "batchid")		
	public String getBatchid() {
		return batchid;
	}

	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}

	@XmlElement(name = "statuscode")		
	public String getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}

	@XmlElement(name = "reason")	
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@XmlElement(name = "submitdatetime")	
	public Date getSubmitdatetime() {
		return submitdatetime;
	}

	public void setSubmitdatetime(Date submitdatetime) {
		this.submitdatetime = submitdatetime;
	}
	
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this);
	}

}
