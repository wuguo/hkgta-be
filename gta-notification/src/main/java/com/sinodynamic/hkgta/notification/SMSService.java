package com.sinodynamic.hkgta.notification;

import java.util.Date;
import java.util.List;

public interface SMSService {
	
	/**
	 * 
	 * @param sendTime: The time to send SMS
	 * @param phonenumbers: The receiver phone number collection,the format is as:+86134xxxxxxxx, it must add the country code +xxx
	 * @param content
	 * @return
	 * @throws Exception 
	 */
	public SMSResponse sendSMS(List<String> phonenumbers,String content,Date sendDateTime)throws Exception;

}
