package com.sinodynamic.hkgta.notification;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "message")
@XmlType(propOrder = { "scheduledatetime", "content", "phonenumbers","senderid"})
public class Message implements Serializable {
	private static final long serialVersionUID = -3133240866388542018L;

	private Date scheduledatetime ;
	
	private String phonenumbers;
	
	private String content;
	
	private String senderid;

	@XmlElement(name = "scheduledatetime")	
	@XmlJavaTypeAdapter(JaxbDateSerializer.class)  
	public Date getScheduledatetime() {
		return scheduledatetime;
	}

	public void setScheduledatetime(Date scheduledatetime) {
		this.scheduledatetime = scheduledatetime;
	}

	@XmlElement(name = "phonenumbers")	
	public String getPhonenumbers() {
		return phonenumbers;
	}

	public void setPhonenumbers(String phonenumbers) {
		this.phonenumbers = phonenumbers;
	}

	@XmlElement(name = "content")	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@XmlElement(name = "senderid")	
	public String getSenderid() {
		return senderid;
	}

	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	
	

}
