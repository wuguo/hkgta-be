<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MonthlyEnrollment" language="groovy" pageWidth="1190" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1150" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="0" uuid="9c87d979-4600-47cd-aa3b-66203774742e">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#FFFFFF" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<subDataset name="dataset1" uuid="9cd064b0-f401-41e6-8d59-0c353f9839b1"/>
	<parameter name="selectedMonth" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="fileType" class="java.lang.String">
		<defaultValueExpression><![CDATA[new String("pdf")]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT DISTINCT result.* FROM (
SELECT
	DATE_FORMAT(trans.transaction_timestamp, '%Y/%m/%d %T') AS transactionDate,
	trans.transaction_no AS transactionId,
	trans.order_no AS orderNo,
	DATE_FORMAT(hd.order_date, '%Y/%m/%d') AS orderDate,
	m.academy_no AS academyId,
	CONCAT_WS(
		' ',
		cp.given_name,
		cp.surname
	) AS patronName,
  plan.plan_name as planName,
  hd.order_total_amount as orderTotalAmount,
  CONCAT_WS(' ',staffProfile.given_name,staffProfile.surname) as salesMan,
  'Enroll' as enrollType,
  CASE trans.payment_method_code
			when 'VISA' then 'VISA'
      when 'MASTER' then 'MASTER'
      when 'CASH' then 'Cash'
      when 'CV' then 'Cash Value'
      when 'BT' then 'Bank Transfer'
      when 'CHEQUE' then 'Cheque'
      when 'cheque' then 'Cheque'
      when 'VAC' then 'Virtual Account'
      when 'CUP' then 'Union Pay'
      when 'AMEX' then 'American Express'
      when 'OTH' then 'N/A'
  END as paymentMethodCode,
  CASE trans.payment_media
			WHEN 'OTH' THEN 'N/A'
      WHEN 'OP' THEN 'Online Payment'
      WHEN 'ECR' THEN 'ECR Terminal'
      WHEN 'FTF' THEN 'N/A'
      WHEN '' THEN 'N/A'
      ELSE 'N/A'
	END as paymentMedia,
  trans.payment_location_code as location,
  trans.status as status,
  (select CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) from staff_profile staffProfileCreateBy where staffProfileCreateBy.user_id = trans.create_by) as createBy,
  (select CONCAT_WS(' ',staffProfileUpdateBy.given_name,staffProfileUpdateBy.surname) from staff_profile staffProfileUpdateBy where staffProfileUpdateBy.user_id = trans.update_by) as updateBy,
  DATE_FORMAT(trans.update_date,'%Y/%m/%d') as updateDate,
  (select CONCAT_WS(' ',staffProfileAuditBy.given_name,staffProfileAuditBy.surname) from staff_profile staffProfileAuditBy where staffProfileAuditBy.user_id = trans.audit_by) as auditBy,
  DATE_FORMAT(trans.audit_date,'%Y/%m/%d') as auditDate,
  1 as qty,
  trans.paid_amount as transAmount
FROM
	customer_order_trans trans,
	customer_order_hd hd,
	member m,
	customer_profile cp,
  customer_order_det det,
  pos_service_item_price pricePos,
  service_plan plan,
  service_plan_pos planPos,
  customer_enrollment enrollment
LEFT JOIN staff_profile staffProfile on staffProfile.user_id = enrollment.sales_follow_by
WHERE
	hd.order_no = trans.order_no
AND enrollment.customer_id = m.customer_id
AND m.customer_id = hd.customer_id
AND cp.customer_id = m.customer_id
AND det.order_no = hd.order_no
AND pricePos.item_no = det.item_no
AND planPos.pos_item_no = pricePos.item_no
AND plan.plan_no = planPos.plan_no
AND pricePos.item_catagory = 'SRV'
AND trans.status = 'SUC'
AND date_format(trans.transaction_timestamp,'%Y-%m') = date_format($P{selectedMonth},'%Y-%m')
AND plan.pass_nature_code = 'LT'
UNION ALL
SELECT
	DATE_FORMAT(trans.transaction_timestamp, '%Y/%m/%d %T') AS transactionDate,
	trans.transaction_no AS transactionId,
	trans.order_no AS orderNo,
	DATE_FORMAT(hd.order_date, '%Y/%m/%d') AS orderDate,
	m.academy_no AS academyId,
	CONCAT_WS(
		' ',
		cp.given_name,
		cp.surname
	) AS patronName,
  plan.plan_name as planName,
  hd.order_total_amount as orderTotalAmount,
  CONCAT_WS(' ',staffProfile.given_name,staffProfile.surname) as salesMan,
  'Renew' as enrollType,
  CASE trans.payment_method_code
			when 'VISA' then 'VISA'
      when 'MASTER' then 'MASTER'
      when 'CASH' then 'Cash'
      when 'CASHVALUE' then 'Cash Value'
      when 'BT' then 'Bank Transfer'
      when 'CHEQUE' then 'Cheque'
      when 'cheque' then 'Cheque'
      when 'VAC' then 'Virtual Account'
      when 'CUP' then 'Union Pay'
      when 'AMEX' then 'American Express'
      when 'OTH' then 'N/A'
  END as paymentMethodCode,
  CASE trans.payment_media
			WHEN 'OTH' THEN 'N/A'
      WHEN 'OP' THEN 'Online Payment'
      WHEN 'ECR' THEN 'ECR Terminal'
      WHEN 'FTF' THEN 'N/A'
      WHEN '' THEN 'N/A'
      ELSE 'N/A'
	END as paymentMedia,
  trans.payment_location_code as location,
  trans.status as status,
  (select CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) from staff_profile staffProfileCreateBy where staffProfileCreateBy.user_id = trans.create_by) as createBy,
  (select CONCAT_WS(' ',staffProfileUpdateBy.given_name,staffProfileUpdateBy.surname) from staff_profile staffProfileUpdateBy where staffProfileUpdateBy.user_id = trans.update_by) as updateBy,
  DATE_FORMAT(trans.update_date,'%Y/%m/%d') as updateDate,
  (select CONCAT_WS(' ',staffProfileAuditBy.given_name,staffProfileAuditBy.surname) from staff_profile staffProfileAuditBy where staffProfileAuditBy.user_id = trans.audit_by) as auditBy,
  DATE_FORMAT(trans.audit_date,'%Y/%m/%d') as auditDate,
  1 as qty,
  trans.paid_amount as transAmount
FROM
	customer_order_trans trans,
	customer_order_hd hd,
	member m,
	customer_profile cp,
  customer_order_det det,
  pos_service_item_price pricePos,
  service_plan plan,
  service_plan_pos planPos,
  service_plan_offer_pos offerPos,
  customer_enrollment enrollment
LEFT JOIN staff_profile staffProfile on staffProfile.user_id = enrollment.sales_follow_by
WHERE
	hd.order_no = trans.order_no
AND enrollment.customer_id = m.customer_id
AND m.customer_id = hd.customer_id
AND cp.customer_id = m.customer_id
AND det.order_no = hd.order_no
AND pricePos.item_no = det.item_no
AND offerPos.pos_item_no = pricePos.item_no
AND offerPos.serv_pos_id = planPos.serv_pos_id
AND plan.plan_no = planPos.plan_no
AND pricePos.item_catagory = 'SRV'
AND trans.status = 'SUC'
AND date_format(trans.transaction_timestamp,'%Y-%m') = date_format($P{selectedMonth},'%Y-%m')
AND plan.pass_nature_code = 'LT'
) result]]>
	</queryString>
	<field name="transactionDate" class="java.lang.String"/>
	<field name="transactionId" class="java.lang.Long"/>
	<field name="orderNo" class="java.lang.Long"/>
	<field name="orderDate" class="java.lang.String"/>
	<field name="academyId" class="java.lang.String"/>
	<field name="patronName" class="java.lang.String"/>
	<field name="planName" class="java.lang.String"/>
	<field name="orderTotalAmount" class="java.math.BigDecimal"/>
	<field name="salesMan" class="java.lang.String"/>
	<field name="enrollType" class="java.lang.String"/>
	<field name="paymentMethodCode" class="java.lang.String"/>
	<field name="paymentMedia" class="java.lang.String"/>
	<field name="location" class="java.lang.String">
		<fieldDescription><![CDATA[specific location (if appliacble)]]></fieldDescription>
	</field>
	<field name="status" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="createBy" class="java.lang.String"/>
	<field name="updateBy" class="java.lang.String"/>
	<field name="updateDate" class="java.lang.String"/>
	<field name="auditBy" class="java.lang.String"/>
	<field name="auditDate" class="java.lang.String"/>
	<field name="qty" class="java.lang.Long"/>
	<field name="transAmount" class="java.math.BigDecimal"/>
	<variable name="totalQty" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{qty}]]></variableExpression>
	</variable>
	<variable name="totalAmount" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{transAmount}]]></variableExpression>
	</variable>
	<group name="Group1" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupHeader>
			<band height="64" splitType="Stretch">
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="0" y="21" width="69" height="21" uuid="3b17e228-abb3-417f-bb29-7496899a6458"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Transaction Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="69" y="21" width="40" height="21" uuid="8549b556-6fc2-4864-b577-af9278e2d256"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Transaction ID"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="109" y="21" width="42" height="21" uuid="3e91cadb-324e-47b8-89f2-7c2c51e32389"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Order #"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="151" y="21" width="71" height="21" uuid="829eabfe-8145-4a9b-acb1-42483f680af8"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Order Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="222" y="21" width="60" height="21" uuid="a0b1365c-26a1-462f-87dc-31fade94b432"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Patron Academy ID"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="282" y="21" width="48" height="21" uuid="3e63660b-174c-4846-8c3e-fd42f43b9eeb"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Patron Name"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="404" y="21" width="66" height="21" uuid="a9fe8941-c837-4a60-b916-d2e641646cf6"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Service Plan Total Amount"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="470" y="21" width="48" height="21" uuid="98712080-89a2-4aca-a0d7-68b9c499fe0e"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Sales Person"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="518" y="21" width="43" height="21" uuid="be921aab-c7da-4312-b68c-7385fc8cf07b"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Enroll Type"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="561" y="21" width="70" height="21" uuid="43dc3005-2087-4ba7-bf3d-c4d04efdc436"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Method"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="631" y="21" width="52" height="21" uuid="9024e2ff-dd91-4dd4-9051-a189b0429814"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Medium"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="683" y="21" width="47" height="21" uuid="539b79fd-6fd8-4c97-86e7-5dc9ad585f98"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Location"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="730" y="21" width="33" height="21" uuid="3b6e12ed-4867-4652-a5c7-4999e2eba5e5"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Status"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="763" y="21" width="39" height="21" uuid="05e2b70e-4df0-4fdf-be9e-80d39c24d631"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Create By"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="802" y="21" width="47" height="21" uuid="3c0f0c5d-bcba-4477-8dc5-67dec033a15d"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Update By"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="849" y="21" width="68" height="21" uuid="58ae89b9-e965-4a41-8f8e-c3ad4eeacf1a"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Update Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="917" y="21" width="40" height="21" uuid="b375aff9-3138-4423-b3e4-2b0f2abc4c26"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Audit By"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="957" y="21" width="70" height="21" uuid="24ae971d-764f-4bdb-a6cf-fa97cbc6b54c"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Audit Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1027" y="21" width="28" height="21" uuid="ae297fd5-297a-4d13-b044-20046377bff6"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Qty"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1055" y="21" width="95" height="21" uuid="d0646d64-928c-4773-a0e3-0312b2cc98ba"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Transaction Amount"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="51" width="1150" height="1" uuid="9aaeb444-d5c1-45f5-9eef-85e1a7903e11"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToTallestObject" x="330" y="21" width="74" height="21" uuid="dc404fb4-7d12-44ba-a450-f680656b1990"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Service Plan Name"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="50">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToBandHeight" x="917" y="13" width="110" height="20" uuid="baba2f59-a850-4d03-8acd-26a0b89d810f"/>
					<textElement>
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Total:"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToBandHeight" x="1027" y="13" width="28" height="20" uuid="af2ea3d7-702e-4ca5-afa1-9b29d75379a9"/>
					<textElement>
						<font fontName="Franklin Gothic Book"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totalQty}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1055" y="13" width="95" height="20" uuid="475a49c1-bff0-450f-baaf-fc3938f11627"/>
					<textElement>
						<font fontName="Franklin Gothic Book"/>
					</textElement>
					<textFieldExpression><![CDATA["HK\$ "+$V{totalAmount}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="1150" height="1" uuid="5e7c3879-0586-41e8-8d80-45d73fbc2e18"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="80" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} ==1]]></printWhenExpression>
			<textField>
				<reportElement style="Title" x="0" y="13" width="1150" height="39" uuid="1d5c64dd-1fb8-49ba-94da-3ba72ef5e4bf"/>
				<textElement textAlignment="Center">
					<font fontName="Constantia" size="26" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Monthly Enrollment Reports - "+new SimpleDateFormat("yyyy-MMM").format(new SimpleDateFormat("yyyy-MM-dd").parse($P{selectedMonth}))]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="38" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="1" width="69" height="20" uuid="d52e5f41-a046-414b-ad33-6a80e22aa8f5"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transactionDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="109" y="1" width="42" height="20" uuid="ea33ac4b-b1e8-4eb1-90dd-963cff5c95b8"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{orderNo}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="151" y="1" width="71" height="20" uuid="68ef31cd-cbd8-4663-869f-3c0456d72456"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{orderDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="222" y="1" width="60" height="20" uuid="6549045a-ca62-45eb-bd64-606e371aaf43"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA["csv".equals($P{fileType})?new String("=\""+$F{academyId}+"\""):$F{academyId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="282" y="1" width="48" height="20" uuid="b085c44c-be2c-4093-93a5-7b01a265ee30"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{patronName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="404" y="1" width="66" height="20" uuid="df48f95e-7b24-4ea2-9e8d-9db8576ff956"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{orderTotalAmount}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="470" y="1" width="48" height="20" uuid="7f2ac0ff-11b9-4cd2-9867-466962ae871f"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{salesMan}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="518" y="1" width="43" height="20" uuid="3e631c86-f769-4682-a601-e58762dac158"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{enrollType}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="561" y="1" width="70" height="20" uuid="f198a6a2-9730-4a87-bf7b-88930b6048dd"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMethodCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="631" y="1" width="52" height="20" uuid="29dc3309-e763-423f-8cad-49cdeab8aaef"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMedia}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="683" y="1" width="47" height="20" uuid="48e25635-227c-4bf9-87ef-6fca50d81ddc"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{location}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="730" y="1" width="33" height="20" uuid="57183604-9e14-4d7f-82d5-1628792a855b"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{status}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="763" y="1" width="39" height="20" uuid="8706f43e-9d80-41a3-8074-64866f8e268c"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{createBy}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="802" y="1" width="47" height="20" uuid="a86b399d-48a9-44e4-93e9-b03090f4be00"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{updateBy}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="849" y="1" width="68" height="20" uuid="eaa28a53-ccf3-4474-a252-be6731cb922e"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{updateDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="917" y="1" width="40" height="20" uuid="c0a9f1c2-58d7-42f7-8f19-a9418065a224"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{auditBy}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="957" y="1" width="70" height="20" uuid="0ba022fd-ff01-4788-b1c4-df62ab0d70d3"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{auditDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1027" y="1" width="28" height="20" uuid="7dcfaf68-b4dc-41cc-b648-2f388e02a952"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1055" y="1" width="95" height="20" uuid="835ba52b-bc98-4c6f-86d9-c60c9a7cc466"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transAmount}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToBandHeight" x="330" y="1" width="74" height="20" uuid="8e09a43e-04d0-4f1a-a401-8552b369ca3a"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{planName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="69" y="1" width="40" height="20" uuid="a0ba7b93-fe9d-4239-84f0-cffc750db1c3"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transactionId}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="48" splitType="Stretch">
			<textField pattern="yyyy/MM/dd HH:mm">
				<reportElement style="Column header" x="140" y="18" width="131" height="20" forecolor="#000000" uuid="78303431-3840-4db8-aad9-06da5873c404"/>
				<textElement>
					<font fontName="Franklin Gothic Book" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="18" width="124" height="20" uuid="8ea97769-0b3e-484d-85cb-4c83c3bea988"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<text><![CDATA[Report Generated  Time: ]]></text>
			</staticText>
			<textField>
				<reportElement style="Column header" x="1055" y="18" width="55" height="20" forecolor="#000000" uuid="c45649a6-0e55-4fd5-b05b-582c9b7dc006"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Franklin Gothic Book" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="1110" y="18" width="40" height="20" forecolor="#000000" uuid="aab7d77f-08e2-4740-bd63-76fa07ecaf13"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Franklin Gothic Book" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
