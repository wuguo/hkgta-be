<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DailyMonthlyFacilityUsage" language="groovy" pageWidth="1190" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1150" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="0" uuid="9c87d979-4600-47cd-aa3b-66203774742e">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#FFFFFF" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<subDataset name="dataset1" uuid="9cd064b0-f401-41e6-8d59-0c353f9839b1"/>
	<parameter name="roomAttendantName" class="java.lang.String"/>
	<parameter name="reportWeek" class="java.lang.String"/>
	<parameter name="userId" class="java.lang.String"/>
	<parameter name="fromReportDate" class="java.lang.String"/>
	<parameter name="toReportDate" class="java.lang.String"/>
	<parameter name="dateSql" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT
	c.on_date AS date,

IF (d.duties, d.duties, 'Off') AS duties
FROM
	(
		$P!{dateSql}
	) AS c
LEFT JOIN (
	SELECT
		b.on_date,
		(
			SELECT
				CONCAT(
					GROUP_CONCAT(
						DISTINCT CONCAT(
							a.begin_time,
							'-',
							a.end_time + 100
						)
						ORDER BY
							CAST(end_time AS SIGNED) ASC SEPARATOR ','
					),
					''
				)
			FROM
				staff_roster AS a
			WHERE
				a.on_date = b.on_date
			AND a.staff_user_id = b.staff_user_id
		) AS duties
	FROM
		staff_roster AS b
	WHERE
		1 = 1
	AND b.staff_user_id = $P{userId}
	AND b.on_date BETWEEN $P{fromReportDate}
	AND $P{toReportDate}
	GROUP BY
		on_date
) AS d ON c.on_date = d.on_date]]>
	</queryString>
	<field name="date" class="java.sql.Date"/>
	<field name="duties" class="java.lang.String"/>
	<group name="Group1" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupHeader>
			<band height="63" splitType="Stretch">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="54" width="1150" height="1" uuid="9aaeb444-d5c1-45f5-9eef-85e1a7903e11"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="256" y="34" width="287" height="20" uuid="94949875-f72e-4448-8b89-9928b0d45516"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="543" y="34" width="281" height="20" uuid="1ba2c190-ff6b-4dab-9d5b-88f88f1a0ae5"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Duties"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="50">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="1150" height="1" uuid="5e7c3879-0586-41e8-8d80-45d73fbc2e18"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="68" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} ==1]]></printWhenExpression>
			<textField>
				<reportElement style="Title" x="0" y="13" width="1150" height="39" uuid="1d5c64dd-1fb8-49ba-94da-3ba72ef5e4bf"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Constantia" size="26" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Weekly Roster of  <" + $P{roomAttendantName} + " > - <" + $P{reportWeek}  + ">"]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="38" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="256" y="0" width="287" height="20" uuid="4d1774a6-1793-4b6b-9fe6-19b590a82057"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="543" y="0" width="281" height="20" uuid="d990b89b-d00d-43ef-b94b-1056d853d05b"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{duties}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="48" splitType="Stretch">
			<textField pattern="yyyy/MM/dd HH:mm">
				<reportElement style="Column header" x="139" y="18" width="193" height="20" forecolor="#000000" uuid="78303431-3840-4db8-aad9-06da5873c404"/>
				<textElement>
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="18" width="139" height="20" uuid="500fbea3-b75f-47eb-bf5b-d084cd50d23c"/>
				<textElement markup="none">
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA["Report Generated Time: "]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Column header" x="1029" y="18" width="45" height="20" forecolor="#000000" uuid="e90d521c-6ed9-404f-aa9d-76d8cccb7a74"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="1074" y="18" width="76" height="20" forecolor="#000000" uuid="b59bba22-cc36-4d28-8bcc-eb4487caade7"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
