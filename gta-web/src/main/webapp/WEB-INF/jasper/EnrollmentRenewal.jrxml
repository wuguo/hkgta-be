<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="EnrollmentRenewal" language="groovy" pageWidth="1190" pageHeight="842" orientation="Landscape" columnWidth="1150" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="0" uuid="9c87d979-4600-47cd-aa3b-66203774742e">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#FFFFFF" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<subDataset name="dataset1" uuid="9cd064b0-f401-41e6-8d59-0c353f9839b1"/>
	<parameter name="SqlCondition" class="java.lang.String"/>
	<parameter name="fileType" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT
	DATE_FORMAT(trans.transaction_timestamp, '%Y/%m/%d %T') AS transactionDate,
	trans.transaction_no AS transactionId,
	hd.order_no AS orderNo,
	DATE_FORMAT(hd.order_date, '%Y/%m/%d') AS orderDate,
	mem.academy_no AS academyId,
	CONCAT(
		pro.given_name,
		' ',
		pro.surname
	) AS memberName,
	plan.plan_name AS servicePlan,
	CONCAT(
		sp.given_name,
		' ',
		sp.surname
	) AS salesman,
	(
		CASE SUBSTR(det.item_no, 1, 3)
		WHEN 'REN' THEN
			'Renew'
		WHEN 'SRV' THEN
			'Enroll'
		ELSE
			''
		END
	) AS enrollType,

  CASE trans.payment_method_code
   when 'VISA' then 'VISA'
      when 'MASTER' then 'MASTER'
      when 'CASH' then 'Cash'
      when 'CV' then 'Cash Value'
      when 'BT' then 'Bank Transfer'
      when 'CHEQUE' then 'Cheque'
      when 'cheque' then 'Cheque'
      when 'VAC' then 'Virtual Account'
      when 'UPAY' then 'Union Pay'
      when 'OTH' then 'N/A'
      else 'N/A'
  END AS paymentMethod,
	CASE trans.payment_media
   WHEN 'OTH' THEN 'N/A'
      WHEN 'OP' THEN 'Online Payment'
      WHEN 'ECR' THEN 'ECR Terminal'
      WHEN 'FTF' THEN 'N/A'
      WHEN '' THEN 'N/A'
      ELSE 'N/A'
  END AS paymentMedia,
	trans.payment_location_code AS location,
  CASE hd.order_status
			WHEN 'OPN' THEN 'Open'
      WHEN 'CMP' THEN 'Complete'
      WHEN 'CAN' THEN 'Cancelled'
      WHEN 'REJ' THEN 'Rejected'
  ELSE ''
  END AS orderStatus,

  CASE trans.status
			WHEN 'VOID' THEN 'Void'
      WHEN 'SUC'  THEN 'Success'
      WHEN 'FAIL' THEN 'Fail'
      WHEN 'PND'  THEN 'Pending'
      WHEN 'RFU'  THEN 'Refund'
      WHEN 'PR'   THEN 'Payment Refund'
  END AS transStatus,

  (SELECT CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) FROM staff_profile staffProfileCreateBy WHERE staffProfileCreateBy.user_id = trans.create_by
   UNION ALL
   SELECT CONCAT_WS(' ',memberProfile.given_name,memberProfile.surname) FROM customer_profile memberProfile, user_master userMasterMember, member memberCreateBy
   WHERE userMasterMember.user_type = 'CUSTOMER' AND memberCreateBy.user_id = userMasterMember.user_id AND memberProfile.customer_id = memberCreateBy.customer_id
   AND trans.create_by = userMasterMember.user_id
  ) AS createBy,

 	if(
   (SELECT CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) FROM staff_profile staffProfileCreateBy WHERE staffProfileCreateBy.user_id = trans.update_by
   UNION ALL
   SELECT CONCAT_WS(' ',memberProfile.given_name,memberProfile.surname) FROM customer_profile memberProfile, user_master userMasterMember, member memberCreateBy
   WHERE userMasterMember.user_type = 'CUSTOMER' AND memberCreateBy.user_id = userMasterMember.user_id AND memberProfile.customer_id = memberCreateBy.customer_id
   AND trans.update_by = userMasterMember.user_id
  ) is null and trans.update_by is not null,
   trans.update_by,
  (SELECT CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) FROM staff_profile staffProfileCreateBy WHERE staffProfileCreateBy.user_id = trans.update_by
   UNION ALL
   SELECT CONCAT_WS(' ',memberProfile.given_name,memberProfile.surname) FROM customer_profile memberProfile, user_master userMasterMember, member memberCreateBy
   WHERE userMasterMember.user_type = 'CUSTOMER' AND memberCreateBy.user_id = userMasterMember.user_id AND memberProfile.customer_id = memberCreateBy.customer_id
   AND trans.update_by = userMasterMember.user_id
  )
  ) AS updateBy,

	DATE_FORMAT(trans.update_date, '%Y/%m/%d') AS updateDate,
	(SELECT CONCAT_WS(' ',staffProfileAuditBy.given_name,staffProfileAuditBy.surname) FROM staff_profile staffProfileAuditBy WHERE staffProfileAuditBy.user_id = trans.audit_by) AS auditBy,
	DATE_FORMAT(trans.audit_date, '%Y/%m/%d') AS auditDate,
	1 AS qty,
	trans.paid_amount AS transAmount
FROM
	customer_order_trans trans,
	customer_order_hd hd,
	customer_order_det det,
	customer_profile pro,
	member mem,
	service_plan plan,
	service_plan_pos pos,
	staff_profile sp,
	customer_enrollment enroll,
	service_plan_offer_pos spo
WHERE
	trans.order_no = hd.order_no
AND hd.order_no = det.order_no
AND hd.customer_id = mem.customer_id
AND pro.customer_id = mem.customer_id
AND pos.plan_no = plan.plan_no
AND hd.order_no = det.order_no
AND sp.user_id = enroll.sales_follow_by
AND enroll.customer_id = hd.customer_id
AND spo.serv_pos_id = pos.serv_pos_id
AND (det.item_no = pos.pos_item_no OR det.item_no = spo.pos_item_no)
AND plan.pass_nature_code = 'LT'
AND (det.item_no LIKE 'SRV%' OR det.item_no LIKE 'RENEW%')
AND $P!{SqlCondition}]]>
	</queryString>
	<field name="transactionDate" class="java.lang.String"/>
	<field name="transactionId" class="java.lang.Long"/>
	<field name="orderNo" class="java.lang.Long"/>
	<field name="orderDate" class="java.lang.String"/>
	<field name="academyId" class="java.lang.String"/>
	<field name="memberName" class="java.lang.String"/>
	<field name="servicePlan" class="java.lang.String"/>
	<field name="salesman" class="java.lang.String"/>
	<field name="enrollType" class="java.lang.String"/>
	<field name="paymentMethod" class="java.lang.String"/>
	<field name="paymentMedia" class="java.lang.String"/>
	<field name="location" class="java.lang.String"/>
	<field name="orderStatus" class="java.lang.String"/>
	<field name="transStatus" class="java.lang.String"/>
	<field name="createBy" class="java.lang.String"/>
	<field name="updateBy" class="java.lang.String"/>
	<field name="updateDate" class="java.lang.String"/>
	<field name="auditBy" class="java.lang.String"/>
	<field name="auditDate" class="java.lang.String"/>
	<field name="qty" class="java.lang.Long"/>
	<field name="transAmount" class="java.math.BigDecimal"/>
	<variable name="totalQty" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{qty}]]></variableExpression>
	</variable>
	<variable name="totalAmount" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{transAmount}]]></variableExpression>
	</variable>
	<group name="Group1" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupHeader>
			<band height="63" splitType="Stretch">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="52" width="1150" height="1" uuid="9aaeb444-d5c1-45f5-9eef-85e1a7903e11"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="0" y="31" width="62" height="20" uuid="f67d6f4f-7d9e-4a4f-abb8-0df8d6b43ff8"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Transaction Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="62" y="31" width="62" height="20" uuid="66de1343-3b0b-4a44-8b7e-272962918b19"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Transaction ID"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="124" y="31" width="54" height="20" uuid="eda9b77c-06bb-4dab-a9bc-1e7d500717cb"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Order No."]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="178" y="31" width="64" height="20" uuid="436d6057-1537-401b-95e0-74a24fc51fe1"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Order Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="242" y="31" width="61" height="20" uuid="f54a6d50-11a8-41fa-b3d8-7dd4542ad004"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Academy ID"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="303" y="31" width="66" height="20" uuid="e5dc19d5-d993-44f2-89ff-f6b3b6cf6e8b"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Member Name"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="369" y="31" width="77" height="20" uuid="6f23f08d-a6a2-4be0-81bd-165c3325fe1e"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Service Plan"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="446" y="31" width="53" height="20" uuid="10db5fae-0e7f-4d34-974d-b91e241e34aa"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Salesman"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="499" y="31" width="49" height="20" uuid="f640f705-1eb0-4605-8b68-8ceb4d619fc0"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Enroll Type"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="548" y="31" width="55" height="20" uuid="3ca169f2-43fe-4d17-96e7-202eb4b573eb"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Method"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="603" y="31" width="47" height="20" uuid="c712eb3a-a331-4b59-b817-72f9007a3830"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Medium"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="650" y="31" width="50" height="20" uuid="d634054c-5478-4503-b9e1-1f5ad2ec29ae"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Location"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="750" y="31" width="43" height="20" uuid="63f092a3-bac2-4464-b8dc-ab5163f1acf7"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Order Status"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="700" y="31" width="50" height="20" uuid="e7b06c6c-ae14-4632-bfaa-661401eee83c"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Trans Status"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="793" y="31" width="53" height="20" uuid="bc5098c2-510d-48c6-b230-c51c346cd5a5"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Create By"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="846" y="31" width="53" height="20" uuid="efdc9e99-bf25-4298-b21b-c1cf4dc7bce8"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Update By"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="899" y="31" width="48" height="20" uuid="e8bdfe83-40a0-4a21-9964-738f1c1d0cd4"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Update Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="947" y="31" width="54" height="20" uuid="1b592428-929e-4a98-a4c7-20e194763b34"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Audit By"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1001" y="31" width="62" height="20" uuid="76262a6b-5e01-43bd-ba45-5361ef5af080"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Audit Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1063" y="31" width="22" height="20" uuid="3f8645dc-9465-4a77-8021-a000ac1c284d"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Qty"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1085" y="31" width="65" height="20" uuid="65755de0-6015-4164-9bc3-0cd2d065f6da"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Trans. Amount"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="50">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToBandHeight" x="947" y="0" width="116" height="20" uuid="baba2f59-a850-4d03-8acd-26a0b89d810f"/>
					<textElement>
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Total:"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="1150" height="1" uuid="5e7c3879-0586-41e8-8d80-45d73fbc2e18"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true">
					<reportElement x="1063" y="0" width="22" height="20" uuid="445053f7-01f4-4931-bfd5-d4f8ad34905c"/>
					<textFieldExpression><![CDATA[$V{totalQty}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement x="1085" y="0" width="65" height="20" uuid="2df22c6a-e2a7-4d07-9936-bdc0c27383c0"/>
					<textFieldExpression><![CDATA["HK\$ "+$V{totalAmount}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="66" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} ==1]]></printWhenExpression>
			<textField>
				<reportElement style="Title" x="0" y="13" width="1150" height="39" uuid="1d5c64dd-1fb8-49ba-94da-3ba72ef5e4bf"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Constantia" size="26" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Transaction Settlement"]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="38" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="0" width="62" height="20" uuid="310e086b-3724-4e57-8e51-9bc0d91b9482"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transactionDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="62" y="0" width="62" height="20" uuid="de08d943-c89c-4802-ba63-a419acb5c9d4"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transactionId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="124" y="0" width="54" height="20" uuid="a83576fd-c0ea-4504-9e84-43b059980ce1"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{orderNo}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="178" y="0" width="64" height="20" uuid="c40737da-9139-48b3-9ef2-c9c95d28013e"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{orderDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="242" y="0" width="61" height="20" uuid="20b30481-c05e-490d-beb3-34189edaefea"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{academyId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="303" y="0" width="66" height="20" uuid="55063ce6-bc08-4596-91a0-4b6416dbaf36"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{memberName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="369" y="0" width="77" height="20" uuid="17671970-5a7e-4cf0-a176-1a24f2f8f9e7"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicePlan}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="446" y="0" width="53" height="20" uuid="b6d27b78-1ce1-4f7b-835f-937fa49ead99"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{salesman}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="499" y="0" width="49" height="20" uuid="da943ee0-6725-49df-9806-75e6e180718f"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{enrollType}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="548" y="0" width="55" height="20" uuid="d71e3537-d440-4f01-b8dd-ad7dc3056075"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMethod}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="603" y="0" width="47" height="20" uuid="52aae3fa-31f6-486e-b10e-5709c562e72c"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMedia}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="650" y="0" width="50" height="20" uuid="94dd83a8-4651-42be-ac8b-0dcd911efa72"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{location}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="750" y="0" width="43" height="20" uuid="12d269f6-b0c5-4e1d-85c9-0ab5e3bf81c6"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{orderStatus}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="700" y="0" width="50" height="20" uuid="a607e4a7-790e-4ed5-afe3-0aa3a328cc39"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transStatus}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="793" y="0" width="53" height="20" uuid="604113d5-cd35-4e18-a7a6-b30ac6e9706a"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{createBy}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="846" y="0" width="53" height="20" uuid="57333066-cfb8-40b4-86a8-50e870cd7b6f"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{updateBy}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="899" y="0" width="48" height="20" uuid="9c68d988-71e1-4231-ab08-8600c0ff6f53"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{updateDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="947" y="0" width="54" height="20" uuid="f13975c1-d8d6-4e65-929d-93ab810fe9d0"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{auditBy}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1001" y="0" width="62" height="20" uuid="c4edcea3-0e29-4b42-9f7d-ad4052b040cf"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{auditDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1063" y="0" width="22" height="20" uuid="42029525-8bd0-413e-99ec-c2815c20ba0a"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1085" y="0" width="65" height="20" uuid="8e9f344e-38d5-4016-b4c1-fd5817560932"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transAmount}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="48" splitType="Stretch">
			<textField pattern="yyyy/MM/dd HH:mm">
				<reportElement style="Column header" x="178" y="18" width="191" height="20" forecolor="#000000" uuid="78303431-3840-4db8-aad9-06da5873c404"/>
				<textElement textAlignment="Right">
					<font fontName="Franklin Gothic Book" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="18" width="178" height="20" uuid="8ea97769-0b3e-484d-85cb-4c83c3bea988"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<text><![CDATA[Report Generated Date Time: ]]></text>
			</staticText>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="1085" y="18" width="65" height="20" forecolor="#000000" uuid="72a0a438-460e-46ea-9ae3-c9694e088dc0"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Franklin Gothic Book" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Column header" x="1001" y="18" width="84" height="20" forecolor="#000000" uuid="c45649a6-0e55-4fd5-b05b-582c9b7dc006"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Franklin Gothic Book" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
