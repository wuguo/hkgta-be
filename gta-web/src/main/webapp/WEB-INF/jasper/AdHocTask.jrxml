<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DailyMonthlyFacilityUsage" language="groovy" pageWidth="1190" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1150" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="0" uuid="9c87d979-4600-47cd-aa3b-66203774742e">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#FFFFFF" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<subDataset name="dataset1" uuid="9cd064b0-f401-41e6-8d59-0c353f9839b1"/>
	<parameter name="fromReportDate" class="java.lang.String"/>
	<parameter name="toReportDate" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT
	rh.task_id AS taskId,
	DATE_FORMAT(
		rh.schedule_datetime,
		'%Y-%M-%D %H:%i:%S'
	) AS assignedDate,
	r.room_no AS roomNumber,
	rh.task_description AS taskDetails,
	rh.monitor_detected AS sbeaconDesc,
	DATE_FORMAT(
		rh.start_timestamp,
		'%Y-%M-%D %H:%i:%S'
	) AS respondTime,
	CASE rh. STATUS
WHEN 'OPN' THEN

IF (log.log_id, 'Arrived', 'New')
WHEN 'RTI' THEN
	'Completed'
WHEN 'IRJ' THEN
	'Inspection Rejected'
WHEN 'CMP' THEN
	'Inspection Completed'
WHEN 'CAN' THEN
	'Cancelled'
WHEN 'DND' THEN
	'Do Not Disturb'
END AS taskStatus,
 (
	SELECT
		CONCAT(
			sp.given_name,
			' ',
			sp.surname
		)
	FROM
		room_housekeep_status_log AS rlog
	LEFT JOIN staff_profile AS sp ON rlog.status_chg_by = sp.user_id
	WHERE
		rlog.task_id = rh.task_id
	AND rlog.status_from = 'OPN'
	AND rlog.status_to = 'ATN'
) AS handlingStaff
FROM
	room_housekeep_task AS rh
LEFT JOIN (
	SELECT
		*
	FROM
		room_housekeep_status_log t
	WHERE
		NOT EXISTS (
			SELECT
				1
			FROM
				room_housekeep_status_log t1
			WHERE
				t.task_id = t1.task_id
			AND t1.status_date > t.status_date
		)
) log ON rh.task_id = log.task_id
AND log.status_from = 'OPN'
AND log.status_to = 'ATN'
LEFT JOIN room AS r ON rh.room_id = r.room_id
WHERE
	1 = 1
AND rh.job_type = 'ADH'
AND rh. STATUS <> 'PND'
AND DATE_FORMAT(
	rh.schedule_datetime,
	'%Y-%m-%d'
) BETWEEN $P{fromReportDate}
AND $P{toReportDate}
ORDER BY assignedDate desc]]>
	</queryString>
	<field name="taskId" class="java.lang.Long"/>
	<field name="assignedDate" class="java.lang.String"/>
	<field name="roomNumber" class="java.lang.String"/>
	<field name="taskDetails" class="java.lang.String"/>
	<field name="sbeaconDesc" class="java.lang.String"/>
	<field name="respondTime" class="java.lang.String"/>
	<field name="taskStatus" class="java.lang.String"/>
	<field name="handlingStaff" class="java.lang.String"/>
	<variable name="totalQty" class="java.lang.Long" calculation="Count">
		<variableExpression><![CDATA[$F{taskId}]]></variableExpression>
	</variable>
	<group name="Group1" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupHeader>
			<band height="63" splitType="Stretch">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="54" width="1150" height="1" uuid="9aaeb444-d5c1-45f5-9eef-85e1a7903e11"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="0" y="31" width="164" height="20" uuid="94949875-f72e-4448-8b89-9928b0d45516"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Task ID"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="163" y="31" width="164" height="20" uuid="1ba2c190-ff6b-4dab-9d5b-88f88f1a0ae5"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Assigned Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="326" y="31" width="164" height="20" uuid="48e0282d-5491-4f14-9ebe-e52839976d91"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Room Number"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="489" y="31" width="164" height="20" uuid="a9933c21-28a2-4ce0-b6fd-74465812ff4c"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Task Details"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="815" y="31" width="164" height="20" uuid="ccb77577-0498-4e0a-8ae2-f113aee8b6f1"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Respond Time"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="978" y="31" width="164" height="20" uuid="d498852a-2ce6-47bc-9daf-ecc9ca4fe486"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Handling Staff"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="652" y="31" width="164" height="20" uuid="b4bd18f7-44bc-4883-8bfd-502000d8a8bd"/>
					<textElement textAlignment="Center" markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Task  Status"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="50">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="1150" height="1" uuid="5e7c3879-0586-41e8-8d80-45d73fbc2e18"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="68" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} ==1]]></printWhenExpression>
			<textField>
				<reportElement style="Title" x="0" y="13" width="1150" height="39" uuid="1d5c64dd-1fb8-49ba-94da-3ba72ef5e4bf"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Constantia" size="26" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Ad Hoc Task - <" + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse($P{fromReportDate})) + " - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse($P{toReportDate}))  + ">"]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="38" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement x="0" y="0" width="164" height="20" uuid="52cf000e-3fc6-4eb9-891f-5e3ab808dab0"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{taskId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="0" y="0" width="164" height="20" uuid="4d1774a6-1793-4b6b-9fe6-19b590a82057"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{taskId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="163" y="0" width="164" height="20" uuid="d990b89b-d00d-43ef-b94b-1056d853d05b"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{assignedDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="326" y="0" width="164" height="20" uuid="16b65e8f-33d2-43e4-9d26-54a846a7e391"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{roomNumber}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="489" y="0" width="164" height="20" uuid="380c058e-5e62-4841-9a10-39d24b9fdc75"/>
				<textElement textAlignment="Center">
					<font pdfFontName="STSong-Light" pdfEncoding="UniGB-UCS2-H" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{taskDetails}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="815" y="0" width="164" height="20" uuid="fb9927e8-5520-465a-a21d-952806715ce4"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{respondTime}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="978" y="0" width="164" height="20" uuid="d826bcdb-f0ba-4a59-87c2-bad1d9b03d3a"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{handlingStaff}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="652" y="0" width="164" height="20" uuid="38f82aa7-8f62-4c25-b178-00e5271ead27"/>
				<textFieldExpression><![CDATA[$F{taskStatus}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="48" splitType="Stretch">
			<textField pattern="yyyy/MM/dd HH:mm">
				<reportElement style="Column header" x="163" y="18" width="164" height="20" forecolor="#000000" uuid="78303431-3840-4db8-aad9-06da5873c404"/>
				<textElement>
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="18" width="164" height="20" uuid="500fbea3-b75f-47eb-bf5b-d084cd50d23c"/>
				<textElement markup="none">
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA["Report Generated Time: "]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Column header" x="815" y="18" width="164" height="20" forecolor="#000000" uuid="e75cbcba-27a0-4d57-bed4-b370cda3d803"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="978" y="18" width="164" height="20" forecolor="#000000" uuid="2521328b-9191-4541-9636-6ce3f1505965"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
