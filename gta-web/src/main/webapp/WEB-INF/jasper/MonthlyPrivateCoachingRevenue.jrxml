<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MonthlyPrivateCoachingRevenue" language="groovy" pageWidth="1190" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1150" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="0" uuid="9c87d979-4600-47cd-aa3b-66203774742e">
	<property name="ireport.zoom" value="1.464100000000001"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#FFFFFF" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<subDataset name="dataset1" uuid="9cd064b0-f401-41e6-8d59-0c353f9839b1"/>
	<parameter name="resvDate" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="facilityType" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="TimePeriodType" class="java.lang.String"/>
	<parameter name="TimePeriodTypeCondition" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[new String("DAILY".equals($P{TimePeriodType})?"Daily":"Monthly")]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFormatCondition" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[new String("DAILY".equals($P{TimePeriodType})?"yyyy-MMM-dd":"yyyy-MMM")]]></defaultValueExpression>
	</parameter>
	<parameter name="FacilityTypeConvert" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[new String("GOLF".equals($P{facilityType})||"TENNIS".equals($P{facilityType})?("TENNIS".equals($P{facilityType})?"Tennis":"Golf"):"All")]]></defaultValueExpression>
	</parameter>
	<parameter name="SQLDateFormatCondition" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["DAILY".equals($P{TimePeriodType})?" '%Y-%m-%d' ":" '%Y-%m' "]]></defaultValueExpression>
	</parameter>
	<parameter name="facilityCondition" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["GOLF".equals($P{facilityType})||"TENNIS".equals($P{facilityType})?" b.resv_facility_type = '"+$P{facilityType}+"' ":" 1=1 "]]></defaultValueExpression>
	</parameter>
	<parameter name="fileType" class="java.lang.String">
		<defaultValueExpression><![CDATA[new String("pdf")]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
    DATE_FORMAT(trans.transaction_timestamp, '%Y/%m/%d') AS transDate,
    trans.transaction_no AS transId,
    DATE_FORMAT(b.begin_datetime_book, '%Y/%m/%d') AS resvDate,
    b.resv_id AS resvId,
    m.academy_no AS academyId,
    CONCAT(cp.salutation,
            ' ',
            cp.given_name,
            ' ',
            cp.surname) AS patronName,
    b.exp_coach_user_id AS coachId,
    CONCAT(sp.given_name, ' ', sp.surname) AS coachName,
    CASE b.resv_facility_type
        WHEN 'GOLF' THEN 'Golf'
        WHEN 'TENNIS' THEN 'Tennis'
        ELSE b.resv_facility_type
    END AS facilityType,
    CASE trans.payment_method_code
        WHEN 'CV' THEN 'Cash Value'
        WHEN 'CASH' THEN 'Cash'
        WHEN 'BT' THEN 'Bank Transfer'
        WHEN 'CUP' THEN 'Union Pay'
        WHEN 'AMEX' THEN 'American Express'
        ELSE trans.payment_method_code
    END AS paymentMethod,
    CASE trans.payment_media
        WHEN 'OP' THEN 'Online Payment'
        WHEN 'ECR' THEN 'ECR Terminal'
        WHEN 'OTH' THEN 'Other'
        WHEN NULL THEN 'N/A'
        WHEN '' THEN 'N/A'
        ELSE trans.payment_media
    END AS paymentMedia,
    CASE trans.payment_location_code
        WHEN 'FD1' THEN 'Front Desk 1'
        WHEN 'FD2' THEN 'Front Desk 2'
        WHEN 'WP' THEN 'Web Portal'
        ELSE trans.payment_location_code
    END AS location,
    CASE trans.status
        WHEN 'SUC' THEN 'Complete'
        WHEN 'RFU' THEN 'Refund'
        WHEN 'REJ' THEN 'Refund'
        ELSE trans.status
    END AS status,
    b.facility_type_qty AS qty,
    trans.paid_amount AS transAmount
FROM
    member_facility_type_booking b
        LEFT JOIN
    member_facility_book_addition_attr a ON b.resv_id = a.resv_id
        LEFT JOIN
    member m ON b.customer_id = m.customer_id
        LEFT JOIN
    customer_profile cp ON b.customer_id = cp.customer_id
        LEFT JOIN
    staff_profile sp ON b.exp_coach_user_id = sp.user_id
        LEFT JOIN
    customer_order_hd hd ON hd.order_no = b.order_no
        LEFT JOIN
    customer_order_trans trans ON hd.order_no = trans.order_no
WHERE
    b.exp_coach_user_id IS NOT NULL
        AND b.status IN ('RSV' , 'ATN', 'NAT', 'CAN')
        AND trans.status IN ('SUC' , 'REJ','RFU')
	AND date_format(trans.transaction_timestamp,$P!{SQLDateFormatCondition})
	=date_format($P{resvDate},$P!{SQLDateFormatCondition})
	AND $P!{facilityCondition}]]>
	</queryString>
	<field name="resvDate" class="java.lang.String"/>
	<field name="resvId" class="java.lang.Long"/>
	<field name="facilityType" class="java.lang.String"/>
	<field name="academyId" class="java.lang.String"/>
	<field name="patronName" class="java.lang.String"/>
	<field name="paymentMedia" class="java.lang.String"/>
	<field name="coachId" class="java.lang.String"/>
	<field name="coachName" class="java.lang.String"/>
	<field name="qty" class="java.lang.Integer"/>
	<field name="paymentMethod" class="java.lang.String"/>
	<field name="transAmount" class="java.math.BigDecimal"/>
	<field name="status" class="java.lang.String">
		<fieldDescription><![CDATA[RSV-resevered; ATN-attended;  NAT-not attended; CAN-cancel booking; PND-pending]]></fieldDescription>
	</field>
	<field name="transDate" class="java.lang.String"/>
	<field name="transId" class="java.lang.String"/>
	<field name="location" class="java.lang.String"/>
	<variable name="totalQty" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{qty}]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<variable name="totalTransAmount" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{transAmount}]]></variableExpression>
	</variable>
	<group name="Group1" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupHeader>
			<band height="52" splitType="Stretch">
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="0" y="21" width="121" height="21" uuid="3b17e228-abb3-417f-bb29-7496899a6458"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Transaction Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1013" y="21" width="40" height="21" uuid="3b6e12ed-4867-4652-a5c7-4999e2eba5e5"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Qty"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="-2" y="51" width="1150" height="1" uuid="9aaeb444-d5c1-45f5-9eef-85e1a7903e11"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="120" y="21" width="68" height="21" uuid="8549b556-6fc2-4864-b577-af9278e2d256"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Transaction Id"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="187" y="21" width="65" height="21" uuid="3e91cadb-324e-47b8-89f2-7c2c51e32389"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Reservation Id"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="636" y="21" width="76" height="21" uuid="98712080-89a2-4aca-a0d7-68b9c499fe0e"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Facility Type"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="711" y="21" width="92" height="21" uuid="be921aab-c7da-4312-b68c-7385fc8cf07b"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Method"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="252" y="21" width="78" height="21" uuid="829eabfe-8145-4a9b-acb1-42483f680af8"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Resservation Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="393" y="21" width="90" height="21" uuid="3e63660b-174c-4846-8c3e-fd42f43b9eeb"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Patron Name"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="958" y="21" width="56" height="21" uuid="539b79fd-6fd8-4c97-86e7-5dc9ad585f98"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Status"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="483" y="21" width="60" height="21" uuid="a9fe8941-c837-4a60-b916-d2e641646cf6"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Coach No"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="330" y="21" width="64" height="21" uuid="a0b1365c-26a1-462f-87dc-31fade94b432"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Patron Academy Id"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="802" y="21" width="90" height="21" uuid="9024e2ff-dd91-4dd4-9051-a189b0429814"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Media"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="543" y="21" width="94" height="21" uuid="ee23c244-a4b0-4e54-828b-7064ee61a0e8"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Coach Name"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="892" y="21" width="66" height="21" uuid="a36af25a-3341-4386-b0db-c359c1f306e7"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Location"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1053" y="21" width="97" height="21" uuid="126672fd-9984-497d-abea-7d9c611fdf8e"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Trans Amount"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="43">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="-2" y="0" width="1150" height="1" uuid="5e7c3879-0586-41e8-8d80-45d73fbc2e18"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField>
					<reportElement x="1053" y="1" width="97" height="20" uuid="5d873ca1-534a-4a2e-ae88-69aca9062e57"/>
					<textElement>
						<font fontName="Franklin Gothic Book"/>
					</textElement>
					<textFieldExpression><![CDATA["HK\$"+((null ==$V{totalTransAmount})?0: $V{totalTransAmount})]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement stretchType="RelativeToBandHeight" x="892" y="1" width="122" height="20" uuid="ebb15ea4-c607-4df1-9091-3ec750c53ea6"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Total"]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="1013" y="1" width="40" height="20" uuid="a0596ba6-bca2-41a3-93bc-cd85aa1086f1"/>
					<textElement>
						<font fontName="Franklin Gothic Book"/>
					</textElement>
					<textFieldExpression><![CDATA[(null == $V{totalQty})?0:$V{totalQty}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="80" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} ==1]]></printWhenExpression>
			<textField>
				<reportElement style="Title" x="0" y="13" width="1150" height="39" uuid="1d5c64dd-1fb8-49ba-94da-3ba72ef5e4bf"/>
				<textElement textAlignment="Center">
					<font fontName="Constantia" size="26" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Monthly Private Coaching Revenue Report("+$P{facilityType}+")  - " + $P{resvDate}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="38" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement x="0" y="0" width="121" height="20" uuid="9cd9a286-0532-424a-bf0d-c162775e0242"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="120" y="0" width="68" height="20" uuid="dea4fb22-aa25-40b3-adb6-fcc5d60ca088"/>
				<textElement markup="none">
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="187" y="0" width="65" height="20" uuid="705a6040-ee54-423c-b5ca-07fba05362e4"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[("GOLF".equalsIgnoreCase($F{facilityType})?"GPC":"TPC")+"-"+$F{resvId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement x="252" y="0" width="78" height="20" uuid="37cd77bd-74e9-416b-9bca-947b9f67493d"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{resvDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="330" y="0" width="64" height="20" uuid="ca6b3159-9eff-4245-9ca8-c4ee6333dda3"/>
				<textElement textAlignment="Left">
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{academyId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="483" y="0" width="60" height="20" uuid="d36bde6e-fff4-493f-a4b8-6f0bda52e062"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{coachId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="543" y="0" width="94" height="20" uuid="1d64292f-a196-41d5-8532-e9a71f5488d0"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{coachName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="1013" y="0" width="40" height="20" uuid="31474cda-97c7-43ff-b38b-cfb866338e3d"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="802" y="0" width="90" height="20" uuid="a29087a7-71cc-43de-b9fa-48d51a4182be"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMedia}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="892" y="0" width="66" height="20" uuid="efb0f386-36a7-4389-ba16-e65f5c7fc68f"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{location}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="958" y="0" width="56" height="20" uuid="2d51d781-74ba-4f46-a67a-cc5eeebd295e"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{status}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="711" y="0" width="92" height="20" uuid="064f0d51-6e68-42dc-803c-bd355c8fa750"/>
				<textElement textAlignment="Left">
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMethod}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00" isBlankWhenNull="true">
				<reportElement x="1053" y="0" width="97" height="20" uuid="d1575d76-36e8-419d-877e-5b92e9c99ada"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transAmount}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="636" y="0" width="76" height="20" uuid="3118190f-d5c4-4d45-a8c1-22e50a46136f"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{facilityType}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="393" y="0" width="90" height="20" uuid="9fd68ddf-bc65-4aab-9ac8-6fc5cc2952cd"/>
				<textElement textAlignment="Left">
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{patronName}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="48" splitType="Stretch">
			<textField pattern="yyyy/MM/dd HH:mm">
				<reportElement style="Column header" x="121" y="18" width="143" height="20" forecolor="#000000" uuid="78303431-3840-4db8-aad9-06da5873c404"/>
				<textElement>
					<font fontName="Franklin Gothic Book" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="18" width="120" height="20" uuid="8ea97769-0b3e-484d-85cb-4c83c3bea988"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<text><![CDATA[Report Generated Time: ]]></text>
			</staticText>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
