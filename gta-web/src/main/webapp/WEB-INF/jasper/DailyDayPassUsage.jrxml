<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MonthlyEnrollment" language="groovy" pageWidth="1190" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1150" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="0" uuid="9c87d979-4600-47cd-aa3b-66203774742e">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#FFFFFF" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<subDataset name="dataset1" uuid="9cd064b0-f401-41e6-8d59-0c353f9839b1"/>
	<parameter name="orderDate" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="fileType" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT DISTINCT
    plan.plan_no planNo,
    t.order_no orderNo,
    t.order_date purchaseDate,
    plan.plan_name daypassName,
    daypassNo.activationDate activationDate,
    daypassNo.deactivationDate deactivationDate,
    CASE
        WHEN t.staff_user_id IS NULL THEN 'Member'
        ELSE 'Staff'
    END purchaseType,
    (CASE
        WHEN
            t.staff_user_id IS NULL
        THEN
            (SELECT
                    m.academy_no
                FROM
                    member m
                WHERE
                    m.customer_id = t.customer_id)
        ELSE NULL
    END) academyId,
    (CASE
        WHEN
            t.staff_user_id IS NULL
        THEN
            (SELECT
                    CONCAT(c.salutation,
                                ' ',
                                c.given_name,
                                ' ',
                                c.surname)
                FROM
                    customer_profile c
                WHERE
                    c.customer_id = t.customer_id)
        ELSE NULL
    END) patronName,
    CASE
        WHEN
            t.staff_user_id IS NOT NULL
        THEN
            (SELECT
                    CONCAT(c.given_name, ' ', c.surname)
                FROM
                    staff_profile c
                WHERE
                    c.user_id = t.staff_user_id)
        ELSE NULL
    END staffName,
    CASE tran.payment_method_code
        WHEN 'VISA' THEN 'VISA'
        WHEN 'MASTER' THEN 'MASTER'
        WHEN 'CASH' THEN 'Cash'
        WHEN 'CASHVALUE' THEN 'Cash Value'
        WHEN 'BT' THEN 'Bank Transfer'
        WHEN 'CHEQUE' THEN 'Cheque'
        WHEN 'cheque' THEN 'Cheque'
        WHEN 'VAC' THEN 'Virtual Account'
        WHEN 'CUP' THEN 'Union Pay'
        WHEN 'AMEX' THEN 'American Express'
        WHEN 'OTH' THEN 'N/A'
    END AS paymentMethodCode,
    CASE tran.payment_media
        WHEN 'OTH' THEN 'N/A'
        WHEN 'OP' THEN 'Online Payment'
        WHEN 'ECR' THEN 'ECR Terminal'
        WHEN 'FTF' THEN 'N/A'
        WHEN '' THEN 'N/A'
        ELSE 'N/A'
    END AS paymentMedia,
    CASE tran.payment_location_code
        WHEN 'FD1' THEN 'Front desk'
        WHEN 'BO' THEN 'Back office'
        WHEN 'FD2' THEN 'Front desk'
        WHEN 'PS' THEN 'Proshop'
        WHEN 'MOBILE' THEN 'APP'
        WHEN '' THEN 'N/A'
        ELSE 'N/A'
    END AS location,
    (SELECT CONCAT(profile.given_name, ' ', profile.surname) FROM staff_profile profile WHERE profile.user_id = t.create_by) createBy,
    t.create_date createDate,
    (daypassPrice.totalPrice / 1) price,
    CASE t.order_status
        WHEN 'OPN' THEN 'Open'
        WHEN 'CAN' THEN 'Cancelled'
        WHEN 'CMP' THEN 'Completed'
        WHEN 'REJ' THEN 'Rejected'
        ELSE 'N/A'
    END AS orderStatus,
    daypassNo.qty qty
FROM
    customer_order_hd t
        INNER JOIN
    customer_order_trans tran ON t.order_no = tran.order_no  AND tran.status<>'FAIL'
        INNER JOIN
    (SELECT
        CONCAT(temp.planNo, ' ', temp.orderNo) planNoOrderNo,
            SUM(temp.itemprice) totalPrice,
            temp.planNo planNo,
            temp.orderNo orderNo
    FROM
        ((SELECT
        plan.plan_no planNo,
            hd.order_no orderNo,
            det.item_no,
            det.order_qty num,
            det.item_total_amout itemprice
    FROM
        customer_order_det det
    INNER JOIN customer_order_hd hd ON det.order_no = hd.order_no
    INNER JOIN service_plan_pos pos ON pos.pos_item_no = det.item_no
    INNER JOIN service_plan plan ON plan.plan_no = pos.plan_no) UNION (SELECT
        plan.plan_no planNo,
            hd.order_no orderNo,
            det.item_no,
            det.order_qty num,
            det.item_total_amout itemprice
    FROM
        customer_order_det det
    INNER JOIN customer_order_hd hd ON det.order_no = hd.order_no
    INNER JOIN service_plan_rate_pos rate ON rate.pos_item_no = det.item_no
    INNER JOIN service_plan_pos pos ON rate.serv_pos_id = pos.serv_pos_id
    INNER JOIN service_plan plan ON plan.plan_no = pos.plan_no) ORDER BY orderNo DESC) temp
    GROUP BY planNoOrderNo) daypassPrice ON daypassPrice.orderNo = t.order_no
        INNER JOIN
    (SELECT
        COUNT(temp.planNoOrderNo) qty,
            temp.planNoOrderNo planNoOrderNo,
            temp.planNo planNo,
            temp.orderNo orderNo,
            temp.deactivationDate deactivationDate,
            temp.activationDate activationDate
    FROM
        (SELECT
        copc.service_plan_no planNo,
            t.order_no orderNo,
            CONCAT(copc.service_plan_no, ' ', t.order_no) planNoOrderNo,
            copc.effective_from activationDate,
            copc.effective_to deactivationDate
    FROM
        customer_order_hd t
    INNER JOIN customer_order_permit_card copc ON t.order_no = copc.customer_order_no) temp
    GROUP BY temp.planNoOrderNo) daypassNo ON daypassPrice.planNoOrderNo = daypassNo.planNoOrderNo
        INNER JOIN
    service_plan plan ON plan.plan_no = daypassNo.planNo
WHERE
    t.order_date = $P{orderDate}]]>
	</queryString>
	<field name="planNo" class="java.lang.Integer"/>
	<field name="orderNo" class="java.lang.Long"/>
	<field name="purchaseDate" class="java.sql.Date"/>
	<field name="daypassName" class="java.lang.String"/>
	<field name="activationDate" class="java.sql.Date"/>
	<field name="deactivationDate" class="java.sql.Date"/>
	<field name="purchaseType" class="java.lang.String"/>
	<field name="academyId" class="java.lang.String"/>
	<field name="patronName" class="java.lang.String"/>
	<field name="staffName" class="java.lang.String"/>
	<field name="paymentMethodCode" class="java.lang.String"/>
	<field name="paymentMedia" class="java.lang.String"/>
	<field name="location" class="java.lang.String">
		<fieldDescription><![CDATA[specific location (if appliacble)]]></fieldDescription>
	</field>
	<field name="createBy" class="java.lang.String"/>
	<field name="createDate" class="java.sql.Timestamp"/>
	<field name="price" class="java.math.BigDecimal"/>
	<field name="orderStatus" class="java.lang.String"/>
	<field name="qty" class="java.lang.Long"/>
	<variable name="totalDayPass" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{qty}]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<variable name="totalDayPassAmount" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{price}]]></variableExpression>
	</variable>
	<group name="Group1" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupHeader>
			<band height="64" splitType="Stretch">
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="0" y="21" width="60" height="21" uuid="3b17e228-abb3-417f-bb29-7496899a6458"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Purchase Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="925" y="21" width="60" height="21" uuid="3b6e12ed-4867-4652-a5c7-4999e2eba5e5"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Create Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1025" y="21" width="59" height="21" uuid="05e2b70e-4df0-4fdf-be9e-80d39c24d631"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Price (HK\$)"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1084" y="21" width="66" height="21" uuid="3c0f0c5d-bcba-4477-8dc5-67dec033a15d"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Order Status"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="51" width="1150" height="1" uuid="9aaeb444-d5c1-45f5-9eef-85e1a7903e11"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToTallestObject" x="390" y="21" width="60" height="21" uuid="dc404fb4-7d12-44ba-a450-f680656b1990"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Academy ID of Patron"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="70" y="21" width="100" height="21" uuid="8549b556-6fc2-4864-b577-af9278e2d256"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Day Pass Type Name"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="180" y="21" width="60" height="21" uuid="3e91cadb-324e-47b8-89f2-7c2c51e32389"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Activation Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="571" y="21" width="60" height="21" uuid="98712080-89a2-4aca-a0d7-68b9c499fe0e"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Staff Name"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="644" y="21" width="60" height="21" uuid="be921aab-c7da-4312-b68c-7385fc8cf07b"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Method"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="250" y="21" width="60" height="21" uuid="829eabfe-8145-4a9b-acb1-42483f680af8"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Deactivation Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="330" y="21" width="60" height="21" uuid="3e63660b-174c-4846-8c3e-fd42f43b9eeb"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Purchaser Type"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="854" y="21" width="60" height="21" uuid="539b79fd-6fd8-4c97-86e7-5dc9ad585f98"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Create By"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="463" y="21" width="97" height="21" uuid="a9fe8941-c837-4a60-b916-d2e641646cf6"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Patron Name"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="994" y="21" width="20" height="21" uuid="a0b1365c-26a1-462f-87dc-31fade94b432"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Qty"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="784" y="21" width="60" height="21" uuid="9024e2ff-dd91-4dd4-9051-a189b0429814"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Medium"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="714" y="21" width="60" height="21" uuid="43dc3005-2087-4ba7-bf3d-c4d04efdc436"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Location"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="50">
				<textField>
					<reportElement stretchType="RelativeToBandHeight" x="947" y="0" width="38" height="20" uuid="0330ccd7-fc62-41d1-a2bf-591497e922a1"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA["Total"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="1150" height="1" uuid="5e7c3879-0586-41e8-8d80-45d73fbc2e18"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField>
					<reportElement x="994" y="0" width="21" height="20" uuid="5d873ca1-534a-4a2e-ae88-69aca9062e57"/>
					<textElement>
						<font fontName="Franklin Gothic Book"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totalDayPass}]]></textFieldExpression>
				</textField>
				<textField pattern="###0.00;-###0.00">
					<reportElement x="1025" y="0" width="59" height="20" uuid="24e22cf5-e8a7-4162-8b99-93dfd420cc82"/>
					<textElement>
						<font fontName="Franklin Gothic Book"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totalDayPassAmount}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="80" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} ==1]]></printWhenExpression>
			<textField pattern="yy-MMM-dd">
				<reportElement style="Title" x="0" y="13" width="1150" height="39" uuid="1d5c64dd-1fb8-49ba-94da-3ba72ef5e4bf"/>
				<textElement textAlignment="Center">
					<font fontName="Constantia" size="26" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Daily Report of Day Pass Usage - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse($P{orderDate}))]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="29" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement x="0" y="0" width="60" height="20" uuid="9cd9a286-0532-424a-bf0d-c162775e0242"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{purchaseDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="70" y="0" width="100" height="20" uuid="dea4fb22-aa25-40b3-adb6-fcc5d60ca088"/>
				<textElement markup="none">
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{daypassName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement x="180" y="0" width="60" height="20" uuid="705a6040-ee54-423c-b5ca-07fba05362e4"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{activationDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement x="250" y="0" width="60" height="20" uuid="37cd77bd-74e9-416b-9bca-947b9f67493d"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{deactivationDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="994" y="0" width="20" height="20" uuid="ca6b3159-9eff-4245-9ca8-c4ee6333dda3"/>
				<textElement textAlignment="Left">
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="330" y="0" width="60" height="20" uuid="2f35eee0-ae95-4658-a772-e1a32753a131"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{purchaseType}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="390" y="0" width="60" height="20" uuid="d36bde6e-fff4-493f-a4b8-6f0bda52e062"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA["csv".equals($P{fileType})?new String("=\""+$F{academyId}+"\""):$F{academyId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="463" y="0" width="97" height="20" uuid="1d64292f-a196-41d5-8532-e9a71f5488d0"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{patronName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="571" y="0" width="60" height="20" uuid="31474cda-97c7-43ff-b38b-cfb866338e3d"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{staffName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="644" y="0" width="60" height="20" uuid="a29087a7-71cc-43de-b9fa-48d51a4182be"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMethodCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="714" y="0" width="60" height="20" uuid="1a31d725-159b-4d9e-b374-555ad51b3b7f"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{location}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="784" y="0" width="60" height="20" uuid="efb0f386-36a7-4389-ba16-e65f5c7fc68f"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMedia}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="854" y="0" width="60" height="20" uuid="f3383122-74a9-44b4-871d-93c5e79b6930"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{createBy}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement x="925" y="0" width="60" height="20" uuid="2d51d781-74ba-4f46-a67a-cc5eeebd295e"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{createDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00;-###0.00" isBlankWhenNull="true">
				<reportElement x="1025" y="0" width="59" height="20" uuid="064f0d51-6e68-42dc-803c-bd355c8fa750"/>
				<textElement textAlignment="Left">
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{price}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="1084" y="0" width="66" height="20" uuid="d1575d76-36e8-419d-877e-5b92e9c99ada"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{orderStatus}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="48" splitType="Stretch">
			<textField pattern="yyyy/MM/dd HH:mm">
				<reportElement style="Column header" x="132" y="18" width="101" height="20" forecolor="#000000" uuid="78303431-3840-4db8-aad9-06da5873c404"/>
				<textElement>
					<font fontName="Franklin Gothic Book" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="18" width="127" height="20" uuid="8ea97769-0b3e-484d-85cb-4c83c3bea988"/>
				<textElement>
					<font fontName="Franklin Gothic Book"/>
				</textElement>
				<text><![CDATA[Report Generated Time: ]]></text>
			</staticText>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
