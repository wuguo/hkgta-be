<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DailyMonthlyFacilityUsage" language="groovy" pageWidth="1190" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1150" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="0" uuid="9c87d979-4600-47cd-aa3b-66203774742e">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#FFFFFF" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<subDataset name="dataset1" uuid="9cd064b0-f401-41e6-8d59-0c353f9839b1"/>
	<parameter name="resvDate" class="java.lang.String"/>
	<parameter name="facilityType" class="java.lang.String"/>
	<parameter name="facilityCondition" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["GOLF".equals($P{facilityType})||"TENNIS".equals($P{facilityType})?" booking.resv_facility_type = '"+$P{facilityType}+"' ":" 1=1 "]]></defaultValueExpression>
	</parameter>
	<parameter name="TimePeriodType" class="java.lang.String">
		<defaultValueExpression><![CDATA[new String("DAILY")]]></defaultValueExpression>
	</parameter>
	<parameter name="TimePeriodTypeCondition" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[new String("DAILY".equals($P{TimePeriodType})?"Daily":"Monthly")]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFormatCondition" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[new String("DAILY".equals($P{TimePeriodType})?"yyyy-MMM-dd":"yyyy-MMM")]]></defaultValueExpression>
	</parameter>
	<parameter name="FacilityTypeConvert" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[new String("GOLF".equals($P{facilityType})||"TENNIS".equals($P{facilityType})?("TENNIS".equals($P{facilityType})?"Tennis":"Golf"):"All")]]></defaultValueExpression>
	</parameter>
	<parameter name="SQLDateFormatCondition" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["DAILY".equals($P{TimePeriodType})?" '%Y-%m-%d' ":" '%Y-%m' "]]></defaultValueExpression>
	</parameter>
	<parameter name="fileType" class="java.lang.String">
		<defaultValueExpression><![CDATA[new String("pdf")]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
  DATE_FORMAT(booking.create_date, '%Y/%m/%d') as resvDate,
  booking.resv_id as resvId,
  CASE booking.resv_facility_type
			WHEN 'GOLF' THEN CONCAT_WS('-','GBF',booking.resv_id)
      WHEN 'TENNIS' THEN CONCAT_WS('-','TCF',booking.resv_id)
  END AS resvIdString,
	CASE booking.resv_facility_type
			WHEN 'GOLF' THEN d.attribute_id
      WHEN 'TENNIS' THEN booking.facility_subtype_id
  END AS facilityType,
  IF(GROUP_CONCAT(distinct ft.facility_no),IF(GROUP_CONCAT(distinct ft.status) like '%TA%','Not Yet allotted',GROUP_CONCAT(distinct ft.facility_no)),'') as facilityNo,
  CASE WHEN roomFacility.room_resv_id IS NOT NULL THEN 'Guest Room Bundled'
       WHEN booking.order_no IS NULL THEN
						CONCAT('Maintenance Offer',(
										SELECT DISTINCT
											IF(fromResvFac.resv_id is not null,IF(booking.resv_facility_type='GOLF',CONCAT('(','GBF-',fromResvFac.resv_id,')'),CONCAT('(','TCF-',fromResvFac.resv_id,')') ),'')
										FROM
											member_facility_type_booking bookingInter
										LEFT JOIN member_reserved_facility resvFac ON bookingInter.resv_id = resvFac.resv_id
										LEFT JOIN facility_timeslot facSlot ON resvFac.facility_timeslot_id = facSlot.facility_timeslot_id
										LEFT JOIN member_reserved_facility fromResvFac ON fromResvFac.facility_timeslot_id = facSlot.transfer_from_timeslot_id
										WHERE
											bookingInter.resv_id = booking.resv_id
								)
            )
				ELSE 'Member Booking'
  END AS resvType,
  m.academy_no AS academyId,
  CONCAT_WS(
		' ',
		cp.given_name,
		cp.surname
	) AS patronName,
  m.member_type AS memberType,
  DATE_FORMAT(booking.begin_datetime_book, '%Y-%m-%d %H:%i:%S') as serviceDate,
  CASE trans.payment_method_code
      when 'VISA' then 'VISA'
      when 'MASTER' then 'MASTER'
      when 'CASH' then 'Cash'
      when 'CASHVALUE' then 'Cash Value'
      when 'BT' then 'Bank Transfer'
      when 'CHEQUE' then 'Cheque'
      when 'cheque' then 'Cheque'
      when 'VAC' then 'Virtual Account'
      when 'CUP' then 'Union Pay'
      when 'AMEX' then 'American Express'
      when 'OTH' then 'N/A'
      ELSE 'N/A'
  END as paymentMethodCode,
  CASE trans.payment_media
			WHEN 'OTH' THEN 'N/A'
      WHEN 'OP' THEN 'Online Payment'
      WHEN 'ECR' THEN 'ECR Terminal'
      WHEN 'FTF' THEN 'N/A'
      WHEN '' THEN 'N/A'
      ELSE 'N/A'
	END AS paymentMedia,
	booking.reserve_via AS location,
  booking.facility_type_qty AS noOfFacility,
  ifnull(trans.paid_amount,0) as paidAmount,
  CASE booking.status
			WHEN 'RSV' THEN 'Reserved'
      WHEN 'ATN' THEN 'Attended'
			WHEN 'NAT' THEN 'Not Attended'
	END AS status,
  (SELECT CONCAT_WS(' ',staffProfileCreateBy.given_name,staffProfileCreateBy.surname) FROM staff_profile staffProfileCreateBy WHERE staffProfileCreateBy.user_id = booking.create_by
   UNION ALL
   SELECT CONCAT_WS(' ',memberProfile.given_name,memberProfile.surname) FROM customer_profile memberProfile, user_master userMasterMember, member memberCreateBy
   WHERE userMasterMember.user_type = 'CUSTOMER' AND memberCreateBy.user_id = userMasterMember.user_id AND memberProfile.customer_id = memberCreateBy.customer_id
   AND booking.create_by = userMasterMember.user_id
  ) AS createBy

FROM
  customer_profile cp,
  member m,
  member_facility_type_booking booking
  LEFT JOIN member_facility_book_addition_attr d ON booking.resv_id = d.resv_id
LEFT JOIN customer_order_hd hd ON hd.order_no = booking.order_no
LEFT JOIN customer_order_trans trans ON trans.order_no = hd.order_no AND trans.status = 'SUC'
LEFT JOIN room_facility_type_booking roomFacility ON roomFacility.facility_type_resv_id = booking.resv_id AND roomFacility.is_bundle = 'Y'
LEFT JOIN member_reserved_facility mrf ON booking.resv_id = mrf.resv_id
LEFT JOIN facility_timeslot ft ON mrf.facility_timeslot_id = ft.facility_timeslot_id AND ft.status in('OP','TA')
WHERE
	booking.customer_id = cp.customer_id
AND cp.customer_id = m.customer_id
AND booking.exp_coach_user_id is NULL
AND booking.status in ('RSV','ATN','NAT')
AND date_format(booking.create_date,$P!{SQLDateFormatCondition})
=date_format($P{resvDate},$P!{SQLDateFormatCondition})
AND $P!{facilityCondition}
group by resvId]]>
	</queryString>
	<field name="resvDate" class="java.lang.String"/>
	<field name="resvId" class="java.lang.Long"/>
	<field name="facilityType" class="java.lang.String"/>
	<field name="resvType" class="java.lang.String"/>
	<field name="academyId" class="java.lang.String"/>
	<field name="patronName" class="java.lang.String"/>
	<field name="memberType" class="java.lang.String"/>
	<field name="serviceDate" class="java.lang.String"/>
	<field name="paymentMethodCode" class="java.lang.String"/>
	<field name="paymentMedia" class="java.lang.String"/>
	<field name="location" class="java.lang.String"/>
	<field name="noOfFacility" class="java.lang.Integer"/>
	<field name="paidAmount" class="java.math.BigDecimal"/>
	<field name="status" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="createBy" class="java.lang.String"/>
	<field name="resvIdString" class="java.lang.String"/>
	<field name="facilityNo" class="java.lang.String"/>
	<variable name="totalQty" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{noOfFacility}]]></variableExpression>
	</variable>
	<variable name="totalAmount" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{paidAmount}]]></variableExpression>
	</variable>
	<group name="Group1" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupHeader>
			<band height="63" splitType="Stretch">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="54" width="1150" height="1" uuid="9aaeb444-d5c1-45f5-9eef-85e1a7903e11"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="0" y="31" width="72" height="20" uuid="94949875-f72e-4448-8b89-9928b0d45516"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Reservation Date"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="71" y="31" width="72" height="20" uuid="1ba2c190-ff6b-4dab-9d5b-88f88f1a0ae5"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Reservation ID"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="142" y="31" width="72" height="20" uuid="48e0282d-5491-4f14-9ebe-e52839976d91"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Facility Type"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="284" y="31" width="72" height="20" uuid="a9933c21-28a2-4ce0-b6fd-74465812ff4c"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Reservation Type"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="355" y="31" width="72" height="20" uuid="ccb77577-0498-4e0a-8ae2-f113aee8b6f1"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Academy ID"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="426" y="31" width="72" height="20" uuid="d498852a-2ce6-47bc-9daf-ecc9ca4fe486"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Patron Name"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="497" y="31" width="72" height="20" uuid="f9fe8a8d-2ef9-4516-a1d8-50d7048875e8"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Member Type"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="568" y="31" width="72" height="20" uuid="285d3323-651a-4a5e-818b-a5b39473a25d"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Service Date Time"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="639" y="31" width="72" height="20" uuid="6a10c53b-b121-4b50-9b5a-4f0ab4369428"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Method"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="710" y="31" width="72" height="20" uuid="ebc63e43-d078-42f2-b031-dbd3a1663109"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Payment Medium"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="781" y="31" width="72" height="20" uuid="ca5fa185-8566-4907-935d-508b70f72a3c"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Location"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="852" y="31" width="72" height="20" uuid="dd36f24d-3f27-4f4f-bf25-02d792b6e6c5"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["No of Facility"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="994" y="31" width="72" height="20" uuid="07b7e958-b2e2-4f9d-9015-a260d2fd9b52"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Status"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="1065" y="31" width="72" height="20" uuid="eabbbd82-db52-4f16-906a-ce6b58810ef6"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["CreateBy"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="923" y="31" width="72" height="20" uuid="49556bde-44c6-4525-96f4-7b1f1dfe5e0c"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Paid Amount"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="213" y="31" width="72" height="20" uuid="d8ca24b8-2d62-45df-b6d3-22188c4eeccf"/>
					<textElement markup="none">
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Bay/Court#"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="51">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="1150" height="1" uuid="5e7c3879-0586-41e8-8d80-45d73fbc2e18"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToBandHeight" x="852" y="13" width="72" height="20" uuid="baba2f59-a850-4d03-8acd-26a0b89d810f"/>
					<textElement>
						<font fontName="Constantia" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Total:"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToBandHeight" x="923" y="13" width="72" height="20" uuid="af2ea3d7-702e-4ca5-afa1-9b29d75379a9"/>
					<textElement>
						<font fontName="SansSerif"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totalQty}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToBandHeight" x="994" y="13" width="72" height="20" uuid="475a49c1-bff0-450f-baaf-fc3938f11627"/>
					<textElement>
						<font fontName="SansSerif"/>
					</textElement>
					<textFieldExpression><![CDATA["HK\$ "+$V{totalAmount}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="68" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} ==1]]></printWhenExpression>
			<textField>
				<reportElement style="Title" x="0" y="13" width="1150" height="39" uuid="1d5c64dd-1fb8-49ba-94da-3ba72ef5e4bf"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Constantia" size="26" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{TimePeriodTypeCondition}+" Facility Usage Report ("+$P{FacilityTypeConvert}+") - " + new SimpleDateFormat($P{DateFormatCondition}).format(new SimpleDateFormat("yyyy-MM-dd").parse($P{resvDate}))]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="38" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="0" width="72" height="20" uuid="3bc035df-4a98-423d-bad4-74c5b0d396d2"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{resvDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="71" y="0" width="72" height="20" uuid="e78e591e-7093-4fde-9251-e18222f250c1"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{resvIdString}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="142" y="0" width="72" height="20" uuid="ec92e251-d597-46b4-858d-acc042c1a536"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{facilityType}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="284" y="0" width="72" height="20" uuid="dfa0ed3c-e02a-43a8-ba5a-c33d7993ebc5"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{resvType}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="355" y="0" width="72" height="20" uuid="58a82277-9cec-44a7-aeb8-da845ae4fd0e"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA["csv".equals($P{fileType})?new String("=\""+$F{academyId}+"\""):$F{academyId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="426" y="0" width="72" height="20" uuid="20deeb54-37db-4084-b6d8-b3d14aed3e08"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{patronName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="497" y="0" width="72" height="20" uuid="38a3ea19-3ea1-421d-8a5a-d5ae2df953e6"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{memberType}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="568" y="0" width="72" height="20" uuid="53e31ba8-a1ba-4c70-983d-e0f5c6680d9e"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="639" y="0" width="72" height="20" uuid="c0c583fa-7571-4a77-bb45-6c94631d7565"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMethodCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="710" y="0" width="72" height="20" uuid="f533adf2-101d-4eb0-b565-1c8a96851f4d"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentMedia}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="781" y="0" width="72" height="20" uuid="dc493b76-7dbf-4be1-b615-5842d33e1049"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{location}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="852" y="0" width="72" height="20" uuid="e954281d-2c18-4baa-9be5-5a8679207cc2"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{noOfFacility}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="994" y="0" width="72" height="20" uuid="33ac1139-39ae-4b64-a16f-7e7d5b9cc904"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{status}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1065" y="0" width="72" height="20" uuid="0eb5f69e-5b80-4a37-aba8-fd4bd5a92511"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{createBy}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="923" y="0" width="72" height="20" uuid="aa89f428-024b-471e-ae55-6f00de1ac7b2"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paidAmount}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="213" y="0" width="72" height="20" uuid="8a0ef7d6-58a6-46ef-8941-3bb1d7fafdd7"/>
				<textFieldExpression><![CDATA[$F{facilityNo}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="48" splitType="Stretch">
			<textField pattern="yyyy/MM/dd HH:mm">
				<reportElement style="Column header" x="162" y="18" width="193" height="20" forecolor="#000000" uuid="78303431-3840-4db8-aad9-06da5873c404"/>
				<textElement>
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="18" width="143" height="20" uuid="500fbea3-b75f-47eb-bf5b-d084cd50d23c"/>
				<textElement markup="none">
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA["Report Generated Time: "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="994" y="18" width="72" height="20" forecolor="#000000" uuid="72a0a438-460e-46ea-9ae3-c9694e088dc0"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Column header" x="923" y="18" width="72" height="20" forecolor="#000000" uuid="c45649a6-0e55-4fd5-b05b-582c9b7dc006"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
