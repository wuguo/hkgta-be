package com.sinodynamic.hkgta.filter;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.util.encrypt.EncryptUtil;

public class EncryptTool
{
	
	public Logger logger = Logger.getLogger(EncryptTool.class);
	
	public String ID_PREFIX = "_ID_";
	public String COMMA = ",";
	public static final String ID_PREFIX_KEY = "encrypt.id.prefix";
 	public static final String IDGENERATION = "key.id.generation";
	public static final String EXCLUDEDURI = "exclude.uri";
	public static final String REQUIRE_ENCRYPT_URI = "requrie.encrypt.uri";
	public static final String RESPONSE_NEED_ENCRYPT_URI = "response.need.encrypt.uri";
	public static final String ENCRYPT_ITEMS = "encrypt.items";
	
	private static EncryptTool instance = null;

	public static void main(String[] args)
	{
//		System.out.println(EncryptTool.getInstance().encryptIds("{\"customerId\":123}"));
//		System.out.println(EncryptTool.getInstance().decryptIds("**ID**57D98CE9798C791C791A71CD89D57C3B", true));
		System.out.println(EncryptTool.getInstance().decryptIds("**ID**9D971F868632F0657F49EABE0EE71874", true));
	}

	public static synchronized EncryptTool getInstance()
	{
		if (instance == null)
		{
			instance = new EncryptTool();
		}
		return instance;
	}
 	
 	private Properties appProps;
 	
 	private EncryptTool()
 	{
 		appProps = new Properties();
 		InputStream input = null;

 		try {
 			String path = "/" + this.getClass().getResource("/placeholder").toString().replace("file:/", "");
 			logger.info("=======================Init Encrypt Tool==========================");
 			input = new FileInputStream(path + "/app.properties");
 			appProps.load(input);
 		} catch (IOException ex) {
 			ex.printStackTrace();
 		} finally {
 			if (input != null) {
 				try {
 					input.close();
 				} catch (IOException e) {
 					e.printStackTrace();
 				}
 			}
 		}
 		ID_PREFIX = appProps.getProperty(ID_PREFIX_KEY);
 	}
 	
 	public String encryptIds(String originalStr)
	{
		String str = originalStr;
		for (EncryptItem item : EncryptItem.values())
		{
			int _index = 0;
			if (!EncryptTool.getInstance().isEncryptItem(item.getCode())) continue;
			
			while(str.toLowerCase().indexOf(item.getSourceString(), _index) >=0 )
			{
				int index = str.toLowerCase().indexOf(item.getSourceString(), _index);
				int start = index + item.getSourceString().length();
				int end = str.indexOf(",", index) < 0 ? (str.indexOf("}", index) < 0 ? str.length() : str.indexOf("}", index)) : str.indexOf(",", index);
				
				String originalValue = str.substring(start, end);
				String originalJson = "";
				originalJson = str.substring(index, end + 1);
				if (originalJson.contains(EncryptTool.getInstance().ID_PREFIX))
				{
					_index = end;
					continue;
				}
				String newJson = originalJson.replace(originalValue, "\"" + EncryptTool.getInstance().ID_PREFIX + EncryptUtil.getInstance().AESencode(originalValue, appProps.getProperty(EncryptTool.IDGENERATION)) + "\"");
				str = str.replace(originalJson, newJson);
				_index = end;
			}
		}

		Charset.forName("UTF-8").encode(str); //SAM 20160128: add this
		return str;
	}

	public String decryptIds(String originStr, boolean isURI)
	{
		int _index = 0;
		while (originStr.contains(ID_PREFIX))
		{
			int index = originStr.indexOf(ID_PREFIX, _index);
			int start = index;
			int end = originStr.indexOf(",", index) < 0 ? (originStr.indexOf("/", index) < 0 ? (originStr.indexOf("}", index) < 0 ? originStr.length() : originStr.indexOf("}", index) -1) : originStr.indexOf("/", index)) : originStr.indexOf(",", index) - 1;
			String ori = originStr.substring(start,end);
			ori = ori.replace("\"", "").replace("]", "").replace("}", "");
			String encryptString = EncryptUtil.getInstance().AESdecode(ori.replace(ID_PREFIX, ""), appProps.getProperty(EncryptTool.IDGENERATION));
			if (isURI)
			{
				encryptString = encryptString.replace("\"", "");
			}
			originStr = originStr.replace(ori, encryptString);
			_index = end;
		}
		Charset.forName("UTF-8").encode(originStr); //SAM 20160128: add this
		return originStr;
	}
	
	public boolean needEncrypt(String URI)
	{
		if (StringUtils.isEmpty(URI)) return false;
		
		return (exist(RESPONSE_NEED_ENCRYPT_URI, URI) || !exist(EXCLUDEDURI, URI));
	}
	
	public boolean requireEncrypt(String URI)
	{
		if (StringUtils.isEmpty(URI)) return false;
		
		return exist(REQUIRE_ENCRYPT_URI, URI);
	}
	
	public boolean isEncryptItem(String code)
	{
		return appProps.getProperty(ENCRYPT_ITEMS).contains(code);
	}
	
	private boolean exist(String key, String URI)
	{
		String[] uris = appProps.getProperty(key).split(",");
		for (String uri : uris)
		{
			if (URI.contains(uri)) return true;
		}
		
		return false;
	}

	public static enum EncryptItem
	{
		USERID("userid", "\"userid\":"), CUSTOMERID("customerid", "\"customerid\":"), RERVID("resvid", "\"resvid\":");

		public String getSourceString()
		{
			return sourceString;
		}
		
		public String getCode()
		{
			return code;
		}

		private String sourceString;
		
		private String code;

		private EncryptItem(String code, String sourceString)
		{
			this.code = code;
			this.sourceString = sourceString;
		}
	}
	
	public String encryptByDES(String originalStr)
	{
		String encryptString = EncryptUtil.getInstance().DESencode(originalStr, appProps.getProperty(EncryptTool.IDGENERATION));		
		Charset.forName("UTF-8").encode(encryptString); 
		return encryptString;
	}
	
	public String decryptByDES(String originalStr)
	{	
		String decryptString = EncryptUtil.getInstance().DESdecode(originalStr, appProps.getProperty(EncryptTool.IDGENERATION));		
		Charset.forName("UTF-8").encode(decryptString); 
		return decryptString;
	}
}
