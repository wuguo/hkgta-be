package com.sinodynamic.hkgta.filter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

public class CorsFilter implements Filter {
	  // For security reasons set this regex to an appropriate value
	  // example: ".*example\\.com"
	  private static final String ALLOWED_DOMAINS_REGEXP = "*";

	  public void doFilter(ServletRequest servletRequest,
	      ServletResponse servletResponse, FilterChain filterChain)
	      throws IOException, ServletException {
	    HttpServletRequest req = (HttpServletRequest) servletRequest;
	    HttpServletResponse resp = (HttpServletResponse) servletResponse;

	    //String origin = req.getHeader("Origin");
	    String origin = ALLOWED_DOMAINS_REGEXP;

	    if(true || origin != null && origin.matches(ALLOWED_DOMAINS_REGEXP)) {
	      resp.addHeader("Access-Control-Allow-Origin", origin);
	      resp.addHeader("Access-Control-Expose-Headers", "token");

	      if ("options".equalsIgnoreCase(req.getMethod())) {

	        resp.setHeader("Allow", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS");
	        if (origin != null) {
	          String headers = req.getHeader("Access-Control-Request-Headers");
	          String method = req.getHeader("Access-Control-Request-Method");
	          resp.addHeader("Access-Control-Allow-Methods", method);
	          resp.addHeader("Access-Control-Allow-Headers", headers);
	          resp.setContentType("text/plain");
	        }
	        resp.getWriter().flush();
	        return;
	      }
	    }

	    //set gta locale, get the value from client 
		String gtaLocale = req.getHeader("GTALocale");
		if(StringUtils.isEmpty(gtaLocale)){
			LocaleContextHolder.setLocale(Locale.ENGLISH);
			req.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, Locale.ENGLISH); 
		}else if("zh".equals(gtaLocale)){
			LocaleContextHolder.setLocale(Locale.TRADITIONAL_CHINESE);
			req.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, Locale.TRADITIONAL_CHINESE); 
		}else{
			LocaleContextHolder.setLocale(Locale.ENGLISH);
			req.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, Locale.ENGLISH); 
		}		
	    
	    // Fix ios6 caching post requests
	    if ("post".equalsIgnoreCase(req.getMethod())) {
	      resp.addHeader("Cache-Control", "no-cache");
	    }

	    if (filterChain != null) {
	      filterChain.doFilter(req, resp);
	    }
	  }

	  @Override public void destroy() {}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	}