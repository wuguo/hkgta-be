package com.sinodynamic.hkgta.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

public class GtaRequestFilter extends GenericFilterBean
{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
	
        HttpServletRequest req = (HttpServletRequest)request;
        
		String uri = req.getRequestURI();
		String context = req.getContextPath();
		String pathinfo = req.getPathInfo();

		
		if (!EncryptTool.getInstance().needEncrypt(pathinfo))
		{
			chain.doFilter(req, response);
			return;
		}
	
		if (EncryptTool.getInstance().requireEncrypt(pathinfo) && !uri.contains(EncryptTool.getInstance().ID_PREFIX))
		{
			ResponseResult result = new ResponseResult();
			result.initResult(GTAError.CommonError.REQUEST_INVALID);
//			result.initResult(GTAError.CommonError.Error, "Request is invalid!");
			response.getOutputStream().write((new ObjectMapper().writeValueAsString(result)).getBytes());
			//response.setContentType("application/json"); 
			response.setContentType("application/json;charset=UTF-8"); // SAM 20160128: add charset=UTF-8 
			return;
		}
		
		if (uri.contains(EncryptTool.getInstance().ID_PREFIX))
		{
			uri = EncryptTool.getInstance().decryptIds(uri, true);
			uri = uri.replace(context, "");
		}	
				
		ResettableStreamHttpServletRequest  requestWrapper = new ResettableStreamHttpServletRequest(req);
		
		Map<String, String[]> params = req.getParameterMap();
		Map<String, String[]> newParams = new HashMap<String, String[]>();
		for(String name : params.keySet())
		{
			String[] newValues = decryptParamValue(params, name);
			newParams.put(name, newValues);
		}
		requestWrapper.setParameterMap(newParams);
		
		BufferedReader reader =requestWrapper.getReader();
		StringBuffer json = new StringBuffer();
		String line = null;
		while ((line = reader.readLine()) != null) {
		 json.append(line); 
		}
		reader.close();

		String newJson = EncryptTool.getInstance().decryptIds(json.toString(), true);
		//requestWrapper.resetInputStream(newJson.getBytes());		
		requestWrapper.resetInputStream(newJson.getBytes("UTF-8")); // SAM 20160128: add UTF-8
        //	Charset.forName("UTF-8").encode(jsonUtf8);
 
		requestWrapper.setRequestURI(uri);
		
		chain.doFilter(requestWrapper, response);
	}

	private String[] decryptParamValue(Map<String, String[]> params, String name)
	{
		String[] values = params.get(name);
		String[] newValues = new String[values.length];
		int index = 0;
		for (String v : values)
		{
			//如果参数没有加密传递过来的情况
			if (!v.contains(EncryptTool.getInstance().ID_PREFIX))	
			{
				newValues[index] = v;
				index++;
				continue;
			}
			//如果v中出现多个参数拼装的情况，
			if(v.indexOf(EncryptTool.getInstance().COMMA) > 0){
				String[] param = v.split(EncryptTool.getInstance().COMMA);
				
				StringBuffer sb = new StringBuffer();
				for(String p : param){
					String rp = EncryptTool.getInstance().decryptIds(p, true);
					sb.append(rp).append(EncryptTool.getInstance().COMMA);
				}
				newValues[index] = sb.toString().substring(0,sb.toString().length()-1);
				index++;
				continue;
			}
			newValues[index] = EncryptTool.getInstance().decryptIds(v, true);
			index++;
		}
		
		return newValues;
	}

}
