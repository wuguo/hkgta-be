package com.sinodynamic.hkgta.filter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;

public class ResettableStreamHttpServletRequest extends HttpServletRequestWrapper
{

	private byte[] rawData;
	
	private String uri;

	private HttpServletRequest request;

	private ResettableServletInputStream servletStream;
	
	private Map<String, String[]> params;

	public ResettableStreamHttpServletRequest(HttpServletRequest request)
	{
		super(request);
		this.request = request;
		this.servletStream = new ResettableServletInputStream();
	}
	
	@Override
	public String getParameter(String name) {
		String result = "";
		
		Object v = params.get(name);
		if (v == null) {
			result = null;
		} else if (v instanceof String[]) {
			String[] strArr = (String[]) v;
			if (strArr.length > 0) {
				result =  strArr[0];
			} else {
				result = null;
			}
		} else if (v instanceof String) {
			result = (String) v;
		} else {
			result =  v.toString();
		}
		
		return result;
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		return params;
	}
	
	public void setParameterMap(Map<String, String[]> newParams)
	{
		this.params = newParams;
	}

	@Override
	public Enumeration getParameterNames() {
		return new Vector(params.keySet()).elements();
	}

	@Override
	public String[] getParameterValues(String name) {
		String[] result = null;
		
		Object v = params.get(name);
		if (v == null) {
			result =  null;
		} else if (v instanceof String[]) {
			result =  (String[]) v;
		} else if (v instanceof String) {
			result =  new String[] { (String) v };
		} else {
			result =  new String[] { v.toString() };
		}
		
		return result;
	}
	
	

	public void resetInputStream()
	{
		servletStream.stream = new ByteArrayInputStream(rawData);
	}

	public void resetInputStream(byte[] newData)
	{
		servletStream.stream = new ByteArrayInputStream(newData);
	}

	@Override
	public ServletInputStream getInputStream() throws IOException
	{

		if (rawData == null)
		{
			rawData = IOUtils.toByteArray(this.request.getReader());
			servletStream.stream = new ByteArrayInputStream(rawData);
		}

		return servletStream;
	}
	
	@Override
    public String getRequestURI() {
		return uri;
    }
	
	public void setRequestURI(String uri)
	{
		this.uri = uri;
	}

	@Override
	public BufferedReader getReader() throws IOException
	{
		if (rawData == null)
		{
			//rawData = IOUtils.toByteArray(this.request.getReader());
			rawData = IOUtils.toByteArray(this.request.getReader(), "UTF-8"); // SAM 20160128: add UTF-8
			
			servletStream.stream = new ByteArrayInputStream(rawData);
		}

		//return new BufferedReader(new InputStreamReader(servletStream));
		return new BufferedReader(new InputStreamReader(servletStream,"UTF-8")); // SAM 20160128: add UTF-8
	}

	private class ResettableServletInputStream extends ServletInputStream
	{
		private InputStream stream;
		
		@Override
		public int read() throws IOException
		{
			return stream.read();
		}
	}

}