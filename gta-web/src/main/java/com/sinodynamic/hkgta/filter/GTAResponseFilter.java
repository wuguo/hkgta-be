package com.sinodynamic.hkgta.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;

public class GTAResponseFilter implements Filter
{
    private FilterConfig filterConfig = null;
    
    private static class ByteArrayServletStream extends ServletOutputStream
    {
        ByteArrayOutputStream baos;

        ByteArrayServletStream(ByteArrayOutputStream baos)
        {
            this.baos = baos;
        }

        public void write(int param) throws IOException
        {
            baos.write(param);
        }
    }

    private static class ByteArrayPrintWriter
    {

        private ByteArrayOutputStream baos = new ByteArrayOutputStream();

        private PrintWriter pw = new PrintWriter(baos);

        private ServletOutputStream sos = new ByteArrayServletStream(baos);

        public PrintWriter getWriter()
        {
            return pw;
        }

        public ServletOutputStream getStream()
        {
            return sos;
        }

        byte[] toByteArray()
        {
            return baos.toByteArray();
        }
    }

    public class CharResponseWrapper extends HttpServletResponseWrapper
    {
        private ByteArrayPrintWriter output;
        private boolean usingWriter;

        public CharResponseWrapper(HttpServletResponse response)
        {
            super(response);
            usingWriter = false;
            output = new ByteArrayPrintWriter();
        }

        public byte[] getByteArray()
        {
            return output.toByteArray();
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException
        {
            // will error out, if in use
            if (usingWriter) {
                super.getOutputStream();
            }
            usingWriter = true;
            return output.getStream();
        }

        @Override
        public PrintWriter getWriter() throws IOException
        {
            // will error out, if in use
            if (usingWriter) {
                super.getWriter();
            }
            usingWriter = true;
            return output.getWriter();
        }

        public String toString()        
        {
            //return output.toString(); original code
        	
        	// SAM 20160128: change { 
        	String out=output.toString();
        	Charset.forName("UTF-8").encode(out); 
        	return out;
        	// end }

        }
    }

    public void init(FilterConfig filterConfig) throws ServletException
    {
        this.filterConfig = filterConfig;
    }

    public void destroy()
    {
        filterConfig = null;
    }

    public void doFilter(
            ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
    	CharResponseWrapper wrappedResponse = new CharResponseWrapper(
                (HttpServletResponse)response);

        String uri = ((HttpServletRequest)request).getPathInfo();
        if (EncryptTool.getInstance().needEncrypt(uri))
        {
        	chain.doFilter(request, wrappedResponse);
        	byte[] bytes = wrappedResponse.getByteArray(); 
        	// String out = new String(bytes); 
        	String out = new String(bytes, "UTF-8"); // SAM 20160128: add UTF-8
        	Charset.forName("UTF-8").encode(out); // SAM 20160128: add this
        	
        	out = EncryptTool.getInstance().encryptIds(out);
        	//response.getOutputStream().write(out.getBytes());
        	response.getOutputStream().write(out.getBytes("UTF-8")); // SAM 20160128: add UTF-8
        }
        else
        {
        	chain.doFilter(request, response);
        }
        
    }
   
}