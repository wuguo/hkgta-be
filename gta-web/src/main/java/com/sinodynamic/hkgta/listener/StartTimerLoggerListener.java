package com.sinodynamic.hkgta.listener;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.sinodynamic.hkgta.scheduler.job.AutoLoggerTask;

public class StartTimerLoggerListener implements ServletContextListener {
	@Override
	/***
	 * web ran load time
	 */
	public void contextInitialized(ServletContextEvent sce) {
		
		JobDetail jobDetail = JobBuilder.newJob(AutoLoggerTask.class).withIdentity("autoLoggerJob_1", "group_1")
				.build();
		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("jobTrigger_1", "group_1")
				.withSchedule(CronScheduleBuilder.cronSchedule("1 0 0 * * ?")).build();
		Scheduler sched;
		try {
			sched =  StdSchedulerFactory.getDefaultScheduler();
			sched.scheduleJob(jobDetail, trigger);
			sched.start();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
	}
}
