package com.sinodynamic.hkgta.controller.crm.sales;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.MemberPaymentAcc;
import com.sinodynamic.hkgta.entity.crm.MemberPlanFacilityRight;
import com.sinodynamic.hkgta.entity.pms.UserConfirmReservedFacility;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class MemberController extends ControllerBase<Member>{
	private static final 	Logger logger = Logger.getLogger(MemberController.class); 
	
	@Autowired
	private MemberService memberService;
	
	// register member
	@RequestMapping(value = "/member_reg", method = RequestMethod.POST)
	public void register(HttpServletRequest request,HttpServletResponse response) {
		try {
			 Member member = parseJson(request,Member.class);
			 memberService.addMember(member);;
			 page.getList().add(member);
			 writeJson(page, response);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	// query member
	@RequestMapping(value = "/memberGet/{id}", method = RequestMethod.GET)
	public @ResponseBody Object getMembers(@PathVariable(value = "id") @EncryptFieldInfo Long customerId) {
		try {
			String id=customerId.toString();
			if(id != null && !"ALL".equals(id)){
				Member member = memberService.getMemberById(new Long(id));
				MemberCashvalue mc = member.getMemberCashvalue();
				if (mc != null) {
					mc.setMember(null);
				}
				for (MemberPaymentAcc mpa: member.getMemberPaymentAccs()) {
					mpa.setMember(null);
				}
				for (MemberPlanFacilityRight mpfr : member.getMemberPlanFacilityRights()) {
					mpfr.setMember(null);
				}
				return member;
				//this.writeJson(member, response);
				//String testjw = this.getI18nMessge("NotBlank");
				//System.out.println(testjw);
			}else{
				memberService.getMemberList(page, null);
				return page;
				//this.writeJson(page, response);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	// update member
	@RequestMapping(value = "/memberUpdate", method = RequestMethod.PUT)
	public void updateMember(HttpServletRequest request, HttpServletResponse response) {
		try {
			 String paraJson = fetchRequestJson(request);
			 Member member = parseJson(paraJson,Member.class);
			 memberService.updateMember(member);
			 page.getList().add(member);
			 writeJson(page, response);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	// delete member
	@RequestMapping(value = "/memberDelete/{id}", method = { RequestMethod.DELETE })
	public void deleteMember(@PathVariable(value = "id") Long id,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Member member = new Member();
			member.setCustomerId(id);
			memberService.deleteMember(member);
			memberService.getMemberList(page, member);
			writeJson(page, response);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@RequestMapping(value = "/member/profile", method = { RequestMethod.GET })
	public ResponseResult getMemberInfo(@PathVariable(value = "qrCode") String qrCode) {
	      //MemberInfoDto memberInfo= new  MemberInfoDto();
	      /*SELECT   m.*
	      FROM   permit_card_master pcm
	      LEFT JOIN  member m ON m.customer_id = pcm.mapping_customer_id
	      WHERE   card_no = SUBSTRING('100000003X', 1, 9)*/
		  return responseResult;
	}
	
	/**
	 * added by Kaster 20160510
	 * SGG-1730:setting VIP for members
	 * vipOrNot:传值只有两种：Y/N
	 */
	@RequestMapping(value="/member/changeVip/{customerId}/{vipOrNot}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult setVip(@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId,@PathVariable(value="vipOrNot") String vipOrNot){
		Member m;
		try {
			m = memberService.getMemberById(customerId);
			if(null!=m){
				m.setVip(vipOrNot);
				memberService.updateMember(m);
				responseResult.initResult(GTAError.Success.SUCCESS);
			}else{
				responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.MemberError.MEMBER_UPDATE_FAILED);
		}
		return responseResult;
	}
	@RequestMapping(value="/member/checkConfirmReservedFacility/{userId}/{type}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkConfirmReservedFacility(@PathVariable(value = "userId")String userId,@PathVariable(value = "type")String type)
	{
		try {
			return memberService.checkConfirmReservedFacility(userId, type);
		} catch (Exception e) {
			 responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR);
			 return responseResult;
		}
	}
	@RequestMapping(value="/member/saveConfirmReservedFacility", method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult saveUserConfirmReservedFacility(@RequestBody UserConfirmReservedFacility confirm)
	{
		try {
			return memberService.saveConfirmReservedFacility(confirm);
		} catch (Exception e) {
			 responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR);
			 return responseResult;
		}
	}
	

}