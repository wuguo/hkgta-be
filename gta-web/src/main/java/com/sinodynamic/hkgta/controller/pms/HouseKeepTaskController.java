package com.sinodynamic.hkgta.controller.pms;


import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.pms.HouseKeepTaskDto;
import com.sinodynamic.hkgta.dto.pms.RoomWeeklyAttendantDto;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.pms.HouseKeepTaskService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/houseKeepTask")
public class HouseKeepTaskController extends ControllerBase {
	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private HouseKeepTaskService houseKeepTaskService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult searchHouseKeepTask(
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
			@RequestParam(value = "jobType", required = false) String jobType,
			@RequestParam(value = "startTime") String startTime, 
			@RequestParam(value = "endTime") String endTime,
			@RequestParam(value = "sortBy", defaultValue = "assignedDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false", required = false) String isAscending,
			@RequestParam(value = "roomNumber", required = false) String roomNumber) {
		try {
			AdvanceQueryDto advanceQueryDto = new AdvanceQueryDto();
			StringBuilder hql = houseKeepTaskService.getListSQLByJobType(jobType);
			if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
				hql.append(" and DATE_FORMAT(rh.schedule_datetime,'%Y-%m-%d') BETWEEN '").append(startTime).append("' ").append("AND '").append(endTime+"'");
			}
			if (StringUtils.isNotEmpty(roomNumber)) {
				hql.append(" AND  r.room_no=").append(roomNumber);
			}
			page.setNumber(pageNumber);
			page.setSize(pageSize);

			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);

			responseResult = advanceQueryService.getInitialQueryResultBySQL(advanceQueryDto, hql.toString(), page,
					HouseKeepTaskDto.class);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/roomWeeklyAttendantList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRoomWeeklyRosterList(
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "startTime") String startTime,
			@RequestParam(value = "endTime") String endTime,
			@RequestParam(value = "sortBy", defaultValue = "date", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending) {
		try {
			AdvanceQueryDto advanceQueryDto = new AdvanceQueryDto();
			StringBuilder hql = houseKeepTaskService.getRoomAttedantSql(userId, startTime, endTime);
			page.setNumber(pageNumber);
			page.setSize(pageSize);

			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);

			responseResult = advanceQueryService.getInitialQueryResultBySQL(advanceQueryDto, hql.toString(), page,
					RoomWeeklyAttendantDto.class);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	
}
