
package com.sinodynamic.hkgta.controller.rpos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.RefundParamterDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.service.rpos.RefundService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RefundServiceType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/refund")
public class RefundController extends ControllerBase {

	@Autowired
	RefundService refundService;
	
	@Autowired
	private CourseService courseService;

	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.PUT)
	public ResponseResult getPendRefundList(
			
			@RequestBody Map<String, Object> params)
			throws Exception {

		String base = (String)params.get("base");
		Integer days = (Integer)params.get("days");
		Integer pageNumber = (Integer)params.get("pageNumber");
		Integer pageSize = (Integer)params.get("pageSize");
		String sortBy = (String)params.get("sortBy");
		String filters = (String)params.get("filters");
		String isAscending = (String)params.get("isAscending");
		
		
		//Integer base = (Integer)params.get("base");
		
		
		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}

		Data list = refundService.getPendingRefundList(DateUtils.parseDate(base, new String[]{"yyyy-MM-dd"}), days, sortBy,
				(isAscending.equals("true") ? "asc" : "desc"), pageSize,
				pageNumber, query);

		this.responseResult.initResult(GTAError.Success.SUCCESS, list);
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/txn/service/list", method = RequestMethod.PUT)
	public ResponseResult getTxnServiceRefundList(
			@RequestBody RefundParamterDto dto  )
			throws Exception {

//		String status = dto.getStatus();
		String status =StringUtils.isEmpty(dto.getStatus())?"APR":dto.getStatus(); 
		String customerId = dto.getCustomerId()+"";
		Integer pageNumber = dto.getPageNumber();
		Integer pageSize = dto.getPageSize();
		String sortBy = dto.getSortBy();
		String filters = dto.getFilters();
		String isAscending = dto.getIsAscending();
		
		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}

		Data list = refundService.getServiceRefundList(status, Long.valueOf(customerId), sortBy,
				(isAscending.equals("true") ? "asc" : "desc"), pageSize,
				pageNumber, query);

		this.responseResult.initResult(GTAError.Success.SUCCESS, list);
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/service/list", method = RequestMethod.PUT)
	public ResponseResult getServiceRefundList(
			
			@RequestBody Map<String, Object> params)
			throws Exception {

		String status = (String)params.get("status");
		Integer pageNumber = (Integer)params.get("pageNumber");
		Integer pageSize = (Integer)params.get("pageSize");
		String sortBy = (String)params.get("sortBy");
		String filters = (String)params.get("filters");
		String isAscending = (String)params.get("isAscending");
		
		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}

		Data list = refundService.getServiceRefundList(status, null, sortBy,
				(isAscending.equals("true") ? "asc" : "desc"), pageSize,
				pageNumber, query);

		this.responseResult.initResult(GTAError.Success.SUCCESS, list);
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/cashvalue/list", method = RequestMethod.PUT)
	public ResponseResult getCashValueRefundList(
			@RequestBody RefundParamterDto dto   )
			throws Exception {
		
		String base = dto.getBase();
		Integer days = dto.getDays();
		String status = dto.getStatus();
		String customerId = dto.getCustomerId() == null ? "" : dto.getCustomerId().toString();
		Integer pageNumber = dto.getPageNumber();
		Integer pageSize = dto.getPageSize();
		String sortBy = dto.getSortBy();
		String filters = dto.getFilters();
		String isAscending = dto.getIsAscending();
		
		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}
		Date date = null;
		if(base != null){
			date = DateUtils.parseDate(base, new String[]{"yyyy-MM-dd"});
		}
		
		Data list = refundService.getCashValueRefundList(date,days,status,customerId == "" ? null : Long.parseLong(customerId),
				sortBy,
				(isAscending.equals("true") ? "asc" : "desc"), pageSize,
				pageNumber, query);

		this.responseResult.initResult(GTAError.Success.SUCCESS, list);
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/advancedSearch", method = RequestMethod.GET)
	public ResponseResult advanceSearch()
	{
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.refundService.advanceSearch());
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/cashvalue/advancedSearch", method = RequestMethod.GET)
	public ResponseResult cashvalueAdvanceSearch()
	{
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.refundService.cashValueAdvanceSearch());
		return this.responseResult;
	}

	@ResponseBody
	@RequestMapping(value = "/txn/service/advancedSearch", method = RequestMethod.GET)
	public ResponseResult serviceRefundTxnAdvanceSearch()
	{
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.refundService.serviceRefundTxnAdvanceSearch());
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/txn/cashvalue/advancedSearch", method = RequestMethod.GET)
	public ResponseResult cashValueRefundTxnAdvanceSearch()
	{
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.refundService.cashValueRefundTxnAdvanceSearch());
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/paymentInfo/{refundId}", method = RequestMethod.GET)
	public ResponseResult getPaymentInfo(@PathVariable("refundId") Long refundId)
	{
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.refundService.getPaymentInfo(refundId));
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/refundInfo/{refundId}", method = RequestMethod.GET)
	public ResponseResult getRefundInfo(@PathVariable("refundId") Long refundId)
	{
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.refundService.getRefundInfo(refundId));
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/{refundId}", method = RequestMethod.PUT)
	public ResponseResult refund(@PathVariable("refundId") Long refundId, @RequestBody Map<String, Object> params )
	{
		
		String amount = (String)params.get("refundAmt");
		String remark = (String)params.get("remark");
		
		this.refundService.refund(refundId, new BigDecimal(amount), this.getUser().getUserId(), remark);
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		this.sendEmail(refundId);
		return this.responseResult;
	}
	
	//modified by Kaster 20160323 增加@EncryptFieldInfo
	@ResponseBody
	@RequestMapping(value = "/cashvalue/request/{customerId}", method = RequestMethod.POST)
	public ResponseResult requestCashValueRefund(@PathVariable("customerId") @EncryptFieldInfo Long customerId, @RequestBody Map<String, Object> params )
	{
		
		String amount = (String)params.get("refundAmt");
		String remark = (String)params.get("remark");
		
		this.refundService.requestCashValueRefund(customerId, new BigDecimal(amount), this.getUser().getUserId(), remark);
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/cashvalue/{refundId}", method = RequestMethod.PUT)
	public ResponseResult cashValueRefund(@PathVariable("refundId") Long refundId, @RequestBody Map<String, Object> params )
	{
		
		String amount = (String)params.get("refundAmt");
		String remark = (String)params.get("remark");
		String refNo = (String)params.get("refNo");
		
		this.refundService.cashValueRefund(refundId, new BigDecimal(amount), this.getUser().getUserId(), remark, refNo);
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		this.sendEmail(refundId);
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/bookingInfo/{refundId}/{bookingType}", method = RequestMethod.GET)
	public ResponseResult getBookingDetail(@PathVariable("refundId") Long refundId, @PathVariable("bookingType") String bookingType)
	{
		RefundServiceType type = null;
		switch (bookingType)
		{
		case "GCH":
			type = RefundServiceType.GCH;
			break;
		case "GMS":
			type = RefundServiceType.GMS;
			break;
		case "GSS":
			type = RefundServiceType.GSS;
			break;
		case "TCH":
			type = RefundServiceType.TCH;
			break;
		case "TMS":
			type = RefundServiceType.TMS;
			break;
		case "TSS":
			type = RefundServiceType.TSS;
			break;
		case "CVL":
			type = RefundServiceType.CVL;
			break;
		case "SRV":
			type = RefundServiceType.SRV;
			break;
		case "ROOM":
			type = RefundServiceType.ROOM;
			break;
		case "WELLNESS":
			type = RefundServiceType.WELLNESS;
		break;	
		default:
			type = RefundServiceType.TCH;
			break;
		}
		
		if (type.equals(RefundServiceType.TSS) || type.equals(RefundServiceType.GSS))
		{
			ResponseResult tmp = refundService.getBookingDetail(refundId, type);
			CourseEnrollment courseEnrollment =  (CourseEnrollment)tmp.getDto();
			return courseService.getCourseAndMemberInfo(courseEnrollment.getCustomerIdValue().toString(), courseEnrollment.getCourseIdValue().toString(), refundId);
		}
		return refundService.getBookingDetail(refundId, type);
		
		
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/sendEmail", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult sendEmail(@RequestParam(value = "refundId",required = true)long refundId){
		Thread emailThread = new Thread(new EmailThread(refundId));
		emailThread.start();
		responseResult.initResult(GTAError.Success.SUCCESS);
        return responseResult;
	}
	private class EmailThread implements Runnable {
		
		private final long refundId;
		
		public EmailThread(long refundId) {
			this.refundId = refundId;
		}

		@Override
		public void run() {
			try {
				refundService.sendRefundResultEmail(refundId);
			} catch (Exception e) {
				logger.error("error sendRefundResultEmail. refundId:"+refundId, e);
				throw new GTACommonException(GTAError.CommonError.SEND_EMAIL_FAILED);
//				throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{"error sent email:",e.getMessage()});
			}
		}
	}
	@RequestMapping(value = "/reject/{refundId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult rejectRefund(@PathVariable(value = "refundId")long refundId){
		this.responseResult = this.refundService.rejectFefund(refundId,this.getUser().getUserId());
		if(GTAError.Success.SUCCESS.getCode().equals(this.responseResult.getReturnCode())){
			this.sendEmail(refundId);
		}
		return this.responseResult;
	}
	
	@ResponseBody
	@RequestMapping(value = "/cancelInfo/{resvId}", method = RequestMethod.GET)
	public ResponseResult getCancelInfo(@PathVariable("resvId") String resvId)
	{
		this.responseResult.initResult(GTAError.Success.SUCCESS, this.refundService.getCancelInfo(resvId));
		return this.responseResult;
	}
	/***
	 * 
	 * @param customerId
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/cashvalueOasis/{trancationId}/{academyId}", method = RequestMethod.GET)
	public ResponseResult requestCashValueRefund(@PathVariable("trancationId")Long transcationId,
			@PathVariable("academyId") String  academyId)
	{
		return this.refundService.refundCashValueOasis(transcationId, academyId);
		
	}
	
}

