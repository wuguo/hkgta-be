package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentDto;
import com.sinodynamic.hkgta.dto.crm.CustomerTransforListDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.RemarksService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
public class EnrollmentSettlementController extends ControllerBase {

	private static final 	Logger logger	= Logger.getLogger(EnrollmentSettlementController.class);

	@Autowired
	CustomerEnrollmentService	customerEnrollmentService;

	@Autowired
	private AdvanceQueryService	advanceQueryService;

	@Autowired
	private RemarksService		remarksService;

	@Autowired
	private UserMasterService	userMasterService;

	@RequestMapping(value = "/membership_mgr/assign_saleperson", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg assignSaleperson(@RequestParam("enrollment_id") Long enrollmentId, @RequestParam("saleperson") String salePerson) {
		try {
			String userId = getUser().getUserId();
			customerEnrollmentService.assignSaleperson(enrollmentId, salePerson, userId);
			responseMsg.initResult(GTAError.Success.SUCCESS);
			return responseMsg;
		} catch (Exception e) {
			logger.error("EnrollmentSettlementContoller.assignSaleperson Exception", e);
			// e.printStackTrace();
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}

	@RequestMapping(value = "/membership_mgr/change_enrollment_status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg changeEnrollmentStatus(@RequestParam("enrollment_id") Long enrollmentId, @RequestParam("status") String status) {
		try {
			customerEnrollmentService.updateEnrollmentStatus(enrollmentId, status, getUser().getUserId(), getUser().getUserName());
			responseMsg.initResult(GTAError.Success.SUCCESS);
			return responseMsg;
		} catch (Exception e) {
			logger.error("EnrollmentSettlementContoller.changeEnrollmentStatus Exception", e);
			// e.printStackTrace();
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}

	@RequestMapping(value = "/membership_mgr/change_renewal_status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg changeRenewalStatus(@RequestParam("orderNo") Long orderNo, @RequestParam("status") String status) {
		try {
			return customerEnrollmentService.updateRenewalStatus(orderNo, status, getUser().getUserId(), getUser().getUserName());
		} catch (Exception e) {
			logger.error("EnrollmentSettlementContoller.changeRenewalStatus Exception", e);
			// e.printStackTrace();
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}

	@RequestMapping(value = "/enrollment/view_enrollment_records", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult viewEnrollmentRecords(@RequestBody CustomerEnrollmentDto customerEnrollmentDto,
			@RequestParam(value = "device", defaultValue = "PC") String device) {
		LoginUser currentUser = getUser();
		ResponseResult result = this.customerEnrollmentService.viewEnrollmentRecords(customerEnrollmentDto, currentUser.getUserId(), device);

		return result;
	}

	@RequestMapping(value = "/enrollment/advancesearch", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult enrollmentQueryData(@RequestParam("pageNumber") Integer pageNumber, @RequestParam("pageSize") Integer pageSize,
			@RequestParam(value = "orderBy", defaultValue = "enrollCreateDate", required = false) String orderBy,
			@RequestParam(value = "status", defaultValue = "", required = false) String status,
			@RequestParam(value = "isAscending", defaultValue = "false", required = false) String isAscending,
			@RequestParam(value = "offerCode", defaultValue = "RENEW", required = false) String offerCode,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters,
			@RequestParam(value = "filterByCustomerId", defaultValue = "") @EncryptFieldInfo Long filterByCustomerId, //if defaultValue = "", return ALL
			@RequestParam(value = "device", defaultValue = "PC", required = false) String device, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String joinHQL = this.customerEnrollmentService.getEnrollmentRecordsSQL(offerCode);
		// advance search
		if (!StringUtils.isEmpty(offerCode) && offerCode.equalsIgnoreCase(Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL)) {
			joinHQL += " and t.offerCode =  '" + Constant.SERVICE_PLAN_OFFER_CODE_RENEWAL.toString() + "'";
		} else if (!StringUtils.isEmpty(offerCode) && offerCode.equalsIgnoreCase(Constant.SERVICE_PLAN_TYPE_ENROLLMENT)) {
			joinHQL += " and t.offerCode is null  ";
		}else if (!StringUtils.isEmpty(offerCode) && offerCode.equalsIgnoreCase(Constant.SERVICE_PLAN_TYPE_COMPLETE)) {
			joinHQL += " and  t.status =  '" + Constant.SERVICE_PLAN_TYPE_COMPLETE.toString() + "'";
		}
		if (!StringUtils.isEmpty(offerCode) && !offerCode.equalsIgnoreCase(Constant.SERVICE_PLAN_TYPE_COMPLETE)) {
			if (!"All".equalsIgnoreCase(status) && !StringUtils.isEmpty(status)) {
				joinHQL += " and  t.status =  '" + status + "'  ";
			}else{
				joinHQL += " and  t.status !=  'CMP'  ";
			}
		}
		//if (!"ALL".equals(filterByCustomerId)) {
		//if 0, return ALL
		if (filterByCustomerId!=null) {
			joinHQL += " and t.customerId2 = '" + filterByCustomerId + "' ";
		}
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null
				&& advanceQueryDto.getGroupOp().length() > 0) {
			joinHQL += " and ";
			if (!orderBy.endsWith("enrollCreateDate")) {
				orderBy = orderBy + ",enrollCreateDate";
				isAscending = isAscending + ",false";
			}
			prepareQueryParameter(pageNumber, pageSize, orderBy, isAscending, advanceQueryDto);
			return dealWithEnrollmentAdvanceResult(
					advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinHQL, page, CustomerEnrollmentDto.class),
					device);
		}
		// no advance condition search
		AdvanceQueryDto queryDto = new AdvanceQueryDto();
		if (!orderBy.endsWith("enrollCreateDate")) {
			orderBy = orderBy + ",enrollCreateDate";
			isAscending = isAscending + ",false";
		}
		prepareQueryParameter(pageNumber, pageSize, orderBy, isAscending, queryDto);
		return dealWithEnrollmentAdvanceResult(
				advanceQueryService.getInitialQueryResultBySQL(queryDto, joinHQL, page, CustomerEnrollmentDto.class),
				device);
	}

	private ResponseResult dealWithEnrollmentAdvanceResult(ResponseResult responseResult, String device) {
		LoginUser currentUser = getUser();
		Data data = (Data) responseResult.getData();
		List<CustomerEnrollmentDto> list = (List<CustomerEnrollmentDto>) data.getList();
		for (CustomerEnrollmentDto customerEnrollmentDto : list) {
			long customerId = customerEnrollmentDto.getCustomerId();
			customerEnrollmentDto.setEnrollDate(DateConvertUtil.getYMDDateAndDateDiff(customerEnrollmentDto.getEnrollDateDB()));
			// do not need to show remark on page
			ResponseResult temp = this.remarksService.checkUnreadRemarkByDevice(customerId, currentUser.getUserId(), device);
			customerEnrollmentDto.setRemarkNo(temp.getData() + "");
			UserMaster userMaster = userMasterService.getUserMasterByCustomerId(customerId);
			Boolean checkActivationEmailEnabledStatus = EnrollStatus.ANC.name().equals(customerEnrollmentDto.getStatus())
					|| EnrollStatus.CMP.name().equals(customerEnrollmentDto.getStatus());
			if (checkActivationEmailEnabledStatus && userMaster != null && userMaster.getPasswdChangeDate() == null) {
				customerEnrollmentDto.setIsActivationEmailEnabled(true);
			} else {
				customerEnrollmentDto.setIsActivationEmailEnabled(false);
			}
		}
		return responseResult;
	}
	
	
	/**
	 * 获取CustomerTransfor列表
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param status
	 * @param isAscending
	 * @return
	 */
	@RequestMapping(value = "/enrollment/getCustomerTransforList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCustomerTransforList(
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "enrollCreateDate", required = false) String sortBy,
			@RequestParam(value = "status", defaultValue = "", required = false) String status,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters) {
		logger.info("EnrollmentSettlementController.getCustomerTransforList start ...");
		try {
			String joinSQL = customerEnrollmentService.getCustomerTransforList();
			ResponseResult tmpResult = null;
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null
					&& advanceQueryDto.getGroupOp().length() > 0) {
				joinSQL = joinSQL + " AND ";
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
				tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page,
						CustomerTransforListDto.class);
			} else {
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page,
						CustomerTransforListDto.class);
			}
			return tmpResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	/**
	 * 批量更新销售人员 【长潘说all更新所有页面的所有记录的销售人员】
	 * @param enrollmentIds
	 * @param salePerson
	 * @return
	 */
	@RequestMapping(value = "/membership_mgr/batchAssignSaleperson", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg assignSaleperson(@RequestParam("enrollmentIds") String enrollmentIds, @RequestParam("salePerson") String salePerson,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters) {
		try {
			String userId = getUser().getUserId();
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
			customerEnrollmentService.assignSaleperson(enrollmentIds, salePerson, userId, advanceQueryDto);
			responseMsg.initResult(GTAError.Success.SUCCESS);
			return responseMsg;
		} catch (Exception e) {
			logger.error("EnrollmentSettlementContoller.assignSaleperson Exception", e);
			// e.printStackTrace();
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
}
