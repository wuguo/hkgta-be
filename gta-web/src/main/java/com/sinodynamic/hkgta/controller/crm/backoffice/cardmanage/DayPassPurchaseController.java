package com.sinodynamic.hkgta.controller.crm.backoffice.cardmanage;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.GuestProfileDto;
import com.sinodynamic.hkgta.dto.crm.PaymentOrderDto;
import com.sinodynamic.hkgta.dto.crm.PermitCardMasterDto;
import com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto;
import com.sinodynamic.hkgta.dto.memberapp.CandidateCustomerDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderPermitCardDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.CorporateMemberService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MembershipCardsManagmentService;
import com.sinodynamic.hkgta.service.crm.cardmanage.CandidateCustomerService;
import com.sinodynamic.hkgta.service.crm.cardmanage.DayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.leads.LeadCustomerService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CorporateServiceAccService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.PayMentOrderSearchType;
import com.sinodynamic.hkgta.util.constant.PermitCardMasterEnumStatus;
import com.sinodynamic.hkgta.util.exception.CommonException;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/cardmanage")
public class DayPassPurchaseController extends ControllerBase {
	private static final Logger							logger			= Logger.getLogger(DayPassPurchaseController.class);
	private static final int				NINE_DIGIT		= 9;
	private static final int				EIGHT_DIGIT		= 8;
	private static final String				PREFIX_BACKUP	= "00000000";

	@Autowired
	private DayPassPurchaseService			dayPassPurchaseService;

	@Autowired
	private AdvanceQueryService				advanceQueryService;

	@Autowired
	private LeadCustomerService				leadCustomerService;

	@Autowired
	private PermitCardMasterService			permitCardMasterService;

	@Autowired
	private CandidateCustomerService		candidateCustomerService;

	@Autowired
	private MembershipCardsManagmentService	membershipCardsManagmentService;
	@Autowired
	private CustomerProfileService			customerProfileService;
	@Autowired
	private CustomerServiceAccService		customerServiceAccService;
	@Autowired
	private MemberService					memberService;
	@Autowired
	private CorporateServiceAccService		corporateServiceAccService;
	@Autowired
	private CorporateMemberService			corporateMemberService;
	
	@Autowired
	private ServicePlanService servicePlanService;

	@RequestMapping(value = "/new/rulecheck/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	/**
	 * 
	 * @param customerId
	 * @param request
	 * @param response
	 * @return
	 * check member can buy daypass  or not.
	 * If member can buy daypass  then return the he can buy WD or FULL and the day  period he can buy
	 */
	public ResponseResult checkAddDayPassRule(@PathVariable(value = "customerId")@EncryptFieldInfo Long customerId, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			DaypassPurchaseDto dto = new DaypassPurchaseDto(customerId, "", null, null);
			DaypassPurchaseDto returnDto = dayPassPurchaseService.checkPurchasaeDayPassLimit(dto);
			if (returnDto.getTotalMemberQuota() > 0) {
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap.put("daypassPeriodType", returnDto.getPassPeriodType());
				// hashMap.put("planNo", dto.getPlanNo());
				hashMap.put("customerId", returnDto.getCustomerId());
				hashMap.put("totalMemberQuota", returnDto.getTotalMemberQuota());
				hashMap.put("effectiveStartDate", returnDto.getActivationDate());
				hashMap.put("effectiveEndDate", returnDto.getDeactivationDate());
				responseResult.initResult(GTAError.Success.SUCCESS, hashMap);
				return responseResult;
			}
			responseResult.initResult(GTAError.DayPassError.HAVE_NOT_GET_PURCHASE_RIGHT);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.DayPassError.DAY_PASS_CHECK_ERROR.getCode(), e);
			responseResult.initResult(GTAError.DayPassError.DAY_PASS_CHECK_ERROR);
			return responseResult;
		}
	}

	/**
	 * list all the daypass which their effectiveStartDate<=current_date<=effectiveEndDate,member can buy all these daypass, use begin and end to
	 * limit member not buy daypass outof their serviceplan date
	 */
	@RequestMapping(value = "/getMemberCanBuyDaypassList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberCanBuyDaypassList(@RequestParam(value = "customerId")@EncryptFieldInfo Long customerId) {
		try {
			DaypassPurchaseDto dto = new DaypassPurchaseDto(customerId, "", null, null);
			DaypassPurchaseDto returnDto = dayPassPurchaseService.checkPurchasaeDayPassLimit(dto);
			Date begin = DateConvertUtil.parseString2Date(returnDto.getActivationDate(), "yyyy-MM-dd");
			Date end = DateConvertUtil.parseString2Date(returnDto.getDeactivationDate(), "yyyy-MM-dd");
			dto = dayPassPurchaseService.getMemberCanBuyDaypassList(begin, end);
			dto.setPassPeriodType(returnDto.getPassPeriodType());
			dto.setTotalMemberQuota(returnDto.getTotalMemberQuota());
			this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
			return this.responseResult;
		} catch (Exception e) {
			logger.error(GTAError.DayPassError.DAY_PASS_CHECK_ERROR.getCode(), e);
			responseResult.initResult(GTAError.DayPassError.DAY_PASS_CHECK_ERROR);
			return responseResult;
		}
	}

	@RequestMapping(value = "/daypasspurchase/rulecheck", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkDaypassRule(@RequestParam(value = "customerId") Long customerId, @RequestParam(value = "planNo") Long planNo,
			@RequestParam(value = "activationDate") String activationDate, @RequestParam(value = "deactivationDate") String deactivationDate,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			DaypassPurchaseDto dto = setRuleDto(customerId, planNo, activationDate, deactivationDate);
			boolean isPermit = dayPassPurchaseService.checkPurchaseRight(dto);
			if (isPermit) {
				boolean isDayOk = dayPassPurchaseService.checkPurchaseDays(dto);
				if (isDayOk) {
					responseResult.initResult(GTAError.Success.SUCCESS);
					return responseResult;
				}
				responseResult.initResult(GTAError.DayPassError.PERIOD_DATE_NOT_ALLOW);
				return responseResult;
			}
			responseResult.initResult(GTAError.DayPassError.HAVE_NOT_GET_PURCHASE_RIGHT);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.DayPassError.DAY_PASS_CHECK_ERROR.getCode(), e);
			responseResult.initResult(GTAError.DayPassError.DAY_PASS_CHECK_ERROR);
			return responseResult;
		}
	}

	@RequestMapping(value = "/daypasspurchase/check_availability", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult checkAvailability(@RequestBody DaypassPurchaseDto dto) throws Exception {
		// try {
		JSONObject jsonObject = JSONObject.fromObject(dto);
		System.out.println(jsonObject);
		ResponseResult result = validateData(dto);
		if (!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
			return result;
		}
		// WD means only can buy weekday
		if ("WD".equalsIgnoreCase(dto.getPassPeriodType())) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar beginDay = Calendar.getInstance();
			beginDay.setTime(sdf.parse(dto.getActivationDate()));
			Calendar endDay = Calendar.getInstance();
			endDay.setTime(sdf.parse(dto.getDeactivationDate()));
			if (beginDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || beginDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
					|| endDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || endDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
				responseResult.initResult(GTAError.DayPassError.PERIOD_DATE_NOT_ALLOW);
				return responseResult;
			}
			int total = endDay.get(Calendar.DAY_OF_YEAR) - beginDay.get(Calendar.DAY_OF_YEAR);
			for (int i = 0; i < total; i++) {
				Calendar tempDay = Calendar.getInstance();
				tempDay.setTime(sdf.parse(dto.getActivationDate()));
				tempDay.add(Calendar.DATE, i);
				if (tempDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || tempDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					responseResult.initResult(GTAError.DayPassError.PERIOD_DATE_NOT_ALLOW);
					return responseResult;
				}
			}
		}
		return this.dayPassPurchaseService.checkAvailability(dto);
		// } catch (Exception e) {
		// this.responseResult.initResult(GTAError.DayPassPurchaseError.CHECKAVAILABILITY,
		// new Object[]{e.getMessage()});
		// logger.error(this.responseResult.getReturnCode(), e);
		// return this.responseResult;
	}

	/**
	 * 
	 * @param dto
	 * @return this method used for save button save order
	 */
	@RequestMapping(value = "/daypasspurchase", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult saveDayPassPurchase(@RequestBody DaypassPurchaseDto dto) {
		try {
			LoginUser currentUser = getUser();
			ResponseResult result = validateData(dto);
			if (!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
				return result;
			}
			return this.dayPassPurchaseService.saveDayPassPurchase(dto, currentUser.getUserId());
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.SAVEDAYPASSPURCHASE);
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}
	}

	/**
	 * 
	 * @param dto
	 * @return this method used for purchase button.If order not exist call saveDayPassPurchase() to save order
	 */
	@RequestMapping(value = "/daypasspurchase", method = RequestMethod.PUT, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult purchaseDaypass(@RequestBody DaypassPurchaseDto dto) {
		try {
			LoginUser currentUser = getUser();
			ResponseResult result = validateData(dto);
			if (!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
				return result;
			}
			return this.dayPassPurchaseService.purchaseDaypass(dto, currentUser.getUserId());
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.PURCHASEDAYPASS);
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}

	}

	/**
	 * 
	 * @param dto
	 * @return this method used for pay purchase daypass submit button really pay money
	 */
	@RequestMapping(value = "/paydaypasspurchase", method = RequestMethod.PUT, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult payDaypassPurchase(@RequestBody DaypassPurchaseDto dto) {
		try {
			LoginUser currentUser = getUser();
			if (null == dto.getOrderNo()) {
				this.responseResult.initResult(GTAError.DayPassPurchaseError.OrderNO_NULL);
				return this.responseResult;
			}
			if (StringUtils.isEmpty(dto.getPaymentMethodCode().trim())) {
				this.responseResult.initResult(GTAError.DayPassPurchaseError.PaymentMethodCode_NULL);
				return this.responseResult;
			}
			this.responseResult = this.dayPassPurchaseService.payDaypassPurchase(dto, currentUser.getUserId(), dto.getPaymentLocation());
			if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(this.responseResult.getReturnCode())
					&& !Constant.CREDIT_CARD.equals(dto.getPaymentMethodCode())&&
					!Constant.UNION_PAY.equals(dto.getPaymentMethodCode())) {
				Thread emailThread = new Thread(new EmailThread(dto, currentUser.getUserId()));
				emailThread.start();
			}
			return this.responseResult;
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.PAYDAYPASSPURCHASE);
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}

	}

	/**
	 * 
	 * @param orderNo
	 * @param acdemyID
	 * @param id
	 * @param memberName
	 * @param cardNo
	 * @return used for right menu's pay funtion
	 */
	@RequestMapping(value = "/paymentdaypass/{orderNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult payment(@PathVariable(value = "orderNo") Long orderNo) {
		try {
			if (null == orderNo) {
				this.responseResult.initResult(GTAError.DayPassPurchaseError.OrderNO_NULL);
				return this.responseResult;
			}
			return this.dayPassPurchaseService.payment(orderNo);
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.Payment, new Object[] { e.getMessage() });
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}

	}

	@RequestMapping(value = "/cardStatus", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult changeDaypassCardStatus(@RequestBody CustomerOrderPermitCardDto dto) {
		try {
			logger.info("DayPassPurchaseController.changeDaypassStatus start..." + dto);
			if (dto == null || dto.getOrderDetId() == null || StringUtils.isEmpty(dto.getStatus())) {
				responseResult.initResult(GTAError.DayPassError.DAY_PASS_CARD_UPDATE_PARAM_ERROR);
				return responseResult;
			}
			if (dayPassPurchaseService.changeDaypassCardStatus(dto)) {
				logger.info("DayPassPurchaseController.changeDaypassStatus end...");
				responseResult.initResult(GTAError.Success.SUCCESS);
			} else {
				responseResult.initResult(GTAError.DayPassError.DAY_PASS_CARD_UPDATE_STATUS_FAILED);
			}
		} catch (Exception e) {
			responseResult.initResult(GTAError.DayPassError.DAY_PASS_CARD_UPDATE_PARAM_ERROR);
		}
		return responseResult;
	}

	/***
	 * add search type  confirm dayPassId or cardNo or paymentNo
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param isAscending
	 * @param orderNo
	 * @param request
	 * @param response
	 * @param type (if type is detail , to search by orderNo  , if type is list ,to search by daypassId or cardNo to search)
	 * @return
	 */
	@RequestMapping(value = "/paymentorder/{orderNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPaymentListBySql(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "paymentNo") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending, @PathVariable(value = "orderNo") String orderNo,
			@RequestParam(value = "type",required=false) String type,
			HttpServletRequest request, HttpServletResponse response) {
		
		StringBuffer tmpSql = new StringBuffer();
		tmpSql.append(" ").append("SELECT t.* ").append(",CONCAT (c.salutation,' ',c.given_Name,' ',c.surname) memberName ")
		.append(",CONCAT (t.planName,'(',t.ageRange,')') daypassType ").append("FROM ( ").append("SELECT * ").append("FROM ( ")
		.append("SELECT copc.linked_card_no cardNo ").append(",copc.cardholder_customer_id customerId ").append(",copc.qr_code paymentNo ")
		.append(",copc.effective_from activationDate ").append(", copc.effective_to  expiryDate ").append(",sp.plan_name planName ")
		.append(",spp.age_range_code ageRange ").append(",copc.customer_order_no orderNo ").append(",card.status cardStatus ")
		.append(",orderhd.order_status orderStatus ").append("FROM customer_order_permit_card copc ")
		.append("LEFT JOIN customer_order_hd orderhd ON copc.customer_order_no = orderhd.order_no ")
		.append("LEFT JOIN permit_card_master card ON copc.linked_card_no = card.card_no ")
		.append("AND copc.cardholder_customer_id = card.mapping_customer_id ").append("INNER JOIN service_plan sp ")
		.append("INNER JOIN service_plan_pos spp ").append("WHERE copc.service_plan_no = sp.plan_no ")
		.append("AND copc.service_plan_no = spp.plan_no ").append("[replaceCondition]").append("ORDER BY card.status_update_date DESC ")
		.append(") subtab ").append("GROUP BY paymentNo ")
		// .append(",customerId ")
		.append(") t ").append("LEFT JOIN customer_profile c ON c.customer_id = t.customerId ")
		.append("LEFT JOIN sys_code s ON s.code_value = t.ageRange ").append("AND s.category = 'ageRange' ");

		String sql = "";
		if (orderNo != null) {
			if(!StringUtils.isEmpty(type)){
				sql=this.convertSql(tmpSql, orderNo, type);
			}else{
				if (StringUtils.startsWith(orderNo.toUpperCase(), "DP")) {
					if (orderNo.substring(2).length() > 8) {
						tmpSql.append(" where t.cardNo=" + orderNo.substring(2).substring(0, 8));
					} else {
						tmpSql.append(" where t.paymentNo=" + orderNo.substring(2));
					}
					sql = tmpSql.toString().replace("[replaceCondition]", " ");
				} else {
					tmpSql.append(" where t.orderNo=" + orderNo);
					sql = tmpSql.toString().replace("[replaceCondition]","AND copc.customer_order_no = " + orderNo + " ");
				}
			}
			try {
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getInitialQueryResultBySQL(queryDto, sql, page, PaymentOrderDto.class);
			} catch (RuntimeException e) {
				logger.error(e);
				responseResult.initResult(GTAError.DayPassError.GET_PAYMENT_LIST_FAILED);
				return responseResult;
			}
		}
		responseResult.initResult(GTAError.DayPassError.GET_PAYMENT_LIST_FAILED);
		return responseResult;

	}
	/***
	 * 
	 * @param tmpSql
	 * @param orderNo
	 * @param type value is list then by daypassId or cardNo to search else is detail to orderNo to search
	 * @return
	 */
	private String convertSql(StringBuffer tmpSql,String orderNo,String type){
		String sql = "";
		//to search by  daypassId or cardNo 
		if(type.equals(PayMentOrderSearchType.LIST.getDesc()))
		{
			if (StringUtils.startsWith(orderNo.toUpperCase(), "DP")) 
			{
				tmpSql.append(" where t.cardNo=" + orderNo.substring(2).substring(0, 8));
			}else{
				tmpSql.append(" where t.paymentNo=" + orderNo);
			}
			sql = tmpSql.toString().replace("[replaceCondition]", " ");
			
		}//to search by orderNo
		else if (type.equals(PayMentOrderSearchType.DETAIL.getDesc()))
		{
			tmpSql.append(" where t.orderNo=" + orderNo);
			sql = tmpSql.toString().replace("[replaceCondition]", " ");
		}
		return sql;
	}
	/**
	 * Method to get CandidateCustomer info
	 * 
	 * @param daypassId
	 * @return ResponseResult.
	 */
	@RequestMapping(value = "/getCandidateCustomer/{daypassId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCandidateCustomer(@PathVariable Long daypassId) {
		try {
			return candidateCustomerService.getCandidateCustomer(daypassId);
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * Method to get CandidateCustomer info for member app
	 * 
	 * @param daypassId
	 * @return CandidateCustomer.
	 */
	@RequestMapping(value = "/getCandidateCustomerInfo/{daypassId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCandidateCustomerInfo(@PathVariable Long daypassId) {
		try {
			return candidateCustomerService.getCandidateCustomerInfo(daypassId);
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * Method to get CandidateCustomer info
	 * 
	 * @param daypassId
	 * @return CandidateCustomer.
	 */
	@RequestMapping(value = "/editCandidateCustomer", method = { RequestMethod.PUT, RequestMethod.POST })
	@ResponseBody
	public ResponseResult editCandidateCustomer(@RequestBody CandidateCustomerDto candidateCustomerDto) {
		try {
			return candidateCustomerService.editCandidateCustomer(candidateCustomerDto);
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param isAscending
	 * @param customerId
	 * @param daypassType
	 * @return member's complete daypass list(effective or history list)
	 */
	@RequestMapping(value = "/getMemberDaypass", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberDaypass(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "activationDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending, @RequestParam(value = "customerId") @EncryptFieldInfo Long customerId,
			@RequestParam(value = "daypassType", defaultValue = "effective") String daypassType,
			@RequestParam(value = "filterBy", defaultValue = "") String filterBy) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT t.*, (CASE WHEN t.customerId is NULL then CONCAT(cc.given_Name,' ', cc.surname)   ");
		stringBuilder.append(" else CONCAT(c.salutation, ' ',c.given_Name,' ', c.surname) END) memberName, ");
		stringBuilder.append(" concat(t.planName,'(',t.ageRange,')') daypassType  ");
		stringBuilder
				.append(" FROM ( SELECT copc.qr_code paymentNo,copc.effective_from activationDate,copc.effective_to expiryDate,copc.linked_card_no cardNo,");
		stringBuilder
				.append(" copc.cardholder_customer_id customerId,sp.plan_name planName,spp.age_range_code ageRange,copc.customer_order_no orderNo,copc.candidate_customer_id candidateId  ");
		stringBuilder.append("  FROM customer_order_permit_card copc, service_plan sp, service_plan_pos spp  ");
		stringBuilder.append(" WHERE  copc.service_plan_no=sp.plan_no AND copc.service_plan_no=spp.plan_no    ) t  ");
		stringBuilder.append(" LEFT JOIN customer_profile c  ON c.customer_id =t.customerId  ");
		stringBuilder.append(" LEFT JOIN sys_code s  on s.code_value=t.ageRange and s.category='ageRange'  ");
		stringBuilder.append(" LEFT JOIN candidate_customer cc ON t.candidateId = cc.candidate_id  ");
		stringBuilder.append(" where t.orderNo in (select t.order_no orderNo from customer_order_hd  t  ");
		if ("effective".equalsIgnoreCase(daypassType)) {
			stringBuilder
					.append("	where exists( select 1  from customer_order_permit_card tt  where tt.customer_order_no=t.order_no and t.order_status = 'CMP' and    tt.effective_to >= CURRENT_DATE )  ");
		} else {
			stringBuilder
					.append("	where exists( select 1  from customer_order_permit_card tt  where tt.customer_order_no=t.order_no and t.order_status = 'CMP' and    tt.effective_to < CURRENT_DATE )  ");
		}
		stringBuilder.append("	and  t.customer_id = " + customerId + " )");
		try {
			if ("ALL".equalsIgnoreCase(filterBy)) {// in order to display all the records in one set
				responseResult = dayPassPurchaseService.getMemberDayPass(stringBuilder.toString(), sortBy, isAscending);
			} else {
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				responseResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, stringBuilder.toString(), page, PaymentOrderDto.class);
			}

			List list = null;
			if (responseResult.getListData() != null) {
				list = responseResult.getListData().getList();
			}

			if (list == null) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				Data emptyData = new Data();
				emptyData.setList(new ArrayList());
				responseResult.setData(emptyData);
				return responseResult;
			}

			for (Object object : list) {
				PaymentOrderDto paymentOrderDto = (PaymentOrderDto) object;
				String startDateString = paymentOrderDto.getStartDate();
				String expiryDateString = paymentOrderDto.getExpiryDate();
				Date startDate = DateConvertUtil.parseString2Date(startDateString, "yyyy-MMM-dd");
				Date expiryDate = DateConvertUtil.parseString2Date(expiryDateString, "yyyy-MMM-dd");
				// Convert them to the same format for compare them exactly.
				Date today = DateConvertUtil.parseString2Date(DateConvertUtil.parseDate2String(new Date(), "yyyy-MMM-dd"), "yyyy-MMM-dd");
				if (startDate.compareTo(today) <= 0 && expiryDate.compareTo(today) >= 0) {
					paymentOrderDto.setToday(true);
				}
				if (paymentOrderDto.getStartDate().equals(paymentOrderDto.getExpiryDate())) {
					paymentOrderDto.setPeriodDate(DateConvertUtil.getYMDDateAndDateDiff(startDate));
				} else {
					paymentOrderDto.setPeriodDate(DateConvertUtil.parseDate2String(startDate, "yyyy MMM dd") + " - "
							+ DateConvertUtil.parseDate2String(expiryDate, "MMM dd"));
				}
			}
			return responseResult;
		} catch (Exception e) {
			logger.error("DayPassPurchaseController.getMemberDaypass", e);
			responseResult.initResult(GTAError.DayPassError.GET_PAYMENT_LIST_FAILED);
			return responseResult;
		}

	}

	@RequestMapping(value = "/candidateByQRcode", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCandidateByQRcode(@RequestParam(value = "qrcode") Long qrcode) {
		CandidateCustomerDto dto = this.dayPassPurchaseService.getCandidateByQRcode(qrcode);
		responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return responseResult;
	}

	@RequestMapping(value = "/candidateByQRcode", method = RequestMethod.PUT, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult updateCandidateByQRcode(@RequestBody CandidateCustomerDto dto) {
		CandidateCustomerDto c = this.dayPassPurchaseService.updateCandidateCandidateCustomer(dto);
		responseResult.initResult(GTAError.Success.SUCCESS, c);
		return responseResult;
	}

	@RequestMapping(value = "/cardmapping/{customerId}/{qrcode}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult cardMappingCustomer(@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId,
			@PathVariable(value = "qrcode") BigInteger qrcode, HttpServletRequest request, HttpServletResponse response) {

		try {
			PaymentOrderDto dto = new PaymentOrderDto();
			dto.setCustomerId(customerId);
			dto.setPaymentNo(qrcode);
			if (dayPassPurchaseService.updateCustomerId2CustomerOrderPermitCard(dto)) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
		} catch (RuntimeException e) {
			logger.error("cardMappingCustomer update failed ", e);
		}
		responseResult.initResult(GTAError.DayPassPurchaseError.UPDATE_CUSTOMER_TO_CUSTOMERORDERPERMITCARD_FAILED);
		return responseResult;
	}

	private DaypassPurchaseDto setRuleDto(Long customerId, Long planNo, String activationDate, String deactivationDate) {
		DaypassPurchaseDto dto = new DaypassPurchaseDto();
		dto.setCustomerId(customerId);
		dto.setPlanNo(planNo);
		dto.setActivationDate(activationDate);
		dto.setDeactivationDate(deactivationDate);
		return dto;
	}

	public ResponseResult validateData(DaypassPurchaseDto dto) {
		if (null == dto.getActivationDate()) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.ACTIVATIONDATE);
			return this.responseResult;
		}
		if (null == dto.getDeactivationDate()) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.DEACTIVATIONDATE);
			return this.responseResult;
		}
		Long[] buysNos = dto.getBuysNos();
		Long[] daypassNos = dto.getDaypassNos();
		if (null == buysNos || buysNos.length <= 0) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.BUYNO_MISS);
			return this.responseResult;
		}
		if (null == daypassNos || daypassNos.length <= 0) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.DAYPASSNO_MISS);
			return this.responseResult;
		}
		if (daypassNos.length != buysNos.length) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.NOT_MATCH);
			return this.responseResult;
		}
		Long sum = (long) 0;
		for (Long temp : buysNos) {
			sum = sum + temp;
		}
		if (null == sum || 0 == sum) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.BUY_ZERO);
			return this.responseResult;
		}
		if (null==dto.getCustomerId()) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.ID_NULL);
			return this.responseResult;
		}
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		return this.responseResult;
	}

	/**
	 * Method to register the guest and create the related customer enrollment detail
	 *
	 * @throws Exception
	 */
	@RequestMapping(value = "/guest", method = { RequestMethod.POST, RequestMethod.PUT })
	@ResponseBody
	public ResponseResult guestRegistrationSave(@RequestBody GuestProfileDto guestProfileDto) {
		try {
			String loginUserId = getUser().getUserId();
			guestProfileDto.setLoginUserId(loginUserId);
			guestProfileDto.setSalesFollowBy(loginUserId);
			ResponseResult result = dayPassPurchaseService.saveOrUpdateDayPassGuestInfo(guestProfileDto);
			if ("0".equals(result.getReturnCode())) {
				CustomerProfile customerProfile = (CustomerProfile) result.getData();
				customerProfileService.moveProfileAndSignatureFile(customerProfile);
			}
			return result;
		} catch (CommonException ce) {
			logger.error("LeadCustomerController Error Message:", ce);
			return new ResponseResult("1", ce.getErrorMessage());
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			return new ResponseResult("1", "", "Unexpected Exception Catched!");
		}
	}

	@RequestMapping(value = "/scancard/{cardNo}/{orderNo}", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult scanQRCode(@PathVariable("cardNo") String cardNo, @PathVariable("orderNo") Long orderNo) {
		DaypassPurchaseDto dto = null;
		try {
			ResponseResult result = advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(cardNo)));
			if (!GTAError.CommomError.GET_MEMBER_BYQRCODE_SUCC.getCode().equals(result.getReturnCode())) {
				return result;
			}
			ScanCardMappingDto cardMappingDto = (ScanCardMappingDto) result.getData();
			dto = this.dayPassPurchaseService.scanQRCode(cardMappingDto, orderNo);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[] { e.getMessage() });
		}
		this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return this.responseResult;
	}

	/**
	 * link card ,update cardno to customer_order_permit_card customer enrollment detail
	 *
	 * @throws Exception
	 */
	@RequestMapping(value = "/card/{orderNo}/{qrcode}/{cardNo}", method = { RequestMethod.PUT })
	@ResponseBody
	public ResponseMsg linkCard(@PathVariable("orderNo") String orderNo, @PathVariable("cardNo") String cardNo, @PathVariable("qrcode") Long qrcode,
			@RequestBody PermitCardMasterDto dto) throws Exception {

		LoginUser user = super.getUser();
		if (cardNo == null || qrcode == null || user == null) {
			responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_PARAMETER_IS_WRONG);
			return responseResult;
		}
		if (membershipCardsManagmentService.orderHasbeenCanceled(Long.parseLong(orderNo))) {
			responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_FAILED);
//			responseResult.initResult(GTAError.CommonError.Error, "The day pass has been cancelled, you can not link card for this day pass.");
			return responseResult;
		}

		String userId = user.getUserId();
		dto.setUpdateBy(userId);
		Long customerId = dto.getCustomerId();
		if (membershipCardsManagmentService.hasIssuedCard(customerId)) {
			responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_RELATED_ERROR);
//			responseResult.initResult(GTAError.CommonError.Error, "The member has already linked a day pass card.");
			return responseResult;
		}
		String result = membershipCardsManagmentService.linkCardForMember(cardNoTransfer(cardNo), customerId, userId);
		if ("0".equals(result)) {
			CustomerOrderPermitCard card = new CustomerOrderPermitCard();
			card.setQrCode(qrcode);
			card.setLinkedCardNo(Long.parseLong(cardNoTransfer(cardNo)));
			card.setLinkedCardDate(new Date());
			if (dayPassPurchaseService.saveOrUpdateCustomerOrderPermitCard(card)) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
		}

		responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_FAILED);
		return responseResult;
	}

	private String cardNoTransfer(String cardNo) throws Exception {

		if (!cardNo.toUpperCase().startsWith("DP")) {
			throw new GTACommonException(GTAError.DayPassPurchaseError.CARD_NO_IS_INVALID);
		}

		cardNo = cardNo.substring(2);

		if (CommUtil.nvl(cardNo).length() == 0)
			return null;

		String cardTemp;
		int length = cardNo.length();
		if (length == NINE_DIGIT) {
			cardTemp = cardNo.substring(0, cardNo.length() - 1);
		} else if (length == EIGHT_DIGIT) {
			cardTemp = cardNo;
		} else {
			throw new GTACommonException(GTAError.DayPassPurchaseError.CARD_NO_IS_INVALID);
		}

		String cardNoStr = cardTemp.replaceFirst("^0*", "");
		return cardNoStr;
	}

	private String cardNoBackup(String cardNo) {

		if (CommUtil.nvl(cardNo).length() == 0)
			return null;
		String cardTemp = PREFIX_BACKUP + cardNo;
		int length = cardTemp.length();
		return cardTemp.substring(length - EIGHT_DIGIT, length);
	}

	@RequestMapping(value = "/replacement/{cardNo}/{qrcode}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg replaceCustomerCard(@PathVariable(value = "cardNo") String oldCardNo, @PathVariable(value = "qrcode") Long qrcode,
			@RequestBody PermitCardMasterDto dto) throws Exception {
		logger.info("DayPassPurchaseController.replaceCustomerCard invocation start ...");
		String newCardNo = dto.getCardNo();
		if (CommUtil.nvl(newCardNo, null) == null || CommUtil.nvl(String.valueOf(oldCardNo), null) == null || !StringUtils.isNumeric(newCardNo)) {
			logger.error("Request parameter error!");
			responseMsg.initResult(GTAError.CommonError.REQUEST_PARAMETER_ERROR);
//			responseMsg.initResult(GTAError.CommonError.Error, "Request parameter error!");
			return responseMsg;
		}
		LoginUser user = super.getUser();
		if (user == null) {
			logger.error("current user is null!");
			responseMsg.initResult(GTAError.CommonError.USER_IS_NULL);
//			responseMsg.initResult(GTAError.CommonError.Error, "can not get current user!");
			return responseMsg;
		}
		dto.setUpdateBy(user.getUserId());
		dto.setCardNo(cardNoTransfer(newCardNo));
		permitCardMasterService.replaceOldPermitCardMaster(oldCardNo, dto);
		CustomerOrderPermitCard card = new CustomerOrderPermitCard();
		card.setQrCode(qrcode);
		card.setLinkedCardNo(Long.valueOf(cardNoTransfer(newCardNo)));
		card.setLinkedCardDate(new Date());
		if (dayPassPurchaseService.saveOrUpdateCustomerOrderPermitCard(card)) {
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
		logger.info("DayPassPurchaseController.replaceCustomerCard invocation end ...");
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/dispose/{qrcode}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg disposeCard(@PathVariable(value = "qrcode") Long qrcode) throws Exception {
		logger.info("DayPassPurchaseController.disposeCustomerCard invocation start ...");

		LoginUser user = super.getUser();
		String userId = user.getUserId();

		ResponseResult result = dayPassPurchaseService.searchDayPassById("DP" + qrcode);
		if (null == result.getListData()) {
			responseResult.initResult(GTAError.DayPassPurchaseError.DOES_NOT_LINK_CARD);
			return responseResult;
		}

		List<PaymentOrderDto> daypass = (List<PaymentOrderDto>) result.getListData().getList();
		Integer linkedCardNo = daypass.get(0).getCardNo();
		if (null == linkedCardNo) {
			responseResult.initResult(GTAError.DayPassPurchaseError.DOES_NOT_LINK_CARD);
			return responseResult;
		}

		if (daypass.get(0).getCardStatus().equals(PermitCardMasterEnumStatus.DPS.getCode())
				|| daypass.get(0).getCardStatus().equals(PermitCardMasterEnumStatus.RTN.getCode())) {
//			responseResult.initResult(
//					GTAError.CommonError.Error,
//					"The card has already "
//							+ (daypass.get(0).getCardStatus().equals(PermitCardMasterEnumStatus.DPS.getCode()) ? "disposed!" : "returned!"));
			responseResult.initResult(
					GTAError.DayPassPurchaseError.DISPOSE_CARD_RELATED_ERROR,
					"The card has already "
							+ (daypass.get(0).getCardStatus().equals(PermitCardMasterEnumStatus.DPS.getCode()) ? "disposed!" : "returned!"));

			return responseResult;
		}

		// CustomerOrderPermitCard card = new CustomerOrderPermitCard();
		// card.setQrCode(qrcode);
		// card.setLinkedCardNo(null);
		// card.setLinkedCardDate(null);
		// dayPassPurchaseService.saveOrUpdateCustomerOrderPermitCard(card);
		permitCardMasterService.disposeCard(linkedCardNo.toString(), userId);

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/returnCard/{cardNo}/{dayPassId}", method = { RequestMethod.PUT })
	@ResponseBody
	public ResponseMsg returnCard(@PathVariable("cardNo") String cardNo, @PathVariable("dayPassId") String dayPassId) throws Exception {

		LoginUser user = super.getUser();
		if (cardNo == null || !cardNo.toUpperCase().startsWith("DP")) {
			responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_PARAMETER_IS_WRONG);
			return responseResult;
		}
		String userId = user.getUserId();

		ResponseResult tmp = dayPassPurchaseService.searchDayPassById("DP" + dayPassId);

		if (tmp.getReturnCode().startsWith("0")) {
			PaymentOrderDto dto = (PaymentOrderDto) tmp.getListData().getList().get(0);
			if (!dto.getCardNo().toString().equals(cardNoTransfer(cardNo))) {
//				responseResult.initResult(GTAError.CommonError.Error, "The card : " + cardNo.substring(0, cardNo.length() - 1)
//						+ " is not belongs to Day Pass : " + dayPassId);
				responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_RELATED_ERROR, "The card : " + cardNo.substring(0, cardNo.length() - 1)
				+ " is not belongs to Day Pass : " + dayPassId);
				return responseResult;
			}
		} else {
			responseResult.initResult(GTAError.DayPassPurchaseError.DOES_NOT_LINK_CARD);
			return responseResult;
		}

		permitCardMasterService.returnCard(cardNoTransfer(cardNo).toString(), userId);
		responseResult.initResult(GTAError.Success.SUCCESS, cardNo.substring(2).substring(0, 8) + ", return card success");
		return responseResult;
	}

	@RequestMapping(value = "/returnCard/{cardNo}", method = { RequestMethod.PUT })
	@ResponseBody
	public ResponseMsg batchReturnCard(@PathVariable("cardNo") String cardNo) throws Exception {

		LoginUser user = super.getUser();
		if (cardNo == null || !cardNo.toUpperCase().startsWith("DP")) {
			responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_PARAMETER_IS_WRONG);
			return responseResult;
		}
		String userId = user.getUserId();
		
		PermitCardMaster pcm = permitCardMasterService.getPermitCardMaster(cardNoTransfer(cardNo).toString());
		if(pcm == null){
			responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_PARAMETER_IS_WRONG);
			return responseResult;
		} else{
			if(PermitCardMasterEnumStatus.RTN.getCode().equals(pcm.getStatus())){								
				responseResult.initResult(GTAError.Success.SUCCESS, cardNo.substring(2).substring(0, 8) + ", already returned");
				return responseResult;
			}
		}
		
		permitCardMasterService.returnCard(cardNoTransfer(cardNo).toString(), userId);
		responseResult.initResult(GTAError.Success.SUCCESS, cardNo.substring(2).substring(0, 8) + ", return card success");
		return responseResult;
	}
	
	@RequestMapping(value = "/dayPassWeeklyRepeatBit", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult daypassWeeklyRepeatBit(@RequestParam(value = "daypassNos",required=true) Long[] daypassNos) {
		
		return servicePlanService.getWeeklyRepeatBitByNos(daypassNos);
	}

	private class EmailThread implements Runnable {

		private DaypassPurchaseDto	dto;
		private String				userId;

		public EmailThread(DaypassPurchaseDto dto, String userId) {
			this.dto = dto;
			this.userId = userId;
		}

		@Override
		public void run() {
			dayPassPurchaseService.sendEmail(dto, userId);
		}
	}
}