package com.sinodynamic.hkgta.controller.pos;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantCustomerBookingDto;
import com.sinodynamic.hkgta.dto.pos.RestaurantMasterDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.NotificationOnOffService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.pos.RestaurantCustomerBookingService;
import com.sinodynamic.hkgta.service.pos.RestaurantMasterService;
import com.sinodynamic.hkgta.service.pos.RestaurantOpenHourService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
public class RestaurantController extends ControllerBase {

	private Logger logger = Logger.getLogger(RestaurantController.class);

	@Autowired
	private RestaurantMasterService restaurantMasterService;

	@Autowired
	private RestaurantCustomerBookingService restaurantCustomerBookingService;

	@Autowired
	private CustomerProfileService customerProfileService;

	@Autowired
	private ShortMessageService smsService;

	@Autowired
	private MessageTemplateService messageTemplateService;

	@Autowired
	private RestaurantOpenHourService restaurantOpenHourService;

	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private UsageRightCheckService	usageRightCheckService;
	
	@RequestMapping(value = "/restaurants", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantList(
			@RequestParam(value = "featureCode", required = false) String imageFeatureCode) {
		List<RestaurantMasterDto> retLst = restaurantMasterService.getRestaurantMasterList(imageFeatureCode);
		
//		for(RestaurantMasterDto restaurantDto: retLst){
//			String facilityName=Constant.FacilityCategory.valueOf("RESTAURANT"+restaurantDto.getRestaurantId()).getFacilityName();
//			restaurantDto.setRestaurantName(responseResult.getI18nMessge(facilityName));			
//		}
		
		responseResult.initResult(GTAError.Success.SUCCESS, retLst);
		return responseResult;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/restaurants/{restaurantId}/reservations", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantReservations(@PathVariable(value = "restaurantId") String restaurantId,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
			@RequestParam(value = "isHistory", defaultValue = "false") Boolean isHistory,
			@RequestParam(value = "customerId", required = false) @EncryptFieldInfo Long customerId,
			@RequestParam(value = "filters", required = false, defaultValue = "") String filters) {

		try {
			// int updateCount =
			// restaurantCustomerBookingService.updateRestaurantCustomerBookingExpired();
			// logger.info("update Restaurant Customer Booking Expired count: "
			// + updateCount);
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			StringBuilder sql = new StringBuilder();
			sql.append("select rcb.resv_Id as resvId,rcb.restaurant_Id as restaurantId,rm.restaurant_name as restaurantName,"
					+ " date_format(book_Time,'%Y-%m-%d %H:%i:%s') as bookTime,rcb.party_Size as partySize,rcb.status as status,rcb.book_Via as bookVia,rcb.remark as remark, "
					+ " m.memberName as memberName,m.academyNo as academyNo ,"
					+ " m.phone_mobile as phoneMobile,m.phone_home as phoneHome , "
					+"	date_format(rcb.create_date,'%Y-%m-%d %H:%i:%s') as createTime "
					+ " from restaurant_customer_booking rcb left join "
					+ " (select m.customer_Id as customerId, m.academy_No as academyNo,concat(cp.salutation,' ',cp.given_name,' ',cp.surname) as memberName ,cp.phone_mobile,cp.phone_home from member m join customer_profile cp on m.customer_Id = cp.customer_Id)as m "
					+ " on m.customerId = rcb.customer_Id left join "
					+ " restaurant_master rm on rm.restaurant_id = rcb.restaurant_id where 1=1 ");
			if (!StringUtils.isEmpty(restaurantId) && !"ALL".equalsIgnoreCase(restaurantId)) {
				sql.append(" and rcb.restaurant_id = '" + restaurantId + "' ");
				if (customerId != null && customerId > 0)
					sql.append(" and rcb.customer_id = " + customerId);
			}
			if (null != advanceQueryDto && null != advanceQueryDto.getRules() && advanceQueryDto.getRules().size() > 0
					&& advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
				List<SearchRuleDto> rule = advanceQueryDto.getRules();
				boolean blean = false;
				if (null != rule && rule.size() > 0) {
					for (SearchRuleDto searchRuleDto : rule) {
						if (searchRuleDto.getField().startsWith("date")) {
							blean = true;
							break;
						}
					}
				}
				if (isHistory) {
					// when is history is true and advanceQueryDto need booktime
					// to search to use advanceQueryDto need booktime search
					if (!blean) {
						sql.append(" and date(rcb.book_Time) <CURRENT_DATE()");
					}
				} else {
					if (!blean) {
						sql.append(" and date(rcb.book_Time) >=CURRENT_DATE()");
					}
				}
				AdvanceQueryDto queryDto = advanceQueryDto;
				queryDto.setSortBy(advanceQueryDto.getSortBy());
				queryDto.setAscending(advanceQueryDto.getAscending());
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				responseResult =  advanceQueryService.getAdvanceQueryResultBySQL(queryDto, sql.toString() + " and ", page,
						RestaurantCustomerBookingDto.class);
				
			} else {
				// no advance condition search
				if (isHistory) {
					sql.append(" and date(rcb.book_Time) <CURRENT_DATE()");
				} else {
					sql.append(" and date(rcb.book_Time) >=CURRENT_DATE()");
				}
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				if(null!=advanceQueryDto){
					queryDto.setSortBy(advanceQueryDto.getSortBy());
					queryDto.setAscending(advanceQueryDto.getAscending());
				}
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				
				responseResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, sql.toString(), page,
						RestaurantCustomerBookingDto.class);
			}
			this.setPrefix();
			return responseResult;
		} catch (Exception e) {
			logger.error("Error querying restaurant reservations", e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
			return responseResult;
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/restaurants/{restaurantId}/reservations", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult createRestaurantCustomerBooking(@PathVariable(value = "restaurantId") String restaurantId,
			@RequestBody RestaurantCustomerBookingDto bookingDto) {
		try {
			if (StringUtils.isEmpty(restaurantId) || null == bookingDto || bookingDto.getCustomerId() == null
					|| null == bookingDto.getBookTime() || null == bookingDto.getPartySize()
					|| bookingDto.getPartySize() == 0) {
				responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
				return responseResult;
			} else if (!StringUtils.isEmpty(bookingDto.getRemark()) && bookingDto.getRemark().length() > 300) {
				responseResult.initResult(GTAError.RestaurantError.REMARK_TOO_LONG);
				return responseResult;
			} else if (DateCalcUtil.parseDateTime(bookingDto.getBookTime()).before(new Date())) {
				responseResult.initResult(GTAError.RestaurantError.BOOKINGTIME_LESSTHAN_CURRENTTIME);
				return responseResult;
			} else if (DateCalcUtil.parseDate(bookingDto.getBookTime()).after(getFutureMonth(new Date(), 3))) {
				responseResult.initResult(GTAError.RestaurantError.BOOKINGTIME_OVER_ADVANCETIME);
				return responseResult;
			}
			/**
			 * 	
			 * SGG-3755
			//checkFacilityRight
			UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(
					bookingDto.getCustomerId(),UsageRightCheckService.FacilityCode.GUEST,CommUtil.toDate(bookingDto.getBookTime().substring(0, 10), "yyyy-MM-dd"));
			if (!facilityRight.isCanBook()) {
				
				if(!usageRightCheckService.checkRenewedFacilityRight(bookingDto.getCustomerId(), UsageRightCheckService.FacilityCode.GUEST)){
					responseResult.initResult(GTAError.FacilityError.NO_PERMISSION);
					return  responseResult;
				}
				
			}
			 */
			bookingDto.setRestaurantId(restaurantId);
			boolean checkAvilable = false;
			// memberprotal check partySize available
			if (bookingDto.getCheckAvailable()) {
				// memberprotal check partySize available
				checkAvilable = restaurantOpenHourService.isAvailableRestaurant(restaurantId,
						Integer.valueOf(bookingDto.getPartySize().toString()),
						DateCalcUtil.parseDateTime(bookingDto.getBookTime()), Constant.RESTAURANT_BOOK_VIA_FD);
			}
			if ((!bookingDto.getCheckAvailable() && !checkAvilable)
					|| (bookingDto.getCheckAvailable() && checkAvilable)) {
				responseResult = restaurantCustomerBookingService.saveRestaurantCustomerBooking(getUser().getUserId(),
						bookingDto);
				try {
					if (GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())) {
						RestaurantCustomerBookingDto result = (RestaurantCustomerBookingDto) responseResult.getData();
						restaurantCustomerBookingService.sendRestaurantBookingSMS(result.getResvId().longValue(),
								Constant.TEMPLATE_ID_RESTAURANT_BOOK_REQUEST);
						restaurantCustomerBookingService.sendRestaurantBookingMail(result.getResvId(),
								Constant.TEMPLATE_ID_RESTAURANT_BOOK_REQUEST_EMAIL);
						restaurantCustomerBookingService.sendNotificationToRestaurantTablet(result.getResvId());
					}
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			} else {
				responseResult.initResult(GTAError.RestaurantError.PARTYSIZE_EXCEED_QUOTA);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return responseResult;
	}

	@RequestMapping(value = "/restaurants/{restaurantId}/reservations/{resvId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult cancelRestaurantCustomerBooking(@PathVariable(value = "resvId") Long resvId) {
		try {
			responseResult = restaurantCustomerBookingService.cancelRestaurantCustomerBooking(getUser().getUserId(),
					resvId);
			try {
				if (GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())) {
					RestaurantCustomerBookingDto result = (RestaurantCustomerBookingDto) responseResult.getData();
					restaurantCustomerBookingService.sendRestaurantBookingSMS(result.getResvId().longValue(),
							Constant.TEMPLATE_ID_RESTAURANT_BOOK_CANCELLED);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/restaurants/{restaurantId}/reservations/{resvId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantCustomerBooking(@PathVariable(value = "resvId") Long resvId) {
		if (null == resvId) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		RestaurantCustomerBookingDto rcb = restaurantCustomerBookingService
				.getRestaurantCustomerBookingByResvId(resvId);
		responseResult.initResult(GTAError.Success.SUCCESS, rcb);
		return responseResult;
	}

	@RequestMapping(value = "/restaurants/{restaurantId}/reservations/{resvId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateRestaurantCustomerBooking(@PathVariable(value = "resvId") Long resvId,
			@RequestBody RestaurantCustomerBookingDto bookingDto) {
		try {
			if (null == resvId || null == bookingDto || null == bookingDto.getBookTime()
					|| null == bookingDto.getPartySize()) {
				responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
				return responseResult;
			} else if (!StringUtils.isEmpty(bookingDto.getRemark()) && bookingDto.getRemark().length() > 300) {
				responseResult.initResult(GTAError.RestaurantError.REMARK_TOO_LONG);
				return responseResult;
			} else if (DateCalcUtil.parseDateTime(bookingDto.getBookTime()).before(new Date())) {
				responseResult.initResult(GTAError.RestaurantError.BOOKINGTIME_LESSTHAN_CURRENTTIME);
				return responseResult;
			} else if (DateCalcUtil.parseDate(bookingDto.getBookTime()).after(getFutureMonth(new Date(), 3))) {
				responseResult.initResult(GTAError.RestaurantError.BOOKINGTIME_OVER_ADVANCETIME, new Object[] { 3 });
				return responseResult;
			}
			bookingDto.setResvId(resvId);
			responseResult = restaurantCustomerBookingService.updateRestaurantCustomerBooking(getUser().getUserId(),
					bookingDto);
			try {
				if (GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())) {
					RestaurantCustomerBookingDto result = (RestaurantCustomerBookingDto) responseResult.getData();
					restaurantCustomerBookingService.sendRestaurantBookingSMS(result.getResvId().longValue(),
							Constant.TEMPLATE_ID_RESTAURANT_BOOK_UPDATED);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return responseResult;
	}

	@RequestMapping(value = "/restaurants/{restaurantId}/reservations/available", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult available(@PathVariable(value = "restaurantId") String restaurantId,
			@RequestParam Integer partySize, @RequestParam String bookTime) {
		if (StringUtils.isEmpty(restaurantId) || StringUtils.isEmpty(bookTime)) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try {
			// boolean flag =
			// restaurantOpenHourService.isAvailableRestaurant(restaurantId,
			// partySize,
			// DateCalcUtil.parseDateTime(bookTime),
			// Constant.RESTAURANT_BOOK_VIA_FD);
			// responseResult.initResult(GTAError.Success.SUCCESS, flag);
			return restaurantOpenHourService.checkBookingQuotaPeriod(restaurantId, partySize,
					DateCalcUtil.parseDateTime(bookTime), Constant.RESTAURANT_BOOK_VIA_FD);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.Success.SUCCESS, false);
		}
		return responseResult;
	}

	@RequestMapping(value = "/member/restaurants/{restaurantId}/reservations/available", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult availableMemberBooking(@PathVariable(value = "restaurantId") String restaurantId,
			@RequestParam Integer partySize, @RequestParam String bookTime) {
		if (StringUtils.isEmpty(restaurantId) || StringUtils.isEmpty(bookTime)) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try {
			boolean flag = restaurantOpenHourService.isAvailableRestaurant(restaurantId, partySize,
					DateCalcUtil.parseDateTime(bookTime), Constant.RESTAURANT_BOOK_VIA_OL);
			
			
			responseResult.initResult(GTAError.Success.SUCCESS, flag);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.RestaurantError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/restaurants/changeStatus/{resvId}/{status}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult changeReservationStatus(@PathVariable(value = "resvId") Long resvId,
			@PathVariable(value = "status") String status) {
		try {
			return restaurantCustomerBookingService.updateRestaurantCustomerBooking(getUser().getUserId(), resvId,
					status);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.RestaurantError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	public Date getFutureMonth(Date date, int month) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, month);
		return DateCalcUtil.parseDate(DateCalcUtil.formatDate(cal.getTime()));
	}
	
	private void setPrefix() throws Exception{
		Resource resource = new ClassPathResource("/placeholder/route_entity_map.properties");
		Properties properties = PropertiesLoaderUtils.loadProperties(resource);
		if(responseResult.getListData().getList() != null && responseResult.getListData().getList().size() != 0){
			for(Object object : responseResult.getListData().getList()){
				RestaurantCustomerBookingDto dto = (RestaurantCustomerBookingDto)object;
				dto.setPrefix(properties.getProperty("restaurant" + dto.getRestaurantId() + ".prefix"));
			}
		}
	}
	public static void main(String[] args) {
		try {
			System.out.println(DateCalcUtil.parseDateTime("2016-06-15+11:30:00"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
