package com.sinodynamic.hkgta.controller.crm.backoffice.presentation;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.FolderInfoDto;
import com.sinodynamic.hkgta.entity.crm.PresentMaterial;
import com.sinodynamic.hkgta.service.crm.backoffice.presentation.PresentationFolderOperationService;
import com.sinodynamic.hkgta.service.crm.backoffice.presentation.UploadPresentMaterialService;
import com.sinodynamic.hkgta.service.crm.sales.presentation.PresentMaterialService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.CustomObjectMapper;
import com.sinodynamic.hkgta.util.MaterialUploadFilter;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.exception.ErrorCodeException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class PresentMaterialController extends ControllerBase<PresentMaterial> {
	@Autowired
	UploadPresentMaterialService		uploadPresentMaterialService;
	@Autowired
	PresentationFolderOperationService	service;

	@Autowired
	PresentationFolderOperationService	presentationFolderOperationService;

	@Autowired
	private MaterialUploadFilter		materialUploadFilter;
	@Autowired
	PresentMaterialService				presentMaterialService;

	String								JPEG_EXT	= ".jpeg";
	String								PNG_EXT		= ".png";

	String								M4V_EXT		= ".m4v";
	String								MP4_EXT		= ".mp4";
	String								MOV_EXT		= ".mov";
	String								AVI_EXT		= ".avi";

	@RequestMapping(value = "/presentation/delete_media", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseMsg deleteMedia(@RequestParam("ids") String ids) {
		try {
			String userId = this.getUser().getUserId();
			String[] idsStr = ids.split(",");
			Long[] idsLong = new Long[idsStr.length];
			for (int i = 0; i < idsStr.length; i++) {
				idsLong[i] = Long.valueOf(idsStr[i]);
			}
			this.uploadPresentMaterialService.deleteMedia(idsLong, userId);
			return ResponseMsgConstant.SUCCESS_MSG;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseMsg msg = new ResponseMsg();
			msg.setReturnCode(ResponseMsgConstant.ERRORCODE);
			msg.setErrorMessageEN(e.getMessage());
			return msg;
		}
	}

	@RequestMapping(value = "/presentation/recover_media", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg recoverMedia(@RequestParam("ids") String ids) {
		try {
			String userId = this.getUser().getUserId();
			String[] idsStr = ids.split(",");
			Long[] idsLong = new Long[idsStr.length];
			for (int i = 0; i < idsStr.length; i++) {
				idsLong[i] = Long.valueOf(idsStr[i]);
			}
			this.uploadPresentMaterialService.recoverMedia(idsLong, userId);
			return ResponseMsgConstant.SUCCESS_MSG;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseMsg msg = new ResponseMsg();
			msg.setReturnCode(ResponseMsgConstant.ERRORCODE);
			msg.setErrorMessageEN(e.getMessage());
			return msg;
		}
	}

	@RequestMapping(value = "/presentation/view_folder/{folderId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult viewFolder(@PathVariable(value = "folderId") Long folderId,
			@RequestParam(value = "terminal", defaultValue = "saleskit") String terminal) {
		try {
			FolderInfoDto result = this.uploadPresentMaterialService.viewFolder(folderId, terminal);
			return new ResponseResult(ResponseMsgConstant.SUCCESS_MSG, result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseResult error = new ResponseResult();
			error.setReturnCode(ResponseMsgConstant.ERRORCODE);
			error.setErrorMessageEN(e.getMessage());
			return error;
		}
	}

	@RequestMapping(value = "/presentation/view_trash_folder", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult viewTrashFolder(@RequestParam(value = "terminal", defaultValue = "saleskit") String terminal) {
		try {
			FolderInfoDto result = this.uploadPresentMaterialService.viewFolder(null, terminal);
			return new ResponseResult(ResponseMsgConstant.SUCCESS_MSG, result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseResult error = new ResponseResult();
			error.setReturnCode(ResponseMsgConstant.ERRORCODE);
			error.setErrorMessageEN(e.getMessage());
			return error;
		}
	}

	@RequestMapping(value = "/presentation/update_description", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg updateDescription(@RequestParam("material_id") Long materialId, @RequestParam("description") String description) {
		try {
			String userId = this.getUser().getUserId();
			uploadPresentMaterialService.updateDescription(materialId, description, userId);
			return ResponseMsgConstant.SUCCESS_MSG;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseMsg msg = new ResponseMsg();
			msg.setReturnCode(ResponseMsgConstant.ERRORCODE);
			msg.setErrorMessageEN(e.getMessage());
			return msg;
		}

	}

	@RequestMapping(value = "/presentation/get_thumbnail_pic", method = RequestMethod.GET)
	@ResponseBody
	public void getThumbnailAsByteStream(@RequestParam("material_id") Long materialId, @RequestParam("media_type") String mediaType, HttpServletResponse response) {
		try {
			byte[] dataStream = uploadPresentMaterialService.getMediaAsByteStream(materialId, mediaType, true);
			response.getOutputStream().write(dataStream);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}
	
	@RequestMapping(value = "/presentation/get_thumbnail", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getThumbnailAsByteStream(@RequestParam("material_id") Long materialId, @RequestParam("media_type") String mediaType) {
		try {
			return uploadPresentMaterialService.getMediaAsByteStream(materialId, mediaType, true);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new byte[0];
		}

	}

	@RequestMapping(value = "/presentation/get_media", method = RequestMethod.GET)
	@ResponseBody
	public void getMediaAsByteStream(@RequestParam("material_id") Long materialId, @RequestParam("media_type") String mediaType,
			HttpServletResponse resp,HttpServletRequest request) {

		try {
			// Content-Length:1124814
			// Content-Range:bytes 0-1124813/1124814
			resp.setHeader("Cache-Control", "max-age=21600");

			PresentMaterial material = presentMaterialService.getById(materialId);
			Long f = material.getFolderId();
			FolderInfoDto folderInfo = presentationFolderOperationService.getFolderInfo(f);
			String fileSever_prefix = super.appProps.getProperty("image.server.prefix");
			String pathName = "/" + Constant.MEDIA_FILE_LINK_IN + folderInfo.getFolderParentPath().replace(fileSever_prefix, "") + "/" + folderInfo.getFolderId();

			String materialName = material.getMaterialFilename();
			String filePath = pathName + "/" + materialName;
			
			String fileSuffix = materialName.substring(materialName.lastIndexOf(".")).toLowerCase();
		
			if (JPEG_EXT.equals(fileSuffix)) {
				resp.setContentType("image/jpeg");
			} else if (PNG_EXT.equals(fileSuffix)) {
				resp.setContentType("image/png");
			} else if (M4V_EXT.equals(fileSuffix)) {
				if(responseForIos(resp, request, filePath)) return;
				// resp.setContentType("video/x-m4v");
				// resp.setHeader("Accept-Ranges", "bytes");
				System.out.println("* /presentation/get_media redirect to " + pathName + "/" + materialName);
				resp.sendRedirect(filePath); 
				return;
			} else if (MP4_EXT.equals(fileSuffix)) {
				if(responseForIos(resp, request, filePath)) return;
				
				/*
				 * resp.setContentType("video/mp4"); resp.setHeader("Accept-Ranges", "bytes"); resp.setHeader("Content-Length", "1124814"); // test
				 * only resp.setHeader("Content-Range", "bytes 0-1124813/1124814"); // test only
				 */

				System.out.println("** /presentation/get_media redirect to " + pathName + "/" + materialName);
				resp.sendRedirect(filePath); 
				return;

			} else if (MOV_EXT.equals(fileSuffix)) {
				if(responseForIos(resp, request, filePath)) return;
				// resp.setContentType("video/quicktime");
				// resp.setHeader("Accept-Ranges", "bytes");
				// resp.setHeader("Content-Length", "1124814");
				// resp.setHeader("Content-Range", "bytes 0-1124813/1124814");
				System.out.println("* /presentation/get_media redirect to " + pathName + "/" + materialName);
				resp.sendRedirect(filePath); 
				return;
			} else if (AVI_EXT.equals(fileSuffix)) {
				if(responseForIos(resp, request, filePath)) return;
				System.out.println("* /presentation/get_media redirect to " + pathName + "/" + materialName);
				resp.sendRedirect(filePath); 
				return;
			}

			resp.getOutputStream().write(uploadPresentMaterialService.getMediaAsByteStream(materialId, mediaType, false));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	private boolean responseForIos(HttpServletResponse response,HttpServletRequest request, String filePath){
		String loginDevice = CommUtil.getAccessDevice(request.getHeader("User-Agent"));//web for Windows
		if(loginDevice != null && (loginDevice.contains("UnKnown") || loginDevice.contains("iPad") || loginDevice.contains("Mac"))){
			String mediaUrl = request.getRequestURL().toString().replace(request.getRequestURI(), "")+filePath;
			responseResult.initResult(GTAError.Success.SUCCESS);
			responseResult.setData(mediaUrl);
			try {
				response.setContentType("application/json");
				response.getOutputStream().write((new CustomObjectMapper().writeValueAsString(responseResult)).getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	@RequestMapping(value = "/presentation/get_media4pc", method = RequestMethod.GET)
	@ResponseBody
	public void getMedia4pcAsByteStream(@RequestParam("material_id") Long materialId, @RequestParam("media_type") String mediaType,
			HttpServletResponse resp) {

		try {
			resp.setHeader("Cache-Control", "max-age=21600");

			PresentMaterial material = presentMaterialService.getById(materialId);

			String materialName = material.getMaterialFilename();
			String fileSuffix = materialName.substring(materialName.lastIndexOf(".")).toLowerCase();
			if (JPEG_EXT.equals(fileSuffix)) {
				resp.setContentType("image/jpeg");
			} else if (PNG_EXT.equals(fileSuffix)) {
				resp.setContentType("image/png");
			} else if (M4V_EXT.equals(fileSuffix)) {
				resp.setContentType("video/x-m4v");
				resp.setHeader("Accept-Ranges", "bytes");
			} else if (MP4_EXT.equals(fileSuffix)) {
				resp.setContentType("video/mp4");
				resp.setHeader("Accept-Ranges", "bytes");
			} else if (MOV_EXT.equals(fileSuffix)) {
				resp.setContentType("video/quicktime");
				resp.setHeader("Accept-Ranges", "bytes");
			} else if (AVI_EXT.equals(fileSuffix)) {
				resp.setContentType("video/x-msvideo");
				resp.setHeader("Accept-Ranges", "bytes");
			}

			resp.getOutputStream().write(uploadPresentMaterialService.getMediaAsByteStream(materialId, mediaType, false));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/presentation/upload_media", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMsg uploadMedia(@RequestParam("folderId") final Long folderId, @RequestParam("file") final MultipartFile[] files,
			final @RequestParam(value = "materialDesc", required = false) String[] materialDesc) {
		try {
			// for(MultipartFile file : files){
			// String originalName = file.getOriginalFilename();
			// this.materialUploadFilter.filter(originalName);
			// }
			String userId = this.getUser().getUserId();
			uploadPresentMaterialService.uploadMedia(folderId, materialDesc, files, new String[files.length], userId);
			responseMsg.initResult(GTAError.Success.SUCCESS);
			return responseMsg;
		} catch (ErrorCodeException e) {
			logger.error(e.getMessage(), e);
			this.responseMsg.initResult(e.getErrorCode(), e.getArgs());
			return this.responseMsg;
		}

	}
}
