package com.sinodynamic.hkgta.controller.pms;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.pms.xml.input.changeroom.HTNGHotelRoomMoveNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.checkin.HTNGHotelCheckInNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.checkout.HTNGHotelCheckOutNotifRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.epayment.HTNGChargePostingRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.facility.OTAGolfCourseAvailRQ;
import com.sinodynamic.hkgta.dto.pms.xml.input.membership.HTNGReadRQ;
import com.sinodynamic.hkgta.dto.pms.xml.output.changroom.HTNGHotelRoomMoveNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.checkin.HTNGHotelCheckInNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.checkout.HTNGHotelCheckOutNotifRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.epayment.HTNGChargePostingRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.facility.OTAGolfCourseAvailRS;
import com.sinodynamic.hkgta.dto.pms.xml.output.membership.HTNGProfileReadRS;
import com.sinodynamic.hkgta.service.pms.PMSRequestProcessorService;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.LoggerType;

@Controller
@RequestMapping(value="/pmsprocessor")
public class PMSRequestProcessorController extends ControllerBase{
	
	@Value(value ="#{apiProperties['checkIn.rootElemnt.name']}")
	private  String HTNG_HotelCheckInNotifRQ; 
	
	@Value(value ="#{apiProperties['changeRoom.rootElemnt.name']}")
	private  String HTNG_HotelRoomMoveNotifRQ;
	
	@Value(value ="#{apiProperties['checkOut.rootElemnt.name']}")
	private  String HTNG_HotelCheckOutNotifRQ;
	
	@Value(value ="#{apiProperties['membership.rootElemnt.name']}")
    private String HTNG_ReadRQ;
	
	@Value(value ="#{apiProperties['querybundle.rootElemnt.name']}")
    private String OTA_GolfCourseAvailRQ;
	
	@Value(value ="#{apiProperties['epayment.rootElemnt.name']}")
    private String HTNG_ChargePostingRQ;
	
	@Resource(name="jaxbMarshaller")
	private Jaxb2Marshaller jaxbMarshaller ;
	
	@Autowired
	private PMSRequestProcessorService pmsRequestProcessorService;
	
//	private static Logger logger = LoggerFactory.getLogger(PMSRequestProcessorController.class);
	private Logger	logger	= LoggerFactory.getLogger(LoggerType.PMS.getName());
	
	
	private String convertXmlBean2Str(Object xmlBean) {
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		jaxbMarshaller.marshal(xmlBean, result);
		return sw.toString();
	}
	
	private String  errorProcess(String errorMsg,String exceptionMsg,String errorCode) {
		com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
		error.setErrorMsg(errorMsg);
		error.setExceptionMsg(exceptionMsg);
		error.setErrorCode(errorCode);
		return convertXmlBean2Str(error);
	}
	
	
	private String checkIn(String payload) {
		HTNGHotelCheckInNotifRS  response =null;
		try{
			StringReader sr = new StringReader(payload);
			StreamSource source = new StreamSource(sr);
			HTNGHotelCheckInNotifRQ request = (HTNGHotelCheckInNotifRQ)jaxbMarshaller.unmarshal(source);
			response = pmsRequestProcessorService.checkIn(request);
		} catch(Exception e) {
			response = new HTNGHotelCheckInNotifRS();
			com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
			error.setErrorCode("009");
			error.setErrorMsg("The program exception happend.");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			error.setExceptionMsg(sw.toString());
			response.setError(error);
		}
		return convertXmlBean2Str(response);
	}
	
	private String changeRoom(String payload) {
		//added by Kaster 20160506 打印日志以便后台跟踪执行过程。
		HTNGHotelRoomMoveNotifRS response = null;
		try{
			StringReader sr = new StringReader(payload);
			StreamSource source = new StreamSource(sr);
			HTNGHotelRoomMoveNotifRQ request = (HTNGHotelRoomMoveNotifRQ)jaxbMarshaller.unmarshal(source);
			response = pmsRequestProcessorService.changeRoom(request);
			
		} catch(Exception e) {
			response = new HTNGHotelRoomMoveNotifRS();
			com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
			error.setErrorCode("009");
			error.setErrorMsg("The program exception happend.");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
//			logger.error(sw.toString());
			error.setExceptionMsg(sw.toString());
			response.setError(error);
		}
		return convertXmlBean2Str(response);
	}
	
	private String checkOut(String payload) {
		HTNGHotelCheckOutNotifRS response = null;
		try{
			StringReader sr = new StringReader(payload);
			StreamSource source = new StreamSource(sr);
			HTNGHotelCheckOutNotifRQ request = (HTNGHotelCheckOutNotifRQ)jaxbMarshaller.unmarshal(source);
			response = pmsRequestProcessorService.checkOut(request);
		}catch(Exception e) {
			response = new HTNGHotelCheckOutNotifRS();
			com.sinodynamic.hkgta.dto.pms.xml.Error error = new com.sinodynamic.hkgta.dto.pms.xml.Error();
			error.setErrorCode("009");
			error.setErrorMsg("The program exception happend.");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			logger.error(sw.toString());
			error.setExceptionMsg(sw.toString());
			response.setError(error);
		}
		
		return convertXmlBean2Str(response);
	}
	
	private String membership(String payload) {
		HTNGProfileReadRS response = null;
		try {
			StringReader sr = new StringReader(payload);
			StreamSource source = new StreamSource(sr);
			HTNGReadRQ request = (HTNGReadRQ)jaxbMarshaller.unmarshal(source);
			response = pmsRequestProcessorService.membership(request);
			
		}catch(Exception e) {
			response = new HTNGProfileReadRS();
			com.sinodynamic.hkgta.dto.pms.xml.output.membership.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Errors();
			List<com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error> error = new ArrayList<com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error>();
			com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error error1 = new com.sinodynamic.hkgta.dto.pms.xml.output.membership.Error();
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
//			logger.error(sw.toString());
			error1.setCode(BigInteger.valueOf(425));
			error1.setType(BigInteger.valueOf(3));
			error1.setContent("The program exception happend.\r\n" + sw.toString());
			error.add(error1);
			errors.setError(error);
			response.setErrors(errors);
			
		}
		
		return convertXmlBean2Str(response);
	}
	
	
	private String queryBundle(String payload) {
		OTAGolfCourseAvailRS response = null;
		try {
			StringReader sr = new StringReader(payload);
			StreamSource source = new StreamSource(sr);
			OTAGolfCourseAvailRQ request = (OTAGolfCourseAvailRQ)jaxbMarshaller.unmarshal(source);
			
			response = pmsRequestProcessorService.queryBundle(request);
			
		}catch(Exception e) {
			response = new OTAGolfCourseAvailRS();
			com.sinodynamic.hkgta.dto.pms.xml.output.facility.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.Errors();
			List<com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error> errorList = new ArrayList();
			com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.facility.Error();
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			logger.error(sw.toString());
			error.setCode(BigInteger.valueOf(425));
			error.setType(BigInteger.valueOf(3));
			error.setContent("The program exception happend.\r\n" + sw.toString());
			errorList.add(error);
			errors.setError(errorList);
			response.setErrors(errors);
		}
		return convertXmlBean2Str(response);
	}
	
	
	private String epayment(String payload) {
		HTNGChargePostingRS response = new HTNGChargePostingRS();
		try {
			StringReader sr = new StringReader(payload);
			StreamSource source = new StreamSource(sr);
			HTNGChargePostingRQ request = (HTNGChargePostingRQ)jaxbMarshaller.unmarshal(source);
			
			response = pmsRequestProcessorService.epayment(request);
			
		}catch(Exception e) {
			com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors errors = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Errors();
			com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error error = new com.sinodynamic.hkgta.dto.pms.xml.output.epayment.Error();
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
//			logger.error(sw.toString());
			error.setCode(BigInteger.valueOf(425));
			error.setType(BigInteger.valueOf(3));
			error.setContent("The program exception happend.\r\n" + sw.toString());
			errors.setError(error);
			response.setErrors(errors);
		}
			
		
		return convertXmlBean2Str(response);
	}
	
	/**
	 * This method support form request which send a parameter value, also support ajax request whose request header include "Content-Type:application/xml" 
	 * @param request
	 * @param requestBody
	 * @return
	 */
	@RequestMapping(value = "/dispather",produces=MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public String dispather(HttpServletRequest  request,@RequestBody String requestBody) {
		String payload;
		String responseXMl = "";
		Enumeration<String> parameterNames = request.getParameterNames();
		
		if (parameterNames.hasMoreElements()) {
			String parameterName = parameterNames.nextElement();
			payload = request.getParameter(parameterName);
		} else {
			payload = requestBody;
		}
		
		logger.info("/dispather:requestBody="+requestBody+" payload="+payload);
		
		String rootElementName;
		StringReader sr = new StringReader(payload);
		
		SAXReader reader = new SAXReader();
		Document doc = null;
		try {
			doc = reader.read(sr);
		} catch (DocumentException e) {
			StringWriter sw = new StringWriter();
		    PrintWriter pw = new PrintWriter(sw);
		    e.printStackTrace(pw);
		    pw.flush();
		    pw.close();
//		    logger.error(sw.toString());
			responseXMl = errorProcess("Invalid xml input parameter format error",sw.toString(),null);
//			logger.error("Invalid xml input parameter format error");
//			logger.error(sw.toString());
			logger.error(Log4jFormatUtil.logErrorMessage(null,null,null,responseXMl,e.getMessage()));
			
			return responseXMl;
		}
		Element root = doc.getRootElement();
		rootElementName  = root.getName();
		
		if (StringUtils.isNotBlank(rootElementName)) {
			if(HTNG_HotelCheckInNotifRQ.equalsIgnoreCase(rootElementName)) {
				responseXMl= checkIn(payload);
			} else if (HTNG_HotelRoomMoveNotifRQ.equalsIgnoreCase(rootElementName)) {
				//added by Kaster 20160506 打印日志以便后台跟踪执行过程。
//				logger.info("before run changeRoom method......");
				responseXMl= changeRoom(payload);
			} else if(HTNG_HotelCheckOutNotifRQ.equalsIgnoreCase(rootElementName)){
				responseXMl= checkOut(payload);
			} else if(HTNG_ReadRQ.equalsIgnoreCase(rootElementName)) {
				responseXMl= membership(payload);
			} else if(OTA_GolfCourseAvailRQ.equalsIgnoreCase(rootElementName)){
				responseXMl= queryBundle(payload);
			} else if(HTNG_ChargePostingRQ.equalsIgnoreCase(rootElementName)){
				responseXMl= epayment(payload);
			} else{
				responseXMl = errorProcess("Invalid input parameter",null,null);
//				logger.error("The root element name" + rootElementName + "is not valid.");
				String msg=" The root element name" + rootElementName + "is not valid.";
				logger.error(Log4jFormatUtil.logErrorMessage(null,null,null,responseXMl,msg));
			}
		} else {
			responseXMl = errorProcess("Input parameter is blank",null,null);
			String msg=" The root element name" + rootElementName + "is not valid.";
			logger.error(Log4jFormatUtil.logErrorMessage(null,null,null, responseXMl,msg));
		}
		logger.info(Log4jFormatUtil.logInfoMessage(null,null,null,responseXMl));
		
		return responseXMl;
	}
}
