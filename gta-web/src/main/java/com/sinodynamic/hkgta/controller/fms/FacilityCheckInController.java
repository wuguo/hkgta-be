package com.sinodynamic.hkgta.controller.fms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.fms.FacilityCheckInDto;
import com.sinodynamic.hkgta.dto.fms.FacilityCheckInRequestDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.fms.FacilityCheckInService;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

/**
 * @author Mason_Yang
 *
 */
@Controller
public class FacilityCheckInController extends ControllerBase<FacilityMaster> {
	private Logger zoomtechLog = Logger.getLogger(LoggerType.ZOOMTECH.getName()); 
	@Autowired
	private FacilityCheckInService	facilityMasterService;

	/**
	 * get ball Feeding Machine List
	 * 
	 * @return facility list
	 */
	@RequestMapping(value = "/facility/check-in/{resvId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkIn(@PathVariable(value = "resvId") long resvId) {

		FacilityCheckInDto<?> facilityCheckInDto = facilityMasterService.getCheckAvailability(resvId, false);
		responseResult.initResult(GTAError.FacilityError.SUCCESS, facilityCheckInDto);
		return responseResult;
	}

	/**
	 * get ball Feeding Machine List
	 * 
	 * @return facility list
	 */
	@RequestMapping(value = "/member/check-in/{resvId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult memberCheckIn(@PathVariable(value = "resvId") long resvId) {

		FacilityCheckInDto<?> facilityCheckInDto = facilityMasterService.getCheckAvailability(resvId, false);
		responseResult.initResult(GTAError.FacilityError.SUCCESS, facilityCheckInDto);
		return responseResult;
	}

	/**
	 * 
	 * Return Checked In Facilities for Web Portal/Member App
	 * 
	 * @author Liky_Pan
	 * @return Checked In Facilities for Member
	 */
	@RequestMapping(value = "/member/checkedin/facilities/{resvId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCheckedInFacilitiesForMember(@PathVariable(value = "resvId") long resvId) {
		try{
			return facilityMasterService.getFacilitiesVenueByResvId(resvId);
		}catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.FacilityError.UNEXPECTED_EXCEPTION));
			return responseResult;
		}
	}

	/**
	 * get ball Feeding Machine List
	 * 
	 * @return facility list
	 */
	@RequestMapping(value = "/member/check-in", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult memberCheckIn(HttpServletRequest request,@RequestBody FacilityCheckInRequestDto requestDto) {
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), JSONObject.fromObject(requestDto).toString(), RequestMethod.POST.name(),null));
		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			facilityMasterService.checkIn(requestDto, userId, false);
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.FacilityError.UNEXPECTED_EXCEPTION));
			
			zoomtechLog.error(Log4jFormatUtil.logErrorMessage(null, null, null,JSONObject.fromObject(responseResult).toString(),e.getMessage()));
			return responseResult;
		}
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(null, null,null,JSONObject.fromObject(responseResult).toString()));
		return responseResult;
	}

	/**
	 * get ball Feeding Machine List
	 *
	 * @return facility list
	 */
	@RequestMapping(value = "/facility/check-in/allotment/{resvId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityAllotment(@PathVariable(value = "resvId") long resvId) {

		try {
			Map<String, List<FacilityMasterDto>> result = new HashMap<>();
			List<FacilityMasterDto> facilityMasterDtoList = facilityMasterService.getFacilityAllotment(resvId);
			result.put("list", facilityMasterDtoList);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, result);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.FacilityError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	/**
	 * get ball Feeding Machine List
	 * 
	 * @return facility list
	 */
	@RequestMapping(value = "/facility/check-in", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult checkIn(HttpServletRequest request,@RequestBody FacilityCheckInRequestDto requestDto) {
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), JSONObject.fromObject(requestDto).toString(), RequestMethod.POST.name(),null));
		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			facilityMasterService.checkIn(requestDto, userId, false);
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.FacilityError.UNEXPECTED_EXCEPTION));
			zoomtechLog.error(Log4jFormatUtil.logErrorMessage(null, null, null,JSONObject.fromObject(responseResult).toString(),e.getMessage()));
			return responseResult;
		}
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(null, null,null,JSONObject.fromObject(responseResult).toString()));
		return responseResult;
	}
}
