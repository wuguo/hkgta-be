package com.sinodynamic.hkgta.controller.dashboard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.dashboard.ServicePlanSalesPercentageDto;
import com.sinodynamic.hkgta.dto.dashboard.StaffTurnoverDto;
import com.sinodynamic.hkgta.service.adm.BiMemberUsageRateService;
import com.sinodynamic.hkgta.service.dashboard.AdmissionReportsService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 为staff portal页面的BI Dashboard菜单返回统计数据，以便前端根据统计数据制作统计图。
 * @author Kaster 20160413
 *
 */
@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/bidashboard")
public class AdmissionReportsController extends ControllerBase{
	
	@Autowired
	private AdmissionReportsService admissionReportsService;
	
	@Autowired
	private BiMemberUsageRateService biMemberUsageRateService;

	/**
	 * 该接口返回的统计数据是各个在职员工的累计销售总额，按销售额从大到小排序。
	 * SQL查询结果是两列：第一列是staff id，第二列是销售额。
	 */
	@RequestMapping(value = "/salesdistribution", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult querySalesAmount(){
		List<StaffTurnoverDto> list = admissionReportsService.querySalesAmount();
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}
	
	/**
	 * 该接口查询各个在职员工的累计销售总额、经手的客户数，以及所有在职员工一共的销售总额以及所有的客户总数。
	 * topNum是数字，表示前台传递过来的要取的前几条记录。如果不传，则查询所有。
	 * staffId是员工的ID，如果不传，则默认查询销售业绩最好的员工的数据。
	 */
	@RequestMapping(value = "/rankingofsalesman", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult querySalesAmountRanking(@RequestParam(value="topNum",required=false) Integer topNum,
			@RequestParam(value="staffId",required=false) String staffId){
		Map<String,Object> map = admissionReportsService.querySalesAmountRanking(topNum,staffId);
		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}
	
	/**
	 * 该接口查询每个service plan的销售总数、销售额以及分别占所有service plan销售总数和销售总额的百分比。
	 */
	@RequestMapping(value = "/serviceplansales", method = {RequestMethod.GET})
	@ResponseBody
	public ResponseResult queryServicePlanSalesPercentage(){
		List<ServicePlanSalesPercentageDto> list = admissionReportsService.queryServicePlanSalesPercentage();
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}
	
	/**
	 * 该接口查询每个月注册会员数、Accumulated Admission Revenue和Accumulated Operational Revenue
	 */
	@RequestMapping(value = "/admissionAndOperationalRevenue", method = {RequestMethod.GET})
	@ResponseBody
	public ResponseResult queryAdmAndOpeAccRevenue(@RequestParam(value="startYearMonth",required=false) String startYearMonth,
			@RequestParam(value="endYearMonth",required=false) String endYearMonth){
		Map<String, Object> map = admissionReportsService.queryAdmAndOpeAccRevenue(startYearMonth,endYearMonth);
		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}
	
	/**
	 * 该接口查询Admission Reports菜单对应统计页面顶部的五项数据
	 */
	@RequestMapping(value = "/activePatronAndAdmissionRelated", method = {RequestMethod.GET})
	@ResponseBody
	public ResponseResult queryActivePatronAndAdmissionRelated(){
		Map<String, Object> map = admissionReportsService.queryActivePatronAndAdmissionRelated();
		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}
	/***
	 * BI facilities Occupancy
	 * @return
	 */
	@RequestMapping(value = "/facilitiesOccupancy", method = {RequestMethod.GET})
	@ResponseBody
	public ResponseResult facilitiesOccupancy(@RequestParam(value="year") String year){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		try {
			 simpleDateFormat.parse(year);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.SysError.SYSTEM_USER_OPERATION_ERROR, e.getMessage());
			return responseResult;
		}
		responseResult.initResult(GTAError.Success.SUCCESS,admissionReportsService.queryFacilityOccupancy(year));
		return responseResult;
	}
	
	@RequestMapping(value = "/applicationUsageNumber", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult applicationUsageNumber(@RequestParam(value="year") String year,@RequestParam(value="type",defaultValue="A") String type)
	{
	   return biMemberUsageRateService.getBiMemberUsageRateListByYear(year,type);
	}
	@RequestMapping(value = "/patronAnalysis", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult patronAnalysis()
	{
		return biMemberUsageRateService.getPatronAnalysisData();
	}
	
	/**
	 * 获取年度活跃用户数
	 * @param year
	 * @return
	 */
	@RequestMapping(value = "/activeUsersNumber", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult activeUsersNumber(@RequestParam(value="year") String year)
	{
	   return biMemberUsageRateService.getActiveUsersNumber(year);
	}
	
	/**
	 * 获取信用卡支付类型数据
	 * @param year
	 * @return
	 */
	@RequestMapping(value = "/creditCardPayTypeData", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult creditCardPayTypeData()
	{
		responseResult.initResult(GTAError.Success.SUCCESS,admissionReportsService.creditCardPayTypeData());
		return responseResult;
	}
	
	/**
	 * 获取topup频率
	 * @param year
	 * @return
	 */
	@RequestMapping(value = "/getTopupFrequency", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult  getTopupFrequency (@RequestParam(value="year") String year)
	{
	   return admissionReportsService.getTopupFrequency(year);
	}
	
	
	/**
	 * 获取每月CashValue平衡柱形图分布
	 * @param year
	 * @return
	 */
	@RequestMapping(value = "/getMonthlyCashValueBalanceDistribution", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult  getMonthlyCashValueBalanceDistribution (@RequestParam(value="year") String year)
	{
	   return admissionReportsService.getMonthlyCashValueBalanceDistribution(year);
	}
	
	/**
	 * 获取每月topup数量分布
	 * @param year
	 * @return
	 */
	@RequestMapping(value = "/getMonthlyTopupAmountDistribution", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult  getMonthlyTopupAmountDistribution (@RequestParam(value="year") String year)
	{
	   return admissionReportsService.getMonthlyTopupAmountDistribution(year);
	}
	
	/**
	 * 获取每月CashValue平衡饼状图分布
	 * @param year
	 * @return
	 */
	@RequestMapping(value = "/getMonthlyCashValueBalanceMultidimensionalDistribution", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult  getMonthlyCashValueBalanceMultidimensionalDistribution (@RequestParam(value="year") String year)
	{
	   return admissionReportsService.getMonthlyCashValueBalanceMultidimensionalDistribution(year);
	}
	
	@RequestMapping(value = "/revenueReports", method = {RequestMethod.GET})
	public @ResponseBody ResponseResult revenueReports(@RequestParam(value="startDate") String startDate,@RequestParam(value="endDate") String endDate)
	{
	   return biMemberUsageRateService.getRevenueReportsDataRange(startDate, endDate);
	}
	
	
}
