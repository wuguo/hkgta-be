package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberQuickSearchService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/membership")
public class QuickSearchMemberController extends ControllerBase {

	@Autowired
	MemberQuickSearchService		memberQuickSearchService;

	@Autowired
	private CustomerProfileService	customerProfileService;

	@Autowired
	private MemberService			memberService;
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;

	@RequestMapping(value = "/manualSearch/{number}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult quickSeachMember(@PathVariable("number") String number) {
		try {
			responseResult=memberQuickSearchService.quickSeachMember(number);
			/***
			 * add kiosk input cardNo (SGG-3673)
			 */
			if(GTAError.MemberError.MEMBER_NOT_FOUND.getCode().equals(responseResult.getReturnCode()))
			{
				Member member=memberService.getMemberByCardNo(number);
				if(null!=member){
					responseResult=memberQuickSearchService.quickSeachMember(member.getAcademyNo());
				}else{
					responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
				}
			}
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("QuickSearchMemberController.quickSeachMember Exception", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/profile/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult displayQuickSeachMemberProfile(@PathVariable("customerID") @EncryptFieldInfo Long customerID, @RequestParam(value = "superiorMemberId",required = false) @EncryptFieldInfo Long superiorMemberId,
			@RequestParam(value = "closeAccount",required = false) String closeAccount, @RequestParam(value = "importStatus",required = false) String importStatus) {
		logger.info("QuickSearchMemberController.displayQuickSeachMemberProfile start ...");
		try {
			if("true".equalsIgnoreCase(closeAccount)){
				responseResult = this.customerProfileService.archiveMember(customerID, getUser().getUserId(),
						"NACT",getUser().getFullname()==null?getUser().getUserName():getUser().getFullname());
				if(!GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())){
					return responseResult;
				}
			}
			CustomerProfile cp = null;
			if(null!=superiorMemberId){
				 cp = customerProfileService.getCustomerByID(customerID, superiorMemberId);
			}else{
				 cp = customerProfileService.getByCustomerID(customerID);
			}
			//如果passportNo中有"-",表明是archive member
			if("true".equalsIgnoreCase(importStatus) && cp.getPassportNo().contains("-")){
				this.customerEnrollmentService.mainBodySeparate(cp);
			}
			responseResult.initResult(GTAError.Success.SUCCESS, cp);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/membertype/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberType(@PathVariable("customerID") @EncryptFieldInfo Long customerID) {
		logger.info("QuickSearchMemberController.getMemberType start ...");
		try {
			String memberType = memberService.getMemberTypeByCustomerId(customerID);
			responseResult.initResult(GTAError.Success.SUCCESS);
			responseResult.setData(memberType);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
}
