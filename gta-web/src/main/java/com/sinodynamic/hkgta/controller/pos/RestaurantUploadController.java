package com.sinodynamic.hkgta.controller.pos;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class RestaurantUploadController extends ControllerBase<RestaurantMaster> {

	@RequestMapping(value = "/restaurant/uploadAsset", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult upload(HttpServletRequest request, @RequestParam("file") MultipartFile file) throws IOException {

		try {
			String saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.RESTAURANT);
			
			File serverFile = new File(saveFilePath);
			saveFilePath = serverFile.getName();
			Map<String, String> imgPathMap = new HashMap<String, String>();
			imgPathMap.put("fileName", saveFilePath);
			responseResult.initResult(GTAError.Success.SUCCESS, imgPathMap);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
			return responseResult;
		}

		return responseResult;
	}
}
