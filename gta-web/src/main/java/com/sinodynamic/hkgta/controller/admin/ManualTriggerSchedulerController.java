package com.sinodynamic.hkgta.controller.admin;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.mms.SpaMemberSync;
import com.sinodynamic.hkgta.integration.spa.request.UpdateGuestRequest;
import com.sinodynamic.hkgta.integration.spa.response.UpdateGuestResponse;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.scheduler.service.AutoActivateEnrollMembershipTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoCloseCourseTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoCountSurveyTimesTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoInactiveExpiredNewsTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoPushMessage4CoachTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoPushMessage4SilentCheckinService;
import com.sinodynamic.hkgta.scheduler.service.AutoPushNoticeToPatronTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoRemindMemberAndCaochInAdvance1HourTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoRemindRoomAssistantTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoRetrieveFinancialFromSpaTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoStatisticsActivationInactiveMembersTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoUpdateRestaurantReservationTaskService;
import com.sinodynamic.hkgta.scheduler.service.AutoUpdateRoomStatusTaskService;
import com.sinodynamic.hkgta.scheduler.service.CarparkDataImportTaskService;
import com.sinodynamic.hkgta.scheduler.service.CheckExpiredIndividualMemberTaskService;
import com.sinodynamic.hkgta.scheduler.service.CheckHelperPassTypeExpireTaskService;
import com.sinodynamic.hkgta.scheduler.service.CourseAbsentTaskService;
import com.sinodynamic.hkgta.scheduler.service.CourseReminderTaskService;
import com.sinodynamic.hkgta.scheduler.service.DDAJobTaskService;
import com.sinodynamic.hkgta.scheduler.service.DaypassCardTaskService;
import com.sinodynamic.hkgta.scheduler.service.DownloadCarParkMemberTaskService;
import com.sinodynamic.hkgta.scheduler.service.ExtensionLetterSendingTaskService;
import com.sinodynamic.hkgta.scheduler.service.FacilityBookingEndRemindService;
import com.sinodynamic.hkgta.scheduler.service.FacilityBookingSMSNotificationService;
import com.sinodynamic.hkgta.scheduler.service.GeneratePatronAccountStatementTaskService;
import com.sinodynamic.hkgta.scheduler.service.GolfBayCheckInTaskService;
import com.sinodynamic.hkgta.scheduler.service.GtaPublicHolidayTaskService;
import com.sinodynamic.hkgta.scheduler.service.ImportRestaurantDataTaskService;
import com.sinodynamic.hkgta.scheduler.service.MemberSynchronizeTaskService;
import com.sinodynamic.hkgta.scheduler.service.OnlinePaymentCheckTaskService;
import com.sinodynamic.hkgta.scheduler.service.ReservationBookingEndRemindService;
import com.sinodynamic.hkgta.scheduler.service.ServicePlanTaskService;
import com.sinodynamic.hkgta.scheduler.service.SpaAppointmentsSynchronizeTaskService;
import com.sinodynamic.hkgta.scheduler.service.StaffDisposeTaskService;
import com.sinodynamic.hkgta.scheduler.service.StartCheckServerTaskService;
import com.sinodynamic.hkgta.scheduler.service.StartRenewServiceAccountTaskService;
import com.sinodynamic.hkgta.scheduler.service.StatisticalEachActiveMemberTaskService;
import com.sinodynamic.hkgta.scheduler.service.UpdateEnrollmentStatusTaskService;
import com.sinodynamic.hkgta.scheduler.service.UpdateTempPassStatusTaskService;
import com.sinodynamic.hkgta.service.adm.UserRecordActionLogService;
import com.sinodynamic.hkgta.service.bi.BiOasisFlexFileService;
import com.sinodynamic.hkgta.service.crm.account.DDITaskService;
import com.sinodynamic.hkgta.service.crm.membercash.VirtualAccountTaskService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.mms.MemberSynchronizeService;
import com.sinodynamic.hkgta.service.mms.SpaAppointmentRecService;
import com.sinodynamic.hkgta.service.mms.SynchronizeSpaData;
import com.sinodynamic.hkgta.service.pms.RoomStatusScheduleService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.service.sys.AlarmEmailService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import io.swagger.annotations.ApiParam;
import net.coobird.thumbnailator.Thumbnailator;

@Controller
@RequestMapping("/sys")
public class ManualTriggerSchedulerController extends ControllerBase {

	private final Logger flexLogger = Logger.getLogger(LoggerType.OASISFLEXFILE.getName());
	private Logger ddxLogger = Logger.getLogger(LoggerType.DDX.getName());

	@Autowired
	private StartCheckServerTaskService startCheckServerTaskService;
	@Autowired
	private GtaPublicHolidayTaskService gtaPublicHolidayTaskService;

	@Autowired
	private ExtensionLetterSendingTaskService extensionLetterSendingTaskService;

	@Autowired
	private VirtualAccountTaskService virtualAccTopupTaskService;

	@Autowired
	private OnlinePaymentCheckTaskService onlinePaymentCheckTaskService;

	@Autowired
	private UpdateEnrollmentStatusTaskService updateEnrollmentStatusTaskService;

	@Autowired
	private AutoCloseCourseTaskService autoCloseCourseTaskService;

	@Autowired
	private AutoInactiveExpiredNewsTaskService autoInactiveExpiredNewsTaskService;

	@Autowired
	private CheckHelperPassTypeExpireTaskService checkHelperPassTypeExpireTaskService;

	@Autowired
	private CheckExpiredIndividualMemberTaskService checkExpiredIndividualMemberTaskService;

	@Autowired
	private StartRenewServiceAccountTaskService startRenewServiceAccountTaskService;

	@Autowired
	private UpdateTempPassStatusTaskService updateTempPassStatusTaskService;

	@Autowired
	private AutoUpdateRoomStatusTaskService autoUpdateRoomStatusTaskService;

	@Autowired
	private AutoRemindRoomAssistantTaskService autoRemindRoomAssistantTaskService;

	@Autowired
	private FacilityBookingSMSNotificationService facilityBookingSMSNotificationService;

	@Autowired
	private FacilityBookingEndRemindService facilityBookingEndRemindService;

	@Autowired
	private DownloadCarParkMemberTaskService downloadCarParkMemberTaskService;

	@Autowired
	private AutoActivateEnrollMembershipTaskService autoActivateEnrollMembershipTaskService;

	@Autowired
	private GeneratePatronAccountStatementTaskService generatePatronAccountStatementTaskService;

	@Autowired
	private ImportRestaurantDataTaskService importRestaurantDataTaskService;

	@Autowired
	private DaypassCardTaskService daypassCardTaskService;

	@Autowired
	private ServicePlanTaskService taskService;

	@Autowired
	private CourseReminderTaskService courseReminderTaskService;

	@Autowired
	private ReservationBookingEndRemindService reservationBookingEndRemindService;

	@Autowired
	private StaffDisposeTaskService staffDisposeTaskService;

	@Autowired
	private CourseAbsentTaskService courseAbsentTaskService;

	@Autowired
	private AutoRemindMemberAndCaochInAdvance1HourTaskService autoRemindCoachTaskService;

	@Autowired
	private AutoPushMessage4CoachTaskService service;

	@Autowired
	private AutoPushMessage4SilentCheckinService SilentCheckinService;

	@Autowired
	private SynchronizeSpaData synchronizeSpaData;

	@Autowired
	private SpaAppointmentsSynchronizeTaskService spaAppointmentsSynchronizeTaskService;

	@Autowired
	private SpaAppointmentRecService spaAppointmentRecService;

	@Autowired
	private MemberSynchronizeTaskService memberSynchronizeTaskService;

	@Autowired
	private AutoUpdateRestaurantReservationTaskService autoUpdateRestaurantReservationTaskService;

	@Autowired
	private GolfBayCheckInTaskService golfBayCheckInTaskService;

	@Autowired
	private DDAJobTaskService ddaTaskService;
	@Autowired
	private AlarmEmailService alarmEmailService;
	@Autowired
	private DDITaskService ddiTaskService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private CustomerOrderTransService customerOrderTransService;

	@Autowired
	private RoomStatusScheduleService roomStatusScheduleService;

	@Autowired
	private UserRecordActionLogService userRecordActionLogService;

	@Autowired
	private CustomerProfileService customerProfileService;

	@Autowired
	private AutoStatisticsActivationInactiveMembersTaskService autoStatisticsActivationInactiveMembersTaskService;

	@Autowired
	private StatisticalEachActiveMemberTaskService statisticalEachActiveMemberTaskService;

	@Autowired
	private BiOasisFlexFileService biOasisFlexFileService;

	@Autowired
	private CustomerEmailContentService customerEmailContentService;

	@Autowired
	private AutoCountSurveyTimesTaskService autoCountSurveyTimesTaskService;

	@Autowired
	private AutoPushNoticeToPatronTaskService autoPushNoticeToPatronTaskService;

	@Autowired
	private AutoRetrieveFinancialFromSpaTaskService autoRetrieveFinancialFromSpaTaskService;

	@Autowired
	private CarparkDataImportTaskService carparkDataImportTaskService;

	@Autowired
	private SpaServcieManager spaServcieManager;
	
	@Autowired
	MemberSynchronizeService memberSynchronizeService;

	@Value(value = "#{oasisProperties['oasis.flex.path']}")
	private String oasisFlexPath;

	@RequestMapping(value = "/startCheckServerTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult startCheckServerTask() {
		try {
			startCheckServerTaskService.check();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/gtaPublicHolidayTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult gtaPublicHolidayTask() {
		try {
			if (!gtaPublicHolidayTaskService.execute()) {
				gtaPublicHolidayTaskService.reTry();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/extensionLetterSendingTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult extensionLetterSendingTask() {
		try {
			extensionLetterSendingTaskService.autoSendingJob();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/virtualAccTopupTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult virtualAccTopupTask() {
		try {
			virtualAccTopupTaskService.downloadVirtualAccountTransactionExtract();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/onlinePaymentCheckTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult onlinePaymentCheckTask() {
		try {
			onlinePaymentCheckTaskService.checkOnlinePaymentStatus();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/updateEnrollmentStatusTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult updateEnrollmentStatusTask() {
		try {
			updateEnrollmentStatusTaskService.updateEnrollmentStatusJob();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoInactiveExpiredNewsTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoInactiveExpiredNewsTask() {
		try {
			autoInactiveExpiredNewsTaskService.updateExpiredNewsToInactive();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoCloseCourseTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoCloseCourseTask() {
		try {
			autoCloseCourseTaskService.closeCourseOnRegistDueDate();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/checkHelperPassTypeExpireTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkHelperPassTypeExpireTask() {
		try {
			checkHelperPassTypeExpireTaskService.autoDailyRun();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/checkExpiredIndividualMemberTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkExpiredIndividualMemberTask() {
		try {
			checkExpiredIndividualMemberTaskService.updateExpiredIndividualMemberStatus();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/startRenewServiceAccountTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult startRenewServiceAccountTask() {
		try {
			startRenewServiceAccountTaskService.startRenewServiceAccount();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/updateTempPassStatusTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult updateTempPassStatusTask() {
		try {
			updateTempPassStatusTaskService.closeCourseOnRegistDueDate();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoUpdateRoomStatusTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoUpdateRoomStatusTask() {
		try {
			autoUpdateRoomStatusTaskService.autoUpdateRoomStatus();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoRemindRoomAssistantTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoRemindRoomAssistantTask() {
		try {
			autoRemindRoomAssistantTaskService.autoRemindRoomAssistant();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/facilityBookingSMSNotificationTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult facilityBookingSMSNotificationTask() {
		try {
			facilityBookingSMSNotificationService.pushMessageNotification();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/facilityBookingEndRemindTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult facilityBookingEndRemindTask() {
		try {
			facilityBookingEndRemindService.pushMessageNotification();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/downloadCarParkMemberTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult downloadCarParkMemberTask() {
		try {
			downloadCarParkMemberTaskService.DownloadCarParkMemberList();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/verifyReturnCarParkMemberTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult verifyReturnCarParkMemberTask() {
		try {
			downloadCarParkMemberTaskService.VerifyReturnCarParkMemberList();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoActivateEnrollMembershipTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoActivateEnrollMembershipTask() {
		try {
			autoActivateEnrollMembershipTaskService.AutoActivateEnrollMembership();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/generatePatronAccountStatementTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult generatePatronAccountStatementTask(
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try {
				simpleDateFormat.parse(startDate);
				simpleDateFormat.parse(endDate);
			} catch (ParseException e) {
				logger.error(e.getMessage(), e);
				throw e;
			}
			generatePatronAccountStatementTaskService.generatePatronAccountStatement(startDate, endDate);
			responseResult.initResult(GTAError.Success.SUCCESS);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/importRestaurantDataTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult importRestaurantDataTask() throws Exception {
		try {
			importRestaurantDataTaskService.importRestaurant();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/daypassCardTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult daypassCardTask() {
		try {
			daypassCardTaskService.autoACTDaypassPermitCard();

			daypassCardTaskService.autoDisposeDaypassCard();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/servicePlanTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult servicePlanTask() {
		try {
			taskService.autoDeactivateServicePlanAndDP();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/courseReminderTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult courseReminderTask() {
		try {
			courseReminderTaskService.sendReminderMsg();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/courseSessionReminderTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult courseSessionReminderTask() {
		try {
			courseReminderTaskService.sendAppReminderMsg();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/reservationBookingEndRemindTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult reservationBookingEndRemindTask() {
		try {
			reservationBookingEndRemindService.pushMessageNotification();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/courseAbsentTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult courseAbsentTask() {
		try {
			courseAbsentTaskService.autoChangeCourseAttendanceToAbsent();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoRemindMemberAndCaochInAdvance1HourTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoRemindMemberAndCaochInAdvance1HourTask() {
		try {
			autoRemindCoachTaskService.autoRemindInAdvance1Hour();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	// @RequestMapping(value = "/autoPushMessage4CoachTask", method =
	// RequestMethod.GET)
	// @ResponseBody
	// public ResponseResult autoPushMessage4CoachTask() {
	// try {
	// service.autoPushMsgInAdvance2Day();
	// } catch (Exception e) {
	// logger.error(e.getMessage(), e);
	// throw e;
	// }
	// responseResult.initResult(GTAError.Success.SUCCESS);
	// return responseResult;
	// }

	@RequestMapping(value = "/autoPushMessage4SilentCheckinTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoPushMessage4SilentCheckinTask() {
		try {
			SilentCheckinService.autoPushMsg4SilentCheckin();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/spaAppointmentsSynchronizeTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult spaAppointmentsSynchronizeTask() {
		try {
			// synchronizeSpaData.synchronizeAppointments();
			spaAppointmentsSynchronizeTaskService.excuteSynchAppointments();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/spaPaymentsSynchronizeTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult spaPaymentsSynchronizeTask() {
		try {
			// synchronizeSpaData.synchronizePayments();
			spaAppointmentsSynchronizeTaskService.excuteSynchPaymentments();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/spaAppointmentBeginNotificationTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult spaAppointmentBeginNotificationTask() throws Exception {
		try {
			spaAppointmentRecService.spaAppointmentBeginNotification();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/memberSynchronizeTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult memberSynchronizeTask() {
		try {
			memberSynchronizeTaskService.memberSynchronize();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoUpdateRestaurantReservationTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoUpdateRestaurantReservationTask() {
		try {
			autoUpdateRestaurantReservationTaskService.autoUpdateReservationStatus();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/golfBayCheckInTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult golfBayCheckInTask() {
		try {
			golfBayCheckInTaskService.openGolfMachine();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/ddaReporterTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult ddaReporterTask() throws Exception {
		try {
			ddaTaskService.excute();
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			alarmEmailService.sendAlarmEmailTask("DDAReporterTask", e);
			ddxLogger.error(e.getMessage(), e);
			throw e;
		}
		return responseResult;
	}

	@RequestMapping(value = "/ddiRejectReporterTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult ddiRejectReporterTask() throws Exception {
		try {
			ddiTaskService.parseDDIRejectReporter();
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			alarmEmailService.sendAlarmEmailTask("ddiRejectReporterTask", e);
			ddxLogger.error(e.getMessage(), e);
			throw e;
		}
		return responseResult;
	}

	@RequestMapping(value = "/generateDDIReporterTask/{date}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult generateDDIReporterTask(@ApiParam(value = "yyyy-MM") @PathVariable("date") String date)
			throws Exception {
		try {
			Date time = null;
			if (StringUtils.isEmpty(date)) {
				time = new Date();
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				try {
					String postfix = DateConvertUtil.parseDate2String(new Date(), "HH:mm:ss");
					time = simpleDateFormat.parse(date + "-01 " + postfix);
				} catch (ParseException e) {
					logger.error(e.getMessage(), e);
					throw e;
				}
			}
			ddiTaskService.generateDDIReport(time);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			ddxLogger.error(e.getMessage(), e);
			alarmEmailService.sendAlarmEmailTask("DDAReporterTask", e);
			throw e;
		}
		return responseResult;
	}

	// @RequestMapping(value = "/autoInactiveAdultDependentTask", method =
	// RequestMethod.GET)
	// @ResponseBody
	// public ResponseResult autoInactiveAdultDependentTask() {
	// try {
	// memberService.AdultDependentSendEamil();
	// } catch (Exception e) {
	// logger.error(e.getMessage(), e);
	// throw e;
	// }
	// responseResult.initResult(GTAError.Success.SUCCESS);
	// return responseResult;
	// }

	@RequestMapping(value = "/autoPushEmail4GoingToAdultMemberTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoPushEmail4GoingToAdultMemberTask() {
		try {
			memberService.pushEmail4GoingToAdultMember();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoPushEmail4ExpiringMemberTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoPushEmail4ExpiringMemberTask() {
		try {
			memberService.pushEmail4ExpiringMember();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/generateSinoFlexFile/{dateTime}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult generateSinoFlexFile(@PathVariable("dateTime") String dateTime) {
		try {
			Date time = null;
			if (StringUtils.isEmpty(dateTime)) {
				time = new Date();
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				try {
					time = simpleDateFormat.parse(dateTime);
				} catch (ParseException e) {
					logger.error(e.getMessage(), e);
					throw e;
				}
			}
			customerOrderTransService.createFlexPaymentReport(time);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/generateManualStatement", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult generateManualStatement(
			@RequestParam(value = "year", defaultValue = "", required = true) String year,
			@RequestParam(value = "month", defaultValue = "", required = true) String month) throws Exception {
		try {
			customerOrderTransService.manualGenerateStatement(year, month);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/autoUpdateRoomStatus/{date}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoUpdateRoomStatus(@PathVariable("date") String date) throws Exception {
		try {
			Date time = null;
			if (StringUtils.isEmpty(date)) {
				time = new Date();
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				try {
					time = simpleDateFormat.parse(date);
				} catch (ParseException e) {
					logger.error(e.getMessage(), e);
					throw e;
				}
			}
			roomStatusScheduleService.autoUpdateRoomStatus(time);
			responseResult.initResult(GTAError.Success.SUCCESS);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}

		return responseResult;
	}

	@RequestMapping(value = "/autoSettingRoomStatus/{date}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoSettingRoomStatus(@PathVariable("date") String date) throws Exception {
		try {
			Date time = null;
			if (StringUtils.isEmpty(date)) {
				time = new Date();
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				try {
					time = simpleDateFormat.parse(date);
				} catch (ParseException e) {
					logger.error(e.getMessage(), e);
					throw e;
				}
			}
			roomStatusScheduleService.autoSettingRoomStatus(time);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/createSmallImage", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult createSmallImage() throws Exception {
		try {
			String basePath = appProps.getProperty(FileUpload.FileCategory.USER.getConfigName());
			File file = new File(basePath + File.separator);
			this.setSmallImage(file);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/createDailyPatronProfileActivity/{date}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult createDailyPatronProfileActivity(@PathVariable("date") String date,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setHeader("Content-Disposition", "attachment; filename=" + "test" + ".pdf");
		response.setContentType("application/pdf");
		try {
			Date dateTime = DateConvertUtil.parseString2Date(date, "yyyy-MM-dd");
			String pdfPath = userRecordActionLogService.excute(dateTime);
			if (!StringUtils.isEmpty(pdfPath)) {
				// send mail
				userRecordActionLogService.sendEmailActivityReport(pdfPath);
			}
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/createPhotoSignatureBase64", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult createPhotoSignatureBase64() {
		try {
			customerProfileService.createBase64Image();
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/autoStatisticsActivationInactiveMembers", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoStatisticsActivationInactiveMembers() {
		try {
			autoStatisticsActivationInactiveMembersTaskService.autoStatisticsActivationInactiveMembers();
			;
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/statisticalEachActiveMembers", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult statisticalEachActiveMembers() {
		try {
			statisticalEachActiveMemberTaskService.statisticalEachActiveMembers();
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/importOasisFlexFile/{date}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult importOasisFlexFile(@PathVariable("date") String date) throws Exception {

		try {
			Date time = null;
			if (StringUtils.isEmpty(date)) {
				time = new Date();
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				try {
					time = simpleDateFormat.parse(date);
				} catch (ParseException e) {
					logger.error(e.getMessage(), e);
					throw e;
				}
			}
			flexLogger.info("manual import oasis flex file start");
			Date originalDate = DateConvertUtil.parseString2Date(DateCalcUtil.getDateBefore(time, 1), "yyyy-MM-dd");
			String filePath = oasisFlexPath + DateConvertUtil.formatCurrentDate(originalDate) + "F.txt";
			File file = new File(filePath);
			if (file.exists()) {
				flexLogger.info("find file :" + filePath);
				if (biOasisFlexFileService.excuteImportFile(filePath, time)) {
					responseResult.initResult(GTAError.Success.SUCCESS);
				} else {
					responseResult.initResult(GTAError.Success.SUCCESS,
							"no data import ,please look atimportOasisFlexFile.log");
				}
			} else {
				responseResult.initResult(GTAError.Success.SUCCESS, " File not found: " + filePath);
			}

		} catch (Exception e) {
			flexLogger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION,
					" happend system Exception please look at importOasisFlexFile..");
		}
		flexLogger.info("manual import oasis flex file end.........");
		return responseResult;
	}

	@RequestMapping(value = "/autoSendMail", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoSendMail() throws Exception {
		customerEmailContentService.autoSendMail();
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;

	}

	private void setSmallImage(File f) {
		if (f.isDirectory()) {
			File[] files = f.listFiles();
			if (null != files && files.length > 0) {
				for (File cuFile : files) {
					setSmallImage(cuFile);
				}
			}
		} else {
			if (f.getName().indexOf("small") == -1) {
				createThumbnail(f);
			}
		}
	}

	private void createThumbnail(File file) {
		String absolutePath = file.getAbsolutePath();
		String[] s = absolutePath.split("\\.");
		File thumbnailFile = new File(s[0] + "_small" + "." + s[1]);
		int width = Integer.valueOf(appProps.getProperty("thumbnail.length"));
		int length = Integer.valueOf(appProps.getProperty("thumbnail.width"));
		try {
			Thumbnailator.createThumbnail(file, thumbnailFile, length, width);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/autoSurveyCount", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoSurveyCount() {
		try {
			autoCountSurveyTimesTaskService.excute();
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;

	}

	@RequestMapping(value = "/autoSendNotice/{date}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult autoSendNotice(@PathVariable("date") String date) {
		try {
			Date time = null;
			if (StringUtils.isEmpty(date)) {
				time = new Date();
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				try {
					time = simpleDateFormat.parse(date);
				} catch (ParseException e) {
					logger.error(e.getMessage(), e);
					throw e;
				}
			}
			autoPushNoticeToPatronTaskService.pushNotice(time);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;

	}

	@RequestMapping(value = "/generateMMSFlexFile", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult generateMMSFlexFile(@RequestParam(value = "date", required = true) String date) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try {
				simpleDateFormat.parse(date);
			} catch (ParseException e) {
				logger.error(e.getMessage(), e);
				throw e;
			}
			autoRetrieveFinancialFromSpaTaskService.excute(date, date);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;

	}

	@RequestMapping(value = "/carParkInterfaceImport", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult carParkInterfaceImport() {

		try {
			carparkDataImportTaskService.excute();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/clearSpaMember", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult clearSpaMember() {
		try {
			List<SpaMemberSync>members=memberSynchronizeService.getAllSpaMemberSynchThenGuestIdIsNotNull();
			ExecutorService pool = Executors.newFixedThreadPool(10);
			if(null!=members&&members.size()>0)
			{
				for(SpaMemberSync spa:members)
				{
					final String spaGuestId=spa.getSpaGuestId();
					final String customerId=spa.getCustomerId().toString();
					final Long sysId=spa.getSysId();
					Thread th=new Thread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							UpdateGuestRequest updateGuestRequest = new UpdateGuestRequest();
							updateGuestRequest.setGuestId(spaGuestId);
							updateGuestRequest.setGuestCode("");
							updateGuestRequest.setGender("");
							updateGuestRequest.setFirstName("");
							updateGuestRequest.setLastName("");
							updateGuestRequest.setEmail("");
							updateGuestRequest.setMobilePhone("");
							updateGuestRequest.setAddress1("");
							updateGuestRequest.setAddress2("");
							updateGuestRequest.setCheckEmailPhoneExists("false");
							UpdateGuestResponse response = spaServcieManager.updateGuest(updateGuestRequest);
							if(response.isSuccess())
							{
								//update db status "Your contact information has been updated."
								memberSynchronizeService.updateSpaMemberSync(sysId, "【"+Thread.currentThread().getName()+"】 excuted the member information has been clear.");
								logger.info("send clear member information to Spa the customer:"+customerId+" update status: "+response.isSuccess());
							}else{
								logger.error("fail : clear member information to Spa the customer:"+customerId+" update status: "+response.isSuccess());
							}
							
						}
					});
					pool.execute(th);
				}
				responseResult.initResult(GTAError.Success.SUCCESS);
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS,"No find need Synch member to Spa side.. ");	
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

}
