package com.sinodynamic.hkgta.controller.common;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.scheduler.service.GtaPublicHolidayTaskService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
@RequestMapping(value = "/publicHolidays")
public class GtaPublicHolidayController extends ControllerBase<CustomerProfile> {

	private Logger	logger	= Logger.getLogger(GtaPublicHolidayController.class);

	@RequestMapping(value = "/tiggerPublicHoildayTask", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult tiggerPublicHoildayTask() {
		try {

			logger.debug("GatPublicHolidayTask  run at:" + new Date().toString());
			WebApplicationContext applicationContext = ContextLoader.getCurrentWebApplicationContext();
			GtaPublicHolidayTaskService service = applicationContext.getBean(GtaPublicHolidayTaskService.class);
			if (!service.execute()) {
				if (!service.reTry()) {
					responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
					return responseResult;
				} else {
					responseResult.initResult(GTAError.Success.SUCCESS);
					return responseResult;
				}
			} else {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}

		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("GtaPublicHolidayController.viewNoteDetail Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
}