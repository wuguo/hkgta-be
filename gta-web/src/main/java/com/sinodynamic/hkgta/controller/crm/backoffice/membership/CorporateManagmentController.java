package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.ActivateMemberDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CorporateProfileDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.DropDownDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.membership.SearchSpendingSummaryDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.adm.UserRecordActionLogService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.CorporateMemberService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.CorporateService;
import com.sinodynamic.hkgta.service.crm.membercash.MemberCashvalueService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.StaffProfileService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.TokenUtils;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.UserRecordActionType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/membership/corporate")
@Scope("prototype")
public class CorporateManagmentController extends ControllerBase{
 
	private Logger logger = Logger.getLogger(CorporateManagmentController.class);
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private CorporateService corporateService;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private CorporateMemberService corporateMemberService;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private MemberCashvalueService memberCashvalueService;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;
	
    @Autowired
    private ServletContext servletContext;
    
    @Autowired
    private UserMasterService userMasterService;
    
    //added by Kaster 20160509
    @Autowired
	private StaffProfileService staffProfileService;
    
    @Autowired
    private UserRecordActionLogService userRecordActionLogService;
    
    /**
     * added by Kaster 20160509
     * SGG-1715:Implement "assign to" some sale person to Corporate account
     * 获取salesman的下拉列表
     */
    @RequestMapping(value = "/salesname/{salesStaffId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllSalesMan(@PathVariable(value = "salesStaffId") String salesStaffId) {
		try {

			List<StaffProfile> StaffProfileList = staffProfileService.searchAllStaffIdsForAssignTo(salesStaffId);
			if (StaffProfileList == null || StaffProfileList.size() == 0) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}

			List<DropDownDto> retData = new ArrayList<DropDownDto>();
			for (StaffProfile sp : StaffProfileList) {

				DropDownDto dto = new DropDownDto();
				dto.setCodeValue(sp.getUserId());
				dto.setCodeDisplay(sp.getGivenName() + " " + sp.getSurname());
				retData.add(dto);
			}

			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
    
    @RequestMapping(value = "/assignTo/{corporateId}/{salesStaffId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult assignToSalesMan(@PathVariable(value = "corporateId") Long corporateId, @PathVariable(value = "salesStaffId") String salesStaffId) {
    	if(corporateId==null || salesStaffId==null || corporateId.toString().equals("") || salesStaffId.equals("")){
    		responseResult.initResult(GTAError.CommonError.PARAMETER_MISSING, "Please input corporateId and salesStaffId");
    		return responseResult;
    	}
   		return corporateService.assignToSalesMan(corporateId,salesStaffId);
    }
    
	/**
	 * Method to register/edit a corporate account
	 * @param corporateDto
	 * @return Response Result
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public ResponseResult createCorporateAcc(
			@RequestBody CorporateProfileDto corporateDto) {
		try {
			String loginUserId = getUser().getUserId();
			corporateDto.setLoginUserId(loginUserId);
			return corporateService.createCorporateAcc(corporateDto);
		} catch (GTACommonException gta) {
			logger.error("CorporateManagmentController.createCorporateAcc Exception",gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch (Exception e) {
			logger.error("CorporateManagmentController.createCorporateAcc Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * Method to view a corporate account
	 * @param corporateId
	 * @return Response Result
	 */
	@ResponseBody
	@RequestMapping(value = "/{corporateId}", method = RequestMethod.GET)
	public ResponseResult viewCorporateAcc(@PathVariable Long corporateId) {
		try {
			return corporateService.viewCorporateAcc(corporateId);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.viewCorporateAcc Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * Method to edit a corporate account
	 * @param corporateId
	 * @return Response Result
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseResult editCorporateAcc(@RequestBody CorporateProfileDto corporateDto) {
		try {
			String loginUserId = getUser().getUserId();
			corporateDto.setLoginUserId(loginUserId);
			corporateDto.setLoginUserName(getUser().getUserName());
			return corporateService.editCorporateAcc(corporateDto);
		} catch (GTACommonException gta) {
			//gta.printStackTrace();
			logger.error("CorporateManagmentController.editCorporateAcc Exception",gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch(HibernateOptimisticLockingFailureException concurrent) {
			//concurrent.printStackTrace();
			logger.error("CorporateManagmentController.editCorporateAcc Exception", concurrent);
			responseResult.initResult(GTAError.CommonError.BEEN_UPDATED_OR_DELETED, new String[]{"corporate account"});
			return responseResult;
			
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.editCorporateAcc Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * Method to remove/delete a corporate account
	 * @param corporateId
	 * @return Response Result
	 */
	@ResponseBody
	@RequestMapping(value = "/changeStatus", method = RequestMethod.PUT)
	public ResponseResult deactivateCorporateAcc(@RequestBody CorporateProfileDto dto) {
		try {
			String loginUserId = getUser().getUserId();
			String userName = getUser().getUserName();
			if (dto.getStatus().equalsIgnoreCase("ACT")|| dto.getStatus().equalsIgnoreCase("NACT")) {
				return corporateService.deactivateCorporateAcc(dto.getCorporateId(),loginUserId,dto.getStatus(),dto.getAccNo(),userName, dto.getCorporateProfileVersion(), dto.getCorporateServiceAccVersion());
			} else {
				responseResult.initResult(GTAError.CorporateManagementError.CORP_INVALID_STATUS);
				return responseResult;
			}
			
		} catch (GTACommonException gta) {
			//gta.printStackTrace();
			logger.error("CorporateManagmentController.deactivateCorporateAcc Exception",gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.deactivateCorporateAcc Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/member/changeStatus", method = RequestMethod.PUT)
	public ResponseResult deactivateCorporatePrimaryMember(@RequestBody CustomerProfileDto customerProfileDto){
		try {
			String loginUserId = getUser().getUserId();
			if (customerProfileDto.getStatus().equalsIgnoreCase("ACT")
					|| customerProfileDto.getStatus().equalsIgnoreCase("NACT")) {
				return corporateService.changeMemberStatus(
						customerProfileDto.getCustomerId(), loginUserId,
						customerProfileDto.getStatus(),getUser().getFullname()==null?getUser().getUserName():getUser().getFullname());
			} else {
				responseResult.initResult(GTAError.CorporateManagementError.INVALID_STATUS);
				return responseResult;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.deactivateCorporatePrimaryMember Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	public ResponseResult viewCorporateAccList(
			   @RequestParam(value="sortBy", defaultValue = "companyName") String sortBy,
			   @RequestParam(value="pageNumber", defaultValue = "1") Integer pageNumber,
			   @RequestParam(value="pageSize", defaultValue = "10")  Integer pageSize,
			   @RequestParam(value="isAscending", defaultValue = "true") String isAscending,
			   @RequestParam(value="filters",defaultValue="", required = false) String filters){
		if(isAscending.equals("true")){
			page.addAscending(sortBy);
		}else if (isAscending.equals("false")){
			page.addDescending(sortBy);
		}
		
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		return corporateService.listCorporateAcc(page);

	}
	/**
	 * description:
	 *  reset the corporate's service plan
	 * 
	 * @param corporateId corporate_profile primary key
	 * @param planNo service_plan primary key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateServicePlan",method = RequestMethod.PUT)
	public ResponseResult changeServicePlanForCorporateAcc(@RequestParam Long corporateId, @RequestParam Long planNo){
		String loginUserId = getUser().getUserId();
		ResponseResult result = null;
		try {
			 result = corporateService.editCorporateServicePlan(corporateId, planNo,loginUserId);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.changeServicePlanForCorporateAcc Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/member", method = RequestMethod.POST)
	public ResponseResult createMember(@RequestBody CustomerProfile customerProfile){
		try{
			ResponseResult responseResult = corporateService.createCorporateMember(customerProfile, super.getUser().getUserId(),getUser().getUserName());
			
			if ("0".equals(responseResult.getReturnCode())) {
				CustomerProfile profile=(CustomerProfile) responseResult.getDto();
				customerProfileService.moveProfileAndSignatureFile(profile);
				
				/***
				 * save  log user_record_action_log
				 */
				CustomerProfile logCustomerProfile=customerProfileService.getCustomerInfoByCustomerID(((CustomerProfile) responseResult.getDto()).getCustomerId());
				logCustomerProfile.setPortraitPhoto(profile.getPortraitPhoto());
				logCustomerProfile.setSignature(profile.getSignature());
				
				userRecordActionLogService.save(logCustomerProfile,super.getUser().getUserId(),UserRecordActionType.A.name());
				
			}
			
			return responseResult;
			
		}catch(GTACommonException gta){
			//gta.printStackTrace();
			logger.error("CorporateManagmentController.createMember Exception",gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("CorporateManagmentController.createMember Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@ResponseBody
	@RequestMapping(value= "/member/{customerId}", method = RequestMethod.GET)
	@Deprecated
	public ResponseResult viewMember(@PathVariable Long customerId) throws Exception{
		CustomerProfile customerProfile = customerProfileService.getByCustomerID(customerId);
		return corporateService.viewMember(customerProfile, customerId);
	}
	
	@ResponseBody
	@RequestMapping(value = "/member", method = RequestMethod.PUT)
	@Deprecated
	public ResponseResult editMember(@RequestBody CustomerProfile customerProfile){
		try{
			ResponseResult responseResult = corporateService.editCorporateMember(customerProfile, super.getUser().getUserId());
			if ("0".equals(responseResult.getReturnCode())) {
				customerProfileService.moveProfileAndSignatureFile(customerProfile);
			}
			return responseResult;
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("CorporateManagmentController.editMember Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/member", method = RequestMethod.GET)
	public ResponseResult getMemberList(@RequestParam(value="sortBy", defaultValue = "academyNo") String sortBy,
			   @RequestParam(value="pageNumber", defaultValue = "1") Integer pageNumber,
			   @RequestParam(value="pageSize", defaultValue = "10")  Integer pageSize,
			   @RequestParam(value="isAscending", defaultValue = "true") String isAscending,
			   @RequestParam(value="byAccount", defaultValue = "ALL") String byAccount,
			   @RequestParam(value="status", defaultValue = "ALL") String status,
			   @RequestParam(value="filters",defaultValue="", required = false) String filters,
			   @RequestParam(value="filterByCustomerId",defaultValue="", required = false) Long filterByCustomerId){
		if(isAscending.equals("true")){
			page.addAscending(sortBy);
		}else if (isAscending.equals("false")){
			page.addDescending(sortBy);
		}
		
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		
		if (status.equalsIgnoreCase("ALL")||status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)) {
			return corporateService.getMemberList(page,byAccount,status,filterByCustomerId);
		} else {
			responseResult.initResult(GTAError.CorporateManagementError.INVALID_STATUS);
			return responseResult;
		}
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/account/member", method = RequestMethod.GET)
	public ResponseResult getMemberListByAccount(
			   @RequestParam(value="sortBy", defaultValue = "academyNo") String sortBy,
			   @RequestParam(value="pageNumber", defaultValue = "1") Integer pageNumber,
			   @RequestParam(value="pageSize", defaultValue = "10")  Integer pageSize,
			   @RequestParam(value="isAscending", defaultValue = "true") String isAscending,
			   @RequestParam(value="corporateId", defaultValue = "",required = true) Long corporateId,
			   @RequestParam(value="status", defaultValue = "ALL") String status,
			   @RequestParam(value="filters",defaultValue="") String filters){
		try {
			if(isAscending.equals("true")){
				page.addAscending(sortBy);
			}else if (isAscending.equals("false")){
				page.addDescending(sortBy);
			}
			
			page.setNumber(pageNumber);
			page.setSize(pageSize);
			
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				if(advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0){
					for(SearchRuleDto rule: advanceQueryDto.rules){
						if(logger.isDebugEnabled()){
							logger.debug("rule field:" + rule.getField() + " op:" + rule.getOp() + " data:" + rule.getData());
						}
					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}
		
			if (status.equalsIgnoreCase("ALL")||status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)) {
				return corporateService.getMemberListByAccount(page, sortBy, isAscending, corporateId,status);
			} else {
				responseResult.initResult(GTAError.CorporateManagementError.INVALID_STATUS);
				return responseResult;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.getMemberListByAccount Exception : ",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/spendingSummary", method = RequestMethod.GET)
	public ResponseResult getSpendingSummary(
			@RequestParam(value = "sortBy", defaultValue = "orderDate") String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "dateField", required = true) String dateField,
			@RequestParam(value = "dateValue", required = true) String dateValue,
			@RequestParam(value = "corporateId", defaultValue = "") String corporateId,
			@RequestParam(value = "customerId", defaultValue = "") String customerId,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) {
		if(isAscending.equals("true")){
			page.addAscending(sortBy);
		}else if (isAscending.equals("false")){
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		SearchSpendingSummaryDto dto = new SearchSpendingSummaryDto(sortBy,pageNumber,pageSize,isAscending,dateField,dateValue,corporateId,customerId);
		try {
			return corporateMemberService.getSpendingSummary(page,dto);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.editMember Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/spendingSummary/email", method = RequestMethod.GET)
	public ResponseResult sendSpendingSummaryReportEmail(
			@RequestParam(value = "sortBy", defaultValue = "orderDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "dateField", required = true) String dateField,
			@RequestParam(value = "dateValue", required = true) String dateValue,
			@RequestParam(value = "corporateId", defaultValue = "") String corporateId,
			@RequestParam(value = "customerId", defaultValue = "") String customerId,
			@RequestParam(value = "email", defaultValue = "") String email,
			@RequestParam(value = "emailTo", defaultValue = "") String emailTo,
			@RequestParam(value = "fileType", defaultValue = "pdf") String fileType,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) {
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		SearchSpendingSummaryDto dto = new SearchSpendingSummaryDto(sortBy,isAscending,dateField,dateValue,corporateId,customerId,email,fileType,getUser().getUserId(),emailTo,getUser().getUserName());
		try {
			return corporateMemberService.sendSpendingSummaryReportEmail(page,dto);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.editMember Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/spendingSummary/downloadreport", method = RequestMethod.GET)
	public byte[] downloadSpendingSummaryReport(
			@RequestParam(value = "sortBy", defaultValue = "orderDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "dateField", required = true) String dateField,
			@RequestParam(value = "dateValue", required = true) String dateValue,
			@RequestParam(value = "corporateId", defaultValue = "") String corporateId,
			@RequestParam(value = "customerId", defaultValue = "") String customerId,
			@RequestParam(value = "fileType", defaultValue = "pdf") String fileType,
			@RequestParam(value="filters",defaultValue="", required = false) String filters,
			HttpServletRequest request,
            HttpServletResponse response) {
		handleExpirySession(request, response);
		byte[] attachement=null;
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		SearchSpendingSummaryDto dto = new SearchSpendingSummaryDto(sortBy,isAscending,dateField,dateValue,corporateId,customerId,"",fileType,getUser().getUserId(),"",getUser().getUserName());
		try {
			StringBuilder fileName = new StringBuilder("SpendingSummary");
			String parentjasperPath = servletContext.getRealPath("/WEB-INF/jasper/SpendingSummary.jasper");
			chooseFileType(request, response, fileName.toString(), fileType);
			attachement = corporateMemberService.downloadSpendingSummaryReport(page,dto,parentjasperPath);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.editMember Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		}
		return attachement;
	}
	protected void handleExpirySession(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getHeader("Referer");
        String base64Token = request.getHeader("token");
        GTAError errorCode = null;
        boolean valid = false;
        if (null==base64Token || StringUtils.isEmpty(base64Token))
        {
            base64Token = request.getParameter("token");
        }
        //If session is expire or token is invalid, redirect to login page.
        if (StringUtils.isEmpty(base64Token))
        {
            try {
                response.sendRedirect(url);
            } catch (IOException e1) {
    			logger.error(e1.getMessage(), e1);
            }
        }else {

            byte[] t = Base64.decodeBase64(base64Token);
            String authToken = new String(t);
            valid = TokenUtils.validateUUIDToken(authToken);
            errorCode = userMasterService.updateSessionToken(authToken);
            if (!valid || !GTAError.Success.SUCCESS.equals(errorCode))
            {
                try {
                    response.sendRedirect(url);
                } catch (IOException e1) {
        			logger.error(e1.getMessage(), e1);
                }
            }
        }
    }
    protected void chooseFileType(HttpServletRequest request,	HttpServletResponse response,String fileName,String fileType) {
        if("pdf".equalsIgnoreCase(fileType)){
            response.setHeader("Content-Disposition", "attachment; filename=" +fileName+".pdf");
            response.setContentType("application/pdf");
        }else{
            response.setHeader("Content-Disposition", "attachment; filename=" +fileName+".csv");
            response.setContentType("text/csv");
        }
    }
    private String getFileDateStr(@RequestParam(value = "transactionDate", required = true) String transactionDate) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM").parseDateTime(transactionDate);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MMM");
        return fmt.print(dateTime);
    }
	
	@ResponseBody
	@RequestMapping(value = "/accountdrops", method = RequestMethod.GET)
	public ResponseResult getCorporateAccountDropList() {
		return corporateService.getCorporateAccountDropList();
	}
	
	@ResponseBody
	@RequestMapping(value = "/memberdrops/{corporateId}", method = RequestMethod.GET)
	public ResponseResult getMemberDropList(@PathVariable Long corporateId) {
		return corporateService.getMemberDropList(corporateId);
	}
	
	/**
	 * checking only corporate primary member credit Limit when update credit limit
	 * @param corporateId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/checkCreditLimit/{corporateId}", method = RequestMethod.GET)
	public ResponseResult checkCreditLimitForCPM(@PathVariable Long corporateId) {
		try {
			return corporateService.checkCreditLimit(corporateId);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.checkCreditLimitForCPM Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * checking only corporate or individual dependent member credit Limit when update credit limit
	 * @param customerId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/checkDMCreditLimit/{customerId}", method = RequestMethod.GET)
	public ResponseResult checkCreditLimitForDM(@PathVariable Long customerId) {
		return  memberService.checkDMCreditLimit(customerId);
	}
	
	@RequestMapping(value = "/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult advanceSearch(@RequestParam(value="module",defaultValue="") String module) {
		
		responseResult.initResult(GTAError.Success.SUCCESS, corporateService.assembleQueryConditions(module));
		return responseResult;
	}
	
	@RequestMapping(value = "/dropdownlists/serviceplan",method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getValidServicePlanIncludeItself(@RequestParam(value="corporateId",required=false) Long corporateId){
		try {
			return corporateService.getValidServicePlanIncludeItself(corporateId);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.getValidServicePlanIncludeItself Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/transaction/getAll", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionList(
			@RequestParam(value = "balanceDue", defaultValue = "All") String balanceDue,
			@RequestParam(value = "serviceType", defaultValue = "All") String serviceType,
			@RequestParam(value = "sortBy", defaultValue = "enrollCreateDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") boolean isAscending,
			@RequestParam(value = "searchText", required= false,defaultValue = "") String searchText,
			@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "0") int pageSize,
			@RequestParam(value = "filterBy", defaultValue = "MY") String filterBy,
			@RequestParam(value = "enrollStatusFilter", defaultValue = "All") String enrollStatusFilter,
			@RequestParam(value = "mode", defaultValue = "saleskit") String mode,
			@RequestParam(value="filters",required= false,defaultValue= "") String filters,
			@RequestParam(value="memberType",required= false,defaultValue= "CPM") String memberType
			//@RequestBody(required = false) AdvanceQueryDto advanceQueryDto
			) {
		try {
			logger.info("Ready to get transaction list");
			page = new ListPage<CustomerOrderHd>();
			String filterBySalesMan = null;
			if(pageNumber>0){
				page.setNumber(pageNumber);
			}
			if(pageSize > 0){
				page.setSize(pageSize);
			}
			if(filterBy.equalsIgnoreCase("my")){
				String loginUserId =  getUser().getUserId();
				filterBySalesMan = (loginUserId == "-1")?"":loginUserId;
			}else if(filterBy.equalsIgnoreCase("ALL")){
				filterBySalesMan = "";
			}
			LoginUser currentUser = getUser();
			if(CommUtil.notEmpty(filters)){
				AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				if(advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0){
					if(sortBy != null && sortBy.length()>0 ){
						advanceQueryDto.setSortBy(sortBy);
					}
					advanceQueryDto.setAscending(isAscending?" true ":" false ");
//					for(SearchRuleDto rule: advanceQueryDto.rules){
//						if("t.offerCode".equals(rule.field)&&"ENROLLMENT".equals(rule.data.toUpperCase())){
//							rule.setData(null);
//							rule.setOp(CommonDataQueryDao.IS_NULL);
//						}
//					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}
			ListPage<CustomerOrderHd> result = customerOrderTransService
					.getCustomerTransactionList(page, balanceDue, serviceType, sortBy,isAscending,searchText,filterBySalesMan, mode,currentUser.getUserId(), enrollStatusFilter,memberType, "PC");
			ResponseResult response = new ResponseResult();
			Data data = new Data();
			data.setList(result.getDtoList());
			data.setLastPage(result.isLast());
			data.setCurrentPage(result.getNumber());
			data.setRecordCount(result.getAllSize());
			data.setPageSize(result.getSize());
			data.setTotalPage(result.getAllPage());
			response.setData(data);
			response.setReturnCode("0");
			response.setErrorMessageEN("");
			return response;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
			//e.printStackTrace();
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION);
			return responseResult;
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/transaction/getAll/advancedSearch", method = RequestMethod.GET)
	public ResponseResult advanceSearchConditions(@RequestParam(value = "serviceType", defaultValue = "Enrollment") String serviceType){
		responseResult.initResult(GTAError.Success.SUCCESS, customerOrderTransService.assembleQueryConditions(serviceType));
		return responseResult;
	}					
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/settlement", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCorporateMemberShipSettlement(
			   @RequestParam(value="sortBy", defaultValue = "enrollDateDB") String sortBy,
			   @RequestParam(value="pageNumber", defaultValue = "1") Integer pageNumber,
			   @RequestParam(value="pageSize", defaultValue = "10")  Integer pageSize,
			   @RequestParam(value="isAscending", defaultValue = "false") String isAscending,
			   @RequestParam(value = "offerCode", defaultValue = "RENEW", required = false) String offerCode,
			   @RequestParam(value="status", defaultValue = "ALL") String status,
			   @RequestParam(value="filterByCustomerId",defaultValue="")  @EncryptFieldInfo Long filterByCustomerId,
			   @RequestParam(value="filters",defaultValue="", required = false) String filters
			) {
		
		if(isAscending.equals("true")){
			page.addAscending(sortBy);
		}else if (isAscending.equals("false")){
			page.addDescending(sortBy);
		}
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		return corporateService.getCorporateMembershipSettlement(page, status,offerCode, filterByCustomerId);
	}
	
	/**
	 * Method is used to activate customer either by selecting date or activate straightforward
	 * @param customerId
	 * @return Response Message
	 */
	@RequestMapping(value = "/activateMember", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult activateMember(@RequestBody ActivateMemberDto activeMemberDto){
		try {
			LoginUser currentUser = super.getUser();
			if(activeMemberDto.getActivationDate()==null){
				return customerEnrollmentService.encapsulateActivationMember(activeMemberDto,currentUser.getUserId(),currentUser.getFullname()==null?currentUser.getUserName():currentUser.getFullname());
			}else{
				return customerEnrollmentService.setActivationDateForMember(activeMemberDto,currentUser.getUserId(),currentUser.getFullname()==null?currentUser.getUserName():currentUser.getFullname());
			}
		}catch (GTACommonException gta) {
			logger.error("CorporateManagementControllment.activeMember Exception",gta);
			//gta.printStackTrace();
			responseResult.initResult(gta.getError());
			return responseResult;
		}catch (Exception e) {
			logger.error("CorporateManagementControllment.activeMember Exception",e);
			//e.printStackTrace();
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/corporateContact", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCorporateContactByCorporateIdAndContactType(
			@RequestParam(value = "corporateId",required = true) Long corporateId,
			@RequestParam(value = "contactType",required = true) String contactType){
		return corporateService.getCorporateContactByCorporateIdAndContactType(corporateId, contactType);
	}
	
	/**
	 * guest room checkin后是否需要发送push message消息开关设置
	 * @param customerId
	 * @param isSilentCheckinPush
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/silentCheckinPushSwitch/{customerId}/{isSilentCheckinPush}", method = RequestMethod.PUT)
	public ResponseResult silentCheckinPushSwitch(@PathVariable(value = "customerId")@EncryptFieldInfo  Long customerId, @PathVariable(value="isSilentCheckinPush") String isSilentCheckinPush){
		try {
			return corporateService.silentCheckinPushSwitch(customerId, isSilentCheckinPush);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("CorporateManagmentController.silentCheckinPushSwitch Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
}
