package com.sinodynamic.hkgta.controller.crm.sales.renewal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.ExpiredMemberDto;
import com.sinodynamic.hkgta.dto.crm.MemberInfoDto;
import com.sinodynamic.hkgta.dto.crm.RenewalDto;
import com.sinodynamic.hkgta.dto.crm.RenewalEmailDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanDto;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.RemarksService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.RenewalService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberExpiredOrderByCol;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/renewals")
@Scope("prototype")
public class RenewalController extends ControllerBase{
		
	private static final String STATUS_ACTIVE = "ACT";
	@Autowired
	private RenewalService renewalService;
	private Logger logger = Logger.getLogger(RenewalController.class);
	
	@Autowired
	private ServicePlanService servicePlanService;
	
	@Autowired
	private CustomerServiceAccService customerServiceAccService;
	
	
	private @Autowired RemarksService remarksService; 


	@RequestMapping(value="/list", method=RequestMethod.GET)
	public @ResponseBody ResponseResult getExpiredMembersList(@RequestParam(value="searchType", required = false, defaultValue= "") String searchType,
			@RequestParam(value="expiredDays", required = false, defaultValue = "") String expiredDays,
			@RequestParam(value="expiringDays", required = false, defaultValue = "") Integer expiringDays,
			@RequestParam(value="searchText",required = false, defaultValue = "") String searchText,
			@RequestParam(value="sortBy", required = false, defaultValue = "") String sortBy,
			@RequestParam(value="isAscending", required = false) Boolean isAscending,
			@RequestParam(value="pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value="pageNumber",required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(value="device",required = false, defaultValue = "IOS") String device,
			@RequestParam(value = "isMyClient",required = false, defaultValue = "false") String isMyClient ){
		page = new ListPage<ExpiredMemberDto>(pageSize, pageNumber);
		//if isMyclient is true and search conditions add userId
		String userId = getUser().getUserId();
		ResponseResult responseResult = new ResponseResult("0","",new Data());
		
		if(StringUtils.isEmpty(searchType))
			return new ResponseResult("1", "", "parameter:searchTypes is null!");
		
		ListPage<ExpiredMemberDto> expiringMemberList = null;
		try {
			if("expiring".equals(searchType.trim())){
				if(null == expiredDays){
					return new ResponseResult("1", "", "parameter:expiredDays is null!");
				}
				expiringMemberList = renewalService.getExpiringMemberList(searchText,expiringDays,getOrderByCol(sortBy), getSortType(isAscending), page,isMyClient,userId);
				
			}else if("expired".equals(searchType.trim())){
				expiringMemberList = renewalService.getExpiredMemberList(searchText,expiredDays,getOrderByCol(sortBy), getSortType(isAscending), page,isMyClient,userId);
			}else{
				return new ResponseResult("1", "parameter:searchTypes is error!");
			}
			
			if(null == expiringMemberList){
				return new ResponseResult("1", "Fail to retrieve the expire member!");
			}
			int count = expiringMemberList.getDtoList().size();
			if(count == 0)
				return new ResponseResult("0","No record find!",new Data());
			
			changeDateFormatForMember(expiringMemberList.getDtoList(),device);
			
			responseResult.getListData().setList(expiringMemberList.getDtoList());
			responseResult.getListData().setRecordCount(page.getAllSize());
			responseResult.getListData().setCurrentPage(expiringMemberList.getNumber());
			responseResult.getListData().setTotalPage(expiringMemberList.getAllPage());
			responseResult.getListData().setPageSize(page.getSize()==count?page.getSize():count);
			responseResult.getListData().setLastPage(page.isLast());
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return responseResult;
	}

	
	private void changeDateFormatForMember(List memberList, String device){
		for(Object obj : memberList){
			ExpiredMemberDto dto = (ExpiredMemberDto)obj;
			dto.setExpiryDate(DateConvertUtil.getObliqueYMDDateAndDateDiff(DateConvertUtil.parseString2Date(dto.getExpiryDate(), "yyyy/MM/dd"), "yyyy/MM/dd"));
			LoginUser currentUser = getUser();
			if (null != dto.getCustomerId()) {
				ResponseResult result = remarksService.checkUnreadRemarkByDevice(Long.parseLong(dto.getCustomerId().toString()),currentUser.getUserId(),device);
				Integer o = (Integer)result.getData();
				if(null != o){
					if (o > 0) {
						dto.setIsRemarkIconShow(true);
					}else{
						dto.setIsRemarkIconShow(false);
					}
				}else{
					dto.setIsRemarkIconShow(false);
				}
			}
			
		}
	}
	
	private String getSortType(Boolean isAscending){
		if(null == isAscending)
			return "";
		
		if(isAscending)
			return "asc";
		return "desc";
	}

	
	/**
	 * @author Vineela_Jyothi
	 * @param customerId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/{customerId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getMemberInfo(
			@PathVariable(value="customerId") Long customerId){
		try {
			MemberInfoDto dto = renewalService.getMemberInfo(customerId);
			responseResult.initResult(GTAError.Success.SUCCESS,dto);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	private MemberExpiredOrderByCol getOrderByCol(String orderByCol) {
		
		if(StringUtils.isEmpty(orderByCol)){
			return null;
		}
		
		MemberExpiredOrderByCol orderCol = null;
		switch (orderByCol.trim().toUpperCase())
		{
		case "NAME":
			orderCol = MemberExpiredOrderByCol.NAME;
			break;
		case "EXPIREDATE":
			orderCol = MemberExpiredOrderByCol.EXPIREDATE;
			break;
		case "CURRENTSERVICENAME":
			orderCol = MemberExpiredOrderByCol.CURRENTSERVICENAME;
			break;
		case "NEWSERVICENAME":
			orderCol = MemberExpiredOrderByCol.NEWSERVICENAME;
			break;
		default:
			orderCol = null;
			break;
		}
		return orderCol;
	}


	
	
	@RequestMapping(value="/email", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMsg sendRenewalInfoByEmail(@RequestBody RenewalEmailDto mailReq) {
		
		logger.info("RenewalController.sendRenewalInfoByEmail invocation start ...");
		
		//modified by Kaster 20160518 将RenewalEmailDto的customerId类型换成了Long。
		Long customerID = mailReq.getCustomerID();
		String emailTitle = mailReq.getEmailTitle();
		String messageSubject = mailReq.getMessageSubject();
		String emailBody = mailReq.getEmailBody();
		
		if(emailTitle==null){
			emailTitle=messageSubject;
			mailReq.setEmailTitle(messageSubject);
		}
		//modified by Kaster 20160518 将RenewalEmailDto的customerId类型换成了Long。
		if (customerID == null || customerID == 0) {
			logger.error("customerID can not be null");
			responseMsg.initResult(GTAError.EnrollmentAdvanceError.CUSTOMER_ID_CAN_NOT_NULL);
			return responseMsg;
		}
		
		if (emailTitle == null || emailTitle.trim().length() == 0) {
			responseMsg.initResult(GTAError.EnrollmentAdvanceError.EMAIL_TITLE_CAN_NOT_NULL);
			return responseMsg;
		}
		
		if (emailBody == null || emailBody.trim().length() == 0) {
			responseMsg.initResult(GTAError.EnrollmentAdvanceError.EMAIL_BODY_CAN_NOT_NULL);
			return responseMsg;
		}
		
		try {
			return renewalService.sendRenewWithInvoiceEmail(mailReq,getUser().getUserId(),getUser().getUserName());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}
	
	
	
	@RequestMapping(value = "/serviceplan", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllServicPlan() {
		
		logger.info("RenewalController.getAllServicPlan invocation start...");
		
		try {
			
			List<ServicePlanDto> detailServicePlans = servicePlanService.getAllServicPlan();
			if (detailServicePlans == null || detailServicePlans.size() == 0) return null;
			
			List<TreeMap<String, Object>> retData = new ArrayList<>();
			for (ServicePlanDto dto : detailServicePlans) {
				
				if (STATUS_ACTIVE.equals(dto.getStatus())) {
					
					TreeMap<String, Object> record = new TreeMap<>();
					record.put("planNo", dto.getPlanNo());
					record.put("planName", dto.getPlanName());
					retData.add(record);
				}
			}
			responseResult.initResult(GTAError.Success.SUCCESS,retData);
			return responseResult;
					
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/newplan", method = RequestMethod.POST , produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseResult saveRenewalAcc(@RequestBody  RenewalDto renewalDto) {
		logger.info("===============================updatePresentation start=============================");
		try {
			renewalDto.setCreateBy(super.getUser().getUserId());
			renewalDto.setUserName(getUser().getUserName());
			return renewalService.startUpMemberAcc(renewalDto);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	
	@RequestMapping(value="/status/{orderNo}", method=RequestMethod.PUT , produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseResult updateServiceAccountStatus(@PathVariable(value="orderNo") Long orderNo){
		logger.info("RenewalController.status invocation start...");
		ResponseResult result = customerServiceAccService.updateCustomerServiceAccStatus(orderNo);
		return result;		
	}
	

}
