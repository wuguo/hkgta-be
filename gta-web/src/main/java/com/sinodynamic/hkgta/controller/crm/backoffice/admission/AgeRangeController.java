package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.AgeRange;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.AgeRangeService;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype") 
public class AgeRangeController extends ControllerBase<AgeRange>{
	@Autowired
	private AgeRangeService ageRangeService;
	
	@RequestMapping(value = "/agerange/get_allagerange", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllServicPlan() {
		try {
			List<AgeRange> retList = ageRangeService.getAllAgeRange();
			ResponseResult result = new ResponseResult("0", "success", retList);
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new GTACommonException(e);
		}
	}
	
}
