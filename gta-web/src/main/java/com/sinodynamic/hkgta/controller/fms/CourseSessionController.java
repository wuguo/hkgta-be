package com.sinodynamic.hkgta.controller.fms;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.service.fms.CourseSessionService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/course-session")
public class CourseSessionController extends ControllerBase<Serializable> {

	private Logger					logger	= Logger.getLogger(CourseSessionController.class);

	@Autowired
	private CourseSessionService	courseSessionService;

	@RequestMapping(value = "/coach/sessions", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCoachSessionList(@RequestParam(value = "coachUserId") String coachUserId,
			@RequestParam(value = "beginDatetime") Date beginDatetime, @RequestParam(value = "endDatetime") Date endDatetiem) {

		logger.info("CourseSessionController.getCoachSessionList invocation start ...");

		try {

			return courseSessionService.getCoachSessionList(coachUserId, beginDatetime, endDatetiem);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/attendance/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAttendanceMemberList(@RequestParam(value = "sessionId") String sessionId) {

		logger.info("CourseSessionController.getAttendanceMemberList invocation start ...");

		try {

			return courseSessionService.getAttendanceMemberList(sessionId);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/session/members", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSessionMembers(@RequestParam(value = "courseId") String courseId, @RequestParam(value = "sessionNo") String sessionNo) {

		logger.info("CourseSessionController.getSessionMembers invocation start ...");

		try {

			return courseSessionService.getSessionMembers(courseId, sessionNo);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
}
