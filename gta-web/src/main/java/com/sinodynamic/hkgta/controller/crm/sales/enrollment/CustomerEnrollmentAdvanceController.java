package com.sinodynamic.hkgta.controller.crm.sales.enrollment;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.Constant.Status;
import com.sinodynamic.hkgta.dto.crm.CustomerCheckExsitDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerAdditionInfoService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.RenewalService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PassportType;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 
 * @author Sam_Pang
 * @date Apr 29, 2015 4:06:27 PM 
 * @Description: check customer whether exist and query all exist information
 */
@Controller
@RequestMapping("/enrollment")
public class CustomerEnrollmentAdvanceController extends ControllerBase<ResponseResult>{

	private static final Logger logger = Logger.getLogger(CustomerEnrollmentAdvanceController.class);
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;

	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private CustomerAdditionInfoService customerAdditionInfoService;
	
	@Autowired
	private RenewalService renewalService;
	
	@Autowired
	private CustomerServiceAccService customerServiceAccService;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	
	@RequestMapping(value="/checkPassportNO", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkWhetherExist(
			@RequestParam(value ="idType") String idType,
			@RequestParam(value = "idNO") String idNO,
			@RequestParam(value = "enrollType",defaultValue="IDM", required = false) String enrollType,
			@RequestParam(value = "driverType" ,required = false) String driverType,
			@RequestParam(value = "superiorMemberId",required = false) @EncryptFieldInfo Long superiorMemberId){		
		logger.info("CustomerEnrollmentAdvanceController.checkWhetherExist start ...");
		try {
			if(!PassportType.HKID.name().equals(idType) && !PassportType.VISA.name().equals(idType)){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.IDTYPE_INVALID);
				return responseResult;
			}
			if(PassportType.HKID.name().equals(idType)&& !CommUtil.validateHKID(idNO)){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.HKID_INVALID);
				return responseResult;
			}
			if(PassportType.VISA.name().equals(idType)&& !CommUtil.validateVISA(idNO)){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.PASSPORT_INVALID);
				return responseResult;
			}
			CustomerCheckExsitDto dto = customerProfileService.checkExistPassportNO(idType, idNO);
			
			/*Pass the checking, Act as a normal registration*/
			if(null == dto){
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
			/***
			 * SGG-3379
			 * FromKit
			 */
			if(StringUtils.isNotEmpty(driverType)&&"FormKit".equals(driverType))
			{
				//
				return this.checkFromKitAddDepent(dto, idType, idNO);
			}else if("DayPass".equals(driverType))
			{
				responseResult=customerProfileService.checkMemberExistByPassportTypeAndPassportNo(idType, idNO);
				if(responseResult.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())&&null!=responseResult.getDto())
				{
					/***
					 * formkit day pass check passport exist ,import guest profile
					 */
					CustomerProfile pro=(CustomerProfile)responseResult.getDto();
					pro.setCode("24");
					pro.setButton("Import Profile");
					pro.setMessage("The customer profile already exists");
					responseResult.initResult(GTAError.Success.SUCCESS,pro);
					return responseResult;
				}
			}
			//saleskit 
			dto.setEnrollType(enrollType);
			
			//13.Click to renew Individual Primary member(Clicked Renew, not pay, inactive)
			if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) && this.renewalService.checkExistsByNewPlanStatus(dto.getCustomerID().longValue(), "toPay")){
				dto.setCode("13");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
				
			//15.Pay all for renew Individual Primary member(Clicked Renew, pay all, inactive, not reach activation date)
			}else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) && this.renewalService.checkExistsByNewPlanStatus(dto.getCustomerID().longValue(), "toActive")){
				dto.setCode("15");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//14.Click to renew Individual Dependent member(Clicked Renew, not pay, inactive)
			else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType()) && this.renewalService.checkExistsByNewPlanStatus(dto.getSupMemberId(), "toPay")){
				dto.setCode("14");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
				
			//16.Pay all for renew Individual Dependent member(Clicked Renew, pay all, inactive, not reach activation date)
			}else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType()) && this.renewalService.checkExistsByNewPlanStatus(dto.getSupMemberId(), "toActive")){
				dto.setCode("16");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//1.Active Individual Primary member
			else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) && "ACT".equals(dto.getStatus())){
				dto.setCode("1");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//2.Inactive Individual Primary member (not expired)
			if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) && "NACT".equals(dto.getStatus()) && this.customerServiceAccService.getActiveByCustomerId(dto.getCustomerID().longValue()) != null){
				dto.setCode("2");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//3.Active Individual Dependent member
			if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType()) && "ACT".equals(dto.getStatus())){
				dto.setCode("3");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//4.Inactive Individual Dependent member (not expired)
			if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType()) && "NACT".equals(dto.getStatus()) && (dto.getSupMemberId() == null || (dto.getSupMemberId() != null && this.customerServiceAccService.getActiveByCustomerId(dto.getSupMemberId().longValue()) != null))){
				/***
				 * dependent close 
				 */
				if(customerProfileService.checkCloseCustomerProfile(Long.valueOf(dto.getCustomerID().toString()))){
					dto.setCode("25");
				}else{
					dto.setCode("4");	
				}
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//5.Rejected / Cancelled Individual Primary 	
			else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) && (EnrollStatus.REJ.name().equalsIgnoreCase(dto.getEnrollStatus())||EnrollStatus.CAN.name().equalsIgnoreCase(dto.getEnrollStatus())||EnrollStatus.OPN.name().equalsIgnoreCase(dto.getEnrollStatus()))){
				dto.setCode("5");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//6.Rejected / Cancelled Individual Dependent member	
			else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType()) && (EnrollStatus.REJ.name().equalsIgnoreCase(dto.getEnrollStatus())||EnrollStatus.CAN.name().equalsIgnoreCase(dto.getEnrollStatus()))){
				dto.setCode("6");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//7.New / Approved / Payment Approved Individual Primary member	
			else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) && (EnrollStatus.NEW.name().equals(dto.getEnrollStatus())||EnrollStatus.APV.name().equals(dto.getEnrollStatus())||EnrollStatus.PYA.name().equals(dto.getEnrollStatus()))){
				dto.setCode("7");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//8.New / Approved / Payment Approved Individual Dependent member	
			else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType()) && (EnrollStatus.NEW.name().equals(dto.getEnrollStatus())||EnrollStatus.APV.name().equals(dto.getEnrollStatus())||EnrollStatus.PYA.name().equals(dto.getEnrollStatus()))){
				dto.setCode("8");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//9.To Active x days Individual Primary member	
			else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) && EnrollStatus.TOA.name().equals(dto.getEnrollStatus())){
				dto.setCode("9");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//10.To Active x days Individual Dependent member	
			else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType()) && EnrollStatus.TOA.name().equals(dto.getEnrollStatus())){
				dto.setCode("10");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//11.Expired Individual Primary Member
			else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) && this.customerServiceAccService.isExpiredAndNotRenewalMember(dto.getCustomerID().longValue())){
				dto.setCode("11");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//12.Expired Individual Dependent Member
			else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType()) && this.customerServiceAccService.isExpiredAndNotRenewalMember(dto.getSupMemberId())){
				dto.setCode("12");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//17.Active Corporate Primary member
			else if(MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType()) && "ACT".equals(dto.getStatus())){
				dto.setCode("17");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//18.Inactive Corporate Primary member (Not expired)
			if(MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType()) && "NACT".equals(dto.getStatus()) && this.customerServiceAccService.getActiveByCustomerId(dto.getCustomerID().longValue()) != null){
				dto.setCode("18");	
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//19.Active Corporate Dependent member
			else if(MemberType.CDM.name().equalsIgnoreCase(dto.getMemberType()) && "ACT".equals(dto.getStatus())){
				dto.setCode("19");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//20.Inactive Corporate Dependent member (Not expired)
			if(MemberType.CDM.name().equalsIgnoreCase(dto.getMemberType()) && "NACT".equals(dto.getStatus()) && this.customerServiceAccService.getActiveByCustomerId(dto.getSupMemberId()) != null){
				/***
				 * dependent close 
				 */
				if(customerProfileService.checkCloseCustomerProfile(Long.valueOf(dto.getCustomerID().toString()))){
					dto.setCode("25");
				}else{
					dto.setCode("20");
				}
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//21.Rejected / Cancelled Corporate Primary member
			else if(MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType()) && (EnrollStatus.REJ.name().equalsIgnoreCase(dto.getEnrollStatus())||EnrollStatus.CAN.name().equalsIgnoreCase(dto.getEnrollStatus()))){
				dto.setCode("21");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//22.New / Approved / Payment Approved Corporate Primary member	
			else if(MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType()) && (EnrollStatus.NEW.name().equals(dto.getEnrollStatus())||EnrollStatus.APV.name().equals(dto.getEnrollStatus())||EnrollStatus.PYA.name().equals(dto.getEnrollStatus()))){
				dto.setCode("22");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			
			//23.To Active x days Corporate Primary member
			else if(MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType()) && EnrollStatus.TOA.name().equals(dto.getEnrollStatus())){
				dto.setCode("23");
				responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
				return responseResult;
			}
			/***
			 * new add when enrollment patron is new ,and delete after ,enrollment status is OPN, 
			 */
			
			else if(MemberType.MG.name().equalsIgnoreCase(dto.getMemberType())){
					/***
					 * staff/salekit import daypass
					 */
					dto.setCode("25");
					responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
					return responseResult;
			}
			
			
			/*Enrolling for Primary Member GTAError.EnrollmentAdvanceError.UPGRADE_FOR_DEPENDENTDM)*/
//			else if((EnrollStatus.NEW.name().equals(dto.getEnrollStatus())||EnrollStatus.APV.name().equals(dto.getEnrollStatus())
//					||EnrollStatus.PYA.name().equals(dto.getEnrollStatus())||EnrollStatus.TOA.name().equals(dto.getEnrollStatus()))
//					&&((MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) || MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType())))){
//				responseResult.initResult(GTAError.EnrollmentAdvanceError.ENROLLING,dto);
//				return responseResult;
//			}
//			
//			/*Upgrade dependent member (PM:Upgrade, DM:Display)*/
//			else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType())||MemberType.CDM.name().equalsIgnoreCase(dto.getMemberType())){//Used to import the dependent
//				//only  member supMember permissions  add exist Depentment member 
//				if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType())){
//					if(superiorMemberId != null && !superiorMemberId.equals(dto.getSupMemberId()) && !dto.getStatus().equals("NACT")){
//						responseResult.initResult(GTAError.EnrollmentAdvanceError.NOQUALIFY_IMPORT);
//					}else{
//						responseResult.initResult(GTAError.EnrollmentAdvanceError.UPGRADE_FOR_DEPENDENT,dto);
//					}
//				}else{
//					responseResult.initResult(GTAError.EnrollmentAdvanceError.UPGRADE_FOR_DEPENDENT,dto);
//				}
//				return responseResult;
//				
//			/*Re-enroll the primary member or import the PM and converted to DM when it was rejected or canceled (PM: Re-enroll, DM:Import)*/	
//			}else if((MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()) || MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType()))
//					&&(EnrollStatus.REJ.name().equalsIgnoreCase(dto.getEnrollStatus())||EnrollStatus.CAN.name().equalsIgnoreCase(dto.getEnrollStatus()))){	
//				responseResult.initResult(GTAError.EnrollmentAdvanceError.RE_ENROLL_FOR_REJ_CAN,dto);
//				return responseResult;
//				
//			/*Display primary member with Active/Inactive status for ANC or CMP (PM,DM)*/
//			}else if((MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType())||MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType()))
//					&&(EnrollStatus.ANC.name().equals(dto.getEnrollStatus())||EnrollStatus.CMP.name().equals(dto.getEnrollStatus()))){
//				responseResult.initResult(GTAError.EnrollmentAdvanceError.ID_REG_ALREADY,new Object[]{dto.getMemberType(),PassportType.VISA.name().equals(idType)?"Passport":idType,idNO},dto);
//				return responseResult;
//				
//			}
//			
//			/*HG,MG detected(PM: Import, DM: display)*/
//			else if(MemberType.MG.name().equalsIgnoreCase(dto.getMemberType())||MemberType.HG.name().equalsIgnoreCase(dto.getMemberType())){
//				responseResult.initResult(GTAError.EnrollmentAdvanceError.MG_HG_DETECTED,dto);
//				return responseResult;
//			}
//			
//			/*Lead detected(PM: Import, DM: display)*/
//			else if(dto.getMemberType()==null&&EnrollStatus.OPN.name().equalsIgnoreCase(dto.getEnrollStatus())){
//				responseResult.initResult(GTAError.EnrollmentAdvanceError.ENROLL_LEAD,dto);
//				return responseResult;
//			}
			/* To be confirmed*/
			/*
			else if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType())&&Constant.General_Status_NACT.equals(dto.getStatus())
					&&(EnrollStatus.ANC.name().equals(dto.getEnrollStatus())||EnrollStatus.CMP.name().equals(dto.getEnrollStatus()))){
				responseResult.initResult(GTAError.EnrollmentAdvanceError.INACT_IPM_RENEW,dto);
				return responseResult;
			}
			*/
			
		} catch (Exception e) {
			logger.debug("CustomerEnrollmentAdvanceController.checkWhetherExist",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		logger.info("CustomerEnrollmentAdvanceController.checkWhetherExist end ...");
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	/***
	 * checking for closed / Inactive / existing passport number or HKID
	 * @param dto
	 * @return
	 */
	private ResponseResult checkFromKitAddDepent(CustomerCheckExsitDto dto,String passportType, String passportNo)
	{
		//check close
		if(customerProfileService.checkCloseCustomerProfile(Long.valueOf(dto.getCustomerID().toString())))
		{
			//CH000035-1488952324311
			dto.setCode("26");
		}else{
			if(Status.ACT.name().equals(dto.getStatus()))
			{
				if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()))
				{
					dto.setCode("1");
				}else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType())){
					dto.setCode("3");
				}else if(MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType())){
					dto.setCode("17");
				}else if(MemberType.CDM.name().equalsIgnoreCase(dto.getMemberType())){
					dto.setCode("19");
				}else if(MemberType.MG.name().equalsIgnoreCase(dto.getMemberType())){
					dto.setCode("26");
					dto.setMessage("The guest customer profile already exists");
						
				}
			}else if(Status.NACT.name().equals(dto.getStatus())){
				if(MemberType.IPM.name().equalsIgnoreCase(dto.getMemberType()))
				{	
					/***
					 * when patron status is new ,salekit delete/ERJ/CAN enrollment patron ,formkit need import patron information   
					 */
					if(EnrollStatus.REJ.name().equalsIgnoreCase(dto.getEnrollStatus())||EnrollStatus.CAN.name().equalsIgnoreCase(dto.getEnrollStatus())||EnrollStatus.OPN.name().equalsIgnoreCase(dto.getEnrollStatus()))
					{
						dto.setCode("26");
						dto.setMessage("Cancelled/Rejected/Delete Individual Primary Member");
					}else{
						dto.setCode("2");	
					}
				}else if(MemberType.IDM.name().equalsIgnoreCase(dto.getMemberType())){
					dto.setCode("4");
				}else if(MemberType.CPM.name().equalsIgnoreCase(dto.getMemberType())){
					dto.setCode("18");
				}else if(MemberType.CDM.name().equalsIgnoreCase(dto.getMemberType())){
					dto.setCode("20");
				}else if(MemberType.MG.name().equalsIgnoreCase(dto.getMemberType())){
					dto.setCode("26");
					dto.setMessage("The guest customer profile already exists");
				}
			}
			else if(MemberType.MG.name().equalsIgnoreCase(dto.getMemberType())||MemberType.HG.name().equalsIgnoreCase(dto.getMemberType()))
			{
				dto.setCode("27");
			}
		}
		responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,dto);
		return responseResult;
	}
	private ResponseResult importDayPassGuest(String passportType, String passportNo){
		responseResult=customerProfileService.checkMemberExistByPassportTypeAndPassportNo(passportType, passportNo);
		if(responseResult.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())&&null!=responseResult.getDto())
		{
			CustomerProfile pro=(CustomerProfile)responseResult.getDto();
			pro.setCode("26");
			pro.setMessage("The guest customer profile already exists");
			responseResult.initResult(GTAError.EnrollmentAdvanceError.EXISTS_MEMEBER,pro);
		}
		return responseResult;
	}
	@RequestMapping(value="/import/{customerID}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult importExistInfo(@PathVariable("customerID") @EncryptFieldInfo Long customerID, @RequestParam(value = "superiorMemberId",required = false) @EncryptFieldInfo Long superiorMemberId,
			@RequestParam(value = "closeAccount",required = false) String closeAccount, @RequestParam(value = "importStatus",required = false) String importStatus) {
		logger.info("CustomerEnrollmentAdvanceController.importExistInfo start ...");
		try {
			if("true".equalsIgnoreCase(closeAccount)){
				responseResult = this.customerProfileService.archiveMember(customerID, getUser().getUserId(),
						"NACT",getUser().getFullname()==null?getUser().getUserName():getUser().getFullname());
				if(!GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())){
					return responseResult;
				}
			}
			CustomerProfile cp =null;
			if(null!=superiorMemberId){
				 cp = customerProfileService.getCustomerByID(customerID, superiorMemberId);
			}else{
				 cp = customerProfileService.getByCustomerID(customerID);
			}
			//如果passportNo中有"-",表明是archive member
			if("true".equalsIgnoreCase(importStatus) && cp.getPassportNo().contains("-")){
				this.customerEnrollmentService.mainBodySeparate(cp);
			}
			
			responseResult.initResult(GTAError.Success.SUCCESS,cp);
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerEnrollmentAdvanceController.importExistInfo",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	@RequestMapping(value="/unCompletedBooking/{customerID}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkUnCompletedBookingRecordsByCustomerId(@PathVariable("customerID") @EncryptFieldInfo Long customerID){
		logger.info("CustomerEnrollmentAdvanceController.checkUnCompletedBookingRecords start ...");
		try {	
			//For every re-enrollment, HKGTA should confirm that all the future reservation/booking are handled to avoid chaos in transaction records
			if(customerProfileService.checkCloseCustomerProfile(customerID))
			{
				responseResult.initResult(GTAError.Success.SUCCESS);			
				return responseResult;
			}
			return  memberFacilityTypeBookingService.checkUnCompletedBookingRecordsByCustomerId(customerID);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerEnrollmentAdvanceController.importExistInfo",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

}
