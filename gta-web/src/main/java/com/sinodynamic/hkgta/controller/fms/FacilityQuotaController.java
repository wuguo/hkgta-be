package com.sinodynamic.hkgta.controller.fms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.fms.FacilityTypeQuotaDto;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilityType;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.FacilityTypeService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.fms.FacilityAdditionAttributeService;
import com.sinodynamic.hkgta.service.fms.FacilityMasterService;
import com.sinodynamic.hkgta.service.fms.FacilityTypeQuotaService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/facility")
public class FacilityQuotaController extends ControllerBase<FacilityMaster> {

	private Logger								logger	= Logger.getLogger(FacilityQuotaController.class);

	@Autowired
	private FacilityTypeQuotaService			facilityTypeQuotaService;

	@Autowired
	private GlobalParameterService				globalParameterService;

	@Autowired
	private FacilityTypeService					facilityTypeService;

	@Autowired
	private FacilityMasterService				facilityMasterService;

	@Autowired
	private FacilityAdditionAttributeService	facilityAdditionAttributeService;

	@RequestMapping(value = "/quota/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityQuotaList(@RequestParam(value = "facilityType") String facilityType,
			@RequestParam(value = "sortBy", defaultValue = "specificDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending, HttpServletRequest request, HttpServletResponse response) {
		logger.info("FacilityQuotaController.getFacilityQuotaList start ...");
		try {
			List<FacilityTypeQuotaDto> quotaList = facilityTypeQuotaService.getFacilityTypeQuotaList(facilityType, sortBy, isAscending);
			FacilityType type = facilityTypeService.getFacilityType(facilityType);
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			int count = facilityMasterService.countFacilityMasterByTypeAndFacilityNos(facilityType, facilityNos);

			Map<String, Object> result = new HashMap<String, Object>();
			result.put("defaultQuota", null != type ? type.getDefaultQuota() : null);
			result.put("totalQty", count);
			result.put("quotaList", quotaList);
			responseResult.initResult(GTAError.Success.SUCCESS, result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/quota", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult saveFacilityQuotaList(@RequestBody FacilityTypeQuotaDto quotaDto) {
		logger.info("FacilityQuotaController.saveFacilityQuotaList start ...");
		try {
			FacilityTypeQuotaDto quota = facilityTypeQuotaService.saveFacilityTypeQuota(quotaDto, getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS, quota);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/{facilityType}/defaultQuota", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateFacilityDefaultQuota(@PathVariable(value = "facilityType") String facilityType,
			@RequestBody FacilityTypeQuotaDto quotaDto) {

		logger.info("FacilityQuotaController.updateFacilityDefaultQuota start ...");
		try {
			quotaDto.setCreateBy(getUser().getUserId());
			FacilityTypeQuotaDto quota = facilityTypeQuotaService.saveFacilityTypeDefaultQuota(quotaDto);
			responseResult.initResult(GTAError.Success.SUCCESS, quota);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/quota/{sysId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityTypeQuota(@PathVariable long sysId) {
		logger.info("FacilityQuotaController.getFacilityQuotaList start ...");
		try {
			FacilityTypeQuotaDto quotaDto = facilityTypeQuotaService.getFacilityTypeQuotaById(sysId);
			responseResult.initResult(GTAError.Success.SUCCESS, quotaDto);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/quota/{sysId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteFacilityTypeQuota(@PathVariable long sysId) {
		logger.info("FacilityQuotaController.deleteFacilityTypeQuota start ...");
		try {
			facilityTypeQuotaService.deleteFacilityTypeQuota(sysId, getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/{facilityType}/maxQuota", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityMaxQuotaList(@PathVariable(value = "facilityType") String facilityType, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("FacilityQuotaController.getFacilityMaxQuotaList start ...");
		try {
			List<Long> facilityNos = facilityAdditionAttributeService.getFacilityAttributeFacilityNoList(Constant.ZONE_PRACTICE);
			int count = facilityMasterService.countFacilityMasterByTypeAndFacilityNos(facilityType, facilityNos);
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("maxQuota", count);
			responseResult.initResult(GTAError.Success.SUCCESS, result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@RequestMapping(value = "/{facilityType}/quota/{specificDate}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityQuotaBySpecificDate(@PathVariable(value = "facilityType") String facilityType,
			@PathVariable(value = "specificDate") String specificDate, HttpServletRequest request, HttpServletResponse response) {
		logger.info("FacilityQuotaController.getFacilityQuotaBySpecificDate start ...");
		try {
			Long quota = facilityTypeQuotaService.getFacilityQuota(facilityType, DateCalcUtil.parseDate(specificDate));
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("quota", quota);
			responseResult.initResult(GTAError.Success.SUCCESS, result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
}
