package com.sinodynamic.hkgta.controller.crm.ibeacon;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.BeaconManageDto;
import com.sinodynamic.hkgta.dto.crm.IBeaconNearbyPersonDto;
import com.sinodynamic.hkgta.dto.pms.GlobalParameterDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.ibeacon.Ibeacon;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.StaffSitePoiService;
import com.sinodynamic.hkgta.service.crm.ibeacon.IbeaconService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/Ibeacon")
@Scope("prototype") 
public class IbeaconController extends ControllerBase{
	private static final 	Logger logger = Logger.getLogger(IbeaconController.class);
	@Autowired
	private IbeaconService beaconService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private StaffSitePoiService staffSitePoiService;
	
	@Autowired
	private GlobalParameterService globalParameterService;
	
	@RequestMapping(value = "/addVistor",  method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult add(@RequestBody IBeaconNearbyPersonDto dto) {
		try {
			return beaconService.addVistorFromApp(dto);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.addVistor Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getBeaconList", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberBeacon(@RequestParam(value="sortBy", defaultValue = "createDate") String sortBy,
			   @RequestParam(value="pageNumber", defaultValue = "1") Integer pageNumber,
			   @RequestParam(value="pageSize", defaultValue = "10")  Integer pageSize,
			   @RequestParam(value="isAscending", defaultValue = "false") String isAscending,
			   @RequestParam(value="byMajor",defaultValue="") String byMajor,
			   @RequestParam(value="status", defaultValue = "ALL") String status,
			   @RequestParam(value="filters",defaultValue="", required = false) String filters,
			   @RequestParam(value="targetType", defaultValue = "MBR") String targetType
			) {
		
		if(isAscending.equals("true")){
			page.addAscending(sortBy);
		}else if (isAscending.equals("false")){
			page.addDescending(sortBy);
		}
		try {
			if(!StringUtils.isEmpty(filters)){
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		if(!StringUtils.isEmpty(byMajor)){
			if(!byMajor.matches("[0-9]{1,}")){
				responseResult.initResult(GTAError.IbeaconError.INVALID_MAJOR);
				return responseResult;
			}
		}
		if (status.equalsIgnoreCase("ALL")||status.equalsIgnoreCase(Constant.General_Status_ACT)|| status.equalsIgnoreCase(Constant.General_Status_NACT)) {
			return beaconService.getRecogBeaconList(page,byMajor,status,targetType);
		} else {
			responseResult.initResult(GTAError.IbeaconError.INVALID_STATUS);
			return responseResult;
		}

	}

	@RequestMapping(value = "/updateStatus", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateIbeaconStatus(@RequestBody BeaconManageDto dto) {
		try{
			return beaconService.changeIbeaconStatus(dto, getUser().getUserId());
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.updateIbeaconStatus Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/saveOrEditIbeacon", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult editIbeaconInfo(@RequestBody BeaconManageDto dto) {
		try{
			return beaconService.saveOrEditIbeaconInfo(dto, getUser().getUserId());
		}catch(GTACommonException ce){
			logger.error(ce.getMessage(),ce);
			logger.debug("IbeaconController.editIbeaconInfo Exception:", ce);
			responseResult.initResult(ce.getError());
			return responseResult;
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.editIbeaconInfo Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/remove/{ibeaconIdDB}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult removeIbeacon(@PathVariable Long ibeaconIdDB) {
		try{
			if(staffSitePoiService.deleteByBeaconId(ibeaconIdDB)){
				return beaconService.deleteIbeacon(ibeaconIdDB,getUser().getUserId());
			}else{
				responseResult.initResult(GTAError.CommonError.BAD_DATA_EXCEPTION);
				return responseResult;
			}
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.removeIbeacon Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/getBeaconMember", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBeaconMember(@RequestParam(defaultValue="",required=false) String poiId,@RequestParam String venueCode,
			@RequestParam(required = false, defaultValue = "50") Integer beaconMemberCount) {
		try{
			String[] splitStr = poiId.split(",");
			Long[] poiIdArray = null;
			if (!StringUtils.isEmpty(poiId)) {
				poiIdArray = new Long[splitStr.length];
				for (int i = 0; i < splitStr.length; i++) {
					poiIdArray[i] = Long.valueOf(splitStr[i]);
				}
			} else {
				poiIdArray = null;
			}
				return beaconService.getBeaconMember(poiIdArray,venueCode,beaconMemberCount);
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.getBeaconMember Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/getBeaconByLocation", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBeaconByLocation(@RequestParam String venueCode) {
		try{
			return beaconService.getBeaconByVenueCode(venueCode);
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.getBeaconByLocation Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/checkBeaconAvailability", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkAvailability(@RequestParam Long major,@RequestParam Long ibeaconId,@RequestParam(required=false,defaultValue="") Long ibeaconIdDB) {
		try{
			return beaconService.checkAvailability(major,ibeaconId,ibeaconIdDB);
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.checkAvailability Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/updateMajor", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateMajor(@RequestParam Long major, @RequestParam(value="targetType", defaultValue = "MBR") String targetType) {
		try{
			if(major==null){
				responseResult.initResult(GTAError.IbeaconError.MAJOR_INVALID);
				return responseResult;
			}
			else{
				int up = major.compareTo(new Long("65535"));
				int down = major.compareTo(new Long("0"));
				if(up==1||down==-1){
					responseResult.initResult(GTAError.IbeaconError.MAJOR_INVALID);
					return responseResult;
				}
			}
			
			return beaconService.updateMajor(major,targetType);
		}catch(GTACommonException ce){
			logger.error(ce.getMessage(),ce);
			logger.debug("IbeaconController.updateMajor Exception:", ce);
			responseResult.initResult(ce.getError());
			return responseResult;
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.updateMajor Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult advanceSearch(@RequestParam(value="module",defaultValue="MBR") String module) {
		responseResult.initResult(GTAError.Success.SUCCESS, beaconService.assembleQueryConditions(module));
		return responseResult;
	}

	@RequestMapping(value = "/rooms", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRommBeaconsList() {
		try {
			return beaconService.getRoomBeaconsList();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.getRommBeaconsList Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/nextAvailableBeaconId", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTheNextAvailableBeaconId(@RequestParam(value="major") Long major) {
		try {
			return beaconService.getTheNextAvailableMinorId(major);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.getTheNextAvailableBeaconId Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/tablet/memberByVenue", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberByBeaconIdForTablet(
			@RequestParam(value="venueCode", defaultValue = "") String venueCode,
			@RequestParam(value="sortBy", defaultValue = "lastTimestamp") String sortBy,
			@RequestParam(value="pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue = "10")  Integer pageSize,
			@RequestParam(value="isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value="resvSortBy", defaultValue = "beginDate") String resvSortBy,
			@RequestParam(value="resvPageNumber", defaultValue = "1") Integer resvPageNumber,
			@RequestParam(value="resvPageSize", defaultValue = "10")  Integer resvPageSize,
			@RequestParam(value="resvIsAscending", defaultValue = "true") String resvIsAscending) {
		try{
			ListPage<Ibeacon> page =new ListPage<Ibeacon>();
			if(isAscending.equals("true")){
				page.addAscending(sortBy);
			}else if (isAscending.equals("false")){
				page.addDescending(sortBy);
			}
			page.setNumber(pageNumber);
			page.setSize(pageSize);
			
			return beaconService.getMemberByBeaconIdForTablet(page,resvSortBy,resvPageNumber,resvPageSize,resvIsAscending,venueCode);
		}catch (Exception e){
			logger.error(e.getMessage(),e);
			logger.debug("IbeaconController.getBeaconMember Exception:", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	@RequestMapping(value = "/getIBeaconUuid", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getIBeaconUuid() {
		try {
			GlobalParameter globalIbeacon = globalParameterService.getGlobalParameter(GlobalParameterType.IBEACON_UUID.name());
			if(null!=globalIbeacon){
				GlobalParameterDto dto=new GlobalParameterDto();
				dto.setParamValue(globalIbeacon.getParamValue());
				responseResult.initResult(GTAError.Success.SUCCESS,dto);
			}
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
}
