package com.sinodynamic.hkgta.controller.ireport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.ReportDto;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;

@Controller
public class IreportController extends ControllerBase {
	public static void main(String[] args) {
		String url = IreportController.class.getResource("").getPath();
		System.out.println(url);

		String aa = null;
		String bbr = null;
		System.out.println(bbr + " ");
	}

	@Autowired
	private CustomerProfileService	customerProfileService;

	@Autowired
	private MemberService			memberService;

	@RequestMapping(value = "/getReport1", method = RequestMethod.GET)
	public String getReport(Model model) {
		List<CustomerProfile> customerProfiles = customerProfileService.getCustomers();
		List<ReportDto> reportDtos = new ArrayList<ReportDto>();

		for (CustomerProfile c : customerProfiles) {
			ReportDto reportDto = new ReportDto();
			reportDto.setCustomerId(c.getCustomerId());
			reportDto.setGivenName(c.getGivenName());
			reportDto.setSalutation(c.getSalutation());
			reportDto.setSurname(c.getSurname());
			reportDto.setPassportNo(c.getPassportNo());
			reportDtos.add(reportDto);
		}
		JRDataSource jrDataSource = new JRBeanCollectionDataSource(reportDtos);
		// model.addAttribute("url", "/WEB-INF/jasper/report1.jasper");
		model.addAttribute("url", "/WEB-INF/jasper/gta_report.jasper");
		model.addAttribute("format", "pdf"); // 报表格式
		model.addAttribute("jrMainDataSource", jrDataSource);
		return "reportView";
	}

	@RequestMapping(value = "/getReport/{customerId}", method = RequestMethod.GET, produces = "application/pdf")
	@ResponseBody
	public byte[] exportpdf(@PathVariable Long customerId, HttpServletRequest request, HttpServletResponse response) throws Exception, IOException {
		return customerProfileService.getEnrollFormReport(customerId);
		// String reportPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		// reportPath = reportPath.substring(0, reportPath.indexOf("WEB-INF")) + "WEB-INF/" + "jasper/";
		// // String ctxpath = request.getSession().getServletContext().getRealPath("/WEB-INF/jasper/EnrollmentForm.jasper");
		// String ctxpath = reportPath + "EnrollmentForm.jasper";
		// File reFile = new File(ctxpath);
		//
		// // List<CustomerProfile> customerProfiles = customerProfileService.getCustomers();
		// List<ReportDto> reportDtos = new ArrayList<ReportDto>();
		//
		// // for (CustomerProfile c : customerProfiles) {
		// // ReportDto reportDto = new ReportDto();
		// // reportDto.setCustomerId(c.getCustomerId());
		// // reportDto.setGivenName(c.getGivenName());
		// // reportDto.setSalutation(c.getSalutation());
		// // reportDto.setSurname(c.getSurname());
		// // reportDto.setPassportNo(c.getPassportNo());
		// // reportDtos.add(reportDto);
		// // }
		// CustomerProfile customerProfile = customerProfileService.getByCustomerID(customerId);
		// ReportDto reportDto = new ReportDto();
		// reportDto.setCustomerId(customerProfile.getCustomerId());
		// reportDto.setGivenName(customerProfile.getGivenName());
		// reportDto.setSalutation(customerProfile.getSalutation());
		// reportDto.setSurname(customerProfile.getSurname());
		// reportDto.setPassportNo(customerProfile.getPassportNo());
		// reportDtos.add(reportDto);
		// Map<String, Object> parameters = new HashMap<String, Object>();
		// String martialStatus = "false";
		// Member member = memberService.getMemberById(customerId);
		// // List<Member> dependentMebers = memberService.getDependentMemberList(customerId);
		// // int i = 1;
		// // for (Member tempMember : dependentMebers) {
		// // CustomerProfile tempCustomer = customerProfileService.getByCustomerID(tempMember.getCustomerId());
		// // if ("SPE".equals(tempMember.getRelationshipCode()) || "Spouse".equals(tempMember.getRelationshipCode())) {
		// // martialStatus = "true";
		// // parameters.put("d_lastName", tempCustomer.getSurname());
		// // parameters.put("d_firstName", tempCustomer.getGivenName());
		// // parameters.put("d_chineseName", tempCustomer.getSurnameNls() + tempCustomer.getGivenNameNls());
		// // parameters.put("d_birthDay", tempCustomer.getDateOfBirth().split("-")[2]);
		// // parameters.put("d_birthMonth", tempCustomer.getDateOfBirth().split("-")[1]);
		// // parameters.put("d_birthYear", tempCustomer.getDateOfBirth().split("-")[0]);
		// // parameters.put("occupation", tempCustomer.getFullBusinessNature());
		// // parameters.put("d_hkid", tempCustomer.getPassportNo());
		// // parameters.put("d_national", tempCustomer.getNationality());
		// // parameters.put("d_mobile", tempCustomer.getPhoneMobile());
		// // parameters.put("d_teleNo", tempCustomer.getPhoneHome());
		// // parameters.put("d_email", tempCustomer.getContactEmail());
		// // } else if ("CHD".equals(tempMember.getRelationshipCode()) || "Child".equals(tempMember.getRelationshipCode())) {
		// // parameters.put("c" + i + "_name", tempCustomer.getSurname() + tempCustomer.getGivenName());
		// // parameters.put("c" + i + "_hkid", tempCustomer.getPassportNo());
		// // i++;
		// // }
		// // }
		// System.out.println(customerProfile.getSurname()+"testaaaaaa"+customerProfile.getGivenName());
		// parameters.put("tick_image", reportPath + "tick_image.jpg");
		// parameters.put("image_path", reportPath + "logo_header.jpg");
		// // parameters.put("image_path", "C:\\Users\\Allen_Yu\\Desktop\\ireport\\gta_title.jpg");
		// // parameters.put("subreport_path", "C:\\Users\\Allen_Yu\\Desktop\\ireport\\gta_report_subreport.jasper");
		// parameters.put("customerId", customerId);
		// parameters.put("lastName", customerProfile.getSurname());
		// parameters.put("firstName", customerProfile.getGivenName());
		// parameters.put("chineseName", customerProfile.getSurnameNls() + customerProfile.getGivenNameNls());
		// parameters.put("hkid", customerProfile.getPassportNo());
		// parameters.put("birthDay", customerProfile.getDateOfBirth().split("-")[2]);
		// parameters.put("birthMonth", customerProfile.getDateOfBirth().split("-")[1]);
		// parameters.put("birthYear", customerProfile.getDateOfBirth().split("-")[0]);
		// parameters.put("gender", customerProfile.getGender());
		// parameters.put("national", customerProfile.getNationality());
		// parameters.put("martialStatus", martialStatus);
		// parameters.put(
		// "resAddr",
		// customerProfile.getPostalAddress1() + ", " + customerProfile.getPostalAddress2() + ", " + customerProfile.getPostalDistrict());
		// parameters.put("contactNo", customerProfile.getPhoneHome());
		// parameters.put("mobile", customerProfile.getPhoneMobile());
		// parameters.put("email", customerProfile.getContactEmail() == null ? "" : customerProfile.getContactEmail());
		// parameters.put("companyName", customerProfile.getCompanyName() == null ? "" : customerProfile.getCompanyName());
		// parameters.put("occupation", customerProfile.getFullBusinessNature() == null ? "" : customerProfile.getFullBusinessNature());
		// parameters.put("companyAddr", "");
		// parameters.put("busEmail", "");
		// parameters.put("busEmail", "");
		// parameters.put("secName", "");
		// parameters.put("busContactNo", "");
		//
		// JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(reportDtos);
		//
		// JasperPrint jasperPrint = JasperFillManager.fillReport(
		//
		// reFile.getPath(), parameters, ds);
		//
		// JRPdfExporter exporter = new JRPdfExporter();
		//
		// exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT, jasperPrint);
		//
		// exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, response.getOutputStream());
		//
		// response.setHeader("Content-Disposition", "attachment;filename=customer.pdf");
		//
		// response.setContentType("application/pdf");
		//
		// response.setCharacterEncoding("utf-8");
		//
		// exporter.exportReport();
		//
		// ByteArrayOutputStream outPut = new ByteArrayOutputStream();
		// exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		// exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPut));
		//
		// exporter.exportReport();
		// return outPut.toByteArray();

	}
}
