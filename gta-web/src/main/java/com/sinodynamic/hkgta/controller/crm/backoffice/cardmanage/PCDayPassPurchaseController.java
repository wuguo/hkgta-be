package com.sinodynamic.hkgta.controller.crm.backoffice.cardmanage;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.crm.GuestProfileDto;
import com.sinodynamic.hkgta.dto.crm.PaymentOrderDto;
import com.sinodynamic.hkgta.dto.crm.PermitCardMasterDto;
import com.sinodynamic.hkgta.dto.crm.ScanCardMappingDto;
import com.sinodynamic.hkgta.dto.memberapp.CandidateCustomerDto;
import com.sinodynamic.hkgta.dto.membership.FacilityTransactionInfo;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderPermitCardDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.CorporateMemberService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MembershipCardsManagmentService;
import com.sinodynamic.hkgta.service.crm.cardmanage.CandidateCustomerService;
import com.sinodynamic.hkgta.service.crm.cardmanage.PCDayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.leads.LeadCustomerService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CorporateServiceAccService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.CommonException;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/pccardmanage")
public class PCDayPassPurchaseController extends ControllerBase {
	private static final 	Logger							logger			= Logger.getLogger(PCDayPassPurchaseController.class);
	private static final int				NINE_DIGIT		= 9;
	private static final int				EIGHT_DIGIT		= 8;
	private static final String				PREFIX_BACKUP	= "00000000";

	@Autowired
	private PCDayPassPurchaseService		pcDayPassPurchaseService;

	@Autowired
	private AdvanceQueryService				advanceQueryService;

	@Autowired
	private LeadCustomerService				leadCustomerService;

	@Autowired
	private PermitCardMasterService			permitCardMasterService;

	@Autowired
	private CandidateCustomerService		candidateCustomerService;

	@Autowired
	private MembershipCardsManagmentService	membershipCardsManagmentService;

	@Autowired
	private CustomerProfileService			customerProfileService;
	@Autowired
	private ServicePlanService				servicePlanService;
	@Autowired
	private CustomerServiceAccService		customerServiceAccService;
	@Autowired
	private MemberService					memberService;
	@Autowired
	private CorporateServiceAccService		corporateServiceAccService;
	@Autowired
	private CorporateMemberService			corporateMemberService;

	@RequestMapping(value = "/serviceplan/{type}/{activationDate}/{deactivationDate}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDaypassListByTypeAndDate(@PathVariable(value = "type") String type,
			@PathVariable(value = "activationDate") String activationDate, @PathVariable(value = "deactivationDate") String deactivationDate,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			DaypassPurchaseDto dto = new DaypassPurchaseDto();
			dto.setActivationDate(activationDate);
			dto.setDeactivationDate(deactivationDate);
			if (StringUtils.equals("MEMBER", type)) {
				dto.setCustomerId((long) 1);
			}
			return servicePlanService.getServiceplanByDate(dto);
		} catch (Exception e) {
			logger.error("get day pass failed!", e);
			responseResult.initResult(GTAError.DayPassError.GET_DAY_PASS_FAILED);
			return responseResult;
		}

	}

	@RequestMapping(value = "/daypasspurchase/check_availability", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult checkAvailability(@RequestBody DaypassPurchaseDto dto) {
		try {
			JSONObject jsonObject = JSONObject.fromObject(dto);
			System.out.println(jsonObject);
			ResponseResult result = validateData(dto);
			if (!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
				return result;
			}
			if ("Member".equalsIgnoreCase(dto.getPurchaser())) {
				//DaypassPurchaseDto checkDay = new DaypassPurchaseDto();
				Long customerId =dto.getCustomerId() ;//dto.getId();
				Member member = this.memberService.getMemberById(customerId);
				if (Constant.Status.NACT.toString().equalsIgnoreCase(member.getStatus())) {
					responseResult.initResult(GTAError.CourseError.COURSE_ENROLLMENT_NACT_MEMBER);
					return responseResult;
				}
				DaypassPurchaseDto returnDto = pcDayPassPurchaseService.checkPurchasaeDayPassLimit(member);
				// check member can buy daypass or not
//				if (returnDto.getTotalMemberQuota() <= 0) {
//					responseResult.initResult(GTAError.DayPassError.BEYOND_PERIOD);
//					return responseResult;
//				}
				// check buy date outof availabe date of the member buy daypass or not
				String passPeriodType = returnDto.getPassPeriodType();
				Date begin = DateConvertUtil.parseString2Date(returnDto.getActivationDate(), "yyyy-MM-dd");
				Date end = DateConvertUtil.parseString2Date(returnDto.getDeactivationDate(), "yyyy-MM-dd");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				if (begin.after(sdf.parse(dto.getActivationDate())) || end.before(sdf.parse(dto.getDeactivationDate()))) {
					responseResult.initResult(GTAError.DayPassError.OutofDay);
					return responseResult;
				}
				// WD means only can buy weekday
				if ("WD".equalsIgnoreCase(passPeriodType)) {
					Calendar beginDay = Calendar.getInstance();
					beginDay.setTime(sdf.parse(dto.getActivationDate()));
					Calendar endDay = Calendar.getInstance();
					endDay.setTime(sdf.parse(dto.getDeactivationDate()));
					if (beginDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || beginDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
							|| endDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || endDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						responseResult.initResult(GTAError.DayPassError.PERIOD_DATE_NOT_ALLOW);
						return responseResult;
					}
					int total = endDay.get(Calendar.DAY_OF_YEAR) - beginDay.get(Calendar.DAY_OF_YEAR);
					for (int i = 0; i < total; i++) {
						Calendar tempDay = Calendar.getInstance();
						tempDay.setTime(sdf.parse(dto.getActivationDate()));
						tempDay.add(Calendar.DATE, i);
						if (tempDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || tempDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
							responseResult.initResult(GTAError.DayPassError.PERIOD_DATE_NOT_ALLOW);
							return responseResult;
						}
					}
				}

			}
			return this.pcDayPassPurchaseService.checkAvailability(dto);
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.CHECKAVAILABILITY);
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}
	}

	/*
	 * public ResponseResult checkAvailability(@RequestBody DaypassPurchaseDto dto) { try { JSONObject jsonObject = JSONObject.fromObject(dto);
	 * System.out.println(jsonObject); ResponseResult result = validateData(dto); if
	 * (!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) { return result; } if(null!=dto.getCardNo()){ result =
	 * advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(dto.getCardNo())));
	 * if(!GTAError.CommomError.GET_MEMBER_BYQRCODE_SUCC.getCode().equals(result.getReturnCode())){ return result; } ScanCardMappingDto cardMappingDto
	 * = (ScanCardMappingDto) result.getData(); Long customerId = cardMappingDto.getCustomerId(); dto.setId(customerId+"");
	 * dto.setAcdemyID(cardMappingDto.getAcademyNo()); DaypassPurchaseDto checkDay = new DaypassPurchaseDto(); Member member =
	 * this.memberService.getMemberById(customerId); String type = member.getMemberType(); if(Constant.memberType.CPM.toString().equals(type) ||
	 * Constant.memberType.IPM.toString().equals(type)){ checkDay.setCustomerId(customerId); }else if(Constant.memberType.CDM.toString().equals(type)
	 * || Constant.memberType.IDM.toString().equals(type)){ checkDay.setCustomerId(member.getSuperiorMemberId()); } //check member can buy daypass or
	 * not boolean isPermit = pcDayPassPurchaseService.checkAddDayPassLimit(checkDay); if (!isPermit) {
	 * responseResult.initResult(GTAError.DayPassError.HAVE_NOT_GET_PURCHASE_RIGHT); return responseResult; } //check buy date outof availabe date of
	 * the member buy daypass or not CustomerServiceAcc customerServiceAcc = null; CorporateServiceAccDto corporateServiceAcc = null; CorporateMember
	 * cm = null; Date begin = null; Date end = null; if(Constant.memberType.IPM.toString().equals(type)){ customerServiceAcc =
	 * this.customerServiceAccService.getAactiveByCustomerId(customerId); begin = customerServiceAcc.getEffectiveDate(); end =
	 * customerServiceAcc.getExpiryDate(); }else if(Constant.memberType.IDM.toString().equals(type)){ customerServiceAcc =
	 * this.customerServiceAccService.getAactiveByCustomerId(member.getSuperiorMemberId()); begin = customerServiceAcc.getEffectiveDate(); end =
	 * customerServiceAcc.getExpiryDate(); }else if(Constant.memberType.CPM.toString().equals(type)){ cm =
	 * this.corporateMemberService.getCorporateMemberById(member.getCustomerId()); corporateServiceAcc =
	 * corporateServiceAccService.getAactiveByCustomerId(cm.getCorporateProfile().getCorporateId()); begin = corporateServiceAcc.getEffectiveDate();
	 * end = corporateServiceAcc.getExpiryDate(); }else if(Constant.memberType.CDM.toString().equals(type)){ cm =
	 * this.corporateMemberService.getCorporateMemberById(member.getSuperiorMemberId()); corporateServiceAcc =
	 * corporateServiceAccService.getAactiveByCustomerId(cm.getCorporateProfile().getCorporateId()); begin = corporateServiceAcc.getEffectiveDate();
	 * end = corporateServiceAcc.getExpiryDate(); } SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	 * if(begin.after(sdf.parse(dto.getActivationDate())) || end.before(sdf.parse(dto.getDeactivationDate()))){
	 * responseResult.initResult(GTAError.DayPassError.DAY_PASS_CHECK_ERROR); return responseResult; } } return
	 * this.pcDayPassPurchaseService.checkAvailability(dto); } catch (Exception e) {
	 * this.responseResult.initResult(GTAError.DayPassPurchaseError.CHECKAVAILABILITY, new Object[]{e.getMessage()});
	 * logger.error(this.responseResult.getReturnCode(), e); return this.responseResult; } }
	 */

	/**
	 * 
	 * @param dto
	 * @return this method used for save button save order
	 */
	@RequestMapping(value = "/daypasspurchase", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult saveDayPassPurchase(@RequestBody DaypassPurchaseDto dto) {
		try {
			LoginUser currentUser = getUser();
			ResponseResult result = validateData(dto);
			if (!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
				return result;
			}
			return this.pcDayPassPurchaseService.saveDayPassPurchase(dto, currentUser.getUserId());
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.SAVEDAYPASSPURCHASE);
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}
	}

	private ScanCardMappingDto getScanCardMappingDto(String cardNo) throws Exception {
		if (null != cardNo) {
			ResponseResult result = advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(cardNo)));
			if (!GTAError.CommomError.GET_MEMBER_BYQRCODE_SUCC.getCode().equals(result.getReturnCode())) {
				throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[] { "Customer cardNo not right:" + cardNo });
			}
			ScanCardMappingDto cardMappingDto = (ScanCardMappingDto) result.getData();
			return cardMappingDto;
		}
		return null;
	}

	/**
	 * 
	 * @param dto
	 * @return this method used for purchase button.If order not exist call saveDayPassPurchase() to save order
	 */
	@RequestMapping(value = "/daypasspurchase", method = RequestMethod.PUT, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult purchaseDaypass(@RequestBody DaypassPurchaseDto dto) {
		try {
			LoginUser currentUser = getUser();
			ResponseResult result = validateData(dto);
			if (!GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
				return result;
			}

			return this.pcDayPassPurchaseService.purchaseDaypass(dto, currentUser.getUserId());
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.PURCHASEDAYPASS);
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}

	}

	/**
	 * 
	 * @param dto
	 * @return this method used for pay purchase daypass submit button really pay money
	 */
	@RequestMapping(value = "/paydaypasspurchase", method = RequestMethod.PUT, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult payDaypassPurchase(@RequestBody DaypassPurchaseDto dto) {
		try {
			LoginUser currentUser = getUser();
			if (null == dto.getOrderNo()) {
				this.responseResult.initResult(GTAError.DayPassPurchaseError.OrderNO_NULL);
				return this.responseResult;
			}
			if (StringUtils.isEmpty(dto.getPaymentMethodCode().trim())) {
				this.responseResult.initResult(GTAError.DayPassPurchaseError.PaymentMethodCode_NULL);
				return this.responseResult;
			}
			this.responseResult = this.pcDayPassPurchaseService.payDaypassPurchase(dto, currentUser.getUserId(), this.getLoginLocation());
			if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(this.responseResult.getReturnCode())
					&& !Constant.CREDIT_CARD.equals(dto.getPaymentMethodCode())&&
					!Constant.UNION_PAY.equals(dto.getPaymentMethodCode())) {
				Thread emailThread = new Thread(new EmailThread(dto, currentUser.getUserId()));
				emailThread.start();
			}
			return this.responseResult;
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.PAYDAYPASSPURCHASE);
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}

	}

	/**
	 * 
	 * @param orderNo
	 * @param acdemyID
	 * @param id
	 * @param memberName
	 * @param cardNo
	 * @return used for right menu's pay funtion
	 */
	@RequestMapping(value = "/paymentdaypass", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult payment(@RequestParam(value = "orderNo", required = true) Long orderNo,
			@RequestParam(value = "acdemyID", required = false) String acdemyID, @RequestParam(value = "customerId", required = false)@EncryptFieldInfo Long customerId,
			@RequestParam(value = "memberName", required = false) String memberName, @RequestParam(value = "cardNo", required = false) String cardNo) {
		try {
			if (null == orderNo) {
				this.responseResult.initResult(GTAError.DayPassPurchaseError.OrderNO_NULL);
				return this.responseResult;
			}
			return this.pcDayPassPurchaseService.payment(orderNo, acdemyID, customerId, memberName, cardNo);
		} catch (Exception e) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.Payment, new Object[] { e.getMessage() });
			logger.error(this.responseResult.getReturnCode(), e);
			return this.responseResult;
		}

	}

	@RequestMapping(value = "/cardStatus", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult changeDaypassCardStatus(@RequestBody CustomerOrderPermitCardDto dto) {
		try {
			logger.info("DayPassPurchaseController.changeDaypassStatus start..." + dto);
			if (dto == null || dto.getOrderDetId() == null || StringUtils.isEmpty(dto.getStatus())) {
				responseResult.initResult(GTAError.DayPassError.DAY_PASS_CARD_UPDATE_PARAM_ERROR);
				return responseResult;
			}
			if (pcDayPassPurchaseService.changeDaypassCardStatus(dto)) {
				logger.info("DayPassPurchaseController.changeDaypassStatus end...");
				responseResult.initResult(GTAError.Success.SUCCESS);
			} else {
				responseResult.initResult(GTAError.DayPassError.DAY_PASS_CARD_UPDATE_STATUS_FAILED);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.DayPassError.DAY_PASS_CARD_UPDATE_PARAM_ERROR);
		}
		return responseResult;
	}

	@RequestMapping(value = "/paymentorder/{orderNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPaymentListBySql(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "paymentNo") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending, @PathVariable(value = "orderNo") String orderNo,
			HttpServletRequest request, HttpServletResponse response) {
		String sql = " SELECT t.*, CONCAT(c.salutation,' ',c.given_Name,' ',c.surname) memberName, concat(t.planName,'(',t.ageRange,')') daypassType  "
				+ "	FROM (   "
				+ "		SELECT copc.qr_code paymentNo, copc.effective_from activationDate,copc.linked_card_no cardNo,copc.cardholder_customer_id customerId,sp.plan_name planName,spp.age_range_code ageRange,copc.customer_order_no orderNo "
				+ "		FROM customer_order_permit_card copc, service_plan sp, service_plan_pos spp "
				+ " 		WHERE copc.service_plan_no=sp.plan_no AND copc.service_plan_no=spp.plan_no "
				+ "   ) t "
				/* +" LEFT JOIN permit_card_master pcm ON pcm.card_no=t.cardNo " */
				+ " LEFT JOIN customer_profile c ON c.customer_id =t.customerId "
				+ " LEFT JOIN sys_code s on s.code_value=t.ageRange  and s.category='ageRange' ";

		if (orderNo != null) {
			if (StringUtils.startsWith(orderNo.toUpperCase(), "DP")) {
				if (orderNo.substring(2).length() > 8) {
					sql += " where t.cardNo=" + orderNo.substring(2).substring(0, 8);
				} else {
					sql += " where t.paymentNo=" + orderNo.substring(2);
				}
			} else {
				sql += " where t.orderNo=" + orderNo;
			}
			try {
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getInitialQueryResultBySQL(queryDto, sql, page, PaymentOrderDto.class);
			} catch (RuntimeException e) {
				logger.error(e);
				responseResult.initResult(GTAError.DayPassError.GET_PAYMENT_LIST_FAILED);
				return responseResult;
			}
		}
		responseResult.initResult(GTAError.DayPassError.GET_PAYMENT_LIST_FAILED);
		return responseResult;

	}

	/**
	 * Method to get CandidateCustomer info
	 * 
	 * @param daypassId
	 * @return ResponseResult.
	 */
	@RequestMapping(value = "/getCandidateCustomer/{daypassId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCandidateCustomer(@PathVariable Long daypassId) {
		try {
			return candidateCustomerService.getCandidateCustomer(daypassId);
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * Method to get CandidateCustomer info for member app
	 * 
	 * @param daypassId
	 * @return CandidateCustomer.
	 */
	@RequestMapping(value = "/getCandidateCustomerInfo/{daypassId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCandidateCustomerInfo(@PathVariable Long daypassId) {
		try {
			return candidateCustomerService.getCandidateCustomerInfo(daypassId);
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * Method to get CandidateCustomer info
	 * 
	 * @param daypassId
	 * @return CandidateCustomer.
	 */
	@RequestMapping(value = "/editCandidateCustomer", method = { RequestMethod.PUT, RequestMethod.POST })
	@ResponseBody
	public ResponseResult editCandidateCustomer(@RequestBody CandidateCustomerDto candidateCustomerDto) {
		try {
			return candidateCustomerService.editCandidateCustomer(candidateCustomerDto);
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param isAscending
	 * @param customerId
	 * @param daypassType
	 * @return member's complete daypass list(effective or history list)
	 */
	@RequestMapping(value = "/getMemberDaypass", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberDaypass(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "activationDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending, @RequestParam(value = "customerId") String customerId,
			@RequestParam(value = "daypassType", defaultValue = "effective") String daypassType,
			@RequestParam(value = "filterBy", defaultValue = "") String filterBy) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT t.*, (CASE WHEN t.customerId is NULL then CONCAT(cc.given_Name,' ', cc.surname)   ");
		stringBuilder.append(" else CONCAT(c.salutation, ' ',c.given_Name,' ', c.surname) END) memberName, ");
		stringBuilder.append(" concat(t.planName,'(',t.ageRange,')') daypassType  ");
		stringBuilder
				.append(" FROM ( SELECT copc.qr_code paymentNo,copc.effective_from activationDate,copc.effective_to expiryDate,copc.linked_card_no cardNo,");
		stringBuilder
				.append(" copc.cardholder_customer_id customerId,sp.plan_name planName,spp.age_range_code ageRange,copc.customer_order_no orderNo,copc.candidate_customer_id candidateId  ");
		stringBuilder.append("  FROM customer_order_permit_card copc, service_plan sp, service_plan_pos spp  ");
		stringBuilder.append(" WHERE  copc.service_plan_no=sp.plan_no AND copc.service_plan_no=spp.plan_no    ) t  ");
		stringBuilder.append(" LEFT JOIN customer_profile c  ON c.customer_id =t.customerId  ");
		stringBuilder.append(" LEFT JOIN sys_code s  on s.code_value=t.ageRange and s.category='ageRange'  ");
		stringBuilder.append(" LEFT JOIN candidate_customer cc ON t.candidateId = cc.candidate_id  ");
		stringBuilder.append(" where t.orderNo in (select t.order_no orderNo from customer_order_hd  t  ");
		if ("effective".equalsIgnoreCase(daypassType)) {
			stringBuilder
					.append("	where exists( select 1  from customer_order_permit_card tt  where tt.customer_order_no=t.order_no and t.order_status = 'CMP' and    tt.effective_to >= CURRENT_DATE )  ");
		} else {
			stringBuilder
					.append("	where exists( select 1  from customer_order_permit_card tt  where tt.customer_order_no=t.order_no and t.order_status = 'CMP' and    tt.effective_to < CURRENT_DATE )  ");
		}
		stringBuilder.append("	and  t.customer_id = " + customerId + " )");
		try {
			if ("ALL".equalsIgnoreCase(filterBy)) {// in order to display all the records in one set
				responseResult = pcDayPassPurchaseService.getMemberDayPass(stringBuilder.toString(), sortBy, isAscending);
			} else {
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				responseResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, stringBuilder.toString(), page, PaymentOrderDto.class);
			}

			List list = null;
			if (responseResult.getListData() != null) {
				list = responseResult.getListData().getList();
			}

			if (list == null) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				Data emptyData = new Data();
				emptyData.setList(new ArrayList());
				responseResult.setData(emptyData);
				return responseResult;
			}

			for (Object object : list) {
				PaymentOrderDto paymentOrderDto = (PaymentOrderDto) object;
				String startDateString = paymentOrderDto.getStartDate();
				String expiryDateString = paymentOrderDto.getExpiryDate();
				Date startDate = DateConvertUtil.parseString2Date(startDateString, "yyyy-MMM-dd");
				Date expiryDate = DateConvertUtil.parseString2Date(expiryDateString, "yyyy-MMM-dd");
				// Convert them to the same format for compare them exactly.
				Date today = DateConvertUtil.parseString2Date(DateConvertUtil.parseDate2String(new Date(), "yyyy-MMM-dd"), "yyyy-MMM-dd");
				if (startDate.compareTo(today) <= 0 && expiryDate.compareTo(today) >= 0) {
					paymentOrderDto.setToday(true);
				}
				if (paymentOrderDto.getStartDate().equals(paymentOrderDto.getExpiryDate())) {
					paymentOrderDto.setPeriodDate(DateConvertUtil.getYMDDateAndDateDiff(startDate));
				} else {
					paymentOrderDto.setPeriodDate(DateConvertUtil.parseDate2String(startDate, "yyyy MMM dd") + " - "
							+ DateConvertUtil.parseDate2String(expiryDate, "MMM dd"));
				}
			}
			return responseResult;
		} catch (Exception e) {
			logger.error("DayPassPurchaseController.getMemberDaypass", e);
			responseResult.initResult(GTAError.DayPassError.GET_PAYMENT_LIST_FAILED);
			return responseResult;
		}

	}

	@RequestMapping(value = "/cardmapping/{customerId}/{qrcode}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult cardMappingCustomer(@PathVariable(value = "customerId") BigInteger customerId,
			@PathVariable(value = "qrcode") BigInteger qrcode, HttpServletRequest request, HttpServletResponse response) {

		try {
			PaymentOrderDto dto = new PaymentOrderDto();
			dto.setCustomerId(customerId);
			dto.setPaymentNo(qrcode);
			if (pcDayPassPurchaseService.updateCustomerId2CustomerOrderPermitCard(dto)) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
		} catch (RuntimeException e) {
			logger.error("cardMappingCustomer update failed ", e);
		}
		responseResult.initResult(GTAError.DayPassPurchaseError.UPDATE_CUSTOMER_TO_CUSTOMERORDERPERMITCARD_FAILED);
		return responseResult;
	}

	private DaypassPurchaseDto setRuleDto(Long customerId, Long planNo, String activationDate, String deactivationDate) {
		DaypassPurchaseDto dto = new DaypassPurchaseDto();
		dto.setCustomerId(customerId);
		dto.setPlanNo(planNo);
		dto.setActivationDate(activationDate);
		dto.setDeactivationDate(deactivationDate);
		return dto;
	}

	public ResponseResult validateData(DaypassPurchaseDto dto) {
		if (null == dto.getActivationDate()) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.ACTIVATIONDATE);
			return this.responseResult;
		}
		if (null == dto.getDeactivationDate()) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.DEACTIVATIONDATE);
			return this.responseResult;
		}
		Long[] buysNos = dto.getBuysNos();
		Long[] daypassNos = dto.getDaypassNos();
		if (null == buysNos || buysNos.length <= 0) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.BUYNO_MISS);
			return this.responseResult;
		}
		if (null == daypassNos || daypassNos.length <= 0) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.DAYPASSNO_MISS);
			return this.responseResult;
		}
		if (daypassNos.length != buysNos.length) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.NOT_MATCH);
			return this.responseResult;
		}
		Long sum = (long) 0;
		for (Long temp : buysNos) {
			sum = sum + temp;
		}
		if (null == sum || 0 == sum) {
			this.responseResult.initResult(GTAError.DayPassPurchaseError.BUY_ZERO);
			return this.responseResult;
		}
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		return this.responseResult;
	}

	/**
	 * Method to register the guest and create the related customer enrollment detail
	 *
	 * @throws Exception
	 */
	@RequestMapping(value = "/guest", method = { RequestMethod.POST, RequestMethod.PUT })
	@ResponseBody
	public ResponseResult guestRegistrationSave(@RequestBody GuestProfileDto guestProfileDto) {
		try {
			String loginUserId = getUser().getUserId();
			guestProfileDto.setLoginUserId(loginUserId);
			guestProfileDto.setSalesFollowBy(loginUserId);
			ResponseResult result = pcDayPassPurchaseService.saveOrUpdateDayPassGuestInfo(guestProfileDto);
			if ("0".equals(result.getReturnCode())) {
				CustomerProfile customerProfile = (CustomerProfile) result.getData();
				customerProfileService.moveProfileAndSignatureFile(customerProfile);
			}
			return result;
		} catch (CommonException ce) {
			logger.error("LeadCustomerController Error Message:", ce);
			return new ResponseResult("1", ce.getErrorMessage());
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			return new ResponseResult("1", "", "Unexpected Exception Catched!");
		}
	}

	@RequestMapping(value = "/scancard/{cardNo}/{orderNo}", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult scanQRCode(@PathVariable("cardNo") String cardNo, @PathVariable("orderNo") Long orderNo) {
		DaypassPurchaseDto dto = null;
		try {
			ResponseResult result = advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(cardNo)));
			if (!GTAError.CommomError.GET_MEMBER_BYQRCODE_SUCC.getCode().equals(result.getReturnCode())) {
				return result;
			}
			ScanCardMappingDto cardMappingDto = (ScanCardMappingDto) result.getData();
			dto = this.pcDayPassPurchaseService.scanQRCode(cardMappingDto, orderNo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new GTACommonException(GTAError.CommomError.DATA_ISSUE, new String[] { e.getMessage() });
		}
		this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return this.responseResult;
	}

	/**
	 * link card ,update cardno to customer_order_permit_card customer enrollment detail
	 *
	 * @throws Exception
	 */
	@RequestMapping(value = "/card/{qrcode}/{cardNo}", method = { RequestMethod.PUT })
	@ResponseBody
	public ResponseMsg linkCard(@PathVariable("cardNo") String cardNo, @PathVariable("qrcode") Long qrcode, @RequestBody PermitCardMasterDto dto)
			throws Exception {

		LoginUser user = super.getUser();
		if (cardNo == null || qrcode == null || user == null) {
			responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_PARAMETER_IS_WRONG);
			return responseResult;
		}
		String userId = user.getUserId();
		dto.setUpdateBy(userId);
		Long customerId = dto.getCustomerId();
		if (membershipCardsManagmentService.hasIssuedCard(customerId)) {
			responseResult.initResult(GTAError.CommonError.MEMBER_ALREADY_LINK_DAYPASS_CARD);
//			responseResult.initResult(GTAError.CommonError.Error, "The member has already linked a day pass card.");
			return responseResult;
		}
		String result = membershipCardsManagmentService.linkCardForMember(cardNoTransfer(cardNo), customerId, userId);
		if ("0".equals(result)) {
			CustomerOrderPermitCard card = new CustomerOrderPermitCard();
			card.setQrCode(qrcode);
			card.setLinkedCardNo(Long.parseLong(cardNoTransfer(cardNo)));
			card.setLinkedCardDate(new Date());
			if (pcDayPassPurchaseService.saveOrUpdateCustomerOrderPermitCard(card)) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
		}

		responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_FAILED);
		return responseResult;
	}

	private String cardNoTransfer(String cardNo) throws Exception {

		if (!cardNo.toLowerCase().startsWith("d")) {
			throw new GTACommonException(GTAError.DayPassPurchaseError.CARD_NO_IS_INVALID);
		}

		cardNo = cardNo.substring(1);

		if (CommUtil.nvl(cardNo).length() == 0)
			return null;

		String cardTemp;
		int length = cardNo.length();
		if (length == NINE_DIGIT) {
			cardTemp = cardNo.substring(0, cardNo.length() - 1);
		} else if (length == EIGHT_DIGIT) {
			cardTemp = cardNo;
		} else {
			throw new GTACommonException(GTAError.DayPassPurchaseError.CARD_NO_IS_INVALID);
		}

		String cardNoStr = cardTemp.replaceFirst("^0*", "");
		return cardNoStr;
	}

	private String cardNoBackup(String cardNo) {

		if (CommUtil.nvl(cardNo).length() == 0)
			return null;
		String cardTemp = PREFIX_BACKUP + cardNo;
		int length = cardTemp.length();
		return cardTemp.substring(length - EIGHT_DIGIT, length);
	}

	@RequestMapping(value = "/replacement/{cardNo}/{qrcode}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg replaceCustomerCard(@PathVariable(value = "cardNo") String oldCardNo, @PathVariable(value = "qrcode") Long qrcode,
			@RequestBody PermitCardMasterDto dto) throws Exception {
		logger.info("DayPassPurchaseController.replaceCustomerCard invocation start ...");
		String newCardNo = dto.getCardNo();
		if (CommUtil.nvl(newCardNo, null) == null || CommUtil.nvl(String.valueOf(oldCardNo), null) == null || !StringUtils.isNumeric(newCardNo)) {
			logger.error("Request parameter error!");
			responseMsg.initResult(GTAError.CommonError.REQUEST_PARAMETER_ERROR);
//			responseMsg.initResult(GTAError.CommonError.Error, "Request parameter error!");
			return responseMsg;
		}
		LoginUser user = super.getUser();
		if (user == null) {
			logger.error("current user is null!");
			responseMsg.initResult(GTAError.CommonError.USER_IS_NULL);
//			responseMsg.initResult(GTAError.CommonError.Error, "can not get current user!");
			return responseMsg;
		}
		dto.setUpdateBy(user.getUserId());
		dto.setCardNo(cardNoTransfer(newCardNo));
		permitCardMasterService.replaceOldPermitCardMaster(oldCardNo, dto);
		CustomerOrderPermitCard card = new CustomerOrderPermitCard();
		card.setQrCode(qrcode);
		card.setLinkedCardNo(Long.valueOf(cardNoTransfer(newCardNo)));
		card.setLinkedCardDate(new Date());
		if (pcDayPassPurchaseService.saveOrUpdateCustomerOrderPermitCard(card)) {
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
		logger.info("DayPassPurchaseController.replaceCustomerCard invocation end ...");
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/dispose/{qrcode}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg disposeCard(@PathVariable(value = "qrcode") Long qrcode) throws Exception {
		logger.info("DayPassPurchaseController.disposeCustomerCard invocation start ...");

		LoginUser user = super.getUser();
		String userId = user.getUserId();

		ResponseResult result = pcDayPassPurchaseService.searchDayPassById("DP" + qrcode);
		if (null == result.getListData()) {
			responseResult.initResult(GTAError.DayPassPurchaseError.DOES_NOT_LINK_CARD);
			return responseResult;
		}

		List<PaymentOrderDto> daypass = (List<PaymentOrderDto>) result.getListData().getList();
		Integer linkedCardNo = daypass.get(0).getCardNo();
		if (null == linkedCardNo) {
			responseResult.initResult(GTAError.DayPassPurchaseError.DOES_NOT_LINK_CARD);
			return responseResult;
		}

		CustomerOrderPermitCard card = new CustomerOrderPermitCard();
		card.setQrCode(qrcode);
		card.setLinkedCardNo(null);
		card.setLinkedCardDate(null);
		pcDayPassPurchaseService.saveOrUpdateCustomerOrderPermitCard(card);
		permitCardMasterService.disposeCard(linkedCardNo.toString(), userId);

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/returnCard/{cardNo}", method = { RequestMethod.PUT })
	@ResponseBody
	public ResponseMsg returnCard(@PathVariable("cardNo") String cardNo) throws Exception {

		LoginUser user = super.getUser();
		if (cardNo == null || !cardNo.toLowerCase().startsWith("d")) {
			responseResult.initResult(GTAError.DayPassPurchaseError.LINK_CARD_PARAMETER_IS_WRONG);
			return responseResult;
		}
		cardNo = cardNoTransfer(cardNo);
		String userId = user.getUserId();

		CustomerOrderPermitCard card = pcDayPassPurchaseService.getDayPassCardByCardNo(cardNo);
		if (card != null) {
			card.setLinkedCardNo(null);
			card.setLinkedCardDate(null);
		}
		// permitCardMasterService.returnCard(cardNo.toString(), userId);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/viewTransaction", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult viewTransaction(@RequestParam(value = "orderNo", required = true) Long orderNo) {
		List<FacilityTransactionInfo> transactionInfos = this.servicePlanService.getTransactionInfoByOrderNo(orderNo);
		this.responseResult.initResult(GTAError.Success.SUCCESS, transactionInfos);
		return this.responseResult;
	}

	@RequestMapping(value = "/viewDayPassTransaction", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult viewDayPassTransaction(@RequestParam(value = "orderNo", required = true) Long orderNo) {
		List<FacilityTransactionInfo> transactionInfos = this.servicePlanService.getTransactionInfoByOrderNo(orderNo);
		this.responseResult.initResult(GTAError.Success.SUCCESS, transactionInfos.size() > 0 ? transactionInfos.get(0) : transactionInfos);
		return this.responseResult;
	}

	private class EmailThread implements Runnable {

		private DaypassPurchaseDto	dto;
		private String				userId;

		public EmailThread(DaypassPurchaseDto dto, String userId) {
			this.dto = dto;
			this.userId = userId;
		}

		@Override
		public void run() {
			pcDayPassPurchaseService.sendEmail(dto, userId);
		}
	}

	@RequestMapping(value = "/getDaypassOrder", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDaypassOrder(@RequestParam(value = "orderNo") Long orderNo) {
		CustomerOrderHd orderHd = this.pcDayPassPurchaseService.getDaypassOrder(orderNo);
		String status = "Fail";
		Map<String, Object> orderStatus = new HashMap<String, Object>();
		if ("CMP".equalsIgnoreCase(orderHd.getOrderStatus())) {
			List<FacilityTransactionInfo> transactionInfos = this.servicePlanService.getTransactionInfoByOrderNo(orderNo);
			orderStatus.put("transactionInfo", transactionInfos);
			status = "SUC";
		}
		orderStatus.put("status", status);
		this.responseResult.initResult(GTAError.Success.SUCCESS, orderStatus);
		return this.responseResult;
	}
}