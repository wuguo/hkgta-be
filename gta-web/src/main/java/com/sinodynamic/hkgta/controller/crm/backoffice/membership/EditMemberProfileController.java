package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.adm.UserRecordActionLogService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.EditMemberProfileService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.UserRecordActionType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/membership")
public class EditMemberProfileController extends ControllerBase {

	private Logger						logger	= Logger.getLogger(EditMemberProfileController.class);

	@Autowired
	private EditMemberProfileService	editMemberProfileService;

	@Autowired
	private CustomerProfileService		customerProfileService;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;
	
	@Autowired
	private UserRecordActionLogService userRecordActionLogService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg editEnrollment(HttpServletRequest request, HttpServletResponse response,@RequestBody CustomerProfile cProfile) {
		try {
			LoginUser currentUser = getUser();
			logger.debug("editMemberProfile controller parameter: givenName =" + cProfile.getGivenName());
			cProfile.setUpdateBy(this.getUser().getUserId());
			
			CustomerProfile targetProfile = customerProfileService.getCustomerInfoByCustomerID(cProfile.getCustomerId());
			
			if("true".equals(cProfile.getImportStatus())){
				responseMsg = customerEnrollmentService.editCorporateEnrollment(cProfile, currentUser.getUserId(),currentUser.getUserName());
			}else{//exists memeber
				responseMsg = editMemberProfileService.editMemberProfile(cProfile, currentUser.getUserId());
			}
			/*
			 * Note: The Profile/Signature only be updated in the following method.
			 */
			if ("0".equals(responseMsg.getReturnCode())) {
				/***
				 * log user_record_action_log  
				 */
//				targetProfile.setPortraitPhoto(cProfile.getPortraitPhoto());
//				targetProfile.setSignature(cProfile.getSignature());
				userRecordActionLogService.save(targetProfile,currentUser.getUserId(),UserRecordActionType.C.name());
				
				customerProfileService.moveProfileAndSignatureFile(cProfile);
				
			}
			return responseMsg;
		} catch (HibernateOptimisticLockingFailureException concurrent) {
			// concurrent.printStackTrace();
			logger.error("EditMemberProfileController.editEnrollment Exception", concurrent);
			responseMsg.initResult(GTAError.CommonError.BEEN_UPDATED_OR_DELETED, new String[] { "member profile" });
			return responseMsg;
		} catch (GTACommonException gta) {
			// gta.printStackTrace();
			logger.error("EditMemberProfileController.editEnrollment Exception", gta);
			responseMsg.initResult(gta.getError(), gta.getArgs());
			return responseMsg;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("EditMemberProfileController.editEnrollment Exception", e);
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
}
