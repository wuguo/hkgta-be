package com.sinodynamic.hkgta.controller.common;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.EntityHistoryDto;
import com.sinodynamic.hkgta.entity.pms.StaffRoster;
import com.sinodynamic.hkgta.filter.EncryptTool;
import com.sinodynamic.hkgta.filter.EncryptTool.EncryptItem;
import com.sinodynamic.hkgta.service.common.EntityHistoryService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/common")
public class EntityHistoryController extends ControllerBase<StaffRoster> {

    @Autowired
    private EntityHistoryService entityHistoryService;
    
    @Autowired
    private AOPEncryptInterceptor aopEncryptInterceptor;

    @RequestMapping(value = "/entityHistory", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult pushRegister(@RequestParam(value = "route", required = false) String route,
                                       @RequestParam(value = "name", required = true) String[] names,
                                       @RequestParam(value = "value", required = true) String[] values
    ) {
        try {
        	
        	for (int i=0 ; i<names.length; i++)
            {
            	for (EncryptItem item : EncryptItem.values())
            	{
            		if (names[i].equalsIgnoreCase(item.getCode()))
            		{
            			//values[i] = EncryptTool.getInstance().decryptIds(values[i], true);
            			values[i] = aopEncryptInterceptor.decryptLongByDES(Long.valueOf(values[i])).toString();
            		}
            	}
            }
        	
            EntityHistoryDto entityHistory = entityHistoryService.getEntityHistory(route, names, values);
            responseResult.initResult(GTAError.Success.SUCCESS, entityHistory);
        } catch (GTACommonException e) {
            logger.error(e.getMessage(), e);
            responseResult.initResult(e.getError());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            responseResult.setErrorMsg(initErrorMsg(GTAError.CommonError.UNEXPECTED_EXCEPTION));
        }
        return responseResult;
    }
}
