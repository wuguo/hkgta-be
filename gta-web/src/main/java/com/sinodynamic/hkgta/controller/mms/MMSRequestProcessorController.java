package com.sinodynamic.hkgta.controller.mms;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SettleMMSPaymentByCashValueResponseDto;
import com.sinodynamic.hkgta.dto.mms.SettleMMSPaymentByGuestFoliorResponseDto;
import com.sinodynamic.hkgta.dto.pms.ChargePostingDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.mms.MMSService;
import com.sinodynamic.hkgta.service.pms.HotelReservationService;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.ChargeType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.MessageResult;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value="/mmsintegration")
public class MMSRequestProcessorController extends ControllerBase{
	private Logger	mmsLog	= Logger.getLogger(LoggerType.MMS.getName());
	
	@Value(value ="#{spaProperties['TOKEN_KEY']}")
	private String TOKEN_KEY;
	
	@Value(value ="#{spaProperties['TOKEY_KEY_UUID']}")
	private String TOKEY_KEY_UUID;	
		
	@Autowired
	private MMSService mmsService;
	
	@Autowired
	private PMSApiService PMSApiService;
	
	@Autowired
	private HotelReservationService hotelReservationService;
	
	@Autowired
	private MemberService memberService;	
		
	@ResponseBody 
	@RequestMapping(value="/settleMMSPaymentByCashValue",method=RequestMethod.POST)
	public SettleMMSPaymentByCashValueResponseDto settleMMSPaymentByCashValue(HttpServletRequest request,
					@RequestParam(value = "invoiceId", required=true) String invoiceId,
					@RequestParam(value = "amount", required=true) BigDecimal amount,
					@RequestParam(value = "appointmentInfo", required=true) String appointmentInfo){			
//		logger.info("apikey:" + token);	
		mmsLog.info("MMS settle payment by Cash Value start...");
		String token = request.getHeader("apikey");
		String requestContent="invoiceId:"+ invoiceId +" ; amount:"+amount +"  ;appointmentInfo: "+appointmentInfo+";token: "+token;
		
		mmsLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(),requestContent,request.getMethod(),null));
		
		SettleMMSPaymentByCashValueResponseDto  response = new SettleMMSPaymentByCashValueResponseDto();
		/***
		 * SGG-3940
		 * Block cash value deduction from MMS
		 */
		response.setReturnCode("1");
		response.setMessage("Disable the API that deduct cash value for MMS. ");
		
//		//check appointmentInfo is empty
//		if(appointmentInfo == null || appointmentInfo.trim().length()<=0)
//		{
//			response.setReturnCode("1");
//			response.setMessage("Invalid appointment info.");
//			mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//			return response;
//		}	
//		//decrypt the token
//		String decrypteKey = decrypt(token, TOKEY_KEY_UUID);				
//		if(decrypteKey == null || decrypteKey.indexOf(TOKEN_KEY)<0)
//		{
//			response.setReturnCode("1");
//			response.setMessage("Invalid token value.");
//			mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//			return response;
//		}			
//		try {		
//			MMSPaymentDto paymentDto = new MMSPaymentDto();
//			paymentDto.setAmount(amount);
//			paymentDto.setInvoiceNo(invoiceId);
//			paymentDto.setIsRemote("Y");
//			paymentDto.setRemoteType(Constant.MMSTHIRD_ITEM_NO);
//			paymentDto.setPaymentMethod(PaymentMethod.CASHVALUE.name());
//			paymentDto.setAppointmentInfo(appointmentInfo);
//			paymentDto.setCreatedBy("MMS payment");
//			
//			ResponseResult result =  mmsService.payment(paymentDto);	
//			/***
//			 * SGG-3345
//			 * if payment isn't same member payment ,alter error message 
//			 */
//			if(GTAError.FacilityError.MMS_API_EXCEPTION.getCode().equals(result.getReturnCode()))
//			{
//				response.setReturnCode("1");
//				response.setMessage("Modifying guest is not allowed, Please create new appointment for the guest.");
//				return response;
//			}
//			
//			MMSPaymentDto returnDto = (MMSPaymentDto) result.getDto();			
//			response.setReturnCode(result.getReturnCode());
//			if(returnDto!=null)
//			{
//				SettleMMSPaymentByCashValueResponseDto.Data  data = response.new Data();
//				data.setOrderNo(returnDto.getOrderNo());
//				data.setTransactionNo(returnDto.getTransNo().toString());
//				data.setSettleTimestamp(returnDto.getSettleTime());
//				response.setData(data);
//				response.setMessage("Payment By CashValue successful.");
//			}else
//			{
//				response.setReturnCode("1");
//				response.setMessage("Payment By CashValue failed!");
//			}
//			mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//			
//		} catch (GTACommonException e) {
//			responseResult.initResult(e.getError());
//			if(GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_PERPAYMENT.getCode().equals(e.getError().getCode())||
//					GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_CREDITLIMIT.getCode().equals(e.getError().getCode())){
//				response.setMessage(responseResult.getI18nMessge(e.getError().getCode(),e.getArgs()));
//			}else{
//				response.setMessage(responseResult.getI18nMessge(e.getError().getCode()));
//			}
//			response.setReturnCode("1");
//			String eMsg=MMSRequestProcessorController.class.getName() + "Payment By CashValue failed!"+e.getMessage();
//			mmsLog.error(Log4jFormatUtil.logErrorMessage(null,null,null,JSONObject.fromObject(response).toString(),eMsg),e);
//		}catch(Exception e){
//			String eMsg=MMSRequestProcessorController.class.getName() + "Payment By CashValue failed!"+e.getMessage();
//			response.setReturnCode("1");	
//			response.setMessage(e.getMessage());
//			mmsLog.error(Log4jFormatUtil.logErrorMessage(null,null,null,JSONObject.fromObject(response).toString(),eMsg),e);
//		}
		mmsLog.info("MMS settle payment by Cash Value end...");
		return response;
	}
		
	@ResponseBody 
	@RequestMapping(value="/settleMMSPaymentByGuestFolio",method=RequestMethod.POST)
	public SettleMMSPaymentByGuestFoliorResponseDto settleMMSPaymentByGuestFolio(HttpServletRequest request,
					@RequestParam(value = "invoiceId", required=true) String invoiceId,
					@RequestParam(value = "amount", required=true) BigDecimal amount,
					@RequestParam(value = "appointmentInfo", required=true) String appointmentInfo,			
					@RequestParam(value = "guestCode", required=true) String guestCode){
		
		mmsLog.info("MMS settle payment by Guest Folio start...");
//	    logger.info("Payment By Guest Folio:invoiceId:"+ invoiceId +" ; amount:"+amount +"  ;appointmentInfo: "+appointmentInfo+"  ;guestCode: "+guestCode);
//		logger.info("Payment By Guest Folio:api_key:" + token);	
		String token = request.getHeader("apikey");		
		String requestContent="Payment By Guest Folio:invoiceId:"+ invoiceId +" ; amount:"+amount +"  ;appointmentInfo: "+appointmentInfo+"  ;guestCode: "+guestCode+"\n"+"Payment By Guest Folio:api_key:" + token;
			
		mmsLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(),requestContent,request.getMethod(),null));
		 
		SettleMMSPaymentByGuestFoliorResponseDto response = new SettleMMSPaymentByGuestFoliorResponseDto();
		
		/***
		 * SGG-3940
		 * Block cash value deduction from MMS
		 */
		response.setReturnCode("1");
		response.setMessage("Disable the API that deduct cash value for MMS. ");
		
//		//check appointmentInfo is empty
//		if(appointmentInfo == null || appointmentInfo.trim().length()<=0)
//		{
//			response.setReturnCode("1");
//			response.setMessage("Payment By Guest Folio:Invalid appointment info.");
//			mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//			return response;
//		}	
//		//decrypt the token
//		String decrypteKey = decrypt(token, TOKEY_KEY_UUID);				
//		if(decrypteKey == null || decrypteKey.indexOf(TOKEN_KEY)<0)
//		{
//			response.setReturnCode("1");
//			response.setMessage("Payment By Guest Folio:Invalid token value.");
//			mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//			return response;
//		}	
//		try {		
//			//get member info by academy number
//			Member member=memberService.getMemberByAcademyNo(guestCode);
//			if(member==null){
//				response.setReturnCode("1");
//				response.setMessage("Payment By Guest Folio failed! the guestCode did not exists in GTA!");
//				mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//				return response;
//			}
//			//get guest check in room info from GTA
//			List roomReservationRecList =hotelReservationService.getReservationByCustomerID(member.getCustomerId());
//			if(roomReservationRecList==null||roomReservationRecList.size()<=0){
//				response.setReturnCode("1");
//				response.setMessage("Payment By Guest Folio failed! the customer did not reserve any room!");
//				mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//				return response;
//			}
//			//send request for charge posting
//			RoomReservationRec roomReservationRec=(RoomReservationRec)roomReservationRecList.get(0);
//			ChargePostingDto chargePostingDto = new ChargePostingDto();
//			chargePostingDto.setAmount(amount);
//			chargePostingDto.setDescription("MMS Payment By Guest Folio Desc");
//			chargePostingDto.setPmsRevenueCode(ChargeType.CHARGE.getCode());
//			chargePostingDto.setResvID(roomReservationRec.getConfirmId());
//			chargePostingDto.setTicketID("MMS Payment By Guest Folio, invoiceId:"+invoiceId);			
//			chargePostingDto.setType(ChargeType.CHARGE.getCode());
//			MessageResult PMSResult = PMSApiService.chargePostingReserved(chargePostingDto);
//			if(!PMSResult.isSuccess()){
//				response.setReturnCode("1");
//				response.setMessage("Payment By Guest Folio failed! Charge Posting fail!");
//				mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//				return response;
//			}
//			
//			//do the payment
//			MMSPaymentDto paymentDto = new MMSPaymentDto();
//			paymentDto.setAmount(amount);
//			paymentDto.setInvoiceNo(invoiceId);
//			paymentDto.setCustomerId(member.getCustomerId());
//			paymentDto.setIsRemote("Y");
//			paymentDto.setRemoteType(Constant.MMSTHIRD_ITEM_NO);
//			paymentDto.setPaymentMethod(PaymentMethod.CHRGPOST.name());
//			paymentDto.setAppointmentInfo(appointmentInfo);
//			paymentDto.setCreatedBy("MMS payment");
//			
//			ResponseResult result =  mmsService.payment(paymentDto);	
//			
//			/***
//			 * SGG-3345
//			 * if payment isn't same member payment ,alter error message 
//			 */
//			if(GTAError.FacilityError.MMS_API_EXCEPTION.getCode().equals(result.getReturnCode()))
//			{
//				response.setReturnCode("1");
//				response.setMessage("Modifying guest is not allowed, Please create new appointment for the guest.");
//				return response;
//			}
//			MMSPaymentDto returnDto = (MMSPaymentDto) result.getDto();			
//			
//			response.setReturnCode(result.getReturnCode());
//			if(returnDto!=null)
//			{
//				SettleMMSPaymentByGuestFoliorResponseDto.Data  data = response.new Data();
//				data.setOrderNo(returnDto.getOrderNo());
//				data.setTransactionNo(returnDto.getTransNo().toString());
//				data.setSettleTimestamp(returnDto.getSettleTime());
//				response.setData(data);
//				response.setMessage("Payment By Guest Folio successful.");
//			}else{
//				response.setReturnCode("1");
//				response.setMessage("Payment By Guest Folio failed!");
//			}
//			
//			mmsLog.info(Log4jFormatUtil.logInfoMessage(null,null,null,JSONObject.fromObject(response).toString()));
//			
//		} catch (GTACommonException e) {
//			responseResult.initResult(e.getError());
//			response.setReturnCode("1");
//			if(GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_PERPAYMENT.getCode().equals(e.getError().getCode())||
//					GTAError.MemberError.PAYMENTAMOUNT_EXCEEDS_CREDITLIMIT.getCode().equals(e.getError().getCode())){
//				response.setMessage(responseResult.getI18nMessge(e.getError().getCode(),e.getArgs()));
//			}else{
//				response.setMessage(responseResult.getI18nMessge(e.getError().getCode()));
//			}
//			String eMsg=MMSRequestProcessorController.class.getName() + "Payment By Guest Folio failed!"+e.getMessage();
//			mmsLog.error(Log4jFormatUtil.logErrorMessage(null,null,null,JSONObject.fromObject(response).toString(),eMsg),e);
//		}catch(Exception e){
//			String eMsg= MMSController.class.getName() + "Payment By Guest Folio failed!"+e.getMessage();
//			response.setReturnCode("1");	
//			response.setMessage(e.getMessage());
//			mmsLog.error(Log4jFormatUtil.logErrorMessage(null,null,null,JSONObject.fromObject(response).toString(),eMsg),e);
//		}
		mmsLog.info("MMS settle payment by Guest Folio end...");
		return response;
	}
	
	
	private final String characterEncoding = "UTF-8";
    private final String cipherTransformation = "AES/CBC/PKCS5Padding";
    private final String aesEncryptionAlgorithm = "AES";
 
    private  byte[] decrypt(byte[] cipherText, byte[] key, byte [] initialVector) throws Exception{
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpecy = new SecretKeySpec(key, aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }
 
    private byte[] encrypt(byte[] plainText, byte[] key, byte [] initialVector) throws Exception{
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        plainText = cipher.doFinal(plainText);
        return plainText;
    }
 
    private byte[] getKeyBytes(String key) throws  Exception{
        byte[] keyBytes= new byte[16];
        byte[] parameterKeyBytes= key.getBytes(characterEncoding);
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
        return keyBytes;
    }
 

    /**
     * Encrypts plain text using AES 128bit key and a Chain Block Cipher and returns a base64 encoded string
     * @param plainText 
     * 		  Plain text to encrypt
     * @param key 
     * 		  Secret key
     * @return Base64 encoded string
     */
    private String encrypt(String plainText, String key) throws Exception{
    	try{
	        byte[] plainTextbytes = plainText.getBytes(characterEncoding);
	        byte[] keyBytes = getKeyBytes(key);
	        byte[] encryptByte=encrypt(plainTextbytes,keyBytes, keyBytes);
	        
	        return new String(Base64.encodeBase64(encryptByte));        
		} catch (Exception e) {
	        e.printStackTrace();
	    }
	
    	return null;
    }
 
    
    /**
     * Decrypts a base64 encoded string using the given key (AES 128bit key and a Chain Block Cipher)
     * @param encryptedText 
     * 		  Base64 Encoded String
     * @param key 
     * 		  Secret key
     * @return Decrypted String
     */
    private String decrypt(String encryptedText, String key){  
    	try{
	        byte[] cipheredBytes = Base64.decodeBase64(encryptedText.getBytes());
	        byte[] keyBytes = getKeyBytes(key);
	        return new String(decrypt(cipheredBytes, keyBytes, keyBytes), characterEncoding);
    	} catch (Exception e) {
            e.printStackTrace();
        }
    	
    	return null;
    }
}
