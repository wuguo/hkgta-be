package com.sinodynamic.hkgta.controller.crm.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.service.crm.sales.UploadFileService;
import com.sinodynamic.hkgta.util.FileUpload;

@Controller
public class UploadFileController extends ControllerBase {
	
	private static Logger logger = Logger.getLogger(FileUpload.class);
	
	@Autowired
	private UploadFileService uploadFileService;
	
	
	@RequestMapping(value = "/upload.do", method = RequestMethod.POST)
	public void uploadImages(HttpServletRequest req, HttpServletResponse res) {
		
		logger.info("UploadFileController.uploadImages() invocation start ...");
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) req;
		MultipartFile myProfile = multipartRequest.getFile("profile");
		
		String retMsg = null;
		try {
			
			retMsg = uploadFileService.uploadFiles(myProfile);
			res.getWriter().write(retMsg);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		
	}
}
