package com.sinodynamic.hkgta.controller.pms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.pms.ManagementLevelDto;
import com.sinodynamic.hkgta.dto.pms.ManagementStaffDto;
import com.sinodynamic.hkgta.service.pms.ManagementLevelService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
public class ManagementController extends ControllerBase {

	@Autowired
	private ManagementLevelService	managementLevelService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/management_level/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getStaffRoster() {
		try {
			List<ManagementLevelDto> managementLevelDtoList = managementLevelService.getAllManagementLevel();
			responseResult.initResult(GTAError.Success.SUCCESS, managementLevelDtoList);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/management_level/ar", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getARStaffRoster() {
		try {
			List<ManagementStaffDto> managementStaffDtoList = managementLevelService.getARManagementLevel();
			responseResult.initResult(GTAError.Success.SUCCESS, managementStaffDtoList);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
}
