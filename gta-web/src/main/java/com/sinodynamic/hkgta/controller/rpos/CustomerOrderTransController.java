package com.sinodynamic.hkgta.controller.rpos;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.DaypassPurchaseDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.rpos.CashValueLogDto;
import com.sinodynamic.hkgta.dto.rpos.CustomerOrderTransDto;
import com.sinodynamic.hkgta.dto.rpos.UpadeCashValueDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberTransactionService;
import com.sinodynamic.hkgta.service.crm.cardmanage.PCDayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping(value = "/transaction")
public class CustomerOrderTransController extends ControllerBase {
	
	private Logger ecrLog = Logger.getLogger(LoggerType.ECRLOG.getName()); 
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	@Autowired
	private PCDayPassPurchaseService pcDayPassPurchaseService;
	
	@Autowired
	private MemberTransactionService memberTransactionService;
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;
	
	private static final Logger logger = Logger.getLogger(CustomerOrderTransController.class);

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionList(
			@RequestParam(value = "balanceDue", defaultValue = "All") String balanceDue,
			@RequestParam(value = "serviceType", defaultValue = "All") String serviceType,
			@RequestParam(value = "sortBy", defaultValue = "enrollCreateDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") boolean isAscending,
			@RequestParam(value = "searchText", required= false,defaultValue = "") String searchText,
			@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "0") int pageSize,
			@RequestParam(value = "filterBy", defaultValue = "MY") String filterBy,
			@RequestParam(value = "enrollStatusFilter", defaultValue = "All") String enrollStatusFilter,
			@RequestParam(value = "mode", defaultValue = "saleskit") String mode,
			@RequestParam(value="filters",required= false,defaultValue= "") String filters,
			@RequestParam(value="memberType",required= false,defaultValue= "IPM") String memberType,
			@RequestParam(value = "device", defaultValue = "IOS") String device
			) {
		try {
			logger.info("Ready to get transaction list");
			page = new ListPage<CustomerOrderHd>();
			String filterBySalesMan = null;
			if(pageNumber>0){
				page.setNumber(pageNumber);
			}
			if(pageSize > 0){
				page.setSize(pageSize);
			}
			if(filterBy.equalsIgnoreCase("my")){
				String loginUserId =  getUser().getUserId();
				filterBySalesMan = (loginUserId == "-1")?"":loginUserId;
			}else if(filterBy.equalsIgnoreCase("ALL")){
				filterBySalesMan = "";
			}
			LoginUser currentUser = getUser();
			if(CommUtil.notEmpty(filters)){
				AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				if(advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0){
					if(sortBy != null && sortBy.length()>0 ){
						advanceQueryDto.setSortBy(sortBy);
					}
					advanceQueryDto.setAscending(isAscending?" true ":" false ");
//					for(SearchRuleDto rule: advanceQueryDto.rules){
//						if("t.offerCode".equals(rule.field)&&"ENROLLMENT".equals(rule.data.toUpperCase())){
//							rule.setData(null);
//							rule.setOp(CommonDataQueryDao.IS_NULL);
//						}
//					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}
			ListPage<CustomerOrderHd> result = customerOrderTransService
					.getCustomerTransactionList(page, balanceDue, serviceType, sortBy,isAscending,searchText,filterBySalesMan, mode,currentUser.getUserId(), enrollStatusFilter,memberType,device);
			ResponseResult response = new ResponseResult();
			Data data = new Data();
			data.setList(result.getDtoList());
			data.setLastPage(result.isLast());
			data.setCurrentPage(result.getNumber());
			data.setRecordCount(result.getAllSize());
			data.setPageSize(result.getSize());
			data.setTotalPage(result.getAllPage());
			response.setData(data);
			response.setReturnCode("0");
			response.setErrorMessageEN("");
			return response;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION);
			return responseResult;
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/getAll/advancedSearch", method = RequestMethod.GET)
	public ResponseResult advanceSearch(@RequestParam(value = "serviceType", defaultValue = "Enrollment") String serviceType){
		responseResult.initResult(GTAError.Success.SUCCESS, customerOrderTransService.assembleQueryConditions(serviceType));
		return responseResult;
	} 

	@RequestMapping(value = "/saveCustomerOrderTrans", method = RequestMethod.PUT)
	@ResponseBody
	public  ResponseResult saveCustomerOrderTrans(HttpServletRequest request,
			@RequestBody CustomerOrderTransDto customerOrderTransDto) {
		
		try {
			customerOrderTransDto.setTerminalType(request.getHeader("terminalType"));
			
			JSONObject jsonObject = JSONObject.fromObject(customerOrderTransDto);
			Map<String, Long> transactionMap = new HashMap<String, Long>();
			System.out.println(jsonObject);
			if (customerOrderTransDto.getPaymentReceivedBy() == null || customerOrderTransDto.getPaymentReceivedBy().length() == 0) {
				customerOrderTransDto.setPaymentReceivedBy(this.getUser().getUserName());
			}
			customerOrderTransDto.setCreateByUserId(getUser().getUserId());
			customerOrderTransDto.setCreateByUserName(getUser().getUserName());
			ResponseResult responseResult = customerOrderTransService.savecustomerOrderTransService(customerOrderTransDto, transactionMap);
			// CreadCard return transaction No for POS purpose
			if (PaymentMethod.VISA.name().equals(customerOrderTransDto.getPaymentMethodCode()) 
					|| PaymentMethod.CUP.name().equals(customerOrderTransDto.getPaymentMethodCode())) {
				responseResult.setData(transactionMap);
				return responseResult;
			}
			if (responseResult.getReturnCode().equals(GTAError.Success.SUCCESS.getCode())) {
				responseResult = customerOrderTransService.getPaymentDetailsByOrderNo(customerOrderTransDto.getOrderNo());
		   }
			return responseResult;
		} catch (GTACommonException gta) {
			logger.error(gta.getMessage(), gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	

	/**
	 * Used to handle the call back of POS payment for paying the service payment.
	 * E.g. Service payment: Enrollment, Renewal
	 * @author Changpan_Wu
	 * @param posResponse
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/servicePayment/posCallback", method = RequestMethod.POST)
	@ResponseBody
    public String callback(HttpServletRequest request,@RequestBody PosResponse posResponse) {
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
		logger.debug("add payment by credit card:"+gson.toJson(posResponse));
		ecrLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), gson.toJson(posResponse), RequestMethod.POST.name(), null));		
		if(null==posResponse){
			responseResult.initResult(GTAError.ERCCallBack.REPONSE_IS_NULL);
			ecrLog.info(Log4jFormatUtil.logInfoMessage(null,null,null, responseResult.getErrorMessageEN()));
			return responseResult.getErrorMessageEN();
		}
		
		responseResult = customerOrderTransService.handleCreditCardByPos(posResponse);
        return responseResult.getReturnCode();
    }
	/**
	 * Used to handle the call back of POS payment for purchase  payment.
	 * E.g. Service payment: Enrollment, Renewal
	 * @author Changpan_Wu
	 * @param posResponse
	 * @return
	 */
	@RequestMapping(value = "/daypassPayment/posCallback", method = RequestMethod.POST)
	@ResponseBody
    public String purchaseDaypassCallback(HttpServletRequest request,@RequestBody PosResponse posResponse) {
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
		logger.debug("add payment by credit card:"+gson.toJson(posResponse));
		ecrLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), gson.toJson(posResponse), RequestMethod.POST.name(), null));		
		if(null==posResponse){
			responseResult.initResult(GTAError.ERCCallBack.REPONSE_IS_NULL);
			ecrLog.info(Log4jFormatUtil.logInfoMessage(null,null,null, responseResult.getErrorMessageEN()));
			return responseResult.getErrorMessageEN();
		}
		
		responseResult = customerOrderTransService.handlePosPayDaypass(posResponse);
		if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(this.responseResult.getReturnCode())){
			DaypassPurchaseDto dto = new DaypassPurchaseDto();
			Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
			dto.setTransactionNo(transactionNo);
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			dto.setOrderNo(customerOrderTrans.getCustomerOrderHd().getOrderNo());
			pcDayPassPurchaseService.sendEmail(dto,this.getUser().getUserId());
		}
        return responseResult.getReturnCode();
	}
	
	
	@RequestMapping(value = "/readTrans", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult readTrans(
			@RequestBody CustomerOrderTransDto customerOrderTransDto) {
		if (customerOrderTransDto != null && customerOrderTransDto.getTransactionNo() > 0) {
			customerOrderTransDto.setReadBy(this.getUser().getUserId());
		}
		ResponseResult responseResult = customerOrderTransService.readTrans(customerOrderTransDto);
		/*if (responseResult.getReturnCode().equals("0")){
			responseResult = customerOrderTransService.getPaymentDetailsByOrderNo(customerOrderTransDto.getOrderNo());
		}*/
		return responseResult;
	}
	
	@RequestMapping(value = "/paymentByCreditCard", method = RequestMethod.POST)
	public @ResponseBody ResponseResult  paymentByCreditCard(
			@RequestBody CustomerOrderTransDto customerOrderTransDto) {
		if (customerOrderTransDto.getPaymentReceivedBy() == null 
				|| customerOrderTransDto.getPaymentReceivedBy().length() == 0) {
			customerOrderTransDto.setPaymentReceivedBy(this.getUser().getUserName());
		}
		customerOrderTransDto.setCreateByUserId(getUser().getUserId());
		ResponseResult responseResult = customerOrderTransService
				.paymentByCreditCard(customerOrderTransDto);
		if ("0".equals(responseResult.getReturnCode())) {
			CustomerOrderTrans customerOrderTrans = (CustomerOrderTrans)responseResult.getDto();
			String redirectUrl = paymentGatewayService.payment(customerOrderTrans);
			Map<String, String> redirectUrlMap = new HashMap<String, String>();
			redirectUrlMap.put("redirectUrl", redirectUrl);
			responseResult.setData(redirectUrlMap);
		}
		return responseResult;
	}

	@RequestMapping(value = "/getPaymentDetails/{orderNo}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getPaymentDetailsByOrderNo(
			@PathVariable(value = "orderNo") Long orderNo) {
		// String pageSize = request.getParameter("pageSize");
		// String pageNumber = request.getParameter("pageNumber");
		// page.setNumber(Integer.parseInt(pageNumber));
		// page.setSize(Integer.parseInt(pageSize));
		ResponseResult responseResult = customerOrderTransService
				.getPaymentDetailsByOrderNo(orderNo);
		return responseResult;
	}

	@RequestMapping(value = "/getPaymentDetailsAccountant/{orderNo}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getPaymentDetailsAccountantByOrderNo(
			@PathVariable(value = "orderNo") Long orderNo) {
		ResponseResult responseResult = customerOrderTransService
				.getPaymentDetailsAccountantByOrderNo(orderNo);
		return responseResult;
	}
	
	
	/**
	 * SGG - 1695/1704
	 * hasFalueUnreadData is true: means has a failure unread payment
	 * sumOfFailureUnreadPayment : sum of failure unread payment
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/getFailureUnreadData", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getFailureUnreadData(@RequestParam(value = "type", defaultValue = "MEMBERSHIP") String type) {
		BigDecimal unreadPaidAmount =  customerOrderTransService .getFalueUnreadStatus(type);
		boolean hasFailureUnreadData = false;
		BigDecimal sumOfFailureUnreadPayment = new BigDecimal(0);
		if(unreadPaidAmount!=null&&unreadPaidAmount.doubleValue()>0){
			hasFailureUnreadData = true;
			sumOfFailureUnreadPayment = unreadPaidAmount;
		}
	    Map<String, String> imgPathMap = new HashMap<String, String>();
	    imgPathMap.put("hasFailureUnreadData", ""+hasFailureUnreadData);
	    imgPathMap.put("sumOfFailureUnreadPayment", ""+sumOfFailureUnreadPayment);
		responseResult.initResult(GTAError.Success.SUCCESS,imgPathMap);
		return responseResult;
	}

	@RequestMapping(value = "/uploadAttachment", method = RequestMethod.POST)
	public @ResponseBody ResponseResult uploadAttachment(
			@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		ResponseResult responseResult = new ResponseResult();
		try {
			if (file == null || file.isEmpty()) {
				logger.info("File is empty!");
				responseResult.setReturnCode("1");
				responseResult.setErrorMessageEN("File is empty!");
				return responseResult;
			}
			String saveFilePath = "";
				saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.USER);
				
			File serverFile = new File(saveFilePath);
		
			saveFilePath = serverFile.getName();
			responseResult.setReturnCode("0");
			Map<String, String> imgPathMap = new HashMap<String, String>();
			imgPathMap.put("imagePath", "/"+saveFilePath);
			responseResult.setData(imgPathMap);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("upload file fail");
			return responseResult;
		}
	}

	@RequestMapping(value = "/getTransactionStatus/{transactionNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionStatus(@PathVariable(value = "transactionNo") Long transactionNo){
		CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
		if (null == customerOrderTrans) {
			responseResult.initResult(GTAError.PaymentError.TRANSACTION_NOT_EXISTS,new String[]{transactionNo.toString()});
		}else {
			Map<String, Object> statusMap = new HashMap<String, Object>();
			statusMap.put("transactionStatus", customerOrderTrans);
			responseResult.initResult(GTAError.Success.SUCCESS,statusMap);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/{transactionNo}/status", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateCustomerOrderTransStatusToFail(
			@PathVariable(value = "transactionNo") Long transactionNo) {
		CustomerOrderTrans trans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
		if(null != trans){
			trans.setStatus(Constant.Status.FAIL.name());
			trans.setUpdateBy(getUser().getUserId());
			trans.setUpdateDate(new Timestamp(new Date().getTime()));
			customerOrderTransService.updateCustomerOrderTrans(trans);
			responseResult.initResult(GTAError.Success.SUCCESS,trans);
		}
		return responseResult;
	}
	/**
	 * 根据cuseromerId获取member CashValue变更日志列表
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param status
	 * @param isAscending
	 * @param filters
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "/getCashVauleDebitList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCashValueChangesLog(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="updateDate", required = false) String sortBy,
			@RequestParam(value="status",defaultValue="ALL", required = false) String status,
			@RequestParam(value="isAscending",defaultValue="false", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters,
			@RequestParam(value="day", defaultValue= "-1", required = false) Integer day,
			@RequestParam(value="customerId",defaultValue="0", required = false) @EncryptFieldInfo Long customerId) {
		logger.info("CustomerOrderTransController.getCashValueChangesLog start ...");
		try {
			String joinSQL = customerOrderTransService.getCashValueChangesLog(status, customerId, day);
			ResponseResult tmpResult = null;
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
				joinSQL = joinSQL + " AND ";
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
				tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, CashValueLogDto.class);
			}
			else
			{
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, CashValueLogDto.class);
			}
			return tmpResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * staff更新member的CashValue
	 * @param dto
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/updateMemberCashValueByStaff", method = RequestMethod.POST)
	public @ResponseBody ResponseResult updateMemberCashValueByStaff(@RequestBody UpadeCashValueDto dto) {
		try {
			if (dto == null || null == dto.getAdjustmentType() || (!dto.getAdjustmentType().equals("credit") && !dto.getAdjustmentType().equals("debit")) || dto.getAdjustmentAmount().compareTo(BigDecimal.ZERO) < 0 || null == dto.getStaffUserId() || dto.getStaffUserId().equals("") || dto.getCustomerId() == null) {
				responseResult.initResult(GTAError.CommonError.REQUIRED_PARAMETERS_IS_NULL);
			}else {
				responseResult = memberTransactionService.updateMemberCashValueByStaff(dto);
			}
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * 获取member加减总数
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "/getMemberCashValueSum", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getMemberDebitCashValueSum(@RequestParam(value="customerId",defaultValue="0", required = false) @EncryptFieldInfo Long customerId) {
		try {
			return  memberTransactionService.getMemberCashValueSum(customerId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	@RequestMapping(value = "/getCustomerOrderTransList", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getCustomerOrderTransList(
			@RequestParam(value = "sortBy", defaultValue = "transactionNo", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate) {
		try {
			AdvanceQueryDto advanceQueryDto = new AdvanceQueryDto();
			StringBuilder hql =customerOrderTransService.getCustomerOrderTransSQL();
			
			if (!StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
				hql.append(" and  trans.transaction_timestamp BETWEEN '"+startDate+"' AND'"+startDate+"' ");
			}
			page.setNumber(pageNumber);
			page.setSize(pageSize);
			
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);

			responseResult = advanceQueryService.getInitialQueryResultBySQL(advanceQueryDto, hql.toString(), page,
					CustomerOrderTransDto.class);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	
	/**
	 * 获取所以member加减总数
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "/getAllDebitAndCreditCashValueSum", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAllDebitAndCreditCashValueSum() {
		try {
			return  memberTransactionService.getAllDebitAndCreditCashValueSum();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/getCashVauleOasis", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCashVauleOasis(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="transactionDate", required = false) String sortBy,
			@RequestParam(value="status",defaultValue="ALL", required = false) String status,
			@RequestParam(value="isAscending",defaultValue="false", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters,
			@RequestParam(value="date",  required = false) String date,
			@RequestParam(value="customerId",defaultValue="0", required = false) @EncryptFieldInfo Long customerId) 
	{
		logger.info("CustomerOrderTransController.getCashVauleOasis start ...");
		try {
			/***
			 *  item is RST00000001 /PMS00000001
			 */
			String joinSQL = customerOrderTransService.getCashValueTxnInOasis(date);//
			
			ResponseResult tmpResult = null;
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
				joinSQL = joinSQL + " AND ";
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
				tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, CashValueLogDto.class);
			}
			else
			{
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, CashValueLogDto.class);
			}
			return tmpResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	public static void main(String[] args) {
		Date d = new Date();

		  SimpleDateFormat s = new SimpleDateFormat("yyyy-MMM-dd",Locale.ENGLISH);

		 Date b=null;
		try {
			b = s.parse("2016-Oct-12");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  System.out.println(b);
	}
}