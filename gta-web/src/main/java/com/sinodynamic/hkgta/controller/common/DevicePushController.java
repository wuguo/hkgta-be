package com.sinodynamic.hkgta.controller.common;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class DevicePushController extends ControllerBase<Date> {
	private static final 	Logger logger = Logger.getLogger(DevicePushController.class);
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;

	@RequestMapping(value = "/push/register", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult pushRegister(@RequestParam(value = "userId", required = false) String userId,
			@RequestParam(value = "deviceArn", required = true) String deviceArn,
			@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "platform", required = false, defaultValue = "unknown") String platform,
			@RequestParam(value = "version", required = false, defaultValue = "unknown") String version,
			@RequestParam(value = "application", required = false, defaultValue = "unknown") String application) {
		devicePushService.pushRegister(userId, deviceArn, token, platform, application, version, this.getUser().getUserName());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
}
