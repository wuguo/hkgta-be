package com.sinodynamic.hkgta.controller.upload;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.FileUpload.FileCategory;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class UploadController extends ControllerBase<RestaurantMaster> {

	@RequestMapping(value = "/uploadAsset/{category}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult upload(@PathVariable(value = "category") String category, @RequestParam("file") MultipartFile file) throws IOException {

		try {
			FileCategory fileCategory = FileUpload.FileCategory.valueOf(category.toUpperCase());
			String saveFilePath = FileUpload.upload(file, fileCategory);
			
			File serverFile = new File(saveFilePath);
			saveFilePath = serverFile.getName();
			Map<String, String> imgPathMap = new HashMap<String, String>();
			imgPathMap.put("fileName", saveFilePath);
			responseResult.initResult(GTAError.Success.SUCCESS, imgPathMap);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
			return responseResult;
		}

		return responseResult;
	}
}
