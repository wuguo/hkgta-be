package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.Constant.memberType;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.AllowMarketingDto;
import com.sinodynamic.hkgta.dto.crm.ApproveTopUpDto;
import com.sinodynamic.hkgta.dto.crm.CorporateMemberDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAdditionInfoDto;
import com.sinodynamic.hkgta.dto.crm.CustomerAnalysisReportDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.DependentMemberRightsDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.OverviewListDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.crm.TopUpDto;
import com.sinodynamic.hkgta.dto.crm.TopUpMemberQuickSearchDto;
import com.sinodynamic.hkgta.dto.crm.TopUpSettlementDto;
import com.sinodynamic.hkgta.dto.fms.AdvancedQueryConditionsDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.membership.MemberFacilityTypeBookingDto;
import com.sinodynamic.hkgta.dto.membership.QuestionAnswerOrBirthDateDto;
import com.sinodynamic.hkgta.dto.staff.StaffPasswordDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerProfileReadLog;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.OtherRightCode;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.CorporateService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.DependentMemberService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberTransactionService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MembershipCardsManagmentService;
import com.sinodynamic.hkgta.service.crm.cardmanage.CandidateCustomerService;
import com.sinodynamic.hkgta.service.crm.cardmanage.DayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerAdditionInfoService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.onlinepayment.AmexPaymentGatewayService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.PhotoType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.coobird.thumbnailator.Thumbnailator;

@SuppressWarnings({ "rawtypes" })
@Controller
@RequestMapping("/membership")
@Scope("prototype")
public class CustomerMemberController extends ControllerBase {

	private Logger logger = Logger.getLogger(CustomerMemberController.class);
	
	
	private Logger ecrLog = Logger.getLogger(LoggerType.ECRLOG.getName());

	@Autowired
	private CustomerProfileService customerProfileService;

	@Autowired
	private MemberTransactionService memberCashValueService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private AdvanceQueryService advanceQueryService;

	private @Autowired DependentMemberService dependentMemberService;

	@Autowired
	private CandidateCustomerService candidateCustomerService;

	private @Autowired MembershipCardsManagmentService membershipCardsManagmentService;

	@Autowired
	private DayPassPurchaseService dayPassPurchaseService;

	@Autowired
	private PaymentGatewayService paymentGatewayService;
	
	@Autowired
	private AmexPaymentGatewayService amexPaymentGatewayService;
	
	@Autowired
	private GlobalParameterService globalParameterService;

	@Autowired
	private CorporateService corporateService;

	@Autowired
	private CustomerOrderTransService customerOrderTransService;

	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;

	@Autowired
	private MemberTransactionService memberTransactionService;
	@Autowired
	UsageRightCheckService usageRightCheckService;

	@Autowired
	private CustomerAdditionInfoService customerAdditionInfoService;
	

	@RequestMapping(value = "/getMemberList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberList(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam(value = "filterByCustomerId", defaultValue = "") @EncryptFieldInfo Long filterByCustomerId, // if
																														// defaultValue
																														// =
																														// "",
																														// return
																														// ALL
			@RequestParam(value = "status", defaultValue = "ALL") String status,
			@RequestParam(value = "expiry", defaultValue = "ALL") String expiry,
			@RequestParam(value = "planName", defaultValue = "") String planName,
			@RequestParam(value = "memberType", defaultValue = "") String memberType,
			@RequestParam(value = "filters", defaultValue = "") String filters, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			logger.info("Ready to get member overview list");
			page.setNumber(pageNumber);
			page.setSize(pageSize);

			if (!StringUtils.isEmpty(filters)) {
				AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0
						&& advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
					for (SearchRuleDto rule : advanceQueryDto.rules) {
						if (logger.isDebugEnabled()) {
							logger.debug("rule field:" + rule.getField() + " op:" + rule.getOp() + " data:"
									+ rule.getData());
						}
						if ("t.status".equals(rule.field) && "ACTIVE".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_ACT);
						} else if ("t.status".equals(rule.field) && "INACTIVE".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_NACT);
						} else if ("t.status".equals(rule.field) && "EXPIRED".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_EXP);
						}
						if ("t.planName".equals(rule.field) && (rule.data.indexOf("'") > 0)) {
							rule.setData(rule.data.replace("'", "\\'"));
						}
					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}
			ResponseResult result = memberTransactionService.getMembersOverviewList(page, sortBy, isAscending,
					filterByCustomerId == null ? "ALL" : filterByCustomerId.toString(), status, expiry, planName,
					memberType);
			logger.info("Member overview list retrieve end...");
			return result;

		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getMemberList Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@ResponseBody
	@RequestMapping(value = "/getMemberList/advancedSearch", method = RequestMethod.GET)
	public ResponseResult advanceSearch() {
		responseResult.initResult(GTAError.Success.SUCCESS, memberTransactionService.assembleQueryConditions());
		return responseResult;
	}

	@RequestMapping(value = "/getPersonalInfo/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	private ResponseResult getCustomerInfo(@PathVariable("customerID") Long customerID) {
		CustomerProfile customerProfile = null;
		try {
			customerProfile = customerProfileService.getCustomerInfoByCustomerID(customerID);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getCustomerInfo Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		responseResult.initResult(GTAError.Success.SUCCESS, customerProfile);
		return responseResult;
	}

	@RequestMapping(value = "/getAccountInfo/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAccountInfo(@PathVariable("customerID") @EncryptFieldInfo Long customerID) {
		ResponseResult responseResult = new ResponseResult();
		responseResult = customerProfileService.getAccountInfo(customerID);
		return responseResult;
	}

	/***
	 * member app/IOS check facitlity golf bay/tennis court perssion
	 * 
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "/checkAccountPerssion/{facilityType}/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkAccountPerssion(@PathVariable("facilityType") String facilityType,
			@PathVariable("customerId") @EncryptFieldInfo Long customerId) {
		/***
		 * check member app/iso golf course/private coaching or tennis
		 * course/private coaching
		 */
		try {
			Member member = memberService.getMemberById(customerId);
			// check IDM – Individual Dependent Member;
			// CDM – Corporate Dependent Member
			if(memberType.IDM.name().equals(member.getMemberType())||memberType.CDM.name().equals(member.getMemberType())){
				customerId=member.getSuperiorMemberId();
			}
			facilityType = facilityType.toUpperCase();
			if (UsageRightCheckService.TrainingCode.TRAIN1.name().equals(facilityType)
					|| UsageRightCheckService.TrainingCode.TRAIN2.name().equals(facilityType)
					|| UsageRightCheckService.TrainingCode.TRAIN3.name().equals(facilityType)
					|| UsageRightCheckService.TrainingCode.TRAIN4.name().equals(facilityType)) {
				ResponseResult result=usageRightCheckService.getTrainingRight(customerId);
				if(null==result.getDto()){
					/***
					 * check rnewed service  ,customer_order_hd status is CMP
					 */
					result=usageRightCheckService.getRenewedTrainingRight(customerId);
				}
				return result;

			} else if (UsageRightCheckService.FacilityCode.GOLF.name().equals(facilityType)
					|| UsageRightCheckService.FacilityCode.TENNIS.name().equals(facilityType)
					|| UsageRightCheckService.FacilityCode.WELLNESS.name().equals(facilityType)
					|| UsageRightCheckService.FacilityCode.GUEST.name().equals(facilityType)) {
				ResponseResult result=usageRightCheckService.getEffectiveFacilityRightByCustomerId(customerId);
				if(null==result.getDto()){
					/***
					 * check rnewed service  ,customer_order_hd status is CMP
					 */
					result=usageRightCheckService.getRenewedFacilityRightByCustomerId(customerId);
				}
				return result;

			} else if (UsageRightCheckService.FacilityCode.DAYPASS.name().equals(facilityType)) {
				// check the quotas, if day pass quotas per day in service plan
				// <=0, means no permission, can't go into next page
				UsageRightCheckService.OtherRight right = usageRightCheckService.checkOtherRight(customerId,
						OtherRightCode.D1);
				if (!right.isCanBook()) {
					responseResult.initResult(GTAError.FacilityError.NO_PERMISSION_AC);
				} else {
					responseResult.initResult(GTAError.Success.SUCCESS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseResult;
	}

	@RequestMapping(value = "/editLimitValue", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult editLimitValue(@RequestBody CorporateMemberDto dto) {
		try {
			BigDecimal creditLimit = dto.getTotalCreditLimit();
			if (null != creditLimit) {
				int check = creditLimit.compareTo(new BigDecimal(99999999.99).setScale(2, BigDecimal.ROUND_HALF_UP));
				if (!StringUtils.isEmpty(creditLimit) && check == 1) {
					// return new ResponseResult("1", "", "The limit value
					// exceeds the maximum! Maximum is 99999999.99!");
					responseResult.initResult(GTAError.MemberShipError.CREDIT_LIMIT_EXCEEDS_THE_MAXIMUM);
					return responseResult;
				}
				if (BigDecimal.ZERO.compareTo(creditLimit) > 0) {
					// return new ResponseResult("1", "", "The limit value can
					// not be negative!");
					responseResult.initResult(GTAError.MemberShipError.CREDIT_LIMIT_VALUE_INVALID);
					return responseResult;
				}
			}
			return customerProfileService.editLimitValue(dto.getCustomerId(), dto.getTotalCreditLimit(),
					getUser().getUserId(), dto.getMemberLimitRuleVersion());
		} catch (HibernateOptimisticLockingFailureException concurrent) {
			// concurrent.printStackTrace();
			logger.error("CustomerMemberController.editLimitValue Exception : ", concurrent);
			responseResult.initResult(GTAError.CommonError.BEEN_UPDATED, new String[] { "Credit Limit" });
			return responseResult;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.editLimitValue Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	@RequestMapping(value = "/transaction/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCurrentBalance(@PathVariable("customerId") @EncryptFieldInfo Long customerId) {
		try {
			return memberCashValueService.getCurrentBalance(customerId);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getCurrentBalance Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	// modified by Kaster 20160323 增加@EncryptFieldInfo
	@RequestMapping(value = "/transaction/total/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTotalAmount(@PathVariable("customerId") @EncryptFieldInfo Long customerId,
			@RequestParam(value = "transactionType", defaultValue = "EXP") String transactionType) {
		try {
			return memberCashValueService.getTotalAmount(customerId, transactionType);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getCurrentBalance Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	// modified by Kaster 20160323 增加@EncryptFieldInfo
	@RequestMapping(value = "/getDependentMembers/{customerID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDependentMembers(@PathVariable("customerID") @EncryptFieldInfo Long customerID,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "memberName") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam(value="platform",required=false) String platform) {

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		return memberService.getDependentMembers(customerID, page,platform);

	}

	@RequestMapping(value = "/transaction/topup", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult topUpByCashCard(HttpServletRequest request, @RequestBody TopUpDto topUpDto) {
		try {
			topUpDto.setTerminalType(request.getHeader("terminalType"));
			topUpDto.setStaffUserId(super.getUser().getUserId());
			ResponseResult responseResult = new ResponseResult();
			responseResult = memberCashValueService.topupByCashCard(topUpDto);
			if (GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())
					&& PaymentMethod.CASH.name().equals(topUpDto.getPaymentMethod())) {
				TopUpDto dto = topUpDto;
				memberCashValueService.sentTopupEmail(dto);
			}
			return responseResult;
		} catch (Exception e) {
			logger.error("CustomerMemberController Error Message:", e);
			responseResult.initResult(GTAError.MemberShipError.TOPUP_EXCEPTION);
			return responseResult;
		}
	}

	// added by Kaster 20160127
	// modified by Kaster 20160216 新需求：IPM用户top up也支持四种充值方式。也调用这个接口。
	@RequestMapping(value = "/transaction/corporateTopup", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult topUpForCorporate(@RequestBody TopUpDto topUpDto) {
		try {
			topUpDto.setStaffUserId(super.getUser().getUserId());
			ResponseResult responseResult = new ResponseResult();
			responseResult = memberCashValueService.topUpForCorporate(topUpDto);
			return responseResult;
		} catch (Exception e) {
			logger.error("CustomerMemberController Error Message:", e);
			responseResult.initResult(GTAError.MemberShipError.TOPUP_EXCEPTION);
			return responseResult;
		}
	}

	// added by Kaster 20160127
	@RequestMapping(value = "/transaction/approveTopup", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult approveCorporateTopup(@RequestBody ApproveTopUpDto approveTopUpDto) {
		try {
			ResponseResult responseResult = new ResponseResult();
			responseResult = memberCashValueService.approveCorporateTopup(approveTopUpDto);
			return responseResult;
		} catch (Exception e) {
			logger.error("CustomerMemberController Error Message:", e);
			return responseResult;
		}
	}

	// added by Kaster 20160128
	// modified by Kaster 20160216 增加新需求：IPM和IDM充值（非cash方式）也需要approve，所以修改sql语句。
	@RequestMapping(value = "/transaction/getCorporateTopUpList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult fetchCorporateTopUpRecords(
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "dateTime", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false", required = false) String isAscending,
			@RequestParam(value = "status", defaultValue = "", required = false) String status,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters) {

		try {
			String sql = "SELECT c.transaction_no as transactionId,c.transaction_timestamp as dateTime,m.customer_id as customerId,m.academy_no as patronId,"
					+ "CONCAT(p.salutation,' ',p.given_name,' ',p.surname) AS patronName, c.payment_method_code as topUpMethod,"
					+ "c.agent_transaction_no as reference,c.paid_amount as topUpAmount,c.status as status "
					+ "FROM customer_order_trans c,customer_order_hd h,member m,customer_profile p "
					+ "WHERE c.order_no=h.order_no AND h.customer_id=m.customer_id AND m.customer_id=p.customer_id "
					+ "AND m.member_type in ('CPM','CDM','IPM','IDM') AND m.status='ACT' AND c.payment_method_code IN ('BT','CHEQUE')"
					+ " AND EXISTS (SELECT 1 FROM customer_order_det d WHERE d.item_no = 'CVT0000001' AND d.order_no=h.order_no ) ";

			page.setNumber(pageNumber);
			page.setSize(pageSize);
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			if (!status.equals("")) {
				sql += " and c.status='" + status + "' ";
			}
			// filters.indexOf("field")>0是为了确保filters中真的有查询条件，如果不带任何查询条件，那么filters:{"groupOp":"","rules":[],"groups":[]}
			if (!StringUtils.isEmpty(filters) && filters.indexOf("field") > 0) {
				sql += " and ";
				// 前端传递过来的日期只有年月日，但是数据库中存入的是年月日时分秒，所以当查询条件是“=某个日期时”，必须取数据库中的数据做格式转化（只比较年月日），否则查不出来。
				if (filters.indexOf("c.transaction_timestamp") > 0) {
					filters = filters.replaceAll("c.transaction_timestamp",
							"DATE_FORMAT(c.transaction_timestamp,'%Y-%m-%d')");
				}
				queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			}
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, sql, page, TopUpSettlementDto.class);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.fetchCorporateTopUpRecords Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	// added by Kaster 20160128
	@RequestMapping(value = "/transaction/getCorporateTopUpList/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult topUpSettlementAdvancedSearch() {
		this.responseResult.initResult(GTAError.Success.SUCCESS,
				this.memberTransactionService.topUpSettlementAdvancedSearch());
		return this.responseResult;
	}

	// added by Kaster 20150216
	@RequestMapping(value = "/quickSearchForTopUp", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult quickSearch(
			@RequestParam(value = "patronName", defaultValue = "", required = false) String patronName,
			@RequestParam(value = "mobile", defaultValue = "", required = false) String mobile,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending) {
		String sql = "SELECT IFNULL(m.academy_no,'') patronId,"
				+ "CONCAT(cp.salutation,' ',cp.given_name,' ',cp.surname) patronName," + "cp.phone_mobile mobile,"
				+ "DATE_FORMAT(cp.date_of_birth,'%Y-%b-%d') dateOfBirth, " + "cp.customer_id customerId,"
				+ "m.academy_no academyNo," + "m.status status," + "m.member_type memberType," + "cp.surname surname,"
				+ "CONCAT(cp.salutation,' ',cp.given_name,' ',cp.surname) memberName," + "cp.given_name givenName,"
				+ "cp.salutation salutation "
				+ " FROM member m,customer_profile cp WHERE m.customer_id=cp.customer_id ";

		if (patronName != null && !patronName.equals("")) {
			sql += " and (CONCAT(cp.given_name,' ',cp.surname) like '%" + patronName + "%') ";
		}
		if (mobile != null && !mobile.equals("")) {
			sql += " and cp.phone_mobile like '%" + mobile + "%' ";
		}
		if (sortBy != null && !sortBy.equals("")) {
			if (isAscending.equals("false")) {
				sql += "order by " + sortBy + " desc ";
			} else {
				sql += "order by " + sortBy + " asc ";
			}
		} else {
			sql += "order by patronName,mobile";
		}
		page.setSize(10);
		responseResult = advanceQueryService.getAdvanceQueryResultBySQL(new AdvanceQueryDto(), sql, page,
				TopUpMemberQuickSearchDto.class);
		if (page.getAllSize() > 10) {
			responseResult.initResult(GTAError.CommonError.RECORDS_EXCESS);
			return responseResult;
		}
		return responseResult;
	}

	// TOPUP callback
	@RequestMapping(value = "/payment/posCallback", method = RequestMethod.POST)
	@ResponseBody
	public String callback(HttpServletRequest request,@RequestBody PosResponse posResponse) {
		logger.info("Got callback !!");
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create();
		logger.debug(gson.toJson(posResponse));
		ecrLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), gson.toJson(posResponse), RequestMethod.POST.name(), null));		
		if(null==posResponse){
			responseResult.initResult(GTAError.ERCCallBack.REPONSE_IS_NULL);
			ecrLog.info(Log4jFormatUtil.logInfoMessage(null,null,null, responseResult.getErrorMessageEN()));
			return responseResult.getErrorMessageEN();
		}
		if (null!=posResponse && null!=posResponse.getReferenceNo()) {
			Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
			if (posResponse.getResponseCode().equalsIgnoreCase("00")) {
				ResponseResult responseResult = memberCashValueService.handlePosPaymentResult(posResponse);
				if (GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())) {
					CustomerOrderTrans customerOrderTrans = customerOrderTransService
							.getCustomerOrderTrans(transactionNo);
					CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
					TopUpDto topUpDto = new TopUpDto();
					topUpDto.setCustomerId(customerOrderHd.getCustomerId());
					topUpDto.setTransactionNo(transactionNo);
					topUpDto.setPaymentMethod(CommUtil.getCardType(posResponse.getCardType()));
					memberCashValueService.sentTopupEmail(topUpDto);
				}
			} else {
				CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
				customerOrderTrans.setStatus(CustomerTransationStatus.FAIL.getDesc());
				customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
				customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
				customerOrderTrans.setInternalRemark(posResponse.getResponseText());
				customerOrderTrans.setPaymentMethodCode(CommUtil.getCardType(posResponse.getCardType()));
				customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
				
				ecrLog.info(Log4jFormatUtil.logInfoMessage(null,null,null, "update CustomerOrderTrans : transactionNo="+transactionNo+",status:"+customerOrderTrans.getStatus()+
						",paymentmethodCode:"+customerOrderTrans.getPaymentMethodCode()));
				
			}
		}
		return responseResult.getReturnCode();
	}

	@RequestMapping(value = "/transaction/topupByCard", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult topUpByCard(@RequestBody TopUpDto topUpDto) {
		logger.info("topUp by bank Card come in.....");
		try {
			topUpDto.setStaffUserId(super.getUser().getUserId());
			if(PaymentMethod.AMEX.getCardCode().equals(topUpDto.getPaymentMethod()))
			{
				logger.info("check payment American Express payment turn on/off start..");
				responseResult=globalParameterService.checkPaymentCardTypeTurnOn(GlobalParameterType.PAYMENT_TYPE_AMEX.getCode());
				if(!responseResult.getReturnCode().equals(GTAError.Success.SUCCESS.getCode()))
				{
					logger.error("system setting American Express payment turn  off .. ");
					return responseResult;
				}
				logger.info("system setting American Express payment turn  on... ");
			}
			responseResult = memberCashValueService.topupByCard(topUpDto);
			if ("0".equals(responseResult.getReturnCode())) {
				CustomerOrderTrans customerOrderTrans = (CustomerOrderTrans) responseResult.getDto();
				String redirectUrl=null;
				if(PaymentMethod.AMEX.getCardCode().equals(topUpDto.getPaymentMethod()))
				{
					 redirectUrl =amexPaymentGatewayService.payment(customerOrderTrans);
				}else{
					 redirectUrl = paymentGatewayService.payment(customerOrderTrans);
				}
				Map<String, String> redirectUrlMap = new HashMap<String, String>();
				redirectUrlMap.put("redirectUrl", redirectUrl);
				redirectUrlMap.put("transactionNo", customerOrderTrans.getTransactionNo().toString());
				logger.info("setting credit card Redirection url to Response.."+redirectUrl);
				responseResult.setData(redirectUrlMap);
			}else{
				logger.info("create topUpByCard record have problem...no setting redirection url...and no pop BANK PAYMENT UI.");
			}
			logger.info("topUp by request bank Card gateway is success...........");
			return responseResult;
		} catch (Exception e) {
			logger.error("CustomerMemberController.topUpByCard Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	// modified by Kaster 20160323 增加@EncryptFieldInfo
	@RequestMapping(value = "/transaction/getTransactionList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionList(@RequestParam("customerID") @EncryptFieldInfo Long customerID,
			@RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") String pageSize,
			@RequestParam(value = "sortBy", defaultValue = "ps.transactionTimestamp") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "clientType", required = false) String clientType,
			@RequestParam(value = "filters", required = false) String filters) {
		try {
			AdvanceQueryDto query = null;
			if (!StringUtils.isEmpty(filters)) {
				query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			}
			return memberCashValueService.getTransactionList(customerID, pageNumber, pageSize, sortBy, isAscending,
					clientType, query);
		} catch (GTACommonException gta) {
			// gta.printStackTrace();
			logger.error("CustomerMemberController.getTransactionList", gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getTransactionList", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * SGG-1592:Return all transaction data
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param isAscending
	 * @param customerID
	 * @param clientType
	 * @return
	 */
	@RequestMapping(value = "/transaction/getAllTransactionList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllTransactionList(
			@RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") String pageSize,
			@RequestParam(value = "sortBy", defaultValue = "c.transactionTimestamp") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam("customerID") @EncryptFieldInfo Long customerID,
			@RequestParam(value = "clientType", required = false) String clientType,
			@RequestParam(value = "filters", required = false) String filters) {
		try {
			ListPage page = new ListPage();
			page.setSize(Integer.parseInt(pageSize));
			page.setNumber(Integer.parseInt(pageNumber));
			if (!StringUtils.isEmpty(sortBy)) {
				if ("false".equals(isAscending)) {
					page.addDescending(sortBy);
				} else {
					page.addAscending(sortBy);
				}
			}
			AdvanceQueryDto query = null;
			if (!StringUtils.isEmpty(filters)) {
				query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			}
			ListPage list = memberCashValueService.getAllTransactionList(customerID, page, clientType, query);
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (GTACommonException gta) {
			// gta.printStackTrace();
			logger.error("CustomerMemberController.getAllTransactionList", gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getAllTransactionList", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	@RequestMapping(value = "/transaction/getVoidPaymentList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getVoidPaymentList(
			@RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") String pageSize,
			@RequestParam(value = "sortBy", defaultValue = "c.transactionTimestamp") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam("customerID") @EncryptFieldInfo Long customerID,
			@RequestParam(value = "clientType", required = false) String clientType,
			@RequestParam(value = "filters", required = false) String filters) {
		try {
			ListPage page = new ListPage();
			page.setSize(Integer.parseInt(pageSize));
			page.setNumber(Integer.parseInt(pageNumber));
			if (!StringUtils.isEmpty(sortBy)) {
				if ("false".equals(isAscending)) {
					page.addDescending(sortBy);
				} else {
					page.addAscending(sortBy);
				}
			}
			AdvanceQueryDto query = null;
			if (!StringUtils.isEmpty(filters)) {
				query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			}
			ListPage list = memberCashValueService.getVoidPaymentList(customerID, page, clientType, query);
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (GTACommonException gta) {
			// gta.printStackTrace();
			logger.error("CustomerMemberController.getAllTransactionList", gta);
			responseResult.initResult(gta.getError());
			return responseResult;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getAllTransactionList", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	@RequestMapping(value = "/transaction/getVoidPaymentList/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getVoidPaymentListAdvancedSearch() {
		this.responseResult.initResult(GTAError.Success.SUCCESS,
				this.memberCashValueService.getgetVoidPaymentListAdvancedSearch());
		return this.responseResult;
	}

	@RequestMapping(value = "/transaction/getAllTransactionList/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllTransactionListAdvancedSearch() {
		this.responseResult.initResult(GTAError.Success.SUCCESS,
				this.memberCashValueService.getAllTransactionListAdvancedSearch());
		return this.responseResult;
	}

	@RequestMapping(value = "/transaction/getTransactionList/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult transactionListAdvancedSearch() {
		this.responseResult.initResult(GTAError.Success.SUCCESS,
				this.memberCashValueService.transactionListAdvanceSearch());
		return this.responseResult;
	}

	// modified by Kaster 20160323 增加@EncryptFieldInfo
	@RequestMapping(value = "/transaction/getTopupHistory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTopupHistory(@RequestParam("customerID") @EncryptFieldInfo Long customerID,
			@RequestParam("pageNumber") String pageNumber, @RequestParam("pageSize") String pageSize,
			@RequestParam(value = "sortBy", defaultValue = "c.transactionTimestamp") String sortBy,
			@RequestParam("isAscending") String isAscending,
			@RequestParam(value = "filters", required = false) String filters) throws Exception {

		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}
		ResponseResult responseResult = memberCashValueService.getTopupHistory(customerID, pageNumber, pageSize, sortBy,
				isAscending, query);
		return responseResult;
	}

	@RequestMapping(value = "/transaction/getTopupHistory/advancedSearch", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult topupHistoryAdvancedSearch() {
		this.responseResult.initResult(GTAError.Success.SUCCESS,
				this.memberCashValueService.topupHistoryAdvanceSearch());
		return this.responseResult;
	}

	@RequestMapping(value = "/getCustomerName/{userId}", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult getCustomerIDByUserId(@PathVariable(value = "userId") String userId) {
		try {
			return customerProfileService.getCustomerIdByUserId(userId);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getCustomerIDByUserId Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/getDependentMemberProfile/{customerId}", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult getDependentMemberProfileByCustomerId(
			@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId) {
		try {
			return customerProfileService.getDependentMmeberInforByCustomerId(customerId);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getDependentMemberProfileByCustomerId Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/updateDependentMemberRights", method = { RequestMethod.PUT })
	@ResponseBody
	public ResponseResult updateDependentMembersRights(@RequestBody DependentMemberRightsDto dependentMemberRightsDto) {
		try {
			Long customerId = dependentMemberRightsDto.getCustomerId();
			String facilityRight = dependentMemberRightsDto.getFacilityRight();
			String eventRight = dependentMemberRightsDto.getEventRight();
			String trainingRight = dependentMemberRightsDto.getTrainingRight();
			String dayPass = dependentMemberRightsDto.getDayPass();
			BigDecimal tran = dependentMemberRightsDto.getTran();
			
			customerProfileService.setMemberLimitExpridate(customerId);
			return customerProfileService.updateDependentMembersRight(getUser().getUserId(),customerId, facilityRight, trainingRight,
					eventRight, dayPass, tran);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.updateDependentMembersRights Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/dependentCreate/{superiorMemberId}",
			"/{superiorMemberId}/dependentmember" }, method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult createDepartmentMember(
			@PathVariable(value = "superiorMemberId") @EncryptFieldInfo Long superiorMemberId,
			HttpServletRequest request) {
		logger.info("CustomerMemberController.createDepartmentMember invocation start ...");
		try {
			CustomerProfile cProfile = (CustomerProfile) parseJson(request, CustomerProfile.class);
			if (StringUtils.isEmpty(String.valueOf(superiorMemberId))) {
				// return new ResponseResult("1", "Member's superiorMemberId is
				// null!");
				responseResult.initResult(GTAError.MemberShipError.SUPERIOR_OF_MEMBER_IS_NULL);
				return responseResult;
			}

			Member member = new Member(superiorMemberId);
			cProfile.setMember(member);
			responseResult = dependentMemberService.saveDependentMemberInfo(cProfile, super.getUser().getUserId(),getUser().getUserName());
			if ("0".equals(responseResult.getReturnCode())) {
				customerProfileService.moveProfileAndSignatureFile((CustomerProfile) responseResult.getDto());
			}
			return responseResult;
		} catch (GTACommonException ce) {
			logger.error("createDepartmentMember Exception", ce);
			// ce.printStackTrace();
			responseResult.initResult(ce.getError());
			return responseResult;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.createDepartmentMember Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = { "/dependentMember/{customerId}",
			"/dependentUpdate/{customerId}" }, method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg updateDepartmentMember(@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId,
			HttpServletRequest request) {

		logger.info("CustomerMemberController.updateDepartmentMember invocation start ...");
		String updateBy = super.getUser().getUserId();
		try {
			CustomerProfile cProfile = (CustomerProfile) parseJson(request, CustomerProfile.class);
			if (StringUtils.isEmpty(customerId)) {
				// return new ResponseMsg("1", "Member's customerId is null!");
				responseResult.initResult(GTAError.MemberShipError.CUSTOMERID_OF_MEMBER_IS_EMPTY);
				return responseResult;
			}
			cProfile.setCustomerId(customerId);
			return dependentMemberService.updateDependentMemberInfo(cProfile, updateBy, getUser().getUserName());
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.updateDepartmentMember Exception : ", e);
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}

	@RequestMapping(value = "/updateprofilephoto", method = RequestMethod.POST)
	public @ResponseBody ResponseMsg updateprofilephoto(HttpServletRequest request, HttpServletResponse response) {
		logger.info("CustomerMemberController.updateprofilephoto invocation start ...");
		try {
			CustomerProfile cProfile = (CustomerProfile) super.parseJson(request, CustomerProfile.class);
			cProfile.setUpdateBy(super.getUser().getUserId());
			return dependentMemberService.updateProfilePhoto(cProfile);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.updateprofilephoto Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/dependentMember/{academyNo}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDependentMemberInfo(@PathVariable String academyNo,
			@RequestParam(value = "driverType" ,required = false) String driverType) {
		logger.error("CustomerMemberController.getDepartmentMemberInfo start!");
		try {
			ResponseResult responseResult = dependentMemberService.checkDependentMember(academyNo,driverType);
			if (null==responseResult.getDto()) {
				return responseResult;
			}
			MemberDto memberDto = (MemberDto) responseResult.getDto();
			if (memberDto.isDependentOrPrimary()) {
				return dependentMemberService.getDependentMemberByAcademyNo(academyNo);
			} else {
				return responseResult;
			}
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getDependentMemberInfo Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/dependentMemberByCustomerId/{customerId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult dependentMemberByCustomerId(@PathVariable @EncryptFieldInfo Long customerId) {
		logger.error("CustomerMemberController.dependentMemberByCustomerId start!");
		try {
			return dependentMemberService.getDepartMemberByCustomerId(customerId);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("Search dependentMember failed!");
			responseResult.initResult(GTAError.MemberShipError.SEARCH_DEPENDENTMEMBER_FAILED);
			return responseResult;
		}
	}

	@RequestMapping(value = "/getCustomerProfile", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getCustomerProfile(
			@RequestParam(value = "daypassId", required = false) Long daypassId,
			@RequestParam(value = "customerId", required = false) String customerId) {
		try {
			return dependentMemberService.getCustomerProfile(daypassId, customerId, "StaffPortal");
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/getCustomerProfile4Formkit", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getCustomerProfile4Formkit(
			@RequestParam(value = "daypassId", required = false) Long daypassId,
			@RequestParam(value = "customerId", required = false) String customerId) {
		try {
			return dependentMemberService.getCustomerProfile(daypassId, customerId, "FormKit");
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * This function is used to save the profile photo for each enrollment
	 * record.
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/saveprofilephoto", method = RequestMethod.POST)
	public @ResponseBody ResponseResult saveProfile(@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		// ResponseResult responseResult = new ResponseResult();
		if (file == null || file.isEmpty()) {
			logger.info("File is empty!");
			// responseResult.setReturnCode("1");
			// responseResult.setErrorMessageEN("{\"warning\":\"File is
			// empty!\"}");
			responseResult.initResult(GTAError.MemberShipError.FIlE_IS_EMPTY, "{\"warning\":\"File is empty!\"}");
			return responseResult;
		}
		String saveFilePath = "";
		try {
			saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.USER);
		} catch (Exception e) {
			// responseResult.setReturnCode("1");
			// responseResult.setErrorMessageEN("fail");
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.FIlE_UPLOAD_FAIL);
			return responseResult;
		}
		File serverFile = new File(saveFilePath);
		saveFilePath = serverFile.getName();
		// responseResult.setReturnCode("0");
		responseResult.initResult(GTAError.Success.SUCCESS);
		Map<String, String> imgPathMap = new HashMap<String, String>();
		imgPathMap.put("imagePath", "/" + saveFilePath);
		responseResult.setData(imgPathMap);
		return responseResult;
	}

	// modified by Kaster 20160323 增加@EncryptFieldInfo
	@RequestMapping(value = "/{customerId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getPrimaryMemberName(
			@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId) {
		logger.info("CustomerMemberController.getPrimaryMemberName invocation start ...");
		try {
			Member m = memberService.getMemberById(customerId);
			if (null == m) {
				// return new ResponseResult("1", "can't find the member with
				// member Id:"+customerId);
				responseResult.initResult(GTAError.MemberShipError.MEMBER_IS_NOT_FOUND, new Object[] { customerId });
				return responseResult;
			}
			if ("IPM".equals(m.getMemberType()) || "CPM".equals(m.getMemberType())) {
				CustomerProfile cp = customerProfileService.getByCustomerID(customerId);
				MemberDto memberDto = new MemberDto();
				memberDto.setCustomerName(cp.getSalutation() + " " + cp.getGivenName() + " " + cp.getSurname());
				responseResult.initResult(GTAError.Success.SUCCESS, memberDto);
				return responseResult;
			} else {
				// return new ResponseResult("1", "Member type is not
				// primaryMember!");
				responseResult.initResult(GTAError.MemberShipError.MEMBER_IS_NOT_PRIMARY);
				return responseResult;
			}
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getPrimaryMemberName Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	@RequestMapping(value = "/recognitionHistory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRecognitionHistory(
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending, HttpServletRequest request,
			HttpServletResponse response) {
		String joinHQL = "select  p.card_purpose cardType,m.customer_id id, CONCAT(c.salutation,' ',c.given_Name,' ',c.surname) name "
				+ "from PermitCardMaster p ,Member m ,CustomerProfile c "
				+ "	where  p.mappingCustomerId=m.customerId and c.customerId=m.customerId  ";
		try {
			String jsonStr = (String) request.getParameter("filters");
			if (jsonStr != null) {
				joinHQL += " and ";
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(jsonStr, AdvanceQueryDto.class);
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				return advanceQueryService.getAdvanceQueryResultByHQL(queryDto, joinHQL, page);
			} else {
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				return advanceQueryService.getInitialQueryResultByHQL(queryDto, joinHQL, page);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getRecognitionHistory Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/getMyAccountRights/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberAccountRightsByCustomerId(@PathVariable(value = "customerId") Long customerId) {
		try {
			return customerProfileService.getMmeberRightsByCustomerId(customerId);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getMemberAccountRightsByCustomerId Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/checkMemberPurchaseLimit", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkMemberLimit(@RequestParam(value = "customerId") Long customerId,
			@RequestParam(value = "spending") BigDecimal spending) {

		try {

			return memberService.checkTransactionLimit(customerId, spending);

		} catch (Exception e) {
			logger.error("MemberController check Member Purchase Limit Error:", e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION);
			return responseResult;
		}
	}

	// added by Kaster 20160323 增加@EncryptFieldInfo
	@RequestMapping(value = "/updateStatus", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateMemberStatus(@RequestParam(value = "status") String status,
			@RequestParam(value = "customerId") @EncryptFieldInfo Long customerId) {
		try {
			if (status.equalsIgnoreCase("ACT") || status.equalsIgnoreCase("NACT")) {
				return corporateService.changeMemberStatus(customerId, super.getUser().getUserId(), status,
						getUser().getFullname() == null ? getUser().getUserName() : getUser().getFullname());
			} else {
				// return new ResponseResult("1", "","Please provide a valid
				// status!");
				responseResult.initResult(GTAError.MemberShipError.INVALID_STATUS);
				return responseResult;
			}
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.updateMemberStatus Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/{passportType}/{passportNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCustomerProfile(@PathVariable("passportType") String passportType,
			@PathVariable("passportNo") String passportNo) {
		try {
			CustomerProfile customerProfile = customerProfileService
					.getCustomerProfileByPassportTypeAndPassportNo(passportType, passportNo);
			responseResult.initResult(GTAError.Success.SUCCESS, customerProfile);
			return responseResult;
		} catch (RuntimeException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.LeadError.NOT_FOUND_CUSTOMER_PROFILE_BY_PASSPORT);
			return responseResult;
		}
	}

	@RequestMapping(value = "/verifyMemberInfo/{surname}/{givenName}/{passportType}/{passportNo}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult verifyMemberInfo(@PathVariable(value = "surname") String surname,
			@PathVariable(value = "givenName") String givenName,
			@PathVariable(value = "passportType") String passportType,
			@PathVariable(value = "passportNo") String passportNo) throws Exception

	{

		return customerProfileService.verifyMemberInfo(surname, givenName, passportType, passportNo);
	}

	@RequestMapping(value = "/validaterAnswer", method = RequestMethod.POST)
	public @ResponseBody ResponseResult validateBirthDateOrQuestionAnswer(
			@RequestBody QuestionAnswerOrBirthDateDto questionAnswerOrBirthDateDto) throws Exception

	{
		return customerProfileService.validateBirthDateOrQuestionAnswer(questionAnswerOrBirthDateDto);
	}

	@RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
	public @ResponseBody ResponseResult updatePwd(@RequestBody StaffPasswordDto staffPasswordDto) throws Exception

	{
		return customerProfileService.updateMemberPwd(staffPasswordDto);
	}

	@RequestMapping(value = "/getUserActiveRequest/{userId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getUserActiveRequest(@PathVariable(value = "userId") String userId) {
		return customerProfileService.getUserActiveRequest(userId);
	}

	@RequestMapping(value = "/checkmember/{passportType}/{passportNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkMemberExistByPassportTypeAndPassportNo(@PathVariable("passportType") String passportType,
			@PathVariable("passportNo") String passportNo) throws Exception {
		this.responseResult = customerProfileService.checkMemberExistByPassportTypeAndPassportNo(passportType,
				passportNo);
		return this.responseResult;
	}

	/**
	 * Used for Showing Booking Records on My Booking Records of Portal(PC)
	 * modified by Kaster 20160323 增加@EncryptFieldInfo
	 * 
	 * @param customerId
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @return
	 */
	@RequestMapping(value = "/bookingRecords", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBookingRecords(@RequestParam @EncryptFieldInfo Long customerId,
			@RequestParam(value = "sortBy", defaultValue = "bookingDateFull") String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "notShowStatus", defaultValue = "") String notShowStatus) {
		/** 解析不需要显示的状态【暂支持CAN状态，其它带扩展】 */
		AdvancedQueryConditionsDto dto = new AdvancedQueryConditionsDto(notShowStatus);
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			return memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId, "PC", null, null,
					null, null, null, dto);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getBookingRecords Exception ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * Used to Get My Booking For Member APP in Calendar(APP My Booking), and
	 * Current Booking for Web Portal (WP) E.g. Will return all the booking for
	 * the selected month(year: 2015, month: 12) for Web Portal The booking from
	 * today for Web Portal
	 * 
	 * @param customerId
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param appType
	 *            WP: Web Portal; AP: Member APP
	 * @param year
	 *            Mandatory for Member APP. E.g. 2015
	 * @param month
	 *            Mandatory for Member APP. E.g. 12
	 * @return
	 */
	@RequestMapping(value = "/myBooking", method = RequestMethod.GET)
	@ResponseBody
	// modified by Kaster 20160323 增加@EncryptFieldInfo
	public ResponseResult getMyBookingForMemberAppAndWebPortalCurrentBooking(
			@RequestParam @EncryptFieldInfo Long customerId,
			@RequestParam(value = "sortBy", defaultValue = "beginDate,resvId") String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "appType", required = false, defaultValue = "WP") String appType,
			@RequestParam(value = "year", required = false, defaultValue = "") String year,
			@RequestParam(value = "month", required = false, defaultValue = "") String month,
			@RequestParam(value = "notShowStatus", defaultValue = "") String notShowStatus) {
		/** 解析不需要显示的状态【暂支持CAN状态，其它带扩展】 */
		AdvancedQueryConditionsDto aqcDto = new AdvancedQueryConditionsDto(notShowStatus);

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			ResponseResult result = memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId,
					appType, year, month, null, null, null, aqcDto);
			final String mapIsAscending = isAscending;
			Map<String, List<MemberFacilityTypeBookingDto>> map = manualSortingInMap(mapIsAscending);
			Data data = (Data) result.getListData();
			assembleMyBookingDataInMap(data, map, appType);
//			System.out.println(JSON.toJSONString(data));
			result.setData(data);
			return result;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getBookingRecordsForWebPortal Exception ", e);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
	}

	/**
	 * Used to Get the My Booking History for Web Portal(WP)
	 * 
	 * @author Liky_Pan
	 * @since 2015-12-10
	 * @param customerId
	 *            The customer Id of the Member
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param appType
	 *            "WP" for Web Portal
	 * @param startDate
	 *            The start date of the booking, E.g. 2015-11-01
	 * @param endDate
	 *            The end date of the booking, E.g. 2015-12-01
	 * @return
	 */
	@RequestMapping(value = "/webPortalBookingHistory", method = RequestMethod.GET)
	@ResponseBody
	// modified by Kaster 20160323 增加@EncryptFieldInfo
	public ResponseResult getMyBookingForWebPortal(@RequestParam @EncryptFieldInfo Long customerId,
			@RequestParam(value = "sortBy", defaultValue = "beginDate") String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "appType", required = false, defaultValue = "WP") String appType,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate,
			@RequestParam(value = "notShowStatus", defaultValue = "") String notShowStatus) {
		/** 解析不需要显示的状态【暂支持CAN状态，其它带扩展】 */
		AdvancedQueryConditionsDto aqcDto = new AdvancedQueryConditionsDto(notShowStatus);

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			ResponseResult result = memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId,
					appType, null, null, null, startDate, endDate, aqcDto);
			final String mapIsAscending = isAscending;
			Map<String, List<MemberFacilityTypeBookingDto>> map = manualSortingInMap(mapIsAscending);
			Data data = (Data) result.getListData();
			assembleMyBookingDataInMap(data, map, null);
			result.setData(data);
			return result;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getBookingRecordsForWebPortal Exception ", e);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
	}

	/**
	 * Used to Get Booking Records for Member APP's Home Page and Recognition
	 * Tablet's Booking History(APP, ANDROID TABLET)
	 * 
	 * @param customerId
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param appType
	 *            "AP" for Member APP, Android Recognition Tablet
	 * @param year
	 *            Mandatory Year. E.g. 2015
	 * @param month
	 *            Mandatory Month. E.g. 12
	 * @param day
	 *            Mandatory Day. E.g. 01
	 * @return
	 * 
	 * 		modified by Kaster 20160505 增加@EncryptFieldInfo
	 */
	@RequestMapping(value = { "/appHomeBooking", "/personalBookingHistory" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBookingRecordsForMemberAppHomePageAndTabletBookingHistory(
			@RequestParam @EncryptFieldInfo Long customerId,
			@RequestParam(value = "sortBy", defaultValue = "beginDate") String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "appType", required = false, defaultValue = "AP") String appType,
			@RequestParam(value = "year", required = true) String year,
			@RequestParam(value = "month", required = true) String month,
			@RequestParam(value = "day", required = true) String day,
			@RequestParam(value = "notShowStatus", defaultValue = "") String notShowStatus) {
		/** 解析不需要显示的状态【暂支持CAN状态，其它带扩展】 */
		AdvancedQueryConditionsDto aqcDto = new AdvancedQueryConditionsDto(notShowStatus);

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {

			if (!StringUtils.isEmpty(year) && !StringUtils.isEmpty(month) && !StringUtils.isEmpty(day)) {
				ResponseResult result = memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId,
						appType, year, month, day, null, null, aqcDto);
				Data data = (Data) result.getListData();
				for (Object item : data.getList()) {
					MemberFacilityTypeBookingDto dto = (MemberFacilityTypeBookingDto) item;
					setRemainingTimeForMyBooking(dto);
				}

				responseResult.initResult(GTAError.Success.SUCCESS, data);
				return responseResult;
			} else {
				Data data = new Data();
				data.setList(new ArrayList());
				responseResult.initResult(GTAError.Success.SUCCESS, data);
				return responseResult;
			}
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getBookingRecordsForWebPortal Exception ", e);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
	}
	
	@RequestMapping(value ="/appHomePageAndTabletBookingHistory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAppHomePageAndTabletBookingHistory(
			@RequestParam @EncryptFieldInfo Long customerId,
			@RequestParam(value = "sortBy", defaultValue = "beginDate") String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "appType", required = false, defaultValue = "WP") String appType,
			@RequestParam(value = "startDate", required = true) String startDate,
			@RequestParam(value = "endDate", required = true) String endDate,
			@RequestParam(value = "notShowStatus", defaultValue = "") String notShowStatus) {
		/** 解析不需要显示的状态【暂支持CAN状态，其它带扩展】 */
		AdvancedQueryConditionsDto aqcDto = new AdvancedQueryConditionsDto(notShowStatus);

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {

			ResponseResult result = memberFacilityTypeBookingService.getBookingRecordsByCustomerId(page, customerId,
						appType, null, null, null, startDate, endDate, aqcDto);
				Data data = (Data) result.getListData();
				for (Object item : data.getList()) {
					MemberFacilityTypeBookingDto dto = (MemberFacilityTypeBookingDto) item;
					setRemainingTimeForMyBooking(dto);
				}

				responseResult.initResult(GTAError.Success.SUCCESS, data);
				return responseResult;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getBookingRecordsForWebPortal Exception ", e);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
	}


	/**
	 * Used for Get Customized Information for Member on Tablet Recognition
	 * modified by Kaster 20160323 增加@EncryptFieldInfo
	 * 
	 * @author Liky_Pan
	 * @param customerId
	 *            Customer Id of Current Member
	 * @return ResponseResult
	 */
	@RequestMapping(value = "/personalInfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPersonalInfoForTablet(
			@RequestParam(value = "customerId", required = false, defaultValue = "") @EncryptFieldInfo Long customerId,
			@RequestParam(value = "academyNo", required = false, defaultValue = "") String academyNo,
			@RequestParam(value = "device", required = false, defaultValue = "") String device) {
		if (!StringUtils.isEmpty(academyNo) && StringUtils.isEmpty(customerId)) {
			Member member = this.memberService.getMemberByAcademyNo(academyNo);
			if (member != null) {
				customerId = member.getCustomerId();
			}
		}
		Map<String, Object> map =null;
		if(null!=customerId){
			map=customerProfileService.getPersonalInfoForTablet(customerId, device);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}
	
	/***
	 * SGG-3764
	 * @param academyNo
	 * @return
	 */
	@RequestMapping(value = "/getPersonalInfoList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPersonalInfoList(@RequestParam(value = "condition",required=false) String condition)
	{
		return customerProfileService.getPersonalInfoListByCondition(condition);
	}

	/**
	 * Used for Get Customized Information for PC portal
	 * 
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "/AnalysisInfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPersonalInfoForPortal(@RequestParam Long customerId) {
		List<CustomerAdditionInfoDto> list = customerProfileService.getAnalysisInfo(customerId);
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}

	/**
	 * Import Primary Member's Part Detail for Auto-filling the Dependent
	 * Member's Form During Creating
	 * 
	 * @author Liky_Pan
	 * @param customerId
	 *            Customer Id of Primary Member
	 * @return ResponseResult
	 */
	@RequestMapping(value = "/importPrimaryDetail", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPrimaryDetailForDependentAutoFilling(@RequestParam @EncryptFieldInfo Long customerId) {
		responseResult.initResult(GTAError.Success.SUCCESS,
				customerProfileService.importPrimaryDeailForAutofillingDependentForm(customerId));
		return responseResult;
	}

	private Map<String, List<MemberFacilityTypeBookingDto>> manualSortingInMap(final String mapIsAscending) {
		return new TreeMap<String, List<MemberFacilityTypeBookingDto>>(new Comparator<String>() {
			public int compare(String obj1, String obj2) {
				if (mapIsAscending.equals("false")) {
					return obj2.compareTo(obj1);
				} else {
					return obj1.compareTo(obj2);
				}
			}
		});
	}

	private void assembleMyBookingDataInMap(Data data, Map<String, List<MemberFacilityTypeBookingDto>> map,
			String appType) {
		// data.getList desc
		for (Object item : data.getList()) {
			MemberFacilityTypeBookingDto dto = (MemberFacilityTypeBookingDto) item;
			setRemainingTimeForMyBooking(dto);
			String date = DateConvertUtil.getYMDFormatDate(dto.getBeginDate());
			if (!map.containsKey(date)) {
				List<MemberFacilityTypeBookingDto> list = new ArrayList<MemberFacilityTypeBookingDto>();
				list.add(dto);
				map.put(date, list);
			} else {
				map.get(date).add(dto);
			}
		}
		// add memberAPP booking display date is desc and same date is asc
		if (!StringUtils.isEmpty(appType) && "AP".equalsIgnoreCase(appType)) {
			// map list<MemberFacilityTypeBookingDto>asc
			Iterator it = map.keySet().iterator();
			while (it.hasNext()) {
				List<MemberFacilityTypeBookingDto> list = map.get(it.next());
				Collections.sort(list, new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						MemberFacilityTypeBookingDto m1 = (MemberFacilityTypeBookingDto) o1;
						MemberFacilityTypeBookingDto m2 = (MemberFacilityTypeBookingDto) o2;
						return m1.getBeginDate().compareTo(m2.getBeginDate());
					}
				});
			}
		}
		List<Map<String, List<MemberFacilityTypeBookingDto>>> alist = new ArrayList<Map<String, List<MemberFacilityTypeBookingDto>>>();
		alist.add(map);
		data.setList(alist);
	}

	private void setRemainingTimeForMyBooking(MemberFacilityTypeBookingDto dto) {
		if (dto.getBeginDate() == null) {
			dto.setBeginDate(new Date());
		}
		if ("RESTAURANT".equals(dto.getFacilityType())) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dto.getBeginDate());
			cal.add(Calendar.HOUR, 2);
			dto.setEndDate(cal.getTime());
		}
		//GUESTROOM CHEING IN TIME 2PM
		Date begineDate=dto.getBeginDate();
		if("GUESTROOM".equals(dto.getFacilityType()))
		{
			begineDate=DateCalcUtil.getDateAddMins(dto.getBeginDate(), 14*60); 
		} 
		dto.setRemainingTime(DateConvertUtil.calcRemainingTime_new(responseResult,begineDate, dto.getEndDate()));
	}

	/**
	 * 根据类型获取Overview列表
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param status
	 * @param isAscending
	 * @return
	 */
	@RequestMapping(value = "/getOverviewList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getOverviewList(
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "createDate", required = false) String sortBy,
			@RequestParam(value = "status", defaultValue = "HKGTA", required = false) String status,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters) {
		logger.info("CustomerMemberController.getOverviewList start ...");
		try {
			customerAdditionInfoService.initMarketingValue();
			String joinSQL = customerProfileService.getOverviewList(status);
			ResponseResult tmpResult = null;
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null
					&& advanceQueryDto.getGroupOp().length() > 0) {
				joinSQL = joinSQL + " AND ";
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
				tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page,
						OverviewListDto.class);
			} else {
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page,
						OverviewListDto.class);
			}
			return tmpResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * 是否批准营销服务
	 * 
	 * @param dto
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/isAllowMarketing", method = RequestMethod.POST)
	public @ResponseBody ResponseResult isAllowMarketing(@RequestBody AllowMarketingDto dto) {
		try {
			if (dto == null || null == dto.getAllowType() || dto.getCustomerId() == null) {
				responseResult.initResult(GTAError.CommonError.REQUIRED_PARAMETERS_IS_NULL);
			} else {
				responseResult = customerAdditionInfoService.isAllowMarketing(dto, getUser().getUserId());
			}
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * HKGTA current liability arising from Cash Value Top-up Calculation(计算会员要求退款时的退款金额)
	 */
	@RequestMapping(value = "/transaction/liability/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getLiabilityAmount(@PathVariable("customerId") @EncryptFieldInfo Long customerId) {
		try {
			return memberCashValueService.getLiabilityAmount(customerId);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("CustomerMemberController.getLiabilityAmount Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	
	/**
	 * 根据类型获取用户分析报告
	 * @param categoryType
	 * @return
	 */
	@RequestMapping(value = "/getCustomerAnalysisReport", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCustomerAnalysisReport(@RequestParam String categoryType) {
		Map<String, Map<String, List<CustomerAnalysisReportDto>>> map = customerProfileService.getCustomerAnalysisReport(categoryType);
		responseResult.initResult(GTAError.Success.SUCCESS, map);
		return responseResult;
	}

	@RequestMapping(value = "/logReadCustomerProfile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult logReadCustomerProfile(@RequestBody CustomerProfileReadLog customerProfileReadLog)
	{
		customerProfileReadLog.setReaderUserId(this.getUser().getUserId());
		return customerProfileService.addCustomerProfileReadLog(customerProfileReadLog);
		
	}
	
	/**
	 * Close Account to archive the dependent member account
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "/closeAccount", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult closeAccount(@RequestParam(value = "customerId") @EncryptFieldInfo Long customerId) {
		try {
			String loginUserId = getUser().getUserId();
			Member member = this.memberService.getMemberById(customerId);
			if(member == null) {
				responseResult.initResult(GTAError.CorporateManagementError.MEMBER_NOT_FOUND);
				return responseResult;
			}
//			if(member.getMemberType().equalsIgnoreCase(MemberType.IDM.name()) || member.getMemberType().equalsIgnoreCase(MemberType.CDM.name())){
			return this.customerProfileService.archiveMember(customerId, loginUserId,
					"NACT",getUser().getFullname()==null?getUser().getUserName():getUser().getFullname());
//			}else{
//				responseResult.initResult(GTAError.MemberShipError.CAN_NOT_CLOSE_PRIMARY_PARTON);
//				return responseResult;
//			}
		} catch (Exception e) {
			logger.error("CustomerMemberController.closeAccount Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * Re-open Account to archive the dependent member account
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "/reOpenAccount", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult reOpenAccount(@RequestParam(value = "customerId") @EncryptFieldInfo Long customerId)  throws Exception {
		String loginUserId = getUser().getUserId();
		Member member = this.memberService.getMemberById(customerId);
		if(member == null) {
			responseResult.initResult(GTAError.CorporateManagementError.MEMBER_NOT_FOUND);
			return responseResult;
		}
//		if(member.getMemberType().equalsIgnoreCase(MemberType.IDM.name()) || member.getMemberType().equalsIgnoreCase(MemberType.CDM.name())){
		return this.customerProfileService.unArchiveMember(customerId, loginUserId,
				"ACT",getUser().getFullname()==null?getUser().getUserName():getUser().getFullname());
//		}else{
//			responseResult.initResult(GTAError.MemberShipError.CAN_NOT_RE_OPEN_PRIMARY_PARTON);
//			return responseResult;
//		}
	}
	@RequestMapping(value = "/getPhoto/{type}/{customerId}", method = RequestMethod.GET)
	public void saveProfile(@PathVariable(value = "type") String type,
			@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			CustomerProfileDto dto = customerProfileService.getCustomerProfilePhotoAndSinature(customerId);
			if (null != dto) {
				if (PhotoType.P.name().equals(type) && !StringUtils.isEmpty(dto.getPortraitPhoto())) {
					response.getOutputStream().write(CommUtil.base64ToImage(dto.getPortraitPhoto()));
				} else if (PhotoType.S.name().equals(type) && !StringUtils.isEmpty(dto.getSignature())) {
					response.getOutputStream().write(CommUtil.base64ToImage(dto.getSignature()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
	}
}
