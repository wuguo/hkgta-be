package com.sinodynamic.hkgta.controller.fms;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.TokenUtils;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/coach/report")
public class CoachReportController extends ControllerBase {

	@Autowired
	private UserMasterService					userMasterService;

	@Autowired
	private MemberFacilityTypeBookingService	memberFacilityTypeBookingService;

	@Autowired
	private CustomerOrderTransService			customerOrderTransService;

	@RequestMapping(value = "/dailyPrivateCoaching", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDailyFacilityUsage(
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "selectedDate", required = true) String selectedDate,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ResponseResult responseResult = memberFacilityTypeBookingService.getDailyMonthlyPrivateCoaching("DAILY", page, selectedDate, facilityType);
		return responseResult;
	}

	@RequestMapping(value = "/dailyPrivateCoachingAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyFacilityUsageAttach(@RequestParam(value = "selectedDate", required = true) String selectedDate,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			String facilityTypeConvert = "GOLF".equals(facilityType) || "TENNIS".equals(facilityType) ? ("TENNIS".equals(facilityType) ? "Tennis"
					: "Golf") : "All";
			chooseFileType(
					request,
					response,
					"Daily Private Coaching (" + facilityTypeConvert + ") - "
							+ new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate)),
					fileType);
			return memberFacilityTypeBookingService.getDailyMonthlyPrivateCoachingAttach(
					"DAILY",
					selectedDate,
					fileType,
					sortBy,
					isAscending,
					facilityType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	@RequestMapping(value = "/monthlyPrivateCoachingRevenue", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getMonthlyPrivateCoachingRevenue(
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "year", required = true) String year, @RequestParam(value = "month", required = true) String month,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType) {

		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		String selectedDate = CommUtil.formatTheGivenYearAndMonthWithString(year, month);
		ResponseResult responseResult = customerOrderTransService.getDailyMonthlyPrivateCoachingRevenue("MONTHLY", page, selectedDate, facilityType);
		return responseResult;
	}

	@RequestMapping(value = "/monthlyPrivateCoachingRevenueAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getMonthlyPrivateCoachingRevenueAttach(@RequestParam(value = "year", required = true) String year,
			@RequestParam(value = "month", required = true) String month, @RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			handleExpirySession(request, response);
			String selectedDate = CommUtil.formatTheGivenYearAndMonthWithString(year, month);
			String facilityTypeConvert = "GOLF".equals(facilityType) || "TENNIS".equals(facilityType) ? ("TENNIS".equals(facilityType) ? "Tennis"
					: "Golf") : "All";
			chooseFileType(
					request,
					response,
					"Monthly Private Coaching Revenue (" + facilityTypeConvert + ") - "
							+ new SimpleDateFormat("yyyy-MMM").format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate)),
					fileType);
			return customerOrderTransService.getDailyMonthlyPrivateCoachingRevenueAttach(
					"MONTHLY",
					selectedDate,
					fileType,
					sortBy,
					isAscending,
					facilityType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	private void handleExpirySession(HttpServletRequest request, HttpServletResponse response) {
		String url = request.getHeader("Referer");
		String base64Token = request.getHeader("token");
		GTAError errorCode = null;
		boolean valid = false;
		if (null == base64Token || StringUtils.isEmpty(base64Token)) {
			base64Token = request.getParameter("token");
		}
		if (StringUtils.isEmpty(base64Token)) {
			try {
				response.sendRedirect(url);
			} catch (IOException e1) {
				logger.error(e1.getMessage(), e1);
			}
		} else {

			byte[] t = Base64.decodeBase64(base64Token);
			String authToken = new String(t);
			valid = TokenUtils.validateUUIDToken(authToken);
			errorCode = userMasterService.updateSessionToken(authToken);
			if (!valid || !GTAError.Success.SUCCESS.equals(errorCode)) {
				try {
					response.sendRedirect(url);
				} catch (IOException e1) {
					logger.error(e1.getMessage(), e1);
				}
			}
		}
	}

	private void chooseFileType(HttpServletRequest request, HttpServletResponse response, String fileName, String fileType) {
		if ("pdf".equalsIgnoreCase(fileType)) {
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
			response.setContentType("application/pdf");
		} else {
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");
			response.setContentType("text/csv");
		}
	}
}
