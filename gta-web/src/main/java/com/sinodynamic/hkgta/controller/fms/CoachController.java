package com.sinodynamic.hkgta.controller.fms;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.fms.AvailableCoachDto;
import com.sinodynamic.hkgta.dto.fms.BayTypeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityAvailabilityDto;
import com.sinodynamic.hkgta.dto.fms.FacilityReservationDto;
import com.sinodynamic.hkgta.dto.fms.MemberFacilityBookingDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.fms.PrivateCoachBookListDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.fms.MemberFacilityTypeBooking;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.backoffice.coachmanage.CoachManagementService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.StaffProfileService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.service.fms.FacilityMasterService;
import com.sinodynamic.hkgta.service.fms.FacilityTimeslotService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.fms.TrainerAppService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CollectionUtil;
import com.sinodynamic.hkgta.util.CollectionUtil.CallBack;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MessageTemplateParams;
import com.sinodynamic.hkgta.util.constant.UserPreferenceSettingParam;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/coach")
@Scope("prototype")
public class CoachController extends ControllerBase {
	private Logger logger = Logger.getLogger(CoachController.class);
	
	private Logger zoomtechLog = Logger.getLogger(LoggerType.ZOOMTECH.getName()); 
	
	private Logger ecrLog = Logger.getLogger(LoggerType.ECRLOG.getName());
	
	@Autowired
	FacilityTimeslotService facilityTimeslotService;
	@Autowired
	FacilityMasterService facilityMasterService;
	@Autowired
	CoachManagementService coachManagementService;
	@Autowired
	MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	@Autowired
	private ShortMessageService smsService;
	@Autowired
	private MessageTemplateService messageTemplateService;
	@Autowired
	private CustomerProfileService customerProfileService;
	@Autowired
	private TrainerAppService trainerAppService;
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	@Autowired
	private StaffProfileService staffProfileService;

	@RequestMapping(value = "/member/reservation/advancedSearch/{facilityType}", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult advancedSearch(@PathVariable String facilityType) {
		AdvanceQueryConditionDto condition0 = new AdvanceQueryConditionDto("Reservation ID", "resvId", "java.lang.Long",
				"", 1);
		AdvanceQueryConditionDto condition1 = new AdvanceQueryConditionDto("Patron ID", "memberID", "java.lang.String",
				"", 2);
		AdvanceQueryConditionDto condition2 = new AdvanceQueryConditionDto("Patron Name", "memberName",
				"java.lang.String", "", 3);
		AdvanceQueryConditionDto condition3 = new AdvanceQueryConditionDto("Date", "showDate3", "java.util.Date", "",
				4);
		String facilityTitle = null;
		if (StringUtils.equalsIgnoreCase("tennis", facilityType)) {
			facilityTitle = "Court Type";
		} else {
			facilityTitle = "Bay Type";
		}

		List<BayTypeDto> dtoList = (List<BayTypeDto>) getBayTypeByFacilityType(facilityType).getDto();

		Collection<SysCode> sysCodes = CollectionUtil.map(dtoList, new CallBack<BayTypeDto, SysCode>() {

			@Override
			public SysCode execute(BayTypeDto t, int index) {
				SysCode sysCode = new SysCode();
				sysCode.setCodeValue(t.getValue());
				sysCode.setCodeDisplay(t.getValue());
				return sysCode;
			}

		});

		AdvanceQueryConditionDto condition5 = new AdvanceQueryConditionDto(facilityTitle, "bayType", "java.lang.String",
				new ArrayList<SysCode>(sysCodes), 5);
		AdvanceQueryConditionDto condition6 = new AdvanceQueryConditionDto("Coach", "coach", "java.lang.String", "", 6);
		final List<SysCode> status = new ArrayList<>();

		SysCode s1 = new SysCode();
		s1.setCategory("status");
		s1.setCodeDisplay("Ready to Check-in");
		s1.setCodeValue("RSV");
		status.add(s1);

		SysCode s2 = new SysCode();
		s2.setCategory("status");
		s2.setCodeDisplay("Checked-in");
		s2.setCodeValue("ATN");
		status.add(s2);

		SysCode s3 = new SysCode();
		s3.setCategory("status");
		s3.setCodeDisplay("Overdue");
		s3.setCodeValue("OD");
		status.add(s3);
		AdvanceQueryConditionDto condition7 = new AdvanceQueryConditionDto("Status", "status", "java.lang.String",
				status, 7);
		final List<SysCode> attendanceStatus = new ArrayList<>();

		SysCode s4 = new SysCode();
		s4.setCategory("attendanceStatus");
		s4.setCodeDisplay("Pending");
		s4.setCodeValue("Pending");
		attendanceStatus.add(s4);

		SysCode s5 = new SysCode();
		s5.setCategory("attendanceStatus");
		s5.setCodeDisplay("Attended");
		s5.setCodeValue("Attended");
		attendanceStatus.add(s5);
		AdvanceQueryConditionDto condition8 = new AdvanceQueryConditionDto("Attendance", "attendanceStatus",
				"java.lang.String", attendanceStatus, 8);
		responseResult.initResult(GTAError.Success.SUCCESS, Arrays.asList(condition0, condition1, condition2,
				condition3, condition5, condition6, condition7, condition8));
		return responseResult;
	}

	@RequestMapping(value = "/member/reservation/attendanceStatus/{resvId}", method = { RequestMethod.PUT })
	@ResponseBody
	public ResponseResult changeAttendanceStatus(@PathVariable Long resvId,
			@RequestParam("attendanceStatus") String attendanceStatus) {
		coachManagementService.changeAttendanceStatus(resvId, attendanceStatus, this.getUser().getUserId());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/member/reservation", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryData(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize, @RequestParam("show") Integer show,
			@RequestParam(value = "status", defaultValue = "PND") String status,
			@RequestParam(value = "sortBy", defaultValue = "") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "filters", defaultValue = "") String filters,
			@RequestParam(value = "facilityType", defaultValue = "TENNIS") String facilityType,
			@RequestParam(value = "customerId", defaultValue = "", required = false) @EncryptFieldInfo Long customerId,
			HttpServletRequest request, HttpServletResponse response) {
		try {

			String newCustomerId=null;
			if(null!=customerId){
				newCustomerId=customerId.toString();
			}
			String joinHQL = memberFacilityTypeBookingService.getPrivateCoachBookList(status, show, facilityType,
					newCustomerId);
			if (StringUtils.isEmpty(sortBy)) {
				sortBy = "createDate";
				isAscending = "false";
			} else {
				sortBy = sortBy + ",createDate";
				isAscending = isAscending + ",false";
			}

			// advance search
			if (!isFilterEmpty(filters)) {
				joinHQL += " and ";
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinHQL, page,
						PrivateCoachBookListDto.class);
			}
			// no advance condition search
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			return advanceQueryService.getInitialQueryResultBySQL(queryDto, joinHQL, page,
					PrivateCoachBookListDto.class);
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCoachInfoByCoachUserId(
			@RequestParam(value = "coachUserId", required = true) String coachUserId) {
		responseResult.initResult(GTAError.Success.SUCCESS, coachManagementService.loadCoachProfile(coachUserId));
		return responseResult;
	}

	@RequestMapping(value = "/available", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAvailableCoachByDateAndTimeslot(
			@RequestParam(value = "date", required = false) String date,
			@RequestParam(value = "timeslot", required = false) String timeslot,
			@RequestParam(value = "staffType", required = true) String staffType,
			@RequestParam(value = "createOffer", required = false, defaultValue = "false") boolean createOffer,
			@RequestParam(value = "originResvId", required = false) Long originResvId,
			@RequestParam(value = "startTime", required = false) String startTime,
			@RequestParam(value = "endTime", required = false) String endTime,
			@RequestParam(value = "type", required = false) String type) {

		List<AvailableCoachDto> dtoList = null;
		//new add golf/tennis training calendar coach list
		// get coach(if month search date is not null and startTime/endTime is null,else weekly to search ,the starTime/endTime is not null and date is null )
		if (!StringUtils.isEmpty(type)) {
			dtoList=coachManagementService.getCoachByStaffType(date, startTime, endTime, staffType);
		} else {
			//keep old no change
			Date dateObject = null;
			try {
				if (date != null) {
					String[] patterns = new String[] { "yyyy-MM-dd" };
					dateObject = DateUtils.parseDate(date, patterns);
				}
			} catch (ParseException e) {
				logger.error(e.getMessage(),e);
				throw new GTACommonException(GTAError.CoachMgrError.FAIL_PARSE_DATE);
			}
			dtoList = coachManagementService.loadAvailableCoach(dateObject, timeslot, staffType, createOffer, originResvId);
		}
		responseResult.initResult(GTAError.Success.SUCCESS, dtoList);
		return responseResult;
	}
	@RequestMapping(value = "/available/timeslot/coach", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAvailableTimelotByCoachIdAndDate(
			@RequestParam(value = "staffUserId", required = true) String staffUserId,
			@RequestParam(value = "date", required = true) String date,
			@RequestParam(value = "facilityType", required = true) String facilityType,
			@RequestParam(value = "bayType", required = true) String bayType) throws Exception {

		Date dateObject = null;
		try {
			String[] patterns = new String[] { "yyyy-MM-dd" };
			dateObject = DateUtils.parseDate(date, patterns);

		} catch (ParseException e) {
			logger.error(e.getMessage(),e);
			throw new GTACommonException(GTAError.CoachMgrError.FAIL_PARSE_DATE);
		}
		List<FacilityAvailabilityDto> dtoList = facilityMasterService.getFacilityAvailability(staffUserId,
				StringUtils.upperCase(facilityType), dateObject, bayType);
		responseResult.initResult(GTAError.Success.SUCCESS, dtoList);
		return responseResult;
	}

	@RequestMapping(value = "/available/date/coach", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAvailableDateByCoachId(
			@RequestParam(value = "staffUserId", required = true) String staffUserId,
			@RequestParam(value = "month", required = true) String date,
			@RequestParam(value = "facilityType", required = false) String facilityType) {
		DateTime dateStart = DateTime.parse(date + "-01T00:00:00Z");
		DateTime dateEnd = dateStart.dayOfMonth().withMaximumValue().millisOfDay().withMaximumValue();
		responseResult.initResult(GTAError.Success.SUCCESS, coachManagementService.loadInavailableRoster(staffUserId,
				dateStart.toLocalDateTime().toDate(), dateEnd.toLocalDateTime().toDate()));
		return responseResult;
	}

	@RequestMapping(value = "/available/timeslot/facilitytype", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAvailableTimelotByDateAndFacilityType(
			@RequestParam(value = "date", required = true) String date,
			@RequestParam(value = "facilityType", required = true) String facilityType,
			@RequestParam(value = "bayType", required = true) String bayType) throws Exception {
		Date dateObject = null;
		try {
			String[] patterns = new String[] { "yyyy-MM-dd" };
			dateObject = DateUtils.parseDate(date, patterns);

		} catch (ParseException e) {
			logger.error(e.getMessage(),e);
			throw new GTACommonException(GTAError.CoachMgrError.FAIL_PARSE_DATE);
		}
		List<FacilityAvailabilityDto> dtoList = facilityMasterService
				.getFacilityAvailability(StringUtils.upperCase(facilityType), dateObject, bayType);
		facilityMasterService.handerFacilityAvailabilityCoach(dateObject, facilityType, dtoList);
		responseResult.initResult(GTAError.Success.SUCCESS, dtoList);
		return responseResult;
	}

	@RequestMapping(value = "/bay/type", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBayTypeByFacilityType(
			@RequestParam(value = "facilityType", required = true) String facilityType) {
		List<BayTypeDto> dtoList = facilityMasterService.getFacilityBayType(StringUtils.upperCase(facilityType));
		dtoList.remove(new BayTypeDto(Constant.BALLFEEDSWITCH));
		dtoList.remove(new BayTypeDto(Constant.ZONE_COACHING));
		dtoList.remove(new BayTypeDto(Constant.ZONE_PRACTICE));
		dtoList.remove(new BayTypeDto(Constant.GOLFBAYTYPE_CAR));

		responseResult.initResult(GTAError.Success.SUCCESS, dtoList);
		return responseResult;
	}

	@RequestMapping(value = "/payment/method", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBayTypeByFacilityType() {
		//List<BayTypeDto> ls = new ArrayList<BayTypeDto>();
		/*
		 * 
		 * select s.code_value,s.code_display from sys_code s where
		 * s.category='settlemethod' ORDER BY s.display_order;
		 * 
		 */
		return responseResult;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reservation/checkright", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkFacilityRight(@RequestParam(value = "customerId", required = true) @EncryptFieldInfo Long customerId,
			@RequestParam(value = "facilityType", required = true) String facilityType) {
		boolean facilityRight = this.memberFacilityTypeBookingService.getRightByCustomerIdAndFacilityType(customerId,
				facilityType);
		
		
		if (false == facilityRight) {
			if ("TENNIS".equals(facilityType)) {
				throw new GTACommonException(GTAError.FacilityError.No_TennisPrivateCoach_Right);
			} else if ("GOLF".equals(facilityType)) {
				throw new GTACommonException(GTAError.FacilityError.No_GolfPrivateCoach_Right);
			}

		}
		this.responseResult.initResult(GTAError.Success.SUCCESS);
		return this.responseResult;
	}

	@RequestMapping(value = "/reservation/settle", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult doPay(HttpServletRequest request,@RequestBody MemberFacilityBookingDto dto) throws Exception {
		// List<BayTypeDto> ls = new ArrayList<BayTypeDto>();
		/*
		 * 业务逻辑： 开启Transaction 1检测用户所选择的时间段和coach 是否可用 ，
		 * 2保存主表member_facility_type_booking 3进行支付操作，当支付成功，则更新
		 * member_facility_type_booking 表的order_det_id，否则回滚以上操作
		 * 
		 */
		dto.setTerminalType(request.getHeader("terminalType"));
		MemberFacilityBookingDto returnDto = null;
		dto.setUpdateBy(this.getUser().getUserId());
		dto.setCreatedBy(this.getUser().getUserId());
		if (null == dto.getLocation()) {
			dto.setLocation(getLoginLocation());
		}
		if (null == dto.getResvId()) {
			// check member status,only activity status can do reservation
			this.memberFacilityTypeBookingService.checkMemberStatus(dto.getCustomerId());
			// check member facility right
			//christ use new check facilitRight 
			//this.checkFacilityRight(dto.getCustomerId(), dto.getFacilityType());
			// check reservation time availability
			this.getAvailableTimelotByDateAndFacilityType(dto.getBookingDate(), dto.getFacilityType(),
					dto.getAttributeType());
			// check reservation coach availability
			String timeslot = dto.getBeginTime() + "," + dto.getEndTime();
			String staffType = null;
			if ("GOLF".equals(dto.getFacilityType())) {
				staffType = "G";
			} else if ("TENNIS".equals(dto.getFacilityType())) {
				staffType = "R";
			}
			if (staffType != null) {
				ResponseResult tmp = this.getAvailableCoachByDateAndTimeslot(dto.getBookingDate(), timeslot, staffType,
						false, null,null,null,null);
				if (null != tmp.getDto()) {
					List<AvailableCoachDto> coachList = (List<AvailableCoachDto>) tmp.getDto();
					AvailableCoachDto selectCoach = new AvailableCoachDto();
					selectCoach.setCoachId(dto.getCoachId());
					if (!coachList.contains(selectCoach)) {
						throw new GTACommonException(GTAError.PrivateCoachingError.COACH_NOT_AVAILABLE);
					}
				} else {
					throw new GTACommonException(GTAError.PrivateCoachingError.COACH_NOT_AVAILABLE);
				}
			}
			returnDto = memberFacilityTypeBookingService.saveAndPayBooking(dto);
		} else {
			Map<String, Object> map = new HashMap<>();
			MemberFacilityTypeBooking booking = this.memberFacilityTypeBookingService
					.getMemberFacilityTypeBooking(dto.getResvId());
			ResponseResult result = this.coachManagementService.calcBookingPrice(dto);
			MemberFacilityBookingDto priceDto = (MemberFacilityBookingDto) result.getData();
			priceDto.setCustomerId(dto.getCustomerId());
			priceDto.setPaymentMethod(dto.getPaymentMethod());
			if (null == dto.getLocation()) {
				priceDto.setLocation(this.getLoginLocation());
			} else {
				priceDto.setLocation(dto.getLocation());
			}
			map.put("bookingDto", priceDto);
			map.put("booking", booking);
			returnDto = memberFacilityTypeBookingService.doPay(map);
		}
		if (null != returnDto) {
			MemberFacilityTypeBooking typeBooking = this.memberFacilityTypeBookingService
					.getMemberFacilityTypeBooking(returnDto.getResvId());
			if (Constant.Status.RSV.toString().equalsIgnoreCase(typeBooking.getStatus())) {
				this.sendEmail(returnDto.getResvId());
				boolean sendSMS=super.sendSMS((null!=typeBooking)?typeBooking.getCustomerId():null);
				if(sendSMS){
					Thread confirmSMSThread = new Thread(new SendConfirmSMSThread(returnDto.getResvId()));
					confirmSMSThread.start();
				}
				Timestamp begin = typeBooking.getBeginDatetimeBook();
				long secend = (begin.getTime() - System.currentTimeMillis()) / 1000;
				if (-3600 < secend && secend < 0) {
					Thread autoRemindThread = new Thread(new AutoRemindThread(typeBooking));
					autoRemindThread.start();
				}
			}
		}
		this.responseResult.initResult(GTAError.Success.SUCCESS, returnDto);
		return this.responseResult;
	}

	@RequestMapping(value = "/payment/posCallback", method = RequestMethod.POST)
	@ResponseBody
	public String coachPOSCallback(HttpServletRequest request,@RequestBody PosResponse posResponse) {
		logger.info("Got callback !!");
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create();
		logger.debug(gson.toJson(posResponse));
		ecrLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), gson.toJson(posResponse), RequestMethod.POST.name(), null));		
		if(null==posResponse){
			responseResult.initResult(GTAError.ERCCallBack.REPONSE_IS_NULL);
			ecrLog.info(Log4jFormatUtil.logInfoMessage(null,null,null, responseResult.getErrorMessageEN()));
			return responseResult.getErrorMessageEN();
		}
		if (null!=posResponse&&null!=posResponse.getReferenceNo()) {
			Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			if (customerOrderTrans != null) {
				if (posResponse.getResponseCode().equalsIgnoreCase("00")) {
					this.responseResult = customerOrderTransService.handlePosPayPrivateCoach(posResponse);
					if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(this.responseResult.getReturnCode())) {
						MemberFacilityTypeBooking booking = memberFacilityTypeBookingService
								.getMemberFacilityTypeBookingByOrderNo(
										customerOrderTrans.getCustomerOrderHd().getOrderNo());
						booking.setStatus(Constant.Status.RSV.toString());
						memberFacilityTypeBookingService.update(booking);
						this.sendEmail(booking.getResvId());
						Thread confirmSMSThread = new Thread(new SendConfirmSMSThread(booking.getResvId()));
						confirmSMSThread.start();
						ecrLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, " update MemberFacilityTypeBooking :resvId:"+booking.getResvId()+",status:"+booking.getStatus()));
					}
				}else{
					if (customerOrderTrans.getStatus().equalsIgnoreCase("PND")) {
						customerOrderTrans.setStatus("FAIL");
						customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
						customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
						customerOrderTrans.setInternalRemark(posResponse.getResponseText());
						customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
						ecrLog.info(Log4jFormatUtil.logInfoMessage(null,null,null, " posResponse:"+posResponse.getResponseCode())+
								",transactionNo:"+customerOrderTrans.getTransactionNo()+",status:"+customerOrderTrans.getStatus());
					}
					
				}

			}else{
				responseResult.initResult(GTAError.MemberShipError.TRANSACTION_NOT_EXISTS);
				ecrLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, responseResult.getErrorMessageEN(), null));
			}
		}
		return "OK";
	}

	@RequestMapping(value = "/available/facility", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAvailableFacilityByType(@RequestParam(value = "bayType", required = true) String bayType,
			@RequestParam(value = "bookDate", required = true) String bookDate,
			@RequestParam(value = "bookTimes", required = true) String bookTimes,
			@RequestParam(value = "facilityType", required = true) String facilityType) {
		//List<AvailableFacilityDto> ls = new ArrayList<AvailableFacilityDto>();

		/*
		 * 
		 * 当facilityType 为 Golf 时 SELECT
		 * t.facility_no,t.facility_name,t.venue_floor,(case when t2.state is
		 * null then 'ACT' else t2.state end) as state from ( SELECT
		 * m.facility_no,m.facility_name,m.venue_floor from facility_master
		 * m,facility_addition_attribute a where a.facility_no=m.facility_no and
		 * a.input_value='1' and m.facility_type='Golf' and m.status='ACT' )t
		 * LEFT JOIN ( SELECT f.facility_no,s.status as state from
		 * facility_timeslot s,facility_master f where
		 * s.facility_no=f.facility_no and f.facility_type='Golf' and
		 * date_format(s.begin_datetime,'%Y-%m-%d')='2015-06-06' and
		 * date_format(s.begin_datetime,'%H') in('17') )t2
		 * on(t.facility_no=t2.facility_no);
		 * 
		 * 当facilityType 为 Tennis 时 SELECT
		 * t.facility_no,t.facility_name,t.venue_floor,(case when t2.state is
		 * null then 'ACT' else t2.state end) as state from ( SELECT
		 * m.facility_no,m.facility_name,v.venue_name as venue_floor from
		 * facility_master m LEFT JOIN venue_master v
		 * on(v.venue_code=m.venue_code) where m.facility_type='Tennis' and
		 * m.status='ACT' )t LEFT JOIN ( SELECT f.facility_no,s.status as state
		 * from facility_timeslot s,facility_master f where
		 * s.facility_no=f.facility_no and f.facility_type='Tennis' and
		 * date_format(s.begin_datetime,'%Y-%m-%d')='2015-06-06' and
		 * date_format(s.begin_datetime,'%H') in('17') )t2
		 * on(t.facility_no=t2.facility_no);
		 * 
		 */

		return responseResult;
	}

	@RequestMapping(value = "/checkin/facility", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult doCheckInFacility(@RequestParam(value = "resvId", required = true) String resvId,
			@RequestParam(value = "facilityNo", required = true) String facilityNo,
			@RequestParam(value = "bookDate", required = true) String bookDate,
			@RequestParam(value = "bookTimes", required = true) String bookTimes) {
		// 1.检测facility 是否可用
		// SELECT count(1) from facility_timeslot s where s.facility_no='18' and
		// date_format(s.begin_datetime,'%Y-%m-%d')='2015-06-06' and
		// date_format(s.begin_datetime,'%H') in('17')
		// 2.根据 resvId 获取到facility_timeslot list ，然后更新 每个 facility_timeslot 的
		// facilityNo

		return responseResult;
	}

	/**
	 * 
	 * @param type
	 *            only can be cousre,facility,coach
	 * @param resvId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reservation/precancel", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult preCancel(@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "resvId", required = true) Long resvId) throws Exception {
		if ("course".equals(type)) {
			responseResult = memberFacilityTypeBookingService.preCancel(resvId, type);
		} else if ("coach".equals(type)) {
			responseResult = memberFacilityTypeBookingService.preCancel(resvId, type);
		} else if ("facility".equals(type)) {
			responseResult = memberFacilityTypeBookingService.preCancel(resvId, type);
		} else if ("daypass".equals(type)) {
			responseResult = memberFacilityTypeBookingService.preCancel(resvId, type);
		} else if ("guestroom".equals(type)) {
			responseResult = memberFacilityTypeBookingService.preCancel(resvId, type);
		}

		return responseResult;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reservation/cancel", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult doCancel(HttpServletRequest request,@RequestParam(value = "refundFlag", required = true) boolean refundFlag,
			@RequestParam(value = "resvId", required = true) Long resvId,
			@RequestParam(value = "requesterType", required = true) String requesterType,
			@RequestParam(value = "remark", required = false) String remark) throws Exception {
		// Actions:
		// 1、based on defined company rule 检测是否可以执行Cancel操作
		// 2、delete member_reserved_facility and facility_timeslot where
		// resv_id=''
		// 3、delete member_facility_book_addition_attr where resv_id=''
		// 4、delete staff_timeslot where staff_timeslot_id=''
		// 5、update member_facility_type_booking set status='CAN'
		// ,staff_timeslot_id=null,
		// 6、执行退款操作
		String requestContent="param:refundFlag:"+refundFlag+",resvId:"+resvId+",requesterType:"+requesterType+",remark:"+remark;
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), requestContent,  RequestMethod.PUT.name(),null));
		
		responseResult = memberFacilityTypeBookingService.doCancel(resvId, this.getUser().getUserId(), remark,
				requesterType, refundFlag);
		
		if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(this.responseResult.getReturnCode())) {
			MemberFacilityTypeBooking booking = memberFacilityTypeBookingService.getCancelBooking(resvId);//.getMemberFacilityTypeBooking(resvId);
			if(null!=booking){
				boolean refundPeriodFlag = memberFacilityTypeBookingService.checkRefundDate(booking.getBeginDatetimeBook(),
						booking.getResvFacilityType(), "coach");
				// check cancel reservation ,if cancel in refund period ,only send email 
				if(!refundPeriodFlag){
					if(super.sendSMS(booking.getCustomerId())){
						Thread sendCancelSMSThread = new Thread(new SendCancelSMSThread(resvId));
						sendCancelSMSThread.start();
					}
					Thread pushCancelMsgThread = new Thread(new PushCancelMsgThread(resvId));
					pushCancelMsgThread.start();	
				}
			}else{
				responseResult.initResult(GTAError.FacilityError.BOOKING_ISNULL);
			}
		}
		
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), requestContent,  RequestMethod.PUT.name(),JSONObject.fromObject(responseResult).toString()));
		
		return responseResult;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reservation/sendEmail", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult sendEmail(@RequestParam(value = "resvId", required = true) Long resvId) {
		Thread emailThread = new Thread(new EmailThread(resvId, getUser().getUserId()));
		emailThread.start();
		responseResult.initResult(GTAError.FacilityError.SUCCESS);
		return responseResult;
	}

	private class EmailThread implements Runnable {

		private final long resvId;
		private final String userId;

		public EmailThread(long resvId, String userId) {
			this.resvId = resvId;
			this.userId = userId;
		}

		@Override
		public void run() {
			try {
				memberFacilityTypeBookingService.sendPrivateCoachingReservationEmail(resvId, userId);
			} catch (Exception e) {
				logger.error("error sent email", e);
				throw new GTACommonException(GTAError.CommonError.SEND_EMAIL_FAILED);
			}
		}
	}

	private class SendConfirmSMSThread implements Runnable {
		private final long resvId;

		public SendConfirmSMSThread(long resvId) {
			this.resvId = resvId;
		}

		@Override
		public void run() {

			MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingService
					.getMemberFacilityTypeBooking(resvId);
			String coachName = "";
			if(memberFacilityTypeBooking != null){
				StaffProfile sp = staffProfileService.getStaffProfileById(memberFacilityTypeBooking.getExpCoachUserId());
				if(sp != null){
					coachName = sp.getGivenName() + " " + sp.getSurname();
				}
			CustomerProfile customerProfile = customerProfileService.getById(memberFacilityTypeBooking.getCustomerId());
			if (customerProfile != null) {
				String mobilePhone = customerProfile.getPhoneMobile();
				List<String> phonenumbers = new ArrayList<>();
				phonenumbers.add(mobilePhone);
				Date begin = new Date(memberFacilityTypeBooking.getBeginDatetimeBook().getTime());
				Date end = new Date(memberFacilityTypeBooking.getEndDatetimeBook().getTime());
				MessageTemplate messageTemplate;
				try {
					if (memberFacilityTypeBooking.getResvFacilityType().equalsIgnoreCase("GOLF")) {
						messageTemplate = messageTemplateService.getTemplateByFuncId(Constant.TEMPLATE_ID_GOLF_COACH_CONFIRM);
					} else {
						messageTemplate = messageTemplateService.getTemplateByFuncId(Constant.TEMPLATE_ID_TENNIS_COACH_CONFIRM);
					}
					
					FacilityReservationDto reservationDto= memberFacilityTypeBookingService.getFacilityReservationCourtTypeByResvId(memberFacilityTypeBooking.getResvId());
					String facilityType = memberFacilityTypeBooking.getResvFacilityType();
					if(null!=reservationDto){
						facilityType=reservationDto.getBayType();//left hand, right hand
					}
					/*
					 * Your booking has been confirmed. {bookingDate}
					 * {startTime}-{endTime} number of facility:
					 * {facilityTypeQty} facility type: {facilityType} HKGTA
					 */
										
					String startTime = Integer.toString(DateCalcUtil.getHourOfDay(begin)) + ":00";
					String endTime = Integer.toString(DateCalcUtil.getHourOfDay(end) + 1) + ":00";
					String content = messageTemplate.getContent();
					String message = content.replace("{bookingDate}", DateCalcUtil.formatDate(begin))
							.replace("{startTime}", startTime).replace("{endTime}", endTime)
							.replace("{facilityType}", facilityType).replace(
									"{facilityTypeQty}", Long.toString(memberFacilityTypeBooking.getFacilityTypeQty())).replace("{coachFullName}", coachName);
					logger.error("CoachController  SendConfirmSMSThread run start ...resvId:" + resvId);
					smsService.sendSMS(phonenumbers, message,
							DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
					logger.error("CoachController  SendConfirmSMSThread run end ...resvId:" + resvId);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("send Confirm SMS error." + e.getMessage());
				}
			}
			}
		}

	}

	private class PushCancelMsgThread implements Runnable {
		private final long resvId;

		public PushCancelMsgThread(long resvId) {
			this.resvId = resvId;
		}

		@Override
		public void run() {
			MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingService
					.getCancelBooking(resvId);
			CustomerProfile customerProfile = customerProfileService.getById(memberFacilityTypeBooking.getCustomerId());
			if (customerProfile != null) {
				String mobilePhone = customerProfile.getPhoneMobile();
				List<String> phonenumbers = new ArrayList<String>();
				phonenumbers.add(mobilePhone);
				MessageTemplate messageTemplate;
				try {
					// if
					// (memberFacilityTypeBooking.getResvFacilityType().equalsIgnoreCase("GOLF")){
					// messageTemplate =
					// messageTemplateService.getTemplateByFuncId(
					// Constant.TEMPLATE_ID_GOLF_COACH_CANCEL);
					// }else{
					// messageTemplate =
					// messageTemplateService.getTemplateByFuncId(
					// Constant.TEMPLATE_ID_TENNIS_COACH_CANCEL);
					// }
					// /*
					// Your booking has been canceled.{bookingDate}
					// {startTime}-{endTime} number of facility:
					// {facilityTypeQty} facility type: {facilityType} HKGTA
					// */
					// String message =
					// assemblyContent(memberFacilityTypeBooking,messageTemplate);
					// logger.error("CoachController PushCancelMsgThread push
					// msg to member run start ...resvId:"+resvId);
					// devicePushService.pushMessage(new
					// String[]{memberFacilityTypeBooking.getCustomerId()+""},message,
					// "member");
					// logger.error("CoachController PushCancelMsgThread push
					// msg to member run end ...resvId:"+resvId);
					// push msg to member end

					// push msg to coach begin
					messageTemplate = messageTemplateService.getTemplateByFuncId(Constant.TEMPLATE_ID_APP_COACH_CANCEL);
					String content = messageTemplate.getContent();
					String username = customerProfile.getSalutation() + " " + customerProfile.getGivenName() + " "
							+ customerProfile.getSurname();
					content = content.replaceAll(MessageTemplateParams.PatronName.getParam(), username);
					messageTemplate.setContent(content);
					String message = assemblyContent(memberFacilityTypeBooking, messageTemplate);
					logger.error(
							"CoachController  PushCancelMsgThread push msg to coach run start ...resvId:" + resvId);
					devicePushService.pushMessage(new String[] { memberFacilityTypeBooking.getExpCoachUserId() },
							message, "trainer");
					logger.error("CoachController  PushCancelMsgThread push msg to coach run end ...resvId:" + resvId);
					// push msg to coach end
				} catch (Exception e) {
					logger.error("privsate coach PushCancelMsgThread push msg error." + e.getMessage(),e);
				}
			}

		}

	}

	private class SendCancelSMSThread implements Runnable {
		private final long resvId;

		public SendCancelSMSThread(long resvId) {
			this.resvId = resvId;
		}

		@Override
		public void run() {
			MemberFacilityTypeBooking memberFacilityTypeBooking = memberFacilityTypeBookingService
					.getCancelBooking(resvId);
			CustomerProfile customerProfile = customerProfileService.getById(memberFacilityTypeBooking.getCustomerId());
			if (customerProfile != null) {
				String mobilePhone = customerProfile.getPhoneMobile();
				List<String> phonenumbers = new ArrayList<String>();
				phonenumbers.add(mobilePhone);
				MessageTemplate messageTemplate;
				try {
					if (memberFacilityTypeBooking.getResvFacilityType().equalsIgnoreCase("GOLF")) {
						messageTemplate = messageTemplateService.getTemplateByFuncId(Constant.TEMPLATE_ID_GOLF_COACH_CANCEL);
					} else {
						messageTemplate = messageTemplateService.getTemplateByFuncId(Constant.TEMPLATE_ID_TENNIS_COACH_CANCEL);
					}
					/*
					 * Your booking has been canceled. {bookingDate}
					 * {startTime}-{endTime} number of facility:
					 * {facilityTypeQty} facility type: {facilityType} HKGTA
					 */
					String message = assemblyContent(memberFacilityTypeBooking, messageTemplate);
					logger.error("CoachController  SendCancelSMSThread run start ...resvId:" + resvId);
					smsService.sendSMS(phonenumbers, message,
							DateCalcUtil.getNearDateTime(new Date(), 1, Calendar.MINUTE));
					logger.error("CoachController  SendCancelSMSThread run end ...resvId:" + resvId);
				} catch (Exception e) {
					logger.error("send Cancel SMS error." + e.getMessage(),e);
				}
			}

		}

	}

	private String assemblyContent(MemberFacilityTypeBooking memberFacilityTypeBooking, MessageTemplate messageTemplate)
			throws Exception {
		String message=null;
		FacilityReservationDto reservationDto= memberFacilityTypeBookingService.getFacilityReservationCourtTypeByResvId(memberFacilityTypeBooking.getResvId());
		String facilityType = memberFacilityTypeBooking.getResvFacilityType();
		if(null!=reservationDto){
			facilityType=reservationDto.getBayType();//left hand, right hand
		}
		String coachName = "";
		if(null!=memberFacilityTypeBooking){
			StaffProfile sp = staffProfileService.getStaffProfileById(memberFacilityTypeBooking.getExpCoachUserId());
			if(sp != null){
				coachName = sp.getGivenName() + " " + sp.getSurname();
			}
		
		Date begin = new Date(memberFacilityTypeBooking.getBeginDatetimeBook().getTime());
		Date end = new Date(memberFacilityTypeBooking.getEndDatetimeBook().getTime());
		String startTime = Integer.toString(DateCalcUtil.getHourOfDay(begin)) + ":00";
		String endTime = Integer.toString(DateCalcUtil.getHourOfDay(end) + 1) + ":00";
		String content = messageTemplate.getContent();
		message = content
				.replaceAll(MessageTemplateParams.BookingDate.getParam(), DateCalcUtil.formatDate(begin))
				.replaceAll(MessageTemplateParams.StartTime.getParam(), startTime)
				.replaceAll(MessageTemplateParams.EndTime.getParam(), endTime)
				.replaceAll(MessageTemplateParams.FacilityType.getParam(),facilityType)
				.replaceAll(MessageTemplateParams.FacilityTypeQty.getParam(),
						Long.toString(memberFacilityTypeBooking.getFacilityTypeQty()))
				.replaceAll(MessageTemplateParams.CoachName.getParam(),coachName);
		}
		return message;
	}

	@RequestMapping(value = "/member/training", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPrivateCoachBookListByMemberId(
			@RequestParam(value = "customerId", required = true) String memberId,
			@RequestParam(value = "facilityType", required = true) String facilityType,
			@RequestParam(value = "filter") String filter) {
		//List<PrivateCoachBookListDto> ls = new ArrayList<PrivateCoachBookListDto>();
		/*
		 * SELECT r.resv_id as resvId, date_format(s.attend_time,'%Y/%m/%d') as
		 * Date,
		 * p.surname,p.given_name,s.facility_timeslot_id,b.exp_coach_user_id,p.
		 * portrait_photo,s.score,s.trainer_comment,s.assignment from
		 * member_facility_attendance s,facility_timeslot
		 * t,member_reserved_facility r,member_facility_type_booking b LEFT JOIN
		 * staff_profile p on(p.user_id=b.exp_coach_user_id) where
		 * r.facility_timeslot_id=t.facility_timeslot_id and
		 * t.facility_timeslot_id=s.facility_timeslot_id and b.resv_id=r.resv_id
		 * and b.customer_id='1' and b.exp_coach_user_id is not null and
		 * b.resv_facility_type='Tennis' GROUP BY r.resv_id;
		 */

		return responseResult;
	}

	@RequestMapping(value = "/calculatePrice", method = RequestMethod.POST)
	public @ResponseBody ResponseResult calcPrice(@RequestBody MemberFacilityBookingDto booking) throws Exception {
		logger.info("CoachController calculate private coaching booking price invocation start ...");
		try {
			// add validation
			/*采用统一验证权限
			UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(
					booking.getCustomerId(),
					UsageRightCheckService.FacilityCode.valueOf(booking.getFacilityType().toUpperCase()),
					CommUtil.toDate(booking.getBookingDate(), "yyyy-MM-dd"));
			if (!facilityRight.isCanBook()) {
				throw new GTACommonException(GTAError.FacilityError.NO_PERMISSION);
			}
			*/

			if (Constant.reserve_via.APS.toString().equals(booking.getReserveVia())
					|| Constant.reserve_via.WP.toString().equals(booking.getReserveVia())) {
				if ((booking.getEndTime() - booking.getBeginTime()) > 1) {
					this.responseResult.initResult(GTAError.CoachMgrError.Over_Timelots);
					return this.responseResult;
				}
				// Able to reserve the private coaching only one day before the
				// selected timeslot in MemberAPP &Portal
				Date current = new Date();
				long before = (DateCalcUtil
						.parseDateTime(booking.getBookingDate() + " " + booking.getBeginTime() + ":00:00").getTime()
						- current.getTime()) / 1000 / 60 / 60;
				if (before < 24) {
					this.responseResult.initResult(GTAError.CoachMgrError.Over_ReserveDate);
					return this.responseResult;
				}
				int timis = memberFacilityTypeBookingService.getbookingTimes(booking);
				if (timis >= 2) {
					this.responseResult.initResult(GTAError.CoachMgrError.Over_Times);
					return this.responseResult;
				}
			}
			if (booking.isCreateOffer()) {
				MemberFacilityTypeBooking reservation = memberFacilityTypeBookingService
						.getMemberFacilityTypeBooking(booking.getOriginResvId());
				if (!ObjectUtils.equals(reservation.getCustomerId(), booking.getCustomerId())) {
					this.responseResult.initResult(GTAError.CoachMgrError.NOT_SAME_CUSTOMER);
					return this.responseResult;
				}
			}
			return coachManagementService.calcBookingPrice(booking);
		} catch (GTACommonException gtaException) {
			logger.debug("CoachController.calcPrice Exception:", gtaException);
			logger.error("CoachController.calcPrice Exception:"+gtaException.getMessage(),gtaException);
			responseResult.initResult(gtaException.getError(), gtaException.getArgs());
			return responseResult;
		} catch (Exception e) {
			logger.debug("CoachController.calcPrice Exception:", e);
			logger.error("CoachController.calcPrice Exception:"+e.getMessage(),e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}
	
	@RequestMapping(value = "/getCoachPrice", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCoachPrice(@RequestParam(value = "userId", required = true) String userId,
			@RequestParam(value = "date", required = true) String date,
			@RequestParam(value = "beginTime", required = true) String beginTime,
			@RequestParam(value = "endTime", required = true) String endTime)
	{
		//get coach price by date,timeslot,staff 
		return coachManagementService.getCoachPrice(userId, date, beginTime, endTime);
	}

	private class AutoRemindThread implements Runnable {
		private final MemberFacilityTypeBooking typeBooking;

		public AutoRemindThread(MemberFacilityTypeBooking typeBooking) {
			this.typeBooking = typeBooking;
		}

		@Override
		public void run() {
			trainerAppService.pushRemindMsg2Coach(typeBooking);
			trainerAppService.sendRemindSMS2Member(typeBooking);

		}

	}
}
