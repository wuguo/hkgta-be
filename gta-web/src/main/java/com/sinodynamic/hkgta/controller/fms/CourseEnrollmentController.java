package com.sinodynamic.hkgta.controller.fms;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.fms.CourseReservationDto;
import com.sinodynamic.hkgta.dto.fms.CourseTransactionDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.fms.CourseMaster;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.service.fms.CourseEnrollmentService;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CourseStatus;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.EnrollCourseStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.constant.UserPreferenceSettingParam;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * @since 7/13/2015
 * @author Mianping_Wu
 */
@Controller
@RequestMapping(value = "/course-enroll")
public class CourseEnrollmentController extends ControllerBase<CourseEnrollment> {

	private Logger								logger	= Logger.getLogger(CourseEnrollmentController.class);
	
	private Logger ecrLog = Logger.getLogger(LoggerType.ECRLOG.getName());

	@Autowired
	private CourseEnrollmentService				courseEnrollmentService;

	@Autowired
	private CustomerEmailContentService			customerEmailContentService;

	@Autowired
	private MemberFacilityTypeBookingService	memberFacilityTypeBookingService;

	@Autowired
	private CustomerOrderTransService			customerOrderTransService;

	@Autowired
	private ShortMessageService					smsService;
	
	@Autowired
	private CourseService courseService;


	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Jul 14, 2015
	 * @Param @param enrollId
	 * @Param @param status
	 * @return ResponseResult
	 */
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	@SuppressWarnings("unchecked")
	public @ResponseBody ResponseResult modifyCourseEnrollmentStatus(@RequestParam(value = "enrollId") String enrollId,
			@RequestParam(value = "status") String status, @RequestParam(value = "internalRemark", required = false) String internalRemark,
			@RequestParam(value = "cancelRequesterType", required = false) String cancelRequesterType,
			@RequestParam(value = "refundFlag", required = false) boolean refundFlag) {

		logger.info("CourseEnrollmentController.modifyCourseEnrollmentStatus invocation start ..");

		if (StringUtils.isEmpty(enrollId) || StringUtils.isEmpty(status)) {
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_PARAMETER_NULL);
			return responseResult;
		}

		LoginUser user = getUser();
		if (user == null) {
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_NO_LOGIN_USER);
			return responseResult;
		}

		try {

			ResponseResult rs = courseEnrollmentService.modifyCourseEnrollmentStatus(
					enrollId,
					status,
					user.getUserName(),
					internalRemark,
					cancelRequesterType,
					refundFlag);
			if ("0".equals(rs.getReturnCode())) {
				Map<String, Object> smsInfo = (Map<String, Object>) rs.getDto();
				if (smsInfo != null) {
					List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
					String message = (String) smsInfo.get("message");
					CourseEnrollment ce = courseEnrollmentService.getCourseEnrollmentById(enrollId);
					boolean sendSMS=true;
					if(null!=ce){
						sendSMS=super.sendSMS(ce.getCustomerId());
					}
					if(status.equals(CourseStatus.CANCEL.getType())){
						//check cancel course in period  auto refund cash value,else muanu refund
						Timestamp courseStartDate = courseService.getCourseSessionStartDate(ce.getCourseId());
						CourseMaster cm = courseService.getCourseMasterByCourseId(ce.getCourseId());
						boolean inRefundPeriod = memberFacilityTypeBookingService.checkRefundDate(courseStartDate, cm.getCourseType(), "course");
						if(!inRefundPeriod){
							if(sendSMS)smsService.sendSMS(phoneNumbers, message, new Date());
						}
					}else{
						if(sendSMS) smsService.sendSMS(phoneNumbers, message, new Date());
					}
					//add send mail
					if (EnrollCourseStatus.REJ.name().equals(status)||EnrollCourseStatus.ACT.name().equals(status)) {
						courseEnrollmentService.sendEmailCourseEnroll(enrollId,status);
					}
				}
			}
			return rs;

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_UNKNOWN_ERROR);
			return responseResult;
		}

	}
	/**
	 * @Author Mianping_Wu
	 * @Date Jul 14, 2015
	 * @Param @param dto
	 * @return ResponseResult
	 */
	@RequestMapping(value = "/confirm", method = RequestMethod.POST)
	@SuppressWarnings("unchecked")
	public @ResponseBody ResponseResult confirmCourseReservation(@RequestBody CourseReservationDto dto) {

		logger.info("CourseEnrollmentController.confirmCourseReservation invocation start...");

		// check parameter

		try {

			ResponseResult rs = courseEnrollmentService.confirmCourseReservation(dto);
			List<CustomerEmailContent> emailList = (List<CustomerEmailContent>) rs.getDto();
			if (emailList != null && emailList.size() > 0) {
				for (CustomerEmailContent cec : emailList) {

					boolean success = MailSender.sendEmail(cec.getRecipientEmail(), null, null, cec.getSubject(), cec.getContent(), null);
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
				}

				customerEmailContentService.updateEmailStatusInBatch(emailList);
			}

			rs.setData((Object) null);
			return rs;

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_UNKNOWN_ERROR);
			return responseResult;
		}
	}

	/**
	 * @Author Mianping_Wu
	 * @Date Jul 14, 2015
	 * @Param @param dto
	 * @return ResponseResult
	 */
	@RequestMapping(value = "/receipt", method = RequestMethod.POST)
	@SuppressWarnings("unchecked")
	public @ResponseBody ResponseResult sendReceiptForCoursePayment(@RequestBody CourseReservationDto dto) {

		logger.info("CourseEnrollmentController.sendReceiptForCoursePayment invocation start...");

		// check parameter

		try {

			return courseEnrollmentService.sendReceiptForCoursePayment(dto);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_UNKNOWN_ERROR);
			return responseResult;
		}
	}

	@RequestMapping(value = "/payment/posCallback", method = RequestMethod.POST)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public String callback(HttpServletRequest request,@RequestBody PosResponse posResponse) {
		logger.info("Got callback !!");
		try {
			Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create();
			logger.debug(gson.toJson(posResponse));
			ecrLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), gson.toJson(posResponse), RequestMethod.POST.name(), null));		
			if(null==posResponse){
				responseResult.initResult(GTAError.ERCCallBack.REPONSE_IS_NULL);
				ecrLog.info(Log4jFormatUtil.logInfoMessage(null,null,null, responseResult.getErrorMessageEN()));
				return responseResult.getErrorMessageEN();
			}
			if (null!=posResponse&&null!=posResponse.getReferenceNo()) {
				Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
				String cardType = CommUtil.getCardType(posResponse.getCardType());
				if (posResponse.getResponseCode().equalsIgnoreCase("00")) {

					ResponseResult rs = courseEnrollmentService.courseEnrollCallBackService(transactionNo, getUser().getUserId(), cardType);
					if ("0".equals(rs.getReturnCode())) {
						
						Map<String, Object> smsInfo = (Map<String, Object>) rs.getDto();
						if (smsInfo != null) {
							boolean sendSMS=true;
							 List<Member>memberList=memberService.getMembersByCustomerId(smsInfo.get("customerId").toString());
							 if(null!=memberList&&memberList.size()>0){
								 sendSMS=userPreferenceSettingService.checkUserSettingValue(memberList.get(0).getUserId(), UserPreferenceSettingParam.NOTIFICATION.getType());
							 }
							 if(sendSMS){
								 List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");							 
								 String message = (String) smsInfo.get("message");
								 smsService.sendSMS(phoneNumbers, message, new Date());
							 }
						}
					}

				} else {

					CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
					if (customerOrderTrans != null && customerOrderTrans.getStatus().equalsIgnoreCase("PND")) {
						/***
						 * only update customerOrderTrans status is FAIL
						 */
//						CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
//						customerOrderHd.setOrderStatus(Constant.Status.CAN.toString());
//						customerOrderHd.setUpdateBy(getUser().getUserId());
//						customerOrderHd.setUpdateDate(new Timestamp(new Date().getTime()));
//						customerOrderTransService.modifyCustomerOrderHd(customerOrderHd);
						customerOrderTrans.setStatus("FAIL");
						customerOrderTrans.setPaymentMethodCode(cardType.trim());
						customerOrderTrans.setOpgResponseCode(posResponse.getResponseCode());
						customerOrderTrans.setAgentTransactionNo(posResponse.getTraceNumber());
						customerOrderTrans.setInternalRemark(posResponse.getResponseText());
						customerOrderTransService.updateCustomerOrderTrans(customerOrderTrans);
					}
				}
			}else{
				ecrLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, "referenceNo is null"));
			}

			return "OK";

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return "ERROR";
		}

	}

	@RequestMapping(value = "/transactionstatus/{transactionNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionStatus(@PathVariable(value = "transactionNo") Long transactionNo) {

		logger.info("CourseEnrollmentController.getTransactionStatus start ...");

		try {
			Map<String, Object> resultMap = new HashMap<String, Object>();
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			CustomerOrderHd customerOrderHd = null;
			if (customerOrderTrans != null) {
				customerOrderHd = customerOrderTrans.getCustomerOrderHd();
				if (customerOrderHd != null) {
					resultMap.put("orderNo", customerOrderHd.getOrderNo());
					resultMap.put("status", customerOrderHd.getOrderStatus());
				}
			}

			resultMap.put("lastTransactionStatus", customerOrderTrans);
			logger.info("CourseEnrollmentController.getTransactionStatus invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_UNKNOWN_ERROR);
			return responseResult;
		}
	}

	@RequestMapping(value = "/transaction/{enrollId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseEnrollTransactionInfo(@PathVariable(value = "enrollId") String enrollId) {

		logger.info("CourseEnrollmentController.getCourseEnrollTransactionInfo start ...");

		try {
			CourseTransactionDto transactionInfo = courseEnrollmentService.getTransactionInfoByEnrollId(enrollId);
			logger.info("CourseEnrollmentController.getCourseEnrollTransactionInfo invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, transactionInfo);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseEnrollmentError.COURSE_ENROLLMENT_UNKNOWN_ERROR);
			return responseResult;
		}
	}

}
