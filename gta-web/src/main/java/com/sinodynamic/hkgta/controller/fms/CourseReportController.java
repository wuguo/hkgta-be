package com.sinodynamic.hkgta.controller.fms;

import com.sinodynamic.hkgta.controller.salesreport.ReportController;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.util.response.ResponseResult;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "/salesreport")
@Scope("prototype")
public class CourseReportController extends ReportController {

    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/monthlyCourseCompletionReport", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult getMonthlyCourseCompletionReport(
            @RequestParam(value = "sortBy", defaultValue = "closeDate", required = false) String sortBy,
            @RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
            @RequestParam(value = "closedDate", required = true) String closedDate,
            @RequestParam(value = "courseType", required = false) String courseType) {
        if (isAscending.equals("true")) {
            page.addAscending(sortBy);
        } else if (isAscending.equals("false")) {
            page.addDescending(sortBy);
        }
        page.setNumber(pageNumber);
        page.setSize(pageSize);

        return courseService.getMonthlyCourseCompletionReport(page, closedDate, courseType);
    }

    @RequestMapping(value = "/monthlyCourseCompletionReportAttach", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getMonthlyCourseCompletionReportAttach(
            @RequestParam(value = "closedDate", required = true) String closedDate,
            @RequestParam(value = "courseType", required = false) String courseType,
            @RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
            @RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
            @RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
            HttpServletRequest request,
            HttpServletResponse response) {
        try {
            handleExpirySession(request, response);

            String facilityTypeTitle = "All";
            if ("GSS".equals(courseType)) {
                facilityTypeTitle = "Golf";
            } else if ("TSS".equals(courseType)) {
                facilityTypeTitle = "Tennis";
            }

            StringBuilder fileName = new StringBuilder();
            String str = getFileDateStr(closedDate);

            fileName.append("Monthly Course Completion").append(" - (").append(facilityTypeTitle).append(") - ").append(str);

            chooseFileType(request, response, fileName.toString(), fileType);

            return courseService.getMonthlyCourseCompletionReportAttach(closedDate, fileType, courseType, sortBy, isAscending);
        } catch (Exception e) {
            logger.error("SalesReportController.getMonthlyEnrollmentAttach Exception:", e);
        }
        return null;
    }


    @RequestMapping(value = "/monthlyCourseRevenueReport", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult getMonthlyCourseRevenueReport(
            @RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
            @RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
            @RequestParam(value = "transactionDate", required = true) String transactionDate,
            @RequestParam(value = "facilityType", required = false) String facilityType) {
        if (isAscending.equals("true")) {
            page.addAscending(sortBy);
        } else if (isAscending.equals("false")) {
            page.addDescending(sortBy);
        }
        page.setNumber(pageNumber);
        page.setSize(pageSize);

        return courseService.getMonthlyCourseRevenueReport(page, transactionDate, facilityType);
    }

    @RequestMapping(value = "/monthlyCourseRevenueReportAttach", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getMonthlyCourseRevenueReportAttach(
            @RequestParam(value = "transactionDate", required = true) String transactionDate,
            @RequestParam(value = "facilityType", required = false) String facilityType,
            @RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
            @RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
            @RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
            HttpServletRequest request,
            HttpServletResponse response) {
        try {
            handleExpirySession(request, response);

            String facilityTypeTitle = "All";
            if ("GSS".equals(facilityType)) {
                facilityTypeTitle = "Golf";
            } else if ("TSS".equals(facilityType)) {
                facilityTypeTitle = "Tennis";
            }

            StringBuilder fileName = new StringBuilder();
            String str = getFileDateStr(transactionDate);

            fileName.append("Monthly Course Revenue").append(" - (").append(facilityTypeTitle).append(") - ").append(str);
            chooseFileType(request, response, fileName.toString(), fileType);

            return courseService.getMonthlyCourseRevenueReportAttach(transactionDate, fileType, facilityType, sortBy, isAscending);
        } catch (Exception e) {
            logger.error("SalesReportController.getMonthlyCourseRevenueReportAttach Exception:", e);
        }
        return null;
    }

    private String getFileDateStr(@RequestParam(value = "transactionDate", required = true) String transactionDate) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM").parseDateTime(transactionDate);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MMM");
        return fmt.print(dateTime);
    }
}
