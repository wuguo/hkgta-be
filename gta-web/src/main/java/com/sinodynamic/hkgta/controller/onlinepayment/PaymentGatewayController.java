package com.sinodynamic.hkgta.controller.onlinepayment;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.util.DateConvertUtil;

/**
 * payment gateway controller
 * @author Allen_Yu
 *
 */
@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/payment")
public class PaymentGatewayController extends ControllerBase{
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	

	@RequestMapping(value = "/pay", method = RequestMethod.POST)
	public String payment(HttpServletRequest request) {
		return paymentGatewayService.payment(null);
	}
	
	/**
	 * return the callback url
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/callback", method = RequestMethod.GET)
	public @ResponseBody String processReturnData(HttpServletRequest request){
		logger.info("call back come in :"+request.getRequestURL().toString());
		String loginUserId =  getUser().getUserId();
		return paymentGatewayService.processResult(request,loginUserId);		
	}
	
	@RequestMapping(value = "/querydr", method = RequestMethod.GET)
	public void queryResult(HttpServletRequest request){
		LoginUser currentUser = getUser();
		paymentGatewayService.queryResult(null);
	}
	public static void main(String[] args) throws IOException {
//		File file = new File("D:\\write.csv");  
//        Writer writer = new FileWriter(file);  
//        CSVWriter csvWriter = new CSVWriter(writer, ',');  
//        String[] strs = {"13424234" , "dsfsdff" , "2432432423"};
//        String[] strs1 = {"33" , "dd" , "33"};
//        String[] strs2 = {"fdfdfd" , "34535435" , "dvxbvvcbvcbvcnnnnnvxcv"};
//        String[] strs3 = {"243243434" , "3434345" , "34324343432434"};
//        String[] strs4 = {"dfssdfdsg" , "vgbsdfsfsdfsdf" , "fdsfsdfdsfsdfsdf"};
//        String[] strs5 = {"dfsdfdsfsdfsdfsdfdsf" , "abc" , "dfsdfdsfsfdf"};
//        String[] strs6 = {"23432432423423" , "abc" , "vbcbcvbn"};
//        csvWriter.writeNext(strs);  
//        csvWriter.writeNext(strs1);  
//        csvWriter.writeNext(strs2);  
//        csvWriter.writeNext(strs3);  
//        csvWriter.writeNext(strs4);  
//        csvWriter.writeNext(strs5);  
//        csvWriter.writeNext(strs6);  
//        csvWriter.close();  
        
        
        Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -7);
		Date timeout = calendar.getTime();
		System.out.println(DateConvertUtil.getYMDDate(timeout));
	}
}
