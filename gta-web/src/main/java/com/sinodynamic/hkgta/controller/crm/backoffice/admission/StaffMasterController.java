package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.SitePoiDto;
import com.sinodynamic.hkgta.dto.crm.StaffCardMasterDto;
import com.sinodynamic.hkgta.dto.staff.RequestStaffDto;
import com.sinodynamic.hkgta.dto.staff.StaffCardInfoDto;
import com.sinodynamic.hkgta.dto.staff.StaffCardDataPrintDto;
import com.sinodynamic.hkgta.dto.staff.StaffCoachInfoDto;
import com.sinodynamic.hkgta.dto.staff.StaffListDto;
import com.sinodynamic.hkgta.dto.staff.StaffMasterInfoDto;
import com.sinodynamic.hkgta.dto.staff.StaffPasswordDto;
import com.sinodynamic.hkgta.dto.staff.UserDto;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.StaffSitePoiMonitor;
import com.sinodynamic.hkgta.entity.ibeacon.SitePoi;
import com.sinodynamic.hkgta.service.adm.StaffMasterService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.StaffSitePoiService;
import com.sinodynamic.hkgta.service.crm.sales.StaffProfileService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.StaffMasterOrderByCol;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/staffs")
public class StaffMasterController extends ControllerBase<StaffMasterInfoDto>{
	
	
	private Logger logger = Logger.getLogger(StaffMasterController.class);
	
	private @Autowired StaffMasterService staffMasterService;
	
	private @Autowired AdvanceQueryService advanceQueryService;
	
	@Autowired
	private StaffProfileService staffProfileService;
	
	@Autowired
	private StaffSitePoiService staffSitePoiService;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public @ResponseBody ResponseResult getStaffMasterList(@ModelAttribute StaffListDto staffListDto) throws Exception {
		
		logger.info("getStaffMasterList start!");
		
		ListPage<StaffMasterInfoDto> listPage = new ListPage<StaffMasterInfoDto>(staffListDto.getPageSize(), staffListDto.getPageNumber());
		ListPage<StaffMasterInfoDto> staffMasterList = null;

		staffMasterList = staffMasterService.getStaffMasterList(listPage, getOrderByCol(staffListDto.getSortBy()), getSortType(staffListDto.getIsAscending()), staffListDto);

		if (null == staffMasterList)
		{
			responseResult.initResult(GTAError.StaffMgrError.RETRIEVE_STAFF_FAILED);
			return responseResult;
		}

		int count = staffMasterList.getDtoList().size();
		if (count == 0)
		{
			responseResult.initResult(GTAError.StaffMgrError.NO_STAFF_FOUND);
			return responseResult;
		}

		Data staffs = new Data();
		staffs.setList(staffMasterList.getDtoList());
		staffs.setRecordCount(listPage.getAllSize());
		staffs.setCurrentPage(staffMasterList.getNumber());
		staffs.setTotalPage(staffMasterList.getAllPage());
		staffs.setPageSize(listPage.getSize() == count ? listPage.getSize() : count);
		staffs.setLastPage(listPage.isLast());

		responseResult.initResult(GTAError.Success.SUCCESS, staffs);
		return responseResult;
	}
	
	@RequestMapping(value = "/coachs", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult getCoachlist(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="createDate", required = false) String sortBy,
			@RequestParam(value="status",defaultValue="ALL", required = false) String status,
			@RequestParam(value="staffType",defaultValue="", required = false) String staffTypes,
			@RequestParam(value="isAscending",defaultValue="false", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) throws Exception{
		
		String joinSQL = staffMasterService.getCoachs();
		if(!"All".equalsIgnoreCase(status) && !StringUtils.isEmpty(status)){
			joinSQL += " and tb.status= '"+status+"'  ";
		}
		//staffTypes maybe FTR,FTG or FTR
		if(!"All".equalsIgnoreCase(staffTypes) && !StringUtils.isEmpty(staffTypes)){
			if(!staffTypes.contains(",")){
				joinSQL += " and tb.staffType= '"+staffTypes+"'  ";
			}else{
				joinSQL += " and tb.staffType in ('"+staffTypes.replaceAll(",", "','")+"' ) ";
			}
		}
		ResponseResult tmpResult = null;
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
		if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
			joinSQL = joinSQL + " AND ";
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
			tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, StaffMasterInfoDto.class);
		}
		else
		{
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, StaffMasterInfoDto.class);
		}
		
		for(Object staff : tmpResult.getListData().getList())
		{
			((StaffMasterInfoDto)staff).setEmployDate(DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(((StaffMasterInfoDto)staff).getEmployDate(), "yyyy/MM/dd")));
			((StaffMasterInfoDto)staff).setQuitDate(DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(((StaffMasterInfoDto)staff).getQuitDate(), "yyyy/MM/dd")));
		}
		
		return tmpResult;
	}
	
	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryData(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="createDate", required = false) String sortBy,
			@RequestParam(value="status",defaultValue="ALL", required = false) String status,
			@RequestParam(value="staffType",defaultValue="", required = false) String staffType,
			@RequestParam(value="isAscending",defaultValue="false", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) throws Exception{
		

		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
		String joinSQL = staffMasterService.getStaffs(advanceQueryDto);
		
		if(!"All".equalsIgnoreCase(status) && !StringUtils.isEmpty(status)){
			joinSQL += " and sub.status= '"+status+"'  ";
		}
		
		if(!"All".equalsIgnoreCase(staffType) && !StringUtils.isEmpty(staffType)){
			joinSQL += " and sub.staffType= '"+staffType+"'  ";
		}
		ResponseResult tmpResult = null;
		if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
			joinSQL = joinSQL + " AND ";
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
			tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, StaffMasterInfoDto.class);
		}
		else
		{
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, StaffMasterInfoDto.class);
		}
		
		for(Object staff : tmpResult.getListData().getList())
		{
			((StaffMasterInfoDto)staff).setEmployDate(DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(((StaffMasterInfoDto)staff).getEmployDate(), "yyyy-MM-dd")));
			((StaffMasterInfoDto)staff).setQuitDate(DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(((StaffMasterInfoDto)staff).getQuitDate(), "yyyy-MM-dd")));
		}
		
		return tmpResult;
	}
	
	/**
	 * 通过查询条件获取StaffCard列表
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param status
	 * @param isAscending
	 * @param filters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/CardList", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryCardListData(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="createDate", required = false) String sortBy,
			@RequestParam(value="status",defaultValue="ALL", required = false) String status,
			@RequestParam(value="isAscending",defaultValue="false", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) throws Exception{
		
		String joinSQL = staffMasterService.getStaffsCard(status);
		ResponseResult tmpResult = null;
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
		if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
			joinSQL = joinSQL + " AND ";
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
			tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, StaffCardInfoDto.class);
		}
		else
		{
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, StaffCardInfoDto.class);
		}
		
		for(Object staff : tmpResult.getListData().getList())
		{
			((StaffCardInfoDto)staff).setEffectiveDate((DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(((StaffCardInfoDto)staff).getEffectiveDate(), "yyyy-MM-dd"))));
		}
		
		return tmpResult;
	}
	
	/**
	 * 给staff绑定卡
	 * @param dto
	 * @return
	 */
	@RequestMapping(value = "/linkcard", method = RequestMethod.POST)
	@ResponseBody
	public  ResponseResult linkCard(@RequestBody StaffCardMasterDto dto) {
		logger.info("StaffMasterController.linkCard invocation start ...");
		try {
			return staffMasterService.linkCard(dto, getUser().getUserId());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	

	private String getSortType(Boolean isAscending){
		if(null == isAscending)
			return "";
		
		if(isAscending)
			return "asc";
		return "desc";
	}
	
	private StaffMasterOrderByCol getOrderByCol(String orderByCol) {
		
		if(StringUtils.isEmpty(orderByCol)){
			return null;
		}
		
		StaffMasterOrderByCol orderCol = null;
		
		switch (orderByCol.trim().toUpperCase())
		{
		case "STAFFID":
			orderCol = StaffMasterOrderByCol.STAFFID;
			break;
		case "STAFFNAME":
			orderCol = StaffMasterOrderByCol.STAFFNAME;
			break;
			
		case "STAFFTYPE":
			orderCol = StaffMasterOrderByCol.STAFFTYPE;
			break;
			
		case "STATUS":
			orderCol = StaffMasterOrderByCol.STATUS;
			break;
			
		case "POSITION":
			orderCol = StaffMasterOrderByCol.POSITIONTITLE;
			break;
			
		case "EMPLOYDATE":
			orderCol = StaffMasterOrderByCol.EMPLOYEEDATE;
			break;
			
		case "QUITDATE":
			orderCol = StaffMasterOrderByCol.QUITDATE;
			break;
			
		case "BOOKINGS":
			orderCol = StaffMasterOrderByCol.BOOKINGS;
			break;
			
		case "HIGHPRICE":
			orderCol = StaffMasterOrderByCol.HIGHPRICE;
			break;
			
		case "LOWPRICE":
			orderCol = StaffMasterOrderByCol.LOWPRICE;
			break;
			
		default:
			orderCol = null;
			break;
		}
		return orderCol;
	}
	
	
	@RequestMapping(value = "/{userId:.+}", method=RequestMethod.GET)
	@ResponseBody
	//public @ResponseBody ResponseResult getStaffMasterById(@RequestParam(value = "userId", required=true) String userId){
	public  ResponseResult getStaffMasterById(@PathVariable(value="userId") String userId){
		if(StringUtils.isEmpty(userId))
			return new ResponseResult("1", "parameter userId is null!");
		try {
			return staffMasterService.getStaffMasterByUserId(userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
			return new ResponseResult("1", "Search staffMaster error!");
		}
	}
	
	@RequestMapping(value = "/coachInfo/{userId}", method=RequestMethod.GET)
	@ResponseBody
	public  ResponseResult getCoachDesc(@PathVariable(value="userId") String userId){
		return staffMasterService.getCoachInfoByUserId(userId);
	}
	
	@RequestMapping(value = "/staffMasterInfo/{userId}", method=RequestMethod.GET)
	@ResponseBody
	public  ResponseResult getStaffMasterOnlyById(@PathVariable(value="userId") String userId){
		if(StringUtils.isEmpty(userId))
			return new ResponseResult("1", "parameter userId is null!");
		try {
			return staffMasterService.getStaffMasterOnlyByUserId(userId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			//logger.error("Search staffMaster by userid error!");
			return new ResponseResult("1", "Search staffMaster error!");
		}
	}
	
	
	@Deprecated
	@RequestMapping(value="/ignore", method=RequestMethod.POST)
	@ResponseBody
	public  ResponseMsg saveStaffMaster(@RequestBody RequestStaffDto staffDto){
		logger.info("saveStaffMaster start!");
		String userId = super.getUser().getUserId();
		try {
			ResponseMsg msg = staffMasterService.saveStaffMaster(staffDto, userId);
			return msg;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseResult("1", "save StaffMaster failed!");
		}
	}
	
	@Deprecated
	@RequestMapping(value="/ignore_1", method=RequestMethod.POST)
	@ResponseBody
	public  ResponseMsg saveStaff(@RequestBody UserDto staffDto) throws Exception{
		logger.info("saveStaffMaster start!");
		String userId = super.getUser().getUserId();

		ResponseMsg msg = staffMasterService.saveStaffMasterOld(staffDto, userId);
		return msg;

	}
	
	@RequestMapping(value="", method=RequestMethod.POST)
	@ResponseBody
	public  ResponseMsg newStaff(@RequestBody UserDto staffDto) throws Exception{
		logger.info("saveStaffMaster start!");
		String userId = super.getUser().getUserId();

		ResponseMsg msg = staffMasterService.saveStaffMaster(staffDto, userId);
		if(GTAError.Success.SUCCESS.getCode().equals(msg.getReturnCode())){
			String id = msg.getErrorMessageEN();
			Thread emailThread = new Thread(new EmailThread(staffDto, id));
			emailThread.start();
			msg.initResult(GTAError.StaffMgrError.SAVE_OR_UPDATE_SUCC, new String[] { "Save staff information", "Please assign roles to the staff before the staff login" });
		}
		return msg;

	}
	private class EmailThread implements Runnable {
		
		private final UserDto staffDto;
		private final String userId;
		public EmailThread(UserDto staffDto,String userId) {
			this.staffDto = staffDto;
			this.userId = userId;
		}

		@Override
		public void run() {
			try {
				staffMasterService.sendEmail2Staff(userId, staffDto);
			} catch (Exception e) {
				logger.error("error sent email", e);
				throw new GTACommonException(GTAError.CommonError.SEND_EMAIL_FAILED);
//				throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{"error sent email:",e.getMessage()});
			}
		}
	}
	@RequestMapping(value="/coach", method=RequestMethod.POST)
	@ResponseBody
	public  ResponseMsg saveCoachInfo(@RequestBody StaffCoachInfoDto coachDto) throws Exception{
		logger.info("save coach info start!");
		String userId = super.getUser().getUserId();

		ResponseMsg msg = staffMasterService.saveCoachDescription(coachDto, userId);
		return msg;

	}
	
	@RequestMapping(value="/coach", method=RequestMethod.PUT)
	public @ResponseBody ResponseMsg updateCoachInfo(@RequestBody StaffCoachInfoDto coachDto) throws Exception{
		logger.info("update coach info start!");
		String userId = super.getUser().getUserId();

		ResponseMsg msg = staffMasterService.saveCoachDescription(coachDto, userId);
		return msg;

	}
	
	@RequestMapping(value="/{userId:.+}", method=RequestMethod.PUT)
	public @ResponseBody ResponseMsg updateStaff(@PathVariable(value="userId") String userId,@RequestBody UserDto staffDto) throws Exception{
		logger.info("updateStaffMaster start!");
		try{
			String updateBy = super.getUser().getUserId();
	
			staffDto.getStaffMaster().setUserId(userId);
			staffDto.getStaffProfile().setUserId(userId);
	
			ResponseMsg msg = staffMasterService.updateStaff(staffDto, updateBy);
			return msg;
		} catch (GTACommonException e) {
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommonError.BAD_DATA_EXCEPTION, e.getErrorMsg());
			return responseResult;
		} catch (Exception e) {
			e.printStackTrace();
			responseResult.initResult(GTAError.CommonError.BAD_DATA_EXCEPTION);
			return responseResult;
		}
	
		
	}
	
	@RequestMapping(value="/ignore/update/{userId}", method=RequestMethod.PUT)
	public @ResponseBody ResponseMsg updateStaffold(@PathVariable(value="userId") String userId,@RequestBody UserDto staffDto) throws Exception{
		logger.info("updateStaffMaster start!");
		String updateBy = super.getUser().getUserId();

		staffDto.getStaffMaster().setUserId(userId);
		staffDto.getStaffProfile().setUserId(userId);

		ResponseMsg msg = staffMasterService.updateStaffMaster(staffDto, updateBy);
		return msg;
		
	}
	
	@Deprecated
	@RequestMapping(value="/ignore/{userId}", method=RequestMethod.PUT)
	public @ResponseBody ResponseMsg updateStaffMaster(@PathVariable(value="userId") String userId,@RequestBody RequestStaffDto staffDto){
		logger.info("updateStaffMaster start!");
		String updateBy = super.getUser().getUserId();
		try {
			
			staffDto.getStaffMaster().setUserId(userId);
			staffDto.getStaffProfile().setUserId(userId);
			
			ResponseMsg msg = staffMasterService.updateStaffMasterOld(staffDto, updateBy);
			return msg;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseResult("1", "update StaffMaster failed!");
		}
	}
	
	@RequestMapping(value="/changeStatus/{userId}", method=RequestMethod.PUT)
	public @ResponseBody ResponseMsg changeStaffMasterStatus(@PathVariable(value="userId") String userId,@RequestBody RequestStaffDto staffDto){
		logger.info("StaffMasterController.changeStaffMasterStatus invocation start ...");
		String updateBy = super.getUser().getUserId();
		
		try {
			staffDto.getStaffMaster().setUserId(userId);
			ResponseMsg msg = staffMasterService.changeStaffMasterStatus(staffDto, updateBy);
			return msg;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
			return new ResponseResult("1", "change StaffMaster status failed!");
		}
	}
	
	@RequestMapping(value="/updateprofilephoto",method=RequestMethod.POST)
	public @ResponseBody ResponseMsg updateprofilephoto(@RequestBody StaffProfile p){
		logger.info("StaffMasterController.updateprofilephoto invocation start ...");
		try {
			p.setUpdateBy(super.getUser().getUserId());
			p.setUpdateDate(new Date());
			return staffMasterService.updateProfilePhoto(p);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseMsg("1", "updateprofilephoto failed!");
		}
	}
	
	@RequestMapping(value="/exists/{staffNo}", method=RequestMethod.GET)
	public @ResponseBody ResponseResult isStaffNoUsed(@PathVariable(value="staffNo") String staffNo){
		logger.info("StaffMasterController.isStaffNoUsed invocation start ...");
		try {
			return staffMasterService.staffNoIsUsed(staffNo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseResult("1", "verify staffNo failed!");
		}
	}
	
	@RequestMapping(value="/email/exists", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody ResponseResult checkEmailUsed(@RequestParam(value="emailAddress", required = false) String emailAddress){
		logger.info("StaffMasterController.isStaffNoUsed invocation start ...");
		try {
			return staffMasterService.emailIsUsed(emailAddress);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseResult("1", "verify email failed!");
		}
	}
	
	@RequestMapping(value="/changepsw", method=RequestMethod.POST, produces="application/json")
	public @ResponseBody ResponseResult changePassword(@RequestBody StaffPasswordDto staffPsw) throws Exception
	{
		logger.info("StaffMasterController.change password for staff ...");

		return staffMasterService.changePsw(staffPsw);
	}
	
	@RequestMapping(value="/validateStaffInfo/{loginId}/{passportType}/{passportNo}", method=RequestMethod.GET)
	public @ResponseBody ResponseResult validatePassport(
			@PathVariable(value="loginId") String loginId,
			@PathVariable(value="passportType") String passportType,
			@PathVariable(value="passportNo") String passportNo) throws Exception 
	
	{
		logger.info("StaffMasterController.validate passport invocation start ...");

		return staffMasterService.validatePassport(loginId, passportType, passportNo);
	}
	
	@RequestMapping(value = "/selectLocation", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult selectLocation(@RequestBody StaffSitePoiMonitor selectedLocation) {
		try {
			
			String staffUserId = selectedLocation.getStaffUserId();
			Long sitePoiId = selectedLocation.getSitePoiId();

			if (StringUtils.isEmpty(staffUserId) ){
				responseResult.initResult(GTAError.CommonError.PARAMETER_VALUE_INVALID);
				return responseResult;
			}

			selectedLocation.setCreateDate(new Date());
			staffSitePoiService.saveStaffSitePoiMonitor(selectedLocation);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/listStaffs", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult listStaffs(@RequestParam(value="sitePoiId") Long sitePoiId) {
		try {
			if (sitePoiId == null){
				responseResult.initResult(GTAError.CommonError.PARAMETER_MISSING);
				return responseResult;
			}
			
			List<StaffSitePoiMonitor> list = staffSitePoiService.listBySiteId(sitePoiId);
			Data data = new Data();
			data.setList(list);
			responseResult.initResult(GTAError.Success.SUCCESS, data);;
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	
	@RequestMapping(value = "/staffMonitor", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult staffMonitor(@RequestParam(value="staffUserId") String staffUserId) {
		try {
			if (staffUserId == null){
				responseResult.initResult(GTAError.CommonError.PARAMETER_MISSING);
				return responseResult;
			}
			
			List<StaffSitePoiMonitor> list = staffSitePoiService.listByUserId(staffUserId);
			Data data = new Data();
			data.setList(list);
			responseResult.initResult(GTAError.Success.SUCCESS, data);;
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/listLocations", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult listLocations() {
		try {
			
			List<SitePoi> list = staffSitePoiService.listLocations();
			List<SitePoiDto> dtolist = new ArrayList<>();
			for (SitePoi site : list){
				SitePoiDto dto = new SitePoiDto();
				dto.setPoiId(site.getPoiId());
				dto.setDescription(site.getDescription());
				dto.setVenueCode(site.getVenueCode());
				dtolist.add(dto);
			}
			
			Data data = new Data();
			data.setList(dtolist);
			responseResult.initResult(GTAError.Success.SUCCESS, data);;
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	

	@RequestMapping(value = "/deleteStaffMonitor", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteStaffMonitor(@RequestParam(value="sysId") Long sysId) {
		try {
		    staffSitePoiService.deleteById(sysId);
			
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * 查询出员工卡上的staff id, staff name and staff photo返回给前端
	 * added by Kaster 20160217
	 */
	@RequestMapping(value = "/getStaffCardDataForPrint", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getStaffCardData(@RequestParam(value="userId") String userId){
		try {
			String sql = "select t.staffId,t.staffName,t.staffPhoto, IFNULL(CONCAT('S',c.card_id),'') cardId "
					+ " from (select m.staff_no staffId,"
					+ "CONCAT(p.surname,' ',p.given_name) staffName,"
					+ "p.portrait_photo staffPhoto,"
					+ "m.user_id user_id "
					+ " from staff_master m,staff_profile p where m.user_id=p.user_id and m.user_id='" + userId + "') t "
					+ " LEFT JOIN internal_permit_card c ON c.staff_user_id=t.user_id";
			return advanceQueryService.getAdvanceQueryResultBySQL(new AdvanceQueryDto(), sql, page, StaffCardDataPrintDto.class);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	@RequestMapping(value = "/getPhoto/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPhoto(@PathVariable(value="userId") String userId){
		try {
			
			StaffProfile profile=staffProfileService.getStaffProfileById(userId);
			if(null!=profile)
			{
				responseResult.initResult(GTAError.Success.SUCCESS,(Object)profile.getPortraitPhoto());
			}else{
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION,"No find photo by userId");	
			}
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

}
