package com.sinodynamic.hkgta.controller.salesreport;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.MemberCashvalueDto;
import com.sinodynamic.hkgta.dto.crm.ReservationDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.crm.SearchSettlementReportDto;
import com.sinodynamic.hkgta.dto.crm.SourceBookingDto;
import com.sinodynamic.hkgta.dto.fms.PatronActivitiesDto;
import com.sinodynamic.hkgta.dto.rpos.CashValueLogDto;
import com.sinodynamic.hkgta.dto.sys.ProgramMasterDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.MemberCashvalue;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.security.service.RoleProgramService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberTransactionService;
import com.sinodynamic.hkgta.service.crm.membercash.MemberCashvalueService;
import com.sinodynamic.hkgta.service.crm.sales.StaffProfileService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.pms.HouseKeepTaskService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.service.sys.UserRoleService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant.Status;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RoomHousekeepTaskJobType;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;


@Controller
@Scope("prototype")
@RequestMapping("/salesreport")
public class SalesReportController extends ReportController {

	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;

	@Autowired
	private CustomerOrderTransService customerOrderTransService;
	
	@Autowired
	private MemberCashvalueService memberCashvalueService;

	@Autowired
	private UserMasterService userMasterService;
	
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private ServicePlanService servicePlanService;

	@Autowired
	private RoleProgramService roleProgramService;

	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private HouseKeepTaskService  houseKeepTaskService;
	
	@Autowired
	private StaffProfileService staffProfileService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired
	private MemberTransactionService memberTransactionService;
	

	@RequestMapping(value = "/getAllSalesmen", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAllSalesmen() {
		ResponseResult responseResult = customerEnrollmentService.getAllSalesmen();
		return responseResult;
	}

	@RequestMapping(value = "/getAllCreatedStaff", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAllCreatedStaff() {
		ResponseResult responseResult = customerOrderTransService.getAllCreatedStaff();
		return responseResult;
	}

	@RequestMapping(value = "/getAllAuditedStaff", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAllAuditedStaff() {
		ResponseResult responseResult = customerOrderTransService.getAllAuditedStaff();
		return responseResult;
	}

	@RequestMapping(value = "/getSattlementReport", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getSattlementReport(
			@RequestParam(value = "sortBy", defaultValue = "transactionId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "transactionDate", defaultValue = "0d", required = false) String transactionDate,
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "orderStatus", defaultValue = "", required = false) String orderStatus,
			@RequestParam(value = "transStatus", defaultValue = "", required = false) String transStatus,
			@RequestParam(value = "enrollType", defaultValue = "", required = false) String enrollType,
			@RequestParam(value = "location", defaultValue = "", required = false) String location,
			@RequestParam(value = "salesman", defaultValue = "", required = false) String salesman,
			@RequestParam(value = "createBy", defaultValue = "", required = false) String createBy,
			@RequestParam(value = "auditBy", defaultValue = "", required = false) String auditBy) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		SearchSettlementReportDto dto = new SearchSettlementReportDto(sortBy, pageNumber, pageSize, isAscending,
				transactionDate, startDate, endDate, orderStatus, transStatus, enrollType, location, salesman,
				createBy, auditBy);

		ResponseResult responseResult = customerOrderTransService.getSattlementReport(page, dto);
		return responseResult;
	}

	@RequestMapping(value = "/getSattlementReportExport", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getSattlementReport(
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "transactionId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "transactionDate", defaultValue = "0d", required = false) String transactionDate,
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "orderStatus", defaultValue = "", required = false) String orderStatus,
			@RequestParam(value = "transStatus", defaultValue = "", required = false) String transStatus,
			@RequestParam(value = "enrollType", defaultValue = "", required = false) String enrollType,
			@RequestParam(value = "location", defaultValue = "", required = false) String location,
			@RequestParam(value = "salesman", defaultValue = "", required = false) String salesman,
			@RequestParam(value = "createBy", defaultValue = "", required = false) String createBy,
			@RequestParam(value = "auditBy", defaultValue = "", required = false) String auditBy,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		handleExpirySession(request, response);
		SearchSettlementReportDto dto = new SearchSettlementReportDto(fileType, sortBy, pageNumber, pageSize,
				isAscending, transactionDate, startDate, endDate, orderStatus, transStatus, enrollType, location,
				salesman, createBy, auditBy);

		ResponseResult responseResult = customerOrderTransService.getSattlementReportExport(page, dto, response);
		return (byte[]) responseResult.getData();
	}
	
	/**
	 * @deprecated Tempory not using. It's a refactored version of the settlement report.
	 * @author Liky_Pan
	 */
	@Deprecated
	@RequestMapping(value = "/getEnrollmentRenewalAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getEnrollmentRenewalAttach(
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Enrollment and Renewal", fileType);
			
			return customerOrderTransService.getEnrollmentRenewal(fileType, sortBy, isAscending);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getMonthlyEnrollment", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMonthlyEnrollment(
			@RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "year",  required = true) String year,
			@RequestParam(value = "month", required = true) String month) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ResponseResult responseResult = customerOrderTransService.getMonthlyEnrollment(page,year,month);
		return responseResult;
	}
	
	
	/**
	 * SGG-1384
	 * 
	 * @param reservationMonth
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param year
	 * @param month
	 * @return
	 */
	@RequestMapping(value = "/transaction/getMonthlyTransaction", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMonthlyTransaction(
			@RequestParam(value = "reservationMonth", defaultValue = "", required = false) String reservationMonth,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending
	 ) {
		page =  page = new ListPage<SourceBookingDto>();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getMonthlyTransaction(page,reservationMonth);
		return responseResult;
	}
	
	/**
	 * SGG-1385
	 * download report
	 * @param reservationMonth
	 * @param fileType
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/transaction/getMonthlyTransactionAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getMonthlyTransactionReport(
			@RequestParam(value = "reservationMonth", defaultValue = "", required = false) String reservationMonth,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		
		handleExpirySession(request, response);
		
		chooseFileType(request, response, "Source of Booking<"+reservationMonth+">", fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getMonthlyTransactionAttachment(page,reservationMonth,fileType);
		return responseResult;
	}
	
	/**
	 * SGG-1380
	 * @param reportMonth
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/transaction/getGolfAndTennisCoachesPerform", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getGolfAndTennisCachesPerform(
			@RequestParam(value = "reportMonth", defaultValue = "", required = false) String reportMonth,
			@RequestParam(value = "", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending
	 ) throws ParseException {
		page =  page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getGolfAndTennisCachesPerform(page,reportMonth);
		return responseResult;
	}
	/**
	 * SGG-1381
	 * @param reportMonth
	 * @param fileType
	 * @param sortBy
	 * @param isAscending
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/transaction/getGolfAndTennisCoachesPerformAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getGolfAndTennisCachesPerformAttachement(
			@RequestParam(value = "reservationMonth", defaultValue = "", required = false) String reportMonth,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		
		handleExpirySession(request, response);
		chooseFileType(request, response, "Golf and Tennis coaches performance<"+reportMonth+">", fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getGolfAndTennisCachesPerformAttachment(page,reportMonth,fileType);
		return responseResult;
	}
	
	
	/**
	 * SGG-1388
	 * @param coachName
	 * @param startTime
	 * @param endTime
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "/transaction/getCoachTraining", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCoachTraining(
			@RequestParam(value = "coachUserId", defaultValue = "", required = false) String coachUserId,
			@RequestParam(value = "startTime", defaultValue = "", required = false) String startTime,
			@RequestParam(value = "endTime", defaultValue = "", required = false) String endTime,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending
	 ) {
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getCoachTraining(page,coachUserId,startTime,endTime);
		return responseResult;
	}
 /**
  * sgg-1389
  * @param fileType
  * @param coachName
  * @param startDate
  * @param endDate
  * @param request
  * @param response
  * @return
 * @throws Exception 
  */
	@RequestMapping(value = "/transaction/getCoachTrainingReport", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getCoachTrainingReport(
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "coachUserId", defaultValue = "", required = false) String coachUserId,
			@RequestParam(value = "coachName", defaultValue = "", required = false) String coachName,
			@RequestParam(value = "startTime", defaultValue = "", required = false) String startTime,
			@RequestParam(value = "endTime", defaultValue = "", required = false) String endTime,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending,
			HttpServletRequest request,
			HttpServletResponse response
	 ) throws Exception { 
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		handleExpirySession(request, response);
		chooseFileType(request, response, "CoachTrainingReport<"+coachName+">-<"+startTime+"-"+endTime+">", fileType);
		byte[] responseResult = customerOrderTransService.getCoachTrainingReport(page,fileType,coachUserId,coachName,startTime,endTime);
		return responseResult;
	}
	

	
	@RequestMapping(value = "/getDailyRevenue", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDailyRevenue(
			@RequestParam(value = "location", required = true) String location,
			@RequestParam(value = "startDay", required = true) String startDay,
			@RequestParam(value = "endDay", required = true) String endDay,
			@RequestParam(value = "startTime", required = true) String startTime,
			@RequestParam(value = "endTime", required = true) String endTime
			) {
		try {
			startTime=startDay+" "+startTime+":00";
			endTime=endDay+" "+endTime+":00";
			ResponseResult responseResult = customerOrderTransService.getDailyRevenue(startTime, endTime,location);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getDailyRevenueAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyRevenueAttach(
			@RequestParam(value = "location", required = true) String location,
			@RequestParam(value = "startDay", required = true) String startDay,
			@RequestParam(value = "endDay", required = true) String endDay,
			@RequestParam(value = "startTime", required = true) String startTime,
			@RequestParam(value = "endTime", required = true) String endTime,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			startTime=startDay+" "+startTime;
			endTime=(StringUtils.isNotEmpty(endDay)?endDay:startDay)+" "+endTime;
			chooseFileType(request, response, "Daily Revenue("+startTime+"_"+endTime+")", fileType);
			startTime=startTime+":00";
			endTime=endTime+":00";
			return customerOrderTransService.getDailyRevenueAttachment(startTime, endTime,location,fileType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 *
	 * 
	 * @param day
	 * @param type
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param year
	 * @param month
	 * @return
	 */
	@RequestMapping(value = "/getNoShowReport", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getNoShowReport(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending
	 ) {
		logger.debug("start getNoShowReport");
		page = new ListPage<ReservationDto>();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getReservationReport(page,day,type,Status.NAT.name());
		return responseResult;
	}
	
	/**
	 *
	 * download no show report
	 * @param day
	 * @param type
	 * @param fileType
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getNoShowReportAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getNoShowReportAttachement(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		logger.debug("start getNoShowReportAttachement");
		handleExpirySession(request, response);
		
		chooseFileType(request, response, "No Show Report "+day, fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getReservationReportAttachement(page,day,fileType,type,Status.NAT.name());
		return responseResult;
	}
	/**
	 *
	 * 
	 * @param day
	 * @param type
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param year
	 * @param month
	 * @return
	 */
	@RequestMapping(value = "/getMemberInfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberInfo(@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending) {
		logger.debug("start getMemberInfo");
		page = new ListPage<ReservationDto>();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getMemberInfo(page, Status.ACT.name());
		return responseResult;
	}

	/**
	 *
	 * download Daily Booking report
	 * 
	 * @param day
	 * @param type
	 * @param fileType
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getMemberInfoAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getMemberInfoAttachement(@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.debug("start getMemberInfoAttachement");
		handleExpirySession(request, response);
		Date day = new Date();
		String theDay = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss").format(day);
		/**
		 * update report for SGG-2693
		 * chooseFileType(request, response, "Cust Info " + theDay, fileType);
		 */
		chooseFileType(request, response, "ActivePatronProfile", fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getMemberInfoAttachement(page, fileType, Status.ACT.name());
		return responseResult;
	}
	/**
	 *
	 * 
	 * @param day
	 * @param type
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param year
	 * @param month
	 * @return
	 */
	@RequestMapping(value = "/getCancellation", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCancellation(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending
	 ) {
		logger.debug("start getCancellationReport");
		page = new ListPage<ReservationDto>();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getReservationReport(page,day,type,Status.CAN.name());
		return responseResult;
	}
	
	/**
	 *
	 * download Cancellation report
	 * @param day
	 * @param type
	 * @param fileType
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getCancellationAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getCancellationAttachement(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		logger.debug("start getCancellationReportAttachement");
	    handleExpirySession(request, response);
		
		chooseFileType(request, response, "Cancellation "+day, fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getReservationReportAttachement(page,day,fileType,type,Status.CAN.name());
		return responseResult;
	}
	/**
	 *
	 * 
	 * @param day
	 * @param type
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param year
	 * @param month
	 * @return
	 */
	@RequestMapping(value = "/getDailyBooking", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDailyBooking(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending
	 ) {
		logger.debug("start getDailyBookingReport");
		page = new ListPage<ReservationDto>();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getReservationReport(page,day,type,Status.RSV.name());
		return responseResult;
	}
	
	/**
	 *
	 * download Daily Booking report
	 * @param day
	 * @param type
	 * @param fileType
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getDailyBookingAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyBookingAttachement(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		logger.debug("start getDailyBookingAttachement");
		handleExpirySession(request, response);
		
		chooseFileType(request, response, "Daily Booking "+day, fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getReservationReportAttachement(page,day,fileType,type,Status.RSV.name());
		return responseResult;
	}
	
	@RequestMapping(value = "/getDailyRevenueInDetail", method = RequestMethod.GET)
	@ResponseBody	
	public ResponseResult getDailyRevenueInDetail(
			@RequestParam(value = "paymentMethod", defaultValue = "ALL", required =false) String paymentMethod,
			@RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "startDate", defaultValue = "", required = true) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = true) String endDate,
			@RequestParam(value = "type", required = true) String type
			) {
		try {
			logger.info("Ready to get transaction list");
			page = new ListPage<CustomerOrderHd>();
			String filterBySalesMan = null;
			if (isAscending.equals("true")) {
				page.addAscending(sortBy);
			} else if (isAscending.equals("false")) {
				page.addDescending(sortBy);
			}
			if(pageNumber>0){
				page.setNumber(pageNumber);
			}
			if(pageSize > 0){
				page.setSize(pageSize);
			}

			return  customerOrderTransService.getDailyRevenueInDetail(page ,paymentMethod, startDate, endDate,  type);
//			ResponseResult response = new ResponseResult();
//			Data data = new Data();
//			data.setList(result.getDtoList());
//			data.setLastPage(result.isLast());
//			data.setCurrentPage(result.getNumber());
//			data.setRecordCount(result.getAllSize());
//			data.setPageSize(result.getSize());
//			data.setTotalPage(result.getAllPage());
//			response.setData(data);
//			response.setReturnCode("0");
//			response.setErrorMessageEN("");
//			return response;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.MemberShipError.SYSTEM_EXCEPTION);
			return responseResult;
		}
	}
	
	
	/**
	 *
	 * download Daily Revenue In Detail report
	 * @param day
	 * @param type
	 * @param fileType
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getDailyRevenueInDetailAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyRevenueInDetailAttachement(
			@RequestParam(value = "paymentMethod", defaultValue = "ALL", required = true) String paymentMethod,
			@RequestParam(value = "startDate", defaultValue = "", required = true) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = true) String endDate,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		logger.debug("start getDailyBookingAttachement");
		handleExpirySession(request, response);
		
		chooseFileType(request, response, "Daily Revenue In Detail " + startDate + "-" + endDate, fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getDailyRevenueInDetailAttachement(page, paymentMethod,startDate, endDate,fileType,type);
		return responseResult;
	}
	
	@RequestMapping(value = "/getMonthlyEnrollmentAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getMonthlyEnrollmentAttach(
			@RequestParam(value = "year", required = true) String year,
			@RequestParam(value = "month", required = true) String month,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "transactionDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			Date rTime=DateConvertUtil.parseString2Date(year+"-"+String.format("%0" + 2 + "d", Integer.valueOf(month)), "yyyy-MM");
			String reportName=DateConvertUtil.parseDate2String(rTime, "yyyy-MMM");
			chooseFileType(request, response, "Monthly Statement("+reportName+")", fileType);
			return customerOrderTransService.getMonthlyEnrollmentAttachment(year, month,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getDailyDayPassUsageAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyDayPassUsageAttach(
			@RequestParam(value = "date", required = true) String date,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "purchaseDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Daily Day Pass Usage Report - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date)), fileType);
			
			return servicePlanService.getDailyDayPassUsageAttachment(date,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	

	@RequestMapping(value = "/getDailyEnrollmentAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyEnrollmentAttach(
			@RequestParam(value = "enrollDate", required = true) String enrollDate,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "enrollDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,	HttpServletResponse response){
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Daily Report of Patron Acquisition - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(enrollDate)), fileType);
			return customerEnrollmentService.getDailyEnrollmentAttach(enrollDate,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getDailyLeads", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDailyLeads(
			@RequestParam(value = "sortBy", defaultValue = "createDate", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "selectedDate", required = true) String selectedDate) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ResponseResult responseResult = customerEnrollmentService.getDailyLeads(page, selectedDate);
		return responseResult;
	}
	
	@RequestMapping(value = "/getDailyLeadsAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyLeadsAttach(
			@RequestParam(value = "selectedDate", required = true) String selectedDate,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "createDate", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,	HttpServletResponse response){
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Daily Generated Prospect Reports - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate)), fileType);
			return customerEnrollmentService.getDailyLeadsAttach(selectedDate,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getDailyFacilityUsage", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDailyFacilityUsage(
			@RequestParam(value = "sortBy", defaultValue = "resvIdString", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "selectedDate", required = true) String selectedDate,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		ResponseResult responseResult = memberFacilityTypeBookingService.getDailyMonthlyFacilityUsage("DAILY",page, selectedDate, facilityType);
		return responseResult;
	}
	@RequestMapping(value = "/getDailyFacilityUsageAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getDailyFacilityUsageAttach(
			@RequestParam(value = "selectedDate", required = true) String selectedDate,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType,
			HttpServletRequest request,	HttpServletResponse response){
		try {
			handleExpirySession(request, response);
			String facilityTypeConvert = "GOLF".equals(facilityType)||"TENNIS".equals(facilityType)?("TENNIS".equals(facilityType)?"Tennis":"Golf"):"All";
			chooseFileType(request, response, "Daily Facility Usage Report ("+facilityTypeConvert+") - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate)), fileType);
			return memberFacilityTypeBookingService.getDailyFacilityUsageAttach("DAILY",selectedDate,fileType,sortBy,isAscending,facilityType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}
	
	@RequestMapping(value = "/getMonthlyFacilityUsage", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getMonthlyFacilityUsage(
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "year", required = true) String year,
			@RequestParam(value = "month", required = true) String month,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType) {
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
        String selectedDate = CommUtil.formatTheGivenYearAndMonthWithString(year, month);
		ResponseResult responseResult = memberFacilityTypeBookingService.getDailyMonthlyFacilityUsage("MONTHLY",page, selectedDate, facilityType);
		return responseResult;
	}
	@RequestMapping(value = "/getMonthlyFacilityUsageAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getMonthlyFacilityUsageAttach(
			@RequestParam(value = "year", required = true) String year,
			@RequestParam(value = "month", required = true) String month,
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "resvId", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "facilityType", defaultValue = "ALL", required = false) String facilityType,
			HttpServletRequest request,	HttpServletResponse response){
		try {
			handleExpirySession(request, response);
			String selectedDate = CommUtil.formatTheGivenYearAndMonthWithString(year, month);
			String facilityTypeConvert = "GOLF".equals(facilityType)||"TENNIS".equals(facilityType)?("TENNIS".equals(facilityType)?"Tennis":"Golf"):"All";
			chooseFileType(request, response, "Monthly Facility Usage Report ("+facilityTypeConvert+") - " + new SimpleDateFormat("yyyy-MMM").format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate)), fileType);
			return memberFacilityTypeBookingService.getDailyFacilityUsageAttach("MONTHLY",selectedDate,fileType,sortBy,isAscending,facilityType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSalesReportCategories() {
		if (getUser() != null && !StringUtils.isEmpty(getUser().getUserId())) {
			List<UserRoleDto> roles = userRoleService.getUserRoleListByUserId(getUser().getUserId());
			if (roles == null || roles.size() == 0) {
				responseResult.initResult(GTAError.Success.SUCCESS, new ArrayList<Object>(0));
				return responseResult;
			}
			List<Long> userRoleIds = new ArrayList<Long>(roles.size());
			for (UserRoleDto userRoleDto : roles) {
				userRoleIds.add(userRoleDto.getRoleId());
			}
			List<ProgramMasterDto> categories = roleProgramService.getSalesReportCategories(userRoleIds);
			responseResult.initResult(GTAError.Success.SUCCESS, categories);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/getHouseKeepTaskAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getHouseKeepTaskAttach(
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "jobType",required = false) String  jobType,
			@RequestParam(value = "startTime" ) String  startTime,
			@RequestParam(value = "endTime" ) String  endTime,
			@RequestParam(value="roomNumber", required = false) String roomNumber,
			@RequestParam(value="sortBy",defaultValue="assignedDate", required = false) String sortBy,
			@RequestParam(value="isAscending",defaultValue="true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			String fileName = "";
			String dateSectionStr = "<" + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(startTime)) + " - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(endTime))  + ">";
			if (RoomHousekeepTaskJobType.ADH.name().equals(jobType)) {
				fileName = "Ad Hoc Task - " + dateSectionStr;
			} else if ((RoomHousekeepTaskJobType.RUT.name().equals(jobType))) {
				fileName = "Routine Task - " + dateSectionStr;
			} else if ((RoomHousekeepTaskJobType.MTN.name().equals(jobType))) {
				fileName = "Maintenance Task - " + dateSectionStr;
			} else if ((RoomHousekeepTaskJobType.SCH.name().equals(jobType))) {
				fileName = "Schedule Task - " + dateSectionStr;
			} else if ((RoomHousekeepTaskJobType.RA.name().equals(jobType))) {
				fileName = "Room Assignment - " + dateSectionStr;
			}
			chooseFileType(request, response, fileName, fileType);
			
			return houseKeepTaskService.getHouseKeepTaskAttach(jobType,startTime,endTime,fileType,roomNumber,sortBy,isAscending);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	@RequestMapping(value = "/getRoomWeeklyAttendantAttach", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getRoomWeeklyAttendantAttach(
			@RequestParam(value = "fileType", required = true) String fileType,
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "startTime" ) String  startTime,
			@RequestParam(value = "endTime" ) String  endTime,
			@RequestParam(value="sortBy",defaultValue="date", required = false) String sortBy,
			@RequestParam(value="isAscending",defaultValue="true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			StaffProfile profile = this.staffProfileService.getStaffProfileById(userId);
			if (profile == null) {
				responseResult.initResult(GTAError.RemarksError.STAFF_MASTER_NULL);
				return null;
			}
			String userName = profile.getGivenName() + " " + profile.getSurname();
			String fileName = "Weekly Roster of  <" + userName + "> - <" + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(startTime)) + " - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(endTime))  + ">";
			chooseFileType(request, response, fileName, fileType);
			
			List<Date> dates = DateCalcUtil.getDates(DateConvertUtil.parseString2Date(startTime, "yyyy-MM-dd"),
					DateConvertUtil.parseString2Date(endTime, "yyyy-MM-dd"));
			int i = 0;
			StringBuffer dateSql = new StringBuffer();
			for (Date date : dates) {
				i++;
				String time = DateConvertUtil.parseDate2String(date, "yyyy-MM-dd");
				dateSql.append(" SELECT");
				dateSql.append(" '" + time + "' AS on_date");
				if (dates.size() != i) {
					dateSql.append(" UNION");
				}
			}
			
			return houseKeepTaskService.getRoomWeeklyAttendantAttach(userId,userName,dateSql.toString(),startTime,endTime,fileType,sortBy,isAscending);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	@RequestMapping(value = "/getCustomerOrderTransReportExport", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getCustomerOrderTransReport(
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "transactionNo", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
			try {
				handleExpirySession(request, response);
				String fileName = "CustomerOrderTrans";
				fileName += "<" + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(startDate)) + " - " + new SimpleDateFormat("yyyy-MMM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(endDate))  + ">";
				chooseFileType(request, response, fileName, fileType);
				return customerOrderTransService.getCustomerOrderTransAttachment(startDate, endDate, fileType, sortBy, isAscending);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			return null;
		}
	
	/**
	 *
	 * 
	 * @param day
	 * @param academyID
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param year
	 * @param month
	 * @return
	 */
	@RequestMapping(value = "/getPatronActivities", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPatronActivities(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "academyID", required = false) String academyID,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending
	 ) {
		logger.debug("start getPatronActivities");
		page = new ListPage<PatronActivitiesDto>();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getPatronActivitiesReport(page,day,academyID);
		return responseResult;
	}
	
	@RequestMapping(value = "/getPatronActivitiesAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getPatronActivitiesAttachement(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "academyID", required = false) String academyID,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		logger.debug("start getPatronActivitiesAttachement");
		handleExpirySession(request, response);
		
		chooseFileType(request, response, "Patron Activities "+day, fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getPatronActivitiesAttachement(page,day,fileType,academyID);
		return responseResult;
	}
	
	/**
	 *
	 * 
	 * @param day
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @return
	 */
	@RequestMapping(value = "/getCashValueVoid", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCashValueTxn(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending) {
		logger.debug("start getCashValueVoid");
		page = new ListPage<CashValueLogDto>();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getCashValueVoidReport(page,day);
		return responseResult;
	}
	
	@RequestMapping(value = "/getCashValueVoidAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getCashValueVoidAttachement(
			@RequestParam(value = "day", defaultValue = "", required = true) String day,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		logger.debug("start getCashValueVoidAttachement");
		handleExpirySession(request, response);
		
		chooseFileType(request, response, "Cash Value Void Report "+day, fileType);
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		byte[] responseResult = customerOrderTransService.getCashValueVoidAttachement(page,day,fileType);
		return responseResult;
	}
	
	/**
	 * 根据时间获取serviceRefund报表
	 * @param startTime
	 * @param endTime
	 * @param fileType
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getServiceRefundReports", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getServiceRefundReports(
			@RequestParam(value = "startTime", required = true) String startTime,
			@RequestParam(value = "endTime", required = true) String endTime,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Service Refund Reports("+startTime+"_"+endTime+")", fileType);
			return customerOrderTransService.getServiceRefundReports(startTime, endTime,fileType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * 根据时间获取CashValueAdjustmentRe报表
	 * @param startTime
	 * @param endTime
	 * @param fileType
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getCashValueAdjustmentReports", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getCashValueAdjustmentReports(
			@RequestParam(value="status",defaultValue="ALL", required = false) String status,
			@RequestParam(value="customerId",defaultValue="0", required = false) @EncryptFieldInfo Long customerId,
			@RequestParam(value = "startTime", required = true) String startTime,
			@RequestParam(value = "endTime", required = true) String endTime,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Cash Value Adjustment Reports("+startTime+"_"+endTime+")", fileType);
			return customerOrderTransService.getCashValueAdjustmentReports(status, customerId, startTime, endTime, fileType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * 获取某月生成会员报表
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param isAscending
	 * @param status
	 * @param expiry
	 * @param planName
	 * @param memberType
	 * @param month
	 * @param filters
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getMemberListByBirthday", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberListByBirthday(@RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "memberName") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam(value = "status", defaultValue = "ALL", required = true) String status,
			@RequestParam(value = "expiry", defaultValue = "ALL", required = true) String expiry,
			@RequestParam(value = "planName", defaultValue = "", required = false) String planName,
			@RequestParam(value = "memberType", defaultValue = "", required = false) String memberType,
			@RequestParam(value = "month", defaultValue = "", required = true) Integer month,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			logger.info("Ready to get member overview list");
			page.setNumber(pageNumber);
			page.setSize(pageSize);

			if (!StringUtils.isEmpty(filters)) {
				AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0
						&& advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
					for (SearchRuleDto rule : advanceQueryDto.rules) {
						if (logger.isDebugEnabled()) {
							logger.debug("rule field:" + rule.getField() + " op:" + rule.getOp() + " data:"
									+ rule.getData());
						}
						if ("t.status".equals(rule.field) && "ACTIVE".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_ACT);
						} else if ("t.status".equals(rule.field) && "INACTIVE".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_NACT);
						} else if ("t.status".equals(rule.field) && "EXPIRED".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_EXP);
						}
						if ("t.planName".equals(rule.field) && (rule.data.indexOf("'") > 0)) {
							rule.setData(rule.data.replace("'", "\\'"));
						}
					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}
			ResponseResult result = memberTransactionService.getMembersOverviewListByBirthday(page, sortBy, isAscending,
					"ALL" , status, expiry, planName,
					memberType, month);
			return result;

		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("SalesReportController.getMemberListByBirthday Exception : ", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	/**
	 * 获取某月生成会员报表文件
	 * @param sortBy
	 * @param isAscending
	 * @param status
	 * @param expiry
	 * @param planName
	 * @param memberType
	 * @param month
	 * @param filters
	 * @param fileType
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getMemberListByBirthdayReports", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getMemberListByBirthdayReports(
			@RequestParam(value = "sortBy", defaultValue = "memberName") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam(value = "status", defaultValue = "ALL", required = true) String status,
			@RequestParam(value = "expiry", defaultValue = "ALL", required = true) String expiry,
			@RequestParam(value = "planName", defaultValue = "", required = false) String planName,
			@RequestParam(value = "memberType", defaultValue = "", required = false) String memberType,
			@RequestParam(value = "month", defaultValue = "", required = true) Integer month,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters, 
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType, HttpServletRequest request,
			HttpServletResponse response) {

		try {

			if (!StringUtils.isEmpty(filters)) {
				AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0
						&& advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
					for (SearchRuleDto rule : advanceQueryDto.rules) {
						if (logger.isDebugEnabled()) {
							logger.debug("rule field:" + rule.getField() + " op:" + rule.getOp() + " data:"
									+ rule.getData());
						}
						if ("t.status".equals(rule.field) && "ACTIVE".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_ACT);
						} else if ("t.status".equals(rule.field) && "INACTIVE".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_NACT);
						} else if ("t.status".equals(rule.field) && "EXPIRED".equals(rule.data.toUpperCase())) {
							rule.setData(Constant.General_Status_EXP);
						}
						if ("t.planName".equals(rule.field) && (rule.data.indexOf("'") > 0)) {
							rule.setData(rule.data.replace("'", "\\'"));
						}
					}
					page.setCondition(advanceQueryService.getSearchCondition(advanceQueryDto, ""));
				}
			}
			handleExpirySession(request, response);
			chooseFileType(request, response, "Patron Birthday Reports(" + month +" Month)", fileType);

			return memberTransactionService.getMembersOverviewListByBirthdayReports(sortBy, isAscending, "", status, expiry, planName, memberType, month, fileType);
		} catch (Exception e) {
			 e.printStackTrace();
			 logger.error(e.getMessage(), e);
		}
		return null;
	}
	@RequestMapping(value = "/transaction/spendingAmountPatron", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpendingAmountPatron(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "paymentMethod", defaultValue = "All", required = false) String paymentMethod,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending
	 ) {
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getSpendingAmountPatron(page, startDate, endDate,servicePlanName,paymentMethod);
		return responseResult;
	}
	@RequestMapping(value = "/transaction/spendingAmountPatronReports", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getSpendingAmountPatronReports(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "paymentMethod", defaultValue = "All", required = false) String paymentMethod,
			@RequestParam(value = "sortBy", defaultValue = "amount", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Total spending amount by Cash Value of Patron(" + startDate+"-"+endDate +")", fileType);
			return customerOrderTransService.getSpendingAmountPatronReport(sortBy,isAscending,startDate, endDate, servicePlanName, paymentMethod, fileType);
		} catch (Exception e) {
			 e.printStackTrace();
			 logger.error(e.getMessage(), e);
		}
		return null;
	}
	@RequestMapping(value = "/transaction/getTopUpAmountPatron", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTopUpAmountPatron(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "amount", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending
	 ) {
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getTopUpAmountPatron(page, startDate, endDate,servicePlanName);
		return responseResult;
	}
	@RequestMapping(value = "/transaction/getTopUpAmountPatronReport", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getTopUpAmountPatronReport(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "sortBy", defaultValue = "amount", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType, HttpServletRequest request,
			HttpServletResponse response
	 ) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Total top-up amount of Patron(" + startDate+"-"+endDate +")", fileType);
			return customerOrderTransService.getTopUpAmountPatronReport(sortBy, isAscending, startDate, endDate, servicePlanName, fileType);
		} catch (Exception e) {
			 e.printStackTrace();
			 logger.error(e.getMessage(), e);
		}
		return null;
	}
	@RequestMapping(value = "/transaction/getTopPatronSpendingFacilities", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTopPatronSpendingFacilities(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "facilityType", defaultValue = "All", required = false) String facilityType,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "amount", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending
	 ) {
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getTopPatronSpendingFacilities(page, startDate, endDate, servicePlanName, facilityType);
		return responseResult;
	}
	@RequestMapping(value = "/transaction/getTopPatronSpendingFacilitiesReport", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getTopPatronSpendingFacilitiesReport(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "facilityType", defaultValue = "All", required = false) String facilityType,
			@RequestParam(value = "sortBy", defaultValue = "amount", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Top 100 Patron spending on all facilities(" + startDate+"-"+endDate +")", fileType);
			return customerOrderTransService.getTopPatronSpendingFacilitiesReport(sortBy, isAscending, startDate, endDate, servicePlanName, facilityType, fileType);
		} catch (Exception e) {
			 e.printStackTrace();
			 logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	//Usage rate of Patron by different facilities
	@RequestMapping(value = "/transaction/getUsageRatePatronDiffFacilities", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getUsageRatePatronDiffFacilities(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "facilityType", defaultValue = "All", required = false) String facilityType,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "number", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending
	 ) {
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getUsageRatePatronDiffFacilities(page, startDate, endDate, servicePlanName, facilityType,sortBy,isAscending);
		return responseResult;
	}
	@RequestMapping(value = "/transaction/getUsageRatePatronDiffFacilitiesReport", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getUsageRatePatronDiffFacilitiesReport(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "facilityType", defaultValue = "All", required = false) String facilityType,
			@RequestParam(value = "sortBy", defaultValue = "number", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Usage rate of Patron by different facilities(" + startDate+"-"+endDate +")", fileType);
			return customerOrderTransService.getUsageRatePatronDiffFacilitiesReport(sortBy, isAscending, startDate, endDate, servicePlanName, facilityType, fileType);
		} catch (Exception e) {
			 e.printStackTrace();
			 logger.error(e.getMessage(), e);
		}
		return null;
	}
	@RequestMapping(value = "/transaction/getSpendingAmountPatronDiffFacilities", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpendingAmountPatronDiffFacilities(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "amount", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending
	 ) {
		page = new ListPage();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setNumber(pageNumber);
		page.setSize(pageSize);
		ResponseResult responseResult = customerOrderTransService.getSpendingAmountPatronDiffFacilities(page, startDate, endDate, servicePlanName);
		return responseResult;
	}
	@RequestMapping(value = "/transaction/getSpendingAmountPatronDiffFacilitiesReport", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getSpendingAmountPatronDiffFacilitiesReport(
			@RequestParam(value = "startDate", defaultValue = "", required = false) String startDate,
			@RequestParam(value = "endDate", defaultValue = "", required = false) String endDate,
			@RequestParam(value = "servicePlanName", defaultValue = "All", required = false) String servicePlanName,
			@RequestParam(value = "sortBy", defaultValue = "amount", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false)String isAscending,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = true) String fileType, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			handleExpirySession(request, response);
			chooseFileType(request, response, "Spending Amount of Patron by different facilities(" + startDate+"-"+endDate +")", fileType);
			return customerOrderTransService.getSpendingAmountPatronDiffFacilitiesReport(sortBy, isAscending, startDate, endDate, servicePlanName,fileType);
		} catch (Exception e) {
			 e.printStackTrace();
			 logger.error(e.getMessage(), e);
		}
		return null;
	}
	/***
	 * SGG-3857
	 * @param amount
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @return
	 */
	@RequestMapping(value = "/transaction/getHighBalanceReportList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getHighBalanceReport(
			@RequestParam(value = "amount", defaultValue = "", required = false) BigDecimal amount,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending
	 ) {
		page = new ListPage<MemberCashvalueDto>();
		if (isAscending.equals("true")) {
			page.addAscending(sortBy);
		} else if (isAscending.equals("false")) {
			page.addDescending(sortBy);
		}
		page.setSize(pageSize);
		page.setNumber(pageNumber);
		ResponseResult responseResult = memberCashvalueService.getHighBalanceReport(page, amount);
		return responseResult;
	}
	
	/***
	 * SGG-3857
	 * @param amount
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @return
	 */
	@RequestMapping(value = "/transaction/getHighBalanceReportListAttachement", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getHighBalanceReportReport(
			@RequestParam(value = "amount", defaultValue = "", required = false) BigDecimal amount,
			@RequestParam(value = "fileType", defaultValue = "pdf", required = false) String fileType,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		
		handleExpirySession(request, response);
		chooseFileType(request, response, "HighBalanceReport", fileType);
		byte[] responseResult = memberCashvalueService.getHighBalanceReport(sortBy, isAscending, amount, fileType);
		return responseResult;
	}
	
}
