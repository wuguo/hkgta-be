package com.sinodynamic.hkgta.controller.fms;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.http.client.utils.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.dto.fms.CourseEnrollmentDto;
import com.sinodynamic.hkgta.dto.fms.CourseInfoDto;
import com.sinodynamic.hkgta.dto.fms.CourseListDto;
import com.sinodynamic.hkgta.dto.fms.CourseRelatedDto;
import com.sinodynamic.hkgta.dto.fms.CourseSessionDto;
import com.sinodynamic.hkgta.dto.fms.MemberCashvalueDto;
import com.sinodynamic.hkgta.dto.fms.MemberInfoDto;
import com.sinodynamic.hkgta.dto.fms.NewCourseDto;
import com.sinodynamic.hkgta.entity.fms.CourseEnrollment;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.common.ShortMessageService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.service.fms.CourseEnrollmentService;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.service.fms.CourseSessionCustomizeException;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.RepeatMode;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.PaymentMediaType;
import com.sinodynamic.hkgta.util.constant.UserPreferenceSettingParam;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping(value = "/course")
@Scope("prototype")
public class CourseController extends ControllerBase {

	private Logger logger = Logger.getLogger(CourseController.class);

	private static final int NINE_DIGIT = 9;

	private static final int SEVEN_DIGIT = 7;

	@Autowired
	private CourseService courseService;

	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private CourseEnrollmentService courseEnrollmentService;

	@Autowired
	private ShortMessageService smsService;

	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	
	@Autowired
	private CustomerOrderTransService customerOrderTransService;

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ResponseResult createCourse(@RequestBody NewCourseDto dto) {

		try {
			String userId = super.getUser().getUserId();
			dto.setCreateBy(userId);
			dto.setUpdateBy(userId);
			return courseService.createCourse(dto);

		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " createCourse Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_CREATE_NEW_COURSE);
			return responseResult;
		}
	}

	ResponseMsg _createCourseSession(CourseRelatedDto paramDto) {

		logger.info("CourseController.createCourseSession invocation start...");
		logger.info("parameter: " + paramDto.toString());

		LoginUser user = getUser();
		String coachNo = paramDto.getCoachNo();
		String editType = paramDto.getEditType();
		String[] facilityNo = paramDto.getFacilityNo();
		CourseSessionDto sessionDto = paramDto.getSessionDto();

		if (user == null) {
			responseResult.initResult(GTAError.CourseError.No_CURRENT_LOGIN_USER);
			return responseResult;
		}

		if (CommUtil.nvl(coachNo).length() == 0 || sessionDto == null) {

			responseResult.initResult(GTAError.CourseError.COURSE_SESSION_ERROR_PARAMETER);
			return responseResult;
		}

		try {
			LoginUser currentUser = getUser();
			ResponseResult rs = courseService.createCourseSession(coachNo, facilityNo, sessionDto,
					currentUser.getUserId());
			if (rs.getDto() != null && !"New".equals(editType)) {
				Map<String, String> appMsg = (Map<String, String>) rs.getDto();
				String message = appMsg.get("message");
				String coachUserId = appMsg.get("coachUserId");

				if (logger.isDebugEnabled()) {
					logger.debug("message:" + message);
					logger.debug("coachUserId:" + coachNo);
				}
				String[] userIds = coachUserId.split(",");
				devicePushService.pushMessage(userIds, message, "trainer");
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			} else {
				return rs;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseError.UNKNOWN_ERROR_HAPPENED);
			return responseResult;
		}

	}

	@RequestMapping(value = "/session", method = RequestMethod.POST)
	public @ResponseBody ResponseMsg createCourseSession(@RequestBody CourseRelatedDto paramDto) {
		List<String[]> periods = getRepeatPeriod(RepeatMode.valueOf(paramDto.getRepeatMode()),
				paramDto.getRepeatCount(), paramDto.getSessionDto().getBeginDatetime(),
				paramDto.getSessionDto().getEndDatetime(), "yyyy-MM-dd HH:mm:ss");
		List<ResponseMsg> errors = new ArrayList<ResponseMsg>();

		for (String[] period : periods) {
			paramDto.getSessionDto().setBeginDatetime(period[0]);
			paramDto.getSessionDto().setEndDatetime(period[1]);
			ResponseMsg rs = _createCourseSession(paramDto);

			if (!StringUtils.equalsIgnoreCase(rs.getReturnCode(), GTAError.Success.SUCCESS.getCode())) {
				errors.add(rs);
			}
		}
		if (errors.size() == periods.size()) {
//			responseResult.initResult(GTAError.CourseError.UNKNOWN_ERROR_HAPPENED);
			responseResult = new ResponseResult(errors.get(0).returnCode, errors.get(0).errorMessageEN);
		} else if (errors.size() > 0) {
			responseResult.initResult(GTAError.CourseError.COURSE_SESSION_BATCH_CREATION_FAIL);
		} else {
			responseResult.initResult(GTAError.Success.SUCCESS);
		}

		return responseResult;
	}

	@RequestMapping(value = "/session", method = RequestMethod.PUT)
	@SuppressWarnings("unchecked")
	public @ResponseBody ResponseMsg modifyCourseSession(@RequestBody CourseRelatedDto paramDto) {
		logger.info("CourseController.modifyCourseSession invocation start...");
		logger.info("parameter: " + paramDto.toString());

		LoginUser user = getUser();
		String coachNo = paramDto.getCoachNo();
		String editType = paramDto.getEditType();
		String[] facilityNo = paramDto.getFacilityNo();
		CourseSessionDto sessionDto = paramDto.getSessionDto();
		Long sysId = null;
		if (sessionDto != null)
			sysId = sessionDto.getSysId();

		if (user == null) {
			responseResult.initResult(GTAError.CourseError.No_CURRENT_LOGIN_USER);
			return responseResult;
		}

		if (CommUtil.nvl(coachNo).length() == 0 || sysId == null) {

			responseResult.initResult(GTAError.CourseError.COURSE_SESSION_ERROR_PARAMETER);
			return responseResult;
		}

		try {

			ResponseResult rs = courseService.modifyCourseSession(coachNo, facilityNo, sessionDto, user.getUserId());
//			if ("0".equals(rs.getReturnCode())) {
//				Map<String, Object> smsInfo = (Map<String, Object>) rs.getDto();
//				if (smsInfo != null) {
//					if (logger.isDebugEnabled()) {
//						logger.debug("start send message to course members:");
//					}
//					List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
//					String message = (String) smsInfo.get("message");
//					if (logger.isDebugEnabled()) {
//						if (phoneNumbers != null && phoneNumbers.size() > 0) {
//							for (String no : phoneNumbers) {
//								logger.debug(no + ",");
//							}
//						}
//
//						logger.debug("message:" + message);
//					}
//
//					if (phoneNumbers != null && phoneNumbers.size() > 0 && !StringUtils.isEmpty(message)) {
//						smsService.sendSMS(phoneNumbers, message, new Date());
//					}
//					courseService.sendPushTask(smsInfo);
//				}
//			}
			rs.setData((Object) null);
			return rs;

		} catch (CourseSessionCustomizeException e) {

			ResponseResult result = e.getResponseResult();
			logger.error(result.getDto());
			return result;

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseError.UNKNOWN_ERROR_HAPPENED);
			return responseResult;
		}

	}

	@RequestMapping(value = "/session/{sysId}/{type}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult searchCourseSession(@PathVariable(value = "sysId") String sysId,
			@PathVariable(value = "type") String type) {

		logger.info("CourseController.searchCourseSession invocation start...");
		logger.info("parameter: sysId[" + sysId + "], type[" + type + "]");
		if (CommUtil.nvl(sysId).length() == 0 || CommUtil.nvl(type).length() == 0) {
			responseResult.initResult(GTAError.CourseError.ERROR_SEARCHING_PARAM);
			return responseResult;
		}

		try {

			ResponseResult rs = courseService.getCourseSessionInfo(Long.parseLong(sysId), type);
			return rs;

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseError.UNKNOWN_ERROR_HAPPENED);
			return responseResult;
		}
	}

	String getGMTTimeString(Date date, String dateFormat) {
		return DateFormatUtils.format(date, dateFormat, TimeZone.getTimeZone("GMT"));
	}

	List<String[]> getRepeatPeriod(RepeatMode repeatMode, int repeatCount, String beginDateTimeString,
			String endDateTimeString, String dateFormat) {
		Date beginDate = DateUtils.parseDate(beginDateTimeString, new String[] { dateFormat });
		Date endDate = DateUtils.parseDate(endDateTimeString, new String[] { dateFormat });

		DateTime jodaBeginDate = new DateTime(beginDate);
		DateTime jodaEndDate = new DateTime(endDate);

		List<String[]> period = new ArrayList<String[]>();
		switch (repeatMode) {
		case ONCE:
			period.add(new String[] { beginDateTimeString, endDateTimeString });
			return period;
		case DAILY:
			for (int i = 0; i < repeatCount; i++) {
				Date tempBegin = jodaBeginDate.dayOfYear().addToCopy(i).toLocalDateTime().toDate();
				Date tempEnd = jodaEndDate.dayOfYear().addToCopy(i).toLocalDateTime().toDate();

				period.add(new String[] { getGMTTimeString(tempBegin, dateFormat),
						getGMTTimeString(tempEnd, dateFormat) });
			}
			return period;
		case WEEKLY:
			for (int i = 0; i < repeatCount; i++) {
				Date tempBegin = jodaBeginDate.weekOfWeekyear().addToCopy(i).toLocalDateTime().toDate();
				Date tempEnd = jodaEndDate.weekOfWeekyear().addToCopy(i).toLocalDateTime().toDate();

				period.add(new String[] { getGMTTimeString(tempBegin, dateFormat),
						getGMTTimeString(tempEnd, dateFormat) });
			}
			return period;
		case MONTHLY:
			for (int i = 0; i < repeatCount; i++) {
				Date tempBegin = jodaBeginDate.monthOfYear().addToCopy(i).toLocalDateTime().toDate();
				Date tempEnd = jodaEndDate.monthOfYear().addToCopy(i).toLocalDateTime().toDate();

				period.add(new String[] { getGMTTimeString(tempBegin, dateFormat),
						getGMTTimeString(tempEnd, dateFormat) });
			}
			return period;
		default:
			return period;
		}
	}

	List<String[]> getRepeatPeriod(RepeatMode repeatMode, int repeatCount, String beginDateTimeString,
			String endDateTimeString) {
		return getRepeatPeriod(repeatMode, repeatCount, beginDateTimeString, endDateTimeString, "yyyy-MM-dd HH:mm");
	}

	@RequestMapping(value = "/coach/list/available", method = RequestMethod.GET)
	public @ResponseBody ResponseResult searchAvailableCoachList(@RequestParam(value = "type") String type,
			@RequestParam(value = "coachTimeslotId", required = false) String coachTimeslotId,
			@RequestParam(value = "beginDatetime") String beginDatetime,
			@RequestParam(value = "endDatetime") String endDatetime,
			@RequestParam(value = "repeatMode") RepeatMode repeatMode,
			@RequestParam(value = "repeatCount") int repeatCount) {

		logger.info("CourseController.searchAvailableCoachList invocation start...");

		try {
			if (beginDatetime.endsWith(":30")) {
				beginDatetime = DateCalcUtil.getLastHalfHourStr(beginDatetime);
			}
			if (endDatetime.endsWith(":30")) {
				endDatetime = DateCalcUtil.getNextHalfHourStr(endDatetime);
			}
			responseResult.initResult(GTAError.Success.SUCCESS, courseService.getAvailableCoachList(type,
					getRepeatPeriod(repeatMode, repeatCount, beginDatetime, endDatetime), coachTimeslotId));
			return responseResult;

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseError.UNKNOWN_ERROR_HAPPENED);
			return responseResult;
		}

	}

	@RequestMapping(value = "/facility/list/available", method = RequestMethod.GET)
	public @ResponseBody ResponseResult searchAvailableFacilityList(@RequestParam(value = "type") String type,
			@RequestParam(value = "beginDatetime") String beginDatetime,
			@RequestParam(value = "endDatetime") String endDatetime,
			@RequestParam(value = "repeatMode") RepeatMode repeatMode,
			@RequestParam(value = "repeatCount") int repeatCount,
			@RequestParam(value = "sessionId",required = false) Long sessionId) {

		logger.info("CourseController.searchAvailableFacilityList invocation start...");

		ResponseResult rs = null;
		try {
			if (beginDatetime.endsWith(":30")) {
				beginDatetime = DateCalcUtil.getLastHalfHourStr(beginDatetime);
			}
			if (endDatetime.endsWith(":30")) {
				endDatetime = DateCalcUtil.getNextHalfHourStr(endDatetime);
			}
			responseResult.initResult(GTAError.Success.SUCCESS, courseService.getAvailableFacilities(type,
					getRepeatPeriod(repeatMode, repeatCount, beginDatetime, endDatetime),sessionId));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseError.UNKNOWN_ERROR_HAPPENED);
			rs = responseResult;
		}

		return rs;
	}

	/**
	 * This method is used to retrieve the details of a specific Course.
	 * 
	 * @param courseId
	 * @return ResponseResult (Course Info)
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/{courseId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseInfo(@PathVariable(value = "courseId") String courseId,
			@RequestParam(value = "device", required = false) String device,
			@RequestParam(value = "customerId", required = false) @EncryptFieldInfo Long customerId) {

		try {
			return courseService.getCourseInfo(courseId, device, null!=customerId?String.valueOf(customerId):null);

		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " getCourseDetails Failed!", e);
			responseResult.initResult(GTAError.CourseError.GET_COURSE_DETAILS_FAIL);
			return responseResult;

		}

	}

	/**
	 * This method is used to retrieve list of members for a specific Course.
	 * 
	 * @param courseId
	 * @param status
	 * @param sortBy
	 * @param isAscending
	 * @param pageNumber
	 * @param pageSize
	 * @return ResponseResult (Member Info)
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/members/{courseId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMemberInfo(@PathVariable(value = "courseId") String courseId,
			@RequestParam(value = "attendance", required = false) String attendance,
			@RequestParam(value = "status", defaultValue = "ALL") String status,
			@RequestParam(value = "sortBy", defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "3") int pageSize,
			@RequestParam(value = "sessionId", required = false) String sessionId) {

		ListPage<CourseEnrollment> page = new ListPage<CourseEnrollment>();
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		if (isAscending.equals("false")) {
			page.addDescending(sortBy);

		} else if (isAscending.equals("true")) {
			page.addAscending(sortBy);

		}
		try {

			return courseService.getMemberInfo(page, courseId, attendance, status, sessionId);

		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " getMemberInfo Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_MEMBER_LIST);
			return responseResult;

		}

	}

	/**
	 * change course session status
	 * 
	 * @param sysId
	 * @param status
	 * @return
	 * @author
	 * @date Sep 29, 2015
	 */
	@RequestMapping(value = "/{courseId}/{status}", method = RequestMethod.PUT)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseResult changeCourseSessionStatus(@PathVariable(value = "courseId") String sysId,
			@PathVariable(value = "status") String status, @RequestParam(value = "editType") String editType) {

		if (StringUtils.isEmpty(sysId)) {
			responseResult.initResult(GTAError.CourseError.COURSE_SESSION_ID_ISNULL);
			return responseResult;
		}
		if (StringUtils.isEmpty(status)) {
			responseResult.initResult(GTAError.CourseError.COURSE_SESSION_STATUS_ISNULL);
			return responseResult;
		}
		if (!(status.toUpperCase().equals("ACT") || status.toUpperCase().equals("CAN")
				|| status.toUpperCase().equals("DEL"))) {
			responseResult.initResult(GTAError.CourseError.COURSE_SESSION_STATUS_ERROR);
			return responseResult;
		}
		if (StringUtils.isEmpty(editType)) {
			editType = "Edit";
		}

		try {
			String userId = super.getUser().getUserId();
			ResponseResult rs = courseService.changeCourseSessionStatus(sysId, status, userId);
			if ("0".equals(rs.getReturnCode())) {
				Map<String, Object> smsInfo = (Map<String, Object>) rs.getDto();
				if (smsInfo != null) {
					List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
					String message = (String) smsInfo.get("message");
					if (phoneNumbers != null && phoneNumbers.size() > 0 && !StringUtils.isEmpty(message)) {
						smsService.sendSMS(phoneNumbers, message, new Date());
					}

					// added by vicky wang 2015-11-03, send the reminder message
					// to coach if course session changed.
					String[] coachUserId = smsInfo.get("coachUserId").toString().split(",");
					String coachMessage = (String) smsInfo.get("coachMessage");
					if (coachUserId != null && coachUserId.length > 0) {
						if (logger.isDebugEnabled()) {
							logger.debug("start push message to coach for course session status change:");
							logger.debug("coachUserId:" + coachUserId[0]);
							logger.debug("message:" + coachMessage);
						}
						if (!"New".equals(editType)) {
							devicePushService.pushMessage(coachUserId, coachMessage, "trainer");
						}
					}
				}
			}

			return rs;
		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " changeCourseSessionStatus!", e);
			responseResult.initResult(GTAError.CourseError.COURSE_SESSION_STATUS_CHANGEDFAILD);
			return responseResult;
		}
	}

	/**
	 * This method is to update Course information
	 * 
	 * @param NewCourseDto
	 * @return ResponseResult
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseResult updateCourse(@RequestBody NewCourseDto dto) {
		LoginUser user = super.getUser();
		if (user == null) {

			logger.error("Current User is null!");
			responseResult.initResult(GTAError.TempPassError.CURRENT_USER_NULL);
			return responseResult;
		}

		String userId = user.getUserId();
		dto.setUpdateBy(userId);
		dto.setUpdateDate(new Date());

		try {
			ResponseResult rs = courseService.updateCourse(dto);
			if ("0".equals(rs.getReturnCode())) {
				Map<String, Object> smsInfo = (Map<String, Object>) rs.getDto();

				if (smsInfo != null) {
					if (dto.isSendSms()) {
						List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
						String message = (String) smsInfo.get("message");
						if (phoneNumbers != null && phoneNumbers.size() > 0 && !StringUtils.isEmpty(message)) {
							smsService.sendSMS(phoneNumbers, message, new Date());
						}
					}
					// added by vicky wang to push app message to coach for a
					// new course.
					// push app message for coach.
					Set<String> coachUserList = (Set<String>) smsInfo.get("coachUserList");
					String coachMessage = smsInfo.get("coachMessage") != null ? smsInfo.get("coachMessage").toString()
							: "";

					if (logger.isDebugEnabled()) {
						logger.debug("user:" + coachUserList);
						logger.debug("message:" + coachMessage);
					}
					if (coachUserList != null && coachUserList.size() > 0) {
						String[] coachArray = coachUserList.toArray(new String[coachUserList.size()]);
						devicePushService.pushMessage(coachArray, coachMessage, "trainer");
					}

					// added end.
				}

			}
			return rs;

		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " updateCourse Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_TO_UPDATE_COURSE_INFO);

			return responseResult;
		}

	}

	/**
	 * change course status
	 * 
	 * @param dto
	 * @return
	 * @author
	 * @date Sep 29, 2015
	 */
	@RequestMapping(value = "/change/status", method = RequestMethod.POST)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseResult doChangeCourseStatus(@RequestBody CourseInfoDto dto) {
		/*
		 * UPDATE course_master set status='' where course_id=''
		 */
		logger.info("CourseController.doChangeCourseStatus invocation start...");
		try {
			String userId = super.getUser().getUserId();
			ResponseResult rs = courseService.changeCourseStatus(dto, userId);
			if ("0".equals(rs.getReturnCode())) {
				Map<String, Object> smsInfo = (Map<String, Object>) rs.getDto();
				if (smsInfo != null) {
					List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
					String message = (String) smsInfo.get("message");
					smsService.sendSMS(phoneNumbers, message, new Date());

					// added by vicky wang to push message to coach when course
					// cancelled.
					Set<String> coachList = (Set<String>) smsInfo.get("coachList");

					if (coachList != null && coachList.size() > 0) {
						String[] coachArray = coachList.toArray(new String[coachList.size()]);
						String coachMessage = (String) smsInfo.get("coachMessage");
						if (logger.isDebugEnabled()) {
							for (String coachId : coachArray) {
								logger.debug("coachUser to push message:" + coachId);
							}
							logger.debug("message for coach:" + coachMessage);
						}

						devicePushService.pushMessage(coachArray, coachMessage, "trainer");
					}
					// added by vicky end
				}
			}
			return rs;
		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " doChangeCourseStatus!", e);
			responseResult.initResult(GTAError.CourseError.COURSE_CHANGESTATUS_ERROR);
			return responseResult;
		}
	}

	/**
	 * get member info and course info when paying
	 * 
	 * @param customerId
	 * @param courseId
	 * @return
	 * @author Miranda_Zhang
	 * @date Sep 29, 2015
	 */
	@RequestMapping(value = "/cam/info", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseAndMemberInfo(@RequestParam(value = "customerId") @EncryptFieldInfo Long customerId,
			@RequestParam(value = "courseId") String courseId) {

		logger.info("CourseController.getCourseAndMemberInfo invocation start ...");
		logger.info("parameter : customerId[" + customerId + "], courseId[" + courseId + "]");

		try {

			return courseService.getCourseAndMemberInfo(customerId.toString(), courseId);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CourseError.UNKNOWN_ERROR_HAPPENED);
			return responseResult;
		}

	}
	@RequestMapping(value = "/cam/memberinfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseAndMemberInfo(@RequestParam(value = "customerId") @EncryptFieldInfo Long customerId) {
		MemberCashvalueDto dto = courseService.getMemberInfo(customerId.toString());
		this.responseResult.initResult(GTAError.Success.SUCCESS, dto);
		return this.responseResult;
	}

	/**
	 * 
	 * enrolmentCourse:This method is used to enroll the course for member.
	 * <br/>
	 * 
	 * @author Nick_Xiong Create Date: Jul 13, 2015 1:37:00 PM <br/>
	 *         Update History: Update By Update Date Update Reason
	 * 
	 * @param courseId
	 * @param customerId
	 * @param remark
	 * @param isPay
	 * @return
	 * @author Nick Xiong
	 */
	@RequestMapping(value = "/enroll", method = RequestMethod.POST)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseResult enrolmentCourse(HttpServletRequest request, @RequestBody CourseEnrollmentDto courseEnrollmentDto) {

		// add payment location info from session
		courseEnrollmentDto.setPaymentLocation(getLoginLocation());
		courseEnrollmentDto.setTerminalType(request.getHeader("terminalType"));
		try {
			boolean sendSms=super.sendSMS((null!=courseEnrollmentDto)?courseEnrollmentDto.getCustomerId():null);
			
			ResponseResult rs = null;
			if (courseService.isRepayCourse(courseEnrollmentDto)) {
				rs = courseService.repaymentCourse(courseEnrollmentDto, getUser().getUserId());
				if ("0".equals(rs.getReturnCode())) {
					Map<String, Object> map = (Map<String, Object>) rs.getDto();
					if (map != null && map.get("smsInfo") != null) {
						Map<String, Object> smsInfo = (Map<String, Object>) map.get("smsInfo");
						List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
						String message = (String) smsInfo.get("message");
						if(sendSms){
							smsService.sendSMS(phoneNumbers, message, new Date());
						}
						map.remove("smsInfo");
					}
				}

			} else {
				rs = courseService.enrollCourseFinal(courseEnrollmentDto, getUser().getUserId());
				if ("0".equals(rs.getReturnCode())) {
					Map<String, Object> map = (Map<String, Object>) rs.getDto();
					if (map != null && map.get("smsInfo") != null) {
						Map<String, Object> smsInfo = (Map<String, Object>) map.get("smsInfo");
						List<String> phoneNumbers = (List<String>) smsInfo.get("phonenumbers");
						String message = (String) smsInfo.get("message");
						if(sendSms){
							smsService.sendSMS(phoneNumbers, message, new Date());
						}
						map.remove("smsInfo");
					}
				}
			}

			return rs;

		} catch (GTACommonException e) {

			logger.error(e.toString());
			GTAError error = e.getError();
			if (error != null) {
				responseResult.initResult(error);
			} else {
				responseResult.initResult(GTAError.CourseError.ENROLL_COURSE_FAIL);
			}

			return responseResult;

		} catch (Exception e) {

			logger.error(CourseController.class.getName() + " enroll course Failed!", e);
			responseResult.initResult(GTAError.CourseError.ENROLL_COURSE_FAIL);
			return responseResult;
		}

	}
	@RequestMapping(value = "/retry", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult retryTransaction(HttpServletRequest request,@RequestBody CourseEnrollmentDto courseEnrollmentDto) {
		logger.info("CourseEnrollController.retryTransaction start ...");
		try {
				courseEnrollmentDto.setTerminalType(request.getHeader("terminalType"));
				
				Map<String, Object> resultMap = new HashMap<String, Object>();
				CustomerOrderHd customerOrderHd = customerOrderTransService
						.getCustomerOrderHdByOrderNo(courseEnrollmentDto.getOrderNo());
				CustomerOrderTrans customerOrderTrans = new CustomerOrderTrans();
				customerOrderTrans.setCustomerOrderHd(customerOrderHd);
				customerOrderTrans.setPaidAmount(courseEnrollmentDto.getTotalPrice());
				customerOrderTrans.setPaymentMethodCode(courseEnrollmentDto.getPaymentMethod());
				customerOrderTrans.setPaymentRecvBy(this.getUser().getUserName());
				customerOrderTrans.setStatus(Constant.Status.PND.name());
				customerOrderTrans.setTransactionTimestamp(new Timestamp(new Date().getTime()));
				if (StringUtils.isEmpty(courseEnrollmentDto.getPaymentMethod()) || courseEnrollmentDto.getPaymentMethod().equals(Constant.CASH)) {
					customerOrderTrans.setPaymentMedia(PaymentMediaType.NA.name());
				} else if (courseEnrollmentDto.getPaymentMethod().equals(Constant.CASH_Value)) {
					customerOrderTrans.setPaymentMedia(PaymentMediaType.OTH.name());
				} else if (courseEnrollmentDto.getPaymentMethod().equals(Constant.CREDIT_CARD)||
						courseEnrollmentDto.getPaymentMethod().equals(Constant.UNION_PAY)) {
					if(StringUtils.isNotBlank(courseEnrollmentDto.getTerminalType())&&
							PaymentMediaType.ECR.name().equalsIgnoreCase(courseEnrollmentDto.getTerminalType())){
						customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
					}else{
						customerOrderTrans.setPaymentMedia(PaymentMediaType.OP.name());
					}
					customerOrderTrans.setPaymentMedia(PaymentMediaType.ECR.name());
				}
				customerOrderTransService.addNewTransactionRecord(customerOrderTrans);
				resultMap.put("transactionNo", customerOrderTrans.getTransactionNo());
				resultMap.put("orderNo", customerOrderTrans.getCustomerOrderHd().getOrderNo());
				logger.info("CourseEnrollController.retryTransaction invocation end...");
				responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
				return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	/**
	 * This method is to update the status of member attendance
	 * 
	 * @return ResponseResultё
	 * @author Vineela_Jyothi Date: Jul 15, 2015
	 * @param sessionId
	 * @param enrollId
	 * @param attendId
	 * @param status
	 * @param qrCode
	 *            member card no
	 */
	@RequestMapping(value = "/member/attendance/status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult changeMemberAttendanceStatus(@RequestParam(value = "sessionId") String sessionId,
			@RequestParam(value = "enrollId", required = false, defaultValue = "") String enrollId,
			@RequestParam(value = "attendId", required = false, defaultValue = "") String attendId,
			@RequestParam(value = "status") String status,
			@RequestParam(value = "qrCode", required = false, defaultValue = "") String qrCode) {

		LoginUser user = super.getUser();
		if (user == null) {

			logger.error("Current User is null!");
			responseResult.initResult(GTAError.TempPassError.CURRENT_USER_NULL);
			return responseResult;
		}

		String userId = user.getUserId();

		try {

			// modified by Kaster 20160302
			// 当QRCODE为空时，说明是从member app to Not Attending(NATD)/Pending for roll
			// call(NULL), default for NULL
			// 当QRCODE不为空时，说明是从coach app Roll Call，status为“ATD”。
			if (qrCode != null && !qrCode.equals("")) {
				String cardNo = null;
				String academyNo = null;
				if (qrCode.length() == NINE_DIGIT) {
					cardNo = CommUtil.cardNoTransfer(qrCode);
				} else if (qrCode.length() == SEVEN_DIGIT) {
					academyNo = qrCode;
				}

				if (!StringUtils.isEmpty(cardNo)) {
					enrollId = getEnrollIdBySessionAndCard(cardNo, sessionId);
					if (StringUtils.isEmpty(enrollId)) {
						responseResult.initResult(GTAError.CourseError.COURSE_SESSION_BAD_QRCODE);
						return responseResult;
					}
				} else if (!StringUtils.isEmpty(academyNo)) {
					enrollId = getEnrollIdBySessionAndMember(academyNo, sessionId);
					if (StringUtils.isEmpty(enrollId)) {
						responseResult.initResult(GTAError.CourseError.COURSE_SESSION_BAD_ACADEMY_NO);
						return responseResult;
					}
				}
				return courseService.changeMemberAttendanceStatus(sessionId, enrollId, attendId, "ATD", userId);
			} else {
				if (status != null && status.equalsIgnoreCase("DELETE")) {
					// 缺省是会员想参加课程，如果not
					// attending之后又想attend了，那么就把student_course_attendance中的记录删除。如果当前为pending
					// for roll call，那么会员可以再次not attending。
					courseService.deleteAttendingRecord(attendId);
					responseResult.initResult(GTAError.Success.SUCCESS);
					return responseResult;
				}
				return courseService.changeMemberAttendanceStatus(sessionId, enrollId, attendId, status, userId);
			}

		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " changeMemberAttendanceStatus Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_UPDATE_MEMBER_ATTENDANCE_STATUS);

			return responseResult;
		}

	}

	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Sep 1, 2015
	 * @Param @param qrCode
	 * @Param @param enrollId
	 * @Param @return
	 * @return ResponseResult
	 */
	private String getEnrollIdBySessionAndCard(String cardNo, String sessionId) {

		return courseEnrollmentService.getEnrollIdBySessionIdAndAcademyCardNo(sessionId, cardNo);
	}

	/**
	 * 
	 * @Author Mianping_Wu
	 * @Date Oct 17, 2015
	 * @Param @param academyNO
	 * @Param @param sessionId
	 * @Param @return
	 * @return String
	 */
	private String getEnrollIdBySessionAndMember(String academyNO, String sessionId) {

		return courseEnrollmentService.getEnrollIdBySessionIdAndAcademyNo(sessionId, academyNO);
	}

	/**
	 * This method is used to retrieve list of available Courses.
	 * 
	 * @param courseType
	 * @param expired
	 * @param status
	 * @param filters
	 * @param customerId
	 * @param soryBy
	 * @param isAscending
	 * @param pageNumber
	 * @param pageSize
	 * @return ResponseResult (list of courses, pagination details etc)
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/list", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult getAdvCourseList(@RequestParam(value = "courseType") String courseType,
			@RequestParam(value = "expired") String expired,
			@RequestParam(value = "status", defaultValue = "ALL") String status,
			@RequestBody(required = false) AdvanceQueryDto filters,
			@RequestParam(value = "customerId", required = false) String customerId,
			@RequestParam(value = "sortBy", defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {

		logger.info("CourseController.getAdvCourseList start ...");

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try {
			String joinHQL = courseService.getCommonQueryByCourseType(courseType, status, expired, customerId);
			if (filters != null && filters.getRules().size() > 0 && filters.getGroupOp() != null
					&& filters.getGroupOp().length() > 0) {
				// joinHQL += " WHERE ";
				joinHQL += " AND ";
				AdvanceQueryDto queryDto = filters;
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getAdvanceQueryCustomizedResultBySQL(queryDto, joinHQL, page,
						CourseListDto.class);
			} else {
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page,
						CourseListDto.class);
			}
		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " getAdvCourseList Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_COURSE_LIST);
			return responseResult;
		}
	}

	/**
	 * get course list by member id
	 * 
	 * @param courseType
	 * @param expired
	 * @param status
	 * @param filter
	 * @param customerId
	 *            member Id
	 * @param sortBy
	 * @param isAscending
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 * @author Miranda_Zhang
	 * @date Sep 29, 2015
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAdvCourseListAlias(@RequestParam(value = "courseType") String courseType,
			@RequestParam(value = "expired") String expired,
			@RequestParam(value = "status", defaultValue = "ALL") String status,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filter,
			@RequestParam(value = "customerId", required = false) String customerId,
			@RequestParam(value = "sortBy", defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {

		logger.info("CourseController.getAdvCourseListAlias start ...");

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try {
			AdvanceQueryDto queryDto = null;
			if (!StringUtils.isEmpty(filter)) {
				queryDto = (AdvanceQueryDto) parseJson(filter, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}

			String joinHQL = courseService.getCommonQueryByCourseType(courseType, status, expired, customerId);
			if (queryDto != null && queryDto.getRules().size() > 0 && queryDto.getGroupOp() != null
					&& queryDto.getGroupOp().length() > 0) {
				joinHQL += " WHERE ";
				// AdvanceQueryDto queryDto = filters;
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getAdvanceQueryCustomizedResultBySQL(queryDto, joinHQL, page,
						CourseListDto.class);
			} else {
				// no advance condition search
				AdvanceQueryDto queryDtoAlias = new AdvanceQueryDto();
				queryDtoAlias.setSortBy(sortBy);
				queryDtoAlias.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDtoAlias, joinHQL, page,
						CourseListDto.class);
			}
		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " getAdvCourseListAlias Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_COURSE_LIST);
			return responseResult;
		}
	}

	/**
	 * to get course list by coach
	 * 
	 * @param courseType
	 * @param expired
	 * @param status
	 * @param filter
	 * @param customerId
	 * @param sortBy
	 * @param isAscending
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 * @author
	 * @date Sep 29, 2015
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/personal/list", method = RequestMethod.GET)
	@ResponseBody

	//modified by Kaster 20160323 增加@EncryptFieldInfo
	//modified by Kaster 20160427 将接收参数customerId改为Long类型,避免加解密不一致。
	public ResponseResult getAdvCourseListAliasAndroid(@RequestParam(value = "courseType", required = false) String courseType,
			@RequestParam(value = "expired") String expired, @RequestParam(value = "status", defaultValue = "ALL") String status,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filter,
			@RequestParam(value = "customerId", required = false) @EncryptFieldInfo Long customerId,
			@RequestParam(value = "sortBy", defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {

		logger.info("CourseController.getAdvCourseListAliasAndroid start ...");

		try {
			AdvanceQueryDto queryDto = null;
			if (!StringUtils.isEmpty(filter)) {
				queryDto = (AdvanceQueryDto) parseJson(filter, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}

			//added by Kaster 20160427 将customerId转为String类型
			String cid = customerId.toString();
			String joinHQL = courseService.getCommonQueryByCourseTypeAndroid(courseType, status, expired, cid);
			if (queryDto != null && queryDto.getRules().size() > 0 && queryDto.getGroupOp() != null
					&& queryDto.getGroupOp().length() > 0) {
				joinHQL += " WHERE ";
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getAdvanceQueryCustomizedResultBySQL(queryDto, joinHQL, page,
						CourseListDto.class);
			} else {
				// no advance condition search
				AdvanceQueryDto queryDtoAlias = new AdvanceQueryDto();
				queryDtoAlias.setSortBy(sortBy);
				queryDtoAlias.setAscending(isAscending);
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				return advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDtoAlias, joinHQL, page,
						CourseListDto.class);
			}
		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " getAdvCourseListAlias Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_COURSE_LIST);
			return responseResult;
		}
	}

	/**
	 * This method is used to retrieve list of enrolled members for a Course.
	 * 
	 * @param courseType
	 * @param expired
	 * @param soryBy
	 * @param isAscending
	 * @param pageNumber
	 * @param pageSize
	 * @return ResponseResult (list of courses, pagination details etc)
	 * @author Vineela_Jyothi
	 */
	@RequestMapping(value = "/advMembers/{courseId}", method = RequestMethod.PUT)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseResult getAdvMemberList(@PathVariable(value = "courseId") String courseId,
			@RequestParam(value = "attendance", required = false) String attendance,
			@RequestParam(value = "status", required = false, defaultValue = "ALL") String status,
			@RequestBody(required = false) AdvanceQueryDto filters,
			@RequestParam(value = "sortBy", required = false, defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", required = false, defaultValue = "false") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "3") int pageSize,
			@RequestParam(value = "sessionId", required = false) String sessionId) {

		logger.info("CourseController.getAdvMemberList start ...");

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try {
			String joinHQL = courseService.getCommonQueryForMembersList(courseId, attendance, status);

			if (filters != null && filters.getRules().size() > 0 && filters.getGroupOp() != null
					&& filters.getGroupOp().length() > 0) {
				joinHQL += " WHERE ";
				AdvanceQueryDto queryDto = filters;
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);

				// added by vicky wang to fix the bug of advance search by issue
				// date.
				for (SearchRuleDto rule : filters.getRules()) {
					if (logger.isDebugEnabled()) {
						logger.debug(
								"rule field:" + rule.getField() + " op:" + rule.getOp() + " data:" + rule.getData());
					}
					if ("enrollDate".equals(rule.field)) {
						rule.setField("date_format(" + rule.field + ",'%Y-%m-%d')");
						if (logger.isDebugEnabled()) {
							logger.debug("updated enrollDate:" + rule.getField());
						}
					}
				}
				// added end

				ResponseResult tempResult = advanceQueryService.getAdvanceQueryCustomizedResultBySQL(queryDto, joinHQL,
						page, MemberInfoDto.class);
				return courseService.prepareMemberListResponse(courseId, tempResult, attendance, status, page,
						sessionId);
			} else {
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				ResponseResult tempResult = advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL,
						page, MemberInfoDto.class);
				return courseService.prepareMemberListResponse(courseId, tempResult, attendance, status, page,
						sessionId);
			}
		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " getAdvMemberList Failed!", e);
			responseResult.initResult(GTAError.CourseError.FAIL_RETRIEVE_MEMBER_LIST);
			return responseResult;
		}
	}

	/**
	 * This method is used to retrieve list of enrolled members for a Course.
	 * 
	 * @param courseType
	 * @param expired
	 * @param soryBy
	 * @param isAscending
	 * @param pageNumber
	 * @param pageSize
	 * @return ResponseResult (list of courses, pagination details etc)
	 * @author Vineela_Jyothi
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/advMembers/{courseId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseMemberList(@PathVariable(value = "courseId") String courseId,
			@RequestParam(value = "attendance", required = false) String attendance,
			@RequestParam(value = "status", defaultValue = "ALL") String status,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters,
			@RequestParam(value = "sortBy", defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "3") int pageSize,
			@RequestParam(value = "sessionId", required = false) String sessionId) {

		logger.info("CourseController.getAdvMemberList start ...");

		page.setNumber(pageNumber);
		page.setSize(pageSize);
		try {
			if (!StringUtils.isEmpty(filters)) {
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				page.setCondition(advanceQueryService.getSearchCondition(queryDto, ""));
			}
			return courseService.getMemberInfo(page, courseId, attendance, status, sessionId);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * This repayment operation can only be used when payment_method = credit
	 * card && member_acceptance = auto
	 * 
	 * @Author Mianping_Wu
	 * @Date Aug 25, 2015
	 * @Param @param courseEnrollmentDto
	 * @Param @return
	 * @return ResponseResult
	 */
	@RequestMapping(value = "/repayment", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult repaymentCourse(@RequestBody CourseEnrollmentDto courseEnrollmentDto) {

		try {

			return courseService.repaymentCourse(courseEnrollmentDto, getUser().getUserId());

		} catch (Exception e) {

			logger.error(CourseController.class.getName() + " credit card repayment failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/****
	 * 
	 * @param facilityType
	 *            GOLF/TENNS
	 * @param month
	 *            yyyy-MM
	 * @param (
	 *            if weekly to search FE set two params) startTime 、endTime
	 * @param coachUserId
	 * @param trainingTypes
	 *            [ FE selected coachings or courses]
	 * @return
	 */
	@RequestMapping(value = "/trainingCalendar/List", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTrainingCalendarList(@RequestParam(value = "facilityType") String facilityType,
			@RequestParam(value = "month", required = false) String month,
			@RequestParam(value = "startTime", required = false) String startTime,
			@RequestParam(value = "endTime", required = false) String endTime,
			@RequestParam(value = "coachUserId", required = false) String coachUserId,
			@RequestParam(value = "trainingType", required = false) String[] trainingTypes) {
		try {
			Date _startTime = null;
			Date _endTime = null;
			// month search
			if (!StringUtils.isEmpty(month)) {
				Date cur = DateConvertUtil.parseString2Date(month, "yyyy-MM");
				List<Date> list = DateCalcUtil.getDates(cur);
				_startTime = list.get(0);
				_endTime = list.get(1);
				return courseService.getCalendarList(coachUserId, facilityType, _startTime, _endTime, trainingTypes,month);
				
			}
			// weekly
			if (!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)) {
				_startTime = DateConvertUtil.parseString2Date(startTime, "yyyy-MM-dd");
				_endTime = DateConvertUtil.parseString2Date(endTime, "yyyy-MM-dd");
			}
			if (null != _startTime && null != _endTime) {
				return courseService.getCalendarList(coachUserId, facilityType, _startTime, _endTime, trainingTypes,null);
			} else {
				responseResult.initResult(GTAError.CourseError.ERROR_SEARCHING_PARAM);
				return responseResult;
			}

		} catch (Exception e) {
			logger.error(CourseController.class.getName() + " getTrainingCalendarList failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;

		}
	}
}
