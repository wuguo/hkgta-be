package com.sinodynamic.hkgta.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.dto.Constant;
import com.sinodynamic.hkgta.dto.UserLoginRequest;
import com.sinodynamic.hkgta.dto.crm.CustomerActionContentDto;
import com.sinodynamic.hkgta.dto.crm.HomePageSummaryDto;
import com.sinodynamic.hkgta.dto.crm.LoginUserDto;
import com.sinodynamic.hkgta.dto.staff.SecurityQuestionDto;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.security.GtaAuthenticationToken;
import com.sinodynamic.hkgta.security.GtaInvocationSecurityMetadataSource;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.adm.BiMemberUsageRateService;
import com.sinodynamic.hkgta.service.adm.StaffMasterService;
import com.sinodynamic.hkgta.service.common.DevicePushService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.sales.HomePageSummaryService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.qst.SurveyMemberService;
import com.sinodynamic.hkgta.service.sys.UserRoleService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.SessionMap;
import com.sinodynamic.hkgta.util.TokenUtils;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GTAError.LoginError;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.constant.UserType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.ehcache.CacheManager;

@Controller
public class HomePageSummaryController extends ControllerBase<String>
{
	private static final 	Logger logger = Logger.getLogger(HomePageSummaryController.class);

	@Autowired
	private HomePageSummaryService homePageSummaryService;

	@Autowired
	private UserMasterService userMasterService;
	
	@Autowired
	private GtaInvocationSecurityMetadataSource sourceMap;
	
	@Autowired
	private MemberService memberService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authManager;
	
	@Autowired
	@Qualifier("asynchronizedPushService")
	private DevicePushService devicePushService;
	
	@Autowired
	private StaffMasterService staffMasterService;
	
	@Autowired
	private GlobalParameterService globalParameterService ;
	@Autowired
	private BiMemberUsageRateService biMemberUsageRateService;
	
	@Autowired
	private SurveyMemberService   surveyMemberService;
	
	@Autowired
	private UserRoleService userRoleService;
		
	@RequestMapping(value = "/home/summary", method = RequestMethod.GET)
	public @ResponseBody ResponseResult homeSummary()
	{
		logger.debug("HomePageSummaryController.countCustomerProfile invocation start ...");
		HomePageSummaryDto ret = null;

		try
		{
			String staffId = null;
			LoginUser user = getUser();
			if (user != null)
				staffId = user.getUserId();

			ret = homePageSummaryService.countNumber(staffId);
			responseResult.initResult(GTAError.Success.SUCCESS, ret);
			logger.debug("HomePageSummaryController.countCustomerProfile invocation end ...");

		}
		catch (Exception e)
		{

			logger.error(e.toString());
			
			responseResult.initResult(GTAError.LoginError.GET_SUMMARY_FAILED);
		}

		return responseResult;
	}

	/**
	 * Get Latest Activities
	 * @param totleSize
	 * @return
	 * @author <please specify>
	 */
	@RequestMapping(value = "/home/news", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getLatestActionContent(@RequestParam(value = "totleSize", required = false, defaultValue = "20") String totleSize)
	{
		try
		{
			List<CustomerActionContentDto> customerActionContent = homePageSummaryService.getLatestActionContent(Integer.parseInt(totleSize));
			responseResult.initResult(GTAError.Success.SUCCESS, customerActionContent);
		}
		catch (Exception e)
		{
			logger.error(e);
			responseResult.initResult(GTAError.LoginError.GET_NEWS_FAILED);
			return responseResult;
		}

		return responseResult;
	}

	/**
	 * Security issue: this API should not be reachable by member. ie. need to check the user type who should not be a customer member (commented by Sam 2016-02-19) 
	 * @param academyId [academyNo,acadeCardNo]
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = { "/retriveMemberToken/{academyId}" }, method = RequestMethod.POST)
	public @ResponseBody ResponseResult retriveMemberToken(@PathVariable(value = "academyId") String academyNo) throws Exception
	{
		Member member = memberService.getMemberByAcademyNo(academyNo);
		if(null==member){
			member=memberService.getMemberByCardNo(academyNo);
		}
		
		if (null == member)
		{
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
			return responseResult;
		}

		UserMaster usermaster = userMasterService.getUserByUserId(member.getUserId());
		if (usermaster != null)
		{

			GtaAuthenticationToken authenticationToken = new GtaAuthenticationToken(usermaster.getUserId(), usermaster.getPassword());
			Authentication authentication = this.authManager.authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
		}
		else
		{
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
			return responseResult;
		}

		String token = TokenUtils.getUUIDToken(member.getUserId(), "Other", "Mobile",null,null);
		String base64token = Base64.encodeBase64String(token.getBytes());
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LoginUser authUser = (LoginUser) auth.getPrincipal();
		
		LoginUserDto user = new LoginUserDto(authUser.getUserId(),
				authUser.getUserNo(), authUser.getUserName(),
				authUser.getUserType(), authUser.getFullname(), authUser.getPortraitPhoto(), authUser.getDateOfBirth(), authUser.getPositionTitle(), authUser.getFirstName(), authUser.getLastName(), authUser.getMemberType());

		user.setToken(base64token);
		try
		{
			userMasterService.SaveSessionToken(member.getUserId(), token, "Other");
			usermaster.setLastLoginFailTime(null);
			usermaster.setLoginFailCount(null);
			userMasterService.updateUserMaster(usermaster);

		}
		catch (Exception e)
		{
			logger.error(e);
			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED);
			return responseResult;
		}

		Map<String, String> memberToken = new HashMap<String, String>();
		memberToken.put("memberToken", base64token);

		responseResult.initResult(GTAError.Success.SUCCESS, user);
		return responseResult;
	}
	
	public LoginError validateStaffJobNatureForCoachApp(String URI,String userID) throws Exception{
		//only a coach can login to the Coach APP
		if(URI.contains("/device/coachapplogin"))
		{ 
			String staffType = staffMasterService.getStaffTypeByUserId(userID);
			if(!(Constant.StaffType.FTR.toString().equals(staffType)||Constant.StaffType.PTR.toString().equals(staffType)||
						Constant.StaffType.FTG.toString().equals(staffType)||
						Constant.StaffType.PTG.toString().equals(staffType))){
				return GTAError.LoginError.LOGIN_FAILED_NO_PERMISSION;
			}
		}
		return null;
	}
	
	/**
	 * Common login endpoint for member portal, staff admin portal and app devices 
	 * @param loginUser
	 * @param request
	 * @param response
	 * @param device
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = {"/home/login", "/home/stafflogin", "/device/login", "/memberapp/login", "/device/stafflogin", "/device/saleskitlogin", "/device/coachapplogin"} , method = RequestMethod.POST)	
	public @ResponseBody ResponseResult login(@RequestBody UserLoginRequest loginUser, HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "device", required = false) String device,
			 @RequestParam(value = "appVersion", required = false) String appVersion,
			 @RequestParam(value = "osVersion", required = false) String osVersion) throws IOException
	{
		String loginDevice = CommUtil.getAccessDevice(request.getHeader("User-Agent"));
		String username = loginUser.getUsername().trim();
		logger.info("Login Request From Device : " + loginDevice);
		logger.info("User Agent:" + request.getHeader("User-Agent"));

		String URI = request.getRequestURI();
		if (URI.contains("/device") || URI.contains("/memberapp")) {
			loginDevice = loginDevice.equals("UnKnown") ? "Other" : loginDevice;
		}
		loginUser.setLocation(getLocation(URI, loginUser));

		ResponseResult result = userMasterService.doLogin(request, response, loginUser, URI, loginDevice);

		UserMaster usermaster = null;
		try {
			if (GTAError.Success.SUCCESS.getCode().equals(result.getReturnCode())) {
				usermaster = (UserMaster) result.getDto();
				String psw = CommUtil.getMD5Password(username, loginUser.getPassword());
				if (logger.isDebugEnabled()) {
					logger.debug("username:" + username + " encrypted password:"
							+ psw);
				}

				GtaAuthenticationToken authenticationToken = new GtaAuthenticationToken(usermaster.getUserId(), psw);
				Authentication authentication = this.authManager.authenticate(authenticationToken);
				salesKitLoginValidation(URI, authentication);
				SecurityContextHolder.getContext().setAuthentication(authentication);

				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				LoginUser authUser = (LoginUser) auth.getPrincipal();
				String token = TokenUtils.getUUIDToken(authUser.getUserId(), loginDevice, loginUser.getLocation(),appVersion,osVersion);
				String base64token = Base64.encodeBase64String((token.getBytes()));

				try {
					String deviceType=CommUtil.getAccessDeviceType(request.getHeader("User-Agent"));
					if (URI.contains("/device") || URI.contains("/memberapp")) {
						deviceType = deviceType.equals("UnKnown") ? "Other" : deviceType;
					}
					userMasterService.SaveSessionToken(authUser.getUserId(), token, deviceType);
					usermaster.setLastLoginFailTime(null);
					usermaster.setLoginFailCount(null);
					userMasterService.updateUserMaster(usermaster);
				} catch (Exception e) {
					logger.error(e);
					result.initResult(GTAError.LoginError.SAVE_SESSION_FAILED);
					return result;
				}
				if (null == usermaster.getPasswdChangeDate()) {
					response.addHeader("token", base64token);
					result.initResult(GTAError.LoginError.REQUIRE_TO_CHANGE_PSW, (Object) authUser.getUserId());
					return result;
				}
				LoginUserDto user = new LoginUserDto(authUser.getUserId(), authUser.getUserNo(), authUser.getUserName(),
						authUser.getUserType(), authUser.getFullname(), authUser.getPortraitPhoto(),
						authUser.getDateOfBirth(), authUser.getPositionTitle(), authUser.getFirstName(),
						authUser.getLastName(), authUser.getMemberType());
				user.setLoginId(authUser.getLoginId());

				if (user.getUserType().equals("CUSTOMER")) {
					try {
						Member member = memberService.getMemberByAcademyNo(authUser.getUserNo());
						if (member != null) {// 瀹归敊澶勭悊
							user.setQuestionnaireURL(surveyMemberService.getMeetConditionsSurveyQuestionnaireURL(
									member.getCustomerId(), authUser.getUserNo()));
							surveyMemberService.seveOrUpdateSurveyMemberByLogin(member.getCustomerId());
						}
					} catch (Exception e) {
						e.printStackTrace();
						logger.error(e);
					}
				}
				GlobalParameter wellnessSwitch = globalParameterService
						.getGlobalParameter(GlobalParameterType.WELLNESS_SWITCH.getCode());
				if (wellnessSwitch != null) {
					user.setPatronReservationWellnessSwitch(wellnessSwitch.getParamValue());
				}

				GlobalParameter accommodationSwitch = globalParameterService
						.getGlobalParameter(GlobalParameterType.ACCOMMODATION_SWITCH.getCode());
				if (accommodationSwitch != null) {
					user.setPatronReservationAccommodationSwitch(accommodationSwitch.getParamValue());
				}
				SessionMap.getInstance().addLocation(user.getUserId(), loginUser.getLocation());
				response.addHeader("token", base64token);

				request.getSession().setAttribute("loginToken", base64token);
				request.getSession().setAttribute("loginUser", authUser);

				// clear menu ehcache
				if (URI.contains("/home/stafflogin")) {
					userRoleService.clearMenuCache(usermaster.getUserId());
					// roleProgramService.initRoleMenu(usermaster.getUserId());
				}
				/***
				 * add bi_member_usage_rate
				 */
				biMemberUsageRateService.saveBiMemberUsageRate(loginDevice, usermaster);

				result.initResult(GTAError.LoginError.LOGIN_SUCCESS, user);

				LoginUserDto userDto = (LoginUserDto) result.getDto();
				logger.debug("request loginUser loginId=" + loginUser.getUsername().trim());
				logger.debug("response userDto loginId=" + userDto.getLoginId());
				logger.debug("response  userDto userId=" + userDto.getUserId());
				logger.debug("response userDto userName=" + userDto.getUserName());
				logger.debug("response userDto  fullName=" + userDto.getFullName());
				/***
				 * add check the same useId
				 */
				if (!loginUser.getUsername().trim().equalsIgnoreCase(userDto.getLoginId())) {
					logger.info("have a different loginId  with  same loginUser:");
					logger.info("request loginId=" + loginUser.getUsername());
					logger.info("response loginId=" + authUser.getLoginId());
					result.initResult(GTAError.LoginError.SAVE_SESSION_FAILED, "Login fail, please try again later");
				}
				if (!usermaster.getUserId().equals(userDto.getUserId())) {
					logger.info("have a different userId  with  same loginUser:");
					logger.info("response userId=" + usermaster.getUserId());
					logger.info("response userId=" + authUser.getUserId());
					result.initResult(GTAError.LoginError.SAVE_SESSION_FAILED, "Login fail, please try again later");
				}
				return result;
			} else {
				return result;
			}
		} catch (BadCredentialsException | UsernameNotFoundException e) {
			try {
				userMasterService.updateLoginFailTime(usermaster);
			} catch (Exception e1) {
				String[] args = { (null != usermaster) ? usermaster.getUserId() : "",
						DateConvertUtil.getStrFromDate(new Date()) };
				logger.warn(getI18nMessge(GTAError.LoginError.UPDATE_LOGIN_FAIL_TIME_FAILED.getCode(), args), e1);
				logger.error(e);
			}
			if (userMasterService.requireValidateKaptchaCode(loginDevice)
					&& userMasterService.requireValidateKaptchaCode(usermaster)) {
				responseResult.initResult(
						new GTAError[] { GTAError.LoginError.LOGIN_FAILED, GTAError.LoginError.VALID_CODE_REQUIRED });
			} else {
				logger.error("login faild.", e);
				responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
			}
			return responseResult;

		} catch (GTACommonException e) {
			if (StringUtils.isEmpty(e.getErrorMsg())) {
				responseResult.initResult(e.getError());
			} else {
				responseResult.initResult(e.getError(), e.getErrorMsg());
			}
			return responseResult;
		} catch (Exception e) {
			logger.error("login faild.", e);
			result.initResult(GTAError.LoginError.LOGIN_FAILED);
			return result;
		}
		
//		UserMaster usermaster = null;
//		try
//		{
//			String username = loginUser.getUsername().trim();
//			usermaster = userMasterService.getUserByLoginId(username);
//			if (usermaster != null)
//			{
//				validateURI(usermaster, URI);
//				if(usermaster.getUserType().equals(UserType.STAFF.getType()) && StringUtils.isEmpty(loginUser.getLocation()) && URI.contains("/home/stafflogin"))
//				{
//					responseResult.initResult(GTAError.LoginError.LOCATION_REQUIRED);
//					return responseResult;
//				}
//				
//				//only Coach Account can login to Coach APP
//				LoginError result = validateStaffJobNatureForCoachApp(URI,usermaster.getUserId());
//				if(result!=null){
//					responseResult.initResult(result);
//					return responseResult;
//				}
//				
//				if (userMasterService.hasBeenLocked(usermaster))
//				{
//					responseResult.initResult(GTAError.LoginError.USER_HAS_BEEN_LOCKED, new Object[] { appProps.get(Constant.LOGIN_FAIL_LOCK_PERIOD) });
//					return responseResult;
//				}
//				boolean requireValidate = userMasterService.requireValidateKaptchaCode(loginDevice) && userMasterService.requireValidateKaptchaCode(usermaster);
//				if (requireValidate)
//				{
//					String kaptchaExpected = SessionMap.getInstance().getSession(request.getHeader("sessionId"));
//					
//					if (StringUtils.isEmpty(loginUser.getValidCode()) || StringUtils.isEmpty(kaptchaExpected) || !kaptchaExpected.equals(loginUser.getValidCode()))
//					{
//						userMasterService.updateLoginFailTime(usermaster);
//						responseResult.initResult(StringUtils.isEmpty(loginUser.getValidCode()) ? GTAError.LoginError.VALID_CODE_REQUIRED : GTAError.LoginError.VALID_CODE_ERROR);
//						return responseResult;
//					}
//				}
//				String psw = CommUtil.getMD5Password(username, loginUser.getPassword());
//				if(logger.isDebugEnabled()){
//					logger.debug("username:" + username + " password:" + loginUser.getPassword() + " encrypt password:" + psw);					
//				}
//				GtaAuthenticationToken authenticationToken = new GtaAuthenticationToken(usermaster.getUserId(), psw);
//				Authentication authentication = this.authManager.authenticate(authenticationToken);
//				
//				salesKitLoginValidation(URI, authentication);
//				SecurityContextHolder.getContext().setAuthentication(authentication);
//					
//			}
//			else
//			{
//				logger.error("userMaster is null, login failed");
//				responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
//				return responseResult;
//			}
//		}
//		catch (BadCredentialsException | UsernameNotFoundException e)
//		{
//			try
//			{
//				userMasterService.updateLoginFailTime(usermaster);
//			}
//			catch (Exception e1)
//			{
//				String[] args = { (null!=usermaster) ?usermaster.getUserId():"", DateConvertUtil.getStrFromDate(new Date()) };
//				logger.warn(getI18nMessge(GTAError.LoginError.UPDATE_LOGIN_FAIL_TIME_FAILED.getCode(), args), e1);
//				logger.error(e);
//			}
//			if (userMasterService.requireValidateKaptchaCode(loginDevice) && userMasterService.requireValidateKaptchaCode(usermaster))
//			{
//				responseResult.initResult(new GTAError[] { GTAError.LoginError.LOGIN_FAILED, GTAError.LoginError.VALID_CODE_REQUIRED });
//			}
//			else
//			{
//				logger.error("login faild.",e);
//				responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
//			}
//			return responseResult;
//
//		}
//		catch (GTACommonException e)
//		{
//			if (StringUtils.isEmpty(e.getErrorMsg()))
//			{
//				responseResult.initResult(e.getError());
//			}
//			else
//			{
//				responseResult.initResult(e.getError(), e.getErrorMsg());
//			}
//			return responseResult;
//		}
//		catch (Exception e)
//		{
//			logger.error("login faild.",e);
//			responseResult.initResult(GTAError.LoginError.LOGIN_FAILED);
//			return responseResult;
//		}
//		

//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		LoginUser authUser = (LoginUser) auth.getPrincipal();
//		String token = TokenUtils.getUUIDToken(authUser.getUserId(), loginDevice, loginUser.getLocation());
//		String base64token = Base64.encodeBase64String((token.getBytes()));
//
//		/***
//		 * add check the same useId 
//		 */
//		if(!loginUser.getUsername().trim().equalsIgnoreCase(authUser.getLoginId()))
//		{
//			logger.error("have a different loginId  with  same loginUser:");
//			logger.error("request loginId="+loginUser.getUsername());
//			logger.error("response loginId="+authUser.getLoginId());
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		
//		if(!usermaster.getUserId().equals(authUser.getUserId()))
//		{
//			logger.error("have a different userId  with  same loginUser:");
//			logger.error("response userId="+usermaster.getUserId());
//			logger.error("response userId="+authUser.getUserId());
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		
//		try
//		{
//			userMasterService.SaveSessionToken(authUser.getUserId(), token, loginDevice);
//			usermaster.setLastLoginFailTime(null);
//			usermaster.setLoginFailCount(null);
//			userMasterService.updateUserMaster(usermaster);
//
//		}
//		catch (Exception e)
//		{
//			logger.error(e);
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED);
//			return responseResult;
//		}
//		
//		if (null == usermaster.getPasswdChangeDate())
//		{
//			response.addHeader("token", base64token);
//			responseResult.initResult(GTAError.LoginError.REQUIRE_TO_CHANGE_PSW, (Object) authUser.getUserId());
//			return responseResult;
//		}
//		
//
//		logger.info("start.line 420....###############################################################");
//		
//		LoginUserDto user = new LoginUserDto(authUser.getUserId(),
//				authUser.getUserNo(), authUser.getUserName(),
//				authUser.getUserType(), authUser.getFullname(), authUser.getPortraitPhoto(), authUser.getDateOfBirth(), authUser.getPositionTitle(), authUser.getFirstName(), authUser.getLastName(), authUser.getMemberType());
//		user.setLoginId(authUser.getLoginId());
//		
//		// **********濡傛灉鏄痬ember鐢ㄦ埛,缁熻鐧婚檰娆℃暟锛屽苟鑾峰彇褰撳墠绗﹀悎鏉′欢璋冩煡闂嵎鐨刄RL缁欏埌瀹㈡埛绔繘琛屽睍绀�***********
//		if (user.getUserType().equals("CUSTOMER")) {
//			try {
//				Member member = memberService.getMemberByAcademyNo(authUser.getUserNo());
//				if (member != null) {//瀹归敊澶勭悊
//					user.setQuestionnaireURL(surveyMemberService.getMeetConditionsSurveyQuestionnaireURL(member.getCustomerId(), authUser.getUserNo()));
//					surveyMemberService.seveOrUpdateSurveyMemberByLogin(member.getCustomerId());
//				} 
//			} catch (Exception e) {
//				e.printStackTrace();
//				logger.error(e);
//			}
//		}
//		
//		
//		if(!loginUser.getUsername().trim().equalsIgnoreCase(authUser.getLoginId()))
//		{
//			logger.info("start.line 447....###############################################################");
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		
//		
//		
//		GlobalParameter wellnessSwitch = globalParameterService.getGlobalParameter(GlobalParameterType.WELLNESS_SWITCH.getCode());
//		if (wellnessSwitch != null) {
//			user.setPatronReservationWellnessSwitch(wellnessSwitch.getParamValue());
//		}
//		
//		if(!loginUser.getUsername().trim().equalsIgnoreCase(authUser.getLoginId()))
//		{
//			logger.info("start.line 461....###############################################################");
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		
//		GlobalParameter accommodationSwitch = globalParameterService.getGlobalParameter(GlobalParameterType.ACCOMMODATION_SWITCH.getCode());
//		if (accommodationSwitch != null) {
//			user.setPatronReservationAccommodationSwitch(accommodationSwitch.getParamValue());
//		}
//		
//		
//		if(!loginUser.getUsername().trim().equalsIgnoreCase(authUser.getLoginId()))
//		{
//			logger.info("start.line 474....###############################################################");
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		
//		
//		if(!loginUser.getUsername().trim().equalsIgnoreCase(authUser.getLoginId()))
//		{
//			logger.info("start.line 485....###############################################################");
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		
//		
//		SessionMap.getInstance().addLocation(user.getUserId(), loginUser.getLocation());
//		response.addHeader("token", base64token);
//
//		request.getSession().setAttribute("loginToken", base64token);		
//		request.getSession().setAttribute("loginUser", authUser);
//	
//		
//		
//		if(!loginUser.getUsername().trim().equalsIgnoreCase(authUser.getLoginId()))
//		{
//			logger.info("start.line 501....###############################################################");
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		
//		
//		// clear menu ehcache 
//		if (URI.contains("/home/stafflogin")) {  
//		   userRoleService.clearMenuCache(usermaster.getUserId());
//		   //roleProgramService.initRoleMenu(usermaster.getUserId());
//		}   
//		/***
//		 * add bi_member_usage_rate
//		 */
//		biMemberUsageRateService.saveBiMemberUsageRate(loginDevice, usermaster);
//		
//		responseResult.initResult(GTAError.LoginError.LOGIN_SUCCESS, user);
//		
//		
//		
//		LoginUserDto userDto=  (LoginUserDto) responseResult.getDto();
//		
//		logger.debug("request loginUser loginId="+loginUser.getUsername().trim());
//		logger.debug("response userDto loginId=" + userDto.getLoginId());
//		logger.debug("response  userDto userId=" + userDto.getUserId());
//		logger.debug("response userDto userName=" + userDto.getUserName());  
//		logger.debug("response userDto  fullName=" + userDto.getFullName());
//		
//		/***
//		 * add check the same useId 
//		 */
//		if(!loginUser.getUsername().trim().equalsIgnoreCase(userDto.getLoginId()))
//		{
//			logger.info("start.line 534....###############################################################");
//			logger.info("have a different loginId  with  same loginUser:");
//			logger.info("request loginId="+loginUser.getUsername());
//			logger.info("response loginId="+authUser.getLoginId());
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		
//		if(!usermaster.getUserId().equals(userDto.getUserId()))
//		{
//			logger.info("start.line 544....###############################################################");
//			logger.info("have a different userId  with  same loginUser:");
//			logger.info("response userId="+usermaster.getUserId());
//			logger.info("response userId="+authUser.getUserId());
//			responseResult.initResult(GTAError.LoginError.SAVE_SESSION_FAILED,"Login fail, please try again later");
//			return responseResult;
//		}
//		logger.info("end.###############################################################");
//		return  responseResult;
	}
	
	
	/**
	 * This is service logic ...should not place here!
	 * @param URI
	 * @param authentication
	 */
	private void salesKitLoginValidation(String URI, Authentication authentication)
	{
		String skLoginURI = "/saleskitlogin";
		if (!URI.contains(skLoginURI)) return;
		
		
		Collection<ConfigAttribute> accessRoles = sourceMap.getResourceMap().get(skLoginURI);
		if (null == accessRoles) return;
		
		Collection<GrantedAuthority> Authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
		StringBuilder userRole = new StringBuilder();
		for(GrantedAuthority a : Authorities)
		{
			userRole.append(a.getAuthority()).append(",");
		}
		
		String roles = userRole.toString();
		Iterator<ConfigAttribute> iterator = accessRoles.iterator();
		
		boolean hasAccessRight = false;
		while(iterator.hasNext())
		{
			if (roles.contains(iterator.next().getAttribute())) hasAccessRight = true;
			break;
		}
		
		if (!hasAccessRight)
		{
			throw new GTACommonException(GTAError.LoginError.LOGIN_FAILED);
		}
	}

	/**
	 * This is service logic ...should not place here!
	 * @param URI
	 * @param loginUser
	 * @return
	 */
	private String getLocation(String URI, UserLoginRequest loginUser)
	{
		if (!StringUtils.isEmpty(loginUser.getLocation()))
		{
			return loginUser.getLocation();
		}
			
		if (URI.contains("/device") || URI.contains("/memberapp"))
		{
			return "Mobile";
		}
		if (URI.contains("/home/login"))
		{
			return "WP";
		}
		
		return "";
	}

	@RequestMapping(value = "/member/securityQuestion", method = RequestMethod.POST)
	public @ResponseBody ResponseResult setSecurityQuestion(@RequestBody SecurityQuestionDto questions) throws Exception
	{
		return memberService.setSecurityQuestion(questions);
	}

	@RequestMapping(value = "/home/logout", method = RequestMethod.GET)
	public @ResponseBody ResponseResult logout(HttpServletRequest request,@RequestParam(required=false) String arnApplication, Model model) throws IOException
	{
		String userId = super.getUser().getUserId();
		String device = CommUtil.getAccessDevice(request.getHeader("User-Agent"));

		boolean pass = userMasterService.deleteSessionToken(userId, device);

		if (pass)
		{
			if (!StringUtils.isEmpty(arnApplication) && (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IPhone")))
			{
				devicePushService.deleteUserDevice(userId,arnApplication);
			}
			
			if (!userMasterService.requireValidateKaptchaCode(device))
			{
				responseResult.initResult(GTAError.LoginError.LOGOUT_SUCCESS);
			}
			
			// Clear session and ehcache on logout
//			if (request.getSession().getAttribute("menuObj")!=null)
//			   System.out.println(" logout menuObj exist"); 
			
			try {
				request.getSession().invalidate();
				//model.asMap().remove("menuObj"); // clear session			
				CacheManager manager = CacheManager.getInstance();
				net.sf.ehcache.Cache cache = manager.getCache("menuCache");
				cache.remove(userId);
			} catch (Exception e) {			
				logger.error(e);
			}	
			
		}
		else
		{
			responseResult.initResult(GTAError.LoginError.LOGOUT_FAILED);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/home/resetIdAndPassword", method = RequestMethod.POST)
	public @ResponseBody ResponseResult resetLoginIdAndPassword(){
		
		try {
			List<Member> members = memberService.getAllMembers();
			if (members == null || members.size() == 0) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
				return responseResult;
			}
			String defaultPassword = "123456";  // ?? poor code!! Why hard code this non-securer password here ?
			userMasterService.setAllLoginIdAndPassword(members, defaultPassword);
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		} catch (Exception e) {
			logger.error(e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
			
		}
		
	}
	
	/**
	 * Verify username and password when accessing the Full profile page
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	@RequestMapping(value = {"/verifyPermission"} , method = RequestMethod.POST)	
	public @ResponseBody ResponseResult verifyPermission(@RequestBody UserLoginRequest loginUser, HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("verify user when accessing the Full profile page");
		
		UserMaster usermaster = userMasterService.getUserByLoginId(loginUser.getUsername());
		try{
			if (usermaster != null) {
				if (!usermaster.getUserType().equals(UserType.STAFF.getType()) && !usermaster.getUserType().equals(UserType.ADMIN.getType())) {
					responseResult.initResult(GTAError.LoginError.VERIFY_FAILED);
					return responseResult;
				}
				String psw = CommUtil.getMD5Password(loginUser.getUsername(), loginUser.getPassword());
				GtaAuthenticationToken authenticationToken = new GtaAuthenticationToken(usermaster.getUserId(), psw);
				this.authManager.authenticate(authenticationToken);
			} else {
				responseResult.initResult(GTAError.LoginError.VERIFY_FAILED);
				return responseResult;
			}
		} catch (BadCredentialsException | UsernameNotFoundException e){
			responseResult.initResult(GTAError.LoginError.VERIFY_FAILED);
			return responseResult;
		}catch (GTACommonException e) {
			if (StringUtils.isEmpty(e.getErrorMsg())) {
				responseResult.initResult(e.getError());
			} else {
				responseResult.initResult(e.getError(), e.getErrorMsg());
			}
			return responseResult;
		} catch (Exception e) {
			logger.error("login faild.",e);
			responseResult.initResult(GTAError.LoginError.VERIFY_FAILED);
			return responseResult;
		}
		
		/***
		 * check permission
		 */
		return userRoleService.checkPermissionsByUserId(usermaster.getUserId(), "t_personal_info_full");
//		responseResult.initResult(GTAError.Success.SUCCESS);
//		return responseResult;
	}
	
	/**
	 * 保存心跳记录
	 * @param request
	 * @param response
	 * @param appTypeCode
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/Apps/memberHeartbeat", method = RequestMethod.POST)
	public @ResponseBody ResponseResult memberHeartbeat( HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "appTypeCode", required = false) String appTypeCode) throws IOException
	{
		String userId = super.getUser().getUserId();
		String device = CommUtil.getAccessDevice(request.getHeader("User-Agent"));
		userMasterService.saveSessionTokenTask(userId, request.getHeader("token"), device, appTypeCode);	
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

}