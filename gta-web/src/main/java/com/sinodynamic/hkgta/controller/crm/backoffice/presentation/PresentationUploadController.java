package com.sinodynamic.hkgta.controller.crm.backoffice.presentation;

import static com.sinodynamic.hkgta.util.constant.ResponseMsgConstant.SUCCESS_MSG;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.FolderInfoDto;
import com.sinodynamic.hkgta.service.crm.backoffice.presentation.PresentationFolderOperationService;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.ResponseMsgConstant;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class PresentationUploadController extends ControllerBase {

	@Autowired
	PresentationFolderOperationService	presentationUploadService;

	@RequestMapping(value = "/presentation/create_folder", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg createFolder(@RequestParam("folderName") String folderName) {
		try {
			String userId = this.getUser().getUserId();
			presentationUploadService.createFolder(folderName, userId);
			this.responseMsg.initResult(GTAError.Success.SUCCESS);
			return this.responseMsg;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			GTACommonException error = (GTACommonException) e;
			this.responseMsg.initResult(error.getError(), error.getArgs());
			return this.responseMsg;
		}
	}

	@RequestMapping(value = "/presentation/rename_folder", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg renameFolder(@RequestParam("originalFolderName") String orgFolderName, @RequestParam("newFolderName") String newFolderName) {
		try {
			String userId = this.getUser().getUserId();
			presentationUploadService.renameFolder(orgFolderName, newFolderName, userId);
			return SUCCESS_MSG;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseMsg msg = new ResponseMsg();
			msg.setErrorMessageEN(e.getMessage());
			msg.setReturnCode(ResponseMsgConstant.ERRORCODE);
			return msg;
		}
	}

	@RequestMapping(value = "/presentation/get_folders", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFolders(@RequestParam("property_name") String name, @RequestParam("order") String order,
			@RequestParam("page_size") int pageSize, @RequestParam("current_page") int currentPage) {
		try {
			Data listData = presentationUploadService.getFoldersInfo(name, order, pageSize, currentPage);
			return new ResponseResult(SUCCESS_MSG, listData);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseResult msg = new ResponseResult();
			msg.setErrorMessageEN(e.getMessage());
			msg.setReturnCode(ResponseMsgConstant.ERRORCODE);
			return msg;
		}
	}

	@RequestMapping(value = "/presentation/get_folder/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFolder(@PathVariable("id") Long id) {
		try {
			FolderInfoDto foldersInfo = presentationUploadService.getFolderInfo(id);
			return new ResponseResult(SUCCESS_MSG, foldersInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseResult msg = new ResponseResult();
			msg.setErrorMessageEN(e.getMessage());
			msg.setReturnCode(ResponseMsgConstant.ERRORCODE);
			return msg;
		}
	}
}
