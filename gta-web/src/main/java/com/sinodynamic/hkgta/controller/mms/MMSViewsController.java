package com.sinodynamic.hkgta.controller.mms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.DtoHelper;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.MmsOrderDto;
import com.sinodynamic.hkgta.dto.mms.MmsOrderItemDto;
import com.sinodynamic.hkgta.entity.mms.SpaAppointmentRec;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetail;
import com.sinodynamic.hkgta.integration.spa.response.AppointmentDetailsResponse;
import com.sinodynamic.hkgta.integration.spa.service.SpaServcieManager;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.mms.SpaAppointmentRecService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller("/mmsview")
public class MMSViewsController extends ControllerBase<Serializable> {
    
	private Logger logger = Logger.getLogger(MMSViewsController.class);
	
    @Autowired
    private SpaAppointmentRecService spaAppointmentRecService;
    
    @Autowired
    private AdvanceQueryService advanceQueryService;
    
    
    @Autowired
    private SpaServcieManager spaServcieManager;
    
    
	@RequestMapping(value = "/orderlist", method = RequestMethod.PUT)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseResult getMMSOrderList(
		@RequestBody(required = false) AdvanceQueryDto filters,
		@RequestParam(value = "beginDate", required = false) String beginDate, 
		@RequestParam(value = "endDate", required = false) String endDate,
		@RequestParam(value = "sortBy", required = false, defaultValue="orderNO") String sortBy, 
		@RequestParam(value = "isAscending", required = false, defaultValue = "false") String isAscending, 
		@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber, 
		@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
		@RequestParam(value = "customerId", required = false) String customerId,
		@RequestParam(value = "status", required = false) String status) {

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try
		{			
			String joinHQL = spaAppointmentRecService.getMmsAdvanceSql(beginDate, endDate, customerId, status);

			if(logger.isDebugEnabled())
			{
				logger.debug("sql:" + joinHQL);
			}
			ResponseResult rs = null;
			if (filters != null && filters.getRules().size() > 0 && filters.getGroupOp() != null && filters.getGroupOp().length() > 0)
			{
				joinHQL += " where ";
				filters.setSortBy(sortBy);
				filters.setAscending(isAscending);
				rs = advanceQueryService.getAdvanceQueryCustomizedResultBySQL(filters, joinHQL, page, MmsOrderDto.class);			
			}
			else
			{
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				rs = advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, MmsOrderDto.class);
			}
			
			if (rs != null && rs.getData() != null) {
			    
			    List<MmsOrderDto> result = (List<MmsOrderDto>) ((Data)rs.getData()).getList();
			    if (result != null && result.size() > 0) {
				for (MmsOrderDto dto : result) {
				    dto.setOrderDate((DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(dto.getOrderDate(), "yyyy-MM-dd"))));
				}
			    }
			}
			return rs;
		}
		catch (Exception e)
		{
			logger.error(MMSViewsController.class.getName() + " getMMSOrderList Failed!", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/orderlist/options", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMMSOrderListTimeOptions() {

		try {
		    
		    Map<String, Map<String, String>> result = new TreeMap<String, Map<String,String>>();
		    Date now = new Date();
		    
		    //All
		    result.put("All", null);
		    //Today
		    Map<String, String> map3 = getTimeOption(now, 0);
		    result.put("Today", map3);
		    //Within 30 days
		    Map<String, String> map2 = getTimeOption(now, 30);
		    result.put("Within 30 days", map2);
		    //Within 90 days
		    Map<String, String> map1 = getTimeOption(now, 90);
		    result.put("Within 90 days", map1);
		    
		    responseResult.initResult(GTAError.Success.SUCCESS, result);
		    return responseResult;
			
		} catch (Exception e) {
		    logger.error(MMSViewsController.class.getName() + " getMMSOrderListTimeOptions Failed!", e);
		    responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		    return responseResult;
		}
	}
	
	
	
	@RequestMapping(value = "/orderitems", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMMSOrderItems(@RequestParam(value = "extInvoiceNo", required = true) String extInvoiceNo) {
	    
	    	if (StringUtils.isEmpty(extInvoiceNo)) return null;
	    
		try {
		    	List<MmsOrderItemDto> result = spaAppointmentRecService.getSpaAppointmentRecByExtinvoiceNo(extInvoiceNo);
		    	if (result != null && result.size() > 0) {
		    	    for (MmsOrderItemDto dto : result) {
		    		dto.setTime(composeTimePeriod(dto.getStartTime(), dto.getEndTime()));
		    		dto.setDate(DtoHelper.getYMDDate(dto.getStartTime()));
		    	    }
		    	}
			responseResult.initResult(GTAError.Success.SUCCESS, result);
			return responseResult;
			
		} catch (Exception e) {
		    
			logger.error(MMSViewsController.class.getName() + " getMMSOrderItems failed!", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	private String composeTimePeriod(Date from, Date to) {
	    
	    if (from == null || to == null) return null;
	    
	    String fromStr = DateCalcUtil.formatDatetime(from);
	    String toStr = DateCalcUtil.formatDatetime(to);
	    String timeStart = fromStr.substring(11, 16);
	    String timeEnd = toStr.substring(11, 16);
	    
	    return timeStart + "-" + timeEnd;
	}
	
	
	private Map<String, String> getTimeOption(Date toDate, int pastDays) throws Exception {
	    
	    Date fromDate = DateCalcUtil.getNearDay(toDate, (-1 * pastDays));
	    String fromDateStr = DateCalcUtil.formatDate(fromDate);
	    String toDateStr = DateCalcUtil.formatDate(toDate);
	    
	    Map<String, String> entry = new TreeMap<String, String>();
	    entry.put("fromDate", fromDateStr);
	    entry.put("toDate", toDateStr);
	    return entry;
	}
	
}
