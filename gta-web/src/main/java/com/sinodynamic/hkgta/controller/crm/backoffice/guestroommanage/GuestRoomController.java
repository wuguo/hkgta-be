package com.sinodynamic.hkgta.controller.crm.backoffice.guestroommanage;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.pms.RoomPicPath;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.service.pms.RoomTypeService;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/roomTypes")
public class GuestRoomController extends ControllerBase{
	
	
	private static final Logger logger = Logger.getLogger(GuestRoomController.class);
	private static String JPEG_EXT = "jpeg";
	private static String JPG_EXT = "jpg";
	private static String PNG_EXT = "png";
	private static String GIF_EXT = "gif";
	
	@Resource(name="oasisProperties") 
	protected Properties oasisProps;
	
	@Autowired 
	private RoomTypeService roomTypeService;
	
	@Autowired
	private AdvanceQueryService advanceQueryService;
	
	@Autowired 
	private RoomService roomService;
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public @ResponseBody ResponseResult getRoomTypeList(@RequestParam(value = "param", required = false) String param) throws Exception {
		List<RoomTypeDto> roomList = roomTypeService.getRoomListByTypes(param);

		responseResult.initResult(GTAError.Success.SUCCESS, roomList);
		return responseResult;
	}
	
	@RequestMapping(value = "/roomlist", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryData(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="", required = false) String sortBy,
			@RequestParam(value="isAscending",defaultValue="true", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters) throws Exception{
		
		String joinSQL = roomTypeService.getRoomList();
		ResponseResult tmpResult = null;
		if (!StringUtils.isEmpty(filters))
		{
			joinSQL = joinSQL + " AND ";
			AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);

			tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinSQL, page, RoomTypeDto.class);
		}
		else
		{
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, RoomTypeDto.class);
		}
		
		List<RoomTypeDto> roomList = (List<RoomTypeDto>) tmpResult.getListData().getList();
		roomTypeService.setRoomPics(roomList);
		tmpResult.getListData().setList(roomList);
		return tmpResult;
	}
	
	
	@RequestMapping(value="/{roomTypeCode}", method=RequestMethod.GET)
	public @ResponseBody ResponseResult getRoomTypeInfo(@PathVariable(value="roomTypeCode") String roomTypeCode) throws Exception {
		return roomTypeService.getRoomTypeByCode(roomTypeCode);
	}
	
	@RequestMapping(value="/isAvailable/{roomTypeCode}", method=RequestMethod.GET)
	public @ResponseBody ResponseResult isAvailable(@PathVariable(value="roomTypeCode") String roomTypeCode) throws Exception {
		ResponseResult tmpResult = null;
		Map<String, String> available = new HashMap<String,String>();
		try
		{
			tmpResult = roomTypeService.getRoomTypeByCode(roomTypeCode);
			
		}catch(GTACommonException e)
		{
			logger.error(e.getMessage(), e);
			if (e.getError().getCode().equals(GTAError.GuestRoomError.ROOM_NOT_EXIST.getCode()))
			{
				available.put("isAvailable", "true");
				responseResult.initResult(GTAError.Success.SUCCESS, available);
				return responseResult;
			}
		}
		
		if (GTAError.Success.SUCCESS.getCode().equals(tmpResult.getReturnCode()))
		{
			RoomTypeDto room = (RoomTypeDto) tmpResult.getDto();
			if ("ACT".equals(room.getStatus()))
			{
				available.put("isAvailable", "false");
				responseResult.initResult(GTAError.Success.SUCCESS, available);
			}
			
			if ("DEL".equals(room.getStatus()))
			{
				responseResult.initResult(GTAError.Success.SUCCESS, room);
			}
		}
		
		return responseResult;
		

	}
	
	@RequestMapping(value="/retrieve/{roomTypeCode}", method=RequestMethod.GET)
	public @ResponseBody ResponseResult retrieve(@PathVariable(value="roomTypeCode") String roomTypeCode) throws Exception {
		return roomTypeService.retrieveGuestRoom(roomTypeCode);
	}
	
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public @ResponseBody ResponseResult createRoomType(@RequestBody RoomTypeDto room) throws Exception {
		
		responseResult = roomTypeService.createRoomType(room, getUser().getUserId());
		return responseResult;
	}
	
	@RequestMapping(value="/{roomTypeCode}", method=RequestMethod.DELETE)
	public @ResponseBody ResponseResult deleteRoomType(@PathVariable(value="roomTypeCode") String roomTypeCode) throws Exception {
		
		roomTypeService.deleteRoomType(roomTypeCode, getUser().getUserId());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value="", method=RequestMethod.PUT)
	public @ResponseBody ResponseResult updateRoomType(@RequestBody RoomTypeDto room) throws Exception {
		
		roomTypeService.updateRoomType(room, getUser().getUserId());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/uploadRoomPics", method = RequestMethod.POST)
	public @ResponseBody ResponseResult uploadPics(@RequestParam(value = "files", required = false) MultipartFile[] files, HttpServletRequest request) throws Exception
	{
		if (files == null || files.length <= 0)
		{
			logger.info("File is empty!");
			responseResult.initResult(GTAError.CommonError.NO_FILE_UPLOAD);
//			responseResult.initResult(GTAError.CommonError.Error, "No file is upload!");
			return responseResult;
		}
		String saveFilePath = "";
		Map<Integer, String> imgPathMap = new HashMap<Integer, String>();
		int index = 0;
		for (MultipartFile tmpFile : files)
		{
			saveFilePath = FileUpload.upload(tmpFile, FileUpload.FileCategory.GUESTROOM, "", true);
			File serverFile = new File(saveFilePath);
			saveFilePath = serverFile.getName();
			imgPathMap.put(index, saveFilePath);
			index++;

		}
		responseResult.initResult(GTAError.Success.SUCCESS, imgPathMap);
		return responseResult;
	}
	
	@RequestMapping(value = "/removeTempPics", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult removeTempPic(@RequestParam(value = "filename", required = false) String filename) throws Exception
	{
		String[] files = filename.split(",");
		for(String f : files)
		{
			FileUpload.deleteFile(f, FileUpload.getBasePath(FileUpload.FileCategory.GUESTROOM));
		}

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/getRoomPic", method = RequestMethod.GET)
	@ResponseBody
	public void getRoomPic(@RequestParam(value = "tmpfile", required = false) String tmpfile, @RequestParam(value = "picId", required = false) Long picId, @RequestParam(value = "isThumbnail", required = false) Boolean isThumbnail, HttpServletResponse resp) throws Exception
	{
		
		String root = appProps.getProperty(FileUpload.FileCategory.GUESTROOM.getConfigName());
		if (!StringUtils.isEmpty(tmpfile))
		{
			File file = new File(root + File.separator + tmpfile);
			byte[] output = IOUtils.toByteArray(new FileInputStream(file));

			resp.getOutputStream().write(output);
		}
		else
		{
			RoomPicPath pic = roomTypeService.getRoomPicByPicId(picId);
			if (null != pic)
			{
				String fileSuffix = pic.getServerFilename().substring(pic.getServerFilename().lastIndexOf(".") + 1, pic.getServerFilename().length()).toUpperCase();
				resp.setHeader("Cache-Control", "max-age=21600");
				if (JPEG_EXT.equalsIgnoreCase(fileSuffix) || JPG_EXT.equalsIgnoreCase(fileSuffix))
				{
					resp.setContentType("image/jpeg");
				}
				else if (PNG_EXT.equalsIgnoreCase(fileSuffix))
				{
					resp.setContentType("image/png");
				}
				else if (GIF_EXT.equalsIgnoreCase(fileSuffix))
				{
					resp.setContentType("image/gif");
				}

				String fileName = pic.getServerFilename();
				if (null != isThumbnail && isThumbnail)
				{
					fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_small." + fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
				}
				File file = new File(root + File.separator + pic.getRoomTypeCode() + File.separator + fileName);
				if (!file.exists()) return;
				byte[] output = IOUtils.toByteArray(new FileInputStream(file));

				resp.getOutputStream().write(output);
			}

		}
	}
	/**
	 * 是否可以checking
	 * @param customerId
	 * @param resvId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/isCanCheckIn/{customerId}/{resvId}", method=RequestMethod.GET)
	public @ResponseBody ResponseResult isCanCheckIn(@PathVariable(value="customerId") @EncryptFieldInfo Long customerId, @PathVariable(value="resvId") Long resvId) throws Exception {
		return roomService.isCanCheckIn(customerId, resvId);
	}
	
	/**
	 * member checkIn
	 * @param customerId
	 * @param resvId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkInProgress/{customerId}/{resvId}", method=RequestMethod.GET)
	public @ResponseBody ResponseResult checkInProgress(@PathVariable(value="customerId") @EncryptFieldInfo Long customerId, @PathVariable(value="resvId") Long resvId) throws Exception {
		return roomService.checkInProgress(customerId, resvId);
	}
	
	
	@RequestMapping(value="/importAllotment", method=RequestMethod.POST)
	public @ResponseBody ResponseResult importAllotment(@RequestParam("file") final MultipartFile csvFile) throws Exception {
		File sourceFile = getUploadCsvFile(csvFile);
		return roomService.importAllotment(getUser().getUserId(),sourceFile);
	}
	
	private File getUploadCsvFile(MultipartFile csvFile) throws Exception{
		String path = oasisProps.getProperty(Constant.IMPORT_FILE_PATH);
		File file = new File(path);
		if (!file.exists() && !file.isDirectory()) {
			file.mkdirs();
		}
		File targetFile = new File(path, csvFile.getOriginalFilename());
		csvFile.transferTo(targetFile);
		return targetFile;
	}
	
}
