package com.sinodynamic.hkgta.controller.admin;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.aspectj.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.LoggerRecordDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.AppsVersionListDto;
import com.sinodynamic.hkgta.dto.qst.QuestionnaireSettingListDto;
import com.sinodynamic.hkgta.dto.sys.AssignProgramDto;
import com.sinodynamic.hkgta.dto.sys.AssignRoleDto;
import com.sinodynamic.hkgta.dto.sys.RoleDto;
import com.sinodynamic.hkgta.dto.sys.RoleMasterDto;
import com.sinodynamic.hkgta.dto.sys.RoleProgramDto;
import com.sinodynamic.hkgta.dto.sys.SysUserDto;
import com.sinodynamic.hkgta.dto.sys.UserRoleDto;
import com.sinodynamic.hkgta.entity.bi.BiOasisFlexFile;
import com.sinodynamic.hkgta.entity.crm.LoginSession;
import com.sinodynamic.hkgta.entity.crm.ProgramMaster;
import com.sinodynamic.hkgta.entity.crm.RoleMaster;
import com.sinodynamic.hkgta.entity.crm.UserMaster;
import com.sinodynamic.hkgta.scheduler.service.MemberSynchronizeTaskService;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.security.service.RoleProgramService;
import com.sinodynamic.hkgta.service.bi.BiOasisFlexFileService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.AppsVersionService;
import com.sinodynamic.hkgta.service.crm.sales.SysCodeService;
import com.sinodynamic.hkgta.service.crm.sales.UserMasterService;
import com.sinodynamic.hkgta.service.mms.MemberSynchronizeService;
import com.sinodynamic.hkgta.service.qst.SurveyService;
import com.sinodynamic.hkgta.service.sys.ProgramMasterService;
import com.sinodynamic.hkgta.service.sys.RoleMasterService;
import com.sinodynamic.hkgta.service.sys.SysUserService;
import com.sinodynamic.hkgta.service.sys.UserRoleService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import io.swagger.annotations.ApiOperation;

@SessionAttributes({ "menuObj" })
@Controller
public class SystemController extends ControllerBase {

	@Autowired
	private RoleMasterService roleMasterService;
	@Autowired
	private ProgramMasterService programMasterService;
	@Autowired
	private RoleProgramService roleProgramService;
	@Autowired
	public UserRoleService userRoleService;

	@Autowired
	public AdvanceQueryService advanceQueryService;

	@Autowired
	SysUserService sysUserService;

	@Autowired
	private UserMasterService userService;

	@Autowired
	private AppsVersionService appsVersionService;
	
	@Autowired
	private SysCodeService  sysCodeService;
	
	@Autowired
	private MemberSynchronizeService memberSynchronizeService;
	
	@Autowired
	private MemberSynchronizeTaskService memberSynchronizeTaskService;
	
	@Autowired
	private BiOasisFlexFileService biOasisFlexFileService;
	
	@Autowired
	private SurveyService surveyService;
	
	
	@Autowired
	private UserMasterService userMasterService;
	
	@Resource(name = "oasisProperties")
	private Properties oasisProps;

	@RequestMapping(value = "/sys/role/list", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryData(
			@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters) throws Exception {

		LoginUser user = getUser();

		String joinSQL = roleMasterService.getRoleList();
		if (StringUtils.isNotEmpty(filters)) {
			joinSQL = joinSQL + " AND ";
			AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);

			return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinSQL, page, RoleMasterDto.class);
		}
		// no advance condition search
		AdvanceQueryDto queryDto = new AdvanceQueryDto();
		prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
		return advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, RoleMasterDto.class);
	}

	@RequestMapping(value = "/sys/role", method = RequestMethod.POST)
	public @ResponseBody ResponseResult createRole(@RequestBody RoleMaster role) {
		return roleMasterService.createRole(role);
	}

	@RequestMapping(value = "/sys/role/status", method = RequestMethod.POST)
	public @ResponseBody ResponseResult changeStatus(@RequestBody RoleDto dto) {
		return roleMasterService.changeStatus(dto);
	}

	@RequestMapping(value = "/sys/role", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateRole(@RequestBody RoleMaster role) {
		try {
			if (roleMasterService.validateRole(role.getRoleId(), role.getRoleName())) {
				responseResult.initResult(GTAError.SysError.ROLE_NAME_ALREADY_EXISTS);
				return responseResult;
			}
			return roleMasterService.updateRole(role);
		} catch (Exception e) {
			logger.error(SystemController.class.getName() + " updateRole Failed!", e);
			responseResult.initResult(GTAError.SysError.FAIL_UPDATE_ROLE);
			return responseResult;
		}
	}

	@RequestMapping(value = "/sys/role/{roleId}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseResult deleteRole(@PathVariable(value = "roleId") BigInteger roleId) {
		return roleMasterService.deleteRole(roleId);
	}

	@Deprecated
	@RequestMapping(value = "/sys/available/programs/{roleId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAvailablePrograms(@PathVariable(value = "roleId") BigInteger roleId) {
		List<ProgramMaster> programMasterList = programMasterService.getAvailablePrograms(roleId);
		responseResult.initResult(GTAError.Success.SUCCESS, programMasterList);
		return responseResult;
	}

	@RequestMapping(value = "/sys/role_program/list/{roleId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getRoleProgramsByRoleId(@PathVariable(value = "roleId") BigInteger roleId,
			@RequestParam(value = "userType", required = false, defaultValue = "STAFF") String userType) {
		return roleProgramService.getRoleProgramsByRoleId(roleId, userType);
	}

	@RequestMapping(value = "/sys/role_program/add", method = RequestMethod.POST)
	public @ResponseBody ResponseResult roleAddProg(@RequestBody RoleProgramDto dto) {
		return roleProgramService.roleAddProg(dto);
	}

	@RequestMapping(value = "/sys/role_program/del", method = RequestMethod.POST)
	public @ResponseBody ResponseResult roleDelProg(@RequestBody RoleProgramDto dto) {
		return roleProgramService.roleDelProg(dto);
	}

	@RequestMapping(value = "/sys/role_program/{roleId}/{programId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRoleProg(@PathVariable(value = "roleId") BigInteger roleId,
			@PathVariable(value = "programId") String programId) {
		return roleProgramService.getRoleProg(roleId, programId);
	}

	@RequestMapping(value = "/sys/role_program/edit", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult roleEditProg(@RequestBody RoleProgramDto dto) {
		return roleProgramService.roleEditProg(dto);
	}

	@RequestMapping(value = "/sys/available/roles/{userType}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getRoleListByUserType(@PathVariable(value = "userType") String userType) {
		List<RoleMaster> roleList = roleProgramService.getRoleListByUserType(userType);
		responseResult.initResult(GTAError.Success.SUCCESS, roleList);
		return responseResult;
	}

	@RequestMapping(value = "/sys/user/roles/{userId}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getUserRoleListByUserId(@PathVariable(value = "userId") String userId) {
		List<UserRoleDto> roleList = userRoleService.getUserRoleListByUserId(userId);
		responseResult.initResult(GTAError.Success.SUCCESS, roleList);
		return responseResult;
	}

	@RequestMapping(value = "/sys/assign/roles/", method = RequestMethod.POST)
	public @ResponseBody ResponseResult assignRole(@RequestBody AssignRoleDto dto) {
		return userRoleService.assignRole(dto, getUser().getUserId());
	}

	@RequestMapping(value = "/sys/remove/roles/", method = RequestMethod.POST)
	public @ResponseBody ResponseResult removeRole(@RequestBody AssignRoleDto dto) {
		return userRoleService.removeRole(dto);
	}

	/**
	 * Retrieve staff admin portal user menu
	 * 
	 * @param model
	 * @return
	 */
	@ApiOperation("Retrieve staff admin portal user menu")
	@RequestMapping(value = "/sys/user/permissions/", method = RequestMethod.GET)
	// public @ResponseBody ResponseResult getUserPermissions() {
	public @ResponseBody ResponseResult getUserPermissions(Model model) {

		// ResponseResult contain non serializable object that can't be
		// replicated to other tomcat. Discard change (SAM commented)
		// ResponseResult menuObj=(ResponseResult) model.asMap().get("menuObj");
		// if (menuObj==null) {
		// menuObj=userRoleService.loadUserPermissionsV2(getUser().getUserId());
		//
		// try {
		// model.addAttribute("menuObj",menuObj);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		//
		// System.out.println("debug: no menuObj session");
		// }
		//
		// return menuObj;
		
		//return userRoleService.loadUserPermissionsV2(getUser().getUserId(), roleProgramService.initRoleMenu());
		return userRoleService.loadUserPermissionsV2(getUser().getUserId(), roleProgramService.initRoleMenu(getUser().getUserId()));
	}

	@RequestMapping(value = "/sys/assign/program", method = RequestMethod.POST)
	public @ResponseBody ResponseResult assignProgram(@RequestBody AssignProgramDto dto) {
		if (StringUtils.isEmpty(dto.getRoleName())) {
			responseResult.initResult(GTAError.SysError.ROLE_NAME_EMPTY);
			return responseResult;
		}
		if (roleMasterService.validateRole(dto.getRoleId(), dto.getRoleName())) {
			responseResult.initResult(GTAError.SysError.ROLE_NAME_ALREADY_EXISTS);
			return responseResult;
		}
		responseResult = roleProgramService.assignProgram(dto);
		/**
		 * Cancel the refreshRole() and retrieve role data from DB real time
		 */
		// roleProgramService.refreshRole(); 
		return responseResult;
	}

	@RequestMapping(value = "/sys/temp_user/new", method = RequestMethod.POST)
	public @ResponseBody ResponseResult createTempUser(@RequestBody SysUserDto user) throws Exception {
		user.setCreateBy(this.getUser().getUserId());
		user.setUpdateBy(this.getUser().getUserId());
		sysUserService.addSysUser(user);

		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/sys/user/list", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getUserList(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize, @RequestParam("sortBy") String sortBy,
			@RequestParam(value = "filters", required = false) String filters,
			@RequestParam("isAscending") Boolean isAscending,@RequestParam(value="roleId",required = false) Long roleId,
			@RequestParam(value = "userType", required = true) String userType) throws Exception {

		/***
		 * add @RequestParam String userType
		 * SGG-3687 
		 */
		AdvanceQueryDto query = null;
		if (!StringUtils.isEmpty(filters)) {
			query = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
		}

		/***
		 * SG-3144
		 * Add "User Role" in System Admin > User page
		 */
		Data data = sysUserService.loadAllUsers(roleId,sortBy, (isAscending ? "asc" : "desc"), pageSize, pageNumber, query,userType);

		responseResult.initResult(GTAError.Success.SUCCESS, data);
		return responseResult;
	}

	@RequestMapping(value = "/sys/role/dropDown", method = RequestMethod.GET)
	public @ResponseBody ResponseResult roleDropDown() throws Exception {
		responseResult.initResult(GTAError.Success.SUCCESS, roleMasterService.getAllRoleList());
		return responseResult;
	}
	
	@RequestMapping(value = "/sys/user/list/advanceSearch", method = RequestMethod.GET)
	public @ResponseBody ResponseResult userListAdvancedSearch() throws Exception {

		responseResult.initResult(GTAError.Success.SUCCESS, sysUserService.userListAdvanceSearch());
		return responseResult;
	}

	@RequestMapping(value = "/sys/user/status", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateUserStatus(@RequestParam("userId") String userId,
			@RequestParam("status") String status) throws Exception {

		UserMaster user = userService.getUserByUserId(userId);
		user.setStatus(status);

		userService.updateUserMaster(user);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/sys/user", method = RequestMethod.GET)
	public @ResponseBody ResponseResult viewSysUser(@RequestParam("userId") String userId) throws Exception {

		responseResult.initResult(GTAError.Success.SUCCESS, sysUserService.viewSysUser(userId));
		return responseResult;
	}

	@RequestMapping(value = "/sys/user", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult editSysUser(@RequestBody SysUserDto user) throws Exception {
		user.setUpdateBy(getUser().getUserId());
		// checkPasswordLegal
		// Staff edit user ,no input password click sumbit Then password is null
		// no checkPasswordLegal
		if (StringUtils.isNotEmpty(user.getPassword())) {
			responseResult = sysUserService.checkPasswordLegal(user.getPassword());
		} else {
			responseResult.initResult(GTAError.Success.SUCCESS);
		}
		if (!GTAError.Success.SUCCESS.getCode().equals(responseResult.getReturnCode())) {
			return responseResult;
		}

		sysUserService.editSysUser(user);
		sysUserService.refreshUserRole(user.getUserId());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	/**
	 * 获取所以app的版本以及下载连接
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/getAppsVersionList", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getAppsVersionList() throws Exception {
		responseResult.initResult(GTAError.Success.SUCCESS, appsVersionService.getAllAppsVersion());
		return responseResult;
	}

	/**
	 * 通过appid更新app版本信息
	 * 
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/updateAppsVersionInfo", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateAppsVersionInfo(@RequestBody AppsVersionListDto info) throws Exception {
		return appsVersionService.updateAppsVersionInfo(info, getUser().getUserId());
	}

	/**
	 * 是否需要升级版本
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/isUpgrade", method = RequestMethod.GET)
	public @ResponseBody ResponseResult isUpgrade(@RequestParam("appId") String appId,
			@RequestParam("versionNo") String versionNo) throws Exception {
		return appsVersionService.isUpgrade(appId, versionNo);
	}

	@RequestMapping(value = "/sys/logTypeList", method = RequestMethod.GET)
	public @ResponseBody ResponseResult logType(@RequestParam(value = "type", required = false) String type) throws Exception {
		if(StringUtils.isNotEmpty(type)){
			List<LoggerRecordDto> list=new ArrayList<>();
			setLoggerList(list, type, null, null, null);
			responseResult.initResult(GTAError.Success.SUCCESS,list);
		}else{
		  responseResult.initResult(GTAError.Success.SUCCESS, (Object)appProps.getProperty("tomcat.node.size"));
		}
		return responseResult;
	}
	@RequestMapping(value = "/sys/logTypeInfo", method = RequestMethod.GET)
	public @ResponseBody ResponseResult logTypeInfo(@RequestParam(value = "fileName", required = true) String fileName,@RequestParam(value = "terminal", required = true) String terminal) throws Exception {
		/**
		 * ${LOG_DIR}/${TOMCAT_ID}/hkgta/ddx.log
		 */
		String path =appProps.getProperty("log.dir")+ File.separator+ terminal+File.separator
				+ File.separator + "hkgta" + File.separator;
 		File file=new File(path+fileName);
 		String content=null;
		if(file.exists()){
		  content=FileUtil.readAsString(file);
		}
		 responseResult.initResult(GTAError.Success.SUCCESS, (Object)content);
		 return responseResult;
	}
	
	@RequestMapping(value = "/sys/logTypeDetail", method = RequestMethod.GET)
	public @ResponseBody ResponseResult logTypeDetail(@RequestParam(value = "type", required = false) String type,
			@RequestParam(value = "date", required = false) String date) throws Exception {
		// /LOGS/${TOMCAT_ID}/hkgta/zoomtech.log？
		String nodeName = System.getenv().get(LoggerType.TOMCAT_ID.getFileName());
		// logs/tomcat FE add indicate
		String rootPath = nodeName.substring(0, nodeName.length()-1);

		String path = File.separator + "hkgta" + File.separator;
		// get all log
		List<LoggerRecordDto> list = new ArrayList<>();
		setLoggerList(list, type, rootPath, path, date);
		responseResult.initResult(GTAError.Success.SUCCESS, list);

		return responseResult;
	}

	private void setLoggerList(List<LoggerRecordDto> list, String type, String rootPath, String path, String date) {
		String time = null;
		if (StringUtils.isEmpty(date)) {
			time = DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd");
		} else {
			time = date;
		}
		Date currentDate = DateConvertUtil.parseString2Date(DateConvertUtil.parseDate2String(new Date(), "yyyy-MM-dd"),
				"yyyy-MM-dd");
		Date paramDate = DateConvertUtil.parseString2Date(time, "yyyy-MM-dd");

		for (LoggerType log : LoggerType.values()) {
			if (!log.name().equalsIgnoreCase(LoggerType.TOMCAT_ID.name())&&!log.name().equalsIgnoreCase(LoggerType.LOG_DIR.name())) {
				
				if (StringUtils.isEmpty(type)) {
					LoggerRecordDto record = new LoggerRecordDto();
					record.setLoggerType(log.getDesc());
					record.setDate(time);
					record.setRootPath(rootPath);
					record.setType(log.getFileName());
					
					if (paramDate.compareTo(currentDate) == 0 || paramDate.after(currentDate)) {
						record.setFilePath(path + log.getFileName());
						record.setFileName(log.getFileName());
					} else {
						record.setFilePath(path + log.getFileName() + "." + time);
						record.setFileName(log.getFileName() + "." + time);
					}
					list.add(record);
					
				} else {
					if (type.equalsIgnoreCase(log.getDesc())) {
						LoggerRecordDto record = new LoggerRecordDto();
						record.setLoggerType(log.getDesc());
						record.setDate(time);
						record.setRootPath(rootPath);
						record.setType(log.getFileName());
						
						if (paramDate.compareTo(currentDate) == 0 || paramDate.after(currentDate)) {
							record.setFilePath(path + log.getFileName());
							record.setFileName(log.getFileName());
						} else {
							record.setFilePath(path + log.getFileName() + time);
							record.setFileName(log.getFileName() + "." + time);
						}
						list.add(record);
					}
				}
			

			}
		}
	}
	/**
	 * 通用接口，根据category，获取SysCodeList
	 * @param category
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/getSysCodeList", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getSysCodeList(@RequestParam(value = "category", required = false) String category) throws Exception {
		responseResult.initResult(GTAError.Success.SUCCESS, sysCodeService.getSysCodeByCategory(category));
		return responseResult;
	}
	@RequestMapping(value = "/sys/downloadLogTypeInfo", method = RequestMethod.GET)
	public void downloadLogTypeInfo(@RequestParam(value = "fileName", required = true) String fileName,@RequestParam(value = "terminal", required = true) String terminal,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		String path =appProps.getProperty("log.dir")+ File.separator+ terminal+File.separator
				+ File.separator + "hkgta" + File.separator;
 		File file=new File(path+fileName);
 		if(file.exists()){
 			response.setHeader("Content-Disposition",
 					"attachment; filename=" + file.getName()); 
 			response.getOutputStream().write(FileUtils.readFileToByteArray(file));	
 		}
 		
	}
	
	/**
	 * 获取所有未同步的member列表
	 * @param academyNo 多个academyNo,号分割 可空
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/getSyncFailureMemberList", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getSyncFailureMemberList(@RequestParam(value = "academyNo", required = false) String academyNos) throws Exception {
		responseResult.initResult(GTAError.Success.SUCCESS, memberSynchronizeService.manualSyncMemberByAcademyNos(academyNos));
		return responseResult;
	}
	
	/**
	 * 手动同步所有Member到MMS
	 * @param academyNo 多个academyNo,号分割 可空
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/manualSyncMemberByMMS", method = RequestMethod.GET)
	public @ResponseBody ResponseResult manualSyncMemberByMMS(@RequestParam(value = "academyNo", required = false) String academyNos) throws Exception {
		int i = memberSynchronizeService.manualSyncMemberByMMS(academyNos);
		memberSynchronizeTaskService.memberSynchronize(academyNos);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	/**
	 * 获取BiOasisFlexFile每天列表
	 * @param category
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/getBioasisFlexFile", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getBioasisFlexFile() throws Exception {
		List<BiOasisFlexFile> list = biOasisFlexFileService.getBioasisFlexFile();
		responseResult.initResult(GTAError.Success.SUCCESS, list);
		return responseResult;
	}
	/**
	 * 导入报表文件
	 * @param txtFile
	 * @return
	 */
	@RequestMapping(value = "/sys/importRevenueReport", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult importDeskStatusReport(@RequestParam("file") final MultipartFile txtFile) {
		try {
			File sourceFile = getUploadtxtFile(txtFile);
			responseResult.initResult(GTAError.Success.SUCCESS, biOasisFlexFileService.importTxtFile(sourceFile));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	
	public File getUploadtxtFile(MultipartFile txtFile) throws Exception{
		String path = oasisProps.getProperty(Constant.IMPORT_FILE_PATH);
		File file = new File(path);
		if (!file.exists() && !file.isDirectory()) {
			file.mkdirs();
		}
		File targetFile = new File(path, txtFile.getOriginalFilename());
		txtFile.transferTo(targetFile);
		return targetFile;
	}
	
	/**
	 * 获取问卷设置
	 * @param category
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/getQuestionnaireSetting", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getQuestionnaireSetting() throws Exception {
		responseResult.initResult(GTAError.Success.SUCCESS, surveyService.getQuestionnaireSetting());
		return responseResult;
	}
	
	/**
	 * 更新问卷设置
	 * @param category
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sys/updateQuestionnaireSetting", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateQuestionnaireSetting(@RequestBody QuestionnaireSettingListDto info) throws Exception {
		return surveyService.updateQuestionnaireSetting(info, getUser().getUserId());
	}
	
	/**
	 * 获取心跳检查列表
	 * @param sortBy
	 * @param pageNumber
	 * @param pageSize
	 * @param isAscending
	 * @param byMajor
	 * @param status
	 * @param filters
	 * @param targetType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/sys/getHealthCheckingList", method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult getHealthCheckingList(@RequestParam(value="sortBy", defaultValue = "lastAccessTime") String sortBy,
			   @RequestParam(value="pageNumber", defaultValue = "1") Integer pageNumber,
			   @RequestParam(value="pageSize", defaultValue = "10")  Integer pageSize,
			   @RequestParam(value="isAscending", defaultValue = "false") String isAscending,
			   @RequestParam(value="status", defaultValue = "ALL") String status,
			   @RequestParam(value="filters",defaultValue="", required = false) String filters,
			   @RequestParam(value="appTypeCode", defaultValue = "KIOSK") String appTypeCode
			) {
		
		if(isAscending.equals("true")){
			page.addAscending(sortBy);
		}else if (isAscending.equals("false")){
			page.addDescending(sortBy);
		}
		ListPage<LoginSession> page = new ListPage<LoginSession>(pageSize, pageNumber);
//		page.setNumber(pageNumber);
//		page.setSize(pageSize);
		
		ResponseResult responseResult = userMasterService.getHealthCheckingList(page, appTypeCode);
		return responseResult;
	}
	
	
}