package com.sinodynamic.hkgta.controller.common;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.portlet.ModelAndView;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.util.SessionMap;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
public class KaptchaController extends ControllerBase<String>
{
	private static final 	Logger logger = Logger.getLogger(KaptchaController.class);
	
	private static String KAPTCHA_FOLDER = "image.server.kaptcha";

	@Autowired
	@Qualifier("captchaProducer")
	public Producer captchaProducer;

	@RequestMapping(value = "/kaptcha.jpg", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		// Set to expire far in the past.
		response.setDateHeader("Expires", 0);
		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		// Set IE extended HTTP/1.1 no-cache headers (use addHeader).
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");

		// return a jpeg
		response.setContentType("image/jpeg");

		// create the text for the image
		String capText = captchaProducer.createText();

		// store the text in the session
		request.getSession().setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);

		// create the image with the text
		BufferedImage bi = captchaProducer.createImage(capText);

		ServletOutputStream out = response.getOutputStream();

		// write the data out
		ImageIO.write(bi, "jpg", out);
		try
		{
			out.flush();
		}
		finally
		{
			out.close();
		}
		return null;
	}

	@RequestMapping(value = "/kaptcha.do", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getImg(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String capText = captchaProducer.createText();
		
		String path = request.getServletContext().getRealPath("/");
		path = path + "kaptcha";

		// store the text in the session
		request.getSession().setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);

		String sessionId = request.getSession().getId();
		String filename = sessionId + ".jpg";
		
		SessionMap.getInstance().addSession(sessionId, capText);

		// create the image with the text
		BufferedImage bi = captchaProducer.createImage(capText);

		
		File folder=new File(path);    
		if (!folder.exists() && !folder.isDirectory()){       
			folder.mkdir();    
		}
		
		FileOutputStream o = new FileOutputStream(new File(path + File.separator + filename));

		// write the data out
		ImageIO.write(bi, "jpg", o);
		// ImageIO.write(bi, "jpg", out);
		try
		{
			o.flush();
		}
		finally
		{
			o.close();
		}

		Map<String, String> filepath = new HashMap<String, String>();
		filepath.put("codepath", request.getContextPath() + "/kaptcha/" + filename);
		responseResult.initResult(GTAError.Success.SUCCESS, filepath);
		return responseResult;

	}
}