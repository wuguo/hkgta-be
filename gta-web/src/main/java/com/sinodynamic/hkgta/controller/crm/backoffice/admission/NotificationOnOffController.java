package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.UserPreferenceSettingDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.UserPreferenceSetting;
import com.sinodynamic.hkgta.entity.pos.RestaurantMaster;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.NotificationOnOffService;
import com.sinodynamic.hkgta.service.pos.RestaurantMasterService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RestaurantSwitchEnum;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/notificationOnOff")
@Scope("prototype") 
public class NotificationOnOffController  extends ControllerBase{
    
	public static final String ON = "On";
	
	public static final String Off= "Off";
	
	private Logger logger = Logger.getLogger(NotificationOnOffController.class);
	 
	@Autowired
	private NotificationOnOffService notificationOnOffService;
	
	@Autowired
	private RestaurantMasterService restaurantMasterService;
	 
	@RequestMapping(value = "/getNotificationSetting", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult getNotificationSettings(@RequestParam(value = "customerId") @EncryptFieldInfo Long customerId){
		
		try {
			if (StringUtils.isEmpty(customerId)){
				responseResult.initResult(GTAError.CommonError.PARAMETER_MISSING);
				return responseResult;
			}
			
			List<UserPreferenceSettingDto> list = notificationOnOffService.getNotificationSettings(customerId);
			Date d = new Date();
			for (UserPreferenceSettingDto dto : list){
				if (StringUtils.isEmpty(dto.getParamValue())){
					dto.setParamValue(ON);
					dto.setUpdateDate(d);
				}
			}
			
			Data data = new Data();
			data.setList(list);
			responseResult.initResult(GTAError.Success.SUCCESS, data);
			
		} catch (Exception e) {			
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/updateNotificationSetting", method = { RequestMethod.POST })
	@ResponseBody
	public ResponseResult updateNotificationStatusTo(@RequestBody UserPreferenceSetting setting){
		
		try {
			String userId = setting.getId().getUserId();
			String paramId = setting.getId().getParamId();
			String status = setting.getParamValue();
			if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(paramId) || StringUtils.isEmpty(status)){
				responseResult.initResult(GTAError.CommonError.PARAMETER_MISSING);
				return responseResult;
			}
			
			if ((!ON.equals(status)) && (!Off.equals(status))){
				responseResult.initResult(GTAError.CommonError.PARAMETER_VALUE_INVALID);
				return responseResult;
			}
			
			notificationOnOffService.updateNotificationSetting(userId, paramId, status);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/updateRestaurantNotificationSetting", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult updateNotificationStatusTo(@RequestParam(value = "userId") String userId,
			@RequestParam(value = "paramId") String paramId,
			@RequestParam(value = "status") String status){
		
		try {
			if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(paramId) || StringUtils.isEmpty(status)){
				responseResult.initResult(GTAError.CommonError.PARAMETER_MISSING);
				return responseResult;
			}
			
			if ((!ON.equals(status)) && (!Off.equals(status))){
				responseResult.initResult(GTAError.CommonError.PARAMETER_VALUE_INVALID);
				return responseResult;
			}
			
			for(RestaurantSwitchEnum switchEnum :RestaurantSwitchEnum.values()){
				if(switchEnum.getDesc().equals(paramId)){
					paramId = switchEnum.toString();
					break;
				}
			}
			
			notificationOnOffService.updateNotificationSetting(userId, paramId, status);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	
	@RequestMapping(value = "/getRestaurantNotificationSetting", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult getRestaurantNotificationSetting(@RequestParam(value = "userId") String userId){
		
		try {
			if (StringUtils.isEmpty(userId)){
				responseResult.initResult(GTAError.CommonError.PARAMETER_MISSING);
				return responseResult;
			}
			
			List<UserPreferenceSettingDto> list = notificationOnOffService.getRestaurantNotificationSettings(userId);
			Date date = new Date();
			for (UserPreferenceSettingDto dto : list){
				if (StringUtils.isEmpty(dto.getParamValue())){
					dto.setParamValue(Off);
				}
				dto.setUpdateDate(date);
				for(RestaurantSwitchEnum switchEnum :RestaurantSwitchEnum.values()){
					if(switchEnum.toString().equals(dto.getParamId())){
						dto.setParamId(switchEnum.getDesc());
						break;
					}
				}
				RestaurantMaster restaurantMaster = this.restaurantMasterService.getRestaurantMaster(dto.getParamId());
				if(restaurantMaster != null){
					dto.setRestaurantName(restaurantMaster.getRestaurantName());
				}
			}
			
			Data data = new Data();
			data.setList(list);
			responseResult.initResult(GTAError.Success.SUCCESS, data);
			
		} catch (Exception e) {			
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
}
