package com.sinodynamic.hkgta.controller.crm.backoffice.membership;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CardsListResponseDto;
import com.sinodynamic.hkgta.dto.crm.PermitCardMasterDto;
import com.sinodynamic.hkgta.dto.crm.SearchRuleDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.adm.PermitCardMaster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.DependentMemberService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MembershipCardsManagmentService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.QRCodeUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/cards")
public class PermitCardMasterController extends ControllerBase {

	private Logger										logger	= Logger.getLogger(PermitCardMasterController.class);

	@Autowired
	private PermitCardMasterService						permitCardMasterService;

	private @Autowired DependentMemberService			departmentMemberService;

	private @Autowired MembershipCardsManagmentService	membershipCardsManagmentService;

	@Autowired
	private AdvanceQueryService							advanceQueryService;

	@RequestMapping(value = "/{cardNo}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg modifyPermitCardMaster(@PathVariable(value = "cardNo") String cardNo, @RequestBody PermitCardMasterDto dto) {

		logger.info("PermitCardMasterController.modifyPermitCardMaster invocation start ...");

		if (CommUtil.nvl(cardNo, null) == null) {

			logger.error("cardNo is null");
			// return new ResponseMsg("-1", "cardNo can not be null");
			responseResult.initResult(GTAError.AcademyCardError.CARD_NO_IS_NULL);
			return responseResult;
		}

		LoginUser user = super.getUser();
		if (user == null) {

			logger.error("current user is null!");
			// return new ResponseMsg("-2", "can not get current user!");
			responseResult.initResult(GTAError.AcademyCardError.USER_IS_NULL);
			return responseResult;
		}

		String userId = user.getUserId();
		dto.setUpdateBy(userId);

		try {

			if (StringUtils.isNotEmpty(dto.getStatus())) {
				permitCardMasterService.changePermitCardMasterStatus(cardNo, dto);
			} else if (StringUtils.isNotEmpty(dto.getCustomerId() + "")) {
				PermitCardMaster permitCardMaster = permitCardMasterService.getPermitCardMaster(CommUtil.cardNoTransfer(cardNo));
				if (null == permitCardMaster) {
					// return new ResponseMsg("-1", "PermitCardMaster with cardNo : " + cardNo + " is not exist!");
					responseResult.initResult(GTAError.AcademyCardError.CARDNO_NOT_EXISTS, new Object[] { cardNo });
					return responseResult;
				}
				if (null != permitCardMaster.getMappingCustomerId()) {
					// return new ResponseMsg("-1", "PermitCardMaster with cardNo : " + cardNo + " has been linked!");
					responseResult.initResult(GTAError.AcademyCardError.CARDNO_HAS_BEEN_LINKED, new Object[] { cardNo });
					return responseResult;
				}
				String result = membershipCardsManagmentService.linkCardForMember(
						CommUtil.cardNoTransfer(cardNo),
						dto.getCustomerId(),
						userId);
				if (!"0".equals(result)) {
					// return new ResponseMsg("-1", result);
					responseResult.initResult(GTAError.AcademyCardError.FAILED_LINK_CARD_FOR_MEMBER, new Object[] { result });
					return responseResult;
				}
			} else {
				// return new ResponseMsg("-3", "parameter error!");
				responseResult.initResult(GTAError.AcademyCardError.PARAMETER_ERROR);
				return responseResult;
			}

			logger.info("PermitCardMasterController.modifyPermitCardMaster invocation end ...");
			// return new ResponseMsg("0", "Success!");
			responseResult.initResult(GTAError.Success.SUCCESS, "Success!");
			return responseResult;

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	@RequestMapping(value = "/linkcard/{cardNo}", method = RequestMethod.POST)
	public @ResponseBody ResponseResult linkCard(@PathVariable(value = "cardNo") String cardNo, @RequestBody PermitCardMasterDto dto) {

		logger.info("PermitCardMasterController.linkCard invocation start ...");

		try {

			return permitCardMasterService.linkCard(cardNo, dto, getUser().getUserId());

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	// @RequestMapping(value = "/replacement/{cardNo}", method = RequestMethod.PUT)
	// @ResponseBody
	// public ResponseMsg replaceCustomerCard(@PathVariable(value="cardNo") String oldCardNo, @RequestBody PermitCardMasterDto dto) {
	//
	// logger.info("PermitCardMasterController.replaceCustomerCard invocation start ...");
	//
	// String newCardNo = dto.getCardNo();
	// if (CommUtil.nvl(newCardNo, null) == null || CommUtil.nvl(oldCardNo, null) == null) {
	// logger.error("Request parameter error!");
	// return new ResponseMsg("-1", "Request parameter error!");
	// }
	//
	// LoginUser user = super.getUser();
	// if (user == null) {
	//
	// logger.error("current user is null!");
	// return new ResponseMsg("-2", "can not get current user!");
	// }
	//
	//
	// try {
	//
	// dto.setUpdateBy(user.getUserId());
	// dto.setCardNo(CommUtil.cardNoTransfer(newCardNo));
	//
	// permitCardMasterService.replaceOldPermitCardMaster(oldCardNo, dto);
	//
	//
	// } catch(RuntimeException e) {
	//
	// String message = e.getMessage();
	// if ("-1".equals(message)) {
	//
	// logger.error("cardNo is not exist!");
	// return new ResponseMsg("-3", "The card you want to be replaced does not exist!");
	// } else if ("-2".equals(message)) {
	//
	// logger.error("cardNo is not exist!");
	// return new ResponseMsg("-3", "The card you want to replace does not exist!");
	// } else if ("-3".equals(message)) {
	//
	// logger.error("The card status is issued and being used by others!");
	// return new ResponseMsg("-3", "The card status is issued and being used by others, please use another card!");
	// }
	//
	// } catch(Exception e) {
	//
	// e.printStackTrace();
	// return new ResponseMsg("-4", e.toString());
	// }
	//
	// logger.info("PermitCardMasterController.replaceCustomerCard invocation end ...");
	// return new ResponseMsg("0", null);
	// }

	@RequestMapping(value = "/print/{customerId}", method = RequestMethod.GET)
	public @ResponseBody ResponseMsg getMemberInfoForPrintCard(@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId) {
		logger.info("PermitCardMasterController.getMemberInfoForPrintCard invocation start ...");
		if (null == customerId) {
			responseResult.initResult(GTAError.CommomError.NO_DATA_FOUND);
			return responseResult;
		}

		try {
			return membershipCardsManagmentService.getMemberCardPrintInfo(customerId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	
	@RequestMapping(value="/printMemberCard/{customerId}", produces="application/pdf", method = RequestMethod.GET)
	public void printCardInfo(@PathVariable @EncryptFieldInfo Long customerId,HttpServletRequest request, HttpServletResponse response){
	    response.setContentType("application/pdf");
	    InputStream inputStream = null;
	    OutputStream outputStream = null;
		logger.info("printCardInfo start");
	    try{
	        inputStream =new  ByteArrayInputStream( membershipCardsManagmentService.getAcademyCardPrintInfo(customerId)) ;
	        outputStream = response.getOutputStream();
	        IOUtils.copy(inputStream, outputStream);
	    }catch(Exception e){
	    	logger.error(e.getMessage(), e);
	    }finally{
	        IOUtils.closeQuietly(inputStream);
	        IOUtils.closeQuietly(outputStream);
	    }
	}
	
//	@RequestMapping(value="/printMemberCard/{customerId}", produces="application/pdf", method = RequestMethod.GET)
//	@ResponseBody
//	public byte[] printCardInfo(@PathVariable @EncryptFieldInfo Long customerId){
//		try {
//			logger.debug("printCardInfo");
//			return membershipCardsManagmentService.getAcademyCardPrintInfo(customerId) ;
//		} catch (Exception e) {
//			logger.debug("CustomerEnrollment.getEnrollForm Exception:", e);
//		}
//		return null;
//	}

	// /**
	// * This method is used to retrieve card information for members.
	// * @param fromDate
	// * @param toDate
	// * @param status
	// * @param pageNumber
	// * @param pageSize
	// * @return ResponseResult (list of cards, pagination details etc)
	// * @author Vineela_Jyothi
	// */
	// @SuppressWarnings("unchecked")
	// @RequestMapping(value = "/list", method = RequestMethod.GET)
	// @ResponseBody
	// public ResponseResult getCards(@RequestParam(value="fromDate", required=false) String fromDate,
	// @RequestParam(value="toDate", required=false, defaultValue="") String toDate,
	// @RequestParam(value="status", required=false, defaultValue="") String status,
	// @RequestParam(value="customerId", required=false, defaultValue="") String customerId,
	// @RequestParam(value="sortBy", defaultValue = "cardIssueDate") String sortBy,
	// @RequestParam(value="isAscending", defaultValue = "false") String isAscending,
	// @RequestParam(value="pageNumber" , required = false, defaultValue = "1") int pageNumber,
	// @RequestParam(value="pageSize" , required = false, defaultValue = "3") int pageSize){
	//
	//
	// page.setNumber(pageNumber);
	// page.setSize(pageSize);
	//
	// if(isAscending.equals("true")){
	// page.addAscending(sortBy);
	//
	// }else if (isAscending.equals("false")){
	// page.addDescending(sortBy);
	//
	// }
	//
	// try {
	//
	// PermitCardMasterDto dto = new PermitCardMasterDto(fromDate, toDate, status, customerId, pageNumber, pageSize);
	// ResponseResult result = membershipCardsManagmentService.getCards(page, dto);
	// return result;
	//
	// } catch (Exception e) {
	// logger.error(PermitCardMasterController.class.getName() + " getCards Failed!", e);
	// responseResult.initResult(GTAError.AcademyCardError.FAIL_RETRIEVE_MEMBER_LIST);
	// return responseResult;
	// }
	// }

	@RequestMapping(value = "/list", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult getCards(@RequestBody(required = false) AdvanceQueryDto filters,
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "sortBy", defaultValue = "cardIssueDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "3") int pageSize) {

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try {

			String joinHQL = membershipCardsManagmentService.getCards(status);
			if (filters != null && filters.getRules().size() > 0 && filters.getGroupOp() != null && filters.getGroupOp().length() > 0) {

				joinHQL += " WHERE ";
				filters.setSortBy(sortBy);
				filters.setAscending(isAscending);
				// added by vicky wang to fix the bug of advance search by issue date.
				for (SearchRuleDto rule : filters.getRules()) {
					if (logger.isDebugEnabled()) {
						logger.debug("rule field:" + rule.getField() + " op:" + rule.getOp() + " data:" + rule.getData());
					}
					if ("cardIssueDate".equals(rule.field)) {
						rule.setField("date_format(" + rule.field + ",'%Y-%m-%d')");
						if (logger.isDebugEnabled()) {
							logger.debug("updated cradIssueDate:" + rule.getField());
						}
					}
				}
				// added end

				return advanceQueryService.getAdvanceQueryCustomizedResultBySQL(filters, joinHQL, page, CardsListResponseDto.class);
			} else {
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				return advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, CardsListResponseDto.class);
			}

		} catch (Exception e) {
			logger.error(PermitCardMasterController.class.getName() + " getCards Failed!", e);
			responseResult.initResult(GTAError.AcademyCardError.FAIL_RETRIEVE_MEMBER_LIST);
			return responseResult;
		}
	}

	@RequestMapping(value = "/previewCard", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPreviewCardInfo(@RequestParam(value = "customerId", required = true) @EncryptFieldInfo Long customerId) {

		return permitCardMasterService.getPreviewCardInfo(customerId);

	}

	@RequestMapping(value = "/academyNoQrCode", method = RequestMethod.GET, produces = { "image/jpg" })
	@ResponseBody
	public byte[] getQRcode(@RequestParam(value = "academyNo", required = true) String academyNo, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			QRCodeUtil generater = new QRCodeUtil();
			BufferedImage bi = generater.encoderQRCode(academyNo);
			InputStream is = QRCodeUtil.BufferedImage2InputStream(bi);
			byte[] byteArray = IOUtils.toByteArray(is);
			return byteArray;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}

	}

}
