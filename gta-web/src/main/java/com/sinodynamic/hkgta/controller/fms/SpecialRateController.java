package com.sinodynamic.hkgta.controller.fms;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateDateDto;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.service.fms.FacilityUtilizationRateDateService;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;

@Controller
@RequestMapping("/facility")
public class SpecialRateController extends ControllerBase<FacilityMaster> {

	@Autowired
	private FacilityUtilizationRateDateService	facilityUtilizationRateDateService;

	/**
	 * get Facility General Price
	 * 
	 * @return Facility Price
	 */
	@RequestMapping(value = "/specialRate/{facilityType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg getFacilitySpecialRate(@PathVariable(value = "facilityType") String facilityType,
			@RequestParam(value = "sortBy") String sortBy, @RequestParam(value = "ascending") String ascending,
			@RequestParam(value = "subType", required = false) String subType) {
		logger.info("FacilityController.getFacilitySpecialRate() invocation start...");
		try {
			logger.info("FacilityController.getFacilitySpecialRate() invocation end...");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			Map<String, Object> responseMap = new HashMap<String, Object>();
			List<FacilityUtilizationRateDateDto> dtos = facilityUtilizationRateDateService.getSpecialRateDateList(
					facilityType.toUpperCase(),
					calendar.get(Calendar.YEAR),
					sortBy,
					ascending,
					subType);
			responseMap.put("RateDateList", dtos);
			responseResult.initResult(GTAError.FacilityError.SUCCESS, responseMap);
			return responseResult;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * update Facility General Price
	 * 
	 * @return responseResult
	 */
	@RequestMapping(value = "/specialRate/{facilityType}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg updateFacilitySpecialRate(@PathVariable(value = "facilityType") String facilityType,
			@RequestBody FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) {
		logger.info("FacilityController.updateFacilitySpecialRate() invocation start...");
		try {
			if (facilityUtilizationRateDateService.checkSpecialDateExistanceById(facilityUtilizationRateDateDto.getDateId()).equals(false)) {
				responseResult.initResult(GTAError.FacilityError.DATE_ID_NOT_EXIST);
				return responseResult;
			}
			if (validateFacilityUtilizationRateDateDto(facilityUtilizationRateDateDto).equals(false)) {
				return responseResult;
			}
			;
			facilityUtilizationRateDateDto.setUpdatedBy(this.getUser().getUserName());
			facilityUtilizationRateDateService.updateSpecialRateDate(facilityType.toUpperCase(), facilityUtilizationRateDateDto);
			logger.info("FacilityController.updateFacilitySpecialRate() invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
			return responseResult;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * delete Facility General Price
	 * 
	 * @return Facility Price
	 */
	@RequestMapping(value = "/specialRate/{dateId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseMsg deleteFacilitySpecialRate(@PathVariable(value = "dateId") int dateId) {
		logger.info("FacilityController.deleteFacilitySpecialRate() invocation start...");
		try {
			facilityUtilizationRateDateService.deleteSpecialRateDate(dateId);
			logger.info("FacilityController.deleteFacilitySpecialRate() invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
			return responseResult;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * get Facility of special date
	 * 
	 * @return responseResult
	 */
	@RequestMapping(value = "/specialRateOfDay/{facilityType}/{specialDate}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg getFacilitySpecialRateByDate(@PathVariable(value = "facilityType") String facilityType,
			@PathVariable(value = "specialDate") String specialDate, @RequestParam(value = "subType", required = false) String subType) {
		logger.info("FacilityController.getFacilitySpecialRate() invocation start...");
		try {
			logger.info("FacilityController.getFacilitySpecialRate() invocation end...");
			responseResult.initResult(
					GTAError.FacilityError.SUCCESS,
					facilityUtilizationRateDateService.getSpecialRateDateListByDate(facilityType.toUpperCase(), specialDate, subType));
			return responseResult;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * save Facility General Price
	 * 
	 * @return responseResult
	 */
	@RequestMapping(value = "/specialRate/{facilityType}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMsg saveFacilitySpecialRate(@PathVariable(value = "facilityType") String facilityType,
			@RequestBody final FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) {
		logger.info("FacilityController.saveFacilitySpecialRate() invocation start...");
		try {
			if (facilityUtilizationRateDateService.checkSpecialDateExistance(
					facilityUtilizationRateDateDto.getSpecialDate(),
					facilityType.toUpperCase(),
					facilityUtilizationRateDateDto.getFacilitySubTypeId()).equals(true)) {
				responseResult.initResult(GTAError.FacilityError.SPECIDAL_DATE_EXISTS);
				return responseResult;
			}
			if (validateFacilityUtilizationRateDateDto(facilityUtilizationRateDateDto).equals(false)) {
				return responseResult;
			}
			;
			facilityUtilizationRateDateDto.setUpdatedBy(this.getUser().getUserName());
			facilityUtilizationRateDateService.saveSpecialRateDate(facilityType.toUpperCase(), facilityUtilizationRateDateDto);
			logger.info("FacilityController.saveFacilitySpecialRate() invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
			return responseResult;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	private Boolean validateFacilityUtilizationRateDateDto(final FacilityUtilizationRateDateDto facilityUtilizationRateDateDto) {
		if (facilityUtilizationRateDateDto.getDateDesc() != null && facilityUtilizationRateDateDto.getDateDesc().length() > 100) {
			responseResult.initResult(GTAError.FacilityError.DESCRIPTION_SHOULD_BE_SHORTER_THAN_100);
			return false;
		}
		if (facilityUtilizationRateDateDto.getDateDesc() == null || facilityUtilizationRateDateDto.getDateDesc().length() == 0) {
			responseResult.initResult(GTAError.FacilityError.DESCRIPTION_SHOULD_NOT_BE_EMPTY);
			return false;
		}
		return true;
	}

}
