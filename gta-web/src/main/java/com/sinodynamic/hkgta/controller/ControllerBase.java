package com.sinodynamic.hkgta.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.setting.UserPreferenceSettingService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.CustomObjectMapper;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.SessionMap;
import com.sinodynamic.hkgta.util.SmartDatetimeFormat;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.UserPreferenceSettingParam;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * 
 * @author refactor by Johnmax_Wang
 * @date   Apr 28, 2015
 * @param <T>
 */

public class ControllerBase<T extends Serializable> {

   protected  final Logger logger = Logger.getLogger(ControllerBase.class); 
   protected CommUtil uc=new CommUtil(); 
   
   @Resource(name="appProperties") 
   protected Properties appProps;
      
   protected ListPage<T> page = new ListPage<T>();
  
//   protected ResponseMsg responseMsg = new ResponseMsg();
   
   @Autowired
   protected ResponseMsg responseMsg;
   
   @Autowired
   protected ResponseResult responseResult;
   
   @Autowired
   private HttpServletRequest request;
   
   @Autowired
   protected MemberService memberService;
   @Autowired
   protected UserPreferenceSettingService  userPreferenceSettingService;
   
   /**
    * To customize the format for date parameter within this controller. 
    * To avoid error when passing empty date string parameter when submit from view page  
    * @param binder
    */
   
   @Resource(name="messageSource") 
   protected MessageSource messageSource;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
       SimpleDateFormat dateFormat = new SmartDatetimeFormat(appProps.getProperty("format.datetime"));
       dateFormat.setLenient(false);

       // true passed to CustomDateEditor constructor means convert empty String to null
       binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
   }
   
   	@ExceptionHandler(RuntimeException.class)
	public void exceptionHandler(GTACommonException ex, HttpServletResponse response, HttpServletRequest request) throws JsonGenerationException, JsonMappingException, IOException
	{
		logger.error(ex.getMessage(),ex);
		responseResult.initResult(ex.getError(), ex.getArgs());
		if(null != ex.getErrorMsg())
		{
			responseResult.initResult(ex.getError(), ex.getErrorMsg());
		}
		response.getOutputStream().write((new CustomObjectMapper().writeValueAsString(responseResult)).getBytes("UTF-8"));	
		response.setContentType("application/json; charset=UTF-8");
	}
   
   /**
    * This method has moved into ResponseMsg, ResponseMsg will handle all of the error message.
    *
    * get i18n message
    * @param msgCode
    * @return
    */
   @Deprecated
   public String getI18nMessge(String msgCode) {
	   return this.getI18nMessge(msgCode, null, LocaleContextHolder.getLocale());	   
   }
   
   /**
    * This method has moved into ResponseMsg, ResponseMsg will handle all of the error message.
    * get i18 message with parameters, just like {0} {1}
    * @param msgCode
    * @param args
    * @return
    */
   public String getI18nMessge(String msgCode, Object[] args) {
	   return this.getI18nMessge(msgCode, args, LocaleContextHolder.getLocale());	   
   }
   
   /**
    * This method has moved into ResponseMsg, ResponseMsg will handle all of the error message.
    */
   @Deprecated
   public String getI18nMessge(String msgCode, Locale locale) {
	   return this.getI18nMessge(msgCode, null, locale);	   
   }
   
   /**
    * This method has moved into ResponseMsg, ResponseMsg will handle all of the error message.
    */
   @Deprecated
   public String getI18nMessge(String msgCode, Object[] args, Locale locale) {
	   return messageSource.getMessage(msgCode, args, locale);	   
   }
   
   public Map<String,String> initErrorMsg(GTAError error)
   {
	   Map<String,String> errorMsg = new HashMap<String,String>();
	   errorMsg.put(error.getCode(), getI18nMessge(error.getCode()));
	   return errorMsg;
   }
   
   public Map<String,String> initErrorMsg(GTAError error, Object[] args)
   {
	   Map<String,String> errorMsg = new HashMap<String,String>();
	   errorMsg.put(error.getCode(), getI18nMessge(error.getCode(), args));
	   return errorMsg;
   }

   
   /**
    * 
    * @param request
    * @return parameter JSON
    * @throws Exception
    */
   protected String fetchRequestJson(HttpServletRequest request) throws IOException{
	    InputStream ism = null;
		try {
			ism = request.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(ism, "UTF-8"));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			return builder.toString();
		} catch (IOException e) {
			throw e;
		}finally{
			/*if(ism != null){
				ism.close();
			}*/
		}
   }
   
   /**
    * @param paraJson
    * @param classz
    * @return T
 * @throws ClassNotFoundException 
 * @throws IllegalAccessException 
 * @throws InstantiationException 
    */
	@SuppressWarnings("unchecked")
	protected T parseJson(String paraJson, Class<T> classz) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (paraJson != null && paraJson.length() > 0) {
			return new Gson().fromJson(paraJson, classz);
		}else{
			return (T) Class.forName(classz.getName()).newInstance();
		}
	}
	
	@SuppressWarnings("unchecked")
	protected T parseJson(HttpServletRequest request, Class<T> classz) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String paraJson = fetchRequestJson(request);
		logger.debug("paraJson:"+paraJson);
		System.out.println("paraJson:"+paraJson);
		JSONObject jsonObject = JSONObject.fromObject(paraJson);
		String dateOfBirth = "";
		try{
			if(StringUtils.isNotEmpty(paraJson) && paraJson.indexOf("dateOfBirth") != -1){
				dateOfBirth = jsonObject.getString("dateOfBirth");
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw new JSONException("dateOfBirth");
		}
		if ("".equals(dateOfBirth)){
			jsonObject.put("dateOfBirth", null);
			paraJson = jsonObject.toString();
		}
		
		if (paraJson != null && paraJson.length() > 0) {
			Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
			return gson.fromJson(paraJson, classz);
		}else{
			return (T)Class.forName(classz.getName()).newInstance();
		}
	}
	
	/**
	 * 
	 * @param page
	 * @param response
	 * @throws IOException 
	 * @throws Exception
	 */
	protected void writeJson(ListPage<T> page,HttpServletResponse response) throws IOException{
		response.setContentType("text/html; charset=UTF-8");
		response.getWriter().write(new Gson().toJson(page.getList()));
	}
	
	/**
	 * 
	 * @param t
	 * @param response
	 * @throws IOException 
	 * @throws Exception
	 */
	protected void writeJson(T t,HttpServletResponse response) throws IOException{
		response.setContentType("text/html; charset=UTF-8");
		response.getWriter().write(new Gson().toJson(t));
		logger.info("");
	}
   
	protected void writeJson(Object t,HttpServletResponse response) throws IOException{
		response.setContentType("text/html; charset=UTF-8");
		response.getWriter().write(new Gson().toJson(t));
	}
	
	protected String getLoginLocation()
	{
		return SessionMap.getInstance().getLocation(getUser().getUserId());
	}
	
	protected LoginUser getUser(){
		Object userObj = request.getAttribute("User");
		if (null == userObj)
		{
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority("STAFF"));
			LoginUser user =  new LoginUser("-1", "", authorities);
			user.setUserId("-1");
			return user;
		}
		else
		{
			return (LoginUser)userObj;
		}
	}

	public void prepareQueryParameter(Integer pageNumber, Integer pageSize, String sortBy, String isAscending, AdvanceQueryDto queryDto) {
		queryDto.setSortBy(sortBy);
		queryDto.setAscending(isAscending);
		page.setNumber(pageNumber);
		page.setSize(pageSize);
	}

	public boolean isFilterEmpty(String filters) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		if(StringUtils.isEmpty(filters)){
			return true;
		}
		AdvanceQueryDto queryDto;
		if ( filters.length() > 0) {
			queryDto= new Gson().fromJson(filters, AdvanceQueryDto.class);
		}else{
			queryDto= (AdvanceQueryDto) Class.forName(AdvanceQueryDto.class.getName()).newInstance();
		}
		return  !(queryDto.getRules().size() > 0 && queryDto.getGroupOp() != null && queryDto.getGroupOp().length() > 0);
	}
	public boolean sendSMS(Long customerId){
		return userPreferenceSettingService.sendSMSByCustomerId(customerId);
	}

}
