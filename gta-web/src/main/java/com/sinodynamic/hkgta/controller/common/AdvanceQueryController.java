package com.sinodynamic.hkgta.controller.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dao.AdvanceQueryConditionDao;
import com.sinodynamic.hkgta.dto.Constant.memberType;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryConditionDto;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.fms.PaymentFacilityDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService.FacilityCode;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.StaffSitePoiService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.MemberQuickSearchService;
import com.sinodynamic.hkgta.service.crm.cardmanage.DayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.fms.CourseService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.AdvancePeriodType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.CourseType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/common")
public class AdvanceQueryController extends ControllerBase {

	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private DayPassPurchaseService dayPassPurchaseService;

	@Autowired
	private MemberQuickSearchService memberQuickSearchService;

	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	@Autowired
	private GlobalParameterService globalParameterService;
	@Autowired
	private CustomerServiceAccService customerServiceAccService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private UsageRightCheckService usageRightCheckService;
	
	@Autowired
	private StaffSitePoiService staffSitePoiService;

	@RequestMapping(value = "/advance/query", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult queryData(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam("joinHQL") String joinHQL,
			@RequestParam(value = "queryType", defaultValue = "HQL") String queryType,
			@RequestParam(value = "filters", defaultValue = "") String filters, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			// advance search
			if (StringUtils.isNotEmpty(filters)) {
				AdvanceQueryDto queryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				if (StringUtils.equals("HQL", queryType)) {
					return advanceQueryService.getAdvanceQueryResultByHQL(queryDto, joinHQL, page);
				}
				return advanceQueryService.getAdvanceQueryResultBySQL(queryDto, joinHQL, page, null);
			}
			// no advance condition search
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			return advanceQueryService.getInitialQueryResultByHQL(queryDto, joinHQL, page);
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	/**
	 * 根据模块名和类型获取高级查询条件
	 * 
	 * @param category
	 * @param type
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/advanceConditions/{category}/{type}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getConditions(@PathVariable("category") String category, @PathVariable("type") String type,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list;
			if (StringUtils.equalsIgnoreCase("code", type) || StringUtils.equalsIgnoreCase("Enroll", type)) {
				ServletContext context = request.getSession().getServletContext();
				WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
				list = advanceQueryService.assembleQueryConditions(category,
						(AdvanceQueryConditionDao) (ctx.getBean(category)));
			} else if (StringUtils.equalsIgnoreCase("Card", type) || StringUtils.equalsIgnoreCase("Debit", type)||StringUtils.equalsIgnoreCase("Oasis", type)
					|| StringUtils.equalsIgnoreCase("Marketing", type) || StringUtils.equalsIgnoreCase("MMS", type)) { // 获取staff
																			// card高级查询条件
																			// 或
																			// 获取cash
																			// value高级查询条件
				ServletContext context = request.getSession().getServletContext();
				WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
				list = advanceQueryService.assembleQueryConditions(category,
						(AdvanceQueryConditionDao) (ctx.getBean(category)), type);
			} else if (StringUtils.equalsIgnoreCase("GOLF", type) || StringUtils.equalsIgnoreCase("TENNIS", type)) {
				list = advanceQueryService.assembleQueryConditions(category, type);
			} else {
				list = advanceQueryService.assembleQueryConditions(category);
			}
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/card/{cardNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult mappingCustomerByCardNo(@PathVariable(value = "cardNo") String cardNo,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			return advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(cardNo)));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.AdvQueryError.CARD_MAPPING_FAILED);
			return responseResult;
		}
	}

	/***
	 * new add params customerId,reservationDate,facilityType to check member
	 * perssion /service account to reservation
	 * 
	 * @param number
	 * @param customerId
	 * @param reservationDate
	 * @param facilityType
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/commonLookup/{number}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult lookupCustomer(@PathVariable(value = "number") String number,
			@RequestParam(value = "reservationDate", required = false) String reservationDate,
			@RequestParam(value = "startTime", required = false) String startTime,
			@RequestParam(value = "endTime", required = false) String endTime,
			@RequestParam(value = "facilityType", required = false) String facilityType, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if (number.toUpperCase().startsWith("DP")) {
			ResponseResult responseResult = dayPassPurchaseService.searchDayPassById(number);

			return responseResult;
		}

		if (number.length() == 9 && number.matches("[\\d]{9}")) {
			return advanceQueryService.mappingCustomerByCardNo(Long.parseLong(CommUtil.cardNoTransfer(number)));
		} else {
			/***
			 * @time 2016-04-18
			 * @author christ
			 * @desc SGG-1140 1.No Permission (The highest) check member
			 *       facility permission ,is no resverion permission pop message
			 *       2.Service account effective period :check memeber account
			 *       effective <currentTime pop message
			 */
			if (StringUtils.isNotEmpty(facilityType)) {
				ResponseResult result = memberQuickSearchService.checkPerssionOrServiceAccount(null, number, startTime,
						endTime, reservationDate, facilityType);
				if (!result.getReturnCode().equals("0")) {
					return result;
				}
			}
			return memberQuickSearchService.quickSeachMember(number);

		}

	}

	/***
	 * @time 2016-04-20
	 * @author christ
	 * @desc SGG-1140 1.No Permission (The highest) check member facility
	 *       permission ,is no resverion permission pop message 2.Service
	 *       account effective period :check memeber account effective
	 *       <currentTime pop message
	 *
	 *       modified by Kaster 20160427 增加@EncryptFieldInfo解密customerId
	 */
	@RequestMapping(value = "/checkServicePeriod/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkServicePeriod(@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId,
			@RequestParam(value = "reservationDate", required = false) String reservationDate,
			@RequestParam(value = "startTime", required = false) String startTime,
			@RequestParam(value = "endTime", required = false) String endTime,
			@RequestParam(value = "facilityType", required = false) String facilityType) throws Exception {

		return memberQuickSearchService.checkPerssionOrServiceAccount(customerId, null, startTime, endTime,
				reservationDate, facilityType);

	}
	@RequestMapping(value = "/commonScan/{number}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult scanCustomer(@PathVariable(value = "number") String number,

	HttpServletRequest request, HttpServletResponse response) throws Exception {

		if (number.toUpperCase().startsWith("DP")) {
			// DPXXX Card NO to search
			return dayPassPurchaseService.searchDayPassByIdOrCardNo(null, number);
		} else {
			// DPASS id to search
			return dayPassPurchaseService.searchDayPassByIdOrCardNo(number, null);
		}
	}

	@RequestMapping(value = "/advanceCourseSearch/{category}/{courseType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseConditions(@PathVariable("category") String category,
			@PathVariable("courseType") String courseType, HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list;
			if (!(StringUtils.equalsIgnoreCase("GSS", courseType) || StringUtils.equalsIgnoreCase("TSS", courseType))) {
				responseResult.initResult(GTAError.CourseError.INVALID_COURSE_TYPE);
				return responseResult;

			}
			list = advanceQueryService.assembleCourseQueryConditions(courseType);
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceCourseSearch/temppass", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTemppassConditions(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "history", defaultValue = "N") String history) {
		try {
			if ("N".equals(history)) {
				List<AdvanceQueryConditionDto> list = advanceQueryService.assembleTemppassQueryConditions();
				responseResult.initResult(GTAError.Success.SUCCESS, list);
				return responseResult;
			} else {

				List<AdvanceQueryConditionDto> list = advanceQueryService.assembleTemppassHistoryQueryConditions();
				responseResult.initResult(GTAError.Success.SUCCESS, list);
				return responseResult;
			}
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceSearch/helptype", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getHelppassTypeConditions(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleHelpPassTypeQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceSearch/MmsOrder", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getMmsOrderConditions(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleMmsOrderQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceSearch/academy", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAcademyCardConditions(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assemblePermitCardQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceCourseMembersSearch/{category}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCourseMembersConditions(@PathVariable("category") String category,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list;
			list = advanceQueryService.assembleCourseMembersQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceSearchRestaurantMenuCat", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRestaurantMenuCateConditions(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list;
			list = advanceQueryService.assembleRestaurantMenuQueryConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceSearch/SpaRetreatList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRetreatConditions(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleSpaRetreatConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceSearch/SpaRetreatItemList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRetreatItemConditions(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleSpaRetreatItemConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	@RequestMapping(value = "/advanceSearch/spaRefundItemList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRefundItemConditions(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<AdvanceQueryConditionDto> list = advanceQueryService.assembleSpaRefundItemConditions();
			responseResult.initResult(GTAError.Success.SUCCESS, list);
			return responseResult;
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}

	/***
	 * check member Not enough Cash Value Not enough Spending Limit Not enough
	 * Credit Limit
	 * 
	 * @param customerId
	 * @param totalPrice
	 * @return
	 * 
	 * 		modified by Kaster 20160427 增加@EncryptFieldInfo解密customerId
	 */
	@RequestMapping(value = "/checkAccountSum/{customerId}/{totalPrice}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkAccountSum(@PathVariable("customerId") @EncryptFieldInfo Long customerId,
			@PathVariable("totalPrice") BigDecimal totalPrice) {
		try {
			return memberFacilityTypeBookingService.checkMemberAccountPriceByCustomerId(customerId, totalPrice);
		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
	}
    /***
     * // Disclaimer /Privacy Policy / Personal Information Collection Statement ("PICS").
     * @param type is (Disclaimer/Privacy Policy/Personal Information Collection Statement ("PICS"))
     * @return
     */
	@RequestMapping(value = "/getUrlSignatureList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getUrlSignatureList() {
//		String url=appProps.getProperty("domain")+appProps.getProperty("portal.login.page");
		List<String>urls=new ArrayList<>();
		urls.add(appProps.getProperty("portal.static.page.disclaimer"));
		urls.add(appProps.getProperty("portal.static.page.privacy"));
		urls.add(appProps.getProperty("portal.static.page.pics"));
		responseResult.initResult(GTAError.Success.SUCCESS,urls);
		return responseResult;
	}
	@RequestMapping(value = "/getFacilityPeriod/{facilityType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityPeriod(@PathVariable("facilityType") String facilityType) 
	{
		if (StringUtils.isEmpty(facilityType)) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		String advancedreSpeirod=null;
		switch (facilityType.toUpperCase()) {
		//GOLF
		case Constant.FACILITY_TYPE_GOLF:
			advancedreSpeirod=Constant.ADVANCEDRESPERIOD_GOLF;
			break;
		//TENNIS
		case Constant.FACILITY_TYPE_TENNIS:
			advancedreSpeirod=Constant.ADVANCEDRESPERIOD_TENNIS;
			break;
		case "WELLNESS":
			advancedreSpeirod=AdvancePeriodType.ADVANCEDRESPERIOD_SPA.getName();
			break;
		}
		
		return globalParameterService.getGlobalParameterById(advancedreSpeirod);
	}
	@RequestMapping(value = "/checkReportIssue", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkReportIssue() 
	{
		return globalParameterService.checkReportIssue();	
	}
	
	@RequestMapping(value = "/checkFacilityBookingPerssion/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkFacilityBookingPerssion(
			@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId,
			@RequestParam(value = "facilityType", required = false,defaultValue="GUEST") String facilityType,
			@RequestParam(value = "device", required = false)String device) throws Exception {
		if (UsageRightCheckService.FacilityCode.GOLF.name().equals(facilityType)
				|| UsageRightCheckService.FacilityCode.TENNIS.name().equals(facilityType)
				|| UsageRightCheckService.FacilityCode.WELLNESS.name().equals(facilityType)
				|| UsageRightCheckService.FacilityCode.GUEST.name().equals(facilityType)) {
			FacilityCode facilityCode = memberQuickSearchService.convertFacilityCodeByType(facilityType);
			// default currentTime
			UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(customerId,
					facilityCode, null);
			if (null == facilityRight || !facilityRight.isCanBook()) {
				if(StringUtils.isEmpty(device)){
					responseResult.initResult(GTAError.GuestRoomError.NO_PERSSIONI);	
				}else{
					responseResult.initResult(GTAError.GuestRoomError.STAFF_BUY_PERSSION);
				}
				
			}else{
				responseResult.initResult(GTAError.Success.SUCCESS);
			}
		}
		return responseResult;
	}

	@RequestMapping(value = "/getPaymentMethod/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPaymentPerssion(@PathVariable(value = "customerId") @EncryptFieldInfo Long customerId,@RequestParam(value = "type", required = false) String type)
	{
		return customerServiceAccService.getPaymentMethodByPatron(customerId,type);
	}

	@RequestMapping(value = "/checkBookingPerssion/{academyNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkBookingPerssion(@PathVariable(value = "academyNo") String academyNo,
			@RequestParam(value = "reservationDate", required = false) String reservationDate,
			@RequestParam(value = "facilityType", required = false) String facilityType,
			@RequestParam(value = "courseId", required = false) Long courseId,
			@RequestParam(value = "sessionDate", required = false) String sessionDate,
			@RequestParam(value = "view", required = false) String view,
			@RequestParam(value = "device", required = false)String device) throws Exception {
		// 1、get service plan by customerId
		// 2、 get service_plan_addition_rule by plan_no
		Member member = memberService.getMemberByAcademyNo(academyNo);
		if (null == member) {
			responseResult.initResult(GTAError.MemberError.MEMBER_NOT_FOUND);
		} else {
			Long customerId = member.getCustomerId();
			if (memberType.IDM.name().equals(member.getMemberType())
					|| memberType.CDM.name().equals(member.getMemberType())) {
				customerId = member.getSuperiorMemberId();
			}
			if (StringUtils.isEmpty(facilityType)) {
			   return customerServiceAccService.checkServicePlanBookingPerssion(customerId, reservationDate,view,device);	
				// golf/tennis course
			} else if (CourseType.GOLFCOURSE.getDesc().equals(facilityType)
					|| CourseType.TENNISCOURSE.getDesc().equals(facilityType)) {
				
				return courseService.checkCourseBookingPerssionByCourseId(customerId, courseId,sessionDate,device);
			}
		}
		return responseResult;

	}
	//Date of Birth yyyyMMdd
	@RequestMapping(value = "/verification", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult verificationBirthDate(@RequestParam(value = "patronID",required=false) Integer patronID,
			@RequestParam(value = "academyID",required=false) Integer academyID,
			@RequestParam(value = "doB",required=false)Integer doB,
			@RequestParam(value = "password",required=false)String password,
			@RequestParam(value = "phone",required=false)String phone,
			@RequestParam(value = "lastName",required=false)String lastName) throws Exception {
		return memberService.verificationBirthDate(null!=patronID?patronID.toString():null,null!=academyID?academyID.toString():null, null!=doB?doB.toString():null,password,phone,lastName);
	} 
}
