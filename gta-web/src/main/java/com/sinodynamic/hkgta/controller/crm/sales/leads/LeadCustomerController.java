package com.sinodynamic.hkgta.controller.crm.sales.leads;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.CustomerLeadNoteDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerLeadNote;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.leads.LeadCustomerNoteService;
import com.sinodynamic.hkgta.service.crm.sales.leads.LeadCustomerService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.pagination.ListPage;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype") 
@RequestMapping(value = "/lead")
public class LeadCustomerController extends
		ControllerBase<CustomerProfile> {
	
	private Logger logger = Logger.getLogger(LeadCustomerController.class);
	
	@Autowired
	private LeadCustomerService leadCustomerService;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private LeadCustomerNoteService leadCustomerNoteService;
	
	/**
	 * Method to register the potential customer and create the related
	 * customer enrollment detail
	 * 
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(method = {RequestMethod.POST,RequestMethod.PUT})
	@ResponseBody
	public ResponseResult leadCustomerSaveOrUpdate(
			@RequestBody CustomerProfileDto customerProfileDto) {
		try {
			String loginUserId = getUser().getUserId();
			customerProfileDto.setLoginUserId(loginUserId);
			customerProfileDto.setSalesFollowBy(loginUserId);	
			ResponseResult result = leadCustomerService
					.saveOrUpdatePotentialCustomer(customerProfileDto);
			if("0".equals(result.getReturnCode())){
				CustomerProfile customerProfile = (CustomerProfile) result.getData();
				return leadCustomerService.movePortraitToCustomerFolder(customerProfile);
			}
			return result;
		}catch(GTACommonException ce){
			logger.error(ce.getMessage(),ce);
			logger.debug("LeadCustomerController.leadCustomerSaveOrUpdate Exception:",ce);
			responseResult.initResult(ce.getError());
			return responseResult;
		}catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("LeadCustomerController.leadCustomerSaveOrUpdate Exception:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	
	/**
	 * Method to delete the selected lead's details
	 * @param customerId
	 * @return
	 */
	@RequestMapping(value = "/{customerId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteLead(@PathVariable @EncryptFieldInfo Long customerId) {
		try {
			return leadCustomerService.deleteLead(customerId,super.getUser().getUserId());
		} catch (Exception e) {
			logger.debug("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * Method to enroll a lead, the returned lead customer profile will be forward to enrollment process in front-end team.
	 * @param customerId
	 * @return Customer Profile.
	 */
	@RequestMapping(value = "/enrollLead/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult enrollLead(@PathVariable @EncryptFieldInfo Long customerId) {
		try {
			return leadCustomerService.enrollLead(customerId);
		} catch (Exception e) {
			logger.debug("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}
	
	/**
	 *
	 * @param request
	 * @param response
	 */

	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCustomerProfileDetail(@PathVariable(value = "id") @EncryptFieldInfo Long id) {
		try {
			return leadCustomerService.getCustomerProfileDetail(id);
		} catch (Exception e) {
			logger.debug("CorporateManagmentController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getLeads(@RequestParam("pageNumber") String pageNumber,
			@RequestParam(value = "pageSize") String pageSize,
			@RequestParam(value = "searchText") String searchText,
			@RequestParam(value = "sortBy") String sortBy,
			@RequestParam(value = "isAscending") String isAscending,
			@RequestParam(value = "status") String status,
			@RequestParam(value = "isMyClient") String isMyClient,
			@RequestParam(value = "contactClassCode",defaultValue="ALL") String contactClassCode,
			@RequestParam(value = "device",defaultValue="IOS",required = false) String device){
		try {
			LoginUser currentUser = getUser();
			CustomerProfileDto dto = new CustomerProfileDto(pageNumber,
					pageSize, searchText, sortBy, isAscending, status,
					isMyClient);
			dto.setContactClassCode(contactClassCode);
			dto.setCreateBy(getUser().getUserId());
			return customerProfileService.getCustomerProfileList(page, dto,currentUser.getUserId(),device);
		} catch (Exception e) {
			logger.debug("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/listNotes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult listNotes(@RequestParam(value="pageNumber", defaultValue="1") Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue="20") Integer pageSize,
			@RequestParam(value="sortBy", defaultValue="createDate") String sortBy,
			@RequestParam(value="isAscending", defaultValue="true") String isAscending,
			@RequestParam(value="customerId") @EncryptFieldInfo Long customerId){
		try {
			if (StringUtils.isEmpty(customerId)) {
				responseResult.initResult(GTAError.LeadError.CUSTOMER_ID_REQUIRED);
				return responseResult;
			}
			
			ListPage<CustomerLeadNote> page = new ListPage<CustomerLeadNote>();
		
			if("true".equals(isAscending)){
				page.addAscending(sortBy);
			}else if("false".equals(isAscending)){
				page.addDescending(sortBy);
			}
			page.setNumber(pageNumber);
	        page.setSize(pageSize);
		
			Data notes = leadCustomerNoteService.listCustomerLeadNotes(customerId, page);
			
			for(Object note : notes.getList())
			{
				CustomerLeadNoteDto dto = (CustomerLeadNoteDto)note;
				
				dto.setBackNoteDate(((CustomerLeadNoteDto)note).getNoteDate());
				dto.setNoteDate(DateConvertUtil.getYMDDateAndDateDiff(DateConvertUtil.parseString2Date(((CustomerLeadNoteDto)note).getNoteDate(), "yyyy-MM-dd")));
				
				if (dto.getSubject() != null)
					dto.setSubject(encodeUTF8(dto.getSubject()));
				if (dto.getContent() != null)
					dto.setContent(encodeUTF8(dto.getContent()));
			}
			
			responseResult.initResult(GTAError.Success.SUCCESS, notes);
			return responseResult;
			
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("LeadCustomerController.listNotes Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/noteDetail/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult viewNoteDetail(@PathVariable(value = "id") Long id) {
		try {
			CustomerLeadNote note = leadCustomerNoteService.getCustomerLeadNoteDetail(id);
			if (note == null){
				responseResult.initResult(GTAError.LeadError.NOTE_IS_NOT_EXISTING);
        		return responseResult;
			}
			else{
				CustomerLeadNoteDto dto = new CustomerLeadNoteDto();
				dto.setSysId(note.getSysId());
				dto.setCustomerId(note.getCustomerId());
				dto.setNoteDate(note.getNoteDate());
				
				if (note.getSubject() != null)
				    dto.setSubject(encodeUTF8(note.getSubject()));
				if (note.getContent() != null)
				    dto.setContent(encodeUTF8(note.getContent()));
				
				dto.setCreateBy(note.getCreateBy());
			    responseResult.initResult(GTAError.Success.SUCCESS, dto);
			    return responseResult;
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("LeadCustomerController.viewNoteDetail Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/deleteNote/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteNote(@PathVariable(value = "id") Long id) {
		try {
			leadCustomerNoteService.deleteCustomerLeadNote(id, getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
			
		} catch (GTACommonException e) {
			logger.debug("LeadCustomerController.deleteNote Error Message:", e);
			responseResult.initResult(e.getError());
			return responseResult;
		}
		catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("LeadCustomerController.deleteNote Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	@RequestMapping(value = "/saveNote", method = {RequestMethod.POST, RequestMethod.PUT})
	@ResponseBody
	public ResponseResult saveNote(@RequestBody CustomerLeadNoteDto customerLeadNoteDto) {
		try {
            customerLeadNoteDto.setLoginUserId(getUser().getUserId());
            //decode
            if (customerLeadNoteDto.getSubject() != null)
                customerLeadNoteDto.setSubject(decodeUTF8(customerLeadNoteDto.getSubject()));
            if (customerLeadNoteDto.getContent() != null)
                customerLeadNoteDto.setContent(decodeUTF8(customerLeadNoteDto.getContent()));
           
			return leadCustomerNoteService.saveCustomerLeadNote(customerLeadNoteDto);	
			
		} catch (GTACommonException e) {
			logger.debug("LeadCustomerController.saveNote Error Message:", e);
			responseResult.initResult(e.getError());
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("LeadCustomerController.saveNote Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	private String decodeUTF8(String str) throws Exception{
		return new String(str.getBytes("UTF-8"), "UTF-8");
	}
	
    private String encodeUTF8(String str) throws Exception{
    	return new String(str.getBytes("UTF-8"), "UTF-8");
	}
	
}