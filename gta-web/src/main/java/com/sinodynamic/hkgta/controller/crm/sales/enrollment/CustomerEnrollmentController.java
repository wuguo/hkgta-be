package com.sinodynamic.hkgta.controller.crm.sales.enrollment;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.lowagie.text.DocumentException;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.ActivateMemberDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEmailDto;
import com.sinodynamic.hkgta.dto.crm.CustomerEnrollmentSearchDto;
import com.sinodynamic.hkgta.dto.crm.MemberDto;
import com.sinodynamic.hkgta.dto.crm.UpdateEnrollmentStatusDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerEnrollment;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.adm.PermitCardMasterService;
import com.sinodynamic.hkgta.service.crm.backoffice.membership.DependentMemberService;
import com.sinodynamic.hkgta.service.crm.sales.UploadFileService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerAdditionInfoService;
import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.EnrollStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/enrollment")
public class CustomerEnrollmentController extends ControllerBase {

	private static final Logger logger = Logger.getLogger(CustomerEnrollmentController.class);
	
	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;

	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private PermitCardMasterService permitCardMasterService;
	
	@Autowired
	private DependentMemberService departmentMemberService;
	
	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param searchText
	 * @param sortBy
	 * @param isAscending
	 * @param customerStatus
	 * @param enrollStatus
	 * @param isMyClient
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getEnrollments(@RequestParam(value = "pageNumber", defaultValue = "1") String pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10") String pageSize,
			@RequestParam(value = "searchText", defaultValue = "") String searchText,
			@RequestParam(value = "sortBy", defaultValue = "enrollName") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "true") String isAscending,
			@RequestParam(value = "customerStatus", defaultValue = "") String customerStatus,
			@RequestParam(value = "enrollStatus", defaultValue = "") String enrollStatus,
			@RequestParam(value = "isMyClient", defaultValue = "false") String isMyClient,
			@RequestParam(value = "device", defaultValue = "IOS") String device){
		String userId = getUser().getUserId();
		CustomerEnrollmentSearchDto dto = new CustomerEnrollmentSearchDto( pageNumber,  pageSize,
				 searchText,  sortBy,  isAscending,customerStatus, enrollStatus,  isMyClient,userId);
		return customerProfileService.getEnrollments(page, dto,userId,device);
	}
	
	/**
	 * This function is used for checking the selected lucky number for academy
	 * Id is available or not. The range of lucky number is 8000000-9999999.
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/checkAcademyId/{academyId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult checkAvailableAcademyID(@PathVariable(value="academyId") String academyId) {
		logger.debug("EnrollmentServiceImpl.checkAvailableAcademyID invocation start ...");
		try {
			boolean isAvailable = customerEnrollmentService.checkAvailableAcademyID(academyId);
			MemberDto dto = new MemberDto();
			if (isAvailable) {
				dto.setAcademyNoStatus("TRUE");
			} else {
				dto.setAcademyNoStatus("FALSE");
			}
			responseResult.initResult(GTAError.Success.SUCCESS, dto);
			return responseResult;
		} catch (Exception e) {
			logger.debug("CustomerEnrollmentController.checkAvailableAcademyID Exception",e);
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * @author Liky_Pan
	 * The method is used to reserve the academy Id for future using. 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/reserveAcademyNo", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult reserveAcademyId(@RequestBody MemberDto dto) {
		logger.debug("CustomerEnrollmentController.reserveAcademyId invocation start ...");
		try {
			LoginUser currentUser = getUser();
			if(dto.isBackEndApi()){
				logger.debug("CustomerEnrollmentController.removeReservedAcademyId invocation start ...");
				try {
					customerEnrollmentService.removeReservedAcademyId(dto.getCustomerId());
				} catch (Exception e) {
					logger.debug("CustomerEnrollmentController.removeReservedAcademyId Exception",e);
					logger.error(e.getMessage(),e);
					responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
					return responseResult;
				}
			}
			return customerEnrollmentService.reserveAcademId(dto,currentUser.getUserId());
		} catch (Exception e) {
			logger.debug("CustomerEnrollmentController.reserveAcademyId Exception",e);
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * @author Liky_Pan
	 * The method is used to remove the reserved academy Id for future using. 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/removeAcademyNo/{customerId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult removeReservedAcademyId(@PathVariable @EncryptFieldInfo Long customerId) {
		logger.debug("CustomerEnrollmentController.removeReservedAcademyId invocation start ...");
		try {
			return customerEnrollmentService.removeReservedAcademyId(customerId);
		} catch (Exception e) {
			logger.debug("CustomerEnrollmentController.removeReservedAcademyId Exception",e);
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * @author Liky_Pan
	 * The method is used to return the dependent list by superior customer Id
	 * @param superiorCustomerId
	 * 
	 * modified by Kaster 20160506 加解密@EncryptFieldInfo
	 */
	@RequestMapping(value = "/getDependent/{superiorCustomerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDependentMember(@PathVariable @EncryptFieldInfo Long superiorCustomerId) {
		try {
			return customerEnrollmentService.getDependentBySuperiorId(superiorCustomerId);
		} catch (Exception e) {
			logger.debug("CustomerEnrollmentController.getDependentMember Exception:",e);
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/setAcademyId", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult setAcademyID(@RequestBody MemberDto memberDto) {
		ResponseResult result = new ResponseResult();
		try {
			boolean isSet = customerEnrollmentService.setAcademyID(memberDto.getAcademyNo(), memberDto.getCustomerId());
			if(isSet){
				result.setErrorMessageEN("Successful!");
				result.setReturnCode("0");
				}
			else{

				result.setErrorMessageEN("Failed!");
				result.setReturnCode("1");
			}
		} catch (Exception e) {
			logger.debug("CustomerEnrollmentController.setAcademyID Exception:",e);
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return result;
	}

	/**
	 * Method is used to activate customer either by selecting date or activate straightforward
	 * @param customerId
	 * @return Response Message
	 */
	@RequestMapping(value = "/activateMember", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult activateMember(@RequestBody ActivateMemberDto activeMemberDto){
		try {
			LoginUser currentUser = super.getUser();
			if(activeMemberDto.getActivationDate()==null){
				responseResult= customerEnrollmentService.encapsulateActivationMember(activeMemberDto,currentUser.getUserId(),currentUser.getFullname()==null?currentUser.getUserName():currentUser.getFullname());
			}else{
				responseResult= customerEnrollmentService.setActivationDateForMember(activeMemberDto,currentUser.getUserId(),currentUser.getFullname()==null?currentUser.getUserName():currentUser.getFullname());
			}
			//when enrollmember status is change   then send mail to sale person
			if(null!=activeMemberDto&&null!=activeMemberDto.getCustomerId()){
				CustomerEnrollment customerEnrollment = customerEnrollmentService.getEnrollmentByCustomerId(activeMemberDto.getCustomerId());
				if(null!=customerEnrollment){
					customerEnrollmentService.sendMailToSalesByUserId(customerEnrollment.getSalesFollowBy(),customerEnrollment.getCustomerId());
				}
			}
			return responseResult;
		}catch (GTACommonException gta) {
			logger.debug("CustomerEnrollmentControllmer.activeMember Exception",gta);
			gta.printStackTrace();
			responseResult.initResult(gta.getError());
			return responseResult;
		}catch (Exception e) {
			logger.debug("CustomerEnrollmentControllmer.activeMember Exception",e);
			e.printStackTrace();
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * Action to check status of customer enrollment once the payment or card
	 * has been settled.(will be used in displaying the pending list and
	 * activated list)
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/{enrollId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteNewEnrollments(@PathVariable (value="enrollId") Long enrollId) {
		logger.info("EnrollmentServiceImpl.deleteNewEnrollments invocation start ...");
		ResponseResult result = new ResponseResult();

		try {
			CustomerEnrollment customerEnrollment = customerEnrollmentService.getEnrollmentById(enrollId);
			if(!customerEnrollment.getSalesFollowBy().equals(super.getUser().getUserId())) return new ResponseResult("1", "You have no authorization to delete other's customer detail!");
			String check = customerEnrollmentService.checkEnrollmentStatus(enrollId);
			if ("0".equals(check)) {
				customerEnrollmentService.deleteEnrollmentsWithoutPayment(enrollId);
				result.setErrorMessageEN("Successful!");
				result.setReturnCode("0");
				
			} else if("1".equals(check)){
				result.setErrorMessageEN("Failed! This enrollment status is not NEW! ");
				result.setReturnCode("1");
				return result;
			}else if("2".equals(check)){
				result.setErrorMessageEN("Failed! This enrollment doesn't exist! ");
				result.setReturnCode("2");
				return result;
			}
		} catch (Exception e) {
			logger.debug("CustomerEnrollmentControllmer.deleteNewEnrollments Exception",e);
			logger.error(e.getMessage(),e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		return result;
	}

	/**

	 * @param request
	 * @param response
	 * modified by Kaster 20160506 使用@RequestBody CustomerProfile cProfile接收参数，以便对superiorMemberId加解密。
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reg", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult regEnrollment(HttpServletRequest request, HttpServletResponse response, @RequestBody CustomerProfile cProfile) {
		try {
			LoginUser currentUser = getUser();
			if(cProfile != null){
				cProfile.setCreateBy(this.getUser().getUserId());
			}else{
				responseResult.initResult(GTAError.EnrollError.INCORRECT_JSON_INPUT);
				return responseResult;
			}
			logger.debug("addEnrollment controller parameter: givenName="+cProfile.getGivenName());
			CustomerEnrollment customerEnrollment = customerEnrollmentService.getEnrollmentByCustomerId(cProfile.getSuperiorMemberId());
			
			if (customerEnrollment != null) {
				//仅在注册IDM时，其IPM是CMP或者ANC的情况下进入判断
				if (EnrollStatus.CMP.name().equals(customerEnrollment.getStatus())
						|| EnrollStatus.ANC.name().equals(customerEnrollment.getStatus())
						&& cProfile.getSuperiorMemberId() != null) {
					Member member = new Member(cProfile.getSuperiorMemberId());
					cProfile.setMember(member);
					responseResult = departmentMemberService.saveDependentMemberInfo(cProfile, super.getUser()
							.getUserId(),currentUser.getUserName());
				}else{
					responseResult =  customerEnrollmentService.regEnrollment(cProfile,currentUser.getUserId());
				}
			}else{
				responseResult =  customerEnrollmentService.regEnrollment(cProfile,currentUser.getUserId());
			}
			
			if ("0".equals(responseResult.getReturnCode())) {
				customerProfileService.moveProfileAndSignatureFile((CustomerProfile) responseResult.getDto());
			}
			return responseResult;
		} catch(GTACommonException ce){
			logger.debug("CustomerEnrollmentControllmer.regEnrollment Exception",ce);
			logger.error(ce.getMessage(),ce);
			responseResult.initResult(ce.getError());
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("CustomerEnrollmentControllmer.regEnrollment Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	 }
	/**
	 *This function is used to save the signature for each enrollment record.
	 * @param request
	 * @param response
	 */

	@RequestMapping(value = "/saveSignature", method = RequestMethod.POST)
	public @ResponseBody ResponseResult saveSignature(
			@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		if (file == null || file.isEmpty()) {
			logger.info("File is empty!");
			responseResult.initResult(GTAError.EnrollError.FILE_IS_EMPTY);
			return responseResult;
		}
		String saveFilePath = "";
		try {
			saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.USER);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("CustomerEnrollmentControllmer.saveSignature Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		File serverFile = new File(saveFilePath);
		saveFilePath = serverFile.getName();
		Map<String, String> imgPathMap = new HashMap<String, String>();
		imgPathMap.put("imagePath", "/"+saveFilePath);
		responseResult.initResult(GTAError.Success.SUCCESS,imgPathMap);
		return responseResult;
	}
	

	/**
	 *This function is used to save the profile photo for each enrollment record.
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/saveProfile", method = RequestMethod.POST)
	public @ResponseBody ResponseResult saveProfile(
			@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		if (file == null || file.isEmpty()) {
			logger.info("File is empty!");
			responseResult.initResult(GTAError.EnrollError.FILE_IS_EMPTY);
			return responseResult;
		}
		String saveFilePath = "";
		try {
			/***
			 * user upload _small patron image
			 */
			saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.USER,true);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			logger.debug("CustomerEnrollmentControllmer.saveProfile Exception",e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		File serverFile = new File(saveFilePath);
		saveFilePath = serverFile.getName();
		Map<String, String> imgPathMap = new HashMap<String, String>();
		imgPathMap.put("imagePath", "/"+saveFilePath);
		responseResult.initResult(GTAError.Success.SUCCESS,imgPathMap);
		return responseResult;
		
	}
	
 /**
	 * 
	 * @param request
	 * @param response
	 * modified by Kaster 20160506
	 * 将接收参数换成@RequestBody CustomerProfile cProfile，避免解析JSON。
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg editEnrollment(HttpServletRequest request, HttpServletResponse response,@RequestBody CustomerProfile cProfile){
		try {
			LoginUser currentUser = getUser();
			logger.debug("editEnrollment controller parameter: givenName="+cProfile.getGivenName());
			responseMsg = customerEnrollmentService.editEnrollment(cProfile, currentUser.getUserId(),currentUser.getUserName());
			if ("0".equals(responseMsg.getReturnCode())) {
				customerProfileService.moveProfileAndSignatureFile(cProfile);
			}
			return responseMsg;
		}catch(GTACommonException ce){
			logger.debug("CustomerEnrollmentControllmer.editEnrollment Exception",ce);
			logger.error(ce.getMessage(),ce);
			responseMsg.initResult(ce.getError(),ce.getArgs());
			return responseMsg;
		}catch (Exception e) { 
			logger.error(e.getMessage(),e);
			logger.debug("CustomerEnrollmentControllmer.editEnrollment Exception",e);
			responseMsg.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseMsg;
		}
	}
	
	/**
	 *This function is used to update the enrollment status of member to be "complete" once 
	 *all payments are approved and card is issued.
	 * @param request
	 * @param response
	 */

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/updateStatus", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateEnrollmentStatus(@RequestBody UpdateEnrollmentStatusDto updateEnrollmentStatusDto){
		ResponseResult result = new ResponseResult();
		LoginUser currentUser = getUser();
		Long enrollId = Long.parseLong(updateEnrollmentStatusDto.getEnrollId());
		String status = updateEnrollmentStatusDto.getStatus();
		String cardFlag = permitCardMasterService.checkPermitCardStatus(enrollId);
			if(("1").equals(cardFlag)){
				result.setErrorMessageEN("Failed! No card is issued!");
				result.setReturnCode("1");
			}else if(("2").equals(cardFlag)){
				result.setErrorMessageEN("Failed! No this enrollment!");
				result.setReturnCode("2");}
 				//if(customerEnrollmentService.checkTransactionsStatusAsSuccessful(enrollId)){
			else if(("0").equals(cardFlag)){
				if(customerEnrollmentService.checkBalanceDueAsZero(enrollId)){
					if(customerEnrollmentService.updateEnrollmentStatusToComplete(enrollId, status,currentUser.getUserId())>0){
					result.setErrorMessageEN("Successfull update to 'CMP'!");
					result.setReturnCode("0");
					}
				}else{
					result.setErrorMessageEN("Failed! The balance due is not zero!");
					result.setReturnCode("3");
				}
			}
		
			return result;
	} 
	/**
	 * using for job execute
	 */
	@SuppressWarnings("unchecked")
	public void updateEnrollmentStatusJob(){
		LoginUser currentUser = getUser();
		customerEnrollmentService.searchAllCustomerEnrollments(page);
		List<CustomerEnrollment> enrollList = (List<CustomerEnrollment>)page.getList();
		if(enrollList.size()>0){
			for(CustomerEnrollment ce:enrollList){
				String cardFlag = permitCardMasterService.checkPermitCardStatus(ce.getEnrollId());
				if(!EnrollStatus.CMP.name().equals(ce.getStatus()) && ("0").equals(cardFlag)){
					if(customerEnrollmentService.checkBalanceDueAsZero(ce.getEnrollId())){
						customerEnrollmentService.updateEnrollmentStatusToComplete(ce.getEnrollId(), EnrollStatus.CMP.name(),currentUser.getUserId());
					}
				}
			}
		}
	} 
	
	@RequestMapping(value = "/getMemberType/{academyNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult validateMemberTypeByAcademyNo(@PathVariable String academyNo){
		try {
			return customerEnrollmentService.getMemberTypeByAcademyNo(academyNo);
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.validateMemberTypeByAcademyNo Exception:", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/getReserveStatus/{customerId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAcademyNoReserveStatus(@PathVariable @EncryptFieldInfo Long customerId){
		try {
			return customerProfileService.getAcademyNoReserveStatus(customerId);
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.getAcademyNoReserveStatus Exception:", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
		
	}
	
	@RequestMapping(value = "/sendEnrollForm", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult sendEnrollForm(@RequestBody CustomerEmailDto emailDto){
		
		try {
			LoginUser currentUser = getUser();
			return customerProfileService.sendEnrollmentFormEmail(emailDto,currentUser.getUserName(),currentUser.getUserId());
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.sendEnrollForm Exception:", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	//modified by Kaster 20160323 增加@EncryptFieldInfo
	@RequestMapping(value = "/getEnrollForm/{customerId}", method = RequestMethod.GET,produces = "application/pdf")
	@ResponseBody
	public byte[] getEnrollForm(@PathVariable @EncryptFieldInfo Long customerId){
		try {
			LoginUser currentUser = getUser();
			return customerProfileService.getEnrollFormInOnePdf(customerId,currentUser.getUserId());
		} catch (DocumentException e) {
			logger.debug("CustomerEnrollment.getEnrollForm Exception:", e);
		} catch (IOException e) {
			logger.debug("CustomerEnrollment.getEnrollForm Exception:", e);
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.getEnrollForm Exception:", e);
		}
		return null;
	}
	
	@RequestMapping(value = "/approve/{customerId}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult approveMember(@PathVariable @EncryptFieldInfo Long customerId){
		try {
			LoginUser currentUser = getUser();
			return customerEnrollmentService.approveMember(customerId,currentUser.getUserId());
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.approveMember Exception:", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	@RequestMapping(value = "/deleteDependent/{customerId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteDependentMember(@PathVariable @EncryptFieldInfo Long customerId){
		try {
			LoginUser currentUser = getUser();
			return customerEnrollmentService.deleteDependentMemebr(customerId,currentUser.getUserId());
		} catch (Exception e) {
			logger.debug("CustomerEnrollment.deleteDependentMember Exception:", e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
}