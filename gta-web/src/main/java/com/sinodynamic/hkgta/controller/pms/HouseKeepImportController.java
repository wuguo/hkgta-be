package com.sinodynamic.hkgta.controller.pms;


import java.io.File;
import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.service.pms.RoomStatusScheduleService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RoomStatus;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/houseKeepImport")
public class HouseKeepImportController extends ControllerBase {

	@Autowired
	private RoomStatusScheduleService roomStatusScheduleService;
	
	@Resource(name = "oasisProperties")
	private Properties oasisProps;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/importOooReport", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult importOooReport(@RequestParam("file") final MultipartFile csvFile) {
		try {
			File sourceFile = getUploadCsvFile(csvFile);
			responseResult=roomStatusScheduleService.importOoCsv(getUser().getUserId(), RoomStatus.OOO.name(),sourceFile);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/importOosReport", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult importOosReport(@RequestParam("file") final MultipartFile csvFile) {
		try {
			File sourceFile = getUploadCsvFile(csvFile);
			responseResult=roomStatusScheduleService.importOoCsv(getUser().getUserId(), RoomStatus.OOS.name(),sourceFile);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	@RequestMapping(value = "/importDeskStatusReport", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult importDeskStatusReport(@RequestParam("file") final MultipartFile csvFile) {
		try {
			File sourceFile = getUploadCsvFile(csvFile);
			responseResult=roomStatusScheduleService.importDeskStatusCsv(getUser().getUserId(),sourceFile);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
		}
		return responseResult;
	}
	public File getUploadCsvFile(MultipartFile csvFile) throws Exception{
		String path = oasisProps.getProperty(Constant.IMPORT_FILE_PATH);
		File file = new File(path);
		if (!file.exists() && !file.isDirectory()) {
			file.mkdirs();
		}
		File targetFile = new File(path, csvFile.getOriginalFilename());
		csvFile.transferTo(targetFile);
		return targetFile;
	}
}


