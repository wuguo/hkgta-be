package com.sinodynamic.hkgta.controller.mms;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.mms.MMSPaymentDto;
import com.sinodynamic.hkgta.dto.mms.SpaAppointmentDto;
import com.sinodynamic.hkgta.dto.mms.SpaCategoryDto;
import com.sinodynamic.hkgta.dto.mms.SpaCategoryResponseDto;
import com.sinodynamic.hkgta.dto.mms.SpaCenterInfoDto;
import com.sinodynamic.hkgta.dto.mms.SpaPicPathDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatDto;
import com.sinodynamic.hkgta.dto.mms.SpaRetreatItemDto;
import com.sinodynamic.hkgta.dto.rpos.CashValueLogDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.spa.request.GetAvailableTimeSlotsRequest;
import com.sinodynamic.hkgta.integration.spa.request.TherapistRequestType;
import com.sinodynamic.hkgta.integration.spa.response.Therapist;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.mms.MMSService;
import com.sinodynamic.hkgta.service.mms.SpaAppointmentRecService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.AdvancePeriodType;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GTAError.Success;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping(value = "/mmsprocessor")
@Scope("prototype")
public class MMSController extends ControllerBase {

	//private Logger						logger	= Logger.getLogger(MMSController.class);
	
	private Logger						mmsLog	= Logger.getLogger(LoggerType.MMS.getName());
	

	@Autowired
	private MMSService					mmsService;

	@Autowired
	private CustomerOrderTransService	customerOrderTransService;

	@Autowired
	private GlobalParameterService		globalParameterService;

	@Autowired
	private AdvanceQueryService			advanceQueryService;

	@Autowired
	private SpaAppointmentRecService	spaAppointmentRecService;

	@Autowired
	private CustomerServiceAccService	customerServiceAccService;

	@Autowired
	private UsageRightCheckService		usageRightCheckService;
	
	@Autowired
	private MemberService				memberService;

			
			
	@Value(value ="#{spaProperties['spa.advanceBookingSlots']}")
	private boolean advanceBookingSlots;
	
	@Value(value ="#{spaProperties['spa.allAvailableSlots']}")
	private boolean allAvailableSlots;
	
	@Value(value ="#{spaProperties['spa.checkRoomAvailablity']}")
	private boolean checkRoomAvailablity;
			
	
	
	@ResponseBody
	@RequestMapping(value = "/queryTopCategories", method = RequestMethod.GET)
	public ResponseResult queryTopCategories() throws Exception {

		try {
			return mmsService.queryTopCategories();
		} catch (Exception e) {

			mmsLog.error(MMSController.class.getName() + " queryTopCategories failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;

		}
	}

	@ResponseBody
	@RequestMapping(value = "/getLocalTopCategories", method = RequestMethod.GET)
	public ResponseResult getLocalTopCategories(@RequestParam(value = "device", required = false) String device) throws Exception {

		try {
			if (device == null || device.length() <= 0)
				device = "WP";
			
			ResponseResult responseResult=mmsService.getLocalTopCategory(device);
			List<SpaCategoryDto> spaCategoryList =(List<SpaCategoryDto>)responseResult.getData();
			for(SpaCategoryDto category: spaCategoryList){
				String facilityName=Constant.FacilityCategory.valueOf(category.getCategoryId()).getFacilityName();
				category.setCategoryName(responseResult.getI18nMessge(facilityName));			
			}
			return responseResult;

		} catch (Exception e) {

			mmsLog.error(MMSController.class.getName() + " getLocalTopCategories failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;

		}
	}
	
	

	/**
	 * get sub category by parent_category_id
	 */
	@ResponseBody
	@RequestMapping(value = "/getLocalSubCategories", method = RequestMethod.GET)
	public ResponseResult getLocalCategories(
			@RequestParam(value = "device", required = false) String device,
			@RequestParam(value = "categoryId", required = true) String parentCatId
			) throws Exception {

		try {
			if (device == null || device.length() <= 0)
				device = "WP";
			
			ResponseResult responseResult=mmsService.getLocalCategory(device,parentCatId);
			List<SpaCategoryDto> spaCategoryList =(List<SpaCategoryDto>)responseResult.getData();
			/*if (Constant.initDate().messageCodeMap.get(subcategory.getName()) != null) {
				subcategory.setName(responseResult.getI18nMessge(Constant.initDate().messageCodeMap.get(subcategory.getName())));
			} */
			for(SpaCategoryDto category: spaCategoryList){				
				//String facilityName=Constant.FacilityCategory.valueOf(category.getCategoryId()).getFacilityName();
				String facilityName=Constant.initDate().messageCodeMap.get(category.getCategoryId());
				category.setCategoryName(responseResult.getI18nMessge(facilityName));			
			}
			return responseResult;

		} catch (Exception e) {

			mmsLog.error(MMSController.class.getName() + " getLocalTopCategories failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	@ResponseBody
	@RequestMapping(value = "/querySubCategories/{categoryId}", method = RequestMethod.GET)
	public ResponseResult querySubCategories(@PathVariable("categoryId") String categoryId) throws Exception {

		try {
			if (StringUtils.isEmpty(categoryId)) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "categoryId cannot be null.");
			} else {
				responseResult = mmsService.getSubcategories(categoryId);
			}

		} catch (Exception e) {

			mmsLog.error(MMSController.class.getName() + " querySubCategories failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
		}

		return responseResult;
	}

	@ResponseBody
	@RequestMapping(value = "/getCenterTherapists", method = RequestMethod.GET)
	public ResponseResult getCenterTherapists(Date requestDate) throws Exception {

		try {
			if (requestDate == null)
				requestDate = new Date();
			return mmsService.getCenterTherapists(requestDate);
		} catch (Exception e) {

			mmsLog.error(MMSController.class.getName() + " getCenterTherapists failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;

		}
	}

	@ResponseBody
	@RequestMapping(value = "/getAvailableTherapists", method = RequestMethod.GET)
	public ResponseResult getAvailableTherapistsForService(@RequestParam(value = "requestDate", required = true) Date requestDate,
			@RequestParam(value = "serviceCode", required = true) String serviceCode,
			@RequestParam(value = "therapistType", required = false) String therapistType) {

		if (mmsLog.isDebugEnabled()) {
			mmsLog.debug("getAvailableTherapistsForService start.");
		}
		try {
			if ("0".equals(therapistType)) {
				return mmsService.therapistsAvailableForService(requestDate, serviceCode, TherapistRequestType.FEMALE);
			} else if ("1".equals(therapistType)) {
				return mmsService.therapistsAvailableForService(requestDate, serviceCode, TherapistRequestType.MALE);
			} else {
				return mmsService.therapistsAvailableForService(requestDate, serviceCode, TherapistRequestType.ANY);
			}

		} catch (Exception e) {

			mmsLog.error(MMSController.class.getName() + " getAvailableTherapistsForService failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;

		}
	}

	@ResponseBody
	@RequestMapping(value = "/getAvailableTimeSlots", method = RequestMethod.GET)
	public ResponseResult getAvailableTimeSlots(@RequestParam(value = "startTime", required = false) String startTime,
			@RequestParam(value = "checkRoomAvailablity", required = false) boolean checkRoomAvailablity,
			@RequestParam(value = "therapistRequestType", required = false) String therapistRequestType,
			@RequestParam(value = "allAvailableSlots", required = false) boolean allAvailableSlots,
			@RequestParam(value = "serviceCodeList", required = true) String serviceCodeList,
			@RequestParam(value = "advanceBookingSlots", required = false) boolean advanceBookingSlots,
			@RequestParam(value = "therapistCode", required = false) String therapistCode,
			@RequestParam(value = "serviceHours", defaultValue = "90", required = false) int serviceHours,
			@RequestParam(value = "device", required = false) String device) throws Exception {

		try {
			GetAvailableTimeSlotsRequest param = new GetAvailableTimeSlotsRequest();
			// use spa.properties config 
			param.setAdvanceBookingSlots(this.advanceBookingSlots);
			param.setAllAvailableSlots(this.allAvailableSlots);
			param.setCheckRoomAvailablity(this.checkRoomAvailablity);
			param.setServiceHours(serviceHours);
			if (serviceCodeList != null && !"".equals(serviceCodeList)) {
				String[] list = serviceCodeList.split(",");
				List<String> serviceList = Arrays.asList(list);
				param.setServiceCodeList(serviceList);
			} else {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "ServiceCode cannot be null");
				return responseResult;
			}

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (startTime != null && !"".equals(startTime)) {
				param.setStartTime(df.parse(startTime));
			} else {
				param.setStartTime(new Date());
			}
			if (therapistCode != null && !"".equals(therapistCode)) {
				param.setTherapistCode(therapistCode);
				param.setTherapistRequestType("2"); // Specific therapist
			} else {
				/***
				 * if therapistCode is null 
				 * MMS auto allocation therapist  
				 */
				param.setTherapistRequestType(therapistRequestType);
//				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "TherapistCode cannot be null");
//				return responseResult;
			}

			if (mmsLog.isDebugEnabled()) {
				mmsLog.debug("getAvailableTimeSlots() start");
				mmsLog.debug("input parameter:" + param.toString());
			}
			if (device != null && ("AP".equals(device) || "WP".equals(device))) {
				return mmsService.getFormatAvailableTimeSlots(param);
			} else {
				return mmsService.getAvailableTimeSlots(param);
			}

		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " getAvailableTimeSlots failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;

		}
	}

	@ResponseBody
	@RequestMapping(value = "/queryServiceListByCategoryId/{categoryId}", method = RequestMethod.GET)
	public ResponseResult queryServiceList(@PathVariable("categoryId") String categoryId) throws Exception {
		try {

			if (StringUtils.isEmpty(categoryId)) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "categoryId for service cannot be null.");
			} else {
				responseResult = mmsService.queryServiceList(categoryId);
			}

		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " queryServiceList failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());

		}
		return responseResult;
	}

	@ResponseBody
	@RequestMapping(value = "/queryServiceDetails/{serviceId}", method = RequestMethod.GET)
	public ResponseResult queryServiceDetails(@PathVariable("serviceId") String serviceId) {

		try {
			if (StringUtils.isEmpty(serviceId)) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "serviceId cannot be null.");
			} else {
				responseResult = mmsService.queryServiceDetails(serviceId);
			}

		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " queryServiceDetails failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@ResponseBody
	@RequestMapping(value = "/queryAppointmentDetail/{invoiceNo}", method = RequestMethod.GET)
	public ResponseResult queryAppointmentDetail(@PathVariable("invoiceNo") String invoiceNo) {

		try {
			if (StringUtils.isEmpty(invoiceNo)) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "invoiceNo cannot be null.");
			} else {
				responseResult = mmsService.getAppointmentDetail(invoiceNo);
			}

		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " queryAppointmentDetail failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@ResponseBody
	@RequestMapping(value = "/queryLatestAppointmentDetail/{invoiceNo}", method = RequestMethod.GET)
	public ResponseResult queryLatestAppointmentDetail(@PathVariable("invoiceNo") String invoiceNo) {

		try {
			if (StringUtils.isEmpty(invoiceNo)) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "invoiceNo cannot be null.");
			} else {
				responseResult = mmsService.getLatestAppointmentDetail(invoiceNo);
			}

		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " queryLatestAppointmentDetail failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@ResponseBody
	@RequestMapping(value = "/queryTherapist", method = RequestMethod.GET)
	public ResponseResult queryAppointmentDetail(@RequestParam(value = "therapistCode", required = true) String therapistCode,
			@RequestParam(value = "requestDate", required = false) String requestDate) {

		if (mmsLog.isDebugEnabled()) {
			mmsLog.debug("queryAppointmentDetail start.");
		}
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			if (requestDate != null && !"".equals(requestDate)) {
				Date inputDate = df.parse(requestDate);
				Therapist therapist = mmsService.getTherapistInfo(therapistCode, inputDate);
				responseResult.initResult(GTAError.Success.SUCCESS, therapist);
			} else {
				Therapist therapist = mmsService.getTherapistInfo(therapistCode, new Date());
				responseResult.initResult(GTAError.Success.SUCCESS, therapist);
			}

		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " queryAppointmentDetail failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}

		return responseResult;
	}

	@ResponseBody
	@RequestMapping(value = "/createReservation", method = RequestMethod.POST)
	public ResponseResult createReservation(HttpServletRequest request,@RequestBody SpaAppointmentDto spaAppointmentDto) {

		mmsLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), JSONObject.fromObject(spaAppointmentDto).toString(), RequestMethod.POST.name(), null));
		
		try {
			// 判断服务时间是否在有限时间内！
			if (customerServiceAccService.isCustomerServiceAccValidByDate(spaAppointmentDto.getCustomerId(), DateCalcUtil.parseDateTime(spaAppointmentDto.getServiceDetails().get(0).getStartDatetime()))) {
				// add validation
				UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(
						spaAppointmentDto.getCustomerId(),UsageRightCheckService.FacilityCode.WELLNESS,null);
				if (!facilityRight.isCanBook()) {
					mmsLog.error("The date of this reservation is beyond your patronage period");
					throw new GTACommonException(GTAError.FacilityError.NO_PERMISSION);
				}
				if(spaAppointmentDto.getCreatedBy()==null||StringUtils.isEmpty(spaAppointmentDto.getCreatedBy())){
					spaAppointmentDto.setCreatedBy(this.getUser().getUserId());	
				}
				
				if(globalParameterService.checkGlobalParameterSwitch(GlobalParameterType.WELLNESS_SWITCH.getCode())){
					if(PaymentMethod.AMEX.getCardCode().equals(spaAppointmentDto.getPaymentMethod())){
						responseResult=globalParameterService.checkPaymentCardTypeTurnOn(GlobalParameterType.PAYMENT_TYPE_AMEX.getCode());
						if(!responseResult.getReturnCode().equalsIgnoreCase(GTAError.Success.SUCCESS.getCode())){
							mmsLog.error("AMEX payment turn off.");
							return responseResult;
						}
					}
					/***
					 * SGG-3865
					 * when use APP reservation wellness complate and send mail to system config receive mail
					 */
					responseResult=mmsService.bookAppointment(spaAppointmentDto);
					if(Success.SUCCESS.getCode().equals(responseResult.getReturnCode()))
					{
						String device = CommUtil.getAccessDevice(request.getHeader("User-Agent"));
						if("Android".equals(device)||"IPhone".equals(device)||"iPad".equals(device)||"APP".equals(device))
						{
							mmsService.sendMailSystemConfigMail((MMSPaymentDto)responseResult.getDto());				
						}
					}
					return responseResult;
				}else{
					mmsLog.error("Wellness reservation is not available now !");
					responseResult.initResult(GTAError.CourseError.COURSE_ENROLLMENT_EXPIRED_SERVICE_ACC,"Wellness reservation is not available now !");
					return responseResult;
				}
				
			} else {
				mmsLog.error("This patron's service account effective period has exceed the course sessions period!");
				responseResult.initResult(GTAError.CourseError.COURSE_ENROLLMENT_EXPIRED_SERVICE_ACC);
				return responseResult;
			}
			
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			if (null != e.getError()) {
				if (null != e.getArgs() && e.getArgs().length > 0)
					responseResult.initResult((GTAError) e.getError(), e.getArgs());
				else
					responseResult.initResult((GTAError) e.getError());
			} else
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getErrorMsg());
			return responseResult;
		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " createReservation failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	@ResponseBody
	@RequestMapping(value = "/cancelReservation", method = RequestMethod.PUT)
	public ResponseResult cancelReservation(HttpServletRequest request,@RequestParam(value = "invoiceNo", required = true) String invoiceNo) {
		mmsLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), invoiceNo, RequestMethod.PUT.name(), null));
		try {
			if (StringUtils.isEmpty(invoiceNo)) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "invoiceNo cannot be null.");
				return responseResult;
			} else {
				String userId = getUser().getUserId();
				ResponseResult rs = mmsService.canCelAppointment(invoiceNo, userId);
				if ("0".equals(rs.getReturnCode()))
					spaAppointmentRecService.sendSmsWhenCancelSpaOrder(invoiceNo);
				return rs;
			}
		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " cancelReservation failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	@ResponseBody
	@RequestMapping(value = "/cancelService", method = RequestMethod.PUT)
	public ResponseResult cancelService(@RequestParam(value = "appointmentId", required = true) String appointmentId) {
		try {
			if (StringUtils.isEmpty(appointmentId)) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "appointmentId cannot be null.");
				return responseResult;
			} else {
				String userId = getUser().getUserId();
				ResponseResult rs = mmsService.canCelAppointmentService(appointmentId, userId);
				if ("0".equals(rs.getReturnCode()))
					spaAppointmentRecService.sendSmsWhenCancelSpaOrderItem(appointmentId);
				return rs;
			}
		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " cancelService failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	@ResponseBody
	@RequestMapping(value = "/payment", method = RequestMethod.POST)
	public ResponseResult payment(@RequestBody MMSPaymentDto paymentDto) {
		try {
			paymentDto.setIsRemote("N");
			return mmsService.payment(paymentDto);

		} catch (GTACommonException e) {
			e.printStackTrace();
			mmsLog.error(e.getMessage());
			if (null != e.getError()) {
				if (null != e.getArgs() && e.getArgs().length > 0)
					responseResult.initResult((GTAError) e.getError(), e.getArgs());
				else
					responseResult.initResult((GTAError) e.getError());
			} else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " payment failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	/**
	 * 获取账号信息【下订单之前 判断服务时间是否在有限时间内！】
	 * 
	 * @param customerId
	 * @param serviceTime
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getMemberCashValue", method = RequestMethod.GET)
	public ResponseResult getMemberCashValue(@RequestParam(value = "customerId") @EncryptFieldInfo Long customerId, @RequestParam(value = "serviceTime") Date serviceTime) {
		try {
			if (StringUtils.isEmpty(String.valueOf(customerId)) || serviceTime == null) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "customerId cannot be null.");
			} else {
				UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(
						Long.valueOf(customerId),UsageRightCheckService.FacilityCode.WELLNESS,serviceTime);
				if (!facilityRight.isCanBook()) {
					throw new GTACommonException(GTAError.FacilityError.NO_PERMISSION);
				}
				// 判断服务时间是否在有限时间内！
				if (customerServiceAccService.isCustomerServiceAccValidByDate(customerId, serviceTime)) {
					responseResult = mmsService.getMemberInfo(String.valueOf(customerId));
				} else {
					responseResult.initResult(GTAError.CourseError.COURSE_ENROLLMENT_EXPIRED_SERVICE_ACC);
				}
			}
		} catch (GTACommonException e) {
			// e.printStackTrace();
			mmsLog.error(e.getMessage(), e);
			if (null != e.getError()) {
				if (null != e.getArgs() && e.getArgs().length > 0)
					responseResult.initResult((GTAError) e.getError(), e.getArgs());
				else
					responseResult.initResult((GTAError) e.getError());
			} else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());

		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " payment failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());

		}
		return responseResult;
	}

	@ResponseBody
	@RequestMapping(value = "/getMemberCashValue/{customerId}", method = RequestMethod.GET)
	public ResponseResult getMemberCashValue(@PathVariable(value = "customerId") @EncryptFieldInfo  Long customerId) {
		try {
			if (StringUtils.isEmpty(String.valueOf(customerId))) {
				responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, "customerId cannot be null.");
			} else {

				responseResult = mmsService.getMemberInfo(String.valueOf(customerId));
			}
		} catch (GTACommonException e) {
			e.printStackTrace();
			mmsLog.error(e.getMessage());
			if (null != e.getError()) {
				if (null != e.getArgs() && e.getArgs().length > 0)
					responseResult.initResult((GTAError) e.getError(), e.getArgs());
				else
					responseResult.initResult((GTAError) e.getError());
			} else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());

		} catch (Exception e) {
			mmsLog.error(MMSController.class.getName() + " payment failed!", e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION, e.getMessage());

		}
		return responseResult;
	}

	@RequestMapping(value = "/transactionstatus/{transactionNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getTransactionStatus(@PathVariable(value = "transactionNo") Long transactionNo) {

		mmsLog.info("MMSController.getTransactionStatus start ...");

		try {
			Map<String, Object> resultMap = new HashMap<String, Object>();
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			if (customerOrderTrans == null) {
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "TransactionNo Not found.");
				return responseResult;
			}
			CustomerOrderHd customerOrderHd = customerOrderTrans.getCustomerOrderHd();
			resultMap.put("orderNo", customerOrderHd.getOrderNo());
			resultMap.put("status", customerOrderHd.getOrderStatus());
			resultMap.put("transactionStatus", customerOrderTrans);
			mmsLog.info("MMSController.getTransactionStatus invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultMap);
			return responseResult;
		} catch (Exception e) {
			// e.printStackTrace();
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}
	}

	@RequestMapping(value = "/generalSetting", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAdvancedResPeriod() {
		return globalParameterService.getGlobalParameterById(AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
	}

	@RequestMapping(value = "/uploadAsset", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult upload(HttpServletRequest request, @RequestParam("file") MultipartFile file, @RequestParam("type") String type)
			throws IOException {

		try {
			if (StringUtils.isEmpty(type))
				type = "WP";
			InputStream input = file.getInputStream();
			String fileName = file.getOriginalFilename();

			BufferedImage img = ImageIO.read(input);

			int width = img.getWidth();
			int height = img.getHeight();
			if (mmsLog.isDebugEnabled()) {
				mmsLog.debug("fileName:" + fileName + "width:" + img.getWidth() + " height:" + img.getHeight());
			}
			if (!fileName.toLowerCase().endsWith(".jpg") && !fileName.toLowerCase().endsWith(".jpeg") && !fileName.toLowerCase().endsWith(".png")) {
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Only support jpg/jpeg/png format.");
				return responseResult;
			}
			if ("WP".equals(type)) {
				if (width != 430 || height != 75) {
					responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Please select a picture of 430x75px");
					return responseResult;
				}
			} else if ("APP".equals(type)) {
				if (width != 480 || height != 280) {
					responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Please select a picture of 480x280px");
					return responseResult;
				}
			} else if ("GS".equals(type)) {
				if (width != 1080 || height != 290) {
					responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Please select a picture of 1080x290px");
					return responseResult;
				}
			} else if ("RET".equals(type)) {
				if (width != 260 || height != 260) {
					responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, "Please select a picture of 260x260px");
					return responseResult;
				}
			}
			String saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.WELLNESS);

			File serverFile = new File(saveFilePath);
			saveFilePath = serverFile.getName();
			Map<String, String> imgPathMap = new HashMap<String, String>();
			imgPathMap.put("fileName", saveFilePath);
			responseResult.initResult(GTAError.Success.SUCCESS, imgPathMap);
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
			return responseResult;
		}

		return responseResult;
	}

	@RequestMapping(value = "/updateSpaCenterInfo", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateSpaCenterInfo(@RequestBody SpaCenterInfoDto dto) {
		try {
			LoginUser user = super.getUser();
			String userId = user.getUserId();
			return mmsService.updateSpaCenterInfo(dto, userId);

		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/getSpaCenterInfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaCenterInfo() {
		try {
			SpaCenterInfoDto dto = mmsService.getSpaCenterInfo();
			responseResult.initResult(GTAError.Success.SUCCESS, dto);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	
	/**
	 * query root(LV1) category 
	 * @return
	 */
	@RequestMapping(value = "/getSpaCategory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaCategory() {
		try {
			Long latestPicId = mmsService.getLatestUpdatedPicId();
			List<SpaCategoryDto> resultList = mmsService.getSpaCategoryList();
			SpaCategoryResponseDto response = new SpaCategoryResponseDto();
			response.setLatestUpdatedPicId(latestPicId);
			response.setSpaCategory(resultList);
			responseResult.initResult(GTAError.Success.SUCCESS, response);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	/**
	 * query All category 
	 * @return
	 */
	@RequestMapping(value = "/getALLSpaCategory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllSpaCategory() {
		try {
			Long latestPicId = mmsService.getLatestUpdatedPicId();
			List<SpaCategoryDto> resultList = mmsService.getALLSpaCategoryList();
			SpaCategoryResponseDto response = new SpaCategoryResponseDto();
			response.setLatestUpdatedPicId(latestPicId);
			response.setSpaCategory(resultList);
			responseResult.initResult(GTAError.Success.SUCCESS, response);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	
	

	@RequestMapping(value = "/updateCategoryImage", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateCategoryImage(@RequestBody List<SpaPicPathDto> dtoList) {
		try {
			responseResult = mmsService.updateSpaCategoryPic(dtoList, getUser().getUserId());

		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/getSpaRetreatList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRetreatList(@RequestParam(value = "filters", required = false, defaultValue = "") String filters,
			@RequestParam(value = "sortBy", required = false, defaultValue = "displayOrder") String sortBy,
			@RequestParam(value = "isAscending", required = false, defaultValue = "true") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
			@RequestParam(value = "status", required = false) String status, @RequestParam(value = "device", required = false) String device,
			@RequestParam(value = "categoryId", required = false, defaultValue = "") String categoryId
			) {
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try {
			if (StringUtils.isEmpty(device)) {
				device = "PC";
			}

			String joinHQL = mmsService.getSpaRetreatListSql(categoryId);
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);

			if (!StringUtils.isEmpty(status)) {
				joinHQL += " where m.status = '" + status + "'";
			}

			ResponseResult rs = null;
			if (advanceQueryDto != null && advanceQueryDto.getRules() != null && advanceQueryDto.getRules().size() > 0
					&& advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
				mmsLog.debug("get advance search result.");
				if (!StringUtils.isEmpty(status)) {
					joinHQL += " and";
				} else {
					if (!"PC".equals(device))// for webportal and app, only can display those records with status 'Show'
					{
						joinHQL += " where m.status = 'Show'";
					} else {
						joinHQL += " where ";
					}
				}
				advanceQueryDto.setSortBy(sortBy);
				advanceQueryDto.setAscending(isAscending);
				rs = advanceQueryService.getAdvanceQueryCustomizedResultBySQL(advanceQueryDto, joinHQL, page, SpaRetreatDto.class);
			} else {
				if (!"PC".equals(device) && StringUtils.isEmpty(status)) // for webportal and app, only can display those records with status 'Show'
				{
					joinHQL += " where m.status = 'Show'";
				}
				mmsLog.debug("getInitial search result.");
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				rs = advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, SpaRetreatDto.class);
			}
			return rs;

		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	/**
	 * same logic as "/getSpaRetreatList", only change the API's naming
	 * @param filters
	 * @param sortBy
	 * @param isAscending
	 * @param pageNumber
	 * @param pageSize
	 * @param status
	 * @param device
	 * @param categoryId
	 * @return
	 */
	@RequestMapping(value = "/getFacilityItems", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityItems(@RequestParam(value = "filters", required = false, defaultValue = "") String filters,
			@RequestParam(value = "sortBy", required = false, defaultValue = "displayOrder") String sortBy,
			@RequestParam(value = "isAscending", required = false, defaultValue = "true") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
			@RequestParam(value = "status", required = false) String status, @RequestParam(value = "device", required = false) String device,
			@RequestParam(value = "categoryId", required = false, defaultValue = "") String categoryId
			) {
		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try {
			if (StringUtils.isEmpty(device)) {
				device = "PC";
			}

			String joinHQL = mmsService.getSpaRetreatListSql(categoryId);
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);

			if (!StringUtils.isEmpty(status)) {
				joinHQL += " where m.status = '" + status + "'";
			}

			ResponseResult rs = null;
			if (advanceQueryDto != null && advanceQueryDto.getRules() != null && advanceQueryDto.getRules().size() > 0
					&& advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
				mmsLog.debug("get advance search result.");
				if (!StringUtils.isEmpty(status)) {
					joinHQL += " and";
				} else {
					if (!"PC".equals(device))// for webportal and app, only can display those records with status 'Show'
					{
						joinHQL += " where m.status = 'Show'";
					} else {
						joinHQL += " where ";
					}
				}
				advanceQueryDto.setSortBy(sortBy);
				advanceQueryDto.setAscending(isAscending);
				rs = advanceQueryService.getAdvanceQueryCustomizedResultBySQL(advanceQueryDto, joinHQL, page, SpaRetreatDto.class);
			} else {
				if (!"PC".equals(device) && StringUtils.isEmpty(status)) // for webportal and app, only can display those records with status 'Show'
				{
					joinHQL += " where m.status = 'Show'";
				}
				mmsLog.debug("getInitial search result.");
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				rs = advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, SpaRetreatDto.class);
			}
			return rs;

		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/updateSpaRetreat", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateSpaRetreat(@RequestBody SpaRetreatDto dto) {
		try {
			mmsService.updateSpaRetreat(dto, getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/addSpaRetreat", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult addSpaRetreat(@RequestBody SpaRetreatDto dto) {
		try {
			mmsService.updateSpaRetreat(dto, getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/getSpaRetreatItemList/{retId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRetreatItemList(@RequestParam(value = "filters", required = false, defaultValue = "") String filters,
			@RequestParam(value = "sortBy", required = false, defaultValue = "itemId") String sortBy,
			@RequestParam(value = "isAscending", required = false, defaultValue = "true") String isAscending,
			@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
			@RequestParam(value = "status", required = false) String status, @PathVariable(value = "retId") Long retId,
			@RequestParam(value = "device", required = false) String device) {

		page.setNumber(pageNumber);
		page.setSize(pageSize);

		try {
			if (StringUtils.isEmpty(device)) {
				device = "PC";
			}

			String joinHQL = mmsService.getSpaRetreatItemListSql(retId);
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) parseJson(filters, AdvanceQueryDto.class);
			if (!StringUtils.isEmpty(status)) {
				joinHQL += " where a.status = '" + status + "'";
			}
			ResponseResult rs = null;
			if (advanceQueryDto != null && advanceQueryDto.getRules() != null && advanceQueryDto.getRules().size() > 0
					&& advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
				// joinHQL += " where ";
				mmsLog.debug("get advanceSearch result.");
				if (!StringUtils.isEmpty(status)) {
					joinHQL += " and";
				} else {
					if (!"PC".equals(device))// for webportal and app, only can display those records with status 'Show'
					{
						joinHQL += " where a.status = 'Show'";
					} else {
						joinHQL += " where ";
					}
				}
				advanceQueryDto.setSortBy(sortBy);
				advanceQueryDto.setAscending(isAscending);
				rs = advanceQueryService.getAdvanceQueryCustomizedResultBySQL(advanceQueryDto, joinHQL, page, SpaRetreatItemDto.class);
			} else {
				mmsLog.debug("getInitial search result.");
				if (!"PC".equals(device) && StringUtils.isEmpty(status)) // for webportal and app, only can display those records with status 'Show'
				{
					joinHQL += " where a.status = 'Show'";
				}
				// no advance condition search
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				queryDto.setSortBy(sortBy);
				queryDto.setAscending(isAscending);
				rs = advanceQueryService.getInitialQueryCustomizedResultBySQL(queryDto, joinHQL, page, SpaRetreatItemDto.class);
			}

			return rs;
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/updateSpaRetreatItem", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateSpaRetreatItem(@RequestBody SpaRetreatItemDto dto) {
		try {
			mmsService.updateSpaRetreatItem(dto, getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/addSpaRetreatItem", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult addSpaRetreatItem(@RequestBody SpaRetreatItemDto dto) {
		try {
			mmsService.updateSpaRetreatItem(dto, getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/switchRetreatOrder", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult switchRetreatOrder(@RequestParam(value = "sourceRetId", required = true) Long sourceRetId,
			@RequestParam(value = "moveType", required = true) String moveType) {
		try {
			mmsService.switchRetreatOrder(sourceRetId, moveType, super.getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/sendReceipt", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult sendReceipt(@RequestParam(value = "invoiceNo", required = true) String invoiceNo) {
		try {
			return mmsService.sendReceipt(invoiceNo);
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/spaRefundList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSpaRefundList(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "createDate") String sortBy,
			@RequestParam(value = "isAscending", defaultValue = "false") String isAscending,
			@RequestParam(value = "status", defaultValue = "ALL") String status,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			mmsLog.info("Ready to get spa refund list");
			if(status.equalsIgnoreCase("ALL")){//如果是查询所有的spaRefund，则从spa系统查询
				responseResult = mmsService.getSpaRefundList(pageNumber, pageSize, sortBy, isAscending, status, filters);
			}else{//如果仅查询refunded状态的refunded,则从本地数据库查询
				page.setNumber(pageNumber);
				page.setSize(pageSize);
				responseResult = this.mmsService.getRefundedSpaPaymentList(page);
			}
			mmsLog.info("spa refund list retrieve end...");
			return responseResult;
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}

	@RequestMapping(value = "/spaRefundRequest", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult generateRefundRequest(@RequestParam(value = "dateTime", required = false) String dateTime,
			@RequestParam("patronId") String patronId,
			@RequestParam("paymentType") String paymentType,
			@RequestParam("amount") BigDecimal amount,
			@RequestParam("refInvoiceId") String refInvoiceId,
			@RequestParam("refPaymentId") String refPaymentId,
			@RequestParam("creditCard") String creditCard,
			@RequestParam(value = "refundRequest", required = false) Long refundRequest, 
			@RequestParam(value = "remark", required = false) String remark, 
			@RequestParam(value="centerrcptno",required=true)String centerrcptno,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			mmsLog.info("Ready to generate spa refund request");
			String userId = this.getUser().getUserId();
			responseResult = this.mmsService.generateRefundRequest(
					dateTime,
					patronId,
					(PaymentMethod.CASHVALUE.getDesc().equalsIgnoreCase(paymentType)?PaymentMethod.CASHVALUE.getCardCode():paymentType),
					amount,
					refInvoiceId,
					refPaymentId,
					creditCard,
					userId,
					refundRequest,
					remark,
					centerrcptno);
			mmsLog.info("generate spa refund request end...");
			return responseResult;
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
	
	
	/**
	 * 获取用CashValue的MMS支付数据
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param status
	 * @param isAscending
	 * @param filters
	 * @param dateRange
	 * @return
	 */
	@RequestMapping(value = "/getCashValueByMMS", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCashValueByMMS(@RequestParam(value="pageNumber", defaultValue= "1", required = false) Integer pageNumber,
			@RequestParam(value="pageSize", defaultValue= "10", required = false) Integer pageSize,
			@RequestParam(value="sortBy",defaultValue="transactionDate", required = false) String sortBy,
			@RequestParam(value="status",defaultValue="ALL", required = false) String status,
			@RequestParam(value="isAscending",defaultValue="false", required = false) String isAscending,
			@RequestParam(value="filters",defaultValue="", required = false) String filters,
			@RequestParam(value = "dateRange", defaultValue = "ALL") String dateRange) 
	{
		logger.info("CustomerOrderTransController.CashValueByMMS start ...");
		try {
			/***
			 *  item is MMS00000001
			 */
			String joinSQL = customerOrderTransService.getCashValueByMMS(dateRange);//
			
			ResponseResult tmpResult = null;
			AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
			if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
				joinSQL = joinSQL + " AND ";
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);
				tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, CashValueLogDto.class);
			}
			else
			{
				AdvanceQueryDto queryDto = new AdvanceQueryDto();
				prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
				tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, CashValueLogDto.class);
			}
			if (tmpResult.getListData().getRecordCount() > 0) {
				for (Object obj : tmpResult.getListData().getList()) {
					CashValueLogDto dto = (CashValueLogDto)obj;
					if (null == dto.getRemark() || dto.getRemark().equals("")) {
						dto.setRemark("");
						continue;
					}
					String[] remark = dto.getRemark().split("@#;");
					dto.setRemark(remark.length == 1 ? "" : remark[1]);
				}
			}
			
			
			return tmpResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	/**
	 * 手动添加MMS CashValue付款记录
	 * @param dto
	 * @return
	 */
	@RequestMapping(value = "/manualAddMMSCashValuePaymenyDB/{academyNo}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult manualAddMMSCashValuePaymenyDB(@PathVariable("academyNo") String academyNo,@RequestBody MMSPaymentDto dto) {
		try {
			if (academyNo.equals("") || null == dto.getInvoiceNo() || dto.getInvoiceNo().equals("")) {
				responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
				return responseResult;
			}
			Member member = memberService.getMemberByAcademyNo(academyNo);
			if (null == member) {
				responseResult.initResult(GTAError.DependentMemberError.ACADEMY_NO_NULL);
				return responseResult;
			}
//			MMSPaymentDto paymentDto = new MMSPaymentDto();
			dto.setCustomerId(member.getCustomerId());
			dto.setIsRemote("N");
			dto.setRemoteType(Constant.MMSTHIRD_ITEM_NO);
			dto.setPaymentMethod(PaymentMethod.CASHVALUE.name());
			dto.setAppointmentInfo("manualAddMMSCashValuePaymenyDB");
			dto.setCreatedBy(this.getUser().getUserId());
			MMSPaymentDto result = mmsService.manualAddMmsPayment(dto, this.getUser().getUserId());
			responseResult.initResult(GTAError.Success.SUCCESS, result);
//			ResponseResult result =  mmsService.payment(dto);	
//			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (GTACommonException e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			mmsLog.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, e.getMessage());
		}
		return responseResult;
	}
}
