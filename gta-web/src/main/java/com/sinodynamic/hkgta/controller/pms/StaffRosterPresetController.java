package com.sinodynamic.hkgta.controller.pms;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.pms.StaffRosterWeeklyPresetDto;
import com.sinodynamic.hkgta.entity.pms.StaffRoster;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.pms.StaffRosterWeeklyPresetService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@Scope("prototype")
public class StaffRosterPresetController extends ControllerBase<StaffRoster> {

	@Autowired
	private StaffRosterWeeklyPresetService	staffRosterWeeklyPresetService;

	@RequestMapping(value = "/staffRosterPreset", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getStaffRoster(@RequestParam(value = "presetName", required = true) String presetName) {
		try {
			StaffRosterWeeklyPresetDto staffRosterWeeklyPresetDto = staffRosterWeeklyPresetService.getStaffRoster(presetName);
			responseResult.initResult(GTAError.Success.SUCCESS, staffRosterWeeklyPresetDto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/staffRosterPreset/search", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult searchStaffRoster(String presetName) {
		try {
			Set<String> searchStaffRoster = staffRosterWeeklyPresetService.searchStaffRoster(presetName);
			responseResult.initResult(GTAError.Success.SUCCESS, searchStaffRoster);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/staffRosterPreset", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(@RequestBody StaffRosterWeeklyPresetDto staffRosterDto) {
		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			staffRosterWeeklyPresetService.save(staffRosterDto, userId);
			responseResult.initResult(GTAError.Success.SUCCESS, staffRosterDto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/staffRosterPreset/{presetName}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@RequestBody StaffRosterWeeklyPresetDto staffRosterDto, @PathVariable(value = "presetName") String presetName) {
		try {
			LoginUser user = getUser();
			String userId = user.getUserId();
			staffRosterWeeklyPresetService.update(staffRosterDto, userId, presetName);
			responseResult.initResult(GTAError.Success.SUCCESS, staffRosterDto);
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(e.getError());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}

	@RequestMapping(value = "/staffRosterPreset", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteStaffRoster(String presetName) {
		try {
			staffRosterWeeklyPresetService.deleteStaffRoster(presetName);
			responseResult.initResult(GTAError.Success.SUCCESS);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.setErrorMsg(initErrorMsg(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION));
		}
		return responseResult;
	}
}
