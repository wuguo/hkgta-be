package com.sinodynamic.hkgta.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.sys.CarparkInterfaceIoDto;
import com.sinodynamic.hkgta.service.sys.CarparkInterfaceIoService;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/carpark")
public class CarParkSystemController extends ControllerBase {

	@Autowired
	private CarparkInterfaceIoService carParkSystemService;

	@RequestMapping(value = "/createCarparkInterface", method = RequestMethod.POST)
	public @ResponseBody ResponseResult createCarparkInterface(@RequestBody CarparkInterfaceIoDto carparkInterfaceIoDto) {
		return carParkSystemService.createCarparkInterface(carparkInterfaceIoDto);
	}
	
	@RequestMapping(value = "/checkCarparkInterface/{academyNo}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult checkCarparkInterface(@PathVariable(value = "academyNo") String academyNo) {
		return carParkSystemService.checkCarparkInterface(academyNo);
	}
	
	@RequestMapping(value = "/updateCarparkInterface", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult updateCarparkInterface(@RequestParam("academyNo") String academyNo) {
		return carParkSystemService.updateCarparkInterface(academyNo);
	}
}