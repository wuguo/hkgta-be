package com.sinodynamic.hkgta.controller.crm.backoffice.advertise;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageDto;
import com.sinodynamic.hkgta.dto.crm.AdvertiseImageUploadDto;
import com.sinodynamic.hkgta.entity.crm.AdvertiseImage;
import com.sinodynamic.hkgta.service.crm.backoffice.advertise.AdvertiseImageService;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/advertise")
@Scope("prototype")
public class AdvertiseImageController extends ControllerBase<AdvertiseImage> {

	@Autowired
	private AdvertiseImageService advertiseImageService;

	@RequestMapping(value = "/{appType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAdvertiseImagesByAppType(@PathVariable(value = "appType") String appType) {
		return advertiseImageService.getList(appType, null);
	}

	@RequestMapping(value = "/{appType}/{dispLoc}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAdvertiseImagesByAppTypeDispLoc(@PathVariable(value = "appType") String appType, @PathVariable(value = "dispLoc") String dispLoc) {
		return advertiseImageService.getList(appType, dispLoc);
	}

	@RequestMapping(value = "/{appType}/{dispLoc}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult uploadAdvertiseImage(HttpServletRequest request, @PathVariable(value = "appType") String appType, @PathVariable(value = "dispLoc") String dispLoc,
			@RequestParam("file") MultipartFile file) throws IOException {

		String saveFilePath = saveImageToStorage(file);
		if (StringUtils.isEmpty(saveFilePath)) {
			responseResult.setErrorMsg(initErrorMsg(GTAError.AdvertiseImageError.IMG_UPLOAD_ERROR));
			return responseResult;
		}
		String userId = this.getUser().getUserId();
		AdvertiseImageDto dto = new AdvertiseImageDto();
		dto.setAppType(appType);
		dto.setDispLoc(dispLoc);
		dto.setFilepath(saveFilePath);
		dto.setCreateBy(userId);
		dto.setUpdateBy(userId);
		return advertiseImageService.create(dto);
	}

	@RequestMapping(value = "/{appType}/{dispLoc}/{imgId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult deleteAdvertiseImage(@PathVariable(value = "appType") String appType, @PathVariable(value = "dispLoc") String dispLoc,
			@PathVariable(value = "imgId") long imgId) throws IOException {

		AdvertiseImage a = advertiseImageService.get(imgId);
		if (a != null) {
			if (!StringUtils.isEmpty(a.getServerFilename()) && !deleteImageInStorage(a.getServerFilename())) {
				responseResult.setErrorMsg(initErrorMsg(GTAError.AdvertiseImageError.IMG_DELETE_ERROR));
				return responseResult;
			}
			return advertiseImageService.delete(a);
			
		} else {
			responseResult.setErrorMsg(initErrorMsg(GTAError.AdvertiseImageError.REQ_AD_NOT_EXISTS));
			return responseResult;
		}
	}

	private String saveImageToStorage(MultipartFile file) {
		String saveFilePath = null;
		try {
			saveFilePath = FileUpload.upload(file, FileUpload.FileCategory.ADVERTISE);
			File serverFile = new File(saveFilePath);
			saveFilePath = serverFile.getName();
		} catch (Exception e) {
			logger.error("Error saving advertise image to storage.", e);
		}
		return saveFilePath;
	}

	private boolean deleteImageInStorage(String filename) {
		try {
			FileUpload.deleteFile(filename, FileUpload.getBasePath(FileUpload.FileCategory.ADVERTISE));
			return true;
		} catch (Exception e) {
			logger.error("Error deleting advertise image in storage.", e);
			return false;
		}
	}

	@RequestMapping(value = "/image/{dispLoc}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult saveAdvertiseImages(@PathVariable(value = "dispLoc") String dispLoc,@RequestBody AdvertiseImageUploadDto advertiseImageDto) throws IOException {

		return advertiseImageService.createAdvertiseImages(dispLoc,advertiseImageDto,this.getUser().getUserId());
	}
	
	@RequestMapping(value = "/image/{dispLoc}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAdvertiseImageList(@PathVariable(value = "dispLoc") String dispLoc,@RequestParam(value = "appType",required=false) String appType) {
		return advertiseImageService.getAdvertiseImageList(appType, dispLoc);
	}
}
