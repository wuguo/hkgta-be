package com.sinodynamic.hkgta.controller.onlinepayment;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.paymentGateway.AmexResultDto;
import com.sinodynamic.hkgta.service.onlinepayment.AmexPaymentGatewayManager;
import com.sinodynamic.hkgta.service.onlinepayment.AmexPaymentGatewayService;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;

/**
 * payment gateway controller
 * 
 * @author Allen_Yu
 *
 */
@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
@RequestMapping("/amexPayment")
public class AmexPaymentGatewayController extends ControllerBase {
	@Autowired
	private AmexPaymentGatewayService amexPaymentGatewayService;
	
	@Autowired
	private AmexPaymentGatewayManager amexPaymentGatewayManager;

	/**
	 * return the callback url
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/callback", method = RequestMethod.GET)
	public @ResponseBody String processReturnData(HttpServletRequest request) {
		logger.info("call back come in :" + request.getRequestURL().toString());
		String loginUserId = getUser().getUserId();
		try {
			Map<String, String> fields = new HashMap<String, String>();
			/***
			 * call back reqeust params
			 */
			for (Enumeration enump = request.getParameterNames(); enump.hasMoreElements();) {
				String fieldName = (String) enump.nextElement();
				String fieldValue = request.getParameter(fieldName);
				if ((fieldValue != null) && (fieldValue.length() > 0)) {
					fields.put(fieldName, fieldValue);
				}
			}
			logger.info("call back...." + Log4jFormatUtil.logInfoMessage(request.getRequestURI(), fields.toString(),
					request.getMethod(), null));
			
			logger.info("call back amex payment call back to search AMEX result ..");
			AmexResultDto amexResultDto=amexPaymentGatewayManager.paymentCallBack(fields);
			
			return amexPaymentGatewayService.processResult(request, amexResultDto, loginUserId);
			
			
		} catch (Throwable e) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(baos));
			logger.error(e.getMessage());
			String htmlString = "<html><body  onload='closeWindow();'>"
					+ " the patron the payment is failed <input type='button' value='ok' onclick='transactionFail()' />"
					+ "<script>" + " function transactionFail() {" + " app.failure();" + " }"
					+ " function closeWindow() {" + " window.close();" + " }" + "</script>" + "</body></html>";
			return htmlString;
		}
	}
}
