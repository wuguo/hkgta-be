package com.sinodynamic.hkgta.controller.admin;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.entity.quartz.QrtzTriggers;
import com.sinodynamic.hkgta.scheduler.service.DownloadCarParkMemberTaskService;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.security.service.RoleProgramService;
import com.sinodynamic.hkgta.service.sys.SysAdminService;
import com.sinodynamic.hkgta.service.sys.UserRoleMap;
import com.sinodynamic.hkgta.service.sys.UserRoleService;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * for technical user (system admin) to controll the system without through the staff admin portal. 
 * @author Sam Hui
 *
 */
@SessionAttributes({"menuObj", "loginToken", "loginUser"})
@Controller
public class SysAdminController extends ControllerBase {
	@Autowired
	SysAdminService sysAdminService; 
	
	@Autowired
	DownloadCarParkMemberTaskService downloadCarParkMemberTaskService;

	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	UserRoleMap userRoleMap;	
	
	@Autowired
	private RoleProgramService roleProgramService;	
	
    /**
     * This is for system admin to generate the car park inteface file immediately rather than wait for the cron job to run
	 * @param secret to prevent attack 
     * @param model
     * @return
	 * @author Sam Hui (20160225 add)      
     */
	@RequestMapping(value = "/sysAdmin/{secret}/generate-carpark-csv", method = RequestMethod.GET)
	public @ResponseBody String generateCarParkCsv(@PathVariable("secret") String secret,  Model model) {
     	LoginUser loginUser=(LoginUser) model.asMap().get("loginUser");
		// System.out.println(" >    debug. Token for " + getUser().getUserId()  + " = " + loginToken);		
		if (loginUser==null || !"ADMIN".equalsIgnoreCase(loginUser.getUserType()) ) {			
			return "<h2>unauthorized access</h2>";
		}			
		
		downloadCarParkMemberTaskService.DownloadCarParkMemberList();
		
		return "success"; 
	}
	

	/**
	 * reload the menu to memory so that needn't restart tomcat to take effect when access right changed from DB  
	 * @param secret to prevent attack 
	 * @return
	 * @author Sam Hui (20160222 add)
	 */
	@ApiOperation("reload the menu to memory so that needn't restart tomcat to take effect when access right changed from DB")
	@RequestMapping(value = "/sysAdmin/{secret}/reload-Menu-mapping", method = RequestMethod.GET)
	public @ResponseBody ResponseResult reloadMenuMap(@PathVariable("secret") String secret, Model model) {		
     	LoginUser loginUser=(LoginUser) model.asMap().get("loginUser");
		// System.out.println(" >    debug. Token for " + getUser().getUserId()  + " = " + loginToken);		
		if (loginUser==null || !"ADMIN".equalsIgnoreCase(loginUser.getUserType()) ) {			
			responseResult.initResult(GTAError.LoginError.AUTHENTICATE_REQRIRED);
			return responseResult;
		}			
		userRoleMap.destroyUserRoleInstance();		
		roleProgramService.refreshRole();
			
//		model.addAttribute("menuObj", userRoleMap.getInstance().getRoleMenu()); // store to session for replicate to other cluster;
//		model.addAttribute("userRole", userRoleMap.getInstance().getUserRole()); // store to session for replicate to other cluster;		
		//model.addAttribute("menuObj", responseResult); // store to session for replicate to other cluster;				

		//System.out.println(" > user type=" + loginUser.getUserType());
		
		return userRoleService.loadUserPermissionsV2(loginUser.getUserId(),roleProgramService.initRoleMenu(loginUser.getUserId()));
	}		

	@RequestMapping(value = "/sysAdmin/{secret}/verify-server", method = RequestMethod.GET, produces="text/html;charset=UTF-8")
	public @ResponseBody String verifyServerEnvironment(@PathVariable("secret") String secret,  Model model) {
     	LoginUser loginUser=(LoginUser) model.asMap().get("loginUser");
		//System.out.println(" >    debug. Token for " + getUser().getUserId()  + " = " + loginToken);		
		if (loginUser==null || !"ADMIN".equalsIgnoreCase(loginUser.getUserType()) ) {			
			return "<h2>unauthorized access</h2>";
		}			

		return sysAdminService.chkProperties() + sysAdminService.chkPaymentGatewayCmd();
		
	}
	
	/**
	 * show the Quartz cron jobs and display invalid next trigger date in red  
	 * @param secret
	 * @param model
	 * @return html table
	 */
	@RequestMapping(value = "/sysAdmin/{secret}/cronjobs", method = RequestMethod.GET, produces="text/html;charset=UTF-8")
	public @ResponseBody String getCronJobs(@PathVariable("secret") String secret,  Model model) {
     	LoginUser loginUser=(LoginUser) model.asMap().get("loginUser");
		//System.out.println(" >    debug. Token for " + getUser().getUserId()  + " = " + loginToken);		
		if (loginUser==null || !"ADMIN".equalsIgnoreCase(loginUser.getUserType()) ) {			
			return "<h2>unauthorized access</h2>";
		}			

		Date currentDate=new Date();
		
		String html= "Current datetime is " +  CommUtil.dateFormat(currentDate, "yyyy-MM-dd HH:mm:ss") + "</br>" +
				     "<table><thead><th>job name</th>  <th>Next fire time</th>  <th>Prev fire time</th></thead><tbody >";
		List<QrtzTriggers> qrtzTriggers=sysAdminService.getCronTriggers();
		
		for (QrtzTriggers qt : qrtzTriggers  ) {
			html+="<tr>";
			html+="<td>"+qt.getJobName()+"</td>"; 
			if (qt.getNextFireTime() < currentDate.getTime() )
				html+="<td>&nbsp; &nbsp;<font color=red>"+ CommUtil.dateFormat(new Date(qt.getNextFireTime()), "yyyy-MM-dd HH:mm:ss") +"</font></td>";
			else	
			    html+="<td>&nbsp; &nbsp;"+ CommUtil.dateFormat(new Date(qt.getNextFireTime()), "yyyy-MM-dd HH:mm:ss") +"</td>";
			
			html+="<td>&nbsp; &nbsp;"+ CommUtil.dateFormat(new Date(qt.getPrevFireTime()), "yyyy-MM-dd HH:mm:ss") +"</td>";
			
			html+="</tr>";		
		}
		
		
		return html+="</tbody></table>";
		
	}
	
	/**
	 * for loadbalancer to health check the app server connection DB status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sysAdmin/ping", method = RequestMethod.GET, produces="text/html;charset=UTF-8")
	public @ResponseBody String healthCheck() throws Exception {
		sysAdminService.dbHealthChk();
		//let dbHealthChk throw exception if db connection fail  
		return "alive";
	}
}
