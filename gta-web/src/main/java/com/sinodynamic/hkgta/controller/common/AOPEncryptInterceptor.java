package com.sinodynamic.hkgta.controller.common;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.filter.EncryptTool;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;
import com.sinodynamic.hkgta.entity.GenericDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;

@Component
public class AOPEncryptInterceptor  implements MethodBeforeAdvice , AfterReturningAdvice  {		
     
	public Logger logger = Logger.getLogger(AOPEncryptInterceptor.class);
	
	@Override
	public void before(Method arg0, Object[] arg1, Object arg2)	throws Throwable {
		logger.debug("Class: "+arg2.getClass().getName()+";  Method: "+arg0.getName());
		try{
			//check if request parameter exists annotation
			String annotationParam="";
			Annotation[][]parameterAnnotations = arg0.getParameterAnnotations();    //get all annotations of parameter
		    for(int j=0;j<parameterAnnotations.length;j++){
	           int length = parameterAnnotations[j].length; 
	           if(length>0){
	              for(int k=0;k<length;k++){
	            	  if(parameterAnnotations[j][k] instanceof EncryptFieldInfo){
	            		  EncryptFieldInfo pa = (EncryptFieldInfo)parameterAnnotations[j][k];
		                  annotationParam=annotationParam+j+";";  
//		                  logger.info(j+" "+k+" "+pa.entrypt());   //get annotations info
//		                  logger.info(annotationParam +" annotationParam "+pa.comments());   
	            	  }	            	 
	              } 
	           }	              
	        }
		    
		    //check if request DTO exists annotation
			if(arg1!=null&&arg1.length>0){
				for(int i=0;i<arg1.length;i++){
					Object argObject=arg1[i];
					logger.info("Method: "+arg0.getName()+"; the parameter "+i+" is: "+argObject);
					//check if the argObject is a subclass of GenericDto	
					if(argObject!=null&&argObject instanceof GenericDto){
						 GenericDto genericDto=(GenericDto)argObject;										        	
						 commonDecryptValue(genericDto);
					//check if the argObject is List, and contain GenericDto	
					}else if(argObject!=null&&argObject instanceof List){	
						List dtoList=(List)argObject;				
						for(int il=0;il<dtoList.size();il++){
							//check if the List data is a subclass of GenericDto
							if(dtoList.get(il) instanceof GenericDto ){
								GenericDto genericDto =(GenericDto)dtoList.get(il);
								commonDecryptValue(genericDto) ; 
							}						 
						}						
			        //decrypt the value of request parameter	 
					}else if(annotationParam.contains(String.valueOf(i))&&argObject!=null){
						if(argObject instanceof java.lang.String){
							String encryptParam=(String)argObject;
	            			String afterEncryptParam=EncryptTool.getInstance().decryptByDES(encryptParam);
//	            			logger.info(encryptParam+" before and after DecryptLongValue"+afterEncryptParam);
	            			arg1[i] =afterEncryptParam;			
						}else if(argObject instanceof java.lang.Long[]){
							Long[] longArray=(Long[])argObject;
							for(int ll=0;ll<longArray.length;ll++){
								Long tempLongParam=longArray[ll];
								Long afterDecryptLongParam=decryptLongByDES(tempLongParam);
//			                	logger.info(tempLongParam+" before and after DecryptLongValue"+afterDecryptLongParam);
		                		longArray[ll] =afterDecryptLongParam;	
							}														
						}else{
							 Long encryptLongParam=(Long)argObject;
							 Long afterDecryptLongParam=decryptLongByDES(Long.valueOf(encryptLongParam));
//	                		 logger.info(encryptLongParam+" before and after DecryptLongValue"+afterDecryptLongParam);
	                		 arg1[i] =afterDecryptLongParam;
						}							        	 
					}					
				}
			}			
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}
 
	/**
	 * common decrypt method
	 * @param genericDto
	 * @throws Exception
	 */
	public void commonDecryptValue(GenericDto genericDto) throws Exception{
		 Field[] fields = genericDto.getClass().getDeclaredFields();
    	 for(Field field :fields){
             if(field.isAnnotationPresent(EncryptFieldInfo.class)){	                
            	 EncryptFieldInfo gtaFieldInfo = (EncryptFieldInfo) field.getAnnotation(EncryptFieldInfo.class);
            	 if(gtaFieldInfo!=null&&gtaFieldInfo.entrypt().equals("true")){
            		 String methodType="";
 					 String beforeDecryptValue="";
					 Method setMethod=null;
                	 Method[] methods = genericDto.getClass().getDeclaredMethods();
                	 for(Method method :methods){
                		 if(method.getName().equalsIgnoreCase("get"+field.getName())){
                			 String methodReturntype =	 method.getGenericReturnType().toString();
	                		 if (methodReturntype.equals("class java.lang.String")) {
	                			 beforeDecryptValue =String.valueOf(method.invoke(genericDto)); //call getter method to get the value
	                			 methodType=methodReturntype;
	                		 }else if (methodReturntype.equalsIgnoreCase("class java.lang.Long")) {
	                			 beforeDecryptValue =String.valueOf(method.invoke(genericDto)); //call getter method to get the value	 
	                			 methodType=methodReturntype;
	                		 }else if (methodReturntype.equalsIgnoreCase("class java.math.BigInteger")) {
	                			 beforeDecryptValue =String.valueOf(method.invoke(genericDto));  //call getter method to get the value 
	                			 methodType=methodReturntype;
	                		 }else{ 
		                		Object invokeObject=method.invoke(genericDto);
	            				if(invokeObject!=null&&invokeObject instanceof GenericDto){									        	
	        						 commonDecryptValue((GenericDto)invokeObject);
	        					}else if(invokeObject!=null&&invokeObject instanceof List){	
	        						List dtoList=(List)invokeObject;				
	        						for(int im=0;im<dtoList.size();im++){
	        							//check if the DTO list data is a subclass of GenericDto
	        							if(dtoList.get(im) instanceof GenericDto ){
	        								commonDecryptValue((GenericDto)dtoList.get(im)) ; 
	        							}						 
	        						}							 
	        					}
	                		 }
                		 }else if(method.getName().equalsIgnoreCase("set"+field.getName())){
                			 setMethod=method;				                		
                		 }
                	 }
                	 //decrypt the value of DTO
                	 if(beforeDecryptValue!=null&&!"".equals(beforeDecryptValue.trim())&&!"null".equals(beforeDecryptValue.trim())&&!"".equals(methodType.trim())&&setMethod!=null){				                		 
                		 if(methodType.equalsIgnoreCase("class java.lang.Long")){
                    		 Long afterDecryptLongValue=decryptLongByDES(Long.valueOf(beforeDecryptValue));
//                    		 logger.info(beforeDecryptValue+" before and after DecryptLongValue"+afterDecryptLongValue);
                    		 setMethod.invoke(genericDto,afterDecryptLongValue); //call setter method to set the value
                		 }else if(methodType.equalsIgnoreCase("class java.lang.String")){ 
                			 String afterDecryptValue=EncryptTool.getInstance().decryptByDES(beforeDecryptValue);
//                			 logger.info(beforeDecryptValue+" before and after DecryptValue"+afterDecryptValue);
	                		 setMethod.invoke(genericDto,afterDecryptValue); //call setter method to set the value
                		 }else if(methodType.equalsIgnoreCase("class java.math.BigInteger")){
                    		 Long afterDecryptLongValue=decryptLongByDES(Long.valueOf(beforeDecryptValue));
//                    		 logger.info(beforeDecryptValue+" before and after DecryptLongValue"+afterDecryptLongValue);
                    		 setMethod.invoke(genericDto,new BigInteger(String.valueOf(afterDecryptLongValue))); //call setter method to set the value
                		 }	
                	 }
            	 }	                	 
             }
    	 }
	}
	
	@Override
	public void afterReturning(Object arg0, Method arg1, Object[] arg2,Object arg3) throws Throwable {	
		try{
			if(arg0!=null&&arg0 instanceof ResponseResult){				
				ResponseResult responseResult=(ResponseResult)arg0;
				//check if the DTO is a subclass of GenericDto
				if(responseResult.getDto()!=null&&responseResult.getDto() instanceof GenericDto){
					GenericDto genericDto=(GenericDto)responseResult.getDto();	
					commonEncryptValue(genericDto) ;	
				//check if the List Data contain GenericDto	
				}else if(responseResult.getListData()!=null&&responseResult.getListData().getList()!=null){
					List dtoList=responseResult.getListData().getList();				
					for(int j=0;j<dtoList.size();j++){
						//check if the DTO list data is a subclass of GenericDto
						if(dtoList.get(j) instanceof GenericDto ){
							GenericDto genericDto =(GenericDto)dtoList.get(j);
							commonEncryptValue(genericDto) ; 
						}						 
					}
				//check if the DTO is a Map, and contain GenericDto	
				}else if(responseResult.getDto()!=null&&responseResult.getDto() instanceof Map){
						Map<Object, Object> dtoMap=(Map)responseResult.getDto();
						for(Map.Entry<Object, Object> entry:dtoMap.entrySet()){   
							if(entry.getValue()!=null&&entry.getValue() instanceof GenericDto){
								commonEncryptValue((GenericDto)entry.getValue()) ; 
							}if(entry.getValue()!=null&&entry.getValue() instanceof Data){
								Data data=(Data)entry.getValue();
								if(data.getList()!=null&&data.getList().size()>0){
									List dtoList=data.getList();				
									for(int j=0;j<dtoList.size();j++){
										//check if the list data is a subclass of GenericDto
										if(dtoList.get(j) instanceof GenericDto){
											GenericDto genericDto =(GenericDto)dtoList.get(j);
											commonEncryptValue(genericDto) ; 
										}						 
									}
								}
							}else if(entry.getValue()!=null&&entry.getValue() instanceof List){	
        						List dtoList=(List)entry.getValue();				
        						for(int jl=0;jl<dtoList.size();jl++){
        							if(dtoList.get(jl) instanceof GenericDto ){
        								commonEncryptValue((GenericDto)dtoList.get(jl)) ; 
        							}						 
        						}							 
        					}	  
						}						 									
						//check if the DTO is a List, and contain GenericDto	
				}else if(responseResult.getDto()!=null&&responseResult.getDto() instanceof List){
					List dtoList=(List)responseResult.getDto();				
					for(int k=0;k<dtoList.size();k++){
						if(dtoList.get(k) instanceof GenericDto ){
							commonEncryptValue((GenericDto)dtoList.get(k)) ; 
						}						 
					}
				}				
			//check if the arg0 is a subclass of GenericDto	
			}else if(arg0!=null&&arg0 instanceof GenericDto){
				commonEncryptValue((GenericDto)arg0) ;	
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}
	
    /**
     * common encrypt method	
     * @param genericDto
     * @throws Exception
     */
	public void commonEncryptValue(GenericDto genericDto) throws Exception{
		Field[] fields = genericDto.getClass().getDeclaredFields();
    	for(Field field :fields){
            if(field.isAnnotationPresent(EncryptFieldInfo.class)){	                
            	 EncryptFieldInfo gtaFieldInfo = (EncryptFieldInfo) field.getAnnotation(EncryptFieldInfo.class);
            	 if(gtaFieldInfo!=null&&gtaFieldInfo.entrypt().equals("true")){
            		 String beforeEncryptValue="";
            		 Method setMethod=null;
            		 String methodType="";
                	 Method[] methods = genericDto.getClass().getDeclaredMethods();
                	 for(Method method :methods){                		               		                 		 
                		 if(method.getName().equalsIgnoreCase("get"+field.getName())){
                			 String methodReturntype =	 method.getGenericReturnType().toString();
	                		 if (methodReturntype.equals("class java.lang.String")) {
	                			 beforeEncryptValue =String.valueOf(method.invoke(genericDto)); //call getter method to get the value
	                			 methodType=methodReturntype;
	                		 }else if (methodReturntype.equalsIgnoreCase("class java.lang.Long")) {
	                			 beforeEncryptValue =String.valueOf(method.invoke(genericDto)); //call getter method to get the value	 
	                			 methodType=methodReturntype;
	                		 }else if (methodReturntype.equalsIgnoreCase("class java.math.BigInteger")) {
	                			 beforeEncryptValue =String.valueOf(method.invoke(genericDto));  //call getter method to get the value 
	                			 methodType=methodReturntype;
	                		 }else{ 	            				 
	            				Object invokeObject=method.invoke(genericDto);	 
	            				if(invokeObject!=null&&invokeObject instanceof GenericDto){									        	
	        						 commonEncryptValue((GenericDto)invokeObject);
	        					}else if(invokeObject!=null&&invokeObject instanceof List){	
	        						List dtoList=(List)invokeObject;				
	        						for(int jm=0;jm<dtoList.size();jm++){
	        							if(dtoList.get(jm) instanceof GenericDto ){
	        								commonEncryptValue((GenericDto)dtoList.get(jm)) ; 
	        							}						 
	        						}							 
	        					}else if(invokeObject!=null&&invokeObject instanceof Set){	
	        						Set dtoList=(Set)invokeObject;
	        						Iterator iterator=dtoList.iterator();
	        						while(iterator.hasNext()){
	        							Object iteratorObject=iterator.next();
	        							if(iteratorObject instanceof GenericDto ){
	        								commonEncryptValue((GenericDto)iteratorObject) ; 
	        							}	
	        						}						 
	        					}
	                		 }
                		 }else if(method.getName().equalsIgnoreCase("set"+field.getName())){
                			 setMethod=method;				                		
                		 }
                	 }
                	 //encrypt the value 
                	 if(beforeEncryptValue!=null&&!"".equalsIgnoreCase(beforeEncryptValue.trim())&&!"null".equals(beforeEncryptValue.trim())&&!"".equals(methodType.trim())&&setMethod!=null){
                		 if(methodType.equalsIgnoreCase("class java.lang.Long")){
                    		 Long afterEncryptLongValue=encryptLongByDES(Long.valueOf(beforeEncryptValue));
//                    		 logger.info(beforeEncryptValue+" before and after EncryptLongValue"+afterEncryptLongValue);
                    		 setMethod.invoke(genericDto,afterEncryptLongValue); //call setter method to set the value
                		 }else if(methodType.equalsIgnoreCase("class java.lang.String")){
                    		 String afterEncryptValue=EncryptTool.getInstance().encryptByDES(beforeEncryptValue);
//                    		 logger.info(beforeEncryptValue+" before and after EncryptValue"+afterEncryptValue);
                    		 setMethod.invoke(genericDto,afterEncryptValue); //call setter method to set the value 
                		 }else if(methodType.equalsIgnoreCase("class java.math.BigInteger")){
                			 Long afterEncryptLongValue=encryptLongByDES(Long.valueOf(beforeEncryptValue));
//                    		 logger.info(beforeEncryptValue+" before and after EncryptLongValue"+afterEncryptLongValue);
                    		 setMethod.invoke(genericDto,new BigInteger(String.valueOf(afterEncryptLongValue))); //call setter method to set the value
                		 }		
                	 }                	                	                	 
            	}	                	 
            }
    	}
	}
	
	int EncryptMultiplyFactor=976248;
	int EncryptPlusFactor    =641289;
	
	/**
	 * encrypt Long Value by DES
	 * @param originalStr
	 * @return
	 */
	public Long encryptLongByDES(Long originalStr)
	{
		if(originalStr==null){
			return null;
		}
		Long encryptString = originalStr*EncryptMultiplyFactor+EncryptPlusFactor ;		 
		return encryptString;
	}
	
	/**
	 * decrypt Long Value by DES
	 * @param originalStr
	 * @return
	 */
	public Long decryptLongByDES(Long originalStr)
	{
		if(originalStr==null){
			return null;
		}
		Long decryptString = (originalStr-EncryptPlusFactor)/EncryptMultiplyFactor ;
		return decryptString;
	}
	
}
