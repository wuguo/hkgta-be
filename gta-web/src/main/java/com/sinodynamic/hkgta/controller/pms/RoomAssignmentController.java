package com.sinodynamic.hkgta.controller.pms;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.pms.RoomCrewDto;
import com.sinodynamic.hkgta.dto.pms.RoomDto;
import com.sinodynamic.hkgta.service.pms.HousekeepPresetParameterService;
import com.sinodynamic.hkgta.service.pms.RoomCrewService;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@Scope("prototype")
public class RoomAssignmentController extends ControllerBase {

	private Logger							logger	= Logger.getLogger(RoomAssignmentController.class);

	@Autowired
	private RoomCrewService					roomCrewService;

	@Autowired
	private RoomService						roomService;

	@Autowired
	private HousekeepPresetParameterService	housekeepPresetParameterService;

	@RequestMapping(value = "/assignRoomCrew/list/{positionTitleCode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAssignRoomList(@PathVariable(value = "positionTitleCode") String positionTitleCode) {
		logger.info("RoomController.getAssignRoomList start ...");
		try {
			List<RoomDto> result = roomService.getRoomCrewList(positionTitleCode);
			logger.info("RoomController.getAssignRoomList invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, result);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/assignRoomCrew/{roomId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAssignRoomDetail(@PathVariable(value = "roomId") Long roomId) {
		logger.info("RoomController.getAssignRoomDetail start ...");
		if (null == roomId) {
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try {
			List<RoomCrewDto> result = roomCrewService.getRoomCrewList(roomId);
			logger.info("RoomController.getAssignRoomDetail invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, result);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	//added by Kaster 20160331 针对housekeep APP只显示每个room的RA的需求新增该接口
	@RequestMapping(value = "/assignRoomCrewForHousekeep/{roomId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAssignRoomDetailForHousekeep(@PathVariable(value = "roomId") Long roomId) {
		logger.info("RoomController.getAssignRoomDetail start ...");
		if (null == roomId) {
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try {
			List<RoomCrewDto> result = roomCrewService.getRoomCrewListForHousekeep(roomId);
			logger.info("RoomController.getAssignRoomDetail invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, result);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/assignRoomCrew/{crewId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult removeRoomCrew(@PathVariable(value = "crewId") Long crewId) {
		logger.info("RoomController.removeRoomCrew start ...");
		if (null == crewId) {
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try {
			roomCrewService.removeRoomCrew(crewId);
			logger.info("RoomController.removeRoomCrew invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/assignRoomCrew", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult assignRoomCrew(@RequestBody RoomCrewDto roomCrewDto) {
		logger.info("RoomController.assignRoomCrew start ...");
		if (null == roomCrewDto || roomCrewDto.getRoomIds() == null || roomCrewDto.getRoomIds().length == 0 || roomCrewDto.getUserId() == null) {
			responseResult.initResult(GTAError.HouseKeepError.REQUESTBODY_ISNULL);
			return responseResult;
		}
		try {
			roomCrewDto.setUpdateBy(this.getUser().getUserId());
			roomCrewService.assignRoomCrew(roomCrewDto);
			logger.info("RoomController.assignRoomCrew invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.HouseKeepError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

}
