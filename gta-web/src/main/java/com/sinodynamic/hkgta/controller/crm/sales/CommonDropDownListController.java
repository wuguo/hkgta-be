package com.sinodynamic.hkgta.controller.crm.sales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.DropDownDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.DepartmentBranch;
import com.sinodynamic.hkgta.entity.crm.HkArea;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.crm.PositionTitle;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.StaffProfile;
import com.sinodynamic.hkgta.entity.crm.SysCode;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.service.crm.backoffice.DepartmentService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.TempPassService;
import com.sinodynamic.hkgta.service.crm.ibeacon.IbeaconService;
import com.sinodynamic.hkgta.service.crm.sales.HKAreaDistrictService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.StaffProfileService;
import com.sinodynamic.hkgta.service.crm.sales.SysCodeService;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@SuppressWarnings("rawtypes")
@Controller
@RequestMapping("/dropdownlists")
@Scope("prototype")
public class CommonDropDownListController extends ControllerBase {

	private static final String STATUS_ACTIVE = "ACT";

	private Logger logger = Logger.getLogger(CommonDropDownListController.class);

	@Autowired
	private ServicePlanService servicePlanService;

	@Autowired
	private SysCodeService sysCodeService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private StaffProfileService staffProfileService;

	@Autowired
	private TempPassService tempPassService;

	@Autowired
	private HKAreaDistrictService hkAreaDistrictService;

	@Autowired
	private IbeaconService ibeaconService;

	@Autowired
	private RoomService roomService;

	@Autowired
	private MemberService memberService;

	/**
	 * Method to get all service plan for notification center.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/servicePlanForNC", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getServicPlanForNC() {

		logger.info("CommonDropDownListController.getAllServicPlan invocation start...");

		try {

			List<ServicePlanDto> detailServicePlans = servicePlanService.getAllServicPlan();
			if (detailServicePlans == null || detailServicePlans.size() == 0)
				return null;

			List<DropDownDto> retData = new ArrayList<DropDownDto>();
			for (ServicePlanDto spd : detailServicePlans) {

				DropDownDto dto = new DropDownDto();
				dto.setCodeDisplay(spd.getPlanName());
				dto.setCodeValue(String.valueOf(spd.getPlanNo()));
				retData.add(dto);

			}

			logger.info("CommonDropDownListController.getAllServicPlan invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * Method to get service plan by type for notification center.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getServicPlanByType/{subscriberType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getServicPlanByType(@PathVariable(value = "subscriberType") String subscriberType) {

		logger.info("CommonDropDownListController.getServicPlanByType invocation start...");

		try {

			List<ServicePlanDto> detailServicePlans = servicePlanService.getAllServicPlanByType(subscriberType);
			if (detailServicePlans == null || detailServicePlans.size() == 0)
				return null;

			List<DropDownDto> retData = new ArrayList<DropDownDto>();
			for (ServicePlanDto spd : detailServicePlans) {

				DropDownDto dto = new DropDownDto();
				dto.setCodeDisplay(spd.getPlanName());
				dto.setCodeValue(String.valueOf(spd.getPlanNo()));
				retData.add(dto);

			}

			logger.info("CommonDropDownListController.getServicPlanByType invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * Method to get valid service plan, excluding not started, expired
	 * 
	 * @return
	 */
	@RequestMapping(value = "/serviceplan", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getValidServicPlan() {

		logger.info("CommonDropDownListController.getAllServicPlan invocation start...");

		try {

			List<ServicePlanDto> detailServicePlans = servicePlanService.getValidServicPlan();
			if (detailServicePlans == null || detailServicePlans.size() == 0)
				return null;

			List<DropDownDto> retData = new ArrayList<DropDownDto>();
			for (ServicePlanDto spd : detailServicePlans) {

				DropDownDto dto = new DropDownDto();
				dto.setCodeDisplay(spd.getPlanName());
				dto.setCodeValue(String.valueOf(spd.getPlanNo()));
				retData.add(dto);

			}

			logger.info("CommonDropDownListController.getAllServicPlan invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/serviceplan/corporateMember", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getValidServicPlanForCorporateMember() {

		try {

			List<ServicePlan> detailServicePlans = servicePlanService.getValidServicPlanForCorporateMember();
			if (detailServicePlans == null || detailServicePlans.size() == 0)
				return null;

			List<DropDownDto> retData = new ArrayList<DropDownDto>();
			for (ServicePlan spd : detailServicePlans) {

				if (STATUS_ACTIVE.equals(spd.getStatus())) {

					DropDownDto dto = new DropDownDto();
					dto.setCodeDisplay(spd.getPlanName());
					dto.setCodeValue(String.valueOf(spd.getPlanNo()));
					retData.add(dto);
				}
			}

			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * Method to get All service plan including not started, expired and valid
	 * 
	 * @return
	 */
	@RequestMapping(value = "/serviceplanAll", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllServicPlan() {

		logger.info("CommonDropDownListController.getAllServicPlan invocation start...");

		try {

			List<ServicePlanDto> detailServicePlans = servicePlanService.getAllServicPlan();
			if (detailServicePlans == null || detailServicePlans.size() == 0)
				return null;

			List<DropDownDto> retData = new ArrayList<DropDownDto>();
			for (ServicePlanDto spd : detailServicePlans) {

				if (STATUS_ACTIVE.equals(spd.getStatus())) {

					DropDownDto dto = new DropDownDto();
					dto.setCodeDisplay(spd.getPlanName());
					dto.setCodeValue(String.valueOf(spd.getPlanNo()));
					retData.add(dto);
				}
			}

			logger.info("CommonDropDownListController.getAllServicPlan invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/salesname/{enrollId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllSalesMan(@PathVariable(value = "enrollId") Long enrollId) {

		logger.info("CommonDropDownListController.getAllSalesMan invocation start...");

		try {
			enrollId = (enrollId == 0l ? null : enrollId);
			List<StaffProfile> StaffProfileList = staffProfileService.searchAllStaffProfile(enrollId);
			if (StaffProfileList == null || StaffProfileList.size() == 0) {
				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}

			List<DropDownDto> retData = new ArrayList<DropDownDto>();
			for (StaffProfile sp : StaffProfileList) {

				DropDownDto dto = new DropDownDto();
				dto.setCodeValue(sp.getUserId());
				dto.setCodeDisplay(sp.getGivenName() + " " + sp.getSurname());
				retData.add(dto);
			}

			logger.info("CommonDropDownListController.getAllSalesMan invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/departments", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDepartmentList() throws Exception {

		logger.info("CommonDropDownListController.getDepartmentList invocation start...");
		List<DropDownDto> dropdownlist = new ArrayList<DropDownDto>();

		List<DepartmentBranch> departments = departmentService.getAllDepartments();
		for (DepartmentBranch depart : departments) {
			DropDownDto dto = new DropDownDto();
			dto.setCodeDisplay(depart.getName());
			dto.setCodeValue(depart.getId().toString());
			dropdownlist.add(dto);
		}

		responseResult.initResult(GTAError.Success.SUCCESS, dropdownlist);
		return responseResult;
	}

	@RequestMapping(value = "/positiontitle/{departId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPositionTitle(@PathVariable(value = "departId") String departId) throws Exception {

		logger.info("CommonDropDownListController.getPositionTitle invocation start...");

		List<DropDownDto> dropdownlist = new ArrayList<DropDownDto>();

		List<PositionTitle> positions = departmentService.getAllPositionTitle(Long.parseLong(departId));
		for (PositionTitle ptitle : positions) {
			DropDownDto dto = new DropDownDto();
			dto.setCodeDisplay(ptitle.getPositionName());
			dto.setCodeValue(ptitle.getPositionCode());
			dropdownlist.add(dto);
		}

		responseResult.initResult(GTAError.Success.SUCCESS, dropdownlist);
		return responseResult;

	}

	@RequestMapping(value = "/staffType", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getStaffType() {

		logger.info("getStaffType invocation start...");

		List<DropDownDto> retData = new ArrayList<DropDownDto>();

		try {

			// List<SysCode> crewRoleConfigs =
			// sysCodeService.getSysCodeByCategory(Constant.SYSCODE_CREW_ROLE);
			List<SysCode> staffTypeConfigs = sysCodeService.getSysCodeByMutipleCategory(
					"'" + Constant.SYSCODE_CREW_ROLE + "','" + Constant.SYSCODE_STAFF_TYPE + "'");

			Boolean blag = Boolean.FALSE;
			List<SysCode> configs = new ArrayList<>();
			// if (crewRoleConfigs != null) {
			// configs.addAll(crewRoleConfigs);
			// }
			if (staffTypeConfigs != null) {
				configs.addAll(staffTypeConfigs);
				blag = Boolean.TRUE;
			}
			// SYSCODE_STAFF_TYPE
			if (!blag)
				return null;

			for (SysCode sc : configs) {

				DropDownDto dto = new DropDownDto();
				dto.setCodeDisplay(sc.getCodeDisplay());
				dto.setCodeValue(sc.getCodeValue());
				dto.setCodeDisplayTC(sc.getCodeDisplayNls());
				retData.add(dto);
			}
			logger.info("CommonDropDownListController.getSysConfig invocation end...");
			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/***
	 * front end call api iso modified by Kaster 20160323
	 * 将customerId的类型改为Long，加上@EncryptFieldInfo
	 * 
	 * @return
	 */
	@RequestMapping(value = "/{dropdownlistId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSysConfig(@PathVariable(value = "dropdownlistId") String id,
			@RequestParam(value = "customerId", required = false) @EncryptFieldInfo Long customerId,
			@RequestParam(value = "accdemyId", required = false) String accdemyId,
			@RequestParam(value = "type", required = false) String type) {
		logger.info("CommonDropDownListController.getSysConfig invocation start...");
		List<Object> retData = new ArrayList<Object>();
		if (id.equalsIgnoreCase("HKDistrict")) {
			List<HkArea> hkArea = hkAreaDistrictService.getHKArea();
			if (hkArea == null || hkArea.size() == 0)
				return null;
			for (HkArea area : hkArea) {
				DropDownDto dto = new DropDownDto();
				dto.setCodeDisplay(area.getAreaName());
				dto.setCodeValue(area.getAreaName());
				dto.setCodeDisplayTC(area.getAreaNameTc());
				retData.add(dto);
			}
			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			return responseResult;
		}
		try {
			List<SysCode> configs = null;// sysCodeService.getSysCodeByCategory(id);

			boolean multiBlean = false;
			Long newCustomerId = null;
			// formkit url/dropdownlists/relationshipCode?accdemyNo=xxx"
			if (!StringUtils.isEmpty(accdemyId)) {
				Member member = memberService.getMemberByAcademyNo(accdemyId);
				if (null != member) {
					newCustomerId = member.getCustomerId();
				}
			} // staff portal url/dropdownlists/relationshipCode?customerId=xxx"

			if (null != customerId) {
				newCustomerId = Long.valueOf(customerId);
			}
			if (null != newCustomerId) {
				multiBlean = memberService.checkMemberServicePlanMulGeneration(newCustomerId);
				if (multiBlean) {
					configs = sysCodeService.getSysCodeByCategory(id);
				} else {
					configs = sysCodeService.getSysCodeByCategory(id, false);
				}
			} else {
				configs = sysCodeService.getSysCodeByCategory(id);
			}
			if (configs == null || configs.size() == 0)
				return null;
			/***
			 * add id is PaymentMethod  sys_code return FE 
			 */
			if (StringUtils.isNotEmpty(type)) {
				this.setTypeDto(retData, configs, type);
			} else {
				for (SysCode sc : configs) {
//					if (sc.getCodeValue().equals("FATIL") || sc.getCodeValue().equals("MOTIL")) {
//						continue;
//					}
					DropDownDto dto = new DropDownDto();
					dto.setCodeDisplay(sc.getCodeDisplay());
					dto.setCodeValue(sc.getCodeValue());
					dto.setCodeDisplayTC(sc.getCodeDisplayNls());
					retData.add(dto);
				}
			}
			responseResult.initResult(GTAError.Success.SUCCESS, new Data(retData));
			logger.info("CommonDropDownListController.getSysConfig invocation end...");
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}
	private void setTypeDto(List<Object> list, List<SysCode> configs, String type) {
		for (SysCode sc : configs) {
			// cash \cash value\credit card//union pay
			if (type.equalsIgnoreCase(Constant.COMMON_PAY)) {
				if (sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CASH.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CASHVALUE.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.VISA.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CUP.name())) {
					DropDownDto dto = new DropDownDto();
					dto.setCodeDisplay(sc.getCodeDisplay());
					dto.setCodeValue(sc.getCodeValue());
					dto.setCodeDisplayTC(sc.getCodeDisplayNls());
					list.add(dto);
				}
			}
			// cash\credit card/union pay
			else if (type.equalsIgnoreCase(Constant.STAFF_PAY)) {
				if (sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CASH.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.VISA.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CUP.name())) {
					DropDownDto dto = new DropDownDto();
					dto.setCodeDisplay(sc.getCodeDisplay());
					dto.setCodeValue(sc.getCodeValue());
					dto.setCodeDisplayTC(sc.getCodeDisplayNls());
					list.add(dto);
				}
			}
			// cash \cash value\credit card\payment upon arrival/union pay
			else if (type.equalsIgnoreCase(Constant.GUEST_ROOM_PAY)) {
				if (sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CASH.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CASHVALUE.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.VISA.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.PREAUTH.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CUP.name())) {
					DropDownDto dto = new DropDownDto();
					dto.setCodeDisplay(sc.getCodeDisplay());
					dto.setCodeValue(sc.getCodeValue());
					dto.setCodeDisplayTC(sc.getCodeDisplayNls());
					list.add(dto);
				}
			} // cash/credit card/cheque/transcript/union pay
			else if (type.equalsIgnoreCase(Constant.TOPUP_PAY)) {
				if (sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CASH.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.VISA.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CHEQUE.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.BT.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CUP.name())) {
					DropDownDto dto = new DropDownDto();
					if(sc.getCodeValue().equalsIgnoreCase(PaymentMethod.BT.name())){
						dto.setCodeDisplay(sc.getCodeDisplay().split("/")[1]);	
					}else{
						dto.setCodeDisplay(sc.getCodeDisplay());
					}
					dto.setCodeValue(sc.getCodeValue());
					dto.setCodeDisplayTC(sc.getCodeDisplayNls());
					list.add(dto);
				}
			}
			/* /cash/cheque/credit card/union pay/ bank transfer */
			else if (type.equalsIgnoreCase(Constant.MEMBER_PAY)) {
				if (sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CASH.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CHEQUE.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.VISA.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.CUP.name())
						|| sc.getCodeValue().equalsIgnoreCase(PaymentMethod.BT.name())) {
					DropDownDto dto = new DropDownDto();
					if(sc.getCodeValue().equalsIgnoreCase(PaymentMethod.BT.name())){
						dto.setCodeDisplay(sc.getCodeDisplay().split("/")[0]);	
					}else{
						dto.setCodeDisplay(sc.getCodeDisplay());
					}
					dto.setCodeValue(sc.getCodeValue());
					dto.setCodeDisplayTC(sc.getCodeDisplayNls());
					list.add(dto);
				}
			}
		}
		/***
		 * MEMBER_PAY  add other onlineOff payment
		 */
		if (type.equalsIgnoreCase(Constant.MEMBER_PAY)) {
			for(PaymentMethod method:PaymentMethod.values()){
				if(method.name().equalsIgnoreCase(PaymentMethod.MASTER.name())||
				   method.name().equalsIgnoreCase(PaymentMethod.AMEX.name())||
				   method.name().equalsIgnoreCase(PaymentMethod.VISA.name())||
				   method.name().equalsIgnoreCase(PaymentMethod.CUP.name())){
					DropDownDto dto = new DropDownDto();
					
					if(method.name().equalsIgnoreCase(PaymentMethod.MASTER.name())){
						dto.setCodeDisplay("Master(StandAlone)");	
					}else if(method.name().equalsIgnoreCase(PaymentMethod.AMEX.name())){
						dto.setCodeDisplay("AE(StandAlone)");
					}else if(method.name().equalsIgnoreCase(PaymentMethod.VISA.name())){
						dto.setCodeDisplay("Visa(StandAlone)");
					}else if(method.name().equalsIgnoreCase(PaymentMethod.CUP.name())){
						dto.setCodeDisplay("UnionPay(StandAlone)");
					}
					dto.setCodeValue(method.name()+"_OffLine");
					list.add(dto);
				}
			}
			
		}
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllSysConfig() {

		logger.info("CommonDropDownListController.getAllSysConfig invocation start...");

		List<Map<String, List<Object>>> retData = new ArrayList<Map<String, List<Object>>>();
		Map<String, List<Object>> dataMap = new HashMap<String, List<Object>>();
		List<Object> codeData = null;
		int totalCount = 0;
		try {
			List<SysCode> configCategorys = sysCodeService.getAllSysCodeCategory();
			for (SysCode sysCode : configCategorys) {
				List<SysCode> configs = sysCodeService.getSysCodeByCategory(sysCode.getCategory());
				if (configs == null || configs.size() == 0)
					continue;
				codeData = new ArrayList<Object>();
				for (SysCode sc : configs) {
					DropDownDto dto = new DropDownDto();
					dto.setCodeDisplay(sc.getCodeDisplay());
					dto.setCodeValue(sc.getCodeValue());
					dto.setCodeDisplayTC(sc.getCodeDisplayNls());
					codeData.add(dto);
				}
				if (codeData.size() > 0) {
					dataMap.put(sysCode.getCategory(), codeData);
					totalCount++;
				}
			}

			// To get valid service plan, excluding not started, expired
			List<ServicePlanDto> validDetailServicePlans = servicePlanService.getValidServicPlan();
			if (validDetailServicePlans != null && validDetailServicePlans.size() > 0) {
				codeData = new ArrayList<Object>();
				for (ServicePlanDto spd : validDetailServicePlans) {

					if (STATUS_ACTIVE.equals(spd.getStatus())) {
						DropDownDto dto = new DropDownDto();
						dto.setCodeDisplay(spd.getPlanName());
						dto.setCodeValue(String.valueOf(spd.getPlanNo()));
						codeData.add(dto);
					}
				}
				if (codeData.size() > 0) {
					dataMap.put("serviceplan", codeData);
					totalCount++;
				}
			}

			// To get All service plan including not started, expired and valid
			List<ServicePlanDto> detailServicePlans = servicePlanService.getAllServicPlan();
			if (detailServicePlans != null && detailServicePlans.size() > 0) {
				codeData = new ArrayList<Object>();
				for (ServicePlanDto spd : detailServicePlans) {

					if (STATUS_ACTIVE.equals(spd.getStatus())) {
						DropDownDto dto = new DropDownDto();
						dto.setCodeDisplay(spd.getPlanName());
						dto.setCodeValue(String.valueOf(spd.getPlanNo()));
						codeData.add(dto);
					}
				}
				if (codeData.size() > 0) {
					dataMap.put("serviceplanAll", codeData);
					totalCount++;
				}
			}

			// to get the hkdistrict
			List<HkArea> hkArea = hkAreaDistrictService.getHKArea();
			if (hkArea != null && hkArea.size() > 0) {
				codeData = new ArrayList<Object>();
				for (HkArea area : hkArea) {
					DropDownDto dto = new DropDownDto();
					dto.setCodeDisplay(area.getAreaName());
					dto.setCodeValue(area.getAreaName());
					dto.setCodeDisplayTC(area.getAreaNameTc());
					codeData.add(dto);
				}
				if (codeData.size() > 0) {
					dataMap.put("HKDistrict", codeData);
					totalCount++;
				}
			}

			List<StaffProfile> staffProfileList = staffProfileService.searchAllStaffProfile(null);
			if (staffProfileList != null && staffProfileList.size() > 0) {
				codeData = new ArrayList<Object>();
				for (StaffProfile sp : staffProfileList) {

					DropDownDto dto = new DropDownDto();
					dto.setCodeValue(sp.getUserId());
					dto.setCodeDisplay(sp.getGivenName() + " " + sp.getSurname());
					codeData.add(dto);
				}

				if (codeData.size() > 0) {
					dataMap.put("salesname", codeData);
					totalCount++;
				}
			}
			if (null != dataMap && !dataMap.isEmpty())
				retData.add(dataMap);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

		logger.info("CommonDropDownListController.getAllSysConfig invocation end...");

		Data result = new Data(retData);
		result.setCurrentPage(1);
		result.setLastPage(true);
		result.setPageSize(totalCount);
		result.setRecordCount(totalCount);
		result.setTotalPage(1);
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}

	/***
	 * check Member ServicePlan MulGeneration display sysCode if MulGeneration
	 * is true display (child&Spouse&Parent) else display (child&Spouse)
	 * 
	 * @param list
	 * @param multiBlean
	 */
	private void settingDropDownDtoMulGeneration(List<Object> list, Boolean multiBlean) {
		Iterator<Object> it = list.iterator();
		while (it.hasNext()) {
			DropDownDto dropDownDto = (DropDownDto) it.next();
			if (dropDownDto.getCodeDisplay().equals("Father") || dropDownDto.getCodeDisplay().equals("Mother")) {
				if (!multiBlean) {
					list.remove(dropDownDto);
				}
			}
		}
	}

	@RequestMapping(value = "/validTempPassType", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getHelperPassType() {
		logger.info("CommonDropDownListController.getHelperPassType invocation start...");
		return tempPassService.getAllPassType();
	}

	@RequestMapping(value = "/allterminal", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getAllTerminal() {
		logger.info("CommonDropDownListController.getAllTerminal invocation start...");
		return tempPassService.getAllTerminalDropDown();
	}

	@RequestMapping(value = "/locationType", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getLocationType() {
		logger.info("CommonDropDownListController.getLocationType invocation start...");
		return ibeaconService.getByLocationType(Constant.General_Status_ACT);
	}

	@RequestMapping(value = "/roomList", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getRoomList() {
		logger.info("CommonDropDownListController.getRoomList invocation start...");
		List<Room> roomList;
		try {
			roomList = roomService.getLimitedInfoRoomList();

			responseResult.initResult(GTAError.Success.SUCCESS, roomList);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommonError.UNEXPECTED_EXCEPTION);
			return responseResult;

		}
	}
}
