package com.sinodynamic.hkgta.controller.common;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.util.FileUpload;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@Controller
@RequestMapping("/common")
public class UploadProfilePhotoController extends ControllerBase {
	private static final 	Logger logger = Logger.getLogger(UploadProfilePhotoController.class);

	/**
	 * This function is used to save the profile photo for each enrollment
	 * record.
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/saveprofilephoto", method = RequestMethod.POST)
	public @ResponseBody ResponseResult saveProfile(
			@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request) {
		ResponseResult responseResult = new ResponseResult();
		if (file == null || file.isEmpty()) {
			logger.info("File is empty!");
			responseResult.setReturnCode("1");
			responseResult
					.setErrorMessageEN("{\"warning\":\"File is empty!\"}");
			return responseResult;
		}
		String saveFilePath = "";
		try {
			saveFilePath = FileUpload
					.upload(file, FileUpload.FileCategory.USER);
		} catch (Exception e) {
			
			logger.error("UploadProfilePhotoController.saveProfile Exception",e);
			responseResult.setReturnCode("1");
			responseResult.setErrorMessageEN("fail");
			return responseResult;
		}
		File serverFile = new File(saveFilePath);
		saveFilePath = serverFile.getName();
		responseResult.setReturnCode("0");
		Map<String, String> imgPathMap = new HashMap<String, String>();
		imgPathMap.put("imagePath", "/" + saveFilePath);
		responseResult.setData(imgPathMap);
		return responseResult;
	}


	/**
	*  This method is used to upload images to the remote server
	*  
	* @param file
	* @param fileCategory
	* @return ResponseResult (image name)
	* @author Vineela_Jyothi
	* Date: Jul 13, 2015
	 */
	@RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
	public @ResponseBody ResponseResult uploadImage(
			@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam(value = "fileCategory", required = false) String fileCategory,
			HttpServletRequest request) {
		ResponseResult responseResult = new ResponseResult();
		if (file == null || file.isEmpty()) {
			logger.info("File is empty!");
			responseResult.setReturnCode("1");
			responseResult
					.setErrorMessageEN("{\"warning\":\"File is empty!\"}");
			return responseResult;
		}
		String saveFilePath = "";
		try {
			switch (fileCategory) {
			case "profile":
				saveFilePath = FileUpload.upload(file,
						FileUpload.FileCategory.USER);
				break;
			case "transaction":
				saveFilePath = FileUpload.upload(file,
						FileUpload.FileCategory.TRANSACTION);
				break;
			case "material":
				saveFilePath = FileUpload.upload(file,
						FileUpload.FileCategory.MATERIAL);
				break;
			case "course":
				saveFilePath = FileUpload.upload(file,
						FileUpload.FileCategory.COURSE);
				break;
			case "advertise":
				saveFilePath = FileUpload.upload(file,
						FileUpload.FileCategory.ADVERTISE);
				break;
			default:
				break;
			}

		} catch (Exception e) {
			logger.error(UploadProfilePhotoController.class.getName()
					+ " uploadImage Failed!", e);
			responseResult
					.initResult(GTAError.CourseError.FAIL_TO_UPLOAD_IMAGE);
			return responseResult;

		}
		File serverFile = new File(saveFilePath);
		saveFilePath = serverFile.getName();
		responseResult.setReturnCode("0");
		Map<String, String> imgPathMap = new HashMap<String, String>();
		imgPathMap.put("imagePath", "/" + saveFilePath);
		responseResult.setData(imgPathMap);
		return responseResult;
	}
}
