package com.sinodynamic.hkgta.controller.pms;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.MemberAccountInfoDto;
import com.sinodynamic.hkgta.dto.fms.AdvancedQueryConditionsDto;
import com.sinodynamic.hkgta.dto.fms.PosResponse;
import com.sinodynamic.hkgta.dto.pms.CheckHotelAvailableDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationCancelDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationChangeDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto;
import com.sinodynamic.hkgta.dto.pms.HotelReservationPaymentDto.PaymentResult;
import com.sinodynamic.hkgta.dto.pms.RoomResDto;
import com.sinodynamic.hkgta.dto.pms.RoomReservationDto;
import com.sinodynamic.hkgta.dto.pms.RoomStayDto;
import com.sinodynamic.hkgta.dto.pms.RoomTypeDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.Member;
import com.sinodynamic.hkgta.entity.pms.Room;
import com.sinodynamic.hkgta.entity.pms.RoomReservationRec;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderTrans;
import com.sinodynamic.hkgta.integration.pms.service.PMSApiService;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.common.UsageRightCheckService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.MemberService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.service.onlinepayment.AmexPaymentGatewayService;
import com.sinodynamic.hkgta.service.onlinepayment.PaymentGatewayService;
import com.sinodynamic.hkgta.service.pms.HotelReservationService;
import com.sinodynamic.hkgta.service.pms.HotelReservationServiceFacade;
import com.sinodynamic.hkgta.service.pms.RoomService;
import com.sinodynamic.hkgta.service.pms.RoomTypeService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderTransService;
import com.sinodynamic.hkgta.util.AbstractCallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor;
import com.sinodynamic.hkgta.util.CommUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.AppType;
import com.sinodynamic.hkgta.util.constant.Constant.RoomFrontdeskStatus;
import com.sinodynamic.hkgta.util.constant.CustomerTransationStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.GlobalParameterType;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.constant.MemberType;
import com.sinodynamic.hkgta.util.constant.PaymentMethod;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.MessageResult;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@Controller
public class HotelReservationController extends ControllerBase {

	private Logger						pmsLog	= Logger.getLogger(LoggerType.PMS.getName());
	private Logger ecrLog = Logger.getLogger(LoggerType.ECRLOG.getName()); 
	@Autowired
	private PMSApiService					pmsApiService;

	@Autowired
	private HotelReservationService			hotelReservationService;

	@Autowired
	private RoomTypeService					roomTypeService;

	@Autowired
	private PaymentGatewayService			paymentGatewayService;
	@Autowired
	private AmexPaymentGatewayService amexPaymentGatewayService;

	@Autowired
	private HotelReservationServiceFacade	hotelReservationServiceFacade;
	
	@Autowired
	private GlobalParameterService globalParameterService;

	@Autowired
	private CustomerProfileService			customerProfileService;

	@Autowired
	private MessageTemplateService			messageTemplateService;

	@Autowired
	private CustomerEmailContentService		cecService;

	@Autowired
	private CustomerOrderTransService		customerOrderTransService;

	@Autowired
	private UsageRightCheckService			usageRightCheckService;
	@Autowired
	private AdvanceQueryService				advanceQueryService;

	@Autowired
	private MemberService					memberService;
	
	@Autowired 
	private RoomService roomService;
	@Autowired 
	private CustomerServiceAccService customerServiceAccService;

	@RequestMapping(value = "/guestroom/reservation", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult guestroomAdvanceSearch(@RequestParam(value = "pageNumber", defaultValue = "1", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "arrivalDate", required = false) String arrivalDate,
			@RequestParam(value = "departureDate", required = false) String departureDate,
			@RequestParam(value = "isAscending", defaultValue = "true", required = false) String isAscending,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters,
			@RequestParam(value = "notShowStatus", defaultValue = "") String notShowStatus) throws Exception {
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<String, org.hibernate.type.Type>();
		/** 解析不需要显示的状态【暂支持CAN状态，其它带扩展】 */
		AdvancedQueryConditionsDto aqcDto = new AdvancedQueryConditionsDto(notShowStatus);
		String joinSQL = hotelReservationServiceFacade.getGuestroomAdvanceSearch(typeMap, aqcDto);
		joinSQL += "where 1=1 ";
		if (StringUtils.isEmpty(sortBy)) {
			sortBy = "confirmId";
			isAscending = "false";
		}
		if (!StringUtils.isEmpty(arrivalDate)) {
			joinSQL += "  and arrivalDate = '" + arrivalDate + "'";
		} else {
			sortBy = "arrivalDate";
			isAscending = "false";
		}
		if (!StringUtils.isEmpty(departureDate)) {
			joinSQL += "  and departureDate = '" + departureDate + "'";
		}

		ResponseResult tmpResult = null;
		// advance search
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
		if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null
				&& advanceQueryDto.getGroupOp().length() > 0) {
			joinSQL = joinSQL + " AND ";
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, advanceQueryDto);

			tmpResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page, RoomReservationDto.class, typeMap);
		} else {
			AdvanceQueryDto queryDto = new AdvanceQueryDto();
			prepareQueryParameter(pageNumber, pageSize, sortBy, isAscending, queryDto);
			tmpResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, RoomReservationDto.class, typeMap);
		}
		return tmpResult;
	}

	@RequestMapping(value = "/hotel/reservation/check", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getHotelAvailable(
			@RequestParam(value = "stayBeginDate", defaultValue = "", required = false) final String stayBeginDate,
			@RequestParam(value = "stayEndDate", defaultValue = "", required = false) final String stayEndDate,
			@RequestParam(value = "adultCount", defaultValue = "1", required = false) final Integer adultCount,
			@RequestParam(value = "childCount", defaultValue = "0", required = false) final Integer childCount,
			@RequestParam(value = "customerId", defaultValue = "0", required = false) @EncryptFieldInfo final Long customerId,
			@RequestParam(value = "appType", defaultValue = "MEMBER", required = false) final AppType appType) throws Exception {

		CallBackExecutor executor = new CallBackExecutor(HotelReservationController.class);

		return (ResponseResult) executor.execute(new AbstractCallBack() {
			@Override
			public Object doTry() throws Exception {
				UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(
						customerId,UsageRightCheckService.FacilityCode.GUEST,CommUtil.toDate(stayEndDate, "yyyy-MM-dd"));
				if (!facilityRight.isCanBook() && AppType.MEMBER.equals(appType)) {
					
					/**
					 * check rnewwal service  : no MemberLimitRule  only customer_service_acc  ,customer_order_hd status is CMP
					 * check every service plan expiry_date 
					 */
					if(!customerServiceAccService.checkRenewedService(customerId, null, stayBeginDate, stayEndDate)){
						throw new GTACommonException(GTAError.FacilityError.NO_PERMISSION); 
					}
					
					//throw new GTACommonException(GTAError.FacilityError.NO_PERMISSION);
				}

				CheckHotelAvailableDto dto = new CheckHotelAvailableDto(stayBeginDate, stayEndDate, adultCount, childCount);

				MessageResult msgResult = pmsApiService.getkHotelAvailable(dto);
				if (msgResult.isSuccess()) {
					RoomStayDto availableRooms = (RoomStayDto) msgResult.getObject();
					List<RoomTypeDto> rooms = roomTypeService.getRoomListByTypes(availableRooms, null, appType);
					responseResult.initResult(GTAError.Success.SUCCESS, rooms);
				} else {
					responseResult.initResult(GTAError.CommonError.CALL_BACK_METHOD_ERROR,msgResult.getMessage());
//					responseResult.initResult(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[] { msgResult.getMessage() });
				}

				return responseResult;
			}
		});
	}

	@RequestMapping(value = "/hotel/reservation", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult bookHotel(@RequestBody List<RoomResDto> reservations) throws Exception {
		try {
			/**
			 * use new check guest room reservation check perssion 
			 */
//			UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(
//					reservations.get(0).getCustomerId(),UsageRightCheckService.FacilityCode.GUEST,null);
//			if (!facilityRight.isCanBook()) {
//				throw new GTACommonException(GTAError.FacilityError.NO_PERMISSION);
//			}
			responseResult.initResult(GTAError.Success.SUCCESS, hotelReservationServiceFacade.bookHotel(reservations, getUser().getUserId()));
			return responseResult;
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			if (GTAError.GuestRoomError.ROOM_NOT_ENOUGH == e.getError()) {
				responseResult.initResult(GTAError.GuestRoomError.ROOM_NOT_ENOUGH, new Object[] { e.getArgs()[0] }, e.getArgs()[1]);
				return responseResult;
			} else {
				throw e;
			}
		}
	}

	@RequestMapping(value = "/hotel/reservation/{customerId}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult bookHotel(@RequestBody List<RoomResDto> reservations, @PathVariable("customerId") Long customerId) throws Exception {
		try {
			/**
			 * use new check guest room reservation check perssion 
			 */
//			UsageRightCheckService.FacilityRight facilityRight = usageRightCheckService.checkFacilityRight(
//					customerId,UsageRightCheckService.FacilityCode.GUEST,null);
//			if (!facilityRight.isCanBook()) {
//				throw new GTACommonException(GTAError.FacilityError.NO_PERMISSION);
//			}

			responseResult.initResult(GTAError.Success.SUCCESS, hotelReservationServiceFacade.bookHotel(reservations, getUser().getUserId()));
			return responseResult;
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			if (GTAError.GuestRoomError.ROOM_NOT_ENOUGH == e.getError()) {
				responseResult.initResult(GTAError.GuestRoomError.ROOM_NOT_ENOUGH, new Object[] { e.getArgs()[0] }, e.getArgs()[1]);
				return responseResult;
			} else {
				throw e;
			}
		}
	}

	@RequestMapping(value = "/hotel/payment/precancel", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult precancel(@RequestBody HotelReservationPaymentDto paymentDto) throws Exception {
		hotelReservationServiceFacade.precancelReservation(paymentDto);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/hotel/payment/requestcancel", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult requestCancel(@RequestBody HotelReservationCancelDto cancelDto) throws Exception {
		cancelDto.setCurrentDate(new Date());
		cancelDto.setUserId(getUser().getUserId());

		hotelReservationServiceFacade.cancelReservation(cancelDto);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/hotel/payment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult payment(HttpServletRequest request,@RequestBody HotelReservationPaymentDto paymentDto) throws Exception {
		
		pmsLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), JSONObject.fromObject(paymentDto).toString(), RequestMethod.POST.name(), null));
		paymentDto.setTerminalType(request.getHeader("terminalType"));
		
		String paymentMethod = paymentDto.getPaymentMethod();

		if (paymentDto.getCurrentDate() == null)
			paymentDto.setCurrentDate(new Date());

		paymentDto.setStaffUserId(getUser().getUserId());
		Long transactionNo = null;
		if (StringUtils.equalsIgnoreCase(paymentMethod, Constant.CASH_Value)) {
			hotelReservationService.verificationCashValue(paymentDto);
		}
		if (StringUtils.equalsIgnoreCase(paymentMethod, Constant.CASH_Value) || StringUtils.equalsIgnoreCase(paymentMethod, Constant.CASH)
				|| StringUtils.equalsIgnoreCase(paymentMethod, Constant.PRE_AUTH)) {
			PaymentResult pr = hotelReservationServiceFacade.hotelReservationLocalPayment(paymentDto);

			responseResult.initResult(GTAError.Success.SUCCESS, pr);

			if (!StringUtils.equalsIgnoreCase(paymentMethod, Constant.PRE_AUTH)) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("userId", getUser().getUserId());
				params.put("userName", getUser().getUserName());
				transactionNo = Long.parseLong(pr.getTransactionNo());
				hotelReservationServiceFacade.sendGuestroomReservationEmail(transactionNo, params);
			}
			
		}

		if (StringUtils.equalsIgnoreCase(paymentMethod, Constant.CREDIT_CARD)||StringUtils.equalsIgnoreCase(paymentMethod, Constant.UNION_PAY)
				||StringUtils.equalsIgnoreCase(paymentMethod, PaymentMethod.AMEX.getCardCode())) {
			pmsLog.info("paymentType is :"+paymentMethod);
			if(StringUtils.equalsIgnoreCase(paymentMethod, PaymentMethod.AMEX.getCardCode())){
				pmsLog.info("check AMEX payment turn on/off .");
				responseResult=globalParameterService.checkPaymentCardTypeTurnOn(GlobalParameterType.PAYMENT_TYPE_AMEX.getCode());
				if(!responseResult.getReturnCode().equals(GTAError.Success.SUCCESS.getCode()))
				{
					pmsLog.info("AMEX payment turn off .");
					return responseResult;
				}else{
					pmsLog.info("AMEX payment turn on .");
				}
			}
			pmsLog.info("create hotelReservation and customerOrderTrans start record.");
			CustomerOrderTrans customerOrderTrans = hotelReservationServiceFacade.hotelReservationOnlinePayment(paymentDto);
			pmsLog.info("create hotelReservation and customerOrderTrans end  record.");

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("terminal", paymentDto.getTerminal());
			params.put("userId", getUser().getUserId());
			params.put("userName", getUser().getUserName());
            //Get the third party request page
//			StringBuilder items = new StringBuilder();
//			for(HotelReservationItemInfoDto itemInfo : paymentDto.getHotelReservationItemsInfo()){
//				items.append(itemInfo.getReservationId()+"|"+itemInfo.getAmountAfterTax()+"||");
//			}
//			params.put("items", items);
			pmsLog.info("setting Gateway redirectUrl.");
			String redirectUrl = paymentGatewayService.payment(customerOrderTrans, params);
			if(StringUtils.equalsIgnoreCase(paymentMethod, PaymentMethod.AMEX.getCardCode()))
			{
				redirectUrl = amexPaymentGatewayService.payment(customerOrderTrans, params);
			}else{
				redirectUrl = paymentGatewayService.payment(customerOrderTrans, params);
			}
			pmsLog.info("Gateway redirectUrl is :"+redirectUrl);
			
			Map<String, String> redirectUrlMap = new HashMap<String, String>();
			transactionNo = customerOrderTrans.getTransactionNo();
			redirectUrlMap.put("redirectUrl", redirectUrl);
			redirectUrlMap.put("transactionNo", String.valueOf(transactionNo));
			redirectUrlMap.put("orderNo", customerOrderTrans.getCustomerOrderHd().getOrderNo().toString());
			redirectUrlMap.put("amount", customerOrderTrans.getPaidAmount().toString());
			List<RoomReservationRec> bookings = this.hotelReservationService.getReservationByOrderNo(customerOrderTrans.getCustomerOrderHd().getOrderNo());
			if(bookings != null && bookings.size() > 0){
				String resvIdStr = "";
				for (RoomReservationRec book : bookings){
					resvIdStr += "GRM-" + book.getResvId() + ",";
				}
				redirectUrlMap.put("resvIdStr", resvIdStr.substring(0, resvIdStr.length()-1));
				
				pmsLog.info(Log4jFormatUtil.logInfoMessage(null, null, null, redirectUrlMap.toString()));
			}else{
				pmsLog.info("No find valid RoomReservationRec record..");
			}
			responseResult.initResult(GTAError.Success.SUCCESS, redirectUrlMap);
		}

		return responseResult;
	}

	@RequestMapping(value = "/hotel/change", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult change(@RequestBody HotelReservationChangeDto changeDto) throws Exception {
		changeDto.setUserId(getUser().getUserId());
		changeDto.setCurrentDate(new Date());
		hotelReservationServiceFacade.changeReservation(changeDto);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}

	@RequestMapping(value = "/guestroom/payment/posCallback", method = RequestMethod.POST)
	@ResponseBody
	public String callbackAfterPayment(HttpServletRequest request,@RequestBody PosResponse posResponse) throws Exception {
		Gson gson = new GsonBuilder().setDateFormat(appProps.getProperty("format.date")).create(); 
		ecrLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), gson.toJson(posResponse).toString(), RequestMethod.POST.name(), null));
		if(null==posResponse){
			responseResult.initResult(GTAError.ERCCallBack.REPONSE_IS_NULL);
			ecrLog.info(Log4jFormatUtil.logInfoMessage(null,null,null, responseResult.getErrorMessageEN()));
			return responseResult.getErrorMessageEN();
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", getUser().getUserId());
		params.put("userName", getUser().getUserName());
		hotelReservationServiceFacade.processCallbackAfterCreditCardLocalPayment(posResponse, params);
		
		if (null!=posResponse&&null!=posResponse.getReferenceNo()) {
			Long transactionNo = Long.parseLong(posResponse.getReferenceNo().trim());
			CustomerOrderTrans customerOrderTrans = customerOrderTransService.getCustomerOrderTrans(transactionNo);
			if (customerOrderTrans != null&&CustomerTransationStatus.SUC.name().equalsIgnoreCase(customerOrderTrans.getStatus())) 
			{
				pmsLog.info(" call oasis to commit by ECR payment start......");
				paymentGatewayService.commitOasisPayment(customerOrderTrans, null);
				pmsLog.info("call oasis to commit by ECR payment end......");
			}
		}
		
		return "OK";
	}
	@RequestMapping(value = "/guestroom/trivialInfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult trivialInfo(@RequestParam("customerId") @EncryptFieldInfo Long customerId) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();

		MemberAccountInfoDto currentAccount = (MemberAccountInfoDto) customerProfileService.getAccountInfo(customerId).getDto();

		String memberType = currentAccount.getMemberType();
		if (MemberType.IDM.name().equals(memberType) || MemberType.CDM.name().equals(memberType)) {
			Member member = memberService.getMemberByAcademyNo(currentAccount.getAcademyNo());
			MemberAccountInfoDto superiorAccount = (MemberAccountInfoDto) customerProfileService.getAccountInfo(member.getSuperiorMemberId())
					.getDto();
			result.put("primaryCreditLimit", superiorAccount.getCreditLimitValue());
			result.put("creditLimit", null);
			result.put("spendingLimit", currentAccount.getSpendingLimitValue());
		} else {
			result.put("primaryCreditLimit", null);
			result.put("creditLimit", currentAccount.getCreditLimitValue());
			result.put("spendingLimit", null);
		}
		result.put("academyId", currentAccount.getAcademyNo());

		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}
	/**
	 * 获取当前今天CheckInProgress状态的总数
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/guestroom/getSilentCheckInProgressTotalNum", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getSilentCheckInProgressTotalNum() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("totalNum", roomService.getSilentCheckInProgressTotalNum());
		responseResult.initResult(GTAError.Success.SUCCESS, result);
		return responseResult;
	}
	
	/**
	 * Pre assign room # to the existing guest room reservation
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/guestroom/preAssignRoom", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult preAssignRoom(@RequestParam(value = "confirmId") String confirmId,
			@RequestParam(value = "roomNo") Long roomNo) throws Exception {
		Room room = this.roomService.getRoomByRoomNo(roomNo);
		if(room == null){
			responseResult.initResult(GTAError.IbeaconError.NO_ROOM_FOUND);
			return responseResult;
		}
		
		if(RoomFrontdeskStatus.OOO.name().equals(room.getFrontdeskStatus())){
			responseResult.initResult(GTAError.IbeaconError.ROOM_OUT_OF_ORDER);
			return responseResult;
		}
		
		RoomReservationRec rec = this.hotelReservationService.getReservationByConfirmId(confirmId);
		if(!rec.getRoomTypeCode().equals(room.getRoomType().getTypeCode())){
			responseResult.initResult(GTAError.IbeaconError.ROOM_TYPE_DISMATCH);
			return responseResult;
		}
		
		if(room.getRoomId().equals(rec.getRoomId())){
			responseResult.initResult(GTAError.Success.SUCCESS);
			return responseResult;
		}
			
		if(this.hotelReservationService.checkRoomOccupy(room, rec)){
			responseResult.initResult(GTAError.IbeaconError.ROOM_ASSIGNED_ALREADY);
			return responseResult;
		}
		
		this.hotelReservationService.preAssignRoom(room.getRoomId(), rec, this.getUser().getUserId());
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/guestroom/extendGuestRoom", method =RequestMethod.GET)
	@ResponseBody
	public ResponseResult extendGuestRoom(@RequestParam(value = "reservationId",required = true) Long reservationId, 
			@RequestParam(value = "night",  required = true) Long night,@RequestParam(value = "roomNo",  required = true) String roomNo){
		
		return hotelReservationService.extendGuestRoomStay(reservationId, night,getUser().getUserId(),roomNo);
	}
	
	/**
	 * 对CheckInProgress状态的数据进行决策 ，accept批准/reject拒绝
	 * @param decision
	 * @param resvId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/guestroom/acceptOrRejectSilentCheckInProgress/{resvId}/{decision}/{customerId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult acceptOrRejectSilentCheckInProgress(@PathVariable("decision") String decision, @PathVariable("resvId") Long resvId, @PathVariable("customerId") @EncryptFieldInfo Long customerId) throws Exception {
		return roomService.acceptOrRejectSilentCheckInProgress(decision, customerId, resvId);
	}

	@RequestMapping(value = "/guestroom/sendReceipt", method =RequestMethod.GET)
	@ResponseBody
	public ResponseResult sendReceipt(@RequestParam(value = "transactionNo",required = true) Long transactionNo){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", getUser().getUserId());
		params.put("userName", getUser().getUserName());
		hotelReservationServiceFacade.sendGuestroomReservationEmail(transactionNo, params);
		responseResult.initResult(GTAError.Success.SUCCESS);
		return responseResult;
	}
	
	@RequestMapping(value = "/guestroom/generateGuestRoomResvAttach", method = RequestMethod.GET,produces = "application/pdf")
	public @ResponseBody byte[] generateGuestRoomResvAttach(@RequestParam(value = "transactionNo",required = true) Long transactionNo){
		return customerOrderTransService.generateGuestRoomResvAttach(transactionNo);
	}
}
