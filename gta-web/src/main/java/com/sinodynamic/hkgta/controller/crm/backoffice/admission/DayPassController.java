package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.crm.AdvanceQueryDto;
import com.sinodynamic.hkgta.dto.crm.CustomerProfileDto;
import com.sinodynamic.hkgta.dto.crm.DaypassDto;
import com.sinodynamic.hkgta.dto.crm.DaypassIssuedDto;
import com.sinodynamic.hkgta.dto.crm.ServicePlanDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTypeDto;
import com.sinodynamic.hkgta.dto.rpos.PurchasedDaypassBackDto;
import com.sinodynamic.hkgta.dto.rpos.PurchasedDaypassDto;
import com.sinodynamic.hkgta.entity.EncryptFieldInfo;
import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.ServicePlan;
import com.sinodynamic.hkgta.entity.crm.ServicePlanDatePos;
import com.sinodynamic.hkgta.entity.crm.ServicePlanFacility;
import com.sinodynamic.hkgta.entity.crm.ServicePlanRatePos;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderHd;
import com.sinodynamic.hkgta.entity.rpos.CustomerOrderPermitCard;
import com.sinodynamic.hkgta.service.common.AdvanceQueryService;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.ServicePlanService;
import com.sinodynamic.hkgta.service.crm.cardmanage.PCDayPassPurchaseService;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.leads.LeadCustomerService;
import com.sinodynamic.hkgta.service.fms.MemberFacilityTypeBookingService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderDetService;
import com.sinodynamic.hkgta.service.rpos.CustomerOrderPermitCardService;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.QRCodeUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.RefundPeriodType;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

/**
 * day pass controller
 * @author Zero_Wang
 * @date   May 12, 2015
 */
/**
 * day pass controller
 * 
 * @author Zero_Wang
 * @date May 12, 2015
 */
@Controller
@RequestMapping("/daypass")
@Scope("prototype")
public class DayPassController extends ControllerBase<ServicePlan> {

	@Autowired
	private ServicePlanService servicePlanService;

	@Autowired
	private AdvanceQueryService advanceQueryService;

	@Autowired
	private LeadCustomerService leadCustomerService;

	@Autowired
	private CustomerProfileService customerProfileService;

	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	@Autowired
	private MemberFacilityTypeBookingService memberFacilityTypeBookingService;
	@Autowired
	private CustomerOrderDetService customerOrderDetService;
	@Autowired
	private PCDayPassPurchaseService pcDayPassPurchaseService;
	@Autowired
	private CustomerOrderPermitCardService customerOrderPermitCardService;

	@RequestMapping(value = "/get_daypass_list", method = RequestMethod.POST)
	@ResponseBody
	public ResponseResult getDayPassPageList(@RequestBody DaypassDto dto) {
		logger.info("DayPassController.getDayPassPageList invocation start...");
		return servicePlanService.getDayPassPageList(dto);
	}

	@RequestMapping(value = "/status", method = RequestMethod.PUT, produces = "application/json; charset=UTF-8")
	public @ResponseBody ResponseResult updateDayPassStatus(@RequestParam("status") String status,
			@RequestParam("planNo") Long planNo) {
		ServicePlanDto dto = new ServicePlanDto();
		dto.setStatus(status);
		dto.setPlanNo(planNo);
		dto.setUpdateBy(this.getUser().getUserId());
		ResponseResult result = servicePlanService.changeDayPassStatus(dto);
		return result;
	}

	private static ResponseMsg verifyDayPass(ServicePlanDto servicePlanJson, ResponseMsg status) {
		if (StringUtils.isEmpty(servicePlanJson.getSubscriberType())) {
			status.initResult(GTAError.DayPassError.ERRORMSG_SUBSCRIBERTYPE_NULL);
			return status;
		}

		if (StringUtils.isEmpty(servicePlanJson.getPlanName())) {
			status.initResult(GTAError.DayPassError.ERRORMSG_PLANNAME_NULL);
			return status;
		}

		if (servicePlanJson.getEffectiveEndDate() == null || servicePlanJson.getEffectiveStartDate() == null) {
			status.initResult(GTAError.DayPassError.ERRORMSG_DATE_NULL);
			return status;
		}
		if (servicePlanJson.getEffectiveStartDate().compareTo(servicePlanJson.getEffectiveEndDate()) > 0) {
			status.initResult(GTAError.DayPassError.ERRORMSG_ACTIVATIONDATE_LATERTHAN_DEACTIVATIONDATE);
			return status;
		}
		// Low Rate can not null
		if (servicePlanJson.getLowRate() == null) {
			status.initResult(GTAError.DayPassError.ERRORMSG_RATE_NULL);
			return status;
		}
		// Low Rate can not less than 0
		if (servicePlanJson.getLowRate().compareTo(BigDecimal.ZERO) < 0) {
			status.initResult(GTAError.DayPassError.ERRORMSG_RATE_LOWERTHAN_ZERO);
			return status;
		}
		// High Rate can null, if have can not less than 0
		if (servicePlanJson.getHighRate() != null && servicePlanJson.getHighRate().compareTo(BigDecimal.ZERO) < 0) {
			status.initResult(GTAError.DayPassError.ERRORMSG_RATE_LOWERTHAN_ZERO);
			return status;
		}
		// Higt Rate >= Low Rate
		if (servicePlanJson.getHighRate() != null
				&& servicePlanJson.getLowRate().compareTo(servicePlanJson.getHighRate()) > 0) {
			status.initResult(GTAError.DayPassError.ERRORMSG_LOWRATE_GREATERTHAN_HIGHRATE);
			return status;
		}
		status.initResult(GTAError.Success.SUCCESS);
		return status;
	}

	@RequestMapping(value = "/save_daypass", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseMsg saveDaypass(@RequestBody ServicePlanDto servicePlanJson) {

		responseMsg = verifyDayPass(servicePlanJson, responseMsg);
		if (!GTAError.Success.SUCCESS.getCode().equals(responseMsg.getReturnCode())) {
			return responseMsg;
		}
		if (Constant.Subscriber_Type.STF.toString().equals(servicePlanJson.getSubscriberType())
				&& (!BigDecimal.ZERO.equals(servicePlanJson.getLowRate())
						|| !BigDecimal.ZERO.equals(servicePlanJson.getHighRate()))) {
			this.responseMsg.initResult(GTAError.DayPassError.Staff_no_need_price);
			return this.responseMsg;
		}
		servicePlanJson.setCreateBy(this.getUser().getUserId());
		servicePlanJson.setUpdateBy(this.getUser().getUserId());
		servicePlanJson.setCreateDate(new Date());
		servicePlanJson.setUpdateDate(new Date());
		ResponseMsg saveresponseMsg = servicePlanService.saveDayPass(servicePlanJson);
		return saveresponseMsg;
	}

	@RequestMapping(value = "/get_daypass_byid/{planNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getDaypassById(@PathVariable(value = "planNo") Long planNo) {
		try {
			ServicePlanDto planJson = servicePlanService.getDaypassById(planNo);
			ResponseResult result = new ResponseResult("0", "success", planJson);
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			ResponseResult result = new ResponseResult("-1", "", "fail");
			return result;
		}

	}

	@RequestMapping(value = "/duplicate_daypass/{planNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg duplicateDaypass(@PathVariable(value = "planNo") Long planNo) {
		String userId = this.getUser().getUserId();
		ResponseMsg status = servicePlanService.duplicateDaypass(planNo, userId);
		return status;
	}

	@RequestMapping(value = "/delete_daypass/{planNo}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseMsg deleteDaypass(@PathVariable(value = "planNo") Long planNo) {
		// NULL/empty-paid but not confirmed; SUC - payment success; NSC -
		// payment not success; CAN - payment cancelled
		if (this.servicePlanService.isDaypassUsed(planNo)) {
			this.responseMsg.initResult(GTAError.DayPassError.Daypass_Used_Error);
			return this.responseMsg;
		}
		ResponseMsg status = servicePlanService.deleteDaypass(planNo);
		return status;
	}
	
	@RequestMapping(value = "/update_daypass", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg updateDaypass(@RequestBody ServicePlanDto servicePlanJson) {
		// NULL/empty-paid but not confirmed; SUC - payment success; NSC -
		// payment not success; CAN - payment cancelled
		//if daypass had buy  2018-05-30 no need to check daypass had used error msg
/*		if (this.servicePlanService.isDaypassUsed(servicePlanJson.getPlanNo())) {
//			boolean validates = validates(servicePlanJson);
			List<CustomerOrderPermitCard> cards = servicePlanService.getCustomerOrderPermitCard(servicePlanJson.getPlanNo());
			ResponseMsg validate = servicePlanService.getSpecialDate(servicePlanJson,cards);
			if(!validate.errorMessageEN.equals("Success")){
				this.responseMsg.initResult(GTAError.DayPassError.Only_Modify_High_Low_Price);
				return this.responseMsg;
			}
//			this.responseMsg.initResult(GTAError.DayPassError.Daypass_Used_Error);
//			return this.responseMsg;
		}*/
		if (Constant.Subscriber_Type.STF.toString().equals(servicePlanJson.getSubscriberType())) {
			if (!BigDecimal.ZERO.equals(servicePlanJson.getLowRate())
					|| !BigDecimal.ZERO.equals(servicePlanJson.getHighRate())) {
				this.responseMsg.initResult(GTAError.DayPassError.Staff_no_need_price);
				return this.responseMsg;
			}
		}
		ServicePlan servicePlanTemp = servicePlanService.get(servicePlanJson.getPlanNo());
		String planName2 = servicePlanTemp.getPlanName();

		String planName = servicePlanJson.getPlanName();
		servicePlanJson.setUpdateBy(this.getUser().getUserId());
		responseMsg = servicePlanService.updateDaypass(servicePlanJson);
		if (this.servicePlanService.checkServicePlanByName(planName, "DP", servicePlanTemp.getPlanNo())
				&& "0".equals(responseMsg.getReturnCode())) {
			if (planName2.equals(servicePlanJson.getPlanName()) && servicePlanTemp.getPassNatureCode().equals("DP"))
				responseMsg.initResult(GTAError.Success.SUCCESS);
			else
				responseMsg.setErrorMessageEN("DayPass name " + planName + " exist");
		}
		return responseMsg;
	}

	@RequestMapping(value = "/get_purchased_daypass_list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getPurchasedDaypassList(

	@RequestParam(value = "planNo", required = false) Long planNo,
			@RequestParam(value = "orderStatus", required = false) String orderStatus,
			@RequestParam(value = "memberName", required = false) String memberName,
			@RequestParam(value = "effectiveStartDate", required = false) Date effectiveStartDate,
			@RequestParam(value = "effectiveEndDate", required = false) Date effectiveEndDate,
			@RequestParam(value = "orderDate", required = false) Date orderDate,
			@RequestParam(value = "planName", required = false) String planName,
			@RequestParam(value = "orderTotalAmount", required = false) java.math.BigInteger orderTotalAmount,
			@RequestParam(value = "orderNo", required = false) java.math.BigInteger orderNo,
			@RequestParam(value = "customerId", required = false) java.math.BigInteger customerId,
			@RequestParam(value = "contactEmail", required = false) String contactEmail,
			@RequestParam(value = "propertyName", required = false) String propertyName,
			@RequestParam(value = "order", required = false) String order,
			@RequestParam(value = "pageNumber") int pageNumber, @RequestParam(value = "pageSize") int pageSize,
			@RequestParam(value = "show", required = false) String show) {
		try {

		} catch (Exception e) {
			logger.error(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER.getCode(), e);
			responseResult.initResult(GTAError.AdvQueryError.NO_ADVANCE_CONDITION_OFFER);
			return responseResult;
		}
		PurchasedDaypassDto purchasedDaypassDto = new PurchasedDaypassDto();

		purchasedDaypassDto.setPageNumber(pageNumber);
		purchasedDaypassDto.setPageSize(pageSize);
		purchasedDaypassDto.setOrderStatus(orderStatus);
		purchasedDaypassDto.setMemberName(memberName);
		purchasedDaypassDto.setEffectiveStartDate(effectiveStartDate);
		purchasedDaypassDto.setEffectiveEndDate(effectiveEndDate);
		purchasedDaypassDto.setOrderDate(orderDate);
		purchasedDaypassDto.setOrderTotalAmount(orderTotalAmount);
		purchasedDaypassDto.setOrderNo(orderNo);
		if (customerId != null) {
			purchasedDaypassDto.setCustomerId(customerId);
		}
		if (null != planNo) {
			purchasedDaypassDto.setPlanNo(planNo);
		}
		purchasedDaypassDto.setPropertyName(propertyName);
		purchasedDaypassDto.setOrder(order);
		purchasedDaypassDto.setShow(show);
		purchasedDaypassDto.setContactEmail(contactEmail);
		return servicePlanService.getPurchasedDaypassList(purchasedDaypassDto);
	}

	@RequestMapping(value = "/verify_payment_item_no/{paymentItemNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult verifyPaymentItemNo(@PathVariable(value = "paymentItemNo") String paymentItemNo) {
		if (!paymentItemNo.matches("[1-9]{1}[0-9]{0,}")) {
			return new ResponseResult("1", "Please provide valid payment item #. E.g. 188 (Start not with 0)");
		}
		return servicePlanService.verifyPaymentItemNo(Long.valueOf(paymentItemNo));
	}

	@RequestMapping(value = "/precancel", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult precancel(@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "orderNo", required = true) Long orderNo) throws Exception {
		responseResult = memberFacilityTypeBookingService.preCancel(orderNo, type);
		return responseResult;

	}

	@RequestMapping(value = "/cancleUnpayedOrder", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult cancleUnpayedOrder(@RequestParam(value = "orderNo", required = true) Long orderNo) {
		this.responseResult = servicePlanService.cancleUnpayedOrder(orderNo);
		return this.responseResult;
	}

	@RequestMapping(value = "/updateOrderStatus", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult updateOrderStatus(@RequestParam(value = "refundFlag", required = true) boolean refundFlag,
			@RequestParam(value = "orderNo", required = true) Long orderNo,
			@RequestParam(value = "requesterType", required = true) String requesterType,
			@RequestParam(value = "remark", required = false) String remark) {

		this.responseResult = servicePlanService.canclePurchaseDaypass(orderNo, this.getUser().getUserId(), remark,
				requesterType, refundFlag);
		if (GTAError.Success.SUCCESS.getCode().equalsIgnoreCase(this.responseResult.getReturnCode())) {
			// check if cancel refund in period only send mail and on sendSMS
			CustomerOrderHd customerOrderHd = customerOrderDetService.getCustomerOrderHdByOrderNo(orderNo);
			boolean refundPeriodFlag = false;
			if (null != customerOrderHd) {
				List<CustomerOrderPermitCard> list = customerOrderHd.getCustomerOrderPermitCards();
				if (null != list && !list.isEmpty()) {
					Timestamp begin = new Timestamp(list.get(0).getEffectiveFrom().getTime());
					refundPeriodFlag = memberFacilityTypeBookingService.checkRefundDate(begin,
							RefundPeriodType.REFPERIOD_DAYPASS.name(), "daypass");
				}
			}
			// check if cancel refund in period only send mail and on sendSMS
			if (!refundPeriodFlag) {
				Thread cancelSMSThread = new Thread(new SendCancelSMSThread(orderNo));
				cancelSMSThread.start();
			}
		}
		return this.responseResult;
	}

	private class SendCancelSMSThread implements Runnable {
		private Long orderNo;

		public SendCancelSMSThread(long orderNo) {
			this.orderNo = orderNo;
		}

		@Override
		public void run() {
			try {
				pcDayPassPurchaseService.sendCancelSMS(orderNo);
			} catch (Exception e) {
				logger.error("send Cancel SMS error." + e.getMessage(), e);
			}
		}
	}

	/**
	 * Method to update the potential customer and create the related customer
	 * enrollment detail
	 * 
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateDayPassPerson", method = RequestMethod.PUT)
	@ResponseBody
	@Deprecated
	public ResponseResult potentialCustomerRegistrationUpdate(@RequestBody CustomerProfileDto customerProfileDto) {
		try {
			String loginUserId = getUser().getUserId();
			customerProfileDto.setLoginUserId(loginUserId);
			Long customerId = customerProfileDto.getCustomerId();
			if (customerId == null) {
				responseResult.initResult(GTAError.LeadError.CUSTOMER_ID_REQUIRED);
				return responseResult;
			}
			ResponseResult result = leadCustomerService.saveOrUpdatePotentialCustomer(customerProfileDto);
			if ("0".equals(result.getReturnCode())) {
				CustomerProfile customerProfile = (CustomerProfile) result.getData();
				customerProfileService.moveProfileAndSignatureFile(customerProfile);
			}

			return result;
		} catch (Exception e) {
			logger.error("LeadCustomerController Error Message:", e);
			responseResult.initResult(GTAError.LeadError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/share/qrcode", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseResult daypassIssuedNotify(@RequestBody DaypassIssuedDto daypassIssuedDto) {

		try {

			ResponseResult rs = customerEmailContentService.daypassIssuedNotify(daypassIssuedDto);
			if ("0".equals(rs.getReturnCode())) {
				CustomerEmailContent cec = (CustomerEmailContent) rs.getDto();
				if (cec != null) {
					File qrcodeImage = QRCodeUtil.createQRCode(daypassIssuedDto.getDayPassId());
					boolean success = MailSender.sendEmail(cec.getRecipientEmail(), null, null, cec.getSubject(),
							cec.getContent(), new File[] { qrcodeImage });
					if (success) {
						cec.setStatus(EmailStatus.SENT.getName());
					} else {
						cec.setStatus(EmailStatus.FAIL.getName());
					}
					List<CustomerEmailContent> emailList = new ArrayList<CustomerEmailContent>();
					emailList.add(cec);
					customerEmailContentService.updateEmailStatusInBatch(emailList);
				}

				responseResult.initResult(GTAError.Success.SUCCESS);
				return responseResult;
			}
			return rs;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.CommomError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/daypass/advancesearch", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult daypassQueryData(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "", required = false) String sortBy,
			@RequestParam(value = "status", defaultValue = "All", required = false) String status,
			@RequestParam(value = "isAscending", defaultValue = "false", required = false) String isAscending,
			@RequestParam(value = "filter", defaultValue = "", required = false) String filter) {
		String _sortBy = "planNo";
		Map<String, org.hibernate.type.Type> typeMap = new HashMap<>();
		typeMap.put("planNo", LongType.INSTANCE);
		typeMap.put("status", StringType.INSTANCE);
		typeMap.put("planName", StringType.INSTANCE);
		typeMap.put("subscriberType", StringType.INSTANCE);
		typeMap.put("passNatureCode", StringType.INSTANCE);
		typeMap.put("effectiveEndDate", DateType.INSTANCE);
		typeMap.put("effectiveStartDate", DateType.INSTANCE);

		StringBuilder sql = new StringBuilder();
		sql.append(" ")
				.append("SELECT planNo, status,planName,passNatureCode,effectiveEndDate,effectiveStartDate, subscriberType FROM ( ")
				.append("SELECT plan_no planNo ").append(",contract_length_month contractLengthMonth ")
				.append(",effective_end_date effectiveEndDate ").append(",effective_start_date effectiveStartDate ")
				.append(",pass_nature_code passNatureCode ").append(",plan_name planName ").append(",status  ")
				.append(",subscriber_type  subscriberType ").append("FROM service_plan ").append(") sub ")
				.append("WHERE 1 = 1 and sub.passNatureCode='DP' ");
		String joinSQL = sql.toString();
		if ("ACT".equalsIgnoreCase(status)) {
			joinSQL += " and status='ACT' ";
		} else if ("NACT".equalsIgnoreCase(status)) {
			joinSQL += " and status='NACT' ";
		} else if ("EXP".equalsIgnoreCase(status)) {
			joinSQL += " and status='EXP' ";
		}
		if (!StringUtils.isEmpty(sortBy)) {
			_sortBy = sortBy;
		}
		// advance search
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filter, AdvanceQueryDto.class);
		if (advanceQueryDto != null && null != advanceQueryDto.getRules() && !advanceQueryDto.getRules().isEmpty()
				&& advanceQueryDto.getGroupOp() != null && advanceQueryDto.getGroupOp().length() > 0) {
			joinSQL += " and ";
			prepareQueryParameter(pageNumber, pageSize, _sortBy, isAscending, advanceQueryDto);
			this.responseResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinSQL, page,
					ServicePlan.class, typeMap);
			return dealWithDaypassListAdvanceResult(this.responseResult);
		}
		// no advance condition search
		AdvanceQueryDto queryDto = new AdvanceQueryDto();
		prepareQueryParameter(pageNumber, pageSize, _sortBy, isAscending, queryDto);
		this.responseResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinSQL, page, ServicePlan.class,
				typeMap);
		return dealWithDaypassListAdvanceResult(this.responseResult);
	}

	private ResponseResult dealWithDaypassListAdvanceResult(ResponseResult responseResult) {
		Data data = (Data) responseResult.getData();
		List<DaypassDto> dtos = new ArrayList<>();
		List<ServicePlan> list = (List<ServicePlan>) data.getList();
		for (ServicePlan temp : list) {
			temp.setServicePlanAdditionRules(null);
			temp.setServicePlanFacilities(null);
			temp.setServicePlanPoses(null);
			DaypassDto daypassDto = new DaypassDto();
			daypassDto.setEffectiveEndDate(DateConvertUtil.getYMDDateAndDateDiff(temp.getEffectiveEndDate()));
			daypassDto.setEffectiveStartDate(DateConvertUtil.getYMDDateAndDateDiff(temp.getEffectiveStartDate()));
			daypassDto.setPlanName(temp.getPlanName());
			daypassDto.setPlanNo(temp.getPlanNo());
			daypassDto.setStatus(temp.getStatus());
			daypassDto.setSubscriberType(temp.getSubscriberType());
			dtos.add(daypassDto);
		}
		data.setList(dtos);
		return responseResult;
	}

	@RequestMapping(value = "/purchasedaypass/advancesearch", method = { RequestMethod.GET })
	@ResponseBody
	public ResponseResult purchasedaypassQueryData(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,
			@RequestParam(value = "propertyName", defaultValue = "orderDate", required = false) String propertyName,
			@RequestParam(value = "show", required = false) String show,
			@RequestParam(value = "order", defaultValue = "false", required = false) String order,
			@RequestParam(value = "customerId", defaultValue = "", required = false) @EncryptFieldInfo Long customerId,
			@RequestParam(value = "filters", defaultValue = "", required = false) String filters) {
		PurchasedDaypassDto purchasedDaypassDto = new PurchasedDaypassDto();
		String joinHQL = this.servicePlanService.getPurchasedDaypassListSQL(purchasedDaypassDto);
		
		joinHQL += "where 1=1 ";
		if (show == null || "All".equals(show)) {
			joinHQL += "  ";
		} else if ("Today".equals(show)) {
			joinHQL += "and purchase.effectiveStartDate = CURRENT_DATE  ";
		} else if ("Within 90 days".equals(show)) {
			joinHQL += "and  purchase.effectiveStartDate <= CURRENT_DATE +90 and purchase.effectiveStartDate>= CURRENT_DATE  ";
		} else if ("Within 30 days".equals(show)) {
			joinHQL += "and  purchase.effectiveStartDate <= CURRENT_DATE +30  and purchase.effectiveStartDate>= CURRENT_DATE  ";
		} else if ("effective".equals(show)) {
			joinHQL += "and  purchase.effectiveEndDate >= CURRENT_DATE   ";
		} else if ("history".equals(show)) {
			joinHQL += "and  purchase.effectiveEndDate < CURRENT_DATE   ";
		}

		if (customerId != null && !StringUtils.isEmpty(customerId + "")) {
			joinHQL += " and purchase.customerId = " + customerId;
		}
		// advance search
		AdvanceQueryDto advanceQueryDto = (AdvanceQueryDto) new Gson().fromJson(filters, AdvanceQueryDto.class);
		if (advanceQueryDto != null && advanceQueryDto.getRules().size() > 0 && advanceQueryDto.getGroupOp() != null
				&& advanceQueryDto.getGroupOp().length() > 0) {
			joinHQL += "  and ";
			prepareQueryParameter(pageNumber, pageSize, propertyName, order, advanceQueryDto);
			this.responseResult=servicePlanService.getDayPassSearchQueryResultBySQL(advanceQueryDto, joinHQL, page, PurchasedDaypassDto.class);
//			this.responseResult = advanceQueryService.getAdvanceQueryResultBySQL(advanceQueryDto, joinHQL, page,
//					PurchasedDaypassDto.class);
			return dealWithPurchaseDaypassAdvanceResult(this.responseResult);
		}
		// no advance condition search
		AdvanceQueryDto queryDto = new AdvanceQueryDto();
		prepareQueryParameter(pageNumber, pageSize, propertyName, order, queryDto);
		this.responseResult = advanceQueryService.getInitialQueryResultBySQL(queryDto, joinHQL, page,
				PurchasedDaypassDto.class);
		return dealWithPurchaseDaypassAdvanceResult(this.responseResult);
	}

	private ResponseResult dealWithPurchaseDaypassAdvanceResult(ResponseResult responseResult) {
		Data data = (Data) responseResult.getData();
		List<PurchasedDaypassBackDto> dtos = new ArrayList<>();
		List<PurchasedDaypassDto> list = (List<PurchasedDaypassDto>) data.getList();
		for (PurchasedDaypassDto dto : list) {
			PurchasedDaypassBackDto purchasedDaypassBackDto = new PurchasedDaypassBackDto();
			purchasedDaypassBackDto.setPageSize(dto.getPageSize());
			purchasedDaypassBackDto.setPageNumber(dto.getPageNumber());
			purchasedDaypassBackDto.setContactEmail(dto.getContactEmail());
			purchasedDaypassBackDto.setOrderNo(dto.getOrderNo());
			purchasedDaypassBackDto.setOrderTotalAmount(dto.getOrderTotalAmount());
			purchasedDaypassBackDto.setOrderDate(DateConvertUtil.getYMDDate(dto.getOrderDate()));
			purchasedDaypassBackDto.setEffectiveEndDate(DateConvertUtil.getYMDDate(dto.getEffectiveEndDate()));
			purchasedDaypassBackDto.setEffectiveStartDate(DateConvertUtil.getYMDDate(dto.getEffectiveStartDate()));
			purchasedDaypassBackDto.setMemberName(dto.getMemberName());
			purchasedDaypassBackDto.setOrderStatus(dto.getOrderStatus());
			purchasedDaypassBackDto.setCustomerId(dto.getCustomerId());
			purchasedDaypassBackDto.setMemberBuy(dto.getMemberBuy());
			purchasedDaypassBackDto = servicePlanService.addTranscationFlag(purchasedDaypassBackDto);
			dtos.add(purchasedDaypassBackDto);
		}
		data.setList(dtos);
		return responseResult;
	}

	/****
	 * get customerOrderPermitCard ByQrcode
	 * 
	 * @param qrcode
	 * @return
	 */
	@RequestMapping(value = "/{qrcode}", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getDayPassByQrCode(@PathVariable(value = "qrcode") String qrcode) {
		if (qrcode.toUpperCase().startsWith("DP")) {
			// DPXXX Card NO to search
			return customerOrderPermitCardService.getCustomerOrderPermitCardByQrCode(null,qrcode);
		} else {
			// DPASS id to search
			return customerOrderPermitCardService.getCustomerOrderPermitCardByQrCode(qrcode,null);
		}
		
		
	}

}