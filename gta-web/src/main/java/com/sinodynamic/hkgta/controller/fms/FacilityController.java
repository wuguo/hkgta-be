package com.sinodynamic.hkgta.controller.fms;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sinodynamic.hkgta.controller.ControllerBase;
import com.sinodynamic.hkgta.dto.fms.FacilityItemStatusDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterChangeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterDto;
import com.sinodynamic.hkgta.dto.fms.FacilityMasterQueryDto;
import com.sinodynamic.hkgta.dto.fms.FacilitySubtypeDto;
import com.sinodynamic.hkgta.dto.fms.FacilitySubtypesDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotDto;
import com.sinodynamic.hkgta.dto.fms.FacilityTimeslotLogDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationPosDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateListDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateTimeDto;
import com.sinodynamic.hkgta.dto.fms.FacilityUtilizationRateTimeListDto;
import com.sinodynamic.hkgta.dto.golfmachine.BatInfo;
import com.sinodynamic.hkgta.entity.crm.GlobalParameter;
import com.sinodynamic.hkgta.entity.fms.FacilityAdditionAttribute;
import com.sinodynamic.hkgta.entity.fms.FacilityMaster;
import com.sinodynamic.hkgta.entity.fms.FacilitySubType;
import com.sinodynamic.hkgta.entity.fms.FacilityTimeslot;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationPos;
import com.sinodynamic.hkgta.entity.fms.FacilityUtilizationRateTime;
import com.sinodynamic.hkgta.entity.rpos.PosServiceItemPrice;
import com.sinodynamic.hkgta.integration.zoomtech.service.GolfMachineService;
import com.sinodynamic.hkgta.security.LoginUser;
import com.sinodynamic.hkgta.service.crm.backoffice.admin.GlobalParameterService;
import com.sinodynamic.hkgta.service.fms.FacilityAdditionAttributeService;
import com.sinodynamic.hkgta.service.fms.FacilityMasterService;
import com.sinodynamic.hkgta.service.fms.FacilitySubTypePosService;
import com.sinodynamic.hkgta.service.fms.FacilitySubTypeService;
import com.sinodynamic.hkgta.service.fms.FacilityTimeslotLogService;
import com.sinodynamic.hkgta.service.fms.FacilityTimeslotService;
import com.sinodynamic.hkgta.service.fms.FacilityUtilizationPosService;
import com.sinodynamic.hkgta.service.fms.FacilityUtilizationRateTimeService;
import com.sinodynamic.hkgta.service.rpos.PosServiceItemPriceService;
import com.sinodynamic.hkgta.util.DateCalcUtil;
import com.sinodynamic.hkgta.util.DateConvertUtil;
import com.sinodynamic.hkgta.util.Log4jFormatUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.AgeRangeCode;
import com.sinodynamic.hkgta.util.constant.CalendarWeekCode;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.FacilityStatus;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.constant.LoggerType;
import com.sinodynamic.hkgta.util.exception.GTACommonException;
import com.sinodynamic.hkgta.util.response.Data;
import com.sinodynamic.hkgta.util.response.ResponseResult;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/facility")
@Scope("prototype")
public class FacilityController extends ControllerBase<FacilityMaster> {

	private Logger logger = Logger.getLogger(FacilityController.class);
	
	private Logger zoomtechLog = Logger.getLogger(LoggerType.ZOOMTECH.getName()); 

	@Autowired
	private FacilityMasterService facilityMasterService;
	@Autowired
	private FacilityTimeslotService facilityTimeslotService;

	@Autowired
	private FacilityUtilizationPosService facilityUtilizationPosService;

	@Autowired
	private FacilityUtilizationRateTimeService facilityUtilizationRateTimeService;

	@Autowired
	private PosServiceItemPriceService posServiceItemPriceService;

	@Autowired
	private GlobalParameterService globalParameterService;

	@Autowired
	private FacilityAdditionAttributeService facilityAdditionAttributeService;

	@Autowired
	private FacilitySubTypeService facilitySubTypeService;

	@Autowired
	private FacilitySubTypePosService facilitySubTypePosService;

	@Autowired
	private FacilityTimeslotLogService facTimeslotLogSvc;

	@Autowired
	private GolfMachineService golfMachineService;

	@Value("#{appProperties['golfmachine.status_on']}")
	String golfBayStatusON;

	/**
	 * get Facility List data
	 * 
	 * @param venueFloor
	 *            ,the facility venue floor for golf (1/2/3)
	 * @param facilityType
	 *            , the facility type (Golf/Tennis)
	 * @param beginDate
	 *            ,the facility timeslot begin date
	 * @return all facility List data
	 */
	@RequestMapping(value = "/golf/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getFacilityList(
			@RequestParam(value = "venueFloor", required = true, defaultValue = "1") int venueFloor,
			@RequestParam(value = "facilityType", required = true, defaultValue = "GOLF") String facilityType,
			@RequestParam(value = "beginDate", required = true) String beginDate) {
		logger.info("FacilityController.getFacilityList start ...");
		try {
			List<FacilityMaster> facilityMasterList = facilityMasterService.getFacilityMasterList(venueFloor,
					facilityType);
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
			List<FacilityTimeslot> facilityTimeslot = facilityTimeslotService.getFacilityTimeslotList(facilityType,
					dateformat.parse(beginDate), venueFloor);
			responseResult = getFacilityMasterDtoList(facilityMasterList, facilityTimeslot);
			return responseResult;
		} catch (Exception e) { // e.printStackTrace();
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	private ResponseResult getFacilityMasterDtoList(List<FacilityMaster> facilityMasterList,
			List<FacilityTimeslot> facilityTimeslot) {
		Map<String, String> map = new HashMap<String, String>();
		Calendar calendar = Calendar.getInstance();
		if (facilityTimeslot != null && facilityTimeslot.size() > 0) {
			for (FacilityTimeslot timeslot : facilityTimeslot) {
				calendar.setTime(timeslot.getBeginDatetime());
				map.put(timeslot.getFacilityMaster().getFacilityNo() + "-" + calendar.get(Calendar.HOUR_OF_DAY),
						timeslot.getStatus());
			}
		}
		if (facilityMasterList == null || facilityMasterList.size() == 0)
			return null;
		Map<Long, String> attriMap = getAttributeTypeMap("GOLFBAYTYPE-LH");
		Map<Long, String> zoneMap = getAttributeTypeMap(Constant.ZONE_COACHING);
		List<Map<Map<String, String>, List<FacilityMasterDto>>> retData = new ArrayList<Map<Map<String, String>, List<FacilityMasterDto>>>();
		Map<String, Object> retMap = null;
		List<FacilityMasterDto> listData = null;
		List<Object> masterList = new ArrayList<Object>();
		for (int i = 7; i <= 22; i++) {
			retMap = new HashMap<String, Object>();
			listData = new ArrayList<FacilityMasterDto>();
			for (FacilityMaster facility : facilityMasterList) {
				FacilityMasterDto dto = new FacilityMasterDto();
				dto.setFacilityNo(facility.getFacilityNo());
				dto.setFacilityName(facility.getFacilityName());
				dto.setVenueCode(null == facility.getVenueMaster() ? null : facility.getVenueMaster().getVenueCode());
				if (map.get(facility.getFacilityNo() + "-" + i) != null) {
					dto.setStatus(map.get(facility.getFacilityNo() + "-" + i));
				} else {
					dto.setStatus(FacilityStatus.VC.toString());
				}

				if (attriMap.get(facility.getFacilityNo()) != null) {
					dto.setFacilityAttribute(attriMap.get(facility.getFacilityNo()));
				}

				if (zoneMap.get(facility.getFacilityNo()) != null) {
					dto.setZone(zoneMap.get(facility.getFacilityNo()));
				}

				listData.add(dto);
			}
			retMap.put("startTime", i + ":00");
			retMap.put("facilityList", listData);

			masterList.add(retMap);
		}
		Data result = new Data(masterList);
		result.setCurrentPage(1);
		result.setLastPage(true);
		result.setPageSize(retData.size());
		result.setRecordCount(retData.size());
		result.setTotalPage(1);
		logger.info("FacilityController.getFacilityList invocation end...");
		responseResult.initResult(GTAError.FacilityError.SUCCESS, result);
		return responseResult;
	}

	private Map<Long, String> getAttributeTypeMap(String attributeId) {
		Map<Long, String> attriMap = new HashMap<Long, String>();
		List<FacilityAdditionAttribute> leftBayTypeAttributes = facilityAdditionAttributeService
				.getFacilityAdditionAttributeList(attributeId);
		if (null != leftBayTypeAttributes && leftBayTypeAttributes.size() > 0) {
			for (FacilityAdditionAttribute attri : leftBayTypeAttributes) {
				attriMap.put(attri.getId().getFacilityNo(), attri.getId().getAttributeId());
			}
		}
		return attriMap;
	}

	/**
	 * get venue floor List
	 * 
	 * @param facilityType
	 *            , the facility type (Golf/Tennis)
	 * @param venueCode
	 *            , the facility venue code for Tennis,if tennis it can not be
	 *            null
	 * @return venue floor List for Golf or Tennis
	 */
	@RequestMapping(value = "/venuefloor/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getVenuefloorList(@RequestParam(value = "facilityType", required = true) String facilityType,
			@RequestParam(value = "venueCode", required = false) String venueCode) {

		logger.info("FacilityController.getVenuefloorList start ...");

		try {

			List<FacilityMaster> facilityMasterList = facilityMasterService.getFacilityVenueFloorList(facilityType,
					venueCode);

			if (facilityMasterList == null || facilityMasterList.size() == 0)
				return null;

			List<FacilityMasterDto> retData = new ArrayList<FacilityMasterDto>();
			for (FacilityMaster facility : facilityMasterList) {
				FacilityMasterDto dto = new FacilityMasterDto();
				if (null != facility.getVenueFloor())
					dto.setVenueFloor(facility.getVenueFloor());
				retData.add(dto);
			}

			Data result = new Data(retData);
			result.setCurrentPage(1);
			result.setLastPage(true);
			result.setPageSize(retData.size());
			result.setRecordCount(retData.size());
			result.setTotalPage(1);

			logger.info("FacilityController.getVenuefloorList invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, result);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/coStudio/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getCoStudioList(@RequestParam(value = "beginDate", required = true) String beginDate) {

		try {
			List<FacilityMaster> facilityMasterList = facilityMasterService
					.getFacilityByAttribute(Constant.FACILITY_TYPE_GOLF, Constant.GOLFBAYTYPE_CAR);
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
			List<FacilityTimeslot> facilityTimeslot = facilityTimeslotService
					.getFacilityTimeslotList(Constant.FACILITY_TYPE_GOLF, dateformat.parse(beginDate), null);
			responseResult = getFacilityMasterDtoList(facilityMasterList, facilityTimeslot);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * Get the status of a single facility.
	 * 
	 * @param facilityNo
	 *            the PK of the facility master
	 * @param beginDatetime
	 *            the start time of facility_timeslot
	 * @return All the facility_timeslot records of the same reservation. If
	 *         there is member information, member name, reserve time will be
	 *         returned also.
	 */
	@RequestMapping(value = "/getFacilityStatus", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getFacilityStatus(
			@RequestParam(value = "facilityNo", required = true) Long facilityNo,
			@RequestParam(value = "begin_datetime", required = true) String beginDatetime) {

		logger.info("FacilityController.getFacilityStatus start ...");

		try {
			FacilityItemStatusDto facilityItemStatusDto = new FacilityItemStatusDto();
			facilityItemStatusDto.setFacilityNo(facilityNo);
			facilityItemStatusDto.setBeginDatetime(DateCalcUtil.parseDateTime(beginDatetime));
			facilityItemStatusDto.setEndDatetime(DateCalcUtil.GetEndTime(facilityItemStatusDto.getBeginDatetime()));
			return facilityMasterService.getFacilityStatus(facilityItemStatusDto);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/{facilityType}/{facilityNo}/timeslotLogs", method = RequestMethod.GET)
	public @ResponseBody ResponseResult getFacilityStatusLogs(@PathVariable(value = "facilityType") String facilityType,
			@PathVariable(value = "facilityNo") Long facilityNo,@RequestParam(value="month",defaultValue="6",required=false) int month) {
		//For the status log , show all records starting from past 6 months. For example
		List<FacilityTimeslotLogDto> logList = facTimeslotLogSvc.getLogsByFacilityNo(facilityType, facilityNo, month);
		responseResult.initResult(GTAError.Success.SUCCESS, logList);
		return responseResult;
	}

	/**
	 * Save the status of facility.
	 * 
	 * @param facilityItemStatusDto
	 *            the data transfer object. Need to provide facility_timeslot_id
	 *            or facilityNo and begin_datetime.
	 * @return responseResult ResponseResult based on the input.
	 */
	@RequestMapping(value = "/saveFacilityStatus", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult saveFacilityStatus(@RequestBody FacilityItemStatusDto facilityItemStatusDto) {
		logger.info("FacilityController.saveFacilityStatus start ...");

		if (null == facilityItemStatusDto) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		} else {
			if (facilityItemStatusDto.getFacilityNo() <= 0) {
				responseResult.initResult(GTAError.FacilityError.FACILITYNO_ISNULL);
				return responseResult;
			}
			if (StringUtils.isEmpty(facilityItemStatusDto.getBeginDate())) {
				responseResult.initResult(GTAError.FacilityError.BEGINEND_ISNULL);
				return responseResult;
			}
			if (StringUtils.isEmpty(facilityItemStatusDto.getStatus())) {
				responseResult.initResult(GTAError.FacilityError.STATUS_INVALID);
				return responseResult;
			} else {
				boolean flag = false;
				FacilityStatus[] facilityStatuss = FacilityStatus.values();
				for (FacilityStatus status : facilityStatuss) {
					if (status.toString().equals(facilityItemStatusDto.getStatus()))
						flag = true;
				}
				if (!flag) {
					responseResult.initResult(GTAError.FacilityError.STATUS_INVALID);
					logger.info("FacilityController.saveFacilityStatus end ...");
					return responseResult;
				}
			}
		}
		LoginUser user = super.getUser();
		if (user == null) {

			logger.error("Can not get current user!");
			responseResult.initResult(GTAError.FacilityError.USER_ISNULL);
			return responseResult;
		}
		facilityItemStatusDto.setUpdateBy(user.getUserId());
		try {
			facilityItemStatusDto.setBeginDatetime(DateCalcUtil.parseDateTime(facilityItemStatusDto.getBeginDate()));
			responseResult = facilityMasterService.saveFacilityStatus(facilityItemStatusDto);
			return responseResult;
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			if (null != e.getError()) {
				if ((e.getError().getCode()).equals(GTAError.FacilityError.DATETIME_LESSTHAN_TODAY.getCode())) {
					responseResult.initResult(e.getError(), e.getArgs());
				} else {
					responseResult.initResult(e.getError());
				}
			} else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

	/**
	 * change facility times lot
	 * 
	 * @param facilityMasterChangeDto
	 *            , receiving all request parameters for change facility
	 * @return change facility success or fail,if fail will response error
	 *         message
	 */
	@RequestMapping(value = "/golf/change", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseResult changeFacilityTimeslot(HttpServletRequest request,@RequestBody FacilityMasterChangeDto facilityMasterChangeDto) {
		
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), JSONObject.fromObject(facilityMasterChangeDto).toString(), RequestMethod.POST.name(),null));
		
		boolean flag = checkChangeFacilityData(facilityMasterChangeDto);
		if (flag)
			return responseResult;
		LoginUser user = super.getUser();
		if (user == null) {
			logger.error("Can not get current user!");
			responseResult.initResult(GTAError.FacilityError.USER_ISNULL);
			zoomtechLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, JSONObject.fromObject(responseResult).toString(),null));
			return responseResult;
		}
		try {
			facilityMasterChangeDto.setUpdateBy(user.getUserId());
			facilityMasterService.changeFacilityTimeslot(facilityMasterChangeDto);
			//check in status is OP turn on/off golf bay
			if(Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityMasterChangeDto.getFacilityType()) && FacilityStatus.OP.name().equals(facilityMasterChangeDto.getStatus()))
			{
				turnOnOffTeeupMachine(facilityMasterChangeDto);	
			}
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			if (null != e.getError()) {
				if (null != e.getArgs() && e.getArgs().length > 0)
					responseResult.initResult(e.getError(), e.getArgs());
				else
					responseResult.initResult(e.getError());
			} else {
				responseResult.initResult(GTAError.FacilityError.CHANGE_FACILITY_FAILED, e.getMessage());
			}

			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.CHANGE_FACILITY_FAILED, e.getMessage());
			zoomtechLog.error(Log4jFormatUtil.logErrorMessage(null, null, null, JSONObject.fromObject(responseResult).toString(), e.getMessage()));
			return responseResult;
		}
		responseResult.initResult(GTAError.FacilityError.SUCCESS);
		logger.info("FacilityController.changeFacilityTimeslot end ...");
		zoomtechLog.info(Log4jFormatUtil.logInfoMessage(request.getRequestURI(), JSONObject.fromObject(facilityMasterChangeDto).toString(), RequestMethod.POST.name(),JSONObject.fromObject(responseResult).toString()));
		
		return responseResult;
	}
	private void turnOnOffTeeupMachine(FacilityMasterChangeDto facilityMasterChangeDto)throws Exception {
		//targetBookingDate+""+targetStartTime+":00:00"
		String start=facilityMasterChangeDto.getTargetBookingDate()+" "+facilityMasterChangeDto.getTargetStartTime()+":00:00";
		String end=facilityMasterChangeDto.getTargetBookingDate()+" "+facilityMasterChangeDto.getTargetEndTime()+":59:59";
		Date startTime = DateConvertUtil.parseString2Date(start, "yyyy-MM-dd HH:mm:ss");
		Date endTime = DateConvertUtil.parseString2Date(end, "yyyy-MM-dd HH:mm:ss");
		//if change bay on same time  turn on targetNo ,and turn on srcNo
		//else turn on srcNo
		if(facilityMasterChangeDto.getSrcBookingDate().equals(facilityMasterChangeDto.getTargetBookingDate())
			&&facilityMasterChangeDto.getSrcStartTime().equals(facilityMasterChangeDto.getTargetStartTime())
			&&facilityMasterChangeDto.getSrcEndTime().equals(facilityMasterChangeDto.getTargetEndTime())
		    &&startTime.before(new Date())){
			// turn on
			golfMachineService.setTeeupMachineStatsbyCheckinTime(facilityMasterChangeDto.getTargetFacilityNo(), startTime,endTime);
		}
		 // turn off
		 golfMachineService.changeTeeupMachineStatsbyBatNo(facilityMasterChangeDto.getSrcFacilityNo(), "OFF");
	}

	private boolean checkChangeFacilityData(FacilityMasterChangeDto facilityMasterChangeDto) {
		if (null == facilityMasterChangeDto) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return true;
		} else {
			if (facilityMasterChangeDto.getFacilityType().equalsIgnoreCase(Constant.FACILITY_TYPE_TENNIS)
					&& facilityMasterChangeDto.getStatus().equals(FacilityStatus.TA.name())) {
				responseResult.initResult(GTAError.FacilityError.STATUS_CANNOT_CHANGE);
				return true;
			} else if (StringUtils.isEmpty(facilityMasterChangeDto.getFacilityType())) {
				responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
				return true;
			} else if (StringUtils.isEmpty(facilityMasterChangeDto.getStatus())) {
				responseResult.initResult(GTAError.FacilityError.STATUS_ISNULL);
				return true;
			} else if (StringUtils.isEmpty(facilityMasterChangeDto.getSrcBookingDate())) {
				responseResult.initResult(GTAError.FacilityError.BOOKINGDATE_ISNULL);
				return true;
			} else if (facilityMasterChangeDto.getSrcFacilityNo() == null
					|| facilityMasterChangeDto.getSrcFacilityNo() <= 0) {
				responseResult.initResult(GTAError.FacilityError.SRCFACILITYNO_ISNULL);
				return true;
			} else if (facilityMasterChangeDto.getSrcStartTime() == null
					|| facilityMasterChangeDto.getSrcStartTime() <= 0) {
				responseResult.initResult(GTAError.FacilityError.STARTTIME_ISNULL);
				return true;
			} else
				if (facilityMasterChangeDto.getSrcEndTime() == null || facilityMasterChangeDto.getSrcEndTime() <= 0) {
				responseResult.initResult(GTAError.FacilityError.ENDTIME_ISNULL);
				return true;
			} else if (facilityMasterChangeDto.getSrcStartTime() > facilityMasterChangeDto.getSrcEndTime()) {
				responseResult.initResult(GTAError.FacilityError.STARTTIME_OVER_ENDTIME);
				return true;
			}

			else if (facilityMasterChangeDto.getTargetFacilityNo() == null
					|| facilityMasterChangeDto.getTargetFacilityNo() <= 0) {
				responseResult.initResult(GTAError.FacilityError.TARGETFACILITYNO);
				return true;
			} else if (StringUtils.isEmpty(facilityMasterChangeDto.getTargetBookingDate())) {
				responseResult.initResult(GTAError.FacilityError.BOOKINGDATE_ISNULL);
				return true;
			} else if (facilityMasterChangeDto.getTargetStartTime() == null
					|| facilityMasterChangeDto.getTargetStartTime() <= 0) {
				responseResult.initResult(GTAError.FacilityError.STARTTIME_ISNULL);
				return true;
			} else if (facilityMasterChangeDto.getTargetEndTime() == null
					|| facilityMasterChangeDto.getTargetEndTime() <= 0) {
				responseResult.initResult(GTAError.FacilityError.ENDTIME_ISNULL);
				return true;
			} else if (facilityMasterChangeDto.getTargetStartTime() > facilityMasterChangeDto.getTargetEndTime()) {
				responseResult.initResult(GTAError.FacilityError.STARTTIME_OVER_ENDTIME);
				return true;
			}
		}
		return false;
	}

	/**
	 * get Tennis courts list data
	 * 
	 * @param facilityType
	 *            the facility type of Tennis
	 * @return Tennis courts list data
	 */
	@RequestMapping(value = "/tenniscourts/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg getTennisCourtsList(
			@RequestParam(value = "facilityType", defaultValue = "TENNIS") String facilityType) {

		List<FacilityMaster> tennisCourts = facilityMasterService.getFacilityMasterList(null, facilityType);
		List<FacilityMasterDto> retList = new ArrayList<FacilityMasterDto>(tennisCourts.size());

		FacilityMasterDto dto = null;
		for (FacilityMaster master : tennisCourts) {
			dto = new FacilityMasterDto();
			dto.setFacilityName(master.getFacilityName());
			dto.setFacilityNo(master.getFacilityNo());
			FacilitySubType subType = facilitySubTypeService
					.getFacilitySubTypeBySubTypeId(master.getFacilitySubtypeId());
			dto.setVenueCode(master.getFacilitySubtypeId());
			dto.setVenueName(null == subType ? "" : subType.getName());
			retList.add(dto);
		}

		Data result = new Data(retList);
		result.setCurrentPage(1);
		result.setLastPage(true);
		result.setPageSize(retList.size());
		result.setRecordCount(retList.size());
		result.setTotalPage(1);

		logger.info("FacilityController.getTennisCourtsList() invocation end...");
		responseResult.initResult(GTAError.FacilityError.SUCCESS, result);
		return responseResult;
	}

	/**
	 * get Tennis courts times lots data
	 * 
	 * @param facilityType
	 *            the facility type of Tennis
	 * @param beginDate
	 *            the begin date of facility timeslot
	 * @return Tennis courts times lots data
	 */
	@RequestMapping(value = "/tenniscourts/timeslots", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg getTennisCourtsTimeslots(
			@RequestParam(value = "facilityType", defaultValue = "TENNIS") String facilityType,
			@RequestParam(value = "beginDate") String beginDate) {

		List<FacilityTimeslot> facilityTimeslot;
		List<FacilityTimeslotDto> retList;
		Calendar c = Calendar.getInstance();

		try {
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
			facilityTimeslot = facilityTimeslotService.getFacilityTimeslotList(facilityType,
					dateformat.parse(beginDate), null);
			retList = new ArrayList<FacilityTimeslotDto>(facilityTimeslot.size());

			FacilityTimeslotDto dto = null;
			for (FacilityTimeslot timeslot : facilityTimeslot) {
				dto = new FacilityTimeslotDto();
				dto.setFacilityNo(timeslot.getFacilityMaster().getFacilityNo());
				c.setTime(timeslot.getBeginDatetime());
				dto.setBeginDatetime(c.get(Calendar.HOUR_OF_DAY) + ":00");
				dto.setStatus(timeslot.getStatus());
				retList.add(dto);
			}

			Data result = new Data(retList);
			result.setCurrentPage(1);
			result.setLastPage(true);
			result.setPageSize(retList.size());
			result.setRecordCount(retList.size());
			result.setTotalPage(1);

			logger.info("FacilityController.getTennisCourtsTimeslots() invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, result);
			return responseResult;

		} catch (ParseException e) {
			logger.error("Error paring date:" + beginDate, e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/reservation/getReservation", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg getReservationInfoByCustomerId(@RequestParam(value = "customerId") long customerId) {
		try {
			return facilityMasterService.getReservationInfoByCustomerId(customerId);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * get Facility General Setting
	 * 
	 * @return Facility Price
	 */
	@RequestMapping(value = "/generalSetting/{facilityType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg getFacilityGeneralPrice(@PathVariable(value = "facilityType") String facilityType,
			@RequestParam(value = "subType", required = false) String subType) {
		logger.info("FacilityController.getFacilityGeneralPrice() invocation start...");
		if (StringUtils.isEmpty(facilityType)) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		}

		try {
			Map<String, Object> dataMap = new HashMap<String, Object>();

			List<?> priceList = null;
			if (Constant.FACILITY_TYPE_TENNIS.equalsIgnoreCase(facilityType)) {
				priceList = getTennisPriceList(facilityType, subType);
			} else {
				priceList = getFacilityPriceList(facilityType);
			}
			// priceList = getFacilityPriceList(facilityType);
			if (priceList != null) {
				dataMap.put("priceList", priceList);
				dataMap.put("advancedResPeriod", getAdvancedResPeriod(facilityType));
				responseResult.initResult(GTAError.FacilityError.SUCCESS, dataMap);
			} else {
				responseResult.initResult(GTAError.FacilityError.SUCCESS);
			}
			logger.info("FacilityController.getFacilityGeneralPrice() invocation end...");
			return responseResult;

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	private String getAdvancedResPeriod(String facilityType) {
		String paramValue = "";
		String facTypeConst = Constant.FACILITY_TYPE_GOLF.equalsIgnoreCase(facilityType)
				? Constant.ADVANCEDRESPERIOD_GOLF : Constant.ADVANCEDRESPERIOD_TENNIS;
		ResponseResult response = globalParameterService.getGlobalParameterById(facTypeConst);
		if (null != response) {
			if (null != response.getData()) {
				GlobalParameter globalParameter = (GlobalParameter) response.getData();
				if (null != globalParameter) {
					paramValue = globalParameter.getParamValue();
				}
			}
		}
		return paramValue;
	}

	private List<FacilityUtilizationPosDto> getFacilityPriceList(String facilityType) {
		List<FacilityUtilizationPos> facilityUtilizationPosList = facilityUtilizationPosService
				.getFacilityUtilizationPosList(facilityType);
		if (facilityUtilizationPosList == null || facilityUtilizationPosList.size() == 0) {
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
			return null;
		}

		List<FacilityUtilizationPosDto> data = new ArrayList<FacilityUtilizationPosDto>();
		for (FacilityUtilizationPos pos : facilityUtilizationPosList) {
			if (null != pos.getAgeRangeCode() && pos.getPosItemNo().contains(Constant.FACILITY_PRICE_PREFIX)
					&& (pos.getAgeRangeCode().equals(AgeRangeCode.ADULT.getDesc())
							|| pos.getAgeRangeCode().equals(AgeRangeCode.CHILD.getDesc()))) {
				PosServiceItemPrice price = posServiceItemPriceService.getByItemNo(pos.getPosItemNo());
				FacilityUtilizationPosDto dto = new FacilityUtilizationPosDto();
				dto.setFacilityPosId(pos.getFacilityPosId());
				dto.setAgeRangeCode(pos.getAgeRangeCode());
				dto.setItemPrice(null != price ? price.getItemPrice() : null);
				dto.setRateType(pos.getRateType());
				data.add(dto);
			}
		}
		return data;
	}

	private List<FacilitySubtypeDto> getTennisPriceList(String facilityType, String subType) {
		return facilitySubTypeService.getFacilitySubType(facilityType, subType);
	}

	/**
	 * save Facility General Setting
	 * 
	 * @return Facility Price
	 */
	@RequestMapping(value = "/generalSetting/{facilityType}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg saveFacilityGeneralPrice(@PathVariable(value = "facilityType") String facilityType,
			@RequestBody FacilityUtilizationPosDto facilityUtilizationPosDto) {
		logger.info("FacilityController.saveFacilityGeneralPrice() invocation start...");
		if (null == facilityUtilizationPosDto) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		} else {
			boolean flag = checkGeneralSettingData(facilityType, facilityUtilizationPosDto);
			if (flag)
				return responseResult;
		}
		try {
			facilityUtilizationPosDto.setUpdateBy(this.getUser().getUserId());
			facilityUtilizationPosDto.setFacilityType(facilityType.toUpperCase());
			facilityUtilizationPosService.saveFacilityUtilizationPos(facilityUtilizationPosDto);

			logger.info("FacilityController.saveFacilityGeneralPrice() invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
			return responseResult;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	@RequestMapping(value = "/tennisGeneralSettings", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg saveTennisGeneralPrice(@RequestBody FacilitySubtypesDto facilitySubtypesDto) {

		if (null != facilitySubtypesDto && facilitySubtypesDto.getAdvancedResPeriod() > 0
				&& null != facilitySubtypesDto.getPriceList() && facilitySubtypesDto.getPriceList().size() > 0) {
			boolean flag = checkGeneralSettingTennisData(facilitySubtypesDto);
			if (flag)
				return responseResult;
			facilitySubtypesDto.setUpdateBy(getUser().getUserId());
			facilitySubTypePosService.saveFacilitySubTypePos(facilitySubtypesDto);
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
		} else
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);

		return responseResult;
	}

	private boolean checkGeneralSettingTennisData(FacilitySubtypesDto subtypesDto) {
		if (subtypesDto.getAdvancedResPeriod() > Constant.MAX_FACILITY_ADVANCEDRESPERIOD) {
			responseResult.initResult(GTAError.FacilityError.ADVANCEDRESPERIOD_EXCEED,
					new Object[] { Constant.MAX_FACILITY_ADVANCEDRESPERIOD });
			return true;
		}
		for (FacilitySubtypeDto posDto : subtypesDto.getPriceList()) {
			BigDecimal adultHighPrice = posDto.getAdultHigh().getPrice();
			BigDecimal childHighPrice = posDto.getChildHigh().getPrice();
			BigDecimal adultLowPrice = posDto.getAdultLow().getPrice();
			BigDecimal childLowPrice = posDto.getChildLow().getPrice();
			if (adultHighPrice.compareTo(childHighPrice) < 0) {
				responseResult.initResult(GTAError.FacilityError.ADULTRATE_LESSTHAN_CHILDRATE,
						new String[] { posDto.getCourtName() });
				return true;
			} else if (adultLowPrice.compareTo(childLowPrice) < 0) {
				responseResult.initResult(GTAError.FacilityError.ADULTRATE_LESSTHAN_CHILDRATE,
						new String[] { posDto.getCourtName() });
				return true;
			} else if (adultHighPrice.compareTo(adultLowPrice) < 0) {
				responseResult.initResult(GTAError.FacilityError.HIGHRATE_LESSTHAN_LOWRATE,
						new String[] { posDto.getCourtName() });
				return true;
			} else if (adultLowPrice.compareTo(childLowPrice) < 0) {
				responseResult.initResult(GTAError.FacilityError.HIGHRATE_LESSTHAN_LOWRATE,
						new String[] { posDto.getCourtName() });
				return true;
			}
		}
		return false;
	}

	private boolean checkGeneralSettingData(String facilityType, FacilityUtilizationPosDto facilityUtilizationPosDto) {
		if (StringUtils.isEmpty(facilityType)) {
			responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
			return true;
		}
		if (facilityUtilizationPosDto.getAdvancedResPeriod() == null
				|| facilityUtilizationPosDto.getAdvancedResPeriod() <= 0) {
			responseResult.initResult(GTAError.FacilityError.ADVANCEDRESPERIOD_ISNULL);
			return true;
		} else if (facilityUtilizationPosDto.getAdvancedResPeriod() > Constant.MAX_FACILITY_ADVANCEDRESPERIOD) {
			responseResult.initResult(GTAError.FacilityError.ADVANCEDRESPERIOD_EXCEED,
					new Object[] { Constant.MAX_FACILITY_ADVANCEDRESPERIOD });
			return true;
		}
		List<FacilityUtilizationPosDto> priceList = facilityUtilizationPosDto.getPriceList();
		if (priceList == null || priceList.size() == 0) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return true;
		}
		BigDecimal adultHighPrice = BigDecimal.ZERO;
		BigDecimal childHighPrice = BigDecimal.ZERO;
		BigDecimal adultLowPrice = BigDecimal.ZERO;
		BigDecimal childLowPrice = BigDecimal.ZERO;
		for (FacilityUtilizationPosDto posDto : priceList) {
			BigDecimal itemPrice = posDto.getItemPrice();
			if (null != itemPrice) {
				if (itemPrice.compareTo(new BigDecimal(Constant.MAX_FACILITY_ITEMPRICE)) > 0) {
					responseResult.initResult(GTAError.FacilityError.FACILITY_ITEMPRICE_EXCEED,
							new Object[] { Constant.MAX_FACILITY_ITEMPRICE });
					return true;
				}
				if (AgeRangeCode.ADULT.getDesc().equals(posDto.getAgeRangeCode())
						&& posDto.getRateType().equals("HI")) {
					adultHighPrice = itemPrice;
				}
				if (AgeRangeCode.CHILD.getDesc().equals(posDto.getAgeRangeCode())
						&& posDto.getRateType().equals("HI")) {
					childHighPrice = itemPrice;
				}
				if (AgeRangeCode.ADULT.getDesc().equals(posDto.getAgeRangeCode())
						&& posDto.getRateType().equals("LO")) {
					adultLowPrice = itemPrice;
				}
				if (AgeRangeCode.CHILD.getDesc().equals(posDto.getAgeRangeCode())
						&& posDto.getRateType().equals("LO")) {
					childLowPrice = itemPrice;
				}
			}
		}
		if (adultHighPrice.compareTo(childHighPrice) < 0) {
			responseResult.initResult(GTAError.FacilityError.ADULTRATE_LESSTHAN_CHILDRATE, new String[] { "" });
			return true;
		}
		if (adultLowPrice.compareTo(childLowPrice) < 0) {
			responseResult.initResult(GTAError.FacilityError.ADULTRATE_LESSTHAN_CHILDRATE, new String[] { "" });
			return true;
		}
		if (adultHighPrice.compareTo(adultLowPrice) < 0) {
			responseResult.initResult(GTAError.FacilityError.HIGHRATE_LESSTHAN_LOWRATE, new String[] { "" });
			return true;
		}
		if (adultLowPrice.compareTo(childLowPrice) < 0) {
			responseResult.initResult(GTAError.FacilityError.HIGHRATE_LESSTHAN_LOWRATE, new String[] { "" });
			return true;
		}
		return false;
	}

	/**
	 * get Facility General Price
	 * 
	 * @return Facility Price
	 */
	@RequestMapping(value = "/defaultRate/{facilityType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMsg getFacilityDefaultRate(@PathVariable(value = "facilityType") String facilityType,
			@RequestParam(value = "subType", required = false) String subType) {
		logger.info("FacilityController.getFacilityDefaultRate() invocation start...");
		if (StringUtils.isEmpty(facilityType)) {
			responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
			return responseResult;
		}
		try {
			List<FacilityUtilizationRateTime> rateTimeList = null;
			if (StringUtils.isEmpty(subType))
				rateTimeList = facilityUtilizationRateTimeService.getFacilityUtilizationRateTimeList(facilityType);
			else
				rateTimeList = facilityUtilizationRateTimeService
						.getFacilityUtilizationRateTimeListBySubType(facilityType, subType);

			if (rateTimeList == null || rateTimeList.size() == 0) {
				responseResult.initResult(GTAError.FacilityError.SUCCESS);
				return responseResult;
			}
			List<FacilityUtilizationRateTimeDto> listData = null;
			Map<String, List<FacilityUtilizationRateTimeDto>> retMap = new HashMap<String, List<FacilityUtilizationRateTimeDto>>();
			for (FacilityUtilizationRateTime rateTime : rateTimeList) {
				listData = retMap.get(rateTime.getWeekDay());
				if (null == listData) {
					listData = new ArrayList<FacilityUtilizationRateTimeDto>();
					retMap.put(rateTime.getWeekDay(), listData);
				}
				FacilityUtilizationRateTimeDto rateTimeDto = new FacilityUtilizationRateTimeDto();
				rateTimeDto.setTimeId(rateTime.getTimeId());
				rateTimeDto.setRateType(rateTime.getRateType());
				rateTimeDto.setBeginTime(rateTime.getBeginTime());
				listData.add(rateTimeDto);
			}
			List<Object> resultData = new ArrayList<Object>();
			Map<String, Object> resultMap = null;
			Iterator<Entry<String, List<FacilityUtilizationRateTimeDto>>> it = retMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, List<FacilityUtilizationRateTimeDto>> entry = it.next();
				resultMap = new HashMap<String, Object>();
				int weekDay = Integer.parseInt(entry.getKey());
				switch (weekDay) {
				case 1:
					resultMap.put("weekName", CalendarWeekCode.Sunday.getDesc());
					break;
				case 2:
					resultMap.put("weekName", CalendarWeekCode.Monday.getDesc());
					break;
				case 3:
					resultMap.put("weekName", CalendarWeekCode.Tuesday.getDesc());
					break;
				case 4:
					resultMap.put("weekName", CalendarWeekCode.Wednesday.getDesc());
					break;
				case 5:
					resultMap.put("weekName", CalendarWeekCode.Thursday.getDesc());
					break;
				case 6:
					resultMap.put("weekName", CalendarWeekCode.Friday.getDesc());
					break;
				case 7:
					resultMap.put("weekName", CalendarWeekCode.Saturday.getDesc());
					break;
				}
				resultMap.put("facilityType", facilityType);
				resultMap.put("subType", subType);
				resultMap.put("weekDay", entry.getKey());
				resultMap.put("rateList", entry.getValue());
				resultData.add(resultMap);
			}
			logger.info("FacilityController.getFacilityDefaultRate() invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS, resultData);
			return responseResult;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * save Facility General Price
	 * 
	 * @return Facility Price
	 */
	@RequestMapping(value = "/defaultRate/{facilityType}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg saveFacilityDefaultRate(@PathVariable(value = "facilityType") String facilityType,
			@RequestBody FacilityUtilizationRateListDto facilityUtilizationRateListDto) {
		logger.info("FacilityController.saveFacilityDefaultRate() invocation start...");
		boolean flag = checkFacilityDefaultRateData(facilityType, facilityUtilizationRateListDto);
		if (!flag)
			return responseResult;
		try {
			facilityUtilizationRateListDto.setFacilityType(facilityType.toUpperCase());
			facilityUtilizationRateTimeService.saveFacilityUtilizationRateTime(facilityUtilizationRateListDto,
					this.getUser().getUserId());

			logger.info("FacilityController.saveFacilityDefaultRate() invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
			return responseResult;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	private boolean checkFacilityDefaultRateData(String facilityType,
			FacilityUtilizationRateListDto facilityUtilizationRateListDto) {
		if (StringUtils.isEmpty(facilityType) || null == facilityUtilizationRateListDto) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return false;
		} else {
			if (null == facilityUtilizationRateListDto.getDayRateList()
					|| facilityUtilizationRateListDto.getDayRateList().size() == 0) {
				responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
				return false;
			}
			for (FacilityUtilizationRateTimeListDto rateTimeListDto : facilityUtilizationRateListDto.getDayRateList()) {
				if (StringUtils.isEmpty(rateTimeListDto.getWeekDay())) {
					responseResult.initResult(GTAError.FacilityError.FACILITYTYPE_ISNULL);
					return false;
				}
				if (rateTimeListDto.getRateList() == null || rateTimeListDto.getRateList().size() == 0) {
					responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
					return false;
				} else {
					for (FacilityUtilizationRateTimeDto rateTimeDto : rateTimeListDto.getRateList()) {
						if (rateTimeDto.getBeginTime() == null || rateTimeDto.getBeginTime() == 0) {
							responseResult.initResult(GTAError.FacilityError.STARTTIME_ISNULL);
							return false;
						}
						if (StringUtils.isEmpty(rateTimeDto.getRateType())) {
							responseResult.initResult(GTAError.FacilityError.RATETYPE_ISNULL);
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * get ball Feeding Machine List
	 * 
	 * @return facility list
	 */
	@RequestMapping(value = "/ballFeedingMachine", method = RequestMethod.GET)
	@ResponseBody
	public ResponseResult getBallFeedingMachineList() {
		logger.info("FacilityController.getBallFeedingMachineList start ...");
		try {
			List<FacilityMasterQueryDto> facilityMasterList = facilityMasterService.getFacilityMasterList("GOLF");
			if (facilityMasterList == null || facilityMasterList.size() == 0) {
				responseResult.initResult(GTAError.FacilityError.SUCCESS);
				return responseResult;
			}

			// { TODO: refresh golf feeding machine status from remote Zoomtech
			// PC (SAMHUI 20160315 added)
			Map<Long, BatInfo> batInfMap = null;
			boolean hasMiddleWareError = false;
			try {
				batInfMap = golfMachineService.getAllBatInfosMap();
			} catch (Exception e) {
				logger.error(e.getMessage());
				// if the middleware is not running that no status get from the
				// middleware, treat them error and the bay should be grey.
				hasMiddleWareError = true;
			}
			// }

			List<FacilityMasterQueryDto> listData = null;
			Map<Long, List<FacilityMasterQueryDto>> retMap = new HashMap<Long, List<FacilityMasterQueryDto>>();
			for (FacilityMasterQueryDto facility : facilityMasterList) {
				if (null == facility.getVenueFloor() || facility.getVenueFloor() == 3 || facility.getVenueFloor() == 99)
					continue;
				listData = retMap.get(facility.getVenueFloor());
				if (null == listData) {
					listData = new ArrayList<FacilityMasterQueryDto>();
					retMap.put(facility.getVenueFloor(), listData);
				}
				listData.add(facility);

				// { TODO: refresh golf feeding machine status from remote
				// Zoomtech PC (SAMHUI 20160315 added)
				try {
					if (batInfMap != null && !batInfMap.isEmpty()) {
						// if
						// (GolfMachineService.IS_ON.contains(batInfMap.get(facility.getFacilityNo()).getBatStat().toString()
						// ) )
						if (batInfMap.get(facility.getFacilityNo()).getErrorNo() != 0)
							facility.setBallFeedingStatus("ERROR");
						else if (golfBayStatusON
								.contains(batInfMap.get(facility.getFacilityNo()).getBatStat().toString()))
							facility.setBallFeedingStatus("ON");
						else
							facility.setBallFeedingStatus("OFF");
					}
					// if no status get from the middleware(maybe no data or any
					// exception),treat them error and the bay should be grey.
					else {
						facility.setBallFeedingStatus("ERROR");
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
				// }

			}
			List<Object> resultData = new ArrayList<Object>();
			Map<String, Object> resultMap = null;
			Iterator<Entry<Long, List<FacilityMasterQueryDto>>> it = retMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Long, List<FacilityMasterQueryDto>> entry = it.next();
				resultMap = new HashMap<String, Object>();
				resultMap.put("venueFloor", entry.getKey());
				resultMap.put("facilityList", entry.getValue());
				resultData.add(resultMap);
			}
			logger.info("FacilityController.getBallFeedingMachineList invocation end...");
			if (hasMiddleWareError) {
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION, resultData);
			} else {
				responseResult.initResult(GTAError.FacilityError.SUCCESS, resultData);
			}
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}

	/**
	 * save ballFeedingMachine
	 * 
	 * @param facilityType
	 * @param facilityUtilizationRateDateDtos
	 * @return
	 */
	@RequestMapping(value = "/ballFeedingMachine", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseMsg saveBallFeedingMachine(@RequestParam(value = "facilityNo", required = true) int facilityNo,
			@RequestParam(value = "ballFeedingStatus", required = true, defaultValue = "OFF") String ballFeedingStatus) {
		logger.info("FacilityController.saveBallFeedingMachine() invocation start...");
		if (StringUtils.isEmpty(facilityNo)) {
			responseResult.initResult(GTAError.FacilityError.FACILITYNO_ISNULL);
			return responseResult;
		}
		if (StringUtils.isEmpty(ballFeedingStatus)) {
			responseResult.initResult(GTAError.FacilityError.STATUS_ISNULL);
			return responseResult;
		}
		try {
			String factorySerialNo = facilityMasterService.getFacilityMasterbyFacilityNo(facilityNo)
					.getFactorySerialNo();
			Long factorySerialNumber = null;
			if (StringUtils.isEmpty(factorySerialNo)) {
				responseResult.initResult(GTAError.FacilityError.BALLFEEDINGMACHINE_ISNULL);
				return responseResult;
			}
			factorySerialNumber = Long.parseLong(factorySerialNo);
			/*
			 * SAMHUI 20160315 changed begnin: replaced by new method using http
			 * webservice instead of Jackcess
			 */
			// File accessFile = AccessDatabaseUtil.getAccessFile();
			// if (!accessFile.exists() || accessFile.isDirectory()) {
			// responseResult.initResult(GTAError.FacilityError.ACCESSDATABASE_ISNULL);
			// return responseResult;
			// }
			// AccessDatabaseUtil.changeTeeupMachineStatsbyBatNo(factorySerialNumber,
			// ballFeedingStatus, accessFile);

			golfMachineService.changeTeeupMachineStatsbyBatNo(factorySerialNumber, ballFeedingStatus);
			/* end */

			facilityAdditionAttributeService.saveFacilityAdditionAttribute(facilityNo, ballFeedingStatus);
			logger.info("FacilityController.saveBallFeedingMachine() invocation end...");
			responseResult.initResult(GTAError.FacilityError.SUCCESS);
			return responseResult;

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}
	}
	
	
	/**
	 * 批量更改网球/高尔夫球场地状态
	 * 
	 * @param facilityItemStatusDto
	 *            the data transfer object. Need to provide facility_timeslot_id
	 *            or facilityNo and begin_datetime.
	 * @return responseResult ResponseResult based on the input.
	 */
	@RequestMapping(value = "/saveFacilityStatusBatch", method = RequestMethod.PUT)
	public @ResponseBody ResponseResult saveFacilityStatusBatch(@RequestBody FacilityItemStatusDto facilityItemStatusDto) {
		logger.info("FacilityController.saveFacilityStatusBatch start ...");

		if (null == facilityItemStatusDto) {
			responseResult.initResult(GTAError.FacilityError.REQUESTBODY_ISNULL);
			return responseResult;
		} else {
			if (facilityItemStatusDto.getBeginFacilityNo() < 1 || facilityItemStatusDto.getEndFacilityNo() < 1 || facilityItemStatusDto.getBeginFacilityNo() > facilityItemStatusDto.getEndFacilityNo()) {
				responseResult.initResult(GTAError.FacilityError.FACILITYNO_ISNULL);
				return responseResult;
			}
			if (StringUtils.isEmpty(facilityItemStatusDto.getBeginDate())) {
				responseResult.initResult(GTAError.FacilityError.BEGINEND_ISNULL);
				return responseResult;
			}
			if (StringUtils.isEmpty(facilityItemStatusDto.getStatus())) {
				responseResult.initResult(GTAError.FacilityError.STATUS_INVALID);
				return responseResult;
			} else {
				boolean flag = false;
				FacilityStatus[] facilityStatuss = FacilityStatus.values();
				for (FacilityStatus status : facilityStatuss) {
					if (status.toString().equals(facilityItemStatusDto.getStatus()))
						flag = true;
				}
				if (!flag) {
					responseResult.initResult(GTAError.FacilityError.STATUS_INVALID);
					logger.info("FacilityController.saveFacilityStatusBatch end ...");
					return responseResult;
				}
			}
		}
		LoginUser user = super.getUser();
		if (user == null) {

			logger.error("Can not get current user!");
			responseResult.initResult(GTAError.FacilityError.USER_ISNULL);
			return responseResult;
		}
		facilityItemStatusDto.setUpdateBy(user.getUserId());
		try {
			facilityItemStatusDto.setBeginDatetime(DateCalcUtil.parseDate(facilityItemStatusDto.getBeginDate()));
			responseResult = facilityMasterService.saveFacilityStatusBatch(facilityItemStatusDto);
			return responseResult;
		} catch (GTACommonException e) {
			logger.error(e.getMessage(), e);
			if (null != e.getError()) {
				if ((e.getError().getCode()).equals(GTAError.FacilityError.DATETIME_LESSTHAN_TODAY.getCode())) {
					responseResult.initResult(e.getError(), e.getArgs());
				} else {
					responseResult.initResult(e.getError());
				}
			} else
				responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseResult.initResult(GTAError.FacilityError.UNEXPECTED_EXCEPTION);
			return responseResult;
		}

	}

}
