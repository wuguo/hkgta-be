package com.sinodynamic.hkgta.application;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.google.common.collect.Lists;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.configuration.Swagger2DocumentationConfiguration;

@Import({Swagger2DocumentationConfiguration.class})
class SwaggerConfig {
	
    @Bean
    public Docket api(){
    	   	
        return new Docket(DocumentationType.SWAGGER_2).pathMapping("/rest")
        .securitySchemes(Lists.newArrayList(new ApiKey("token", "api_key", "header") ) )
        .select()
        .build()
        .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "HKGTA REST API", 
                "HKGTA REST API.", 
                "API TOS",
                "www.sinodynamic.com",
                "Sinodynamic solution Ltd", 
                "", 
                "");
        
    }
}


@Configuration
class WebAppConfig extends WebMvcConfigurerAdapter {

    @Override 
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

}