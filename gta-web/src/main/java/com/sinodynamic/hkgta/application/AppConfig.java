package com.sinodynamic.hkgta.application;

import org.apache.log4j.Logger;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class AppConfig {
	public static final Logger logger = Logger.getLogger(AppConfig.class);
    AppConfig() {
      logger.debug("start AppConfig");    	
    }
}
