package com.sinodynamic.hkgta.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.service.adm.StaffMasterService;

@Deprecated
@Component
public class ChangeExpiredStaffMasterStatus {
	
	private Logger logger = Logger.getLogger(ChangeExpiredStaffMasterStatus.class);
	
	@Autowired
	StaffMasterService staffMasterService;
	
	@Scheduled(cron="0 0/2 * * * ?")
	public void updateServicePlan() {
		try{
			logger.info("Updating service plan");
			staffMasterService.changeExpiredStaffMasterStatus();
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
	}
}
