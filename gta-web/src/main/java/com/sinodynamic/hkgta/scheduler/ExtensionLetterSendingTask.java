package com.sinodynamic.hkgta.scheduler;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.entity.crm.CustomerEmailContent;
import com.sinodynamic.hkgta.entity.crm.CustomerProfile;
import com.sinodynamic.hkgta.entity.crm.CustomerServiceAcc;
import com.sinodynamic.hkgta.entity.crm.MessageTemplate;
import com.sinodynamic.hkgta.service.crm.sales.CustomerEmailContentService;
import com.sinodynamic.hkgta.service.crm.sales.leads.CustomerProfileService;
import com.sinodynamic.hkgta.service.crm.sales.renewal.CustomerServiceAccService;
import com.sinodynamic.hkgta.service.crm.sales.template.MessageTemplateService;
import com.sinodynamic.hkgta.util.MailSender;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailStatus;

@Deprecated
@Component
public class ExtensionLetterSendingTask {
	
	private Logger logger = Logger.getLogger(ExtensionLetterSendingTask.class);
	
	private static final String EXTENSION_LETTER = Constant.TEMPLATE_ID_EXTENSION;
	
	@Autowired
	private CustomerEmailContentService customerEmailContentService;
	
	@Autowired
	private MessageTemplateService messageTemplateService;
	
	@Autowired
	private CustomerProfileService customerProfileService;
	
	@Autowired
	private CustomerServiceAccService customerServiceAccService;
	
	public void autoSendingJob() {
		
		logger.info("ExtensionLetterSendingController.autoSendingJob auto execute start ...");
		
		try {
			
			List<CustomerServiceAcc> customers = customerServiceAccService.getExpiringCustomers();
			if (customers != null && customers.size() > 0 ) {
				
				MessageTemplate template = getMessageTemplate();
				if (template == null) {
					logger.error("Failed to find template type extension");
					return;
				}
				
				for (CustomerServiceAcc customer : customers) {
					CustomerProfile cp = customerProfileService.getById(customer.getCustomerId());
					sendExtensionEmail(cp, template);
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return;
		}

		logger.info("ExtensionLetterSendingController.autoSendingJob auto execute end ...");
	}

	
	private String getLetterContent(CustomerProfile customer, MessageTemplate mt) throws Exception {
		
		if (customer == null || mt == null) return null;
		String content = mt.getContentHtml();
		StringBuilder named = new StringBuilder();
		named.append(customer.getSalutation())
		.append(" ")
		.append(customer.getGivenName())
		.append(" ")
		.append(customer.getSurname());
		return content.replace(Constant.PLACE_HOLDER_TO_CUSTOMER, named.toString()).replace(Constant.PLACE_HOLDER_FROM_USER, Constant.HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER);
	}
	
	
	private boolean sendExtensionEmail(CustomerProfile customer, MessageTemplate mt) throws Exception {
		
		if (customer == null || mt == null) return false;
		String emailTitle = mt.getMessageSubject();
		String emailBody = getLetterContent(customer, mt);
		CustomerEmailContent cec = customerEmailContentService.sendExtensionLetter(customer.getCustomerId(), emailTitle, emailBody);
		boolean success = MailSender.sendEmail(customer.getContactEmail(), null, null, emailTitle, emailBody, null);
		if (success) {
			cec.setStatus(EmailStatus.SENT.getName());
		} else {
			cec.setStatus(EmailStatus.FAIL.getName());
		}
		
		customerEmailContentService.modifyCustomerEmailContent(cec);
		return true;
	}
	
	
	private MessageTemplate getMessageTemplate() throws Exception {
		
		return messageTemplateService.getTemplateByFuncId( EXTENSION_LETTER);
	}
}
