package com.sinodynamic.hkgta.controller.fms;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import com.sinodynamic.hkgta.security.AuthUserDetailService;
import com.sinodynamic.hkgta.util.JsonUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.response.ResponseResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/config/applicationContext.xml" })
@WebAppConfiguration
@TestExecutionListeners({ 
	DependencyInjectionTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class,
	SpecialRateControllerTest.class })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
public class SpecialRateControllerTest extends AbstractTestExecutionListener
{
	Logger logger = LoggerFactory.getLogger(SpecialRateControllerTest.class);
	@Autowired
	ApplicationContext applicationContext;

	private MockMvc mockMvc;
	
	@Mock
	private HttpServletRequest request;
	
	@Autowired
	@InjectMocks
	private SpecialRateController specialRateController;
	
	@Mock
	private AuthUserDetailService userDetailsService;
	
	@Before
	public final void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(specialRateController)
				.build();
	}
	
	/**
	 * get Facility General Price
	 * @throws Exception 
	 * 
	 */
	@Test
	@Transactional
	public void getFacilitySpecialRateGolf() throws Exception
	{
		
		MvcResult result = this.mockMvc
				.perform(
						get("/facility/specialRateOfDay/golf/2015-06-24")
								.header("Accept-Language", "En")
								.header("token", "TEST")
								.contentType(
										MediaType
												.parseMediaType("application/json;charset=UTF-8"))
								)

				.andExpect(status().isOk())
				.andExpect(
						content().contentType("application/json;charset=UTF-8"))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		if (response!=null) {
			logger.info("The response " + response);
		}else{
			logger.info("response is null");
		}
		Assert.notNull(response);
		ResponseMsg responseResult = JsonUtil.fromJson(response, ResponseMsg.class);
		Assert.isTrue(responseResult.returnCode.equalsIgnoreCase("0"));
		Assert.isTrue(responseResult.getErrorMessageEN().equalsIgnoreCase("SUCCESS"));
		//Assert.isTrue(responseResult.getListData().getList().size()>0);
	}

	@Test
	@Transactional
	public void getFacilitySpecialRateTennis() throws Exception
	{
		
		MvcResult result = this.mockMvc
				.perform(
						get("/facility/specialRateOfDay/tennis/2015-06-24?subType=TENNISCRT-STD")
								.header("Accept-Language", "En")
								.header("token", "TEST")
								.contentType(
										MediaType
												.parseMediaType("application/json;charset=UTF-8"))
								)

				.andExpect(status().isOk())
				.andExpect(
						content().contentType("application/json;charset=UTF-8"))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		if (response!=null) {
			logger.info("The response " + response);
		}else{
			logger.info("response is null");
		}
		Assert.notNull(response);
		ResponseMsg responseResult = JsonUtil.fromJson(response, ResponseMsg.class);
		Assert.isTrue(responseResult.returnCode.equalsIgnoreCase("0"));
		Assert.isTrue(responseResult.getErrorMessageEN().equalsIgnoreCase("SUCCESS"));
		//Assert.isTrue(responseResult..getList().size()>0);
	}
}
