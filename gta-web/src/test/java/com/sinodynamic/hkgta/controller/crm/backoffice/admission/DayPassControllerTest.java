package com.sinodynamic.hkgta.controller.crm.backoffice.admission;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.sinodynamic.hkgta.util.JsonUtil;
import com.sinodynamic.hkgta.util.ResponseMsg;

import org.springframework.util.Assert;

/**
 * //TODO Eric - 2016年1月27日
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-test.xml" })
@WebAppConfiguration
public class DayPassControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private DayPassController dayPassController;

	@Before
	public final void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(dayPassController).build();
	}

	@Test
	public void getDayPassByQrCode() throws Exception {

		MvcResult result = mockMvc.perform(get("/daypass/DP6").header("token", "TEST")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8")).andReturn();

		String response = result.getResponse().getContentAsString();
		System.out.println(response);

		Assert.notNull(response);

		ResponseMsg responseMsg = JsonUtil.fromJson(response, ResponseMsg.class);
		//Assert.isTrue(responseMsg.returnCode.equalsIgnoreCase("0"));
		//Assert.isTrue(responseMsg.getErrorMessageEN().equalsIgnoreCase("SUCCESS"));

	}
}
