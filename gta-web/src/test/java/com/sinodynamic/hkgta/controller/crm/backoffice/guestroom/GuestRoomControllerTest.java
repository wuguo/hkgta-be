//package com.sinodynamic.hkgta.controller.crm.backoffice.guestroom;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.TestExecutionListeners;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.AbstractTestExecutionListener;
//import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
//import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.Assert;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.sinodynamic.hkgta.controller.crm.backoffice.guestroommanage.GuestRoomController;
//import com.sinodynamic.hkgta.security.AuthUserDetailService;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:/config/applicationContext.xml" })
//@WebAppConfiguration
//@TestExecutionListeners({ 
//	DependencyInjectionTestExecutionListener.class,
//	DirtiesContextTestExecutionListener.class,
//	TransactionalTestExecutionListener.class,
//	GuestRoomControllerTest.class })
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
//public class GuestRoomControllerTest extends AbstractTestExecutionListener {
//    
//    private MockMvc mockMvc;
//	
//	@Mock
//	private HttpServletRequest request;
//	
//	@Autowired
//	@InjectMocks
//	private GuestRoomController guestRoomController;
//	
//	@Mock
//	private AuthUserDetailService userDetailsService;
//	
//	@Before
//	public final void setUp() throws Exception {
//		MockitoAnnotations.initMocks(this);
//
//		mockMvc = MockMvcBuilders.standaloneSetup(guestRoomController)
//				.build();
//		
//		
//	}
//	
//	
//}
