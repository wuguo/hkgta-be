package com.sinodynamic.hkgta.service.common;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/config/applicationContext.xml" })
@WebAppConfiguration
@TestExecutionListeners({ 
	DependencyInjectionTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class,
	DevicePushServiceTest.class })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
@Transactional
public class DevicePushServiceTest extends AbstractTestExecutionListener
{
	@Autowired
	@Qualifier("asynchronizedPushService")
	DevicePushService devicePushService;

	@Before
	public void setUp() throws Exception
	{
	}

	/*@Test
	public void testPushRegister()
	{
	}

	@Test
	public void testDeleteUserDevice()
	{
	}

	@Test
	public void testPushMessage()
	{
	}*/

	@Test
	public void testBroadcastMessage()
	{
		try{
			devicePushService.broadcastMessage("arn:aws:sns:us-east-1:819247114127:HKGTA_HOUSEKEEP_UAT", "unit test message");
		}catch(Exception e){
			Assert.fail("BroadcastMessage test failed " + e.getMessage());
		}
	}

}
