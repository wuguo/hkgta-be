package com.sinodynamic.hkgta.integration.zoomtech.service;


import java.util.Date;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sinodynamic.hkgta.dto.golfmachine.BatInfo;
import com.sinodynamic.hkgta.integration.zoomtech.service.GolfMachineService;
import com.sinodynamic.hkgta.integration.zoomtech.service.GolfMachineServiceImpl;

/*public class GolfMachineTest {

	@Test
	public void testAccess() throws Exception {
		AccessDatabaseUtil.changeTeeupMachineStatsbyBatNo(3l, "ON", null);
		
		Thread.sleep(5000);
		AccessDatabaseUtil.changeTeeupMachineStatsbyBatNo(3l, "OFF", null);
		
	}
	
}*/
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration({"classpath*:/applicationContext-test.xml", "classpath*:/log4j.xml" })
@ContextConfiguration({"classpath:/applicationContext-basic.xml", "classpath*:/config/applicationContext-integration.xml", "classpath*:/config/applicationContext-properties.xml"})
@ComponentScan("com.sinodynamic.hkgta.integration.zoomtech")
//@WebAppConfiguration
public class GolfMachineTest  {
    @Autowired private GolfMachineService golfMachineService; // = new GolfMachineServiceImpl();
	//GolfMachineService golfMachineService = null; //new GolfMachineServiceImpl();
    @Value("#{appProperties['golfmachine.status_on']}") String golfBayStatusON;
	//@Before
	public void before() {
		golfMachineService = new GolfMachineServiceImpl();
		golfMachineService.setRemoteHost("http://localhost:8085");
	}
	
	//@Test
	public void testGolfMachinebyBatNo() throws Exception {
		golfMachineService.changeTeeupMachineStatsbyBatNo(5l, "ON");
		
		Thread.sleep(5000);
		golfMachineService.changeTeeupMachineStatsbyBatNo(5l, "OFF");		
	}
	
	//@Test
	public void testTeeupMachineBytime() throws Exception {
//		golfMachineService.changeTeeupMachineStatsbyBatNo(4l, "ON");
//		
//		Thread.sleep(5000);
//		golfMachineService.changeTeeupMachineStatsbyBatNo(4l, "OFF");
		
		Date startTime=new Date((new Date()).getTime() + 1000 * 1000);
		Date endTime=new Date((new Date()).getTime() + 6000 * 1000);
		
		System.out.println("start time: " + startTime );
		System.out.println("end  time: " + endTime );
		
		golfMachineService.setTeeupMachineStatsbyCheckinTime(7l, startTime, endTime);	
		
	}	
	
	@Test
	public void testGolfMachineInfo() throws Exception {
		System.out.println(golfMachineService.testHost());
		
/*		Long inBatNo=30L;
		List<BatInfo> batInfos=golfMachineService.getBatInfo(inBatNo);
		
		System.out.print("get batInfo " + inBatNo + " = ");
		//System.out.println(batInfos);
		for (BatInfo bi: batInfos) {
			System.out.print("\nbatNo: " +  bi.getBatNo());
			System.out.print("  status: " +  bi.getBatStat());
			System.out.print("  updateDate: " +  bi.getUpdateDate());
		}		
		
		
		batInfos=golfMachineService.getBatInfo(null);
		System.out.println("\n------------\nAll batInfos=");
		//System.out.println(batInfos);
		for (BatInfo bi: batInfos) {
			System.out.print("\nbatNo: " +  bi.getBatNo());
			System.out.print("  status: " +  bi.getBatStat());
			System.out.print("  updateDate: " +  bi.getUpdateDate());
		}
*/		
		

/*		for (long i=1; i < 44; i++) {			
		   BatInfo batInfo=golfMachineService.getBatInfo(i).get(0);
			System.out.print("\nbatNo: " +  batInfo.getBatNo());
			System.out.print("  status: " +  batInfo.getBatStat());
			System.out.print("  updateDate: " +  batInfo.getUpdateDate());
		   
		   if (GolfMachineService.IS_ON.contains(batInfo.getBatStat().toString() ) ) 
			  System.out.println(" is ON");
		   else
			  System.out.println(" is OFF");
		}   
*/		
		Map<Long, BatInfo> batInfMap=golfMachineService.getAllBatInfosMap();
		for (long i=1; i < 44; i++) {
			System.out.print("\nbatNo: " +  batInfMap.get(i).getBatNo());
			System.out.print("  status: " +  batInfMap.get(i).getBatStat());
			System.out.print("  updateDate: " +  batInfMap.get(i).getUpdateDate());
		   
		   if (golfBayStatusON.contains(batInfMap.get(i).getBatStat().toString() ) ) 
			  System.out.println(" is ON");
		   else
			  System.out.println(" is OFF");				
		}
		
	}
	
	
	
}