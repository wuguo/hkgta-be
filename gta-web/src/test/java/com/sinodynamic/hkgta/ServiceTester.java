package com.sinodynamic.hkgta;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.sinodynamic.hkgta.service.crm.sales.enrollment.CustomerEnrollmentService;
import com.sinodynamic.hkgta.util.exception.EnrollmentException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/applicationContext.xml"})
@WebAppConfiguration
public class ServiceTester {

	@Autowired
	private CustomerEnrollmentService customerEnrollmentService;
	
	@Test
	public void testallocateVirtualAccount() throws EnrollmentException {
		//customerEnrollmentService.testallocateVirtualAccount();
	}
}
