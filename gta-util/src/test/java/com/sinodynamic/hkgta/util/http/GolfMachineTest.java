package com.sinodynamic.hkgta.util.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.sinodynamic.hkgta.util.AccessDatabaseUtil;
import com.sinodynamic.hkgta.util.http.HttpClientUtil;

public class GolfMachineTest {

	@Test
	public void testHttp() throws Exception {
		//String result=HttpClientUtil.post("http://localhost:8085/bayInfo", "batNo=1", 12000, 12000, "application/x-www-form-urlencoded");
		String result;

		System.out.println("------------full info--------------------");
		result=HttpClientUtil.get("http://localhost:8085/bayInfo", "");		
		System.out.println("by empty string paramter:" + result);				
		System.out.println("-------------------------------------");


		
		result=HttpClientUtil.get("http://localhost:8085/bayInfo", "batNo=2" );		
		System.out.println("by String param:\n"+result);
		
		
		Map<String,String> param=new HashMap<String,String>();
		param.put("batNo", "1");		
		result=HttpClientUtil.get("http://localhost:8085/bayInfo", param );		
		System.out.println("\n by Map param:\n"+result);
		
//		result=HttpClientUtil.get("http://localhost:8085/bayInfo?batNo=2", "");		
//		System.out.println("by uri params:\n " + result);				
				
   
       // test post
	  
		System.out.println("--------------------------------");
		System.out.println("\n**test incorrect date " );
		result=HttpClientUtil.postForm("http://localhost:8085/bayCmd", "cmd=987,'2016-03-1x 01:49:34', 999, 999, 0, 1");		
		System.out.println(result);						
		
		System.out.println("\n**test incorrect parameter number " );
		result=HttpClientUtil.postForm("http://localhost:8085/bayCmd", "cmd=987,'2016-03-14 01:49:34', 999, 999, 0");		
		System.out.println(result);								
		
		System.out.println("\n**test success cmd=987,'2016-03-14 01:49:34', 999, 999, 0, 1" );
		result=HttpClientUtil.postForm("http://localhost:8085/bayCmd", "cmd=987,'2016-03-14 01:49:34', 999, 999, 0, 1");		
		System.out.println(result);
		
		System.out.println("--------------------------------");
		
	}

	// @Test
	public void testAccess() throws Exception {
		AccessDatabaseUtil.changeTeeupMachineStatsbyBatNo(3l, "ON", null);
		
	}
	
}
