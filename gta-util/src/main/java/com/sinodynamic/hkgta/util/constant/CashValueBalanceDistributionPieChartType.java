package com.sinodynamic.hkgta.util.constant;

public enum CashValueBalanceDistributionPieChartType {
	ONE(" < -5,000",1,0,-5000),
	TWO("-5,000 to 0",2,-5000,0	),
	THREE("0 to 5,000",2, 0,5000),
	FOUR("5,000 to 10,000",2,5000,10000),
	FIVE("10,000 to 15,000",2,10000,15000),
	SIX("15,000 to 20,000",2,15000,20000),
	SEVEN("20,000 to 25,000",2,20000,25000),
	EIGHT("25,000 to 30,000",2,25000,30000),
	NINE(" > 30,000",3,30000,0);
	
	private String code;
	private int conditionsType;
	private int startRateNum;
	private int endRateNum;
	
	public int getStartRateNum() {
		return startRateNum;
	}

	public void setStartRateNum(int startRateNum) {
		this.startRateNum = startRateNum;
	}

	public int getEndRateNum() {
		return endRateNum;
	}

	public void setEndRateNum(int endRateNum) {
		this.endRateNum = endRateNum;
	}

	private CashValueBalanceDistributionPieChartType(String code, int conditionsType, int startRateNum, int endRateNum)
	{
		this.code = code;
		this.conditionsType = conditionsType;
		this.startRateNum = startRateNum;
		this.endRateNum = endRateNum;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getConditionsType() {
		return conditionsType;
	}

	public void setConditionsType(int conditionsType) {
		this.conditionsType = conditionsType;
	}

	

	
	
}
