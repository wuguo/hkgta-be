package com.sinodynamic.hkgta.util.constant;

public enum AccessBatCmdType {
	
	None(0), Checkin(1), Finish(2);
	
	private int cmd;
	
	private AccessBatCmdType(int i){
		this.cmd = i;
	}
	
	public int getCmd(){
		return this.cmd;
	}
	

}
