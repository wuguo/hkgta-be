package com.sinodynamic.hkgta.util.constant;

public enum ServicePlanRight {
    
    GOLF_COURSE("TRAIN1"),
    GOLF_COACHING("TRAIN2"),
    TENNIS_COURSE("TRAIN3"),
    TENNIS_COACHING("TRAIN4");
    
    private String value;
    private ServicePlanRight(String value) {
	this.value = value;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    
}
