package com.sinodynamic.hkgta.util.constant;

public enum PaymentCmdType {
	PAY("pay"),QUERYDR("querydr"),REFUND("refund"),VOIDPURCHASE("voidPurchase"),CAPTURE("capture");
	private String desc;

	private PaymentCmdType(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
