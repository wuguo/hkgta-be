package com.sinodynamic.hkgta.util.constant;

public enum ServicePlanOfferCode {
	
	
	
	RENEW("RENEW");
	
	private String desc;

	private ServicePlanOfferCode(String d) {
		this.desc = d;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
