package com.sinodynamic.hkgta.util.constant;

public enum PeriodType {
	FULL("Any Days"),WD("Weekdays"),Y("Year"),M("Month"),D("Day");

	private PeriodType(String desc) {
		this.desc = desc;
	}

	private String desc;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return super.name();
	}

}
