package com.sinodynamic.hkgta.util.constant;

public enum PaymentGatewayProvider {
	FIRSTDATA("First Data"),AMEX("American Express");
	
	private String desc;
	
	private PaymentGatewayProvider(String desc) {	
		this.desc = desc;	
	}
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
