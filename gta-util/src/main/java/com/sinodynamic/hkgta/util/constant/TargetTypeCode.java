package com.sinodynamic.hkgta.util.constant;

public enum TargetTypeCode {

	MBR("Member"), HKC("Housekeep Crew"), STF("All Staffs (reserved)");

	private String desc;

	private TargetTypeCode(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return super.name();
	}

}
