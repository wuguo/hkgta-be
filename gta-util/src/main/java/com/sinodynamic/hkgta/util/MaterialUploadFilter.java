package com.sinodynamic.hkgta.util;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class MaterialUploadFilter extends AbstractUploadFilter{
	public final static String AllOW_MATERIAL_UPLOAD_TYPE = "allow.material.upload.type";
//	@Resource(name="appProperties") 
//	protected Properties appProps; 
//	String allowTypeStr = appProps.getProperty(AllOW_MATERIAL_UPLOAD_TYPE);
	String allowTypeStr = "JPEG,JPG,PNG,MP4,AVI,MOV,M4V";
	String[] includes = allowTypeStr.split(",");
	Set<String> allowTypes = new HashSet<String>();
	MaterialUploadFilter() {
		for(String str : includes){
			allowTypes.add(str);
		}
	}
	@Override
	public void filter(String originalFilename) {
		String suffix = getSuffix(originalFilename);
		if(!allowTypes.contains(suffix.toUpperCase())){
			throw new RuntimeException(suffix + " is not allowed.");
		}
	}

}
