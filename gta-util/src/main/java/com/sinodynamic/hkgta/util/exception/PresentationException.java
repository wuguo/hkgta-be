package com.sinodynamic.hkgta.util.exception;


public class PresentationException extends Exception {

	private static final long serialVersionUID = -8822661861522994147L;
	
	private String errorCode;
	
	private String errorMessage;
	

	public PresentationException(){}
	
	public PresentationException(Throwable e){
		super(e);
	}
	
	public PresentationException(String errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	
	public PresentationException(String errorCode,String errorMessage,Throwable e){
		super(e);
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	
}
