package com.sinodynamic.hkgta.util.constant;

public enum CalendarWeekCode {
	
	Sunday("Sunday"), Monday("Monday"), Tuesday("Tuesday"), Wednesday("Wednesday"), Thursday("Wednesday"), Friday("Friday"), Saturday("Saturday");

		private String desc;

		private CalendarWeekCode(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

