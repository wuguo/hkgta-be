package com.sinodynamic.hkgta.util.constant;

public enum PayMentOrderSearchType {
	LIST("list"), DETAIL("detail");
	private String desc;

	private PayMentOrderSearchType(String d) {
		this.desc = d;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
