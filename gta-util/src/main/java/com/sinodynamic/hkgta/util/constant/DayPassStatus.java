package com.sinodynamic.hkgta.util.constant;
/***
 * DayPassScannerStatus 
 * @author christ
 *
 */
public enum DayPassStatus {
	/***
	 * 1. Pending for Redeem(PND) - The start of the Valid Date is after Current Date
	   2. Ready for Redeem(Ready) - The current date is within the range of valid date 
	   3. Active(ACT) - The current date is within the range of valid date
	   4. Expired(EXP) - The current date is after the end of the valid date
	 */
	EXP("Expired"), ACT("Active"), Ready("Ready for Redeem"), PND("Pending for Redeem"),INVD("invalid card");

	private String desc;

	private DayPassStatus(String desc) {
		this.desc = desc;
	}
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
