package com.sinodynamic.hkgta.util.constant;

public enum StaffFacilityScheduleStatus {
	
	ATN("ATN"), NAT("NAT"), OSA("OSA");

		private String desc;

		private StaffFacilityScheduleStatus(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

