package com.sinodynamic.hkgta.util.constant;

public enum CarParkCSVRowType {
	HEADER("H"), DETAIL("D"), FOOTER("F");
	
	private String rowType;
	
	private CarParkCSVRowType(String s){
		this.rowType = s;
	}
	
	public String getRowType(){
		return this.rowType;
	}
}
