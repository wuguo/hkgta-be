package com.sinodynamic.hkgta.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
public class Dom4jUtils {
	private static final Logger LOGGER = Logger.getLogger(Dom4jUtils.class);
    public static final String CRLF = "\r\n";
    public static Document parseText(String text) {
        try {
            if (text==null||"".equals(text)) {
                return null;
            } else {
                return DocumentHelper.parseText(text.trim());
            }
        } catch (Exception ex) {
        	LOGGER.error("DocumentHelper", ex);
        }
        return null;

    }
    public static String getAttrVal(Element e,String attrName){
    	String val="";
    	try {
    		val=e.attribute(attrName).getValue();
			if(val.equals("1899-12-30"))
				val="";	
		} catch (Exception e1) {
			val="";
		}
    	return val;
    	
    }
    public static String getText(Element e, String nodename) {
        String text = "";
        try {
        	
            text = e.element(nodename).getTextTrim();
            if(text.equals("1899-12-30")){
            	text="";
            }
        } catch (Exception Ex) {
        	LOGGER.error("DocumentHelper", Ex);
            text = "";
        }
        return text;
    }

    public static String getTextEx(Element e, String nodename) {
        String text = "";
        try {
            text = e.element(nodename).getTextTrim();
            if (text.length() == 0)
                return null;
        } catch (Exception Ex) {
        	LOGGER.error("DocumentHelper", Ex);
            return null;
        }
        return text;
    }

    public static int getIntValue(Element e, String nodename, int defValue) {
        String text = getText(e, nodename);
        if (text==null||"".equals(text)) {
            return defValue;
        }
        try {
            return Integer.parseInt(text);
        } catch (Exception exception) {
        	LOGGER.error("DocumentHelper", exception);
            return defValue;
        }
    }

    public static byte[] getBytes(Element e, String name) {
        String s = getText(e, name);
        return s.getBytes();
    }

    public static Element getChild(Element e, String name) {
        try {
            return e.element(name);
        } catch (Exception Ex) {
        	LOGGER.error("DocumentHelper", Ex);
            return null;
        }
    }

    public static long getLongValue(Element e, String nodename, long defValue) {
        String text = getText(e, nodename);
        if (text==null||"".equals(text)) {
            return defValue;
        }
        try {
            return Long.parseLong(text);
        } catch (Exception exception) {
        	LOGGER.error("DocumentHelper", exception);
            return defValue;
        }
    }

    public static double getDoubleValue(Element e, String nodename,
            double defValue) {
        String text = getText(e, nodename);
        if (text==null||"".equals(text)) {
            return defValue;
        }
        try {
            return Double.parseDouble(text);
        } catch (Exception exception) {
            return defValue;
        }
    }

    public static float getFloatValue(Element e, String nodename, float defValue) {
        String text = getText(e, nodename);
        if (text==null||"".equals(text)) {
            return defValue;
        }
        try {
            return Float.parseFloat(text);
        } catch (Exception exception) {
            return defValue;
        }
    }

    public static boolean getBooleanValue(Element e, String nodename,
            boolean defValue) {
        String text = getText(e, nodename);
        if (text==null||"".equals(text)) {
            return defValue;
        }
        try {
            return Boolean.parseBoolean(text);
        } catch (Exception exception) {
            return defValue;
        }
    }

    public static short getShortValue(Element e, String nodename, short defValue) {
        String text = getText(e, nodename);
        if (text==null||"".equals(text)) {
            return defValue;
        }
        try {
            return Short.parseShort(text);
        } catch (Exception exception) {
            return defValue;
        }
    }
    
    public static Date getDateValue(Element e, String nodename,Date defValue) {
        String text = getText(e, nodename);
        if (text==null||"".equals(text)) {
            return defValue;
        }
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(text);
        } catch (Exception exception) {
            return defValue;
        }
    }

    public static Date getDateTimeValue(Element e, String nodename,Date defValue) {
        String text = getText(e, nodename);
        if (text==null||"".equals(text)) {
            return defValue;
        }
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(text);
        } catch (Exception exception) {
            return defValue;
        }
    }
    
    static public String compose(String tag, String text) {

        return compose(tag, text, "", "",false, false);
    }

    static public String compose(String tag, Integer val) {
        String text = "";
        if (val != null)
            text = text.valueOf(val);
        return compose(tag, text, "", "", false,false);
    }

    static public String compose(String tag, String text, boolean newLineTag) {
        return compose(tag, text, "", "", false,newLineTag);
    }
    /**
     * 
     * @param tag
     * @param text
     * @param namespace
     * @param attributeText
     * @param alowEmptyTag
     * @param newLineTag
     * @return
     */
    static public String compose(String tag, String text, String namespace,
            String attributeText,boolean alowEmptyTag, boolean newLineTag) {
        /* avoid null exception */
        if (tag == null)
            tag = new String("");
        if (namespace == null)
            namespace = new String("");
        if (attributeText == null)
            attributeText = new String("");
        if (text == null)
            text = new String("");
        if ((text.length() == 0 && attributeText.length() == 0) )
//              || tag.length() == 0
                {
            if(tag.length()!=0&&alowEmptyTag){
                if (namespace.length() > 0) {
                    tag = namespace + ":" + tag;
                }
                return "<"+tag+"/>";
            }
            return "";// 空字段
        }
        /* trim the string */
        tag = tag.trim();
        namespace = namespace.trim();
        text = text.trim();
        attributeText = attributeText.trim();
        /* tag is empty,so return text */
        if (tag.length() == 0)
            return text;
        StringBuffer buffer = new StringBuffer();
        /* combine the namespace and tag in [ns]:[tag] format */
        if (namespace.length() > 0) {
            tag = namespace + ":" + tag;
        }
        buffer.append("<"); /* begin element start tag */
        buffer.append(tag);
        /* if has attribules(as a whole string),append it */
        if (attributeText.length() > 0) {
            buffer.append(" ");
            buffer.append(attributeText);
        }
        if (text.length() == 0) {
            buffer.append("/>");
            return buffer.toString();
        } else {
            buffer.append(">");
        }
        /* if force newline beteween tag and text */
        if (newLineTag)
            buffer.append(CRLF);
        /* end element start tag */
        buffer.append(text); /* append the element text */
        if (newLineTag)
            buffer.append(CRLF);
        buffer.append("</"); /* start element end tag */
        buffer.append(tag);
        buffer.append(">");
        buffer.append(CRLF);
        String s = new String(buffer);
        return s;
    }

    
    public static Map<String, Object> Dom2Map(Document doc){  
        Map<String, Object> map = new HashMap<String, Object>();  
        if(doc == null)  
            return map;  
        Element root = doc.getRootElement();  
        for (Iterator iterator = root.elementIterator(); iterator.hasNext();) {  
            Element e = (Element) iterator.next();  
            //System.out.println(e.getName());  
            List list = e.elements();  
            if(list.size() > 0){  
                map.put(e.getName(), Dom2Map(e));  
            }else  
                map.put(e.getName(), e.getText().trim());  
        }  
        return map;  
    }  
      
  
    public static Map Dom2Map(Element e){  
        Map map = new HashMap();  
        List list = e.elements();  
        if(list.size() > 0){  
            for (int i = 0;i < list.size(); i++) {  
                Element iter = (Element) list.get(i);  
                List mapList = new ArrayList();  
                  
                if(iter.elements().size() > 0){  
                    Map m = Dom2Map(iter);  
                    if(map.get(iter.getName()) != null){  
                        Object obj = map.get(iter.getName());  
                        if(!obj.getClass().getName().equals("java.util.ArrayList")){  
                            mapList = new ArrayList();  
                            mapList.add(obj);  
                            mapList.add(m);  
                        }  
                        if(obj.getClass().getName().equals("java.util.ArrayList")){  
                            mapList = (List) obj;  
                            mapList.add(m);  
                        }  
                        map.put(iter.getName(), mapList);  
                    }else  
                        map.put(iter.getName(), m);  
                }  
                else{  
                    if(map.get(iter.getName()) != null){  
                        Object obj = map.get(iter.getName());  
                        if(!obj.getClass().getName().equals("java.util.ArrayList")){  
                            mapList = new ArrayList();  
                            mapList.add(obj);  
                            mapList.add(iter.getText());  
                        }  
                        if(obj.getClass().getName().equals("java.util.ArrayList")){  
                            mapList = (List) obj;  
                            mapList.add(iter.getText());  
                        }  
                        map.put(iter.getName(), mapList);  
                    }else  
                        map.put(iter.getName(), iter.getText());  
                }  
            }  
        }else  
            map.put(e.getName(), e.getText());  
        return map;  
    }  
}
