package com.sinodynamic.hkgta.util.constant;

public enum EmailStatus {

	SENT("Sent"), REJ("Rejected"), CAN("Canceled"), FAIL("Fail"),PND("Pending");

	private String desc;

	private EmailStatus(String d) {
		this.desc = d;
	}

	public String getName() {
		return this.name();
	}

	public String getDesc(){
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

}
