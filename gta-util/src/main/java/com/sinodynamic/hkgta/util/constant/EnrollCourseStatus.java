package com.sinodynamic.hkgta.util.constant;

/**
 * 
 * @author Nick_Xiong
 *
 */
public enum EnrollCourseStatus {

	ACT("ACT"), REG("REG"), PND("PND"), REJ("REJ");

	private String desc;

	private EnrollCourseStatus(String status) {
		this.desc = status;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
