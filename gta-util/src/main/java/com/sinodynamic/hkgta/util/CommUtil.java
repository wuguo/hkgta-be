package com.sinodynamic.hkgta.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.DigestUtils;

import com.sinodynamic.hkgta.util.constant.AgeRangeCode;
import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.Constant.PaymentMethodCode;
import com.sun.mail.util.BASE64DecoderStream;

import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;


public class CommUtil {
     
    private final static String regexHKID = "[A-Z]{1,2}[0-9]{6}[A0-9]{1}";//Follow the HKID format
 	private final static String regexVISA = "[A-Za-z0-9]*";
 	private final static String regexEmail = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
 	private final static String YYYY_MM_dd = "yyyy-MM-dd";
 	private final static int academyNoLength = 7;
 	private final static String regexPhoneNo = "[+]{0,1}[0-9]{8,}";
 	
	private final static String CONTENT_TYPE_PLAIN = "text/plain;charset=utf-8";
 	private final static String CONTENT_TYPE_HTML = "text/html;charset=utf-8";
 	
 	/**
 	 * Method used to validate the HKID's format and HKID's check digit
 	 * @param passportNO
 	 * @return
 	 */
 	public static boolean validateHKID(String passportNo){
 		if (passportNo.matches(regexHKID)&&HKIDAlgContentValidate(passportNo)){
 			return true;
 		}else{
 			return false;
 		}
 	 }

 	public static boolean validatePhoneNo(String phoneNo){
 		if(phoneNo.matches(regexPhoneNo)){
 			return true;
 		}else{
 			return false;
 		}
 	}
 	
 	public static String getDuplicateName(String originName)
	{
		String duplicateName = "";
		String cnt = originName.replaceAll("[\\w\\W]+[_copy]+[(]*", "").replaceAll("[)]*", "");
		if (!originName.contains("_copy"))
		{
			duplicateName = originName + "_copy";
		}
		else
		{
			if (StringUtils.isEmpty(cnt))
			{
				cnt = "0";
				originName = originName + "(" + cnt + ")";
			}
			Integer index = Integer.parseInt(cnt) + 1;
			duplicateName = originName.replaceAll("[_copy(]+[\\d]*[)]+", "_copy(" + index.toString() + ")");
		}
		return duplicateName;
	}
 	
	public static String getPrefixName(String originName)
	{
		String preFixName = "";
		if (originName.contains("_copy"))
		{
			preFixName = originName.replaceAll("_copy[(]*[\\d]*[)]*", "") + "_copy";
		}
		else
		{
			return originName;
		}
		return preFixName;
	}
 	
 	/**
 	 * Method used to validate the HKID's check digit
 	 * @param hkid
 	 * @return
 	 */
	public static boolean HKIDAlgContentValidate(String hkid) {
		String prefix = null;
		String serial = null;
		String checkdigit = null;
		long value = 0;
		if(hkid.length()==8){
			prefix = hkid.substring(0,1);
			serial = hkid.substring(1,7);
			checkdigit = hkid.substring(7,8);
		} else if(hkid.length()==9){
			prefix = hkid.substring(0,2);
			serial = hkid.substring(2,8);
			checkdigit = hkid.substring(8,9);
		}
		if(StringUtils.isNotBlank(prefix)){
			String prefixU = prefix.toUpperCase();
			
			if (prefixU.length() == 2) {
				value += (prefixU.charAt(0) - 55) * 9 + (prefixU.charAt(1) - 55)
						* 8;
			} else if (prefixU.length() == 1) {
				value += 36 * 9 + (prefixU.charAt(0) - 55) * 8;
			}
			
			for (int i = 0; i < 6; i++) {
				value += Integer.parseInt(serial.substring(i, i + 1)) * (7 - i);
			}
			
			long reminder = value % 11;
			long valid_checkdigit = 11 - reminder;
			if ("A".equalsIgnoreCase(checkdigit) && valid_checkdigit == 10) return true;
			
			if("0".equalsIgnoreCase(checkdigit) && valid_checkdigit == 11) return true;
			
			try {
				if (valid_checkdigit == Integer.parseInt(checkdigit)) {
					return true;
				}
			} catch (Exception e) {
			}
		}
		return false;
	}
	
	public static String formatAcademyNo(int academyNo){
		return String.format("%0"+academyNoLength+"d", academyNo);
	}
	
 	public static boolean validateVISA(String passportNo){
 		 if (passportNo.matches(regexVISA)){
 			return true;
 		 } else{
 		 	return false;
 		 }
 	 }
 	 
 	public static boolean validateEmail(String email){
 		if (email.matches(regexEmail)){
 			return true;
 		}else{
 			return false;
 		}
 	 }
     /**
      * like oracle nvl() function, return p2 if p1 is null
      * @param p1
      * @param p2
      * @return return p2 if p1 is null
      */
	  public static String nvl(String p1, String p2) {
		     return (p1==null || nvl(p1).length()==0)? p2 : p1;
	  }

	  /**
	   * return empty string if p1 is null
	   * @param p1
	   * @return p1 or empty string
	   */
	  public static String nvl(String p1) {
	     return (p1==null)? "" : p1;
	  }

	  /** return second argument if first parameter is null or ''
	   * @param p1
	   * @param p2
	   * @return
	   */
	  public static String ifempty(String p1, String p2) {
	      return (nvl(p1).trim().length()==0) ? p2 :p1;
	  }

	  /**
	   * Return p2 if empty string/null, else return p_else
	   * @param p1 test string
	   * @param p2 return this if p1 is empty string/null
	   * @param pElse return this if p1 is not empty/null
	   * @return
	   */
	  public static String ifempty(String p1, String p2, String pElse) {
	     return (nvl(p1).trim().length()==0) ? p2 : pElse;
	  }

	  /* *********** Date related functions *************** */

	  // created on : 7/6/2007: add days to a date (like oracle add_months)
	  public static Date addDays(Date p_date, double p_days) {
	     long l_days = (long) (p_days * 1000 * 3600 * 24);

	     return new Date(p_date.getTime() + l_days);
	  }


	  /**
	   * like oracle add_months. overload function since some Date functions are Deprecated  (11/11/2010)
	   * @param p_date original Date
	   * @param p_months number of months to be added. -/+
	   * @return new Calendar date
	   */
	  public static Date addMonths(Date p_date, int p_months) {
	     Calendar newDate=Calendar.getInstance();
	     newDate.setTime(p_date);
	     newDate.add(Calendar.MONTH , p_months);
	     return newDate.getTime();
	  }

	  // created on : 7/6/2007: convert most oracle date format patterns into java patterns
	  public static String convOracleDateFormat(String p_format) {
	     String l_format;
	     l_format=p_format.replace("mm", "MM");

	     l_format=l_format.replace("mon", "MMM");
	     l_format=l_format.replace("MON", "MMMM");
	     l_format=l_format.replace("mi", "mm");
	     l_format=l_format.replace("Y", "y");
	     l_format=l_format.replace("H", "h");
	     l_format=l_format.replace("hh24", "HH");
	     if (l_format.indexOf("h")>=0 && l_format.indexOf("a") < 0)
		//l_format=l_format.substring(0, l_format.lastIndexOf("m")) + "a" + l_format.substring(l_format.lastIndexOf("m"));
		l_format += "a";


	     return l_format;
	  }


	  // created on: 7/6/2007:
	  /** convert date to string by date pattern
	   * Month: MM; min: mm; Year yyyy; Day: dd 	    
	   * @param p_date 
	   * @param p_format
	   * @return
	  */
	  public static String toChar(Date p_date, String p_format) {
	     if (p_date == null)
	        return null;
	     if (p_format==null)
	        return p_date.toString(); 
	     
	     DateFormat dateformat;
	     try {
		    dateformat = new SimpleDateFormat(convOracleDateFormat(p_format));
	     } catch (Exception e) {
		   return p_date.toString();
	     }

	     return dateformat.format(p_date);
	  }

	/** 
	 * convert a decimal to a string by specified format
	 *  Month: MM; min: mm; Year yyyy; Day: dd  
    * @param p_num
    * @param p_format
    * @return
    */
	  public static String toChar(Double p_num, String p_format) {
	     DecimalFormat form=new DecimalFormat(nvl(p_format,"0.00###"));
	     return form.format(p_num);
	  }

	  // created on: 7/6/2007: 
	  /**
	   * difference of days between 2 dates (like oracle months_between)
	   * @param p_from_date
	   * @param p_to_date
	   * @return
	   */
	  public static double daysBetween(Date p_from_date, Date p_to_date) {
	     return (p_from_date.getTime() - p_to_date.getTime())/1000.0/3600.0/24.0 ;
	  }


	  // created on: 7/6/2007: 
	  /**
	   * convert string to date by date pattern
	   * @param p_date
	   * @param p_format
	   * @return
	   * @throws Exception
	   */
	  public static Date toDate(String p_date, String p_format) throws Exception
	  {
	     String v_date=p_date.trim();
	     DateFormat dateformat = new SimpleDateFormat(convOracleDateFormat(p_format));

	     if (p_format.indexOf('-')>0)
		     v_date=p_date.replace("/","-");
	     else
		     v_date=p_date.replace("-","/");

	     if (p_format.indexOf(':') > 0 && v_date.indexOf(':') < 0)
	        v_date = v_date + " 00:00:00";

	     return dateformat.parse(v_date);
	  }

	  /**
	   * right trime character
	   * @param p_str
	   * @param p_match trim out this pattern
	   * @return
	   */
	  public static String rtrim(String p_str, char p_match) {
	     int i=p_str.length();
	     for (; i > 0 ; i--)
		if (p_str.charAt(i-1)!=p_match)
		   break;

	     return p_str.substring(0, i);
	  }

	  public String ltrim(String p_str, char p_match) {
	     int i=0;
	     for (; i < p_str.length() ; i++)
		if (p_str.charAt(i)!=p_match)
		   break;

	     return p_str.substring(i);
	  }

	  /** convert single quote or double quote to html code (17/2/2011)
	  */
	  public static String htmlQuote(String p) {
	       if (nvl(p).length()==0 )
		  return p;

	       return p.replace("'", "&apos;").replace("\"", "&quot;");
	  }

	  /**
	   * convert String to Java class naming style. eg. abc_def => AbcDef
	   * @param p any String
	   * @return formatted name
	   */
	   public static String toJavaClassStyle(String p) {
	      if (nvl(p).length()<2)
		 return p;

	      String className="";
	      String []tmpStr=p.split("_");
	      for (int i=0; i < tmpStr.length; i++)
		 if (tmpStr[i].length() > 1)
		    className+=tmpStr[i].substring(0,1).toUpperCase() + tmpStr[i].substring(1).toLowerCase();
		 else
		    className+=tmpStr[i].toUpperCase();
	      return className;
	  }

	  /**
	   * convert String to Java variable style. eg. abc_def => abcDef
	   * @param p any String
	   * @return formatted name
	   */
	   public static String toJavaVarStyle(String p) {
	      if (nvl(p).length()<2)
		      return p;

	      String varName=toJavaClassStyle(p);
	      return varName.substring(0,1).toLowerCase() + varName.substring(1);
	  }
	  
	  /**
	   * to prevent sql injection, escape those dangerous codes
	   * @param in
	   * @return
	   */
	  public static String escapeSqlparm(String in) {
		 String out;
		 
		 if (in==null)
			return null;
		 
		 if (in.replace(" ","").indexOf("';")>=0)
			 return "suspected sql injection: ';";
		 if (in.replace(" ","").indexOf("'or")>=0)
			 return "suspected sql injection: 'or";
		 if (in.replace(" ","").indexOf("'union")>=0)
			 return "suspected sql injection: 'union";
		 
		 out=in.replace("'", "''");
		       //in.replace("delete", "")
//			   .replace("drop", "")
//			   .replace("truncate", "")
//			   .replace("union ", "")
		 
		 return out;
	  }
	  


	  /**
	   * convert BigDecimal to double
	   * @param x
	   * @return double value
	   */
	  public static double num(BigDecimal x) {
		return x==null? 0 : x.doubleValue(); 
	  }	  
	  
	  /**
	   * translate system message to end user understandable message and language
	   * @param pMsg
	   * @param lang TC-traditional Chinese, SC - simplified Chinese. null or EN is English   
	   * @return
	   */
	  public static String convertMsg(String pMsg, String lang) {	     
	     if (pMsg==null) 
	        return "";
	     String msg=pMsg;
	     if (pMsg.indexOf("constraint [PRIMARY]") >=0 || pMsg.indexOf("UNIQUE]") > 0)
	        return "TC".equals(lang)? "\u8CC7\u6599\u5DF2\u5B58\u5728": "Record already exists";
	     
	     return msg;	     
	  }
	  
	  /**
	   * convert an image file to base64 format
	   * @param imgPath the full path of the image file
	   * @return base64 string
	   */
	  public static String imgToBase64(String imgPath, int width, int height) {
			String imageString = null;
			try {
				BufferedImage img = ImageIO.read(new File(imgPath));
				if (img.getWidth() > width && img.getHeight() > height) {
					resizeImage(imgPath, width, height);
					img = ImageIO.read(new File(imgPath));
				}
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				String imgType = imgPath.substring(imgPath.lastIndexOf('.') + 1);

				ImageIO.write(img, imgType, bos);
				byte[] imageBytes = bos.toByteArray();
				BASE64Encoder encoder = new BASE64Encoder();
				imageString = encoder.encode(imageBytes);
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return imageString;
		}

		public static String imgToBase64(String imgPath) {
			String imageString = null;
			try {
				BufferedImage img = ImageIO.read(new File(imgPath));
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				String imgType = imgPath.substring(imgPath.lastIndexOf('.') + 1);
				ImageIO.write(img, imgType, bos);
				byte[] imageBytes = bos.toByteArray();
				BASE64Encoder encoder = new BASE64Encoder();
				imageString = encoder.encode(imageBytes);
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return imageString;
		}

		public static byte[] base64ToImage(String base64Encoder) {
			byte[] imagebyte = null;
			BASE64Decoder decoder = new BASE64Decoder();
			try {
				imagebyte = decoder.decodeBuffer(base64Encoder);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return imagebyte;
		}

		public static String imgToBase64(byte[] imageBytes) {
			String imageString = null;
			BASE64Encoder encoder = new BASE64Encoder();
			imageString = encoder.encode(imageBytes);
			return imageString;
		}

		public static byte[] imageToBytes(Image image, String format, int width, int height) {
			BufferedImage resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = resizedImage.createGraphics();
			g.drawImage(image, 0, 0, width, height, null);
			g.dispose();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				ImageIO.write(resizedImage, format, out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return out.toByteArray();
		}

		public static BufferedImage byteToImage(byte[] data) {
			ByteArrayInputStream in = new ByteArrayInputStream(data);
			try {
				BufferedImage image = ImageIO.read(in);
				return image;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

	  /**
	   * resize image
	   * @param path image path
	   * @param width new width, -1 to keep aspect proportion to height
	   * @param height new height, -1 to keep aspect proportion to width 
	   */
     public static void resizeImage(String path, int width, int height){
        try {
           BufferedImage originalImage = ImageIO.read(new File(path));
           int type=originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
           /*  BufferedImage resizedImage = new BufferedImage(width, height, type);           
           Graphics2D g = resizedImage.createGraphics();
           g.drawImage(originalImage, 0, 0, width, height, null);
           g.dispose();      
           ImageIO.write(resizedImage, path.substring(path.lastIndexOf('.')+1), new File(path)); */
           
           Image img=originalImage.getScaledInstance(width, height, type);
           BufferedImage resizedImage = new BufferedImage(img.getWidth(null), img.getHeight(null), type);
           Graphics2D g = resizedImage.createGraphics();
           g.drawImage(img, 0, 0, null);
           g.dispose();      
                      
           ImageIO.write(resizedImage, path.substring(path.lastIndexOf('.')+1), new File(path));
        } catch (Exception e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
        }        
     }     
     public static byte[] resizeImage(BufferedImage originalImage, int width, int height){
         try {
            int type=originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
            Image img=originalImage.getScaledInstance(width, height, type);
            BufferedImage resizedImage = new BufferedImage(img.getWidth(null), img.getHeight(null), type);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(img, 0, 0, null);
            g.dispose();    
            ByteArrayOutputStream out = new ByteArrayOutputStream();
			ImageIO.write(resizedImage, null, out);
			return out.toByteArray();
			
         } catch (Exception e) {
            e.printStackTrace();
         }   
         return null;
      }  
     
     public static Timestamp getCurrentTimestamp(){
    	 return new Timestamp(System.currentTimeMillis());
     }
     
     public static boolean notEmpty(String obj){
    	 if(obj != null && obj.trim().length()>0){
    		 return true;
    	 }else{
    		 return false;
    	 }
     }
     
     public static Serializable deeplyCopy(Serializable src){
  		try {
  			ByteArrayOutputStream baos = new ByteArrayOutputStream();
  			ObjectOutputStream oos = new ObjectOutputStream(baos);
  			oos.writeObject(src);
  			oos.close();
  			baos.close();
  			byte[] data = baos.toByteArray();
  			ByteArrayInputStream bais = new ByteArrayInputStream(data);
  			ObjectInputStream ois = new ObjectInputStream(bais);
  			Serializable copy = (Serializable) ois.readObject();
  			ois.close();
  			bais.close();
  			return copy ;
  		} catch (Exception e) {
  			e.printStackTrace();
  		}
  		return null ;
  	}
     
  	public static String getAbsoluteClassPath(Class<?> clazz) {

 		String classPath = clazz.getClassLoader().getResource("/").getPath();

 		// For Windows
 		if ("\\".equals(File.separator)) {
 			classPath = classPath.substring(1).replace("/", "\\");
 		}

 		// For Linux
 		if ("/".equals(File.separator)) {
 			classPath = classPath.replace("\\", "/");
 		}
 		
 		return classPath;
 	}
  	
  	public static String dateFormat(Date date, String format) {
  		
  		if (format == null || "".equals(format)) format = YYYY_MM_dd;
  		SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
  	}
  	
  	public static String getAccessDevice(String userAgent)
  	{
  		String device = "";
  		if (userAgent.toLowerCase().indexOf("windows") >= 0 )
        {
  			device = "Windows";
        } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
        {
        	device = "Mac";
        } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
        {
        	device = "Unix";
        }
  		/***
  		 * SGG-3811 only app login at same time
  		 */
        else if(userAgent.toLowerCase().indexOf("android") >= 0
        		||userAgent.toLowerCase().indexOf("imgfornote") >= 0)
        {
        	device = "Android";
        } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
        {
        	device = "IPhone";
        }
        else if(userAgent.toLowerCase().indexOf("ipad") >= 0)
        {
        	device = "iPad";
        }
        else{
        	device = "UnKnown";
        }
  		
  		return device;
  	}
  	public static String getAccessDeviceType(String userAgent)
  	{
  		String device = "";
  		if (userAgent.toLowerCase().indexOf("windows") >= 0 )
        {
  			device = "Windows";
        } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
        {
        	device = "Mac";
        } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
        {
        	device = "Unix";
        }
  		/***
  		 * SGG-3811 only app login at same time
  		 */
        else if((userAgent.toLowerCase().indexOf("android") >= 0
        		||userAgent.toLowerCase().indexOf("imgfornote") >= 0)
        		||(userAgent.toLowerCase().indexOf("iphone") >= 0)
        		||(userAgent.toLowerCase().indexOf("ipad") >= 0))
        {
        	
          device="APP";
        }
        else{
        	device = "UnKnown";
        }
  		
  		return device;
  	}
  	public static Long getIncTimeInMillis(Date originDate, int increment)
	{
  		if (null == originDate) return null;
		Calendar c = Calendar.getInstance();
		c.setTime(originDate);
		c.add(Calendar.MINUTE, increment);
		return c.getTimeInMillis();
	}
  	
  	public static String getMD5Password(String loginId, String password)
  	{
  		return DigestUtils.md5DigestAsHex((password + loginId.toLowerCase()).getBytes());
  	}
  	
  	public static boolean validatePassword(String password)
  	{
  		return (password.matches("[a-zA-Z0-9]{8,20}") && password.matches("[a-zA-Z]*[0-9]*[a-zA-Z]+[0-9]*[a-zA-Z]*"));
  	}
  	
  	public static String generateRandomPassword()
  	{
  		String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
  	    Random random = new Random();  
  	    StringBuffer buf = new StringBuffer();  
  	    for (int i = 0; i < 8; i++) {  
  	        int num = random.nextInt(62);  
  	        buf.append(str.charAt(num));  
  	    }  
  	    return buf.toString();
  	}
  	
	public static boolean validateEmailAddress(String addressList) {
		
		if (nvl(addressList).length() == 0) return false; 
        String[] addresses = addressList.split(";");
        
        boolean tag = true;
        for (String address : addresses) {
        	
        	tag = validateEmail(address);
        	if (!tag) break;
        }
        
        return tag;
    }
	
	public static String generatePdfName(String emailType){

		if(Constant.TEMPLATE_ID_INVOICE.equals(emailType)){
			return "HKGTA-INVOICE-SP00001.pdf";
		}else if(Constant.TEMPLATE_ID_RECEIPT.equals(emailType)){
			return "HKGTA-RECEIPT-SP00001.pdf";
		}

		return "HKGTA-RECEIPT-SP00001.pdf";
	}
	
/**
 * to generate check digit by luhn alogrithm 
 * @param input numeric digit in string format
 * @return a single number check digit
 * @throws Exception
 */
	public static String generateLuhnDigit(String input) throws Exception {
		
		try{
			Long.parseLong(input);
		}catch(Exception e){
			throw e;
		}
		
		int sum = 0;
		
		for(int i=0; i<input.length(); i++){
			sum += Integer.parseInt(input.substring(i,i+1));
		}
		
		int[] delta = {0,1,2,3,4,-4,-3,-2,-1,0};
		for (int i=input.length()-1; i>=0; i-=2 ){
			int deltaIndex = Integer.parseInt(input.substring(i,i+1));
			int deltaValue = delta[deltaIndex]; 
			sum += deltaValue;
		}
		
		int mod10 = sum % 10;
		mod10 = 10 - mod10; 
		if (mod10==10){  
		  mod10=0;
		}
		
		return mod10 + "";
	}
	/**
	 * 
	 * @param id
	 * @return
	 */
	public static String formatPosServiceItemNo(Long id){
		DecimalFormat df = new DecimalFormat("00000000");
		return df.format(id.longValue());
	}
	
	private static final int NINE_DIGIT = 9;
	private static final int STAFF_CARD_LENGTH = 7; //staff卡长度
	
	/**
	 * This method is used to transfer QR code into card no
	 * @Author Mianping_Wu
	 * @Date Aug 3, 2015
	 * @Param @param cardNo
	 * @Param @return
	 * @return String
	 */
	public static String cardNoTransfer(String cardNo) throws Exception {
		
	    if (CommUtil.nvl(cardNo).length() != NINE_DIGIT) throw new Exception("Illegal QR code found!");
	    String cardTemp = cardNo.substring(0, cardNo.length() - 1);
	    return cardTemp.replaceFirst("^0*", "");
	}

	public static String getMessageContentType(String content) {
	    
	    if (StringUtils.isEmpty(content)) return CONTENT_TYPE_PLAIN;
	    if (content.indexOf("</p>") >= 0 || content.indexOf("</h") >= 0) return CONTENT_TYPE_HTML;
	    return CONTENT_TYPE_PLAIN;
	}
	
	public static int getDayOfWeek(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	public static AgeRangeCode getAgeRange(int age) {
	    
	    if (age < 0) return null;
	    
	    if (age >= 0 && age <= 10) {
		
		return AgeRangeCode.KID;
		
	    } else if (age >= 11 && age <= 17) {
		
		return AgeRangeCode.CHILD;
		
	    } else if (age >= 18 && age <= 60) {
		
		return AgeRangeCode.ADULT;
		
	    } else if (age >= 61 && age <= 100) {
		
		return AgeRangeCode.SENIOR;
		
	    } else {
		
		return AgeRangeCode.ALL;
	    }
	    
	}
	
	public static Integer getAge(String birthDate) throws ParseException{
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		Date birth = myFormatter.parse(birthDate);
		
		int yearNow = calendar.get(Calendar.YEAR);
		int monthNow = calendar.get(Calendar.MONTH) + 1;
		int dayOfMonthNow = calendar.get(Calendar.DAY_OF_MONTH);
		
		calendar.setTime(birth);
		int yearBirth = calendar.get(Calendar.YEAR);
		int monthBirth = calendar.get(Calendar.MONTH);
		int dayOfMonthBirth = calendar.get(Calendar.DAY_OF_MONTH);
		
		Integer age = yearNow - yearBirth;
		
		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				if (dayOfMonthNow < dayOfMonthBirth) {age--;}
			}
			else if (monthNow < monthBirth) {age--;}
		}
		
		return age;
		
		
	}
	
	public static String formatVirtualAcc(String virtualAccount){
		if(virtualAccount!=null&&virtualAccount.length()==12){
			return virtualAccount.substring(0, 3)+" - "+virtualAccount.substring(3, 9)+" - "+virtualAccount.substring(9);
		}else{
		    return  nvl(virtualAccount);
		}
	}
	
	public static Date formatTheGivenYearAndMonth(String year, String month){
		String dateStr = year+"-"+String.format("%0"+2+"d", Integer.valueOf(month))+"-01";
		return DateConvertUtil.parseString2Date(dateStr, "yyyy-MM-dd");
	}
	
	public static String formatTheGivenYearAndMonthWithString(String year, String month){
		return year+"-"+String.format("%0"+2+"d", Integer.valueOf(month))+"-01";
	}
	
	public static Date formatTheGivenYearAndMonthAndDay(String year, String month,String day){
		String dateStr = year+"-"+String.format("%0"+2+"d", Integer.valueOf(month))+"-"+String.format("%0"+2+"d", Integer.valueOf(day));
		return DateConvertUtil.parseString2Date(dateStr, "yyyy-MM-dd");
	}
	
	
	public static Long CompareDates(Date date1, Date date2) {

		if (date1 == null || date2 == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Long dateDiff;
		try {
			dateDiff = df.parse(df.format(date1)).getTime() - df.parse(df.format(date2)).getTime();
			return dateDiff;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getCardType(String codeType){
		if(Constant.POS_MASTER_CARD_RSP.equals(codeType)){
			return PaymentMethodCode.MASTER.name();
		}else{
			return codeType;
		}
	}
	
	/**
	 * 验证staff卡格式是否正确
	 * @Author jack
	 * @Date Aug 3, 2015
	 * @Param @param cardNo
	 * @Param @return
	 * @return String
	 */
	public static String staffCardNoTransfer(String cardNo) throws Exception {
		
	    if (CommUtil.nvl(cardNo).length() != STAFF_CARD_LENGTH) throw new Exception("Illegal QR code found!");
	    String cardTemp = cardNo.substring(0, cardNo.length() - 1);
	    return cardTemp.replaceFirst("^0*", "");
	}
	public static void main(String[] args) {
		System.out.println(CommUtil.validatePhoneNo("13570956595"));
	}
}

