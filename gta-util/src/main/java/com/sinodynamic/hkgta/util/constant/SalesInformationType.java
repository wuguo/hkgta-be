package com.sinodynamic.hkgta.util.constant;

/*
 * VALUES('207001','SPA','TT','Treatment','ACT',NOW(),NULL);
INSERT INTO `item_payment_acc_code` (`account_code`, `category_code`, `payment_method_code`, `description`, `status`, `create_date`, `create_by`) 
VALUES('207002','SPA','SA','Sacred Arts','ACT',NOW(),NULL);
INSERT INTO `item_payment_acc_code` (`account_code`, `category_code`, `payment_method_code`, `description`, `status`, `create_date`, `create_by`) 
VALUES('207003','SPA','BQ','Boutique','ACT',NOW(),NULL);
INSERT INTO `item_payment_acc_code` (`account_code`, `category_code`, `payment_method_code`, `description`, `status`, `create_date`, `create_by`) 
VALUES('207004','SPA','DIM','Deferred Income - Membership','ACT',NOW(),NULL);
INSERT INTO `item_payment_acc_code` (`account_code`, `category_code`, `payment_method_code`, `description`, `status`, `create_date`, `create_by`) 
VALUES('207005','SPA','DIG','Deferred Income - Gift Card','ACT',NOW(),NULL);
 */
public enum SalesInformationType {
	TT("ServiceSales"),
	SA("ClassRefund"),
	BQ("ProductSales"),
	DIM("MembershipSales"),
	DIG("GiftCardSales");

	private String desc;

	private SalesInformationType(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
