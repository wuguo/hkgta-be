package com.sinodynamic.hkgta.util;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.util.constant.GTAError;

@Component
@Scope("prototype")
public class ResponseMsg implements Serializable
{

	@Resource(name = "messageSource")
	protected MessageSource messageSource;

	private static final long serialVersionUID = 1L;
	public String returnCode;
	public String errorMessageEN;

	public ResponseMsg(MessageSource messageSource)
	{
		super();
		if (this.messageSource == null)
		{
			this.messageSource = messageSource;
		}
	}
	
	public ResponseMsg()
	{
		super();
	}

	@Deprecated
	public ResponseMsg(String returnCode)
	{
		super();
		this.returnCode = returnCode;
	}

	/**
	 * Use initErrorMsg(GTAError error) instead
	 * */
	@Deprecated
	public ResponseMsg(String returnCode, String messageEN)
	{
		super();
		this.returnCode = returnCode;
		this.errorMessageEN = messageEN;
	}

	public ResponseMsg(Map<String, String> error)
	{
		super();
		this.returnCode = getErrorCode(error.keySet().iterator().next());
		this.errorMessageEN = error.get(returnCode);
	}

	public String getReturnCode()
	{
		return returnCode;
	}

	/**
	 * Use initErrorMsg instead
	 * */
	@Deprecated
	public void setReturnCode(String returnCode)
	{
		this.returnCode = returnCode;
	}

	public String getErrorMessageEN()
	{
		return errorMessageEN;
	}

	/**
	 * Use initErrorMsg instead
	 * */
	@Deprecated
	public void setErrorMessageEN(String errorMessageEN)
	{
		this.errorMessageEN = errorMessageEN;
	}

	/**
	 * Initial the Error Message without parameters
	 * */
	public void setErrorMsg(Map<String, String> error)
	{
		String errorCode = error.keySet().iterator().next();
		this.returnCode = getErrorCode(errorCode);
		this.errorMessageEN = error.get(errorCode);
	}

	private String getErrorCode(String errorCode)
	{
		if (errorCode.startsWith("1"))
		{
			return GTAError.Success.SUCCESS.getCode();
		}
		else
		{
			return errorCode;
		}
	}

	/**
	 * Initial the Error Message with parameters
	 * */
	public void initResult(GTAError error, Object[] args)
	{
		this.returnCode = getErrorCode(error.getCode());
		this.errorMessageEN = getI18nMessge(error.getCode(), args);
	}
	
	public void initResult(GTAError error, String errorMessage)
	{
		this.returnCode = getErrorCode(error.getCode());
		this.errorMessageEN = errorMessage;
	}

	/**
	 * Initial the Error Message with parameters
	 * */
	public void initResult(GTAError error)
	{
		this.returnCode = getErrorCode(error.getCode());
		this.errorMessageEN = getI18nMessge(error.getCode());
	}
	
	/**
	 * Initial the Error Message with parameters
	 * */
	public void initResult(GTAError[] errors)
	{
		StringBuffer code = new StringBuffer();
		for(GTAError err : errors)
		{
			code.append(",");
			code.append(err.getCode());
		}
		this.returnCode = getErrorCode(code.substring(1));
		this.errorMessageEN = getI18nMessge(errors[0].getCode());
	}

	/**
	 * This method has moved into ResponseMsg, ResponseMsg will handle all of
	 * the error message.
	 *
	 * get i18n message
	 * 
	 * @param msgCode
	 * @return
	 */
	public String getI18nMessge(String msgCode)
	{
		return this.getI18nMessge(msgCode, null, LocaleContextHolder.getLocale());
	}

	/**
	 * This method has moved into ResponseMsg, ResponseMsg will handle all of
	 * the error message. get i18 message with parameters, just like {0} {1}
	 * 
	 * @param msgCode
	 * @param args
	 * @return
	 */
	public String getI18nMessge(String msgCode, Object[] args)
	{
		return this.getI18nMessge(msgCode, args, LocaleContextHolder.getLocale());
	}

	/**
	 * This method has moved into ResponseMsg, ResponseMsg will handle all of
	 * the error message.
	 */
	public String getI18nMessge(String msgCode, Locale locale)
	{
		return this.getI18nMessge(msgCode, null, locale);
	}

	/**
	 * This method has moved into ResponseMsg, ResponseMsg will handle all of
	 * the error message.
	 */
	public String getI18nMessge(String msgCode, Object[] args, Locale locale)
	{
		return messageSource.getMessage(msgCode, args, locale);
	}
}
