package com.sinodynamic.hkgta.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public class FileUtil {
	
	private FileUtil(){}
	
	public static boolean moveFile(String oriFilePath, String newFilePath) {
		FileInputStream fi = null;
		FileOutputStream fo = null;
		FileChannel in = null;
		FileChannel out = null;
		try {
			File filein = new File(oriFilePath);
			File fileout = new File(newFilePath);
			if (fileout.exists()) {
				if (filein.isFile() && fileout.isFile()) {
					fi = new FileInputStream(oriFilePath);
					fo = new FileOutputStream(newFilePath);
				}
			} else {
				if (filein.isFile()) {
					fi = new FileInputStream(oriFilePath);
					fo = new FileOutputStream(newFilePath);
				}
			}
			if(null!=fi&&null!=fo){
				in = fi.getChannel();
				out = fo.getChannel();
				in.transferTo(0, in.size(), out);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (null != fi) {
					fi.close();
				}
				if (null != fo) {
					fo.close();
				}
				if (null != in) {
					in.close();
				}
				if (null != out) {
					out.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		File oriFile = new File(oriFilePath);
		if (oriFile.isFile()) {
			oriFile.delete();
		}
		return true;
	}

	public static String saveFileContent(byte[] b, String fileName, String filesDirectory) throws IOException {
		InputStream in = null;
		FileOutputStream out = null;

		File dir = new File(filesDirectory);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		String targetPath = dir.getPath() + File.separator + fileName;
		File destinationFile = new File(targetPath);
		try {
			in = new ByteArrayInputStream(b);
			out = new FileOutputStream(destinationFile);
			int c;

			while ((c = in.read()) != -1) {
				out.write(c);
			}

		}catch(IOException e)
		{
			e.printStackTrace();
		}
		finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
		return targetPath;

	}
	
	public static  String getFilePath(String appPath) throws IOException {

		Resource resource = new ClassPathResource("placeholder/app.properties");
		Properties appProps = PropertiesLoaderUtils.loadProperties(resource);
		String path = appProps.getProperty(appPath);

		return path;
	}
	
	public static boolean copyFile(String oriFilePath, String newFilePath) {
		FileInputStream fi = null;
		FileOutputStream fo = null;
		FileChannel in = null;
		FileChannel out = null;
		try {
			File filein = new File(oriFilePath);
			File fileout = new File(newFilePath);
			if (fileout.exists()) {
				if (filein.isFile() && fileout.isFile()) {
					fi = new FileInputStream(oriFilePath);
					fo = new FileOutputStream(newFilePath);
				}
			} else {
				if (filein.isFile()) {
					fi = new FileInputStream(oriFilePath);
					fo = new FileOutputStream(newFilePath);
				}
			}
			if(null!=fi&&null!=fo){
				in = fi.getChannel();
				out = fo.getChannel();
				in.transferTo(0, in.size(), out);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (null != fi) {
					fi.close();
				}
				if (null != fo) {
					fo.close();
				}
				if (null != in) {
					in.close();
				}
				if (null != out) {
					out.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return true;
	}
}
