package com.sinodynamic.hkgta.util.urlmatcher;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

/**
 * @see org.springframework.security.web.util.AntUrlPathMatcher which deprecated and removed after
 *      spring-security-web:3.1.x, this class a copy of the removed class..
 * @author Luke Taylor (Spring-security-web)
 * @author Kevin_He
 */
public class AntUrlPathMatcher {

	private boolean requiresLowerCaseUrl = true;
	private PathMatcher pathMatcher = new AntPathMatcher();

	public AntUrlPathMatcher() {
		this(true);
	}

	public AntUrlPathMatcher(boolean requiresLowerCaseUrl) {
		this.requiresLowerCaseUrl = requiresLowerCaseUrl;
	}

	public Object compile(String path) {
		if (requiresLowerCaseUrl) {
			return path.toLowerCase();
		}

		return path;
	}

	public void setRequiresLowerCaseUrl(boolean requiresLowerCaseUrl) {
		this.requiresLowerCaseUrl = requiresLowerCaseUrl;
	}

	public boolean pathMatchesUrl(Object path, String url) {
		return pathMatcher.match((String) path, url);
	}

	public String getUniversalMatchPattern() {
		return "/**";
	}

	public boolean requiresLowerCaseUrl() {
		return requiresLowerCaseUrl;
	}

	public String toString() {
		return getClass().getName() + "[requiresLowerCase='" + requiresLowerCaseUrl + "']";
	}

}
