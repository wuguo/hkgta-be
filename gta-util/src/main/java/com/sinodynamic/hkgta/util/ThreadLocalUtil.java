package com.sinodynamic.hkgta.util;

import java.util.HashMap;
import java.util.Map;

public class ThreadLocalUtil {
	private static ThreadLocal<Map<String, Object>> threadLocalObject = new ThreadLocal<Map<String, Object>>();
	
	public static void setObject(String key, Object object){
		if(threadLocalObject.get() != null){
			threadLocalObject.get().put(key, object);
		}else{
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(key, object);
			threadLocalObject.set(map);
		}
		
	}
	
	public static <T> T getObject(Class<T> clazz, String key){
		if(threadLocalObject.get() != null){
			return (T) threadLocalObject.get().get(key);
		}else{
			return null;
		}
	}
	
	public void reset(){
		threadLocalObject.remove();
	}
}
