package com.sinodynamic.hkgta.util.constant;

public enum GlobalParameterType {

	WELLNESS_SWITCH("PATRON_RESERVATION_WELLNESS_SWITCH"),
	PAYMENT_TYPE_AMEX("PAYMENT_TYPE_AMEX"),
	STAFF_MAIL_DAILY_PATRON("STAFF_MAIL_DAILY_PATRON"),
	IBEACON_UUID("IBEACON_UUID"),
	REFUNDREQUESTRECIPIENTMAIL("REFUND_REQUEST_RECIPIEN_TMAIL"),
	REPORT_ISSUE("REPORT_ISSUE"),
	ACCOMMODATION_SWITCH("PATRON_RESERVATION_ACCOMMODATION_SWITCH"),
	DDA_DDIREPORTRECIPIENTEMAIL("DDA_DDIREPORTRECIPIENTEMAIL"),
	WELLNESS_RESERVATION_BY_MOBILE_APP("WELLNESS_RESERVATION_BY_MOBILE_APP");
	
	private String code;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	private GlobalParameterType(String code){
		this.code=code;
	}
}
