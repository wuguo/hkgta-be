/**
 * 
 * @author Miranda_Zhang
 * @date Dec 1, 2015
 */
package com.sinodynamic.hkgta.util.constant;


/**
 *
 * @author Miranda_Zhang
 * @date Dec 1, 2015
 */
public enum AdvancePeriodType {

	ADVANCEDRESPERIOD_SPA("SpaAdvancedResPeriod");
	
	private String name;
	private AdvancePeriodType(String name){
		this.name = name;
	}
	
	@Override
	public String toString(){
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public static void main(String[] args) {
		System.out.println(AdvancePeriodType.ADVANCEDRESPERIOD_SPA.toString());
	}
	
}
