package com.sinodynamic.hkgta.util.constant;

public enum AgeRangeCode {
	
	ALL("ALL"), ADULT("ADULT"), CHILD("CHILD"), KID("KID"), SENIOR("SENIOR");

		private String desc;

		private AgeRangeCode(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

