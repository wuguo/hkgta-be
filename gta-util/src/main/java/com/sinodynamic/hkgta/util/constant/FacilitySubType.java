package com.sinodynamic.hkgta.util.constant;

public enum FacilitySubType {

	/*
	 * Facility Sub Type for Tennis
	 */
	TENNISCRTHALF("TENNISCRT-HALF"), TENNISCRTITFID("TENNISCRT-ITFID"), TENNISCRTITFOD("TENNISCRT-ITFOD"), TENNISCRTMINI("TENNISCRT-MINI")
	,TENNISCRTPAD("TENNISCRT-PAD"),TENNISCRTSTD("TENNISCRT-STD");

	private String desc;

	private FacilitySubType(String d) {
		this.desc = d;
	}

	public String getName() {
		return this.name();
	}

	public String getDesc(){
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

}
