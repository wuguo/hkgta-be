package com.sinodynamic.hkgta.util.constant;

public enum EnrollStatus {
	NEW("New"), APV("Approved"), OPN("Open"), ANC("Active(No Card)"), CMP("Complete"), PYF("Payment Fail"), REJ("Rejected"), CAN("Cancelled"), PYA("Payment Approved"), TOA("To Active");

	private EnrollStatus(String desc) {
		this.desc = desc;
	}

	private String desc;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return super.name();
	}

}
