package com.sinodynamic.hkgta.util.constant;

public enum RestaurantQuotaPeriodCode {
    MORNING("MORNING"), AFTERNOON("AFTERNOON"), NIGHT("NIGHT"), MIDNIGHT("MID-NIGHT");

    private String value;
    RestaurantQuotaPeriodCode(String value)
    {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}