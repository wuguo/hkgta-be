package com.sinodynamic.hkgta.util;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;

public class GatImageProcessor {

	private static GatImageProcessor processor = new GatImageProcessor();

	private GatImageProcessor() {

	}

	private static enum FlipCategary {
		MirrorHorizontal, MirrorVertical, Normal
	}

	private BufferedImage rotateAccordingToExif(File file, Metadata metadata)
			throws ImageProcessingException, IOException, Exception {

		BufferedImage bufferedimage = ImageIO.read(file);
		return processor.flipAndRotate(bufferedimage, metadata);
	}

	public static boolean rotateAccordingToExif(File file) {
		try {
			Metadata metadata = ImageMetadataReader.readMetadata(file);
			BufferedImage img = processor.rotateAccordingToExif(file, metadata);
			if (img != null) {
				String fileName = file.getName();
				String postfix = fileName
						.substring(fileName.lastIndexOf(".") + 1);
				ImageIO.write(img, postfix, file);	
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private BufferedImage flipAndRotate(BufferedImage bufferedimage,
			Metadata metadata) throws Exception {
		int orientation = processor.getOrientation(metadata);
		switch (orientation) {
		// "Top, left side (Horizontal / normal)"
		case 1:
			return null;
			// "Top, right side (Mirror horizontal)"
		case 2:
			processor.flipAndRotate(bufferedimage, 180,
					FlipCategary.MirrorHorizontal);
			// "Bottom, right side (Rotate 180)"
		case 3:
			return processor.flipAndRotate(bufferedimage, 180,
					FlipCategary.Normal);
			// "Bottom, left side (Mirror vertical)"
		case 4:
			return processor.flipAndRotate(bufferedimage, 0,
					FlipCategary.MirrorVertical);
			// "Left side, top (Mirror horizontal and rotate 270 CW)"
		case 5:
			return processor.flipAndRotate(bufferedimage, 270,
					FlipCategary.MirrorHorizontal);
			// "Right side, top (Rotate 90 CW)"
			// case 6: return processor.rotateImg(bufferedimage, 90);
		case 6:
			return processor.flipAndRotate(bufferedimage, 90,
					FlipCategary.Normal);
			// "Right side, bottom (Mirror horizontal and rotate 90 CW)"
		case 7:
			return processor.flipAndRotate(bufferedimage, 90,
					FlipCategary.MirrorHorizontal);
			// "Left side, bottom (Rotate 270 CW)"
		case 8:
			return processor.flipAndRotate(bufferedimage, 270,
					FlipCategary.Normal);
		default:
			return null;
		}
	}

	private BufferedImage flipAndRotate(BufferedImage bufferedimage,
			int degree, FlipCategary flipCategary) {
		int iw = bufferedimage.getWidth();
		int ih = bufferedimage.getHeight();
		int w;
		int h = 0;
		int x = 0;
		int y = 0;
		degree = degree % 360;
		if (degree < 0)
			degree = 360 + degree;
		double ang = Math.toRadians(degree);
		w = iw;
		h = ih;
		if (degree == 90 || degree == 270) {
			w = ih;
			h = iw;
		}
		x = (w / 2) - (iw / 2);
		y = (h / 2) - (ih / 2);

		// degree = degree - 180;
		BufferedImage newImg = new BufferedImage(w, h, bufferedimage.getType());

		Graphics2D graphics2d = newImg.createGraphics();
		AffineTransform transform = graphics2d.getTransform();

		if (FlipCategary.MirrorHorizontal.equals(flipCategary)) {
			transform = new AffineTransform(-1, 0, 0, 1,
					bufferedimage.getWidth(), 0);

		} else if (FlipCategary.MirrorVertical.equals(flipCategary)) {
			transform = new AffineTransform(1, 0, 0, -1, 0,
					bufferedimage.getHeight());
		}

		graphics2d.setTransform(transform);
		graphics2d.rotate(ang, w / 2, h / 2);
		graphics2d.translate(x, y);
		graphics2d.drawImage(bufferedimage, 0, 0, null);
		graphics2d.dispose();

		return newImg;
	}

	private int getOrientation(Metadata metadata) throws MetadataException {
		int orentation = -1;
		for (Directory directory : metadata.getDirectories()) {
			if (directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION)) {
				orentation = directory
						.getInt(ExifIFD0Directory.TAG_ORIENTATION);
			}
			for (Tag tag : directory.getTags()) {
				System.out.println(tag);
			}
		}
		return orentation;
	}

	public static void main(String[] args) throws Exception {
		File imageFile = new File("D:\\fileServer\\report\\123.jpg");
		GatImageProcessor.rotateAccordingToExif(imageFile);
	}

}
