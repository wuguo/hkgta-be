package com.sinodynamic.hkgta.util.pagination;

public abstract interface Pagination
{
  public abstract int getAllSize();

  public abstract int getSize();

  public abstract int getAllPage();

  public abstract int getNumber();

  public abstract boolean isFirst();

  public abstract boolean isLast();

  public abstract int getNextNumber();

  public abstract int getPreviousNumber();
}
