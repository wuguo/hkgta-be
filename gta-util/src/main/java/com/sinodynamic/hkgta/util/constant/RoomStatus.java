package com.sinodynamic.hkgta.util.constant;

public enum RoomStatus {
	
		//modified by Kaster 20160318 为了与第三方接口接受的状态一致，将R的描述修改为NEEDS_INSPECTION，原值是READY_TO_INSPECT
	 	D("VACANT_DIRTY"),R("NEEDS_INSPECTION"),OC("OCCUPIED_CLEAN"),VC("VACANT_CLEAN"),OOO("OFF_MARKET"),OOS("OUT_OF_ORDER");
	
		private String desc;

		private RoomStatus(String desc) {
			this.desc = desc;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

