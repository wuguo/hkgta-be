package com.sinodynamic.hkgta.util.constant;

public interface BiOasisTypeItem {
	public enum Fangan {
		FANBFK("FAN|BFK", "Breakfast"), 
		FANLCH("FAN|LCH", "Lunch"), 
		FANAFT("FAN|AFT", "Tea"), 
		FANDNR("FAN|DNR","Dinner");
		
		private Fangan(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum BarLounge {
		LOUNGEBFK("LOUNGE|BFK","Breakfast"),
		LOUNGELCH("LOUNGE|LCH","Lunch"),
		LOUNGEAFT("LOUNGE|AFT","Tea"),
		LOUNGEDNR("LOUNGE|DNR","Dinner");
		private BarLounge(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum Sakti {
		//Sakti
		FEBFK("FE|BFK","Breakfast"),
		FELCH("FE|LCH","Lunch"),
		FEAFT("FE|AFT","Tea"),
		FEDNR("FE|DNR","Dinner");
		private Sakti(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum Add {
		//ADD
		ADDBFK("ADD|BFK","Breakfast"),
		ADDLCH("ADD|LCH","Lunch"),
		ADDAFT("ADD|AFT","Tea"),
		ADDDNR("ADD|DNR","Dinner");
		private Add(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum Proshop {
		//Pro Shop
		PSGOLF("PS|GOLF","Golf"),
		PSTENNIS("PS|TENNIS","Tennis"),
		PSOTHERS("PS|OTHERS","Others");
		private Proshop(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum Accommodation {
		//Accommodation
		ROOM("ROOM","Room"),
		MINIBAR("MINIBAR","Mini Bar");
		private Accommodation(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum Facility {
		//Accommodation
		GOLF("GFBK","Golfing Bay"),
		TENNIS("TFBK","Tennis Court");
		private Facility(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum School {
		//Accommodation
		TSS("TSS","Tennis Course"),
		TS("TS","Tennis Coaching"),
		GSS("GSS","Golf Course"),
		GS("GS","Golf Coaching");
		private School(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum Admission {
		//Accommodation
		IPMN("IPMN","Individual Service Plan (Non Multi-Generation)"),
		IPMM("IPMM","Individual Service Plan (Multi-Generation)"),
		CPM("CPM","Corporate Service Plan"),
		DP("DP","Day Pass");
		private Admission(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum Banquet {
		//ADD
		BFK("BANQUET|BFK","Breakfast"),
		LCH("BANQUET|LCH","Lunch"),
		TEA("BANQUET|AFT","Tea"),
		DNR("BANQUET|DNR","Dinner");
		private Banquet(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum TeaLounge {
		//ADD
		BFK("TEA|BFK","Breakfast"),
		LCH("TEA|LCH","Lunch"),
		TEA("TEA|AFT","Tea"),
		DNR("TEA|DNR","Dinner");
		private TeaLounge(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum Recreation {
		//ADD
		FITNESS("RECREATION|FC","Fitness Center"),
		KID("RECREATION|KZ","Kid Zone");
		private Recreation(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
	public enum RoomService {
		//ADD
		BFK("RS|BFK","Breakfast"),
		LCH("RS|LCH","Lunch"),
		TEA("RS|AFT","Tea"),
		DNR("RS|DNR","Dinner");
		private RoomService(String gtaCode, String desc) {
			this.gtaCode = gtaCode;
			this.desc = desc;
		}
		private String gtaCode;
		private String desc;
		public String getGtaCode() {
			return gtaCode;
		}
		public void setGtaCode(String gtaCode) {
			this.gtaCode = gtaCode;
		}
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}
}
