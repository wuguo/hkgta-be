package com.sinodynamic.hkgta.util.exception;

import com.sinodynamic.hkgta.util.constant.GTAError;

public class ErrorCodeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private GTAError errorCode;

	private String errorMessage;

	private Object[] args;

	public ErrorCodeException()
	{
		super();
	}

	public ErrorCodeException(Throwable e)
	{
		super(e);
	}
	
	public ErrorCodeException(GTAError errorCode)
	{
		super();
		this.errorCode = errorCode;
	}
	
	public ErrorCodeException(GTAError errorCode,Object[] args)
	{
		this(errorCode);
		this.args = args;
	}

	public ErrorCodeException(GTAError errorCode, String errorMessage)
	{
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public ErrorCodeException(GTAError errorCode, String errorMessage, Object[] args)
	{
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.args = args;
	}

	public ErrorCodeException(GTAError errorCode, String errorMessage, Throwable e)
	{
		super(e);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public GTAError getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(GTAError errorCode)
	{
		this.errorCode = errorCode;
	}

	public Object[] getArgs()
	{
		return args;
	}

	public void setArgs(Object[] args)
	{
		this.args = args;
	}

}
