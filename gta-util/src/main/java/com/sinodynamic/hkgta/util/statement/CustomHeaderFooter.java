package com.sinodynamic.hkgta.util.statement;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

public class CustomHeaderFooter extends PdfPageEventHelper {
	private Phrase phrase;
	private Rectangle rect;
	
	public Phrase getPhrase() {
		return phrase;
	}
	public void setPhrase(Phrase phrase) {
		this.phrase = phrase;
	}
	public Rectangle getRect() {
		return rect;
	}
	public void setRect(Rectangle rect) {
		this.rect = rect;
	}
	public void onEndPage(PdfWriter writer, Document document) {
		try {
			ColumnText ct = new ColumnText(writer.getDirectContent());
			ct.setSimpleColumn(phrase,rect.getLeft(), rect.getBottom(), rect.getRight(), rect.getTop(),0,Element.ALIGN_LEFT);
			ct.go();
//			switch (writer.getPageNumber() % 2) {
//			case 0:
//				ColumnText ct = new ColumnText(writer.getDirectContent());
//				ct.setSimpleColumn(phrase,rect.getLeft(), rect.getBottom(), rect.getRight(), rect.getTop(),0,Element.ALIGN_LEFT);
//				ct.go();
//				break;
//			case 1:
//				ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("odd headerdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"), rect.getLeft(), rect.getTop(), 0);
//				break;
//			}
			//page no setting
//			ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(String.format("Page %d", writer.getPageNumber())), (rect.getLeft() + rect.getRight()) / 2, rect.getBottom() - 18, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void onEndPage(PdfWriter writer, Document document,Rectangle rect,Phrase phrase) {
		this.rect = rect;
		this.phrase = phrase;
		onEndPage(writer,document);
	}
}