package com.sinodynamic.hkgta.util.pagination;

import java.util.List;

public class PageBean {
	// transfer from page
	private int currentPage;
	private int pageSize;
	// select from DB
	// datalist to show in the page
	private List recordList;
	private int recordCount;
	// calculate
	private int pageCount;
	private int beginPageIndex;
	private int endPageIndex;
	private int defaultPageNumber = 10;

	/**
	 * this 4 field must be transfer, other 3 field will be calculate
	 * 
	 * @param currentPage
	 * @param pageSize
	 * @param recordList
	 * @param recordCount
	 */
	public PageBean(int currentPage, int pageSize, List recordList,
			int recordCount) {
		this.currentPage = currentPage;
		this.pageSize = pageSize;
		this.recordList = recordList;
		this.recordCount = recordCount;
		// calculate pageCount
		pageCount = (recordCount + pageSize - 1) / pageSize;

		// calculate beginPageIndex and endPageIndex
		// when pageCount < defaultPageNumber, it will show all the page
		if (pageCount <= defaultPageNumber) {
			beginPageIndex = 1;
			endPageIndex = pageCount;
		}
		// when pageCount > defaultPageNumber, it will defaultPageNumber  page,currentPage will be defaultPageNumber/2 page
		// for example if the defaultPageNumber is 10,currentPage will be fifth page
		else {
			// default will show before defaultPageNumber/2-1 + pageCount + last
			// defaultPageNumber/2
			// for example if the defaultPageNumber is 10,default will show
			// first 4 + pageCount + last 5
			beginPageIndex = currentPage - defaultPageNumber / 2 + 1; // 7 - 4 =  3;
			endPageIndex = currentPage + defaultPageNumber / 2; // 7 + 5 = 12; --> 3 ~ 12
			// if before page < defaultPageNumber/2-1, it will show the first defaultPageNumber
			if (beginPageIndex < 1) {
				beginPageIndex = 1;
				endPageIndex = 10;
			}
			// if last page < defaultPageNumber/2, it will show the last defaultPageNumber
			else if (endPageIndex > pageCount) {
				endPageIndex = pageCount;
				beginPageIndex = pageCount - defaultPageNumber +1 ;
			}
		}
	}

	public List getRecordList() {
		return recordList;
	}

	public void setRecordList(List recordList) {
		this.recordList = recordList;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	public int getBeginPageIndex() {
		return beginPageIndex;
	}

	public void setBeginPageIndex(int beginPageIndex) {
		this.beginPageIndex = beginPageIndex;
	}

	public int getEndPageIndex() {
		return endPageIndex;
	}

	public void setEndPageIndex(int endPageIndex) {
		this.endPageIndex = endPageIndex;
	}
}
