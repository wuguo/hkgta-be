package com.sinodynamic.hkgta.util.constant;

public enum LimitType {
	TRN("TRN", "transaction limit"),
	CR("CR", "credit limit"),
	PASSQUOTA("PASSQUOTA", "guest pass quota");
	
	private LimitType(String name, String desc){
		this.name = name;
		this.desc = desc;
	}
	
	private String desc;
	private String name;
	
	public String getDesc(){
		return this.desc;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	@Override
	public String toString() {
		return this.desc;
	}
}
