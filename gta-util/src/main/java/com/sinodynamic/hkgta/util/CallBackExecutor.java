package com.sinodynamic.hkgta.util;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

public class CallBackExecutor {
	public Logger logger;
	
	private HashMap<String,Object> contextValues = new HashMap<String,Object>();
	static String EXCEPTION_KEY = "_EXCEPTION_";
	public CallBackExecutor(Class<?> clazz){
		logger = Logger.getLogger(clazz);
	}
	
	public CallBackExecutor(){ 
		logger = Logger.getLogger(CallBackExecutor.class);
	}
	
	public enum MethodPart{
		TRY,CATCH
	}
	
	public interface CallBack{
		HashMap<String,Object> getContextValues();
		public Exception getException();
		
		void setContextValues(HashMap<String,Object> contextValues);
		/**
		 * The main callback method , which simulate the try{} block in java language
		 * @return the return object
		 * @throws Exception any exception
		 */
		Object doTry() throws Exception;
		
		/**
		 * The exception handler, which simulates the catch() block in java language
		 * In this method , some rollbacks should be done here
		 * @throws Exception any exception
		 */
		void doCatch() throws Exception;
		
		
		/**
		 * Simulate the finally() block in java language , this method SHOULD NOT throw any exception
		 */
		void doFinally();
		
		
		/**
		 * Generate the error msg according to which part in code such as Try,Catch or Finally
		 * @param methodPart which part of java code
		 * @return an GTACommonException
		 */
		GTACommonException throwException(MethodPart methodPart);
		
		
	}
	
	public Object execute(CallBack callback) throws GTACommonException{
		
		try{
			callback.setContextValues(this.contextValues);
			return callback.doTry();
		}catch(Exception _try){
			this.getContextValues().put(EXCEPTION_KEY, _try);
			logger.error("ERROR: ", _try);
			try{
				callback.doCatch();
			}catch(Exception _catch){
				logger.error("ERROR: ", _catch);
				if(_catch instanceof GTACommonException){
					throw (GTACommonException)_catch;
				}else{
					GTACommonException errorCatch = callback.throwException(MethodPart.CATCH);
					if(errorCatch == null){
						logger.error(_catch.getMessage());
						throw new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR);
//						throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{_catch.getMessage()});
					}else{
						throw errorCatch;
					}
				}
			}
			
			if(_try instanceof GTACommonException){
				throw (GTACommonException)_try;
			}else{
				GTACommonException errorTry = callback.throwException(MethodPart.TRY);
				if(errorTry == null){
					logger.error(_try.getMessage());
					throw new GTACommonException(GTAError.CommonError.CALL_BACK_METHOD_ERROR);
//					throw new GTACommonException(GTAError.CommomError.UNHANDLED_EXCEPTION, new String[]{_try.getMessage()});
				}else{
					throw errorTry;
				}
			}
		}finally{
			callback.doFinally();
		}
	}

	public HashMap<String, Object> getContextValues() {
		return contextValues;
	}

	public void setContextValues(HashMap<String, Object> contextValues) {
		this.contextValues = contextValues;
	}
	
	public Exception getException(){
		Exception ex = (Exception) this.getContextValues().get(EXCEPTION_KEY);
		return ex;
	}
}
