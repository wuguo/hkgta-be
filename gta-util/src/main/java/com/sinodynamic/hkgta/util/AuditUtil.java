package com.sinodynamic.hkgta.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;


public class AuditUtil {
	
	public static String getPrimaryKeyValue(Object entity) throws Exception{
		Class<?> clazz = entity.getClass();
		
		List<AccessibleObject> members = new ArrayList<AccessibleObject>();
		members.addAll(Arrays.asList(clazz.getDeclaredFields()));
		members.addAll(Arrays.asList(clazz.getDeclaredMethods()));
		
		for(AccessibleObject member : members){
			if(member.getAnnotations().length == 0) continue;
			
			for(Annotation filter : member.getAnnotations()){
				if(!StringUtils.contains(filter.toString(), "javax.persistence.Id") &&
						!StringUtils.contains(filter.toString(), "javax.persistence.EmbeddedId")){
					continue;
				}
			}
			
			member.setAccessible(true);
			Object value = null;
			String singleKeyName = null;
			if(member instanceof Field){
				value = ((Field)member).get(entity);
				singleKeyName = ((Member)member).getName();
			}else{
				value = ((Method)member).invoke(entity, new Object[0]);
				singleKeyName = StringUtils.uncapitalize(((Member)member).getName().replaceAll("get", ""));
			}
			
			for(Annotation anno : member.getAnnotations()){
				if(StringUtils.contains(anno.toString(), "javax.persistence.Id")){
					Map<String, Object> json = new HashMap<String, Object>();
					json.put(singleKeyName, value);
					return new Gson().toJson(json);
				}
				
				if(StringUtils.contains(anno.toString(), "javax.persistence.EmbeddedId")){
					return new Gson().toJson(value);
				}
			}
		}
		
		return "";
	}
}
