package com.sinodynamic.hkgta.util.constant;

public enum MassageIntervalCode {
	
	ONE("00"), TWO("15"), THREE("30"),FOUR("45");

		private String desc;

		private MassageIntervalCode(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

