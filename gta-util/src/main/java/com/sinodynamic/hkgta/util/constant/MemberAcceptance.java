package com.sinodynamic.hkgta.util.constant;

/**
 *
 *@author Miranda_Zhang
 */
public enum MemberAcceptance {

	AUTO("A"),
	MANUAL("M");
	
	private String type;
	
	public String getType()
	{
		return type;
	}

	private MemberAcceptance(String type){
		this.type = type;
	}
	
	@Override  
    public String toString() {  
        return this.type;  
    }  
}
