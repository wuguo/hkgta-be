package com.sinodynamic.hkgta.util.constant;

public enum PaymentMediaType {
	OP("Online Payment"),ECR("ECR Terminal"),OTH("N/A"),NA("Not applicant"),OF("Offline");
	private PaymentMediaType(String desc) {
		this.desc = desc;
	}

	private String desc;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return super.name();
	}
}
