package com.sinodynamic.hkgta.util;

public class CarkParkMemberUtil {
	
	public static String formatNumber(int length, Long number) {
	    String f = "%0" + length + "d";
	    return String.format(f, number);
	}

	public static String getCheckDigitByLuhnAlogrithm(String s) throws Exception{
		String fullNo = s + "x";
		int[] digitList = new int[fullNo.length()];
		
		//Collect number for even position
		for (int i = fullNo.length()-2; i>=0; i=i-2){
			int digit = Integer.parseInt(fullNo.substring(i, i+1))*2;
			digitList[i] = (digit >= 10 ? digit-9 : digit);
		}
		
		//Collect number for odd position
		for (int i = fullNo.length()-3; i>=0; i=i-2){
			digitList[i] = Integer.parseInt(fullNo.substring(i, i+1));
		}
		int sum = 0;
		for (int i=0; i<digitList.length;i++){
			sum = sum + digitList[i];
		}
		int residual = sum%10;
		int checkDigit = (residual == 0 ? residual : 10 - residual);
		return fullNo.replace("x",checkDigit+"");

	}
}
