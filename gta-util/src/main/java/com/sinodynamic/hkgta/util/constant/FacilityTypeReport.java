package com.sinodynamic.hkgta.util.constant;

public enum FacilityTypeReport {
	GOLF("Golf", "Golf"), TENNIS("Tennis", "Tennis"),WELLNESS("Fivelements","Fivelements"),GUESTROOM("Accommodation","Accommodation"),CENTURIONHOUSE("Accommodation (Centurion House)","Accommodation (Centurion House)"),RESTAURANT("Restaurant","Restaurant");

	private FacilityTypeReport(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	private String code;
	private String desc;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
