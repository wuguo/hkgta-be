package com.sinodynamic.hkgta.util.constant;

public enum MemberType {

	IPM("IPM","1"),
	IDM("IDM","2"),
	CPM("CPM","3"),
	CDM("CDM","4"),
	MG("MG","5"),
	HG("HG","6");
	
		
	private MemberType(String type,String typeNO){
		this.type = type;
	}
	
	private String type;
	
	private String typeNO;
	
	public String getType(){
		return this.type;
	}
	
	public String getTypeNO() {
		return typeNO;
	}
	
	@Override
	public String toString() {
		return this.typeNO;
	}
	
}
