package com.sinodynamic.hkgta.util.response;

public class UsageRageData extends Data {
	
	private static final long serialVersionUID = 1L;
	private String usageRage;
	
	private String countNumber;

	public String getCountNumber() {
		return countNumber;
	}

	public void setCountNumber(String countNumber) {
		this.countNumber = countNumber;
	}

	public String getUsageRage() {
		return usageRage;
	}

	public void setUsageRage(String usageRage) {
		this.usageRage = usageRage;
	}
	
	

}
