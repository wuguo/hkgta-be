package com.sinodynamic.hkgta.util.constant;

public enum AddressType {
	BILL("Billing");

	private String desc;

	private AddressType(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
