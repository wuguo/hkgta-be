package com.sinodynamic.hkgta.util.constant;

public enum BiMemberCntPeriod {
	
	Monthly("M"), Daily("D"), Yearly("Y");
	
	private String desc;
	
	private BiMemberCntPeriod(String desc){
		this.desc = desc;
	}
	
	public String getDesc(){
		return this.desc;
	}
	

}
