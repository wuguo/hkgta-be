package com.sinodynamic.hkgta.util.constant;

public enum StaffType
{
	FGC("STAFF"),
	FTC("ADMIN"),
	SYSADMIN("SYSADMIN"),
	CUSTOMER("CUSTOMER");
	
	private String type;
	
	public String getType()
	{
		return type;
	}

	private StaffType(String type){
		this.type = type;
	}
}

