package com.sinodynamic.hkgta.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dom4j.Branch;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.sinodynamic.hkgta.util.encrypt.EncryptUtil;

public class TokenUtils
{
	private static final Logger LOG = LogManager.getLogger(TokenUtils.class);

	public static final String MAGIC_KEY = "obfuscate";

	public static final String AES_KEY = "sino";

	public static void main(String[] args)
	{
		System.out.println(TokenUtils.getUUIDToken("12345678901234567890123456789012345678901234567890", "Windows", "WP","",""));
	}
	
	private static String generateDESKey()
	{
		String part1 = DigestUtils.md5DigestAsHex(AES_KEY.getBytes());
		String part2 = DigestUtils.md5DigestAsHex(MAGIC_KEY.getBytes());
		return part1 + part2.substring(8);
	}

	public static String getToken(String clientInfo)
	{
		/* Expires in one hour */
		long minsToExpires = 60 * 1;
		long expiration = System.currentTimeMillis() + 1000L * 60 * minsToExpires;

		clientInfo = EncryptUtil.getInstance().AESencode(clientInfo, AES_KEY);

		return TokenUtils.getToken(clientInfo, expiration, minsToExpires);
	}

	public static String getUUIDToken(String clientInfo, String device, String location,String appVersion,String osVersion)
	{
		clientInfo = EncryptUtil.getInstance().DESencode(clientInfo, generateDESKey());
		return TokenUtils.getToken(clientInfo, device, location, UUID.randomUUID().toString().replace("-", ""),appVersion,osVersion);
	}

	public static String getToken(String clientInfo, String device, String uuid,String appVersion,String osVersion)
	{
		return getToken(clientInfo, device, "UNKNOWN", uuid,appVersion,osVersion);
	}
	
	public static String getToken(String clientInfo, String device, String location, String uuid,String appVersion,String osVersion)
	{
		Long timestamp = System.currentTimeMillis();
		StringBuilder tokenBuilder = new StringBuilder();
		tokenBuilder.append(clientInfo);
		tokenBuilder.append(":");
		tokenBuilder.append(timestamp);
		tokenBuilder.append(":");
		tokenBuilder.append(device);
		tokenBuilder.append(":");
		tokenBuilder.append(location);
		tokenBuilder.append(":");
		tokenBuilder.append(uuid);
		tokenBuilder.append(":");
		tokenBuilder.append(TokenUtils.computeSignature(clientInfo, timestamp, uuid));
		if(!StringUtils.isEmpty(appVersion))
		{
			tokenBuilder.append(":");
			tokenBuilder.append(appVersion);
		}
		if(!StringUtils.isEmpty(osVersion))
		{
			tokenBuilder.append(":");
			tokenBuilder.append(osVersion);
		}
		return tokenBuilder.toString();
	}

	public static String getToken(String clientInfo, Long expiration, Long minsToExpires)
	{
		StringBuilder tokenBuilder = new StringBuilder();
		tokenBuilder.append(clientInfo);
		tokenBuilder.append(":");
		tokenBuilder.append(expiration);
		tokenBuilder.append(":");
		tokenBuilder.append(TokenUtils.computeSignature(clientInfo, expiration));
		tokenBuilder.append(":");
		tokenBuilder.append(minsToExpires);
		return tokenBuilder.toString();
	}

	public static String computeSignature(String clientInfo, long expiration)
	{
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(clientInfo);
		signatureBuilder.append(":");
		signatureBuilder.append(expiration);
		signatureBuilder.append(":");
		signatureBuilder.append(TokenUtils.MAGIC_KEY);
		MessageDigest digest;
		try
		{
			digest = MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new IllegalStateException("No MD5 algorithm available!");
		}
		return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
	}

	public static String computeSignature(String clientInfo, long timestamp, String uuid)
	{
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(clientInfo);
		signatureBuilder.append(":");
		signatureBuilder.append(timestamp);
		signatureBuilder.append(":");
		signatureBuilder.append(uuid);
		signatureBuilder.append(":");
		signatureBuilder.append(TokenUtils.MAGIC_KEY);
		MessageDigest digest;
		try
		{
			digest = MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new IllegalStateException("No MD5 algorithm available!");
		}
		return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
	}

	public static boolean validateToken(String authToken)
	{
		String[] parts = authToken.split(":");
		long expires = Long.parseLong(parts[1]);
		String signature = parts[2];
		if (expires < System.currentTimeMillis())
		{
			LOG.warn("Auth-Token expired!");
			return false;
		}

		boolean valid = signature.equals(TokenUtils.computeSignature(parts[0], expires));
		if (!valid)
		{
			LOG.warn("Auth-Token has been changed!");
		}
		return valid;
	}

	public static boolean validateUUIDToken(String authToken)
	{
		String[] parts = authToken.split(":");
		if(parts.length==6||parts.length==8)
		{
			String uuid = parts[4];
			String signature = parts[5];

			boolean valid = signature.equals(TokenUtils.computeSignature(parts[0], Long.parseLong(parts[1]), uuid));
			if (!valid)
			{
				LOG.warn("Auth-Token has been changed!");
			}
			return valid;
		}
		return false;
	}

	public static String getUserNameFromToken(String authToken)
	{
		if (null == authToken)
		{
			return null;
		}
		String[] parts = authToken.split(":");
		return EncryptUtil.getInstance().DESdecode(parts[0], generateDESKey());
	}
	
	public static String getLocationFromUUIDToken(String authToken)
	{
		if (null == authToken)
		{
			return null;
		}
		String[] parts = authToken.split(":");
		return parts[3];
	}

}