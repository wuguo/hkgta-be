package com.sinodynamic.hkgta.util.constant;

public enum SurveyType {
	GOLF("Golfing Bay"), 
	TENNIS("Tennis Court"), 
	GSSC("Golf Course/Private Coaching"), 
	TSSC("Tennis Course/Private Coaching"),
	ACCOMMODATION("Accommodation"), 
	RESTAURANT("Food & Beverage"), 
	APP("Mobile App");
	
	private String desc;

	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	private SurveyType(String desc) {
		this.desc = desc;
	}

}
