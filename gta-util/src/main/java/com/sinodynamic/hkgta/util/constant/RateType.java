package com.sinodynamic.hkgta.util.constant;

public enum RateType {
	
	HIGH("HI"), LOW("LO");

		private String desc;

		private RateType(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

