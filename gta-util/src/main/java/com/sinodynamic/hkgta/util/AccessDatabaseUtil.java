package com.sinodynamic.hkgta.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.sinodynamic.hkgta.util.constant.AccessBatCmdType;
import com.sinodynamic.hkgta.util.constant.GTAError;
import com.sinodynamic.hkgta.util.exception.CommonException;
import com.sinodynamic.hkgta.util.http.HttpClientUtil;

/**
 * 
 * @Deprecated for using Jackcess library to insert MDB record method will be replaced by http call
 *
 */
@Deprecated
public class AccessDatabaseUtil {

	private static Logger logger = Logger.getLogger(AccessDatabaseUtil.class);
	
	private final static String Access_MDB_PATH = "access.path";
	
	private final static String TABLE_BATCMD = "BatCmd";
	
	private final static String TABLE_BATINFO = "BatInfo";
	
	private static String GOLF_MACHINE_IP;
	 
	private final static int MAX_BALLCOUNT = 9999;
	private final static int MAX_TIMECOUNT = 9999;
	
	private final static String HTTP_SUCCESS = "success";
	
	private static File accessFile = null;
	
	//private static SmartDatetimeFormat smartDatetimeFormat = new SmartDatetimeFormat();
	
			
	@Deprecated
	public static File getAccessFile()throws IOException{
		
		if(null != accessFile){
			return accessFile;
		}
		Resource resource = new ClassPathResource("placeholder/app.properties");
		Properties appProps = PropertiesLoaderUtils.loadProperties(resource);
		String path = appProps.getProperty(Access_MDB_PATH);
		accessFile = new File(path);
		return accessFile;
	}

	/**
	 * send  command to golfbay machine webservice 
	 * @param batNo
	 * @param startTime
	 * @param timeCount
	 * @param ballCount
	 * @param CmdType
	 * @throws Exception
	 */
	public static void sendBatchCmd(long batNo, String startTime, long timeCount, long ballCount, long CmdType) throws Exception {
		String params="cmd="
		          + batNo 
				  + ",'" + startTime +  "'," 
	              + timeCount + ","
	              + ballCount  
	              + ",0," // use Type is always 0
				  + CmdType;

		try {
			Resource resource = new ClassPathResource("placeholder/app.properties");
			Properties appProps = PropertiesLoaderUtils.loadProperties(resource);
			GOLF_MACHINE_IP = appProps.getProperty("golfmachine.host");			
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		
		logger.debug("send to golfmachine batCmd:" + params);		
		
		String result=HttpClientUtil.postForm(GOLF_MACHINE_IP + "/bayCmd", params);
		if(null!=result){
			if (!HTTP_SUCCESS.equals(result))
			    throw new Exception(result);
		}
	}
	
	@Deprecated	
	public static Boolean changeTeeupMachineStatsbyBatNo(Long batNo, String ballFeedingStatus, File accessFile)throws Exception{
		
		try {
			SmartDatetimeFormat smartDatetimeFormat = new SmartDatetimeFormat();
			String currentTime = smartDatetimeFormat.format(new Date());
			if ("OFF".equals(ballFeedingStatus)) {
				 sendBatchCmd(batNo, currentTime, 0,0, AccessBatCmdType.Finish.getCmd());
				} 
			else {
				 sendBatchCmd(batNo, currentTime, MAX_TIMECOUNT, MAX_BALLCOUNT , AccessBatCmdType.Checkin.getCmd());
				}

			return true;

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return false;
		}	
	}
	
	@Deprecated
	public static void setTeeupMachineStatsbyCheckinTime(Long batNo, Date startTime, Date endTime) throws Exception {
		
		File accessFile = AccessDatabaseUtil.getAccessFile();
		if(!accessFile.exists() || accessFile.isDirectory()){
			throw new CommonException(GTAError.FacilityError.ACCESSDATABASE_ISNULL,"");
		}	
		SmartDatetimeFormat smartDatetimeFormat = new SmartDatetimeFormat();
		Date currentTime = new Date();
		String currentTimeStr = smartDatetimeFormat.format(currentTime);
		Long timeDiff;

		if (currentTime.after(startTime)) {
			timeDiff = endTime.getTime() - currentTime.getTime();
			Long min = timeDiff / (1000 * 60);
			sendBatchCmd(batNo, currentTimeStr, min, MAX_BALLCOUNT , AccessBatCmdType.Checkin.getCmd());
		} 
		else {
			timeDiff = endTime.getTime() - startTime.getTime();
			Long min = timeDiff / (1000 * 60);
			String startTimeStr = smartDatetimeFormat.format(startTime);
			sendBatchCmd(batNo, startTimeStr, min, MAX_BALLCOUNT , AccessBatCmdType.Checkin.getCmd());
		}

	}
	
	@Deprecated
	public static Boolean setTeeupMachineCommand(Long batNo, String startTime, Long timeCount, Long ballCount, Long useType, Long cmdType, File accessFile)throws Exception{
		
		try {
//			Database database = new DatabaseBuilder().open(accessFile);
//			Table cmdTable = database.getTable(TABLE_BATCMD);
//			String currentTime = smartDatetimeFormat.format(new Date());			
//			cmdTable.addRow(currentTime, batNo, startTime, timeCount,
//					ballCount, useType, cmdType);
//			database.flush();
//			database.close();
			
			sendBatchCmd(batNo, startTime, timeCount, ballCount , cmdType);			
			return true;
		} catch (IOException ex) {
			// TODO: handle exception
			logger.error(ex.getMessage(), ex);
			return false;
		}
	}
	
	@Deprecated
	public Map<String, String> getTeeupMachineStatus()throws Exception{
		
		
		File file = AccessDatabaseUtil.getAccessFile();
		if(!file.exists() || file.isDirectory()){
			throw new CommonException(GTAError.FacilityError.ACCESSDATABASE_ISNULL,"");
		}	
		Database database = DatabaseBuilder.open(accessFile);
		Table cmdTable = database.getTable(TABLE_BATINFO);
		Map<String, String> statsMap = new HashMap<String, String>();
		
		for(Row row : cmdTable) {
			statsMap.put(row.get("BatNo").toString(), row.get("BatStat").toString());
		}
		if(null!=database){
			database.flush();
			database.close();
		}
		return statsMap;
	}
}
