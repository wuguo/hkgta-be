package com.sinodynamic.hkgta.util.constant;

public enum TopupAmountDistributionType {
	ONE("1 to 5,000",2,1,5000),TWO("5,001 to 10,000",2,5001,10000	),THREE("10,001 to 15,000",2, 10001,15000),FOUR(" > 15,000",3,15000,0);
	
	private String code;
	private int conditionsType;
	private int startRateNum;
	private int endRateNum;
	
	public int getStartRateNum() {
		return startRateNum;
	}

	public void setStartRateNum(int startRateNum) {
		this.startRateNum = startRateNum;
	}

	public int getEndRateNum() {
		return endRateNum;
	}

	public void setEndRateNum(int endRateNum) {
		this.endRateNum = endRateNum;
	}

	private TopupAmountDistributionType(String code, int conditionsType, int startRateNum, int endRateNum)
	{
		this.code = code;
		this.conditionsType = conditionsType;
		this.startRateNum = startRateNum;
		this.endRateNum = endRateNum;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getConditionsType() {
		return conditionsType;
	}

	public void setConditionsType(int conditionsType) {
		this.conditionsType = conditionsType;
	}

	

	
	
}
