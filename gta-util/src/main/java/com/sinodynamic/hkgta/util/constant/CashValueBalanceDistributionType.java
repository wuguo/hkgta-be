package com.sinodynamic.hkgta.util.constant;

public enum CashValueBalanceDistributionType {
	ONE("0 to 10,000",2,0,10000),TWO("10,000 to 30,000",2,10000,30000),THREE("30,000 to 50,000",2, 30000,50000),FOUR(" > 50,000",3,50000,0);
	
	private String code;
	private int conditionsType;
	private int startRateNum;
	private int endRateNum;
	
	public int getStartRateNum() {
		return startRateNum;
	}

	public void setStartRateNum(int startRateNum) {
		this.startRateNum = startRateNum;
	}

	public int getEndRateNum() {
		return endRateNum;
	}

	public void setEndRateNum(int endRateNum) {
		this.endRateNum = endRateNum;
	}

	private CashValueBalanceDistributionType(String code, int conditionsType, int startRateNum, int endRateNum)
	{
		this.code = code;
		this.conditionsType = conditionsType;
		this.startRateNum = startRateNum;
		this.endRateNum = endRateNum;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getConditionsType() {
		return conditionsType;
	}

	public void setConditionsType(int conditionsType) {
		this.conditionsType = conditionsType;
	}

	

	
	
}
