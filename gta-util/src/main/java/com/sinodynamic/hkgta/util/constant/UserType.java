package com.sinodynamic.hkgta.util.constant;

public enum UserType
{
	STAFF("STAFF"),
	ADMIN("ADMIN"),
	CUSTOMER("CUSTOMER");
	
	private String type;
	
	public String getType()
	{
		return type;
	}

//	public void setType(String type)
//	{
//		this.type = type;
//	}

	private UserType(String type){
		this.type = type;
	}
}
