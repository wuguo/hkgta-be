package com.sinodynamic.hkgta.util.constant;

public enum StaffMasterOrderByCol {

	STAFFID,
	STAFFNAME,
	STAFFTYPE,
	STATUS,
	POSITIONTITLE,
	EMPLOYEEDATE,
	BOOKINGS,
	HIGHPRICE,
	LOWPRICE,
	QUITDATE;
	
}
