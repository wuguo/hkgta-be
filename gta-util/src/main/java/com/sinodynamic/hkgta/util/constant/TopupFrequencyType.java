package com.sinodynamic.hkgta.util.constant;

public enum TopupFrequencyType {
//	ONE("1 to 10",1,10),TWO("11 to 20",11,20),THREE("21 to 30", 21,30 ),FOUR(" > 30",31,99999);//99999不可能达到的数字，相当于大于30的区间
	ONE(" 0 ",0,10),TWO(" 1 ",1,1),THREE(" 2 ", 2,2 ),FOUR(" 3+ ",3,99999);//99999不可能达到的数字，相当于大于30的区间
	private String code;
	private int startRateNum;
	private int endRateNum;
	
	public int getStartRateNum() {
		return startRateNum;
	}

	public void setStartRateNum(int startRateNum) {
		this.startRateNum = startRateNum;
	}

	public int getEndRateNum() {
		return endRateNum;
	}

	public void setEndRateNum(int endRateNum) {
		this.endRateNum = endRateNum;
	}

	private TopupFrequencyType(String code, int startRateNum, int endRateNum)
	{
		this.code=code;
		this.startRateNum = startRateNum;
		this.endRateNum = endRateNum;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	
	
}
