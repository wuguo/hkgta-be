package com.sinodynamic.hkgta.util.constant;

public enum RoomServiceStatus {
	 	NRM("normal"),OOS("out of service");
	
		private String desc;

		private RoomServiceStatus(String desc) {
			this.desc = desc;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

