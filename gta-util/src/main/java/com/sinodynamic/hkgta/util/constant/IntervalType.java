package com.sinodynamic.hkgta.util.constant;

public enum IntervalType {
	ONE("0-1 Days","0-20%",0,1),TWO("2-3 Days","21-40%",2,3),THREE("4-6 Days","41-60%", 4,6),FOUR("7-10 Days","61-80%",7,10),FIVES("11-15 Days","81-100%",11,15), SIX("16-20 Days","81-100%",16,20), SEVEN("21-30 Days","81-100%",21,30);
	
	private String code;
	private String device;
	private int startRateNum;
	private int endRateNum;
	
	public int getStartRateNum() {
		return startRateNum;
	}

	public void setStartRateNum(int startRateNum) {
		this.startRateNum = startRateNum;
	}

	public int getEndRateNum() {
		return endRateNum;
	}

	public void setEndRateNum(int endRateNum) {
		this.endRateNum = endRateNum;
	}

	private IntervalType(String code,String device, int startRateNum, int endRateNum)
	{
		this.code=code;
		this.device=device;
		this.startRateNum = startRateNum;
		this.endRateNum = endRateNum;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	
	
}
