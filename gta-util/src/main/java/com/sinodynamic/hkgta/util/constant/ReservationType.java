package com.sinodynamic.hkgta.util.constant;

public enum ReservationType {
	GMS("GBF","Golf bay"),
	GSS("GCE","Golf course"),
	GCH("GPC","Golf private coaching"),
	TMS("TCF","Tennis court"),
	TSS("TCE","Tennis course"),
	TCH("TPC","Tennis private coaching"),
	
	ROOM("GRM","Room"),
	WELLNESS("SPA","Wellness")	;
	
	private ReservationType(String name, String desc){
		this.name = name;
		this.desc = desc;
	}
	
	private String desc;
	private String name;
	
	public String getDesc(){
		return this.desc;
	}
	
	public String getName()
	{
		return this.name;
	}
}
