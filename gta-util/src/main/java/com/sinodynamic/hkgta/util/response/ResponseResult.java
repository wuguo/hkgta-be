package com.sinodynamic.hkgta.util.response;

import java.io.Serializable;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.sinodynamic.hkgta.util.ResponseMsg;
import com.sinodynamic.hkgta.util.constant.GTAError;

/**
 * Common class for storing searching result including paging info
 * 
 * @author Mianping_Wu
 *
 */
@Component
@Scope("prototype")
public class ResponseResult extends ResponseMsg implements Serializable {

	private static final long serialVersionUID = 1L;

	private Data listData;	
	private Object dto;
	
	public ResponseResult() {
		super();
	}   
	
	public ResponseResult(MessageSource messageSource) {
		super(messageSource);
	}  
	
	/**
	 * Use ResponseResult(GTAError error) instead.
	 * */
	@Deprecated
	public ResponseResult(String returnCode, String errorMessageEN) {
		super(returnCode,errorMessageEN);
	}
	
	public ResponseResult(Map<String,String> error) {
		super(error);
	}
	
	@Deprecated
	public ResponseResult(String returnCode, String errorMessageTC, String errorMessageEN) {
		super(returnCode,errorMessageEN);
	}

	@Deprecated
	public ResponseResult(ResponseMsg msg,Object dto){
		this.returnCode = msg.getReturnCode();
		this.errorMessageEN = msg.getErrorMessageEN();
		this.dto = dto;
	}

	@Deprecated
	public ResponseResult(ResponseMsg msg,Data data){
		this.returnCode = msg.getReturnCode();
		this.errorMessageEN = msg.getErrorMessageEN();
		this.listData = data;
	}
	
	/**
	 * Use initErrorMsg(GTAError error) instead
	 * */
	@Deprecated
	public ResponseResult(String returnCode, String errorMessageEN, Object dto) {
		super(returnCode, errorMessageEN);
		this.dto = dto;
	}
	
	/**
	 * Use ResponseResult(GTAError error, Data data) instead
	 * */
	@Deprecated
	public ResponseResult(String returnCode, String errorMessageEN, Data data) {
		super(returnCode,errorMessageEN);
		this.listData = data;
	}
	
	@Deprecated
	public ResponseResult(String returnCode, String errorMessageTC, String errorMessageEN, Object dto) {
		super(returnCode, errorMessageEN);
		this.dto = dto;
	}
	
	@Deprecated
	public ResponseResult(String returnCode, String errorMessageTC, String errorMessageEN, Data data) {
		super(returnCode,errorMessageEN);
		this.listData = data;
	}

	public Object getData() {
		if (dto != null) {
			return dto;
		} else {
			return listData;
		}
	}
	
	@JsonIgnore
	public Data getListData() {
		return listData;
	}
	
	@JsonIgnore
	public Object getDto() {
		return dto;
	}

	public void setData(Object dto) {
		this.dto = dto;
	}

	public void setData(Data data) {
		this.listData = data;
	}
	
	/**
	 * Initial the response result with error message and data
	 * */
	@Override
	public void initResult(GTAError error)
	{
		super.initResult(error);
		this.listData = null;
		this.dto = null;
	}
	
	@Override
	public void initResult(GTAError error, String errorMessage)
	{
		super.initResult(error, errorMessage);
		this.listData = null;
		this.dto = null;
	}
	
	@Override
	public void initResult(GTAError[] errors)
	{
		super.initResult(errors);
		this.listData = null;
		this.dto = null;
	}
	
	@Override
	public void initResult(GTAError error, Object[] args)
	{
		super.initResult(error, args);
		this.listData = null;
		this.dto = null;
	}
	
	/**
	 * Initial the response result with error message and data
	 * */
	public void initResult(GTAError error, Data data)
	{
		super.initResult(error);
		this.listData = data;
		this.dto = null;
	}
	
	/**
	 * Initial the response result with error message and data
	 * */
	public void initResult(GTAError error, Object dto)
	{
		super.initResult(error);
		this.dto = dto;
		this.listData = null;
	}
	
	/**
	 * Initial the response result with error message and data
	 * */
	public void initResult(GTAError error, Object[] args, Data data)
	{
		super.initResult(error, args);
		this.listData = data;
	}
	
	/**
	 * Initial the response result with error message and data
	 * */
	public void initResult(GTAError error, Object[] args, Object dto)
	{
		super.initResult(error, args);
		this.dto = dto;
	}

	
}