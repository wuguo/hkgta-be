package com.sinodynamic.hkgta.util;

import java.util.HashMap;

import com.sinodynamic.hkgta.util.CallBackExecutor.CallBack;
import com.sinodynamic.hkgta.util.CallBackExecutor.MethodPart;
import com.sinodynamic.hkgta.util.exception.GTACommonException;

public abstract class AbstractCallBack implements CallBack {
	private HashMap<String,Object> contextValues = null;
	
	@Override
	public void doCatch() throws Exception {
	}

	@Override
	public void doFinally() {
	}

	@Override
	public Exception getException() {
		Exception ex = (Exception) this.getContextValues().get(CallBackExecutor.EXCEPTION_KEY);
		return ex;
	}

	@Override
	public GTACommonException throwException(MethodPart methodPart) {
		switch (methodPart){
		case TRY: 
			return newTryException();
		case CATCH:
			return newCatchException();
		}
		return null;
	}

	public String[] getErrorValues(MethodPart methodPart) {
		return null;
	}
	
	protected GTACommonException newTryException(){
		return null;
	}
	
	protected GTACommonException newCatchException(){
		return null;
	}

	@Override
	public void setContextValues(HashMap<String, Object> contextValues) {
		this.contextValues = contextValues;
	}
	
	@Override
	public HashMap<String, Object> getContextValues() {
		return contextValues;
	}
}
