package com.sinodynamic.hkgta.util.constant;

import java.util.HashMap;

/**
 * 
 * @author Mianping_Wu
 * @since 5/11/2015
 */
public enum ChargeType {

	CHARGE("4600", "CHARGE"), REFUND("8880", "REFUND");

	private String								code;
	private String								description;
	private static HashMap<String, ChargeType>	map;

	static {

		map = new HashMap<String, ChargeType>();
		for (ChargeType status : ChargeType.values()) {
			map.put(status.getCode(), status);
		}
	}

	private ChargeType(String code, String desc) {

		this.code = code;
		this.description = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static boolean isValid(String code) {

		ChargeType status = map.get(code);
		if (status == null)
			return false;
		return true;
	}

}
