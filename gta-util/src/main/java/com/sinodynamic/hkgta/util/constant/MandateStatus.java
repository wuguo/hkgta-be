package com.sinodynamic.hkgta.util.constant;

public enum MandateStatus {
	A("A","Accepted"),//add 
	R("R","AMEND"),//Override
	C("C","CANCEL");//cancle
	private String code;
	private String desc;
	
	private MandateStatus(String code,String desc){
		this.code=code;
		this.desc=desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
