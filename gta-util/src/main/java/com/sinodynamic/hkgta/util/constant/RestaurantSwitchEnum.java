package com.sinodynamic.hkgta.util.constant;

public enum RestaurantSwitchEnum {
	
		AllDayDiningAlertSwitch("0001","CAF"),BarAndLoungeAlertSwitch("0002","BAR"),ChineseRestaurantAlertSwitch("0003","RES"),SaktiDiningAlertSwitch("0004","SKD"),RoyalTeaLoungeAlertSwitch("0005","RTL");
	
		private String desc;
		
		private String reservId;
		
		private RestaurantSwitchEnum(String desc,String reservId) {
			this.desc = desc;
			this.reservId=reservId;
		}

		public String getReservId() {
			return reservId;
		}

		public void setReservId(String reservId) {
			this.reservId = reservId;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

