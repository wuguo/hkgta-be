package com.sinodynamic.hkgta.util.response;

import java.io.Serializable;

/**
 * Common class for storing paging parameter and response paging info
 * @author Mianping_Wu
 *
 */
public class NetValue implements Serializable {

	private static final long serialVersionUID = 1L;

	private String visaNet;
	private String masterNet;
	private String amexNet;
	private String cupNet;
	private String totalCreditCardNet;
	private String totalNet;
	private String cashValueNet;
	private String cashNet;
	private String otherNet;
	private String jcbNet;
	public String getVisaNet() {
		return visaNet;
	}
	public void setVisaNet(String visaNet) {
		this.visaNet = visaNet;
	}
	public String getMasterNet() {
		return masterNet;
	}
	public void setMasterNet(String masterNet) {
		this.masterNet = masterNet;
	}
	public String getAmexNet() {
		return amexNet;
	}
	public void setAmexNet(String amexNet) {
		this.amexNet = amexNet;
	}
	public String getCupNet() {
		return cupNet;
	}
	public void setCupNet(String cupNet) {
		this.cupNet = cupNet;
	}
	public String getTotalCreditCardNet() {
		return totalCreditCardNet;
	}
	public void setTotalCreditCardNet(String totalCreditCardNet) {
		this.totalCreditCardNet = totalCreditCardNet;
	}
	public String getTotalNet() {
		return totalNet;
	}
	public void setTotalNet(String totalNet) {
		this.totalNet = totalNet;
	}
	public String getCashValueNet() {
		return cashValueNet;
	}
	public void setCashValueNet(String cashValueNet) {
		this.cashValueNet = cashValueNet;
	}
	public String getCashNet() {
		return cashNet;
	}
	public void setCashNet(String cashNet) {
		this.cashNet = cashNet;
	}
	public String getOtherNet() {
		return otherNet;
	}
	public void setOtherNet(String otherNet) {
		this.otherNet = otherNet;
	}
	public String getJcbNet() {
		return jcbNet;
	}
	public void setJcbNet(String jcbNet) {
		this.jcbNet = jcbNet;
	}
	@Override
	public String toString() {
		return "NetValue [visaNet=" + visaNet + ", masterNet=" + masterNet + ", amexNet=" + amexNet + ", cupNet=" + cupNet + ", totalCreditCardNet="
				+ totalCreditCardNet + ", totalNet=" + totalNet + ", cashValueNet=" + cashValueNet + ", cashNet=" + cashNet + ", otherNet=" + otherNet
				+ ", jcbNet=" + jcbNet + "]";
	}

	
}
