package com.sinodynamic.hkgta.util;

import org.apache.commons.lang.StringUtils;

import com.sinodynamic.hkgta.util.constant.Constant;

/***
 * 
 * @author christ
 * @time 2016-07-11
 * @desc the thrid call api logger
 *
 */
public class Log4jFormatUtil {

	private static final String IDENFYING="||";
	private static final String SUCCESS="success";
	private static final String SEND="send";
	private static final String FAIL="failure";
	private static final String REQUEST="request: ";
	private static final String RESPONSE="response: ";
	
	/***
	 * //TOMCATID||SUCCESS||requestUrl||METHOD||INFO||
	 * request:xx
	 * response:xxx
	 * @param requestUrl
	 * @param responseMsg
	 * @return
	 */
	/***
	 * 
	 * @param requestUrl
	 * @param reqeust
	 * @param method
	 * @param responseMsg
	 * @return
	 */
	public static String logInfoMessage(String requestUrl,String reqeust,String method,
			String responseMsg) {
		StringBuilder loggDer = new StringBuilder();
		if(StringUtils.isNotEmpty(reqeust)&&null==responseMsg)
		{
			loggDer.append(SEND);
		}else{
			loggDer.append(SUCCESS);	
		}
		loggDer.append(IDENFYING);
		if(StringUtils.isNotEmpty(requestUrl)){
			loggDer.append(requestUrl);
			loggDer.append(IDENFYING);
		}
		if(StringUtils.isNotEmpty(method)){
			loggDer.append(method);
			loggDer.append(IDENFYING);
		}
		if(StringUtils.isNotEmpty(reqeust)){
			loggDer.append(REQUEST+reqeust);
			loggDer.append(IDENFYING);
		}
		if(StringUtils.isNotEmpty(responseMsg)){
			loggDer.append(RESPONSE+responseMsg);
		}
		return loggDer.toString();
	}
	/*** //TOMCATID||SUCCESS||requestUrl||METHOD||ERROR
	 * request:
	 * response:
	 * exception:
	 * @param requestUrl 
	 * @param reqeust
	 * @param method
	 * @param responseMsg
	 * @param exception
	 * @return
	 */
	public static String logErrorMessage(String requestUrl,String reqeust,String method,String responseMsg,String exception) {
		StringBuilder loggDer = new StringBuilder();
		loggDer.append(FAIL);
		loggDer.append(IDENFYING);
		if(StringUtils.isNotEmpty(requestUrl)){
			loggDer.append(requestUrl);
			loggDer.append(IDENFYING);
		}
		if(StringUtils.isNotEmpty(method)){
			loggDer.append(method);
			loggDer.append(IDENFYING);
		}
		if(StringUtils.isNotEmpty(reqeust)){
			loggDer.append(REQUEST+reqeust);
			loggDer.append(IDENFYING);
		}
		if(StringUtils.isNotEmpty(responseMsg)){
			loggDer.append(RESPONSE+responseMsg);
			loggDer.append(IDENFYING);
		}
		if(StringUtils.isNotEmpty(exception)){
			loggDer.append(exception);
		}
		return loggDer.toString();
	}

	public static String logStaffEmailInfo(boolean Success, String sendDate,String senderUserId,String recipientUserId, String recipientEmail, String noticeType, String subject) {
		StringBuilder loggDer = new StringBuilder();
		if(Success){
			loggDer.append(SUCCESS);
		}else{
			loggDer.append(FAIL);
		}
		loggDer.append(IDENFYING);
		// SGG-2810 the send time stamp should be human readable instead of unix time. i.e. yyyy-mm-dd hh:mm:ss
		if(StringUtils.isNotEmpty(sendDate)){
			loggDer.append("sendDate:" + sendDate);
			loggDer.append(IDENFYING);
		}
		// SGG-2810  missing sender user id. If it is sent by system, log [sysAdmin001]
		if(!StringUtils.isNotEmpty(senderUserId)){
			senderUserId = "[sysAdmin001]";
		}
		loggDer.append("senderUserId:" + senderUserId);
		loggDer.append(IDENFYING);
		if(StringUtils.isNotEmpty(recipientUserId)){
			loggDer.append("recipientUserId:" + recipientUserId);
			loggDer.append(IDENFYING);
		}
		if(StringUtils.isNotEmpty(recipientEmail)){
			loggDer.append("recipientEmail:" + recipientEmail);
			loggDer.append(IDENFYING);
		}
		// SGG-2810 missing notice type
		if(!StringUtils.isNotEmpty(noticeType)){
			noticeType = Constant.NOTICE_TYPE_STAFF;
		}
		loggDer.append("noticeType:" + noticeType);
		loggDer.append(IDENFYING);
		if(StringUtils.isNotEmpty(subject)){
			loggDer.append("subject:" + subject);
		}
		return loggDer.toString();
	}
}
