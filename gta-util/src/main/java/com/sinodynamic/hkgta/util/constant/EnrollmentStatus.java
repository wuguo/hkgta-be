package com.sinodynamic.hkgta.util.constant;

public enum EnrollmentStatus {
	// NEW- NEW Enrollment, ANC-Active(No Card), CMP-Complete, PYF-Payment Fail,REJ-Reject,CAN-Cancel,APV-approve
	   NEW("NEW"), ANC("ANC"), CMP("CMP"), PYF("PYF"),REJ("REJ"), CAN("CAN"), OPN("OPN"),APV("APV");

		private String desc;

		private EnrollmentStatus(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}
