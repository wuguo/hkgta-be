package com.sinodynamic.hkgta.util.constant;

public enum CourseType {
    GOLFCOURSE("GSS"), TENNISCOURSE("TSS");
    private String desc;

    private CourseType(String desc) {
	this.desc = desc;
    }

    @Override
    public String toString() {
	return desc;
    }
    
    public String getDesc(){
    	return desc;
    }

}
