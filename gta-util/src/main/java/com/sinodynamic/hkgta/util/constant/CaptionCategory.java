package com.sinodynamic.hkgta.util.constant;

public enum CaptionCategory {

	CNSGH("CNS-GH"), CNSKID("CNS-KID"), CNSFB("CNS-FB"), CNSSPA("CNS-SPA"), CNSGF("CNS-GF"), CNSTNS("CNS-TNS");

	private String desc;

	private CaptionCategory(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
