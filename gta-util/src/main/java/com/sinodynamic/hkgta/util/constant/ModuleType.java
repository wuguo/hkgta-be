package com.sinodynamic.hkgta.util.constant;

public enum ModuleType {
	CRM("CRM"), UAT("UAT");

	private ModuleType(String desc) {
		this.desc = desc;
	}

	private String desc;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return super.name();
	}

}
