package com.sinodynamic.hkgta.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.helpers.LogLog;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class DDXAppender extends DailyRollingFileAppender {
	Date now = new Date();  
	SimpleDateFormat sdf;  
	/**"文件名+上次最后更新时间"*/  
	private String scheduledFilename; 
	/**不允许改写的datepattern */  
	private final String datePattern = "'.'yyyy-MM-dd";  
	private void init() {
		Resource res = new ClassPathResource("placeholder/dda_i.properties");
		Properties ddxProp = new Properties();
		try {
			ddxProp.load(res.getInputStream());
			String fileName = (String)ddxProp.get("ddx.log.dir");
			super.setFile(fileName);  
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	
	/** 
	   * 初始化本Appender对象的时候调用一次 
	   */  
	  public void activateOptions() {  
		init();
	    super.activateOptions();  
	    if(fileName != null) { //perf.log  
	      now.setTime(System.currentTimeMillis());  
	      sdf = new SimpleDateFormat(datePattern);  
	      File file = new File(fileName);  
	      //获取最后更新时间拼成的文件名  
	      scheduledFilename = fileName+sdf.format(new Date(file.lastModified()));  
	    } else {  
	      LogLog.error("File is not set for appender ["+name+"].");  
	    }  
	     
	  }  
	  
	
	

}