package com.sinodynamic.hkgta.util.constant;

public enum PlanRight {
	
	D1("Day Pass Purchasing"),
	G1("General Right"),
	EVENT1("Events Right"),
	TRAIN1("Training Right"),
	TRAIN2("Training Right"),
	TRAIN3("Training Right"),
	TRAIN4("Training Right");
	
	private String type;

	private PlanRight(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	

}
