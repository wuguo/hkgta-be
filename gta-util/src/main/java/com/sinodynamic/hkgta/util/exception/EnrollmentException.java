package com.sinodynamic.hkgta.util.exception;

public class EnrollmentException extends Exception {
	private static final long serialVersionUID = 1L;

	private Object errorCode;

	private String errorMessage;

	private Object[] args;

	public EnrollmentException()
	{
	}

	public EnrollmentException(Throwable e)
	{
		super(e);
	}

	public EnrollmentException(Object errorCode, String errorMessage)
	{
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public EnrollmentException(Object errorCode, String errorMessage, Object[] args)
	{
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.args = args;
	}

	public EnrollmentException(Object errorCode, String errorMessage, Throwable e)
	{
		super(e);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public Object getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(Object errorCode)
	{
		this.errorCode = errorCode;
	}

	public Object[] getArgs()
	{
		return args;
	}

	public void setArgs(Object[] args)
	{
		this.args = args;
	}

}
