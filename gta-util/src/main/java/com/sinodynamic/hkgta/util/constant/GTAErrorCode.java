package com.sinodynamic.hkgta.util.constant;

/**
 * 
 * 
 * Error Code 
 * @author Vian Tang
 * 
 * */
@Deprecated
public enum GTAErrorCode
{
	SUCCESS("0"),
	
	/*
	 * This is for login and logout function
	 * */
	LOGIN_SUCCESS("1010100"),
	
	LOGIN_FAILED("9010101"),
	
	TOKEN_EXPIRED("9010102"),
	
	TOKEN_INVALID("9010103"),
	
	AUTHENTICATE_REQRIRED("9010104"),
	
	TOKEN_NOT_FOUND("9010105"),
	
	SAVE_SESSION_FAILED("9010106"),
	
	/**
	 * This is for MessageTemplateController
	 */
	MESSAGE_TEMPLATE_PARAMETER_ERROR("9020101"),
	
	MESSAGE_TEMPLATE_TYPE_ERROR("9020102"),
	
	MESSAGE_TEMPLATE_CUSTOMERID_ERROR("9020103"),
	
	MESSAGE_TEMPLATE_NO_CURRENTUSER("9020104"),
	
	MESSAGE_TEMPLATE_OTHER_ERROR("9020105"), 
	
	
	/**
	 * 
	 * Blow ERROR CODE for DayPassPurchaseController
	 */
	PERIOD_DATE_NOT_ALLOW("9130101"),
	HAVE_NOT_GET_PURCHASE_RIGHT("9130102"),
	DAY_PASS_CHECK_ERROR("9130103"),
	
	/**
	 * 
	 * Blow ERROR CODE for AdvanceQueryController
	 */
	NO_ADVANCE_CONDITION_OFFER("9990101"),
	CARD_MAPPING_FAILED("9990201"), 
	CARD_NOT_NUMBERRIC("9990202"), 
	;
	public String getCode()
	{
		return code;
	}

	private String code;
	
	
	private GTAErrorCode(String code){
		this.code = code;
	}
}
