package com.sinodynamic.hkgta.util.constant;

public enum PaymentMethod {
	VISA("Visa","VI"),
	MASTER("Master","MC"),
	CASH("Cash","CASH"),
	CASHVALUE("CashValue","CASHVALUE"),
	BT("Bank Transfer","BT"),
	CHEQUE("Cheque","CHEQUE"),
	VAC("Virtual Account Transfer","VAC"),
	CUP("Union Pay","CU"),
	OTH("Others","OTH"),
	PREAUTH("Payment Upon Arrival","PREAUTH"),
	CHRGPOST("Charge Posting","CHRGPOST"), 
	REFUND("REFUND","REFUND"),
	AMEX("American Express","AMEX"),
	AX("Oasis American Express","AX"),
	DDI("Direct Debit","DDI"),
	JCB("JCB","JB"),
	DINERSCLUB("DINERSCLUB","DN");
	
	private String desc;

	private String cardCode ;
	
	
	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	private PaymentMethod(String desc,String cardCode ) {
		
		this.desc = desc;
		this.cardCode=cardCode;
	}
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
