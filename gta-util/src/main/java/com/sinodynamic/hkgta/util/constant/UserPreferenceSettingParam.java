package com.sinodynamic.hkgta.util.constant;

public enum UserPreferenceSettingParam {
	
	NOTIFICATION("SMS_NOTIFICATION"),
	ALLDAYDINING("AllDayDiningAlertSwitch");
	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	private UserPreferenceSettingParam(String type){
		this.type=type;
	}
	
}
