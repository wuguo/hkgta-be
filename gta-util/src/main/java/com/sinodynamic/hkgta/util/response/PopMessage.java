package com.sinodynamic.hkgta.util.response;

import java.io.Serializable;

public class PopMessage implements Serializable {
	private Boolean pop;
	private String step1;
	private String step2;
	public Boolean getPop() {
		return pop;
	}
	public void setPop(Boolean pop) {
		this.pop = pop;
	}
	public String getStep1() {
		return step1;
	}
	public void setStep1(String step1) {
		this.step1 = step1;
	}
	public String getStep2() {
		return step2;
	}
	public void setStep2(String step2) {
		this.step2 = step2;
	}
}
