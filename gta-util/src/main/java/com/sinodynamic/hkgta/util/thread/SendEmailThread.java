package com.sinodynamic.hkgta.util.thread;

import java.util.List;

import org.apache.log4j.Logger;

import com.sinodynamic.hkgta.util.MailSender;

public class SendEmailThread implements Runnable {
	private Logger logger = Logger.getLogger(SendEmailThread.class);
	
	private String to;
	private String copyto;
	private String blindCopyTo;
	private String subject;
	private String content;
	private List<byte[]> bytesList;
	private List<String> fileNameList;
	private List<String> mineTypeList;

	public SendEmailThread() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SendEmailThread(String to, String copyto, String blindCopyTo, String subject, String content,
			List<byte[]> bytesList, List<String> fileNameList,List<String> mineTypeList) {
		super();
		this.to = to;
		this.copyto = copyto;
		this.blindCopyTo = blindCopyTo;
		this.subject = subject;
		this.content = content;
		this.bytesList = bytesList;
		this.fileNameList = fileNameList;
		this.mineTypeList = mineTypeList;
	}

	@Override
	public void run() {
		try {
			MailSender.sendEmailWithBytesMineAttach(to,copyto,blindCopyTo,subject,content,mineTypeList,bytesList,fileNameList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.debug(e.getMessage());
		}
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCopyto() {
		return copyto;
	}

	public void setCopyto(String copyto) {
		this.copyto = copyto;
	}

	public String getBlindCopyTo() {
		return blindCopyTo;
	}

	public void setBlindCopyTo(String blindCopyTo) {
		this.blindCopyTo = blindCopyTo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<byte[]> getBytesList() {
		return bytesList;
	}

	public void setBytesList(List<byte[]> bytesList) {
		this.bytesList = bytesList;
	}

	public List<String> getFileNameList() {
		return fileNameList;
	}

	public void setFileNameList(List<String> fileNameList) {
		this.fileNameList = fileNameList;
	}
	
}
