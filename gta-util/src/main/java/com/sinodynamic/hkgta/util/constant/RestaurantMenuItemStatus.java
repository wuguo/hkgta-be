package com.sinodynamic.hkgta.util.constant;

public enum RestaurantMenuItemStatus {

	H("Hidden"), S("Show"), D("Deleted");

	private String desc;

	private RestaurantMenuItemStatus(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
