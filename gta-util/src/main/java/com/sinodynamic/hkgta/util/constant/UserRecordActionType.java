package com.sinodynamic.hkgta.util.constant;

public enum UserRecordActionType {
	
	A("Create"),C("Update"),D("Delete"),R("read");
	
	private String desc;

	private UserRecordActionType(String desc){
		this.desc=desc;
	}
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	

}
