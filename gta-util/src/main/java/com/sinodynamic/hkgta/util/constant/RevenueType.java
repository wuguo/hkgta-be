package com.sinodynamic.hkgta.util.constant;

public enum RevenueType {
	
	ADMISSION("Admission Sales","Admission Sales"),
	FACILITY("Facilities","Facilities"),
	SCHOOL("School","School"),
	SAKTI("Sakti", "FIVELEME"), 
	ALLDAYDINING("ADD", "ADD"), 
	BARLOUNGE("Bar & Lounge","LOUNGE"), 
//	TEALOUNGE("tea & Lounge", "FAN GUAN"),
	PROSHOP("Pro Shop","PROSHOP"),
	FANGUAN("Fan Guan","FAN GUAN"),
	ACCOMMODATION("Accommodation","'ROOMS','MINIBAR'"),
	RECREATION("Recreation","RECREATI"),
	TEALOUNGE("Tea Lounge","FIVELEME"),
	BANQUET("Banquet","BANQUET"),
	ROOMSERVICE("Room Service","ROOM SER");
	
	
	
	private String name;
	
	private String party;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParty() {
		return party;
	}

	public void setParty(String party) {
		this.party = party;
	}

	private RevenueType(String name, String party) {
		this.name = name;
		this.party = party;
	}
}
