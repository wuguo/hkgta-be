package com.sinodynamic.hkgta.util.constant;

public enum RoomCrewRoleType {
	
	 MGR("MANAGER"), ISP("INSPECTOR"), SPV("SUPERVISOR"), RA("ROOMATTENDANT"), NGR("ENGINEER");
	
		private String desc;

		private RoomCrewRoleType(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

