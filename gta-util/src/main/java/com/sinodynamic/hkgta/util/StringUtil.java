package com.sinodynamic.hkgta.util;

public class StringUtil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	  public final static String captureName(String name) {
		   //     name = name.substring(0, 1).toUpperCase() + name.substring(1);
//		        return  name;
		        char[] cs=name.toCharArray();
		        cs[0]-=32;
		        return String.valueOf(cs);
		        
		    }
	  
	  /**
		 * 把字符串转换为双精度型数字
		 * 
		 * @param a
		 *            String 要转换的字符串
		 * @return double 返回一个双精度数
		 */
		public static double stringtodouble(String a) {
			double b = 0.0;
			int l = a.length();
			int p = a.indexOf(".");
			// 要分成有小数和无小数两部分
			double c = 0.0;
			// 没有小数点时，小数点的索引为-1
			if (p == -1 && a.charAt(0) != '-') // 正整数
			{
				for (int i = 0; i < l; i++) {
					c = chartonumber(a.charAt(i));
					for (int j = 0; j < l - i - 1; j++)
						c *= 10.0;
					b += c;
				}
			} else if (p == -1 && a.charAt(0) == '-') // 负整数
			{
				for (int i = 1; i < l; i++) {
					c = chartonumber(a.charAt(i));
					for (int j = 0; j < l - i - 1; j++)
						c *= 10.0;
					b += c;
				}
				b *= -1;
			} else if (p != -1 && a.charAt(0) != '-') // 正小数
			{
				for (int i = 0; i < l; i++) {
					if (i < p) {
						c = chartonumber(a.charAt(i));
						for (int j = 0; j < p - i - 1; j++)
							c *= 10.0;
						b += c;
					}
					if (i > p) {
						c = chartonumber(a.charAt(i));
						for (int m = 0; m < i - p; m++)
							c /= 10.0;
						b += c;
					}
				}
			} else // 负小数
			{
				for (int i = 1; i < l; i++) {
					if (i < p) {
						c = chartonumber(a.charAt(i));
						for (int j = 0; j < p - i - 1; j++)
							c *= 10.0;
						b += c;
					}
					if (i > p) {
						c = chartonumber(a.charAt(i));
						for (int m = 0; m < i - p; m++)
							c /= 10.0;
						b += c;
					}
				}
				b *= -1;
			}
			return b;
		}

		/**
		 * 把数字字符转变为双精度型
		 * 
		 * @param a
		 *            char 要转变的字符
		 * @return double 返回的结果
		 */
		public static double chartonumber(char a) {
			double b = 0;
			if (a == '0')
				b = 0.0;
			if (a == '1')
				b = 1.0;
			if (a == '2')
				b = 2.0;
			if (a == '3')
				b = 3.0;
			if (a == '4')
				b = 4.0;
			if (a == '5')
				b = 5.0;
			if (a == '6')
				b = 6.0;
			if (a == '7')
				b = 7.0;
			if (a == '8')
				b = 8.0;
			if (a == '9')
				b = 9.0;
			return b;
		}


}
