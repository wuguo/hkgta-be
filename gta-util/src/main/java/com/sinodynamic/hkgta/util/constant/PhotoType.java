package com.sinodynamic.hkgta.util.constant;

public enum PhotoType {
	P("portraitPhoto"), S("signature");

	private String desc;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private PhotoType(String desc) {
		this.desc = desc;
	}

}
