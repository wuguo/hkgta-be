package com.sinodynamic.hkgta.util;

import com.sinodynamic.hkgta.util.constant.Constant;
import com.sinodynamic.hkgta.util.constant.EmailType;

public class EmailUtil {

	/**
	 * This method has moved into com.sinodynamic.hkgta.util.CommUtil,<br>
	 * Use CommUtil.generatePdfName(String emailType) instead.
	 * **/
	@Deprecated
	public static String generatePdfName(String emailType){

		if(Constant.TEMPLATE_ID_INVOICE.equals(emailType)){
			return "HKGTA-INVOICE-SP00001.pdf";
		}else if(Constant.TEMPLATE_ID_RECEIPT.equals(emailType)){
			return "HKGTA-RECEIPT-SP00001.pdf";
		}

		return "HKGTA-RECEIPT-SP00001.pdf";
	}


}
