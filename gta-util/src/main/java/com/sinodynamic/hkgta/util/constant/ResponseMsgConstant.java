package com.sinodynamic.hkgta.util.constant;

import com.sinodynamic.hkgta.util.ResponseMsg;

public interface ResponseMsgConstant {

	/**
	 * if sucess return 0 
	 */
	String SUCESSCODE = "0"; 
	
	/**
	 * ERRORCODE
	 */
	String ERRORCODE = "70000";
	String ERRORMSG_EN = "FAIL";
	String ERRORMSG_TC = "失败";
	
	String ERRORCODE_PARAM_NULL = "70001";
	String ERRORMSG_PARAM_NULL_EN = "parameter is null!";
	String ERRORMSG_PARAM_NULL_TC = "传入参数为空！";
	
	
	String ERRORCODE_PRESENTNAME_NULL = "70002";
	String ERRORMSG_PRESENTNAME_NULL_EN = "presentationname is null!";
	String ERRORMSG_PRESENTNAME_NULL_TC = "presentationname为空！";
	
	String ERRORCODE_PRESENTATION_EXIST = "70003";
	String ERRORMSG_PRESENTATION_EXIST_EN = "presentation is exist!";
	String ERRORMSG_PRESENTATION_EXIST_TC = "presentation已经存在！";
	
	String ERRORCODE_PRESENTATION_NOTEXIST = "70004";
	String ERRORMSG_PRESENTATION_NOTEXIST_EN = "presentation is not exist!";
	String ERRORMSG_PRESENTATION_NOTEXIST_TC = "presentation不存在！";
	
	
	String ERRORCODE_LOWRATE_GREATERTHAN_HIGHRATE = "60001";
	String ERRORMSG_LOWRATE_GREATERTHAN_HIGHRATE_EN = "lowerrate greater than highrate";
	String ERRORMSG_LOWRATE_GREATERTHAN_HIGHRATE_TC = "低费率高于高费率！";
	
	String ERRORCODE_ACTIVATIONDATE_LATERTHAN_DEACTIVATIONDATE = "60002";
	String ERRORMSG_ACTIVATIONDATE_LATERTHAN_DEACTIVATIONDATE_EN = "activationdate later than deactivationdate";
	String ERRORMSG_ACTIVATIONDATE_LATERTHAN_DEACTIVATIONDATE_TC = "起始日期晚于结束日期！";
	
	String ERRORCODE_INPUT_MUSTBE_NUMBER = "60003";
	String ERRORMSG_INPUT_MUSTBE_NUMBER_EN = "input must be number";
	String ERRORMSG_INPUT_MUSTBE_NUMBER_TC = "输入必须为数字！";
	
	String ERRORCODE_PLANNAME_NULL = "60004";
	String ERRORMSG_PLANNAME_NULL_EN = "planname is null!";
	String ERRORMSG_PLANNAME_NULL_TC = "传入planname为空！";
	
	String ERRORCODE_RATE_LOWERTHAN_ZERO = "60005";
	String ERRORMSG_RATE_LOWERTHAN_ZERO_EN = "input rate lower than zero";
	String ERRORMSG_RATE_LOWERTHAN_ZERO_TC = "费率小于零！";
	
		
	
	ResponseMsg SUCCESS_MSG = new ResponseMsg(SUCESSCODE,null);
	ResponseMsg ERROR_MSG = new ResponseMsg(ERRORCODE,ERRORMSG_EN);
}
