package com.sinodynamic.hkgta.util.exception;

public class LockFailedException extends RuntimeException {

	public LockFailedException() {
		super();
	}

	public LockFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public LockFailedException(String message) {
		super(message);
	}

	public LockFailedException(Throwable cause) {
		super(cause);
	}

}
