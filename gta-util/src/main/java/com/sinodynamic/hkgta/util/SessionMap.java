package com.sinodynamic.hkgta.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class SessionMap
{
	private static SessionMap instance = null;

	private Map<String, String> sessionMap = new HashMap<String,String>();
	
	private Map<String, String> userLocation = new HashMap<String,String>();
	
	private Date lastUpdateTime;

	public static synchronized SessionMap getInstance()
	{
		if (instance == null)
		{
			instance = new SessionMap();
		}
		return instance;
	}
	
	public void addSession(String key, String value)
	{
		this.sessionMap.put(key, value);
		this.lastUpdateTime = new Date();
	}
	
	public String getSession(String key)
	{
		return this.sessionMap.get(key);
	}
	
	public void addLocation(String key, String value)
	{
		this.userLocation.put(key, value);
		this.lastUpdateTime = new Date();
	}
	
	public String getLocation(String key)
	{
		return this.userLocation.get(key);
	}
	
	public void clearMap()
	{
		Long tmp = CommUtil.getIncTimeInMillis(lastUpdateTime, 5);
		if (tmp > System.currentTimeMillis())
		{
			this.sessionMap.clear();
		}
	}
}


