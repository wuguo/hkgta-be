package com.sinodynamic.hkgta.util.constant;

public enum ContractHelperStatus {
    
    //ACT, NACT, EXP
    ACT("Active"),
    NACT("Inactive");
//    EXP("Expired");
    
    private String desc;
    
    private ContractHelperStatus(String desc) {
	this.desc = desc;
    }
    
    public String getDesc() {
	return desc;
    }
}
