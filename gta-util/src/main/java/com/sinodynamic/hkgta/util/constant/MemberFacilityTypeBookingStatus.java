package com.sinodynamic.hkgta.util.constant;

public enum MemberFacilityTypeBookingStatus {
	
	RSV("RSV"), ATN("ATN"), NAT("NAT"), CAN("CAN"), PND("PND");

		private String desc;

		private MemberFacilityTypeBookingStatus(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

