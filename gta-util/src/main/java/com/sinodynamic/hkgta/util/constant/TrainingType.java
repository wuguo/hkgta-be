package com.sinodynamic.hkgta.util.constant;

public enum TrainingType {
	COACH("coachings"),
	COURSE("courses");
	private TrainingType(String type){
		this.type = type;
	}
	
	private String type;
	
	public String getType(){
		return this.type;
	}
	
	@Override
	public String toString() {
		return this.type;
	}
}
