
package com.sinodynamic.hkgta.util.constant;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Constant {

	// NoticeType for email
	public static final String NOTICE_TYPE_ENROLLMENT_SUCCESS="ENRAPR"; //enrollment success; 
	public static final String NOTICE_TYPE_ENROLLMENT_REJECT="ENRREJ"; //enrollment reject; 
	public static final String NOTICE_TYPE_GOLF_BAY="RCG"; //golf bay receipt; 
	public static final String NOTICE_TYPE_TENNIS_COURT="RCT"; //tennis court receipt; 
	public static final String NOTICE_TYPE_GOLF_COACHING="RCGPC"; //receipt-golf private coaching;  
	public static final String NOTICE_TYPE_TENNIS_COACHING="RCTPC"; //Tennis Private Coaching; 
	public static final String NOTICE_TYPE_GOLF_COURSE="RCGC"; //Golf Course; 
	public static final String NOTICE_TYPE_TENNIS_COURSE="RCTC"; //Tennis Course;
	public static final String NOTICE_TYPE_GUEST_ROOM="RCGR"; //Guest Room; 
	public static final String NOTICE_TYPE_DAY_PASS="RCDP"; //Day Pass; 
	public static final String NOTICE_TYPE_SERVICE_PLAN_TRANSACTION="RCSPT"; //Service Plan Transaction; 
	public static final String NOTICE_TYPE_WELLNESS_CENTER="RCWC"; //Wellness Center
	public static final String NOTICE_TYPE_CORPORATE="corporate"; //corporate
	public static final String NOTICE_TYPE_EVENT="event"; //event
	public static final String NOTICE_TYPE_HOLIDAY="holiday"; //holiday
	public static final String NOTICE_TYPE_ACCOUNT="account"; //account	
	public static final String NOTICE_TYPE_MARKET="market"; //market
	public static final String NOTICE_TYPE_REFUND="refund"; //refund	
	public static final String NOTICE_TYPE_RESTAURANT="restaurant"; //restaurant
	public static final String NOTICE_TYPE_STAFF="staff"; //staff
	
	public static final String NOTICE_TYPE_BATCH="RCBS"; //BATCH SEND

	// Service Plan offer code
	public static final String	SERVICE_PLAN_OFFER_CODE_RENEWAL				= "RENEW";
	public static final String	SERVICE_PLAN_TYPE_RENEWAL					= "Renewal";
	public static final String	SERVICE_PLAN_TYPE_ENROLLMENT				= "Enrollment";
	public static final String	SERVICE_PLAN_TYPE_COMPLETE					= "CMP";

	// permit card master status literal
	public static final String	PERMIT_CARD_MASTER_OH						= "OH";
	public static final String	PERMIT_CARD_MASTER_ISS						= "ISS";
	public static final String	PERMIT_CARD_MASTER_CAN						= "CAN";
	public static final String	PERMIT_CARD_MASTER_LOST						= "LOST";
	// daypass
	public static final String	ENROLL_PRICE_PREFIX_DP						= "DP_000000";
	public static final String	ENROLL_PRICE_ITEM_PREFIX_DP					= "DP_";
	public static final String	SRV_HI_PREFIX_DP							= "DP_HI000000";
	public static final String	SRV_HI_RATE_ITEM_PREFIX_DP					= "DP_HI";
	public static final String	SRV_NA_RATE_ITEM_PREFIX_DP					= "DP_NA";
	// serviceplan
	public static final String	ENROLL_PRICE_PREFIX							= "SRV000000";
	public static final String	ENROLL_PRICE_ITEM_PREFIX					= "SRV";
	public static final String	RENEW_PRICE_PREFIX							= "RENEW000000";
	public static final String	RENEW_PRICE_ITEM_PREFIX						= "RENEW";
	public static final String	TOPUP_ITEM_NO								= "CVT0000001";
	public static final String	SRV_REFUND_ITEM_NO							= "SRR00000001";
	public static final String	SRV_HI_PREFIX								= "SRVHI000000";
	public static final String	SRV_HI_RATE_ITEM_PREFIX						= "SRVHI";
	public static final String	GSSHI_RATE_ITEM_PREFIX						= "GSSHI";
	public static final String	TSSHI_RATE_ITEM_PREFIX						= "TSSHI";
	public static final String	GSSLO_RATE_ITEM_PREFIX						= "GSSLO";
	public static final String	TSSLO_RATE_ITEM_PREFIX						= "TSSLO";
	public static final String	FACILITY_PRICE_PREFIX						= "FMS";
	public static final String	PMS_ROOMTYPE_ITEM_PREFIX					= "PMS";
	public static final String	GOLF_COURSE_PRICE_PREFIX					= "GSSCOS";
	public static final String	TENNIS_COURSE_PRICE_PREFIX					= "TSSCOS";
	public static final String	RENEW_ITEM_NO_PREFIX						= "RENEW";
	public static final String	STAFF_REFUND_FOR_CASH_VALUE					= "SCVR00000001";
	public static final String	STAFF_DEBITED_FOR_CASH_VALUE				= "SCVD00000001";

	public static final String	ENROLL_PRICE_DESC_SUFFIX					= " - Enrollment";
	public static final String	RENEW_PRICE_DESC_SUFFIX						= " - Renewal";
	public static final String	SPECIAL_PRICE_DESC_SUFFIX					= " - Special";

	public static final String	TRANSACTION_BALANCEDUE_ALL					= "All";

	public static final String	TRANSACTION_BALANCEDUE_PENDING				= "PENDING";										// PendingPayment
	public static final String	TRANSACTION_BALANCEDUE_COMPLETED			= "COMPLETED";										// CompletedPayment

	public static final String	DELETE_NO									= "N";
	public static final String	DELETE_YES									= "Y";

	public static final String	NO											= "N";
	public static final String	YES											= "Y";
	public static final String	BLOCK										= "B";

	public static final String	Member_Status_ACT							= "ACT";
	public static final String	Member_Status_NACT							= "NACT";

	public static final String	General_Status_ACT							= "ACT";
	public static final String	General_Status_NACT							= "NACT";
	public static final String	General_Status_EXP							= "EXP";

	public static final String	HK_PUBLIC_HOLODAYS_URL						= "hk_government_holidays_ical_url";

	public static final String	CHECK_SERVER_URL							= "check_server_url";
	public static final String	CHECK_SERVER_TO_EMAIL						= "check_server_to_email";
	public static final String[] EMAIL_ALERT_GROUP                          = {"email_alert_group.0"};
	
	public static final String MEMBER_PAY="memberPay";
	public static final String TOPUP_PAY="topUpPay";
	public static final String COMMON_PAY="pay";
	public static final String GUEST_ROOM_PAY="guestRoomPay";
	public static final String STAFF_PAY="staffPay";
	
	// SFTP literal define
		public static final String SFTP_HOST = "hkgta.virtual.account.sftp.host";
		public static final String SFTP_PORT = "hkgta.virtual.account.sftp.port";
		public static final String SFTP_USERNAME = "hkgta.virtual.account.sftp.username";
		public static final String SFTP_PASSWORD = "hkgta.virtual.account.sftp.password";
		public static final String SFTP_RECEIVE_PATH = "hkgta.virtual.account.sftp.receive.path";
		public static final String SFTP_RECEIVE_BACK_PATH = "hkgta.virtual.account.sftp.receive.back.path";
		public static final String SFTP_TIMEOUT = "hkgta.virtual.account.sftp.timeout";
		public static final String SFTP_SNED_PATH = "hkgta.virtual.account.sftp.upload.path";
		public static final int SFTP_DEFAULT_PORT = 22;
		public static final int SFTP_DEFAULT_TIMEOUT = 60000;
		public static final String SFTP_CONNECT_TIMEOUT = "hkgta.virtual.account.sftp.connect.timeout";
		

		// CSV file name format
		public static final String CSV_UPLOAD_PATH = "csv.upload.path";
		public static final String CSV_UPLOAD_BACK_PATH = "csv.upload.back.path";
		public static final String CSV_DOWNLOAD_PATH = "csv.download.path";
		public static final String CSV_DOWNLOAD_BACK_PATH = "csv.download.back.path";
		

		// search sysconfig from global_parameter
		public static final String CLIENT_ID = "ClientID";
		public static final String PHYSICAL_ACCOUNT = "PhysicalAccount";
		public static final String RECORD_CODE = "RecordCode";
		public static final String PAYER_NAME = "PayerName";

		public static final String ROLE_OFFICER = "Manager";
		public static final String ROLE_SALES = "SALES";

		public static final String ADVANCEDRESPERIOD_GOLF = "GolfingBayAdvancedResPeriod";
		public static final String ADVANCEDRESPERIOD_TENNIS = "TennisCourtAdvancedResPeriod";
		public static final String FACILITY_TYPE_GOLF = "GOLF";
		public static final String FACILITY_TYPE_TENNIS = "TENNIS";
		public static  String GUEST_ROOM_CHECK_IN_PUSH_MESSAGE = null;
		public static  String GUEST_ROOM_PUSH_MESSAGE_TIME = null;


		// sort by column for contractor help
		public static final String COL_TEMP_USER = "tempUser";
		public static final String COL_PASS_TYPE = "temporaryPassType";
		public static final String COL_CARD_ID = "cardId";
		public static final String COL_ACT_DATE = "activationDate";
		public static final String COL_DEACT_DATE = "deactivationDate";
		public static final String COL_REFERRAL = "referral";
		public static final String COL_STATUS = "status";

		public static final String LOGIN_FAIL_TRY_TIME = "login.failed.try.times";
		public static final String LOGIN_FAIL_TRY_TIME_LOCK = "login.failed.try.times.lock";
		public static final String LOGIN_FAIL_TRY_PERIOD = "login.failed.try.period";
		public static final String LOGIN_FAIL_LOCK_PERIOD = "login.failed.lock.period";
		public static final String LOGIN_DEVICE_CHECK_LIST = "login.device.check.list";
		public static final String LOCATION_CHECK_DEVICE_LIST = "location.check.device.list";

		public static final String BALLFEEDSWITCH = "BALLFEEDSWITCH";
		public static final int MAX_FACILITY_ADVANCEDRESPERIOD = 1000;
		public static final int MAX_FACILITY_ITEMPRICE = 1000000;

		// sort by column for course list
		public static final String COL_COURSE_ID = "courseId";
		public static final String COL_COURSE_NAME = "courseName";
		public static final String COL_REG_DATE = "regDate";
		public static final String COL_COURSE_PERIOD = "periodStart";
		public static final String COL_COURSE_STATUS = "status";
		public static final String COL_ENROLLMENT = "enrollment";

		public static final String GOLFBAYTYPE = "GOLFBAYTYPE";
		public static final String TENNISCRT = "TENNISCRT";
		public static final String GOLFBAYTYPE_CAR = "GOLFBAYTYPE-CAR";
		
		public static final String ZONE_ATTRIBUTE = "ZONE";
		
		public static final String RESV_TYPE = "resvType";
		
		public static final String RESV_TYPE_PRIVATE_COACHING = "Private Coaching";

		public static final String RESV_TYPE_MEMBER_BOOKING = "Member Booking";
		
		public static final String RESV_TYPE_GUEST_ROOM_BUNDLED = "Guest room bundled";

		public static final String RESV_TYPE_MAINTENANCE_OFFER = "Maintenance Offer";

		public static final String RESV_TYPE_COURSE = "Course";
		
		public static final String COURSE_NAME = "courseName";

		public static final String COACH_NAME = "coachName";
		
		public static final String UPDATE_CUSTOMER_PROFILE_SMS_TEMPLETE = "Your profile is updated --HKGTA";
		public static final String UPDATE_CUSTOMER_STATUS_SMS_TEMPLETE = "Your Member status is updated --HKGTA";
		public static final String UPDATE_CUSTOMER_CREDIT_LIMIT_SMS_TEMPLETE = "Your credit limit is updated --HKGTA";
		public static final String UPDATE_CUSTOMER_SPENING_LIMIT_SMS_TEMPLETE = "Your spending limit is updated --HKGTA";
		
		public static final String MEMBER_SYN_ACACEMY_NO_ALREADY_EXISTS = "The academy_no already exists in MMS --HKGTA";
		public static final String MEMBER_SYN_SPA_GUEST_ID_NULL = "The spa guest id can't be null --HKGTA";
		
		public static final String SALESKIT_PUSH_APPLICATION = "SalesKit";
		public static final String STAFFAPP_PUSH_APPLICATION = "StaffApp";
		public static final String MEMBERAPP_PUSH_APPLICATION = "member";
		public static final String RESTAURANTTABLETAPP_PUSH_APPLICATION = "RestaurantTabletApp";
		
		public static final int THREAD_NUM = 3;
		
		public static final String MEMBER_SYNCHRONIZE_INSERT = "INSERT";
		public static final String MEMBER_SYNCHRONIZE_UPDATE = "UPDATE";
		public static final String MEMBER_SYNCHRONIZE_LINKCARD = "ZLINKCARD";
		public static final String STATUS_WAITING_TO_SYNCHRONIZE = "OPN";
		public static final String STATUS_FINISHED_SYNCHRONIZE = "CMP";
		
		public static final String RESTAURANT_BOOK_VIA_OL = "OL";
		public static final String RESTAURANT_BOOK_VIA_FD = "FD";
		public static final String RESTAURANT_BOOK_VIA_PH = "PH";
		
		public static final String MEDIA_FILE_LINK_IN = "files";
		public static final String LOGS_LINK_IN = "logs";
		
		public static final String TENNIS_AGE_RANGE = "tennisAgeRange";
		public static final String GOLF_AGE_RANGE = "golfAgeRange";
		public static final String REPORT_CUTOFF_DATE_HMS = " 23:59:59";
		
		public static final String POS_MASTER_CARD_RSP = "MASTERCARD";
		
		public static final String SILENTCHKIN_PUSHTIME_SEITCH = "SILENTCHKIN_PUSHTIME_SEITCH";
		
		public static final String WEEKDAY = "WKD";
		
		public static final String WEEKEND = "WKE";
		
		public static final String PUBLIC_HOLIDAY = "PH";
		
		public static ExecutorService getThreadPool(){
			return Executors.newFixedThreadPool(THREAD_NUM);
		}
		
		public static ExecutorService getThreadPool(Integer num){
			return Executors.newFixedThreadPool(num);
		}
		
		public enum PriceCatagory {
			Enrollment, RENEW, SRV, TOPUP, REFUND, PMS_ROOM,DP
		}

		public enum RateType {
			HI("HI"),
			LO("LO"),
			OFF("OFF"),
			BLK("BLK"),
			NA("NA");
			
			private RateType(String name)
			{
				this.name = name;
			}
			
			private String name;
			
			public String getName()
			{
				return this.name;
			}
		}
		
		public enum StaffType {
			FTR("Tennis Coach"),
			FTG("Golf Coach"),
			HK("Housekeep"),
			CONCG("Concierge"),
			MGNT("Management"),
			SALESMBR("Sales(membership/Service)"),
			RETAIL("Sales(Retail)"),
			WTR("Waiter"),
			HR("Human Resources"),
			NGR("Engineer"),
			BDA("Bell/Door attendant"),
			SEC("Security"),
			LNDSCP("Landscape"),
			ACC("Account/Audit"),
			IT("IT"),
			PROC("Procurement"),
			CUSTS("Customer service"),
			DRV("Driver"),
			RFE("Recreation Facilities & Events"),
			OTH("OTHER(general staff)");
			
			public String getTypeName()
			{
				return typeName;
			}

			private String typeName;

			private StaffType(String typeName)
			{
				this.typeName = typeName;
			}
		}
		
		

		public enum Purchase_Daypass_Type {
			Staff, Member
		}

		public enum Purchase_Flag_Type {
			OK, FAIL
		}
		
		public enum Subscriber_Type {
			STF, M
		}
		
		public enum Status {
			ACT, NACT,
			//for table service_plan
			//ACT-active, NACT-Inactive, EXP- expired, CAN-cancelled
			EXP,
			// for table customer_order_hd. OPN-open, CAN-cancelled, CMP-paid and
			// transaction completed, REJ - Rejected
			OPN, CAN, CMP, REJ,
			// for table customer_order_permit_card. ACT-activated; RA - ready to
			// activate (card_no not mapped yet); PND-pending
			RA, PND,
			// for table customer_order_trans. NULL/empty-paid but not confirmed;
			/*
			 *  NULL/empty-paid but not confirmed; SUC - payment success; FAIL - payment not success; 
			 *  VOID- paid and success but cancelled finally; PND-pending for transaction (oline payment gateway) ; 
			 *  RFU-refund; PR-paid but need to refund\n
			 */
			// success but cancelled finally \n
			SUC, FAIL, VOID,
			RFU,PR, // refund
			//for table staff_timeslot OP-Occupied; LV-on leave; SL-sick leave
			OP,LV,SL,
			//for member_facility_type_booking RSV-resevered; ATN-attended;  NAT-not attended; CAN-cancel booking
			RSV,ATN,NAT,
			//course Absent or Absented
			ABS,ATD,
			//for table permit_card_master OH- onhand; ISS-issued; CAN-cancelled; DPS-Disposal
			OH,ISS,DPS,CFM
		}
		public enum CustomerRefundRequest {
			/*
			 * for table customer_refund_request 
			 * FMS - facility; SRV-service plan; TSS-tennis course; GSS-golf course
			 * OPN - open case; APR-approved; REJ- rejected; PND-pending; NRF-no refund
			 * CASH-cash; CASHVALUE-cash value
			 */
			
			FMS("FMS","facility"),
			SRV("SRV","service plan"),
			TSS("TSS","tennis course"),
			GSS("GSS","golf course"),
			OPN("OPN","open case"),
			APR("APR","approved"),
			REJ("REJ","rejected"),
			PND("PND","pending"),
			NRF("NRF","no refund"),
			CASH("CASH","cash"),
			CASHVALUE("CASHVALUE","cash value"),
			;
			
			private CustomerRefundRequest(String name, String desc){
				this.name = name;
				this.desc = desc;
			}
			
			private String desc;
			private String name;
			
			public String getDesc(){
				return this.desc;
			}
			
			public String getName()
			{
				return this.name;
			}
		}
		public enum duty_category{
			//for table staff_timeslot NULL-no category; PVCOACH-private coach; TSS - Trainer for a tennis course; GSS - Trainer for a golf course; CLNROOM- Room cleaning; MEET-meeting; .....etc
			PVTCOACH,PVGCOACH,TSS,GSS,CLNROOM,MEET
		}
		
		public enum  reserve_via{
			//for table member_facility_type_booking reservation method: APS-mobile apps; WP-web portal; FD-front desk request
			APS,WP,FD
		}
		public enum AnnualRepeat {
			Y, N
		}

		public enum ServiceplanType {
			SERVICEPLAN, DAYPASS
		}

		public enum memberType {
			/*
			 * IPM – Individual Primary Member;IDM – Individual Dependent Member;
			 * CPM – Corporate Primary Member; CDM – Corporate Dependent Member; MG
			 * – Member Guest;HG – House Gues
			 */
			IPM, IDM, CPM, CDM, MG,HG
		}
		
		// spring security authorization filter access right for specific
		// url/program_master
		public static final String SECURITY_ACCESS_RIGHT_ALL = "*";
		public static final String SECURITY_ACCESS_RIGHT_READ = "R";
		public static final String SECURITY_ACCESS_RIGHT_CREATE_UPDATE = "U"; // create
																				// +
																				// update
																				// +
																				// read;
																				// all(*)
																				// except
																				// delete.
		// public static final String SECURITY_ACCESS_RIGHT_DELETE = "D"; //
		// equivalent to ALL(*)
		public static final String DAY_PASS_PURCHASING = "Day Pass Purchasing";

		// payment account type
		public static final String VIRTUAL_ACCOUNT = "VA";
		public static final String BANK_ACCOUNT = "BNK";

		// topup method type
		public static final String VIRTUAL = "VA";
		public static final String CASH = "CASH";
		public static final String CASH_Value = "CASHVALUE";
		public static final String CREDIT_CARD = "VISA";
		public static final String UNION_PAY = "CUP";
		
		public static final String CASHVALUECODE = "901000";
		
		public static final String PRE_AUTH = "PREAUTH";

		// virtual acc status
		// AV-available; US-used; NR-Not ready; DEL-deleted
		public static final String VIR_ACC_STATUS_VA = "AV";
		public static final String VIR_ACC_STATUS_US = "US";
		public static final String VIR_ACC_STATUS_NR = "NR";
		public static final String VIR_ACC_STATUS_DEL = "DEL";

		// member payment account status
		public static final String PAYMENT_ACC_STATUS_ACT = "ACT";
		public static final String PAYMENT_ACC_STATUS_NACT = "NACT";

		// staff timeslot status
		// OP-Occupied; LV-on leave; SL-sick leave
		public static final String STAFF_TIMESLOT_OP = "OP";
		public static final String STAFF_TIMESLOT_LV = "LV";
		public static final String STAFF_TIMESLOT_SL = "SL";

		// staff timeslot dutyCategory
		// NULL-no category; TRAINER-Trainer for a course; CLNROOM- Room cleaning;
		// MEET-meeting; .....etc
		public static final String STAFF_TIMESLOT_TRAINER = "TRAINER";
		public static final String STAFF_TIMESLOT_CLNROOM = "CLNROOM";
		public static final String STAFF_TIMESLOT_MEET = "MEET";

		// staff timeslot dutyDescription
		public static final String STAFF_TIMESLOT_CATEGORY_DESCRIPTION = "Primary TRAINER";
		
		public static final String COL_CREATE_DATE = "createDate";
		
		// Common placeholder for userName and fromName in email content
		public static final String PLACE_HOLDER_FROM_USER = "{fromname}";
		public static final String PLACE_HOLDER_TO_CUSTOMER = "{username}";
		public static final String HKGTA_DEFAULT_SYSTEM_EMAIL_SENDER = "HKGTA";

		public static final String INTERNAL_REMARK_REFUND = "refund to cash value";
		
		public static final int REFUND_PERIOD_DEFAULT_HOURS = 96;
		
		public static final String CATEGORY_4_SECURITY_QUESTION = "pwdquestion";
		
		public static final String SYSCODE_CREW_ROLE = "HK-CREW-ROLE";

		public static final String SYSCODE_STAFF_TYPE = "staffType";
		
		public static final String ZONE_COACHING = "ZONE-COACHING";
		
		public static final String ZONE_PRACTICE = "ZONE-PRACTICE";
		
		/**
		 * private coach training
		 */
		public static final String RESERVE_TYPE_PC = "PC";
		
		/**
		 * course session
		 */
		public static final String RESERVE_TYPE_CS = "CS";
		
		/**
		 * member reserve
		 */
		public static final String RESERVE_TYPE_MR = "MR";
		
		/**
		 * maintenance
		 */
		public static final String RESERVE_TYPE_MT = "MT";
		
		
		//template id Constant.TEMPLATE_ID_RENEWAL
		public static final String TEMPLATE_ID_PRESENT                                      ="present"; //
		public static final String TEMPLATE_ID_ACTIVATION                                   ="activation";//
		public static final String TEMPLATE_ID_EXTENSION                                    ="extension";//
		public static final String TEMPLATE_ID_INVOICE                                      ="invoice"; //
		public static final String TEMPLATE_ID_RECEIPT                                      ="receipt";//
		public static final String TEMPLATE_ID_ACTSTAFF                                     ="actstaff";//
		public static final String TEMPLATE_ID_ENROLLfORM                                   ="enrollForm";//
		public static final String TEMPLATE_ID_RENEWAL                                      ="renewal";//
		public static final String TEMPLATE_ID_REFUND                                       ="refund";//
		public static final String TEMPLATE_ID_GOLF_COURSE_CHANGE                           ="golf_course_change";//
		public static final String TEMPLATE_ID_GOLF_COURSE_M_ENROLL_ACCEPT                  ="golf_course_m_enroll_accept";//
		public static final String TEMPLATE_ID_GOLF_COURSE_M_ENROLL_RECEIVE                 ="golf_course_m_enroll_receive";//
		public static final String TEMPLATE_ID_GOLF_COURSE_A_ENROLL_CONFIRM                 ="golf_course_a_enroll_confirm";//
		public static final String TEMPLATE_ID_GOLF_COURSE_A_ENROLL_RECEIPT                 ="golf_course_a_enroll_receipt";//
		public static final String TEMPLATE_ID_TOPUP                                        ="topup";//
		public static final String TEMPLATE_ID_PTCRC                                        ="ptcrc";//
		public static final String TEMPLATE_ID_CVCL                                         ="cvcl";//
		public static final String TEMPLATE_ID_SRCL                                         ="srcl";//
		public static final String TEMPLATE_ID_PGCRC                                        ="pgcrc";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_CHANGE                         ="tennis_course_change";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_REMIND                         ="tennis_course_remind";//
		public static final String TEMPLATE_ID_GOLF_CONFIRM_EMAIL                           ="golf_confirm_email";//
		public static final String TEMPLATE_ID_TENNIS_CONFIRM_EMAIL                         ="tennis_confirm_email";//
		public static final String TEMPLATE_ID_RENEWAL_NOTICE                               ="renewal_notice";//
		public static final String TEMPLATE_ID_PAYMENT_IN_PROSHOP                           ="payment_in_proshop";//
		public static final String TEMPLATE_ID_GOLF_BOOK_CONFIRM                            ="golf_book_confirm";//
		public static final String TEMPLATE_ID_GOLF_BOOK_CANCEL                             ="golf_book_cancel";//
		public static final String TEMPLATE_ID_GOLF_BOOK_REMIND                             ="golf_book_remind";//
		public static final String TEMPLATE_ID_TENNIS_BOOK_CONFIRM                          ="tennis_book_confirm";//
		public static final String TEMPLATE_ID_TENNIS_BOOK_CANCEL                           ="tennis_book_cancel";//
		public static final String TEMPLATE_ID_TENNIS_BOOK_REMIND                           ="tennis_book_remind";//
		public static final String TEMPLATE_ID_TENNIS_COACH_CONFIRM                         ="tennis_coach_confirm";//
		public static final String TEMPLATE_ID_TENNIS_COACH_REMIND                          ="tennis_coach_remind";//
		public static final String TEMPLATE_ID_TENNIS_COACH_CANCEL                          ="tennis_coach_cancel";//
		public static final String TEMPLATE_ID_TENNIS_COACH_CHANGE                          ="tennis_coach_change";//
		public static final String TEMPLATE_ID_GOLF_COACH_CONFIRM                           ="golf_coach_confirm";//
		public static final String TEMPLATE_ID_GOLF_COACH_REMIND                            ="golf_coach_remind";//
		public static final String TEMPLATE_ID_GOLF_COACH_CANCEL                            ="golf_coach_cancel";//
		public static final String TEMPLATE_ID_GOLF_COACH_CHANGE                            ="golf_coach_change";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_A_ENROLL_RECEIPT               ="tennis_course_a_enroll_receipt";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_A_ENROLL_CONFIRM               ="tennis_course_a_enroll_confirm";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_M_ENROLL_RECEIVE               ="tennis_course_m_enroll_receive";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_M_ENROLL_ACCEPT                ="tennis_course_m_enroll_accept";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_CANCEL                         ="tennis_course_cancel";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_M_ENROLL_CONFIRM               ="tennis_course_m_enroll_confirm";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_M_ENROLL_RECEIPT               ="tennis_course_m_enroll_receipt";//
		public static final String TEMPLATE_ID_GOLF_COURSE_REMIND                           ="golf_course_remind";//
		public static final String TEMPLATE_ID_GOLF_COURSE_M_ENROLL_RECEIPT                 ="golf_course_m_enroll_receipt";//
		public static final String TEMPLATE_ID_GOLF_COURSE_M_ENROLL_CONFIM                  ="golf_course_m_enroll_confim";//
		public static final String TEMPLATE_ID_GOLF_COURSE_CANCEL                           ="golf_course_cancel";//
		public static final String TEMPLATE_ID_NEWS                                         ="news";//
		public static final String TEMPLATE_ID_NEWS_ATTACHMENT                              ="news_attachment";//
		public static final String TEMPLATE_ID_REFUND_STATUS                                ="refund_status";//
		public static final String TEMPLATE_ID_APP_COACH_NOTICE                             ="app_coach_notice";//
		public static final String TEMPLATE_ID_APP_COURSE_NOTICE                            ="app_course_notice";//
		public static final String TEMPLATE_ID_APP_COACH_CANCEL                             ="app_coach_cancel";//
		public static final String TEMPLATE_ID_APP_GOLF_COURSE_SESSION_CANCEL               ="app_golf_course_session_cancel";//
		public static final String TEMPLATE_ID_TASK_ASSIGNED                                ="taskAssigned";//
		public static final String TEMPLATE_ID_TASK_UPDATED                                 ="taskUpdated";//
		public static final String TEMPLATE_ID_TASK_EXPIRED                                 ="taskExpired";//
		public static final String TEMPLATE_ID_TASK_COMPLETED                               ="taskCompleted";//
		public static final String TEMPLATE_ID_TASK_REASSIGNED                              ="taskReassigned";//
		public static final String TEMPLATE_ID_GOLF_END_REMIND                              ="golf_end_remind";//
		public static final String TEMPLATE_ID_TENNIS_END_REMIND                            ="tennis_end_remind";//
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_CONFIRM                      ="restaurant_book_confirm";//
		public static final String TEMPLATE_ID_TEMP_PROFILE_CREATE                          ="temp_profile_create";//
		public static final String TEMPLATE_ID_DAYPASS_ISSUE_NOTIFY                         ="daypass_issue_notify";//
		public static final String TEMPLATE_ID_REJECT                                       ="reject";//
		public static final String TEMPLATE_ID_MONTHLY_STATEMENT                            ="monthly_statement";//
		public static final String TEMPLATE_ID_GOLF_COURSE_ENROLL_REJECT                    ="golf_course_enroll_reject";//
		public static final String TEMPLATE_ID_GOLF_COURSE_ENROLL_CANCEL                    ="golf_course_enroll_cancel";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_ENROLL_REJECT                  ="tennis_course_enroll_reject";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_ENROLL_CANCEL                  ="tennis_course_enroll_cancel";//
		public static final String TEMPLATE_ID_GUEST_ROOM_BOOK_CONFIRM                      ="guest_room_book_confirm";//
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_UPDATED                      ="restaurant_book_updated";//
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_CANCELLED                    ="restaurant_book_cancelled";//
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_REMINDER                     ="restaurant_book_reminder";//
		public static final String TEMPLATE_ID_TASK_CANCELED                                ="taskCanceled";//
		public static final String TEMPLATE_ID_DND_STATE                                    ="DNDState";//
		public static final String TEMPLATE_ID_HOUSEKEEP                                    ="housekeep";
		public static final String TEMPLATE_ID_APP_GOLF_COURSE_REMINDER                     ="app_golf_course_reminder";//
		public static final String TEMPLATE_ID_APP_TENNIS_COURSE_REMINDER                   ="app_tennis_course_reminder";//
		public static final String TEMPLATE_ID_CASHVALUE_REFUND_EMAIL                       ="cashValue_refund_email";//
		public static final String TEMPLATE_ID_SERVICE_REFUND_EMAIL                         ="service_refund_email";//
		public static final String TEMPLATE_ID_DAYPASS_PURCHASE_NOTIFY                      ="daypass_purchase_notify";//
		public static final String TEMPLATE_ID_DAYPASS_CANCEL_SMS                           ="daypass_cancel_sms";//
		public static final String TEMPLATE_ID_UPDATE_STATUS                                ="update_status";//
		public static final String TEMPLATE_ID_UPDATE_CREDIT_LIMIT                          ="update_credit_limit";//
		public static final String TEMPLATE_ID_UPDATE_PROFILE                               ="update_profile";//
		public static final String TEMPLATE_ID_UPDATE_MEMBER_STATUS_EMAIL                   ="update_member_status_email";//
		public static final String TEMPLATE_ID_UPDATE_SPENDING_LIMIT                        ="update_spending_limit";//
		public static final String TEMPLATE_ID_UPDATE_CORPORATE_STATUS_EMAIL                ="update_corporate_status_email"; //
		public static final String TEMPLATE_ID_UPDATE_CORPORATE_PROFILE_EMAIL               ="update_corporate_profile_email"; //
		public static final String TEMPLATE_ID_APP_GOLF_COURSE_CHANGE                       ="app_golf_course_change";//
		public static final String TEMPLATE_ID_APP_GOLF_COURSE_CHANGE_OLDCOACH              ="app_golf_course_change_oldcoach";//
		public static final String TEMPLATE_ID_APP_GOLF_COURSE_CHANGE_NEWCOACH              ="app_golf_course_change_newcoach";//
		public static final String TEMPLATE_ID_APP_TENNIS_COURSE_CHANGE                     ="app_tennis_course_change";//
		public static final String TEMPLATE_ID_APP_TENNIS_COURSE_CHANGE_OLDCOACH            ="app_tennis_course_change_oldcoach";//
		public static final String TEMPLATE_ID_APP_TENNIS_COURSE_CHANGE_NEWCOACH            ="app_tennis_course_change_newcoach";//
		public static final String TEMPLATE_ID_TEMP_PROFILE_EDIT                            ="temp_profile_edit";//
		public static final String TEMPLATE_ID_NEW_REMARK_NOTIFICATION                      ="new_remark_notification";//
		public static final String TEMPLATE_ID_TRANSACTION_SUCCESS_NOTIFICATION             ="transaction_success_notification";//
		public static final String TEMPLATE_ID_TRANSACTION_FAIL_NOTIFICATION                ="transaction_fail_notification";//
		public static final String TEMPLATE_ID_ENROLLMENT_APPROVED_NOTIFICATION             ="enrollment_approved_notification";//
		public static final String TEMPLATE_ID_ENROLLMENT_REJECTED_NOTIFICATION             ="enrollment_rejected_notification";//
		public static final String TEMPLATE_ID_MEMBER_PAYMENT_APPROVED                      ="member_payment_approved";//
		public static final String TEMPLATE_ID_MEMBER_ACTIVIATED_NOTIFICATION               ="member_activiated_notification";//
		public static final String TEMPLATE_ID_SCORED_REMIND                                ="scored_remind";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_CREATE                         ="tennis_course_create";//
		public static final String TEMPLATE_ID_GOLF_COURSE_CREATE                           ="golf_course_create";//
		public static final String TEMPLATE_ID_APP_TENNIS_COURSE_SESSION_CANCEL             ="app_tennis_course_session_cancel";//
		public static final String TEMPLATE_ID_APP_TENNIS_COURSE_CANCEL                     ="app_tennis_course_cancel";//
		public static final String TEMPLATE_ID_APP_GOLF_COURSE_CANCEL                       ="app_golf_course_cancel";//
		public static final String TEMPLATE_ID_GOLF_COURSE_SESSION_REMINDER                 ="golf_course_session_reminder";//
		public static final String TEMPLATE_ID_TENNIS_COURSE_SESSION_REMINDER               ="tennis_course_session_reminder";//
		public static final String TEMPLATE_ID_CORPORATE_SPENDING_SUMMARY                   ="corporate_spending_summary";//
		public static final String TEMPLATE_ID_APP_IBEACON_STAFF_NOTIFICATION               ="app_ibeacon_staff_notification";//
		public static final String TEMPLATE_ID_MMS_ITEM_CANCEL_NOTIFICATION                 ="mms_item_cancel_notification";//
		public static final String TEMPLATE_ID_BOOKING_START_REMINDER                       ="booking_start_reminder";//
		public static final String TEMPLATE_ID_MMS_ORDER_CANCEL_NOTIFICATION                ="mms_order_cancel_notification";//
		public static final String TEMPLATE_ID_EMAIL_WELLNESS_PAYMENT_RECEIPT               ="email_wellness_payment_receipt";
		public static final String TEMPLATE_ID_SMS_WELLNESS_PAYMENT_CONFIRM                 ="sms_wellness_payment_confirm";
		public static final String TEMPLATE_ID_PUBLIC_HOLIDAY                               ="public_holiday";
		public static final String TEMPLATE_ID_EMAIL_WELLNESS_PAYMENT_RECEIPT_SYNCHRONIZED  ="email_wellness_payment_receipt_synchronized";
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_REJECT                       ="restaurant_book_reject";
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_REQUEST                      ="restaurant_book_request";
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_REQUEST_EMAIL                ="restaurant_book_request_email";
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_CONFIRM_EMAIL                ="restaurant_book_confirm_email";
		public static final String TEMPLATE_ID_RESTAURANT_BOOK_REJECT_EMAIL                 ="restaurant_book_reject_email";
		public static final String TEMPLATE_ID_GOLF_COURSE_M_ENROLL_REJECT_EMAIL            ="golf_course_m_enroll_reject_email";
		public static final String TEMPLATE_ID_TENNIS_COURSE_M_ENROLL_REJECT_EMAIL          ="tennis_course_m_enroll_reject_email";
		public static final String TEMPLATE_ID_GOLF_COURSE_M_ENROLL_APPROVE_EMAIL           ="golf_course_m_enroll_approve_email";
		public static final String TEMPLATE_ID_TENNIS_COURSE_M_ENROLL_APPROVE_EMAIL         ="tennis_course_m_enroll_approve_email";
		public static final String TEMPLATE_ID_MEMBER_PROFILE_EDIT                          ="member_profile_edit";
		public static final String TEMPLATE_ID_SARCL                                        ="sarcl";
		public static final String TEMPLATE_ID_UPDATE_USER_PROFILE							="update_user_profile";
		public static final String TEMPLATE_ID_DAYPASS_RECEIPT								="daypass_receipt";//
		public static final String TEMPLATE_ID_STATUS_CHANGED								="statusChanged";//
		public static final String TEMPLATE_ID_APP_GUEST_ROOM_READY_MEMBER_NOTIFICATION     ="room_checkin_ready_mbr_msg";//
		public static final String TEMPLATE_ID_APP_GUEST_ROOM_SILENT_CHECK_IN_STAFF_NOTIFICATION      ="app_silent_checkin_staff_alert";//
		public static final String TEMPLATE_ID_APP_GUEST_ROOM_CHECK_IN_MEMBER_NOTIFICATION  ="app_silent_checkin_mbr_msg";//
		public static final String TEMPLATE_ID_STATUS_CHANGE_MEMBER_PAYMENT_RECEIVED  ="status_change_member_payment_received";//
		public static final String TEMPLATE_ID_MEMBER_PAYMENT_RECEIVED_NOTICE_ACCOUNTANT ="member_payment_received_notice_accountant";//
		public static final String INACTIVE_DPENDENT_18YEAR_OLD_3MONTH_NOTICE  ="inactive_dependent_18year_old_3month_notice";
		public static final String INACTIVE_DPENDENT_18YEAR_OLD_1MONTH_NOTICE  ="inactive_dependent_18year_old_1month_notice";
		public static final String INACTIVE_DPENDENT_18YEAR_OLD_0MONTH_NOTICE  ="inactive_dependent_18year_old_0month_notice";
		public static final String MEMBERSHIP_EXPIRE_3_MONTHS  ="membership_expire_3_months";
		public static final String MEMBERSHIP_EXPIRE_1_MONTH  ="membership_expire_1_month";
		public static final String MEMBERSHIP_EXPIRE_TODAY  ="membership_expire_today";
		public static  Long OVERVIEW_HKGTA_MARKETING									    = null;//
		public static  Long OVERVIEW_WMD_MARKETING											= null;//
		public static final String TEMPLATE_ID_APP_GUEST_ROOM_NOTICE_MEMBER_SILENT_CHECKIN_READY      = "notice_member_silent_checkin_ready";//
		public static final String TEMPLATE_ID_APP_GUEST_ROOM_NOTICE_MEMBER_SILENT_CHECKIN_DONE      = "notice_member_silent_checkin_done";//
		public static final String TEMPLATE_ID_APP_GUEST_ROOM_NOTICE_MEMBER_SILENT_CHECKIN_REJECTED      = "notice_member_silent_checkin_rejected";//
		public static final String PAYMENT_BUSINESS_TYPE_CHANGE_ROOM      = "PAYMENT_BUSINESS_TYPE_CHANGE_ROOM";   //支付业务类型-更改房间
		public static final String ADULT_DPENDENT_18YEAR_TIPS  ="adult_dependent_member_email";  //年满18岁子会员提醒

		public static final String TEMPLATE_ID_REFUND_REQUEST_RECIPIENT_EMAIL="refund_request_recipient_email"; //Service Refund Request Recipient Email
		
		public static final String TEMPLATE_ID_WELLNESS_RESERVATION_BY_MOBILE_APP                ="wellness_reservation_by_mobile_app";
		
		public enum MessageTemplate {
			
			//common 
			Letf_Symbol("{"),
			Right_Symbol("}"),
			MemberEmail("(?i)Member’s Email"),
			Salutation("(?i)Salutation"),
			FirstName("(?i)Member First Name"),
			LastName("(?i)Member Last Name"),
			FullName("(?i)\\{Member Full Name\\}"),
			//for Private Coaching Reservation Confirmation Template begin
			/*
			To: <Member’s Email>
			Dear <Salutation><Member’s Last Name>,
			Confirmation ID: {Confirmation #}   -- orderNo
			Reserved Person: <Member’s Full Name>
			Reservation Period: {Start Time} to {End Time}
			Coach: <Coach Full Name>
			Facility Name: <Facility’s Name>
			Tennis Court Type: <Tennis Court Type>
			
			Bay Type: <Bay Type>
			 */
			Confirmation("(?i)\\{Confirmation #\\}"),
			StartTime("(?i)\\{startTime\\}"),
			EndTime("(?i)\\{endTime\\}"),
			CoachName("(?i)Coach Full Name"),
			FacilityName("(?i)FacilityName"),
			TennisType("(?i)Tennis Court Type"),
			BayType("(?i)Golf Bay Type"),
			//for Private Coaching Reservation Confirmation Template end
			
		
			
			
			//for private coach auto push message begin 
			BookingDate("(?i)bookingDate"),
			//for private coach auto push message end
			
			TimeLeft("(?i)timeLeft"),
			UserName("(?i)\\{username\\}"),
			TopupMethod("(?i)\\{topupMethod\\}"),
			CurrentBalance("(?i)\\{currentBalance\\}"),
			
			//for purchase daypass
			/**
			Dear {username},
			This is to confirm your reservation request. Please find your reservation details below. We have approved your payment and you can find the receipt in the attachment. For reservation cancelation, please go to HKGTA front-desk or via Service Hotline.
			
			Order ID:{Confirmation #}
			Reserved Date:{Start Time} to {End Time}
			No of Day Pass: {no of day pass}
			Day Pass Type: {type}
			If you have any questions, please call us. We are looking forward to your arrival.
			Best Regards,
			Sales & Marketing Office of HKGTA
			 */
			Type("(?i)\\{type\\}"),
			OffPass("(?i)\\{no of day pass\\}"),
			;
			
			private MessageTemplate(String name)
			{
				this.name = name;
			}
			
			private String name;
			
			public String getName()
			{
				return this.name;
			}
		}
		public static final String REJECT_SERVICE_REMARK = "Reject refund for service";
		public static final String REFUND_SERVICE_REMARK = "Refund for service";
		
		public static final String REFUND_CASH_VALUE_REMARK = "Refund for cashvalue";
		
		public static final String REFUND_ITEM_NO = "CVR00000001";
		public static final String REFUND_SERVICE_ITEM_NO = "SRR00000001";
		public static final String CREDIT_ITEM_NO = "SCVR00000001";
		public static final String DEBIT_ITEM_NO = "SCVD00000001";
		
		public static final String MMSTHIRD_ITEM_NO = "MMS00000001";
		public static final String PMSTHIRD_ITEM_NO = "PMS00000001";
		public static final String POSTHIRD_ITEM_NO = "RST00000001";
		public static final String REMOTE_MEMBER_PORTAL = "MemberPortal";
		public static final String REMOTE_STAFF_PORTAL = "StaffPortal";
		public static final String REMOTE_TYPE = "PC";
		public static final String BOOKING_DESC_ROOMCHARGE = "Room Charges";
		public static final String BOOKING_DESC_VARIASIA = "VariAsia";
		
		
		public final static String	IMPORT_FILE_PATH	= "oasis.import.path";
		//public final static String	OOS_REPORT_NO	= "2455";
		//public final static String	OOO_REPORT_NO	= "2432";
		public final static String	ALLOTMENT_REPORT_NO	= "1140";
		
		public enum PaymentMethodCode{
			VISA("VISA"), MASTER("MASTER"), CASH("CASH"), CASHVALUE("CASHVALUE"), CHEQUE("CHEQUE"), UPAY("UPAY"), REFUND("REFUND")
			,CUP("CUP"),PREAUTH("PREAUTH"),BT("BT");
			private String desc;
			private PaymentMethodCode(String desc){
				this.desc = desc;
			}
			@Override
			public String toString(){
				return desc;
			}
		}
		
		public enum BizParties{
			CUSTOMER, HKGTA
		}
		
		public enum UserType{
			CUSTOMER, ADMIN, STAFF
		}
		
		public enum AdvertiseAppType { // advertise_image.application_type
			MAPP, MPORT
		}
		
		public enum AdvertiseDispLoc { // advertise_image.display_location
			MSCR, TOPUP
		}
		
		public enum ServiceItem{

			GFBK("Golfing Bay"), TFBK("Tennis Court"), TS("Tennis Private Coaching"), GS("Golf Private Coaching"),SRV("Service Plan"),DP("Day Pass Purchasing"), 
			GSS("Golf Course Enrollment"), TSS("Tennis Course Enrollment"), REFUND("Refund"),TOPUP("Top up"),PMS_ROOM("Accommodation"),MMS_SPA("Wellness Center"),MMS("Wellness Center");
			private String desc;
			private ServiceItem(String desc){
				this.desc = desc;
			}
			@Override
			public String toString(){
				return desc;
			}
		}
		
		public enum StatementDescItem{

			GFBK("Golfing Bay"), TFBK("Tennis Court"), TS("Tennis Private Coaching"), GS("Golf Private Coaching"),SRV("Service Plan"),DP("Day Pass"), 
			GSS("Golf Course Enrollment"), TSS("Tennis Course Enrollment"), REFUND("Refund"),TOPUP("Top up"),PMS_ROOM("Accommodation"),MMS_SPA("Wellness Center"),
			MMS_THIRD("Wellness Center"),PMS_THIRD("Accommodation"),POS_THIRD("Restaurant payment"),CREDIT("Cash Value Adjustment"),DEBIT("Cash Value Adjustment"),
			OASIS_ROOM ("Accommodation");
			
			private String desc;
			private StatementDescItem(String desc){
				this.desc = desc;
			}
			@Override
			public String toString(){
				return desc;
			}
		}
		
		public enum RoomReservationStatus
		{
			SUC, //both oasis system and our system successfully pay the reservation
			IGN, // ?
			CAN, //those order not yet paid by our system , and cancel manually or automatically
			RSV, //the initial status
			PAY, //paid by oasis system, not yet paid by our system
			EXP, // ?
			CKI, // Check - In
			CKO, // Check - Out
			CKP, // Check - In in progress
			RFU , //both oasis system and our system successfully pay the reservation, and cancel manually
			SA, //checkin_method ： SLA-silent check in accept 
			SR, //checkin_method ： SLR-silent check in reject
			DUE  /** 1. Staff pre-assign a room to member
					2. Member have not checked-in the room
					3. The room start date is Today
					*/
		}
		
		public enum RoomFrontdeskStatus
		{
			O,V,OOO
		}		

		public static final String PROGRAM_SALES_REPORT_OVERVIEW = "m_salesreport_overview";
		public static final String PROGRAM_TYPE_MENU = "M";
		public static final String PROGRAM_TYPE_REPORT = "R";

		public enum Terminal{
			APP, //iPhone , Android, WindowsPhone ...
			WEB  //Website
		}

		public enum AppType{
			STAFF, //For HKGTA all staff(front desk, admin , and so on)
			MEMBER //For member who use HKGTA service
		}
		
		public enum RepeatMode{
			ONCE, DAILY, WEEKLY, MONTHLY
		}
		
		public enum FacilityCategory {
			ATS("6200006"),
			MASSAGES001("6100001"),
			BWELLNESS001("6100002"),
			ENERGY001("6100003"),
			WHEALING001("6100004"),
			SARTS001("6100005"),
			PROGRAMS001("6100006"),
			CUISINE001("6100007"),
			RTL001("6100008"),
			CONSULT001("6100009"),
			SPECIALS001("6100010"),
			PROG_WELLNESSDAYS("6100011"),
			PROG_WELLNESSSTAYS("6100012"),
			RESTAURANT0001("6200001"),
			RESTAURANT0002("6200002"),
			RESTAURANT0003("6200003"),
			RESTAURANT0004("6200004"),
			RESTAURANT0005("6200005");
			
			public String getFacilityName()
			{
				return facilityName;
			}

			private String facilityName;

			private FacilityCategory(String facilityName)
			{
				this.facilityName = facilityName;
			}
		}
		
		public static Map<String, String> messageCodeMap = new HashMap<>();
		private static Constant initBean = new Constant();
		public final static  Constant initDate(){
			if (messageCodeMap.isEmpty()) {
				messageCodeMap.put("Balinese Massages", "80000006");
				messageCodeMap.put("Chinese Massage", "80000007");
				
				messageCodeMap.put("MASSAGES001", "6100001");
				messageCodeMap.put("BWELLNESS001", "6100002");
				messageCodeMap.put("ENERGY001", "6100003");
				messageCodeMap.put("WHEALING001", "6100004");
				messageCodeMap.put("SARTS001", "6100005");
				messageCodeMap.put("PROGRAMS001", "6100006");
				messageCodeMap.put("CUISINE001", "6100007");
				messageCodeMap.put("SAKITDININGROOM", "6100014");
				
				messageCodeMap.put("RTL001", "6100008");
				messageCodeMap.put("Consultations001", "6100009");
				messageCodeMap.put("SPECIALS001", "6100010");
				messageCodeMap.put("PROG_WELLNESSDAYS", "6100011");
				messageCodeMap.put("PROG_WELLNESSSTAYS", "6100012");
				messageCodeMap.put("PROG_WELLNESPECIALIS", "6100013");
				
				messageCodeMap.put("RESTAURANT0001", "6200001");
				messageCodeMap.put("RESTAURANT0002", "6200002");
				messageCodeMap.put("RESTAURANT0003", "6200003");
				messageCodeMap.put("RESTAURANT0004", "6200004");
				messageCodeMap.put("RESTAURANT0005", "6200005");
//				System.out.println("------数据初始化完成！------");
			}
			return initBean;
		}
		public Constant(){
			initDate();
//			System.out.println("null 构造函数");
		}
}
