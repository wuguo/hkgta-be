package com.sinodynamic.hkgta.util.constant;

public enum BiRateType {
	IOS("LOGIN-IOS","IOS","iOS"),IPAD("LOGIN-IOS","iPad","iOS"),IPHONE("LOGIN-IOS","IPhone","iOS"),ANDROID("LOGIN-ANDR","Android","Android"),WEB("LOGIN-WEB","Windows","Web");
	
	private String code;
	private String device;
	private String desc;
	
	private BiRateType(String code,String device,String desc)
	{
		this.code=code;
		this.device=device;
		this.desc=desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
