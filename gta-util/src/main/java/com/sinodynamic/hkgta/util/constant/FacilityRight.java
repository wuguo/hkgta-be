package com.sinodynamic.hkgta.util.constant;

public enum FacilityRight {
	BA("Booking&Access "),
	NO("No permission"),
	AO("Access only");
	
	private FacilityRight(String desc){
		this.desc = desc;
	}
	
	private String desc;
	
	public String getDesc(){
		return this.desc;
	}
	
	@Override
	public String toString() {
		return this.desc;
	}
}
