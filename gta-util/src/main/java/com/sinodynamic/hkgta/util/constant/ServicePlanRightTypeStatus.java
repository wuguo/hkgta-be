package com.sinodynamic.hkgta.util.constant;

public enum ServicePlanRightTypeStatus {

	// SENT- sent, REJ-Rejected, CAN-cancelled, FAIL-Failed
	INT("INT"), TXT("TXT");

	private String desc;

	private ServicePlanRightTypeStatus(String d) {
		this.desc = d;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
