package com.sinodynamic.hkgta.util.constant;

import java.util.HashMap;

/**
 * 
 * @author Angus_Zhu
 *
 */
public enum CustomerOrderPermitCardEnumStatus {

	//ACT-activated; RA - ready to activate (card_no  not mapped yet); PND-pending
	ACT("ACT", "activated"), RA("RA", "ready to activate"), PND("PND", "pending"), DPS("DPS", "Disposal");

	private String code;
	private String description;
	private static HashMap<String, CustomerOrderPermitCardEnumStatus> map;
	
	static {
		
		map = new HashMap<String, CustomerOrderPermitCardEnumStatus>();
		for (CustomerOrderPermitCardEnumStatus status : CustomerOrderPermitCardEnumStatus.values()) {
			map.put(status.getCode(), status);
		}
	}

	private CustomerOrderPermitCardEnumStatus(String code, String desc) {

		this.code = code;
		this.description = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public static boolean isValid(String code) {
		
		CustomerOrderPermitCardEnumStatus status = map.get(code);
		if (status == null) return false; 
		return true;
	}

}
