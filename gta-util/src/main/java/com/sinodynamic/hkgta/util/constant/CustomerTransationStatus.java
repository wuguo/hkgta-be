package com.sinodynamic.hkgta.util.constant;

public enum CustomerTransationStatus {
	// NULL/empty-paid but not confirmed; SUC - payment success; NSC - payment not success; CAN - payment cancelled 
	   SUC("SUC"), NSC("NSC"), CAN("CAN"), CMP("CMP"),PND("PND"), REFUND("RFU"), FAIL("FAIL"), VOID("VOID"),PR("PR");

		private String desc;

		private CustomerTransationStatus(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}
