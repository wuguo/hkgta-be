package com.sinodynamic.hkgta.util.constant;

public enum InternalPermitCardStatus {
    
    //ISS, DPS, RTN
    ISS("Issued"),
    DPS("Disposal"),
    RTN("Returned");
    
    private String desc;
    
    private InternalPermitCardStatus(String desc) {
	this.desc = desc;
    }
    
    public String getDesc() {
	return desc;
    }
}
