package com.sinodynamic.hkgta.util.constant;
/***
 * 
 * @author christ
 * @descrition
 *
 */
public enum LoggerType {
	/***
	 * ("001"[FE display cod],"mms.log"[log file xx.log],"mmsLog"[log4j config name]
	 * if(name is null ,no scheduler create log)
	 */
	MMS("0001","mms.log","mmsLog"), 
	PMS("0002","pms.log","pmsLog"),
	ZOOMTECH("0003","zoomtech.log","zoomtechLog"),
	DDX("0004","ddx.log","ddx"),
	VIRTUALACC("0005","virtualAccTopup.log","virtualAccTopupLog"),
	SCHEDULER("0006","scheduler.log",""),
	ECRLOG("0007","ecr.log","ecrLog"),
	SYSTEMDEBUG("0008","system_debug.log",""),
	SYSTEMERROR("0009","system_error.log",""),
	OOOLOG("0010","importOOO.log","importOOOLog"),
	OOSLOG("0011","importOOS.log","importOOSLog"),
	ALLOTMENTLOG("0012","importAllotment.log","importAllotmentLog"),
	DESKLOG("0013","importDesk.log","importDeskLog"),
	TOMCAT_ID("TOMCAT_ID","TOMCAT_ID",""),
	LOG_DIR("LOG_DIR","LOG_DIR",""),
	STAFFEMAIL("0014","staffEmailSend.log","staffEmailSendLog"),
	OASISFLEXFILE("0015","importOasisFlexFile.log","oasisFlexFileLog");
	
	private String desc;
	
	private String fileName;
	
	private String name;
	
	private LoggerType(String d,String fileName,String name) {
		this.desc = d;
		this.fileName=fileName;
		this.name=name;
		
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}
