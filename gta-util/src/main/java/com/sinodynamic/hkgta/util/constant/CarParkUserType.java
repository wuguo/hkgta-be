package com.sinodynamic.hkgta.util.constant;

public enum CarParkUserType {
	MEMBER(1), STAFF(11);
	
	private Integer role;
	
	private CarParkUserType(Integer role){
		this.role = role;
	}
	
	public Integer getRole() {
		return this.role;
	}
}
