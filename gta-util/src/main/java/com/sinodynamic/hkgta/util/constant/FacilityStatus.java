package com.sinodynamic.hkgta.util.constant;

public enum FacilityStatus {
	
	MT("MT"), OP("OP"), RS("RS"), VC("VC"),TA("TA"),CAN("CAN");

		private String desc;

		private FacilityStatus(String d) {
			this.desc = d;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
}

