package com.sinodynamic.hkgta.util.constant;

public enum limitUnit {
	MONTH("per month for TRN"),
	DAY("per day for TRN"),
	EACH("each transaction limit");
	
	private limitUnit(String desc){
		this.desc = desc;
	}
	
	private String desc;
	
	public String getDesc(){
		return this.desc;
	}
	
	@Override
	public String toString() {
		return this.desc;
	}
}
