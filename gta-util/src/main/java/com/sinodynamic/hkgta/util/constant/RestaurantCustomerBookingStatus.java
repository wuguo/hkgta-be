package com.sinodynamic.hkgta.util.constant;

public enum RestaurantCustomerBookingStatus {

	CFM("Confirmed"), EXP("Expired"), DEL("Mark deleted"), ATN("Attended and completed"), PND(
			"Pending"), REJ("Rejected"),WAIT("Waiting for confirmation"),CAN("Cancelled"),
	NAV("Table(s) not available");

	private String desc;

	private RestaurantCustomerBookingStatus(String d) {
		this.desc = d;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
