package com.sinodynamic.hkgta.util.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 返回结果的领域对象
 * 
 */
public class MessageResult implements Serializable {
	/** UID */
	private static final long serialVersionUID = -5373178385961519087L;

	/* 是否成功 */
	private boolean success;

	/* 返回码 */
	private String resultCode;

	/* 返回信息描述 */
	private String message;

	/* 属性值 */
	private Map<String, String> properties;
	private List obj;
	private Object object;

	/**
	 * 构造方法
	 */
	public MessageResult() {
		properties = new HashMap<String, String>();
	}

	

	/**
	 * Getter method for property <tt>success</tt>.
	 * 
	 * @return property value of success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * Setter method for property <tt>success</tt>.
	 * 
	 * @param success
	 *            value to be assigned to property success
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * Getter method for property <tt>resultCode</tt>.
	 * 
	 * @return property value of resultCode
	 */
	public String getResultCode() {
		return resultCode;
	}

	/**
	 * Setter method for property <tt>resultCode</tt>.
	 * 
	 * @param resultCode
	 *            value to be assigned to property resultCode
	 */
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	/**
	 * Getter method for property <tt>message</tt>.
	 * 
	 * @return property value of message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Setter method for property <tt>message</tt>.
	 * 
	 * @param message
	 *            value to be assigned to property message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public void addProperty(String key, String value) {
		properties.put(key, value);
	}

	public String getProperty(String key) {
		return properties.get(key);
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public List getObj() {
		return obj;
	}

	public void setObj(List obj) {
		this.obj = obj;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
